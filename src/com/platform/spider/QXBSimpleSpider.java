package com.platform.spider;

import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;

import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlDivision;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.util.Cookie;

public class QXBSimpleSpider {
	
	private static final Logger log = Logger.getLogger(QXBSimpleSpider.class);

	public static String searchUrl = "https://www.qixin.com/search?key=$";
	public static String detailUrl = "https://www.qixin.com/company/$";
	public static String homeUrl = "https://www.qixin.com/";
	public static String loginUrl = "http://www.qixin.com/api/user/login";

	private String companyId;
	private String companyName;
	private String detailHtmPage;
	
	private WebClient webClient;
	
	public QXBSimpleSpider (){
		
		webClient = new WebClient();
		webClient.getCookieManager().setCookiesEnabled(true);
		webClient.getOptions().setCssEnabled(true);
		webClient.getOptions().setJavaScriptEnabled(true);
		webClient.getOptions().setThrowExceptionOnScriptError(false);
		webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
		webClient.getOptions().setTimeout(5000);
	}
	
	public int grabDetailByCompanyId(String companyId){
		
		int status = -1;//默认出错
		
		try{
			
			//判断是否已经登录
			/*if(!isLogined()) {
				//登录失败
				if(!login("18868831954", "1234abcd"))
					return status;
			}*/
			
			String url = detailUrl.replace("$", companyId);
			HtmlPage detailPage = (HtmlPage) webClient.getPage(url);
			webClient.waitForBackgroundJavaScript(10000);
			
			this.detailHtmPage = handleHtmlPage(detailPage);
			
			DomElement  id = detailPage.getElementById("_companyId");
			if(id != null)
				this.companyId = id.getAttribute("value");
			DomElement  name = detailPage.getElementById("_companyName");
			if(id != null)
				this.companyName = name.getAttribute("value");
			
			status = 1;
			
		} catch (Exception e) {
			
			status = -1;
			log.error("抓取出错", e);
			e.printStackTrace();
		}
		
		return status;
	}
	
	public int grabDetailByCompanyName(String companyName){
		
		int status = -1;//默认出错
		
		try{
			
			String	searchUrl = QXBSimpleSpider.searchUrl.replace("$", 
					URLEncoder.encode(companyName, "UTF-8"));
		
			//判断是否已经登录
			if(!isLogined()) {
				//登录失败
				if(!login("18868831954", "1234abcd"))
					return status;
			}
			
			//搜索
			HtmlPage searchResultPage = webClient.getPage(searchUrl);
			webClient.waitForBackgroundJavaScript(5000);
			
			List<?> elements = searchResultPage.getByXPath("//a[@tyc-event-ch='CompanySearch.Company']");
			if(elements.isEmpty())
				return 0;
			
			HtmlAnchor a = (HtmlAnchor) elements.get(0);
			String name = a.getTextContent();
			/*if(!companyName.equals(name))
				return 0;*/
			
			this.companyName = name;
			
			String detailUrl = a.getAttribute("href");
			this.companyId = detailUrl.substring(detailUrl.lastIndexOf("/") + 1);
	
			HtmlPage detailPage = (HtmlPage) webClient.getPage(detailUrl);
			webClient.waitForBackgroundJavaScript(10000);
			
			this.detailHtmPage = handleHtmlPage(detailPage);
			
			status = 1;
			
		} catch (Exception e) {
			
			e.printStackTrace();
			status = -1;
			log.error("抓取出错", e);
		}
		
		return status;
	}
	
	private String handleHtmlPage(HtmlPage page){
		
		HtmlDivision webContent = (HtmlDivision) page.getElementById("web-content");
		HtmlDivision container = webContent.querySelector(".company_container");
		container.getChildNodes().get(0).getChildNodes().get(5).remove();
		
		HtmlDivision webTop = (HtmlDivision) page.getElementById("company_web_top");
		
		//删除浏览量数
		DomNode tempNode = webTop.querySelector("span.sec-c3");
		if(tempNode != null)
			tempNode.remove();
		
		//删除我要投诉
		tempNode = webTop.querySelector("div.new-button-sm");
		if(tempNode != null)
			tempNode.remove();
		
		//删除下载报告
		tempNode = webTop.querySelector("div.button-yel-sm");
		if(tempNode != null)
			tempNode.remove();
		
		//删除更新数据按钮
		tempNode = webTop.querySelector("a[event-name='企业详情-数据更新']");
		if(tempNode != null)
			tempNode.remove();
		
		//删除附近公司
		tempNode = webTop.querySelector("span.c9");
		if(tempNode != null)
			tempNode.remove();
		
		tempNode = webTop.querySelector("span.c9");
		if(tempNode != null)
			tempNode.remove();
		
		//删除分享栏
		tempNode = webTop.querySelector(".shareBox");
		if(tempNode != null)
			tempNode.remove();
		
		//删除所有提示小问号
		List<DomNode> nodes = container.querySelectorAll("span.describeIcon");
		if(nodes != null) {
			for(DomNode node : nodes) {
				node.remove();
			}
		}
		
		//删除天眼风险
		container.querySelector(".tyfxBox").remove();
		
		String style = "width:80%;margin-left:230px;";
		container.setAttribute("style", style);
		
		return container.asXml();
	}
	
	/**
	 * TODO 判断是否登录
	 * @return
	 * @throws Exception
	 */
	private boolean isLogined() throws Exception{
		
		boolean logined = false;
		
		Cookie cookie = webClient.getCookieManager().getCookie("auth_token");
		if(cookie != null)
			logined = true;
		
		return logined;
	}
	
	private boolean login(String userName, String password) throws Exception{
		
		boolean success = false;
		
		Map<String,String> header = new HashMap<String,String>();
	    
	    header.put("1b4872273d2048da5e29", "61fa014ce33e4db334b7045abf99c453a872c3af68f911b92dd808ae5320281ca6a702af1ae593a490f1176e3931badc0cdf2ed3339749215a38129d92992ed6");
	    header.put("Accept", "application/json,text/plain,*/*");
	    header.put("Accept-Encoding", "gzip,deflate");
	    header.put("Accept-Language", "zh-CN,zh;q=0.9");
	    //header.put("Connection", "keep-alive");
	    header.put("Content-Type", "application/json;charset=UTF-8");
	    header.put("Host", "www.qixin.com");
	    header.put("Origin", "http://www.qixin.com");
	    header.put("Referer", "http://www.qixin.com/auth/login");
	    header.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36");
	    header.put("X-Requested-With", "XMLHttpRequest");
	    
	    JSONObject json = new JSONObject();
	    json.put("acc", userName);
	    json.put("pass", password);
	    JSONObject captcha = new JSONObject();
	    captcha.put("isTrusted", true);
	    json.put("captcha", captcha);
	    
		WebRequest webRequest = new WebRequest(new URL(loginUrl));
	    webRequest.setHttpMethod(HttpMethod.POST);
	    webRequest.setAdditionalHeaders(header);
	    webRequest.setRequestBody(json.toString());
	    
	    Page page = webClient.getPage(webRequest);  
	    WebResponse webResponse = page.getWebResponse();  
	      
	    int status = webResponse.getStatusCode();
	    
	    // 读取数据内容  
	    if (status == 200) {  
	        String content = webResponse.getContentAsString();
	        System.out.println(content);
	        int returnStatus = JSONObject.fromObject(content).getInt("status");
	        if(returnStatus == 1)
	        	success = true;
	    }
	    
	    // 关闭响应流  
	    webResponse.cleanUp();
	    
	    return success;
	}
	
	public void desdroy() {
		this.webClient.close();
		this.companyId = null;
		this.companyName = null;
		this.detailHtmPage = null;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getDetailHtmPage() {
		return detailHtmPage;
	}

	public void setDetailHtmPage(String detailHtmPage) {
		this.detailHtmPage = detailHtmPage;
	}

	public WebClient getWebClient() {
		return webClient;
	}

	public void setWebClient(WebClient webClient) {
		this.webClient = webClient;
	}

}
