package com.platform.spider;

import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;

import com.gargoylesoftware.htmlunit.HttpMethod;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlDivision;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSpan;
import com.gargoylesoftware.htmlunit.util.Cookie;
import com.platform.util.EncryptUtil;

public class TYCSimpleSpider {
	
	private static final Logger log = Logger.getLogger(TYCSimpleSpider.class);

	public static String searchUrl = "https://www.tianyancha.com/search?key=$&checkFrom=searchBox";
	public static String detailUrl = "https://www.tianyancha.com/company/$";
	public static String homeUrl = "https://www.tianyancha.com/";
	public static String loginUrl = "https://www.tianyancha.com/cd/login.json";

	private String companyId;
	private String companyName;
	private String detailHtmPage;
	
	private WebClient webClient;
	
	public TYCSimpleSpider (){
		
		webClient = new WebClient();
		webClient.getCookieManager().setCookiesEnabled(true);
		webClient.getOptions().setCssEnabled(true);
		webClient.getOptions().setJavaScriptEnabled(true);
		webClient.getOptions().setThrowExceptionOnScriptError(false);
		webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
		webClient.getOptions().setTimeout(5000);
	}
	
	public int grabDetailByCompanyId(String companyId){
		
		int status = -1;//默认出错
		
		try{
			
			//判断是否已经登录
			/*if(!isLogined()) {
				//登录失败
				if(!login("18868831954", "1234abcd"))
					return status;
			}*/
			
			String url = detailUrl.replace("$", companyId);
			HtmlPage detailPage = (HtmlPage) webClient.getPage(url);
			webClient.waitForBackgroundJavaScript(10000);
			
			this.detailHtmPage = handleHtmlPage(detailPage);
			
			DomElement  id = detailPage.getElementById("_companyId");
			if(id != null)
				this.companyId = id.getAttribute("value");
			DomElement  name = detailPage.getElementById("_companyName");
			if(id != null)
				this.companyName = name.getAttribute("value");
			
			status = 1;
			
		} catch (Exception e) {
			
			status = -1;
			log.error("抓取出错", e);
			e.printStackTrace();
		}
		
		return status;
	}
	
	public int grabDetailByCompanyName(String companyName){
		
		int status = -1;//默认出错
		
		try{
			
			String	searchUrl = TYCSimpleSpider.searchUrl.replace("$", 
					URLEncoder.encode(companyName, "UTF-8"));
		
			//判断是否已经登录
			if(!isLogined()) {
				//登录失败
				if(!login("18868831954", "1234abcd"))
					return status;
			}
			
			//搜索
			HtmlPage searchResultPage = webClient.getPage(searchUrl);
			webClient.waitForBackgroundJavaScript(5000);
			
			List<?> elements = searchResultPage.getByXPath("//a[@tyc-event-ch='CompanySearch.Company']");
			if(elements.isEmpty())
				return 0;
			
			HtmlAnchor a = (HtmlAnchor) elements.get(0);
			String name = a.getTextContent();
			/*if(!companyName.equals(name))
				return 0;*/
			
			this.companyName = name;
			
			String detailUrl = a.getAttribute("href");
			this.companyId = detailUrl.substring(detailUrl.lastIndexOf("/") + 1);
	
			HtmlPage detailPage = (HtmlPage) webClient.getPage(detailUrl);
			webClient.waitForBackgroundJavaScript(10000);
			
			this.detailHtmPage = handleHtmlPage(detailPage);
			
			status = 1;
			
		} catch (Exception e) {
			
			e.printStackTrace();
			status = -1;
			log.error("抓取出错", e);
		}
		
		return status;
	}
	
	private List<Map<String, Object>> getMenuData(HtmlPage page){
		
		List<Map<String, Object>> menuData = new ArrayList<Map<String, Object>>();
		
		HtmlDivision navigation = (HtmlDivision) page.querySelector(".js-company-navigation");
		List<DomNode> nodes = navigation.querySelectorAll("div.nav_item_Box");
		
		for(DomNode node : nodes){
			
			Map<String, Object> menu = new HashMap<String, Object>();
			HtmlDivision head = node.querySelector(".headerHover");
			menu.put("menuId", head.getAttribute("onclick").split("'")[1]);
			menu.put("menuName", head.getTextContent().trim());
			
			List<Map<String, Object>> childMenuData = new ArrayList<Map<String, Object>>();
			
			HtmlDivision childMenu = node.querySelector(".nav-item-p");
			if(childMenu != null){
				List<DomNode> childList = childMenu.querySelectorAll("div");
				for(DomNode child : childList){
					Map<String, Object> childMap = new HashMap<String, Object>();
					HtmlDivision div = (HtmlDivision) child;
					
					if(div.getAttribute("class").contains("canClick")){
						childMap.put("status", "enable");
						childMap.put("menuId", div.getAttribute("onclick").split("'")[1].trim());
					} else
						childMap.put("status", "unable");
					
					childMap.put("menuName", div.getTextContent().split("\n")[1].trim());
					HtmlSpan span = div.querySelector("span");
					if(span != null)
						childMap.put("count", span.getTextContent().trim());
					else
						childMap.put("count", "");
					
					childMenuData.add(childMap);
				}
			}
			
			menu.put("childMenu", childMenuData);
			menuData.add(menu);
		}
		
		navigation.remove();
		
		return menuData;
	}
	
	private String handleHtmlPage(HtmlPage page){
		
		HtmlDivision webContent = (HtmlDivision) page.getElementById("web-content");
		HtmlDivision container = webContent.querySelector(".company_container");
		container.getChildNodes().get(0).getChildNodes().get(5).remove();
		
		HtmlDivision webTop = (HtmlDivision) page.getElementById("company_web_top");
		
		//删除浏览量数
		DomNode tempNode = webTop.querySelector("span.sec-c3");
		if(tempNode != null)
			tempNode.remove();
		
		//删除我要投诉
		tempNode = webTop.querySelector("div.new-button-sm");
		if(tempNode != null)
			tempNode.remove();
		
		//删除下载报告
		tempNode = webTop.querySelector("div.button-yel-sm");
		if(tempNode != null)
			tempNode.remove();
		
		//删除更新数据按钮
		tempNode = webTop.querySelector("a[event-name='企业详情-数据更新']");
		if(tempNode != null)
			tempNode.remove();
		
		//删除附近公司
		tempNode = webTop.querySelector("span.c9");
		if(tempNode != null)
			tempNode.remove();
		
		tempNode = webTop.querySelector("span.c9");
		if(tempNode != null)
			tempNode.remove();
		
		//删除分享栏
		tempNode = webTop.querySelector(".shareBox");
		if(tempNode != null)
			tempNode.remove();
		
		//删除所有提示小问号
		List<DomNode> nodes = container.querySelectorAll("span.describeIcon");
		if(nodes != null) {
			for(DomNode node : nodes) {
				node.remove();
			}
		}
		
		//删除天眼风险
		container.querySelector(".tyfxBox").remove();
		
		String style = "width:80%;margin-left:230px;";
		container.setAttribute("style", style);
		
		return container.asXml();
	}
	
	/**
	 * TODO 判断是否登录
	 * @return
	 * @throws Exception
	 */
	private boolean isLogined() throws Exception{
		
		boolean logined = false;
		
		Cookie cookie = webClient.getCookieManager().getCookie("auth_token");
		if(cookie != null)
			logined = true;
		
		return logined;
	}
	
	private boolean login(String userName, String password) throws Exception{
		
		boolean success = false;
		
		Map<String,String> header = new HashMap<String,String>();
	    header.put("Accept", "*/*");
	    header.put("Accept-Encoding", "gzip,deflate,br");
	    header.put("Accept-Language", "zh-CN,zh;q=0.9");
	    /*header.put("Connection", "keep-alive");*/
	    header.put("Content-Type", "application/json;charset=UTF-8");
	    header.put("Host", "www.tianyancha.com");
	    header.put("Origin", "https://www.tianyancha.com");
	    header.put("Referer", "https://www.tianyancha.com/login");
	    header.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.89 Safari/537.36");
	    header.put("X-Requested-With", "XMLHttpRequest");
	    
	    JSONObject json = new JSONObject();
	    json.put("autoLogin", "true");
	    json.put("loginway", "PL");
	    json.put("cdpassword", EncryptUtil.md5(password).toLowerCase());
	    json.put("mobile", userName);
	    
		WebRequest webRequest = new WebRequest(new URL(loginUrl));
	    webRequest.setHttpMethod(HttpMethod.POST);
	    webRequest.setAdditionalHeaders(header);
	    webRequest.setRequestBody(json.toString());
	    
	    Page page = webClient.getPage(webRequest);  
	    WebResponse webResponse = page.getWebResponse();  
	      
	    int status = webResponse.getStatusCode();
	    
	    // 读取数据内容  
	    if (status == 200) {  
	        String content = webResponse.getContentAsString();
	        System.out.println(content);
	        JSONObject data = JSONObject.fromObject(content).getJSONObject("data");
	        String token = data.getString("token");
	        
	        Cookie cookie1 = new Cookie("www.tianyancha.com", "auth_token", token);
	        Cookie cookie2 = new Cookie("www.tianyancha.com", "tyc-user-info",
	        		URLEncoder.encode(data.toString(), "UTF-8"));
	        webClient.getCookieManager().addCookie(cookie1);
	        webClient.getCookieManager().addCookie(cookie2);
	        
	        success = true;
	    }
	    
	    // 关闭响应流  
	    webResponse.cleanUp();
	    
	    return success;
	}
	
	public void desdroy() {
		this.webClient.close();
		this.companyId = null;
		this.companyName = null;
		this.detailHtmPage = null;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getDetailHtmPage() {
		return detailHtmPage;
	}

	public void setDetailHtmPage(String detailHtmPage) {
		this.detailHtmPage = detailHtmPage;
	}

	public WebClient getWebClient() {
		return webClient;
	}

	public void setWebClient(WebClient webClient) {
		this.webClient = webClient;
	}

}
