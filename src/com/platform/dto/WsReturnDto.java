package com.platform.dto;


/**
 * 行内webservice返回数据结果集
 * @author User
 *
 */
public class WsReturnDto {

	private String result;
 
	public String getResult() {
		return result;
	}
 
	public void setResult(String result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "WebserviceResultBean [result=" + result + "]";
	}
	
	
 

}
