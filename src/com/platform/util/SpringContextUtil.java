/**
 * Copyright (c) 2014-07 to forever 杭州碧湾信息技术有限公司 All Rights Reserved.  
 *
 * @project mcesys
 * @file_name SpringContextUtil.java
 * @author Rain See
 * @create_time 2016年4月10日 下午11:33:57
 * @last_modified_date 2016年4月10日
 * @version 1.0
 */
package com.platform.util;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @author Rain See
 * @create_time 2016年4月10日 下午11:33:57
 */


/**
 * 功能描述：获取spring容器，以访问容器中定义的其他bean
 */
@Component
public class SpringContextUtil implements ApplicationContextAware {

	/**
	 * TODO 以静态变量保存Spring ApplicationContext,
	 * 可在任何代码任何地方任何时候中取出ApplicaitonContext.
	 * 
	 */

	private static ApplicationContext applicationContext; // Spring应用上下文环境

	/**
	 * TODO 实现ApplicationContextAware接口的回调方法，设置上下文环境
	 * 
	 * @param applicationContext
	 * @throws BeansException
	 */
	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		System.out.println("我被初始化了==============");
		SpringContextUtil.applicationContext = applicationContext;
	}

	/**
	 * @return ApplicationContext
	 */
	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	/**
	 * TODO 获取对象
	 * 
	 * @param name
	 * @return Object 一个以所给名字注册的bean的实例
	 * @throws BeansException
	 */
	public static Object getBean(String name) throws BeansException {
		refresh();
		return applicationContext.getBean(name);
	}

	/**
	 * TODO 获取类型为requiredType的对象
	 * 如果bean不能被类型转换，相应的异常将会被抛出（BeanNotOfRequiredTypeException）
	 * 
	 * @param name
	 *            bean注册名
	 * @param requiredType
	 *            返回对象类型
	 * @return Object 返回requiredType类型对象
	 * @throws BeansException
	 */
	public static Object getBean(String name, Class<?> requiredType)
			throws BeansException {
		refresh();
		return applicationContext.getBean(name, requiredType);
	}

	/**
	 * TODO 如果BeanFactory包含一个与所给名称匹配的bean定义，则返回true
	 * 
	 * @param name
	 * @return boolean
	 */
	public static boolean containsBean(String name) {
		refresh();
		return applicationContext.containsBean(name);
	}

	/**
	 * TODO 判断以给定名字注册的bean定义是一个singleton还是一个prototype。
	 * 如果与给定名字相应的bean定义没有被找到，将会抛出一个异常（NoSuchBeanDefinitionException）
	 * 
	 * @param name
	 * @return boolean
	 * @throws NoSuchBeanDefinitionException
	 */
	public static boolean isSingleton(String name)
			throws NoSuchBeanDefinitionException {
		refresh();
		return applicationContext.isSingleton(name);
	}

	/**
	 * TODO
	 * 
	 * @param name
	 * @return Class 注册对象的类型
	 * @throws NoSuchBeanDefinitionException
	 */
	public static Class<?> getType(String name)
			throws NoSuchBeanDefinitionException {
		refresh();
		return applicationContext.getType(name);
	}

	/**
	 * TODO 如果给定的bean名字在bean定义中有别名，则返回这些别名
	 * 
	 * @param name
	 * @return
	 * @throws NoSuchBeanDefinitionException
	 */
	public static String[] getAliases(String name)
			throws NoSuchBeanDefinitionException {
		refresh();
		return applicationContext.getAliases(name);
	}

	private static void refresh() {
		/*
		 * AbstractRefreshableApplicationContext arac =
		 * (AbstractRefreshableApplicationContext) applicationContext; if
		 * (!arac.isActive()) arac.refresh();
		 */
	}
}
