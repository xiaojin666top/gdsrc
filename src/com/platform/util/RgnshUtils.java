package com.platform.util;


/**
 * 零散的操作工具类
 * @ClassName: RgnshUtils
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author xyh
 * @date 17 Jun 2020
 *
 */
public class RgnshUtils {
	/**
	 * 判断Integer是否为null或者0
	 * @param value
	 * @return
	 */
	public static boolean isEmptyInteger(Integer value){
		boolean isEmpty = false;
		if(value == null || value == 0){
			isEmpty = true;
		}
		return isEmpty;
	}

}
