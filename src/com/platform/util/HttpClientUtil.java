package com.platform.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

/**
 *  参考：https://www.cnblogs.com/luken2017/p/6386055.html
 *    https://www.2cto.com/net/201709/681448.html
 *  https://www.cnblogs.com/likaitai/p/5431246.html
 */
//有问题，get方法的url还是用的单例构造方法的url，而且代理类
// 似乎不行，思路有点莫名奇妙，把CloseableHttpClient直接拿出来用，又何必把HttpClientUtil存到map里，正常思路是注册好连接池，
//放到线程安全的map里，根据url来拿连接池，建造CloseableHttpClient，消费完回复主体后关闭。
public class HttpClientUtil {

	private static transient Logger log = LoggerFactory.getLogger(HttpClientUtil.class);

	/** 连接池 */
	private static PoolingHttpClientConnectionManager connManager = null;

	// 代理信息
	private static final String PROXY_IP = "10.5.3.9";
	private static final int PROXY_PORT = 80;

	private static int maxTotalPool = 1000;
	private static int maxConPerRoute = 20;
	private static int allTimeout = 100000;

	private static HashMap<String, HttpClientUtil> map = new HashMap<>();

	private static Object object = new Object();

	private String url = null;

	private CloseableHttpClient httpClient = null;

	public static HttpClientUtil getInstance(String strUrl) {
		HttpClientUtil instance = null;
		if ((instance = map.get(strUrl)) == null) {
			synchronized (object) {
				initConnManager();

				instance = new HttpClientUtil();
				instance.httpClient = getHttpClient(false, allTimeout);
				instance.url = strUrl;
				map.put(strUrl, instance);
			}
		}
		return instance;
	}

	public static HttpClientUtil getProxyInstance(String strUrl) {
		HttpClientUtil instance = null;
		if ((instance = map.get(strUrl)) == null) {
			synchronized (object) {
				initConnManager();

				instance = new HttpClientUtil();
				instance.httpClient = getHttpClient(true, allTimeout);
				instance.url = strUrl;
				map.put(strUrl, instance);
			}
		}
		return instance;
	}

	public static void initConnManager() {
		if (connManager == null) {
			// 创建ssl安全访问连接
			// 获取创建ssl上下文对象
			try {
				SSLContext sslContext = getSSLContext(true, null, null);
				// 注册
				Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
						.register("http", PlainConnectionSocketFactory.INSTANCE)
						.register("https", new SSLConnectionSocketFactory(sslContext)).build();
				// ssl注册到连接池
				connManager = new PoolingHttpClientConnectionManager(registry);
				connManager.setMaxTotal(maxTotalPool); // 连接池最大连接数
				connManager.setDefaultMaxPerRoute(maxConPerRoute); // 每个路由最大连接数

			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
	}

	private static SSLContext getSSLContext(boolean isDeceive, File creFile, String crePwd)
			throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, CertificateException,
			IOException {
		SSLContext sslContext = null;

		if (isDeceive) {
			sslContext = SSLContext.getInstance("SSLv3");
			// 实现一个X509TrustManager接口，用于绕过验证，不用修改里面的方法
			X509TrustManager x509TrustManager = new X509TrustManager() {
				@Override
				public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
					// Do nothing
				}

				@Override
				public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
					// Do nothing
				}

				@Override
				public X509Certificate[] getAcceptedIssuers() {
					// Do nothing
					return new X509Certificate[0];
				}
			};
			sslContext.init(null, new TrustManager[] { x509TrustManager }, null);
		} else {
			if (null != creFile && creFile.length() > 0) {
				if (null != crePwd) {
					KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
					keyStore.load(new FileInputStream(creFile), crePwd.toCharArray());
					sslContext = SSLContexts.custom().loadTrustMaterial(keyStore, new TrustSelfSignedStrategy())
							.build();
				} else {
					throw new SSLHandshakeException("整数密码为空");
				}
			}
		}

		return sslContext;
	}

	public static CloseableHttpClient getHttpClient(boolean startProxy, int timeout) {
		return getHttpClient(startProxy, timeout, timeout, timeout);
	}

	private static CloseableHttpClient getHttpClient(boolean startProxy, int connectionRquestTimeout, int connectTimeout, int socketTimeout) {
		// 配置请求参数
		RequestConfig requestConfig = RequestConfig.custom()
				.setConnectionRequestTimeout(connectionRquestTimeout)
				.setConnectTimeout(connectTimeout)
				.setSocketTimeout(socketTimeout)
				.build();

		// 配置超时回调机制
		HttpRequestRetryHandler retryHandler = new HttpRequestRetryHandler() {

			@Override
			public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
				if (executionCount >= 3) {// 如果已经重试了3次，就放弃
					return false;
				}
				if (exception instanceof NoHttpResponseException) {// 如果服务器丢掉了连接，那么就重试
					return true;
				}
				if (exception instanceof SSLHandshakeException) {// 不要重试SSL握手异常
					return false;
				}
				if (exception instanceof InterruptedIOException) {// 超时
					return true;
				}
				if (exception instanceof UnknownHostException) {// 目标服务器不可达
					return false;
				}
				if (exception instanceof ConnectTimeoutException) {// 连接被拒绝
					return false;
				}
				if (exception instanceof SSLException) {// ssl握手异常
					return false;
				}
				HttpClientContext clientContext = HttpClientContext.adapt(context);
				HttpRequest request = clientContext.getRequest();
				// 如果请求是幂等的，就再次尝试
				return !(request instanceof HttpEntityEnclosingRequest) ? true : false;
			}

		};

		HttpClientBuilder httpClientBuilder = HttpClients.custom();

		httpClientBuilder.setConnectionManager(connManager).setDefaultRequestConfig(requestConfig)
				.setRetryHandler(retryHandler);

		if (startProxy) {
			HttpHost proxy = new HttpHost(PROXY_IP, PROXY_PORT);
			DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy);
			httpClientBuilder.setRoutePlanner(routePlanner);
		}

		return httpClientBuilder.build();
	}

	public String postJson(String json) {
		HttpPost post = new HttpPost(url);
		StringEntity entity = new StringEntity(json, ContentType.APPLICATION_JSON);
		post.setEntity(entity);
		String responseContent = null;
		CloseableHttpResponse response = null;
		try {
			response = httpClient.execute(post);
			int status = response.getStatusLine().getStatusCode();
			printStatus(status);
			if (status == HttpStatus.SC_OK) {
				responseContent = EntityUtils.toString(response.getEntity(), Consts.UTF_8.name());
			}

		} catch (IOException e) {
			log.error(e.getMessage(), e);
		} finally {
			post.abort();
			post.releaseConnection();
			if (response != null) {
				try {
					EntityUtils.consume(response.getEntity());
					response.close();
				} catch (IOException e) {
					log.error(e.getMessage(), e);
				}
			}
		}
		return responseContent;
	}

	/**
	 * 发送 GET 请求（HTTP），不带输入数据
	 *
	 * @param url
	 * @return
	 */
	public String doGet(String url) {
		return doGet(new HashMap<String, Object>());
	}

	/**
	 * 发送 GET 请求（HTTP），K-V形式
	 *
	 * @param
	 * @param params
	 * @return
	 */
	public String doGet(Map<String, Object> params) {
		String apiUrl = url;
		StringBuilder param = new StringBuilder();
		int i = 0;
		if(params!=null && params.size()!=0){
			for (Map.Entry<String, Object> entry : params.entrySet()) {
				if (i == 0) {
					param.append("?");
				} else {
					param.append("&");
				}
				param.append(entry.getKey()).append("=").append(entry.getValue());
				i++;
			}
			apiUrl += param;
		}



		String result = null;
		CloseableHttpResponse response = null;
		HttpGet httpGet = null;
		try {
			httpGet = new HttpGet(apiUrl);
//			httpGet.setHeader("Authorization","69f6acbe-215d-4a5c-8dcb-d8ac36ac6810");
			response = httpClient.execute(httpGet);
			int status = response.getStatusLine().getStatusCode();
			printStatus(status);
			log.info(EntityUtils.toString(response.getEntity(), Consts.UTF_8.name()));
			if (status == HttpStatus.SC_OK) {
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					result = EntityUtils.toString(response.getEntity(), Consts.UTF_8.name());
				}
			}
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		} finally {
			httpGet.abort();
			httpGet.releaseConnection();
			if (response != null) {
				try {
					EntityUtils.consume(response.getEntity());
					response.close();
				} catch (IOException e) {
					log.error(e.getMessage(), e);
				}
			}
		}

		return result;
	}

	public static String httpGet(String url,Map<String, Object> params) throws Exception{
		String apiUrl = url;
		StringBuilder param = new StringBuilder();
		int i = 0;
		if(params!=null && params.size()!=0){
			for (Map.Entry<String, Object> entry : params.entrySet()) {
				if (i == 0) {
					param.append("?");
				} else {
					param.append("&");
				}
				param.append(entry.getKey()).append("=").append(entry.getValue());
				i++;
			}
			apiUrl += param;
		}
		//初始化HttpClient
		CloseableHttpClient httpClient = HttpClients.createDefault();
		//创建HttpGet
		HttpGet httpGet = new HttpGet(apiUrl);
		//发起请求，获取response对象
		CloseableHttpResponse response = httpClient.execute(httpGet);
		//获取请求状态码
		//response.getStatusLine().getStatusCode();
		//获取返回数据实体对象
		HttpEntity entity = response.getEntity();
		//转为字符串
		String result = EntityUtils.toString(entity,"UTF-8");
		System.out.println(result);
		return result;

	}

	/**
	 * 发送 POST 请求（HTTP），K-V形式
	 *
	 * @param
	 *
	 * @param params
	 *            参数map
	 * @return
	 */
	public String doPost(Map<String, String> params) {
		String result = null;
		HttpPost httpPost = new HttpPost(url);
		CloseableHttpResponse response = null;
		try {
			List<NameValuePair> pairList = new ArrayList<>(params.size());
			for (Map.Entry<String, String> entry : params.entrySet()) {
				NameValuePair pair = new BasicNameValuePair(entry.getKey(), entry.getValue());
				pairList.add(pair);
			}
			httpPost.setEntity(new UrlEncodedFormEntity(pairList, Consts.UTF_8.name()));
			response = httpClient.execute(httpPost);
			int status = response.getStatusLine().getStatusCode();
			printStatus(status);
			if (status == HttpStatus.SC_OK) {
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					result = EntityUtils.toString(response.getEntity(),  Consts.UTF_8.name());
				}
			}

		} catch (IOException e) {
			log.error(e.getMessage(), e);
		} finally {
			httpPost.abort();
			httpPost.releaseConnection();
			if (response != null) {
				try {
					EntityUtils.consume(response.getEntity());
					response.close();
				} catch (IOException e) {
					log.error(e.getMessage(), e);
				}
			}
		}
		return result;
	}
	
	/**
     * 使用httpclint 发送文件
     * @author: qingfeng
     * @date: 2019-05-27
     * @param file
     *            上传的文件
     * @return 响应结果
     */
    public static String uploadFile(String url ,MultipartFile file,String fileParamName,Map<String,String>headerParams,Map<String,String>otherParams) {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        String result = "";
        try {
            String fileName = file.getOriginalFilename();
            HttpPost httpPost = new HttpPost(url);
            //添加header
            for (Map.Entry<String, String> e : headerParams.entrySet()) {
                httpPost.addHeader(e.getKey(), e.getValue());
            }
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setCharset(Charset.forName("utf-8"));
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);//加上此行代码解决返回中文乱码问题
            builder.addBinaryBody(fileParamName, file.getInputStream(), ContentType.MULTIPART_FORM_DATA, fileName);// 文件流
            for (Map.Entry<String, String> e : otherParams.entrySet()) {
                builder.addTextBody(e.getKey(), e.getValue());// 类似浏览器表单提交，对应input的name和value
            }
            HttpEntity entity = builder.build();
            httpPost.setEntity(entity);
            HttpResponse response = httpClient.execute(httpPost);// 执行提交
            HttpEntity responseEntity = response.getEntity();
            if (responseEntity != null) {
                // 将响应内容转换为字符串
                result = EntityUtils.toString(responseEntity, Charset.forName("UTF-8"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                httpClient.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
	

	private void printStatus(int status) {
		log.info("返回状态：{}", status);
	}

	public static void main(String[] aggs){
		HttpClientUtil instance = getProxyInstance("https://www.baidu.com/");
		String respongse = instance.doGet("");
		log.info(respongse);
	}
}







