package com.platform.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class ExcelUtil {

	/**
	 * 读取cell单元格的值，如果为日期格式，进行转换
	 * 
	 * @param cell
	 * @return
	 */
	public static String getCellValue(Cell cell) {
		if (cell == null)
			return "";
		if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
			return cell.getStringCellValue();
		} else if (cell.getCellType() == Cell.CELL_TYPE_BOOLEAN) {
			return String.valueOf(cell.getBooleanCellValue());
		} else if (cell.getCellType() == Cell.CELL_TYPE_FORMULA) {
			String strCell = "";
			try {
				strCell = String.valueOf(cell.getStringCellValue());
			} catch (IllegalStateException e) {
				strCell = String.valueOf(cell.getNumericCellValue());
			}
			return strCell;
		} else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
			short format = cell.getCellStyle().getDataFormat();
			// System.out.println("format:"+format+";;;;;value:"+cell.getNumericCellValue());
			SimpleDateFormat sdf = null;
			if (format == 14 || format == 31 || format == 57 || format == 58 || (176 <= format && format <= 178)
					|| (182 <= format && format <= 196) || (210 <= format && format <= 213) || (208 == format)) { // 日期
				sdf = new SimpleDateFormat("yyyy-MM-dd");
			} else if (format == 20 || format == 32 || format == 183 || (200 <= format && format <= 209)) { // 时间
				sdf = new SimpleDateFormat("HH:mm");
			} else { // 不是日期格式
				return String.valueOf(cell.getNumericCellValue());
			}
			double value = cell.getNumericCellValue();
			Date date = org.apache.poi.ss.usermodel.DateUtil.getJavaDate(value);
			if (date == null || "".equals(date)) {
				return "";
			}
			String result = "";
			try {
				result = sdf.format(date);
			} catch (Exception e) {
				e.printStackTrace();
				return "";
			}
			return result;
		}
		return "";
	}

	public static String getStringValue(Cell cell) {
		cell.setCellType(cell.CELL_TYPE_STRING);
		return String.valueOf(cell.getStringCellValue());
	}

	public static CellStyle getTitle(Workbook wb) {
		return getStyle(wb, IndexedColors.CORNFLOWER_BLUE.getIndex(), null, null, null, null, null, null);
	}

	public static CellStyle getCensus(Workbook wb) {
		return getStyle(wb, IndexedColors.TAN.getIndex(), null, null, null, null, null, null);
	}

	/**
	 * 返回 单元格样式
	 * 
	 * @param wb
	 * @param groundColor  背景颜色
	 * @param algin        水平位置
	 * @param vertical     上下位置
	 * @param bottomBorder 下边框
	 * @param leftBorder   左边框
	 * @param topBorder    上边框
	 * @param rightBorder  右边框
	 * @return
	 */
	public static CellStyle getStyle(Workbook wb, Short groundColor, Short algin, Short vertical, Short bottomBorder,
			Short leftBorder, Short topBorder, Short rightBorder) {
		CellStyle setBorder = wb.createCellStyle();
		if (groundColor != null && !"".equals(groundColor)) {
			setBorder.setFillForegroundColor(groundColor);
			setBorder.setFillPattern(CellStyle.SOLID_FOREGROUND);
		}
		if (algin != null && !"".equals(algin)) {
			setBorder.setAlignment(algin); // 水平居中
		}
		if (vertical != null && !"".equals(vertical)) {
			setBorder.setVerticalAlignment(vertical); // 上下位置
		}
		if (bottomBorder != null && !"".equals(bottomBorder)) {
			setBorder.setBorderBottom(bottomBorder);
		}
		if (leftBorder != null && !"".equals(leftBorder)) {
			setBorder.setBorderLeft(leftBorder);
		}
		if (topBorder != null && !"".equals(topBorder)) {
			setBorder.setBorderTop(topBorder);
		}
		if (rightBorder != null && !"".equals(rightBorder)) {
			setBorder.setBorderRight(rightBorder);
		}
		return setBorder;
	}
	
	
	/****************导出excel的工具类***********************/
	
	/**
	 * 
	 *@Description 获取标题栏的字体样式
	 *             加粗、背景为灰色
	 *@param workbook
	 *@return
	 * @author xyh
	 */
	public static CellStyle getHeadCellStyle(Workbook workbook) {
		// 表头样式
		CellStyle headStyle = workbook.createCellStyle();
		//字体样式
		Font headFront = workbook.createFont();
		headFront.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);//加粗
		headFront.setFontName("宋体");//字体名字
		headFront.setFontHeightInPoints((short)12);//字号
		//设置cell的字体，以及背景颜色的设置
		headStyle.setFont(headFront);
		headStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		headStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		headStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());  
		headStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		headStyle.setBorderTop(CellStyle.BORDER_THIN);
		headStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headStyle.setBorderTop(CellStyle.BORDER_THIN);
		headStyle.setBorderRight(CellStyle.BORDER_THIN);
		headStyle.setBorderLeft(CellStyle.BORDER_THIN);
		return headStyle;
	}
	
	public static CellStyle getCellStyle(Workbook workbook) {
		// 单元格样式
		CellStyle cellStyle = workbook.createCellStyle();
		//字体
		Font cellFront = workbook.createFont();
		cellFront.setFontName("宋体");
		cellFront.setFontHeightInPoints((short)12);
		//行样式
		cellStyle.setFont(cellFront);
		cellStyle.setBorderBottom(CellStyle.BORDER_THIN); //下边框    
		cellStyle.setBorderLeft(CellStyle.BORDER_THIN);//左边框    
		cellStyle.setBorderTop(CellStyle.BORDER_THIN);//上边框    
		cellStyle.setBorderRight(CellStyle.BORDER_THIN);//右边框    
		cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		cellStyle.setWrapText(true);
		return cellStyle;
	}
	
	/**
	 * 
	 *@Description 创建标题栏，可指定不同样式
	 *@param rowNum 标题栏所在行号
	 *@param cellStyle 标题栏样式
	 *@param sheet
	 *@param heads 标题数组
	 * @author xyh
	 */
	public static void createHeadRow(int rowNum,CellStyle cellStyle,Sheet sheet,String[] heads){
		Row header = sheet.createRow(rowNum);
		for(int i = 0 ;i < heads.length;i++){
			Cell cell = header.createCell(i);
			cell.setCellValue(heads[i]);
			cell.setCellStyle(cellStyle);
		}
	}
	
	
	
	
	
}
