package com.platform.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;
import java.util.zip.ZipOutputStream;
import java.util.zip.ZipEntry;


public class ZipCompressUtil {
	public static void specifiedFilesZip(String sourcefilePath,String zipFileName,List<String> specifiedFiles)throws Exception{
		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFileName));
		File sourceFile = new File(sourcefilePath);
		
		compress(out,sourceFile,sourceFile.getName(),specifiedFiles);
		
		out.close();
		
	}

	private static void compress(ZipOutputStream out, File sourceFile, String basePath, List<String> specifiedFiles) throws Exception {
		
		if(sourceFile.isDirectory()){
			File[] flist = sourceFile.listFiles();
			
			if(flist.length!=0){
				for(int i = 0; i < flist.length; i++){
					if(flist[i].isDirectory()){
						compress(out, flist[i], basePath + File.separator + flist[i].getName(), specifiedFiles);
					} else{
						String fileName = basePath + File.separator + flist[i].getName();
						for(String specifiedFileName : specifiedFiles){
							if(fileName.contains(specifiedFileName)){
								compress(out, flist[i], fileName, specifiedFiles);	
							}
						}
					}
				}
			}
			
		}else{
			out.putNextEntry(new ZipEntry(basePath));
			FileInputStream fos = new FileInputStream(sourceFile);
			BufferedInputStream bis = new BufferedInputStream(fos);
			
			int len;
			byte[] buf = new byte[1024];
			while((len = bis.read(buf,0,1024))!=-1){
				out.write(buf, 0, len);
			}
			bis.close();
			fos.close();
		}
	}
}
