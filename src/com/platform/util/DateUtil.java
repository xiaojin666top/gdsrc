package com.platform.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang3.StringUtils;

import oracle.sql.TIMESTAMP;

import com.beawan.common.Constants;
import com.beawan.common.ServiceManager;

/**
 * 日期操作工具类
 * 
 */
public class DateUtil {
	public static final int SKIP_TYPE_SECOND = 1;// 秒

	public static final int SKIP_TYPE_MINUTE = 2;// 分

	public static final int SKIP_TYPE_HOUR = 3;// 小时

	public static final int SKIP_TYPE_DAY = 4;// 天
	
	
	/**
	 * TODO 获取两个日期间的差值
	 * @param startDate 起始日期
	 * @param endDate 结束日期
	 * @param type 返回差值类型，默认毫秒，1：秒，2：分，3：时，4：天
	 * @return
	 */
	public static long getTime(Date startDate, Date endDate, int type){
		
		if (startDate == null || endDate == null ) {
			return -1;
		}
		long time = endDate.getTime() - startDate.getTime();
		
		long result = 0;
		switch (type) {
		case SKIP_TYPE_SECOND:
			result = time / 1000;
			break;
		case SKIP_TYPE_MINUTE:
			result = time / 60000;
			break;
		case SKIP_TYPE_HOUR:
			result = time / 3600000;
			break;
		case SKIP_TYPE_DAY:
			result = time / 43200000;
			break;
		default:
			result = time;
		}
		
		return result;
	}

	/**
	 * 获取时间差
	 * 
	 * @param date1
	 * @param date2
	 * @param skipType
	 * @return 返回时间差,可以返回负数
	 */
	public static int getSkip(Date date1, Date date2, int skipType)
			throws Exception {
		if (date1 == null || date2 == null || skipType < 1 || skipType > 4) {
			throw new Exception("参数传递出错！");
		}
		long diff = date1.getTime() - date2.getTime();
		int flag = 1;
		if (diff < 0) {
			flag = -1;
		}
		int result = 0;
		switch (skipType) {
		case SKIP_TYPE_SECOND:
			result = (int) diff / 1000;
			break;
		case SKIP_TYPE_MINUTE:
			result = (int) diff / 60000;
			break;
		case SKIP_TYPE_HOUR:
			result = (int) diff / 3600000;
			break;
		case SKIP_TYPE_DAY:
			result = (int) diff / 43200000;
			break;
		}
		return flag * result;
	}
	/**
	 * 字符串转化成不带时分秒格式格式的日期
	 * @param dateStr 日期字符串
	 * @return
	 * @throws Exception
	 */
	public static Date parseDate(String dateStr) throws Exception {
		return parse(dateStr, Constants.DATE_MASK);
	}
	
	
	/**
	 * 字符串转化成指定格式的日期
	 * @param dateStr 日期字符串
	 * @param format 指定格式
	 * @return
	 * @throws Exception
	 */
	public static Date parseDate(String dateStr, String format) {
		Date date = null;
		try {
			date =  parse(dateStr, format);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}
	
	
	/**
	 * 字符串转化成指定格式的日期，不指定格式返回不带时分秒格式
	 * @param dateStr 日期字符串
	 * @param format 指定格式
	 * @return
	 * @throws Exception
	 */
	public static Date parse(String dateStr, String format) throws Exception {
		if (dateStr == null || dateStr.equals(""))
			return null;

		if (format == null || format.equals(""))
			format = Constants.DATE_MASK;

		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.parse(dateStr);
	}
	/**
	 * 转换日期到指定格式的字符串
	 * @param date
	 * @param format
	 * @return
	 * @throws Exception
	 */
	public static String format(Date date, String format) throws Exception {
		return new SimpleDateFormat(format).format(date);
	}
	
	public static String formatDate(Date date, String format) {
		String str = null;
		if(date != null){
			str = new SimpleDateFormat(format).format(date);
		}
		return str;
	}
	
	/**
	 * TODO 格式转换
	 * @param dateStr 日期字符串
	 * @param sourceFormat 源格式
	 * @param targetFormat 目标格式
	 * @return
	 * @throws Exception
	 */
	public static String convert(String dateStr, String sourceFormat,
			String targetFormat) throws Exception {
		if(dateStr == null || sourceFormat == null || targetFormat == null)
			return dateStr;
		Date date = DateUtil.parse(dateStr, sourceFormat);
		return new SimpleDateFormat(targetFormat).format(date);
	}
	
	public static String convertNoException(String dateStr, String sourceFormat,
			String targetFormat) {
		
		if(dateStr == null || sourceFormat == null || targetFormat == null)
			return dateStr;
		
		String result = dateStr;
		
		try {
			Date date = DateUtil.parse(dateStr, sourceFormat);
			result = new SimpleDateFormat(targetFormat).format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	/**
	 * 转换日期到字符串,如果带时分秒则显示时分秒,如果不带则不显示时分秒
	 */
	public static String formatDate2String(Date date) {
		String str = null;
		try {
			str = DateUtil.format(date, Constants.DATETIME_MASK);
			int pos = str.indexOf(" 00:00:00");
			if (pos >= 0) {
				str = str.substring(0, pos);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return str;
	}

	/**
	 * 获取数据库时间
	 * 
	 * @return DATE
	 * @throws Exception
	 */
	public static Date getSystemTime() throws Exception {
		Date systemTime = null;
		systemTime = ServiceManager.getSysTime(Constants.DataSource.TCE_DS);
		return systemTime;
	}
	
	/**
	 * 获取当前服务器时间
	 * @return
	 */
	public static String getServerTime(){
		Date date = new Date();
		return formatDate2String(date);
	}

	public static String getDateByTime(Long timeLong) {
		if (timeLong > 0) {
			int dd = (int) (timeLong / 1000 / 60 / 60 / 8);// 计算剩余的天数
			int hh = (int) (timeLong / 1000 / 60 / 60 % 8);// 计算剩余的小时数
			int mm = (int) (timeLong / 1000 / 60 % 60);// 计算剩余的分钟数
			// int ss = (int)(timeLong / 1000 % 60);//计算剩余的秒数
			return dd + "工作日" + hh + "小时" + mm + "分钟";
		}
		return null;
	}

	/**
	 * 根据字符串获得Calendar对象
	 */
	public static Calendar getCalendarByString(String timeString,
			String formatString) throws Exception {
		Calendar c = new GregorianCalendar();
		DateFormat format = new SimpleDateFormat(formatString);
		Date d = format.parse(timeString);
		c.setTime(d);
		return c;
	}

	/**
	 * 判断是否是周六
	 * 
	 * @return
	 */
	public static boolean isSaturday(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int week = cal.get(Calendar.DAY_OF_WEEK) - 1;
		if (week == 6) {// 0代表周日，6代表周六
			return true;
		}
		return false;
	}
	
	/**
	 * 判断是否是周日
	 * 
	 * @return
	 */
	public static boolean isSunday(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int week = cal.get(Calendar.DAY_OF_WEEK) - 1;
		if (week == 0) {// 0代表周日，6代表周六
			return true;
		}
		return false;
	}
	
	/**
	 * 判断是否是周末
	 * 
	 * @return
	 */
	public static boolean isWeekend(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int week = cal.get(Calendar.DAY_OF_WEEK) - 1;
		if (week == 6 || week == 0) {// 0代表周日，6代表周六
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description: 两个日期间隔天数
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static int getDaysBetween(Date startDate, Date endDate) {
		Calendar fromCalendar = Calendar.getInstance();
		fromCalendar.setTime(startDate);
		fromCalendar.set(Calendar.HOUR_OF_DAY, 0);
		fromCalendar.set(Calendar.MINUTE, 0);
		fromCalendar.set(Calendar.SECOND, 0);
		fromCalendar.set(Calendar.MILLISECOND, 0);

		Calendar toCalendar = Calendar.getInstance();
		toCalendar.setTime(endDate);
		toCalendar.set(Calendar.HOUR_OF_DAY, 0);
		toCalendar.set(Calendar.MINUTE, 0);
		toCalendar.set(Calendar.SECOND, 0);
		toCalendar.set(Calendar.MILLISECOND, 0);

		return (int)(toCalendar.getTime().getTime() - fromCalendar.getTime()
				.getTime()) / (1000 * 60 * 60 * 24) + 1;
	}

	/**
	 * 获取两个日期之间的周末的天数
	 * 
	 * @param startDate
	 * @param endDate
	 * @return 间隔的天数
	 */
	public static int getDutyDays(Date startDate, Date endDate)
			throws Exception {
		int result = 0;
		startDate = parse(formatDate2String(startDate), "yyyy-MM-dd");
		endDate = parse(formatDate2String(endDate), "yyyy-MM-dd");
		while (startDate.compareTo(endDate) <= 0) {
			if (isWeekend(startDate)) {
				result++;
			}
			startDate = addDay(startDate, 1); // 这个时间就是日期往后推一天的结果
		}
		return result;
	}

	/**
	 * 获取两个日期之间的工作日的天数（包括起始日和结束日）
	 * 
	 * @param startDate
	 * @param endDate
	 * @return 工作日的天数
	 */
	public static int getDutyWeekDays(Date startDate, Date endDate)
			throws Exception {
		int result = 0;
		startDate = parse(formatDate2String(startDate), "yyyy-MM-dd");
		endDate = parse(formatDate2String(endDate), "yyyy-MM-dd");
		Date temp = startDate;
		while (temp.compareTo(endDate) <= 0) {
			if (!isWeekend(temp)) {
				result++;
			}
			temp = addDay(temp, 1); // 这个时间就是日期往后推一天的结果
		}
		return result;
	}

	/**
	 * 功能描述：返回年
	 * @param date 日期
	 * @return 返回年
	 */
	public static int getYear(Date date) {
		Calendar calendar = Calendar.getInstance();
		if (date != null)
			calendar.setTime(date);
		return calendar.get(Calendar.YEAR);
	}

	/**
	 * 功能描述：返回月
	 * @param date 日期
	 * @return 返回月
	 */
	public static int getMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		if (date != null)
			calendar.setTime(date);
		return calendar.get(Calendar.MONTH);
	}

	/**
	 * 功能描述：返回日
	 * @param date 日期
	 * @return 返回日
	 */
	public static int getDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		if (date != null)
			calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_MONTH);
	}

	/**
	 * 功能描述：返回小时
	 * @param date 日期
	 * @return 返回小时
	 */
	public static int getHour(Date date) {
		Calendar calendar = Calendar.getInstance();
		if (date != null)
			calendar.setTime(date);
		return calendar.get(Calendar.HOUR_OF_DAY);
	}

	/**
	 * 功能描述：返回分钟
	 * @param date 日期
	 * @return 返回分钟
	 */
	public static int getMinute(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.MINUTE);
	}

	/**
	 * 功能描述：返回今年第几个星期
	 * 
	 * @param date 日期
	 * @return 返回分钟
	 */
	public static int getWeek(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.WEEK_OF_YEAR);
	}

	/**
	 * 功能描述：日期往后移num天
	 * 
	 * @param date  日期
	 * @return 返回日期
	 */
	public static Date addDay(Date date, int num) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, num);// 把日期往后增加一天.整数往后推,负数往前移动
		return calendar.getTime(); // 这个时间就是日期往后推一天的结果
	}
	
	/**
	 * 功能描述：日期往后移num月
	 * 
	 * @param date  日期
	 * @return 返回日期
	 */
	public static Date addMonth(Date date, int num) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, num);// 把日期往后增加num月.整数往后推,负数往前移动
		return calendar.getTime(); // 这个时间就是日期往后推num月的结果
	}

	/**
	 * 功能描述：日期往后移num年
	 * 
	 * @param date  日期
	 * @return 返回日期
	 */
	public static Date addYear(Date date, int num) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.YEAR, num);// 把日期往后增加num年.整数往后推,负数往前移动
		return calendar.getTime(); // 这个时间就是日期往后推num年的结果
	}
	
	/**
	 * @Title: calculAges
	 * @Description: 计算周年
	 * @param birthday
	 * @param nowDate
	 * @return
	 */
	public static int calculAnniversary(Date birthday, Date nowDate) {
		int ages = 0;
		if(birthday == null)
			return -1;
		Calendar nowCal = Calendar.getInstance();
		nowCal.setTime(nowDate);
		int nowYear = nowCal.get(Calendar.YEAR);
		int nowMonth = nowCal.get(Calendar.MONTH) + 1;
		int nowDay = nowCal.get(Calendar.DAY_OF_MONTH);
		Calendar birthCal = Calendar.getInstance();
		birthCal.setTime(birthday);
		int birthYear = birthCal.get(Calendar.YEAR);
		int birthMonth = birthCal.get(Calendar.MONTH) + 1;
		int birthDay = birthCal.get(Calendar.DAY_OF_MONTH);
		ages = nowYear - birthYear;
		if (nowMonth < birthMonth) {
			ages--;
		} else if (nowMonth == birthMonth) {
			if (nowDay < birthDay) {
				ages--;
			}
		}
		return ages;
	}

	public static int getDays(String str1, String str2, String format) {
		int days = -1;

		if (str1 == null || str1.equals("") || str2 == null || str2.equals(""))
			return -1;

		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			Date d1 = sdf.parse(str1);
			Date d2 = sdf.parse(str2);
			days = (int) ((d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return days;
	}

	public static int getDays(Date d1, Date d2) {
		if (d1 == null || d2 == null)
			return -1;

		int day = (int) ((d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));

		return day;
	}
	
	public static long getDayStart(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
			try {
				String curDay = sdf.format(date);
				date = sdf.parse(curDay);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		
		return date.getTime();
	}
	
	public static String getLastMonthFirstDay(String dateStr) {

		Calendar cal = Calendar.getInstance();
		if (dateStr != null && !"".equals(dateStr)) {
			try {
				Date d = DateUtil.parse(dateStr, "yyyy-MM-dd");
				cal.setTime(d);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;

		if (month == 1) {
			month = 12;
			year--;
		} else
			month--;

		String date = "" + year + "-";
		if (month < 10)
			date += "0";

		date += month + "-01";

		return date;
	}
	
	public static String getLastDay(String dateStr){
		Date date = new Date();
		if (dateStr != null && !"".equals(dateStr)) {
			try {
				date = DateUtil.parse(dateStr, "yyyy-MM-dd");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		long time = date.getTime()-24*60*60*1000;
		date = new Date(time);
		return new SimpleDateFormat("yyyy-MM-dd").format(date);
	}
	
	public static boolean isMonthEndDay(String date) {
		boolean is = false;
		try {
			Date d = DateUtil.parse(date, "yyyyMMdd");
			long nt = d.getTime() + 24 * 60 * 60 * 1000;
			String ndStr = DateUtil.format(new Date(nt), "yyyyMMdd");
			if ("01".equals(ndStr.subSequence(6, 8))) {
				is = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return is;
	}
	
	/**
	 * 给出起始日期、间隔天数，获得结束日期
	 * @param date 开始日期
	 * @param interval 时间间隔
	 * @param iflag 间隔单位，5：天，2：月，1：年
	 * @return 返回Date对象
	 */
	public static Date moveDateTo(Date date , int interval , int iflag){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		// 把日期往后增加
		calendar.add(iflag, interval);
		return calendar.getTime();
	}
	
	/**
	 * 给出起始日期、间隔天数
	 * @param date 开始日期 Date对象
	 * @param days 间隔天数
	 * @param format 时间格式化
	 * @return 返回格式化的日期字符串
	 */
	public static String moveDateTo(Date date,int days,String format){
		if(date == null)
			return null;
		Date targetDate = moveDateTo(date,days,Calendar.DATE);
		return new SimpleDateFormat(format).format(targetDate);
	}
	
	/**
	 * 给出起始日期、间隔天数
	 * @param date 开始日期
	 * @param days 间隔天数
	 * @param format 时间格式化
	 * @return 返回格式化的日期字符串
	 * @throws ParseException
	 */
	public static String moveDateTo(String date,int days,String format) throws ParseException{
		Date tempDate = new SimpleDateFormat(format).parse(date);
		return moveDateTo(tempDate ,days,format);
	}
	
	/**
	 * 给出起始日期、间隔月数，获得结束日期
	 * @param startDate 开始日期
	 * @param months 间隔天数
	 * @return 返回Date对象
	 */
	public static Date toDateByMonth(Date startDate , int months){
		return moveDateTo(startDate,months,Calendar.MONTH);
	}
	
	/**
	 * 给出起始日期
	 * @param date 开始日期
	 * @param months 时间月数
	 * @param format 时间格式化
	 * @return 返回格式化的日期字符串
	 * @throws ParseException
	 */
	public static String toDateByMonth(String date , int months , String format) {
		
		String result = null;
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		
		try {
			Date tempDate = sdf.parse(date);
			Date targetDate = moveDateTo(tempDate,months,Calendar.MONTH);
			result = sdf.format(targetDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	/**
	 * TODO 将yyyy年MM月dd日格式转换为yyyy-MM-dd
	 * @param dateStr
	 * @return
	 */
	public static String replace(String dateStr){
		if(!StringUtil.isEmptyString(dateStr))
			dateStr = dateStr.replace("年", "-").replace("月", "-").replace("日", "");
		return dateStr;
	}
	
	
	public static Date oracleSqlTimestamp2Date(Object sqlTimestamp)throws Exception{
		TIMESTAMP timestamp = (TIMESTAMP)sqlTimestamp;
		Date date = new Date(timestamp.timestampValue().getTime());
		return date;
	}
	
	/**
	 * TODO 年份加减计算
	 * @return
	 */
	public static String dateCountYearNoMonth(String str,int i) throws Exception {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        Date dt = sdf.parse(str);//将字符串生成Date
        Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(dt);//使用给定的 Date 设置此 Calendar 的时间。
        rightNow.add(Calendar.YEAR, i);// 计算
        Date dt1 = rightNow.getTime();//返回一个表示此 Calendar 时间值的 Date 对象。
        String reStr = sdf.format(dt1);//将给定的 Date 格式化为日期/时间字符串，并将结果添加到给定的 StringBuffer。
        return reStr;
    }
	
	public static String getNow() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmsss");
		return sdf.format(new Date());
	}

	public static String getNowY_m_dStr() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(new Date());
	}
	

	public static String getNowYmdStr() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		return sdf.format(new Date());
	}
	

	public static String getNowDatetime() {
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATETIME_MASK);
		return sdf.format(new Date());
	}

	/**
	 * 获取当前时间戳
	 * @return
	 */
	public static Timestamp getNowTimestamp(){
		Timestamp timestamp = new Timestamp(new Date().getTime());
		return timestamp;
	}

	/**
	 * 将 yyyy-MM-dd转化成时间戳
	 * @param str
	 * @return
	 */
	public static Timestamp parseYmdTimestamp(String str) throws Exception{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = sdf.parse(str);
		Timestamp timestamp = new Timestamp(date.getTime());
		return timestamp;
	}
	
	public static String parseTimestamp(long timestamp, String format){
		return new SimpleDateFormat(format).format(timestamp);
	}

	public static String parseTimestamp(long timestamp){
		return new SimpleDateFormat(Constants.DATETIME_MASK).format(timestamp);
	}
	/**
	 * 获取多少月之前或者之后的Timestamp
	 * @param month    正数 之后  负数 之前
	 * @return
	 */
	public static Timestamp getLastMonthTimestamp(Integer month){
		Date resultDate = addMonth(new Date(),month);
		Timestamp timestamp = new Timestamp(resultDate.getTime());
		return timestamp;
	}

	/**
	 * 获取多少天之前或者之后的Timestamp
	 * 正数 之后
	 * 负数 之前
	 * @param day
	 * @return
	 */
	public static Timestamp getAddDayTimestamp(Integer day){
		Date resultDate = addDay(new Date(),day);
		Timestamp timestamp = new Timestamp(resultDate.getTime());
		return timestamp;
	}

	/**
	 * 获取给定时间段所处 月份、季度、半年、年
	 * @param startTime 起始日期
	 * @param endTime 结束日期
	 * @param format 日期字符串格式，如"yyyy-MM-dd"
	 * @return
	 */
	public static String getTimeLocation(String startTime, String endTime, String format) {
		if (startTime == null || "".equals(startTime) || endTime == null || "".equals(endTime))
			return LocalDate.now().getYear() + "年"; // 返回当前年份
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
		LocalDate start = LocalDate.parse(startTime, formatter);
		LocalDate end = LocalDate.parse(endTime, formatter);
		int year = start.getYear();
		int startMonth = start.getMonthValue();
		int monthDiff = end.getMonthValue() - start.getMonthValue();

		if (monthDiff == 0){                        // 月
			return year + "年" + startMonth + "月";
		}else if (monthDiff > 0 && monthDiff <= 3){ // 季度
			if (startMonth <= 3) {
				return year + "年第1季度";
			}else if (startMonth <= 6){
				return year + "年第2季度";
			}else if (startMonth <= 9){
				return year + "年第3季度";
			}else {
				return year + "年第4季度";
			}
		} else if (monthDiff > 3 && monthDiff <= 6){ // 半年度
			if (startMonth <= 6) {
				return year + "年上半年";
			}else {
				return year + "年下半年";
			}
		}else if (monthDiff > 6){   	// 年度
			return year + "年年度";
		}else {
			return year + "年";
		}
	}

	/**
 	* 获取给定时间段所处 月份、季度、半年、年
 	*/
	public static String getTimeLocation(String startTime, String endTime) {
		return getTimeLocation(startTime, endTime, Constants.DATE_MASK);
	}

	/**
	 * 获取上几月的第一天 (相对于当前时间)
	 * @return
	 */
	public static String getLastFirstData(int index){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -index);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		Date date = calendar.getTime();
		String str = null;
		try {
			str = format(date, "yyyy-MM-dd");
		}catch (Exception e){
			e.printStackTrace();
		}
		return str;
	}


	/**
	 * 获取上几月的最后一天(相对于当前时间)
	 * @return
	 */
	public static String getLastEndData(int index){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -index);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		Date date = calendar.getTime();
		String str = null;
		try {
			str = format(date, "yyyy-MM-dd");
		}catch (Exception e){
			e.printStackTrace();
		}
		return str;
	}

	/**
	 * 获取当前年的第一天
	 * @return
	 */
	public static String getCurrYearFirst(){
		Calendar currCal=Calendar.getInstance();
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Calendar.YEAR, currCal.get(Calendar.YEAR));
		Date time = calendar.getTime();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(time);
	}

	/**
	 * 根据出生日期获取年龄
	 * @param birthday 出生日期 "yyyy-MM-dd"
	 * @return age
	 */
	public static int getAgeByBirthday(String birthday){
		int age = 0;
		if (StringUtil.isEmptyString(birthday)) return 0;
		try {
			Calendar now = Calendar.getInstance();
			now.setTime(new Date());
			Calendar birthDate = Calendar.getInstance();
			birthDate.setTime(parseDate(birthday));
			if (birthDate.after(now)) return 0;
			age = now.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR);
			if (now.get(Calendar.DAY_OF_YEAR) > birthDate.get(Calendar.DAY_OF_YEAR))
				age += 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return age;
	}

	//将yyyy-MM-dd转化成时间戳
	public static Timestamp formarYMD(String ymd) throws Exception{
		Date date = parse(ymd, "yyyy-MM-dd");
		Timestamp timestamp = new Timestamp(date.getTime());
		return timestamp;
	}


	/**
	 * 获取某年某月的最后一天
	 * @param year
	 * @param month
	 * @return
	 */
	public static String getLastDayOfMonth(int year, int month) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month - 1);
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DATE));
		return new SimpleDateFormat("yyyy-MM-dd ").format(cal.getTime());

	}
	
		
	/**
	 * 获取下nextMonth个月的第一天   默认当月
	 * @param pattry  默认yyyyMMdd
	 * @return
	 */
	public static String getNextMonthFirstDay(Integer nextMonth, String pattern){
		if(nextMonth==null){
			nextMonth = 0;
		}
		Date date = new Date();
		if(StringUtils.isEmpty(pattern)){
			pattern = "yyyyMMdd";
		}
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.MONTH, nextMonth );
        String lastDay = sdf.format(calendar.getTime());
        return lastDay;
	}
	
	/**
	 * 获取下2个月的最后一天
	 * @param pattry  默认yyyyMMdd
	 * @return
	 */
	public static String getNextMonthLastDay(Integer nextMonth, String pattry){
		if(nextMonth==null){
			nextMonth = 0;
		}
		Date date = new Date();
		if(StringUtils.isEmpty(pattry)){
			pattry = "yyyyMMdd";
		}
        SimpleDateFormat sdf = new SimpleDateFormat(pattry);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, nextMonth + 1);
        calendar.set(Calendar.DATE, 0);
        String lastDay = sdf.format(calendar.getTime());
        return lastDay;
	}

	/**
	 * 获取某年某月的第一天
	 * @param year
	 * @param month
	 * @return
	 */
	public static String getFirstDayOfMonth(int year, int month) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month - 1);
		cal.set(Calendar.DAY_OF_MONTH, cal.getMinimum(Calendar.DATE));
		return new SimpleDateFormat("yyyy-MM-dd ").format(cal.getTime());
	}

	
	public static void main(String[] args) throws Exception {
			System.out.println(getNextMonthFirstDay(5, null));
			System.out.println(getNextMonthLastDay(5,null));
			
		
	}
}
