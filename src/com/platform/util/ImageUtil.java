package com.platform.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

//import sun.misc.BASE64Encoder;

/**
 * 图片工具类 ------ 好像没有被任何地方使用
 * 
 * @author
 * 
 */
public class ImageUtil
{
	
	public static String genBase64(String filePath) throws Exception{
		File file = new File(filePath);
		return genBase64(file);
	}
	
	public static String genBase64(File file) throws Exception{
		InputStream in = new FileInputStream(file);
		return genBase64(in);
	}
	
	public static String genBase64(InputStream in) throws Exception{
		
        byte[] data = null;  
        // 读取图片字节数组  
        try {  
            data = new byte[in.available()];  
            in.read(data);  
            in.close();  
        } finally {
        	if(in != null){
        		in.close();
        	}
        }
        // 对字节数组Base64编码  
        //BASE64Encoder encoder = new BASE64Encoder();  
        // 返回Base64编码过的字节数组字符串  
        return null;//encoder.encode(data);  
	}
	
	public static void main(String[] args){
		String filePath = "C:/Users/rain/Desktop/sy_logo.png";
		try {
			String base64 = genBase64(filePath);
			System.out.println(base64);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
