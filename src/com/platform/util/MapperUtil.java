package com.platform.util;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

import java.util.ArrayList;
import java.util.List;

/**
 * pojo 对象转换工具类
 * @author zxh
 * @date 2020/7/1 19:50
 */
public class MapperUtil {

    static Mapper mapper = new DozerBeanMapper();

    /**
     * Dto转Entity / Entity转Dto
     *
     */
    public static <D,E> E trans(D t,Class<E> clazz){
        if(t == null)
            return null;
        return mapper.map(t, clazz);
    }

    /**
     * Dto集合对象转EntityList / Entity集合对象转DtoList
     */
    public static <D,E> List<E> trans(D[] ts, Class<E> clazz){
        List<E> es = new ArrayList<E>();
        if(ts == null)
            return es;

        for(D d:ts) {
            E e = (E)trans(d,clazz);
            if(e != null)
                es.add(e);
        }

        return es;
    }

    /**
     * DtoList转EntityList / EntityList转DtoList
     */
    public static <D,E> List<E> trans(List<D> ts,Class<E> clazz){
        List<E> es = new ArrayList<E>();
        if(ts == null )
            return es;
        for(D d:ts) {
            E e = (E)trans(d,clazz);
            if(e != null)
                es.add(e);
        }
        return es;
    }
}
