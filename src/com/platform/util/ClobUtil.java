package com.platform.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Clob;
import java.sql.SQLException;

/**
 * Colb类型相关方法
 * @author beawan_fengjj
 *
 */
public class ClobUtil {

	public static String clobToString(Clob clob) throws Exception{
		String result = "";
		Reader reader = null;
		BufferedReader br = null;
		try {
			reader = clob.getCharacterStream();//得到流
			br = new BufferedReader(reader);
			String str = br.readLine();
			StringBuffer sb = new StringBuffer();
			while ( str != null) {
				sb.append(str);
				str = br.readLine();
			}
			result = sb.toString();
		} catch (SQLException e) {
			e.printStackTrace();
		}catch(IOException ie){
			ie.printStackTrace();
		}finally{
			try {
				reader.close();
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
}
