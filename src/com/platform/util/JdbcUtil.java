package com.platform.util;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.persister.entity.AbstractEntityPersister;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.beawan.common.Constants;
import com.beawan.common.ServiceFactory;

/**
 * JDBC工具类
 * 
 * @author mengbin
 * 
 */
public class JdbcUtil {

	private static transient Logger log = Logger.getLogger(JdbcUtil.class);

	/**
	 * 获取底层JDBC模板
	 * 
	 * @author mengbin
	 * @date Feb 8, 20148:29:09 PM
	 */
	public static JdbcTemplate getJdbcTemplate(String ds) throws Exception {
		if (StringUtil.isEmptyString(ds)) {
			return null;
		}
		if (ds.endsWith("DataSource")) {
			ds = ds.replace("DataSource", "JdbcTemplate");
		} else {
			ds = ds + "JdbcTemplate";
		}
		return (JdbcTemplate) ServiceFactory.getService(ds);
	}

	/**
	 * 获取结果集列名
	 * 
	 * @param rs
	 * @return
	 * @throws Exception
	 */
	public static String[] getResultFields(ResultSet rs) throws SQLException {
		if (rs == null) {
			return null;
		}
		ResultSetMetaData rsmd = rs.getMetaData();
		String[] result = new String[rsmd.getColumnCount()];
		for (int i = 0; i < result.length; i++) {
			result[i] = rsmd.getColumnLabel(i + 1);
		}
		return result;
	}

	/**
	 * 执行SQL,返回结果集
	 * 
	 * @version: v1.0.0
	 * @author: rain see
	 */
	public static List<Object[]> execQuery(String sql, String ds)
			throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("数据源:" + ds);
			log.debug("执行查询SQL:" + sql);
		}
		return getJdbcTemplate(ds).query(sql,
				new ResultSetExtractor<List<Object[]>>() {
					@Override
					public List<Object[]> extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						List<Object[]> result = new ArrayList<Object[]>();
						String[] colNames = getResultFields(rs);
						while (rs.next()) {
							Object[] cols = new Object[colNames.length];
							for (int i = 0; i < colNames.length; i++) {
								cols[i] = rs.getObject(colNames[i]);
							}
							result.add(cols);
						}
						return result;
					}
				});
	}

	/**
	 * 执行SQL,返回结果集
	 * @version: v1.0.0
	 * @param params 占位符实际值
	 */
	public static List<Object[]> execQuery(String sql, String ds, Object[] params)
			throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("数据源:" + ds);
			log.debug("执行查询SQL:" + sql);
		}
		return getJdbcTemplate(ds).query(sql,params,
				new ResultSetExtractor<List<Object[]>>() {
					@Override
					public List<Object[]> extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						List<Object[]> result = new ArrayList<Object[]>();
						String[] colNames = getResultFields(rs);
						while (rs.next()) {
							Object[] cols = new Object[colNames.length];
							for (int i = 0; i < colNames.length; i++) {
								cols[i] = rs.getObject(colNames[i]);
							}
							result.add(cols);
						}
						return result;
					}
				});
	}
	
	/**
	 * 执行SQL,返回结果集,Map中的键为字段名,使用javabean形式 例如USER_NAME将会被转换成userName形式
	 * 
	 * @version: v1.0.0
	 * @author: mengbin
	 * @date: Oct 10, 2013 3:26:41 PM
	 */
	public static List<Map<String, Object>> executeQuery(String sql, String ds)
			throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("数据源:" + ds);
			log.debug("执行查询SQL:" + sql);
		}
		return getJdbcTemplate(ds).query(sql,
				new ResultSetExtractor<List<Map<String, Object>>>() {
					@Override
					public List<Map<String, Object>> extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
						String[] colNames = getResultFields(rs);
						while (rs.next()) {
							Map<String, Object> map = new HashMap<String, Object>();
							for (String colName : colNames) {
								map.put(StringUtil.getCamel(colName),
										rs.getObject(colName));
							}
							result.add(map);
						}
						return result;
					}
				});
	}

	/**
	 * 查询第一行第一列整形结果
	 * 
	 * @author mengbin
	 * @date 2014-1-6 下午07:11:35
	 * @param countSql  count语句,可以使用?作为占位符
	 * @param params 占位符实际值
	 */
	public static int queryForInt(String ds, String sql, Object[] params)
			throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("数据源:" + ds);
			log.debug("执行查询SQL:" + sql);
			log.debug("查询参数:" + StringUtil.joinSql(params));
		}
		return getJdbcTemplate(ds).query(sql, params,
				new ResultSetExtractor<Integer>() {
					@Override
					public Integer extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						if (rs.next()) {
							return rs.getInt(1);
						}
						return 0;
					}
				});
	}

	/**
	 * 查询第一行第一列long型结果
	 * 
	 * @author mengbin
	 * @date Feb 8, 20148:53:46 PM
	 */
	public static long queryForLong(String ds, String sql, Object[] params)
			throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("数据源:" + ds);
			log.debug("执行查询SQL:" + sql);
			log.debug("查询参数:" + StringUtil.joinSql(params));
		}
		return getJdbcTemplate(ds).query(sql, params,
				new ResultSetExtractor<Long>() {
					@Override
					public Long extractData(ResultSet rs) throws SQLException,
							DataAccessException {
						if (rs.next()) {
							return rs.getLong(1);
						}
						return 0L;
					}
				});
	}

	/**
	 * 查询第一行第一列Double型结果
	 * 
	 * @author mengbin
	 * @date Feb 8, 20149:00:19 PM
	 */
	public static double queryForDouble(String ds, String sql, Object[] params)
			throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("数据源:" + ds);
			log.debug("执行查询SQL:" + sql);
			log.debug("查询参数:" + StringUtil.joinSql(params));
		}
		return getJdbcTemplate(ds).query(sql, params,
				new ResultSetExtractor<Double>() {
					@Override
					public Double extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						if (rs.next()) {
							return rs.getDouble(1);
						}
						return 0D;
					}
				});
	}

	/**
	 * 查询第一行第一列String型结果
	 * 
	 * @author mengbin
	 * @date Feb 8, 20149:52:39 PM
	 */
	public static String queryForString(String ds, String sql, Object[] params)
			throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("数据源:" + ds);
			log.debug("执行查询SQL:" + sql);
			log.debug("查询参数:" + StringUtil.joinSql(params));
		}
		return getJdbcTemplate(ds).query(sql, params,
				new ResultSetExtractor<String>() {
					@Override
					public String extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						if (rs.next()) {
							return rs.getString(1);
						}
						return null;
					}
				});
	}

	/**
	 * 查询第一行第一列Timestamp类型结果
	 * 
	 * @author mengbin
	 * @date Feb 8, 201410:00:13 PM
	 */
	public static Timestamp queryForTimestamp(String ds, String sql,
			Object[] params) throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("数据源:" + ds);
			log.debug("执行查询SQL:" + sql);
			log.debug("查询参数:" + StringUtil.joinSql(params));
		}
		return getJdbcTemplate(ds).query(sql, params,
				new ResultSetExtractor<Timestamp>() {
					@Override
					public Timestamp extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						if (rs.next()) {
							return rs.getTimestamp(1);
						}
						return null;
					}
				});
	}

	/**
	 * 执行SQL,返回结果集,Map中的键为字段名,使用javabean形式 例如USER_NAME将会被转换成userName形式
	 * 
	 * @author mengbin
	 * @date 2014-1-6 下午07:17:34
	 * @param countSql
	 *            count语句,可以使用?作为占位符
	 * @param params
	 *            占位符实际值
	 */
	public static List<Map<String, Object>> query(String ds, String sql,
			Object[] params) throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("数据源:" + ds);
			log.debug("执行查询SQL:" + sql);
			log.debug("查询参数:" + StringUtil.joinSql(params));
		}
		return getJdbcTemplate(ds).query(sql, params,
				new ResultSetExtractor<List<Map<String, Object>>>() {
					@Override
					public List<Map<String, Object>> extractData(ResultSet rs)
							throws SQLException, DataAccessException {
						List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
						String[] colNames = getResultFields(rs);
						while (rs.next()) {
							Map<String, Object> map = new HashMap<String, Object>();
							for (String colName : colNames) {
								map.put(StringUtil.getCamel(colName),
										rs.getObject(colName));
							}
							result.add(map);
						}
						return result;
					}
				});
	}

	/**
	 * 执行SQL语句,非查询语句
	 * @description:
	 * @version: v1.0.0
	 * @author: mengbin
	 * @date: Oct 10, 2013 3:53:16 PM
	 * @param sql sql语句
	 * @param ds  数据源名称
	 * @throws Exception
	 */
	public static void execute(String sql, String ds) throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("数据源:" + ds);
			log.debug("执行查询SQL:" + sql);
		}
		getJdbcTemplate(ds).execute(sql);
	}

	/**
	 * 执行待参数的SQL
	 * 
	 * @param ds
	 *            数据源
	 * @param sql
	 *            带参数的SQL,参数使用?作为占位符
	 * @param params
	 *            参数实际值
	 * @author mengbin
	 * @date 2014-1-11 下午09:55:34
	 */
	public static void execute(String ds, String sql, Object[] params)
			throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("数据源:" + ds);
			log.debug("执行查询SQL:" + sql);
			log.debug("查询参数:" + StringUtil.joinSql(params));
		}
		getJdbcTemplate(ds).update(sql, params);
	}

	/**
	 * 根据查询结果封装成对象
	 * 
	 * @author mengbin
	 * @date 2013-11-18 下午09:42:48
	 */
	public static <T> List<T> executeQuery(String sql, String ds, Class<T> clazz)
			throws Exception {
		List<Map<String, Object>> result = executeQuery(sql, ds);
		return convert(result, clazz);
	}

	/**
	 * 类型转换
	 * 
	 * @author mengbin
	 * @date 2014-1-22 下午08:19:14
	 */
	public static <T> List<T> convert(List<Map<String, Object>> result,
			Class<T> clazz) throws Exception {
		if (result == null || result.size() < 1) {
			return null;
		}
		List<T> retVals = new ArrayList<T>(result.size());
		for (int i = 0; i < result.size(); i++) {
			Map<String, Object> map = result.get(i);
			T obj = clazz.newInstance();
			for (Iterator<String> iter = map.keySet().iterator(); iter.hasNext();) {
				String name = iter.next();
				Object value = map.get(name);
				BeanUtil.setProperty(obj, name, value);
			}
			retVals.add(obj);
		}
		return retVals;
	}

	/**
	 * 待参数查询
	 * 
	 * @author mengbin
	 * @date 2014-1-22 下午08:19:33
	 */
	public static <T> List<T> executeQuery(String sql, String ds,
			Object[] params, Class<T> clazz) throws Exception {
		List<Map<String, Object>> result = query(ds, sql, params);
		return convert(result, clazz);
	}
	
	/**
	 * TODO 查询序列值
	 * @param seqName
	 * @param ds
	 * @return
	 * @throws Exception
	 */
	public static long getSequenceValue(String seqName, String ds) throws Exception {
		
		//oracle:
		//String sql ="select " + seqName + ".nextval from DUAL";
		//db2:
		String sql = "SELECT next value for " + seqName + " from Sysibm.sysdummy1";
		return queryForLong(ds, sql, null);
	}
	
	/**
	 * 根据实体类获得对应表名
	 * @param clazz
	 * @param sessionFactoryName
	 * @return
	 */
	public static String getTableName(Class<?> clazz , String sessionFactoryName){
		SessionFactory sessionFactory = (SessionFactory) ServiceFactory.getBean(sessionFactoryName);
		AbstractEntityPersister metaData = (AbstractEntityPersister) sessionFactory.getClassMetadata(clazz);
		metaData.getTableName();
		return metaData.getTableName();
	}
	
	/**
	 * 根据实体类和属性获得数据库对应字段名
	 * @param clazz
	 * @param propertyName
	 * @param sessionFactoryName
	 * @return
	 */
	public static String getColumnName(Class<?> clazz, String propertyName, String sessionFactoryName) {
		SessionFactory sessionFactory = (SessionFactory) ServiceFactory.getBean(sessionFactoryName);
		AbstractEntityPersister metaData = (AbstractEntityPersister) sessionFactory.getClassMetadata(clazz);
		String columnName = "";
		for(String str : metaData.getPropertyColumnNames(propertyName)) {
			columnName = str;
		}
		return columnName;
	}
	/**
	 * 根据实体类获得对应表名
	 * @param className 实体类全名
	 * @param sessionFactoryName
	 * @return
	 */
	public static String getTableName(String className , String sessionFactoryName){
		SessionFactory sessionFactory = (SessionFactory) ServiceFactory.getBean(sessionFactoryName);
		AbstractEntityPersister metaData = (AbstractEntityPersister) sessionFactory.getClassMetadata(className);
		metaData.getTableName();
		return metaData.getTableName();
	}

}
