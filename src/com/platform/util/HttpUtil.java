package com.platform.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.beawan.base.entity.User;
import com.beawan.common.Constants;

/**
 * HTTP请求工具类
 * 
 * @author mengbin
 * 
 */
public class HttpUtil {
	/**
	 * 获取当前请求参数
	 * 
	 * @param name
	 * @return
	 */
	public static String getParameter(HttpServletRequest request,String name) {
		return request.getParameter(name);
	}

	/**
	 * 获取当前用户
	 * 
	 * @return
	 */
	public static User getCurrentUser(HttpServletRequest request) {
		return (User) request.getSession().getAttribute(Constants.SessionKey.SESSION_KEY_USER);
	}
	
	/**
	 * 获取当前用户
	 * 
	 * @return
	 */
	public static User getCurrentUser() {
		return (User) getSession().getAttribute(Constants.SessionKey.SESSION_KEY_USER);
	}
	
	public static String getAsString(String name) {
		return getParameter(getRequest(),name);
	}
	/**
	 * 获取字符串参数,不存在返回null
	 * 
	 * @author: mengbin
	 * 
	 * @param name
	 * @return
	 */
	public static String getAsString(HttpServletRequest request,String name) {
		return getParameter(request,name);
	}


	/**
	 * 获取整型参数
	 * 
	 * @author: mengbin
	 * 
	 * @param name
	 * @return
	 */
	public static int getAsInt(HttpServletRequest request,String name) throws Exception {
		String value = getParameter(request,name);
		if (StringUtil.isEmptyString(value)) {
			throw new Exception("Can not find parameter with name : " + name);
		}
		return Integer.parseInt(value);
	}
	
	public static boolean getAsBoolean(HttpServletRequest request,String name) throws Exception {
		String value = getParameter(request,name);
		if (StringUtil.isEmptyString(value)) {
			throw new Exception("Can not find parameter with name : " + name);
		}
		return Boolean.parseBoolean(value);
	}
	
	/**
	 * 获取整型参数
	 * 
	 * @author: mengbin
	 * 
	 * @param name
	 * @return
	 */
	public static double getAsDouble(HttpServletRequest request,String name) throws Exception {
		String value = getParameter(request,name);
		if (StringUtil.isEmptyString(value)) {
			throw new Exception("Can not find parameter with name : " + name);
		}
		return Double.parseDouble(value);
	}

	/**
	 * 获取Long型参数
	 * 
	 * @author: mengbin
	 * 
	 * @param name
	 * @return
	 */
	public static long getAsLong(HttpServletRequest request,String name) throws Exception {
		String value = getParameter(request,name);
		if (value == null || "".equals(value)) {
			return 0;
			// throw new Exception("Can not find parameter with name : " + name);
		}
		return Long.parseLong(value);
	}

	

	/**
	 * 获取日期类型参数
	 * 
	 * @author: mengbin
	 * 
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public static Date getAsDate(HttpServletRequest request,String name) throws Exception {
		String value = getParameter(request,name);
		if (value == null) {
			throw new Exception("Can not find parameter with name : " + name);
		}
		return DateUtil.parse(value, Constants.DATE_MASK);
	}

	/**
	 * 获取日期类型参数
	 * 
	 * @author: mengbin
	 * 
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public static Date getAsDateTime(HttpServletRequest request,String name) throws Exception {
		String value = getParameter(request,name);
		if (value == null) {
			throw new Exception("Can not find parameter with name : " + name);
		}
		return DateUtil.parse(value, Constants.DATETIME_MASK);
	}

	/**
	 * 判断是否为Ajax请求
	 * 
	 * @author mengbin
	 * @date 2013-12-5 下午09:10:43
	 */
	public static boolean isAjaxRequest(HttpServletRequest request) {
		// return !StringUtil.isEmptyString(getAsString("_J_"));
		// 使用jQuery自带的鉴别标识
		String header = request.getHeader("X-Requested-With");
		return header != null && "XMLHttpRequest".equals(header);
	}

	
	/**
	 * 判断是否为iframe请求（url后跟着iframe标记）
	 * 
	 */
	public static boolean isIframe(HttpServletRequest request) {
		String iframe = request.getParameter("iframe");
		return iframe != null && "iframe".equals(iframe);
	}
	

	/**
	 * 获取客户端IP地址
	 * 
	 * @author mengbin
	 * @date 2013-12-5 下午09:10:52
	 */
	public static String getRemoteAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
			if(ip.equals("127.0.0.1")){
				//根据网卡取本机配置的IP
				InetAddress inetAddress = null;
				try {
					inetAddress = InetAddress.getLocalHost();
				} catch (UnknownHostException e) {
					e.printStackTrace();
				}
				ip = inetAddress.getHostAddress();
			}
		}
		return ip;
	}

	
	public static String getContextPath(HttpServletRequest request){
		String scheme = request.getScheme();
		String serverName = request.getServerName();
		int port = request.getServerPort();
		String path = request.getContextPath();
		String basePath = scheme + "://" + serverName + ":" + port + path;
		return basePath;
	}


	/**
	 * 获取当前请求对象
	 * 
	 * @return
	 */
	public static HttpServletRequest getRequest() {
		return ((ServletRequestAttributes) 
				RequestContextHolder.getRequestAttributes()).getRequest();
	}
	
	/**
	 * 获取响应对象
	 * 
	 * @return
	 */
	public static HttpServletResponse getResponse() {
		return ((ServletRequestAttributes) 
				RequestContextHolder.getRequestAttributes()).getResponse();
	}
	
	/**
	 * 获取当前会话对象
	 * 
	 * @return
	 */
	public static HttpSession getSession() {
		return getRequest().getSession();
	}
	
	
	
	/**
	 * 获取web应用根目录绝对路径
	 * 
	 * @return
	 */
	public static String getWebRootPath() {
		return getRequest().getServletPath().replace("\\", "/");
	}
	
	/**
	 * 获取客户端IP地址
	 */
	public static String getRemoteAddr() {
		HttpServletRequest request = getRequest();
		return getRemoteAddr(request);
	}
	
	/**
	 * 获取request请求中的参数
	 * @param request
	 * @param charset
	 * @return
	 * @throws IOException
	 */
	public static String getRequestMsg(HttpServletRequest request, String charset) throws IOException{
		request.setCharacterEncoding(charset);
		StringBuilder result = new StringBuilder();
		InputStream in = request.getInputStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(in, charset));
		int contentLength = request.getContentLength();
		char[] buffer = new char[contentLength];
		int size;
		while((size=reader.read(buffer))>0 ) {
			result.append(buffer, 0, size);
		}
		String requestParam = result.toString();
		try {
			requestParam = URLDecoder.decode(requestParam, "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return requestParam;
			
	}
	
}
