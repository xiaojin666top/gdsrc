package com.platform.util;

import java.security.MessageDigest;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

/**
 * 加密解密工具类
 */
public class EncryptUtil
{
	private static final Logger log = Logger
			.getLogger(EncryptUtil.class);
	
	/**
	 * @param bytes
	 * @return 返回字母为小写
	 */
	private static String toHex(byte[] bytes)
	{
		StringBuffer buf = new StringBuffer();
		for (int offset = 0; offset < bytes.length; offset++)
		{
			int i = bytes[offset];
			if (i < 0)
				i += 256;
			if (i < 16)
				buf.append("0");
			buf.append(Integer.toHexString(i));
		}
		
		return buf.toString();
	}
	
	/**
	 * TODO MD5加密
	 * @param msg 需要加密的字符串
	 * @return 返回字母为大写
	 * @throws Exception
	 */
	public static String md5(String msg) throws Exception {
		return md5(msg, true);
	}

	/**
	 * TODO MD5加密
	 * @param msg 需要加密的字符串
	 * @param upper 是否返回大写
	 * @return 
	 * @throws Exception
	 */
	public static String md5(String msg, boolean upper) throws Exception
	{
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(msg.getBytes());
		byte b[] = md.digest();
		String md5Str = toHex(b);
		
		if(upper)
			md5Str = md5Str.toUpperCase();
		
		return md5Str;
	}
	
	/**
	 * TODO 获得md5密文
	 * @param msg 需要加密的字符串
	 * @return 返回字母为大写
	 */
	public static String getMd5(String msg)
	{
		String result = "";
		
		try {
			md5(msg);
		} catch (Exception e) {
			log.error("获取md5值异常", e);
		}
		
		return result;
	}

	/**
	 * BASE64加密
	 * 
	 * @param msg
	 * @return
	 * @throws Exception
	 */
	public static String encryptBASE64(String msg)
	{
		byte[] b = msg.getBytes();
		Base64 base64 = new Base64();
		b = base64.encode(b);
		String s = new String(b);
		return s;
	}

	/**
	 * BASE64解密
	 * 
	 * @param msg
	 * @return
	 * @throws Exception
	 */
	public static String decryptBASE64(String msg)
	{
		byte[] b = msg.getBytes();
		Base64 base64 = new Base64();
		b = base64.decode(b);
		String s = new String(b);
		return s;
	}

	public static void main(String[] args) throws Exception
	{
		System.out.println(md5("zhoujing"));
	}
}
