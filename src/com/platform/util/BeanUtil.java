package com.platform.util;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Clob;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import oracle.sql.CLOB;
import oracle.sql.TIMESTAMP;

/**
 * @author rain
 */
public class BeanUtil {
	
	private static transient Logger log = Logger.getLogger(BeanUtil.class);

	/**
	 * 获取参数信息完整度,即获取非默认值字段所占比率 * 100
	 * 
	 * @param obj
	 * @return
	 */
	public static int getBeanCompleteRate(Object obj) {
		Class<?> clazz = obj.getClass();
		Method[] methods = clazz.getDeclaredMethods();
		double totalGet = 0;
		double totalNotEmptyGet = 0;
		for (Method method : methods) {
			if (method.getName().startsWith("get")) {
				try {
					Object result = method.invoke(obj);
					totalGet += 1d;
					if (result == null) {
						continue;
					}
					if (result instanceof Number) {
						Number num = (Number) result;
						if (num.byteValue() == 0) {
							continue;
						}
					}
					totalNotEmptyGet += 1d;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println(totalNotEmptyGet + "/" + totalGet);
		return (int) (totalNotEmptyGet / totalGet * 100);
	}

	/**
	 * 拷贝对象属性到另一个对象中(如果属性存在)
	 * 
	 * @author mengbin
	 * @date Feb 16, 201411:29:14 AM
	 */
	public static void copyProperties(Object source, Object target,
			String[] ignoreProperties) throws Exception {
		BeanUtils.copyProperties(source, target, ignoreProperties);
	}

	public static void copyProperties(Object source, Object target)
			throws Exception {
		copyProperties(source, target, null);
	}

	public static JSON ehchance(JSON json, Map<String, Map<String, String>> map)
			throws Exception {
		return enchance(json, map, null);
	}

	/**
	 * 增强JSON数据 json可以为JsonObject或者JsonArray
	 * map键为需要增强的字段名,值为该字段枚举值映射,键为value值为text suffix 为增强字段后缀
	 * 
	 * @version: v1.0.0
	 * @author: mengbin
	 * @date: 2013-11-6下午08:13:46
	 */
	public static JSON enchance(JSON json,
			Map<String, Map<String, String>> map, String suffix)
			throws Exception {
		JSONArray array = null;
		boolean isArray = false;
		if (json instanceof JSONObject) {
			array = new JSONArray();
			array.add(json);
		} else if (json instanceof JSONArray) {
			array = (JSONArray) json;
			isArray = true;
		} else {
			return json;
		}
		if (map == null || map.size() < 1) {
			return json;
		}
		if (StringUtil.isEmptyString(suffix)) {
			suffix = "text";
		}
		for (Iterator<String> iter = map.keySet().iterator(); iter.hasNext();) {
			String key = iter.next();
			Map<String, String> _map = map.get(key);
			if (_map == null || _map.size() < 1) {
				continue;
			}
			for (int i = 0; i < array.size(); i++) {
				JSONObject obj = array.getJSONObject(i);
				if (obj.has(key)) {
					String val = obj.get(key).toString();
					String text = _map.get(val);
					obj.put(key + "_" + suffix, text);
				}
			}
		}
		if (!isArray) {
			return array.getJSONObject(0);
		}
		return array;
	}

	/**
	 * 设置属性
	 * 
	 * @author mengbin
	 * @date 2013-11-18 下午09:50:12
	 */
	public static void setProperty(Object obj, String name, Object value)
			throws Exception {
		PropertyDescriptor pd = PropertyUtils.getPropertyDescriptor(obj, name);
		if (pd == null) {
			log.error("未获取到属性:" + name + "的set方法!");
		}
		Class<?> type = pd.getPropertyType();
		if (value == null) {
			if (!BeanUtils.isSimpleProperty(type)) {
				pd.getWriteMethod().invoke(obj, value);
			}
		} else {
			if(BigDecimal.class.equals(value.getClass())){
				if(Integer.class.equals(type)){
					value = ((BigDecimal)value).intValue();
				}else if(Long.class.equals(type)){
					value = ((BigDecimal)value).longValue();
				}else if(Float.class.equals(type)){
					value = ((BigDecimal)value).floatValue();
				}else if(Double.class.equals(type)){
					value = ((BigDecimal)value).doubleValue();
				}
			}
			if(CLOB.class.equals(value.getClass())){
				value = ClobUtil.clobToString((Clob)value);
			}
			if(TIMESTAMP.class.equals(value.getClass())){
				value = DateUtil.oracleSqlTimestamp2Date(value);
			}
			pd.getWriteMethod().invoke(obj, value);
		}
	}
	
	/**
	 * TODO 比较两个对象属性值是否相等，返回不相等的属性数组
	 * @param oldBean 旧的实体类
	 * @param newBean 新的实体类
	 * @return
	 * @author rain see
	 * @throws Exception
	 */
	public static JSONArray compareProperties(Object oldBean , Object newBean) throws Exception{
		JSONObject ignoreFields = new JSONObject();
		ignoreFields.put("serialVersionUID", "serialVersionUID");
		return comparePropByAccessField(oldBean , newBean , ignoreFields);
	}
	
	/**
	 * TODO 比较两个对象属性值是否相等，返回不相等的属性数组
	 * @param oldBean 旧的实体类
	 * @param newBean 新的实体类
	 * @return
	 * @author rain see
	 * @throws Exception
	 */
	public static JSONArray compareProperties(Object oldBean , Object newBean , JSONObject ignoreFields) throws Exception{
		
		if(ignoreFields == null)
			ignoreFields = new JSONObject();
		
		ignoreFields.put("serialVersionUID", "serialVersionUID");
		
		return comparePropByAccessField(oldBean , newBean , ignoreFields);
	}
	
	/**
	 * TODO 通过get方法获取属性值，并比较两个对象属性值是否相等，返回不相等的属性数组
	 * @param oldBean 旧的实体类
	 * @param newBean 新的实体类
	 * @param ignoreFields 忽略的属性集合
	 * @return
	 * @author rain see
	 * @throws Exception
	 */
	public static JSONArray comparePropByGetMethod(Object oldBean , Object newBean , JSONObject ignoreFields) throws Exception{
		
		//如果对象为空或者两个对象类型不一致
		if(oldBean == null || newBean == null
				|| (!oldBean.getClass().getName().equals(newBean.getClass().getName())))
			throw new Exception();
		
		Field[] oldFields = oldBean.getClass().getDeclaredFields();
		
		String fieldName  ,methodName;
		Object oldFieldValue , newFieldValue;
		Method getMethod;
		JSONObject json;
		JSONArray arr = new JSONArray();
		
		for(int i=0;i<oldFields.length;i++){
			
			fieldName = oldFields[i].getName();
			if(ignoreFields != null && ignoreFields.containsKey(fieldName))
				continue;
			
			methodName = "get"+fieldName.substring(0, 1).toUpperCase()+fieldName.substring(1);
			
			getMethod = oldBean.getClass().getMethod(methodName);
			oldFieldValue = getMethod.invoke(oldBean);
			
			getMethod = newBean.getClass().getMethod(methodName);
			newFieldValue = getMethod.invoke(newBean);
			
			if(oldFieldValue == null && newFieldValue == null)
				continue;
			
			if((oldFieldValue != null && newFieldValue != null)
					&& oldFieldValue.equals(newFieldValue)){
				continue;
			}
			
			json = new JSONObject();
			json.put("fieldName", fieldName);
			json.put("oldFieldValue", oldFieldValue);
			json.put("newFieldValue", newFieldValue);
			arr.add(json);
		}
		
		return arr;
	}
	
	/**
	 * TODO 通过直接访问属性值方式，并比较两个对象属性值是否相等，返回不相等的属性数组
	 * @param oldBean 旧的实体类
	 * @param newBean 新的实体类
	 * @param ignoreFields 忽略的属性集合
	 * @return
	 * @author rain see
	 * @throws Exception
	 */
	public static JSONArray comparePropByAccessField(Object oldBean , Object newBean , JSONObject ignoreFields) throws Exception{
		
		//如果对象为空或者两个对象类型不一致
		if(oldBean == null || newBean == null
				|| (!oldBean.getClass().getName().equals(newBean.getClass().getName())))
			throw new Exception();
		
		Field[] oldFields = oldBean.getClass().getDeclaredFields();
		
		String fieldName ;
		Object oldFieldValue , newFieldValue;
		Field oldFiled , newFiled;
		JSONObject json;
		JSONArray arr = new JSONArray();
		
		for(int i=0;i<oldFields.length;i++){
			
			oldFiled = oldFields[i];
			
			fieldName = oldFiled.getName();
			if(ignoreFields != null && ignoreFields.containsKey(fieldName))
				continue;
			
			oldFiled.setAccessible(true);
			oldFieldValue = oldFiled.get(oldBean);
			oldFiled.setAccessible(false);
			
			newFiled = newBean.getClass().getDeclaredField(fieldName);
			newFiled.setAccessible(true);
			newFieldValue = newFiled.get(newBean);
			newFiled.setAccessible(false);
			
			if(oldFieldValue == null && newFieldValue == null)
				continue;
			
			if((oldFieldValue != null && newFieldValue != null)
					&& oldFieldValue.equals(newFieldValue)){
				continue;
			}
			
			json = new JSONObject();
			json.put("fieldName", fieldName);
			json.put("oldFieldValue", oldFieldValue);
			json.put("newFieldValue", newFieldValue);
			arr.add(json);
		}
		
		return arr;
	}
	
	/**
	 * TODO 合并两个对象属性，把源实体类属性值复制到目标实体类（包括null值），合并的属性由fieldNames指定
	 * @param source 源实体类
	 * @param target 目标实体类
	 * @param fieldNames 属性名称集合
	 * @return 返回合并后的目标实体类
	 * @throws Exception
	 * @author rain see
	 */
	public static Object mergeProperties(Object source , Object target,
			JSONObject fieldNames) throws Exception{
		return mergePropByAccessField(source , target , fieldNames, false, false);
	}
	
	/**
	 * TODO 合并两个对象属性，把源实体类属性值复制到目标实体类
	 * @param source 源实体类
	 * @param target 目标实体类
	 * @param fieldNames 属性名称集合
	 * @param ignoreField true：忽略fields中的属性，flase：仅合并fields中的属性，默认false
	 * @param ignoreNull 是否忽略为空的属性，默认false
	 * @return 返回合并后的目标实体类
	 * @throws Exception
	 * @author rain see
	 */
	public static Object mergeProperties(Object source , Object target,
			JSONObject fieldNames, boolean ignoreField, boolean ignoreNull) throws Exception{
		return mergePropByAccessField(source , target , fieldNames, ignoreField, ignoreNull);
	}
	
	/**
	 * TODO 合并两个对象属性，通过get set 方法把源实体类属性值复制到目标实体类
	 * 如果源属性值为null，则跳过不复制
	 * @param source 源实体类
	 * @param target 目标实体类
	 * @param fieldNames 属性名称集合
	 * @param ignoreField true：忽略fields中的属性，flase：仅合并fields中的属性，默认false
	 * @param ignoreNull 是否忽略为空的属性，默认false
	 * @return 返回合并后的目标实体类
	 * @throws Exception
	 * @author rain see
	 */
	private static Object mergePropByGSMethod(Object source , Object target , 
			JSONObject fieldNames, boolean ignoreField, boolean ignoreNull) throws Exception{
		
		//如果对象为空或者两个对象类型不一致
		if(source == null || target == null
				|| (!source.getClass().getName().equals(target.getClass().getName())))
			throw new Exception();
		
		Field[] sourceFields = source.getClass().getDeclaredFields();
		
		String fieldName  , methodName;
		Object sourceFieldValue;
		Method sourceGetMethod , targetSetMethod;
		Field sourceField;
		for(int i=0;i<sourceFields.length;i++){
			sourceField = sourceFields[i];
			fieldName = sourceField.getName();
			
			if(ignoreField && fieldNames != null && fieldNames.containsKey(fieldName))
				continue;
			else if(!ignoreField && fieldNames != null && !fieldNames.containsKey(fieldName))
				continue;
			
			methodName = fieldName.substring(0, 1).toUpperCase()+fieldName.substring(1);
			
			sourceGetMethod = source.getClass().getMethod("get" + methodName);
			sourceFieldValue = sourceGetMethod.invoke(source);
			
			if(ignoreNull && sourceFieldValue == null)
				continue;
			
			targetSetMethod = target.getClass().getMethod("set" + methodName , sourceField.getType());
			targetSetMethod.invoke(target, sourceFieldValue);
		}
		
		return target;
	}
	
	/**
	 * TODO 合并两个对象属性，通过直接访问类属性把源实体类属性值复制到目标实体类
	 * 如果源属性值为null，则跳过不复制
	 * @param source 源实体类
	 * @param target 目标实体类
	 * @param fieldNames 属性名称集合
	 * @param ignoreField true：忽略fields中的属性，flase：仅合并fields中的属性，默认false
	 * @param ignoreNull 是否忽略为空的属性，默认false
	 * @return 返回合并后的目标实体类
	 * @throws Exception
	 * @author rain see
	 */
	private static Object mergePropByAccessField(Object source , Object target ,
			JSONObject fieldNames, boolean ignoreField, boolean ignoreNull) throws Exception{
		
		//如果对象为空或者两个对象类型不一致
		if(source == null || target == null
				|| (!source.getClass().getName().equals(target.getClass().getName())))
			throw new Exception();
		
		Field[] sourceFields = source.getClass().getDeclaredFields();
				
		String fieldName ;
		Object sourceFieldValue;
		Field sourceField , targetField;
		
		for(int i=0;i<sourceFields.length;i++){
			sourceField = sourceFields[i];
			fieldName = sourceField.getName();
			
			if(ignoreField && fieldNames != null && fieldNames.containsKey(fieldName))
				continue;
			else if(!ignoreField && fieldNames != null && !fieldNames.containsKey(fieldName))
				continue;
			
			sourceField.setAccessible(true);
			sourceFieldValue = sourceField.get(source);
			sourceField.setAccessible(false);
			
			if(ignoreNull && sourceFieldValue == null)
				continue;
			
			targetField = target.getClass().getDeclaredField(fieldName);
			targetField.setAccessible(true);
			targetField.set(target, sourceFieldValue);
			targetField.setAccessible(false);
		}
		
		return target;
	}
	
	public static Object getField(Object obj, String fieldName) throws Exception{
		
		if(obj == null || StringUtil.isEmptyString(fieldName))
			return null;
		
		Field field = obj.getClass().getDeclaredField(fieldName);
		field.setAccessible(true);
		Object sourceFieldValue = field.get(obj);
		field.setAccessible(false);
		
		return sourceFieldValue;
	}
	
	public static Map<String, Object> convertObject2Map(Object obj){
		if(obj==null) return null;
		Map<String, Object> map = new LinkedHashMap<String, Object>();
		Class clazz = obj.getClass();
		Field[] fields = clazz.getDeclaredFields();
		if(fields.length==0) return null;
		for(int i=0;i<fields.length;i++) {
			fields[i].setAccessible(true);
			String name = fields[i].getName();
			Object value =convertfieldType(fields[i], obj)==null?"":convertfieldType2(fields[i], obj);
			map.put(name,  value);
		}
		return map;
		
	}

	public static String convertfieldType(Field field,Object o) {
		if(field==null) return null;
		String returnValue="";
		try {
	
		
		field.setAccessible(true);
		
		switch (field.getGenericType().toString()) {
		case "class java.lang.String":
			    if(field.get(o) == null) return null;
				returnValue ="\"" +field.get(o).toString()+"\"";
			
			break;
		case "class java.lang.Long":
			if(field.get(o) == null) return null;
			returnValue = field.get(o).toString()+"l";
			break;
		case "class java.lang.Integer":
			if(field.get(o) == null) return null;
			returnValue = field.get(o).toString();
			break;
		default:
			return null;
		
		}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			
			return null;
		}
		return returnValue;
	}

	public static Object convertfieldType2(Field field,Object o) {
		if(field==null) return null;
		Object returnValue="";
		try {
		field.setAccessible(true);
		switch (field.getGenericType().toString()) {
		case "class java.lang.String":
			    if(field.get(o) == null) return null;
				returnValue =field.get(o).toString();
			
			break;
		case "class java.lang.Long":
			if(field.get(o) == null) return null;
			returnValue = (Long)field.get(o);
			break;
		case "class java.lang.Integer":
			if(field.get(o) == null) return null;
			returnValue = (Integer)field.get(o);
			break;
		default:
			return null;
		
		}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			
			return null;
		}
		return returnValue;
		
	}
	
}
