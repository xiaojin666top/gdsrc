package com.platform.util;

import java.io.*;
import java.net.URL;
import java.util.List;

/**
 * 文件操作工具类
 * 
 * @author mengbin
 * 
 */
public class FileUtil {

	/**
	 * 关闭IO
	 * 
	 * @param iss
	 * @param oss
	 */
	public static void closeIO(InputStream[] iss, OutputStream[] oss) {
		if (iss != null && iss.length > 0) {
			for (int i = 0; i < iss.length; i++) {
				if (iss[i] != null) {
					try {
						iss[i].close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		if (oss != null && oss.length > 0) {
			for (int i = 0; i < oss.length; i++) {
				if (oss[i] != null) {
					try {
						oss[i].close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	/**
	 * 保存文件
	 * 
	 * @author: mengbin
	 * 
	 * @param is
	 * @param file
	 * @param bufferSize
	 */
	public static void saveFile(InputStream is, File file, int bufferSize) {
		try {
			OutputStream os = new FileOutputStream(file);
			byte[] buf = new byte[bufferSize];
			int len = 0;
			while ((len = is.read(buf)) > 0) {
				os.write(buf, 0, len);
				os.flush();
			}
			os.flush();
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 获取文件扩展名,包括前面的点号
	 * 
	 * @author: mengbin
	 * 
	 * @param filename
	 * @return
	 */
	public static String getFileExtension(String filename) {
		int pos = filename.indexOf(".");
		if (pos < 0) {
			return "";
		}
		return filename.substring(pos);
	}

	/**
	 * 读取文本文件 返回字符串
	 * @param file
	 * @return
	 */
	public static String txt2String(File file){
		StringBuilder result = new StringBuilder();
		try{
			BufferedReader br = new BufferedReader(new FileReader(file));//构造一个BufferedReader类来读取文件
			String s = null;
			while((s = br.readLine())!=null){//使用readLine方法，一次读一行
				result.append(System.lineSeparator()+s);
			}
			br.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return result.toString();
	}

	/**
	 * 获取classpath下的文件
	 * @param fileName
	 */
	public  File getFile(String fileName) {
		ClassLoader classLoader = this.getClass().getClassLoader();
		/**
		 getResource()方法会去classpath下找这个文件，获取到url resource, 得到这个资源后，调用url.getFile获取到 文件 的绝对路径
		 */
		URL url = classLoader.getResource(fileName);
		/**
		 * url.getFile() 得到这个文件的绝对路径
		 */
		System.out.println(url.getFile());
		File file = new File(url.getFile());
		return file;
	}


		/**
         * 拷贝文件
         *
         * @author mengbin
         * @date 2013年12月11日 下午2:11:18
         */
	public static void copyFile(File srcFile, File targetFile) throws Exception {
		byte[] buf = new byte[2048];
		InputStream is = null;
		OutputStream os = null;
		try {
			is = new FileInputStream(srcFile);
			os = new FileOutputStream(targetFile);
			int len = 0;
			while ((len = is.read(buf)) > 0) {
				os.write(buf, 0, len);
				os.flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeIO(new InputStream[] { is }, new OutputStream[] { os });
		}
	}
	/**
	 * 递归删除目录下所有文件及子目录下所有文件
	 * @param dir 要删除的文件目录
	 * @return
	 * @throws Exception
	 */
	public static boolean deleteDir(File dir)throws Exception{
		if(dir.isDirectory()){
			String[] children = dir.list();
			//递归删除目录中的子目录
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir,children[i]));
				if(!success){
					return false;
				}
			}
		}
		//目录此时为空，可以删除
		return dir.delete();
	}
	
	/**
	 * 递归计算目录下所有文件及子目录下所有文件个数
	 * @param dir 要计算的文件目录
	 * @return
	 * @throws Exception
	 */
	public static int getDirFileCount(File dir,int count)throws Exception{
		if(dir.isDirectory()){
			String[] children = dir.list();
			//递归目录中的子目录
			for (int i = 0; i < children.length; i++) {
				count = getDirFileCount(new File(dir,children[i]),count);
			}
		}else{
			count ++;
		}
		return count;
	}
	/**
	 * 将文件夹下面的文件移动到指定文件夹
	 * @param oldFileDirPath 原文件夹
	 * @param newFileDirPath 指定移动到的文件夹
	 * @return
	 * @throws Exception
	 */
	public static void renameFile(String oldFileDirPath,String newFileDirPath)throws Exception{
		File oldFileDir = new File(oldFileDirPath);
		if(oldFileDir.isDirectory()){
			String[] children = oldFileDir.list();
			for (int i = 0; i < children.length; i++) {
				renameFile(new File(oldFileDir,children[i]).getAbsolutePath(),
						newFileDirPath);
			}
		}
		if(oldFileDir.isFile()){
			File newFile = new File(newFileDirPath+oldFileDir.getName());
			oldFileDir.renameTo(newFile);
		}
	}
	/**
	 * 获取指定目录下的文件
	 * @param pathDir
	 * @param filePath
	 * @return
	 * @throws Exception
	 */
	public static List<File> getChildFiles(File pathDir,List<File> filePath)throws Exception{
		if(pathDir.isDirectory()){
			String[] children = pathDir.list();
			for (int i = 0; i < children.length; i++) {
				getChildFiles(new File(pathDir,children[i]),filePath);
			}
		}
		if(pathDir.isFile()){
			filePath.add(pathDir);
		}
		return filePath;
	}
	
	
	public static void main(String[] args) throws Exception{
		FileUtil.renameFile("F:\\ydxddata\\mcesys\\image\\temp\\mediaSystem\\SQYD305206920160511000009", 
				"F:\\ydxddata\\mcesys\\image\\temp\\mediaSystem\\SQYD305206920160511000009Zip\\");
	}
}
