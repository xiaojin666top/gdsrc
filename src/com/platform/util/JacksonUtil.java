package com.platform.util;

import java.io.IOException;
import java.text.SimpleDateFormat;

import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**  
 * 
 * @ClassName: JacksonUtil.java
 * @Description: 该类的功能描述
 *
 */
public class JacksonUtil {
	
	private static ObjectMapper mapper = new ObjectMapper();
	
	/**
	 * 
	 * @Function: JacksonUtil::serialize
	 * @Description: 将对象转换为JSON字符串
	 * @param object
	 * @return
	 * @throws JsonGenerationException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public static String serialize(Object object)
			throws JsonGenerationException, JsonMappingException, IOException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");  
		mapper.getSerializationConfig().with(formatter);
		return mapper.writeValueAsString(object);
	}
	
	/**
	 * 
	 * @Function: JacksonUtil::fromJson
	 * @Description: 反序列化POJO或简单Collection如List<String>
	 * 				如需反序列化复杂Collection如List<MyBean>, 请使用fromJson(String,JavaType)
	 * @param jsonString
	 * @param clazz
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @version: v1.0.0
	 */
	public static <T> T fromJson(String jsonString, Class<T> clazz)
			throws JsonParseException, JsonMappingException, IOException {
		if (StringUtils.isEmpty(jsonString)) {
			return null;
		}
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"); 
		mapper.getDeserializationConfig().with(formatter);
		return mapper.readValue(jsonString, clazz);
	}

	
	/**
	 * 
	 * @Function: JacksonUtil::fromJson
	 * @Description: 反序列化复杂Collection如List<Bean>, 先使用函数createCollectionType构造类型,然后调用本函数.
	 * @param jsonString
	 * @param javaType
	 * @return
	 * @version: v1.0.0
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	public static <T> T fromJson(String jsonString, JavaType javaType)
			throws JsonParseException, JsonMappingException, IOException {
		if (StringUtils.isEmpty(jsonString)) {
			return null;
		}
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"); 
		mapper.getDeserializationConfig().with(formatter);
		return mapper.readValue(jsonString, javaType);
	}
	
	public static JavaType getJavaType(Class<?> collectionClass, Class<?>... elementClasses){
		return mapper.getTypeFactory().constructParametricType(  
				collectionClass, elementClasses);
	}

	/***
	 *
	 * @param jsonString
	 * @param type
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public static <T> T fromJson(String jsonString, TypeReference<T> type)
			throws JsonParseException, JsonMappingException, IOException{
		if (StringUtils.isEmpty(jsonString)) {
			return null;
		}
		//SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		//mapper.getDeserializationConfig().setDateFormat(formatter);
		return mapper.readValue(jsonString, type);
	}
}
