package com.platform.util;

import java.io.*;

public class OracleToDb2Util {

    private OracleToDb2Util() {
        super();
    }

    public static void main(String[] args)throws Exception {
//        formatOraToDb2Sql("E:\\cust_data\\rugao\\GDTCESYS","E:\\cust_data\\rugao\\GDTCESYS", "TCESYS_DATA");

        formatOraToDb2Seq("D:\\文件库\\客户文档\\如皋\\ORACLE数据转db2\\序列文件",
                 "D:\\文件库\\客户文档\\如皋\\ORACLE数据转db2\\格式化\\");

    }
    /**
     * 将导出的oracle脚本
     * 将表结构及数据转换成db2语法（ddl dml 转化成 db2的语法）
     *
     * @param originPath 原始文件夹
     * @param targetPath 目标文件夹
     */
    public static void formatOraToDb2Sql(String originPath, String targetPath, String tabspaceName) throws Exception  {
        if (originPath == null || "".equals(originPath)) {
            return;
        }
        File originDir = new File(originPath);
        if (!originDir.isDirectory()) {
            return;
        }
        File[] origins = originDir.listFiles();
        if (origins == null || origins.length == 0)
            return;

        InputStream input = null;
        OutputStream out = null;
        BufferedReader reader = null;
        BufferedWriter writer = null;


        try {

            for (File sqlFile : origins) {
                input = new FileInputStream(sqlFile);
                String fileName = sqlFile.getName();
                out = new FileOutputStream(targetPath + File.separator + fileName);
                reader = new BufferedReader(new InputStreamReader(input));
                writer = new BufferedWriter(new OutputStreamWriter(out));

                StringBuilder originStr = new StringBuilder();
                String line = null;
                int index = 1;
                while((line = reader.readLine())!=null){
                    if(line!=null){
                        if(line.contains("--")
//                                || line.startsWith("COMMENT") || line.startsWith("INSERT")
                        ){
                            index++;
                            continue;
                        }
                    }
                    //第二行是建表的语句
//                    if(index==1){
//                        index++;
//                        continue;
//                    }
//                    if(index==2){
//                        int len = line.length();
//                        line = line.substring(0, len - 1) + " in " + tabspaceName + ";";
//                    }
                    originStr.append(line + "\n");
                    index++;
                }
                //替换字符串规则
                /**
                 * 规则1： NUMBER(10,2) 替换成 INTEGER
                 * 规则2： NUMBER 替换成 INTEGER
                 * 规则3： VARCHAR2 替换成 VARCHAR
                 * 规则4： 第二行创建表的时候指定表空间  在最后面加上表空间名字tabspaceName
                 * 规则5：修改schema名字
                 *
                 * --》以上是从dbvisul里面导出的就够了  下面的是从Navicat导出来
                 *
                 * 规则6：去掉 byte
                 * 建表语句中 存在如下的内容  去掉 替换出  in  tabspaceName
                 * LOGGING
                 * NOCOMPRESS
                 * NOCACHE
                 * 规则7：去掉索引中的  LOGGING  VISIBLE
                 * 规则8：去掉 CHAR
                 */

                //NUMBER(22)
                String targetStr = originStr.toString().replaceAll("NUMBER\\(.*?\\)", "INTEGER ")
                        .replaceAll("NUMBER", "INTEGER")
                        .replaceAll("VARCHAR2", "VARCHAR")
                        .replaceAll("GDTCESYS", "GDTCESYS")
                        .replaceAll("BYTE", "")
                        .replaceAll("LOGGING", "")
                        .replaceAll("NOCOMPRESS", "in " + tabspaceName)
                        .replaceAll("NOCACHE", "")
                        .replaceAll("VISIBLE;", "")
                        .replaceAll("\\sCHAR", "");

//                        .replaceAll("AAA", "")

                writer.write(targetStr);

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(writer!=null){
                writer.close();
            }
            if(reader!=null){
                reader.close();
            }
            if(out!=null){
                out.close();
            }
            if(input!=null){
                input.close();
            }
        }


    }

    /**
     * 将oracle导出的序列格式化成 db2语法
     * @param originPath
     * @param targetPath
     * @throws Exception
     */
    public static void formatOraToDb2Seq(String originPath, String targetPath) throws Exception  {
        if (originPath == null || "".equals(originPath)) {
            return;
        }
        File originDir = new File(originPath);
        if (!originDir.isDirectory()) {
            return;
        }
        File[] origins = originDir.listFiles();
        if (origins == null || origins.length == 0)
            return;

        InputStream input = null;
        OutputStream out = null;
        BufferedReader reader = null;
        BufferedWriter writer = null;


        try {

            for (File sqlFile : origins) {
                input = new FileInputStream(sqlFile);
                String fileName = sqlFile.getName();
                out = new FileOutputStream(targetPath + File.separator + fileName);
                reader = new BufferedReader(new InputStreamReader(input));
                writer = new BufferedWriter(new OutputStreamWriter(out));

                StringBuilder originStr = new StringBuilder();
                String line = null;
                int index = 1;
                while((line = reader.readLine())!=null){
                    if(line!=null){
                        if(line.contains("prompt")){
                            index++;
                            continue;
                        }
                    }
                    originStr.append(line + "\n");
                    index++;
                }

                String targetStr = originStr.toString().replaceAll("GDTCESYS", "GDTCESYS")
                    .replaceAll("9{5,}", "999999999");

                writer.write(targetStr);

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(writer!=null){
                writer.close();
            }
            if(reader!=null){
                reader.close();
            }
            if(out!=null){
                out.close();
            }
            if(input!=null){
                input.close();
            }
        }
    }
}
