package com.platform.util;

import java.io.ByteArrayInputStream;
import java.util.Iterator;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPMessage;

import com.platform.dto.WsReturnDto;

public class SoapUtil {

	
	 /**
     * 解析soapXML
     * @param soapXML
     * @return
     */
    public static WsReturnDto parseSoapMessage(String soapXML) {
        WsReturnDto resultBean = new WsReturnDto();
        try {
            SOAPMessage msg = formatSoapString(soapXML);
            SOAPBody body = msg.getSOAPBody();
            Iterator<SOAPElement> iterator = body.getChildElements();
            parse(iterator, resultBean);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultBean;
    }
 
 
    public static void main(String[] args) {
		String deptXML = "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><ns2:getJudgmentDocResponse xmlns:ns2=\"http://webservice.rg.com/\"><return>{\"a\":\"123\"}</return></ns2:getJudgmentDocResponse></soap:Body></soap:Envelope>";
        WsReturnDto ret = parseSoapMessage(deptXML);
        System.out.println(ret);
        try {
//            SOAPMessage msg = formatSoapString(deptXML);
//            SOAPBody body = msg.getSOAPBody();
//            Iterator<SOAPElement> iterator = body.getChildElements();
//            PrintBody(iterator, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
 
 
    /**
     * 把soap字符串格式化为SOAPMessage
     *
     * @param soapString
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static SOAPMessage formatSoapString(String soapString) {
        MessageFactory msgFactory;
        try {
            msgFactory = MessageFactory.newInstance();
            SOAPMessage reqMsg = msgFactory.createMessage(new MimeHeaders(),
                    new ByteArrayInputStream(soapString.getBytes("UTF-8")));
            reqMsg.saveChanges();
            return reqMsg;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
 
 
    /**
     * 解析soap xml
     * @param iterator
     * @param resultBean
     */
    private static void parse(Iterator<SOAPElement> iterator, WsReturnDto resultBean) {
        while (iterator.hasNext()) {
            SOAPElement element = iterator.next();
            if ("return".equals(element.getNodeName())) {
            	String result = element.getValue();
            	resultBean.setResult(result);
            } else if (null == element.getValue() && element.getChildElements().hasNext()) {
                parse(element.getChildElements(), resultBean);
            }
        }
    }
 
 


}
