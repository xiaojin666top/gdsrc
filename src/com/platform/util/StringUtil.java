package com.platform.util;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.beawan.base.dto.RandomizingID;
import com.beawan.common.Constants;

public class StringUtil extends org.apache.commons.lang3.StringUtils {
	public static String getMD5(String str) {
		MessageDigest messageDigest = null;
		try {
			messageDigest = MessageDigest.getInstance("MD5");
			messageDigest.reset();
			messageDigest.update(str.getBytes("UTF-8"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		byte[] byteArray = messageDigest.digest();
		StringBuffer md5Buffer = new StringBuffer();
		for (int i = 0, length = byteArray.length; i < length; i++) {
			if (Integer.toHexString(0xFF & byteArray[i]).length() == 1)
				md5Buffer.append("0").append(Integer.toHexString(0xFF & byteArray[i]));
			else
				md5Buffer.append(Integer.toHexString(0xFF & byteArray[i]));
		}
		return md5Buffer.toString();
	}

	public static List<String> getValueList(String value) {
		List<String> results = new ArrayList<String>();
		String[] values = value.split("\\|");
		for (int i = 0; i < values.length; i++) {
			results.add(values[i]);
		}
		return results;
	}
	
	/***
	 * 去除字符串中的空格、回车、换行符、制表符
	 * @param str
	 * @return
	 */
	public static String replaceBlank(String str) {
		String dest = "";
		if (str != null) {
			Pattern p = Pattern.compile("\\s*|\t|\r|\n");
			Matcher m = p.matcher(str);
			dest = m.replaceAll("");
		}
		return dest;

	}
	
	public static boolean isNotEmptyString(String str) {
		return !(str == null || "".equals(str));
	}
	
	/**
	 * 获取驼峰命名
	 * 
	 * @param name
	 *            只能使用下划线分割
	 * @return 首字母小写,第二单词首字母大写,去掉下划线
	 */
	public static String getCamel(String name) {
		name = name.toLowerCase();
		String[] tmps = name.split("_");
		if (tmps.length == 1) {
			return name;
		}
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < tmps.length; i++) {
			if (i == 0) {
				buf.append(tmps[i]);
			} else {
				String tmp = tmps[i];
				buf.append(tmp.substring(0, 1).toUpperCase()).append(
						tmp.substring(1));
			}
		}
		return buf.toString();
	}

	public static boolean isEmptyString(String str) {
		return str == null || "".equals(str);
	}

	/**
	 * 字符串左边打补丁
	 * 
	 * @author mengbin
	 * @param str
	 *            : 需要打补丁的字符串
	 * @param padChar
	 *            : 补丁字符
	 * @param len
	 *            : 打补丁后的长度
	 * @date 2013-12-16 下午07:33:25
	 */
	public static String paddingLeft(String str, String padChar, int len) {
		if (str == null) {
			return null;
		}
		if (str.length() >= len) {
			return str;
		}
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < (len - str.length()); i++) {
			buf.append(padChar);
		}
		buf.append(str);
		return buf.toString();
	}

	/**
	 * 去除html片段中标签
	 * 
	 * @author mengbin
	 * @date 2014年1月14日 上午11:09:49
	 */
	public static String trimHtml(String html) {
		String regEx_script = "<script[^>]*?>[\\s\\S]*?<\\/script>"; // 定义script的正则表达式
		String regEx_style = "<style[^>]*?>[\\s\\S]*?<\\/style>"; // 定义style的正则表达式
		String regEx_html = "<[^>]+>"; // 定义HTML标签的正则表达式

		Pattern p_script = Pattern.compile(regEx_script,
				Pattern.CASE_INSENSITIVE);
		Matcher m_script = p_script.matcher(html);
		html = m_script.replaceAll(""); // 过滤script标签

		Pattern p_style = Pattern
				.compile(regEx_style, Pattern.CASE_INSENSITIVE);
		Matcher m_style = p_style.matcher(html);
		html = m_style.replaceAll(""); // 过滤style标签

		Pattern p_html = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);
		Matcher m_html = p_html.matcher(html);
		html = m_html.replaceAll(""); // 过滤html标签

		return html.trim(); // 返回文本字符串
	}

	/**
	 * 去除字符串中标签
	 * 
	 * @author fengjj
	 * @date 2014年1月14日 上午11:09:49
	 */
	public static String removeHtml(String html) {
		String regEx_left = "<[^>]*>"; // 定义HTML标签的正则表达式
		String regEx_centre = "\\&[a-zA-Z]{1,10};";
		String regEx_right = "[(/>)<]";

		Pattern p_centre = Pattern.compile(regEx_centre,
				Pattern.CASE_INSENSITIVE);
		Matcher m_centre = p_centre.matcher(html);
		html = m_centre.replaceAll(""); // 替换特殊字符

		Pattern p_left = Pattern.compile(regEx_left, Pattern.CASE_INSENSITIVE);
		Matcher m_left = p_left.matcher(html);
		html = m_left.replaceAll(""); // 替换左标签

		Pattern p_right = Pattern
				.compile(regEx_right, Pattern.CASE_INSENSITIVE);
		Matcher m_right = p_right.matcher(html);
		html = m_right.replaceAll(""); // 替换右标签

		return html.trim(); // 返回文本字符串
	}

	/**
	 * 剔除结尾的br-正则
	 * @param cs 字符序列
	 * @return 删除html标签后的字符序列
	 */
	public static String replaceEndBrHtml(String cs){
		String rex = "^(.*)(<br/?>)$";
		Pattern comPile = Pattern.compile(rex);
		Matcher matcher = comPile.matcher(cs);
		while(matcher.find()){
			cs = matcher.group(1);
			matcher = comPile.matcher(cs);
		}
		return cs;
	}

	/**
	 * 链接数组返回SQL字符串
	 * 
	 * @author mengbin
	 * @date 2014-1-21 下午07:04:11
	 */
	public static String joinSql(String[] arr) {
		if (arr == null || arr.length < 1) {
			return null;
		}
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < arr.length; i++) {
			buf.append((i < arr.length - 1) ? "'" + arr[i] + "'," : "'"
					+ arr[i] + "'");
		}
		return buf.toString();
	}

	/**
	 * 返回SQL字符串
	 * 
	 * @author mengbin
	 * @date Feb 22, 20143:33:41 PM
	 */
	public static String joinSql(long[] arr) {
		if (arr == null || arr.length < 1) {
			return null;
		}
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < arr.length; i++) {
			buf.append((i < arr.length - 1) ? "'" + arr[i] + "'," : "'"
					+ arr[i] + "'");
		}
		return buf.toString();
	}

	/**
	 * 返回SQL字符串
	 * 
	 * @author mengbin
	 * @date Jan 25, 20145:38:27 PM
	 */
	public static String joinSql(Object[] objs) {
		if (objs == null || objs.length < 1) {
			return null;
		}
		String[] arr = new String[objs.length];
		for (int i = 0; i < objs.length; i++) {
			arr[i] = String.valueOf(objs[i]);
		}
		return joinSql(arr);
	}

	/**
	 * 转换成SQL查询语句
	 * 
	 * @author mengbin
	 * @date Jan 25, 20142:19:07 PM
	 */
	public static String joinSql(List<?> list) {
		if (list == null || list.size() < 1) {
			return null;
		}
		String[] arr = new String[list.size()];
		for (int i = 0; i < list.size(); i++) {
			arr[i] = String.valueOf(list.get(i));
		}
		return joinSql(arr);
	}

	/**
	 * 判断字符串是否在字符串数组内
	 * 
	 * @author yizuchao
	 * @date March 24, 20142:19:07 PM
	 */

	public static boolean strIsInArray(String str, String[] strArray)
			throws Exception {
		if (str == null || strArray == null || strArray.length <= 0) {
			throw new Exception("the params is invalid");
		}
		for (String s : strArray) {
			if (s.equals(str))
				return true;
		}
		return false;
	}

	/*
	 * 由于Java是基于Unicode编码的，因此，一个汉字的长度为1，而不是2。
	 * 但有时需要以字节单位获得字符串的长度。例如，“123abc长城”按字节长度计算是10，而按Unicode计算长度是8。
	 * 为了获得10，需要从头扫描根据字符的Ascii来获得具体的长度
	 * 。如果是标准的字符，Ascii的范围是0至255，如果是汉字或其他全角字符，Ascii会大于255。
	 * 因此，可以编写如下的方法来获得以字节为单位的字符串长度。
	 */
	public static int getWordCount(String s) {
		int length = 0;
		for (int i = 0; i < s.length(); i++) {
			int ascii = Character.codePointAt(s, i);
			if (ascii >= 0 && ascii <= 255) {
				length++;
			} else {
				length += 2;
			}
		}
		return length;
	}

	/**
	 * 用特定字符填充字符串
	 * 
	 * @param sSrc 要填充的字符串
	 * @param ch 用于填充的特定字符
	 * @param nLen 要填充到的长度
	 * @param bLeft  要填充的方向：true:左边；false:右边
	 * @return 填充好的字符串
	 */
	public static String fill(String sSrc, char ch, int nLen, boolean bLeft) {
		if (sSrc == null || sSrc.equals("")) {
			StringBuffer sbRet = new StringBuffer();
			for (int i = 0; i < nLen; i++)
				sbRet.append(ch);

			return sbRet.toString();
		}
		byte[] bySrc = sSrc.getBytes();
		int nSrcLen = bySrc.length;
		if (nSrcLen >= nLen) {
			return sSrc;
		}
		byte[] byRet = new byte[nLen];
		if (bLeft) {
			for (int i = 0, n = nLen - nSrcLen; i < n; i++)
				byRet[i] = (byte) ch;
			for (int i = nLen - nSrcLen, n = nLen; i < n; i++)
				byRet[i] = bySrc[i - nLen + nSrcLen];
		} else {
			for (int i = 0, n = nSrcLen; i < n; i++)
				byRet[i] = bySrc[i];
			for (int i = nSrcLen, n = nLen; i < n; i++)
				byRet[i] = (byte) ch;
		}
		return new String(byRet);
	}

	/**
	 * 去字符串中的数字
	 * @param str
	 * @return
	 */
	public static String getNumber(String str) {
		String regEx = "[^0-9]";
		Pattern p = Pattern.compile(regEx);
		Matcher m = p.matcher(str);
		String result = m.replaceAll("").trim();
		return result;
	}

	public static String getTagVaule(String str , String tag){
		String result = "";
		String regEx = "<" + tag + "[^>]*?>[\\s\\S]*?<\\/" + tag +">";
		Pattern p = Pattern.compile(regEx);
		Matcher m = p.matcher(str);
		if(m.find()){
			result = m.group().replace("<" + tag + ">", "").replace("</" + tag + ">", "");
		}
		return result;
	}
	/**
	 * 从身份证中取生日
	 * @param idCard
	 * @return
	 * @throws Exception
	 */
	public static Date getBirthdayByIdCard(String idCard)throws Exception{
		
		Date date = null;
		
		if(!isEmptyString(idCard)){
			if(idCard.length()==15){
				date = DateUtil.parse("19"+idCard.substring(6, 12), Constants.DATE_MASK2);
			}else if(idCard.length()==18){
				date = DateUtil.parse(idCard.substring(6, 14), Constants.DATE_MASK2);
			}
		}
		
		return date;
	}
	
	/**
	 * TODO 判断字符串是否为纯数字（包含+-号，00视为0，01视为1）
	 * @param str
	 * @return
	 */
	public static boolean isNumber(String str){
		
		if(isEmpty(str))
			return false;
		
		String pattern = "^(-)?\\d+(\\.\\d+)?$";
		
		if(Pattern.matches(pattern, str))
			return true;
		else
			return false;
	}
	
	/**
	 * TODO 判断字符串是否为数值（包含+-号，但0开头的不算，如00,01等）
	 * @param str
	 * @return
	 */
	public static boolean isRealNumber(String str){
		
		if(isEmpty(str))
			return false;
		
		String pattern = "(^(-|\\+)?0)|(^(-|\\+)?[1-9](\\.\\d+)?$)|(^((-|\\+)?0\\.)\\d+$)";
		
		if(Pattern.matches(pattern, str))
			return true;
		else
			return false;
	}
	
	/**
	 * TODO 判断两个字符串是否相等
	 * @param str1
	 * @param str2
	 * @return
	 */
	public static boolean equals(String str1, String str2){
		
		boolean equals = false;
		
		//都为空，则认为相等
		if(str1 == null && str2 == null)
			equals = true;
		
		//都不为空且相等
		if(str1 != null && str2 != null && str1.equals(str2))
			equals = true;
		
		return equals;
	}
	
	public static String subRangeString(String body, String str1, String str2) {
		while (true) {
			int index1 = body.indexOf(str1);
			if (index1 != -1) {
				int index2 = body.indexOf(str2, index1);
				if (index2 != -1) {
					String str3 = body.substring(0, index1) + body.substring(index2 + str2.length(), body.length());
					body = str3;
				} else {
					return body;
				}
			} else {
				return body;
			}
		}
	}
	
	public static String stripHtml(String content) { 
		// <p>段落替换为换行 
		content = content.replaceAll("<p .*?>", "\r\n"); 
		// <br><br/>替换为换行 
		content = content.replaceAll("<br\\s*/?>", "\r\n"); 
		// 去掉其它的<>之间的东西 
		content = content.replaceAll("\\<.*?>", ""); 
		// 还原HTML 
		// content = HTMLDecoder.decode(content); 
		return content; 
		}

	/**
	 * 将金额切分 如： 2500万人民币 切分成 2500，万，人民币 三个部分
	 * 
	 * @return
	 */
	public static String[] spiltMoney(String input) {
		String[] result = new String[3];
		String val = "0.00";
		String unit = "";
		String type = "";
		Pattern pattern1 = Pattern.compile("\\d+");
		Matcher matcher1 = pattern1.matcher(input);
		if (matcher1.find()) {
			val = matcher1.group();
		}
		Pattern pattern2 = Pattern.compile("百亿|十亿|亿|千万|百万|十万|万");
		Matcher matcher2 = pattern2.matcher(input);
		if (matcher2.find()) {
			unit = matcher2.group();
		}
		Pattern pattern3 = Pattern.compile("人民币|美元|欧元|英镑|澳元|日元|加元|新西兰元|澳门币|香港港元|台币");
		Matcher matcher3 = pattern3.matcher(input);
		if (matcher3.find()) {
			type = matcher3.group();
		}
		result[0] = val;
		result[1] = unit;
		result[2] = type;

		return result;
	}
	
	/**
	 * 将 字符串 转化为 double类型 
	 * 如 100万 = 1000000.00
	 * @param str
	 * @return
	 */
	public static Double getDouVal(String str){
		try{
			String[] result = spiltMoney(str);
			String val = result[0];
			String util = result[1];
			if(val==null||"".equals(val))
				return 0.0;
			if("百亿".equals(util)){
				return 10000000000.0*Double.parseDouble(val);
			}else if("十亿".equals(util)){
				return 1000000000.0*Double.parseDouble(val);
			}else if("亿".equals(util)){
				return 100000000.0*Double.parseDouble(val);
			}else if("千万".equals(util)){
				return 10000000.0*Double.parseDouble(val);
			}else if("百万".equals(util)){
				return 1000000.0*Double.parseDouble(val);
			}else if("十万".equals(util)){
				return 100000.0*Double.parseDouble(val);
			}else if("万".equals(util)){
				return 10000.0*Double.parseDouble(val);
			}else{
				return Double.parseDouble(val);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0.0;
	}
	
	/**
	 * 生成14位随机客户号：CM + yyyyMMdd + 6位随机数
	 * @return "CM20201015927126"
	 */
	public static String genRandomNo(){
		RandomizingID random = new RandomizingID("CM", "yyyyMMdd", 6, false);
		return random.genNewId();
	}
}