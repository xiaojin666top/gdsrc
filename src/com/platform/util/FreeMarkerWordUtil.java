package com.platform.util;

import java.io.*;
import java.util.Date;
import java.util.Map;

import com.beawan.common.config.AppConfig;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;

import javax.servlet.http.HttpServletResponse;

public class FreeMarkerWordUtil {

	/**
	 * @Desc：生成word文件
	 * @Author：comlc
	 * @Date：2014-1-22下午05:33:42
	 * @param dataMap
	 *            word中需要展示的动态数据，用map集合来保存
	 * @param templateName
	 *            word模板名称，例如：test.ftl
	 * @param fileName
	 *            生成的文件名称，例如：test.doc
	 */
	public static void createWord(Map<?, ?> dataMap, String templateName,String destPath, String fileName) {
		try {
			// 创建配置实例
			Configuration configuration = new Configuration();
			// 设置编码
			configuration.setDefaultEncoding("UTF-8");
			configuration.setTemplateExceptionHandler(TemplateExceptionHandler.IGNORE_HANDLER);

			//模板文件统一放至template 包下面
			configuration.setClassForTemplateLoading(FreeMarkerWordUtil.class,"/template");

			// 获取模板
			Template template = configuration.getTemplate(templateName);
			template.setTemplateExceptionHandler(TemplateExceptionHandler.IGNORE_HANDLER);

			// 输出文件
			File outFile = new File(destPath + File.separator + fileName);

			// 如果输出目标文件夹不存在，则创建
			if (!outFile.getParentFile().exists()) {
				outFile.getParentFile().mkdirs();
			}

			// 将模板和数据模型合并生成文件
			Writer out = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(outFile), "UTF-8"));

			// 生成文件
			template.process(dataMap, out);

			// 关闭流
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static byte[] createHTML(Map<String, Object> dataMap, String templateName) throws Exception {
		
		byte[] data = null;
		
		Configuration cfg = new Configuration();
		cfg.setDefaultEncoding("UTF-8");
		//String basePath = new String(FinanceModelUtil.class.getResource("/templete/").getPath().getBytes(),"UTF-8");
		try {
			//cfg.setDirectoryForTemplateLoading(new File(basePath));
			cfg.setClassForTemplateLoading(FreeMarkerWordUtil.class,"/templete");
			Template t = cfg.getTemplate(templateName);
			t.setEncoding("UTF-8");
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			Writer out = new OutputStreamWriter(baos, "UTF-8"); // 输出流
			t.process(dataMap, out);
			out.flush();
			out.close();
			
			data = baos.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return data;
	}

	/**
	 * 生成 Excel 文件
	 * @param dataMap 动态数据
	 * @param templateName 模板名称
	 * @param fileName 文件名
	 * @return excel文件
	 */
	public static void exportExcel(Map<?, ?> dataMap, String templateName, String fileName, HttpServletResponse response) {
		OutputStream os = null;
		InputStream is = null;
		try {
			// 创建配置实例
			Configuration configuration = new Configuration();
			// 设置编码
			configuration.setDefaultEncoding("UTF-8");
			configuration.setTemplateExceptionHandler(TemplateExceptionHandler.IGNORE_HANDLER);

			// Excel 模板文件统一放至 template/excel/ 包下面
			configuration.setClassForTemplateLoading(FreeMarkerWordUtil.class, "/template/");

			// 获取模板
			Template template = configuration.getTemplate(templateName,  "utf-8");
			template.setTemplateExceptionHandler(TemplateExceptionHandler.IGNORE_HANDLER);

			String destPath = AppConfig.Cfg.ACCOUNT_PATH +DateUtil.formatDate(new Date(), "yyyy-MM" ) ;
			File excelFile = new File(destPath + File.separator + fileName);

			// 如果输出目标文件夹不存在，则创建
			if (!excelFile.getParentFile().exists()) {
				excelFile.getParentFile().mkdirs();
			}

			// 将模板和数据模型合并生成文件
			Writer out = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(excelFile), "UTF-8"));

			// 生成Excel文件
			template.process(dataMap, out);
			out.close();

			// 导出Excel文件
			response.setContentType("application/msexcel; charset=utf-8");
			response.setHeader("Content-Disposition","attachment;filename="+new String(fileName.getBytes(),"iso-8859-1"));
			os = response.getOutputStream();
			is = new BufferedInputStream(new FileInputStream(excelFile));

			byte[] buf = new byte[1024];
			int len = 0;
			while ((len = is.read(buf)) > 0) {
				os.write(buf, 0, len);
				os.flush();
			}
			os.flush();
			os.close();
			is.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
