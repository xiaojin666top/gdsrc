package com.platform.util;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;


/**
 * 动态创建Java类，并初始化返回实例
 * @version 0.5
 * @author rain
 */
public class BeanCreator {
	
	private static JavaCompiler compiler;
	private static Map<String,Class<?>> classesPool;
	
    static{
        compiler = ToolProvider.getSystemJavaCompiler();
        classesPool = new HashMap<String,Class<?>>();
    }
    
	public static void main(String[] args) throws Exception{
		
		String packegeName = "com.rain";
		String className = "rain_test";
		FieldDescriptor fd1 = new FieldDescriptor("sfs_kkk", String.class);
		FieldDescriptor fd2 = new FieldDescriptor("idijd_ee_f", Double.class);
		FieldDescriptor[] fields = new FieldDescriptor[]{fd1, fd2};
		
		Object obj = createBean(packegeName, className, null, fields);
		System.out.println(obj.toString());
	}
	
	public static Object createBean(String packegeName, String className,
			Class<?>[] parents, FieldDescriptor[] fields) throws Exception{
		
		Object obj = null;
		
		String fullClassName = genFullClassName(packegeName, className);
		Class<?> clazz = null;
		if(classesPool.containsKey(fullClassName))
			clazz = classesPool.get(fullClassName);
		else
			clazz = BeanCreator.createClass(packegeName, className, parents, fields);
		
		if(clazz != null)
			obj = clazz.newInstance();
		
		return obj;
	}
	
	/**
	 * TODO 创建Java类并装载
	 * @param packegeName 包名
	 * @param className 类名
	 * @param parents 父类集合
	 * @param fields 属性集合
	 * @return
	 * @throws Exception
	 */
	public static Class<?> createClass(String packegeName, String className,
			Class<?>[] parents, FieldDescriptor[] fields) throws Exception{
		
		Class<?> clazz = null;
		//生成Java文件
		File javaFile = genJavaFile(packegeName, className, parents, fields, null);
		//编译
		if(compile(javaFile)){
			String fullClassName = genFullClassName(packegeName, className);
			clazz = BeanCreator.class.getClassLoader().loadClass(fullClassName);
			classesPool.put(fullClassName, clazz);
		}
		
		return clazz;
	}
	
	/**
	 * TODO 编译Java文件
	 * @param javaFile
	 * @return
	 */
	public static boolean compile(File javaFile){
		
		boolean success = false;
		StandardJavaFileManager manager = null;
		
        try{
			//设置编译参数
			List<String> ops = new ArrayList<String>();
	        ops.add("-Xlint:unchecked");
			
			manager = compiler.getStandardFileManager(null, null, null);
	        Iterable<? extends JavaFileObject> it = manager.getJavaFileObjects(javaFile);
	        JavaCompiler.CompilationTask task = compiler.getTask(null, manager, null, ops, null, it);
	        task.call();
	        
	        success = true;
	        
        }finally{
            if(manager!=null){
                try {
                    manager.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            
            javaFile.delete();
        }
        
        return success;
	}
	
    /**
     * TODO 生成java文件
     * @param packegeName 包名
	 * @param className 类名
	 * @param parents 父类集合
	 * @param fields 属性集合
     * @param filePath 文件存储路径
     * @return
     * @throws Exception
     */
    private static File genJavaFile(String packegeName, String className,
			Class<?>[] parents, FieldDescriptor[] fields, String filePath) throws Exception{
        
    	File file = null;
        BufferedWriter bw = null;
        
        try{
        	//生成Java代码
    		String javaCode = genJavaCode(packegeName, className, parents, fields);
    		
        	packegeName = genPackegeName(packegeName);
    		className = genClassName(className);
    		URL url = BeanCreator.class.getResource("/");
    		if(filePath == null)
    			filePath = URLDecoder.decode(url.getPath(),"UTF-8") + packegeName.replace(".", "/");
    		File dir = new File(filePath);
    		if(!dir.exists())
    			dir.mkdirs();
    		
    		file = new File(dir, className+".java");
    		file.createNewFile();
    		
            bw = new BufferedWriter(new FileWriter(file));
            bw.write(javaCode);
            bw.flush();
            
        }catch(Exception e){
            throw e;
        }finally{
            if(bw!=null){
                bw.close();
            }
        }
        
        return file;
    }

	/**
	 * TODO 生成Java源代码
	 * @param packegeName 包名
	 * @param className 类名
	 * @param parents 父类集合
	 * @param fields 属性集合
	 * @return
	 */
	public static String genJavaCode(String packegeName, String className,
			Class<?>[] parents, FieldDescriptor[] fields) {
		
		if(className == null || className == "")
			return null;
		
		className = genClassName(className);
		
		String code = "";
		
		if(packegeName != null)
			code += "package " + genPackegeName(packegeName) + ";\n\n";
		
		String importStr = "";
		String extendstr = "";
		if(parents != null && parents.length > 0){
			for(Class<?> clazz : parents){
				if(!importStr.contains(clazz.getName()))
					importStr += "import " + clazz.getName() + ";\n";
				extendstr += "," + clazz.getName();
			}
			extendstr = " extends " + extendstr.substring(1, extendstr.length());
		}
		
		String fieldStr = "";
		String getsetStr = "";
		String toString = "";
		if(fields != null && fields.length > 0){
			for(FieldDescriptor field : fields){
				String fieldType = field.getFieldType().getName();
				if(!importStr.contains(fieldType))
					importStr += "import " + fieldType + ";\n";
				
				String fieldName = genFieldName(field.getFieldName());
				fieldStr += "	private " + fieldType + " " + fieldName + ";\n";
				
				getsetStr += "	public " + fieldType + " " + genGetMethodName(fieldName) + "() {\n"
						   + "		return this." + fieldName + ";\n"
						   + "	}\n\n";
				
				getsetStr += "	public void " + genSetMethodName(fieldName) + "("
						   + fieldType + " " + fieldName + ") {\n"
						   + "		this." + fieldName + " = " + fieldName + ";\n"
						   + "	}\n\n";
				
				toString += fieldName + "=\"+" + fieldName + "+\",";
			}
			
			toString = toString.substring(0, toString.length() - 3);
			toString = "	@Override\n	public String toString() {\n		return \""
			         + className + "[\"+\"" + toString + "+\"]\";\n	}";
		}
		
		code += importStr + "\npublic class " + className + extendstr 
			  + " implements java.io.Serializable {\n\n"
			  + fieldStr + "\n" + getsetStr + toString + "}\n";
		
		return code;
	}
	
	public static String genFullClassName(String packegeName, String className) {
		return genPackegeName(packegeName) + "." + genClassName(className);
	}
	
	public static String genPackegeName(String packegeName) {
		return packegeName.toLowerCase();
	}
	
	public static String genClassName(String className) {
		String temp = genFieldName(className);
		className = temp.substring(0, 1).toUpperCase()+temp.substring(1);
		return className;
	}
	
	public static String genFieldName(String fieldName) {
		
		fieldName = fieldName.toLowerCase();
		if(!fieldName.contains("_"))
			return fieldName;
		
		String[] parts = fieldName.split("_");
		String newName = parts[0];
		if(parts.length > 1){
			for(int i=1;i<parts.length;i++){
				newName += parts[i].substring(0, 1).toUpperCase() + parts[i].substring(1);
			}
		}
		
		return newName;
	}
	
	public static String genGetMethodName(String fieldName) {
		return "get"+fieldName.substring(0, 1).toUpperCase()+fieldName.substring(1);
	}
	
	public static String genSetMethodName(String fieldName) {
		return "set"+fieldName.substring(0, 1).toUpperCase()+fieldName.substring(1);
	}

	/**
	 * bean属性描述
	 * @author rain
	 */
	public static class FieldDescriptor {

		private String fieldName;// 属性名称
		private Class<?> fieldType;// 属性类型
		
		public FieldDescriptor(){}
		
		public FieldDescriptor(String fieldName, Class<?> fieldType){
			this.fieldName = fieldName;
			this.fieldType = fieldType;
		}

		public String getFieldName() {
			return fieldName;
		}

		public void setFieldName(String fieldName) {
			this.fieldName = fieldName;
		}

		public Class<?> getFieldType() {
			return fieldType;
		}

		public void setFieldType(Class<?> fieldType) {
			this.fieldType = fieldType;
		}

	}

}