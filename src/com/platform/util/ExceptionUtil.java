package com.platform.util;

import com.beawan.common.BusinessException;

public class ExceptionUtil
{
	public static void throwException(String message)throws Exception
	{
		throw new BusinessException(message);
	}
	
	public static void throwException(String message, String flag)throws Exception
	{
		throw new BusinessException(message, flag);
	}
	
}
