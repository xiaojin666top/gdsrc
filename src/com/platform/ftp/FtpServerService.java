package com.platform.ftp;

import java.io.File;
import java.net.URLDecoder;

import org.apache.ftpserver.FtpServer;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.listener.ListenerFactory;
import org.apache.ftpserver.usermanager.ClearTextPasswordEncryptor;
import org.apache.ftpserver.usermanager.PropertiesUserManagerFactory;
import org.apache.log4j.Logger;

import com.beawan.common.Constants;
import com.platform.util.PropertiesUtil;

public class FtpServerService {
	
	private static final Logger log = Logger.getLogger(FtpServerService.class);

	private FtpServer ftpServer = null;
	
	private static FtpServerService instance;
	
	private FtpServerService(){
		initFtpServer();
	}
	
	public static FtpServerService getInstance(){
		if(instance == null)
			instance = new FtpServerService();
		return instance;
	}

	private void initFtpServer() {
		
		try {
			
			if(ftpServer != null)
				return;
			
			String ftpserverPort = PropertiesUtil.getProperty(Constants.SYSTEM_PROPS_PATH, "ftpserver.port");
			
			FtpServerFactory serverFactory = new FtpServerFactory();
			ListenerFactory listenerFactory = new ListenerFactory();
			listenerFactory.setPort(Integer.parseInt(ftpserverPort)); // 设置端口
			serverFactory.addListener("default", listenerFactory.createListener());
			
			String filePath = URLDecoder.decode(this.getClass()
					.getClassLoader().getResource("/properties").getPath(), "UTF-8");
			
			PropertiesUserManagerFactory userManagerFactory = new PropertiesUserManagerFactory();
			userManagerFactory
					.setFile(new File(filePath + "ftpuser.properties"));
			// 设定了用户输入密码时 直接输入明文
			userManagerFactory
					.setPasswordEncryptor(new ClearTextPasswordEncryptor());
			serverFactory
					.setUserManager(userManagerFactory.createUserManager());
			ftpServer = serverFactory.createServer();
			
		} catch (Exception e) {
			log.info("ftp server init failed", e);
			e.printStackTrace();
		}
	}

	public void startFtpServer() {
//		try {
//			if(ftpServer.isStopped()){
//				ftpServer.start();
//				log.info("ftp server starting");
//			}else
//				log.info("ftp server is running");
//		} catch (FtpException e) {
//			log.info("ftp server start failed", e);
//			e.printStackTrace();
//		}
	}

	public void stopFtpServer() {
		if(!ftpServer.isStopped())
			ftpServer.stop();
		log.info("ftp server is stopped");
	}

	public FtpServer getFtpServer() {
		return ftpServer;
	}

}
