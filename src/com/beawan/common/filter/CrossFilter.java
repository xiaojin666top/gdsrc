package com.beawan.common.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.OncePerRequestFilter;


/**
 * 前后端项目分离的跨域问题：
 * 通过拦截器 或者过滤器 对响应头内容进行配置  
 * 具体配置信息如如下；
 * 需注意一点originHeader  写*会出现问题，这个要配置成前端服务器的地址。待项目上线之后 固定下来
 * @author yuzhejia
 *
 */
public class CrossFilter extends OncePerRequestFilter{

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
//		System.out.println("filter 拦截");
		
		String originHeader = request.getHeader("Origin");
		//System.out.println(originHeader);
		response.addHeader("Access-Control-Allow-Origin", originHeader);  
		response.addHeader("Access-Control-Allow-Methods","PUT,POST,GET,DELETE");
		response.addHeader("Access-Control-Allow-Headers", "Content-Type"); 
		response.addHeader("Access-Control-Allow-Headers", "x-requested-with"); 
		response.addHeader("Access-Control-Max-Age", "1800"); 
		response.addHeader("Access-Control-Allow-Credentials", "true");
		filterChain.doFilter(request, response);
	}

}
