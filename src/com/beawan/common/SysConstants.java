package com.beawan.common;

public class SysConstants {
	
	public interface YesOrNo {
		public static final String Yes = "Y";
		public static final String No = "N";
		/**
		 * 是否
		 */
		public static final String STD_ZX_YES_NO="STD_ZX_YES_NO";
	}
	
	/** 用户状态 **/
	public interface UserStatus {
		/**
		 * 正常
		 */
		public static final String NORMAL = "N";// 正常
		/**
		 * 锁定
		 */
		public static final String LOCKED = "L";// 锁定
		/**
		 * 注销
		 */
		public static final String CANCEL = "C";// 注销
	}
	
	/***用户签到状态***/
	public interface UserSignStatus{
		/**
		 * 在线
		 */
		public static final String ON_LINE = "online";// 在线
		/**
		 * 离线
		 */
		public static final String OFF_LINE = "offline";// 离线
	}
	
	/***用户角色***/
	public interface UserRole{
		
		/**
		 * 超级管理员
		 */
		public static final String SUPER_ADMIN = "0008";
		
		/**
		 * 系统管理员
		 */
		public static final String SYSTEM_ADMIN = "0000";
		
		/**
		 * 客户经理
		 */
		public static final String CUSTOMER_MANAGER = "0001";
		
		/**
		 * 运行主管（支行）
		 */
		public static final String OPER_DIRECTOR = "0002";
		
		/**
		 * 支行行长
		 */
		public static final String BRANCH_MANAGER = "0003";
		
		/**
		 * 评审经理
		 */
		public static final String REVIEW_MANAGER = "0004";
		
		/**
		 * 总行审批岗
		 */
		public static final String APPROVER = "0005";
	}
	
	/***用户数据查询权限***/
	public interface UserDataAccess{
		/**
		 * 全部数据
		 */
		public static final String ALL = "0";
		/**
		 * 所属机构下数据
		 */
		public static final String ORG = "1";
		/**
		 * 所属用户下数据
		 */
		public static final String PERSONAL = "2";
	}
	
	/**
	 * 平台操作日志类型
	 * @author beawan_fengjj
	 *
	 */
	public interface PlatformLogType {
		
		/**
		 * 用户登陆
		 */
		public static final String USER_LOGIN = "01";
		
		/**
		 * 新建任务
		 */
		public static final String NEW_TASK = "02";
		
		/**
		 * 删除任务
		 */
		public static final String DELETE_TASK = "03";
		
		/**
		 * 提交任务
		 */
		public static final String SUBMIT_TASK = "04";
		
		
		/**
		 * 新增用户
		 */
		public static final String NEW_USER = "NU";
		/**
		 * 修改用户
		 */
		public static final String MODIFY_USER = "MU";
		/**
		 * 注销用户
		 */
		public static final String CANCAL_USER = "CU";
		/**
		 * 启用用户
		 */
		public static final String USE_USER = "UU";
		/**
		 * 删除用户
		 */
		public static final String DELETE_USER = "DU";
		/**
		 * 修改密码
		 */
		public static final String MODIFY_PASSWORD = "MP";
	}

	public interface ExceptionConstant {
		/** @Field @UNDEFINED_EXP : (Controller层异常) */
		public static final String UNDEFINED_EXP = "系统未知异常";

		/** @Field @DATA_ACCESS_EXP : (Dao层数据操作异常) */
		public static final String DATA_ACCESS_EXP = "数据操作异常";

		/** @Field @DATA_ACCESS_EXP : (Service层业务异常) */
		public static final String BUSINESS_ACCESS_EXP = "系统业务异常";
	}
	
	public interface TreeDicConstant {

		/** @Field @STD_ZB_LOAN_TYPE : 国标行业分类 （2011） */
		public static final String GB_IC_4754_2011 = "STD_GB_4754-2011";
		
		/** @Field @STD_ZB_LOAN_TYPE : 国标行业分类 （2017）*/
		public static final String GB_IC_4754_2017 = "STD_GB_4754-2017";
	}

	public interface BsDicConstant {


		/** @Field STD_REL_TYPE : (个人客户类型) */
		public static final String  STD_REL_ENT_TYPE = "STD_REL_ENT_TYPE";

		/** @Field STD_REL_TYPE : (企业客户类型) */
		public static final String  STD_REL_IND_TYPE = "STD_REL_IND_TYPE";
		
		/** @Field @STD_ZB_LOAN_TYPE : (证件类型) */
		public static final String STD_ZB_CERT_TYP = "STD_ZB_CERT_TYP";

		/** @Field @STD_ZB_USE_TYPE : (贷款用途) */
		public static final String STD_ZB_USE_TYPE = "STD_ZB_USE_TYPE";

		/** @Field @STD_GUAR_FORM : (保证形式) */
		public static final String STD_GUAR_FORM = "STD_GUAR_FORM";

		/** @Field @STD_GUAR_FORM : (还款方式) */
		public static final String STD_REPLY_WAY = "STD_REPLY_WAY";
		
		
		/** @Field STD_SY_GUAR_TYPE : (射阳担保方式) */
		public static final String  STD_SY_GUAR_TYPE = "STD_SY_GUAR_TYPE";
		
		/** @Field STD_ZB_FIVE_SORT : (贷款现五级分类) */
		public static final String  STD_ZB_FIVE_SORT = "STD_ZB_FIVE_SORT";
		
		/** @Field STD_ZB_LIMIT_TYPE : (授信额度类型) */
		public static final String  STD_ZB_LIMIT_TYPE = "STD_ZB_LIMIT_TYPE";
		
		/** @Field STD_IQP_PAY_TYPE : (贷款支付方式) */
		public static final String  STD_IQP_PAY_TYPE = "STD_IQP_PAY_TYPE";
		
		/** @Field STD_SY_GUARANTOR_TYPE : (保证人类型) */
		public static final String  STD_SY_GUARANTOR_TYPE = "STD_SY_GUARANTOR_TYPE";
		
		/** @Field STD_SY_COM_SURVEY_TYPE : (企业授信调查报告模板类型) */
		public static final String  STD_SY_COM_SURVEY_TYPE = "STD_SY_COM_SURVEY_TYPE";
		
		/** @Field STD_PROMPT_MSG_CATEGORY : 指导提示消息类别*/
		public static final String  STD_PROMPT_MSG_CATEGORY = "STD_PROMPT_MSG_CATEGORY";
		
		/** @Field STD_MB_ACC_TYPE : (本行账户性质) */
		public static final String  STD_MB_ACC_TYPE = "STD_MB_ACC_TYPE";
		
		/** @Field STD_IS_MB_HOLDER : (是否本行股东) */
		public static final String  STD_IS_MB_HOLDER = "STD_IS_MB_HOLDER";
		
		/** @Field STD_CUS_BUSINESS_STATUS : (存量客户标识) */
		public static final String  STD_CUS_BIZ_STATUS = "STD_CUS_BIZ_STATUS";
		
		/** @Field STB_CUS_BIZ_TAXES : (纳税等级标识) */
		public static final String STB_CUS_BIZ_TAXES = "STB_CUS_BIZ_TAXES";
		
		/** @Field STB_CUS_BIZ_EP : (环评信用等级) */
		public static final String STB_CUS_BIZ_EP = "STB_CUS_BIZ_EP";
		
		/** @Field STD_IS_DISCHARGE_PERMIT : (是否有排污许可证) */
		public static final String STD_IS_DISCHARGE_PERMIT = "STD_IS_DISCHARGE_PERMIT";
		
		/** @Field STD_COM_RELATION : (企业关联关系) */
		public static final String  STD_COM_RELATION = "STD_COM_RELATION";
		
		/** @Field STD_INDUSTRY_POLICY : 行业政策 */
		public static final String  STD_INDUSTRY_POLICY = "STD_INDUSTRY_POLICY";
		
		/** @Field STD_INDUS_POLICY_TYPE : 行业政策类型 */
		public static final String  STD_INDUS_POLICY_TYPE = "STD_INDUS_POLICY_TYPE";
		

		/** @Field STD_ENERGY_AND_PAY : 能源耗用及工资情况统计项目 */
		public static final String  STD_ENERGY_AND_PAY = "STD_ENERGY_AND_PAY";
		
		/** @Field STD_CONST_QUALI_TYPE : 施工资质类型 */
		public static final String  STD_CONST_QUALI_TYPE = "STD_CONST_QUALI_TYPE";
		
		/** @Field STD_PROJECT_TAKE_TYPE : 工程承接方式 */
		public static final String  STD_PROJECT_TAKE_TYPE = "STD_PROJECT_TAKE_TYPE";
		
		/** @Field STD_COM_EXT_GUARA_REL : 企业对外担保关系 */
		public static final String  STD_COM_EXT_GUARA_REL = "STD_COM_EXT_GUARA_REL";
	
		/** @Field STD_FIXED_INVEST : 固定资产投资资金使用情况 */
		public static final String  STD_FIXED_INVEST = "STD_FIXED_INVEST";
		
		/** @Field STD_FIXED_INVESTEST : 固定资产投资估算 */
		public static final String  STD_FIXED_INVESTEST = "STD_FIXED_INVESTEST";
		
		/** @Field STD_FINAPLAN_TYPE : 固定资产筹资计划 */
		public static final String  STD_FINAPLAN_TYPE = "STD_FINAPLAN_TYPE";
		
		/** @Field STD_CASHFLOW_TYPE : 固定资产现金流测算*/
		public static final String  STD_CASHFLOW_TYPE = "STD_CASHFLOW_TYPE";
		
		/** @Field STD_ZB_FIXED_PROJ_TYPE : 固定资产贷款类型*/
		public static final String  STD_ZB_FIXED_PROJ_TYPE = "STD_ZB_FIXED_PROJ_TYPE";
		
		/** @Field STD_ZX_YES_NO : 是否*/
		public static final String  STD_ZX_YES_NO = "STD_ZX_YES_NO";
		
		
		/** @Field STD_OWNERSHIP_NATURE : 固定资产-权属性质*/
		public static final String  STD_OWNERSHIP_NATURE = "STD_OWNERSHIP_NATURE";
		
		/** @Field STD_LAND_NATURE : 固定资产-土地性质*/
		public static final String  STD_LAND_NATURE = "STD_LAND_NATURE";
		
		/** @Field STD_LAND_USE : 固定资产-土地用途*/
		public static final String  STD_LAND_USE = "STD_LAND_USE";
		
		/** @Field STD_ACC_TYPE : 账款类型*/
		public static final String  STD_ACC_TYPE = "STD_ACC_TYPE";
		
		/** @Field STD_CASH_TYPE : 资金类型*/
		public static final String  STD_CASH_TYPE = "STD_CASH_TYPE";
		
		/** @Field STD_INVENTORY_TYPE : 存货类型*/
		public static final String  STD_INVENTORY_TYPE = "STD_INVENTORY_TYPE";
		
		/** @Field STD_POWER_TYPE : 能源类型*/
		public static final String  STD_POWER_TYPE = "STD_POWER_TYPE";
		
		/** @Field STD_ACC_AGE_TYPE : 应收账款账龄*/
		public static final String  STD_ACC_AGE_TYPE = "STD_ACC_AGE_TYPE";
		
		/** @Field STD_SY_CL : (授信企业关联关系) */
		public static final String  STD_SY_CL = "STD_SY_CL";
		
		/** @Field STD_SY_GB00015 : (企业性质) */
		public static final String  STD_SY_GB00015 = "STD_SY_GB00015";
		
		/** @Field STD_SY_JB00001 : (企业规模) */
		public static final String  STD_SY_JB00001 = "STD_SY_JB00001";
		
		/** @Field STD_SY_GB00016 : (关联关系) */
		public static final String  STD_SY_GB00016 = "STD_SY_GB00016";
		
		/** @Field STD_SY_GB00009 : (国籍) */
		public static final String  STD_SY_GB00009 = "STD_SY_GB00009";
		
		/** @Field STD_SY_JB00035 : (证件类型) */
		public static final String  STD_SY_JB00035 = "STD_SY_JB00035";
		
		/** @Field STD_SY_GB00005 : (性别) */
		public static final String  STD_SY_GB00005 = "STD_SY_GB00005";
		
		/** @Field STD_SY_GB00002 : (婚姻状况) */
		public static final String  STD_SY_GB00002 = "STD_SY_GB00002";
		
		/** @Field STD_SY_GB00003 : (学历) */
		public static final String  STD_SY_GB00003 = "STD_SY_GB00003";
		
		/** @Field STD_SY_ODS0004 :职务*/
		public static final String  STD_SY_ODS0004 = "STD_SY_ODS0004";
		
		/** @Field STD_SY_JB00036 :职称*/
		public static final String  STD_SY_JB00036 = "STD_SY_JB00036";
		
		/** @Field STD_SY_JB00029 :贷款十级分类*/
		public static final String  STD_SY_JB00029 = "STD_SY_JB00029";
		
		/** @Field STD_SY_JB00015 :贷款担保方式*/
		public static final String  STD_SY_JB00015 = "STD_SY_JB00015";
		
		/** @Field STD_SY_ODS0400 :还款方式*/
		public static final String  STD_SY_ODS0400 = "STD_SY_ODS0400";
		
		/** @Field STD_SY_COM01 :射阳币种*/
		public static final String  STD_SY_COM01 = "STD_SY_COM01";
		
		/** @Field STD_SY_ODS0001 :客户类型*/
		public static final String  STD_SY_ODS0001 = "STD_SY_ODS0001";
		
		/** @Field STD_SY_ODS0112 :客户财务报表所属*/
		public static final String  STD_SY_ODS0112 = "STD_SY_ODS0112";
		
		/** @Field STD_SY_CFS_CYC :财务报表周期*/
		public static final String  STD_SY_CFS_CYC = "STD_SY_CFS_CYC";
		
		/** @Field STD_SY_CFS_STATUS :财务报表状态*/
		public static final String  STD_SY_CFS_STATUS = "STD_SY_CFS_STATUS";
		
		
		
		/** @Field STD_SY_BUSI_LINE :用户业务条线*/
		public static final String  STD_SY_BUSI_LINE = "STD_SY_BUSI_LINE";
		
		/** @Field STD_USER_STATUS :用户状态*/
		public static final String  STD_USER_STATUS = "STD_USER_STATUS";
		
		/** @Field STD_ALLOW_PLAT :允许用户登录的平台*/
		public static final String  STD_ALLOW_PLAT = "STD_ALLOW_PLAT";
		
		/** @Field STD_SY_DATA_ACCESS :数据查询权限*/
		public static final String  STD_SY_DATA_ACCESS = "STD_SY_DATA_ACCESS";

		/** @Field STD_SY_DATA_ACCESS :如皋业务品种*/
		public static final String  STD_PRO_RG = "STD_PRO_RG";

		/** @Field RG_PRODUCT_LIST :如皋授信申请产品 种类*/
		public static final String  RG_PRODUCT_LIST = "RG_PRODUCT_LIST";

		/** @Field RG_ESTATE_TYPE :如皋房地产抵押类型*/
		public static final String  RG_ESTATE_TYPE = "RG_ESTATE_TYPE";
		

		/** @Field STD_SY_DATA_ACCESS :贷款审查类别*/
		public static final String  EXAMINE_TYP = "EXAMINE_TYP";
		/** @Field STD_SY_DATA_ACCESS :对接人拜访方式*/
		public static final String RG_VISIT_TYPE = "RG_VISIT_TYPE";

		/** @Field STD_SY_DATA_ACCESS :如皋本行评级分类*/
		public static final String RG_GRADE_LEVEL = "RG_GRADE_LEVEL";

		/** @Field STD_SY_DATA_ACCESS :如皋街道乡镇*/
		public static final String RG_STREET_TOWNSHIP = "RG_STREET_TOWNSHIP";
		
		/** @Field STD_SY_DATA_ACCESS :如皋内控名单进入理由*/
		public static final String RG_IN_LIST_REASON = "IN_LIST_REASON";

		/** @Field RG_PRE_ITEM :如皋预设分析类别*/
		public static final String RG_PRE_ITEM = "RG_PRE_ITEM";

		/** @Field RG_PRE_ITEM :如皋预设分析类别--分析结果等级 1-5 由好到差*/
		public static final String RG_PRE_ITEM_LEVEL = "RG_PRE_ITEM_LEVEL";
		

		

	}

	public interface LoanTypeConstant {
		
		/**
		 * 通用授信调查
		 */
		public static final String LOAN_GENERAL =  "00";
		
		
		
		/**
		 * 流动资金贷款（通用制造业）
		 */
		public static final String LOAN_LIQUIDITY_MANUFACT =  "01";
		
		/**
		 * 流动资金贷款（纺织业）
		 */
		public static final String LOAN_LIQUIDITY_TEXTILE =  "02";
		
		/**
		 * 流动资金贷款（农副产品加工业）
		 */
		public static final String LOAN_LIQUIDITY_AFP =  "03";
		
		/**
		 * 流动资金贷款（批发零售业）
		 */
		public static final String LOAN_LIQUIDITY_WRT =  "04";
		
		/**
		 * 流动资金贷款（建筑业）
		 */
		public static final String LOAN_LIQUIDITY_CONST=  "05";
		
		/**
		 * 固定资产贷款
		 */
		public static final String LOAN_FIXED_ASSETS =  "06";
		
		/**
		 * 房地产开发贷款
		 */
		public static final String LOAN_ESTATE =  "07";
		
		/**
		 * 小微企业主经营性贷款
		 */
		public static final String LOAN_PERSONAL =  "08";
		
	}
	
	public interface AbnormalSystemType {
		
		public static final String TAX_TYPE=  "0";
		
		public static final String CREDIT_TYPE=  "1";
	}
	
	/**
	 * @Description 担保方式
	 * @author rain
	 */
	public interface GuaraWay {
		
		/**
		 * 抵押
		 */
		public static final String DY=  "01";
		
		/**
		 * 质押
		 */
		public static final String ZY=  "02";
		
		/**
		 * 保证
		 */
		public static final String BZ=  "03";
		
		/**
		 * 信用
		 */
		public static final String XY=  "04";
		
		/**
		 * 其他
		 */
		public static final String QT=  "05";
	}
	
	/**
	 * @Description 保证人类型
	 * @author rain
	 */
	public interface GuarantorType {
		/**
		 * 一般公司
		 */
		public static final String COMP_GUARA=  "10";
		/**
		 * 专业担保公司
		 */
		public static final String PROF_GUARA=  "20";
		/**
		 * 自然人
		 */
		public static final String PERSON_GUARA=  "30";
		
	}
	
	
	public interface CusNoType {
		
		/**
		 * 对公客户
		 */
		public static final String COMP_CUS=  "C";
		/**
		 * 个人客户
		 */
		public static final String PERSON_CUS=  "S";
		/**
		 * 未知客户（客户不存在）
		 */
		public static final String UNKOWN_CUS=  "U";
		
	}
	
	/**
	 * 客户标识
	 */
	public interface CusFlag {
		
		/**
		 * 借款人
		 */
		public static final String BORROWER=  "10";
		
		/**
		 * 子公司
		 */
		public static final String SUB_COM=  "20";
		
		/**
		 * 一般企业保证人
		 */
		public static final String GUARA_COM=  "30";
		
	}
	
	/**
	 * 客户业务状态
	 */
	public interface CusBusiStatus {
		
		/**
		 * 存量
		 */
		public static final String STOCK = "01";
		
		/**
		 * 新增
		 */
		public static final String NEW = "02";
	}
	
	public interface  ReportType{
		
		public static final String BALANCE=  "balance";
		
		public static final String NEW_BALANCE=  "newBalance";
		
		public static final String INCOME=  "income";
		
		public static final String NEW_INCOME=  "newIncome";
		
		public static final String CASH_FLOW=  "cashFlow";
		
		public static final String NEW_CASH_FLOW=  "newCashFlow";
		
		
		
		public static final String PARENT_BALANCE=  "parentBalance";
		
		public static final String NEW_PARENT_BALANCE=  "newParentBalance";
		
		public static final String PARENT_INCOME=  "parentIncome";
		
		public static final String NEW_PARENT_INCOME=  "newParentIncome";
		
		public static final String PARENT_CASH_FLOW=  "parentCashFlow";
		
		public static final String NEW_PARENT_CASH_FLOW=  "newParentCashFlow";
		
		
		
		public static final String TAX_BALANCE=  "taxBalance";
		
		public static final String NEW_TAX_BALANCE=  "newTaxBalance";
		
		public static final String TAX_INCOME=  "taxIncome";
		
		public static final String NEW_TAX_INCOME=  "newTaxIncome";
		
		public static final String TAX_CASH_FLOW=  "taxCashFlow";
		
		public static final String NEW_TAX_CASH_FLOW=  "newTaxCashFlow";
		
	}
	
	/**
	 * 任务状态
	 * @author beawan_fengjj
	 *
	 */
	public interface TaskState {
		
		/**
		 * 待处理的
		 */
		public static final int TO_DEAL_WITH =  0;
		/**
		 * 已提交
		 */
		public static final int SUBMITTED =  1;
	}
	
	/**
	 * 信贷系统财务报表类型，资产负债表、利润表、现金流量表、财务比率指标表
	 * @author rain
	 */
	public interface CmisFianRepType {
		
		/**
		 * 资产负债表
		 */
		public static final String BALANCE =  "balance";
		
		/**
		 * 利润表
		 */
		public static final String INCOME =  "income";
		
		/**
		 * 现金流量表（人工录入）
		 */
		public static final String CASH_FLOW_INPUT =  "cashFlowInput";
		
		/**
		 * 现金流量表（自动生成）
		 */
		public static final String CASH_FLOW =  "cashFlow";
		
		/**
		 * 财务比率指标表
		 */
		public static final String RATIO_INDEX =  "ratioIndex";
	}
	
	/**
	 * 信贷系统财务报表模型类别
	 * @author rain
	 */
	public interface CmisFianRepModClass {
		
		/**
		 * 一般企业法人财务报表(新会计准则口径)
		 */
		public static final String NEW_ACC =  "010";
		
		/**
		 * 一般企业法人财务报表(旧会计准则口径)
		 */
		public static final String OLD_ACC =  "020";
		
		/**
		 * 其他事业单位财务报表
		 */
		public static final String INSTITUTION =  "030";
		
		/**
		 * 教育行业财务报表
		 */
		public static final String EDUCATION =  "040";
		
		/**
		 * 医疗卫生行业财务报表
		 */
		public static final String MEDICAL =  "050";
	}
	
	/**
	 * 客户财报记录状态
	 * @author rain
	 */
	public interface CusFSRecordStatus {
		
		/**
		 * 新增
		 */
		public static final String NEW =  "01";
		
		/**
		 * 完成
		 */
		public static final String FINISHED =  "02";
		
		/**
		 * 锁定
		 */
		public static final String LOCKED =  "03";
	}
	
	/**
	 * 系统分析要求对财报完整性验证标识
	 * @author rain
	 */
	public interface CusFinaValidFlag {
		
		/**
		 * 财务报表无法满足系统分析要求
		 */
		public static final String UNAVAIL =  "0";
		
		/**
		 * 财务报表完全满足系统分析要求，并已保存为系统所需结构数据
		 */
		public static final String AVAIL =  "1";
		
		/**
		 * 财务报表暂时不能满足系统分析要求
		 */
		public static final String TEMP_UNAVAIL =  "2";
	}
	
	/**
	 * 系统财务分析组成部分
	 */
	public interface SysFinaAnalyPart {
		
		/**
		 * 全部分析结果
		 */
		public static final String ALL =  "ALL";
		
		/**
		 * 利润分析
		 */
		public static final String INCOME =  "INCOME";
		
		/**
		 * 资产质量分析
		 */
		public static final String ASSETS =  "ASSETS";
		
		/**
		 * 现金流量分析
		 */
		public static final String CASH_FLOW =  "CASH_FLOW";
		
		/**
		 * 财务比率指标分析
		 */
		public static final String RATIO_INDEX =  "RATIO_INDEX";
		
		/**
		 * 最终分析结论
		 */
		public static final String CONCLUSION =  "CONCLUSION";
	}
	

	public interface FinanceAbility {
		public static final String YLNLZK_SCOPE = "盈利能力状况";
		public static final String ZCZLZK_SCOPE = "资产质量状况";
		public static final String ZWFXZK_SCOPE = "债务风险状况";
		public static final String JYZZZK_SCOPE = "经营增长状况";
		public static final String BCZL_SCOPE = "补充资料";
	}
	
	public interface IndustryScope {
		public static final String ALL_INDUSTRY = "全行业";
		public static final String LARGE_INDUSTRY = "大型行业";
		public static final String MIDDLE_INDUSTRY = "中型行业";
		public static final String SMALL_INDUSTRY = "小型行业";
	}
	
	public interface FinanceQuota {
		public static final String ZZCSY_RATE = "净资产收益率（％）";
		public static final String ZZCBC_RATE = "总资产报酬率（％）";
		public static final String ZYYWLR_RATE = "主营业务利润率（％）";
		public static final String YYXJBZ_COUNT = "盈余现金保障倍数";
		public static final String CBFYLR_RATE = "成本费用利润率（％）";
		public static final String ZBSY_RATE = "资本收益率（％）";
		public static final String YYLR_RATE = "营业利润率（％）";
		public static final String ML_RATE = "毛利率（％）";
		
		public static final String ZZZZZ_RATE = "总资产周转率（次）";
		public static final String YSZKZZ_RATE = "应收账款周转率（次）";
		public static final String BLZC_RATE = "不良资产比重（％）";
		public static final String LDZCZZ_RATE = "流动资产周转率（次）";
		public static final String ZCXJHS_RATE = "资产现金回收率（％）";
		
		public static final String ZCFZ_RATE = "资产负债率（％）";
		public static final String YHLX_COUNT = "已获利息倍数";
		public static final String SD_RATE = "速动比率（％）";
		public static final String XJLDFZ_RATE = "现金流动负债比率（％）";
		public static final String DXFZ_RATE = "带息负债比率（％）";
		public static final String HYFZ_RATE = "或有负债比率（％）";
		
		public static final String XSZZ_RATE = "销售（营业）增长率（％）";
		public static final String ZBBZZZ_RATE = "资本保值增长率（％）";
		public static final String XSLRZZ_RATE = "销售（营业）利润增长率（％）";
		public static final String ZZCZZ_RATE = "总资产增长率（％）";
		public static final String JSTR_RATE = "技术投入比率（％）";
		
		public static final String CHZZ_COUNT = "存货周转率（次）";
		public static final String XJZLDZC_RATE = "现金占流动资产比重（％）";
		public static final String CBFYZSR_RATE = "成本费用占主营业务收入比重（％）";
		public static final String JJZJZ_RATE = "经济增加值率（％）";
		public static final String EBITDA_RATE = "EBITDA率（％）";
		public static final String ZBJL_RATE = "资本积累率（％）";
	}
	
	public interface BsDicRateConstant {
		
		/**
		 * 用款方式
		 */
		public static final String STD_RATE_BASE_EDIT =  "STD_RATE_BASE_EDIT";
		
		
		/**
		 * 授信模式
		 */
		public static final String STD_RATE_MODEL_EDIT =  "STD_RATE_MODEL_EDIT";
		
		/**
		 * 抵押
		 */
		public static final String STD_RATE_MOR =  "STD_RATE_MOR";
		
		/**
		 * 质押
		 */
		public static final String STD_RATE_POL =  "STD_RATE_POL";
		
		/**
		 * 保证
		 */
		public static final String STD_RATE_GUR =  "STD_RATE_GUR";
	}
	

	public interface STD_ZX_CUR_TYPE {
		/**
		 * 保证
		 */
		public static final String STD_ZX_CUR_TYPE =  "STD_ZX_CUR_TYPE";
		
	}
	 
	/**
	 * 预设项列表
	 * @author yuzhejia
	 */
	public interface RG_PRE_ITEM {
		/** 法定代表人  */
		public static final String LEGALER =  "01";
		/** 实际控制人  */
		public static final String CONTROLLER =  "02";
		/** 管理情况  */
		public static final String MANAGE_INFO =  "03";
		/** 组织结构  */
		public static final String COMBINATION =  "04";
		/** 融资情况分析  */
		public static final String FINANCE_INFO =  "05";
		/** 担保情况分析 */
		public static final String GUARANTEE_INFO =  "06";
		/** 产品情况分析  */
		public static final String PRODUCT_INFO =  "07";
		/** 原材料情况分析  */
		public static final String MATERIAL_INFO =  "08";
		/** 一般员工情况  */
		public static final String STAFF_INFO =  "09";
		/** 生产情况分析  */
		public static final String PRODUCTING_INFO =  "10";
		/** 一环保达标情况  */
		public static final String ENVIRON_INFO =  "11";
		/** 资质情况分析  */
		public static final String ZIZHI_INFO =  "12";
		/** 供应情况分析  */
		public static final String SUPPLY_INFO =  "13";
		/** 销售情况分析  */
		public static final String SELL_INFO =  "14";
		/** 销售情况分析  */
		public static final String RUN_AREA_INFO =  "15";
	}
}
