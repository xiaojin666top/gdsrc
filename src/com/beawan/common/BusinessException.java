package com.beawan.common;

/**
 * 业务逻辑异常，Controller只能向前台抛出业务异常 其他异常记录日志
 */
public class BusinessException extends Exception {
	
	private static final long serialVersionUID = 4457113998128457652L;
	
	private String flag;

	public BusinessException(String message) {
		super(message);
	}
	
	public BusinessException(String message, String flag) {
		super(message);
		this.flag = flag;
	}

	public BusinessException(Throwable e) {
		super(e);
	}

	public String getFlag() {
		return this.flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}
	
}