package com.beawan.common.aopLog;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @Author: xyh
 * @Date: 2020年10月16日
 * @Description:
 * Target注解：用于指定这个注解是使用于说明场景
 *           ElementType是使用场景的一个枚举（METHOD：方法）
 * 
 * Retention注解：用于指定注解保留多少时间
 *             RetentionPolicy描述保留注解的各种策略
 *             SOURCE:注解只在源代码级别保留，编译时被忽略
 *             CLASS:被编译器在类文件中记录，运行时不需要保留jvm，默认级别
 *             RUNTIME：注解将被编译器记录在类文件中，在运行是保留jvm，
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface LoanAopLog {
	/**
	 * 
	 *@Description 操作类型
	 * @author xyh
	 */
	public String operationType() default "";
	/**
	 * 
	 *@Description 操作名称
	 * @author xyh
	 */
	public String operationName() default "";
}
