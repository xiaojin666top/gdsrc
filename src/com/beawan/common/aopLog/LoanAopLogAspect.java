package com.beawan.common.aopLog;

import java.lang.reflect.Method;

import javax.annotation.Resource;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import com.beawan.base.entity.User;
import com.beawan.base.service.IPlatformLogSV;
import com.platform.util.HttpUtil;

/**
 * 
 * @Author: xyh
 * @Date: 2020年10月16日
 * @Description: 记录贷款流程的切面
 */
@Aspect
@Component
public class LoanAopLogAspect {
	@Resource
	private IPlatformLogSV platformLogSV;
	
	@Pointcut("@annotation(com.beawan.common.aopLog.LoanAopLog)")
	public void logPointcut(){
		//这个方法没有方法体
		//annotation表示用于匹配持有指定注解的方法
	}
	
	@Before("logPointcut()")
	public void logAround(JoinPoint joinPoint) throws Throwable{
		MethodSignature signature= (MethodSignature)joinPoint.getSignature();
		Method method = signature.getMethod();
		LoanAopLog loanAopLog = method.getAnnotation(LoanAopLog.class);
		String operationType = loanAopLog.operationType();
		String operationName = loanAopLog.operationName();
		//会空指针
		String oprateUserName = "";
		User currentUser = HttpUtil.getCurrentUser();
		if(currentUser!=null){
			oprateUserName=currentUser.getUserId();
		}else{
			//从参数中获取userId
			oprateUserName=HttpUtil.getAsString("userId");
		}
		platformLogSV.saveOperateLog(operationName,
				operationType, oprateUserName);
	}
}
