package com.beawan.common.service;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.Date;

/**
 * 公共服务接口
 *
 */
public interface ICommonSV
{
	/**
	 * 获取指定数据源数据库日期
	 * @param datasource
	 * @return
	 * @throws Exception
	 */
	public Date getSysDate(String datasource)throws Exception;
	
	/**
	 * 获取指定数据源数据库时间
	 * @param datasource
	 * @return
	 * @throws Exception
	 */
	public Timestamp getSysTime(String datasource) throws Exception;	
	/**
	 * 获取序列值
	 * @param squenceName 序列名
	 * @param datasource
	 * @return
	 * @throws Exception
	 */
	public String getSquenceVal(String squenceName,String datasource)throws Exception;
}
