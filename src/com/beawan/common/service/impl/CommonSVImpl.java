package com.beawan.common.service.impl;

import java.sql.Timestamp;
import java.util.Date;





import org.springframework.stereotype.Service;

import com.beawan.common.service.ICommonSV;
import com.platform.util.JdbcUtil;

@Service("commonSV")
public class CommonSVImpl implements ICommonSV {

	@Override
	public Date getSysDate(String datasource) throws Exception {
		/* oracle SELECT SYSDATE FROM DUAL */
		/* db2 SELECT CURRENT TIMESTAMP FROM SYSIBM.SYSDUMMY1 */
		return JdbcUtil.queryForTimestamp(datasource,
				"SELECT CURRENT TIMESTAMP FROM SYSIBM.SYSDUMMY1;", null);
	}

	@Override
	public Timestamp getSysTime(String datasource) throws Exception {
		return JdbcUtil.queryForTimestamp(datasource,
				/*"SELECT CURRENT TIMESTAMP FROM SYSIBM.SYSDUMMY1"*/
				"select current_date as Systemtime", null);
	}

	@Override
	public String getSquenceVal(String squenceName, String datasource)
			throws Exception {
		//oracle:
		//String sql = "SELECT GDTCESYS." + squenceName + ".NEXTVAL FROM DUAL";
		//db2:
		String sql = "SELECT next value for GDTCESYS."+squenceName+ " from Sysibm.sysdummy1";
		return JdbcUtil.queryForString(datasource, sql, null);
	}

}
