package com.beawan.common;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class MyTest implements ApplicationContextAware {

	

	@Autowired
      private EntityManagerFactory entityManagerFactory;
	
	@PersistenceContext(name = "entityManagerFactory")
	protected EntityManager entityManager;
	
	@Test
	public void test1(){
		EntityManager entityManager1 = entityManagerFactory.createEntityManager();
		entityManager1.getTransaction().begin();;
       Query query= entityManager1.createQuery("delete from UserLoginLog where platform='MB' ");       
       query.executeUpdate();
        System.out.println("rows");
    }

	
	 ApplicationContext applicationContext;
	
	
	
	
	@Override
	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		applicationContext=arg0;
		
	}
	
}
