package com.beawan.common;
/**
 * 系统常量
 *
 */
public class Constants {
	
	public static final String DATE_MASK = "yyyy-MM-dd";
	
	public static final String DATE_MASK2 = "yyyyMMdd";
	
	public static final String DATE_MASK_YEAR = "yyyy";

	public static final String DATETIME_MASK = "yyyy-MM-dd HH:mm:ss";

	public static final String TIMESTAMP_MASK = "yyyyMMddHHmmss"; //时间戳

	public static final String INTERFACE_PROPS_PATH = "/properties/interface.properties";
	
    public static final String SYSTEM_PROPS_PATH = "/properties/system.properties";
    
    public static final String SYSTEM_LOGS_PATH = "/log4j.properties";

	public static final String CFG_PROPS_PATH = "/cfg.properties";

    public static final int ZOOM_IMAGE_WIDTH = 220;
	
	public static final int ZOOM_IMAGE_HEIGHT = 165;
	
	public static final String SYS_SHORT_NAME = "DGZJ";//系统简称

	public static final String STATE_NORMAL = "0";//正常

	public static final String STATE_DELETE = "1";//已删除
	
	public static final Integer FALSE = 0; //否
	
	public static final Integer TRUE = 1; //是
	
	/**
	 * 现使用--- 正常
	 */
	public static final int NORMAL = 0;
	/**
	 * 现使用----删除
	 */
	public static final int DELETE= 1;

	/**可编辑状态 ***/
	public static final String EDIT = "0";
	/**不可编辑状态 ***/
	public static final String VIEW = "1";

	
	
	/** Session Key定义 **/
	public interface SessionKey {

		public static final String SESSION_KEY_USER = "SESSION_KEY_USER";

		public static final String SESSION_KEY_USER_REG = "SESSION_KEY_USER_REG";

		public static final String SESSION_KEY_USER_ADVICE_CODE = "SESSION_KEY_USER_ADVICE_CODE";

	}
	
	/** Session Key定义 **/
	public interface ApplicationKey {

		public static final String APPLICATION_KEY_LOGINUSER = "APPLICATION_KEY_LOGINUSER";

	}
	

	/**数据源名称**/
	public interface DataSource {
		
		public static final String TCE_DS = "tce";
		
		public static final String CPDJ_DS = "cpdj";
		
//		public static final String CMIS_DS = "cmis";
		
		//public static final String QPCS_DS = "qpcs";
	}
	
	/** 数据库序列名称* */
	public interface DbSequenceName {
		
		public static final String SERIAL_NO = "SERIAL_NO_SEQ";//业务流水号序列
		
		public static final String MORTGAGE_NO = "MORTGAGE_NO_SEQ";//押品编号序列
		
		public static final String TEMP_CUS_NO = "TEMP_CUS_NO_SEQ";//临时客户号序列
		
		public static final String CUS_FS_NO = "CUS_FS_NO_SEQ";//客户财报记录编号序列
		
		public static final String REPORT_NO = "REPORT_NO_SEQ";//报表编号序列
	}
	/**
	 *
	 * @ClassName: LoanProcessType
	 * @Description: 贷款管理主表--贷款流程--跨度100
	 *               贷款异常结束---被拒绝，否决贷款，使用负数表示
	 *               例：-199营销结束，-299贷前拒绝，-399合规否决
	 *               999为贷款正常结束，完成贷款流程，放款了
	 * @author xyh
	 * @date 16 Jun 2020
	 *
	 */
	public interface LoanProcessType{
		/**
		 *  营销中---初始状态--------这个状态被隐式地放入到了客户的拜访信息中
		 */
		public static final int RETAIL_ING = 101;
		/**
		 * 营销进件中----
		 */
		public static final int RETAIL_IN_ING = 102;
		/**
		 * 营销结束---没有营销进件的营销
		 */
		public static final int RETAIl_STOP = -199;


		/** 201_营销提交过来的任务（待分配后客户经理） **/
		public static final int SURVEY_PENDING = 201;

		/** 202 待确认调查 (客户经理待确认调查的状态)**/
		public static final int SURVEY_RECEIVE = 202;

		/** 203贷前调查中 (客户经理确认后 调查中的状态)**/
		public static final int SURVEY_ING = 203;

		/** 213 合规审查打回 **/
		public static final int SURVEY_REPLUSE_LC = 213;

		/** 223 信贷审查审批打回 **/
		public static final int SURVEY_REPLUSE_LE = 223;

		/** 204贷前已提交 (时间上主流程中没有这个状态，用来查询已提交列表)**/
		public static final int SURVEY_SUBMIT = 204;


		/** 301_待处理合规审查 **/
		public static final int COMPLIANCE_PENDING = 301;
		/** 302_已提交合规审查  **/
		public static final int COMPLIANCE_FINISH = 302;
		
		/**
		 * 审查分配
		 */
		public static final int EXAMINE_DISTRIBUTION = 400;
		/**
		 * 待处理审查
		 */
		public static final int EXAMINE_PENDING = 401;
		/**
		 * 审查通过
		 */
		public static final int EXAMINE_PASS = 402;

		
	}


	public interface LoanImageType {
		/**
		 * 100营销影像
		 */
		public static final String RETAIL = "RETAIL";
		/**
		 * 200贷前影像
		 */
		public static final String SURVEY = "SURVEY";
		/**
		 * 300审批影像
		 */
		public static final String APPROVAL = "APPROVAL";
		/**
		 * 400贷后影像
		 */
		public static final String AFTER = "AFTER";
	}

	/**
	 * 营销记录表操作类型
	 * @ClassName: LrRecordType
	 * @author xyh
	 * @date 18 Jun 2020
	 *
	 */
	public interface LrRecordType{
		/**
		 * 被机器分配到营销客户
		 */
		public static final int MARKET_CUST_DISTRBUTION_M = 20;
		/**
		 * 被人手工分配到任务
		 */
		public static final int MARKET_CUST_DISTRBUTION_P = 21;
		/**
		 * 主动领取任务
		 */
		public static final int MARKET_CUST_DISTRBUTION_S = 22;

		/**
		 * 创建营销任务成功
		 */
		public static final int CREATE_MARKET_SUCCESS = 100;
		/**
		 * 营销进件
		 */
		public static final int MARKET_IN = 200;
		/**
		 * 营销进件完成-提交到贷前调查
		 */
		public static final int	MARKET_IN_SUCCESS = 300;
		/**
		 * 营销客户被人工回收
		 */
		public static final int MARKET_CUST_RECOVERY_P = 921;
		/**
		 * 营销客户被机器回收
		 */
		public static final int MARKET_CUST_RECOVERY_M = 920;
		/**
		 * 营销未进件
		 */
		public static final int MARKET_FINISH = 999;
	}
	/**
	 *  营销结果--这个结果为了使得营销模块统计，内置
	 * @ClassName: RetailResult
	 * @author xyh
	 * @date 18 Jun 2020
	 *
	 */
	public interface RetailResult {
		/**
		 * 营销成功---点击了营销进件算成功
		 */
		public static final String RETAIL_SUCCESS = "0";
		/**
		 * 营销失败----点击了营销结束算失败
		 */
		public static final String RETAIL_FAIL = "1";
	}
	
	
	
	
	
	/** 平台类型 **/
	public interface Platform {
		
		public static final String ALL = "ALL";
		
		public static final String PC = "PC";
		
		public static final String MOBILE = "MB";
		

		//本地财务分析平台
		public static final int LOCAL_FINRISK = 1;
		//云端财务分析平台
		public static final int CLOUD_FINRISK = 2;
		//云端行业分析
		public static final int CLOUD_INDU = 3;
	}
	
	/** 主要附注科目 **/
	public interface Anno {
		/** 货币资金 **/
		public static final int MONETARY_FUND = 1;
		/** 应收账款 **/
		public static final int ACCOUNT_RECE = 2;
		/** 存货 **/
		public static final int STOCK = 3;
		/** 应付账款 **/
		public static final int ACCOUNT_PAY = 4;
		/** 固定资产 **/
		public static final int FIXED_ASSET = 5;
		/** 其他应收款 **/
		public static final int OTHER_RECE = 6;
		/** 长期投资 **/
		public static final int LONG_INVEST = 7;
		/** 在建工程 **/
		public static final int CONSTRU_PROGRESS = 8;
		/** 无形资产 **/
		public static final int INTANG_ASSET = 9;
		/** 短期借款**/
		public static final int SHORT_LOAN = 10;
		/** 实收资本 **/
		public static final int PAID_CAPITAL = 11;
		/** 其他应付款 **/
		public static final int OTHER_PAY = 12;
		/** 预付账款 **/
		public static final int PRE_PAY = 13;
		/** 预收账款 **/
		public static final int PRE_RECE = 14;
	}
	
	/** 机构状态 **/
	public interface ORG_STATE {

		public static final int NORMAL = 0;//正常
		public static final int DELETE = 1;//被删除
		public static final int REVOKE = 2;//被撤销
		public static final int MERGED = 3;//被合并
		public static final int NEW_MERGE = 4;//合并后
	}
	/**
	 *父级机构
	 */
	public interface ORG_PARENT{
		//最高父亲机构
		public static final String PARENTNO = "000000000";
	}
	
	/**
	 * 注入模型
	 * @author yuzhejia
	 *
	 */
	public interface Model{
		/** 模型状态 1启用 2：停用 */
		public static final int STATUS_USE = 1;
		public static final int STATUS_DIS = 2;

		/*type;//类型 1：系统刚空 2：风险提醒*/		
		public static final int TYPE_GK = 1;
		public static final int TYPE_FX = 2;

		public static final int FALG_FALSE = 0;
		public static final int FALG_TRUE = 1;
		
		public static final int DEEP_FIRST = 1;
		public static final int DEEP_SECOND = 2;
		public static final int DEEP_THIRD = 3;
	}

	/**
	 * 准入模型  与页面、数据库相对应
	 */
	public interface MD_ADMIT_QUOTA{
		String MANAGER_CHANGE = "AD_001";//高管变化率
		String BUILD_TIME = "AD_002";//成立时间
		String EXCEPTION = "AD_003";//经营异常
		String COMP_SX = "AD_004";//企业失信
		String LEGAL_SX = "AD_005";//企业法人失信
	}

	/**
	 * 利率模型  具体指标
	 */
	public interface MD_RATE_QUOTA{
		String CUS_GRADE = "客户评级";//客户评级
		String GUARWAY = "担保方式";//担保方式
		String CREDIT_RECORD = "信用记录";//信用记录
		String REVOLV_LIMIT = "循环额度";//是否循环额度
		String CREDIT_AMOUNT = "授信金额(万元)";//授信金额
		String CUS_LOYAL = "客户忠诚";//客户忠诚度
	}
	
	public interface Risk{
		//风险预警种类
		public static final int IC = 1;//工商
		public static final int JUDICIAL = 2;//司法
		public static final int REFERENCE = 3;//征信
		public static final int WATER_ELECT = 4;
		public static final int TAX = 5;//税务
		public static final int RUNNING_WATER = 6;//流水

		//风险预警数据来源
		//风险适用范围：1.大数据 2.征信报告 3.客户经理收集
		public static final int CLOUD_DATA = 1;
		public static final int CREDIT_REPORT = 2;
		public static final int SURVEY_COLLECT = 3;
		
		//风险预警等级 由低到高  1 黄色预警  2 橙色预警  3 红色预警  4 不予准入
		public static final String LEVEL_1 = "1";//1级
		public static final String LEVEL_2 = "2";//2级
		public static final String LEVEL_3 = "3";//3级
		public static final String LEVEL_4 = "4";//4级
		
		//状态 状态：1 激活 2未激活（默认）  3删除
		public static final int ACTIVE = 1;
		public static final int NOT_ACTIVE = 2;
		public static final int DELETTED = 3;
		
		
	}


	/******************************角色编号的常量***************************************/
	public interface ROLE {
		/**
		 * 超级管理员
		 */
		public static final String MAX_MANAGER = "0008";

		/**
		 * 营销客户经理
		 */
		public static final String LR_USER = "0010";
		/**
		 * 营销主管
		 */
		public static final String LR_MANAGER = "1001";
		/**
		 * 公司部主管
		 */
		public static final String LR_LB_MANAGER = "0011";

		/**
		 * 贷前调查客户经理
		 */
		public static final String LB_USER = "0001";

		/**
		 * 合规部经理
		 */
		public static final String LC_USER = "3002";
		/**
		 * 合规部主管
		 */
		public static final String LC_MANAGER = "3001";
		/**
		 * 信贷审查主管---用于分配审查任务
		 */
		public static final String LE_MANAGER = "4001";
		/**
		 * 信贷审查经理
		 */
		public static final String LE_USER = "4002";
		

		/**
		 * 贷后专职风险经理
		 */
		public static final String LF_RISK_USER = "5001";
		/**
		 * 贷后管理员	支持对贷后配置进行管理
		 */
		public static final String LF_CONFIG_MANAGER = "5002";
	}



	/******************************营销的常量******************************************/
	/**是否营销类型客户 0是  1否  **/
	public interface RetailCust {
		public static final int TRUE = 0;
		public static final int FALSE = 1;
	}

	//营销管户关系是否稳定 营销成功就永远是当前客户经理管户。若失败会过期。0营销成功 1还未营销成功
	public interface RetailReal{
		public static final int SUCCESS = 0;
		public static final int NOT_SUCC = 1;
	}

	//导入方式，未营销成功期限不同  1后台管理员批量导入  默认3个月   2客户经理自己手动导入 1一个月
	public interface ImportWay{
		public static final int BATCH = 1;
		public static final int MANUAL = 2;
	}

	//营销拜访方式，0微信  1电话  2实地
	public interface RetailWay{
		public static final int WECHAT = 0;
		public static final int PHONE = 1;
		public static final int INDEED = 2;
	}

	//客户分类结果   0无效客户  1潜在客户 2机会客户 3意向客户 4成功客户
	public interface CustClassify{
		/**
		 * 无效客户
		 */
		public static final int INVALID = 0;
		/**
		 * 潜在客户
		 */
		public static final int LEADS = 1;
		/**
		 * 机会客户
		 */
		public static final int OPPORTUNITY = 2;
		/**
		 * 意向客户
		 */
		public static final int INTENDED = 3;
		/**
		 * 成功客户
		 */
		public static final int SUCCESS = 4;
	}
	/**
	 * @ClassName: LrCustType
	 * @Description:营销客户，客户类型
	 * @author xyh
	 * @date 19 Jun 2020
	 *
	 */
	public interface LrCustType{
		/**
		 * 黑名单客户
		 */
		public static final String BLACK_CUST = "-1";
		/**
		 * 公共池客户
		 */
		public static final String PUB_POOL_CUST = "0";
		/**
		 * 营销客户
		 */
		public static final String RETAIL_CUST = "1";
	}
	
	public interface LR_PROCESS{
		public static final int TEMP = 0;
		public static final int FINISH_FAIL = 1;
		public static final int FINISH_SUCC = 2;
	}


	/********************************   合规审查的常量 ***************************************/

	/**
	 * 合规审查文档类型
	 */
	public interface LC_DOC_TYPE {
		/**
		 * 职责与权限
		 */
		public static final String REMIT = "REMIT";
		/**
		 * 工作内容
		 */
		public static final String WORK = "WORK";
		/**
		 * 审查要点
		 */
		public static final String REVIEW_POINT = "REVIEW_POINT";
	}



	public interface LC_REVIEW_CONTENT_STATE {
		/** 0 已解决**/
		public static final int SOLVE = 0;
		/** 1 待解决**/
		public static final int NOT_SOLVE = 1;
		/** 2 部分整改 **/
		public static final int SOME_SOLVE = 2;


		/** 10 汇总统计 **/
		public static final String SUM_STAT = "10";
		/** 11 问题统计 **/
		public static final String RISK_STAT = "11";
		/** 12 客户经理问题统计 **/
		public static final String MANAGER_RISK_STAT = "12";
	}

	/**
	 * 合规审查 分行业风险点
	 */
	public interface LC_RISK_INDU {
		/** 公共部分 **/
		public static final String COMM = "COMM";
		/** 制造业 **/
		public static final String MANUF = "MANUF";
		/** 建筑业 **/
		public static final String BUILD = "BUILD";
		/** 贸易类 **/
		public static final String BUSINESS = "BUSINESS";
		/** 其他 **/
		public static final String OTHER = "OTHER";


	}

	/**
	 * 审查的常量
	 */
	public interface LE_REVIEW_CONTENT_STATE{
		/**
		 * 细项通过
		 */
		public static final int IS_PASS = 0;
		/**
		 * 细项不通过
		 */
		public static final int IS_UN_PASS = 1;
		/**
		 * 细项已经处理
		 */
		public static final int SOLVE = 0;
		/**
		 * 细项未处理
		 */
		public static final int NOT_SOLVE = 1;
	}


//	财务信息保存分析标识，0：财报数据完全不能满足系统分析要求；1：已保存并分析；2：暂时不能满足系统系统分析要求，待分析
//	public interface LB_FINA_SYNC_FLAG{
//		String CAN_NOT_ANALYSIS = "0";
//		String ANALYSISED = "1";
//		String PENDING = "2";
//	}

	/**
	 * 财报模板列数据类型
	 * ALL代表财报所有字段
	 * IMPORT代表其中的重要字段
	 */
	public interface FINA_MODEL_TYPE{
		String ALL_ROWS = "ALL";
		String IMPORT_ROWS = "IMPORT";
	}

	/**
	 * 财务比率指标类型
	 */
	public interface FINA_RATIO_TYPE{
		String PROFIT = "profit";
		String OPERATE = "operate";
		String DEBT = "debt";
		String DEVELOP = "develop";
	}

	/**
	 * 财务科目 rowNo对应值
	 */
	public interface FINA_OBJECT_ROW_NO{
		String CASH = "101";//货币资金
		String RECEIVE = "107";//应收账款
		String OTHER_RECEIVE = "115";//其他应收款
		String INVENTORY = "117";//存货
		String FIX_NEW = "137";//固定资产  新会计准则
		String FIX_OLD = "146";//固定资产  旧会计准则

		String PAY = "206";//应付账款
		String OTHER_PAY = "218";//其他应付款

		String PRE_PAY = "109";//预付款项

		String PRE_RECE = "208";//预收账款


		String TOTAL_ASSET = "165";//资产总计

	}

	/********************************   贷前调查的常量 ***************************************/
	/**
	 * 贷前调查模板常量
	 */
	public interface LB_SURVEY_TYPE {
		/** 00 通用贷款类型 **/
		public static final String COMMON = "00";
		/** 01 流动资金贷款（商贸类）**/
		public static final String COMMERCIAL_TRADE = "01";
		/** 02 流动资金贷款（项目类） **/
		public static final String PROJECT = "02";
		/** 03 流动资金贷款（纺织业） **/
		public static final String TEXTILE = "03";
		/** 04 流动资金贷款（制造业） **/
		public static final String MANUFACTURE = "04";
		/** 05 流动资金贷款（建筑业） **/
		public static final String BUILDING = "05";
		/** 06 流动资金贷款（农副食品加工业） **/
		public static final String FOOD = "06";
		/** 07 流动资金贷款 (银行) **/
		public static final String BANK = "07";
	}

	/**
	 * 企业授信常量（建筑业）
	 */
	public interface CM_CREDIT{
		/** 01 借款人授信 **/
		public static final String BORROWER = "01";
		/** 02 关联方授信 **/
		public static final String RELATED_PARTY = "02";
	}

	/**
	 * 贷前 页面跳转常量
	 */
	public interface LB_PAGE_TYPE{
		/** 01 房地产抵押 **/
		public static final String REAL_ESTATE = "01";
		/** 02 动产抵押 **/
		public static final String CHATTEL = "02";
		/** 03 应收账款质押 **/
		public static final String ACCOUNTS_RECEIVABLE = "03";
		/** 04 存单质押 **/
		public static final String DEPOSIT_RECEIPT = "04";
		/** 05 第三方企业保证担保 **/

		/** 06 自然人保证担保 **/

		/** 11 主要产品情况 **/
		public static final String PRODUCT = "11";
		/** 12 能源消耗情况 **/
		public static final String ENERGY = "12";
		/** 13 生产设备情况 **/
		public static final String EQUIPMENT = "13";

		/** 21 资金投入情况 **/
		public static final String FUNDS_INVEST= "21";
		/** 22 资金使用明细 **/
		public static final String FUNDS_USE= "22";
		/** 23 设备购置情况 **/
		public static final String EQUIPMENT_BUY= "23";
		/** 24 工程费用明细 **/
		public static final String PROJECT_COST= "24";
		
		
		/** 31 借款人授信情况 **/
		public static final String BORROWER_CREDIT= "31";
		/** 32 关联方授信情况 **/
		public static final String LINKED_PARTY_CREDIT= "32";
		
		/** 41 担保企业基本信息 **/
		public static final String GUA_COMP_INFO= "41";
		/** 42 担保企业财务状况 **/
		public static final String GUA_COMP_FINANCE= "42";
		/** 43 担保企业资信状况 **/
		public static final String GUA_COMP_CREDIT= "43";
		
	}

	/*************************  贷后常量 ******************************/
    /**
     * 贷后任务类型
     */
	public interface LF_TASK_TYPE{
	    /**
	     * 初始化认定任务--不需要做了
	     */
	    String INIT_TASK = "01";
	    /**
	     * 管户日常贷后
	     */
	    String GH_DAY_TASK = "02";
	    /**
	     * 管户15日首检
	     */
	    String GH_FIFTEEN_DAY_TASK = "03";
	    /**
	     * 风险经理15日首检
	     */
	    String FX_FIFTEEN_DAY_TASK = "04";
	    /**
	     * 风险经理3月到期检查
	     */
	    String FX_THREE_MONTH_TASK = "05";
	    /**
	     * 风险经理--风险信号检查
	     */
	    String FX_YJ_TASK = "06";
	    
	    /**
	     * 手动创建任务
	     */
	    String CREATE_MANUALLY_TASK = "07";
	    /**
	     * 日常贷后任务清单----待处理任务
	     */
	    String DAY_TASK_LIST = "11";
	    /**
	     * 15日首检任务清单----待处理任务
	     */
	    String FIFTEEN_DAY_TASK_LIST = "12";
	    /**
	     * 3月到期检查贷后任务清单----待处理任务
	     */
	    String THREE_MONTH_TASK_LIST = "13";
	    /**
	     * 风险经理--风险信号检查任务清单----待处理任务
	     */
	    String FX_YJ_TASK_LIST = "14";
	    
	    /**
	     * 风险经理--手动创建  贷后任务检查任务清单----待处理任务
	     */
	    String CREATE_MANUALLY_LIST = "15";
	    
	    
	    /**
	     * 日常贷后任务清单----检查结束任务
	     */
	    String DAY_TASK_LIST_OVER = "21";
	    /**
	     * 15日首检任务清单----检查结束任务
	     */
	    String FIFTEEN_DAY_TASK_LIST_OVER = "22";
	    /**
	     * 3月到期检查贷后任务清单----检查结束任务
	     */
	    String THREE_MONTH_TASK_LIST_OVER = "23";
	    /**
	     * 风险经理--风险信号检查任务清单----检查结束任务
	     */
	    String FX_YJ_TASK_LIST_OVER = "24";
	    
	    /**
	     * 风险经理--手动创建  贷后任务检查任务清单----检查结束任务
	     */
	    String CREATE_MANUALLY_LIST_OVER = "25";
  
    }

	/**
	 * 贷后客户风险分类
	 */
	public interface LF_CUST_RISK_CLASSI{
		/**
		 * 正常类
		 */
		String NOMAL_TYPE = "01";
		/**
		 * 关注类
		 */
		String FOLLOW_TYPE = "02";
		/**
		 * 风险一级
		 */
		String FX_ONE_TYPE = "03";
		/**
		 * 风险二级
		 */
		String FX_TWO_TYPE = "04";
		/**
		 * 风险三级
		 */
		String FX_THREE_TYPE = "05";
	}

	/**
	 * 贷后客户授信分类
	 */
	public interface LF_CUST_CREDIT_CLASSI{
		/**
		 * 增
		 */
		String ADD_TYPE = "01";
		/**
		 * 持
		 */
		String KEEP_TYPE = "02";
		/**
		 * 减
		 */
		String REDUCE_TYPE = "03";
		/**
		 * 退
		 */
		String OUT_TYPE ="04";
	}
	//贷后字典项
	public interface LF_SYS_DIC{
		/**
		 * 15首检核查内容
		 */
		String FIFTEEN_DAY_CHECK = "RG_FIFTEEN_DAY_CHECK_T";
		/**
		 * 现场贷后客户变动检查
		 */
		String LF_CHANGE_CUST = "RG_LF_CHANGE_CUST_T";
		/**
		 * 现场贷后融资变动检查
		 */
		String LF_CHANGE_FINANCE = "RG_LF_CHANGE_FINANCE_T";
		/**
		 * 现场贷后对外担保变动检查
		 */
		String LF_CHANGE_GUAT = "RG_LF_CHANGE_GUAT_T";
		
		/**
		 * 现场贷后-财务状况检查细项
		 */
		String LF_FIN_CHECK = "RG_LF_FIN_CHECK_01";
		/**
		 * 现场贷后-财务状况检查细项--建筑业
		 */
		String LF_FIN_CHECK_JZ = "RG_LF_FIN_CHECK_02";
		/**
		 * 现场贷后-财务状况检查细项--批发零售业
		 */
		String LF_FIN_CHECK_LS = "RG_LF_FIN_CHECK_03";
		
	}


	/**
	 * 贷后检查阶段
	 */
	public interface LF_TASK_POSITION{
		/**
		 * 待检查阶段
		 */
		int CHECKING_TASK = 1;
		/**
		 * 检查结束任务
		 */
		int CHECKED_TASK = 99;
	}
	
	/**
	 * 贷后五级分类任务 阶段
	 */
	public interface LFC_TASK_POSITION{
		/**
		 * 待处理阶段
		 */
		int PENDING_TASK = 1;
		/**
		 * 检查结束任务
		 */
		int FINISHED_TASK = 99;
	}
	

	/**
	 * 贷后五级分类任务对应某个客户的子任务  所在 阶段
	 */
	public interface LFC_TASK_CUST_STATE {
		/**
		 * 为完成阶段
		 */
		int PENDING = 1;
		/**
		 * 检查结束任务
		 */
		int FINISHED = 2;
	}

	/**
	 * 存款类型
	 */
	public interface DEPOSIT_TYPE{
		/** 对公存款 */
		public static final String PUBLIC_DEPOSIT = "01";
		/** 储蓄存款 */
		public static final String SAVINGS_DEPOSIT = "02";
		/** 保证金存款 */
		public static final String MARGIN_DEPOSIT = "03";
	}
	

	/**
	 * 存款类型
	 */
	public interface FINA_MOLD{
		/** 比率指标 */
		public static final String RATIO = "01";
		/** 现金流指标 */
		public static final String CASHFLOW = "02";
	}
	
	/**
	 * 关联关系  --》工作底稿中需要法人信息和财务信息
	 */
	public interface RELATIONSHIP{
		/**法人**/
		public static final String LAGER = "0100";
		/**财务**/
		public static final String FINANCE = "0103";
	}

	
	/**
	 * 授信发生类型
	 */
	public interface OCCURTYPE {
		//首次授信
		public static final String FIRST = "090";
		//存量授信
		public static final String STOCK = "100";
		//增量授信
		public static final String INCRE = "110";
		//调整授信
		public static final String ADJUST = "120";
	}
	
	
	
	/**
	 * 贷后 客户风险分类点
	 */
	public interface LF_RISK_CLASSIFY {
		/** 正常类 **/
		public static final String NORMAL = "01";
		/** 关注类 **/
		public static final String ATTENTION = "02";
		/** 风险一类 **/
		public static final String RISK_ONE = "03";
		/** 风险二类 **/
		public static final String RISK_TWO = "04";
		/** 风险三类 **/
		public static final String RISK_THREE = "05";
		/** 增加授信 **/
		public static final String INCREASE_CREDIT = "06";
		/** 维持授信 **/
		public static final String KEEP_CREDIT = "07";
		/** 减少授信 **/
		public static final String REDUCE_CREDIT = "08";
		/** 退出授信 **/
		public static final String QUIT_CREDIT = "09";
		
		
	}
	

	/**
	 * 项目效益测算   固定列名
	 */

	public interface BENEFIT {
		public static final String ID = "id";
		public static final String ITEM = "item"; 
		public static final String[] KEYS = new String[]{"key1", "key2", "key3", "key4", "key5"
				, "key6", "key7", "key8", "key9", "key10"
				, "key11", "key12", "key13", "key14", "key15"
				, "key16", "key17", "key18", "key19", "key20"};
	}

}
