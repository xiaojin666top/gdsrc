package com.beawan.common.datasource;

public class CustomerContextHolder {
	public static final String DATA_MASTER = "dataSource";
	protected static final ThreadLocal<String> contextHolder = new ThreadLocal<String>();

	public static void setCustomerType(String customerType) {
		CustomerContextHolder.contextHolder.set(customerType);
	}

	public static String getCustomerType() {
		return CustomerContextHolder.contextHolder.get();
	}

	public static void clearCustomerType() {
		CustomerContextHolder.contextHolder.remove();
	}
}