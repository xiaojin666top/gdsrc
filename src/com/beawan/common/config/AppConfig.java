package com.beawan.common.config;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

public class AppConfig {
	public static class Cfg{
		static Properties properties = new Properties();
		/**
		 * 云端获取分析结果
		 */
		public static String CLOUD_FINRISK = "";
		/**
		 * 云端分析
		 */
		public static String CLOUD_ANALYSIS = "";
		public static String LOCAL_FINRISK = "";
		public static String LOCAL_ANALYSIS = "";


		/**
		 * 如皋本地财务分析接口
		 * 本地云端两个版本，在贷前一直都是用的本地版本，审查阶段用的云端版本，到时候根本过期时间和远程入口控制
		 */
		public static String LOCAL_FINRISK_ANALYSIS = "";
		public static String LOCAL_ANALYSIS_RESULT = "";
		public static String CLOUD_ANALYSIS_RESULT = "";
		public static String GET_QUOTA_INFO = "";

		
		/** 财务分析接口根地址 *****/
		public static String ROOT_FINRISK = "";
		/** 财务分析本地版本地址 *****/
		public static String V_LOCAL = "";
		/** 财务分析云端版本地址 *****/
		public static String V_CLOUD = "";
		
		/**
		 * 异常预警界面
		 * errSummary
		 */
		public static String ERR_SUMMARY = "";
		/**
		 * 资产分析界面
		 * errSummary
		 */
		public static String BALANCE_ASSETS = "";
		/**
		 * 负债分析界面
		 * errSummary
		 */
		public static String BALANCE_DEBT = "";
		/**
		 * 所有者权益分析界面
		 * errSummary
		 */
		public static String BALANCE_OWNER = "";
		/**
		 * 利润分析界面
		 * errSummary
		 */
		public static String INCOME_PANDECT = "";
		/**
		 * 现金流分析界面
		 * errSummary
		 */
		public static String CASH_FLOW_PANDECT = "";
		
		/**
		 * 还款来源  八层次现金流 分析界面
		 * errSummary
		 */
		public static String EIGHT_CASH_FLOW = "";
		
		/**
		 * 比率分析界面
		 * errSummary
		 */
		public static String RATIO = "";


		/**
		 * 影像资料 实际保存地址
		 */
		public static String IMAGE_PATH = "";
		/*** 营销资料映射必须加的前缀 **/
		public static String IMAGE_MAPPING = "";

//		营销文件 映射地址前缀
		public static String IMAGE_RETAIL_MAPPING = "";
		public static String IMAGE_SURVEY_MAPPING = "";
//		#台帐保存地址
		public static String ACCOUNT_PATH = "";
		
		
		//五级分类文件地址（三份报告总地址）
		public static String FIVE_CLASS_PATH = "";
		
		//五级分类工作底稿
		public static String FIVE_CLASS_WORKING_PAPER_PATH ="";
		
		//五级分类分类认定表
		public static String FIVE_CLASS_IDENTIFICATION_TABLE_PATH ="";
		
		//报告五级分类报告保存地址 
		public static String REPORT_PATH = "";

		//获得流动资金测算
		public static String WORK_CAPITAL_MEASURE= "";
		//企查查外网全报文接口
		public static String GET_QCC_FULL_URL = "";
		
		//企查查内网数据接口   qcc_api_url
		public static String QCC_API_URL = "";
		//企查查密钥    qcc_syskey
		public static String QCC_SYSKEY = "";

		//获取企业基本信息   qcc_getEntBasicInfo
		public static String QCC_GET_ENT_BASICINFO = "";
		//获取企业受益人穿透信息    qcc_getBeneficiaryInfo  
		@Deprecated
		public static String QCC_GET_BENEFICIARY_INFO = "";
		//获取企业疑似实际控制人    qcc_getActualControl
		public static String QCC_GET_ACTUAL_CONTROL = "";
		//获取企业受益人穿透(股权穿透)信息    qcc_getStockList
		public static String QCC_GET_STOCK_LIST = "";
		//获取企业投资列表    qcc_getInvestmentList
		public static String QCC_INVESTMENT_LIST = "";
		//获取企业投资穿透   qcc_getInvestmentThrough
		public static String QCC_GET_INVESTMENT_THROUTH = "";
		//裁判文书查询    qcc_getJudgmentDoc
		public static String QCC_GET_JUDGMENT_DOC = "";
		//裁判文书详情查询    qcc_getJudgmentDtl
		public static String QCC_GET_JUDGMENT_DTL = "";
		//企查查 是否使用生产环境地址的开关   qcc_switch -->     on  off
		public static String QCC_SWITCH = "";

		//碧湾云平台数据接口   qcc_api_url
		public static String BEAWAN_API_URL = "";
		//碧湾云平台使用密钥
		public static String BEAWAN_SYSKEY = "";
		//碧湾云平台 ------ 云平台开放标志
		public static String BEAWAN_GETANALYSISFLAG = "";
		
		
		
		
		
		
		//报表格式自动转化接口
		public static String TRANSFORM_EXCEL_RG = "";
//		行内模板文件夹地址   --该文件下有资产负债、利润 对应2套新旧准则报表  共4个文件。注：文件名且不能修改
		public static String TRANS_BANK_MODEL = "";
//		#上传企业电子报表的文件夹目录
		public static String TRANS_IN_PATH = "";
//		#转换后生成文件的根文件夹目录
		public static String TRANS_OUT_PATH = "";
		

		//行业分析接口报文
		public static String INDU_ANALYSIS = "";

		
		static{
			try {
				properties.load(new InputStreamReader(AppConfig.class.getClassLoader()
						.getResourceAsStream("cfg.properties"),"UTF-8"));
				
				CLOUD_FINRISK = getAttribute("cloud_finrisk");
				CLOUD_ANALYSIS = getAttribute("cloud_analysis");
				
				LOCAL_FINRISK = getAttribute("local_finrisk");
				LOCAL_ANALYSIS = getAttribute("local_analysis");

				WORK_CAPITAL_MEASURE = getAttribute("work_capital_measure");
				
				//如皋在用的
				LOCAL_FINRISK_ANALYSIS = getAttribute("local_finrisk_analysis");
				LOCAL_ANALYSIS_RESULT = getAttribute("local_finrisk_result");
				CLOUD_ANALYSIS_RESULT = getAttribute("cloud_finrisk_result");
				GET_QUOTA_INFO = getAttribute("getQuotaInfo");

				ROOT_FINRISK = getAttribute("root_finrisk");
				V_LOCAL = getAttribute("v_local");
				V_CLOUD = getAttribute("v_cloud");
				ERR_SUMMARY = getAttribute("errSummary");
				BALANCE_ASSETS = getAttribute("balanceAssets");
				BALANCE_DEBT = getAttribute("balanceDebt");
				BALANCE_OWNER = getAttribute("balanceOwner");
				INCOME_PANDECT = getAttribute("incomePandect");
				CASH_FLOW_PANDECT = getAttribute("cashFlowPandect");
				EIGHT_CASH_FLOW = getAttribute("eightCashflow");
				
				
				RATIO = getAttribute("ratio");


				IMAGE_PATH = getAttribute("image_path");
				IMAGE_MAPPING = getAttribute("image_mapping");
				IMAGE_RETAIL_MAPPING = getAttribute("image_retail_mapping");
				IMAGE_SURVEY_MAPPING = getAttribute("image_survey_mapping");
				ACCOUNT_PATH = getAttribute("account_path");
				REPORT_PATH = getAttribute("report_path");
				FIVE_CLASS_PATH = getAttribute("five_class_path");
				FIVE_CLASS_WORKING_PAPER_PATH = getAttribute("five_class_working_paper_path");
				FIVE_CLASS_IDENTIFICATION_TABLE_PATH = getAttribute("five_class_identification_table_path");
				
				GET_QCC_FULL_URL = getAttribute("get_qcc_full_path");
				QCC_API_URL = getAttribute("qcc_api_url");
				QCC_SYSKEY = getAttribute("qcc_syskey");

				QCC_GET_ENT_BASICINFO = getAttribute("qcc_getEntBasicInfo");
				QCC_GET_BENEFICIARY_INFO = getAttribute("qcc_getBeneficiaryInfo");
				QCC_GET_ACTUAL_CONTROL = getAttribute("qcc_getActualControl");
				QCC_GET_STOCK_LIST = getAttribute("qcc_getStockList");
				QCC_INVESTMENT_LIST = getAttribute("qcc_getInvestmentList");
				QCC_GET_INVESTMENT_THROUTH = getAttribute("qcc_getInvestmentThrough");
				QCC_GET_JUDGMENT_DOC = getAttribute("qcc_getJudgmentDoc");
				QCC_GET_JUDGMENT_DTL = getAttribute("qcc_getJudgmentDtl");
				QCC_SWITCH = getAttribute("qcc_switch");
				

				BEAWAN_API_URL = getAttribute("beawan_api_url");
				BEAWAN_SYSKEY = getAttribute("beawan_syskey");
				BEAWAN_GETANALYSISFLAG = getAttribute("beawan_getAnalysisFlag");
				
				TRANSFORM_EXCEL_RG = getAttribute("transformExcelRG");
				TRANS_BANK_MODEL = getAttribute("trans_bank_model");
				TRANS_IN_PATH = getAttribute("trans_in_path");
				TRANS_OUT_PATH = getAttribute("trans_out_path");
				
				
				
				INDU_ANALYSIS = getAttribute("indu_analysis");




			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		static String getAttribute(String key) {
			return properties.getProperty(key, "");
		}
	}
}
