package com.beawan.common.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.platform.util.StringUtil;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class SqlParam {
	protected List<String> sqls;
	protected List<Object> parameters;

	public String getSql() {
		if (this.sqls == null) {
			return null;
		} else {
			return StringUtil.join(this.sqls, " and ");
		}
	}

	public void setSql(String sql) {
		this.sqls = new ArrayList<String>();
		this.sqls.add(sql);
	}

	public void addSql(String sql) {
		if (StringUtil.isNotBlank(sql)) {
			if (this.sqls == null) {
				this.sqls = new ArrayList<String>();
			}
			this.sqls.add(sql);
		}
	}

	public Object[] getParameters() {
		if (this.parameters == null) {
			return null;
		} else {
			return this.parameters.toArray(new Object[this.parameters.size()]);
		}
	}

	public void setParameters(List<Object> parameters) {
		this.parameters = parameters;
	}

	public void addParameters(List parameters) {
		if (this.parameters == null) {
			this.parameters = new ArrayList<Object>();
		}
		this.parameters.addAll(parameters);
	}

	public void addParameters(Object... parameters) {
		if (this.parameters == null) {
			this.parameters = new ArrayList<Object>();
		}
		for (int i = 0, length = parameters.length; i < length; i++) {
			this.parameters.add(parameters[i]);
		}
	}

	public void addParameters(Object parameters) {
		if (parameters != null) {
			if (parameters instanceof List) {
				this.addParameters((List) parameters);
			} else if (parameters.getClass().isArray()) {
				this.addParameters((Object[]) parameters);
			} else {
				this.addParameters(new Object[] { parameters });
			}
		}
	}

	public void addSqlParam(SqlParam sqlParam) {
		if (sqlParam != null) {
			this.addSqlParam(sqlParam.getSql(), sqlParam.getParameters());
		}
	}

	public void addSqlParam(String sql, Object... parameters) {
		this.addSql(sql);
		this.addParameters((Object) parameters);
	}
}