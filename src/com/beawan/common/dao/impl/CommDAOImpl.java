package com.beawan.common.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import java.util.Map;

import com.beawan.common.dao.ICommDAO;

@SuppressWarnings({ "unchecked" })
public abstract class CommDAOImpl<T> extends DaoHandler implements ICommDAO<T> {
	
	public Class<T> getClazz() {
		ParameterizedType genType = (ParameterizedType)this.getClass().getGenericSuperclass();
		Type[] params = genType.getActualTypeArguments();
		return (Class<T>) params[0];
	}

	@Override
	public T findByPrimaryKey(Object id) {
		return this.selectByPrimaryKey(this.getClazz(), id);
	}

	@Override
	public List<T> getAll() {
		return this.select("");
	}

	@Override
	public T saveOrUpdate(T entity) {
		return this.save(this.getClazz(), entity);
	}
	
	@Override

	public void deleteEntity(T entity) {
		this.delete(this.getClazz(), entity);
	}

	@Override
	public void refresh(T entity) {
		this.refresh(this.getClazz(), entity);
	}

	@Override
	public List<T> select(String query, Object... args) {
		return this.select(this.getClazz(), query, args);
	}
	
	@Override
	public List<T> select(String query, Map<String,Object> params) {
		return this.select(this.getClazz(), query, params);
	}

	@Override
	public List<T> selectByNative(String query, Object... args) {
		return this.selectByNative(this.getClazz(), query, args);
	}

	@Override
	public List<T> selectByProperty(String property, Object value) {
		return this.selectByProperty(this.getClazz(), property, value);
	}

	@Override
	public List<T> selectByProperty(Map<String, Object> params) {
		return this.selectByProperty(this.getClazz(), params);
	}

	@Override
	public T selectByPrimaryKey(Object key) {
		return selectByPrimaryKey(this.getClazz(), key);
	}

	@Override
	public List<T> selectDistinct(String query, Object... args) {
		return this.selectDistinct(this.getClazz(), query, args);
	}

	@Override
	public List<T> selectDistinctByNative(String query, Object... args) {
		return this.selectDistinctByNative(this.getClazz(), query, args);
	}

	@Override
	public List<T> selectDistinctByProperty(String property, Object value) {
		return this.selectDistinctByProperty(this.getClazz(), property, value);
	}
	
	@Override
	public T selectSingle(String query, Map<String,Object> params) {
		return this.selectSingle(this.getClazz(), query, params);
	}

	@Override
	public T selectSingle(String query, Object... args) {
		return this.selectSingle(this.getClazz(), query, args);
	}

	@Override
	public T selectSingleByNative(String query, Object... args) {
		return this.selectSingleByNative(this.getClazz(), query, args);
	}

	@Override
	public T selectSingleByProperty(String property, Object value) {
		return this.selectSingleByProperty(this.getClazz(), property, value);
	}

	@Override
	public List<T> selectRange(String query, int index, int count, Object... args) {
		return this.selectRange(this.getClazz(), query, index, count, args);
	}

	@Override
	public List<T> selectRange(String query, int index, int count, Map<String,Object> params){
		return this.selectRange(this.getClazz(),query, index, count, params);
	}
	
	@Override
	public List<T> selectRangeByNative(String query, int index, int count, Object... args) {
		return this.selectRangeByNative(this.getClazz(), query, index, count, args);
	}

	@Override
	public List<T> selectRangeByProperty(String property, Object value, int index, int count) {
		return this.selectRangeByProperty(this.getClazz(), property, value, index, count);
	}

	@Override
	public long selectCount(String query, Object... args) {
		return this.selectCount(this.getClazz(), query, args);
	}
	
	@Override
	public long selectCount(String query, Map<String,Object> params) {
		return this.selectCount(this.getClazz(), query, params);
	}

	@Override
	public long selectCountByNative(String query, Object... args) {
		return this.selectCountByNative(this.getClazz(), query, args);
	}

	@Override
	public long selectCountByProperty(String property, Object value) {
		return this.selectCountByProperty(this.getClazz(), property, value);
	}

	@Override
	public boolean isExist(String query, Object... args) {
		return this.isExist(this.getClazz(), query, args);
	}

	@Override
	public boolean isExistByNative(String query, Object... args) {
		return this.isExistByNative(this.getClazz(), query, args);
	}

	@Override
	public boolean isExistByProperty(String property, Object value) {
		return this.isExistByProperty(this.getClazz(), property, value);
	}
	
}