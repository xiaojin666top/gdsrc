package com.beawan.common.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.beawan.common.dao.IGenericDao;
import com.beawan.common.entity.IdentifiableEntity;
@Repository("genericDao")
@SuppressWarnings({ "unchecked" })
public class GenericDao<T extends IdentifiableEntity> extends DaoHandler implements IGenericDao<T> {
	
	public Class<T> getClazz() {
		Type genType = this.getClass().getGenericSuperclass();
		Type[] params = ((ParameterizedType) genType).getActualTypeArguments();
		return (Class<T>) params[0];
	}

	@Override
	public T get(String id) {
		return this.selectByPrimaryKey(this.getClazz(), id);
	}

	@Override
	public List<T> getAll() {
		return this.select("");
	}

	@Override
	public T save(T entity) {
		entity.setUpdateTime(new Date());
		return this.save(this.getClazz(), entity);
	}


	@Override
	public void delete(T entity) {
		this.delete(this.getClazz(), entity);
	}

	@Override
	public void delete(String id) {
		T entity = this.get(id);
		this.delete(entity);
	}

	@Override
	public void refresh(T entity) {
		this.refresh(this.getClazz(), entity);
	}

	@Override
	public List<T> select(String query, Object... args) {
		return this.select(this.getClazz(), query, args);
	}

	@Override
	public List<T> selectByNative(String query, Object... args) {
		return this.selectByNative(this.getClazz(), query, args);
	}

	@Override
	public List<T> selectByProperty(String property, Object value) {
		return this.selectByProperty(this.getClazz(), property, value);
	}

	@Override
	public List<T> selectDistinct(String query, Object... args) {
		return this.selectDistinct(this.getClazz(), query, args);
	}

	@Override
	public List<T> selectDistinctByNative(String query, Object... args) {
		return this.selectDistinctByNative(this.getClazz(), query, args);
	}

	@Override
	public List<T> selectDistinctByProperty(String property, Object value) {
		return this.selectDistinctByProperty(this.getClazz(), property, value);
	}

	@Override
	public T selectSingle(String query, Object... args) {
		return this.selectSingle(this.getClazz(), query, args);
	}

	@Override
	public T selectSingleByNative(String query, Object... args) {
		return this.selectSingleByNative(this.getClazz(), query, args);
	}

	@Override
	public T selectSingleByProperty(String property, Object value) {
		return this.selectSingleByProperty(this.getClazz(), property, value);
	}

	@Override
	public List<T> selectRange(String query, int index, int count, Object... args) {
		return this.selectRange(this.getClazz(), query, index, count, args);
	}

	@Override
	public List<T> selectRangeByNative(String query, int index, int count, Object... args) {
		return this.selectRangeByNative(this.getClazz(), query, index, count, args);
	}

	@Override
	public List<T> selectRangeByProperty(String property, Object value, int index, int count) {
		return this.selectRangeByProperty(this.getClazz(), property, value, index, count);
	}

	@Override
	public long selectCount(String query, Object... args) {
		return this.selectCount(this.getClazz(), query, args);
	}

	@Override
	public long selectCountByNative(String query, Object... args) {
		return this.selectCountByNative(this.getClazz(), query, args);
	}

	@Override
	public long selectCountByProperty(String property, Object value) {
		return this.selectCountByProperty(this.getClazz(), property, value);
	}

	@Override
	public boolean isExist(String query, Object... args) {
		return this.isExist(this.getClazz(), query, args);
	}

	@Override
	public boolean isExistByNative(String query, Object... args) {
		return this.isExistByNative(this.getClazz(), query, args);
	}

	@Override
	public boolean isExistByProperty(String property, Object value) {
		return this.isExistByProperty(this.getClazz(), property, value);
	}
	
}