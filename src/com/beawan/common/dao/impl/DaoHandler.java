package com.beawan.common.dao.impl;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.beanutils.ConvertUtils;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.beawan.common.Constants;
import com.beawan.core.Pagination;
import com.platform.util.StringUtil;

@Repository("daoHandler")
@SuppressWarnings({ "unchecked", "rawtypes" })
public class DaoHandler {
	protected static final Logger LOGGER = LoggerFactory.getLogger(DaoHandler.class);

   @PersistenceContext(name = "entityManagerFactory")
	protected EntityManager entityManager;

	
/*	protected EntityManagerFactory entityManagerFactory=(EntityManagerFactory) SpringContextUtil.getBean("entityManagerFactory");
	protected EntityManager entityManager=this.entityManagerFactory.createEntityManager();*/

	public DaoHandler() {

	}

	public DaoHandler(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public EntityManager getEntityManager() {
		return this.entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public <T> String getSql(Class<T> cls, String query, String target) {
		String sql = "select ";
		boolean removeOrder = false;
		if (StringUtil.isBlank(target)) {
			sql += "t";
		} else {
			sql += target + "(t)";
			target = target.trim().toLowerCase();
			if (target.equals("count")) {
				removeOrder = true;
			}
		}
		sql += " from " + cls.getSimpleName() + " t";
		if (StringUtil.isNotBlank(query)) {
			String lower = query.trim().toLowerCase();
			if (lower.startsWith("select ") || lower.startsWith("from ")) {
				sql = query;
			} else if (lower.startsWith("where ") || lower.startsWith("order ")) {
				sql += " " + query;
			} else {
				sql += " where " + query;
			}
		}
		if (removeOrder) {
			int index = sql.toLowerCase().lastIndexOf(" order ");
			if (index >= 0) {
				sql = sql.substring(0, index);
			}
		}
		return sql;
	}

	public <T> String getSql(Class<T> cls, String query) {
		return this.getSql(cls, query, null);
	}

	public <T> Query getQuery(Class<T> cls, String query, String target, Object... args) {
		String sql = this.getSql(cls, query, target);
		Query q = this.entityManager.createQuery(sql);
		q = this.setParameters(q, (Object) args);
		return q;
	}
	
	public <T> Query getQuery(Class<T> cls, String query, String target,  Map<String, Object> args) {
		String sql = this.getSql(cls, query, target);
		Query q = this.entityManager.createQuery(sql);
		q = this.setParameters(q, args);
		return q;
	}

	public <T> Query getQuery(Class<T> cls, String query, Object... args) {
		return this.getQuery(cls, query, null, args);
	}

	public <T> Query getNativeQuery(Class<T> cls, String query, String target, Object... args) {
		String sql = this.getSql(cls, query, target);
		Query q = this.entityManager.createNativeQuery(sql);
		q = this.setParameters(q, (Object) args);
		return q;
	}

	public <T> Query getNativeQuery(Class<T> cls, String query, Object... args) {
		return this.getNativeQuery(cls, query, null, args);
	}

	public Query setParameters(Query query, Map<String, Object> parameters) {
		if(parameters!=null){
			for (String key : parameters.keySet()) {
				Object obj = parameters.get(key);
				/*if (obj instanceof Collection<?>) {
					query.setParameter(key, (Collection<?>)obj);
				} else if (obj instanceof Object[]) {
					query.setParameter(key, (Object[])obj);
				}else{
					query.setParameter(key, obj);
				}*/
				query.setParameter(key, obj);
			}
		}
		
		return query;
	}

	public Query setParameters(Query query, Object... parameters) {
		if(parameters==null){
			return query;
		}
		for (int i = 0, length = parameters.length; i < length; i++) {
			query.setParameter(i + 1, parameters[i]);
		}
		return query;
	}

	public Query setParameters(Query query, Object arg) {
		if (arg == null) {
			return query;
		} else if (arg instanceof Map) {
			return this.setParameters(query, (Map) arg);
		} else if (arg.getClass().isArray()) {
			return this.setParameters(query, (Object[]) arg);
		} else {
			return this.setParameters(query, new Object[] { arg });
		}
	}

	public <T> List<T> select(Class<T> cls, Query query) {
		List<T> list = query.getResultList();
		if (list == null) {
			list = new ArrayList<T>();
		}
		return list;
	}

	public <T> T selectSingle(Class<T> cls, Query query) {
		List<T> list = query.getResultList();
		if (list == null || list.size() <= 0) {
			return null;
		} else {
			return list.get(0);
		}
	}

	public <T> long selectCount(Class<T> cls, Query query) {
		Object obj = query.getSingleResult();
		return ((Long) obj).longValue();
	}

	public <T> List<T> select(Class<T> cls, String query, Object... args) {
		Query q = this.getQuery(cls, query, args);
		return this.select(cls, q);
	}
	/**
	 * 查询
	 * @param cls
	 * @param query
	 * @param args 占位符采用参数命名的方式
	 * @return
	 */
	public <T> List<T> select(Class<T> cls, String query, Map<String, Object> args) {
		String sql = this.getSql(cls, query, null);
		Query q = this.entityManager.createQuery(sql);
		q = this.setParameters(q, args);
		return this.select(cls, q);
	}
	
	public <T>  T  selectSingle(Class<T> cls, String query, Map<String, Object> args) {
		String sql = this.getSql(cls, query, null);
		Query q = this.entityManager.createQuery(sql);
		q = this.setParameters(q, args);
		return this.selectSingle(cls, q);
	}

	public <T> List<T> selectByNative(Class<T> cls, String query, Object... args) {
		Query q = this.getNativeQuery(cls, query, args);
		return this.select(cls, q);
	}

	public <T> List<T> selectAll(Class<T> cls) {
		return this.select(cls, "");
	}

	public <T> List<T> selectByProperty(Class<T> cls, String property, Object value) {
		return this.select(cls, property + " = ?1 and status = ?2", value,Constants.NORMAL);
	}

	public <T> List<T> selectByProperty(Class<T> cls, String property, Object value, String orderType) {
		return this.select(cls, property + " = ?1 and status = ?2 order by "+ orderType, value ,Constants.NORMAL);
	}

	public <T> List<T> selectByProperty(Class<T> cls, Map<String, Object> params) {
		StringBuilder properties = new StringBuilder();
		if(params == null || params.size() == 0) return null;
		params.put("status", Constants.NORMAL);
		for(Map.Entry<String, Object> entry : params.entrySet()) {
			properties.append(" and ").append(entry.getKey()).append("='").append(entry.getValue()).append("'");
		}
		String sql = properties.substring(4);//截取前面多的 and
		params.clear();
		return this.select(cls, sql, params);//让params为null，后面设置参数打开
	}
	
	public <T> List<T> selectByProperty(Class<T> cls, Map<String, Object> params, String orderType) {
		StringBuilder properties = new StringBuilder();
		if(params==null || params.size()==0)
			return null;
//			throw new Exception("查询参数为空");
		params.put("status", Constants.NORMAL);
		for(Map.Entry<String, Object> entry : params.entrySet()) {
			properties.append(" and " + entry.getKey()+ "=:" +entry.getKey());
		}
		properties.append(" order by " + orderType);
		String sql = properties.substring(4);
		return this.select(cls, sql, params);
	}
	
	

	public <T> List<T> selectDistinct(Class<T> cls, String query, Object... args) {
		Query q = this.getQuery(cls, query, "distinct", args);
		return this.select(cls, q);
	}

	public <T> List<T> selectDistinctByNative(Class<T> cls, String query, Object... args) {
		Query q = this.getNativeQuery(cls, query, "distinct", args);
		return this.select(cls, q);
	}

	public <T> List<T> selectDistinctByProperty(Class<T> cls, String property, Object value) {
		return this.selectDistinct(cls, property + " = ?1", value);
	}

	public <T> T selectSingle(Class<T> cls, String query, Object... args) {
		Query q = this.getQuery(cls, query, args);
		return this.selectSingle(cls, q);
	}

	public <T> T selectSingleByNative(Class<T> cls, String query, Object... args) {
		Query q = this.getNativeQuery(cls, query, args);
		return this.selectSingle(cls, q);
	}

	public <T> T selectSingleByProperty(Class<T> cls, String property, Object value) {
		return this.selectSingle(cls, property + " = ?1 and status= ?2", value, Constants.NORMAL);//最后参数表示状态，是否正常使用
	}

	public <T> T selectByPrimaryKey(Class<T> cls, Object id) {
		return this.selectSingleByProperty(cls, "id", id);
	}

	/**
	 * 分页查询（占位符采用参数命名方式）
	 * @param cls
	 * @param query
	 * @param index
	 * @param count
	 * @param args
	 * @return
	 */
	public <T> List<T> selectRange(Class<T> cls, String query, int index, int count, Map<String, Object> args) {
		String sql = this.getSql(cls, query, null);
		Query q = this.entityManager.createQuery(sql);
		q = this.setParameters(q, args);
		if (index >= 0) {
			q = q.setFirstResult(index);
		}
		if (count >= 0) {
			q = q.setMaxResults(count);
		}
		return this.select(cls, q);
	}

	/**
	 * 分页查询（占位符采用物理顺序的方式）
	 * @param cls
	 * @param query
	 * @param index
	 * @param count
	 * @param args
	 * @return
	 */
	public <T> List<T> selectRange(Class<T> cls, String query, int index, int count, Object... args) {
		Query q = this.getQuery(cls, query, args);
		if (index >= 0) {
			q = q.setFirstResult(index);
		}
		if (count >= 0) {
			q = q.setMaxResults(count);
		}
		return this.select(cls, q);
	}
	
	public <T> List<T> selectRangeByNative(Class<T> cls, String query, int index, int count, Object... args) {
		Query q = this.getNativeQuery(cls, query, args);
		if (index >= 0) {
			q = q.setFirstResult(index);
		}
		if (count >= 0) {
			q = q.setMaxResults(count);
		}
		return this.select(cls, q);
	}

	public <T> List<T> selectRangeAll(Class<T> cls, int index, int count) {
		return this.selectRange(cls, null, index, count);
	}

	public <T> List<T> selectRangeByProperty(Class<T> cls, String property, Object value, int index, int count) {
		return this.selectRange(cls, property + " = ?1", index, count, value);
	}

	public <T> long selectCount(Class<T> cls, String query, Object... args) {
		Query q = this.getQuery(cls, query, "count", args);
		return this.selectCount(cls, q);
	}

	public <T> long selectCount(Class<T> cls, String query, Map<String, Object> args) {
		Query q = this.getQuery(cls, query, "count", args);
		return this.selectCount(cls, q);
	}


	public <T> long selectMax(Class<T> cls, String query, Map<String, Object> args) {
		Query q = this.getQuery(cls, query, "max", args);
		return this.selectCount(cls, q);
	}
	
	public <T> long selectCountByNative(Class<T> cls, String query, Object... args) {
		Query q = this.getNativeQuery(cls, query, "count", args);
		return this.selectCount(cls, q);
	}

	public <T> long selectCountAll(Class<T> cls) {
		return this.selectCount(cls, null);
	}

	public <T> long selectCountByProperty(Class<T> cls, String property, Object value) {
		return this.selectCount(cls, property + " = ?1", value);
	}

	public <T> boolean isExist(Class<T> cls, String query, Object... args) {
		long count = this.selectCount(cls, query, args);
		return count > 0;
	}

	public <T> boolean isExistByNative(Class<T> cls, String query, Object... args) {
		long count = this.selectCountByNative(cls, query, args);
		return count > 0;
	}

	public <T> boolean isExistByProperty(Class<T> cls, String property, Object value) {
		return this.isExist(cls, property + " = ?1", value);
	}

	public <T> T save(Class<T> cls, T entity) {
		return this.entityManager.merge(entity);
	}
	
	public <T> T save(T entity) {
		return this.entityManager.merge(entity);
	}

	public <T> boolean batchSaveOrUpdate(List<T> list){
		return batchSaveOrUpdate(list, 500);
	}

	public <T> boolean batchSaveOrUpdate(List<T> list, int batchSize){
		if(list==null)
			return false;
//		EntityTransaction transaction = entityManager.getTransaction();
//		transaction.begin();
//		Session session = (Session)entityManager.getDelegate();
//		session.setFlushMode(FlushMode.MANUAL);
//		long t1 = System.currentTimeMillis();
//		System.out.println("开始批量插入");
		for(int i = 0; i < list.size(); i++){
			/** 将要批量插入的实例转换为managed(托管)状态 */
			entityManager.merge(list.get(i));
			if(i % batchSize == (batchSize - 1) || i==(list.size() - 1)){
//				LOGGER.info("第" + (i/batchSize) + "次刷新entityManager!");
//				long t2 = System.currentTimeMillis();
//				System.out.println("开始第"+(i+1)+"刷新输出,merge用时：" + (t2 - t1));
				entityManager.flush();
//				long t3 = System.currentTimeMillis();
//				System.out.println("flush用时：" + (t3 - t2));
				entityManager.clear();
//				transaction.commit();
			}
		}
		LOGGER.info("批量保存操作完成");
		return true;
	}
	
	public <T> void delete(Class<T> cls, T entity) {
	   try {
		   Object object = this.entityManager.merge(entity);
		   this.entityManager.remove(object);
       } catch (RuntimeException re) {
           throw re;
       }
	}
	
	public <T> void delete(T entity) {
		   try {
			   Object object = this.entityManager.merge(entity);
			   this.entityManager.remove(object);
	       } catch (RuntimeException re) {
	           throw re;
	       }
		}
	
	
	/**
	 * 执行HQL,返回结果集（占位符采用顺序的方式）
	 * @param cls
	 * @param sql 完整的HQL语句
	 * @param params 数组
	 * @return
	 * @throws Exception
	 */
	public <T> List<T>  execQuery(Class<T> cls, String sql, Object... params)throws Exception{
		Query query = this.entityManager.createQuery(sql);
		query = this.setParameters(query, params);
		List<T> list = query.getResultList();
		if (list == null) {
			list = new ArrayList<T>();
		}
		return list;
	}
	
	/**
	 * 执行HQL,返回结果集（占位符采用参数命名的方式）
	 * @param cls
	 * @param sql 完整的HQL语句
	 * @param params Map集合KEY参数名，VALUE参数值
	 * @return
	 * @throws Exception
	 */
	public <T> List<T>  execQuery(Class<T> cls, String sql, Map<String,Object> params)throws Exception{
		Query query = this.entityManager.createQuery(sql);
		query = this.setParameters(query, params);
		List<T> list = query.getResultList();
		if (list == null) {
			list = new ArrayList<T>();
		}
		return list;
	}
	
	/**
	 * 分页查询 （占位符采用顺序的方式）
	 * @param cls
	 * @param sql 完整的HQL语句
	 * @param index
	 * @param count
	 * @param params 数组
	 * @return
	 * @throws Exception
	 */
	public <T> List<T>  execQueryRange(Class<T> cls, String sql, int index, int count, Object... params)throws Exception{
		Query query = this.entityManager.createQuery(sql);
		query = this.setParameters(query, params);
		if (index >= 0) {
			query = query.setFirstResult(index);
		}
		if (count >= 0) {
			query = query.setMaxResults(count);
		}
		List<T> list = query.getResultList();
		if (list == null) {
			list = new ArrayList<T>();
		}
		return list;
	}
	
	/**
	 * 分页查询 （占位符采用参数命名的方式）
	 * @param cls
	 * @param sql 完整的HQL语句
	 * @param index
	 * @param count
	 * @param params Map集合KEY参数名，VALUE参数值
	 * @return
	 * @throws Exception
	 */
	public <T> List<T>  execQueryRange(Class<T> cls, String sql, int index, int count, Map<String,Object> params)throws Exception{
		Query query = this.entityManager.createQuery(sql);
		query = this.setParameters(query, params);
		if (index >= 0) {
			query = query.setFirstResult(index);
		}
		if (count >= 0) {
			query = query.setMaxResults(count);
		}
		List<T> list = query.getResultList();
		if (list == null) {
			list = new ArrayList<T>();
		}
		return list;
	}

	
 
	public <T> void refresh(Class<T> cls, T entity) {
		this.entityManager.refresh(entity);
	}

	public void flush() {
		this.entityManager.flush();
	}
	
	/**
	 * yuzhejia
	 * 
	 */
	public <T> List<T> findCustListBySql(Class<T> beanClass, CharSequence queryStr, Object... params){
		Query query = entityManager.createNativeQuery(queryStr.toString());
		query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		if(params != null) {
			for(int i =0; i < params.length; i++) {
				query.setParameter(i, params[i]);
			}
		}
		List<Map<String,Object>> list = query.getResultList();
		
		if(list==null || list.size() <=0) {
			return new ArrayList<T>();
		}
		
		List<T> result = new ArrayList<T>();
		try {
			PropertyDescriptor[] props = Introspector.getBeanInfo(beanClass).getPropertyDescriptors();
			for(Map<String,Object> map : list) {
				T t = beanClass.newInstance();
				for(Map.Entry<String, Object> entry : map.entrySet()) {
					String attrName = entry.getKey();
					for(PropertyDescriptor prop : props) {
						//加个   驼峰转化
						if(!attrName.equalsIgnoreCase(prop.getName()) )
							continue;
						Method method = prop.getWriteMethod();
						
						Object value = entry.getValue();
						//类型转化
						if(value != null) {
							value = ConvertUtils.convert(value, prop.getPropertyType());
						}
						method.invoke(t, value);
					}
				}
				result.add(t);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public <T> T findCustOneBySql(Class<T> beanClass, CharSequence queryStr, Object... params){
		Query query = entityManager.createNativeQuery(queryStr.toString());
		if(params != null) {
			for(int i =0; i < params.length; i++) {
				query.setParameter(i, params[i]);
			}
		}
		List<Object> list = query.getResultList();

		if(list==null || list.size() <=0) {
			return null;
		}
		Object obj = list.get(0);
		if(obj==null){
			return null;
		}
		return (T)obj;
	}
	

	public <T> List<T> findCustOneListBySql(Class<T> beanClass, CharSequence queryStr, Object... params){
		Query query = entityManager.createNativeQuery(queryStr.toString());
		if(params != null) {
			for(int i =0; i < params.length; i++) {
				query.setParameter(i, params[i]);
			}
		}
		List<Object> list = query.getResultList();

		if(list==null || list.size() <=0) {
			return null;
		}
		List<T> reList = new ArrayList();
		for(Object obj : list){
			reList.add((T)obj);
		}
		return reList;
	}
	
	public <T> List<T> findCustListBySql(Class<T> beanClass, CharSequence queryStr, Map<String,Object> params){
		Query query = entityManager.createNativeQuery(queryStr.toString());
		query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		if (params != null) {
			query = setParameters(query,params);
		}
		List<Map<String,Object>> list = query.getResultList();
		
		if(list==null || list.size() <=0) {
			return new ArrayList<T>();
		}
		
		List<T> result = new ArrayList<T>();
		try {
			PropertyDescriptor[] props = Introspector.getBeanInfo(beanClass).getPropertyDescriptors();
			for(Map<String,Object> map : list) {
				T t = beanClass.newInstance();
				for(Map.Entry<String, Object> entry : map.entrySet()) {
					String attrName = entry.getKey();
					for(PropertyDescriptor prop : props) {
						if(!attrName.equalsIgnoreCase(prop.getName()))
							continue;
						Method method = prop.getWriteMethod();
						
						Object value = entry.getValue();
						//类型转化
						if(value != null) {
							value = ConvertUtils.convert(value, prop.getPropertyType());
						}
						method.invoke(t, value);
					}
				}
				result.add(t);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public Integer findCustSingeBySql(CharSequence queryStr, Map<String,Object> params){
		Query query = entityManager.createNativeQuery(queryStr.toString());
		//这个是返回map对象  数据列-》对应值
//		query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		if (params != null) {
			query = setParameters(query,params);
		}
//		Object obj = query.getSingleResult();
//		return ((Long) obj).longValue();
		return (Integer)query.getSingleResult();
//		return query.getFirstResult();

	}

    public <T> Pagination<T> findSqlPagination(Class beanClass, CharSequence queryString, final CharSequence countString,
                                               final Map<String, Object> params, int pageIndex, int pageSize) {
    	Query query = entityManager.createNativeQuery(queryString.toString());
		query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
//        BigInteger rowsCount = BigInteger.valueOf(0L);
        long rowsCount = 0l;

        if ((params != null) && (!(params.isEmpty()))) {
            setParameters(query, params);
        }
        if ((pageSize > 0) && (pageIndex > 0)) {
        	Query countQuery = entityManager.createNativeQuery(countString.toString());
        	if ((params != null) && (!(params.isEmpty()))) {
                setParameters(countQuery, params);
            }
        	Object obj = countQuery.getSingleResult();
        	rowsCount = ((Integer)obj).longValue();
        }

        if ((pageSize > 0) && (pageIndex > 0)) {
            query.setFirstResult((pageIndex < 2) ? 0 : (pageIndex - 1) * pageSize);
            query.setMaxResults(pageSize);
        }

		List<Map<String,Object>> list = query.getResultList();
		List<T> items = (List<T>) hibListToListBean(list,beanClass);
		
        Pagination pagination = new Pagination(pageIndex, pageSize, rowsCount);
        pagination.setItems(items);
        return pagination;
    }

    @SuppressWarnings("serial")
	public <T> Pagination<T> findCustSqlPagination(Class beanClass,CharSequence queryString, final CharSequence countString,
												   final Map<String, Object> params, int pageIndex, int pageSize) {
		Query query = entityManager.createNativeQuery(queryString.toString());
		query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

		long rowsCount = 0l;

		if ((pageSize > 0) && (pageIndex > 0)) {
			Query countQuery = entityManager.createNativeQuery(countString.toString());
			//setParameters(countQuery, params);
			Object obj = countQuery.getSingleResult();
			//rowsCount = ((Long)obj).longValue();
            long i = Long.parseLong(obj.toString());
            rowsCount = i;
		}
		if ((pageSize > 0) && (pageIndex > 0)) {
			query.setFirstResult((pageIndex < 2) ? 0 : (pageIndex - 1) * pageSize);
			query.setMaxResults(pageSize);
		}
	/*	if ((params != null) && (!(params.isEmpty()))) {
			setParameters(query, params);
		}*/
		List<Map<String, Object>> list = (List<Map<String, Object>>) query
				.getResultList();

		List<T> items = (List<T>) hibListToListBean(list,beanClass);

		Pagination pagination = new Pagination(pageIndex, pageSize, rowsCount);
		pagination.setItems(items);
		return pagination;
	}

	@SuppressWarnings("serial")
	public <T> Pagination<T> findCustSqlPagination(Class beanClass,CharSequence queryString,
												   final Map<String, Object> params, int pageIndex, int pageSize) {
		Query query = entityManager.createNativeQuery(queryString.toString());
		query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

		long rowsCount = 0l;

		if ((pageSize > 0) && (pageIndex > 0)) {
			String countString = "select count(*) from (" + queryString + ") tb9";
			Query countQuery = entityManager.createNativeQuery(countString);
			setParameters(countQuery, params);
			Object obj = countQuery.getSingleResult();
			rowsCount = ((Integer)obj).longValue();
		}
		if ((pageSize > 0) && (pageIndex > 0)) {
			query.setFirstResult((pageIndex < 2) ? 0 : (pageIndex - 1) * pageSize);
			query.setMaxResults(pageSize);
		}
		if ((params != null) && (!(params.isEmpty()))) {
			setParameters(query, params);
		}
		List<Map<String, Object>> list = (List<Map<String, Object>>) query
				.getResultList();

		List<T> items = (List<T>) hibListToListBean(list,beanClass);

		Pagination pagination = new Pagination(pageIndex, pageSize, rowsCount);
		pagination.setItems(items);
		return pagination;
	}
    
    public <T> List<T>  hibListToListBean(List<Map<String, Object>> list,Class<T> beanClass){

		List<T> result = new ArrayList<T>();
		try {
			PropertyDescriptor[] props = Introspector.getBeanInfo(beanClass).getPropertyDescriptors();
			for(Map<String,Object> map : list) {
				T t = beanClass.newInstance();
				for(Map.Entry<String, Object> entry : map.entrySet()) {
					String attrName = entry.getKey();
					for(PropertyDescriptor prop : props) {
						if(!attrName.equalsIgnoreCase(prop.getName()))
							continue;
						Method method = prop.getWriteMethod();
						
						Object value = entry.getValue();
						//类型转化
						if(value != null) {
							value = ConvertUtils.convert(value, prop.getPropertyType());
						}
						method.invoke(t, value);
					}
				}
				result.add(t);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
    }


	public <T> T getDtmpById(Class<T> cls, Object id) {
		return getDtmpSingleByProperty(cls, "id", id);
	}

	public <T> T getDtmpSingleByProperty(Class<T> cls, String property, Object value) {
		List<T> list = getDtmpByProperty(cls, property, value);
		if(list!=null && list.size()!=0)
			return list.get(0);
		return null;
	}

	public <T> List<T> getDtmpByProperty(Class<T> cls, String property, Object value) {
		Map<String, Object> params = new HashMap<>();
		params.put(property, value);
		List<T> list = getDtmpByProperty(cls, params);
		return list;
	}

	public <T> List<T> getDtmpByProperty(Class<T> cls, Map<String, Object> params) {
		StringBuilder properties = new StringBuilder();
		if(params==null || params.size()==0)
			return null;
		for(Map.Entry<String, Object> entry : params.entrySet()) {
			properties.append(" and ").append(entry.getKey()).append("=:").append(entry.getKey());
		}
		String sql = properties.substring(4);
		return this.select(cls, sql, params);

	}

}