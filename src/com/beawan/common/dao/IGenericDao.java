package com.beawan.common.dao;

import java.util.List;

import com.beawan.common.entity.IdentifiableEntity;

public interface IGenericDao<T extends IdentifiableEntity> {
	public T get(String id);

	public List<T> getAll();

	public T save(T entity);

	public void delete(T entity);

	public void delete(String id);

	public void refresh(T entity);

	public void flush();

	public List<T> select(String query, Object... args);

	public List<T> selectByNative(String query, Object... args);

	public List<T> selectByProperty(String property, Object value);

	public List<T> selectDistinct(String query, Object... args);

	public List<T> selectDistinctByNative(String query, Object... args);

	public List<T> selectDistinctByProperty(String property, Object value);

	public T selectSingle(String query, Object... args);

	public T selectSingleByNative(String query, Object... args);

	public T selectSingleByProperty(String property, Object value);

	public List<T> selectRange(String query, int index, int count, Object... args);

	public List<T> selectRangeByNative(String query, int index, int count, Object... args);

	public List<T> selectRangeByProperty(String property, Object value, int index, int count);

	public long selectCount(String query, Object... args);

	public long selectCountByNative(String query, Object... args);

	public long selectCountByProperty(String property, Object value);

	public boolean isExist(String query, Object... args);

	public boolean isExistByNative(String query, Object... args);

	public boolean isExistByProperty(String property, Object value);
}