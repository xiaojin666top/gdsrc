package com.beawan.common.dao;

import java.util.List;
import java.util.Map;

public interface ICommDAO<T> {
	
	/**
	 * 主键查询
	 * @param id
	 * @return
	 */
	public T findByPrimaryKey(Object id);
	
	/**
	 * 查询所有
	 * @return
	 */
	public List<T> getAll();
	/**
	 * 保存或更新
	 * @param entity
	 * @return
	 */
	public T saveOrUpdate(T entity);
	
	public void deleteEntity(T entity);
	
	public void refresh(T entity);

	public void flush();
	/**
	 * 查询（占位符采用物理顺序的方式）
	 * @param query
	 * @param args
	 * @return
	 */
	public List<T> select(String query, Object... args);
	/**
	 * 查询（占位符采用参数命名方式）
	 * @param query
	 * @param params
	 * @return
	 */
	public List<T> select(String query, Map<String,Object> params);

	public List<T> selectByNative(String query, Object... args);

	public List<T> selectByProperty(String property, Object value);

	public List<T> selectByProperty(Map<String, Object> params);

	public T selectByPrimaryKey(Object key);

	public List<T> selectDistinct(String query, Object... args);

	public List<T> selectDistinctByNative(String query, Object... args);

	public List<T> selectDistinctByProperty(String property, Object value);

	public T selectSingle(String query, Map<String,Object> params);
	
	public T selectSingle(String query, Object... args);

	public T selectSingleByNative(String query, Object... args);

	public T selectSingleByProperty(String property, Object value);
	/**
	 * 分页查询（占位符采用物理顺序的方式）
	 * @param query
	 * @param index
	 * @param count
	 * @param args
	 * @return
	 */
	public List<T> selectRange(String query, int index, int count, Object... args);

	/**
	 * 分页查询（占位符采用参数命名方式）
	 * @param query
	 * @param index
	 * @param count
	 * @return
	 */
	public List<T> selectRange(String query, int index, int count,  Map<String,Object> params);
	
	public List<T> selectRangeByNative(String query, int index, int count, Object... args);

	public List<T> selectRangeByProperty(String property, Object value, int index, int count);

	public long selectCount(String query, Object... args);
	
	public long selectCount(String query, Map<String,Object> params);

	public long selectCountByNative(String query, Object... args);

	public long selectCountByProperty(String property, Object value);

	public boolean isExist(String query, Object... args);

	public boolean isExistByNative(String query, Object... args);

	public boolean isExistByProperty(String property, Object value);
}