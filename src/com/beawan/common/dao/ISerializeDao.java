package com.beawan.common.dao;

import com.beawan.common.dao.impl.SqlParam;
import com.beawan.common.entity.IdentifiableEntity;
import com.beawan.web.IRequestSerializer;


public interface ISerializeDao<T extends IdentifiableEntity> extends IGenericDao<T> {
	public SqlParam getSqlParam(IRequestSerializer request);



}