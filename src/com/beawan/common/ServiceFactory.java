package com.beawan.common;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * 服务工厂类
 */
@Component
public class ServiceFactory implements ApplicationContextAware {

    /**
     * TODO 以静态变量保存Spring ApplicationContext,
     * 可在任何代码任何地方任何时候中取出ApplicaitonContext.
     */
    private static ApplicationContext applicationContext; // Spring应用上下文环境

    /**
     * TODO 实现ApplicationContextAware接口的回调方法，设置上下文环境
     *
     * @param applicationContext
     * @throws BeansException
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext)
            throws BeansException {
        ServiceFactory.applicationContext = applicationContext;
    }

    /**
     * @return ApplicationContext
     */
    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public static void init() {

        // 存在问题，会初始化spring上下文两次
        /*
         * ActionContext ctx = ActionContext.getContext(); if(ctx!=null){
         * context =
         * WebApplicationContextUtils.getRequiredWebApplicationContext(
         * ServletActionContext.getServletContext()); } if (applicationContext
         * == null) { applicationContext = new
         * ClassPathXmlApplicationContext("applicationContext.xml"); }
         */

        /*
         * AbstractRefreshableApplicationContext arac =
         * (AbstractRefreshableApplicationContext) applicationContext;
         * if(!arac.isActive()) arac.refresh();
         */
    }

    public static Object getService(String name) {
        init();
        return applicationContext.getBean(name);
    }

    public static <T> T getService(Class<T> className) {
        init();
        return applicationContext.getBean(className);
    }

    public static Object getBean(String className) {
        init();
        return applicationContext.getBean(className);
    }

}
