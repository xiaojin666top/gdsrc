package com.beawan.common;

import java.sql.Timestamp;
import java.util.Date;

import com.beawan.common.service.ICommonSV;


/**
 * 服务管理器
 * @author mengbin
 *
 */
public class ServiceManager
{
	/**
	 * 获取数据库日期
	 * @param datasource
	 * @return
	 * @throws Exception
	 */
	public static Date getSysDate(String datasource)throws Exception
	{
		ICommonSV commonSV = (ICommonSV)ServiceFactory.getService(ICommonSV.class);
		
		return commonSV.getSysDate(datasource);
	}
	
	/**
	 * 获取数据库时间
	 * @param datasource
	 * @return
	 * @throws Exception
	 */
	public static Timestamp getSysTime(String datasource)throws Exception
	{
		ICommonSV commonSV = (ICommonSV)ServiceFactory.getService(ICommonSV.class);
		
		return commonSV.getSysTime(datasource);
	}
	
	/**
	 * 判断是否为超级管理员
	 * @version: v1.0.0
	 */
	/*public static boolean isSuperAdmin()throws Exception
	{
		boolean result = false;
		User user = (User)HttpUtil.getCurrentUser();
		if(user != null && LoanConstants.SUPER_ADMIN_USERNAME.equals(user.getUsername()))
		{
			result = true;
		}
		return result;
	}*/
}
