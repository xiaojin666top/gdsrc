package com.beawan.common.tags;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.view.freemarker.FreeMarkerView;

public class ASFreemarkerView extends FreeMarkerView {

	@Override
	protected void exposeHelpers(Map<String, Object> model,HttpServletRequest request)throws Exception{
		String scheme = request.getScheme();
		String serverName = request.getServerName();
		int port = request.getServerPort();
		String path = request.getContextPath();
		String basePath = scheme + "://" + serverName + ":" + port + path;
		model.put("basePath", basePath);
		//操作类型，view：查看，edit：编辑
		model.put("pageOption", request.getParameter("pageOption"));
		super.exposeHelpers(model, request);
	}
}
