package com.beawan.common.interceptor;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.beawan.base.entity.User;
import com.beawan.common.Constants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.platform.util.HttpUtil;

/**
 * 用户后台权限控制
 * 
 */
public class UserSessionInterceptor extends HandlerInterceptorAdapter {
	
	public static final String CF_SYSTEM = "cf/system.properties";
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		super.afterCompletion(request, response, handler, ex);
	}

	@Override
	public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		super.afterConcurrentHandlingStarted(request, response, handler);
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		super.postHandle(request, response, handler, modelAndView);
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		HttpSession session =  request.getSession();
		if(handler instanceof HandlerMethod){
			HandlerMethod hm = (HandlerMethod)handler;
			Class<?> clazz = hm.getBeanType();
			Method method = hm.getMethod();
			try {
				if(clazz!=null && method!=null){
					boolean isClzAnnotation = clazz.isAnnotationPresent(UserSessionAnnotation.class);
					boolean isMethodAnnotation = method.isAnnotationPresent(UserSessionAnnotation.class);
					UserSessionAnnotation userSessionAnnotation = null;
					if(isMethodAnnotation){
						userSessionAnnotation = method.getAnnotation(UserSessionAnnotation.class);
					}else if(isClzAnnotation){
						userSessionAnnotation = clazz.getAnnotation(UserSessionAnnotation.class);
					}
					if(userSessionAnnotation==null || userSessionAnnotation.validate()==false){
						return true;
					}else{
						User user = (User) session.getAttribute(Constants.SessionKey.SESSION_KEY_USER);
						if (user == null){
							System.out.println(request);
							if (HttpUtil.isAjaxRequest(request) || HttpUtil.isIframe(request)) {
								StringBuffer scriptBuf = new StringBuffer();
								scriptBuf.append("top.location.href='").append(HttpUtil.getContextPath(request));
								String userLoginPage = "/login.do";
								scriptBuf.append(userLoginPage).append("'");
								response.setContentType("text/html;charset=utf-8");
								response.getWriter().print("<script>" + scriptBuf.toString() + "</script>");
							}else{
								response.sendRedirect(request.getContextPath() + "/login.do");
								response.setStatus(302);
							}
							
							return false;
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return super.preHandle(request, response, handler);
	}
	
	
}
