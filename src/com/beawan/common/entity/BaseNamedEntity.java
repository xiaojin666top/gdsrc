package com.beawan.common.entity;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import com.platform.util.StringUtil;

@MappedSuperclass
public abstract class BaseNamedEntity extends BaseEntity {
	private static final long serialVersionUID = 4887561926189865246L;

	@Column(name = "name", length = 150)
	protected String name;

	@Column(name = "remark", length = 255)
	protected String remark;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String getDisplayText() {
		if (StringUtil.isBlank(this.displayText)) {
			return this.name;
		} else {
			return this.displayText;
		}
	}
}