package com.beawan.common.entity;

import java.util.Date;
import javax.persistence.MappedSuperclass;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@MappedSuperclass
@JsonIgnoreProperties({ "deleted", "insertTime", "updateTime" })
public abstract class IdentifiableEntity implements Identifiable, java.io.Serializable {
	private static final long serialVersionUID = 2053203288091896576L;

	public abstract String getId();

	public abstract void setId(String id);

	public abstract Date getInsertTime();

	public abstract String getInsertTimeString();

	public abstract void setInsertTime(Date insertTime);

	public abstract Date getUpdateTime();

	public abstract String getUpdateTimeString();

	public abstract void setUpdateTime(Date updateTime);

	public abstract String getDisplayText();

	public abstract void setDisplayText(String displayText);
}