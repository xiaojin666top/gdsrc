package com.beawan.common.entity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.platform.util.StringUtil;

@MappedSuperclass
@JsonIgnoreProperties({ "deleted", "insertTime", "updateTime" })
public abstract class BaseEntity extends IdentifiableEntity {
	private static final long serialVersionUID = 6286789239527402364L;

	protected static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	protected static DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Id
	@Column(name = "id", length = 36)
	protected String id = UUID.randomUUID().toString();
	
	@Column(name = "insertTime")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date insertTime = new Date();

	@Column(name = "inserterName")
	protected String inserterName;

	@Column(name = "inserterId", length = 36)
	protected String inserterId;

	@Column(name = "updateTime")
	@Temporal(TemporalType.TIMESTAMP)
	protected Date updateTime = new Date();

	@Column(name = "updaterName")
	protected String updaterName;

	@Column(name = "updaterId", length = 36)
	protected String updaterId;

	@Transient
	protected String displayText;

	public String getId() {
		if (StringUtil.isBlank(this.id)) {
			this.id = UUID.randomUUID().toString();
		}
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getInsertTime() {
		if (this.insertTime == null) {
			return new Date();
		}
		return this.insertTime;
	}

	public String getInsertTimeString() {
		if (this.insertTime == null) {
			return "";
		}
		return BaseEntity.dateTimeFormat.format(this.insertTime);
	}

	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

	public String getInserterName() {
		return this.inserterName;
	}

	public void setInserterName(String inserterName) {
		this.inserterName = inserterName;
	}

	public String getInserterId() {
		return this.inserterId;
	}

	public void setInserterId(String inserterId) {
		this.inserterId = inserterId;
	}

	public Date getUpdateTime() {
		if (this.updateTime == null) {
			return new Date();
		}
		return this.updateTime;
	}

	public String getUpdateTimeString() {
		if (this.updateTime == null) {
			return "";
		}
		return BaseEntity.dateTimeFormat.format(this.updateTime);
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdaterName() {
		return this.updaterName;
	}

	public void setUpdaterName(String updaterName) {
		this.updaterName = updaterName;
	}

	public String getUpdaterId() {
		return this.updaterId;
	}

	public void setUpdaterId(String updaterId) {
		this.updaterId = updaterId;
	}

	public String getDisplayText() {
		if (StringUtil.isBlank(this.displayText)) {
			return this.getClass().getSimpleName() + "{" + this.id + "}";
		} else {
			return this.displayText;
		}
	}

	public void setDisplayText(String displayText) {
		this.displayText = displayText;
	}
}