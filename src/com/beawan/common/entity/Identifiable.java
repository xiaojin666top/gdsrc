package com.beawan.common.entity;

import java.io.Serializable;

public interface Identifiable extends Serializable {
	public String getId();
}