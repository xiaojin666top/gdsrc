package com.beawan.common.timer;

import org.apache.log4j.Logger;

import com.beawan.common.Constants;
import com.platform.util.JdbcUtil;

/**
 * 序列重置定时器
 */
public class SetSequence2OneTimer {
	
	private static final Logger log = Logger.getLogger(SetSequence2OneTimer.class);
	
	public void setSequence2One(){
		
		String serialNoSql = "ALTER SEQUENCE GDTCESYS.SERIAL_NO_SEQ RESTART WITH 1";
		String mortgageNoSql = "ALTER SEQUENCE GDTCESYS.MORTGAGE_NO_SEQ RESTART WITH 1";
		String tempCusNoSql = "ALTER SEQUENCE GDTCESYS.TEMP_CUS_NO_SEQ RESTART WITH 1";
		String cusFSNoSql = "ALTER SEQUENCE GDTCESYS.CUS_FS_NO_SEQ RESTART WITH 1";
		String reportNoSql = "ALTER SEQUENCE GDTCESYS.REPORT_NO_SEQ RESTART WITH 1";
		
		try {
			
			JdbcUtil.execute(serialNoSql, Constants.DataSource.TCE_DS);
			log.info("序列重置定时器：流水号序列重置设置成功。");
			
			JdbcUtil.execute(mortgageNoSql, Constants.DataSource.TCE_DS);
			log.info("序列重置定时器：押品编号序列重置设置成功。");
			
			JdbcUtil.execute(tempCusNoSql, Constants.DataSource.TCE_DS);
			log.info("序列重置定时器：临时客户号序列重置设置成功。");
			
			JdbcUtil.execute(cusFSNoSql, Constants.DataSource.TCE_DS);
			log.info("序列重置定时器：客户财报记录编号序列重置设置成功。");
			
			JdbcUtil.execute(reportNoSql, Constants.DataSource.TCE_DS);
			log.info("序列重置定时器：报表编号序列重置设置成功。");
			
		} catch (Exception e) {
			log.error("序列重置定时器：序列重置出错！", e);
			e.printStackTrace();
		}
	}
	
}
