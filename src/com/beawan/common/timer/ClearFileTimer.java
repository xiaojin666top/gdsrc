package com.beawan.common.timer;

import java.io.File;

import org.apache.log4j.Logger;

/**
 * 清除缓存文件
 */
public class ClearFileTimer {
	
	private static final Logger log = Logger.getLogger(ClearFileTimer.class);
	
	public void clearFile(){
		
		log.info("清除缓存文件开始");
		
		clearAtcesysTempFile();
		clearPtcesysTempFile();
		
		log.info("清除缓存文件结束");
	}
	
	public void clearAtcesysTempFile(){
		String[] tempPathArr = new String[]{"/appdata/atcesys/temp/", "/appdata/atcesys/image/temp/"};
		
		deleteSubFile(tempPathArr);
	}
	
	public void clearPtcesysTempFile(){
		String[] tempPathArr = new String[]{"/appdata/ptcesys/temp/", "/appdata/ptcesys/image/temp/"};
		
		deleteSubFile(tempPathArr);
	}

	private void deleteSubFile(String[] tempPathArr){
		
		if(tempPathArr == null || tempPathArr.length == 0)
			return;
		
		for(int i=0; i<tempPathArr.length; i++){
			
			File folder = new File(tempPathArr[i]);
			
			if(folder.exists()){
				
				File[] subFiles = folder.listFiles();
				
				if(subFiles != null && subFiles.length > 0){
					for(int j=0; j<subFiles.length; j++){
						subFiles[j].delete();
					}
				}
			}
		}
	}
	
}
