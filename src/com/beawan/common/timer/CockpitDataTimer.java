package com.beawan.common.timer;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.beawan.dtmp.service.DtmpService;
import com.platform.util.DateUtil;

public class CockpitDataTimer {

	private static final Logger log = Logger.getLogger(CockpitDataTimer.class);
	
	@Resource
	private DtmpService dtmpService;
	
	public void syncCockpitData(){
		try{
			System.out.println(DateUtil.getNowDatetime() + "：定时器开始同步驾驶舱数据！");
			log.info(DateUtil.getNowDatetime() + "：定时器开始同步驾驶舱数据！");
			
			dtmpService.syncCockpitData();
			
			log.info(DateUtil.getNowDatetime() + "：定时器同步驾驶舱数据结束！");
			System.out.println(DateUtil.getNowDatetime() + "：定时器同步驾驶舱数据结束！");
		} catch (Exception e) {
			log.error("驾驶舱定时器：同步驾驶舱数据出错！", e);
			e.printStackTrace();
		} 
	}
}
