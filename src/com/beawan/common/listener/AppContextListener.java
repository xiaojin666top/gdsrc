package com.beawan.common.listener;

import java.sql.DriverManager;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.platform.ftp.FtpServerService;

public class AppContextListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		
		try {
			while (DriverManager.getDrivers().hasMoreElements()) {
				DriverManager.deregisterDriver(DriverManager.getDrivers()
						.nextElement());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		FtpServerService.getInstance().stopFtpServer();
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {

		FtpServerService.getInstance().startFtpServer();
	}

}
