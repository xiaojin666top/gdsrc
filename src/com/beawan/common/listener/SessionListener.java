/**
 * Copyright (c) 2014-07 to forever 苏州碧湾信息技术有限公司 All Rights Reserved.  
 *
 * @project mcesys
 * @file_name SessionListener.java
 * @author Rain See
 * @create_time 2016年4月29日 上午10:08:45
 * @last_modified_date 2016年4月29日
 * @version 1.0
 */
package com.beawan.common.listener;

/**
 * @author rain
 * @create_time 2016年4月29日 上午10:08:45
 */

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionListener;
import javax.servlet.http.HttpSessionEvent;

import org.apache.log4j.Logger;

import com.beawan.base.entity.User;
import com.beawan.base.service.IUserSV;
import com.beawan.common.Constants;
import com.beawan.common.ServiceFactory;

public class SessionListener implements HttpSessionListener {
	
	private static final Logger log = Logger.getLogger(SessionListener.class);
	
	public SessionListener() {
		super();
	}

	/* Session创建事件 */
	@Override
	public void sessionCreated(HttpSessionEvent event) {
	}

	/* Session失效事件 */
	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		
		HttpSession session = event.getSession();
		
		User user = (User) session
				.getAttribute(Constants.SessionKey.SESSION_KEY_USER);
		
		if (user != null) {
			/*ServletContext ctx = session.getServletContext();
			@SuppressWarnings("unchecked")
			Map<String, String> loginedUsers = (Map<String, String>) ctx
					.getAttribute(Constants.ApplicationKey.APPLICATION_KEY_LOGINUSER);
			if (loginedUsers != null)
				loginedUsers.remove(user.getLoginName());*/
			
			String logMsg = user.getUserId() + " session destroyed and loginout";
			try {
				IUserSV userSV = (IUserSV) ServiceFactory.getBean("userSV");
				//从PC电脑上退出
				userSV.loginout(user.getUserId(), Constants.Platform.PC);
				log.info(logMsg + " : success");
			} catch (Exception e) {
				log.error(logMsg + " : error", e);
				e.printStackTrace();
			}
		}
		
	}
}
