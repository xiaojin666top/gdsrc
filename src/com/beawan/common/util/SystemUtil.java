package com.beawan.common.util;

import java.util.ArrayList;
import java.util.List;

import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.platform.util.DateUtil;
import com.platform.util.JdbcUtil;
import com.platform.util.StringUtil;

/**
 * 系统公共方法
 * @author beawan_fengjj
 *
 */
public class SystemUtil {
	

	/**
	 * TODO 生成业务流水号 ; 生成规则：系统简称 + 8为日期 + 3位序号
	 * @return
	 * @throws Exception
	 */
	public static String generateSerialNo() throws Exception {

		String date = DateUtil.format(DateUtil.getSystemTime(), Constants.DATE_MASK2);// 8位日期

		String number = genThreeSerialNo();

		String serialNo = Constants.SYS_SHORT_NAME + date + number;

		return serialNo;
	}
	
	/**
	 * TODO 生成三位业务流水序号
	 * @return
	 * @throws Exception
	 */
	public static String genThreeSerialNo() throws Exception {

		//oracle:
		//String sql = "SELECT GDTCESYS." + Constants.DbSequenceName.SERIAL_NO + ".NEXTVAL FROM DUAl";
		//db2:
		String sql = "SELECT next value for GDTCESYS." + Constants.DbSequenceName.SERIAL_NO + " from Sysibm.sysdummy1";
		

		String number = StringUtil.fill(
				JdbcUtil.queryForString(Constants.DataSource.TCE_DS, sql, null),
				'0', 3, true);// 三位序号
		
		return number;
	}

	/**
	 * TODO 生成押品编号 ; 生成规则：系统简称 + “YP” + 8为日期 + 3位序号
	 * @return
	 * @throws Exception
	 */
	public static String generateMortNo() throws Exception {

		String date = DateUtil.format(DateUtil.getSystemTime(), Constants.DATE_MASK2);// 8位日期

		String number = genThreeMortNo();

		String serialNo = Constants.SYS_SHORT_NAME + "YP" + date + number;

		return serialNo;
	}
	
	/**
	 * TODO 生成三位押品序号
	 * @return
	 * @throws Exception
	 */
	public static String genThreeMortNo() throws Exception {

		//oracle:
		//String sql = "SELECT GDTCESYS." + Constants.DbSequenceName.MORTGAGE_NO + ".NEXTVAL FROM DUAl";
		//db2:
		String sql = "SELECT next value for GDTCESYS." + Constants.DbSequenceName.MORTGAGE_NO + " from Sysibm.sysdummy1";
		String number = StringUtil.fill(
				JdbcUtil.queryForString(Constants.DataSource.TCE_DS, sql, null),
				'0', 3, true);// 三位序号
		
		return number;
	}


	/**
	 * TODO 生成业临时客户号 ;生成规则：系统简称  + "TC" + 1位客户类别标识（0：个人客户，1：对公客户） + 8位日期 + 4位序号
	 * 
	 * @param cusType  客户类型
	 * @return
	 * @throws Exception
	 */
	public static String genTempCusNo(String cusType) throws Exception {

		String number = genFourCusSerNo();
		
		String date = DateUtil.format(DateUtil.getSystemTime(), Constants.DATE_MASK2);// 8位日期

		String cusFlag = "1";
		if(SysConstants.CusNoType.PERSON_CUS.equals(cusType))
			cusFlag = "0";

		String serialNo = "TC" + cusFlag + date + number;

		return serialNo;
	}
	
	/**
	 * TODO 生成四位客户序号
	 * @return
	 * @throws Exception
	 */
	public static String genFourCusSerNo() throws Exception {

		//oracle:
		//String sql = "SELECT GDTCESYS." + Constants.DbSequenceName.TEMP_CUS_NO  + ".NEXTVAL FROM DUAl";
		//db2:
		String sql = "SELECT next value for GDTCESYS." + Constants.DbSequenceName.TEMP_CUS_NO + " from Sysibm.sysdummy1";
		String number = StringUtil.fill(
				JdbcUtil.queryForString(Constants.DataSource.TCE_DS, sql, null),'0',
				4, true);// 4位序号
		
		return number;
	}
	
	/**
	 * TODO 生成客户财务报表记录编号 ;生成规则："CFS" + 8位日期 + 6位序号
	 * @return
	 * @throws Exception
	 */
	public static String genCFSRecordNo() throws Exception {

		String number = genSixCFSRecordNo();
		
		String date = DateUtil.format(DateUtil.getSystemTime(), Constants.DATE_MASK2);// 8位日期

		String serialNo = "CFS" + date + number;

		return serialNo;
	}
	
	/**
	 * TODO 生成六位客户财务报表记录编号
	 * @return
	 * @throws Exception
	 */
	public static String genSixCFSRecordNo() throws Exception {

	
		//oracle:
		//String sql = "SELECT GDTCESYS." + Constants.DbSequenceName.CUS_FS_NO  + ".NEXTVAL FROM DUAl";
		//db2:
		String sql = "SELECT next value for GDTCESYS." + Constants.DbSequenceName.CUS_FS_NO + " from Sysibm.sysdummy1";
		
		String number = StringUtil.fill(
				JdbcUtil.queryForString(Constants.DataSource.TCE_DS, sql, null),'0',
				6, true);// 6位序号
		
		return number;
	}
	
	/**
	 * TODO 生成报表编号 ;生成规则：8位日期 + 6位序号
	 * @return
	 * @throws Exception
	 */
	public static String genReportNo() throws Exception {

		String number = genEightReportNo();
		
		String date = DateUtil.format(DateUtil.getSystemTime(), Constants.DATE_MASK2);// 8位日期

		String serialNo = date + number;

		return serialNo;
	}
	
	/**
	 * TODO 生成8位报表编号
	 * @return
	 * @throws Exception
	 */
	public static String genEightReportNo() throws Exception {

		//oracle:
		//String sql = "SELECT GDTCESYS." + Constants.DbSequenceName.REPORT_NO  + ".NEXTVAL FROM DUAl";
		//db2:
		String sql = "SELECT next value for GDTCESYS." + Constants.DbSequenceName.REPORT_NO + " from Sysibm.sysdummy1";
		
		String number = StringUtil.fill(
				JdbcUtil.queryForString(Constants.DataSource.TCE_DS, sql, null),'0',
				8, true);// 6位序号
		
		return number;
	}
	
	/**
	 * TODO 生成财务报表中的日期列表
	 * @param year 年份
	 * @param month 月份
	 * @param lastFlag 是否需要上年同期
	 * @param isShow 是否用于显示
	 * @return
	 */
	public static List<String> genFinaDateList(int year, int month,
			boolean lastFlag, boolean isShow){
		
		List<String> dateList = new ArrayList<String>();
		
		String monthStr = "" + (month<10?"0"+month:month);
		
		if(isShow){
			
			for(int i=3;i>0;i--)
				dateList.add((year-i)+"年");
			
			dateList.add(year + "年" + month + "月");
			if(month != 12 && lastFlag)
				dateList.add((year-1) + "年" + month + "月");
		}else{
			
			for(int i=3;i>0;i--)
				dateList.add((year-i)+"-12");
			
			dateList.add(year + "-" + monthStr);
			if(month != 12 && lastFlag)
				dateList.add((year-1) + "-" + monthStr);
		}
		
		return dateList;
	}
	
	/**
	 * TODO 生成财务报表中的日期列表(半年)
	 * @param year 年份
	 * @param month 月份
	 * @return
	 */
	public static List<String> genFinaDateHalfList(int year, int month){
		
		List<String> dateList = new ArrayList<String>();
		for(int i=5;i>=0;i--) {
			int newmonth=0;
			int newyear=0;
			if ((month-i)<=0) {
				 newmonth=month-i+12;
				 newyear=year-1;
				
				
			}else {
			 newmonth=month-i;
			 newyear=year;
			}
			dateList.add(newyear + "年" + newmonth + "月");
		}
		return dateList;
	}
	
}
