package com.beawan.common.util;

import net.sf.json.JSONObject;

public class WSResult {

	public final static String STATUS_ERROR = "error";
	public final static String STATUS_SUCCESS = "success";

	public String status; // 状态，成功或失败
	public String flag; // 返回标记码，
	public String message; // 返回消息
	public String data; // 返回数据
	
	/*public static WSResult getInstance() {
		return new WSResult();
	}*/

	public WSResult() {
		this.status = WSResult.STATUS_SUCCESS; // 默认返回成功
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String json() {
		return JSONObject.fromObject(this).toString();
	}
}
