package com.beawan.model.entity;

import com.beawan.core.BaseEntity;

import java.util.List;

public class AdmitQuotaEntity extends BaseEntity {
	private int id;
	private String quotaName;
	private String quotaCode;
	private String parentCode;
	private String valType;
	List<AdmitQuotaAttrEntity> options;
	List<AdmitQuotaEntity> subQuotaDtos;
	List<AdmitQuotaAttrEntity> actulOptions;
	
	
	public List<AdmitQuotaAttrEntity> getActulOptions() {
		return actulOptions;
	}

	public void setActulOptions(List<AdmitQuotaAttrEntity> actulOptions) {
		this.actulOptions = actulOptions;
	}

	public List<AdmitQuotaAttrEntity> getOptions() {
		return options;
	}

	public void setOptions(List<AdmitQuotaAttrEntity> options) {
		this.options = options;
	}

	public List<AdmitQuotaEntity> getSubQuotaDtos() {
		return subQuotaDtos;
	}

	public void setSubQuotaDtos(List<AdmitQuotaEntity> subQuotaDtos) {
		this.subQuotaDtos = subQuotaDtos;
	}

	public AdmitQuotaEntity() {
		super();
	}

	public AdmitQuotaEntity(int id, String quotaName, String quotaCode, String parentCode, String valType) {
		super();
		this.id = id;
		this.quotaName = quotaName;
		this.quotaCode = quotaCode;
		this.parentCode = parentCode;
		this.valType = valType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getQuotaName() {
		return quotaName;
	}

	public void setQuotaName(String quotaName) {
		this.quotaName = quotaName;
	}

	public String getQuotaCode() {
		return quotaCode;
	}

	public void setQuotaCode(String quotaCode) {
		this.quotaCode = quotaCode;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

	public String getValType() {
		return valType;
	}

	public void setValType(String valType) {
		this.valType = valType;
	}
	
	

}
