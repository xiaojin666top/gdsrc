package com.beawan.model.entity;

public class LrAdjustItemEntity {

	private Integer id;
	private String name;// 调整项名称

	private String fieldType;// 调整项值类型（数值型：number，字符串型：string）

	private String conitionType;// 条件类型（区间：interval，固定值：fixed）

	private String order;// 排序类型（降序：desc，升序：asc）

	private String state;// 使用状态
	private int leadId;// 关联定价模型库

	public LrAdjustItemEntity() {
		super();
	}

	public LrAdjustItemEntity(Integer id, String name, String fieldType, String conitionType, String order,
			String state, int leadId) {
		super();
		this.id = id;
		this.name = name;
		this.fieldType = fieldType;
		this.conitionType = conitionType;
		this.order = order;
		this.state = state;
		this.leadId = leadId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public String getConitionType() {
		return conitionType;
	}

	public void setConitionType(String conitionType) {
		this.conitionType = conitionType;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getLeadId() {
		return leadId;
	}

	public void setLeadId(int leadId) {
		this.leadId = leadId;
	}

	@Override
	public String toString() {
		return "LrAdjustItemEntity [id=" + id + ", name=" + name + ", fieldType=" + fieldType + ", conitionType="
				+ conitionType + ", order=" + order + ", state=" + state + ", leadId=" + leadId + "]";
	}

}
