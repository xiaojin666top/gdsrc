package com.beawan.model.entity;

import java.sql.Timestamp;

//定价模型库
public class LrLibEntity {

	private int id;
	private String name;
	private int status;
	private String identifCode;
	private Timestamp createTime;
	private String fileName;
	private Double baseRate;

	public LrLibEntity() {
		super();
	}

	public LrLibEntity(int id, String name, int status, String identifCode, Timestamp createTime, String fileName,
			Double baseRate) {
		super();
		this.id = id;
		this.name = name;
		this.status = status;
		this.identifCode = identifCode;
		this.createTime = createTime;
		this.fileName = fileName;
		this.baseRate = baseRate;
	}

	public Double getBaseRate() {
		return baseRate;
	}

	public void setBaseRate(Double baseRate) {
		this.baseRate = baseRate;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getIdentifCode() {
		return identifCode;
	}

	public void setIdentifCode(String identifCode) {
		this.identifCode = identifCode;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

}
