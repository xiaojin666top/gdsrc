package com.beawan.model.entity;

public class AdjustItemEntity {
    private Long id;
    private String itemName;  //参数名称
    private String fieldType; //参数值类型，（0是数字，1是百分比）
    private String  conitionType;//条件类型 （S是固定值，v是变量值）
    private Long modelId;//关联的模型库id
    
    
	public AdjustItemEntity() {
		super();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getFieldType() {
		return fieldType;
	}
	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}
	public String getConitionType() {
		return conitionType;
	}
	public void setConitionType(String conitionType) {
		this.conitionType = conitionType;
	}
	public Long getModelId() {
		return modelId;
	}
	public void setModelId(Long modelId) {
		this.modelId = modelId;
	}
	public AdjustItemEntity(Long id, String itemName, String fieldType, String conitionType, Long modelId) {
		super();
		this.id = id;
		this.itemName = itemName;
		this.fieldType = fieldType;
		this.conitionType = conitionType;
		this.modelId = modelId;
	}
	@Override
	public String toString() {
		return "AdjustItemEntity [id=" + id + ", itemName=" + itemName + ", fieldType=" + fieldType + ", conitionType="
				+ conitionType + ", modelId=" + modelId + "]";
	}
	
	
	
}
