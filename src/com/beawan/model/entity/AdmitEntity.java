package com.beawan.model.entity;

import com.beawan.core.BaseEntity;

import java.sql.Timestamp;
import java.util.List;

public class AdmitEntity extends BaseEntity {

	private int id;
	private String name;
	private int state;
	private String identifCode;
	private Integer totalScore;
	
	List<AdmitQuotaEntity> quotaList;
	
	public List<AdmitQuotaEntity> getQuotaList() {
		return quotaList;
	}
	public void setQuotaList(List<AdmitQuotaEntity> quotaList) {
		this.quotaList = quotaList;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getIdentifCode() {
		return identifCode;
	}
	public void setIdentifCode(String identifCode) {
		this.identifCode = identifCode;
	}
	public Integer getTotalScore() {
		return totalScore;
	}
	public void setTotalScore(Integer totalScore) {
		this.totalScore = totalScore;
	}
	
	
}
