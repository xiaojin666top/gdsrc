package com.beawan.model.entity;

import java.sql.Timestamp;

public class LimitEntity {
	private Long id;
	private String name;//额度库名称
	private String identifCode;//标识号
	private Timestamp createTime;
	private int state;//状态
	private String framework;//架构方式
	private String accountWay;//计算方式
	private String description;//其他描述
	private Double baseRatio;//基础系数

	public LimitEntity() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdentifCode() {
		return identifCode;
	}

	public void setIdentifCode(String identifCode) {
		this.identifCode = identifCode;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getFramework() {
		return framework;
	}

	public void setFramework(String framework) {
		this.framework = framework;
	}

	public String getAccountWay() {
		return accountWay;
	}

	public void setAccountWay(String accountWay) {
		this.accountWay = accountWay;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getBaseRatio() {
		return baseRatio;
	}

	public void setBaseRatio(Double baseRatio) {
		this.baseRatio = baseRatio;
	}

	public LimitEntity(Long id, String name, String identifCode, Timestamp createTime, int state, String framework,
			String accountWay, String description, Double baseRatio) {
		super();
		this.id = id;
		this.name = name;
		this.identifCode = identifCode;
		this.createTime = createTime;
		this.state = state;
		this.framework = framework;
		this.accountWay = accountWay;
		this.description = description;
		this.baseRatio = baseRatio;
	}

	@Override
	public String toString() {
		return "LimitEntity [id=" + id + ", name=" + name + ", identifCode=" + identifCode + ", createTime="
				+ createTime + ", state=" + state + ", framework=" + framework + ", accountWay=" + accountWay
				+ ", description=" + description + ", baseRatio=" + baseRatio + "]";
	}

	
	
	
}
