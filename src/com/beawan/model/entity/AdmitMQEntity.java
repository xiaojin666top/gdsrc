package com.beawan.model.entity;

import com.beawan.core.BaseEntity;

/**
 * 准入模型和指标关联表
 * */

public class AdmitMQEntity extends BaseEntity {
	private int id;
	private String identifCode;
	private String quotaCode;
	private String parentCode;
	private int attrId;
	private String optionCode;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIdentifCode() {
		return identifCode;
	}
	public void setIdentifCode(String identifCode) {
		this.identifCode = identifCode;
	}
	public String getQuotaCode() {
		return quotaCode;
	}
	public void setQuotaCode(String quotaCode) {
		this.quotaCode = quotaCode;
	}
	public String getParentCode() {
		return parentCode;
	}
	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}
	public int getAttrId() {
		return attrId;
	}
	public void setAttrId(int attrId) {
		this.attrId = attrId;
	}
	public String getOptionCode() {
		return optionCode;
	}
	public void setOptionCode(String optionCode) {
		this.optionCode = optionCode;
	}

}
