package com.beawan.model.entity;

import com.beawan.core.BaseEntity;

import java.math.BigDecimal;

public class AdmitQuotaAttrEntity extends BaseEntity {
	private int id;
	private String quotaCode;
	private String content;
	private BigDecimal upperReg;
	private BigDecimal lowerReg;
	private String regMould;//闭型
	private String optionCode;//准入类型

	public AdmitQuotaAttrEntity(int id, String quotaCode, String content, BigDecimal upperReg, BigDecimal lowerReg,
			String regMould, String optionCode) {
		super();
		this.id = id;
		this.quotaCode = quotaCode;
		this.content = content;
		this.upperReg = upperReg;
		this.lowerReg = lowerReg;
		this.regMould = regMould;
		this.optionCode = optionCode;
	}
	public AdmitQuotaAttrEntity() {
		super();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getQuotaCode() {
		return quotaCode;
	}
	public void setQuotaCode(String quotaCode) {
		this.quotaCode = quotaCode;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public BigDecimal getUpperReg() {
		return upperReg;
	}
	public void setUpperReg(BigDecimal upperReg) {
		this.upperReg = upperReg;
	}
	public BigDecimal getLowerReg() {
		return lowerReg;
	}
	public void setLowerReg(BigDecimal lowerReg) {
		this.lowerReg = lowerReg;
	}
	public String getRegMould() {
		return regMould;
	}
	public void setRegMould(String regMould) {
		this.regMould = regMould;
	}
	public String getOptionCode() {
		return optionCode;
	}
	public void setOptionCode(String optionCode) {
		this.optionCode = optionCode;
	}
	
	
}
