package com.beawan.model.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 *
 * 准入分析结果表
 */

@Entity
@Table(name = "MD_ADMIT_RESULT",schema = "GDTCESYS")
public class AdmitResult extends BaseEntity {

    @Id
  //  @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="MD_ADMIT_RESULT_SEQ")
    //@SequenceGenerator(name="MD_ADMIT_RESULT_SEQ",allocationSize=1,initialValue=1, sequenceName="MD_ADMIT_RESULT_SEQ")
    private Integer id;


    @Column(name = "CUST_NO")
    private String custNo;//客户号
    @Column(name = "TIMES")
    private Integer times;//准入分析 结果次数 --》每次取最大的
    @Column(name = "QUOTA_NAME")
    private String quotaName;//准入项名称
    @Column(name = "REAL_DESC")
    private String realDesc;//实际情况描述
    @Column(name = "RESULT")
    private String result;//准入结果描述

    public String getCustNo() {
        return custNo;
    }

    public void setCustNo(String custNo) {
        this.custNo = custNo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }

    public String getQuotaName() {
        return quotaName;
    }

    public void setQuotaName(String quotaName) {
        this.quotaName = quotaName;
    }

    public String getRealDesc() {
        return realDesc;
    }

    public void setRealDesc(String realDesc) {
        this.realDesc = realDesc;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
