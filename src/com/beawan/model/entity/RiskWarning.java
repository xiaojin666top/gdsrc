package com.beawan.model.entity;

/**
 *     风险预警实体类
 * @author yuzhejia
 *
 */
public class RiskWarning {
	private Long id;
	private String warnName;
	private String riskCode;
	private String riskRule;
	private Integer riskType;
	private Integer riskBasics;
	private String riskLevel;
	private Integer status;
	private String updateTime;
	private Long updater;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRiskCode() {
		return riskCode;
	}
	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}
	public String getRiskRule() {
		return riskRule;
	}
	public void setRiskRule(String riskRule) {
		this.riskRule = riskRule;
	}
	public Integer getRiskType() {
		return riskType;
	}
	public void setRiskType(Integer riskType) {
		this.riskType = riskType;
	}
	public Integer getRiskBasics() {
		return riskBasics;
	}
	public void setRiskBasics(Integer riskBasics) {
		this.riskBasics = riskBasics;
	}
	
	public String getWarnName() {
		return warnName;
	}
	public void setWarnName(String warnName) {
		this.warnName = warnName;
	}
	public String getRiskLevel() {
		return riskLevel;
	}
	public void setRiskLevel(String riskLevel) {
		this.riskLevel = riskLevel;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public Long getUpdater() {
		return updater;
	}
	public void setUpdater(Long updater) {
		this.updater = updater;
	}
	
	
}
