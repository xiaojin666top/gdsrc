package com.beawan.model.entity;

public class DimensionEntity {
	private Long id;
	private String name;
	
	private Double value;
	private Double downValue;
	private Double upValue;
	private Long itemId;

	public DimensionEntity() {
		// TODO Auto-generated constructor stub
	}

	public DimensionEntity(Long id, String name, Double value, Double downValue, Double upValue, Long itemId) {
		super();
		this.id = id;
		this.name = name;
		this.value = value;
		this.downValue = downValue;
		this.upValue = upValue;
		this.itemId = itemId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public Double getDownValue() {
		return downValue;
	}

	public void setDownValue(Double downValue) {
		this.downValue = downValue;
	}

	public Double getUpValue() {
		return upValue;
	}

	public void setUpValue(Double upValue) {
		this.upValue = upValue;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}

	@Override
	public String toString() {
		return "DimensionEntity [id=" + id + ", name=" + name + ", value=" + value + ", downValue=" + downValue
				+ ", upValue=" + upValue + ", itemId=" + itemId + "]";
	}


	
}
