package com.beawan.model.entity;

public class AdmitTemplateEntity {

	private int id;
	private String name;//名称
	private int deep;//层数 从1开始
	private int parentId;//0是初始节点
	private int type;//类型 1：系统刚空 2：风险提醒
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getDeep() {
		return deep;
	}
	public void setDeep(int deep) {
		this.deep = deep;
	}
	public int getParentId() {
		return parentId;
	}
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	
	
}
