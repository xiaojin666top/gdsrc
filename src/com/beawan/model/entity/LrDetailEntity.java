package com.beawan.model.entity;


public class LrDetailEntity {

	private Integer id;//主键
	private int reocrdId;
	private int dimentsionId;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public int getReocrdId() {
		return reocrdId;
	}
	public void setReocrdId(int reocrdId) {
		this.reocrdId = reocrdId;
	}
	public int getDimentsionId() {
		return dimentsionId;
	}
	public void setDimentsionId(int dimentsionId) {
		this.dimentsionId = dimentsionId;
	}
	
	
}
