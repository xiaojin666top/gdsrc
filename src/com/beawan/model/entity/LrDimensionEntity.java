package com.beawan.model.entity;

public class LrDimensionEntity {
	private Integer id;//主键
	private String name;//

	private String fixedValue;

	private Double upValue;

	private Double downValue;

	private Double flowValue;

	private Integer creditMonths;

	private Integer creditOverdueNums;

	private Integer creditOverdueDays;

	private Integer lrAdjustItemId;// 关联具体调整项

	public LrDimensionEntity() {
		super();
	}

	public LrDimensionEntity(Integer id, String name, String fixedValue, Double upValue, Double downValue,
			Double flowValue, Integer creditMonths, Integer creditOverdueNums, Integer creditOverdueDays,
			Integer lrAdjustItemId) {
		super();
		this.id = id;
		this.name = name;
		this.fixedValue = fixedValue;
		this.upValue = upValue;
		this.downValue = downValue;
		this.flowValue = flowValue;
		this.creditMonths = creditMonths;
		this.creditOverdueNums = creditOverdueNums;
		this.creditOverdueDays = creditOverdueDays;
		this.lrAdjustItemId = lrAdjustItemId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFixedValue() {
		return fixedValue;
	}

	public void setFixedValue(String fixedValue) {
		this.fixedValue = fixedValue;
	}

	public Double getUpValue() {
		return upValue;
	}

	public void setUpValue(Double upValue) {
		this.upValue = upValue;
	}

	public Double getDownValue() {
		return downValue;
	}

	public void setDownValue(Double downValue) {
		this.downValue = downValue;
	}

	public Double getFlowValue() {
		return flowValue;
	}

	public void setFlowValue(Double flowValue) {
		this.flowValue = flowValue;
	}

	public Integer getCreditMonths() {
		return creditMonths;
	}

	public void setCreditMonths(Integer creditMonths) {
		this.creditMonths = creditMonths;
	}

	public Integer getCreditOverdueNums() {
		return creditOverdueNums;
	}

	public void setCreditOverdueNums(Integer creditOverdueNums) {
		this.creditOverdueNums = creditOverdueNums;
	}

	public Integer getCreditOverdueDays() {
		return creditOverdueDays;
	}

	public void setCreditOverdueDays(Integer creditOverdueDays) {
		this.creditOverdueDays = creditOverdueDays;
	}

	public Integer getLrAdjustItemId() {
		return lrAdjustItemId;
	}

	public void setLrAdjustItemId(Integer lrAdjustItemId) {
		this.lrAdjustItemId = lrAdjustItemId;
	}

	
	




}
