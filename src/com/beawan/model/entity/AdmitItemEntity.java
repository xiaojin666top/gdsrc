package com.beawan.model.entity;

import java.sql.Timestamp;

public class AdmitItemEntity {

	private int id;
	private int admitId;
	private int templateId;
	private Double weight;//权重 百分比制
	private int score;
	private Timestamp createTime;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAdmitId() {
		return admitId;
	}
	public void setAdmitId(int admitId) {
		this.admitId = admitId;
	}
	public int getTemplateId() {
		return templateId;
	}
	public void setTemplateId(int templateId) {
		this.templateId = templateId;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	
	
	
	
	
	
	
	
}
