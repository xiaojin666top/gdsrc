package com.beawan.model.entity;

import java.sql.Timestamp;

public class LrRecordEntity {

	private Integer id;//主键
	private String custName;
	private Double resultRate;
	private Timestamp createTime;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public Double getResultRate() {
		return resultRate;
	}
	public void setResultRate(Double resultRate) {
		this.resultRate = resultRate;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	
	
}
