package com.beawan.model.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.common.Constants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.Pagination;
import com.beawan.core.ResultDto;
import com.beawan.model.entity.RiskWarning;
import com.beawan.model.service.RiskWarningService;
import com.platform.util.DateUtil;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/model/risk" })
public class RiskWarningCtl extends BaseController {
	private static Logger log = Logger.getLogger(RiskWarningCtl.class);

	@Resource
	private RiskWarningService riskWarningService;

	/* 跳转模型库列表 */
	@RequestMapping("list.do")
	public String list(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return "views/model/LrModel/leadRate";
	}

	@RequestMapping("saveRiskWarning.json")
	@ResponseBody
	public ResultDto saveRiskWarning() {
		ResultDto re = returnFail("");
		try {
			RiskWarning ICGG001 = new RiskWarning();
			ICGG001.setRiskCode("ICGG001");
			ICGG001.setRiskRule("近三年高管变化率超过 50%");
			ICGG001.setRiskType(Constants.Risk.IC);
			ICGG001.setRiskBasics(Constants.Risk.CLOUD_DATA);
			ICGG001.setRiskLevel(Constants.Risk.LEVEL_1);
			ICGG001.setStatus(Constants.Risk.NOT_ACTIVE);
			ICGG001.setUpdateTime(DateUtil.getNow());

			RiskWarning ICGG002 = new RiskWarning();
			ICGG002.setRiskCode("ICGG002");
			ICGG002.setRiskRule("近一年之内高管变化率超过 30%");
			ICGG002.setRiskType(Constants.Risk.IC);
			ICGG002.setRiskBasics(Constants.Risk.CLOUD_DATA);
			ICGG002.setRiskLevel(Constants.Risk.LEVEL_1);
			ICGG002.setStatus(Constants.Risk.NOT_ACTIVE);
			ICGG002.setUpdateTime(DateUtil.getNow());

			RiskWarning ICGG003 = new RiskWarning();
			ICGG003.setRiskCode("ICGG003");
			ICGG003.setRiskRule("法定代表人近一年内出现变更");
			ICGG003.setRiskType(Constants.Risk.IC);
			ICGG003.setRiskBasics(Constants.Risk.CLOUD_DATA);
			ICGG003.setRiskLevel(Constants.Risk.LEVEL_1);
			ICGG003.setStatus(Constants.Risk.NOT_ACTIVE);
			ICGG003.setUpdateTime(DateUtil.getNow());

			RiskWarning ICGG004 = new RiskWarning();
			ICGG004.setRiskCode("ICGG004");
			ICGG004.setRiskRule("股权最高方近一年内出现在新增名单中");
			ICGG004.setRiskType(Constants.Risk.IC);
			ICGG004.setRiskBasics(Constants.Risk.CLOUD_DATA);
			ICGG004.setRiskLevel(Constants.Risk.LEVEL_1);
			ICGG004.setStatus(Constants.Risk.NOT_ACTIVE);
			ICGG004.setUpdateTime(DateUtil.getNow());
			
			//开始水电预警
			RiskWarning SDYJ001 = new RiskWarning();
			SDYJ001.setRiskCode("SDYJ001");
			SDYJ001.setRiskRule("企业电费同比下降");
			SDYJ001.setRiskType(Constants.Risk.WATER_ELECT);
			SDYJ001.setRiskBasics(Constants.Risk.SURVEY_COLLECT);
			SDYJ001.setRiskLevel(Constants.Risk.LEVEL_1 + "," + Constants.Risk.LEVEL_2 + "," + Constants.Risk.LEVEL_3);
			SDYJ001.setStatus(Constants.Risk.NOT_ACTIVE);
			SDYJ001.setUpdateTime(DateUtil.getNow());

			RiskWarning SDYJ002 = new RiskWarning();
			SDYJ002.setRiskCode("SDYJ002");
			SDYJ002.setRiskRule("企业电费环比下降");
			SDYJ002.setRiskType(Constants.Risk.WATER_ELECT);
			SDYJ002.setRiskBasics(Constants.Risk.SURVEY_COLLECT);
			SDYJ002.setRiskLevel(Constants.Risk.LEVEL_1 + "," + Constants.Risk.LEVEL_2 + "," + Constants.Risk.LEVEL_3);
			SDYJ002.setStatus(Constants.Risk.NOT_ACTIVE);
			SDYJ002.setUpdateTime(DateUtil.getNow());

			RiskWarning SDYJ003 = new RiskWarning();
			SDYJ003.setRiskCode("SDYJ003");
			SDYJ003.setRiskRule("企业电费下降，销售收入大幅上升");
			SDYJ003.setRiskType(Constants.Risk.WATER_ELECT);
			SDYJ003.setRiskBasics(Constants.Risk.SURVEY_COLLECT);
			SDYJ003.setRiskLevel(Constants.Risk.LEVEL_1 + "," + Constants.Risk.LEVEL_2 + "," + Constants.Risk.LEVEL_3);
			SDYJ003.setStatus(Constants.Risk.NOT_ACTIVE);
			SDYJ003.setUpdateTime(DateUtil.getNow());

			RiskWarning SDYJ004 = new RiskWarning();
			SDYJ004.setRiskCode("SDYJ004");
			SDYJ004.setRiskRule("企业电费与销售收入均下降");
			SDYJ004.setRiskType(Constants.Risk.WATER_ELECT);
			SDYJ004.setRiskBasics(Constants.Risk.SURVEY_COLLECT);
			SDYJ004.setRiskLevel(Constants.Risk.LEVEL_3);
			SDYJ004.setStatus(Constants.Risk.NOT_ACTIVE);
			SDYJ004.setUpdateTime(DateUtil.getNow());

			RiskWarning SDYJ005 = new RiskWarning();
			SDYJ005.setRiskCode("SDYJ005");
			SDYJ005.setRiskRule("企业电费大幅上升，销售收入大幅减少");
			SDYJ005.setRiskType(Constants.Risk.WATER_ELECT);
			SDYJ005.setRiskBasics(Constants.Risk.SURVEY_COLLECT);
			SDYJ005.setRiskLevel(Constants.Risk.LEVEL_1 + "," + Constants.Risk.LEVEL_2 + "," + Constants.Risk.LEVEL_3);
			SDYJ005.setStatus(Constants.Risk.NOT_ACTIVE);
			SDYJ005.setUpdateTime(DateUtil.getNow());

			RiskWarning SDYJ006 = new RiskWarning();
			SDYJ006.setRiskCode("SDYJ006");
			SDYJ006.setRiskRule("企业每万元产值能耗低于行业均值");
			SDYJ006.setRiskType(Constants.Risk.WATER_ELECT);
			SDYJ006.setRiskBasics(Constants.Risk.SURVEY_COLLECT);
			SDYJ006.setRiskLevel(Constants.Risk.LEVEL_1 + "," + Constants.Risk.LEVEL_2 + "," + Constants.Risk.LEVEL_3);
			SDYJ006.setStatus(Constants.Risk.NOT_ACTIVE);
			SDYJ006.setUpdateTime(DateUtil.getNow());

			RiskWarning SDYJ007 = new RiskWarning();
			SDYJ007.setRiskCode("SDYJ007");
			SDYJ007.setRiskRule("企业每万元产值能耗高于行业均值");
			SDYJ007.setRiskType(Constants.Risk.WATER_ELECT);
			SDYJ007.setRiskBasics(Constants.Risk.SURVEY_COLLECT);
			SDYJ007.setRiskLevel(Constants.Risk.LEVEL_1 + "," + Constants.Risk.LEVEL_2 + "," + Constants.Risk.LEVEL_3);
			SDYJ007.setStatus(Constants.Risk.NOT_ACTIVE);
			SDYJ007.setUpdateTime(DateUtil.getNow());
			
			//开始征信预警信号
			RiskWarning ZXYJ001 = new RiskWarning();
			ZXYJ001.setRiskCode("ZXYJ001");
			ZXYJ001.setRiskRule("近三个月内被超过 3 家以上信贷机构查询");
			ZXYJ001.setRiskType(Constants.Risk.REFERENCE);
			ZXYJ001.setRiskBasics(Constants.Risk.CREDIT_REPORT);
			ZXYJ001.setRiskLevel(Constants.Risk.LEVEL_1);
			ZXYJ001.setStatus(Constants.Risk.NOT_ACTIVE);
			ZXYJ001.setUpdateTime(DateUtil.getNow());

			RiskWarning ZXYJ002 = new RiskWarning();
			ZXYJ002.setRiskCode("ZXYJ002");
			ZXYJ002.setRiskRule("近三个月内被超过 6家以上信贷机构查询");
			ZXYJ002.setRiskType(Constants.Risk.REFERENCE);
			ZXYJ002.setRiskBasics(Constants.Risk.CREDIT_REPORT);
			ZXYJ002.setRiskLevel(Constants.Risk.LEVEL_2);
			ZXYJ002.setStatus(Constants.Risk.NOT_ACTIVE);
			ZXYJ002.setUpdateTime(DateUtil.getNow());

			RiskWarning ZXYJ003 = new RiskWarning();
			ZXYJ003.setRiskCode("ZXYJ003");
			ZXYJ003.setRiskRule("近三个月内被超过 9家以上信贷机构查询");
			ZXYJ003.setRiskType(Constants.Risk.REFERENCE);
			ZXYJ003.setRiskBasics(Constants.Risk.CREDIT_REPORT);
			ZXYJ003.setRiskLevel(Constants.Risk.LEVEL_3);
			ZXYJ003.setStatus(Constants.Risk.NOT_ACTIVE);
			ZXYJ003.setUpdateTime(DateUtil.getNow());

			RiskWarning ZXYJ004 = new RiskWarning();
			ZXYJ004.setRiskCode("ZXYJ004");
			ZXYJ004.setRiskRule("融资银行超过 3 家");
			ZXYJ004.setRiskType(Constants.Risk.REFERENCE);
			ZXYJ004.setRiskBasics(Constants.Risk.CREDIT_REPORT);
			ZXYJ004.setRiskLevel(Constants.Risk.LEVEL_1);
			ZXYJ004.setStatus(Constants.Risk.NOT_ACTIVE);
			ZXYJ004.setUpdateTime(DateUtil.getNow());

			RiskWarning ZXYJ005 = new RiskWarning();
			ZXYJ005.setRiskCode("ZXYJ005");
			ZXYJ005.setRiskRule("融资银行超过 6 家");
			ZXYJ005.setRiskType(Constants.Risk.REFERENCE);
			ZXYJ005.setRiskBasics(Constants.Risk.CREDIT_REPORT);
			ZXYJ005.setRiskLevel(Constants.Risk.LEVEL_2);
			ZXYJ005.setStatus(Constants.Risk.NOT_ACTIVE);
			ZXYJ005.setUpdateTime(DateUtil.getNow());

			RiskWarning ZXYJ006 = new RiskWarning();
			ZXYJ006.setRiskCode("ZXYJ006");
			ZXYJ006.setRiskRule("融资银行超过 9 家");
			ZXYJ006.setRiskType(Constants.Risk.REFERENCE);
			ZXYJ006.setRiskBasics(Constants.Risk.CREDIT_REPORT);
			ZXYJ006.setRiskLevel(Constants.Risk.LEVEL_3);
			ZXYJ006.setStatus(Constants.Risk.NOT_ACTIVE);
			ZXYJ006.setUpdateTime(DateUtil.getNow());

			RiskWarning ZXYJ007 = new RiskWarning();
			ZXYJ007.setRiskCode("ZXYJ007");
			ZXYJ007.setRiskRule("贷款余额总数大于经营活动现金流入");
			ZXYJ007.setRiskType(Constants.Risk.REFERENCE);
			ZXYJ007.setRiskBasics(Constants.Risk.CREDIT_REPORT);
			ZXYJ007.setRiskLevel(Constants.Risk.LEVEL_1);
			ZXYJ007.setStatus(Constants.Risk.NOT_ACTIVE);
			ZXYJ007.setUpdateTime(DateUtil.getNow());

			RiskWarning ZXYJ008 = new RiskWarning();
			ZXYJ008.setRiskCode("ZXYJ008");
			ZXYJ008.setRiskRule("担保金额超过企业净资产");
			ZXYJ008.setRiskType(Constants.Risk.REFERENCE);
			ZXYJ008.setRiskBasics(Constants.Risk.CREDIT_REPORT);
			ZXYJ008.setRiskLevel(Constants.Risk.LEVEL_1);
			ZXYJ008.setStatus(Constants.Risk.NOT_ACTIVE);
			ZXYJ008.setUpdateTime(DateUtil.getNow());

			//这个应该放到准入中去
			RiskWarning ZXYJ009 = new RiskWarning();
			ZXYJ009.setRiskCode("ZXYJ009");
			ZXYJ009.setRiskRule("关联企业相互担保");
			ZXYJ009.setRiskType(Constants.Risk.REFERENCE);
			ZXYJ009.setRiskBasics(Constants.Risk.CREDIT_REPORT);
			ZXYJ009.setRiskLevel(Constants.Risk.LEVEL_4);
			ZXYJ009.setStatus(Constants.Risk.NOT_ACTIVE);
			ZXYJ009.setUpdateTime(DateUtil.getNow());

			RiskWarning ZXYJ010 = new RiskWarning();
			ZXYJ010.setRiskCode("ZXYJ010");
			ZXYJ010.setRiskRule("短期借款+长期借款+应付票据与融资余额不匹配");
			ZXYJ010.setRiskType(Constants.Risk.REFERENCE);
			ZXYJ010.setRiskBasics(Constants.Risk.CREDIT_REPORT);
			ZXYJ010.setRiskLevel(Constants.Risk.LEVEL_1);
			ZXYJ010.setStatus(Constants.Risk.NOT_ACTIVE);
			ZXYJ010.setUpdateTime(DateUtil.getNow());
			
			//司法异常预警
			RiskWarning SFYJ001 = new RiskWarning();
			SFYJ001.setRiskCode("SFYJ001");
			SFYJ001.setRiskRule("近五年内发生借贷纠纷");
			SFYJ001.setRiskType(Constants.Risk.JUDICIAL);
			SFYJ001.setRiskBasics(Constants.Risk.CLOUD_DATA);
			SFYJ001.setRiskLevel(Constants.Risk.LEVEL_3);
			SFYJ001.setStatus(Constants.Risk.NOT_ACTIVE);
			SFYJ001.setUpdateTime(DateUtil.getNow());

			RiskWarning SFYJ002 = new RiskWarning();
			SFYJ002.setRiskCode("SFYJ002");
			SFYJ002.setRiskRule("近 1 年股权转让纠纷");
			SFYJ002.setRiskType(Constants.Risk.JUDICIAL);
			SFYJ002.setRiskBasics(Constants.Risk.CLOUD_DATA);
			SFYJ002.setRiskLevel(Constants.Risk.LEVEL_1+ ","+Constants.Risk.LEVEL_2+","+Constants.Risk.LEVEL_3);
			SFYJ002.setStatus(Constants.Risk.NOT_ACTIVE);
			SFYJ002.setUpdateTime(DateUtil.getNow());

			RiskWarning SFYJ003 = new RiskWarning();
			SFYJ003.setRiskCode("SFYJ003");
			SFYJ003.setRiskRule("近 1 年失信被执行人次数");
			SFYJ003.setRiskType(Constants.Risk.JUDICIAL);
			SFYJ003.setRiskBasics(Constants.Risk.CLOUD_DATA);
			SFYJ003.setRiskLevel(Constants.Risk.LEVEL_4);
			SFYJ003.setStatus(Constants.Risk.NOT_ACTIVE);
			SFYJ003.setUpdateTime(DateUtil.getNow());
			
			riskWarningService.saveOrUpdate(ICGG001);
			riskWarningService.saveOrUpdate(ICGG002);
			riskWarningService.saveOrUpdate(ICGG003);
			riskWarningService.saveOrUpdate(ICGG004);
			riskWarningService.saveOrUpdate(SFYJ001);
			riskWarningService.saveOrUpdate(SFYJ002);
			riskWarningService.saveOrUpdate(SFYJ003);
			riskWarningService.saveOrUpdate(SDYJ001);
			riskWarningService.saveOrUpdate(SDYJ002);
			riskWarningService.saveOrUpdate(SDYJ003);
			riskWarningService.saveOrUpdate(SDYJ004);
			riskWarningService.saveOrUpdate(SDYJ005);
			riskWarningService.saveOrUpdate(SDYJ006);
			riskWarningService.saveOrUpdate(SDYJ007);
			riskWarningService.saveOrUpdate(ZXYJ001);
			riskWarningService.saveOrUpdate(ZXYJ002);
			riskWarningService.saveOrUpdate(ZXYJ003);
			riskWarningService.saveOrUpdate(ZXYJ004);
			riskWarningService.saveOrUpdate(ZXYJ005);
			riskWarningService.saveOrUpdate(ZXYJ006);
			riskWarningService.saveOrUpdate(ZXYJ007);
			riskWarningService.saveOrUpdate(ZXYJ008);
			riskWarningService.saveOrUpdate(ZXYJ009);
			riskWarningService.saveOrUpdate(ZXYJ010);
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	@RequestMapping("viewRkWarn.do")
	public ModelAndView viewRkWarn(Integer riskType) {
		ModelAndView mav = new ModelAndView("views/model/risk/riskwarnList");
		mav.addObject("riskType", riskType);
		return mav;
	}
	
	/**
	 * 获得根据风险预警类型  获得预警模型列表
	 * 
	 * @return
	 */
	@RequestMapping("listRkWarn.json")
	@ResponseBody
	public ResultDto listRkWarn(@RequestParam(defaultValue = "1", value = "page") int page,
			@RequestParam(defaultValue = "10", value = "rows") int rows, Integer riskType) {
		ResultDto re = returnFail("");
		try {
			System.out.println(riskType);
			Pagination<RiskWarning> pager = riskWarningService.getPagerByRiskType(page, rows, riskType);
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount()); 	 
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("风险模型列表获取成功");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(re.getMsg(), e);
		}

		return re;
	}
	
	@RequestMapping("changeStatus.json")
	@ResponseBody
	public ResultDto changeStatus(long id, int status) {
		ResultDto re = returnFail("更改风险模型启用状态失败");
		
		RiskWarning riskWarning = riskWarningService.selectSingleByProperty("id", id);
		if(riskWarning != null) {
			riskWarning.setStatus(status);
			riskWarningService.saveOrUpdate(riskWarning);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		}
		
		return re;
	}

}
