package com.beawan.model.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.model.dto.LeadRateLib;
import com.beawan.model.dto.SaveLrModelDto;
import com.beawan.model.entity.LrLibEntity;
import com.beawan.model.service.LeadRateLibService;
import com.platform.util.GsonUtil;
import com.platform.util.HttpUtil;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/model/leadRate" })
public class LeadRateCtl extends BaseController {
	private static Logger log = Logger.getLogger(LeadRateCtl.class);

	@Resource
	private LeadRateLibService leadRateLibService;

	/* 跳转模型库列表 */
	@RequestMapping("list.do")
	public String list(HttpServletRequest request, HttpServletResponse response) throws Exception {
		return "views/model/LrModel/leadRate";
	}

	/**
	 * 获得模型列表
	 * 
	 * @return
	 */
	@RequestMapping("listPrice.json")
	@ResponseBody
	public ResultDto listPrice(@RequestParam(defaultValue = "1", value = "page") int page,
			@RequestParam(defaultValue = "10", value = "rows") int rows, String name) {
		ResultDto re = null;
		try {
			re = leadRateLibService.queryLeadPaging(page, rows, name);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("准人列表获取成功");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(re.getMsg(), e);
		}

		return re;
	}

	/*** 模型详情 ***/
	@RequestMapping("getRateModel.do")

	public String getRateModel(HttpServletRequest request, HttpServletResponse response, Integer id, int type) {
		try {

			Map<String, Object> model = new HashMap<String, Object>();
			if (null != id && !"".equals(id)) {
				LeadRateLib leadRateLib = leadRateLibService.findByLeadRateId(id);
				model.put("data", leadRateLib);
				model.put("id", id);
				model.put("paramFileName", leadRateLib.getParamFileName());
				System.out.println(model);

				request.setAttribute("model", model);
				if(type == 1) {
					return "views/fiveLevel/modelBwInfo";
				}
				else if (leadRateLib.getParamFileName().equals("leadRate_bw")) {
					return "views/model/LrModel/modelBWInfo";
				} else
					return "views/model/LrModel/modelXDInfo";
			}
		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
		}

		return "views/model/LrModel/modelBWInfo2";
	}

	/**
	 * 保存更新定价模型数据
	 * 
	 * @return
	 */
	@RequestMapping(value = "saveModelLib.json")
	@ResponseBody
	public ResultDto saveModelLib(HttpServletRequest request, HttpServletResponse response) {

		ResultDto resultDto = returnFail("保存失败");
		try {
			String jsonString = HttpUtil.getAsString(request, "jsonString");
			jsonString = java.net.URLDecoder.decode(jsonString);
			System.out.println(jsonString);
			SaveLrModelDto saveLrModelDto = GsonUtil.GsonToBean(jsonString, SaveLrModelDto.class);
			System.out.println(saveLrModelDto);
			leadRateLibService.updateByDto(saveLrModelDto);
			resultDto.setMsg("success");
		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
		}
		return resultDto;
	}

	@RequestMapping("changeModelState.do")
	@ResponseBody
	public ResultDto changeModelState(Integer modelId) {
		ResultDto resultDto = returnFail("");
		Integer state = 0;
		try {
			LrLibEntity leadRateLibEntity = leadRateLibService.findByPrimaryKey(modelId);
			System.out.println(leadRateLibEntity);
			state = leadRateLibEntity.getStatus();

			leadRateLibEntity.setStatus(changeState(state));
			leadRateLibService.saveOrUpdate(leadRateLibEntity);
		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
			resultDto.setMsg("fail");
		}
		resultDto.setMsg("true");
		return resultDto;
	}

	/* 更改状态工具 */
	public Integer changeState(Integer state) {
		return state == 1 ? 0 : 1;
	}
}
