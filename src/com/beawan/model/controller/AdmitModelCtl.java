package com.beawan.model.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.model.entity.AdmitEntity;
import com.beawan.model.entity.AdmitQuotaAttrEntity;
import com.beawan.model.entity.AdmitQuotaEntity;
import com.beawan.model.service.AdmitQuotaAttrService;
import com.beawan.model.service.AdmitQuotaService;
import com.beawan.model.service.AdmitService;
import com.platform.util.HttpUtil;
import com.platform.util.JacksonUtil;

@Controller
@UserSessionAnnotation
@RequestMapping({"/model/admitModel/"})
public class AdmitModelCtl extends BaseController{
	private static final Logger log = Logger.getLogger(AdmitModelCtl.class);
	
	@Resource
	private AdmitQuotaService admitQuotaSV;
	@Resource
	private AdmitQuotaAttrService admitQuotaAttrSV;
	@Resource
	private AdmitService admitSV;
	
	@RequestMapping("/createAdmitModel.do")
	public String createAdmitModel(){
		return "views/model/admit/add_model";
	}
	
	@RequestMapping("/getQuotaHtml.do")
	public ModelAndView getQuotaHtml(HttpServletRequest request, HttpServletResponse response){
		ModelAndView mav = new ModelAndView("/views/model/admit/quota_add_temp");	
		return mav;
	}
	
	@RequestMapping("/getQuotaList.json")
	@ResponseBody
	public ResultDto getQuotaList(HttpServletRequest request, HttpServletResponse response){
		ResultDto re = returnFail("");
		try {
			List<AdmitQuotaEntity> quotaList = admitQuotaSV.queryParentQuota();
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setRows(quotaList);
		} catch (Exception e) {
			re.setMsg("获取指标列表异常！");
			log.error("获取指标列表异常！",e);
			e.printStackTrace();
		}
		return re;
	}
	
	@RequestMapping("/subQuotaListHtml.json")
	@ResponseBody
	public ResultDto subQuotaListHtml(HttpServletRequest request, HttpServletResponse response){
		ResultDto re = returnFail("");
		try {
			String pCode = HttpUtil.getAsString(request, "pCode");
			List<AdmitQuotaEntity> quotaList = admitQuotaSV.queryByPCode(pCode);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setRows(quotaList);
		} catch (Exception e) {
			re.setMsg("获取子指标模板系统异常！");
			log.error("获取子指标模板系统异常！",e);
			e.printStackTrace();
		}
		return re;
	}
	
	@RequestMapping("/getQuotaAttrList.json")
	@ResponseBody
	public ResultDto getQuotaAttrList(HttpServletRequest request){
		ResultDto re = returnFail("");
		try {
			String subquotaCode = HttpUtil.getAsString(request, "subquotaCode");
			List<AdmitQuotaAttrEntity> attrList = admitQuotaAttrSV.selectByProperty("quotaCode", subquotaCode);
			re.setRows(attrList);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("获取子指标对应数据");
		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
		}
		return re;
	}
	
	@RequestMapping("/saveAdmitModel.json")
	@ResponseBody
	public ResultDto saveAdmitModel(String jsonstring, HttpServletRequest request){
		ResultDto re = returnFail("模型保存失败");
		try {
			
			jsonstring = java.net.URLDecoder.decode(jsonstring, "UTF-8");
			
			AdmitEntity model = JacksonUtil.fromJson(jsonstring, AdmitEntity.class);
			
			if(model.getId() == 0){
				admitSV.saveAdmitModel(model);
			}else{
				admitSV.modifyAdmitModel(model);
			}
			
			
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功");
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
		}
		return re;
	}
	
	@RequestMapping("/viewAdmitModel.json")
	public String viewAdmitModel(String identifCode, HttpServletRequest request){
		try {
			AdmitEntity admitModel = admitSV.queryByIdentifCode(identifCode);
			
			request.setAttribute("admitModel", admitModel);
			
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
		}
		return "views/model/admit/admitModel_view";
	}
	
	@RequestMapping("/modifyAdmitModelPage.do")
	public String modifyAdmitModelPage(String identifCode, HttpServletRequest request){
		
		try {
			AdmitEntity admitModel = admitSV.queryByIdentifCode(identifCode);
			
			request.setAttribute("admitModel", admitModel);
			
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
		}
		
		return "views/model/admit/admitModel_modify";
	}
	
	@RequestMapping("/getAdmitModelList.do")
	@ResponseBody
	public ResultDto getAdmitModelList(){
		ResultDto re = returnFail("获取准入模型列表失败");
		
		try {
			List<AdmitEntity> admitModelList = admitSV.getAll();
			
			re.setRows(admitModelList);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
		}
		return re;
	}
	
	@RequestMapping("/getAdmitModel.do")
	@ResponseBody
	public ResultDto getAdmitQuotaList(String identifCode){
		ResultDto re = returnFail("获取准入模型列表失败");
		
		try {
			AdmitEntity admitModel = admitSV.queryByIdentifCode(identifCode);
			
			re.setRows(admitModel);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e);
			e.printStackTrace();
		}
		
		return re;
	}
	
	@RequestMapping("/checkInModel.do")
	@ResponseBody
	public ResultDto checkInModel(String quotaCode){
		ResultDto re = returnFail("验证是否应用在模型中失败");
		
		try {
			boolean flag = admitSV.checkInModel(quotaCode);
			
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setRows(flag);
					
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return re;
	}
}
