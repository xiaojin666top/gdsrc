package com.beawan.model.controller;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.common.Constants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.Pagination;
import com.beawan.core.ResultDto;
//import com.beawan.grade.dto.GradeQuotaDto;
import com.beawan.model.dto.AdmitDto;
import com.beawan.model.entity.AdmitEntity;
import com.beawan.model.entity.AdmitQuotaEntity;
import com.beawan.model.entity.AdmitTemplateEntity;
import com.beawan.model.service.AdmitItemService;
import com.beawan.model.service.AdmitQuotaService;
import com.beawan.model.service.AdmitService;
import com.beawan.model.service.AdmitTemplateService;
import com.google.gson.Gson;
import com.platform.util.JacksonUtil;


@Controller
@UserSessionAnnotation
@RequestMapping({"/model/admit"})
public class AdmitCtl extends BaseController{

	private static Logger log = Logger.getLogger(AdmitCtl.class);
	
	@Resource
	private AdmitItemService admitItemSV;
	@Resource
	private AdmitService admitSV;
	@Resource
	private AdmitTemplateService admitTemplateSV;
	@Resource
	private AdmitQuotaService admitQuotaSV;
	
	@RequestMapping("/demo.json")
	@ResponseBody
	public ResultDto demo() {
		ResultDto re = returnFail("数据获取失败");
		try {
			
		}catch (Exception e) {
			e.printStackTrace();
			log.error(re.getMsg(), e);
		}
		return re;
	}
	
	/**
	 * 页面跳转到准入配置 页面
	 * @param request
	 * @param response
	 * @param customerNo
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/viewAdmitance.do")
	public String admittance(HttpServletRequest request,
			HttpServletResponse response,String customerNo)throws Exception{
		HashMap<String,Object> map=new HashMap<String, Object>();
		try {
			
		} catch (Exception e) {
			log.error("跳转准入页面异常", e);
		}
		return "views/model/admit/admittanceList";
	}
	
	
	
	//获得准入模型列表
	@RequestMapping("/listAdmitance.json")
	@ResponseBody
	public ResultDto listAdmitance() {
		ResultDto re = returnFail("数据获取失败");
		try {
			
		}catch (Exception e) {
			e.printStackTrace();
			log.error(re.getMsg(), e);
		}
		return re;
	}
	
	
	
	@RequestMapping("/saveAdmitInfo.json")
	@ResponseBody
	public ResultDto saveAdmitInfo(String jsonstring) {
		ResultDto re = returnFail("准入模型保存失败");
		if(jsonstring==null||"".equals(jsonstring))
			return re;
		try {
			jsonstring = java.net.URLDecoder.decode(jsonstring,"UTF-8");
			Gson gson = new Gson();
			AdmitDto dto= gson.fromJson(jsonstring, AdmitDto.class);
			admitSV.modifyAdmit(dto);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功");
		}catch(Exception e) {
			log.error(re.getMsg(), e);
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 获得当前节点的子节点列表
	 * @return
	 */
	@RequestMapping("/getChildList.json")
	@ResponseBody
	public ResultDto getChildList(Integer templateId) {
		ResultDto re = returnFail("数据获取失败");
		try {
			List<AdmitTemplateEntity> templates = null;
			if(templateId!=null && !"".equals(templateId)) {
				templates = admitTemplateSV.selectByProperty("parentId", templateId);
			}
			re.setRows(templates);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("数据获取成功");
		}catch (Exception e) {
			e.printStackTrace();
			log.error(re.getMsg(), e);
		}
		return re;
	}
	
	//手动增加准入模版
	@RequestMapping("/addAdmit.json")
	@ResponseBody
	public ResultDto addAdmit() {
		ResultDto re = returnFail("数据获取失败");
		try {
//			AdmitEntity admit = new AdmitEntity();
//			admit.setName("碧湾准入模型");
//			admit.setStatus(Constants.Model.STATUS_USE);
//			admit.setCreateTime(DateUtil.getNowTimestamp());
//			admit = admitSV.saveOrUpdate(admit);
//			if(admit.getId()==0)
//				return re;
			AdmitTemplateEntity xtgk = new AdmitTemplateEntity();
			xtgk.setDeep(1);
			xtgk.setName("系统刚控");
			xtgk.setParentId(0);
			xtgk.setType(Constants.Model.TYPE_GK);
			xtgk = admitTemplateSV.saveOrUpdate(xtgk);
			//分割
			int gkId = xtgk.getId();
			AdmitTemplateEntity gk1 = new AdmitTemplateEntity();
			gk1.setDeep(2);
			gk1.setName("重大行政处罚");
			gk1.setParentId(gkId);
			gk1.setType(Constants.Model.TYPE_GK);
			gk1 = admitTemplateSV.saveOrUpdate(gk1);
			//分割
			int gk1Id = gk1.getId();
			AdmitTemplateEntity gk1A = new AdmitTemplateEntity();
			gk1A.setDeep(3);
			gk1A.setName("无行政处罚");
			gk1A.setParentId(gk1Id);
			gk1A.setType(Constants.Model.TYPE_GK);
			gk1A = admitTemplateSV.saveOrUpdate(gk1A);
			//分割
			AdmitTemplateEntity gk1B = new AdmitTemplateEntity();
			gk1B.setDeep(3);
			gk1B.setName("受行政处罚，申请复议且复议结果对本人有利的");
			gk1B.setParentId(gk1Id);
			gk1B.setType(Constants.Model.TYPE_GK);
			gk1B = admitTemplateSV.saveOrUpdate(gk1B);
			//分割
			AdmitTemplateEntity gk1C = new AdmitTemplateEntity();
			gk1C.setDeep(3);
			gk1C.setName("受行政处罚被暂扣或者吊销许可证、暂扣或者吊销执照，没有申请复议或者复议结果对本人不利的");
			gk1C.setParentId(gk1Id);
			gk1C.setType(Constants.Model.TYPE_GK);
			gk1C = admitTemplateSV.saveOrUpdate(gk1C);
			
			//分割
			AdmitTemplateEntity gk2 = new AdmitTemplateEntity();
			gk2.setDeep(2);
			gk2.setName("企业征信记录");
			gk2.setParentId(gkId);
			gk2.setType(Constants.Model.TYPE_GK);
			gk2 = admitTemplateSV.saveOrUpdate(gk2);
			//分割
			int gk2Id = gk1.getId();
			AdmitTemplateEntity gk2A = new AdmitTemplateEntity();
			gk2A.setDeep(3);
			gk2A.setName("正常");
			gk2A.setParentId(gk2Id);
			gk2A.setType(Constants.Model.TYPE_GK);
			gk2A = admitTemplateSV.saveOrUpdate(gk1A);
			//分割
			AdmitTemplateEntity gk2B = new AdmitTemplateEntity();
			gk2B.setDeep(3);
			gk2B.setName("非正常");
			gk2B.setParentId(gk2Id);
			gk2B.setType(Constants.Model.TYPE_GK);
			gk2B = admitTemplateSV.saveOrUpdate(gk1B);
			//分割
			AdmitTemplateEntity gk2C = new AdmitTemplateEntity();
			gk2C.setDeep(3);
			gk2C.setName("有主观不良记录");
			gk2C.setParentId(gk2Id);
			gk2C.setType(Constants.Model.TYPE_GK);
			gk2C = admitTemplateSV.saveOrUpdate(gk1C);
			//分割
			AdmitTemplateEntity gk2D = new AdmitTemplateEntity();
			gk2D.setDeep(3);
			gk2D.setName("有非主观不良记录");
			gk2D.setParentId(gk2Id);
			gk2D.setType(Constants.Model.TYPE_GK);
			gk2D = admitTemplateSV.saveOrUpdate(gk2D);
			//分割
			AdmitTemplateEntity gk2E = new AdmitTemplateEntity();
			gk2E.setDeep(3);
			gk2E.setName("无不良记录");
			gk2E.setParentId(gk2Id);
			gk2E.setType(Constants.Model.TYPE_GK);
			gk2E = admitTemplateSV.saveOrUpdate(gk2E);
			
			//分割
			AdmitTemplateEntity gk3 = new AdmitTemplateEntity();
			gk3.setDeep(2);
			gk3.setName("行外交叉违约");
			gk3.setParentId(gkId);
			gk3.setType(Constants.Model.TYPE_GK);
			gk3 = admitTemplateSV.saveOrUpdate(gk3);
			//分割
			int gk3Id = gk3.getId();
			AdmitTemplateEntity gk3A = new AdmitTemplateEntity();
			gk3A.setDeep(3);
			gk3A.setName("无违约情况");
			gk3A.setParentId(gk3Id);
			gk3A.setType(Constants.Model.TYPE_GK);
			gk3A = admitTemplateSV.saveOrUpdate(gk3A);
			//分割
			AdmitTemplateEntity gk3B = new AdmitTemplateEntity();
			gk3B.setDeep(3);
			gk3B.setName("存在轻微行外违约");
			gk3B.setParentId(gk3Id);
			gk3B.setType(Constants.Model.TYPE_GK);
			gk3B = admitTemplateSV.saveOrUpdate(gk3B);
			//分割
			AdmitTemplateEntity gk3C = new AdmitTemplateEntity();
			gk3C.setDeep(3);
			gk3C.setName("存在严重行外违约情况");
			gk3C.setParentId(gk3Id);
			gk3C.setType(Constants.Model.TYPE_GK);
			gk3C = admitTemplateSV.saveOrUpdate(gk3C);
			
			//分割
			AdmitTemplateEntity gk4 = new AdmitTemplateEntity();
			gk4.setDeep(2);
			gk4.setName("关联企业违约情况");
			gk4.setParentId(gkId);
			gk4.setType(Constants.Model.TYPE_GK);
			gk4 = admitTemplateSV.saveOrUpdate(gk4);
			//分割
			int gk4Id = gk4.getId();
			AdmitTemplateEntity gk4A = new AdmitTemplateEntity();
			gk4A.setDeep(3);
			gk4A.setName("无违约情况");
			gk4A.setParentId(gk4Id);
			gk4A.setType(Constants.Model.TYPE_GK);
			gk4A = admitTemplateSV.saveOrUpdate(gk4A);
			//分割
			AdmitTemplateEntity gk4B = new AdmitTemplateEntity();
			gk4B.setDeep(3);
			gk4B.setName("关联企业存在轻微违约");
			gk4B.setParentId(gk4Id);
			gk4B.setType(Constants.Model.TYPE_GK);
			gk4B = admitTemplateSV.saveOrUpdate(gk4B);
			//分割
			AdmitTemplateEntity gk4C = new AdmitTemplateEntity();
			gk4C.setDeep(3);
			gk4C.setName("关联企业存在严重违约");
			gk4C.setParentId(gk4Id);
			gk4C.setType(Constants.Model.TYPE_GK);
			gk4C = admitTemplateSV.saveOrUpdate(gk4C);


			AdmitTemplateEntity fxtx = new AdmitTemplateEntity();
			fxtx.setDeep(1);
			fxtx.setName("风险提醒");
			fxtx.setParentId(0);
			fxtx.setType(Constants.Model.TYPE_FX);
			fxtx = admitTemplateSV.saveOrUpdate(fxtx);
			//分割
			int fxId = fxtx.getId();
			AdmitTemplateEntity fx1 = new AdmitTemplateEntity();
			fx1.setDeep(2);
			fx1.setName("在本行贷款和结算记录");
			fx1.setParentId(fxId);
			fx1.setType(Constants.Model.TYPE_FX);
			fx1 = admitTemplateSV.saveOrUpdate(fx1);
			//分割
			int fx1Id = fx1.getId();
			AdmitTemplateEntity fx1A = new AdmitTemplateEntity();
			fx1A.setDeep(3);
			fx1A.setName("有贷款且无不良记录");
			fx1A.setParentId(fx1Id);
			fx1A.setType(Constants.Model.TYPE_FX);
			fx1A = admitTemplateSV.saveOrUpdate(fx1A);
			//分割
			AdmitTemplateEntity fx1B = new AdmitTemplateEntity();
			fx1B.setDeep(3);
			fx1B.setName("无贷款");
			fx1B.setParentId(fx1Id);
			fx1B.setType(Constants.Model.TYPE_FX);
			fx1B = admitTemplateSV.saveOrUpdate(fx1B);
			//分割
			AdmitTemplateEntity fx1C = new AdmitTemplateEntity();
			fx1C.setDeep(3);
			fx1C.setName("有贷款但有不良记录");
			fx1C.setParentId(fx1Id);
			fx1C.setType(Constants.Model.TYPE_FX);
			fx1C = admitTemplateSV.saveOrUpdate(fx1C);
			
			//分割
			AdmitTemplateEntity fx2 = new AdmitTemplateEntity();
			fx2.setDeep(2);
			fx2.setName("财务杠杆率（总负债/净资产）");
			fx2.setParentId(fxId);
			fx2.setType(Constants.Model.TYPE_FX);
			fx2 = admitTemplateSV.saveOrUpdate(fx2);
			//分割
			int fx2Id = fx2.getId();
			AdmitTemplateEntity fx2A = new AdmitTemplateEntity();
			fx2A.setDeep(3);
			fx2A.setName("[0,15%)");
			fx2A.setParentId(fx2Id);
			fx2A.setType(Constants.Model.TYPE_FX);
			fx2A = admitTemplateSV.saveOrUpdate(fx2A);
			//分割
			AdmitTemplateEntity fx2B = new AdmitTemplateEntity();
			fx2B.setDeep(3);
			fx2B.setName("[15%,50%)");
			fx2B.setParentId(fx2Id);
			fx2B.setType(Constants.Model.TYPE_FX);
			fx2B = admitTemplateSV.saveOrUpdate(fx2B);
			//分割
			AdmitTemplateEntity fx2C = new AdmitTemplateEntity();
			fx2C.setDeep(3);
			fx2C.setName("[50%,150%)");
			fx2C.setParentId(fx2Id);
			fx2C.setType(Constants.Model.TYPE_FX);
			fx2C = admitTemplateSV.saveOrUpdate(fx2C);
			//分割
			AdmitTemplateEntity fx2D = new AdmitTemplateEntity();
			fx2D.setDeep(3);
			fx2D.setName("[150%,1000%)");
			fx2D.setParentId(fx2Id);
			fx2D.setType(Constants.Model.TYPE_FX);
			fx2D = admitTemplateSV.saveOrUpdate(fx2D);

			//分割
			AdmitTemplateEntity fx3 = new AdmitTemplateEntity();
			fx3.setDeep(2);
			fx3.setName("财报完整性");
			fx3.setParentId(fxId);
			fx3.setType(Constants.Model.TYPE_FX);
			fx3 = admitTemplateSV.saveOrUpdate(fx3);
			//分割
			int fx3Id = fx3.getId();
			AdmitTemplateEntity fx3A = new AdmitTemplateEntity();
			fx3A.setDeep(3);
			fx3A.setName("完整");
			fx3A.setParentId(fx3Id);
			fx3A.setType(Constants.Model.TYPE_FX);
			fx3A = admitTemplateSV.saveOrUpdate(fx3A);
			//分割
			AdmitTemplateEntity fx3B = new AdmitTemplateEntity();
			fx3B.setDeep(3);
			fx3B.setName("不完整");
			fx3B.setParentId(fx3Id);
			fx3B.setType(Constants.Model.TYPE_FX);
			fx3B = admitTemplateSV.saveOrUpdate(fx3B);

			//分割
			AdmitTemplateEntity fx4 = new AdmitTemplateEntity();
			fx4.setDeep(2);
			fx4.setName("应收账款账期风险(应收账款周转天数较年初增加)");
			fx4.setParentId(fxId);
			fx4.setType(Constants.Model.TYPE_FX);
			fx4 = admitTemplateSV.saveOrUpdate(fx4);
			//分割
			int fx4Id = fx4.getId();
			AdmitTemplateEntity fx4A = new AdmitTemplateEntity();
			fx4A.setDeep(3);
			fx4A.setName("[0,40)天");
			fx4A.setParentId(fx4Id);
			fx4A.setType(Constants.Model.TYPE_FX);
			fx4A = admitTemplateSV.saveOrUpdate(fx4A);
			//分割
			AdmitTemplateEntity fx4B = new AdmitTemplateEntity();
			fx4B.setDeep(3);
			fx4B.setName("[40,55)天");
			fx4B.setParentId(fx4Id);
			fx4B.setType(Constants.Model.TYPE_FX);
			fx4B = admitTemplateSV.saveOrUpdate(fx4B);
			//分割
			AdmitTemplateEntity fx4C = new AdmitTemplateEntity();
			fx4C.setDeep(3);
			fx4C.setName("[55,1000)天");
			fx4C.setParentId(fx4Id);
			fx4C.setType(Constants.Model.TYPE_FX);
			fx4C = admitTemplateSV.saveOrUpdate(fx4C);

			//分割
			AdmitTemplateEntity fx5 = new AdmitTemplateEntity();
			fx5.setDeep(2);
			fx5.setName("公司成立年限");
			fx5.setParentId(fxId);
			fx5.setType(Constants.Model.TYPE_FX);
			fx5 = admitTemplateSV.saveOrUpdate(fx5);
			//分割
			int fx5Id = fx5.getId();
			AdmitTemplateEntity fx5A = new AdmitTemplateEntity();
			fx5A.setDeep(3);
			fx5A.setName("[0,1)年");
			fx5A.setParentId(fx5Id);
			fx5A.setType(Constants.Model.TYPE_FX);
			fx5A = admitTemplateSV.saveOrUpdate(fx5A);
			//分割
			AdmitTemplateEntity fx5B = new AdmitTemplateEntity();
			fx5B.setDeep(3);
			fx5B.setName("[1,3)年");
			fx5B.setParentId(fx5Id);
			fx5B.setType(Constants.Model.TYPE_FX);
			fx5B = admitTemplateSV.saveOrUpdate(fx5B);
			//分割
			AdmitTemplateEntity fx5C = new AdmitTemplateEntity();
			fx5C.setDeep(3);
			fx5C.setName("[3,100)年");
			fx5C.setParentId(fx5Id);
			fx5C.setType(Constants.Model.TYPE_FX);
			fx5C = admitTemplateSV.saveOrUpdate(fx5C);

			//分割
			AdmitTemplateEntity fx6 = new AdmitTemplateEntity();
			fx6.setDeep(2);
			fx6.setName("存量客户，过去一年内预警情况");
			fx6.setParentId(fxId);
			fx6.setType(Constants.Model.TYPE_FX);
			fx6 = admitTemplateSV.saveOrUpdate(fx6);
			//分割
			int fx6Id = fx6.getId();
			AdmitTemplateEntity fx6A = new AdmitTemplateEntity();
			fx6A.setDeep(3);
			fx6A.setName("未出现");
			fx6A.setParentId(fx6Id);
			fx6A.setType(Constants.Model.TYPE_FX);
			fx6A = admitTemplateSV.saveOrUpdate(fx6A);
			//分割
			AdmitTemplateEntity fx6B = new AdmitTemplateEntity();
			fx6B.setDeep(3);
			fx6B.setName("出现严重预警情况，已解决");
			fx6B.setParentId(fx6Id);
			fx6B.setType(Constants.Model.TYPE_FX);
			fx6B = admitTemplateSV.saveOrUpdate(fx6B);
			//分割
			AdmitTemplateEntity fx6C = new AdmitTemplateEntity();
			fx6C.setDeep(3);
			fx6C.setName("出现严重预警情况，未解决");
			fx6C.setParentId(fx6Id);
			fx6C.setType(Constants.Model.TYPE_FX);
			fx6C = admitTemplateSV.saveOrUpdate(fx6C);

			//分割
			AdmitTemplateEntity fx7 = new AdmitTemplateEntity();
			fx7.setDeep(2);
			fx7.setName("盈利年限");
			fx7.setParentId(fxId);
			fx7.setType(Constants.Model.TYPE_FX);
			fx7 = admitTemplateSV.saveOrUpdate(fx7);
			//分割
			int fx7Id = fx7.getId();
			AdmitTemplateEntity fx7A = new AdmitTemplateEntity();
			fx7A.setDeep(3);
			fx7A.setName("3年内盈利2年以上");
			fx7A.setParentId(fx7Id);
			fx7A.setType(Constants.Model.TYPE_FX);
			fx7A = admitTemplateSV.saveOrUpdate(fx7A);
			//分割
			AdmitTemplateEntity fx7B = new AdmitTemplateEntity();
			fx7B.setDeep(3);
			fx7B.setName("3年内盈利1年");
			fx7B.setParentId(fx7Id);
			fx7B.setType(Constants.Model.TYPE_FX);
			fx7B = admitTemplateSV.saveOrUpdate(fx7B);
			//分割
			AdmitTemplateEntity fx7C = new AdmitTemplateEntity();
			fx7C.setDeep(3);
			fx7C.setName("3年内未盈利");
			fx7C.setParentId(fx7Id);
			fx7C.setType(Constants.Model.TYPE_FX);
			fx7C = admitTemplateSV.saveOrUpdate(fx7C);

			//分割
			AdmitTemplateEntity fx8 = new AdmitTemplateEntity();
			fx8.setDeep(2);
			fx8.setName("过度融资情况");
			fx8.setParentId(fxId);
			fx8.setType(Constants.Model.TYPE_FX);
			fx8 = admitTemplateSV.saveOrUpdate(fx8);
			//分割
			int fx8Id = fx8.getId();
			AdmitTemplateEntity fx8A = new AdmitTemplateEntity();
			fx8A.setDeep(3);
			fx8A.setName("正常");
			fx8A.setParentId(fx8Id);
			fx8A.setType(Constants.Model.TYPE_FX);
			fx8A = admitTemplateSV.saveOrUpdate(fx8A);
			//分割
			AdmitTemplateEntity fx8B = new AdmitTemplateEntity();
			fx8B.setDeep(3);
			fx8B.setName("不正常");
			fx8B.setParentId(fx8Id);
			fx8B.setType(Constants.Model.TYPE_FX);
			fx8B = admitTemplateSV.saveOrUpdate(fx8B);

			//分割
			AdmitTemplateEntity fx9 = new AdmitTemplateEntity();
			fx9.setDeep(2);
			fx9.setName("涉诉不良外部信息");
			fx9.setParentId(fxId);
			fx9.setType(Constants.Model.TYPE_FX);
			fx9 = admitTemplateSV.saveOrUpdate(fx9);
			//分割
			int fx9Id = fx9.getId();
			AdmitTemplateEntity fx9A = new AdmitTemplateEntity();
			fx9A.setDeep(3);
			fx9A.setName("企业存在涉诉信息");
			fx9A.setParentId(fx9Id);
			fx9A.setType(Constants.Model.TYPE_FX);
			fx9A = admitTemplateSV.saveOrUpdate(fx9A);
			//分割
			AdmitTemplateEntity fx9B = new AdmitTemplateEntity();
			fx9B.setDeep(3);
			fx9B.setName("担保人或担保企业存在涉诉信息");
			fx9B.setParentId(fx9Id);
			fx9B.setType(Constants.Model.TYPE_FX);
			fx9B = admitTemplateSV.saveOrUpdate(fx9B);
			//分割
			AdmitTemplateEntity fx9C = new AdmitTemplateEntity();
			fx9C.setDeep(3);
			fx9C.setName("无涉诉信息");
			fx9C.setParentId(fx9Id);
			fx9C.setType(Constants.Model.TYPE_FX);
			fx9C = admitTemplateSV.saveOrUpdate(fx9C);

			//分割
			AdmitTemplateEntity fx10 = new AdmitTemplateEntity();
			fx10.setDeep(2);
			fx10.setName("限售收入增长情况");
			fx10.setParentId(fxId);
			fx10.setType(Constants.Model.TYPE_FX);
			fx10 = admitTemplateSV.saveOrUpdate(fx10);
			//分割
			int fx10Id = fx10.getId();
			AdmitTemplateEntity fx10A = new AdmitTemplateEntity();
			fx10A.setDeep(3);
			fx10A.setName("[-200%,0)");
			fx10A.setParentId(fx10Id);
			fx10A.setType(Constants.Model.TYPE_FX);
			fx10A = admitTemplateSV.saveOrUpdate(fx10A);
			//分割
			AdmitTemplateEntity fx10B = new AdmitTemplateEntity();
			fx10B.setDeep(3);
			fx10B.setName("[0,15%)");
			fx10B.setParentId(fx10Id);
			fx10B.setType(Constants.Model.TYPE_FX);
			fx10B = admitTemplateSV.saveOrUpdate(fx10B);
			//分割
			AdmitTemplateEntity fx10C = new AdmitTemplateEntity();
			fx10C.setDeep(3);
			fx10C.setName("[15%,80%)");
			fx10C.setParentId(fx10Id);
			fx10C.setType(Constants.Model.TYPE_FX);
			fx10C = admitTemplateSV.saveOrUpdate(fx10C);
			//分割
			AdmitTemplateEntity fx10D = new AdmitTemplateEntity();
			fx10D.setDeep(3);
			fx10D.setName("[80%,200%)");
			fx10D.setParentId(fx10Id);
			fx10D.setType(Constants.Model.TYPE_FX);
			fx10D = admitTemplateSV.saveOrUpdate(fx10D);
			//分割
			AdmitTemplateEntity fx10E = new AdmitTemplateEntity();
			fx10E.setDeep(3);
			fx10E.setName("[200%,1000%)");
			fx10E.setParentId(fx10Id);
			fx10E.setType(Constants.Model.TYPE_FX);
			fx10E = admitTemplateSV.saveOrUpdate(fx10E);
			
		}catch (Exception e) {
			e.printStackTrace();
			log.error(re.getMsg(), e);
		}
		return re;
	}


	/**
	 * 获得准入列表
	 * @return
	 */
	@RequestMapping("/listAdmint.json")
	@ResponseBody
	public ResultDto listAdmint(@RequestParam(defaultValue = "1", value = "page") int page,
			@RequestParam(defaultValue = "10", value = "rows") int rows,String name) {
		ResultDto re = returnFail("数据获取失败");
		try {
			re = admitSV.queryAdmitPaging(page, rows, name);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("准人列表获取成功");
		}catch (Exception e) {
			e.printStackTrace();
			log.error(re.getMsg(), e);
		}
		return re;
	}

	/**
	 * 修改准入模型
	 * @return
	 */
	@RequestMapping("/modifyAdmit.json")
	@ResponseBody
	public ResultDto modifyAdmit(int id, int status) {
		ResultDto re = returnFail("修改失败！");
		try {
			AdmitEntity entity = admitSV.findByPrimaryKey(id);
			entity.setState(status);
			admitSV.saveOrUpdate(entity);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("");
		}catch (Exception e) {
			e.printStackTrace();
			log.error(re.getMsg(), e);
		}
		return re;
	}
	/////你写在这个下面 
	
	
	/*************    准入指标管理   *****************/
	
	@RequestMapping("/viewAdminQuota.do")
	public String viewAdminQuota(HttpServletRequest request,HttpServletResponse response) {
		return "/views/model/admit/admitQuotaList";
	}
	
	/**
	 * 获得准入指标列表
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping("/getAdminQuotaList.json")
	@ResponseBody
	public ResultDto getAdminQuotaList(@RequestParam(defaultValue = "1", value = "page") int page,
			@RequestParam(defaultValue = "10", value = "rows") int rows,String quotaName) {
		ResultDto re = returnFail("");
		try {
			
			
			Pagination<AdmitQuotaEntity> pager = admitQuotaSV.getAdmitQuotaPager(quotaName, page, rows);
			if(pager == null) {
				re.setMsg("获取准入指标失败");
				return re;
			}
			List<AdmitQuotaEntity> quotaList = pager.getItems();
			
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setRows(quotaList);
			re.setTotal(pager.getRowsCount());
		}catch(Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 跳转到新增指标页面
	 * @return
	 */
	@RequestMapping("/addQuotaPage.do")
	public String addQuotaPage() {
		return "views/model/admit/add_quota";
	}
	
	/* 保存新指标 */
	@RequestMapping("/saveQuota.json")
	@ResponseBody
	public ResultDto saveQuota(String jsonstring) {
		ResultDto re = returnFail("指标保存失败");
		if (jsonstring == null || "".equals(jsonstring))
			return re;
		try {
			jsonstring = java.net.URLDecoder.decode(jsonstring, "UTF-8");

			AdmitQuotaEntity admitQuota = JacksonUtil.fromJson(jsonstring, AdmitQuotaEntity.class);
			
			admitQuotaSV.save(admitQuota);
		    
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功");
		} catch (Exception e) {
			log.error(re.getMsg(), e);
			e.printStackTrace();
		}
		return re;
	}
	
	@RequestMapping("/getAdmitByCode.do")
	public String getAdmitInfo(String code, HttpServletRequest request) {
		try {
			//List<AdmitDto> dtos = admitSV.queryAdmitInfo(id);
			AdmitQuotaEntity admitQuota = admitQuotaSV.getAdmitQuotaEntityTree(code);
			request.setAttribute("admitQuota", admitQuota);
		}catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return "views/model/admit/admitQuota_view";
	}
	@RequestMapping("/modifyAdmitQuotaPage.do")
	public String modifyAdmitQuotaPage(String code, HttpServletRequest request){
		try {
			
			AdmitQuotaEntity admitQuota = admitQuotaSV.getAdmitQuotaEntityTree(code);
			request.setAttribute("admitQuota", admitQuota);
		}catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return "views/model/admit/admitQuota_modify";
	}
	@RequestMapping("/modifyQuota.json")
	@ResponseBody
	public ResultDto modifyQuota(String jsonstring, HttpServletRequest request){
		ResultDto re = returnFail("准入指标修改失败");
		if(jsonstring==null||"".equals(jsonstring))
			return re;
		try {
			jsonstring = java.net.URLDecoder.decode(jsonstring, "UTF-8");

			AdmitQuotaEntity admitQuota = JacksonUtil.fromJson(jsonstring, AdmitQuotaEntity.class);
			
			admitQuotaSV.modify(admitQuota);
		    
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("修改成功");
		} catch (Exception e) {
			// TODO: handle exception
			log.error(re.getMsg(), e);
			e.printStackTrace();
		}
		return re;
	}
	

	
	
	
}
