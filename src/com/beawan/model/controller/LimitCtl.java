package com.beawan.model.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.model.dto.LimitDto;
import com.beawan.model.entity.LimitEntity;
import com.beawan.model.service.LimitService;
import com.google.gson.Gson;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/model/limit" })
public class LimitCtl extends BaseController {
	private static Logger log = Logger.getLogger(LimitCtl.class);
	@Resource
	private LimitService limitService;

	@RequestMapping("/limitPage.do")
	public String limitPage() {
		return "views/model/limit/limit_list";

	}

	@RequestMapping("/limitList.json")
	@ResponseBody
	public ResultDto limitList(@RequestParam(defaultValue = "1", value = "page") int page,
			@RequestParam(defaultValue = "10", value = "rows") int rows, String name) {
		ResultDto resultDto = returnFail("获取额度表失败");
		try {
			resultDto = limitService.queryLimitList(page, rows, name);
			resultDto.setCode(resultDto.RESULT_CODE_SUCCESS);
			resultDto.setMsg("获取成功");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取额度表失败", e);
		}
		return resultDto;
	}

	@RequestMapping("/getlimit.do")
	public ModelAndView getLimit(Long id, HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("views/model/limit/limit_modify");
		try {
			LimitDto limitDto = limitService.getLimitById(id);
			mav.addObject("model", limitDto);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return mav;
	}

	@RequestMapping("/savelimit.json")
	@ResponseBody
	public ResultDto saveLimit(String jsonString) {
		ResultDto resultDto = returnFail("保存参数失败");
		if (jsonString == null || "".equals(jsonString))
			return resultDto;
		try {
			jsonString = java.net.URLDecoder.decode(jsonString, "UTF-8");
			Gson gson = new Gson();
			LimitDto dto = gson.fromJson(jsonString, LimitDto.class);
			System.out.println(dto);
			limitService.updateLimit(dto);
			resultDto.setCode(ResultDto.RESULT_CODE_SUCCESS);
			resultDto.setMsg("修改成功");
		} catch (Exception e) {
			log.error(resultDto.getMsg(), e);
			e.printStackTrace();
		}
		return resultDto;
	}

	@RequestMapping("/modifyState.json")
	@ResponseBody
	public ResultDto modifyState(Long id, Integer state) {
		ResultDto resultDto = returnFail("修改状态失败");
		if (state == null || null == id)
			return resultDto;
		try {
			LimitEntity limitEntity = limitService.findByPrimaryKey(id);
			if (null == limitEntity)
				return resultDto;
			limitEntity.setState(state);
			limitService.saveOrUpdate(limitEntity);
			resultDto.setCode(ResultDto.RESULT_CODE_SUCCESS);
			resultDto.setMsg("修改状态成功");

		} catch (Exception e) {
			log.error(resultDto.getMsg(), e);
			e.printStackTrace();
		}
		return resultDto;
	}

}
