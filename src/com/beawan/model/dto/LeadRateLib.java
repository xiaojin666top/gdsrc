package com.beawan.model.dto;

import java.sql.Timestamp;
import java.util.List;

import com.beawan.model.entity.LrLibEntity;

public class LeadRateLib {
	private int id;
	private String modelName;
	private int status;
	private String identifCode;
	private Timestamp createTime;
	private String paramFileName;
	private Double baseRate;
	private List<LrAdjustItem> adjustItem;

	public LeadRateLib() {
		super();
	}

	public LeadRateLib(int id, String name, int status, String identifCode, Timestamp createTime, String paramFileName,
			Double baseRate, List<LrAdjustItem> adjustItem) {
		super();
		this.id = id;
		this.modelName = name;
		this.status = status;
		this.identifCode = identifCode;
		this.createTime = createTime;
		this.paramFileName = paramFileName;
		this.baseRate = baseRate;
		this.adjustItem = adjustItem;
	}

	public LeadRateLib(LrLibEntity leadRateLibEntity, List<LrAdjustItem> lrAdjustItems) {
		this.id = leadRateLibEntity.getId();
		this.modelName = leadRateLibEntity.getName();
		this.status = leadRateLibEntity.getStatus();
		this.identifCode = leadRateLibEntity.getIdentifCode();
		this.createTime = leadRateLibEntity.getCreateTime();
		this.adjustItem = lrAdjustItems;
		this.baseRate = leadRateLibEntity.getBaseRate();
		this.paramFileName = leadRateLibEntity.getFileName();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}



	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getIdentifCode() {
		return identifCode;
	}

	public void setIdentifCode(String identifCode) {
		this.identifCode = identifCode;
	}

	public Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	public List<LrAdjustItem> getAdjustItem() {
		return adjustItem;
	}

	public void setAdjustItem(List<LrAdjustItem> adjustItem) {
		this.adjustItem = adjustItem;
	}

	public String getParamFileName() {
		return paramFileName;
	}

	public void setParamFileName(String paramFileName) {
		this.paramFileName = paramFileName;
	}

	public Double getBaseRate() {
		return baseRate;
	}

	public void setBaseRate(Double baseRate) {
		this.baseRate = baseRate;
	}

	@Override
	public String toString() {
		return "LeadRateLib [id=" + id + ", name=" + modelName + ", status=" + status + ", identifCode=" + identifCode
				+ ", createTime=" + createTime + ", paramFileName=" + paramFileName + ", baseRate=" + baseRate
				+ ", lrAdjustItems=" + adjustItem + "]";
	}

}
