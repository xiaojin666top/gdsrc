package com.beawan.model.dto;

import java.util.List;

public class AdmitDto {

	private int admitId;
	private String admitName;
	private String identifCode; 
	private int itemId;//科目id
	private String itemName;//名称
	private int deep;//层数 从1开始
	private Integer score;//分数
	private Double weight;//权重 默认百分制若为空 默认为100%
	private int parentId;//0是初始节点
	private int templateId;
	private int useCount;
	private int flag;//0默认没有    1为模型中有当前项 
	private int type;//类型 1：系统刚空 2：风险提醒
	private List<AdmitDto> nextNodes;
	
	public int getAdmitId() {
		return admitId;
	}
	public void setAdmitId(int admitId) {
		this.admitId = admitId;
	}
	public String getAdmitName() {
		return admitName;
	}
	public void setAdmitName(String admitName) {
		this.admitName = admitName;
	}
	public String getIdentifCode() {
		return identifCode;
	}
	public void setIdentifCode(String identifCode) {
		this.identifCode = identifCode;
	}
	
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public int getDeep() {
		return deep;
	}
	public void setDeep(int deep) {
		this.deep = deep;
	}
	public int getParentId() {
		return parentId;
	}
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}
	public int getTemplateId() {
		return templateId;
	}
	public void setTemplateId(int templateId) {
		this.templateId = templateId;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public List<AdmitDto> getNextNodes() {
		return nextNodes;
	}
	public void setNextNodes(List<AdmitDto> nextNodes) {
		this.nextNodes = nextNodes;
	}
	public int getFlag() {
		return flag;
	}
	public void setFlag(int flag) {
		this.flag = flag;
	}
	public int getUseCount() {
		return useCount;
	}
	public void setUseCount(int useCount) {
		this.useCount = useCount;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	} 
	
	
	
	
}
