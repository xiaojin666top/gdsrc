package com.beawan.model.dto;

import java.sql.Timestamp;
import java.util.List;

public class LimitDto {
	private Long id;
	private String name;//额度库名称
	private String framework;//架构方式
	private String accountWay;//计算方式
	private String description;//其他描述
	private Double baseRatio;//基础系数
	private List<AdjustItemDto> adjustItemDtos;

	public LimitDto() {
		// TODO Auto-generated constructor stub
	}

	public LimitDto(Long id, String name, String framework, String accountWay, String description, Double baseRatio,
			List<AdjustItemDto> adjustItemDtos) {
		super();
		this.id = id;
		this.name = name;
		this.framework = framework;
		this.accountWay = accountWay;
		this.description = description;
		this.baseRatio = baseRatio;
		this.adjustItemDtos = adjustItemDtos;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFramework() {
		return framework;
	}

	public void setFramework(String framework) {
		this.framework = framework;
	}

	public String getAccountWay() {
		return accountWay;
	}

	public void setAccountWay(String accountWay) {
		this.accountWay = accountWay;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getBaseRatio() {
		return baseRatio;
	}

	public void setBaseRatio(Double baseRatio) {
		this.baseRatio = baseRatio;
	}

	public List<AdjustItemDto> getAdjustItemDtos() {
		return adjustItemDtos;
	}

	public void setAdjustItemDtos(List<AdjustItemDto> adjustItemDtos) {
		this.adjustItemDtos = adjustItemDtos;
	}

	@Override
	public String toString() {
		return "LimitDto [id=" + id + ", name=" + name + ", framework=" + framework + ", accountWay=" + accountWay
				+ ", description=" + description + ", baseRatio=" + baseRatio + ", adjustItemDtos=" + adjustItemDtos
				+ "]";
	}

	

}
