package com.beawan.model.dto;

import java.io.Serializable;

public class SaveLeadDto implements Serializable {
	private Integer lrdataid;// 更改信息id
	private Double downValue;// 小于值
	private Double upValue;// 大于值
	private Double flowValue;// 浮动率

	public SaveLeadDto() {
		super();
	}


	public SaveLeadDto(Integer lrdataid, Double downValue, Double upValue, Double flowValue) {
		super();
		this.lrdataid = lrdataid;
		this.downValue = downValue;
		this.upValue = upValue;
		this.flowValue = flowValue;
	}



	public Integer getLrdataid() {
		return lrdataid;
	}


	public void setLrdataid(Integer lrdataid) {
		this.lrdataid = lrdataid;
	}


	public Double getDownValue() {
		return downValue;
	}


	public void setDownValue(Double downValue) {
		this.downValue = downValue;
	}


	public Double getUpValue() {
		return upValue;
	}


	public void setUpValue(Double upValue) {
		this.upValue = upValue;
	}


	public Double getFlowValue() {
		return flowValue;
	}


	public void setFlowValue(Double flowValue) {
		this.flowValue = flowValue;
	}


	@Override
	public String toString() {
		return "SaveLeadDto [lrdataid=" + lrdataid + ", downValue=" + downValue + ", upValue=" + upValue
				+ ", flowValue=" + flowValue + "]";
	}

	
}
