package com.beawan.model.dto;

/**
 * 非财务异常预警分析结果
 * @author yzj
 *
 */
public class RiskWarnResultDto {

	private Long id;
	private String taskId;
	private String warnName;
	private String riskRule;
	private int riskType;
	private int warningLv;
	private String riskContent;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public String getWarnName() {
		return warnName;
	}
	public void setWarnName(String warnName) {
		this.warnName = warnName;
	}
	public String getRiskRule() {
		return riskRule;
	}
	public void setRiskRule(String riskRule) {
		this.riskRule = riskRule;
	}
	public int getRiskType() {
		return riskType;
	}
	public void setRiskType(int riskType) {
		this.riskType = riskType;
	}
	public int getWarningLv() {
		return warningLv;
	}
	public void setWarningLv(int warningLv) {
		this.warningLv = warningLv;
	}
	public String getRiskContent() {
		return riskContent;
	}
	public void setRiskContent(String riskContent) {
		this.riskContent = riskContent;
	}
	
	
}
