package com.beawan.model.dto;

import java.util.List;

public class SaveLrModelDto {
	private Integer modelId;//模型id
	private String modelName;//模型名字
	private List<SaveLeadDto> saveLeadDtos;//更改数据组
	public SaveLrModelDto() {
		// TODO Auto-generated constructor stub
	}
	
	public SaveLrModelDto(Integer modelId, String modelName, List<SaveLeadDto> saveLeadDtos) {
		super();
		this.modelId = modelId;
		this.modelName = modelName;
		this.saveLeadDtos = saveLeadDtos;
	}

	public Integer getModelId() {
		return modelId;
	}
	public void setModelId(Integer modelId) {
		this.modelId = modelId;
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public List<SaveLeadDto> getSaveLeadDtos() {
		return saveLeadDtos;
	}
	public void setSaveLeadDtos(List<SaveLeadDto> saveLeadDtos) {
		this.saveLeadDtos = saveLeadDtos;
	}

	@Override
	public String toString() {
		return "SaveLrModelDto [modelId=" + modelId + ", modelName=" + modelName + ", saveLeadDtos=" + saveLeadDtos
				+ "]";
	}
   
}
