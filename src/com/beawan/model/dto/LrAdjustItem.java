package com.beawan.model.dto;

import java.util.List;

import com.beawan.model.entity.LrAdjustItemEntity;
import com.beawan.model.entity.LrDimensionEntity;

/***
 * 贷款定价模型调整项
 */
public class LrAdjustItem {

	private String itemName;// 调整项名称

	private String fieldType;// 调整项值类型（数值型：number，字符串型：string）

	private String conitionType;// 条件类型（区间：interval，固定值：fixed）

	private String order;// 排序类型（降序：desc，升序：asc）

	private String state;// 使用状态

	private List<LrDimensionEntity> dimensionsList;

	public LrAdjustItem() {
		super();
	}

	public LrAdjustItem(String itemName, String fieldType, String conitionType, String order, String state,
			List<LrDimensionEntity> dimensionsList) {
		super();
		this.itemName = itemName;
		this.fieldType = fieldType;
		this.conitionType = conitionType;
		this.order = order;
		this.state = state;
		this.dimensionsList = dimensionsList;
	}

	public LrAdjustItem(LrAdjustItemEntity lrAdjustItemEntity, List<LrDimensionEntity> dimensionsList) {
		this.itemName = lrAdjustItemEntity.getName();
		this.fieldType = lrAdjustItemEntity.getFieldType();
		this.conitionType = lrAdjustItemEntity.getConitionType();
		this.order = lrAdjustItemEntity.getOrder();
		this.state = lrAdjustItemEntity.getState();
		this.dimensionsList = dimensionsList;

	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public String getConitionType() {
		return conitionType;
	}

	public void setConitionType(String conitionType) {
		this.conitionType = conitionType;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public List<LrDimensionEntity> getDimensionsList() {
		return dimensionsList;
	}

	public void setDimensionsList(List<LrDimensionEntity> dimensionsList) {
		this.dimensionsList = dimensionsList;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "LrAdjustItem [itemName=" + itemName + ", fieldType=" + fieldType + ", conitionType=" + conitionType
				+ ", order=" + order + ", state=" + state + ", dimensionsList=" + dimensionsList + "]";
	}

}
