package com.beawan.model.dao;

import com.beawan.core.BaseDao;
import com.beawan.model.entity.AdmitEntity;

/**
 * @author yzj
 */
public interface AdmitDao extends BaseDao<AdmitEntity> {
}
