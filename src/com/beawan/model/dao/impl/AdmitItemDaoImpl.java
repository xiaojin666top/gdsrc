package com.beawan.model.dao.impl;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.core.BaseDaoImpl;
import com.beawan.customer.bean.Admittance;
import com.beawan.model.dao.AdmitItemDao;
import com.beawan.model.entity.AdmitItemEntity;
import org.springframework.stereotype.Repository;

/**
 * @author yzj
 */
@Repository("admitItemDao")
public class AdmitItemDaoImpl extends BaseDaoImpl<AdmitItemEntity> implements AdmitItemDao{

}
