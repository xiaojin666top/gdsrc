package com.beawan.model.dao.impl;

import com.beawan.core.BaseDaoImpl;

import org.springframework.stereotype.Repository;

import com.beawan.model.dao.DimensionDao;
import com.beawan.model.entity.DimensionEntity;

/**
 * @author yzj
 */
@Repository("dimensionDao")
public class DimensionDaoImpl extends BaseDaoImpl<DimensionEntity> implements DimensionDao{

}
