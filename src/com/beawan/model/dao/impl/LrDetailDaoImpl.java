package com.beawan.model.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.model.dao.LrDetailDao;
import com.beawan.model.entity.LrDetailEntity;
import org.springframework.stereotype.Repository;
import com.beawan.model.entity.LrDetailEntity;

/**
 * @author yzj
 */
@Repository("lrDetailDao")
public class LrDetailDaoImpl extends BaseDaoImpl<LrDetailEntity> implements LrDetailDao{

}
