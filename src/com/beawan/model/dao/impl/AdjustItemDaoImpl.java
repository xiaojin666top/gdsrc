package com.beawan.model.dao.impl;

import com.beawan.core.BaseDaoImpl;

import org.springframework.stereotype.Repository;

import com.beawan.model.dao.AdjustItemDao;
import com.beawan.model.entity.AdjustItemEntity;

/**
 * @author yzj
 */
@Repository("adjustItemDao")
public class AdjustItemDaoImpl extends BaseDaoImpl<AdjustItemEntity> implements AdjustItemDao{

}
