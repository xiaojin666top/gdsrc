package com.beawan.model.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.model.dao.LrRecordDao;
import com.beawan.model.entity.LrRecordEntity;
import org.springframework.stereotype.Repository;
import com.beawan.model.entity.LrRecordEntity;

/**
 * @author yzj
 */
@Repository("lrRecordDao")
public class LrRecordDaoImpl extends BaseDaoImpl<LrRecordEntity> implements LrRecordDao{

}
