package com.beawan.model.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.model.dao.RiskWarningRecordDao;
import com.beawan.model.dto.RiskWarnResultDto;
import com.beawan.model.entity.RiskWarningRecord;

@Repository("riskWarningRecordDao")
public class RiskWarningRecordDaoImpl extends BaseDaoImpl<RiskWarningRecord> implements RiskWarningRecordDao {

	@Override
	public List<RiskWarnResultDto> getRiskWarningResult(String taskId) {
		StringBuilder sql = new StringBuilder();
		
		sql.append("select b.id id,b.task_id taskId,a.warn_name warnName,a.risk_rule riskRule,a.risk_type riskType,")
			.append("b.risk_result warningLv,b.risk_content riskContent")
			.append(" from DG_RK_WARNING a ")
			.append(" inner join DG_RK_WARNING_RECORD b on a.Risk_Code=b.risk_code")
			.append(" where b.risk_result is not null and b.risk_result<4 ")
			.append(" and b.task_id=:taskId");
		Map<String,Object> params = new HashMap<>();
		params.put("taskId", taskId);
		return findCustListBySql(RiskWarnResultDto.class, sql.toString(), params);
	}
	
}
