package com.beawan.model.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.model.dao.AdmitQuotaDao;
import com.beawan.model.entity.AdmitQuotaEntity;

@Repository("admitQuotaDao")
public class AdmitQuotaDaoImpl extends BaseDaoImpl<AdmitQuotaEntity> implements AdmitQuotaDao{
	
}
