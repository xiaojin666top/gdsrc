package com.beawan.model.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.model.dao.LrDimensionDao;
import com.beawan.model.entity.LrDimensionEntity;
import org.springframework.stereotype.Repository;
import com.beawan.model.entity.LrDimensionEntity;

/**
 * @author yzj
 */
@Repository("lrDimensionDao")
public class LrDimensionDaoImpl extends BaseDaoImpl<LrDimensionEntity> implements LrDimensionDao{

}
