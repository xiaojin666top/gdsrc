package com.beawan.model.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.model.dao.LrAdjustItemDao;
import com.beawan.model.entity.LrAdjustItemEntity;
import org.springframework.stereotype.Repository;
import com.beawan.model.entity.LrAdjustItemEntity;

/**
 * @author yzj
 */
@Repository("lrAdjustItemDao")
public class LrAdjustItemDaoImpl extends BaseDaoImpl<LrAdjustItemEntity> implements LrAdjustItemDao{

}
