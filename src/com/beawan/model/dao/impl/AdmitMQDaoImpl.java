package com.beawan.model.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.model.dao.AdmitMQDao;
import com.beawan.model.entity.AdmitMQEntity;


@Repository("admitMQDao")
public class AdmitMQDaoImpl extends BaseDaoImpl<AdmitMQEntity> implements AdmitMQDao{

}
