package com.beawan.model.dao.impl;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.core.BaseDaoImpl;
import com.beawan.model.dao.AdmitTemplateDao;
import com.beawan.model.entity.AdmitTemplateEntity;
import org.springframework.stereotype.Repository;
import com.beawan.model.entity.AdmitTemplateEntity;

/**
 * @author yzj
 */
@Repository("admitTemplateDao")
public class AdmitTemplateDaoImpl extends BaseDaoImpl<AdmitTemplateEntity> implements AdmitTemplateDao{

}
