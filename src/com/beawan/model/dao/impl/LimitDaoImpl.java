package com.beawan.model.dao.impl;

import com.beawan.core.BaseDaoImpl;

import org.springframework.stereotype.Repository;

import com.beawan.model.dao.LimitDao;
import com.beawan.model.entity.LimitEntity;

/**
 * @author yzj
 */
@Repository("limitDao")
public class LimitDaoImpl extends BaseDaoImpl<LimitEntity> implements LimitDao{

}
