package com.beawan.model.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.core.Pagination;
import com.beawan.model.dao.RiskWarningDao;
import com.beawan.model.entity.RiskWarning;

/**
 * @author yzj
 */
@Repository("riskWarningDao")
public class RiskWarningDaoImpl extends BaseDaoImpl<RiskWarning> implements RiskWarningDao{

	@Override
	public Pagination<RiskWarning> getPagerByRiskType(int page, int pageSize, Integer riskType) {
		// TODO Auto-generated method stub
		Map<String, Object> params = new HashMap<>();
		StringBuilder queryString = new StringBuilder();
		queryString.append("select id,RISK_CODE as riskCode,RISK_RULE riskRule,RISK_TYPE riskType,RISK_BASICS riskBasics,")
			.append(" status,update_time updateTime,RISK_LEVEL riskLevel from DG_RK_WARNING t where status！=3");
		if(riskType!=null && !"".equals(riskType+"")) {
			queryString.append(" and RISK_TYPE= :riskType");
			params.put("riskType", riskType);
		}
		String countString = "select count(*) from (" + queryString + ")";
		return findCustSqlPagination(RiskWarning.class, queryString, countString, params, page, pageSize);
	}

}
