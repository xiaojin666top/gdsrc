package com.beawan.model.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.model.dao.LeadRateLibDao;
import com.beawan.model.entity.LrLibEntity;
import org.springframework.stereotype.Repository;
import com.beawan.model.entity.LrLibEntity;

/**
 * @author yzj
 */
@Repository("leadRateLibDao")
public class LeadRateLibDaoImpl extends BaseDaoImpl<LrLibEntity> implements LeadRateLibDao{

}
