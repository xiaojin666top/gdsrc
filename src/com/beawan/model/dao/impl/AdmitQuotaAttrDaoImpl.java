package com.beawan.model.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.core.Pagination;
import com.beawan.model.dao.AdmitQuotaAttrDao;
import com.beawan.model.entity.AdmitQuotaAttrEntity;

@Repository("admitQuotaAttrDao")
public class AdmitQuotaAttrDaoImpl extends BaseDaoImpl<AdmitQuotaAttrEntity> implements AdmitQuotaAttrDao {

}
