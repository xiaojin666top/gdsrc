package com.beawan.model.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.model.dao.AdmitResultDao;
import org.springframework.stereotype.Repository;
import com.beawan.model.entity.AdmitResult;

/**
 * @author yzj
 */
@Repository("admitResultDao")
public class AdmitResultDaoImpl extends BaseDaoImpl<AdmitResult> implements AdmitResultDao{

}
