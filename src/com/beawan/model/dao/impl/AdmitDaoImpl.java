package com.beawan.model.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.core.BaseDaoImpl;
import com.beawan.model.dao.AdmitDao;
import com.beawan.model.entity.AdmitEntity;

/**
 * @author yzj
 */
@Repository("admitDao")
public class AdmitDaoImpl extends BaseDaoImpl<AdmitEntity> implements AdmitDao{

}
