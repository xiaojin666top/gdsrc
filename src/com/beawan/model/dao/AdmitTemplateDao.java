package com.beawan.model.dao;

import com.beawan.core.BaseDao;
import com.beawan.model.entity.AdmitTemplateEntity;

/**
 * @author yzj
 */
public interface AdmitTemplateDao extends BaseDao<AdmitTemplateEntity> {
}
