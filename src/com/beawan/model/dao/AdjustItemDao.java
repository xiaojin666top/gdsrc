package com.beawan.model.dao;

import com.beawan.core.BaseDao;
import com.beawan.model.entity.AdjustItemEntity;

/**
 * @author yzj
 */
public interface AdjustItemDao extends BaseDao<AdjustItemEntity> {
}
