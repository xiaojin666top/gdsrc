package com.beawan.model.dao;

import com.beawan.core.BaseDao;
import com.beawan.model.entity.LrDetailEntity;

/**
 * @author yzj
 */
public interface LrDetailDao extends BaseDao<LrDetailEntity> {
}
