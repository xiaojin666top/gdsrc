package com.beawan.model.dao;

import com.beawan.core.BaseDao;
import com.beawan.model.entity.AdmitItemEntity;

/**
 * @author yzj
 */
public interface AdmitItemDao extends BaseDao<AdmitItemEntity>  {
}
