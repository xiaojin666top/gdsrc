package com.beawan.model.dao;

import com.beawan.core.BaseDao;
import com.beawan.model.entity.DimensionEntity;

/**
 * @author yzj
 */
public interface DimensionDao extends BaseDao<DimensionEntity> {
}
