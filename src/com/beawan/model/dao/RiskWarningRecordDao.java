package com.beawan.model.dao;

import java.util.List;

import com.beawan.core.BaseDao;
import com.beawan.model.dto.RiskWarnResultDto;
import com.beawan.model.entity.RiskWarningRecord;

public interface RiskWarningRecordDao extends BaseDao<RiskWarningRecord>{

	List<RiskWarnResultDto> getRiskWarningResult(String taskId);

}
