package com.beawan.model.dao;

import java.util.Map;

import com.beawan.core.BaseDao;
import com.beawan.model.entity.AdmitQuotaAttrEntity;

public interface AdmitQuotaAttrDao extends BaseDao<AdmitQuotaAttrEntity>{

}
