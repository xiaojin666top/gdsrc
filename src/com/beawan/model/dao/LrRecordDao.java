package com.beawan.model.dao;

import com.beawan.core.BaseDao;
import com.beawan.model.entity.LrRecordEntity;

/**
 * @author yzj
 */
public interface LrRecordDao extends BaseDao<LrRecordEntity> {
}
