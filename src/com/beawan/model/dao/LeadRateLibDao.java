package com.beawan.model.dao;

import com.beawan.core.BaseDao;
import com.beawan.model.entity.LrLibEntity;

/**
 * @author yzj
 */
public interface LeadRateLibDao extends BaseDao<LrLibEntity> {
}
