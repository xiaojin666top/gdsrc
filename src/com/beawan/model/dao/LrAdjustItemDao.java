package com.beawan.model.dao;

import com.beawan.core.BaseDao;
import com.beawan.model.entity.LrAdjustItemEntity;

/**
 * @author yzj
 */
public interface LrAdjustItemDao extends BaseDao<LrAdjustItemEntity> {
}
