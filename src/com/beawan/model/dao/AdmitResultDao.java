package com.beawan.model.dao;

import com.beawan.core.BaseDao;
import com.beawan.model.entity.AdmitResult;

/**
 * @author yzj
 */
public interface AdmitResultDao extends BaseDao<AdmitResult> {
}
