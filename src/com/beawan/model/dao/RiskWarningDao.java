package com.beawan.model.dao;

import com.beawan.core.BaseDao;
import com.beawan.core.Pagination;
import com.beawan.model.entity.RiskWarning;

/**
 * @author yzj
 */
public interface RiskWarningDao extends BaseDao<RiskWarning> {

	Pagination<RiskWarning> getPagerByRiskType(int page, int pageSize, Integer riskType);
}
