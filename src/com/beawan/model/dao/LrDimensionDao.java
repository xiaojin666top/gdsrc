package com.beawan.model.dao;

import com.beawan.core.BaseDao;
import com.beawan.model.entity.LrDimensionEntity;

/**
 * @author yzj
 */
public interface LrDimensionDao extends BaseDao<LrDimensionEntity> {
}
