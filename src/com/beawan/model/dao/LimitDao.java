package com.beawan.model.dao;

import com.beawan.core.BaseDao;
import com.beawan.model.entity.LimitEntity;

/**
 * @author yzj
 */
public interface LimitDao extends BaseDao<LimitEntity> {
}
