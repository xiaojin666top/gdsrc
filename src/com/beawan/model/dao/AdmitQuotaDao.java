package com.beawan.model.dao;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDao;
import com.beawan.model.entity.AdmitItemEntity;
import com.beawan.model.entity.AdmitQuotaEntity;

@Repository("admitQuotaDao")
public interface AdmitQuotaDao extends BaseDao<AdmitQuotaEntity> {

}
