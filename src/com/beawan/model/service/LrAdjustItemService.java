package com.beawan.model.service;

import com.beawan.core.BaseService;
import com.beawan.model.entity.LrAdjustItemEntity;

/**
 * @author yzj
 */
public interface LrAdjustItemService extends BaseService<LrAdjustItemEntity> {
	
	
}
