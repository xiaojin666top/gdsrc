package com.beawan.model.service;

import com.beawan.core.BaseService;
import com.beawan.model.entity.AdmitItemEntity;

/**
 * @author yzj
 */
public interface AdmitItemService extends BaseService<AdmitItemEntity> {
}
