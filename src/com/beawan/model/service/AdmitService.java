package com.beawan.model.service;

import java.util.List;

import com.beawan.core.BaseService;
import com.beawan.core.ResultDto;
import com.beawan.model.dto.AdmitDto;
import com.beawan.model.entity.AdmitEntity;

/**
 * @author yzj
 */
public interface AdmitService extends BaseService<AdmitEntity>{

	ResultDto queryAdmitPaging(int page, int rows, String modelName);

	List<AdmitDto> queryAdmitInfo(int id);
	
	//获取所有的template列表
	List<AdmitDto> queryAdmitTemplate();
	//获得当前模型的具体内容
	List<AdmitDto> getTreeAdmit(int id);
	//在所有的template中标识当前模型中有的科目
	public List<AdmitDto> getAllWithOwn(int admitId);
	//保存准入信息
	void modifyAdmit(AdmitDto dto);
	
	void saveAdmitModel(AdmitEntity admitModel);
	
	AdmitEntity queryByIdentifCode(String identifCode);
	
	void modifyAdmitModel(AdmitEntity admitModel);
	
	boolean checkInModel(String quotaCode);
}
