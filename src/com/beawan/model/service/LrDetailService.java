package com.beawan.model.service;

import com.beawan.core.BaseService;
import com.beawan.model.entity.LrDetailEntity;

/**
 * @author yzj
 */
public interface LrDetailService extends BaseService<LrDetailEntity> {
}
