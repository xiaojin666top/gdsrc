package com.beawan.model.service;

import java.util.List;

import com.beawan.core.BaseService;
import com.beawan.core.Pagination;
import com.beawan.model.entity.AdmitQuotaEntity;

public interface AdmitQuotaService extends BaseService<AdmitQuotaEntity>{
	
	public void save(AdmitQuotaEntity admitQuota);
	
	public Pagination<AdmitQuotaEntity> getAdmitQuotaPager(String quotaName, int page, int pageSize);
	
	public AdmitQuotaEntity getAdmitQuotaEntityTree(String code);
	
	public void modify(AdmitQuotaEntity admitQuota) throws Exception;
	
	public List<AdmitQuotaEntity> queryParentQuota();
	
	public List<AdmitQuotaEntity> queryByPCode(String pCode);
	
}
