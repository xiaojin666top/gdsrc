package com.beawan.model.service;

import com.beawan.core.BaseService;
import com.beawan.model.entity.AdmitResult;

/**
 * @author yzj
 */
public interface AdmitResultService extends BaseService<AdmitResult> {


    /**
     *	营销准入分析
     * @param custNo
     * @throws Exception
     */
    void saveAdmitCheck(String custNo) throws Exception;

    /**
     * 获取客户准入分析的最新结果
     * @param custNo
     * @return
     * @throws Exception
     */
    public Integer getMaxTimes(String custNo)throws Exception;
}
