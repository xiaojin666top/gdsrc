package com.beawan.model.service;

import com.beawan.core.BaseService;
import com.beawan.model.dto.SaveLeadDto;
import com.beawan.model.entity.LrDimensionEntity;

/**
 * @author yzj
 */
public interface LrDimensionService extends BaseService<LrDimensionEntity> {
	
	public void saveOrUpdateByDto(SaveLeadDto saveLeadDto);
}
