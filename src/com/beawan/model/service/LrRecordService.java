package com.beawan.model.service;

import com.beawan.core.BaseService;
import com.beawan.model.entity.LrRecordEntity;

/**
 * @author yzj
 */
public interface LrRecordService extends BaseService<LrRecordEntity> {
}
