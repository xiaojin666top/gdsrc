package com.beawan.model.service;

import com.beawan.core.BaseService;
import com.beawan.core.ResultDto;
import com.beawan.model.dto.LimitDto;
import com.beawan.model.entity.LimitEntity;

/**
 * @author yzj
 */
public interface LimitService extends BaseService<LimitEntity> {
	ResultDto queryLimitList(int page,int rows,String name);
	
	LimitDto getLimitById(Long id);
	
	void updateLimit(LimitDto limitDto);
	
}
