package com.beawan.model.service;

import com.beawan.core.BaseService;
import com.beawan.core.Pagination;
import com.beawan.model.entity.AdmitTemplateEntity;

/**
 * @author yzj
 */
public interface AdmitTemplateService extends BaseService<AdmitTemplateEntity>{
	
	
	public Pagination<AdmitTemplateEntity> getAdmitTempPager(Integer deep, String name, int page, int pageSize);
}
