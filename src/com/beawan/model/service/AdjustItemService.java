package com.beawan.model.service;

import com.beawan.core.BaseService;
import com.beawan.model.dto.AdjustItemDto;
import com.beawan.model.entity.AdjustItemEntity;

/**
 * @author yzj
 */
public interface AdjustItemService extends BaseService<AdjustItemEntity> {
	
	AdjustItemDto getAdjustItemDtoById(Long id);
	void removeDimensionsById(Long adjustItemId);
}
