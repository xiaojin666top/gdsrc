package com.beawan.model.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.model.dao.LrDimensionDao;
import com.beawan.model.dto.SaveLeadDto;
import com.beawan.model.entity.LrDimensionEntity;
import com.beawan.model.service.LrDimensionService;

/**
 * @author yzj
 */
@Service("lrDimensionService")
public class LrDimensionServiceImpl extends BaseServiceImpl<LrDimensionEntity> implements LrDimensionService {
	/**
	 * 注入DAO
	 */
	@Resource(name = "lrDimensionDao")
	public void setDao(BaseDao<LrDimensionEntity> dao) {
		super.setDao(dao);
	}

	@Resource
	public LrDimensionDao lrDimensionDao;

	@Override
	public void saveOrUpdateByDto(SaveLeadDto saveLeadDto) {
		LrDimensionEntity lrDimensionEntity = lrDimensionDao.findByPrimaryKey(saveLeadDto.getLrdataid());
		if (null != saveLeadDto.getDownValue() ) {
			lrDimensionEntity.setDownValue(saveLeadDto.getDownValue());
		}
		if (null != saveLeadDto.getFlowValue() ) {
			lrDimensionEntity.setFlowValue(saveLeadDto.getFlowValue());
		}
		if (null != saveLeadDto.getUpValue() ) {
			lrDimensionEntity.setUpValue(saveLeadDto.getUpValue());
		}
		lrDimensionDao.saveOrUpdate(lrDimensionEntity);
	}
}
