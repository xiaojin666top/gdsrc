package com.beawan.model.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.core.Pagination;
import com.beawan.model.dao.AdmitTemplateDao;
import com.beawan.model.entity.AdmitTemplateEntity;
import com.beawan.model.service.AdmitTemplateService;

/**
 * @author yzj
 */
@Service("admitTemplateService")
public class AdmitTemplateServiceImpl extends BaseServiceImpl<AdmitTemplateEntity> implements AdmitTemplateService{
    /**
    * 注入DAO
    */
    @Resource(name = "admitTemplateDao")
    public void setDao(BaseDao<AdmitTemplateEntity> dao) {
        super.setDao(dao);
    }
    @Resource
    public AdmitTemplateDao admitTemplateDao;
    
    
	@Override
	public Pagination<AdmitTemplateEntity> getAdmitTempPager(Integer deep, String name, int page, int pageSize) {
		StringBuilder sql = new StringBuilder(" 1=1");
		Map<String,Object> params = new HashMap<String, Object>();
		if(name!=null && !"".equals(name)) {
			sql.append(" and name like :name");
			params.put("name", "%"+name+"%");
		}
		if(deep!=null && !"".equals(deep)) {
			sql.append(" and deep = :deep");
			params.put("deep", deep);
		}
		List<AdmitTemplateEntity> data = admitTemplateDao.selectRange(sql.toString(), page-1, pageSize, params);
		long total = admitTemplateDao.selectCount(sql.toString(), params);

		Pagination<AdmitTemplateEntity> pager = new Pagination<AdmitTemplateEntity>(page, pageSize, total);
		pager.setItems(data);
		return pager;
	}
}
