package com.beawan.model.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.core.ResultDto;
import com.beawan.model.dao.LeadRateLibDao;
import com.beawan.model.dao.LrAdjustItemDao;
import com.beawan.model.dao.LrDimensionDao;
import com.beawan.model.dto.LeadRateLib;
import com.beawan.model.dto.LrAdjustItem;
import com.beawan.model.dto.SaveLeadDto;
import com.beawan.model.dto.SaveLrModelDto;
import com.beawan.model.entity.AdmitEntity;
import com.beawan.model.entity.LrLibEntity;
import com.beawan.model.entity.LrAdjustItemEntity;
import com.beawan.model.entity.LrDimensionEntity;
import com.beawan.model.service.LeadRateLibService;
import com.beawan.model.service.LrAdjustItemService;
import com.beawan.model.service.LrDimensionService;
import com.ibm.db2.jcc.a.d;

import org.nutz.lang.random.ArrayRandom;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import com.beawan.model.entity.LrLibEntity;

/**
 * @author yzj
 */
@Service("leadRateLibService")
public class LeadRateLibServiceImpl extends BaseServiceImpl<LrLibEntity> implements LeadRateLibService {
	/**
	 * 注入DAO
	 */
	@Resource(name = "leadRateLibDao")
	public void setDao(BaseDao<LrLibEntity> dao) {
		super.setDao(dao);
	}

	@Resource
	public LeadRateLibDao leadRateLibDao;

	@Resource
	public LrAdjustItemService lrAdjustItemService;

	@Resource
	public LrDimensionService lrDimensionService;

	@Override
	/*通过库主键拿到所有调整项*/
	public LeadRateLib findByLeadRateId(Integer id) {
		LrLibEntity leadRateLibEntity = leadRateLibDao.findByPrimaryKey(id);
		List<LrAdjustItemEntity> lrAdjustItems = lrAdjustItemService.selectByProperty("leadId", id);
		List<LrAdjustItem> lrAdjustItemss = new ArrayList<LrAdjustItem>();
		if (lrAdjustItems != null) {
			for (LrAdjustItemEntity lrAdjustItemEntity : lrAdjustItems) {
				List<LrDimensionEntity> lrDimensionEntities = lrDimensionService.selectByProperty("lrAdjustItemId",
						lrAdjustItemEntity.getId());
				if(!"贷款期限".equals(lrAdjustItemEntity.getName())) {
					Collections.sort(lrDimensionEntities, new Comparator<LrDimensionEntity>() {
						@Override
						public int compare(LrDimensionEntity o1, LrDimensionEntity o2) {
							// TODO Auto-generated method stub
							Double d1 = o1.getFlowValue();
							Double d2 = o2.getFlowValue();
							if(d1 == null || d2 == null)
								return 0;
							return (Math.abs(o1.getFlowValue()) - Math.abs(o2.getFlowValue())) > 0? 1 : -1;
						}
					});
				}
				LrAdjustItem lrAdjustItem = new LrAdjustItem(lrAdjustItemEntity, lrDimensionEntities);
				lrAdjustItemss.add(lrAdjustItem);

			}
		}
		LeadRateLib leadRateLib = new LeadRateLib(leadRateLibEntity, lrAdjustItemss);
		return leadRateLib;
	}
    /*分页功能*/
	@Override
	public ResultDto queryLeadPaging(int page, int rows, String param) {
		ResultDto re = new ResultDto();
		StringBuilder sql = new StringBuilder("1=1");
		Map<String, Object> params = new HashMap<String, Object>();
		if (null != param && !"".equals(param)) {
			sql.append(" and name like :param");
			params.put("param", "%" + param + "%");
		}
		List<LrLibEntity> data = leadRateLibDao.selectRange(sql.toString(), page - 1, rows, params);
		long total = leadRateLibDao.selectCount(sql.toString(), params);
		re.setRows(data);
		re.setTotal(total);

		return re;

	}
     /*通过前台项更改利率*/
	@Override
	public void updateByDto(SaveLrModelDto saveLrModelDto) {

		if (null != saveLrModelDto.getModelName() && !"".equals(saveLrModelDto)) {
			LrLibEntity leadRateLibEntity = leadRateLibDao.findByPrimaryKey(saveLrModelDto.getModelId());
			leadRateLibEntity.setName(saveLrModelDto.getModelName());
			leadRateLibDao.saveOrUpdate(leadRateLibEntity);
		}

		for (SaveLeadDto saveLeadDto : saveLrModelDto.getSaveLeadDtos()) {
			lrDimensionService.saveOrUpdateByDto(saveLeadDto);
		}

	}

}
