package com.beawan.model.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.core.ResultDto;
import com.beawan.model.dao.LimitDao;
import com.beawan.model.dto.AdjustItemDto;
import com.beawan.model.dto.LimitDto;
import com.beawan.model.entity.AdjustItemEntity;
import com.beawan.model.entity.DimensionEntity;
import com.beawan.model.entity.LimitEntity;
import com.beawan.model.service.AdjustItemService;
import com.beawan.model.service.DimensionService;
import com.beawan.model.service.LimitService;
import com.platform.util.StringUtil;

/**
 * @author yzj
 */
@Service("limitService")
public class LimitServiceImpl extends BaseServiceImpl<LimitEntity> implements LimitService {
	/**
	 * 注入DAO
	 */
	@Resource(name = "limitDao")
	public void setDao(BaseDao<LimitEntity> dao) {
		super.setDao(dao);
	}

	@Resource
	public LimitDao limitDao;
	@Resource
	public AdjustItemService adjustItemService;
	@Resource
	public DimensionService dimensionService;

	@Override
	public ResultDto queryLimitList(int page, int rows, String name) {
		ResultDto resultDto = new ResultDto();
		StringBuilder sql = new StringBuilder("1=1");
		Map<String, Object> params = new HashMap<String, Object>();
		if (name != null && !"".equals(name)) {
			sql.append("and name like :name");
			params.put("name", "%" + name + "%");
		}
		List<LimitEntity> limitEntities = limitDao.selectRange(sql.toString(), page - 1, rows, params);
		Long total = limitDao.selectCount(sql.toString(), params);

		if (!CollectionUtils.isEmpty(limitEntities)) {
			resultDto.setRows(limitEntities);
			resultDto.setTotal(total);
		}
		return resultDto;
	}

	@Override
	public LimitDto getLimitById(Long id) {
		if (null == id)
			return null;
		LimitEntity limitEntity = limitDao.findByPrimaryKey(id);
		LimitDto limitDto = convertLimit2Dto(limitEntity);
		List<AdjustItemDto> adjustItemDtos = new ArrayList<AdjustItemDto>();
		List<AdjustItemEntity> adjustItemEntities = adjustItemService.selectByProperty("modelId", id);
		if (!CollectionUtils.isEmpty(adjustItemEntities)) {
			for (AdjustItemEntity adjustItemEntity : adjustItemEntities) {
				AdjustItemDto adjustItemDto = adjustItemService.getAdjustItemDtoById(adjustItemEntity.getId());
				adjustItemDtos.add(adjustItemDto);
			}
		}
		limitDto.setAdjustItemDtos(adjustItemDtos);
		return limitDto;
	}

	@Override
	public void updateLimit(LimitDto limitDto) {
		if (null == limitDto || limitDto.getId() == null)
			return;
		LimitEntity limitEntity = limitDao.findByPrimaryKey(limitDto.getId());
		if (StringUtil.isEmpty(limitDto.getName())) {
			limitEntity.setName(limitDto.getName());
		}
		limitDao.saveOrUpdate(limitEntity);
		List<AdjustItemDto> adjustItemDtos = limitDto.getAdjustItemDtos();
		if (!CollectionUtils.isEmpty(adjustItemDtos)) {
			for (AdjustItemDto adjustItemDto : adjustItemDtos) {
				Long adjustItemId = adjustItemDto.getId();
				if (null != adjustItemId) {
					adjustItemService.removeDimensionsById(adjustItemId);
				}
				if(CollectionUtils.isEmpty(adjustItemDto.getDimensionEntities())){
					continue;
				}
				for (DimensionEntity dimensionEntity : adjustItemDto.getDimensionEntities()) {
					dimensionEntity.setItemId(adjustItemId);
					dimensionService.saveOrUpdate(dimensionEntity);
				}
				/*if (null != adjustItemDto.getDimensionEntities() && adjustItemDto.getDimensionEntities().size() != 0) {
					for (DimensionEntity dimensionEntity : adjustItemDto.getDimensionEntities()) {
						dimensionEntity.setItemId(adjustItemId);
						dimensionService.saveOrUpdate(dimensionEntity);

					}
				}*/
			}
		}
	}

	private LimitDto convertLimit2Dto(LimitEntity limitEntity) {
		if (null == limitEntity)
			return null;
		LimitDto limitDto = new LimitDto();
		if (limitEntity.getId() != null) {
			limitDto.setId(limitEntity.getId());
		}
		if (!StringUtil.isEmpty(limitEntity.getName())) {
			limitDto.setName(limitEntity.getName());
		}
		if (!StringUtil.isEmpty(limitEntity.getDescription())) {
			limitDto.setDescription(limitEntity.getDescription());
		}
		if (!StringUtil.isEmpty(limitEntity.getFramework())) {
			limitDto.setFramework(limitEntity.getFramework());
		}
		if (!StringUtil.isEmpty(limitEntity.getAccountWay())) {
			limitDto.setAccountWay(limitEntity.getAccountWay());
		}
		if (limitEntity.getBaseRatio() != null) {
			limitDto.setBaseRatio(limitEntity.getBaseRatio());
		}
		return limitDto;
	}

}
