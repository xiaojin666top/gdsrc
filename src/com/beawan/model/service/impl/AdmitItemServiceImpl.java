package com.beawan.model.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.model.dao.AdmitItemDao;
import com.beawan.model.entity.AdmitItemEntity;
import com.beawan.model.service.AdmitItemService;

/**
 * @author yzj
 */
@Service("admitItemService")
public class AdmitItemServiceImpl extends BaseServiceImpl<AdmitItemEntity> implements AdmitItemService{
    /**
    * 注入DAO
    */
    @Resource(name = "admitItemDao")
    public void setDao(BaseDao<AdmitItemEntity> dao) {
        super.setDao(dao);
    }
    @Resource
    public AdmitItemDao admitItemDao;
}
