package com.beawan.model.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.corporateloan.entity.LrCustInfo;
import com.beawan.corporateloan.service.LrCustInfoService;
import com.beawan.model.dao.AdmitResultDao;
import com.beawan.model.entity.AdmitEntity;
import com.beawan.model.entity.AdmitQuotaEntity;
import com.beawan.model.entity.AdmitResult;
import com.beawan.model.service.AdmitQuotaService;
import com.beawan.model.service.AdmitResultService;
import com.beawan.model.service.AdmitService;
import com.beawan.qcc.dto.ExceptionsDto;
import com.beawan.qcc.dto.QccResultFullDatailDto;
import com.beawan.qcc.dto.ShiXinItemsDto;
import com.beawan.qcc.service.SynchronizationQCCService;
import com.beawan.survey.custInfo.dto.CompBaseDto;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.platform.util.DateUtil;
import com.platform.util.ExceptionUtil;
import com.platform.util.StringUtil;

/**
 * @author yzj
 */
@Service("admitResultService")
public class AdmitResultServiceImpl extends BaseServiceImpl<AdmitResult> implements AdmitResultService {
	private static final Logger log = Logger.getLogger(AdmitResultServiceImpl.class);

	/**
	 * 注入DAO
	 */
	@Resource(name = "admitResultDao")
	public void setDao(BaseDao<AdmitResult> dao) {
		super.setDao(dao);
	}

	@Resource
	public AdmitResultDao admitResultDao;

	@Resource
	public SynchronizationQCCService qccService;
	@Resource
	private AdmitResultService admitResultService;
	@Resource
	private AdmitService admitSV;
	@Resource
	protected ICompBaseSV compBaseSV;
	@Resource
	private AdmitQuotaService admitQuotaService;
	@Resource
	private LrCustInfoService lrCustInfoService;

	@Override
	public void saveAdmitCheck(String custNo) throws Exception {
		// 企查查全部数据
		QccResultFullDatailDto fullData = qccService.getQccFullByCustNo(custNo);
		if (fullData == null) {
			ExceptionUtil.throwException("进行营销准入分析中，获取企查查数据为空，客户号为：" + custNo);
		}

		List<ShiXinItemsDto> shiXinItems = fullData.getShiXinItems();
		Collections.sort(shiXinItems, new Comparator<ShiXinItemsDto>() {
			@Override
			public int compare(ShiXinItemsDto arg0, ShiXinItemsDto arg1) {
				// TODO Auto-generated method stub
//				Long diff;
//				try {
					return arg1.getLiandate().compareTo(arg0.getLiandate());
//					diff = DateUtil.parseDate(arg0.getLiandate()).getTime() - DateUtil.parseDate(arg1.getLiandate()).getTime();
//					return diff >=0?-1:1;
//				} catch (Exception e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				return 1;
			}
		});

		// 营销准入模型
		AdmitEntity md = admitSV.queryByIdentifCode("LOAN_RATAIL_MD");
		if (md == null || Constants.Model.STATUS_DIS == md.getState()) {
			// 营销准入模型未开启 直接准入分析结束
			return;
		}
		List<AdmitQuotaEntity> quotaList = md.getQuotaList();
		// 获取法人失信人
		// 获取企业失信人
		if (CollectionUtils.isEmpty(quotaList)) {
			return;
		}
		for (AdmitQuotaEntity quota : quotaList) {
			List<AdmitQuotaEntity> list = admitQuotaService.queryByPCode(quota.getQuotaCode());
			if (CollectionUtils.isEmpty(list)) {
				continue;
			}
			// 对上一次准入分析结果进行删除
			Integer maxTimes = admitResultService.getMaxTimes(custNo);
			Map<String, Object> queryMax = new HashMap<>();
			queryMax.put("times", maxTimes);
			queryMax.put("custNo", custNo);
			List<AdmitResult> admitResultList1 = admitResultService.selectByProperty(queryMax);
			if (admitResultList1 != null && admitResultList1.size() != 0) {
				for (AdmitResult result : admitResultList1) {
					result.setStatus(Constants.DELETE);
					admitResultService.saveOrUpdate(result);
				}
			}
			maxTimes = maxTimes + 1;

			// 准入分析异常结果集 -->暂时这边只有分析
			List<AdmitResult> admitResultList = new ArrayList<>();
			for (AdmitQuotaEntity entity : list) {
				// ggg 目前失信人名单不确定是否有个人的
				if (Constants.MD_ADMIT_QUOTA.LEGAL_SX.equals(entity.getQuotaCode())) {
					if (shiXinItems != null && shiXinItems.size() != 0) {
						AdmitResult admitResult = new AdmitResult();
						admitResult.setCustNo(custNo);
						admitResult.setTimes(maxTimes);
						admitResult.setQuotaName(entity.getQuotaName());
						admitResult.setResult("不可准入");
						StringBuilder desc = new StringBuilder();
						
						ShiXinItemsDto shiXinItem = shiXinItems.get(0);
						//直接做统计功能
						desc.append(shiXinItem.getName()+"共发生"+shiXinItems.size()+"起失信人记录。最近一笔立案日期是"
						+shiXinItem.getLiandate() + "，被"+shiXinItem.getExecutegov()+"列入失信人名单");
//						for (ShiXinItemsDto dto : shiXinItems) {
//							if(desc.length()>=1000){
//								break;
//							}
//							desc.append("立案日期：" + dto.getLiandate() + "，").append(dto.getName() + "被")
//									.append(dto.getExecutegov() + "列入失信人名单；");
//						}
						admitResult.setRealDesc(desc.toString());
						admitResultService.saveOrUpdate(admitResult);
						admitResultList.add(admitResult);
						LrCustInfo lrCust = lrCustInfoService.selectSingleByProperty("customerNo", custNo);
						if (lrCust != null) {
							lrCust.setCustomerLrType(Constants.LrCustType.BLACK_CUST);
							lrCust.setCustomerClass(Constants.CustClassify.INVALID);
							lrCustInfoService.saveOrUpdate(lrCust);
							log.info("客户号为： " + custNo + "存在失信人信息，客户转移到黑名单列表。");
						}
					}
				}

				// 获取经营异常信息
				if (Constants.MD_ADMIT_QUOTA.EXCEPTION.equals(entity.getQuotaCode())) {
					List<ExceptionsDto> exceptions = fullData.getExceptions();
					if (exceptions != null && exceptions.size() != 0) {
						AdmitResult admitResult = new AdmitResult();
						admitResult.setCustNo(custNo);
						admitResult.setTimes(maxTimes);
						admitResult.setQuotaName(entity.getQuotaName());
						admitResult.setResult("增信准入");
						StringBuilder desc = new StringBuilder();
						for (ExceptionsDto dto : exceptions) {
							desc.append(dto.getAddDate() + ",").append("被" + dto.getDecisionOffice())
									.append("列入经营异常原因：").append(dto.getAddReason());
						}
						admitResult.setRealDesc(desc.toString());
						admitResultService.saveOrUpdate(admitResult);
						admitResultList.add(admitResult);
					}
				}

				CompBaseDto compBase = compBaseSV.findCompBaseByCustNo(custNo);
				// 成立年限
				if (Constants.MD_ADMIT_QUOTA.BUILD_TIME.equals(entity.getQuotaCode())) {
					String foundDate = compBase.getFoundDate();
					if (StringUtil.isEmptyString(foundDate)) {
						AdmitResult admitResult = new AdmitResult();
						admitResult.setCustNo(custNo);
						admitResult.setTimes(maxTimes);
						admitResult.setQuotaName(entity.getQuotaName());
						admitResult.setResult("增信准入");
						admitResult.setRealDesc("当前企业成立时间未知，请调查核实");
						admitResultService.saveOrUpdate(admitResult);
						admitResultList.add(admitResult);
						continue;
					}
					String startYear = foundDate.substring(0, 4);
					int diff = DateUtil.getYear(new Date()) - Integer.parseInt(startYear);
					if (diff < 3) {
						AdmitResult admitResult = new AdmitResult();
						admitResult.setCustNo(custNo);
						admitResult.setTimes(maxTimes);
						admitResult.setQuotaName(entity.getQuotaName());
						admitResult.setResult("增信准入");
						admitResult.setRealDesc("企业成立时间是：" + foundDate.substring(0, 10));
						admitResultService.saveOrUpdate(admitResult);
						admitResultList.add(admitResult);
					}
				}

				// GGG 高管变化率 -->数据拿不到
			}
		}
	}

	@Override
	public Integer getMaxTimes(String custNo) throws Exception {
		Map<String, Object> params = new HashMap<>();
		StringBuilder sql = new StringBuilder();
		sql.append(" select max(times) from MD_ADMIT_RESULT where cust_no=:custNo and status=:status");
		params.put("custNo", custNo);
		params.put("status", Constants.NORMAL);
		Integer max = admitResultDao.findCustSingeBySql(sql, params);
		if (max == null) {
			return 1;
		}
		return max;
	}
}
