package com.beawan.model.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.Resource;

import com.beawan.model.dao.AdjustItemDao;
import com.beawan.model.dto.AdjustItemDto;
import com.beawan.model.entity.AdjustItemEntity;
import com.beawan.model.entity.DimensionEntity;
import com.beawan.model.service.AdjustItemService;
import com.beawan.model.service.DimensionService;
import com.platform.util.StringUtil;

/**
 * @author yzj
 */
@Service("adjustItemService")
public class AdjustItemServiceImpl extends BaseServiceImpl<AdjustItemEntity> implements AdjustItemService {
	/**
	 * 注入DAO
	 */
	@Resource(name = "adjustItemDao")
	public void setDao(BaseDao<AdjustItemEntity> dao) {
		super.setDao(dao);
	}

	@Resource
	public AdjustItemDao adjustItemDao;

	@Resource
	public DimensionService dimensionService;

	@Override
	public AdjustItemDto getAdjustItemDtoById(Long id) {
		if (null == id)
			return null;
		AdjustItemEntity adjustItemEntity = adjustItemDao.findByPrimaryKey(id);
		AdjustItemDto adjustItemDto = convertEntity2Dto(adjustItemEntity);
		List<DimensionEntity> dimensionEntities = dimensionService.selectByProperty("itemId", id);
		if (!CollectionUtils.isEmpty(dimensionEntities)) {
			adjustItemDto.setDimensionEntities(dimensionEntities);
		}
		return adjustItemDto;
	}

	@Override
	public void removeDimensionsById(Long adjustItemId) {
		if (null != adjustItemId) {
			List<DimensionEntity> dimensionEntities = dimensionService.selectByProperty("itemId", adjustItemId);
			if (!CollectionUtils.isEmpty(dimensionEntities)) {
				for (DimensionEntity dimensionEntity : dimensionEntities) {
					dimensionService.deleteEntity(dimensionEntity);
				}
			}
		}
	}

	private AdjustItemDto convertEntity2Dto(AdjustItemEntity adjustItemEntity) {
		if (null == adjustItemEntity)
			return null;
		AdjustItemDto adjustItemDto = new AdjustItemDto();
		if (null != adjustItemEntity.getId()) {
			adjustItemDto.setId(adjustItemEntity.getId());
		}
		if (!StringUtil.isEmpty(adjustItemEntity.getItemName())) {
			adjustItemDto.setItemName(adjustItemEntity.getItemName());
		}
		if (!StringUtil.isEmpty(adjustItemEntity.getFieldType())) {
			adjustItemDto.setFieldType(adjustItemEntity.getFieldType());
		}
		if (!StringUtil.isEmpty(adjustItemEntity.getConitionType())) {
			adjustItemDto.setConitionType(adjustItemEntity.getConitionType());
		}
		if (null != adjustItemEntity.getModelId()) {
			adjustItemDto.setModelId(adjustItemEntity.getModelId());
		}
		return adjustItemDto;
	}
}
