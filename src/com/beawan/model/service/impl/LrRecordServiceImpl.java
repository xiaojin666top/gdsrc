package com.beawan.model.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.model.dao.LrRecordDao;
import com.beawan.model.entity.LrRecordEntity;
import com.beawan.model.service.LrRecordService;

/**
 * @author yzj
 */
@Service("lrRecordService")
public class LrRecordServiceImpl extends BaseServiceImpl<LrRecordEntity> implements LrRecordService{
    /**
    * 注入DAO
    */
    @Resource(name = "lrRecordDao")
    public void setDao(BaseDao<LrRecordEntity> dao) {
        super.setDao(dao);
    }
    @Resource
    public LrRecordDao lrRecordDao;
}
