package com.beawan.model.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.model.dao.AdmitMQDao;
import com.beawan.model.entity.AdmitMQEntity;
import com.beawan.model.service.AdmitMQService;

@Service("admitMQService")
public class AdmitMQServiceImpl  extends BaseServiceImpl<AdmitMQEntity> implements AdmitMQService{
	/**
    * 注入DAO
    */
    @Resource(name = "admitMQDao")
    public void setDao(BaseDao<AdmitMQEntity> dao) {
        super.setDao(dao);
    }
    @Resource
    public AdmitMQDao admitMQDao;

}
