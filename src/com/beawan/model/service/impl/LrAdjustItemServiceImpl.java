package com.beawan.model.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.model.dao.LrAdjustItemDao;
import com.beawan.model.entity.LrAdjustItemEntity;
import com.beawan.model.service.LrAdjustItemService;

/**
 * @author yzj
 */
@Service("lrAdjustItemService")
public class LrAdjustItemServiceImpl extends BaseServiceImpl<LrAdjustItemEntity> implements LrAdjustItemService{
    /**
    * 注入DAO
    */
    @Resource(name = "lrAdjustItemDao")
    public void setDao(BaseDao<LrAdjustItemEntity> dao) {
        super.setDao(dao);
    }
    @Resource
    public LrAdjustItemDao lrAdjustItemDao;
}
