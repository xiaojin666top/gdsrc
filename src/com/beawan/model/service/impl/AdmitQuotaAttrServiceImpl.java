package com.beawan.model.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.model.dao.AdmitQuotaAttrDao;
import com.beawan.model.entity.AdmitQuotaAttrEntity;
import com.beawan.model.service.AdmitQuotaAttrService;

@Service("admitQuotaAttrService")
public class AdmitQuotaAttrServiceImpl extends BaseServiceImpl<AdmitQuotaAttrEntity> implements AdmitQuotaAttrService {

	/**
	 * 注入DAO
	 */
	@Resource(name = "admitQuotaAttrDao")
	public void setDao(BaseDao<AdmitQuotaAttrEntity> dao) {
		super.setDao(dao);
	}

	@Resource
	AdmitQuotaAttrDao admitQuotaAttrDao;

}
