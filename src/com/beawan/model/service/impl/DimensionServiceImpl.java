package com.beawan.model.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.beawan.model.dao.DimensionDao;
import com.beawan.model.entity.DimensionEntity;
import com.beawan.model.service.DimensionService;

/**
 * @author yzj
 */
@Service("dimensionService")
public class DimensionServiceImpl extends BaseServiceImpl<DimensionEntity> implements DimensionService{
    /**
    * 注入DAO
    */
    @Resource(name = "dimensionDao")
    public void setDao(BaseDao<DimensionEntity> dao) {
        super.setDao(dao);
    }
    @Resource
    public DimensionDao dimensionDao;
}
