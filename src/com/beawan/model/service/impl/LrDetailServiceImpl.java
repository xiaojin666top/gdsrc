package com.beawan.model.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.model.dao.LrDetailDao;
import com.beawan.model.entity.LrDetailEntity;
import com.beawan.model.service.LrDetailService;

/**
 * @author yzj
 */
@Service("lrDetailService")
public class LrDetailServiceImpl extends BaseServiceImpl<LrDetailEntity> implements LrDetailService{
    /**
    * 注入DAO
    */
    @Resource(name = "lrDetailDao")
    public void setDao(BaseDao<LrDetailEntity> dao) {
        super.setDao(dao);
    }
    @Resource
    public LrDetailDao lrDetailDao;
}
