package com.beawan.model.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.core.Pagination;
import com.beawan.model.dao.RiskWarningDao;
import com.beawan.model.dao.RiskWarningRecordDao;
import com.beawan.model.dto.RiskWarnResultDto;
import com.beawan.model.entity.RiskWarning;
import com.beawan.model.entity.RiskWarningRecord;
import com.beawan.model.service.RiskWarningService;

/**
 * @author yzj
 */
@Service("riskWarningService")
public class RiskWarningServiceImpl extends BaseServiceImpl<RiskWarning> implements RiskWarningService{
    /**
    * 注入DAO
    */
    @Resource(name = "riskWarningDao")
    public void setDao(BaseDao<RiskWarning> dao) {
        super.setDao(dao);
    }
    @Resource
    public RiskWarningDao riskWarningDao;
    @Resource
    public RiskWarningRecordDao riskWarningRecordDao;
    
    @Override
    public Pagination<RiskWarning> getPagerByRiskType(int page, int pageSize, Integer riskType){
    	return riskWarningDao.getPagerByRiskType(page, pageSize, riskType);
    }

	@Override
	public List<RiskWarnResultDto> getRiskWarningResult(String taskId, String customerNo) {
		// TODO Auto-generated method stub
		RiskWarnResultDto dto = new RiskWarnResultDto();
		List<RiskWarnResultDto> result = riskWarningRecordDao.getRiskWarningResult(taskId);
		return result;
	}
	
	//近三年高管变化率超过 50%
	/*@Override
	public String ICGG001(String taskId, List<TycChangeRecord> changeRecordList) throws Exception {
		
		
		double changeRate = countGGBHL(changeRecordList, 1080);
		
		String riskResult = ICGG001result(taskId, changeRate);
		
		return riskResult;
	}*/
	
	public String ICGG001result(String taskId, double changeRate) {
		String riskResult = "";
		String riskContent = "";
		
		if(changeRate > 0.5) {
			riskResult = Constants.Risk.LEVEL_1;
			riskContent = "近三年高管变化率为" + String.format("%.2f", changeRate*100) + "%";
		}
		
		saveRecord(taskId, "ICGG001", riskResult, riskContent);
		return riskResult;
	}
	
	//近一年之内高管变化率超过 30%
	/*@Override
	public String ICGG002(String taskId, List<TycChangeRecord> changeRecordList) throws Exception {
		
		double changeRate = countGGBHL(changeRecordList, 360);
		
		String riskResult = ICGG001result(taskId, changeRate);
		
		return riskResult;
	}*/
	
	public String ICGG002result(String taskId, double changeRate) {
		String riskResult = "";
		String riskContent = "";
		
		if(changeRate > 0.5) {
			riskResult = Constants.Risk.LEVEL_1;
			riskContent = "近一年高管变化率为" + String.format("%.2f", changeRate*100);
		}
		
		saveRecord(taskId, "ICGG002", riskResult, riskContent);
		return riskResult;
	}
	
	/*public double countGGBHL(List<TycChangeRecord> changeRecordList, int days) throws Exception {
		
		Date current = new Date();
		
		double changeRate = 0;
		
		//历次高管人员总数
		double totalNumber = 0;
		//变更次数总和
		double totalChange = 0;
		
		for (TycChangeRecord tycChangeRecord : changeRecordList) {
			//在变更记录中找高管变更记录，"董事（理事）、经理、监事" 这个标题的条目最像，但不确定这个标题是不是固定的，先用这个标题来找高管变更记录
			String item = tycChangeRecord.getChangeItem();
			if(item != null && !item.equals("") && item.equals("董事（理事）、经理、监事")) {
				Date time = DateUtil.parse(tycChangeRecord.getChangeTime(), "yyyy-MM-dd");
				int skipDays = DateUtil.getSkip(current, time, DateUtil.SKIP_TYPE_DAY);
				if( skipDays < days) {
					
					String after = tycChangeRecord.getContentAfter();
					Document doc1 = Jsoup.parse(after);
					Elements elements1 = doc1.select("a");
					//变更后人数
					int numberAfter = elements1.size();
					totalNumber += numberAfter;
					//变更后名单的新增人次
					int changeAfter = getStringNumber(after, "新增");
					totalChange += changeAfter;
					
					String before = tycChangeRecord.getContentBefore();
					Document doc2 = Jsoup.parse(before);
					Elements elements2 = doc2.select("a");
					//变更前人数
					int numberBefore = elements2.size();
					totalNumber += numberBefore;
					//变更前名单的退出人次
					int changeBefore = getStringNumber(before, "退出");
					totalChange += changeBefore;
				}
			}
		}
		if(totalChange == 0) {
			changeRate = 0;
		}else {
			changeRate = totalChange/(totalNumber/totalChange);
		}
		
		return changeRate;
	}*/
	
	//法定代表人近一年内出现变更
	/**
	 * 
	 * @param taskId
	 * @param recordList 变更记录
	 * @return
	 */
	/*@Override
	public String ICGG003(String taskId, List<TycChangeRecord> recordList) {
		
		String riskResult = "";
		String riskContent = "";
		if(recordList != null && recordList.size()>0) {
			for (TycChangeRecord tycChangeRecord : recordList) {
				String item = tycChangeRecord.getChangeItem();
				//不确定法定代表人变更条目的标题，猜测是"法定代表人"
				if(item != null && !item.equals("") && item.equals("法定代表人")) {
					riskResult = Constants.Risk.LEVEL_1;
					riskContent = "法定代表人近一年内出现变更";
					break;
				}
			}
			saveRecord(taskId, "ICGG003", riskResult, riskContent);
		}
		return riskResult;
	}*/
	
	//股权最高方近一年内出现在新增名单中
	/**
	 * 
	 * @param taskId
	 * @param name 股权最高方名称
	 * @param nameList 新增名单
	 * @return
	 */
	@Override
	public String ICGG004(String taskId, String name, List<String> nameList) {
		
		try {
			String riskResult = "";
			String riskContent = "";

			if(nameList.contains(name)) {
				riskResult = Constants.Risk.LEVEL_1;	
				riskContent = "股权最高方近一年内出现在新增名单中";
			}
			
			saveRecord(taskId, "ICGG004", riskResult, riskContent);
			
			return riskResult;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return null;
	}
	
	//近五年内发生借贷纠纷
	/**
	 * 
	 * @param taskId
	 * @return
	 */
	/*@Override
	public String SFYJ001(String taskId, List<TycSfLegal> legalList) {
		
		
		int times = 0;

		for (TycSfLegal tycSfLegal : legalList) {
			String item = tycSfLegal.getCasereason();
			//不确定民间借贷纠纷和金融借款合同纠纷的案由名字是什么，暂时用这两个来找
			if(item != null && !item.equals("")) {
				if(item.equals("民间借贷纠纷") || item.equals("金融借款合同纠纷")) {
					times ++;
				}
			}
		}
		
		String riskResult = SFYJ001result(taskId, times);
		
		return riskResult;
	}*/
	
	public String SFYJ001result(String taskId, int times) {
		String riskResult = "";
		String riskContent = "";
		
		if(times > 3) {
			riskResult = Constants.Risk.LEVEL_4;
		}else if(times > 0) {
			riskResult = Constants.Risk.LEVEL_3;
		}
		
		riskContent = "近五年内发生借贷纠纷" + times + "次";
		saveRecord(taskId, "SFYJ001", riskResult, riskContent);
		
		return riskResult;
	}
	
	//近 1 年股权转让纠纷
	/**
	 * 
	 * @param taskId
	 * @return
	 */
	/*@Override
	public String SFYJ002(String taskId,  List<TycSfLegal> legalList) {
		
		
		int times = 0;
		
		for (TycSfLegal tycSfLegal : legalList) {
			String item = tycSfLegal.getCasereason();
			//不确定股权转让纠纷的案由是什么，暂时用"股权转让纠纷"
			if(item !=null && !item.equals("") && item.equals("股权转让纠纷")) {
				times ++;
			}
		}
		
		String riskResult = SFYJ002result(taskId, times);
		
		return riskResult;
	}*/
	
	public String SFYJ002result(String taskId, int times) {
		String riskResult = "";
		String riskContent = "";
		
		if(times > 6) {
			riskResult = Constants.Risk.LEVEL_3;
		}else if(times > 3) {
			riskResult = Constants.Risk.LEVEL_2;
		}else if(times > 0) {
			riskResult = Constants.Risk.LEVEL_1;
		}
		
		riskContent = "近一年内发生股权转让纠纷" + times +"次";
		saveRecord(taskId, "SFYJ002", riskResult, riskContent);
		
		return riskResult;
	}
	
	//近 1 年失信被执行人次数
	/**
	 * 
	 * @param taskId
	 * @param dishonestList 失信被执行人记录
	 * @return
	 */
	/*@Override
	public String SFYJ003(String taskId, List<TycSfDishonest> dishonestList) {
		
		String riskResult = "";
		String riskContent = "";

		if(dishonestList!=null && dishonestList.size() > 0) {
			riskResult = Constants.Risk.LEVEL_4;
			riskContent = "近一年失信被执行人次数达" + dishonestList.size() + "人次";
		}
		
		saveRecord(taskId, "SFYJ003", riskResult, riskContent);
		
		return riskResult;
	}
	*/
	//企业电费同比下降
	/**
	 * 
	 * @param taskId
	 * @param lastYearEleCharge 上一年电费
	 * @param currentEleCharge	当期电费
	 * @return
	 */
	@Override
	public String SDYJ001(String taskId, double lastYearEleCharge, double currentEleCharge) {
		
		String riskResult = "";
		String riskContent = "";
		
		double value = (currentEleCharge - lastYearEleCharge)/lastYearEleCharge;
		
		if( value < -0.9) {
			riskResult = Constants.Risk.LEVEL_3;
		}else if(value < -0.6) {
			riskResult = Constants.Risk.LEVEL_2;
		}else if(value < -0.3) {
			riskResult = Constants.Risk.LEVEL_1;
		}
		riskContent = "企业电费同比下降" + String.format("%.2f", value*100*-1) + "%";
		saveRecord(taskId, "SDYJ001", riskResult, riskContent);
		return riskResult;
	}
	
	//企业电费环比下降
	/**
	 * 
	 * @param taskId
	 * @param lastMonthEleCharge 上一月电费
	 * @param currentEleCharge 当期电费
	 * @return
	 */
	public String SDYJ002(String taskId, double lastMonthEleCharge, double currentEleCharge) {
		
		String riskResult = "";
		String riskContent = "";
		
		double value = (currentEleCharge - lastMonthEleCharge)/lastMonthEleCharge;
		
		if( value < -0.9) {
			riskResult = Constants.Risk.LEVEL_3;
		}else if(value < -0.6) {
			riskResult = Constants.Risk.LEVEL_2;
		}else if(value < -0.3) {
			riskResult = Constants.Risk.LEVEL_1;
		}

		riskContent = "企业电费环比下降" + String.format("%.2f", value*100*-1) + "%";
		
		saveRecord(taskId, "SDYJ002", riskResult, riskContent);
		return riskResult;
	}
	
	//企业电费下降，销售收入大幅上升
	/**
	 * 
	 * @param taskId
	 * @param lastYearEleCharge 上一年电费
	 * @param currentEleCharge 当期电费
	 * @param lastIncome 上一年销售收入
	 * @param currentIncome 当期销售收入
	 * @return
	 */
	public String SDYJ003(String taskId, double lastYearEleCharge, double currentEleCharge, double lastIncome, double currentIncome) {
		
		String riskResult = "";
		String riskContent = "";
		
		double value = (currentEleCharge - lastYearEleCharge)/lastYearEleCharge + (currentIncome - lastIncome)/lastIncome;
		
		if(value > 0.9) {
			riskResult = Constants.Risk.LEVEL_3;
		}else if(value > 0.6){
			riskResult = Constants.Risk.LEVEL_2;
		}else if(value > 0.3) {
			riskResult = Constants.Risk.LEVEL_1;
		}
		riskContent = "企业电费与销售收入同比增长之和为" + String.format("%.2f", value*100*-1) + "%";
		
		saveRecord(taskId, "SDYJ003", riskResult, riskContent);
		return riskResult;
	}
	
	//企业电费与销售收入均下降
	/**
	 * 
	 * @param taskId
	 * @param lastMonthEleCharge 上一月电费
	 * @param currentEleCharge 当期电费
	 * @param lastIncome 上一年销售收入
	 * @param currentIncome 当期销售收入
	 * @return
	 */
	public String SDYJ004(String taskId, double lastMonthEleCharge, double currentEleCharge, double lastIncome, double currentIncome) {
		
		String riskResult = "";
		String riskContent = "";

		double eleValue = (currentEleCharge - lastMonthEleCharge)/lastMonthEleCharge;
		
		double incomeValue = (currentIncome - lastIncome)/lastIncome;
		
		if(eleValue < 0 && incomeValue < 0) {
			riskResult = Constants.Risk.LEVEL_3;
			riskContent = "企业电费下降" + String.format("%.2f", eleValue*100*-1) + "%, 销售收入下降" + String.format("%.2f", incomeValue*100*-1) + "%";
		}
		
		saveRecord(taskId, "SDYJ004", riskResult, riskContent);
		return riskResult;
	}
	
	//企业每万元产值能耗低于行业均值
	/**
	 * 
	 * @param taskId
	 * @param currentEleCharge 当期电费
	 * @param industryOutput 工业总产值
	 * @param tradeInduOutPut 行业均值
	 * @return
	 */
	public String SDYJ006(String taskId, double currentEleCharge, double industryOutput, double tradeInduOutPut) {
		
		String riskResult = "";
		String riskContent = "";
		
		double value = (currentEleCharge / industryOutput - tradeInduOutPut)/tradeInduOutPut;
		
		if(value < -0.9) {
			riskResult = Constants.Risk.LEVEL_3;
		}else if(value < -0.6) {
			riskResult = Constants.Risk.LEVEL_2;
		}else if(value < -0.3) {
			riskResult = Constants.Risk.LEVEL_1;
		}
		
		riskContent = "企业每万元产值能耗低于行业均值" + String.format("%.2f", value*100*-1) + "%";

		saveRecord(taskId, "SDYJ006", riskResult, riskContent);
		return riskResult;
	}
	
	//企业每万元产值能耗高于行业均值
	/**
	 * 
	 * @param taskId
	 * @param currentEleCharge 当期电费
	 * @param industryOutput 工业总产值
	 * @param tradeInduOutPut 行业均值
	 * @return
	 */
	public String SDYJ007(String taskId, double currentEleCharge, double industryOutput, double tradeInduOutPut) {
		
		String riskResult = "";
		String riskContent = "";

		double value = (currentEleCharge / industryOutput - tradeInduOutPut)/tradeInduOutPut;
		
		if(value > 0.9) {
			riskResult = Constants.Risk.LEVEL_3;
		}else if(value > 0.6) {
			riskResult = Constants.Risk.LEVEL_2;
		}else if(value > 0.3) {
			riskResult = Constants.Risk.LEVEL_1;
		}
		
		riskContent = "企业每万元产值能耗高于行业均值" + String.format("%.2f", value*100) + "%";
		
		saveRecord(taskId, "SDYJ007", riskResult, riskContent);
		return riskResult;
	}
	
	//近三个月内被信贷机构查询数量超过阈值
	/**
	 * 
	 * @param taskId
	 * @param times 近三个月内被信贷机构查询数量
	 * @return
	 */
	public String ZXYJ001(String taskId, int times) {
		
		String riskResult = "";
		String riskContent = "";
		
		if(times > 9) {
			riskResult = Constants.Risk.LEVEL_3;
		}else if(times > 6) {
			riskResult = Constants.Risk.LEVEL_2;
		}else if(times > 3) {
			riskResult = Constants.Risk.LEVEL_1;
		}
		
		riskContent = "近三个月内被信贷机构查询数量达" + times +"次";

		saveRecord(taskId, "ZXYJ001", riskResult, riskContent);
		return riskResult;
	}
	
	//近五年内存在不良贷款
	/**
	 *负债历史汇总信息
	 * @return
	 * @throws Exception
	 */
	/*public String ZXYJ002(String taskId, List<EB02CH> eb02chList) throws Exception {
		
		
		int times = 0;
		
		Date current = new Date();
		
		for (EB02CH eb02ch : eb02chList) {
			Date time = DateUtil.parse(eb02ch.getEb02cr01(), "yyyy-MM");
			if(DateUtil.getSkip(current, time, DateUtil.SKIP_TYPE_DAY) < 1800) {
				if(Integer.valueOf(eb02ch.getEb02cs04()) > 0) {
					times += Integer.valueOf(eb02ch.getEb02cs04());
				}
			}
		}
		
		String riskResult = ZXYJ002result(taskId, times);
		
		return riskResult;
	}*/
	
	public String ZXYJ002result(String taskId, int times) {
		String riskResult = "";
		String riskContent = "";
		
		if(times > 0) {
			riskResult = Constants.Risk.LEVEL_4;
			riskContent = "近五年内存在不良贷款" + times + "笔";
		}

		saveRecord(taskId, "ZXYJ002", riskResult, riskContent);
		return riskResult;
	}
	
	//近五年存在逾期天数(次数)超过阈值
	/**
	 *
	 * @return
	 * @throws Exception 
	 */
	/*public String ZXYJ003(String taskId, List<ED01B> ed01bList, List<EB02CH> eb02chList) throws Exception {
		
		
		int days = 0;
		int times = 0;

		Date current = new Date();
		
		for (EB02CH eb02ch : eb02chList) {
			Date time = DateUtil.parse(eb02ch.getEb02cr01(), "yyyy-MM");
			if(DateUtil.getSkip(current, time, DateUtil.SKIP_TYPE_DAY) < 1800) {
				if(Integer.valueOf(eb02ch.getEb02cs05()) > 0) {
					times += Integer.valueOf(eb02ch.getEb02cs05());
				}
			}
		}
		
		for (ED01B ed01b : ed01bList) {
			Date time = DateUtil.parse(ed01b.getEd01br01(), "yyyy-MM-dd");
			if(DateUtil.getSkip(current, time, DateUtil.SKIP_TYPE_DAY) < 1800) {
				if(Integer.valueOf(ed01b.getEd01bs02()) > 0) {
					days += Integer.valueOf(ed01b.getEd01bs02())*30;
				}
			}
		}
		
		String riskResult = ZXYJ003result(taskId, days, times);
		
		return riskResult;
	}*/
	
	public String ZXYJ003result(String taskId, int days, int times) {
		String riskResult = "";
		String riskContent = "";
		
		if(days >= 90) {
			riskResult = Constants.Risk.LEVEL_4;
		}else if(days >= 30) {
			riskResult = Constants.Risk.LEVEL_3;
		}else if(days > 0){
			riskResult = Constants.Risk.LEVEL_2;
		}
		
		riskContent = "近五年累计逾期天数达" + days + "天";
		
		if(times >= 3) {
			riskResult = Constants.Risk.LEVEL_4;
			riskContent += "，近五年累计逾期次数达" + times + "次";
		}
		saveRecord(taskId, "ZXYJ003", riskResult, riskContent);
		return riskResult;
	}
	
	//融资银行数量超过阈值
	/*public String ZXYJ004(String taskId, EB01A eb01a) {
		
		String riskResult = "";
		String riskContent = "";
		
		int number = Integer.valueOf(eb01a.getEb01as02());
		
		if(number > 9) {
			riskResult = Constants.Risk.LEVEL_3;
		}else if(number > 6) {
			riskResult = Constants.Risk.LEVEL_2;
		}else if(number >3 ) {
			riskResult = Constants.Risk.LEVEL_1;
		}

		riskContent = "未结清贷款机构达" + number + "家";
		
		saveRecord(taskId, "ZXYJ004", riskResult, riskContent);
		return riskResult;
	}*/
	
	//未结清贷款余额总数大于经营活动现金流入
	/**
	 * 
	 * @param taskId
	 * @param WJQDKYE 未结清贷款余额
	 * @param JYHDXJLR 经营活动现金流入
	 * @return
	 */
	public String ZXYJ007(String taskId, double WJQDKYE, double JYHDXJLR) {
		
		String riskResult = "";
		String riskContent = "";

		if(WJQDKYE > JYHDXJLR) {
			riskResult = Constants.Risk.LEVEL_1;
			riskContent = "未结清贷款余额大于经营活动现金流入";
		}
		
		saveRecord(taskId, "ZXYJ007", riskResult, riskContent);
		return riskResult;
	}
	
	//担保金额超过企业净资产
	/**
	 * 
	 * @param taskId
	 * @param DBJE 担保金额
	 * @param QYJZC 企业净资产
	 * @return
	 */
	public String ZXYJ008(String taskId, double DBJE, double QYJZC) {
		
		String riskResult = "";
		String riskContent = "";
		
		if(DBJE > QYJZC) {
			riskResult = Constants.Risk.LEVEL_1;
			riskContent = "担保金额总数大于企业净资产";
		}

		saveRecord(taskId, "ZXYJ008", riskResult, riskContent);
		return riskResult;
	}
	
	//关联企业相互担保
	/**
	 * 
	 * @param taskId
	 * @param number 关联企业相互担保数量
	 * @return
	 */
	public String ZXYJ009(String taskId, int number) {
		
		String riskResult = "";
		String riskContent = "";
		
		if(number > 1) {
			riskResult = Constants.Risk.LEVEL_4;
			riskContent = "关联企业相互担保数量达到" + number + "次";
		}

		saveRecord(taskId, "ZXYJ009", riskResult, riskContent);
		return riskResult;
	}
	
	//短期借款+长期借款+应付票据与融资余额不匹配
	/**
	 * 
	 * @param taskId
	 * @param DQJK 短期借款
	 * @param CQJK 长期借款
	 * @param YFPJ 应付票据
	 * @param RZYE 融资余额
	 * @return
	 */
	public String ZXYJ010(String taskId, double DQJK, double CQJK, double YFPJ, double RZYE) {
		
		String riskResult = "";
		String riskContent = "";
		
		if( (DQJK + CQJK + YFPJ) > RZYE) {
			riskResult = Constants.Risk.LEVEL_1;
			riskContent = "可能存在民间融资";
		}else if( (DQJK + CQJK + YFPJ) < RZYE) {
			riskResult = Constants.Risk.LEVEL_1;
			riskContent = "可能存在财务报表欺诈";
		}

		saveRecord(taskId, "ZXYJ010", riskResult, riskContent);
		return riskResult;
	}
	@Transactional
	public void saveRecord(String taskId, String riskCode, String riskResult, String riskContent) {
		StringBuilder hql = new StringBuilder();
		if(taskId != null && riskCode != null) {
			hql.append("taskId = :taskId and riskCode = :riskCode");
			Map<String, Object> params = new HashMap<>();
			params.put("taskId", taskId);
			params.put("riskCode", riskCode);
			RiskWarningRecord record = riskWarningRecordDao.selectSingle(hql.toString(), params);
			
			if(record == null) {
				record  = new RiskWarningRecord();
				record.setTaskId(taskId);
				record.setRiskCode(riskCode);
			}
			
			record.setRiskResult(riskResult);
			record.setRiskContent(riskContent);
			riskWarningRecordDao.saveOrUpdate(record);
		}
		
		
	}
	
	public int getStringNumber(String oriStr, String str) {
		int fromIndex = 0;
		int count = 0;
		
		while(true) {
			int flag = oriStr.indexOf(str, fromIndex);
			if(flag != -1) {
				fromIndex = flag + 1;
				count ++;
			}else {
				break;
			}
		}
		
		return count;
	}
}
