package com.beawan.model.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.core.ResultDto;
import com.beawan.model.dao.AdmitDao;
import com.beawan.model.dao.AdmitItemDao;
import com.beawan.model.dao.AdmitMQDao;
import com.beawan.model.dao.AdmitQuotaAttrDao;
import com.beawan.model.dao.AdmitQuotaDao;
import com.beawan.model.dto.AdmitDto;
import com.beawan.model.entity.AdmitEntity;
import com.beawan.model.entity.AdmitItemEntity;
import com.beawan.model.entity.AdmitMQEntity;
import com.beawan.model.entity.AdmitQuotaAttrEntity;
import com.beawan.model.entity.AdmitQuotaEntity;
import com.beawan.model.service.AdmitMQService;
import com.beawan.model.service.AdmitQuotaService;
import com.beawan.model.service.AdmitService;

/**
 * @author yzj
 */
@Service("admitService")
public class AdmitServiceImpl extends BaseServiceImpl<AdmitEntity> implements AdmitService{
    /**
    * 注入DAO
    */
    @Resource(name = "admitDao")
    public void setDao(BaseDao<AdmitEntity> dao) {
        super.setDao(dao);
    }
    @Resource
    public AdmitDao admitDao;
    @Resource
    public AdmitItemDao admitItemDao;
    @Resource
    public AdmitMQDao admitMQDao;
    @Resource
    public AdmitQuotaDao admitQuotaDao;
    @Resource
    public AdmitQuotaAttrDao admitQuotaAttrDao;
    @Resource
    public AdmitQuotaService admitQuotaService;
    @Resource
    public AdmitMQService admitMQservice;
    
    
	@Override
	public ResultDto queryAdmitPaging(int page, int rows, String modelName) {
		ResultDto re = new ResultDto();
		StringBuilder sql = new StringBuilder(" status=:status");
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("status", Constants.NORMAL);
		if(modelName!=null && !"".equals(modelName)) {
			sql.append(" and name like '%:modelName%'");
			params.put("modelName", modelName);
		}
		List<AdmitEntity> data = admitDao.selectRange(sql.toString(), page-1, rows, params);
		long total = admitDao.selectCount(sql.toString(), params);
		re.setRows(data);
		re.setTotal(total);
		return re;
	}
	
	@Override
	public List<AdmitDto> queryAdmitInfo(int id) {
		StringBuilder sql = new StringBuilder();
		Map<String, Object> params = new HashMap<String,Object>();
		sql.append("SELECT B.ID admitId,B.NAME admitName,B.IDENTIF_CODE identifCode,")
		.append(" C.DEEP deep,C.PARENT_ID parentId,C.NAME itemName,A.ID itemId,C.ID templateId")
		.append(" FROM DG_MD_ADMIT_ITEM A ")
		.append(" JOIN DG_MD_ADMIT B ON A.ADMIT_ID=B.ID")
		.append(" JOIN DG_MD_ADMIT_TEMPLATE C ON A.TEMPLATE_ID=C.ID")
		.append(" WHERE B.ID=:id");
		params.put("id", id);
		List<AdmitDto> list = admitDao.findCustListBySql(AdmitDto.class, sql.toString(), params);
		return list;
	}
	
	@Override
	public List<AdmitDto> queryAdmitTemplate(){
		StringBuilder sql = new StringBuilder();
		Map<String, Object> params = new HashMap<String,Object>();
		sql.append("SELECT C.DEEP deep,C.PARENT_ID parentId,C.NAME itemName,C.ID templateId")
		.append(" FROM DG_MD_ADMIT_TEMPLATE C ");
		List<AdmitDto> list = admitDao.findCustListBySql(AdmitDto.class, sql.toString(), params);
		return list;
	}
	
	@Override
	public List<AdmitDto> getTreeAdmit(int id) {
		// TODO Auto-generated method stub
		List<AdmitDto> rootNodes = queryAdmitInfo(id);
		List<AdmitDto> re = new ArrayList<AdmitDto>();
		for(AdmitDto root : rootNodes) {
			if(root.getParentId()==0) {
				re.add(root);
			}
		}
		for(AdmitDto root : re) {
			root.setNextNodes(getChild(root.getTemplateId(),rootNodes));
		}
		return re;
	}
	
	@Override
	public List<AdmitDto> getAllWithOwn(int admitId){
		List<AdmitDto> templates = queryAdmitTemplate();
		List<AdmitItemEntity> owns = admitItemDao.selectByProperty("admitId", admitId);
		if(owns==null||owns.size()<=0)
			return templates;
		for(AdmitDto dto : templates) {
			for(AdmitItemEntity entity : owns) {
				if(dto.getTemplateId()==entity.getTemplateId()) {
					dto.setFlag(Constants.Model.FALG_TRUE);
					dto.setScore(entity.getScore());
					dto.setWeight(entity.getWeight());
					continue;
				}
			}
		}
		
		//计算每个节点有属于自己的有几个
		for(AdmitDto a : templates) {
			for(AdmitDto b : templates) {
				if(a.getTemplateId()==b.getParentId() && b.getFlag()==Constants.Model.FALG_TRUE) {
					a.setUseCount(a.getUseCount()+1);
				}
			}
		}
		
		List<AdmitDto> re = new ArrayList<AdmitDto>();
		for(AdmitDto root : templates) {
			if(root.getParentId()==0) {
				re.add(root);
			}
		}
		for(AdmitDto root : re) {
			root.setNextNodes(getChild(root.getTemplateId(),templates));
		}
		return re;
	}
	
	private List<AdmitDto> getChild(int templateId, List<AdmitDto> rootNodes) {
		List<AdmitDto> childList = new ArrayList<AdmitDto>();
		for(AdmitDto dto : rootNodes) {
			if(dto.getParentId()==templateId) {
				childList.add(dto);
			}
		}
		// 把子节点的子节点在遍历一边
		for(AdmitDto dto : childList) {
			dto.setNextNodes(getChild(dto.getTemplateId(), rootNodes));
		}
		if(childList.size() == 0)
			return null;
		return childList;
	}

	@Override
	public void modifyAdmit(AdmitDto dto) {
		if(dto==null)
			return ;
		int admitId = dto.getAdmitId();
		AdmitEntity admitEntity = admitDao.findByPrimaryKey(admitId);
		admitEntity.setTotalScore(dto.getScore());
		admitEntity.setName(dto.getAdmitName());
		admitDao.saveOrUpdate(admitEntity);
		
		//删除原来的数据
		List<AdmitItemEntity> removeList = admitItemDao.selectByProperty("admitId", admitId);
		if(removeList!=null&&removeList.size()>=0) {
			for(AdmitItemEntity remove : removeList) {
				admitItemDao.deleteEntity(remove);
			}
		}
		
		List<AdmitDto> firstList = dto.getNextNodes();
		if(firstList==null || firstList.size() ==0)
			return;
		for(AdmitDto fir : firstList) {
			AdmitItemEntity item = new AdmitItemEntity();
			item.setAdmitId(admitId);
			item.setTemplateId(fir.getTemplateId());
			item.setWeight(fir.getWeight());
			admitItemDao.saveOrUpdate(item);
			
			List<AdmitDto> secondList = fir.getNextNodes();
			if(secondList==null || secondList.size() ==0)
				continue;
			for(AdmitDto sec : secondList) {
				AdmitItemEntity secItem = new AdmitItemEntity();
				secItem.setAdmitId(admitId);
				secItem.setTemplateId(sec.getTemplateId());
				secItem.setWeight(sec.getWeight());
				admitItemDao.saveOrUpdate(secItem);
				
				List<AdmitDto> thirdList = sec.getNextNodes();
				if(thirdList==null || thirdList.size() ==0)
					continue;
				for(AdmitDto thi : thirdList) {
					AdmitItemEntity thiItem = new AdmitItemEntity();
					thiItem.setAdmitId(admitId);
					thiItem.setTemplateId(thi.getTemplateId());
					thiItem.setWeight(thi.getWeight());
					thiItem.setScore(thi.getScore());
					admitItemDao.saveOrUpdate(thiItem);
				}
			}
		}
	}
	
	@Override
	public void saveAdmitModel(AdmitEntity admitModel){
		Timestamp createTime = new Timestamp(System.currentTimeMillis()); 
		admitModel.setCreateTime(createTime);
		admitModel.setStatus(2);
		
		admitDao.saveOrUpdate(admitModel);
		
		List<AdmitQuotaEntity> quotaList = admitModel.getQuotaList();
		
		for (AdmitQuotaEntity quota : quotaList) {
			//保存父指标与模型关联关系
			AdmitMQEntity admitMQ_parent = new AdmitMQEntity();
			admitMQ_parent.setIdentifCode(admitModel.getIdentifCode());
			admitMQ_parent.setQuotaCode(quota.getQuotaCode());
			admitMQDao.saveOrUpdate(admitMQ_parent);
			
			List<AdmitQuotaEntity> subQuotaDtos = quota.getSubQuotaDtos();
			for (AdmitQuotaEntity child : subQuotaDtos) {
				//保存子指标与模型关联关系
				AdmitMQEntity admitMQ_child = new AdmitMQEntity();
				admitMQ_child.setIdentifCode(admitModel.getIdentifCode());
				admitMQ_child.setParentCode(quota.getQuotaCode());
				admitMQ_child.setQuotaCode(child.getQuotaCode());
				admitMQDao.saveOrUpdate(admitMQ_child);
				
				List<AdmitQuotaAttrEntity> actulOptions = child.getActulOptions();
				for (AdmitQuotaAttrEntity attr : actulOptions) {
					//保存指标项与模型关联关系
					AdmitMQEntity admitMQ_attr = new AdmitMQEntity();
					admitMQ_attr.setIdentifCode(admitModel.getIdentifCode());
					admitMQ_attr.setParentCode(child.getQuotaCode());
					admitMQ_attr.setAttrId(attr.getId());
					admitMQ_attr.setOptionCode(attr.getOptionCode());
					admitMQDao.saveOrUpdate(admitMQ_attr);
				}
			}
		}
	}
    
	@Override
	public AdmitEntity queryByIdentifCode(String identifCode){
		AdmitEntity admitModel = admitDao.selectSingleByProperty("identifCode", identifCode);
		StringBuilder sql = new StringBuilder(" 1=1");
		Map<String, Object> params = new HashMap();
		if(identifCode != null || "".equals(identifCode)){
			sql.append(" and identifCode = :identifCode");
			params.put("identifCode", identifCode);
		}else{
			return null;
		}
		
		sql.append(" and parentCode is NULL");
		List<AdmitMQEntity> admitMQList = admitMQDao.select(sql.toString(), params);
		
		List<AdmitQuotaEntity> quotaList = new ArrayList(); 
		for (AdmitMQEntity admitMQ_parent : admitMQList) {
			String quotaCode = admitMQ_parent.getQuotaCode();
			AdmitQuotaEntity quotaParent = admitQuotaDao.selectSingleByProperty("quotaCode", quotaCode);
			sql = new StringBuilder(" 1=1");
			sql.append(" and identifCode = :identifCode");
			sql.append(" and parentCode = :parentCode");
			params.put("parentCode", quotaCode);
			List<AdmitMQEntity> admitMQ_childList = admitMQDao.select(sql.toString(), params);
			
			List<AdmitQuotaEntity> childList = new ArrayList();
			for (AdmitMQEntity admitMQ_child : admitMQ_childList) {
				String childCode = admitMQ_child.getQuotaCode();
				AdmitQuotaEntity quotaChild = admitQuotaDao.selectSingleByProperty("quotaCode", childCode);
				sql = new StringBuilder(" 1=1");
				sql.append(" and identifCode = :identifCode");
				sql.append(" and parentCode = :parentCode");
				params.put("parentCode", childCode);
				List<AdmitMQEntity> admitMQ_attrList = admitMQDao.select(sql.toString(),  params);
				
				List<AdmitQuotaAttrEntity> attrList = new ArrayList();
				for (AdmitMQEntity admitMQ_attr : admitMQ_attrList) {
					int attrId = admitMQ_attr.getAttrId();
					AdmitQuotaAttrEntity attr = admitQuotaAttrDao.selectSingleByProperty("id", attrId);
					attrList.add(attr);
				}
				quotaChild.setActulOptions(attrList);
				
				List<AdmitQuotaAttrEntity> options = admitQuotaAttrDao.selectByProperty("quotaCode", childCode);
				quotaChild.setOptions(options);
				
				childList.add(quotaChild);
				
			}
			quotaParent.setSubQuotaDtos(childList);
			quotaList.add(quotaParent);
		}
		admitModel.setQuotaList(quotaList);	
		return admitModel;
	}

	@Override
	public void modifyAdmitModel(AdmitEntity admitModel) {
		// TODO Auto-generated method stub
		admitDao.saveOrUpdate(admitModel);
		
		List<AdmitMQEntity> AdmitMQList = admitMQDao.selectByProperty("identifCode", admitModel.getIdentifCode());
		for (AdmitMQEntity admitMQ : AdmitMQList) {
			admitMQDao.deleteEntity(admitMQ);
		}
		
		saveAdmitModel(admitModel);
	}

	@Override
	public boolean checkInModel(String quotaCode) {
		// TODO Auto-generated method stub
		
		AdmitQuotaEntity parentQuota = admitQuotaService.getAdmitQuotaEntityTree(quotaCode);
		
		if(parentQuota != null){
			List<AdmitQuotaEntity> childList = parentQuota.getSubQuotaDtos();
			for (AdmitQuotaEntity child : childList) {
				List<AdmitQuotaAttrEntity> attrList = child.getOptions();
				for (AdmitQuotaAttrEntity attr : attrList) {
					List<AdmitMQEntity> admitMQs = admitMQservice.selectByProperty("attrId", attr.getId());
					if(admitMQs != null && admitMQs.size()>0){
						return false;
					}
				}
			}
		}
		return true;
	}
}
