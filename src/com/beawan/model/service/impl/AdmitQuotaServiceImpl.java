package com.beawan.model.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.core.Pagination;
import com.beawan.model.dao.AdmitQuotaAttrDao;
import com.beawan.model.dao.AdmitQuotaDao;
import com.beawan.model.entity.AdmitQuotaAttrEntity;
import com.beawan.model.entity.AdmitQuotaEntity;
import com.beawan.model.service.AdmitQuotaService;

@Service("admitQuotaService")
public class AdmitQuotaServiceImpl extends BaseServiceImpl<AdmitQuotaEntity> implements AdmitQuotaService {

	@Resource(name = "admitQuotaDao")
	public void setDao(BaseDao<AdmitQuotaEntity> dao) {
		super.setDao(dao);
	}

	@Resource
	AdmitQuotaDao admitQuotaDao;
	@Resource
	AdmitQuotaAttrDao admitQuotaAttrDao;

	@Override
	public void save(AdmitQuotaEntity admitQuota) {
		// 保存父指标
		admitQuotaDao.saveOrUpdate(admitQuota);
		List<AdmitQuotaEntity> childList = admitQuota.getSubQuotaDtos();
		for (AdmitQuotaEntity admitQuotaEntity : childList) {
			admitQuotaEntity.setParentCode(admitQuota.getQuotaCode());
			List<AdmitQuotaAttrEntity> attrList = admitQuotaEntity.getOptions();
			// 保存子指标
			admitQuotaDao.saveOrUpdate(admitQuotaEntity);
			for (AdmitQuotaAttrEntity admitQuotaAttrEntity : attrList) {
				admitQuotaAttrEntity.setQuotaCode(admitQuotaEntity.getQuotaCode());
				// 保存指标的属性项
				admitQuotaAttrDao.saveOrUpdate(admitQuotaAttrEntity);
			}
		}
	}

	@Override
	public Pagination<AdmitQuotaEntity> getAdmitQuotaPager(String quotaName, int page, int pageSize) {
		StringBuilder sql = new StringBuilder(" status=:status");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("status", Constants.NORMAL);
		if (quotaName != null && !"".equals(quotaName)) {
			sql.append(" and quotaName like :quotaName");
			params.put("quotaName", "%" + quotaName + "%");
		}
		sql.append(" and parentCode is NULL");
		List<AdmitQuotaEntity> data = admitQuotaDao.selectRange(sql.toString(), (page - 1) * pageSize, pageSize,
				params);
		long total = admitQuotaDao.selectCount(sql.toString(), params);

		Pagination<AdmitQuotaEntity> pager = new Pagination<AdmitQuotaEntity>(page, pageSize, total);
		pager.setItems(data);
		return pager;
	}

	@Override
	public AdmitQuotaEntity getAdmitQuotaEntityTree(String code) {
		AdmitQuotaEntity admitQuota = admitQuotaDao.selectSingleByProperty("quotaCode", code);
		if (admitQuota != null && admitQuota.getQuotaCode() != null) {
			List<AdmitQuotaEntity> childList = admitQuotaDao.selectByProperty("parentCode", admitQuota.getQuotaCode());
			for (AdmitQuotaEntity child : childList) {
				if (child.getQuotaCode() != null) {
					List<AdmitQuotaAttrEntity> attrList = admitQuotaAttrDao.selectByProperty("quotaCode",
							child.getQuotaCode());
					child.setOptions(attrList);
				}
			}
			admitQuota.setSubQuotaDtos(childList);
		}

		return admitQuota;
	}

	public void modify(AdmitQuotaEntity admitQuota) throws Exception {
		List<AdmitQuotaEntity> childs = admitQuotaDao.selectByProperty("parentCode", admitQuota.getQuotaCode());
		for (AdmitQuotaEntity child : childs) {
			if (child != null) {
				List<AdmitQuotaAttrEntity> attrs = admitQuotaAttrDao.selectByProperty("quotaCode",
						child.getQuotaCode());
				admitQuotaDao.deleteEntity(child);

				for (AdmitQuotaAttrEntity attr : attrs) {
					admitQuotaAttrDao.deleteEntity(attr);
				}
			}
		}
		AdmitQuotaEntity quotaown = admitQuotaDao.selectSingleByProperty("quotaCode", admitQuota.getQuotaCode());
		admitQuotaDao.deleteEntity(quotaown);
		save(admitQuota);
	}

	public List<AdmitQuotaEntity> queryParentQuota() {
		StringBuilder sql = new StringBuilder(" 1=1");
		sql.append(" and parentCode is NULL");
		List<AdmitQuotaEntity> quotaList = admitQuotaDao.select(sql.toString(), new HashMap());
		return quotaList;
	}

	public List<AdmitQuotaEntity> queryByPCode(String pCode) {
		List<AdmitQuotaEntity> quotaList = admitQuotaDao.selectByProperty("parentCode", pCode);
		return quotaList;
	}
}
