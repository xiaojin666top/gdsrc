package com.beawan.model.service;

import java.util.List;

import com.beawan.core.BaseService;
import com.beawan.core.Pagination;
import com.beawan.model.dto.RiskWarnResultDto;
import com.beawan.model.entity.RiskWarning;

/**
 * @author yzj
 */
public interface RiskWarningService extends BaseService<RiskWarning>{

	public Pagination<RiskWarning> getPagerByRiskType(int page, int pageSize, Integer riskType);
	
	public List<RiskWarnResultDto> getRiskWarningResult(String taskId, String customerNo);
	
	/*public String ICGG001(String taskId, List<TycChangeRecord> changeRecordList) throws Exception;
	
	public String ICGG002(String taskId, List<TycChangeRecord> changeRecordList) throws Exception;
	
	public String ICGG003(String taskId, List<TycChangeRecord> recordList);*/
	
	public String ICGG004(String taskId, String name, List<String> nameList);
	
	/*public String SFYJ001(String taskId, List<TycSfLegal> legalList);
	
	public String SFYJ002(String taskId,  List<TycSfLegal> legalList);
	
	public String SFYJ003(String taskId, List<TycSfDishonest> dishonestList);*/
	
	public String SDYJ001(String taskId, double lastYearEleCharge, double currentEleCharge);
	
	public String SDYJ002(String taskId, double lastMonthEleCharge, double currentEleCharge);
	
	public String SDYJ003(String taskId, double lastYearEleCharge, double currentEleCharge, double lastIncome, double currentIncome);
	
	public String SDYJ004(String taskId, double lastMonthEleCharge, double currentEleCharge, double lastIncome, double currentIncome);
	
	public String SDYJ006(String taskId, double currentEleCharge, double industryOutput, double tradeInduOutPut);
	
	public String SDYJ007(String taskId, double currentEleCharge, double industryOutput, double tradeInduOutPut);
	
	public String ZXYJ001(String taskId, int times);
	
	public String ZXYJ007(String taskId, double WJQDKYE, double JYHDXJLR);
	
	public String ZXYJ008(String taskId, double DBJE, double QYJZC);
	
	public String ZXYJ009(String taskId, int number);
	
	public String ZXYJ010(String taskId, double DQJK, double CQJK, double YFPJ, double RZYE);
	
	
}
