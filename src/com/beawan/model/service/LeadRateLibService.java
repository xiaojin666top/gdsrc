package com.beawan.model.service;

import com.beawan.core.BaseService;
import com.beawan.core.ResultDto;
import com.beawan.model.dto.LeadRateLib;
import com.beawan.model.dto.SaveLrModelDto;
import com.beawan.model.entity.LrLibEntity;

/**
 * @author yzj
 */
public interface LeadRateLibService extends BaseService<LrLibEntity> {
	
	//通过库id拿到库具体信息
	public LeadRateLib findByLeadRateId(Integer id) ;
	public ResultDto queryLeadPaging(int page, int rows, String param);
	public void updateByDto(SaveLrModelDto saveLrModelDto);
	
	
}
