package com.beawan.model.service;

import com.beawan.core.BaseService;
import com.beawan.model.entity.DimensionEntity;

/**
 * @author yzj
 */
public interface DimensionService extends BaseService<DimensionEntity> {
	
}
