package com.beawan.base.entity;

import com.platform.util.StringUtil;

/**
 * 报表模型数据（行）实体类
 */

public class SReportModelRow implements java.io.Serializable {

	// Fields

	private String modelNo;
	private String rowNo;
	private String rowName;
	private String rowSubject;
	private String displayOrder;
	private String rowDimType;
	private String rowAttribute;
	private String col1Def;
	private String col2Def;
	private String col3Def;
	private String col4Def;
	private Double standardValue;
	private String deleteFlag;
	private String formulaExp1;
	private String formulaExp2;

	/** default constructor */
	public SReportModelRow() {
	}

	/** minimal constructor */
	public SReportModelRow(String modelNo, String rowNo) {
		this.modelNo = modelNo;
		this.rowNo = rowNo;
	}

	public String getModelNo() {
		return modelNo;
	}

	public void setModelNo(String modelNo) {
		this.modelNo = modelNo;
	}

	public String getRowNo() {
		return rowNo;
	}

	public void setRowNo(String rowNo) {
		this.rowNo = rowNo;
	}

	public String getRowName() {
		return rowName;
	}

	public void setRowName(String rowName) {
		this.rowName = rowName;
	}

	public String getRowSubject() {
		return rowSubject;
	}

	public void setRowSubject(String rowSubject) {
		this.rowSubject = rowSubject;
	}

	public String getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(String displayOrder) {
		this.displayOrder = displayOrder;
	}

	public String getRowDimType() {
		return rowDimType;
	}

	public void setRowDimType(String rowDimType) {
		this.rowDimType = rowDimType;
	}

	public String getRowAttribute() {
		return rowAttribute;
	}

	public void setRowAttribute(String rowAttribute) {
		this.rowAttribute = rowAttribute;
	}

	public String getCol1Def() {
		return col1Def;
	}

	public void setCol1Def(String col1Def) {
		this.col1Def = col1Def;
	}

	public String getCol2Def() {
		return col2Def;
	}

	public void setCol2Def(String col2Def) {
		this.col2Def = col2Def;
	}

	public String getCol3Def() {
		return col3Def;
	}

	public void setCol3Def(String col3Def) {
		this.col3Def = col3Def;
	}

	public String getCol4Def() {
		return col4Def;
	}

	public void setCol4Def(String col4Def) {
		this.col4Def = col4Def;
	}

	public Double getStandardValue() {
		return standardValue;
	}

	public void setStandardValue(Double standardValue) {
		this.standardValue = standardValue;
	}

	public String getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getFormulaExp1() {
		return formulaExp1;
	}

	public void setFormulaExp1(String formulaExp1) {
		this.formulaExp1 = formulaExp1;
	}

	public String getFormulaExp2() {
		return formulaExp2;
	}

	public void setFormulaExp2(String formulaExp2) {
		this.formulaExp2 = formulaExp2;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SReportModelRow other = (SReportModelRow) obj;
		if(StringUtil.isEmptyString(other.getRowSubject()))
			return false;
		if(other.getRowSubject().equals(this.getRowSubject())){
			return true;
		}
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		return this.getRowNo().hashCode();
	}
}