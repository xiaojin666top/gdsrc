package com.beawan.base.entity;

import java.io.Serializable;

/*
 * 违规者信息
 */
public class IllegalRecord implements Serializable{
	
	private static final long serialVersionUID = -758902171728805546L;
	
	private Long id; 
	private String illPersonId;   //违规人编号
	private String illPerson;  	//违规人
	private String illTime; //违规时间
	private String illContent; //违规内容
	private String recordPerson;  //记录者
	private String recordTime;  //记录时间
	private String modPerson;   //修改人
	private String modTime;   //修改时间
	private String state;    //状态     0:删除 , 1:存在
	

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getIllPersonId() {
		return illPersonId;
	}


	public void setIllPersonId(String illPersonId) {
		this.illPersonId = illPersonId;
	}


	public String getIllPerson() {
		return illPerson;
	}


	public void setIllPerson(String illPerson) {
		this.illPerson = illPerson;
	}


	public String getIllTime() {
		return illTime;
	}


	public void setIllTime(String illTime) {
		this.illTime = illTime;
	}


	public String getIllContent() {
		return illContent;
	}


	public void setIllContent(String illContent) {
		this.illContent = illContent;
	}


	public String getRecordPerson() {
		return recordPerson;
	}


	public void setRecordPerson(String recordPerson) {
		this.recordPerson = recordPerson;
	}


	public String getRecordTime() {
		return recordTime;
	}


	public void setRecordTime(String recordTime) {
		this.recordTime = recordTime;
	}


	public String getModPerson() {
		return modPerson;
	}


	public void setModPerson(String modPerson) {
		this.modPerson = modPerson;
	}


	public String getModTime() {
		return modTime;
	}


	public void setModTime(String modTime) {
		this.modTime = modTime;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	@Override
	public String toString() {
		return "IllegalRecord [id=" + id + ", illPersonId=" + illPersonId + ", illPerson=" + illPerson + ", illTime="
				+ illTime + ", illContent=" + illContent + ", recordPerson=" + recordPerson + ", recordTime="
				+ recordTime + ", modPerson=" + modPerson + ", modTime=" + modTime + ", state=" + state + "]";
	}




	

}
