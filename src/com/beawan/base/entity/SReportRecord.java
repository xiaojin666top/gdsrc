package com.beawan.base.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 报表记录实体类
 * 一期报表对应5类财报：资产负债、利润、现金流量人工、现金流量自动、比率指标
 */
@SuppressWarnings("serial")
@Entity
@Table(schema = "GDTCESYS", name = "CUS_REPORT_RECORD")
public class SReportRecord extends BaseEntity implements java.io.Serializable {

	// Fields

	@Id
	@Column(name = "REPORT_NO")
	private String reportNo;
	@Column(name = "OBJECT_TYPE")
	private String objectType;//对象类型
	@Column(name = "OBJECT_NO")
	private String objectNo;//对象编号
	@Column(name = "MODEL_NO")
	private String modelNo;//模型编号
	@Column(name = "REPORT_NAME")
	private String reportName;//报表名称
	@Column(name = "INPUT_TIME")
	private String inputTime;//登记时间
	@Column(name = "ORG_ID")
	private String orgId;//创建机构
	@Column(name = "UPDATE_DATE")
	private String updateDate;//更新事件
	@Column(name = "USER_ID")
	private String userId;//操作员
	@Column(name = "VALID_FLAG")
	private String validFlag;//生效标志

	// Constructors

	/** default constructor */
	public SReportRecord() {
	}

	/** minimal constructor */
	public SReportRecord(String reportNo) {
		this.reportNo = reportNo;
	}

	// Property accessors

	public String getReportNo() {
		return this.reportNo;
	}

	public void setReportNo(String reportNo) {
		this.reportNo = reportNo;
	}

	public String getObjectType() {
		return this.objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getObjectNo() {
		return this.objectNo;
	}

	public void setObjectNo(String objectNo) {
		this.objectNo = objectNo;
	}

	public String getModelNo() {
		return this.modelNo;
	}

	public void setModelNo(String modelNo) {
		this.modelNo = modelNo;
	}

	public String getReportName() {
		return this.reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getInputTime() {
		return this.inputTime;
	}

	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}

	public String getOrgId() {
		return this.orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getValidFlag() {
		return this.validFlag;
	}

	public void setValidFlag(String validFlag) {
		this.validFlag = validFlag;
	}

}