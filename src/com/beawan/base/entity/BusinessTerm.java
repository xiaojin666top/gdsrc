package com.beawan.base.entity;

import com.beawan.core.BaseEntity;

/**
 * 信贷业务期限   暂时包括 营销任务、贷后任务 期限
 */
public class BusinessTerm extends BaseEntity{

    private Integer id;
    private Integer businessType;
    private Integer term;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Integer businessType) {
        this.businessType = businessType;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }
}
