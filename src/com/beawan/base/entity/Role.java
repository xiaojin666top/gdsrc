package com.beawan.base.entity;

import javax.persistence.*;

/**
 * 角色信息表 entity. 
 * @author  beawan_fengjj
 */
@Entity
@Table(name = "BS_ROLE_INFO",schema = "GDTCESYS")
public class Role implements java.io.Serializable {
	
	private static final long serialVersionUID = -7771674475987216385L;

	// Fields
	@Id
	@Column(name = "ROLE_NO")
	private String roleNo;
	@Column(name = "ROLE_NAME")
	private String roleName;
	@Column(name = "DEPARTMENG_NO")
	private String departmengNo;
	@Column(name = "ROLE_DESCR")
	private String roleDescr;
	@Column(name = "ORGANNO")
	private String organno;

	// Constructors

	/** default constructor */
	public Role(String roleNo) {
		this.roleNo = roleNo;
	}
	
	public Role() {
	}

	/** full constructor */
	public Role(String roleName, String departmengNo, String roleDescr) {
		this.roleName = roleName;
		this.departmengNo = departmengNo;
		this.roleDescr = roleDescr;
	}

	// Property accessors
	public String getRoleNo() {
		return this.roleNo;
	}

	public void setRoleNo(String roleNo) {
		this.roleNo = roleNo;
	}

	public String getRoleName() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getDepartmengNo() {
		return this.departmengNo;
	}

	public void setDepartmengNo(String departmengNo) {
		this.departmengNo = departmengNo;
	}

	public String getRoleDescr() {
		return this.roleDescr;
	}

	public void setRoleDescr(String roleDescr) {
		this.roleDescr = roleDescr;
	}

	public String getOrganno() {
		return organno;
	}

	public void setOrganno(String organno) {
		this.organno = organno;
	}

	@Override
	public String toString() {
		return "Role [roleNo=" + roleNo + ", roleName=" + roleName + ", departmengNo=" + departmengNo + ", roleDescr="
				+ roleDescr + ", organno=" + organno + "]";
	}
	
	

}