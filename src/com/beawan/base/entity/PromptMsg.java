package com.beawan.base.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;
import java.io.UnsupportedEncodingException;

/**
 * 提示信息表
 */
@Entity
@Table(name = "BS_PROMPT_MSG",schema = "GDTCESYS")
public class PromptMsg extends BaseEntity implements java.io.Serializable {

	@Id
	@Column(name = "CODE")
	private String code; //代码标识
	@Column(name = "NAME")
	private String name; //名称
	@Column(name = "BUSINESS_NO")
	private String businessNo; //适用的调查模板（编号）
	@Column(name = "CATEGORY")
	private String category; //分类标识
	@Column(name = "CONTENT")
	private byte[] content; //内容
	@Column(name = "CONTENT_TEXT")
	private String contentText;
	
	// Constructors

	/** default constructor */
	public PromptMsg() {
	}

	// Property accessors

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public byte[] getContent() {
		return this.content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public String getBusinessNo() {
		return businessNo;
	}

	public void setBusinessNo(String businessNo) {
		this.businessNo = businessNo;
	}


	public void setContentText(String contentText) {
		this.contentText = contentText;
	}

	public String getContentText(){
		if(contentText != null){
			try {
				contentText = new String(content, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				contentText = new String(content);
			}
		}else{
			return "";
		}
		return contentText;
	}
	
}