package com.beawan.base.entity;

/**
 * 报表数据主键类
 */

@SuppressWarnings("serial")
public class SReportDataId implements java.io.Serializable {

	private String reportNo;
	private String rowNo;

	public String getReportNo() {
		return reportNo;
	}

	public void setReportNo(String reportNo) {
		this.reportNo = reportNo;
	}

	public String getRowNo() {
		return rowNo;
	}

	public void setRowNo(String rowNo) {
		this.rowNo = rowNo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((reportNo == null) ? 0 : reportNo.hashCode());
		result = prime * result + ((rowNo == null) ? 0 : rowNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SReportDataId other = (SReportDataId) obj;
		if (reportNo == null) {
			if (other.reportNo != null)
				return false;
		} else if (!reportNo.equals(other.reportNo))
			return false;
		if (rowNo == null) {
			if (other.rowNo != null)
				return false;
		} else if (!rowNo.equals(other.rowNo))
			return false;
		return true;
	}

}