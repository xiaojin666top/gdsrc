package com.beawan.base.entity;

/**
 * 报表模型实体类
 */

public class SReportModel implements java.io.Serializable {

	// Fields

	private String modelNo;
	private String modelName;
	private String modelType;
	private String modelDescribe;
	private String modelAbbr;
	private String modelClass;
	private String attribute1;
	private String attribute2;
	private String displayMethod;
	private String headerMethod;
	private String deleteFlag;
	private String remark;

	// Constructors

	/** default constructor */
	public SReportModel() {
	}

	/** minimal constructor */
	public SReportModel(String modelNo) {
		this.modelNo = modelNo;
	}

	// Property accessors

	public String getModelNo() {
		return modelNo;
	}

	public void setModelNo(String modelNo) {
		this.modelNo = modelNo;
	}

	public String getModelName() {
		return modelName;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getModelType() {
		return modelType;
	}

	public void setModelType(String modelType) {
		this.modelType = modelType;
	}

	public String getModelDescribe() {
		return modelDescribe;
	}

	public void setModelDescribe(String modelDescribe) {
		this.modelDescribe = modelDescribe;
	}

	public String getModelAbbr() {
		return modelAbbr;
	}

	public void setModelAbbr(String modelAbbr) {
		this.modelAbbr = modelAbbr;
	}

	public String getModelClass() {
		return modelClass;
	}

	public void setModelClass(String modelClass) {
		this.modelClass = modelClass;
	}

	public String getAttribute1() {
		return attribute1;
	}

	public void setAttribute1(String attribute1) {
		this.attribute1 = attribute1;
	}

	public String getAttribute2() {
		return attribute2;
	}

	public void setAttribute2(String attribute2) {
		this.attribute2 = attribute2;
	}

	public String getDisplayMethod() {
		return displayMethod;
	}

	public void setDisplayMethod(String displayMethod) {
		this.displayMethod = displayMethod;
	}

	public String getHeaderMethod() {
		return headerMethod;
	}

	public void setHeaderMethod(String headerMethod) {
		this.headerMethod = headerMethod;
	}

	public String getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}