package com.beawan.base.entity;


import javax.persistence.*;

import com.beawan.core.BaseEntity;

/**
 * @ClassName BsDic
 * @Description (数据字典)
 * @author comlc
 * @Date 2017年2月26日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */

@Entity
@Table(name = "BS_DIC",schema = "GDTCESYS")
public class SysDic extends BaseEntity{
 
	@Id
	@Column(name = "SD_ID")
	//@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="BS_DIC_SEQ")
	//@SequenceGenerator(name="BS_DIC_SEQ",allocationSize=1,initialValue=1, sequenceName="BS_DIC_SEQ")
	private Long sdId;

	/** @Field @enName : (英文名称) */
	@Column(name = "ENNAME")
	private String enName;
	
	/** @Field @cnName : (中文名称) */
	@Column(name = "CNNAME")
	private String cnName;
	
	/** @Field @optType : (字典类别代码) */
	@Column(name = "OPTTYPE")
	private String optType;
	
	/** @Field @memo : (字典类别描述) */
	@Column(name = "MEMO")
	private String memo;
	
	/**
	 * 作为一个保留字段
	 * 在贷后中，取这个作为是否的默认选中值
	 */
	@Column(name = "FLAG")
	private String flag;

	@Column(name = "LEVELS")
	private String levels;
	/**
	 * 排序
	 */
	@Column(name = "ORDERID")
	private Long orderId;

	@Column(name = "STDID")
	private Long stdId;

	public SysDic() {
		super();
	}

	
	public SysDic(String enName, String cnName, String optType, String memo, Long orderId) {
		super();
		this.enName = enName;
		this.cnName = cnName;
		this.optType = optType;
		this.memo = memo;
		this.orderId = orderId;
	}


	public Long getSdId() {
		return sdId;
	}

	public void setSdId(Long sdId) {
		this.sdId = sdId;
	}

	public String getEnName() {
		return enName;
	}

	public void setEnName(String enName) {
		this.enName = enName;
	}

	public String getCnName() {
		return cnName;
	}

	public void setCnName(String cnName) {
		this.cnName = cnName;
	}

	public String getOptType() {
		return optType;
	}

	public void setOptType(String optType) {
		this.optType = optType;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getLevels() {
		return levels;
	}

	public void setLevels(String levels) {
		this.levels = levels;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getStdId() {
		return stdId;
	}

	public void setStdId(Long stdId) {
		this.stdId = stdId;
	}
}
