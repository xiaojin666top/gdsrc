package com.beawan.base.entity;

import java.io.Serializable;

/*
 * 培训信息
 */
public class TrainInfo implements Serializable{
	
	private Long id;
	private String trainerId;   //培训人编号
	private String trainPerson;  	//培训人
	private String trainTime; //培训时间
	private String trainTheme;  //培训主题
	private String trainContent; //培训内容
	private String recordPerson;  //记录者
	private String recordTime;  //记录时间
	private String modPerson;   //修改人
	private String modTime;   //修改时间
	private String attachmentPath; //附件地址
	private String state;    //状态 : 0:删除 ,1:存在
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTrainerId() {
		return trainerId;
	}
	public void setTrainerId(String trainerId) {
		this.trainerId = trainerId;
	}
	public String getTrainPerson() {
		return trainPerson;
	}
	public void setTrainPerson(String trainPerson) {
		this.trainPerson = trainPerson;
	}
	public String getTrainTime() {
		return trainTime;
	}
	public void setTrainTime(String trainTime) {
		this.trainTime = trainTime;
	}
	public String getTrainTheme() {
		return trainTheme;
	}
	public void setTrainTheme(String trainTheme) {
		this.trainTheme = trainTheme;
	}
	public String getTrainContent() {
		return trainContent;
	}
	public void setTrainContent(String trainContent) {
		this.trainContent = trainContent;
	}
	public String getRecordPerson() {
		return recordPerson;
	}
	public void setRecordPerson(String recordPerson) {
		this.recordPerson = recordPerson;
	}
	public String getRecordTime() {
		return recordTime;
	}
	public void setRecordTime(String recordTime) {
		this.recordTime = recordTime;
	}
	public String getModPerson() {
		return modPerson;
	}
	public void setModPerson(String modPerson) {
		this.modPerson = modPerson;
	}
	public String getModTime() {
		return modTime;
	}
	public void setModTime(String modTime) {
		this.modTime = modTime;
	}
	public String getAttachmentPath() {
		return attachmentPath;
	}
	public void setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Override
	public String toString() {
		return "TrainInfo [id=" + id + ", trainerId=" + trainerId + ", trainPerson=" + trainPerson + ", trainTime="
				+ trainTime + ", trainTheme=" + trainTheme + ", trainContent=" + trainContent + ", recordPerson="
				+ recordPerson + ", recordTime=" + recordTime + ", modPerson=" + modPerson + ", modTime=" + modTime
				+ ", attachmentPath=" + attachmentPath + ", state=" + state + "]";
	}
	
	


}
