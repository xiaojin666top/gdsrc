package com.beawan.base.entity;


import com.beawan.core.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @ClassName User
 * @Description (系统用户表)
 * @Company 杭州碧湾信息技术有限公司
 */
@Entity
@Table(name = "BS_USER_INFO",schema = "GDTCESYS")
public class User extends BaseEntity implements Serializable{

	@Id
	@Column(name = "USER_ID")
	private String userId; //用户编号

	@Column(name = "LOGIN_ID")
	private String loginId; //登录名（不用）

	@Column(name = "USER_NAME")
	private String userName; //用户姓名
	@Column(name = "PASSWORD")
	private String password; //登录密码
	@Column(name = "MGE_ORG_NO")
	private String mgeOrgNo; //管理机构号（部门）
	@Column(name = "ACC_ORG_NO")
	private String accOrgNo; //账务机构号（支行）
	@Column(name = "ORG_LVL")
	private String orgLvl;//机构等级
	@Column(name = "ROLE_NO")
	private String roleNo; //角色编号，多角色用逗号分隔
	@Column(name = "BUSINESS_LINE")
	private String businessLine; //业务条线，全部0、对公业务1、个人业务2
	@Column(name = "STATE")
	private String state; //状态，N正常、L锁定、C注销----当前字段原为Status
	@Column(name = "PHONE")
	private String phone; //手机号码
	@Column(name = "EMAIL")
	private String email; //邮箱
	@Column(name = "SEX")
	private String sex; //性别
	@Column(name = "BIRTHDAY")
	private String birthday; //生日
	@Column(name = "CERTID")
	private String certid;//身份证号码

	@Column(name = "PWD_EXPIRY_DATE")
	private String pwdExpiryDate; //密码失效日期
	@Column(name = "WRONG_PIN_NUM")
	private Integer wrongPinNum; //密码错误累计次数
	@Column(name = "LAST_PASSWORD")
	private String lastPassword; //上一次设置的密码
	@Column(name = "CON_WRONG_PIN_NUM")
	private Integer conWrongPinNum; //密码连续输错次数
	@Column(name = "ALLOW_WORK_PLAT")
	private String allowWorkPlat; //允许的应用平台，MB:移动设备，PC:PC电脑，ALL：全部
	@Column(name = "ALLOW_WORK_SYS")
	private String allowWorkSys; //允许的应用系统
	@Column(name = "DATA_ACCESS")
	private String dataAccess; //数据访问权限，0全部数据、1所属机构下数据、2所属用户下数据

	@Column(name = "SIGN_STATE")
	private String signState; //签到状态
	@Column(name = "INPUT_ORG")
	private String inputOrg; //登记机构
	@Column(name = "INPUT_USER")
	private String inputUser; //登记人编号
	@Column(name = "INPUT_TIME")
	private String inputTime; //登记时间
	@Column(name = "UPDATE_USER")
	private String updateUser; //更新人编号

	/** default constructor */
	public User() {
	}

	/** minimal constructor */
	public User(String userId) {
		this.userId = userId;
	}

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getMgeOrgNo() {
		return mgeOrgNo;
	}
	public void setMgeOrgNo(String mgeOrgNo) {
		this.mgeOrgNo = mgeOrgNo;
	}
	public String getAccOrgNo() {
		return accOrgNo;
	}
	public void setAccOrgNo(String accOrgNo) {
		this.accOrgNo = accOrgNo;
	}
	public String getRoleNo() {
		return roleNo;
	}
	public void setRoleNo(String roleNo) {
		this.roleNo = roleNo;
	}
	public String getBusinessLine() {
		return businessLine;
	}
	public void setBusinessLine(String businessLine) {
		this.businessLine = businessLine;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getPwdExpiryDate() {
		return pwdExpiryDate;
	}
	public void setPwdExpiryDate(String pwdExpiryDate) {
		this.pwdExpiryDate = pwdExpiryDate;
	}
	public Integer getWrongPinNum() {
		return wrongPinNum;
	}
	public void setWrongPinNum(Integer wrongPinNum) {
		this.wrongPinNum = wrongPinNum;
	}
	public String getLastPassword() {
		return lastPassword;
	}
	public void setLastPassword(String lastPassword) {
		this.lastPassword = lastPassword;
	}
	public Integer getConWrongPinNum() {
		return conWrongPinNum;
	}
	public void setConWrongPinNum(Integer conWrongPinNum) {
		this.conWrongPinNum = conWrongPinNum;
	}
	public String getAllowWorkPlat() {
		return allowWorkPlat;
	}
	public void setAllowWorkPlat(String allowWorkPlat) {
		this.allowWorkPlat = allowWorkPlat;
	}
	public String getAllowWorkSys() {
		return allowWorkSys;
	}
	public void setAllowWorkSys(String allowWorkSys) {
		this.allowWorkSys = allowWorkSys;
	}
	public String getDataAccess() {
		return dataAccess;
	}
	public void setDataAccess(String dataAccess) {
		this.dataAccess = dataAccess;
	}
	public String getSignState() {
		return signState;
	}
	public void setSignState(String signState) {
		this.signState = signState;
	}
	public String getInputOrg() {
		return inputOrg;
	}
	public void setInputOrg(String inputOrg) {
		this.inputOrg = inputOrg;
	}
	public String getInputUser() {
		return inputUser;
	}
	public void setInputUser(String inputUser) {
		this.inputUser = inputUser;
	}
	public String getInputTime() {
		return inputTime;
	}
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}

	public String getOrgLvl() {
		return orgLvl;
	}

	public void setOrgLvl(String orgLvl) {
		this.orgLvl = orgLvl;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCertid() {
		return certid;
	}

	public void setCertid(String certid) {
		this.certid = certid;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	@Override
	public String toString() {
		return "User{" +
				"userId='" + userId + '\'' +
				", loginId='" + loginId + '\'' +
				", userName='" + userName + '\'' +
				", password='" + password + '\'' +
				", mgeOrgNo='" + mgeOrgNo + '\'' +
				", accOrgNo='" + accOrgNo + '\'' +
				", orgLvl='" + orgLvl + '\'' +
				", roleNo='" + roleNo + '\'' +
				", businessLine='" + businessLine + '\'' +
				", state='" + state + '\'' +
				", phone='" + phone + '\'' +
				", email='" + email + '\'' +
				", sex='" + sex + '\'' +
				", birthday='" + birthday + '\'' +
				", certid='" + certid + '\'' +
				", pwdExpiryDate='" + pwdExpiryDate + '\'' +
				", wrongPinNum=" + wrongPinNum +
				", lastPassword='" + lastPassword + '\'' +
				", conWrongPinNum=" + conWrongPinNum +
				", allowWorkPlat='" + allowWorkPlat + '\'' +
				", allowWorkSys='" + allowWorkSys + '\'' +
				", dataAccess='" + dataAccess + '\'' +
				", signState='" + signState + '\'' +
				", inputOrg='" + inputOrg + '\'' +
				", inputUser='" + inputUser + '\'' +
				", inputTime='" + inputTime + '\'' +
				'}';
	}
}