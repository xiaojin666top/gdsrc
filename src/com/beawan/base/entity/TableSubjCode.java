package com.beawan.base.entity;
// default package


/**
 * 表格项目表 entity. 
 * @author beawan_fengjj
 */

public class TableSubjCode implements java.io.Serializable {

	// Fields

	private static final long serialVersionUID = -758902171728805546L;
	
	private Long id;
	private String subjCode;
	private String subjName;
	private Long sort;
	private String subjType;
	private String businType;
	
	private String cmisSubjCode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSubjCode() {
		return subjCode;
	}

	public void setSubjCode(String subjCode) {
		this.subjCode = subjCode;
	}

	public String getSubjName() {
		return subjName;
	}

	public void setSubjName(String subjName) {
		this.subjName = subjName;
	}

	public Long getSort() {
		return sort;
	}

	public void setSort(Long sort) {
		this.sort = sort;
	}

	public String getSubjType() {
		return subjType;
	}

	public void setSubjType(String subjType) {
		this.subjType = subjType;
	}

	public String getBusinType() {
		return businType;
	}

	public void setBusinType(String businType) {
		this.businType = businType;
	}

	public String getCmisSubjCode() {
		return cmisSubjCode;
	}

	public void setCmisSubjCode(String cmisSubjCode) {
		this.cmisSubjCode = cmisSubjCode;
	}

}