package com.beawan.base.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * 报表数据实体类
 * 对应报表详细信息的数据
 */

@SuppressWarnings("serial")
@Entity
@Table(schema = "GDTCESYS", name = "CUS_REPORT_DATA")
@IdClass(SReportDataId.class)
public class SReportData extends BaseEntity implements java.io.Serializable {

	// Fields

	@Id
	@Column(name = "REPORT_NO", columnDefinition = "VARCHAR(32) comment '报告编号'")
	private String reportNo;

	@Id
	@Column(name = "ROW_NO")
	private String rowNo;

	@Column(name = "ROW_NAME")
	private String rowName;

	@Column(name = "ROW_SUBJECT")
	private String rowSubject;

	@Column(name = "DISPLAY_ORDER")
	private String displayOrder;

	@Column(name = "ROW_DIM_TYPE")
	private String rowDimType;

	@Column(name = "ROW_ATTRIBUTE")
	private String rowAttribute;

	@Column(name = "COL1_VALUE", precision=22, scale=6, columnDefinition = "DECIMAL(22,6)")
	private Double col1Value;

	@Column(name = "COL2_VALUE", precision=22, scale=6, columnDefinition = "DECIMAL(22,6)")
	private Double col2Value;

	@Column(name = "COL3_VALUE", precision=22, scale=6, columnDefinition = "DECIMAL(22,6)")
	private Double col3Value;

	@Column(name = "COL4_VALUE", precision=22, scale=6, columnDefinition = "DECIMAL(22,6)")
	private Double col4Value;

	@Column(name = "STANDARD_VALUE", precision=22, scale=6, columnDefinition = "DECIMAL(22,6)")
	private Double standardValue;

	// Constructors

	/** default constructor */
	public SReportData() {
	}

	public String getReportNo() {
		return reportNo;
	}

	public void setReportNo(String reportNo) {
		this.reportNo = reportNo;
	}

	public String getRowNo() {
		return rowNo;
	}

	public void setRowNo(String rowNo) {
		this.rowNo = rowNo;
	}

	public String getRowName() {
		return rowName;
	}

	public void setRowName(String rowName) {
		this.rowName = rowName;
	}

	public String getRowSubject() {
		return rowSubject;
	}

	public void setRowSubject(String rowSubject) {
		this.rowSubject = rowSubject;
	}

	public String getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(String displayOrder) {
		this.displayOrder = displayOrder;
	}

	public String getRowDimType() {
		return rowDimType;
	}

	public void setRowDimType(String rowDimType) {
		this.rowDimType = rowDimType;
	}

	public String getRowAttribute() {
		return rowAttribute;
	}

	public void setRowAttribute(String rowAttribute) {
		this.rowAttribute = rowAttribute;
	}

	public Double getCol1Value() {
		return col1Value;
	}

	public void setCol1Value(Double col1Value) {
		this.col1Value = col1Value;
	}

	public Double getCol2Value() {
		return col2Value;
	}

	public void setCol2Value(Double col2Value) {
		this.col2Value = col2Value;
	}

	public Double getCol3Value() {
		return col3Value;
	}

	public void setCol3Value(Double col3Value) {
		this.col3Value = col3Value;
	}

	public Double getCol4Value() {
		return col4Value;
	}

	public void setCol4Value(Double col4Value) {
		this.col4Value = col4Value;
	}

	public Double getStandardValue() {
		return standardValue;
	}

	public void setStandardValue(Double standardValue) {
		this.standardValue = standardValue;
	}

	@Override
	public String toString() {
		return "SReportData [reportNo=" + reportNo + ", rowNo=" + rowNo
				+ ", rowName=" + rowName + ", rowSubject=" + rowSubject
				+ ", displayOrder=" + displayOrder + ", rowDimType="
				+ rowDimType + ", rowAttribute=" + rowAttribute
				+ ", col1Value=" + col1Value + ", col2Value=" + col2Value
				+ ", col3Value=" + col3Value + ", col4Value=" + col4Value
				+ ", standardValue=" + standardValue + "]";
	}

}