package com.beawan.base.entity;

//用户登录错误信息
public class UserLoginErrorInfo {
	
	private Long id;
	private String userId;   //用户id
	private String userName;  //用户名
	private String loginErrorTime;  //上一次登录错误时间
	private Integer loginErrorNum;  //错误次数
	private String ipAddress;  //IP地址

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getLoginErrorTime() {
		return loginErrorTime;
	}
	public void setLoginErrorTime(String loginErrorTime) {
		this.loginErrorTime = loginErrorTime;
	}
	public Integer getLoginErrorNum() {
		return loginErrorNum;
	}
	public void setLoginErrorNum(Integer loginErrorNum) {
		this.loginErrorNum = loginErrorNum;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	
	

}
