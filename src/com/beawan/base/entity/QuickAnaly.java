package com.beawan.base.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;


/**
 * 合规审查内容说明表
 */
@Entity
@Table(name = "BS_QUICK_ANALY",schema = "GDTCESYS")
public class QuickAnaly extends BaseEntity {

    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="BS_QUICK_ANALY_SEQ")
    //@SequenceGenerator(name="BS_QUICK_ANALY_SEQ",allocationSize=1,initialValue=1, sequenceName="BS_QUICK_ANALY_SEQ")
    private Integer id;

    @Column(name = "ITEMNAME")
    private String itemName;

    @Column(name = "ITEMCODE")
    private String itemCode;
    
    //评价等级      由1极好  2较好  3一般  4较差   5极差
    @Column(name = "RELEVEL")
    private Integer reLevel;

    //提示分析的内容
    @Column(name = "ANALYMSG")
    private String analyMsg;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public Integer getReLevel() {
		return reLevel;
	}

	public void setReLevel(Integer reLevel) {
		this.reLevel = reLevel;
	}

	public String getAnalyMsg() {
		return analyMsg;
	}

	public void setAnalyMsg(String analyMsg) {
		this.analyMsg = analyMsg;
	}

    
    
}
