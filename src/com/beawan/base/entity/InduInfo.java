package com.beawan.base.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;

@Entity
@Table(name = "BS_INDU_INFO", schema = "GDTCESYS")
public class InduInfo extends BaseEntity {
	
	@Id
	@Column(name = "ID")
	private Integer id;
	@Column(name = "INDU_CODE")
	private String induCode;
	@Column(name = "INDU_NAME")
	private String induName;
	@Column(name = "INDU_SURVEY")
	private String induSurvey;//行业概况
	@Column(name = "GROW_TREND")
	private String growTrend;//发展趋势
	@Column(name = "UP_DOWN_RELE")
	private String upDownRele;//上下游情况
	@Column(name = "INDU_BARRIER")
	private String induBarrier;//壁垒
	@Column(name = "BEN_FACTOR")
	private String benFactor;//有利因素
	@Column(name = "HAR_FACTOR")
	private String harFacror;//不利因素
	@Column(name = "INDU_RISK")
	private String induRisk;//行业风险
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getInduCode() {
		return induCode;
	}
	public void setInduCode(String induCode) {
		this.induCode = induCode;
	}
	public String getInduName() {
		return induName;
	}
	public void setInduName(String induName) {
		this.induName = induName;
	}
	public String getInduSurvey() {
		return induSurvey;
	}
	public void setInduSurvey(String induSurvey) {
		this.induSurvey = induSurvey;
	}
	public String getGrowTrend() {
		return growTrend;
	}
	public void setGrowTrend(String growTrend) {
		this.growTrend = growTrend;
	}
	public String getUpDownRele() {
		return upDownRele;
	}
	public void setUpDownRele(String upDownRele) {
		this.upDownRele = upDownRele;
	}
	public String getInduBarrier() {
		return induBarrier;
	}
	public void setInduBarrier(String induBarrier) {
		this.induBarrier = induBarrier;
	}
	public String getBenFactor() {
		return benFactor;
	}
	public void setBenFactor(String benFactor) {
		this.benFactor = benFactor;
	}
	public String getHarFacror() {
		return harFacror;
	}
	public void setHarFacror(String harFacror) {
		this.harFacror = harFacror;
	}
	public String getInduRisk() {
		return induRisk;
	}
	public void setInduRisk(String induRisk) {
		this.induRisk = induRisk;
	}
	
	
}
