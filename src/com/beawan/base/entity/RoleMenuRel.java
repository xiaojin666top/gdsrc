package com.beawan.base.entity;

import javax.persistence.*;

/**
 * 角色菜单映射表 entity. 
 * @author beawan_fengjj
 */
@Entity
@Table(name = "BS_ROLE_MENU_REL",schema = "GDTCESYS")
public class RoleMenuRel implements java.io.Serializable {
	
	private static final long serialVersionUID = -435846186486811512L;

	// Fields

	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BS_ROLE_MENU_REL_SEQ")
	//@SequenceGenerator(name = "BS_ROLE_MENU_REL_SEQ",allocationSize=1,initialValue=1, sequenceName = "BS_ROLE_MENU_REL_SEQ")
	private Long id;
	@Column(name = "ROLE_NO")
	private String roleNo;
	@Column(name = "MENU_NO")
	private String menuNo;
	@Column(name = "AUTHORITY")
	private String authority;

	// Constructors

	/** default constructor */
	public RoleMenuRel() {
	}

	/** full constructor */
	public RoleMenuRel(String roleNo, String menuNo, String authority) {
		this.roleNo = roleNo;
		this.menuNo = menuNo;
		this.authority = authority;
	}

	// Property accessors
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRoleNo() {
		return this.roleNo;
	}

	public void setRoleNo(String roleNo) {
		this.roleNo = roleNo;
	}

	public String getMenuNo() {
		return this.menuNo;
	}

	public void setMenuNo(String menuNo) {
		this.menuNo = menuNo;
	}

	public String getAuthority() {
		return this.authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

}