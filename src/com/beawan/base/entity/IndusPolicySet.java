package com.beawan.base.entity;

import java.io.Serializable;

public class IndusPolicySet implements Serializable {

	private Long id;

	private String indusCode; // 国标行业代码
	
	private String indusPolicy; // 本行政策#STD_INDUSTRY_POLICY

	/*private String bankPolicy; // 本行政策#STD_INDUSTRY_POLICY
	
	private String bankPolicyDesc; //本行政策说明
	
	private String countryPolicy; // 国家政策#STD_INDUSTRY_POLICY
	
	private String countryPolicyDesc; //国家政策说明*/

	private String policyType; // 政策类别#STD_INDUS_POLICY_TYPE，10：本行，20：国家，30：地区

	private String inputUser;// 登记人ID

	private String inputTime;// 登记时间

	private String updateUser;// 更新人ID

	private String updateTime;// 更新时间

	private String status;// 状态，1：启用，0：禁用

	private String remarks;// 备注说明
	
	/*非持久字段*/
	private String indusName; // 行业名称

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIndusCode() {
		return indusCode;
	}

	public void setIndusCode(String indusCode) {
		this.indusCode = indusCode;
	}

	public String getIndusPolicy() {
		return indusPolicy;
	}

	public void setIndusPolicy(String indusPolicy) {
		this.indusPolicy = indusPolicy;
	}

	public String getPolicyType() {
		return policyType;
	}

	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}

	public String getInputUser() {
		return inputUser;
	}

	public void setInputUser(String inputUser) {
		this.inputUser = inputUser;
	}

	public String getInputTime() {
		return inputTime;
	}

	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getIndusName() {
		return indusName;
	}

	public void setIndusName(String indusName) {
		this.indusName = indusName;
	}

}
