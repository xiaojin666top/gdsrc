package com.beawan.base.entity;
// default package

import java.util.List;


/**
 * 菜单信息类 entity. 
 * @author beawan_fengjj
 */
public class Menu implements java.io.Serializable {
	
	private static final long serialVersionUID = 5778317260728910590L;

	// Fields

	private String menuNo;
	private String menuName;
	private String parentNo;
	private String url;
	private String imageSrc;
	private Long sort;
	private String menuType;
	
	private List<Menu> childMenuList;

	// Constructors

	/** default constructor */
	public Menu() {
	}

	/** full constructor */
	public Menu(String menuName, String parentNo, String url, String imageSrc,
			Long sort, String menuType) {
		this.menuName = menuName;
		this.parentNo = parentNo;
		this.url = url;
		this.imageSrc = imageSrc;
		this.sort = sort;
		this.menuType = menuType;
	}

	// Property accessors
	public String getMenuNo() {
		return this.menuNo;
	}

	public void setMenuNo(String menuNo) {
		this.menuNo = menuNo;
	}

	public String getMenuName() {
		return this.menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getParentNo() {
		return this.parentNo;
	}

	public void setParentNo(String parentNo) {
		this.parentNo = parentNo;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getImageSrc() {
		return this.imageSrc;
	}

	public void setImageSrc(String imageSrc) {
		this.imageSrc = imageSrc;
	}

	public Long getSort() {
		return this.sort;
	}

	public void setSort(Long sort) {
		this.sort = sort;
	}

	public String getMenuType() {
		return this.menuType;
	}

	public void setMenuType(String menuType) {
		this.menuType = menuType;
	}

	public List<Menu> getChildMenuList() {
		return childMenuList;
	}

	public void setChildMenuList(List<Menu> childMenuList) {
		this.childMenuList = childMenuList;
	}

}