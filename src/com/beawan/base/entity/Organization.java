package com.beawan.base.entity;
// default package

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 机构表
 */

@Entity
@Table(name = "BS_ORG",schema = "GDTCESYS")
public class Organization extends BaseEntity {

    // Fields    
    @Id
    @Column(name = "ORGANNO")
    private String organno;//机构码
    @Column(name = "SUPORGANNO")
    private String suporganno;//上级机构码
    @Column(name = "LOCATE")
    private String locate;//位置属性
    @Column(name = "ORGANNAME")
    private String organname;//机构名称

    @Column(name = "CORPORATEORGNAME")
    private String corporateorgname;//法人机构名称
    @Column(name = "ORGANSHORTFORM")
    private String organshortform;//机构简称
    @Column(name = "ENNAME")
    private String enname;//英文名，存放是否总部部室，为总部部室时设值为1否则为空
    @Column(name = "ORDERNO")
    private String orderno;//序号
    @Column(name = "DISTNO")
    private String distno;//地区编号
    @Column(name = "LAUNCHDATE")
    private String launchdate;//开办日期
    @Column(name = "ORGANLEVEL")
    private String organlevel;//机构级别
    @Column(name = "FINCODE")
    private String fincode;//金融代码
    @Column(name = "STATE")
    private Integer states;//状态
    @Column(name = "ORGANCHIEF")
    private String organchief;//机构负责人
    @Column(name = "TELNUM")
    private String telnum;//联系电话
    @Column(name = "ADDRESS")
    private String address;//地址
    @Column(name = "POSTCODE")
    private String postcode;//邮编
    @Column(name = "CONTROL")
    private String control;//控制字
    @Column(name = "ARTI_ORGANNO")
    private String artiOrganno;//所属法人机构码
    @Column(name = "DISTNAME")
    private String distname;//地区名称
    @Column(name = "AREA_DEV_CATE_TYPE")
    private String areaDevCateType;//地区发展分类
    @Column(name = "BANK_SRRNO")
    private String bankSrrno;//大额行号
    @Column(name = "BSINS_LIC")
    private String bsinsLic;//营业执照

    @Column(name = "ORG_LVL")
    private String orgLvl;//机构等级
    @Column(name = "FAX")
    private String fax;//传真


	/** default constructor */
    public Organization() {
    }

    public Organization(String organno, String suporganno, String locate, String organname, String corporateorgname, String organshortform, String enname, String orderno, String distno, String launchdate, String organlevel, String fincode, Integer states, String organchief, String telnum, String address, String postcode, String control, String artiOrganno, String distname, String areaDevCateType, String bankSrrno, String bsinsLic, String orgLvl, String fax) {
        this.organno = organno;
        this.suporganno = suporganno;
        this.locate = locate;
        this.organname = organname;
        this.corporateorgname = corporateorgname;
        this.organshortform = organshortform;
        this.enname = enname;
        this.orderno = orderno;
        this.distno = distno;
        this.launchdate = launchdate;
        this.organlevel = organlevel;
        this.fincode = fincode;
        this.states = states;
        this.organchief = organchief;
        this.telnum = telnum;
        this.address = address;
        this.postcode = postcode;
        this.control = control;
        this.artiOrganno = artiOrganno;
        this.distname = distname;
        this.areaDevCateType = areaDevCateType;
        this.bankSrrno = bankSrrno;
        this.bsinsLic = bsinsLic;
        this.orgLvl = orgLvl;
        this.fax = fax;
    }

    /** full constructor */

   
    // Property accessors

    public String getOrganno() {
        return this.organno;
    }
    
    public void setOrganno(String organno) {
        this.organno = organno;
    }

    public String getSuporganno() {
        return this.suporganno;
    }
    
    public void setSuporganno(String suporganno) {
        this.suporganno = suporganno;
    }

    public String getLocate() {
        return this.locate;
    }
    
    public void setLocate(String locate) {
        this.locate = locate;
    }

    public String getOrganname() {
        return this.organname;
    }
    
    public void setOrganname(String organname) {
        this.organname = organname;
    }

    public String getOrganshortform() {
        return this.organshortform;
    }
    
    public void setOrganshortform(String organshortform) {
        this.organshortform = organshortform;
    }

    public String getEnname() {
        return this.enname;
    }
    
    public void setEnname(String enname) {
        this.enname = enname;
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    public String getDistno() {
        return this.distno;
    }
    
    public void setDistno(String distno) {
        this.distno = distno;
    }

    public String getLaunchdate() {
        return this.launchdate;
    }
    
    public void setLaunchdate(String launchdate) {
        this.launchdate = launchdate;
    }

    public String getOrganlevel() {
        return organlevel;
    }

    public void setOrganlevel(String organlevel) {
        this.organlevel = organlevel;
    }

    public String getFincode() {
        return this.fincode;
    }
    
    public void setFincode(String fincode) {
        this.fincode = fincode;
    }

    public Integer getStates() {
        return states;
    }

    public void setStates(Integer states) {
        this.states = states;
    }

    public String getOrganchief() {
        return this.organchief;
    }
    
    public void setOrganchief(String organchief) {
        this.organchief = organchief;
    }

    public String getTelnum() {
        return this.telnum;
    }
    
    public void setTelnum(String telnum) {
        this.telnum = telnum;
    }

    public String getAddress() {
        return this.address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostcode() {
        return this.postcode;
    }
    
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getControl() {
        return this.control;
    }
    
    public void setControl(String control) {
        this.control = control;
    }

    public String getArtiOrganno() {
        return this.artiOrganno;
    }
    
    public void setArtiOrganno(String artiOrganno) {
        this.artiOrganno = artiOrganno;
    }

    public String getDistname() {
        return this.distname;
    }
    
    public void setDistname(String distname) {
        this.distname = distname;
    }

    public String getAreaDevCateType() {
        return this.areaDevCateType;
    }
    
    public void setAreaDevCateType(String areaDevCateType) {
        this.areaDevCateType = areaDevCateType;
    }

    public String getBankSrrno() {
        return this.bankSrrno;
    }
    
    public void setBankSrrno(String bankSrrno) {
        this.bankSrrno = bankSrrno;
    }

    public String getBsinsLic() {
        return this.bsinsLic;
    }
    
    public void setBsinsLic(String bsinsLic) {
        this.bsinsLic = bsinsLic;
    }

    public String getOrgLvl() {
        return this.orgLvl;
    }
    
    public void setOrgLvl(String orgLvl) {
        this.orgLvl = orgLvl;
    }

    public String getFax() {
        return this.fax;
    }
    
    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getCorporateorgname() {
        return corporateorgname;
    }

    public void setCorporateorgname(String corporateorgname) {
        this.corporateorgname = corporateorgname;
    }
}