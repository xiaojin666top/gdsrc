package com.beawan.base.entity;

/**
 * 操作日志 entity. 
 * @author beawan_fengjj
 */

public class PlatformLog implements java.io.Serializable {

	// Fields
	
	private Long id;
	private String ipAddress;
	private String organizationNo;
	private String operationType;
	private String operationTime;
	private String operator;
	private String operationContent;

	// Constructors

	/** default constructor */
	public PlatformLog() {
	}

	/** full constructor */
	public PlatformLog(String ipAddress, String organizationNo,
			String operationType, String operationTime, String operator,
			String operationContent) {
		this.ipAddress = ipAddress;
		this.organizationNo = organizationNo;
		this.operationType = operationType;
		this.operationTime = operationTime;
		this.operator = operator;
		this.operationContent = operationContent;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIpAddress() {
		return this.ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getOrganizationNo() {
		return this.organizationNo;
	}

	public void setOrganizationNo(String organizationNo) {
		this.organizationNo = organizationNo;
	}

	public String getOperationType() {
		return this.operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public String getOperationTime() {
		return this.operationTime;
	}

	public void setOperationTime(String operationTime) {
		this.operationTime = operationTime;
	}

	public String getOperator() {
		return this.operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public String getOperationContent() {
		return this.operationContent;
	}

	public void setOperationContent(String operationContent) {
		this.operationContent = operationContent;
	}

}