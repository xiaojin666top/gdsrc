package com.beawan.base.webservice.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.beawan.base.entity.SysDic;
import com.beawan.base.entity.PromptMsg;
import com.beawan.base.service.IPromptMsgSV;
import com.beawan.base.service.ISysDicSV;
import com.beawan.base.service.ISysTreeDicSV;
import com.beawan.base.webservice.IBaseWS;
import com.beawan.common.util.WSResult;
import com.beawan.library.bean.TreeData;
import com.platform.util.JacksonUtil;

/**
 * 基础数据模块webservice接口实现
 */
@Service("baseWS")
public class BaseWSImpl implements IBaseWS{
	
	private static final Logger log = Logger.getLogger(BaseWSImpl.class);
	
	@Resource
	private ISysDicSV sysDicSV;
	
	@Resource
	private ISysTreeDicSV sysTreeDicSV;
	
	@Resource
	private IPromptMsgSV promptMsgSV;

	@Override
	public String findAllDics() {
		
		WSResult result = new WSResult();
		
		try {
			List<SysDic> dicList = sysDicSV.findAll();
			if (CollectionUtils.isEmpty(dicList))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(dicList);
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取字典信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findDicByType(String optType) {
		
		WSResult result = new WSResult();
		
		try {
			List<SysDic> dicList = sysDicSV.findByType(optType);
			if (CollectionUtils.isEmpty(dicList))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(dicList);
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取字典信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findDic(String enName, String optType) {
		
		WSResult result = new WSResult();
		
		try {
			SysDic dic = sysDicSV.findByEnNameAndType(enName, optType);
			if (dic == null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(dic);
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取字典信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findAllTreeDics() {
		
		WSResult result = new WSResult();
		
		try {
			List<TreeData> dicList = sysTreeDicSV.findAll();
			if (CollectionUtils.isEmpty(dicList))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(dicList);
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取树形结构字典信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findTreeDicByAbvn(String abvenName, String optType) {
		
		WSResult result = new WSResult();
		
		try {
			List<TreeData> dicList = sysTreeDicSV.findByAbvenName(abvenName, optType);
			if (CollectionUtils.isEmpty(dicList))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(dicList);
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取树形结构字典信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findTreeDic(String enName, String optType) {
		
		WSResult result = new WSResult();
		
		try {
			TreeData dic = sysTreeDicSV.findByEnNameAndType(enName, optType);
			if (dic == null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(dic);
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取树形结构字典信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findPromptMsgByCode(String code) {
		
		WSResult result = new WSResult();
		
		try {
			PromptMsg promptMsg = promptMsgSV.findByCode(code);
			if (promptMsg == null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(promptMsg);
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取提示信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findPromptMsgMapByCodes(String codes) {
		
		WSResult result = new WSResult();
		
		try {
			String[] codeArray = codes.split(",");
			result.data = promptMsgSV.findMapJsonByCodes(codeArray);;
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取提示信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

}
