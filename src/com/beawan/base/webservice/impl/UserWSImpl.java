package com.beawan.base.webservice.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.beawan.base.entity.User;
import com.beawan.base.qbean.QUser;
import com.beawan.base.service.IPlatformLogSV;
import com.beawan.base.service.IUserSV;
import com.beawan.base.webservice.IUserWS;
import com.beawan.common.Constants;
import com.beawan.common.util.WSResult;
import com.platform.util.JacksonUtil;

@Service("userWS")
public class UserWSImpl implements IUserWS {
	
	private static final Logger log = Logger.getLogger(IUserWS.class);
	
	@Resource
	protected IUserSV userSV;
	
	@Resource
	private IPlatformLogSV platformLogSV;

	@Override
	public String getUserByLoginName(String loginName) {
		
		WSResult result = new WSResult();
		
		try {
			
			User user = userSV.queryById(loginName);
			if(user != null)
				result.data = JacksonUtil.serialize(user);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("用户查询异常！",e);
		}
		
		return result.json();
	}

	@Override
	public String getUserById(String userId) {
		
		WSResult result = new WSResult();
		
		try {
			
			User user = userSV.queryById(userId);
			if(user != null)
				result.data = JacksonUtil.serialize(user);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("用户查询异常！",e);
		}
		
		return result.json();
	}

	@Override
	public String modifyPwd(String userId, String oldPassword, String newPassword) {
		
		WSResult result = new WSResult();
		
		try {
			
			int flag = userSV.modifyPwdMB(userId, oldPassword, newPassword);
			result.data = flag + "";
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("移动端修改密码异常！", e);
		}
		
		return result.json();
	}

	@Override
	public String saveUserInfo(String jsondata) {
		
		WSResult result = new WSResult();
		
		try {
			
			User user = userSV.saveUserInfo(jsondata);
			if(user != null)
				result.data = JacksonUtil.serialize(user);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存用户信息异常！",e);
		}
		
		return result.json();
	}

/*	@Override
	public String login(String loginInfo) {
		
		WSResult result = new WSResult();
		
		try {
			
			Map<String, Object> data = userSV.loginMB(loginInfo);
		
			if(data != null)
				result.data = JacksonUtil.serialize(data);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("用户移动端登录异常！",e);
		}
		
		return result.json();
	}*/

	@Override
	public String loginout(String userId) {
		
		WSResult result = new WSResult();
		
		try {
			
			userSV.loginout(userId, Constants.Platform.MOBILE);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("用户移动端登出异常！",e);
		}
		
		return result.json();
	}

	@Override
	public String queryUserPaging(String condition, String order,
			int pageIndex, int pageSize) {
		
		WSResult result = new WSResult();
		
		try {
			
			QUser queryCondition = JacksonUtil.fromJson(condition, QUser.class);
			
			List<?> list = userSV.queryUserPaging(queryCondition, order, pageIndex, pageSize);
			if(list != null)
				result.data = JacksonUtil.serialize(list);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("移动端查询用户分页异常！",e);
		}
		
		return result.json();
	}

	@Override
	public String queryUserCount(String condition) {
		
		WSResult result = new WSResult();
		
		try {
			
			QUser queryCondition = JacksonUtil.fromJson(condition, QUser.class);
			
			long count = userSV.queryUserCount(queryCondition);
			result.data = count + "";
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("移动端查询用户记录数异常！",e);
		}
		
		return result.json();
	}

}
