package com.beawan.base.webservice;


/**
 * 用户webservice接口
 */
public interface IUserWS {
	
	/**
	 *  @Description: 根据用户名查询用户
	 *  @param loginName 根据用户
	 */
	public String getUserByLoginName(String loginName);
	
	/**
	 *  @Description: 根据用户名查询用户
	 *  @param userId 根据用户
	 */
	public String getUserById(String userId);
	
	/**
	 * @param userId
	 * @param oldPassword
	 * @param newPassword
	 * @return
	 */
	public String modifyPwd(String userId, String oldPassword, String newPassword);

	/**
	 * TODO 保存或更新
	 * @param jsondata 用户信息json串
	 * @throws Exception
	 */
	public String saveUserInfo(String jsondata);

	/**
	 * TODO 移动端用户用户登录
	 * @param loginInfo 登录信息
	 * @return
	 */
	//public String login(String loginInfo);
	
	
	/**
	 * TODO 用户退出登录
	 * @param userId 用户号
	 * @return
	 */
	public String loginout(String userId);
	
	/**
	 * TODO 根据条件查询分页
	 * @param condition 查询条件
	 * @param order 排序条件
	 * @param pageIndex 起始页数
	 * @param pageSize 每页大小
	 * @return
	 */
	public String queryUserPaging(String condition,
			String order, int pageIndex, int pageSize);

	/**
	 * TODO 根据条件查询记录总数
	 * @param condition 查询条件
	 * @return
	 * @throws Exception
	 */
	public String queryUserCount(String condition);
}
