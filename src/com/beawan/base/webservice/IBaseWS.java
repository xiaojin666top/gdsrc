package com.beawan.base.webservice;

/**
 * 基础数据模块webservice接口
 *
 */
public interface IBaseWS {
	
	/**
	 * @Description 查询所有字典项
	 * @return
	 */
	public String findAllDics();
	
	/**
	 * @Description 通过类型查找字典项
	 * @param optType
	 * @return
	 */
	public String findDicByType(String optType);
	
	/**
	 * @Description 通过类型和名称查找字典项
	 * @param enName 英文名称
	 * @param optType 类型
	 * @return
	 */
	public String findDic(String enName, String optType);
	
	/**
	 * @Description 查询所有树形结构字典项
	 * @return
	 */
	public String findAllTreeDics();
	
	/**
	 * @Description 当前节点的所有子节点
	 * @param abvenName	父节点标识
	 * @param optType	类型
	 * @return
	 */
	public String findTreeDicByAbvn(String abvenName, String optType);
	
	/**
	 * @Description 通过类型和名称查找字典项
	 * @param enName 英文名称
	 * @param optType 类型
	 * @return
	 */
	public String findTreeDic(String enName, String optType);
	
	/**
	 * @Description 根据代码标识查询提示信息
	 * @param code	代码标识
	 * @return 返回单个实体对象的json串
	 */
	public String findPromptMsgByCode(String code);
	
	/**
	 * @Description 根据代码标识查询
	 * @param codes	代码标识列表
	 * @return 返回map集合的json串
	 */
	public String findPromptMsgMapByCodes(String codes);
}
