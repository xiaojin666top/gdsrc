package com.beawan.base.dao;

import com.beawan.base.entity.UserLoginLog;
import com.beawan.common.dao.ICommDAO;

/**
 * @Description 申请信息持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IUserLoginLogDAO extends ICommDAO<UserLoginLog> {
	
}
