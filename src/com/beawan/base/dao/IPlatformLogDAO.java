package com.beawan.base.dao;

import com.beawan.base.entity.PlatformLog;
import com.beawan.common.dao.ICommDAO;
/**
 * 操作日志持久层接口
 * @author beawan_fengjj
 *
 */
public interface IPlatformLogDAO extends ICommDAO<PlatformLog> {

}
