package com.beawan.base.dao;

import java.util.List;
import java.util.Map;

import com.beawan.common.dao.ICommDAO;
import com.beawan.library.bean.TreeData;

public interface ISysTreeDicDAO extends ICommDAO<TreeData> {
	
	public TreeData saveOrUpdate(TreeData treeData);
	
	public List<TreeData> getTreeDataByLocate(String locate);
	
	public List<TreeData> getTreeDataAbvenName(String abvenName, String optType);

	public Map<String, Object> getTreeData(String query, int index, int count, Object... args);
	
	public TreeData getTreeDataById(String id);
	
	public TreeData geTreeDataByCnName(String cnName, String optType);
	
	public TreeData geTreeDataByEnName(String enName, String optType);
	
}
