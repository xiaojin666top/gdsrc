package com.beawan.base.dao;

import java.util.List;

import com.beawan.base.entity.Menu;
import com.beawan.core.BaseDao;

/**
 * 系统菜单持久层接口
 * @author beawan_fengjj
 *
 */
public interface IMenuDAO extends BaseDao<Menu>{
	
	/**
	 * 保存更新
	 * @param menu
	 * @throws Exception
	 */
//	public void saveOrUpdate(Menu menu)throws Exception;
	
	/**
	 * 查询所有菜单
	 * @return
	 * @throws Exception
	 */
	public List<Menu> query()throws Exception;
	/***查询主菜单***/
	public List<Menu> queryMainMenu()throws Exception;
	/**
	 * 根据父编号查询子菜单
	 * @param parentNo
	 * @return
	 * @throws Exception
	 */
	public List<Menu> queryByParentNo(String parentNo)throws Exception;
	/**
	 * 根据编号查询
	 * @param no
	 * @return
	 * @throws Exception
	 */
	public Menu queryByNo(String no)throws Exception;
	/**
	 * 分页查询
	 * @param queryString
	 * @param params
	 * @param start
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	public List<Menu> queryPaginate(String queryString,Object[] params,int start,int limit)throws Exception;
	/**
	 * 条件查询
	 * @param queryString
	 * @return
	 * @throws Exception
	 */
	public List<Menu> queryCondition(String queryString, Object[] params)throws Exception;
	/**
	 * 删除
	 * @param Menu
	 * @throws Exception
	 */
	public void delete(Menu menu)throws Exception;

}
