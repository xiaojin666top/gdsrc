package com.beawan.base.dao;

import com.beawan.base.entity.PromptMsg;
import com.beawan.common.dao.ICommDAO;

/**
 * 提示信息持久层接口
 */
public interface IPromptMsgDAO extends ICommDAO<PromptMsg>{

}
