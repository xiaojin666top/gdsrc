package com.beawan.base.dao;

import com.beawan.base.entity.IndusPolicySet;
import com.beawan.common.dao.ICommDAO;

public interface IIndusPolicySetDAO extends ICommDAO<IndusPolicySet> {

}
