package com.beawan.base.dao;

import java.util.List;

import com.beawan.base.entity.Organization;
import com.beawan.core.BaseDao;

public interface IOrganizationDAO extends BaseDao<Organization>{
	/**
	 * 根据机构编号查询
	 * @param no
	 * @return
	 * @throws Exception
	 */
	public Organization queryByNo(String no) throws Exception;
	/**
	 * 分页查询
	 * @param sqlString
	 * @param start
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	public List<Organization>  queryPaginate(String sqlString, int start,
			int limit) throws Exception;
	/**
	 * 条件查询
	 * @param queryString
	 * @return
	 * @throws Exception
	 */
	public List<Organization> queryByCondition(String queryString)throws Exception;
}
