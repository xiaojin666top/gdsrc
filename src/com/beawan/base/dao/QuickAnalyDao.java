package com.beawan.base.dao;

import com.beawan.core.BaseDao;
import com.beawan.base.entity.QuickAnaly;

/**
 * @author yzj
 */
public interface QuickAnalyDao extends BaseDao<QuickAnaly> {
}
