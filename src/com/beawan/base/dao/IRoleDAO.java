package com.beawan.base.dao;

import java.util.List;
import java.util.Map;

import com.beawan.base.entity.Role;
/**
 * 角色表持久层接口
 * @author beawan_fengjj
 *
 */
public interface IRoleDAO {
	
	/**
	 * 保存或更新
	 * @param role
	 * @throws Exception
	 */
	public void saveOrUpdate(Role role)throws Exception;
	/**
	 * 删除
	 * @param role
	 * @throws Exception
	 */
	public void delete(Role role)throws Exception;
	/**
	 * 条件查询
	 * @param sqlString
	 * @param params
	 * @return
	 * @throws Exception
	 */
	public List<Role> queryByCondition(String sqlString,Map<String, Object> params)throws Exception;
	/**
	 * 根据角色编号查询
	 * @param no
	 * @return
	 * @throws Exception
	 */
	public Role queryByNo(String no)throws Exception;
	/**
	 * 分页查询 
	 * @param sqlString
	 * @param params
	 * @param start
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	public List<Role> queryPaging(String sqlString,Map<String, Object> params,int start,int limit)throws Exception;

}
