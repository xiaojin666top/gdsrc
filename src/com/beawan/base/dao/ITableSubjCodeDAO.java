package com.beawan.base.dao;

import com.beawan.base.entity.TableSubjCode;
import com.beawan.common.dao.ICommDAO;

/**
 * 表格项目持久层接口
 * @author beawan_fengjj
 *
 */
public interface ITableSubjCodeDAO extends ICommDAO<TableSubjCode> {
	
}
