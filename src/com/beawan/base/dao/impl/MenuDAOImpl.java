package com.beawan.base.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.beawan.base.entity.Menu;
import com.beawan.base.dao.IMenuDAO;
import com.beawan.core.BaseDaoImpl;
/**
 * 系统菜单持久层实现
 * @author beawan_fengjj
 *
 */
@Repository("menuDAO")
public class MenuDAOImpl extends BaseDaoImpl<Menu> implements IMenuDAO {

//	@Override
//	public void saveOrUpdate(Menu menu) throws Exception {
//		entityManager.merge(menu);
//	}

	@Override
	public List<Menu> query() throws Exception {
		return execQuery(Menu.class, "from Menu as model");
	}

	@Override
	public List<Menu> queryMainMenu() throws Exception {
		/*return execQuery(Menu.class, "from Menu as model where model.menuType='P' order by model.sort asc");*/
		return execQuery(Menu.class, "from Menu as model where model.parentNo = '0' order by model.sort asc");
		
	}

	@Override
	public List<Menu> queryByParentNo(String parentNo) throws Exception {
		String queryString = "from Menu as model where model.parentNo=:parentNo  order by model.sort asc";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("parentNo", parentNo);
		return execQuery(Menu.class,queryString,params);
	}

	@Override
	public Menu queryByNo(String no) throws Exception {
		return entityManager.find(Menu.class,no);
	}

	@Override
	public List<Menu> queryPaginate(String queryString, Object[] params,
			int start, int limit) throws Exception {
		int index = start < 0 ? 0 : start;
		int count = limit < 0 ? 0 : limit;
		List<Menu> list = execQueryRange(Menu.class, queryString, index, count, params);
		return list;
	}

	@Override
	public List<Menu> queryCondition(String queryString, Object[] params) throws Exception {
		return execQuery(Menu.class,queryString,params);
	}

	@Override
	public void delete(Menu menu) throws Exception {
		entityManager.remove(menu);

	}

}
