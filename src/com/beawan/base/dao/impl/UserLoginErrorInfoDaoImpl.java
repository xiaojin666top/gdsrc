package com.beawan.base.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.beawan.base.entity.UserLoginErrorInfo;
import com.beawan.base.dao.UserLoginErrorInfoDao;
import com.beawan.common.dao.impl.CommDAOImpl;
@Repository("loginErrorInfoDao")
public class UserLoginErrorInfoDaoImpl extends CommDAOImpl<UserLoginErrorInfo> implements UserLoginErrorInfoDao{

	@Override
	public UserLoginErrorInfo getLoginError(String userId,String oneDayTime) {
		Map<String, Object> params = new HashMap<String, Object>();
		String sql = "from UserLoginErrorInfo where userId =:userId and loginErrorTime >=:loginErrorTime";
		params.put("userId", userId);
		params.put("loginErrorTime", oneDayTime);
		UserLoginErrorInfo errorInfo = selectSingle(sql, params);
		
		return errorInfo;
	}
	
	

	/*@Override
	public void saveLoginErrorInfo(UserLoginErrorInfo errorInfo) {
		save(UserLoginErrorInfo.class, errorInfo);
		
	}

	@Override
	public void updateErrorInfo(UserLoginErrorInfo errorInfo) {
		// TODO Auto-generated method stub
		
	}*/

}
