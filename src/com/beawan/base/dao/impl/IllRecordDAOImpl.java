package com.beawan.base.dao.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.beawan.base.entity.IllegalRecord;
import com.beawan.base.dao.IllRecordDAO;
import com.beawan.base.qbean.QUser;
import com.beawan.core.BaseDaoImpl;
import com.platform.util.StringUtil;
@Repository("illRecordDAO")
public class IllRecordDAOImpl extends BaseDaoImpl<IllegalRecord> implements IllRecordDAO{

	@Override
	public Long count(QUser queryCondition) {

		return null;
	}
	
	
	@Override
	public Map<String, Object> queryAll(QUser queryCondition,String orderCondition, int index, int count) {
		StringBuffer listSql = new StringBuffer("1=1");
		StringBuffer totalSql = new StringBuffer("1=1");
		
		Map<String, Object> params = new HashMap<String, Object>();
		if (queryCondition != null) {
			
			if (!StringUtil.isEmptyString(queryCondition.getUserName())) {
				listSql.append(" and illPerson like:illPerson");
				totalSql.append(" and illPerson like:illPerson");
				String name = null;
				try {
					name = URLDecoder.decode(queryCondition.getUserName(), "UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				params.put("illPerson", "%"+name+"%");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getBirthday())) {
				listSql.append(" and illTime =:illTime");
				totalSql.append(" and illTime =:illTime");
				params.put("illTime", queryCondition.getBirthday());
			}
		}
		listSql.append(" and state =:state");
		totalSql.append(" and state =:state");
		params.put("state", "1");
	
		if (!StringUtil.isEmptyString(orderCondition))
			listSql.append(" order by ").append(orderCondition);
		else
			listSql.append(" order by recordTime desc");
		
		Map<String, Object> dataMap = new HashMap<String, Object>();
				List<IllegalRecord> list = selectRange(listSql.toString(),index, count, params);
		long total = selectCount(totalSql.toString(), params);
		dataMap.put("total", total);
		dataMap.put("rows", list);
		
		return dataMap;
	}

	@Override
	public IllegalRecord saveIllegalRecord(IllegalRecord ilRecord) {
		return save(IllegalRecord.class, ilRecord);
	}


	@Override
	public void deleteIllRecord(IllegalRecord illegalRecord) {
		
		//deleteEntity(illegalRecord);
		saveOrUpdate(illegalRecord);
		//delete(illegalRecord);
		
	}


	@Override
	public IllegalRecord querySingle(Long id) {
		
		Map<String, Object> args = new HashMap<String, Object>();
		String sql = "from IllegalRecord where id=:id";
		args.put("id", id);
		IllegalRecord illegalRecord = selectSingle(sql, args);
		
		return illegalRecord;
	}


	@Override
	public IllegalRecord updtaInfo(IllegalRecord illegalRecord) {
		
		//IllegalRecord updtaInfo = updtaInfo(illegalRecord);
		IllegalRecord illegalRecord2 = saveOrUpdate(illegalRecord);
		
		return illegalRecord2;
	}



}
