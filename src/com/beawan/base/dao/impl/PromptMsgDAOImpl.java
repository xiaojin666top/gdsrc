package com.beawan.base.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.base.entity.PromptMsg;
import com.beawan.base.dao.IPromptMsgDAO;
import com.beawan.common.dao.impl.CommDAOImpl;


/**
 * 提示信息持久层接口实现
 */
@Repository("promptMsgDAO")
public class PromptMsgDAOImpl extends CommDAOImpl<PromptMsg> implements IPromptMsgDAO {

}
