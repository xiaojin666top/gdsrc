package com.beawan.base.dao.impl;

import org.springframework.stereotype.Service;

import com.beawan.base.entity.SReportData;
import com.beawan.base.dao.ISReportDataDAO;
import com.beawan.common.dao.impl.CommDAOImpl;

@Service("sReportDataDAO")
public class SReportDataDAOImpl extends CommDAOImpl<SReportData> implements ISReportDataDAO {

}
