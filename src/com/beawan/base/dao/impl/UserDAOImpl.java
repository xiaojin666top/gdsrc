package com.beawan.base.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.beawan.core.BaseDaoImpl;
import org.springframework.stereotype.Service;

import com.beawan.base.entity.User;
import com.beawan.base.dao.IUserDAO;
import com.beawan.common.dao.impl.CommDAOImpl;

@Service("userDAO")
public class UserDAOImpl extends BaseDaoImpl<User> implements IUserDAO {

	@Override
	public User getUserById(String userId) throws Exception {
		return this.selectByPrimaryKey(userId);
	}

	@Override
	public List<User> getUsers(String query, Object... args) throws Exception {
		return this.select(query, args);
	}
	
	@Override
	public List<User> getUsers() throws Exception {
		
		String query = " from User where userName <> '超级管理员' and userName <> '系统管理员' and userName <> '总行系统管理员'";
		
		return this.select(query);
	}

	@Override
	public User getUserByName(String userName) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		String query = "from User where userName=:userName";
		params.put("userName", userName);
		return selectSingle(query,params);
	}


}
