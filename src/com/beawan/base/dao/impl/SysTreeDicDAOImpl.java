package com.beawan.base.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.beawan.base.dao.ISysTreeDicDAO;
import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.library.bean.TreeData;

@Repository("sysTreeDicDAO")
public class SysTreeDicDAOImpl extends CommDAOImpl<TreeData> implements ISysTreeDicDAO {
	
	@Override
	public TreeData saveOrUpdate(TreeData treeData) {
		return this.save(treeData);
	}
	
	@Override
	public List<TreeData> getTreeDataAbvenName(String abvenName, String optType) {
		String hsql = "from TreeData where abvenName = ? and optType = ? order by orderId asc";
		return this.select(hsql, abvenName, optType);
	}

	@Override
	public Map<String, Object> getTreeData(String query, int index, int count, Object... args) {
		return null;
	}

	@Override
	public List<TreeData> getTreeDataByLocate(String locate) {
		return this.select("locate = ?", locate);
	}
	
	@Override
	public TreeData getTreeDataById(String id){
		return  this.selectSingle("stdId = ?", id);
	}
	
	@Override
	public TreeData geTreeDataByEnName(String enName,String optType){
		return this.selectSingle("enName = ? and optType = ?", enName, optType);
	}
	
	@Override
	public TreeData geTreeDataByCnName(String cnName,String optType){
		return  this.selectSingle("cnName = ? and optType = ?", cnName,optType);
	}
}
