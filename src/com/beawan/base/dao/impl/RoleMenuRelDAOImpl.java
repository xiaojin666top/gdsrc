package com.beawan.base.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.beawan.base.entity.RoleMenuRel;
import com.beawan.base.dao.IRoleMenuRelDAO;
import com.beawan.common.dao.impl.DaoHandler;
/**
 * 系统菜单与角色映射关系持久层实现
 * @author beawan_fengjj
 *
 */
@Repository("roleMenuRelDAO")
public class RoleMenuRelDAOImpl extends DaoHandler implements IRoleMenuRelDAO {

	@Override
	public void save(RoleMenuRel roleMenuRel) throws Exception {
		entityManager.merge(roleMenuRel);
	}

	@Override
	public List<RoleMenuRel> queryByRoleNo(String roleNo) throws Exception {
		String queryString = "from RoleMenuRel as model where model.roleNo in :roleNo";
		Map<String, Object> params = new HashMap<String, Object>();
		String[] roleNoArr = roleNo.split(",");
		List<String> roleNoList = new ArrayList<String>(Arrays.asList(roleNoArr));
		params.put("roleNo", roleNoList);
		return execQuery(RoleMenuRel.class, queryString, params);
	}

	@Override
	public void delete(RoleMenuRel roleMenuRel) throws Exception {
		entityManager.remove(roleMenuRel);
	}

}
