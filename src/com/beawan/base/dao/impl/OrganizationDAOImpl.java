package com.beawan.base.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.beawan.base.entity.Organization;
import com.beawan.base.dao.IOrganizationDAO;
import com.beawan.core.BaseDaoImpl;

@Repository("organizationDAO")
public class OrganizationDAOImpl extends BaseDaoImpl<Organization> implements
		IOrganizationDAO {
	
	@Override
	public Organization queryByNo(String no) throws Exception {
		return this.selectByPrimaryKey(Organization.class, no);
	}

	@Override
	public List<Organization> queryPaginate(String sqlString, int start, int limit)
			throws Exception {
		return selectRange(Organization.class, sqlString, start, limit);
	}

	@Override
	public List<Organization> queryByCondition(String queryString) throws Exception {
		return select(Organization.class, queryString);
	}
	
}
