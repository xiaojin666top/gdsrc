package com.beawan.base.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.base.dao.InduInfoDao;
import com.beawan.base.entity.InduInfo;
import org.springframework.stereotype.Repository;
import com.beawan.base.entity.InduInfo;

/**
 * @author yzj
 */
@Repository("induInfoDao")
public class InduInfoDaoImpl extends BaseDaoImpl<InduInfo> implements InduInfoDao{

}
