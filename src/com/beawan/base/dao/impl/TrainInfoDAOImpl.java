package com.beawan.base.dao.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.beawan.base.entity.TrainInfo;
import com.beawan.base.dao.TrainInfoDAO;
import com.beawan.base.qbean.QUser;
import com.beawan.core.BaseDaoImpl;
import com.platform.util.StringUtil;
@Repository("trainInfoDAO")
public class TrainInfoDAOImpl extends BaseDaoImpl<TrainInfo> implements TrainInfoDAO{

	@Override
	public Long count(QUser queryCondition) {

		return null;
	}
	
	
	@Override
	public Map<String, Object> queryAll(QUser queryCondition,String orderCondition, int index, int count) {
		StringBuffer listSql = new StringBuffer("1=1");
		StringBuffer totalSql = new StringBuffer("1=1");
		
		Map<String, Object> params = new HashMap<String, Object>();
		if (queryCondition != null) {
			
			if (!StringUtil.isEmptyString(queryCondition.getUserName())) {
				listSql.append(" and trainPerson like:trainPerson");
				totalSql.append(" and trainPerson like:trainPerson");
				String name = null;
				try {
					name = URLDecoder.decode(queryCondition.getUserName(), "UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				params.put("trainPerson","%"+name+"%");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getBirthday())) {
				listSql.append(" and trainTime=:trainTime");
				totalSql.append(" and trainTime=:trainTime");
				params.put("trainTime", queryCondition.getBirthday());
			}
			if (!StringUtil.isEmptyString(queryCondition.getOrgName())) {
				listSql.append(" and trainTheme like:trainTheme");
				totalSql.append(" and trainTheme like:trainTheme");
				params.put("trainTheme", "%"+queryCondition.getOrgName()+"%");
			}
		}
		listSql.append(" and state =:state");
		totalSql.append(" and state =:state");
		params.put("state", "1");
		
		if (!StringUtil.isEmptyString(orderCondition))
			listSql.append(" order by ").append(orderCondition);
		else
			listSql.append(" order by recordTime desc");
		
		Map<String, Object> dataMap = new HashMap<String, Object>();
				List<TrainInfo> list = selectRange(listSql.toString(),index, count, params);
		long total = selectCount(totalSql.toString(), params);
		dataMap.put("total", total);
		dataMap.put("rows", list);
		
		return dataMap;
	}

	@Override
	public TrainInfo saveIllegalRecord(TrainInfo trainInfo) {
		return save(TrainInfo.class, trainInfo);
	}


	@Override
	public void deleteIllRecord(TrainInfo trainInfo) {
		
	//	deleteEntity(trainInfo);
		saveOrUpdate(trainInfo);
		
	}


	@Override
	public TrainInfo querySingle(Long id) {
		
		Map<String, Object> args = new HashMap<String, Object>();
		String sql = "from TrainInfo where id=:id";
		args.put("id", id);
		TrainInfo trainInfo = selectSingle(sql, args);
		
		return trainInfo;
	}


	@Override
	public TrainInfo updtaInfo(TrainInfo trainInfo) {
		
		//IllegalRecord updtaInfo = updtaInfo(illegalRecord);
		TrainInfo trainInfo2 = saveOrUpdate(trainInfo);
		
		return trainInfo;
	}





}
