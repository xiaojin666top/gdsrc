package com.beawan.base.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.base.entity.TableSubjCode;
import com.beawan.base.dao.ITableSubjCodeDAO;
import com.beawan.common.dao.impl.CommDAOImpl;

/**
 * 表格项目持久层实现
 * @author beawan_fengjj
 *
 */
@Repository("tableSubjCodeDAO")
public class TableSubjCodeDAOImpl extends CommDAOImpl<TableSubjCode> implements
		ITableSubjCodeDAO {
	
}
