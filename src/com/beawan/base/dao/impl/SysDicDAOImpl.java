package com.beawan.base.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.beawan.base.entity.SysDic;
import com.beawan.base.dao.ISysDicDAO;
import com.beawan.common.dao.impl.CommDAOImpl;

@Repository("sysDicDAO")
public class SysDicDAOImpl  extends CommDAOImpl<SysDic> implements ISysDicDAO {

	@Override
	public List<SysDic> queryByOptType(String optType)  throws Exception {
		String hsql = "optType=:optType";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("optType", optType);
		return select(hsql, params);
	}

	@Override
	public SysDic queryByEnNameAndOptType(String enName, String optType) {
		String hsql = "enName= '"+ enName +"' and optType= '"+optType+"' ";
		Map<String, Object> params = new HashMap<String, Object>();
		//params.put("enName", enName);
		//params.put("optType", optType);
		return this.selectSingle(hsql, params);
	}

    @Override
    public SysDic queryByCnNameAndOptType(String cnName, String optType) {
        String hsql = "cnName= '"+ cnName +"' and optType= '"+optType+"' ";
        Map<String, Object> params = new HashMap<String, Object>();
        //params.put("enName", enName);
        //params.put("optType", optType);
        return this.selectSingle(hsql, params);
    }
}

