package com.beawan.base.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.base.entity.PlatformLog;
import com.beawan.base.dao.IPlatformLogDAO;
import com.beawan.common.dao.impl.CommDAOImpl;
/**
 * 操作日志持久层实现
 * @author beawan_fengjj
 *
 */
@Repository("platformLogDAO")
public class PlatformLogDAOImpl extends CommDAOImpl<PlatformLog> implements
		IPlatformLogDAO {
	
}
