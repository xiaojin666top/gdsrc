package com.beawan.base.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.base.entity.IndusPolicySet;
import com.beawan.base.dao.IIndusPolicySetDAO;
import com.beawan.common.dao.impl.CommDAOImpl;

@Repository("indusPolicySetDAO")
public class IndusPolicySetDAOImpl extends CommDAOImpl<IndusPolicySet>
		implements IIndusPolicySetDAO {

}
