package com.beawan.base.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.beawan.base.entity.FunMenu;
import com.beawan.base.dao.IFunMenuDAO;
import com.beawan.core.BaseDaoImpl;
/**
 * 系统菜单持久层实现
 * @author beawan_fengjj
 *
 */
@Repository("funMenuDAO")
public class FunMenuDAOImpl extends BaseDaoImpl<FunMenu> implements IFunMenuDAO {

//	@Override
//	public void saveOrUpdate(FunMenu FunMenu) throws Exception {
//		entityManager.merge(FunMenu);
//	}

	@Override
	public List<FunMenu> query() throws Exception {
		return execQuery(FunMenu.class, "from FunMenu as model");
	}

	@Override
	public List<FunMenu> queryMainMenu() throws Exception {
		/*return execQuery(FunMenu.class, "from FunMenu as model where model.FunMenuType='P' order by model.sort asc");*/
		return execQuery(FunMenu.class, "from FunMenu as model where model.parentNo = '0' order by model.sort asc");
		
	}

	@Override
	public List<FunMenu> queryByParentNo(String parentNo) throws Exception {
		String queryString = "from FunMenu as model where model.parentNo=:parentNo  order by model.sort asc";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("parentNo", parentNo);
		return execQuery(FunMenu.class,queryString,params);
	}

	@Override
	public FunMenu queryByNo(String no) throws Exception {
		return entityManager.find(FunMenu.class,no);
	}

	@Override
	public List<FunMenu> queryPaginate(String queryString, Object[] params,
			int start, int limit) throws Exception {
		int index = start < 0 ? 0 : start;
		int count = limit < 0 ? 0 : limit;
		List<FunMenu> list = execQueryRange(FunMenu.class, queryString, index, count, params);
		return list;
	}

	@Override
	public List<FunMenu> queryCondition(String queryString, Object[] params) throws Exception {
		return execQuery(FunMenu.class,queryString,params);
	}

	@Override
	public void delete(FunMenu FunMenu) throws Exception {
		entityManager.remove(FunMenu);

	}

}
