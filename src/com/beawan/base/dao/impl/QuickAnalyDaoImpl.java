package com.beawan.base.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.base.dao.QuickAnalyDao;
import com.beawan.base.entity.QuickAnaly;
import org.springframework.stereotype.Repository;
import com.beawan.base.entity.QuickAnaly;

/**
 * @author yzj
 */
@Repository("quickAnalyDao")
public class QuickAnalyDaoImpl extends BaseDaoImpl<QuickAnaly> implements QuickAnalyDao{

}
