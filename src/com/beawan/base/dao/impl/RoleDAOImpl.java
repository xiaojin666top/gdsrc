package com.beawan.base.dao.impl;

import java.util.List;



import java.util.Map;

import org.springframework.stereotype.Repository;

import com.beawan.base.entity.Role;
import com.beawan.base.dao.IRoleDAO;
import com.beawan.common.dao.impl.DaoHandler;
/**
 * 角色持久层实现
 * @author beawan_fengjj
 *
 */
@Repository("roleDAO")
public class RoleDAOImpl extends DaoHandler implements IRoleDAO {

	@Override
	public void saveOrUpdate(Role role) throws Exception {
		entityManager.merge(role);
	}

	@Override
	public void delete(Role role) throws Exception {
		entityManager.remove(role);
	}

	@Override
	public List<Role> queryByCondition(String sqlString, Map<String, Object> params)
			throws Exception {
		return execQuery(Role.class, sqlString, params);
	}

	@Override
	public Role queryByNo(String no) throws Exception {
		return entityManager.find(Role.class, no);
	}

	@Override
	public List<Role> queryPaging(String sqlString, Map<String, Object> params, int start, int limit) throws Exception {
		int index = start < 0 ? 0 : start;
		int count = limit < 0 ? 0 : limit;
		List<Role> list = execQueryRange(Role.class, sqlString, index, count, params);
		return list;
	}
	
	

}
