package com.beawan.base.dao.impl;

import org.springframework.stereotype.Service;

import com.beawan.base.entity.SReportRecord;
import com.beawan.base.dao.ISReportRecordDAO;
import com.beawan.common.dao.impl.CommDAOImpl;

@Service("sReportRecordDAO")
public class SReportRecordDAOImpl extends CommDAOImpl<SReportRecord> implements ISReportRecordDAO {

}
