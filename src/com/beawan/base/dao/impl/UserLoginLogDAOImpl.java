package com.beawan.base.dao.impl;

import org.springframework.stereotype.Service;

import com.beawan.base.entity.UserLoginLog;
import com.beawan.base.dao.IUserLoginLogDAO;
import com.beawan.common.dao.impl.CommDAOImpl;

@Service("userLoginLogDAO")
public class UserLoginLogDAOImpl extends CommDAOImpl<UserLoginLog> implements
		IUserLoginLogDAO {

}
