package com.beawan.base.dao;

import java.util.List;

import com.beawan.base.entity.RoleMenuRel;


/**
 * 系统菜单与角色映射关系持久层接口
 * @author beawan_fengjj
 *
 */
public interface IRoleMenuRelDAO {

	/**
	 * 保存角色菜单映射关系
	 * @param roleMenuRel
	 * @throws Exception
	 */
	public void save(RoleMenuRel roleMenuRel)throws Exception;
	/**
	 * 根据角色查询菜单映射关系
	 * @param roleNo
	 * @return
	 * @throws Exception
	 */
	public List<RoleMenuRel> queryByRoleNo(String roleNo)throws Exception;
	/**
	 * 删除角色菜单映射关系
	 * @param roleMenuRel
	 * @throws Exception
	 */
	public void delete(RoleMenuRel roleMenuRel)throws Exception;
	
}
