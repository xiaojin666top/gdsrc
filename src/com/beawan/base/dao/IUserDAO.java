package com.beawan.base.dao;

import java.util.List;

import com.beawan.base.entity.User;
import com.beawan.common.dao.ICommDAO;
import com.beawan.core.BaseDao;

/**
 * @Description
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IUserDAO extends BaseDao<User> {
	
	/**
	 * TODO (通过Id查找用户 )
	 * @param userId
	 * @return
	 */
	public User getUserById(String userId) throws Exception;
	
	/**
	 * TODO 查询用户
	 * @param query hsql查询语句
	 * @param args 占位参数
	 * @return
	 */
	public List<User> getUsers(String query, Object... args) throws Exception;
	
	
	public List<User> getUsers() throws Exception;
	
	public User getUserByName(String userName) throws Exception;
	

}
