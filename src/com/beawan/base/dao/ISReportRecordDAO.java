package com.beawan.base.dao;

import com.beawan.base.entity.SReportRecord;
import com.beawan.common.dao.ICommDAO;

/**
 * @Description
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface ISReportRecordDAO extends ICommDAO<SReportRecord> {
	
}
