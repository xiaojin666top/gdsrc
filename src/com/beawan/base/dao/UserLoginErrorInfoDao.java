package com.beawan.base.dao;

import com.beawan.base.entity.UserLoginErrorInfo;
import com.beawan.common.dao.ICommDAO;

public interface UserLoginErrorInfoDao extends ICommDAO<UserLoginErrorInfo>{
	
	//获取当天登录错误信息
	public UserLoginErrorInfo getLoginError(String userId,String oneDayTime);
	
	//新增一条错误记录
	//void saveLoginErrorInfo(UserLoginErrorInfo errorInfo);
	
	//更新错误次数
	//void updateErrorInfo(UserLoginErrorInfo errorInfo);
	
}
