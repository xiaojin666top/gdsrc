package com.beawan.base.dao;

import java.util.List;

import com.beawan.base.entity.FunMenu;
import com.beawan.core.BaseDao;

/**
 * 系统菜单持久层接口
 * @author beawan_fengjj
 *
 */
public interface IFunMenuDAO extends BaseDao<FunMenu>{
	
	/**
	 * 保存更新
	 * @param menu
	 * @throws Exception
	 */
//	public void saveOrUpdate(Menu menu)throws Exception;
	
	/**
	 * 查询所有菜单
	 * @return
	 * @throws Exception
	 */
	public List<FunMenu> query()throws Exception;
	/***查询主菜单***/
	public List<FunMenu> queryMainMenu()throws Exception;
	/**
	 * 根据父编号查询子菜单
	 * @param parentNo
	 * @return
	 * @throws Exception
	 */
	public List<FunMenu> queryByParentNo(String parentNo)throws Exception;
	/**
	 * 根据编号查询
	 * @param no
	 * @return
	 * @throws Exception
	 */
	public FunMenu queryByNo(String no)throws Exception;
	/**
	 * 分页查询
	 * @param queryString
	 * @param params
	 * @param start
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	public List<FunMenu> queryPaginate(String queryString,Object[] params,int start,int limit)throws Exception;
	/**
	 * 条件查询
	 * @param queryString
	 * @return
	 * @throws Exception
	 */
	public List<FunMenu> queryCondition(String queryString, Object[] params)throws Exception;
	/**
	 * 删除
	 * @param Menu
	 * @throws Exception
	 */
	public void delete(FunMenu menu)throws Exception;

}
