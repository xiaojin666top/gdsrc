package com.beawan.base.dao;

import java.util.Map;

import com.beawan.base.entity.IllegalRecord;
import com.beawan.base.qbean.QUser;
import com.beawan.core.BaseDao;

public interface IllRecordDAO extends BaseDao<IllegalRecord>{
	
	
	//查询总数
	Long count(QUser queryCondition);
	
	//查询所有违规记录
	Map<String, Object> queryAll(QUser queryCondition,
			String orderCondition, int index, int count);
	
	//查询单条记录
	IllegalRecord querySingle(Long id);
	
	//保存违规记录
	IllegalRecord saveIllegalRecord(IllegalRecord ilRecord);
	
	//删除违规记录
	void deleteIllRecord(IllegalRecord illegalRecord);
	
	//更新违规信息
	IllegalRecord updtaInfo(IllegalRecord illegalRecord);
	
}
