package com.beawan.base.dao;

import com.beawan.base.entity.SReportData;
import com.beawan.common.dao.ICommDAO;

/**
 * @Description
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface ISReportDataDAO extends ICommDAO<SReportData> {
	
}
