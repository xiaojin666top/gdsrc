package com.beawan.base.dao;

import java.util.List;

import com.beawan.base.entity.SysDic;
import com.beawan.common.dao.ICommDAO;

public interface ISysDicDAO  extends ICommDAO<SysDic> {

	public List<SysDic> queryByOptType(String optType) throws Exception ;
	
	public SysDic queryByEnNameAndOptType(String enName, String optType) throws Exception ;

	public SysDic queryByCnNameAndOptType(String cnName, String optType) throws Exception;//后来加的，用来和中文普通住房匹配
}
