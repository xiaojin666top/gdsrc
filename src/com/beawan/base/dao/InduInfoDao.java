package com.beawan.base.dao;

import com.beawan.core.BaseDao;
import com.beawan.base.entity.InduInfo;

/**
 * @author yzj
 */
public interface InduInfoDao extends BaseDao<InduInfo> {
}
