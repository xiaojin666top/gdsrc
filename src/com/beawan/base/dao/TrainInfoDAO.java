package com.beawan.base.dao;

import java.util.Map;

import com.beawan.base.entity.TrainInfo;
import com.beawan.base.qbean.QUser;
import com.beawan.core.BaseDao;

public interface TrainInfoDAO extends BaseDao<TrainInfo>{
	
	
	//查询总数
	Long count(QUser queryCondition);
	
	//查询所有违规记录
	Map<String, Object> queryAll(QUser queryCondition,
			String orderCondition, int index, int count);
	
	//查询单条记录
	TrainInfo querySingle(Long id);
	
	//保存违规记录
	TrainInfo saveIllegalRecord(TrainInfo trainInfo);
	
	//删除违规记录
	void deleteIllRecord(TrainInfo trainInfo);
	
	//更新违规信息
	TrainInfo updtaInfo(TrainInfo trainInfo);
	
	
}
