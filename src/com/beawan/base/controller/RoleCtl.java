package com.beawan.base.controller;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.platform.util.StringUtil;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.base.entity.Menu;
import com.beawan.base.entity.Role;
import com.beawan.base.entity.User;
import com.beawan.base.service.IAuthoritySV;
import com.beawan.base.service.IRoleSV;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.platform.util.HttpUtil;
import com.platform.util.JacksonUtil;

@Controller
@RequestMapping({ "/base/role" })
@UserSessionAnnotation
public class RoleCtl{
	
	private static final Logger log = Logger.getLogger(RoleCtl.class);
	
	@Resource
	private IAuthoritySV authoritySV;
	@Resource
	private IRoleSV roleSV;
	
	/***跳转到更改权限页面***/
	@RequestMapping("modify.do")
	public String modify(HttpServletRequest request, HttpServletResponse response){
		Map<String,Object> model = new HashMap<String,Object>();
		try {
			String roleName = HttpUtil.getAsString(request,"roleName");
			String roleNo = HttpUtil.getAsString(request,"roleNo");
			List<Menu> allMenuList = authoritySV.allMenuList();
			model.put("allMenuList", allMenuList);
			model.put("mapList", authoritySV.queryMapByRoleNo(roleNo));
			if(!StringUtil.isEmptyString(roleName)){
				roleName = URLDecoder.decode(roleName, "UTF-8");
				roleName = URLDecoder.decode(roleName, "UTF-8");
				model.put("roleName", roleName);
			}
			model.put("roleNo", roleNo);
			request.setAttribute("model", model);
		} catch (Exception e) {
			log.error("系统异常！",e);
			e.printStackTrace();
			return "error";
		}
		return "views/base/authority/modify_authority";
	}
	/***保存权限***/
	@RequestMapping("modifySubmit.json")
	@ResponseBody
	public Map<String , Object> modifySubmit(HttpServletRequest request, HttpServletResponse response){
		Map<String,Object> model = new HashMap<String,Object>();
		try {
			String roleNo = HttpUtil.getAsString(request,"roleNo");
			String menuNoStr = HttpUtil.getAsString(request,"menuNoStr");
			authoritySV.saveOrUpdateMap(roleNo, menuNoStr);
			model.put("success", true);
		} catch (Exception e) {
			log.error("系统异常！",e);
			model.put("msg", "系统异常，请联系管理员！");
			e.printStackTrace();
		}
		return model;
	}
	/***查看权限***/
	@RequestMapping("viewInfo.do")
	public String viewInfo(HttpServletRequest request, HttpServletResponse response){
		Map<String,Object> model = new HashMap<String,Object>();
		try {
			String roleName = HttpUtil.getAsString(request,"roleName");
			String roleNo = HttpUtil.getAsString(request,"roleNo");
			List<Menu> allMenuList = authoritySV.allMenuList();
			model.put("allMenuList", allMenuList);
			model.put("mapList", authoritySV.queryMapByRoleNo(roleNo));
			model.put("roleName", roleName);
			model.put("roleNo", roleNo);
		} catch (Exception e) {
			log.error("系统异常！",e);
			e.printStackTrace();
			return "error";
		}
		return "success";
	}
	
	/**
	 * @Description (得到所有角色列表)
	 * @param request
	 * @param response
	 * @param search
	 * @param order
	 * @param offset
	 * @param limit
	 * @return
	 */
	@RequestMapping("roleTable.do")
	@ResponseBody
	public Map<String , Object> roleTable(HttpServletRequest request, HttpServletResponse response,
			String search, String order,int page, int rows) {
		Map<String,Object> model = new HashMap<String,Object>();
		try {
			User user = HttpUtil.getCurrentUser(request);
			String orgNo = user.getAccOrgNo();
			
			Role queryCondition = JacksonUtil.fromJson(search, Role.class);
			if(orgNo!=null && !"".equals(orgNo) && !"100000000".equals(orgNo))
				queryCondition.setOrganno(orgNo);
			List<Role> list = roleSV.paginateQuery(queryCondition, order, page-1, rows);
			long totalCount = roleSV.totalCount(queryCondition);
			model.put("rows", list);
			model.put("total", totalCount);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return model;
	}
	
	/***进入新增角色页面***/
	@RequestMapping("addRole.do")
	public String addRole(HttpServletRequest request, HttpServletResponse response){
		return "views/base/authority/add_role";
	}
	/***进入编辑角色页面***/
	@RequestMapping("modifyRole.do")
	public String modifyRole(HttpServletRequest request, HttpServletResponse response){
		try {
			String roleNo = HttpUtil.getAsString(request, "roleNo");
			Role role = roleSV.queryByNo(roleNo);
			request.setAttribute("role", role);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "views/base/authority/modify_role";
	}
	/**
	 * 保存角色信息
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveSubmit.json")
	@ResponseBody
	public Map<String , Object> saveSubmit(HttpServletRequest request, HttpServletResponse response){
		Map<String,Object> model = new HashMap<String,Object>();
		try {
			String jsonString = HttpUtil.getAsString(request, "jsonString");
			Role role = JacksonUtil.fromJson(jsonString, Role.class);
			roleSV.saveOrUpdate(role);
			model.put("success", true);
		} catch (Exception e) {
			model.put("msg", "系统异常，请联系管理员！");
			e.printStackTrace();
		}
		return model;
	}
	
	
	/**
	 * 删除角色信息
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("deleteRole.json")
	@ResponseBody
	public Map<String , Object> deleteRole(HttpServletRequest request, HttpServletResponse response){
		Map<String,Object> model = new HashMap<String,Object>();
		try {
			String roleNo = HttpUtil.getAsString(request, "roleNo");
			roleSV.delete(roleNo);
			model.put("success", true);
		} catch (Exception e) {
			model.put("msg", "系统异常，请联系管理员！");
			e.printStackTrace();
		}
		return model;
	}
	
}
