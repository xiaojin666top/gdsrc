package com.beawan.base.controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.common.SysConstants.ExceptionConstant;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.base.entity.SysDic;
import com.beawan.base.service.ISysDicSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/base/dic" })
public class SysDicCtl {
	
	private static final Logger log = Logger.getLogger(SysDicCtl.class);

	@Resource
	private ISysDicSV sysDicSV;
	
	/**
	 * @Description 根据字典类型进行匹配查询
	 * @param request
	 * @param response
	 * @param optTypes 包含多个类型的字符串
	 * @return
	 */
	@RequestMapping("loadDicByOptTypes.json")
	@ResponseBody
	public String loadDicByOptTypes(HttpServletRequest request, HttpServletResponse response,
			String optTypes) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			List<SysDic> dicList = sysDicSV.queryByTypes(optTypes);
			json.put("dicList", dicList);
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description 根据字典类型进行查询
	 * @param request
	 * @param response
	 * @param optType 字典类型
	 * @return
	 */
	@RequestMapping("getDicByOptType.json")
	@ResponseBody
	public String getDicByOptType(HttpServletRequest request, HttpServletResponse response,
			String optType) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			List<SysDic> dicList = sysDicSV.findByType(optType);
			json.put("dicList", dicList);
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (页面跳转，跳转新增字典项页面)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("addDic.do")
	public String addDic(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("dic", new SysDic());
		return "views/base/staticdata/dicEdit";
	}
	
	/**
	 * @Description (页面跳转，跳转编辑字典项页面)
	 * @param request
	 * @return
	 */
	@RequestMapping("editDic.do")
	public String editDic(HttpServletRequest request, long sdId) {
		try {
			SysDic dic = sysDicSV.findById(sdId);
			request.setAttribute("dic", dic);
		} catch (Exception e) {
			log.error("跳转编辑字典项页面异常", e);
		}
		return "views/base/staticdata/dicEdit";
	}
	
	/**
	 * @Description 保存字典信息
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveDic.do")
	@ResponseBody
	public String saveDic(HttpServletRequest request, HttpServletResponse response,
			String jsondata) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			sysDicSV.saveDicInfo(jsondata);
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", "保存字典信息异常！");
			log.error("保存字典信息异常！", e);
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description 删除字典项
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("deleteDic.do")
	@ResponseBody
	public String deleteDic(HttpServletRequest request, HttpServletResponse response,
			long sdId) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			sysDicSV.delete(sysDicSV.findById(sdId));
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", "删除字典异常！");
			log.error("删除字典异常！", e);
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (页面跳转，跳转字典项列表)
	 * @param request
	 * @param response
	 * @param index
	 * @return
	 */
	@RequestMapping("dicManage.do")
	public String dicManage(HttpServletRequest request, HttpServletResponse response) {
		return "views/base/staticdata/dicManage";
	}

	/**
	 * @Description 根据字典类型进行查询
	 * @param request
	 * @param response
	 * @param optType 字典类型
	 * @return
	 */
	@RequestMapping("getDics.json")
	@ResponseBody
	public String getDics(HttpServletRequest request, HttpServletResponse response,
			String search, String order, int page, int rows) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			
			SysDic queryCondition = null;
			if (!StringUtil.isEmptyString(search)) {
				queryCondition = JacksonUtil.fromJson(search, SysDic.class);
			}
			
			long totalCount = sysDicSV.queryDicCount(queryCondition);
			List<SysDic> list = sysDicSV.queryDicPaging(queryCondition, order, page-1, rows);	
			json.put("rows", list);
			json.put("total", totalCount);
			
			json.put("result", true);
		} catch (Exception e) {
			
			json.put("result", false);
			json.put("msg", "字典项查询异常");
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
}
