package com.beawan.base.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.common.SysConstants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.base.entity.IndusPolicySet;
import com.beawan.base.service.IIndusPolicySetSV;
import com.beawan.base.service.ISysDicSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/base/indusPolicy" })
public class IndusPolicySetCtl {
	
	private static final Logger log = Logger.getLogger(IndusPolicySetCtl.class);

	@Resource
	private ISysDicSV sysDicSV;
	
	@Resource
	private IIndusPolicySetSV indusPolicySetSV;
	
	/**
	 * @Description (页面跳转，跳转行业政策设置页面)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("indusPolicySet.do")
	public String indusPolicySet(HttpServletRequest request, HttpServletResponse response) {
		
		try {
			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.STD_INDUSTRY_POLICY,
					SysConstants.BsDicConstant.STD_INDUS_POLICY_TYPE};
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
		} catch (Exception e) {
			log.error("跳转行业政策设置页面异常 ", e);
		}
		
		return "views/base/indusPolicy/indusPolicySet";
	}
	
	/**
	 * TODO 查询行业政策设置记录列表
	 * @param request
	 * @param response
	 * @param search 查询条件
	 * @param order 排序条件
	 * @param page 页码，从1开始
	 * @param rows 每页行数
	 * @return
	 */
	@RequestMapping("getIndusPolicys.do")
	@ResponseBody
	public String getIndusPolicys(HttpServletRequest request, HttpServletResponse response,
			String search, String order, int page, int rows) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);
		
		try {
			
			IndusPolicySet queryCondition = null;
			if (!StringUtil.isEmptyString(search)) {
				queryCondition = JacksonUtil.fromJson(search, IndusPolicySet.class);
			}
			
			List<?> list = indusPolicySetSV.queryPaging(queryCondition, order,
					page-1, rows);
			long total = indusPolicySetSV.queryCount(queryCondition);
			json.put("total", total);
			json.put("rows", list);
			
			json.put("result", true);
		} catch (Exception e) {
			log.error(" 查询行业政策设置记录列表异常", e);
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * TODO 保存行业政策设置记录
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("saveIndusPolicy.do")
	@ResponseBody
	public String saveIndusPolicy(HttpServletRequest request, HttpServletResponse response,
			String jsondata) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);
		
		try {
			
			IndusPolicySet indusPolicySet = JacksonUtil.fromJson(jsondata, IndusPolicySet.class);
			indusPolicySetSV.saveOrUpdate(indusPolicySet);
			
			json.put("result", true);
		} catch (Exception e) {
			json.put("msg", "保存出错！");
			log.error("保存行业政策设置记录异常", e);
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * TODO 删除行业政策设置记录
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("deleteIndusPolicy.do")
	@ResponseBody
	public String deleteIndusPolicy(HttpServletRequest request, HttpServletResponse response,
			long id) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);
		
		try {
			
			indusPolicySetSV.deleteById(id);
			
			json.put("result", true);
		} catch (Exception e) {
			json.put("msg", "删除出错！");
			log.error("删除行业政策设置记录异常", e);
		}
		
		return jRequest.serialize(json, true);
	}
	
}
