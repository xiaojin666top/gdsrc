package com.beawan.base.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.beawan.base.dto.OrganizationDto;
import com.beawan.base.dto.UserDto;

import net.sf.json.JSONObject;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.common.BusinessException;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.Pagination;
import com.beawan.core.ResultDto;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.beawan.base.entity.Organization;
import com.beawan.base.entity.Role;
import com.beawan.base.entity.User;
import com.beawan.base.qbean.QUser;
import com.beawan.base.service.IOrganizationSV;
import com.beawan.base.service.IRoleSV;
import com.beawan.base.service.ISysDicSV;
import com.beawan.base.service.IUserSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.DateUtil;
import com.platform.util.EncryptUtil;
import com.platform.util.ExceptionUtil;
import com.platform.util.HttpUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/base/user" })
public class UserCtl extends BaseController{
	
	private static final Logger log = Logger.getLogger(UserCtl.class);

	@Resource
	private IUserSV userSV;
	
	@Resource
	private IRoleSV roleSV;
	
	@Resource
	private ISysDicSV sysDicSV;
	
	@Resource
	private ICmisInInvokeSV cmisInInvokeSV;
	@Resource
	private IOrganizationSV organizationSV;
	
	/**
	 * @Description 跳转新增用户页面
	 * @param request
	 * @param response
	 * @param userId
	 * @return
	 */
	@RequestMapping("addUser.do")
	public String addUser(HttpServletRequest request, HttpServletResponse response,
			String userId) {
		
		try {
			
			User currUser = HttpUtil.getCurrentUser(request);
			List<Role> roles = null;
			//超级管理员
			if(currUser.getRoleNo().contains(SysConstants.UserRole.SUPER_ADMIN))
				roles = roleSV.queryAll();
			else
				roles = roleSV.query();
			request.setAttribute("roles", JacksonUtil.serialize(roles));
			
			//加载字典数据
			String[] optTypes = {
					SysConstants.BsDicConstant.STD_SY_GB00005, 
					SysConstants.BsDicConstant.STD_SY_BUSI_LINE,
					SysConstants.BsDicConstant.STD_USER_STATUS,
					SysConstants.BsDicConstant.STD_ALLOW_PLAT,
					SysConstants.BsDicConstant.STD_SY_DATA_ACCESS};
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
			
		} catch (Exception e) {
			log.error("跳转新增用户页面出错", e);
			e.printStackTrace();
		}
		
		return "views/base/user/userAdd";
	}
	
	/**
	 * @Description 检查用户是否已经存在
	 * @param request
	 * @param response
	 * @param userId 用户编号
	 * @return
	 */
	@RequestMapping("userCheck.do")
	@ResponseBody
	public String userCheck(HttpServletRequest request, HttpServletResponse response,
			String userId) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			
			if(StringUtil.isEmptyString(userId))
				ExceptionUtil.throwException("用户编号不能为空！");
			
			User user = userSV.queryById(userId);
			if(user != null)
				ExceptionUtil.throwException("用户已存在！");
			
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", e.getMessage());
			log.error("检查用户是否已经存出错：" + e.getMessage());
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description 保存新增用户信息
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveUser.do")
	@ResponseBody
	public String saveUser(HttpServletRequest request, HttpServletResponse response,
			String jsondata) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			JSONObject userInfo = JSONObject.fromObject(jsondata);
			User user = userSV.queryById(userInfo.getString("userId"));
			if(user != null)
				ExceptionUtil.throwException("用户已存在！");
			userSV.saveUserInfo(jsondata);
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", e.getMessage());
			log.error("保存用户信息出错", e);
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description 更新用户信息
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("updateUser.do")
	@ResponseBody
	public String updateUser(HttpServletRequest request, HttpServletResponse response,
			String jsondata) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			
			User newUser = userSV.saveUserInfo(jsondata);
			User loginUser = HttpUtil.getCurrentUser(request);
			//修改当前用户信息
			if(newUser.getUserId().equals(loginUser.getUserId())){
				HttpUtil.getSession().setAttribute(
						Constants.SessionKey.SESSION_KEY_USER, newUser);
			}
			
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", e.getMessage());
			log.error("更新用户信息出错", e);
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description 删除用户信息
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("deleteUser.do")
	@ResponseBody
	public String deleteUser(HttpServletRequest request, HttpServletResponse response,
			String userId) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			
			User currUser = HttpUtil.getCurrentUser(request);
			String currUserRole = currUser.getRoleNo();
			User user = userSV.queryById(userId);
			//如果是超级管理员可以任意删除
			if(!currUserRole.contains(SysConstants.UserRole.SUPER_ADMIN)){
				if(currUserRole.contains(SysConstants.UserRole.SYSTEM_ADMIN)){
					//系统管理员不能删除系统管理员及超级管理员
					if(user.getRoleNo().contains(SysConstants.UserRole.SUPER_ADMIN)
							|| user.getRoleNo().contains(SysConstants.UserRole.SYSTEM_ADMIN)){
						ExceptionUtil.throwException("对不起，您没有权限删除该用户！");
					}
				}else
					ExceptionUtil.throwException("对不起，您没有权限删除该用户！");
			}
			
			userSV.delete(user);
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", e.getMessage());
			log.error("删除用户信息出错", e);
		}
		
		return jRequest.serialize(json, true);
	}

	/**
	 * @Description 跳转修改用户信息页面
	 * @param request
	 * @param response
	 * @param userId
	 * @return
	 */
	@RequestMapping("editUser.do")
	public String editUser(HttpServletRequest request, HttpServletResponse response,
			String userId) {
		
		try {
			
			User user = userSV.queryById(userId);
			request.setAttribute("orgName", cmisInInvokeSV.queryOrgName(user.getAccOrgNo(), false));
			request.setAttribute("user", user);
			
			User currUser = HttpUtil.getCurrentUser(request);
			List<Role> roles = null;
			//超级管理员
			if(currUser.getRoleNo().contains(SysConstants.UserRole.SUPER_ADMIN))
				roles = roleSV.queryAll();
			else
				roles = roleSV.query();
			request.setAttribute("roles", JacksonUtil.serialize(roles));
			
			//加载字典数据
			String[] optTypes = {
					SysConstants.BsDicConstant.STD_SY_GB00005, 
					SysConstants.BsDicConstant.STD_SY_BUSI_LINE,
					SysConstants.BsDicConstant.STD_USER_STATUS,
					SysConstants.BsDicConstant.STD_ALLOW_PLAT,
					SysConstants.BsDicConstant.STD_SY_DATA_ACCESS};
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
			
			String flag = request.getParameter("flag");
			
			//查看
			if(!StringUtil.isEmptyString(flag) && flag.equals("view")){
				request.setAttribute("inputOrg", cmisInInvokeSV.queryOrgName(user.getInputOrg(), false));
				request.setAttribute("inputUser", userSV.queryById(user.getInputUser()));
				request.setAttribute("updateUser", userSV.queryById(user.getUpdateUser()));
			}else
				flag = "edit";
			request.setAttribute("flag", flag);
			
		} catch (Exception e) {
			log.error("跳转修改用户信息页面出错",e);
			e.printStackTrace();
		}
		
		return "views/base/user/userEdit";
	}
	
	/**
	 * @Description 跳转用户修改个人信息页面
	 * @param request
	 * @param response
	 * @param userId
	 * @return
	 */
	@RequestMapping("personInfo.do")
	public String personInfo(HttpServletRequest request, HttpServletResponse response) {
		
		try {
			
			User user = HttpUtil.getCurrentUser(request);
			request.setAttribute("orgName", cmisInInvokeSV.queryOrgName(user.getAccOrgNo(), false));
			request.setAttribute("user", user);
			
			List<Role> roles = roleSV.query();
			request.setAttribute("roles", JacksonUtil.serialize(roles));
			
			//加载字典数据
			String[] optTypes = {
					SysConstants.BsDicConstant.STD_SY_GB00005, 
					SysConstants.BsDicConstant.STD_SY_BUSI_LINE,
					SysConstants.BsDicConstant.STD_USER_STATUS,
					SysConstants.BsDicConstant.STD_ALLOW_PLAT,
					SysConstants.BsDicConstant.STD_SY_DATA_ACCESS};
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
			
			
			//查看
			request.setAttribute("inputOrg", cmisInInvokeSV.queryOrgName(
					user.getInputOrg(), false));
			request.setAttribute("inputUser", userSV.queryById(
					user.getInputUser()).getUserName());
			request.setAttribute("updateUser", userSV.queryById(
					user.getUpdateUser()).getUserName());
			
		} catch (Exception e) {
			log.error("跳转用户修改个人信息页面出错",e);
			e.printStackTrace();
		}
		
		return "views/base/user/personInfo";
	}

	/**
	 * @Description 跳转到密码重置页面
	 * @param request
	 * @param response
	 * @param userId
	 * @return
	 */
	@RequestMapping("resetPwd.do")
	public String resetPassword(HttpServletRequest request, HttpServletResponse response,
			String userId) {
		try {
			User user = userSV.queryById(userId);
			request.setAttribute("userId", userId);
			request.setAttribute("userName", user.getUserName());
		} catch (Exception e) {
			log.error("跳转修改用户信息页面出错",e);
			e.printStackTrace();
		}
		return "views/base/user/resetPwd";
	}
	
	/**
	 * 跳转到修改密码页面
	 * @author: xyh
	 * @date:   2020年5月20日 下午3:27:30   
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("modifyPassword.do")
	public ModelAndView modifyPasswordreal(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("views/base/user/modify_pwd");
		return mav;
	}
	
	/**
	 * 跳转修改客户信息界面
	 * @author: xyh
	 * @date:   2020年5月20日 下午3:28:24   
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("modifyMyInfo.do")
	public ModelAndView modifyMyInfo(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("views/base/user/modify_my_info");
		//session中获取客户信息
		User user = HttpUtil.getCurrentUser(request);
		try {
			//用户信息
			User reUser = userSV.queryById(user.getUserId());
			mav.addObject("user", reUser);
			//角色信息
			List<Role> roles = roleSV.queryListByNos(reUser.getRoleNo());
			StringBuffer roleString = new StringBuffer();
			if(roles != null){
				for(Role role : roles){
					roleString.append(role.getRoleName());
				}
			}
			mav.addObject("roleName",roleString.toString());
			//机构信息
			Organization org = organizationSV.queryByNo(reUser.getAccOrgNo());
			mav.addObject("organName",org.getOrganname());
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取用户信息失败",e);
		}
		return mav;
	}
	
	/**
	 * 跳转修改客户信息界面
	 * @author: xyh
	 * @date:   2020年5月20日 下午3:28:24   
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("submitMyInfo.do")
	@ResponseBody
	public ResultDto submitMyInfo(HttpServletRequest request, HttpServletResponse response,
			String realName,String phone,String mail) {
		ResultDto result = returnFail("");
		//session中获取客户信息
		User user = HttpUtil.getCurrentUser(request);
		try {
			//用户信息
			User reUser = userSV.queryById(user.getUserId());
			reUser.setUserName(realName);
			reUser.setPhone(phone);
			reUser.setEmail(mail);
			userSV.saveOrUpdate(reUser);
			result = returnSuccess();
			//reUser.setPassword("******");
			result.setRows(reUser);
		} catch (Exception e) {
			e.printStackTrace();
			result.setMsg("修改个人信息失败");
			log.error("修改个人信息失败",e);
		}
		return result;
	}
	
	
	/**
	 * @Description 密码重置提交
	 * @param request
	 * @param response
	 * @param userId
	 * @param newPwd
	 * @return
	 */
	@RequestMapping("submitResetPwd.do")
	@ResponseBody
	public ResultDto submitResetPwd(HttpServletRequest request, HttpServletResponse response,
			String userId, String newPwd) {
		ResultDto result = returnFail("密码重置错误");
		try {
			if(StringUtil.isEmptyString(userId)||StringUtil.isEmptyString(newPwd)){
				ExceptionUtil.throwException("密码重置错误：参数不完整");
			}
			
			userSV.resetPassword(userId, newPwd);
			result = returnSuccess();
		}catch (BusinessException e) {
			result.setMsg(e.getMessage());
			log.error("密码重置出错:"+e.getMessage());
		} catch (Exception e) {
			log.error("密码重置出错", e);
		}
		return result;
	}
	
	@RequestMapping("myPswSubmit.do")
	@ResponseBody
	public ResultDto myPswSubmit(HttpServletRequest request, HttpServletResponse response,
			String lastPassword, String newPassword,String finalPassword) {
		ResultDto result = returnFail("密码重置错误");
		User user = HttpUtil.getCurrentUser(request);
		try {
			if(StringUtil.isEmptyString(lastPassword)||StringUtil.isEmptyString(newPassword)
					||StringUtil.isEmptyString(finalPassword)){
				ExceptionUtil.throwException("密码重置错误：参数不完整");
			}
			if(!newPassword.equals(finalPassword)){
				ExceptionUtil.throwException("密码重置错误：两次密码不一致");
			}
			userSV.resetPassword(user.getUserId(),lastPassword, newPassword);
			result = returnSuccess();
		}catch (BusinessException e) {
			result.setMsg(e.getMessage());
			log.error("密码重置出错:"+e.getMessage());
		} catch (Exception e) {
			log.error("密码重置出错", e);
		}
		return result;
	}
	/**
	 * @Description 跳转到个人密码修改页面
	 * @param request
	 * @param response
	 * @param userId
	 * @return
	 */
	@RequestMapping("modifyPwd.do")
	public String modifyPassword(HttpServletRequest request, HttpServletResponse response) {
		request.setAttribute("userId", HttpUtil.getCurrentUser(request).getUserId());
		return "views/base/user/modifyPwd";
	}
	
	/**
	 * @Description 个人密码修改提交
	 * @param request
	 * @param response
	 * @param userId
	 * @param oldPwd
	 * @param newPwd
	 * @return
	 */
	@RequestMapping("submitModifyPwd.do")
	@ResponseBody
	public String submitModifyPwd(HttpServletRequest request, HttpServletResponse response,
			String userId, String oldPwd, String newPwd) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			userSV.modifyPwdPC(userId, oldPwd, newPwd);
			json.put("result", true);
		} catch (BusinessException be) {
			json.put("result", false);
			json.put("msg", be.getMessage());
			log.error("密码修改出错:"+be.getMessage());
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", "密码修改出错");
			log.error("密码修改出错", e);
		}
		
		return jRequest.serialize(json, true);
	}

	/**
	 * @Description (得到所有用户列表)
	 * @param request
	 * @param response
	 * @param search 查询条件json串
	 * @param order 排序条件
	 * @param page 起始页
	 * @param rows 每页大小
	 * @return
	 */
	@RequestMapping("getUsers.do")
	@ResponseBody
	public ResultDto getUsers(HttpServletRequest request, HttpServletResponse response,
			String search, String order,int page, int rows) {
		ResultDto re = returnFail("");
		try {
			//获取查询条件
			QUser queryCondition = null;
			if (!StringUtil.isEmptyString(search)) {
				queryCondition = JacksonUtil.fromJson(search, QUser.class);
			}
			//获取当前登录用户
			User currentUser = HttpUtil.getCurrentUser(request);
			//获取当前登录用户的当前机构及子机构
			List<OrganizationDto> orgTree = organizationSV.queryChildTree(currentUser.getAccOrgNo());
			Set<String> orgs = new HashSet<>();
			orgs.add(currentUser.getAccOrgNo());
			organizationSV.getChildList(orgs, orgTree);
			queryCondition.setOrgChilds(orgs);
			long totalCount = userSV.queryUserCount(queryCondition);
			List<?> list = userSV.queryUserPaging(queryCondition, order, page-1, rows);
			re = returnSuccess();
			re.setRows(list);
			re.setTotal(totalCount);
		} catch (Exception e) {
			re.setMsg("查询用户列表出错！");
			log.error("查询用户列表出错", e);
		}
		return re;
	}
	
	/**
	 * @Description 跳转到批量导入页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("uploadXls.do")
	public String uploadXls(HttpServletRequest request, HttpServletResponse response) {
		return "views/base/user/uploadXlsPage";
	}

	@RequestMapping("importUsers.do")
	@ResponseBody
	public String importUsers(HttpServletRequest request, HttpServletResponse response) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);
		
		try {
			
			User operator = HttpUtil.getCurrentUser(request);
			
			// 保存图片
			List<MultipartFile> mFileList = ((MultipartRequest) request).getFiles("file");
			if (CollectionUtils.isEmpty(mFileList)) 
				ExceptionUtil.throwException("未上传文件！");
			
			MultipartFile mfile = mFileList.get(0);
			
			Sheet sheet;
			if(mfile.getOriginalFilename().endsWith(".xls")){
				HSSFWorkbook wb = new HSSFWorkbook(mfile.getInputStream());
				sheet = wb.getSheetAt(0);
			}else{
				XSSFWorkbook wb = new XSSFWorkbook(mfile.getInputStream());
				sheet = wb.getSheetAt(0);
			}
			
			Row rowHeader = sheet.getRow(sheet.getFirstRowNum());
			String[] headers = new String[rowHeader.getLastCellNum()+1];
			for(int i = rowHeader.getFirstCellNum(); i <= rowHeader.getLastCellNum(); i++){
				Cell cell = rowHeader.getCell(i);
				if(cell == null)
					continue;
				headers[i] = cell.getStringCellValue();
			}
			
			for(int i = sheet.getFirstRowNum()+1; i <= sheet.getLastRowNum(); i++){
				
				Row row = sheet.getRow(i);
				User user = new User();
				for(int j = row.getFirstCellNum(); j <= row.getLastCellNum(); j++){
					
					Cell cell = row.getCell(j);
					if(cell == null)
						continue;
					
					String value;
					switch(cell.getCellType()){
						case Cell.CELL_TYPE_NUMERIC: value = ""+cell.getNumericCellValue(); break;
						case Cell.CELL_TYPE_BOOLEAN: value = ""+cell.getBooleanCellValue(); break;
						default: value = cell.getStringCellValue();
					}
					
					if("信贷系统ID".equals(headers[j])){
						user.setUserId(value);
						if(value != null)
							user.setLoginId(value.replace("I05", ""));
					} else if("姓名".equals(headers[j])) 
						user.setUserName(value);
					else if("机构号".equals(headers[j])) 
						user.setAccOrgNo(value);
					else if("用户角色".equals(headers[j])){
						
						if("客户经理".equals(value)){
							user.setRoleNo(SysConstants.UserRole.CUSTOMER_MANAGER);
							user.setDataAccess(SysConstants.UserDataAccess.PERSONAL);
						} else if("运行主管".equals(value)){
							user.setRoleNo(SysConstants.UserRole.OPER_DIRECTOR);
							user.setDataAccess(SysConstants.UserDataAccess.ORG);
						} else if("支行行长".equals(value)){
							user.setRoleNo(SysConstants.UserRole.BRANCH_MANAGER);
							user.setDataAccess(SysConstants.UserDataAccess.ORG);
						} else if("评审经理".equals(value)){
							user.setRoleNo(SysConstants.UserRole.REVIEW_MANAGER);
							user.setDataAccess(SysConstants.UserDataAccess.ALL);
						} else if("总行审批岗".equals(value)){
							user.setRoleNo(SysConstants.UserRole.APPROVER);
							user.setDataAccess(SysConstants.UserDataAccess.ALL);
						} else if("系统管理员".equals(value)){
							user.setRoleNo(SysConstants.UserRole.SYSTEM_ADMIN);
							user.setDataAccess(SysConstants.UserDataAccess.ALL);
						}
						
					} else if("手机号码".equals(headers[j])){
						user.setPhone(value);
					}
				}
				
				String datetime = DateUtil.format(new Date(), Constants.DATETIME_MASK);
				
				user.setPassword(EncryptUtil.md5(user.getUserId()));
				user.setState(SysConstants.UserStatus.NORMAL);
				user.setBusinessLine("0");
				user.setSignState("online");//新增用户默认在岗
				user.setWrongPinNum(0);
				user.setConWrongPinNum(0);
				user.setAllowWorkPlat(Constants.Platform.ALL);
				user.setLastPassword(user.getPassword());
				user.setInputOrg(operator.getAccOrgNo());
				user.setInputUser(operator.getUserId());
				user.setInputTime(datetime);
				user.setUpdateUser(operator.getUserId());
				user.setUpdateTime(DateUtil.getNowTimestamp());
				
				userSV.saveOrUpdate(user);
			}
			
			json.put("result", true);
			
		} catch (BusinessException be) {
			json.put("msg", be.getMessage());
			log.error("导入用户列表出错："+be.getMessage());
		} catch (Exception e) {
			json.put("msg", "导入用户列表出错！");
			log.error("导入用户列表出错", e);
		}
		
		return jRequest.serialize(json, true);
	}
	
	/*
	 * 解除账号锁定
	 */
	@RequestMapping("relieveLock.do")
	@ResponseBody
	public String relieveLock(HttpServletRequest request, HttpServletResponse response,String userId){
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		int status = this.userSV.updateLock(userId);
		if(status == 1){
			json.put("msg", "解除锁定成功!");

		}else{
			json.put("msg", "接触锁定失败!");

		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * 
	 *@Description 获取审查客户经理的列表
	 *@param page
	 *@param rows
	 *@param userName
	 *@param userNo
	 *@return
	 * @author xyh
	 */
	@RequestMapping("queryLeUserList.json")
	@ResponseBody
	public ResultDto queryLeUserList( @RequestParam(value="page",defaultValue="1")int page,
									  @RequestParam(value="rows",defaultValue="10")int rows,
									  String userName, String userNo) {
		ResultDto re = returnFail();
		try {
			Pagination<UserDto> pager = userSV.queryUserPaging(Constants.ROLE.LE_USER, userName, userNo, page, rows);
			re = returnSuccess();
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	
}
