package com.beawan.base.controller;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.base.entity.FunMenu;
import com.beawan.base.service.IFunMenuSV;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;


@Controller
@RequestMapping({ "/base/funMenu" })
@UserSessionAnnotation
public class FunMenuCtl {
	
	private static final Logger log = Logger.getLogger(FunMenuCtl.class);
	
	@Resource
	private IFunMenuSV funMenuSV;
	
	/***跳转到功能菜单管理页面***/
	@RequestMapping("menuMge.do")
	public String menuMge(HttpServletRequest request, HttpServletResponse response){
		return "views/base/funMenu/menuList";
	}
	
	
	/**
	 * TODO 根据菜单编号获得子菜单列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getChildren.json")
	@ResponseBody
	public String getChildren(HttpServletRequest request, HttpServletResponse response) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<FunMenu> dataGrid = null;
		try {
			String menuNo = request.getParameter("id");
			if(StringUtil.isEmptyString(menuNo))
				menuNo = "0";
			dataGrid = funMenuSV.queryTreeMenu(menuNo);
		} catch (Exception e) {
			log.error("得到子菜单列表异常", e);
		}
		
		return jRequest.serialize(dataGrid, true);
	}
	
	/**
	 * @Description 保存菜单
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveMenu.json")
	@ResponseBody
	public String saveMenu(HttpServletRequest request, HttpServletResponse response) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);
		
		try {
			String jsondata = request.getParameter("jsondata");
			FunMenu menu = JacksonUtil.fromJson(jsondata, FunMenu.class);
			funMenuSV.saveOrUpdate(menu);
			
			json.put("result", true);
		} catch (Exception e) {
			json.put("msg", "系统异常，请联系管理员！");
			log.error("保存菜单异常", e);
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description 删除菜单
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("deleteMenu.json")
	@ResponseBody
	public String deleteMenu(HttpServletRequest request, HttpServletResponse response) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);
		
		try {
			String menuNo = request.getParameter("menuNo");
			funMenuSV.deleteByMenuNo(menuNo);
			
			json.put("result", true);
		} catch (Exception e) {
			json.put("msg", "删除菜单异常！");
			log.error("删除菜单异常：", e);
		}
		
		return jRequest.serialize(json, true);
	}
	
}
