package com.beawan.base.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.base.entity.IllegalRecord;
import com.beawan.base.entity.User;
import com.beawan.base.qbean.QUser;
import com.beawan.base.service.IUserSV;
import com.beawan.base.service.IllRecordSV;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("/illRecord")
@UserSessionAnnotation
public class IllegalRecordCtl extends BaseController {

	private static final Logger log = Logger.getLogger(UserCtl.class);
	
	@Resource
	private IUserSV userSV;
	
	@Resource
	private IllRecordSV illRecordSV;
	
	/*
	 * 跳转违规信息新增页面
	 */
	@RequestMapping("illAddPage.do")
	public String illegalAddPage(HttpServletRequest request, HttpServletResponse response){
		
		
		try {
			List<User> users = this.userSV.queryUserNOAdmin();
			request.setAttribute("users", JacksonUtil.serialize(users));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/base/user/illegalAdd";
	}
	
	
	
	/**
	 * @Description (得到所有用户列表)
	 * @param request
	 * @param response
	 * @param search 查询条件json串
	 * @param order 排序条件
	 * @param offset 起始页
	 * @param limit 每页大小
	 * @return
	 */
	@RequestMapping("getIllRecord.do")
	@ResponseBody
	public String getUsers(HttpServletRequest request, HttpServletResponse response,
			String search, String order,int page, int rows) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);		
		Map<String, Object> map =null;
		try {
			
			QUser queryCondition = null;
			if (!StringUtil.isEmptyString(search)) {
				queryCondition = JacksonUtil.fromJson(search, QUser.class);
			}
			
		     map = illRecordSV.queryUserPaging(queryCondition,order, page-1, rows);
			 map.put("result", true);
		} catch (Exception e) {
			log.error("查询用户列表出错", e);
		}
		
		return jRequest.serialize(map, true);
	}
	
	
	
	/**
	 * @Description 保存新增违规用户信息
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveIllUser.do")
	@ResponseBody
	public String saveIllUser(HttpServletRequest request, HttpServletResponse response,
			String jsondata) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			illRecordSV.saveIllegalRecord(jsondata);
			
			json.put("result", true);
			
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", e.getMessage());
			log.error("保存用户信息出错", e);
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description 删除违规用户信息
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("delIllUser.do")
	@ResponseBody
	public String delIllUser(HttpServletRequest request, HttpServletResponse response,
			String id) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			this.illRecordSV.deleteIllRecord(Long.valueOf(id));
			json.put("result", true);
			
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", e.getMessage());
			log.error("删除用户信息出错", e);
		}
		
		return jRequest.serialize(json, true);
	}
	
	
	
	/**
	 * @Description 跳转查看/修改用户违规内容页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("editIllUser.do")
	public String editIllUser(HttpServletRequest request, HttpServletResponse response,
			String id) {
		
		try {
			IllegalRecord illRecord = this.illRecordSV.viewIllRecord(Long.valueOf(id));
			request.setAttribute("illRecord", illRecord);
			String flag = request.getParameter("flag");
			
			if(!"view".equals(flag)){
				List<User> users = this.userSV.queryUserNOAdmin();
				request.setAttribute("users", JacksonUtil.serialize(users));
				
			}
			request.setAttribute("flag", flag);
			request.setAttribute("id", id);
		} catch (Exception e) {
			log.error("跳转修改用户信息页面出错",e);
			e.printStackTrace();
		}
		
		return "views/base/user/illRecordEdit";
	}
	
	/**
	 * @Description 修改用户违规信息
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("updataIllUser.do")
	@ResponseBody
	public String updataIllUser(HttpServletRequest request, HttpServletResponse response,
			String jsondata) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> jsonResult = new HashMap<String, Object>();
		
		try {
			JSONObject json = JSONObject.fromObject(jsondata);
			IllegalRecord illegalRecord = (IllegalRecord) JSONObject.toBean(json, IllegalRecord.class);
			System.out.println("id:"+illegalRecord.getId());
			System.out.println("userID:"+illegalRecord.getIllPersonId());
			
			this.illRecordSV.updateInfo(illegalRecord);
			jsonResult.put("result", true);
		} catch (Exception e) {
			jsonResult.put("result", false);
			jsonResult.put("msg", e.getMessage());
			log.error("更新用户信息出错", e);
		}
		
		return jRequest.serialize(jsonResult, true);
	}
	
	
	
}
