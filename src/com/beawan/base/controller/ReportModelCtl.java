package com.beawan.base.controller;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.beawan.base.service.IPlatformLogSV;
import com.beawan.common.annotation.UserSessionAnnotation;

@Controller
@RequestMapping({ "/reportmodel" })
@UserSessionAnnotation
public class ReportModelCtl {
	
	private static final Logger log = Logger.getLogger(ReportModelCtl.class);
	@Resource
	private IPlatformLogSV platformLogSV;
	
	/**
	 * @Description (登录请求返回登录页面)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("reportmodel.do")
	public String reportmodel(HttpServletRequest request, HttpServletResponse response) {
		return "views/base/reportmodel/reportModelList";
	}

}
