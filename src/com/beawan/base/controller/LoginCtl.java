package com.beawan.base.controller;


import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.base.entity.User;
import com.beawan.base.service.IPlatformLogSV;
import com.beawan.base.service.IUserSV;
import com.beawan.common.BusinessException;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.aopLog.LoanAopLog;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.platform.util.ExceptionUtil;
import com.platform.util.HttpUtil;
import com.platform.util.StringUtil;
/**
 * 
 * @ClassName:  LoginCtl   
 * @Description:TODO(登录控制器)   
 * @author: xyh
 * @date:   2020年5月20日 上午9:11:46
 *  修正：1.集成了BaseController，统一返回数据格式
 *        2.修复登录错误次数锁定账号功能
 */
@Controller
@RequestMapping({ "/" })
public class LoginCtl extends BaseController{
	
	private static final Logger log = Logger.getLogger(LoginCtl.class);
	@Resource
	private IPlatformLogSV platformLogSV;
	@Resource
	private IUserSV userSV;
	
	/**
	 * @Description (登录请求返回登录页面)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("login.do")
	public ModelAndView login(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("views/login");
		return mav;
	}
	
	/**
	 * 清除session退出登录，并返回到登录界面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("logout.do")
	@LoanAopLog(operationName="用户退出登录",operationType="页面跳转")
	public ModelAndView logout(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("views/login");
		try {
			//将用户的session清除
			HttpSession session = request.getSession();
			session.invalidate();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}

	/**
	 * 登陆验证-----web
	 * @param request
	 * @param response
	 * @param userId  登陆名（用户编号）
	 * @param password  登陆密码
	 * @return
	 */
	@RequestMapping("userLogin.do")
	@ResponseBody
	//@LoanAopLog(operationType=SysConstants.PlatformLogType.USER_LOGIN,operationName="用户登录")
	public ResultDto userLoginPC(HttpServletRequest request, HttpServletResponse response,
			String userId, String password) {
		ResultDto resultDto = returnFail("登录失败");
		try {
			if (StringUtil.isEmptyString(userId) || StringUtil.isEmptyString(password)) {
				ExceptionUtil.throwException("请输入登录名或密码！");
			}
			//session中取出用户
			HttpSession session = HttpUtil.getSession();
			User user = (User) session
					.getAttribute(Constants.SessionKey.SESSION_KEY_USER);
			//用户不存在
			if (user == null) {
				//调用登录方法
				Map<String, Object> result = userSV.loginPC(userId, password,HttpUtil.getRemoteAddr(), null);
				//获取结果
				String status = result.get("status").toString();
				//成功
				if("1".equals(status))
					user = (User) result.get("user");
				else
					ExceptionUtil.throwException(result.get("remarks").toString());
				
				session.setAttribute(Constants.SessionKey.SESSION_KEY_USER, user);
			}
			resultDto = returnSuccess();
			resultDto.setRows(user);
		} catch (BusinessException b) {
			resultDto.setMsg(b.getMessage());
			log.error("用户登录异常:" + b.getMessage());
		} catch (Exception e) {
			resultDto.setMsg("用户登录异常！");
			log.error("用户登录异常", e);
		}
		return resultDto;
	}
	
	/**
	 * 登陆验证-----mobile
	 * @param request
	 * @param response
	 * @param userId 登陆名（用户编号）
	 * @param password 登陆密码
	 * @return
	 */
	@RequestMapping("loginMac.do")
	@ResponseBody
	public ResultDto userLoginMAC(HttpServletRequest request, HttpServletResponse response,
			String userId, String password) {
		ResultDto resultDto = returnFail("登录失败");
		try {
			if (StringUtil.isEmptyString(userId) || StringUtil.isEmptyString(password)) {
				ExceptionUtil.throwException("请输入登录名或密码！");
			}
			//session中取出用户
			HttpSession session = HttpUtil.getSession();
			User user = (User) session
					.getAttribute(Constants.SessionKey.SESSION_KEY_USER);
			//用户不存在
			if (user == null) {
				//调用登录方法
				Map<String, Object> result = userSV.loginMB(userId, password,
						HttpUtil.getRemoteAddr(), null);
				//获取结果
				String status = result.get("status").toString();
				//成功
				if("1".equals(status))
					user = (User) result.get("user");
				else
					ExceptionUtil.throwException(result.get("remarks").toString());
				
				session.setAttribute(Constants.SessionKey.SESSION_KEY_USER, user);
			}

			//先注释，这里序列有问题
			platformLogSV.saveOperateLog("用户移动端登录",
					SysConstants.PlatformLogType.USER_LOGIN, userId);// 保存操作日志
			resultDto = returnSuccess();
			resultDto.setRows(user);
		} catch (BusinessException b) {
			resultDto.setMsg(b.getMessage());
			log.error("用户登录异常:" + b.getMessage());
		} catch (Exception e) {
			resultDto.setMsg("用户登录异常！");
			log.error("用户登录异常", e);
		}
		return resultDto;
	}


	@RequestMapping("logoutMac.do")
	@ResponseBody
	public ResultDto logoutMac(HttpServletRequest request, HttpServletResponse response) {
		ResultDto resultDto = returnFail("登出失败");
		try {
			//将用户的session清除
			HttpSession session = request.getSession();
			session.invalidate();
			resultDto = returnSuccess();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultDto;
	}
}
