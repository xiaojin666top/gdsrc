package com.beawan.base.controller;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.beawan.base.entity.TrainInfo;
import com.beawan.base.entity.User;
import com.beawan.base.qbean.QUser;
import com.beawan.base.service.IUserSV;
import com.beawan.base.service.TrainInfoSV;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("/train")
@UserSessionAnnotation
public class TrainInfoCtl {

	private static final Logger log = Logger.getLogger(UserCtl.class);
	
	@Resource
	private IUserSV userSV;
	
	@Resource
	private TrainInfoSV trainInfoSV;
	
	/*
	 * 跳转违规信息新增页面
	 */
	@RequestMapping("trainAddPage.do")
	public String illegalAddPage(HttpServletRequest request, HttpServletResponse response){
		
		
		try {
			List<User> users = this.userSV.queryUserNOAdmin();
			request.setAttribute("users", JacksonUtil.serialize(users));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/base/user/trainAdd";
	}
	
	
	
	/**
	 * @Description (得到所有用户列表)
	 * @param request
	 * @param response
	 * @param search 查询条件json串
	 * @param order 排序条件
	 * @param offset 起始页
	 * @param limit 每页大小
	 * @return
	 */
	@RequestMapping("getTrainInfo.do")
	@ResponseBody
	public String getUsers(HttpServletRequest request, HttpServletResponse response,
			String search, String order,int page, int rows) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);		
		Map<String, Object> map =null;
		try {
			
			QUser queryCondition = null;
			if (!StringUtil.isEmptyString(search)) {
				queryCondition = JacksonUtil.fromJson(search, QUser.class);
			}
			
		     map = trainInfoSV.queryUserPaging(queryCondition,order, page-1, rows);
			 map.put("result", true);
		} catch (Exception e) {
			log.error("查询用户列表出错", e);
		}
		
		return jRequest.serialize(map, true);
	}
	
	
	
	/**
	 * @Description 保存新增违规用户信息
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveTrainUser.do")
	@ResponseBody
	public String saveIllUser(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value="file") MultipartFile file,TrainInfo trainInfo){
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
	 	Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			trainInfoSV.saveIllegalRecord(trainInfo,file);
			
			json.put("result", true);
			
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", e.getMessage());
			log.error("保存用户信息出错", e);
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description 删除违规用户信息
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("delTrainUser.do")
	@ResponseBody
	public String delIllUser(HttpServletRequest request, HttpServletResponse response,
			String id) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			this.trainInfoSV.deleteIllRecord(Long.valueOf(id));
			json.put("result", true);
			
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", e.getMessage());
			log.error("删除用户信息出错", e);
		}
		
		return jRequest.serialize(json, true);
	}
	
	
	
	/**
	 * @Description 跳转查看/修改用户违规内容页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("editTrainUser.do")
	public String editIllUser(HttpServletRequest request, HttpServletResponse response,
			String id) {
		
		try {
			
			System.out.println("id="+id);
			TrainInfo trainInfo = this.trainInfoSV.viewIllRecord(Long.valueOf(id));
			
			System.out.println("查看="+trainInfo);
			request.setAttribute("trainInfo", trainInfo);
			String flag = request.getParameter("flag");
			
			if(!"view".equals(flag)){
				List<User> users = this.userSV.queryUserNOAdmin();
				request.setAttribute("users", JacksonUtil.serialize(users));
				
			}
			request.setAttribute("flag", flag);
			request.setAttribute("id", id);
		} catch (Exception e) {
			log.error("跳转修改用户信息页面出错",e);
			e.printStackTrace();
		}
		
		return "views/base/user/trainInfoEdit";
	}
	
	/**
	 * @Description 修改用户违规信息
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("updataTrainUser.do")
	@ResponseBody
	public String updataIllUser(HttpServletRequest request, HttpServletResponse response,
			String jsondata) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> jsonResult = new HashMap<String, Object>();
		
		try {
			JSONObject json = JSONObject.fromObject(jsondata);
			TrainInfo trainInfo = (TrainInfo) JSONObject.toBean(json, TrainInfo.class);
			this.trainInfoSV.updateInfo(trainInfo);
			jsonResult.put("result", true);
		} catch (Exception e) {
			jsonResult.put("result", false);
			jsonResult.put("msg", e.getMessage());
			log.error("更新用户信息出错", e);
		}
		
		return jRequest.serialize(jsonResult, true);
	}
	
	/**
	 * @Description 下载附件
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("downloadFile.do")
	@ResponseBody
	public ResponseEntity<byte[]> downloadFile(HttpServletRequest request, HttpServletResponse response,String id) {
		
		ResponseEntity<byte[]> result = null;
		try {

			TrainInfo trainInfo = this.trainInfoSV.downloadAttachment(id);
			String filePath = trainInfo.getAttachmentPath();			
			int indexOf1 = filePath.indexOf("localFile\\");
			String fileName = filePath.substring(indexOf1+10);
			 File file = new File(filePath);
			 if(!file.exists()){	
				 System.out.println("文件不存在!");
			 }
			HttpHeaders headers = new HttpHeaders();
			headers.setContentDispositionFormData("attachment", new String(fileName.getBytes("utf-8"),"ISO8859-1"));
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			result = new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file), headers, HttpStatus.CREATED);
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error("贷前调查最终报告下载异常", e);
		}
		return result;
	}
	
}
