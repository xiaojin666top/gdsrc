package com.beawan.base.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.base.entity.SysDic;
import com.beawan.base.service.ISysDicSV;
import com.beawan.common.SysConstants;
import com.beawan.core.BaseController;
import com.beawan.library.bean.TreeData;
import com.beawan.library.service.IIndustrySV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.HttpUtil;
import com.platform.util.StringUtil;

@Controller
@RequestMapping({ "/base/common" })
public class CommonCtl extends BaseController{
	
	private static final Logger log = Logger.getLogger(CommonCtl.class);
	
	@Resource
	private IIndustrySV industrySV;
	@Resource
	private ISysDicSV sysDicSV;
	
	/**
	 * @Description (查找树型常量)
	 * @param request
	 * @param response
	 * @param abvenName
	 * @return
	 */
	@RequestMapping("queryTreeData.json")
	@ResponseBody
	public Map<String,Object> queryTreeData(HttpServletRequest request, HttpServletResponse response, String abvenName) {
		Map<String,Object> model = new HashMap<String,Object>();
		try {
			String opttype = HttpUtil.getAsString(request, "opttype");
			List<TreeData> treeDataList = this.industrySV.getTreeDataByAbvenName(abvenName, opttype);
			TreeData treeData = this.industrySV.getTreeDataByEnname(abvenName, opttype);
			boolean flag = industrySV.haveChildIndustry(treeData);
			if (treeDataList == null) {
				treeDataList = new ArrayList<TreeData>();
			}
			model.put("flag", flag);
			model.put("treeData", treeDataList);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("查找树型常量异常！",e);
		}
		return model;
	}

	/**
	 * @Description (查找普通常量)
	 * @param request
	 * @param response
	 * @param abvenName
	 * @return
	 */
	@RequestMapping("queryDicData.json")
	@ResponseBody
	public Map<String,Object> queryDicData(HttpServletRequest request, HttpServletResponse response) {
		Map<String,Object> model = new HashMap<String,Object>();
		try {
			String opttype = HttpUtil.getAsString(request, "opttype");
			List<SysDic> list = sysDicSV.findByType(opttype);
			model.put("list", list);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("查找普通常量异常！",e);
		}
		return model;
	}
	
	/**
	 * @Description (跳转行业选择页面)
	 * @param request
	 * @param response
	 * @param level 行业层级，例如3则必须选到3级，空则可选到任意一级
	 * @return
	 */
	@RequestMapping("selectIndustry.do")
	public String selectIndustry(HttpServletRequest request, HttpServletResponse response, 
			String industry, int level) {
		
		try {
			
			TreeData treeData = industrySV.getTreeDataByEnname(industry,
					SysConstants.TreeDicConstant.GB_IC_4754_2017);
			
			if(treeData != null)
				request.setAttribute("indusLocate", treeData.getLocate());
			
		} catch (Exception e) {
			log.error("跳转行业选择页面异常", e);
		}
		
		request.setAttribute("level", level);
		
		return "views/common/industry_select";
	}
	
	/**
	 * @Description 根据行业代码，查询查询子行业列表
	 * @param request
	 * @param response
	 * @param id 行业代码
	 * @return
	 */
	@RequestMapping("queryIndusChilds.json")
	@ResponseBody
	public String queryIndusChilds(HttpServletRequest request,
			HttpServletResponse response, String id) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);
		try {
			
			if(StringUtil.isEmptyString(id))
				id = "all";
			
			List<TreeData> treeDataList = industrySV.getTreeDataByAbvenName(id,
					SysConstants.TreeDicConstant.GB_IC_4754_2017);
			json.put("list", treeDataList);
			json.put("result", true);
		} catch (Exception e) {
			json.put("msg", "查询出错！");
			log.error("根据行业代码，查询查询子行业列表异常", e);
		}
		
		return jRequest.serialize(json, true);
	}
	
	
	/**
	 * @Description 根据行业代码查询行业信息
	 * @param request
	 * @param response
	 * @param industryCode 行业代码
	 * @return
	 */
	@RequestMapping("queryIndustryObj.json")
	@ResponseBody
	public String queryIndustryObj(HttpServletRequest request,
			HttpServletResponse response, String industryCode) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);
		
		try {
			
			TreeData treeData = industrySV.getTreeDataByEnname(industryCode,
					SysConstants.TreeDicConstant.GB_IC_4754_2017);
			json.put("industry", treeData);
			json.put("result", true);
		} catch (Exception e) {
			json.put("msg", "查询出错！");
			log.error("根据行业代码查询行业信息异常", e);
		}
		
		return jRequest.serialize(json, true);
	}
	
}
