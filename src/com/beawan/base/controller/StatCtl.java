package com.beawan.base.controller;


import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.msg.service.IMsgSV;
import com.beawan.task.dto.TaskStatDto;
import com.beawan.task.service.ITaskSV;

/**
 * 查看统计报表
 * @author yzj
 *
 */
@Controller
@RequestMapping({ "/base/stat" })
@UserSessionAnnotation
public class StatCtl extends BaseController{
	
	private static final Logger log = Logger.getLogger(StatCtl.class);
	
	@Resource
	private IMsgSV msgSV;
	@Resource
	private ITaskSV taskSV;
	
	/**
	 * 今日业务统计界面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("nowDayStatics.do")
	public ModelAndView nowDayStatics(HttpServletRequest request, HttpServletResponse response) {
		
		ModelAndView mav= new ModelAndView("views/base/stat/nowday-statics");
//		List<TaskStatDto> taskDtos = taskSV.getTaskStatByBussinessType(year, loanTerm);
//		mav.addObject("taskDtos", taskDtos);
		return mav;
	}
	
	/**
	 * 数据驾驶舱（总）
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("allStatics.do")
	public ModelAndView allStatics(HttpServletRequest request, HttpServletResponse response) {
		
		ModelAndView mav= new ModelAndView("views/base/stat/all-statics");
//		List<TaskStatDto> taskDtos = taskSV.getTaskStatByBussinessType(year, loanTerm);
//		mav.addObject("taskDtos", taskDtos);
		return mav;
	}
	
	/**
	 * 数据驾驶舱（公司）
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("companyStatics.do")
	public ModelAndView companyStatics(HttpServletRequest request, HttpServletResponse response) {
		
		ModelAndView mav= new ModelAndView("views/base/stat/company-statics");
//		List<TaskStatDto> taskDtos = taskSV.getTaskStatByBussinessType(year, loanTerm);
//		mav.addObject("taskDtos", taskDtos);
		return mav;
	}
	
	/**
	 * 数据驾驶舱（审批）
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("approvalStatics.do")
	public ModelAndView approvalStatics(HttpServletRequest request, HttpServletResponse response) {
		
		ModelAndView mav= new ModelAndView("views/base/stat/aproval-statics");
//		List<TaskStatDto> taskDtos = taskSV.getTaskStatByBussinessType(year, loanTerm);
//		mav.addObject("taskDtos", taskDtos);
		return mav;
	}
	
	/**
	 * 跳转 按业务类型统计
	 * @return
	 */
	@RequestMapping("taskCensusByType.do")
	public ModelAndView taskCensusByType() {
		return new ModelAndView("views/base/stat/statModel");
	}
	
	/**
	 * 通过调查模板 获取统计数据
	 * @param applicationTime
	 * @param loanTerm
	 * @return
	 */
	@RequestMapping("getTaskStatByModel.json")
	@ResponseBody
	public ResultDto getTaskStatByModel(String applicationTime,String loanTerm) {
		ResultDto re = returnFail("统计数据获取失败");
		try {
			List<TaskStatDto> entityList = taskSV.getTaskStatByBusinessType(applicationTime, loanTerm);
			re.setRows(entityList);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("统计数据获取成功");
		}catch(Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	

	/**
	 * 跳转按贷款期限统计分析界面
	 * @return
	 */
	@RequestMapping("taskCensusByTerm.do")
	public ModelAndView taskCensusByTerm() {
		ModelAndView mav= new ModelAndView("views/base/stat/statByTerm");
		return mav;
	}
	
	/**
	 * 获取按贷款期限统计数据
	 * @param applicationTime 申请时间
	 * @param businessNo 贷款类型编号
	 * @return
	 */
	@RequestMapping("getTaskStatByTerm.json")
	@ResponseBody
	public ResultDto getTaskStatByTerm(String applicationTime,String businessNo) {
		ResultDto re = returnFail("");
		try {
			List<TaskStatDto> entityList = taskSV.getTaskStatByLoanTerm(applicationTime, businessNo);
			re.setRows(entityList);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("统计数据获取成功");
		}catch(Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	

	/**
	 * 跳转按申请贷款年份统计分析界面
	 * @return
	 */
	@RequestMapping("taskCensusByYear.do")
	public ModelAndView taskCensusByYear() {
		return new ModelAndView("views/base/stat/statByYear");
	}

	/**
	 * 获取按贷款期限统计数据
	 * @return
	 */
	@RequestMapping("getTaskStatByYear.json")
	@ResponseBody
	public ResultDto getTaskStatByYear(String businessNo) {
		ResultDto re = returnFail("统计数据获取失败");
		try {
			List<TaskStatDto> entityList = taskSV.getTaskStatByYear(businessNo);
			re.setRows(entityList);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("统计数据获取成功");
		}catch(Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 获取统计数据
	 * 通过贷款期限统计数据
	 * @return
	 */
	public ResultDto getTaskStatByLoanTime(HttpServletRequest request, HttpServletResponse response,String year) {
		ResultDto re = returnFail("");
		try {
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	
	
	
	
}
