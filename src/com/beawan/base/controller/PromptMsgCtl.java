package com.beawan.base.controller;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.platform.util.StringUtil;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.base.entity.PromptMsg;
import com.beawan.base.service.IPromptMsgSV;
import com.beawan.base.service.ISysDicSV;
import com.beawan.common.BusinessException;
import com.beawan.common.SysConstants;
import com.beawan.common.SysConstants.ExceptionConstant;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.ExceptionUtil;
import com.platform.util.JacksonUtil;

@Controller
@RequestMapping({ "/base/prompt" })
@UserSessionAnnotation
public class PromptMsgCtl{
	
	private static final Logger log = Logger.getLogger(PromptMsgCtl.class);
	
	@Resource
	private IPromptMsgSV promptMsgSV;
	
	@Resource
	private ISysDicSV sysDicSV;

	private Map<String , Object> model = new HashMap<String, Object>();
	
	public Map<String, Object> getModel() {
		return model;
	}

	public void setModel(Map<String, Object> model) {
		this.model = model;
	}
	
	/**
	 * @Description (返回)
	 * @param request
	 * @param response
	 * @return
	 */
	@ResponseBody
	@RequestMapping("getPromptMsgMap.do")
	public String getPromptMsgMap(HttpServletRequest request, HttpServletResponse response,
			String codes) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			List<PromptMsg> msgs = promptMsgSV.findByCodes(codes);
			if(!CollectionUtils.isEmpty(msgs)){
				Map<String, String> temp = new HashMap<String, String>();
				for(PromptMsg msg : msgs){
					temp.put(msg.getCode(), msg.getContentText());
				}
				json.put("msgs", temp);
			}
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (页面跳转，跳转到指导提示设置)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("promptMsg.do")
	public String promptMsg(HttpServletRequest request, HttpServletResponse response) {
		
		try {
			//加载字典数据
			String[] optTypes = {
					SysConstants.BsDicConstant.STD_SY_COM_SURVEY_TYPE, 
					SysConstants.BsDicConstant.STD_PROMPT_MSG_CATEGORY};
		
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/base/prompt/promptMsgList";
	}
	
	/**
	 * @Description (得到所有角色列表)
	 * @param request
	 * @param response
	 * @param search
	 * @param order
	 * @return
	 */
	@RequestMapping("promptMsgTable.do")
	@ResponseBody
	public Map<String , Object> roleTable(HttpServletRequest request, HttpServletResponse response,
			String search, String order,int page, int rows) {
		try {
			PromptMsg queryCondition = JacksonUtil.fromJson(search, PromptMsg.class);
			model = promptMsgSV.findPaging(queryCondition, order, page-1, rows);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return model;
	}
	
	/***进入新增提示信息页面***/
	@RequestMapping("addPromptMsg.do")
	public String addPromptMsg(HttpServletRequest request, HttpServletResponse response){
		
		try {
			//加载字典数据
			String[] optTypes = {
					SysConstants.BsDicConstant.STD_SY_COM_SURVEY_TYPE, 
					SysConstants.BsDicConstant.STD_PROMPT_MSG_CATEGORY};
		
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/base/prompt/newPromptMsg";
	}
	
	/**
	 * @Description (保存提示信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("savePromptMsg.do")
	@ResponseBody
	public String savePromptMsg(HttpServletRequest request, HttpServletResponse response) {
		
		String code = request.getParameter("code");
		String name = request.getParameter("name");
		String businessNo = request.getParameter("businessNo");
		String content = request.getParameter("content");
		String option = request.getParameter("option");
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {

			if(!StringUtil.isEmptyString(content)){
				content = URLDecoder.decode(URLDecoder.decode(content, "UTF-8"), "UTF-8");
			}
			String contentText = StringUtil.removeHtml(content);

			PromptMsg msg = promptMsgSV.findByCode(code);
			if(msg != null && "add".equals(option)){
				ExceptionUtil.throwException(code + "已经存在！");
			}else {
				msg = new PromptMsg();
				msg.setCode(code);
			}
			msg.setName(name);
			msg.setBusinessNo(businessNo);
			msg.setContent(content.getBytes("UTF-8"));
			msg.setContentText(contentText);
			
			promptMsgSV.save(msg);
			
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (BusinessException e) {
			json.put("result", false);
			json.put("msg", e.getMessage());
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	/***进入修改提示信息页面***/
	@RequestMapping("editPromptMsg.do")
	public String editPromptMsg(HttpServletRequest request, HttpServletResponse response,
			String code){
		
		try {
			PromptMsg promptMsg = promptMsgSV.findByCode(code);
			request.setAttribute("name", promptMsg.getName());
			request.setAttribute("code", promptMsg.getCode());
			request.setAttribute("businessNo", promptMsg.getBusinessNo());
			request.setAttribute("content", promptMsg.getContentText());
			
			//加载字典数据
			String[] optTypes = {
					SysConstants.BsDicConstant.STD_SY_COM_SURVEY_TYPE, 
					SysConstants.BsDicConstant.STD_PROMPT_MSG_CATEGORY};
		
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/base/prompt/editPromptMsg";
	}
	
	/**
	 * TODO 删除提示信息记录
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("deletePromptMsg.do")
	@ResponseBody
	public String deletePromptMsg(HttpServletRequest request, HttpServletResponse response,
			String code){
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			PromptMsg old = promptMsgSV.findByCode(code);
			promptMsgSV.delete(old);
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (BusinessException e) {
			json.put("result", false);
			json.put("msg", e.getMessage());
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
}
