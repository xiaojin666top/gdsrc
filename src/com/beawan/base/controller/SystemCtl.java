package com.beawan.base.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.Pagination;
import com.beawan.core.ResultDto;
import com.beawan.corporateloan.dto.LmTaskInfoDto;
import com.beawan.corporateloan.service.LmProcessService;
import com.beawan.base.entity.Menu;
import com.beawan.base.entity.Role;
import com.beawan.base.entity.User;
import com.beawan.base.service.IAuthoritySV;
import com.beawan.base.service.IRoleSV;
import com.beawan.base.service.ISysDicSV;
import com.beawan.base.service.IUserSV;
import com.beawan.base.service.IllRecordSV;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.library.service.IIndustrySV;
import com.beawan.library.service.ITempletSV;
import com.beawan.library.uitls.InitKnowledge;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.beawan.web.impl.UserSession;
import com.platform.util.HttpUtil;
import com.platform.util.StringUtil;
/**
 *
 * @ClassName:  SystemCtl
 * @Description:TODO(系统管理控制器)
 * @author: xyh
 * @date:   2020年5月20日 上午9:13:29
 * 修正：继承BaseController,这个界面主要用于跳转，所以继承不继承无所为
 *      是使用ModelAndView比较好还是直接返回界面路径比较好
 *      加入登录拦截校验
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/system" })
public class SystemCtl extends BaseController{

    private static final Logger log = Logger.getLogger(SystemCtl.class);

    @Resource
    private IUserSV userSV;
    @Resource
    private ITaskSV taskSV;
    @Resource
    private ITempletSV templetService;
    @Resource
    private IIndustrySV industrySV;
    @Resource
    private IAuthoritySV authoritySV;
    @Resource
    private IRoleSV roleSV;
    @Resource
    private ISysDicSV sysDicSV;

    @Resource
    private IllRecordSV illRecordSV;
    @Resource
    private LmProcessService lmProcessService;
    /**
     * 退出登录的json方法，用于ajax请求和移动端的请求
     * @author: xyh
     * @date:   2020年5月20日 上午11:08:56
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("logoff.do")
    @ResponseBody
    public ResultDto logoff(HttpServletRequest request, HttpServletResponse response) {
        ResultDto result = returnFail("登出失败");
        try {
            //移除session
            request.getSession().invalidate();
            result = returnSuccess();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @Description (将用户信息存入session)
     * @param request
     * @param response
     * @param id
     * @return
     */
    @RequestMapping("expert.do")
    public ModelAndView expert(HttpServletRequest request, HttpServletResponse response,
                               String id, String customerNo) {
        ModelAndView mav=  new ModelAndView("views/Application/analysis_index");
        UserSession us = UserSession.getUserSession(request);
        User user = HttpUtil.getCurrentUser(request);
        if(us == null){
            us = UserSession.setUserSession(request);
        }
        Task task = taskSV.getTaskById(Long.valueOf(id));

        request.getSession().setAttribute(Constants.SessionKey.SESSION_KEY_USER, user);
        mav.addObject("name",us.getLoggedUserOrgName());
        mav.addObject("cusFlag",SysConstants.CusFlag.BORROWER);
        mav.addObject("customerNo",task.getCustomerNo());
        mav.addObject("customerName",task.getCustomerName());
        mav.addObject("taskId",id);
        return mav;
    }

    /**
     * @Description (页面跳转，跳转到任务列表和 客户经理管理)
     * @param request
     * @param response
     * @param index
     * @return
     */
    @RequestMapping("taskPage.do")
    public ModelAndView taskPage(HttpServletRequest request, HttpServletResponse response, String url) {
        ModelAndView mav=  new ModelAndView("views/systemManage/userManager");
        if(!url.equals("userManager")){
            mav.setViewName("views/task/" + url);
        }
        return mav;
    }

    /**
     * @Description (页面跳转，跳转到待处理任务列表)
     * @param request
     * @return
     */
    @RequestMapping("pendingTaskList.do")
    public ModelAndView pendingTaskList(HttpServletRequest request, Integer stage) {
        ModelAndView mav=  new ModelAndView("views/survey/task/pendingTaskList");
        try {
            User user = HttpUtil.getCurrentUser(request);
            mav.addObject("user", user);
            mav.addObject("stage", stage);
            //加载字典数据
            String[] optTypes = {
                    SysConstants.BsDicConstant.STD_SY_COM_SURVEY_TYPE};///** @Field STD_SY_COM_SURVEY_TYPE : (企业授信调查报告模板类型) */
            mav.addObject("dicData", sysDicSV.queryMapListByTypes(optTypes, user));
        } catch (Exception e) {
            log.error("跳转到待处理任务列表异常！",e);
            e.printStackTrace();
        }
        return mav;
    }

    /**
     * 转跳到我的工作台
     * @return
     */
    @RequestMapping("workbench.do")
    public ModelAndView workbench() {
        ModelAndView mav=  new ModelAndView("views/tasks/workbench");
        return mav;
    }

    /**
     * 转跳到我的工作台
     * @return
     */
    @RequestMapping("approvlAdmit.do")
    public ModelAndView approvlAdmit() {
        ModelAndView mav = new ModelAndView("views/approval/approvlAdmit");
        return mav;
    }

    /**
     * @Description (页面跳转，跳转到已处理任务列表)
     * @param request
     * @param response
     * @param index
     * @return
     */
    @RequestMapping("usedTaskList.do")
    public ModelAndView usedTaskList(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("views/survey/task/usedTaskList");
        try {
            User user = HttpUtil.getCurrentUser(request);
            request.setAttribute("user", user);
            mav.addObject("user", user);
            //加载字典数据
            String[] optTypes = {
                    SysConstants.BsDicConstant.STD_SY_COM_SURVEY_TYPE};
            mav.addObject("dicData", sysDicSV.queryMapListByTypes(optTypes, user));
        } catch (Exception e) {
            log.error("跳转到已处理任务列表异常！",e);
            e.printStackTrace();
        }
        return mav;
    }

    /**
     * @Description (页面跳转，跳转到申请记录列表)
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("applyRecord.do")
    public ModelAndView applyRecord(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("views/survey/task/applyRecord");
        try {
            User user = HttpUtil.getCurrentUser(request);
            mav.addObject("user", user);
            //加载字典数据
            String[] optTypes = {
                    SysConstants.BsDicConstant.STD_SY_COM_SURVEY_TYPE};
            mav.addObject("dicData", sysDicSV.queryMapListByTypes(optTypes, user));
        } catch (Exception e) {
            log.error("跳转到申请记录列表异常！",e);
            e.printStackTrace();
        }

        return mav;
    }

    /**
     * @Description (页面跳转，跳转行业标准设置)
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("induStand.do")
    public ModelAndView induStand(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView("views/libraryManage/indu_stand");
    }
    /**
     * @Description (页面跳转，跳转到常规调查设置)
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("convSurvey.do")
    public ModelAndView convSurvey(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("views/libraryManage/conv_survey");
        return mav;
    }

    /**
     * @Description (页面跳转，跳转到异常触发设置)
     * @param request
     * @param response
     * @param index
     * @return
     */
    @RequestMapping("excepTrigg.do")
    public ModelAndView excepTrigg(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("views/libraryManage/excep_trigg");
        return mav;
    }

    /**
     * @Description (页面跳转，跳转到行业分析设置)
     * @param request
     * @param response
     * @param index
     * @return
     */
    @RequestMapping("induAnaly.do")
    public ModelAndView induAnaly(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("views/libraryManage/indu_analy");
        return mav;
    }

    /**
     * @Description (页面跳转，跳转到用户管理)
     * @param request
     * @param response
     * @param index
     * @return
     */
    @RequestMapping("userMge.do")
    public ModelAndView userMge(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("views/base/user/userMge");
        try {
            List<Role> roles = roleSV.query();
            mav.addObject("roles", roles);
            //加载字典数据
            String[] optTypes = {
                    SysConstants.BsDicConstant.STD_SY_BUSI_LINE,
                    SysConstants.BsDicConstant.STD_USER_STATUS};
            mav.addObject("dicData", sysDicSV.queryMapListByTypes(optTypes));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }

    /**
     * @Description (页面跳转，跳转到权限管理)
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("authorityMge.do")
    public ModelAndView authorityMge(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("views/base/authority/authority_mge");
        return mav;
    }

    /**
     * @Description (跳转到设置密码页面)
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("resetPassword.do")
    public ModelAndView resetPassword(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("views/systemManage/resetpsd");
        return mav;
    }

    /**
     * @Description (保存修改过后的密码)
     * @param request
     * @param response
     * @param oldpsd
     * @param newpsd
     * @return
     */
    @RequestMapping("savePassword.do")
    public String savePassword(HttpServletRequest request, HttpServletResponse response,
                               String oldpsd, String newpsd) {
        String msg = "密码修改失败!";
        UserSession us = UserSession.getUserSession(request);
        try {
            User user = userSV.queryById(us.getLoggedUserId());
            if (user != null) {
                if (StringUtil.getMD5(oldpsd).equals(user.getPassword())) {
                    user.setPassword(StringUtil.getMD5(newpsd));
                    userSV.saveOrUpdate(user);
                    msg = "密码修改成功!";
                }
            }
            request.setAttribute("message", msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "views/Application/tip";
    }

    /**
     * @Description (初始化用、底层知识库 以及 任务)
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping("install.do")
    public String install(HttpServletRequest request, HttpServletResponse response) throws Exception {

        InitKnowledge initKnowledge = new InitKnowledge();
        initKnowledge.initdatabase(request, response, taskSV, templetService);
        initKnowledge.initErrTempletKnowledge(request, response, templetService);
        initKnowledge.initIndustryAnalysis(request, response, industrySV);

        return "install";
    }

    /**
     * 跳转到index界面，并将user和菜单列表传入界面
     * @author: xyh
     * @date:   2020年5月21日 上午9:26:32
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("home.do")
    public ModelAndView home(HttpServletRequest request, HttpServletResponse response) {
        //默认跳转到客户经理页面
        ModelAndView mav = new ModelAndView("views/manager_index");
        //会带着数据跳过去
        try {
            //获取用户
            User user = (User) request.getSession().getAttribute(Constants.SessionKey.SESSION_KEY_USER);
            String userNo = null;
            String orgNo = null;
            //密码应该不能直接传到前台
            mav.addObject("user", user);

            //根据用户角色编号查询菜单，菜单包括上面的主菜单和下面的子菜单，主菜单在DG_BS_MENU表中用parent_no为0来区分
            List<Menu> menus = authoritySV.queryPrimaryMenuByRoleNo(user.getRoleNo());
            mav.addObject("menuList", menus);

            List<Long> loanRetailCounts = new ArrayList<>();
            String userDA = user.getDataAccess();//dataAccess; //数据访问权限，0全部数据、1所属机构下数据、2所属用户下数据
            if(SysConstants.UserDataAccess.PERSONAL.equals(userDA)) {//所属用户下数据2
                userNo = user.getUserId();
            }else if(SysConstants.UserDataAccess.ORG.equals(userDA)) {//所属机构下数据1
                orgNo = user.getAccOrgNo();//账务机构号（支行）
            }

            //Map<String, String> query = new HashMap<>();
            //贷款任务信息表
            //Pagination<LmTaskInfoDto> retailingPager =
             //       lmProcessService.queryLmProcessPaging(query,orgNo, userNo, "=", 101, 1, 10);
            //Long retailing = retailingPager.getRowsCount();
          //  Pagination<LmTaskInfoDto> retailTaskInPager =
           //         lmProcessService.queryLmProcessPaging(query,orgNo, userNo, "=", 102, 1, 10);
           // Long retailTaskIn = retailTaskInPager.getRowsCount();
            //Pagination<LmTaskInfoDto> retailTaskUpPager = lmProcessService.queryUpRetailPaging(userNo,orgNo,
            //        null, null, 1,10);
            //Long retailTaskUp = retailTaskUpPager.getRowsCount();
           // loanRetailCounts.add(retailing);
            //loanRetailCounts.add(retailTaskIn);
           // loanRetailCounts.add(retailTaskUp);
             loanRetailCounts.add(24L);
            loanRetailCounts.add(66L);
             loanRetailCounts.add(7L);
           mav.addObject("loanRetailCountList", loanRetailCounts);

        } catch (Exception e) {
            //这里配置上ModelAndView，跳转到404/其他错误界面
            e.printStackTrace();
        }
        return mav;
    }

    @RequestMapping("comingSoon.do")
    public String comingSoon(HttpServletRequest request, HttpServletResponse response) {
        return "views/common/coming_soon";
    }



    /*
     * 跳转违规记录页面
     */
    @RequestMapping("illegalRecord.do")
    public ModelAndView illegalRecord(HttpServletRequest request, HttpServletResponse response){
        ModelAndView mav = new ModelAndView("views/base/user/illegalRecord");
        return mav;
    }

    /*
     * 跳转培训信息页面
     */
    @RequestMapping("trainingInfo.do")
    public String trainingInfo(HttpServletRequest request, HttpServletResponse response){


        try {
            List<Role> roles = roleSV.query();
            //加载字典数据
            String[] optTypes = {
                    SysConstants.BsDicConstant.STD_SY_BUSI_LINE,
                    SysConstants.BsDicConstant.STD_USER_STATUS};
            request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));

            request.setAttribute("roles", roles);
        } catch (Exception e) {
            e.printStackTrace();
        }


        return "views/base/user/trainingInfo";
    }

    /*
     * 跳转贷款计算器页面
     */
    @RequestMapping("loanCalculator.do")
    public ModelAndView loanCalculator(HttpServletRequest request, HttpServletResponse response){
        ModelAndView mav = new ModelAndView("views/base/tools/calculator");
        return mav;
    }

    /*
     * 跳转流动资金计算器页面
     */
    @RequestMapping("flowloanCalculator.do")
    public ModelAndView flowloanCalculator(HttpServletRequest request, HttpServletResponse response){
        ModelAndView mav = new ModelAndView("views/base/tools/flow_calculator");
        return mav;
    }

}
