package com.beawan.base.controller;

import java.io.File;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.platform.util.*;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.base.entity.Organization;
import com.beawan.base.entity.User;
import com.beawan.base.dto.OrganizationDto;
import com.beawan.base.service.IOrganizationSV;
import com.beawan.base.service.IRoleSV;
import com.beawan.base.service.IUserSV;
import com.beawan.common.Constants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.Pagination;
import com.beawan.core.ResultDto;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;

import net.sf.json.JSONObject;

@Controller
@RequestMapping({ "/base/org" })
@UserSessionAnnotation
public class OrganizationCtl extends BaseController{
	
	private static final Logger log = Logger.getLogger(OrganizationCtl.class);
	
	@Resource
	private IOrganizationSV organizationSV;

	@Resource
	private IRoleSV roleSV;
	@Resource
	private IUserSV userSV;
	
	/**
	 * @Description (跳转机构选择页面)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("selectOrg.do")
	public String selectOrg(HttpServletRequest request, HttpServletResponse response) {
		return "views/common/org_select";
	}
	
	/**
	 * @Description 得到所有机构列表（原-》信贷系统数据  现在支持手动新增及导入）
	 * @param request
	 * @param response
	 * @param order 排序条件
	 * @return
	 */
	@RequestMapping("getOrgsFromCmis.do")
	@ResponseBody
	public ResultDto getOrgsFromCmis(HttpServletRequest request, HttpServletResponse response,
			String orgNo, String orgName, String order, int page, int rows) {
		ResultDto re = returnFail("查询机构数据异常");
		Map<String, Object> params = new HashMap<String, Object>();
		try {
//			QOrganization queryCondition = null;
//				queryCondition = JacksonUtil.fromJson(search, QOrganization.class);
//			long totalCount = organizationSV.totalCount(queryCondition);
//			List<?> list = organizationSV.paginateQuery(queryCondition,
//					order, page-1, rows);
			Pagination<OrganizationDto> pagin = organizationSV.paginateOrg(OrganizationDto.class, orgNo, orgName, order, page, rows);
			
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setRows(pagin.getItems());
			re.setMsg("查询机构列表成功");
			re.setTotal(pagin.getRowsCount());
		} catch (Exception e) {
			e.printStackTrace();
			log.error("查询机构列表出错", e);
		}
		return re;
	}
	
	/**
	 * @Description 机构管理
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("orgListMge.do")
	public String orgListMge(HttpServletRequest request, HttpServletResponse response) {
		//return "views/base/org/orgList";
		return "views/base/org/orgTreeList";
	}
	
	@RequestMapping("getChildren.json")
	@ResponseBody
	public String getOrgTree(HttpServletRequest request, HttpServletResponse response,String id) {
		List<OrganizationDto> dataGrid = new ArrayList<OrganizationDto>();
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		try {
			/**
			 * easyui的tree是一层一层加载的，而不是一次加载完就在页面上了
			 */
			String orgno = id;
			//第一次进入，加载登录用户的本机构
			if(StringUtil.isEmptyString(id)){
				User currUser = HttpUtil.getCurrentUser(request);
				orgno = currUser.getAccOrgNo();
				//加入自己这个机构，暂时想不到好的办法处理得简单一点
				Organization selfOrg = organizationSV.queryByOrgNo(orgno);

				OrganizationDto dto = MapperUtil.trans(selfOrg, OrganizationDto.class);
				dto.setChildList(organizationSV.queryChildTree(orgno));
				/**
				 * 所以这个递归调用其实只需调用到第二层----不过为了后续的使用，就直接把整棵树都建立好了
				 */
				dataGrid.add(dto);
			}else{
				//查询机构下的子机构树
				dataGrid = organizationSV.queryChildTree(orgno);
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("查询机构列表出错", e);
		}
		return jRequest.serialize(dataGrid, true);
	}
	
	
	/**
	 * @Description 新增机构
	 * @param request
	 * @param response
	 * @param userId
	 * @return
	 */
	@RequestMapping("orgAdd.do")
	public String orgAdd(HttpServletRequest request, HttpServletResponse response,
			String userId) {
		
		try {
			
			User currUser = HttpUtil.getCurrentUser(request);
			String orgLvl = currUser.getOrgLvl();//机构等级   每个机构最高只能设置和自己机构平级的
			//加载机构列表
			List<Organization> orgList = organizationSV.queryByCondition("1=1 AND orgLvl >='"+orgLvl+"' AND orgLvl <='3' order by orgLvl");
			
			request.setAttribute("orgList", orgList);
			
		} catch (Exception e) {
			log.error("跳转新增用户页面出错", e);
			e.printStackTrace();
		}
		
		return "views/base/org/orgAdd";
	}
	
	@RequestMapping("orgCheck.do")
	@ResponseBody
	public String orgCheck(HttpServletRequest request, HttpServletResponse response,
			String organno) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			
			if(StringUtil.isEmptyString(organno))
				ExceptionUtil.throwException("机构编号不能为空！");
			
			Organization org = organizationSV.queryByNo(organno);
			if(org != null)
				ExceptionUtil.throwException("机构已存在！");
			
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", e.getMessage());
			log.error("检查机构是否已经存出错：" + e.getMessage());
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * 通过excel导入机构信息
	 * @param request
	 * @param response
	 * @param file
	 * @return
	 */
	@RequestMapping("orgImport.json")
	@ResponseBody
	public ResultDto orgImport(HttpServletRequest request, HttpServletResponse response,
			MultipartFile file) {
		
		ResultDto re = returnFail("");
		try {
			if (file == null) {
				re.setMsg("文件不存在");
				return re;
			}
			String filePath = PropertiesUtil.getProperty(Constants.SYSTEM_PROPS_PATH, "org.excel.upload.path");
			File directory = new File(filePath);
			if (!directory.exists() && !directory.isDirectory()) {
				directory.mkdirs();
			}
			String fileName = file.getOriginalFilename();
			String suffixName = fileName.substring(fileName.lastIndexOf("."));
			fileName = UUID.randomUUID() + suffixName;
			File dest = new File(filePath + fileName);
			File dstFile = new File(filePath);
			if (!dstFile.exists()) {
				dstFile.exists();
			}
			file.transferTo(dest);
			
			/**
			 * 上传结束之后   读取excel文件中的数据  在前台页面上展示
			 */
			organizationSV.saveOrUpdateList(dest);
//			Map<String,Object> map = ExcelUtil.readBigInstitution(new FileInputStream(dest));
//			map.put("fileName", fileName);
			re = returnSuccess();
//			re.setRows(map);
		} catch (Exception e) {
			re.setMsg("导入外部数据异常");
			e.printStackTrace();
		}
		return re;
	}
	
	
	/**
	 * 保存机构
	 * @param request
	 * @param response
	 * @param jsondata
	 * @return
	 */
	@RequestMapping("saveOrg.do")
	@ResponseBody
	public String saveOrg(HttpServletRequest request, HttpServletResponse response,
			String jsondata) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			JSONObject jsonObject = JSONObject.fromObject(jsondata);
			Organization data = (Organization) JSONObject.toBean(jsonObject, Organization.class);
			if(data==null)
				ExceptionUtil.throwException("机构数据为空！");
			String organno = data.getOrganno();
			Organization org = organizationSV.queryByNo(organno);
			if(org != null)
				ExceptionUtil.throwException("机构已存在！");
//			data.setSuporganno("000000000");
//			data.setLocate("123");
			data.setStates(Constants.ORG_STATE.NORMAL);
			organizationSV.saveOrUpdate(data);
			/**
			 * 二级法人  3级法人创建时   默认增加法人系统管理员角色 及账号
			 */
			//1.创建角色
//			Role role = new Role();
//			role.setOrganno(organno);
//			role.setRoleNo("0000");
//			role.setRoleName("系统管理员");
//			role.setRoleDescr("系统管理员");
//			roleSV.saveOrUpdate(role);
//			//2.创建系统管理员角色的用户
//			User user = new User();
//			user.setUserId("0000");
//			user.setPassword("E10ADC3949BA59ABBE56E057F20F883E");
//			userSV.saveOrUpdate(user);
			
			
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", e.getMessage());
			log.error("保存机构信息出错", e);
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * 跳转到合并机构页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("orgMerge.do")
	public ModelAndView orgMerge(HttpServletRequest request, HttpServletResponse response,
			String suporganno,String suporganname,String orgLvl, String orgnos) {
		ModelAndView mav = new ModelAndView("views/base/org/orgMerge");
		try {
			User currUser = HttpUtil.getCurrentUser(request);
			mav.addObject("suporganno", suporganno);
			suporganname = URLDecoder.decode(suporganname,"utf-8");
			mav.addObject("suporganname", suporganname);
			mav.addObject("orgLvl", orgLvl);
			mav.addObject("orgnos", orgnos);
		} catch (Exception e) {
			log.error("跳转新增用户页面出错", e);
			e.printStackTrace();
		}
		
		return mav;
	}
	
	@RequestMapping("orgView.do")
	public ModelAndView orgView(HttpServletRequest request, HttpServletResponse response, String orgno) {
		ModelAndView mav = new ModelAndView("views/base/org/orgView");
		try {

			Organization org = organizationSV.queryByNo(orgno);
			if(org==null) {
				ExceptionUtil.throwException("机构不存在！");
			}
			String supororganno = org.getSuporganno();
			Organization supOrg = organizationSV.queryByNo(supororganno);
			if(supOrg==null)
				ExceptionUtil.throwException("上级机构不存在！");
			OrganizationDto dto = new OrganizationDto(org);
			dto.setSuporganname(supOrg.getOrganname());
			
			mav.addObject("org", dto);
			
		} catch (Exception e) {
			log.error("跳转新增用户页面出错", e);
			e.printStackTrace();
		}
		
		return mav;
	}
	
	
	/**
	 * 撤销机构
	 * @param request
	 * @param response
	 * @param orgno
	 * @return
	 */
	@RequestMapping("deleteOrg.json")
	@ResponseBody
	public ResultDto deleteOrg(HttpServletRequest request, HttpServletResponse response,String orgno) {
		ResultDto re = returnFail("");
	
		try {
//			JSONObject jsonObject = JSONObject.fromObject(jsondata);
//			Organization orgInfo = (Organization) JSONObject.toBean(jsonObject, Organization.class);	
			if(orgno==null || "".equals(orgno)) {
				re.setMsg("未选中机构");
				return re;
			}
			Organization org = organizationSV.queryByNo(orgno);
			if(org == null)
				ExceptionUtil.throwException("机构不存在！");
//			data.setSuporganno("000000000");
//			data.setLocate("123");
			org.setStates(Constants.ORG_STATE.REVOKE);
			organizationSV.saveOrUpdate(org);
			
			re = returnSuccess();
			re.setMsg("机构撤销成功");
		}catch(Exception e) {
			e.printStackTrace();
			log.error("合并机构异常");
		}
		
		return re;
		
	}
	
	
	
	/**
	 * @param request
	 * @param response
	 * @param orgnos    合并的组织机构号   用,分割
	 * @param orgInfo   合并后新的组织机构信息
	 * @return
	 */
	@RequestMapping("mergeOrgInfo.json")
	@ResponseBody
	public ResultDto mergeOrgInfo(HttpServletRequest request, HttpServletResponse response,String orgnos,
			Organization orgInfo) {
		ResultDto re = returnFail("");
	
		try {
//			JSONObject jsonObject = JSONObject.fromObject(jsondata);
//			Organization orgInfo = (Organization) JSONObject.toBean(jsonObject, Organization.class);	
			if(orgInfo==null) {
				re.setMsg("合并后的机构信息为空");
				return re;
			}
			String organno = orgInfo.getOrganno();
			if(organno==null || "".equals(organno)) {
				re.setMsg("合并后的机构编号为空");
				return re;
			}
			Organization org = organizationSV.queryByNo(organno);
			if(org != null)
				ExceptionUtil.throwException("机构已存在！");
//			data.setSuporganno("000000000");
//			data.setLocate("123");
			orgInfo.setStates(Constants.ORG_STATE.NORMAL);
			organizationSV.saveAndmergeOrgs(orgnos, orgInfo);
			
			re = returnSuccess();
		}catch(Exception e) {
			e.printStackTrace();
			log.error("合并机构异常");
		}
		
		return re;
		
	}
	
}
