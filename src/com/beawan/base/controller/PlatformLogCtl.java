package com.beawan.base.controller;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.base.dto.LogFileDto;
import com.beawan.base.entity.PlatformLog;
import com.beawan.base.qbean.QPlatformLog;
import com.beawan.base.service.IPlatformLogSV;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.Pagination;
import com.beawan.core.ResultDto;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;


@Controller
@RequestMapping({ "/base/log" })
@UserSessionAnnotation
public class PlatformLogCtl extends BaseController{
	
	private static final Logger log = Logger.getLogger(PlatformLogCtl.class);
	
	@Resource
	private IPlatformLogSV platformLogSV;
	
	/***跳转到日志管理页面***/
	@RequestMapping("logMge.do")
	public String logMge(HttpServletRequest request, HttpServletResponse response){
		return "views/base/logs/mge_log";
	}
	
	
	/**
	 * @Description 查询平台操作日志
	 * @param search
	 * @param order
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping("logTable.json")
	@ResponseBody
	public Map<String , Object> logTable(String search, String order, int page, int rows) {
		
		Map<String,Object> model = new HashMap<String,Object>();
		QPlatformLog queryCondition=null;
		
		try {
			if(search!=null) {
			queryCondition = JacksonUtil.fromJson(search, QPlatformLog.class);
			}
			List<PlatformLog> list = platformLogSV.paginate(queryCondition, order, page-1, rows);
			int total = platformLogSV.totalCount(queryCondition);
			model.put("total", total);
			model.put("rows", list);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("得到所有菜单列表异常", e);
		}
		return model;
	}
	
	/**
	 * 跳转到 系统日志信息管理页面
	 * @return
	 */
	@RequestMapping("infoLogMg.do")
	public ModelAndView MgSysLog(){
		return new ModelAndView("views/base/logs/InfoLogMg");
	}

	/**
	 * 跳转到 系统错误日志信息管理页面
	 * @return
	 */
	@RequestMapping("errorLogMg.do")
	public ModelAndView MgErrorLog(){
		return new ModelAndView("views/base/logs/ErrorLogMg");
	}
	
	/**
	 * 获取 系统日志信息列表
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping("getInfoLogs.json")
	@ResponseBody
	public ResultDto getInfoLogs(int page, int rows){
		ResultDto re = returnFail("获取日志文件列表失败！");
		try {
			Pagination<LogFileDto> pager = platformLogSV.getLogList("INFOLOG", page, rows);
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());
			re.setMsg("获取日志文件列表成功！");
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 获取 系统错误日志信息列表
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping("getErrorLogs.json")
	@ResponseBody
	public ResultDto getErrorLogs(int page, int rows){
		ResultDto re = returnFail("获取日志文件列表失败！");
		try {
			Pagination<LogFileDto> pager = platformLogSV.getLogList("ERRORLOG", page, rows);
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());
			re.setMsg("获取日志文件列表成功！");
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 下载 日志文件
	 * @param filePath
	 * @return
	 */
	@RequestMapping("downloadFile.json")
	@ResponseBody
	public ResponseEntity<byte[]> downloadFile(String filePath) {
		ResponseEntity<byte[]> result = null;
		try {
			if(StringUtil.isEmptyString(filePath)) return result;
			
			int indexOf = filePath.lastIndexOf("/");
			String fileName = filePath.substring(indexOf+1, filePath.length()) + ".txt";
			File file = new File(filePath);
			if(!file.exists()){	
				file.mkdir();
			}
			HttpHeaders headers = new HttpHeaders();
			headers.setContentDispositionFormData("attachment", new String(fileName.getBytes("utf-8"),"ISO8859-1"));
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			result = new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file), headers, HttpStatus.CREATED);
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error("日志报告下载异常", e);
		}
		return result;
	}
	
	/**
	 * 跳转到 日志详情页
	 * @return
	 */
	@RequestMapping("viewLogFile.do")
	public ModelAndView viewLogFile(String filePath){
		ModelAndView mav = new ModelAndView("views/base/logs/viewLog");
		try {
			List<String> contents = platformLogSV.getLogContent(filePath);
			mav.addObject("contents", contents);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
}
