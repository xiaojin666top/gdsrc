package com.beawan.base.controller;

import java.net.URLDecoder;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.base.entity.QuickAnaly;
import com.beawan.base.service.ISysDicSV;
import com.beawan.base.service.QuickAnalyService;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.Pagination;
import com.beawan.core.ResultDto;


/**
 * 在贷前调查中，预设在自动生成在内容
 * @author yuzhejia
 *
 */
@Controller
@RequestMapping({ "/base/quickAnaly/" })
@UserSessionAnnotation
public class QuickAnalyCtl extends BaseController{
	
	private static final Logger log = Logger.getLogger(QuickAnalyCtl.class);
	
	@Resource
	private QuickAnalyService quickAnalyService;
	@Resource
	private ISysDicSV sysDicSV;
	
	
	@RequestMapping("quickAnalyList.do")
	public ModelAndView quickAnalyList(){
		ModelAndView mav = new ModelAndView("views/base/quickAnaly/quickAnalyList");
		try {
			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.RG_PRE_ITEM,SysConstants.BsDicConstant.RG_PRE_ITEM_LEVEL};
			String dicData = sysDicSV.queryMapListByTypes(optTypes);
			mav.addObject("dicData", dicData);
		}catch (Exception e){
			log.error("跳转快速生成分析内容页面异常", e);
			e.printStackTrace();
		}
		return mav;
	}
	
	
	@ResponseBody
	@RequestMapping("getQuickAnalyList.json")
	public ResultDto getQuickAnalyList(@RequestParam(value="page",defaultValue="1")int page,
			   @RequestParam(value="rows",defaultValue="10")int rows){
		ResultDto re = returnFail();
		try{
			Pagination<QuickAnaly> pager = quickAnalyService.queryPageList(page,rows);
			re = returnSuccess();
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());
		}catch(Exception e){
			e.printStackTrace();
		}
		return re;
	}
	

	@RequestMapping("addQuickAnalyList.do")
	public ModelAndView addQuickAnalyList(Integer id){
		ModelAndView mav = new ModelAndView("views/base/quickAnaly/addQuickAnaly");
		try{
			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.RG_PRE_ITEM,SysConstants.BsDicConstant.RG_PRE_ITEM_LEVEL};
			String dicData = sysDicSV.queryMapListByTypes(optTypes);
			mav.addObject("dicData", dicData);
			
			if(id!=null && id!=0){
				QuickAnaly quickAnaly = quickAnalyService.findByPrimaryKey(id);
				if(quickAnaly==null){
					log.error("预设信息id为：" + id + "在数据库中不存在");
					return mav;
				}
				mav.addObject("obj", quickAnaly);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return mav;
	}
	


	@ResponseBody
	@RequestMapping("saveQuickAnaly.json")
	public ResultDto saveQuickAnaly(QuickAnaly quickAnaly){
		ResultDto re = returnFail();
		if(quickAnaly==null){
			re.setMsg("传输内容为空，请重试");
			return re;
		}
		try{
			String msg = quickAnaly.getAnalyMsg();
			if(!StringUtils.isEmpty(msg)){
				String decode = URLDecoder.decode(URLDecoder.decode(msg, "UTF-8"), "UTF-8");
				quickAnaly.setAnalyMsg(decode);
			}
			quickAnalyService.saveOrUpdate(quickAnaly);
			re = returnSuccess();
		}catch(Exception e){
			e.printStackTrace();
		}
		return re;
	}

	@ResponseBody
	@RequestMapping("delQuickAnaly.json")
	public ResultDto delQuickAnaly(Integer id){
		ResultDto re = returnFail();
		if(id==null){
			re.setMsg("传输内容为空，请重试");
			return re;
		}
		try{
			QuickAnaly quickAnaly = quickAnalyService.findByPrimaryKey(id);
			if(quickAnaly==null){
				log.error("预设信息id为：" + id + "在数据库中不存在");
				re.setMsg("数据异常，请重试");
				return re;
			}
			quickAnaly.setStatus(Constants.DELETE);
			quickAnalyService.saveOrUpdate(quickAnaly);
			re = returnSuccess();
		}catch(Exception e){
			e.printStackTrace();
		}
		return re;
	}
	
}
