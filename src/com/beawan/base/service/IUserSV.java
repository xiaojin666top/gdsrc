package com.beawan.base.service;

import java.util.List;
import java.util.Map;

import com.beawan.base.dto.UserDto;
import com.beawan.base.entity.User;
import com.beawan.base.qbean.QUser;
import com.beawan.core.Pagination;

public interface IUserSV {

	/**
	 * TODO 保存或更新（直接保存对象，不做处理）
	 * @param user 用户对象
	 * @throws Exception
	 */
	public User saveOrUpdate(User user) throws Exception;
	
	/**
	 * TODO 保存或更新（如果用户已经存在，则更新，否则新增）
	 * @param jsondata 用户信息json串
	 * @throws Exception
	 */
	public User saveUserInfo(String jsondata) throws Exception;
	
	/**
	 * TODO 根据条件查询分页
	 * @param queryCondition 查询条件
	 * @param orderCondition 排序条件
	 * @param index 起始页数
	 * @param count 每页大小
	 * @return
	 * @throws Exception
	 */
	List<Map<String, Object>> queryUserPaging(QUser queryCondition,
			String orderCondition, int index, int count) throws Exception;

	Pagination<UserDto> queryUserPaging(String roleNo, String userName, String userNo, int page, int pageSize);

	/*
	 * 
	 * 查询超级管理员之外的用户
	 */
	public List<User> queryUserNOAdmin();
	
	/**
	 * TODO 根据条件查询记录总数
	 * @param queryCondition 查询条件
	 * @return
	 * @throws Exception
	 */
	public long queryUserCount(QUser queryCondition) throws Exception;

	/**** 根据用户编号查询 ****/
	public User queryById(String userId) throws Exception;

	/**** 更新 ****/
	public void update(User user) throws Exception;

	/**** 注销 ***/
	public void cancal(User user) throws Exception;

	/**** 启用 ***/
	public void use(User user) throws Exception;

	/**** 删除 ****/
	public void delete(User user) throws Exception;

	/**** 根据用户角色查询 ****/
	public List<User> queryByRoleNo(String roleNo) throws Exception;

	/**** 根据用户编号字符串查询 ****/
	public List<User> queryByNos(String userIds) throws Exception;

	/**** 根据用户编号字符串查询姓名 ****/
	public String queryNameByNos(String userIds) throws Exception;

	/**
	 * TODO PC端用户登录
	 * 
	 * @param loginName 登录名
	 * @param password
	 *            用户密码
	 * @param ip
	 *            客户端IP地址
	 * @param mac
	 *            客户端MAC地址
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> loginPC(String loginName, String password, String ip,
			String mac) throws Exception;

	/**
	 * TODO 移动端用户登录
	 *
	 * @param loginName 登录名
	 * @param password
	 *            用户密码
	 * @param ip
	 *            客户端IP地址
	 * @param mac
	 *            客户端MAC地址
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> loginMB(String loginName, String password, String ip,
									   String mac) throws Exception;

	/**
	 * TODO 退出登录
	 * @param userId 用户号
	 * @throws Exception
	 */
	public void loginout(String userId, String platform) throws Exception;

	/**
	 * TODO 通过登录日志判断用户是否处于登录状态
	 * 
	 * @param userId
	 *            用户号
	 * @return
	 * @throws Exception
	 */
	public boolean onlineCheck(String userId, String platform) throws Exception;
	/**
	 * TODO 密码重置
	 * @param userId 用户编码
	 * @param newPassword 新密码
	 * @throws Exception
	 */
	public void resetPassword(String userId, String newPassword) throws Exception;
	/**
	 * TODO 密码重置
	 * @param userId 用户编码
	 * @param newPassword 新密码
	 * @throws Exception
	 */
	public void resetPassword(String userId,String oldPassword, String newPassword) throws Exception;

	/**
	 * TODO PC端密码修改（密码参数都是明文）
	 * @param userId
	 * @param oldPassword
	 * @param newPassword
	 * @throws Exception
	 */
	public void modifyPwdPC(String userId, String oldPassword, String newPassword)
			throws Exception;
	
	/**
	 * TODO 移动端密码修改（密码参数都是密文）
	 * @param userId
	 * @param oldPassword
	 * @param newPassword
	 * @return
	 * @throws Exception
	 */
	public int modifyPwdMB(String userId, String oldPassword, String newPassword);
	
	/*
	 * 解除账号锁定
	 */
	public int updateLock(String userId);

	public List<User> getUserByFullName(String userName);
	
}
