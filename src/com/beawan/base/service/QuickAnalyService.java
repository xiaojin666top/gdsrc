package com.beawan.base.service;

import java.util.List;
import java.util.Map;

import com.beawan.base.entity.QuickAnaly;
import com.beawan.core.BaseService;
import com.beawan.core.Pagination;

/**
 * @author yzj
 */
public interface QuickAnalyService extends BaseService<QuickAnaly> {

	Pagination<QuickAnaly> queryPageList(int page, int rows);
	
	Map<String, List<QuickAnaly>> queryAnalyByItem(String[] items);
}
