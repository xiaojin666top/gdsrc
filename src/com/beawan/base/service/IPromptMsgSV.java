package com.beawan.base.service;

import java.util.List;
import java.util.Map;

import com.beawan.base.entity.PromptMsg;

public interface IPromptMsgSV {

	/**
	 * TODO 保存提示信息
	 * @param promptMsg 提示信息实体
	 * @return
	 */
	public PromptMsg save(PromptMsg promptMsg) throws Exception;
	
	/**
	 * TODO 删除提示信息
	 * @param promptMsg 提示信息实体
	 * @return
	 */
	public void delete(PromptMsg promptMsg) throws Exception;
	
	/**
	 * @Description 根据代码标识查询
	 * @param code	代码标识
	 * @return
	 */
	public PromptMsg findByCode(String code) throws Exception;
	
	/**
	 * @Description 根据代码标识查询
	 * @param codes	代码标识（多个且用逗号隔开）
	 * @return
	 */
	public List<PromptMsg> findByCodes(String codes) throws Exception;
	
	/**
	 * @Description 根据代码标识查询
	 * @param codes	代码标识列表
	 * @return
	 */
	public Map<String, PromptMsg> findMapByCodes(String[] codes) throws Exception;
	
	/**
	 * @Description 根据代码标识查询
	 * @param codes	代码标识列表
	 * @return
	 */
	public String findMapJsonByCodes(String[] codes) throws Exception;
	
	/**
	 * @TODO 分页查询
	 * @param queryCondition 查询条件
	 * @param orderCondition 排序条件
	 * @param index 起始游标
	 * @param count 每页数量
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> findPaging(PromptMsg queryCondition,
			String orderCondition, int index, int count) throws Exception;
	
}
