package com.beawan.base.service;

import java.util.List;

import com.beawan.base.entity.Menu;

public interface IMenuSV {
	/**保存或更新**/
	public void saveOrUpdate(Menu menu)throws Exception;
	public void delete(Menu menu) throws Exception;
	
	/**
	 * TODO 根据菜单编号递归删除所有关联菜单
	 * @param menuNo
	 * @throws Exception
	 */
	public void deleteByMenuNo(String menuNo) throws Exception;
	
	/**分页查询**/
	public List<Menu> paginate(Menu queryCondition,String sortCondition,int satrt,int limit)throws Exception;
	/**符合条件的总数**/
	public int totalCount(Menu queryCondition)throws Exception;
	/**根据编号查询**/
	public Menu queryByNo(String menuNo)throws Exception;
	/**
	 * 查询子菜单
	 * @param parentNo
	 * @return
	 * @throws Exception
	 */
	public List<Menu> queryChildrenMenu(String parentNo)throws Exception;
	
	/**
	 * 递归查询这个父菜单编号下的树形菜单
	 * @param parentNo
	 * @return
	 * @throws Exception
	 */
	public List<Menu> queryTreeMenu(String parentNo)throws Exception;
	
}
