package com.beawan.base.service;

import java.util.List;
import java.util.Map;

import com.beawan.base.entity.SysDic;
import com.beawan.base.entity.User;

public interface ISysDicSV {

	/**
	 * TODO 直接保存或更新对象
	 * @param dic
	 * @return
	 * @throws Exception
	 */
	public SysDic saveOrUpdate(SysDic dic) throws Exception;
	
	/**
	 * TODO 保存字典信息，如果存在则更新json串中包含的字段
	 * @param jsondata
	 * @return
	 * @throws Exception
	 */
	public SysDic saveDicInfo(String jsondata) throws Exception;
	
	/**
	 * TODO 删除
	 * @param dic
	 * @throws Exception
	 */
	public void delete(SysDic dic) throws Exception;
	
	/**
	 * TODO 根据主键查询
	 * @param sdId
	 * @return
	 */
	public SysDic findById(long sdId) throws Exception;
	
	/**
	 * @Description (查询证件类型字典)
	 * @return
	 */
	public List<SysDic> findCertType() throws Exception ;
	
	/**
	 * TODO 根据字典类型和英文名查询字典项
	 * @param enName
	 * @param optType
	 * @return
	 */
	public SysDic findByEnNameAndType(String enName, String optType) throws Exception ;
	
	/**
	 * TODO 根据字典类型查询字典项列表
	 * @param optType
	 * @return
	 */
	public List<SysDic> findByType(String optType) throws Exception ;
	
	/**
	 * TODO 查询所有字典项列表
	 * @return
	 */
	public List<SysDic> findAll();
	
	/**
	 * TODO 根据字典类型进行匹配查询
	 * @param optTypes 包含多个类型的字符串
	 * @return
	 */
	public List<SysDic> queryByTypes(String optTypes) throws Exception ;

	/**
	 * 将value值进行排序
	 * @param optTypes
	 * @return
	 * @throws Exception
	 */
	List<SysDic> queryByTypesOrderByENName(String optTypes)throws Exception;
	/**
	 * TODO 根据字典类型进行匹配查询
	 * @param optTypes 包含多个类型的字符串
	 * @return 返回字典项集合  <字典项类型，取值列表>
	 */
	public String queryMapListByTypes(String optTypes) throws Exception ;
	
	/**
	 * TODO 根据字典类型进行匹配查询
	 * @param optTypes 包含多个类型的字符串
	 * @return 返回字典项集合  <字典项类型，取值列表>
	 */
	public String queryMapListByTypes(String[] optTypes) throws Exception ;

	/**
	 * TODO 根据字典类型进行匹配查询
	 * @param optTypes 包含多个类型的字符串
	 * @return 返回字典项集合  <字典项类型，取值列表>
	 */
	public String queryMapListByTypes(String[] optTypes, User curUser) throws Exception ;
	
	/**
	 * TODO 根据字典类型进行匹配查询
	 * @param optTypes 包含多个类型的字符串
	 * @return 返回字典项集合  <字典项类型，<代码，中文名>>
	 */
	public Map<String, Object> queryMapByTypes(String[] optTypes) throws Exception ;
	
	/**
	 * TODO 根据条件查询分页
	 * @param queryCondition 查询条件
	 * @param index 起始页数
	 * @param count 每页大小
	 * @return
	 * @throws Exception
	 */
	List<SysDic> queryDicPaging(SysDic queryCondition,
			String order, int index, int count) throws Exception;
	
	/**
	 * TODO 根据条件查询记录总数
	 * @param queryCondition 查询条件
	 * @return
	 * @throws Exception
	 */
	public long queryDicCount(SysDic queryCondition) throws Exception;

}
