package com.beawan.base.service;

import com.beawan.core.BaseService;
import com.beawan.base.entity.InduInfo;

/**
 * @author yzj
 */
public interface InduInfoService extends BaseService<InduInfo> {
}
