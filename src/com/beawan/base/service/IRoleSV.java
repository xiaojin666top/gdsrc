package com.beawan.base.service;

import java.util.List;
import java.util.Map;

import com.beawan.base.entity.Role;

/**
 * 角色逻辑层接口
 * 
 * @author beawan_fengjj
 * 
 */
public interface IRoleSV {
	/*** 保存或更新 ***/
	public void saveOrUpdate(Role role) throws Exception;

	/*** 删除角色 ***/
	public void delete(String roleNo) throws Exception;

	/*** 条件查询 ***/
	public List<Role> queryByCondition(String queryCondition) throws Exception;

	/*** 根据角色代码查询 ***/
	public Role queryByNo(String roleNo) throws Exception;

	/*** 查询所有角色（除了超级管理员） ***/
	public List<Role> query() throws Exception;
	
	/*** 查询所有角色 ***/
	public List<Role> queryAll() throws Exception;

	/***** 分页查询 *****/
	public List<Role> paginateQuery(Role queryCondition,
			String sortCondition, int start, int limit) throws Exception;

	/***** 符合条件总数 *****/
	public int totalCount(Role queryCondition) throws Exception;

	/**
	 * 获取编号和名称映射（KEY为编号，VALUE为名称）
	 * 
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> getRoleNoNameMap() throws Exception;

	/**
	 * 根据roleNo查询角色list
	 * 
	 * @param roleNos 多个角色逗号隔开
	 * @return
	 * @throws Exception
	 */
	List<Role> queryListByNos(String roleNos) throws Exception;
}
