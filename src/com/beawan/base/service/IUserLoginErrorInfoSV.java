package com.beawan.base.service;

import com.beawan.base.entity.UserLoginErrorInfo;
/**
 * 涉及到保存或者更新数据的方法，在SV层中需要命名为可以被事务管理到，否则会有问题
 * @author 86178
 *
 */
public interface IUserLoginErrorInfoSV {
	/**
	 * 增加登录错误次数
	 * @param userLoginErrorInfo
	 * @return
	 * @throws Exception
	 */
	public UserLoginErrorInfo addLoginErrorTime(String userId) throws Exception;
	/**
	 * 重置登录次数
	 * @param userLoginErrorInfo
	 * @return
	 * @throws Exception
	 */
	public UserLoginErrorInfo removeLoginErrorTime(String userId) throws Exception;
	/**
	 * 获取用户指定时间内登录错误次数
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public int getErrorNum(String userId)throws Exception;
}
