package com.beawan.base.service;

import java.util.List;

import com.beawan.library.bean.TreeData;

public interface ISysTreeDicSV {
	
	public TreeData saveOrUpdate(TreeData treeData);

	/**
	 * TODO 根据字典类型和英文名查询字典项
	 * @param enName
	 * @param optType
	 * @return
	 */
	public TreeData findByEnNameAndType(String enName, String optType);
	
	/**
	 * @Description 当前节点的所有子节点
	 * @param abvenName	父节点标识
	 * @return
	 */
	public List<TreeData> findByAbvenName(String abvenName, String optType);
	
	/**
	 * TODO 查询所有字典项列表
	 * @return
	 */
	public List<TreeData> findAll();

	public String getFullNameByLocate(String locate, String optType);
	
	public String getFullNameByEnName(String enName, String optType);
	
}
