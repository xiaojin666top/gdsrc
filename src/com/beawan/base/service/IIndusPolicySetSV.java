package com.beawan.base.service;

import java.util.List;
import java.util.Map;

import com.beawan.base.entity.IndusPolicySet;
import com.beawan.library.bean.TreeData;

public interface IIndusPolicySetSV {
	
	/**** 根据id查询 ****/
	public IndusPolicySet queryById(long id) throws Exception;
	
	/**
	 * TODO 删除
	 * @return
	 * @throws Exception
	 */
	public void delete(IndusPolicySet indusPolicySet) throws Exception;
	
	public void deleteById(long id) throws Exception;

	/**
	 * TODO 保存或更新
	 * @param indusPolicySet
	 * @throws Exception
	 */
	public IndusPolicySet saveOrUpdate(IndusPolicySet indusPolicySet) throws Exception;
	
	
	/**
	 * TODO 根据条件分页查询
	 * @param queryCondition 查询条件
	 * @param orderCondition 排序条件
	 * @param pageIndex 起始页数
	 * @param pageSize 每页大小
	 * @return
	 * @throws Exception
	 */
	public List<Map<String,Object>> queryPaging(IndusPolicySet queryCondition,
			String orderCondition, int pageIndex, int pageSize) throws Exception;
	
	
	/**
	 * TODO 根据条件查询记录总数
	 * @param queryCondition 查询条件
	 * @return
	 * @throws Exception
	 */
	public long queryCount(IndusPolicySet queryCondition) throws Exception;
	
	/**
	 * TODO 查询有效的行业政策
	 * @param industryCode 国标行业代码
	 * @param policyType 政策类型
	 * @return 行业政策对象
	 * @throws Exception
	 */
	public IndusPolicySet queryIndusPolicy(String industryCode, String policyType) throws Exception;
	
	/**
	 * TODO 查询有效的行业政策
	 * @param indusObj 行业对象
	 * @param policyType 政策类型
	 * @return 行业政策对象
	 * @throws Exception
	 */
	public IndusPolicySet queryIndusPolicy(TreeData indusObj, String policyType) throws Exception;
	
}
