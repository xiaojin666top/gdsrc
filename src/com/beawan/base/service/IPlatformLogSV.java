package com.beawan.base.service;

import java.util.List;

import com.beawan.base.dto.LogFileDto;
import com.beawan.base.entity.PlatformLog;
import com.beawan.base.qbean.QPlatformLog;
import com.beawan.core.Pagination;

/**
 * 操作日志业务层接口
 * @author beawan_fengjj
 *
 */
public interface IPlatformLogSV {
	
	/**
	* @Title: saveOperateLog 
	* @Description: 保存操作日志
	*  @param msg 操作说明
	*  @param opterateType 操作类型
	*  @throws Exception
	 */
	public void saveOperateLog(String msg,String opterateType,String userId)throws Exception;
	
	/**
	 * 分页查询
	 * @param queryCondition
	 * @param sortCondition
	 * @param start
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	public List<PlatformLog> paginate(QPlatformLog queryCondition,
			String sortCondition, int start, int limit)throws Exception;
	
	/**
	 * 查询总数
	 * @param queryCondition
	 * @return
	 * @throws Exception
	 */
	public int totalCount(QPlatformLog queryCondition)throws Exception;
	
	/**
	 * 分页获取 日志列表
	 * @param logType 日志类型
	 * @param page 当前页
	 * @param pageSize 每页条数
	 * @return
	 * @throws Exception
	 */
	public Pagination<LogFileDto> getLogList(String logType, int page, int pageSize) throws Exception;
	
	/**
	 * 获取 日志详情
	 * @param filePath
	 * @return
	 * @throws Exception
	 */
	public List<String> getLogContent(String filePath) throws Exception;
}
