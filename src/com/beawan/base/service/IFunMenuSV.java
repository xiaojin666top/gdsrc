package com.beawan.base.service;

import java.util.List;

import com.beawan.base.entity.FunMenu;

public interface IFunMenuSV {
	/**保存或更新**/
	public void saveOrUpdate(FunMenu FunMenu)throws Exception;
	public void delete(FunMenu FunMenu) throws Exception;
	
	/**
	 * TODO 根据菜单编号递归删除所有关联菜单
	 * @param FunMenuNo
	 * @throws Exception
	 */
	public void deleteByMenuNo(String FunMenuNo) throws Exception;
	
	/**分页查询**/
	public List<FunMenu> paginate(FunMenu queryCondition,String sortCondition,int satrt,int limit)throws Exception;
	/**符合条件的总数**/
	public int totalCount(FunMenu queryCondition)throws Exception;
	/**根据编号查询**/
	public FunMenu queryByNo(String FunMenuNo)throws Exception;
	/**
	 * 查询子菜单
	 * @param parentNo
	 * @return
	 * @throws Exception
	 */
	public List<FunMenu> queryChildrenMenu(String parentNo)throws Exception;
	
	/**
	 * 递归查询这个父菜单编号下的树形菜单
	 * @param parentNo
	 * @return
	 * @throws Exception
	 */
	public List<FunMenu> queryTreeMenu(String parentNo)throws Exception;
	
}
