package com.beawan.base.service;

import java.util.List;
import java.util.Map;

import com.beawan.base.entity.SReportData;
import com.beawan.base.entity.SReportDataId;


public interface ISReportDataSV {

	/**
	 * TODO 根据复合主键查询
	 * @param reportNo 报告编号
	 * @param rowNo 行编号
	 * @return
	 * @throws Exception
	 */
	public SReportData queryById(String reportNo, String rowNo) throws Exception;
	
	/**
	 * TODO 根据复合主键查询
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public SReportData queryById(SReportDataId id) throws Exception;

	/**
	 * TODO 保存
	 * @param sReportData
	 * @return
	 * @throws Exception
	 */
	public SReportData saveOrUpdate(SReportData sReportData) throws Exception;
	
	/**
	 * TODO 根据符合主键删除
	 * @param reportNo 报告编号
	 * @param rowNo 行编号
	 * @throws Exception
	 */
	public void deleteById(String reportNo, String rowNo) throws Exception;

	/**
	 * TODO 删除
	 * @param sReportData
	 * @throws Exception
	 */
	public void delete(SReportData sReportData) throws Exception;
	
	/**
	 * TODO 根据报告编号查询
	 * @param reportNo 报告编号
	 * @return
	 * @throws Exception
	 */
	public List<SReportData> queryByReportNo(String reportNo) throws Exception;
	
	/**
	 * TODO 根据报告编号查询
	 * @param reportNo 报告编号
	 * @return 数据集合<科目编号，数据对象>
	 * @throws Exception
	 */
	public Map<String, SReportData> queryMapByRepNo(String reportNo) throws Exception;
	
	public Map<String, SReportData> getMap(List<SReportData> dataList) throws Exception;
	
}
