package com.beawan.base.service;

import java.util.Map;

import com.beawan.base.entity.IllegalRecord;
import com.beawan.base.qbean.QUser;

public interface IllRecordSV {
	
	/**
	 * TODO 根据条件查询记录总数
	 * @param queryCondition 查询条件
	 * @return
	 * @throws Exception
	 */
	public long queryUserCount(QUser queryCondition) throws Exception;
	
	//查询所有违规记录
	Map<String, Object> queryUserPaging(QUser queryCondition,
			String orderCondition, int index, int count) throws Exception;
	
	//保存违规记录
	IllegalRecord saveIllegalRecord(String jsondata);
	
	//删除违规记录
	void deleteIllRecord(Long id);
	
	//查看违规信息
	IllegalRecord viewIllRecord(Long id); 
	
	//更新违规信息
	void updateInfo(IllegalRecord illegalRecord);

}
