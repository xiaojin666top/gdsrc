package com.beawan.base.service;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.beawan.base.entity.Organization;
import com.beawan.base.dto.OrganizationDto;
import com.beawan.base.qbean.QOrganization;
import com.beawan.core.Pagination;
/**
 * 机构业务层接口
 * @author fengjunjian
 *
 */
public interface IOrganizationSV {
	/**
	 * 保存更新
	 * @param organization
	 * @throws Exception
	 */
	public void saveOrUpdate(Organization organization)throws Exception;
	
	/**
	 * 读取excel机构表的数据
	 * @param fileInputStream
	 * @throws Exception
	 */
	public void saveOrUpdateList(File file) throws Exception;
	/**
	 * 更新
	 * @param organization
	 * @throws Exception
	 */
	public void update(Organization organization)throws Exception;
	/**
	 * 删除
	 * @param organization
	 * @throws Exception
	 */
	public void delete(Organization organization)throws Exception;
	/**
	 * 根据编号查询
	 * @param no 机构编号
	 * @return
	 * @throws Exception
	 */
	public Organization queryByNo(String no)throws Exception;
	/**
	 * 分页查询
	 * @param querycondition 查询条件
	 * @param sortCondition 排序条件
	 * @param start 开始
	 * @param limit 限制条数
	 * @return
	 * @throws Exception
	 */
	public List<Organization> paginateQuery(QOrganization querycondition,
			String sortCondition, int start, int limit) throws Exception;
	/**
	 * 查询满足条件记录总数
	 * @param querycondition
	 * @return
	 * @throws Exception
	 */
	public int totalCount(QOrganization querycondition)throws Exception;
	/**
	 * 条件查询------这个接口不应该出现在sercive层
	 * @param queryString
	 * @return
	 * @throws Exception
	 */
	public List<Organization> queryByCondition(String queryString)throws Exception;
	/**
	 * 查询出该机构下的子机构
	 * @param suporganno
	 * @return
	 * @throws Exception
	 */
	public List<Organization> queryBySuporganno(String suporganno)throws Exception;
	/**
	 * 查询机构信息（Map形式，key：编号，value：简称）
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> queryNoAndNameMap()throws Exception;

	
	/**
	 * TODO 根据条件查询分页（信贷系统数据）
	 * @param queryCondition 查询条件
	 * @param orderCondition 排序条件
	 * @param index 起始页数
	 * @param count 每页大小
	 * @return
	 * @throws Exception
	 */
	List<Map<String, Object>> queryCmisOrgPaging(QOrganization queryCondition,
			String orderCondition, int pageIndex, int pageSize) throws Exception;
	
	/**
	 * TODO 根据条件查询记录总数（信贷系统数据）
	 * @param queryCondition 查询条件
	 * @return
	 * @throws Exception
	 */
	public long queryCmisOrgCount(QOrganization queryCondition) throws Exception;

	public Pagination<OrganizationDto> paginateOrg(Class<OrganizationDto> clazz, String orgno, String orgName,
			String order, int page, int rows);

	public void saveAndmergeOrgs(String orgnos, Organization orgInfo) throws Exception;
	
	/**
	 * 根据机构编号获取机构
	 * @author: xyh
	 * @date:   2020年5月26日 下午4:06:58   
	 * @param organno
	 * @return
	 * @throws Exception
	 */
	public Organization queryByOrgNo(String organno)throws Exception;
	
	/**
	 * 获取机构树
	 * @author: xyh
	 * @date:   2020年5月26日 下午2:25:11   
	 * @param organno
	 * @return
	 * @throws Exception
	 */
	public List<OrganizationDto> queryChildTree(String organno)throws Exception;
	/**
	 * 将树形的机构转为Set<String>数组
	 * @author: xyh
	 * @date:   2020年5月26日 下午2:34:35   
	 * @param list
	 * @param orgTreeChild
	 * @return
	 * @throws Exception
	 */
	public Set<String> getChildList(Set<String> list,List<OrganizationDto> orgTreeChild)throws Exception;
}
