package com.beawan.base.service;

import java.util.List;

import com.beawan.base.entity.TableSubjCode;

/**
 * 业务层接口
 * @author beawan_fengjj
 *
 */
public interface ITableSubjCodeSV {

	/**
	 * 保存或更新
	 * @param tableSubjCode
	 * @throws Exception
	 */
	public void saveOrUpdate(TableSubjCode tableSubjCode)throws Exception;
	
	/**
	 * 查询
	 * @param businType
	 * @param subjType
	 * @return
	 * @throws Exception
	 */
	public List<TableSubjCode> queryByTableSubjCodes(String businType,String subjType)throws Exception;
	
	/**
	 * 查询
	 * @param businType  贷款类型
	 * @return
	 * @throws Exception
	 */
	public List<TableSubjCode> queryByBusType(String businType)throws Exception;
	
	
}
