package com.beawan.base.service;

import java.util.List;

import com.beawan.base.entity.SReportModel;
import com.beawan.base.entity.SReportModelRow;


public interface ISReportModelSV {

	/**
	 * TODO 查询报表模型信息
	 * @param modelNo 模型编号
	 * @return
	 * @throws Exception
	 */
	public SReportModel queryModelByNo(String modelNo) throws Exception;
	
	/**
	 * TODO 根据模型类别查询报表模型记录
	 * @param modelClass 模型类别
	 * @return
	 * @throws Exception
	 */
	public List<SReportModel> queryModelByClass(String modelClass) throws Exception;
	
	/**
	 * TODO 查询报表模型名称
	 * @param modelNo 模型编号
	 * @return
	 * @throws Exception
	 */
	public String queryModelNameByNo(String modelNo) throws Exception;
	
	/**
	 * TODO 查询报表模型行信息
	 * @param modelNo 模型编号
	 * @return
	 * @throws Exception
	 */
	public List<SReportModelRow> queryModelRowByNo(String modelNo) throws Exception;

}
