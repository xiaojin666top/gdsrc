/**
 * Copyright (c) 2014-07 to forever 杭州碧湾信息技术有限公司 All Rights Reserved.  
 *
 * @project mcesys
 * @file_name IUserLoginLogSV.java
 * @author Rain See
 * @create_time 2016年8月18日 下午3:16:35
 * @last_modified_date 2016年8月18日
 * @version 1.0
 */
package com.beawan.base.service;

import java.util.List;
import java.util.Map;

import com.beawan.base.entity.UserLoginLog;

/**
 * TODO
 * @author Rain See
 * @create_time 2016年8月18日 下午3:16:35
 */
public interface IUserLoginLogSV {

	/**
	 * TODO
	 * @param instance
	 */
	public void saveOrUpdate(UserLoginLog instance) throws Exception;

	/**
	 * TODO 最后一次登录
	 * @param userId 用户号
	 * @param platform 登录平台，移动或PC
	 * @return
	 * @throws Exception
	 */
	public UserLoginLog lastLogin(String userId, String platform) throws Exception;
	
	/**
	 * TODO 分页查询
	 * @param condition
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	public List<UserLoginLog>  paginate(Map<String,String> condition,int pageIndex,int pageSize) throws Exception;

	/**
	 * TODO 获得查询记录总数
	 * @param condition
	 * @return
	 * @throws Exception
	 */
	public int getTotalCount(Map<String, String> condition) throws Exception;
	
	/**
	 * TODO 获取当天登录情况
	 * @param userNo 用户号
	 * @return 包含当天登录次数，当天登录累计时长
	 * @throws Exception
	 */
	public Map<String,Object> getCurrentDayLoginInfo(Map<String, String> condition) throws Exception;
	
	/**
	 * TODO 获取历史登录情况
	 * @param userNo 用户号
	 * @return 包含历史登录次数，历史登录累计时长
	 * @throws Exception
	 */
	public Map<String,Object> getHistoryLoginInfo(Map<String, String> condition) throws Exception;
}
