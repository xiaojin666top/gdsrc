package com.beawan.base.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.base.entity.UserLoginErrorInfo;
import com.beawan.base.dao.UserLoginErrorInfoDao;
import com.beawan.base.service.IUserLoginErrorInfoSV;
import com.platform.util.HttpUtil;
import com.platform.util.PropertiesUtil;

@Service("userLoginErrorInfoSV")
public class UserLoginErrorInfoSVImpl implements IUserLoginErrorInfoSV {

	@Resource
	private UserLoginErrorInfoDao loginErrorInfoDao;

	@Override
	public UserLoginErrorInfo addLoginErrorTime(String userId) throws Exception {
		// 获取登录锁定间隔时间
		String lock = PropertiesUtil.getProperty("system.properties", "user.login.lockSecond");
		Integer lockTime = Integer.valueOf(lock);
		// 查出锁定时间内的登录错误记录间隔期内
		SimpleDateFormat formData = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date data = new Date();
		// 获取到的配置是秒，所以需要乘以1000
		String oneDayAgo = formData.format(new Date(data.getTime() - lockTime * 1000));
		UserLoginErrorInfo errorInfo = this.loginErrorInfoDao.getLoginError(userId, oneDayAgo);
		String format = formData.format(new Date());
		// 24小时内不存在登录错误记录
		if (errorInfo == null) {
			errorInfo = new UserLoginErrorInfo();
			errorInfo.setUserId(userId);
			errorInfo.setLoginErrorTime(format);
			errorInfo.setLoginErrorNum(0);// 新建设置为0
			errorInfo.setIpAddress(HttpUtil.getRemoteAddr());
		}
		errorInfo.setLoginErrorNum(errorInfo.getLoginErrorNum() + 1);// 只要是走到这里，错误次数都要+1
		return this.loginErrorInfoDao.saveOrUpdate(errorInfo);
	}

	@Override
	public UserLoginErrorInfo removeLoginErrorTime(String userId) throws Exception {
		// 获取登录锁定间隔时间
		String lock = PropertiesUtil.getProperty("system.properties", "user.login.lockSecond");
		Integer lockTime = Integer.valueOf(lock);
		// 查出锁定时间内的登录错误记录间隔期内
		SimpleDateFormat formData = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date data = new Date();
		// 获取到的配置是秒，所以需要乘以1000
		String oneDayAgo = formData.format(new Date(data.getTime() - lockTime * 1000));
		UserLoginErrorInfo errorInfo = this.loginErrorInfoDao.getLoginError(userId, oneDayAgo);
		String format = formData.format(new Date());
		// 指定时间内存在登录错误记录
		if (errorInfo != null) {
			errorInfo.setLoginErrorNum(0);
			errorInfo.setLoginErrorTime(format);
			return this.loginErrorInfoDao.saveOrUpdate(errorInfo);
		}
		return null;
	}

	@Override
	public int getErrorNum(String userId) throws Exception {
		// 获取登录锁定间隔时间
		String lock = PropertiesUtil.getProperty("system.properties", "user.login.lockSecond");
		Integer lockTime = Integer.valueOf(lock);
		// 查出锁定时间内的登录错误记录间隔期内
		SimpleDateFormat formData = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date data = new Date();
		// 获取到的配置是秒，所以需要乘以1000
		String oneDayAgo = formData.format(new Date(data.getTime() - lockTime * 1000));
		UserLoginErrorInfo errorInfo = this.loginErrorInfoDao.getLoginError(userId, oneDayAgo);
		if(errorInfo != null){
			return errorInfo.getLoginErrorNum();
		}else{
			return 0;
		}
		
	}

}
