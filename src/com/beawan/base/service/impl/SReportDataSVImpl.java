package com.beawan.base.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.beawan.base.entity.SReportData;
import com.beawan.base.entity.SReportDataId;
import com.beawan.base.dao.ISReportDataDAO;
import com.beawan.base.service.ISReportDataSV;

@Service("sReportDataSV")
public class SReportDataSVImpl implements ISReportDataSV {

	@Resource
	private ISReportDataDAO sReportDataDAO;

	@Override
	public SReportData queryById(String reportNo, String rowNo) throws Exception {
		String hql = "reportNo=:reportNo and rowNo=:rowNo";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("reportNo", reportNo);
		params.put("rowNo", rowNo);
		return sReportDataDAO.selectSingle(hql, params);
	}
	
	@Override
	public SReportData queryById(SReportDataId id) throws Exception {
		return sReportDataDAO.selectByPrimaryKey(id);
	}
	
	@Override
	public SReportData saveOrUpdate(SReportData sReportData) throws Exception {
		return sReportDataDAO.saveOrUpdate(sReportData);
	}
	
	@Override
	public void deleteById(String reportNo, String rowNo) throws Exception {
		sReportDataDAO.deleteEntity(this.queryById(reportNo, rowNo));
	}
	
	@Override
	public void delete(SReportData sReportData) throws Exception {
		sReportDataDAO.deleteEntity(sReportData);
	}
	
	@Override
	public List<SReportData> queryByReportNo(String reportNo) throws Exception {
		String hql = "reportNo=:reportNo order by displayOrder asc";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("reportNo", reportNo);
		return sReportDataDAO.select(hql, params);
	}
	
	@Override
	public Map<String, SReportData> queryMapByRepNo(String reportNo) throws Exception {
		
		String hql = "reportNo=:reportNo order by displayOrder asc";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("reportNo", reportNo);
		List<SReportData> dataList = sReportDataDAO.select(hql, params);
		
		return this.getMap(dataList);
	}
	
	@Override
	public Map<String, SReportData> getMap(List<SReportData> dataList) throws Exception {

		Map<String, SReportData> dataMap = null;
		
		if(!CollectionUtils.isEmpty(dataList)){
			dataMap = new HashMap<String, SReportData>();
			for(SReportData data : dataList)
				dataMap.put(data.getRowSubject(), data);
		}
		
		return dataMap;
	}
}
