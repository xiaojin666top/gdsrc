package com.beawan.base.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.base.entity.Menu;
import com.beawan.base.entity.RoleMenuRel;
import com.beawan.base.dao.IMenuDAO;
import com.beawan.base.dao.IRoleMenuRelDAO;
import com.beawan.base.service.IAuthoritySV;

@Service("authoritySV")
public class AuthoritySVImpl implements IAuthoritySV {
	@Resource
	private IMenuDAO menuDAO;
	@Resource
	private IRoleMenuRelDAO roleMenuMapDAO;
	

	@Override
	public List<Menu> allMenuList() throws Exception {
		//查询所有一级菜单
		List<Menu> menuList = menuDAO.queryCondition("from Menu as model "
				+ "where model.parentNo='0' order by model.sort asc",null);
		for (Menu menu : menuList) {
			List<Menu> childMenuList = menuDAO.queryByParentNo(menu.getMenuNo());
			if(childMenuList!=null && childMenuList.size()>0){
				for (Menu childMenu : childMenuList) {
					List<Menu> subMenuList = menuDAO.queryByParentNo(childMenu.getMenuNo());
					if(subMenuList!=null && subMenuList.size()>0){
						childMenu.setChildMenuList(subMenuList);
					}
				}
				menu.setChildMenuList(childMenuList);
			}
		}
		return menuList;
	}

	@Override
	public void saveOrUpdateMap(String roleNo, String menuNoStr) throws Exception {
		List<RoleMenuRel> mappings = roleMenuMapDAO.queryByRoleNo(roleNo);
		if(mappings.size()>0){
			for(RoleMenuRel mapping : mappings){
				roleMenuMapDAO.delete(mapping);
			}
		}
		
		String[] menuNoArray = menuNoStr.split(",");
		for(int i=0;i<menuNoArray.length;i++){
			RoleMenuRel roleMenuMap = new RoleMenuRel();
			roleMenuMap.setRoleNo(roleNo);
			roleMenuMap.setMenuNo(menuNoArray[i]);
			roleMenuMapDAO.save(roleMenuMap);
		}
	}

	@Override
	public List<RoleMenuRel> queryMapByRoleNo(String roleNo)
			throws Exception {
		List<RoleMenuRel> mappings = roleMenuMapDAO.queryByRoleNo(roleNo);
		return mappings;
	}

	@Override
	public List<Menu> queryPrimaryMenuByRoleNo(String roleNo) throws Exception {
		//根据角色查询角色权限映射关系
		List<RoleMenuRel> mappings = roleMenuMapDAO.queryByRoleNo(roleNo);
		//定义该角色（组）的所有映射的菜单编号集合（包括一级、二级、三级...）
		List<String> menuNoList= new ArrayList<String>();
		for(RoleMenuRel mapping : mappings){
			if(!menuNoList.contains(mapping.getMenuNo())){
				menuNoList.add(mapping.getMenuNo());
			}
		}
		//查询所有一级菜单
		List<Menu> menuList = menuDAO.queryMainMenu();
		this.removeNoPowerMenu(menuNoList, menuList);
		return menuList;
	}
	
	/**
	 * 采用递归算法筛选角色权限
	 * @param menuNoList
	 * @param menuList
	 * @throws Exception
	 */
	private void removeNoPowerMenu(List<String> menuNoList,List<Menu> menuList)throws Exception{
		//定义权限外的菜单集合
		List<Menu> removeMenuList = new ArrayList<Menu>();
		//遍历菜单集合
		for (Menu menu : menuList) {
			//判断菜单是存否在该角色所有映射的菜单集合中
			if(menuNoList.contains(menu.getMenuNo())){
				//查询子菜单
				List<Menu> childMenuList = menuDAO.queryByParentNo(menu.getMenuNo());
				if(childMenuList!=null && childMenuList.size()>0){
					removeNoPowerMenu(menuNoList,childMenuList);
					//将子菜单赋给父菜单
					menu.setChildMenuList(childMenuList);
				}
			}else{
				//将权限外的菜单添加到权限外的菜单集合
				removeMenuList.add(menu);
			}
		}
		//移除权限外的菜单集合
		menuList.removeAll(removeMenuList);
	}
	

	@Override
	public Menu querySecondaryMenuByRoleNoAndMenuNo(String roleNo, 
			String menuNo) throws Exception {
		List<RoleMenuRel> mappings = roleMenuMapDAO.queryByRoleNo(roleNo);
		List<String> menuNoList= new ArrayList<String>();
		for(RoleMenuRel mapping : mappings){
			menuNoList.add(mapping.getMenuNo());
		}
		Menu menu = menuDAO.queryByNo(menuNo);
		List<Menu> childMenuList = menuDAO.queryByParentNo(menu.getMenuNo());
		if(childMenuList!=null && childMenuList.size()>0){
			List<Menu> removeChildMenuList = new ArrayList<Menu>();
			for (Menu childMenu : childMenuList) {
				if(menuNoList.contains(childMenu.getMenuNo())){
					List<Menu> subMenuList = menuDAO.queryByParentNo(childMenu.getMenuNo());
					if(subMenuList!=null && subMenuList.size()>0){
						List<Menu> removeSubMenuList = new ArrayList<Menu>();
						for (Menu subMenu : subMenuList) {
							if(!menuNoList.contains(subMenu.getMenuNo())){
								removeSubMenuList.add(subMenu);
							}
						}
						subMenuList.removeAll(removeSubMenuList);
						childMenu.setChildMenuList(subMenuList);
					}
				}else{
					removeChildMenuList.add(childMenu);
				}
			}
			childMenuList.removeAll(removeChildMenuList);
			menu.setChildMenuList(childMenuList);
		}
		return menu;
	}


}
