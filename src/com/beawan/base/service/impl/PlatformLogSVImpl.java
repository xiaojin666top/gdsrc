package com.beawan.base.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.beawan.base.entity.PlatformLog;
import com.beawan.base.entity.User;
import com.beawan.base.dao.IPlatformLogDAO;
import com.beawan.base.dao.IUserDAO;
import com.beawan.base.dto.LogFileDto;
import com.beawan.base.qbean.QPlatformLog;
import com.beawan.base.service.IPlatformLogSV;
import com.beawan.common.Constants;
import com.beawan.core.Pagination;
import com.platform.util.DateUtil;
import com.platform.util.HttpUtil;
import com.platform.util.PropertiesUtil;
import com.platform.util.StringUtil;

/**
 * 操作日志业务层实现
 * @author beawan_fengjj
 *
 */
@Service("platformLogSV")
public class PlatformLogSVImpl implements IPlatformLogSV {
	
	@Resource
	private IPlatformLogDAO platformLogDAO;
	
	@Resource
	private IUserDAO userDAO;
	

	@Override
	public void saveOperateLog(String msg, String opterateType, String userId)
			throws Exception {
		
		PlatformLog log = new PlatformLog();
		User user = userDAO.getUserById(userId);
		
		log.setOperator(userId);
		log.setOrganizationNo(user.getAccOrgNo());
		//获取到ip地址
		ServletRequestAttributes srt = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		if(srt!=null){
			HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
			log.setIpAddress(HttpUtil.getRemoteAddr(request));
		}
		
		log.setOperationType(opterateType);
		log.setOperationTime(DateUtil.format(DateUtil.getSystemTime(), Constants.DATETIME_MASK));
		String content = msg + "，操作人：" + user.getUserName();
		log.setOperationContent(content);
		platformLogDAO.saveOrUpdate(log);
	}

	@Override
	public List<PlatformLog> paginate(QPlatformLog queryCondition,
			String sortCondition, int start, int limit) throws Exception {
		StringBuffer queryString = new StringBuffer("1=1");
		Map<String, Object> params = new HashMap<String, Object>();
		if(queryCondition!=null){
			if(!StringUtil.isEmptyString(queryCondition.getOperationType())){
				queryString.append(" and operationType=:operationType");
				params.put("operationType", queryCondition.getOperationType());
			}
			if(!StringUtil.isEmptyString(queryCondition.getStartTime())){
				queryString.append(" and operationTime>=:startTime");
				params.put("startTime", queryCondition.getStartTime());
			}
			if(!StringUtil.isEmptyString(queryCondition.getEndTime())){
				queryString.append(" and operationTime<=:endTime");
				params.put("endTime",queryCondition.getEndTime());
			}
		}
		if(!StringUtil.isEmptyString(sortCondition)){
			queryString.append(" order by ").append(sortCondition);
		}
		int startIndex=start*limit;
		return platformLogDAO.selectRange(queryString.toString(), startIndex, limit, params);
	}

	@Override
	public int totalCount(QPlatformLog queryCondition) throws Exception {
		StringBuffer queryString = new StringBuffer("1=1");
		Map<String, Object> params = new HashMap<String, Object>();
		if(queryCondition!=null){
			if(!StringUtil.isEmptyString(queryCondition.getOperationType())){
				queryString.append(" and operationType=:operationType");
				params.put("operationType", queryCondition.getOperationType());
			}
			if(!StringUtil.isEmptyString(queryCondition.getStartTime())){
				queryString.append(" and operationTime>=:startTime");
				params.put("startTime",queryCondition.getStartTime());
			}
			if(!StringUtil.isEmptyString(queryCondition.getEndTime())){
				queryString.append(" and operationTime<=:endTime");
				params.put("endTime", queryCondition.getEndTime());
			}
		}
		return (int)platformLogDAO.selectCount(queryString.toString(), params);
	}

	@Override
	public Pagination<LogFileDto> getLogList(String logType, int page, int pageSize) throws Exception {
		String key = "";
		if("ERRORLOG".equals(logType)) {
			key = "log4j.appender.erroFile.File";
		}else if("INFOLOG".equals(logType)){
			key = "log4j.appender.infoFile.File";
		}
		String path = PropertiesUtil.getProperty(Constants.SYSTEM_LOGS_PATH, key);
		String realPath = path.substring(0, path.lastIndexOf("/") + 1);
		
		List<LogFileDto> list = new ArrayList<>();
		File file = new File(realPath);
		File[] files = file.listFiles();
		if (files.length <= 0) return null; 
		for (File file1 : files) {
			if (!file1.isFile()) continue;
			LogFileDto logFile = new LogFileDto();
			logFile.setFileName(file1.getName());
			logFile.setLastModified(file1.lastModified());
			logFile.setFilePath(file1.getAbsolutePath().replaceAll("\\\\", "/"));
			logFile.setModifiedTime(DateUtil.parseTimestamp(file1.lastModified()));
			logFile.setModifiedDate(logFile.getModifiedTime().substring(0, 10));
			list.add(logFile);
		}
		List<LogFileDto> sortedList = 
				list.stream().sorted(Comparator.comparingLong(LogFileDto::getLastModified).reversed()).collect(Collectors.toList());
		// 分页操作
		int rowsCount = sortedList.size();
		Pagination<LogFileDto> pagination = new Pagination<LogFileDto>(page, pageSize, rowsCount);
		int fromIndex = (page -1) * pageSize, toIndex = 0;
		if(page != pagination.getPagesCount()){
			toIndex = fromIndex + pageSize;
		}else{
			toIndex = rowsCount;
		}
		pagination.setItems(sortedList.subList(fromIndex, toIndex));
		return pagination;
	}

	@Override
	public List<String> getLogContent(String filePath) throws Exception {
		if(StringUtil.isEmptyString(filePath)) return null;
		List<String> result = new ArrayList<>();
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		String lineContent = "";
		while(!StringUtil.isEmptyString(lineContent = reader.readLine())){
			result.add(lineContent);
		}
		if (reader != null) reader.close();
		return result;
	}
}
