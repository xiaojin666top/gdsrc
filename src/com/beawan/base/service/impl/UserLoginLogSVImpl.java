/**
 * Copyright (c) 2014-07 to forever 杭州碧湾信息技术有限公司 All Rights Reserved.  
 *
 * @project mcesys
 * @file_name UserLoginLogImpl.java
 * @author Rain See
 * @create_time 2016年8月18日 下午3:17:53
 * @last_modified_date 2016年8月18日
 * @version 1.0
 */
package com.beawan.base.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.base.entity.UserLoginLog;
import com.beawan.base.dao.IUserLoginLogDAO;
import com.beawan.base.service.IUserLoginLogSV;
import com.beawan.common.Constants;
import com.platform.util.DateUtil;
import com.platform.util.StringUtil;

/**
 * TODO
 * 
 * @author Rain See
 * @create_time 2016年8月18日 下午3:17:53
 */
@Service("userLoginLogSV")
public class UserLoginLogSVImpl implements IUserLoginLogSV {

	@Resource
	private IUserLoginLogDAO userLoginLogDAO;

	@Override
	public void saveOrUpdate(UserLoginLog instance) throws Exception {
		userLoginLogDAO.saveOrUpdate(instance);
	}

	@Override
	public UserLoginLog lastLogin(String userId, String platform) throws Exception {
		
		String queryString = "select model from UserLoginLog as model"
				+ " where model.userId = '" + userId + "'"
				+ " and model.status = '1' "
				+ " and model.platform = '"+ platform +"'"
				+ " order by model.loginTime desc";
		
		return userLoginLogDAO.selectSingle(queryString);
	}

	@Override
	public int getTotalCount(Map<String, String> condition) throws Exception {
		String queryString = genQueryString(condition, 0);
		return (int)userLoginLogDAO.selectCount(queryString, condition);
	}

	@Override
	public List<UserLoginLog> paginate(Map<String, String> condition,
			int pageIndex, int pageSize) throws Exception {

		String queryString = genQueryString(condition, 1);
		int startIndex = pageIndex * pageSize;
		
		return userLoginLogDAO.selectRange(queryString, startIndex, pageSize);
	}

	private String genQueryString(Map<String, String> condition, int flag) {

		StringBuffer sb = new StringBuffer();
		if (flag == 0)
			sb.append("select count(*)");
		else
			sb.append("select model");

		sb.append(" from UserLoginLog as model where 1=1");

		String userId = condition.get("userId");
		if (!StringUtil.isEmptyString(userId))
			sb.append(" and userId = '").append(userId).append("'");

		String status = condition.get("status");
		if (!StringUtil.isEmptyString(status))
			sb.append(" and status = '").append(status).append("'");

		String platform = condition.get("platform");
		if (!StringUtil.isEmptyString(platform))
			sb.append(" and platform = '").append(platform).append("'");

		String deviceId = condition.get("deviceId");
		if (!StringUtil.isEmptyString(deviceId))
			sb.append(" and deviceId = '").append(deviceId).append("'");

		String ipAddress = condition.get("ipAddress");
		if (!StringUtil.isEmptyString(ipAddress))
			sb.append(" and ipAddress = '").append(ipAddress).append("'");

		String startTime = condition.get("startTime");
		String endTime = condition.get("endTime");
		if (!StringUtil.isEmptyString(startTime)
				&& !StringUtil.isEmptyString(endTime)) {
			sb.append(" and ( loginTime between '").append(startTime)
					.append("' and '").append(endTime).append("' )");
		}

		if (flag == 1)
			sb.append(" order by model.loginTime desc");

		return sb.toString();
	}

	@Override
	public Map<String,Object> getCurrentDayLoginInfo(Map<String, String> condition)
			throws Exception {
		
		long loginNum = 0,loginTime = 0;
		
		long dayStart = DateUtil.getDayStart();
		String startTime = DateUtil.formatDate(new Date(dayStart),
				Constants.DATETIME_MASK);
		String endTime = DateUtil.formatDate(new Date(),
				Constants.DATETIME_MASK);
		condition.put("startTime", startTime);
		condition.put("endTime", endTime);

		String queryString = genQueryString(condition, 1);
		List<UserLoginLog> logs = userLoginLogDAO.select(queryString);
		if(logs != null){
			loginNum = logs.size();
			loginTime = getLoginTime(logs);
		}
		
		
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("loginNum", loginNum);
		result.put("loginTime", loginTime);
		
		return result;
	}

	@Override
	public Map<String,Object> getHistoryLoginInfo(Map<String, String> condition)
			throws Exception {
		
		long loginNum = 0,loginTime = 0;
		
		String queryString = genQueryString(condition, 1);
		List<UserLoginLog> logs = userLoginLogDAO.select(queryString);
		if(logs != null){
			loginNum = logs.size();
			loginTime = getLoginTime(logs);
		}
		
		
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("loginNum", loginNum);
		result.put("loginTime", loginTime);
		
		return result;
	}

	private long getLoginTime(List<UserLoginLog> logs) {
		long time = 0;
		try {
			if (logs != null && logs.size() != 0) {
				long hour = 36000000;
				for (int i = 0; i < logs.size(); i++) {

					UserLoginLog log = logs.get(i);

					Date startDate = DateUtil.parseDate(log.getLoginTime(),
							Constants.DATETIME_MASK);

					Date endDate = null;
					String endTime = log.getLoginoutTime();
					
					//历史登录记录且无退出时间
					boolean flag = false;
					if (endTime == null || endTime.equals("")) {
						if (i == 0) {
							endDate = new Date();
						} else {
							UserLoginLog prevlog = logs.get(i - 1);
							endDate = DateUtil.parseDate(
									prevlog.getLoginTime(),
									Constants.DATETIME_MASK);
							flag = true;
						}
					} else {
						endDate = DateUtil.parseDate(log.getLoginoutTime(),
								Constants.DATETIME_MASK);
					}

					long temp = DateUtil.getTime(startDate, endDate, -1);
					
					if(flag){
						// 会话超时时间为1小时，如果时间差大于1小时，则取一小时
						if (temp > hour)
							temp = hour;
					}
					
					time += temp;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return time;
	}
	
}
