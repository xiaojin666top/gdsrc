package com.beawan.base.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.base.entity.TableSubjCode;
import com.beawan.base.dao.ITableSubjCodeDAO;
import com.beawan.base.service.ITableSubjCodeSV;


@Service("tableSubjCodeSV")
public class TableSubjCodeSVImpl implements ITableSubjCodeSV {
	
	@Resource
	private ITableSubjCodeDAO tableSubjCodeDAO;

	@Override
	public void saveOrUpdate(TableSubjCode tableSubjCode) throws Exception {
		tableSubjCodeDAO.saveOrUpdate(tableSubjCode);
	}

	@Override
	public List<TableSubjCode> queryByTableSubjCodes(String businType,
			String subjType) throws Exception {
		String hql = "businType=:businType and subjType=:subjType order by sort asc";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("businType", businType);
		params.put("subjType", subjType);
		List<TableSubjCode> list = tableSubjCodeDAO.select(hql, params);
		return list;
	}

	@Override
	public List<TableSubjCode> queryByBusType(String businType)throws Exception{
		String hql = "businType=:businType";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("businType", businType);
		List<TableSubjCode> list = tableSubjCodeDAO.select(hql, params);
		return list;
	}
	
	
}
