package com.beawan.base.service.impl;

import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.beawan.base.dto.UserDto;
import com.beawan.core.Pagination;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Service;

import com.beawan.base.entity.User;
import com.beawan.base.entity.UserLoginErrorInfo;
import com.beawan.base.entity.UserLoginLog;
import com.beawan.base.dao.IUserDAO;
import com.beawan.base.qbean.QUser;
import com.beawan.base.service.IPlatformLogSV;
import com.beawan.base.service.IUserLoginErrorInfoSV;
import com.beawan.base.service.IUserSV;
import com.beawan.base.service.IUserLoginLogSV;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.platform.util.BeanUtil;
import com.platform.util.DateUtil;
import com.platform.util.EncryptUtil;
import com.platform.util.ExceptionUtil;
import com.platform.util.HttpUtil;
import com.platform.util.JdbcUtil;
import com.platform.util.PropertiesUtil;
import com.platform.util.StringUtil;

@Service("userSV")
public class UserSVImpl implements IUserSV {

	@Resource
	private IUserDAO userDAO;
	@Resource
	private IUserLoginLogSV userLoginLogSV;
	@Resource
	private IPlatformLogSV platformLogSV;
	@Resource
	private IUserLoginErrorInfoSV userLoginErrorInfoSV;
	
	@Override
	public User saveOrUpdate(User user) throws Exception {
		return userDAO.saveOrUpdate(user);
	}

	@Override
	public User saveUserInfo(String jsondata) throws Exception {
		
		JSONObject json = JSONObject.fromObject(jsondata);
		//解析出用户信息数据
		User user = (User) JSONObject.toBean(json, User.class);
		
		if (StringUtil.isEmptyString(user.getUserId())) {
			ExceptionUtil.throwException("工号不能为空！");
		}
		//查询出数据库中的用户
		User oldUser = this.queryById(user.getUserId());
		
		User operator = HttpUtil.getCurrentUser();
		String datetime = DateUtil.format(new Date(), Constants.DATETIME_MASK);
		
		//新增
		if (oldUser == null) {
			
			String password = user.getPassword();
			if (!StringUtil.isEmptyString(password))
				user.setPassword(EncryptUtil.md5(password));

			user.setLoginId(user.getUserId().replace("I05", ""));
			user.setState(SysConstants.UserStatus.NORMAL);
			user.setSignState("online");//新增用户默认在岗
			user.setWrongPinNum(0);
			user.setConWrongPinNum(0);
			//user.setPwdExpiryDate(sdFormat.format(new Date()));
			user.setLastPassword(user.getPassword());
			user.setInputOrg(operator.getAccOrgNo());
			user.setInputUser(operator.getUserId());
			user.setInputTime(datetime);
			user.setUpdateUser(operator.getUserId());
			user.setUpdateTime(DateUtil.getNowTimestamp());
			
			oldUser = user;
			
			//保存操作日志
			platformLogSV.saveOperateLog("新增用户，用户名称：" + user.getUserName(),
					SysConstants.PlatformLogType.NEW_USER, operator.getUserId());
			
		} else {//修改
			
			BeanUtil.mergeProperties(user, oldUser, json);
			oldUser.setUpdateUser(operator.getUserId());
			oldUser.setUpdateTime(DateUtil.getNowTimestamp());
			
			//保存操作日志
			platformLogSV.saveOperateLog("修改用户，用户名称：" + user.getUserName(),
					SysConstants.PlatformLogType.MODIFY_USER, operator.getUserId());
		}
		
		return userDAO.saveOrUpdate(oldUser);
	}
	
	@Override
	public List<Map<String, Object>> queryUserPaging(QUser queryCondition,
			String orderCondition, int pageIndex,
			int pageSize) throws Exception {
		
		System.out.println("查询所有用户");
		String sql = "SELECT T.*"
				   + " FROM ("
		           + " SELECT ROW_NUMBER() OVER() AS ROW_NUM,"
		           + "U.USER_ID,U.USER_NAME,U.ACC_ORG_NO AS ORG_NO,U.ROLE_NO,U.PHONE,U.INPUT_TIME,"
		           + "O.organname AS ORG_NAME,D1.CNNAME AS STATUS,D2.CNNAME AS BUSINESS_LINE"
		           + " FROM GDTCESYS.BS_USER_INFO U"
		           + " LEFT JOIN GDTCESYS.BS_ORG O ON O.ORGANNO = U.ACC_ORG_NO"
		           + " LEFT JOIN GDTCESYS.BS_DIC D1 ON D1.OPTTYPE='STD_USER_STATUS' AND D1.ENNAME = U.STATE"
		           + " LEFT JOIN GDTCESYS.BS_DIC D2 ON D2.OPTTYPE='STD_SY_BUSI_LINE' AND D2.ENNAME = U.BUSINESS_LINE"
		           + " WHERE $"
		           + " ) T WHERE T.ROW_NUM > ? AND T.ROW_NUM <=?";
		
		String queryStr = genQueryStr(queryCondition);
		sql = sql.replace("$", queryStr);
		
		if (!StringUtil.isEmptyString(orderCondition))
			sql = sql.replace("OVER()", "OVER(ORDER BY " + orderCondition + ")");
		else
			sql = sql.replace("OVER()", "OVER(ORDER BY U.INPUT_TIME )");
		int startIndex = pageIndex*pageSize;
		int endIndex = startIndex + pageSize;
		Object[] params = new Object[]{startIndex, endIndex};
		
		return JdbcUtil.query(Constants.DataSource.TCE_DS, sql, params);
	}

	@Override
	public Pagination<UserDto> queryUserPaging(String roleNo, String userName, String userNo, int page, int pageSize) {
		StringBuilder sql = new StringBuilder();
		Map<String, Object> params = new HashMap<>();
//		select a.user_id userId,a.user_name userName,b.ORGANNAME
//		from GDTCESYS.bs_user_info a
//		left join GDTCESYS.bs_org b on a.acc_org_no = b.ORGANNO
//		where a.role_no='0011' and a.user_id like '%0600%' and a.user_name like '%周%'
		sql.append("select a.user_id userId,a.user_name userName,b.ORGANNAME orgName")
				.append(" from GDTCESYS.bs_user_info a")
				.append(" left join GDTCESYS.bs_org b on a.acc_org_no = b.ORGANNO")
				.append(" where 1=1");
		if(!StringUtil.isEmptyString(roleNo)){
			sql.append(" and a.role_no like :roleNo");
			params.put("roleNo", "%"+roleNo+"%");
		}
		if(!StringUtil.isEmptyString(userName)){
			sql.append(" and a.user_name like :userName");
			params.put("userName", "%" + userName + "%");
		}
		if(!StringUtil.isEmptyString(userNo)){
			sql.append(" and a.user_id like :userNo");
			params.put("userNo", "%" + userNo + "%");
		}
		return userDAO.findCustSqlPagination(UserDto.class, sql.toString(), params, page, pageSize);
	}

	@Override
	public long queryUserCount(QUser queryCondition) throws Exception {
		
		String sql = "SELECT COUNT(*)"
				   + " FROM GDTCESYS.BS_USER_INFO U"
				   + " LEFT JOIN GDTCESYS.BS_ORG O ON O.ORGANNO = U.ACC_ORG_NO"
				   + " WHERE $";
		
		String queryStr = genQueryStr(queryCondition);
		sql = sql.replace("$", queryStr);
		
		return JdbcUtil.queryForLong(Constants.DataSource.TCE_DS, sql, null);
	}
	
	private String genQueryStr(QUser queryCondition) throws Exception {
		
		StringBuffer listSql = new StringBuffer("1=1");
		
		if (queryCondition != null) {
			
			if (!StringUtil.isEmptyString(queryCondition.getUserId())) {
				listSql.append(" AND U.USER_ID = '")
				       .append(queryCondition.getUserId())
				       .append("'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getUserName())) {
				String name = URLDecoder.decode(queryCondition.getUserName(), "UTF-8");
				listSql.append(" AND U.USER_NAME LIKE '%")
			       .append(name)
			       .append("%'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getOrgNo())) {
				listSql.append(" AND U.ACC_ORG_NO = '")
				       .append(queryCondition.getOrgNo())
				       .append("'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getOrgName())) {
				String name = URLDecoder.decode(queryCondition.getOrgName(), "UTF-8");
				listSql.append(" AND O.ORGANNAME LIKE '%")
			       .append(name)
			       .append("%'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getRoleNo())) {
				listSql.append(" AND U.ROLE_NO LIKE '%")
			       .append(queryCondition.getRoleNo())
			       .append("%'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getBusinessLine())) {
				listSql.append(" AND U.BUSINESS_LINE = '")
			       .append(queryCondition.getBusinessLine())
			       .append("'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getStatus())) {
				listSql.append(" AND U.STATE = '")
			       .append(queryCondition.getStatus())
			       .append("'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getSignState())) {
				listSql.append(" AND U.SIGN_STATE = '")
			       .append(queryCondition.getSignState())
			       .append("'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getStartTime())) {
				listSql.append(" AND U.INPUT_TIME >= '")
			       .append(queryCondition.getStartTime())
			       .append("'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getEndTime())) {
				listSql.append(" AND U.INPUT_TIME <= '")
			       .append(queryCondition.getEndTime())
			       .append("'");
			}
			if(!queryCondition.getOrgChilds().isEmpty()){
				listSql.append(" AND U.ACC_ORG_NO IN (");
				boolean flag = false;
				for(String org : queryCondition.getOrgChilds()){
					if(flag){
						listSql.append(",");
					}
					flag = true;
					listSql.append("'"+org+"'");
				}
				listSql.append(")");
			}
		}
		
		return listSql.toString();
	}

	@Override
	public User queryById(String userId) throws Exception {
		User userById = userDAO.getUserById(userId);
		return userById;
	}
	
	@Override
	public void update(User user) throws Exception {
		userDAO.saveOrUpdate(user);
	}

	@Override
	public void cancal(User user) throws Exception {
		user.setState(SysConstants.UserStatus.CANCEL);
		userDAO.saveOrUpdate(user);
	}
	
	@Override
	public void use(User user) throws Exception {
		user.setState(SysConstants.UserStatus.NORMAL);
		userDAO.saveOrUpdate(user);
	}
	
	@Override
	public void delete(User user) throws Exception {
		userDAO.deleteEntity(user);
	}

	@Override
	public List<User> queryByRoleNo(String roleNo) throws Exception {
		String queryString = "from User as model where model.roleNo = :roleNo";
		Map<String, Object> params = new HashMap<>();
		params.put("roleNo", roleNo);
		return userDAO.select(queryString, params);
	}

	@Override
	public List<User> queryByNos(String userIds) throws Exception {
		String[] userIdArray = userIds.split(",");
		String inSqlString = "";
		for (String userId : userIdArray) {
			inSqlString += "'" + userId + "',";
		}
		if(inSqlString.length()>0){
			inSqlString = inSqlString.substring(0, inSqlString.length()-1);
		}
		String queryString = "from User as model where model.userId in("+ inSqlString + ")";
		List<User> list = userDAO.select(queryString);
		return list;
	}
	
	@Override
	public String queryNameByNos(String userIds) throws Exception {
		
		String names = "";
		if(StringUtil.isEmptyString(userIds))
			return names;
		
		String[] userIdArray = userIds.split(",");
		String inSqlString = "";
		for (String userId : userIdArray) {
			inSqlString += "'" + userId + "',";
		}
		if(inSqlString.length()>0){
			inSqlString = inSqlString.substring(0, inSqlString.length()-1);
		}
		
		String sql = "SELECT USER_NAME FROM GDTCESYS.BS_USER_INFO WHERE USER_ID IN("+inSqlString+")";
		List<Object[]> list = JdbcUtil.execQuery(sql, Constants.DataSource.TCE_DS);
		
		for(Object[] fileds : list){
			names += fileds[0]+"，";
		}
		
		if(names.length() > 0){
			names = names.substring(0, names.length()-1);
		}
		
		return names;
	}

	@Override
	public Map<String, Object> loginPC(String userId, String password, String ip,
			String mac) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		User user = this.queryById(userId);
		result.put("user", user);
		String remarks = null, status = null;
		//错几次锁定
		int LoginErrorMaxNum = Integer.parseInt(PropertiesUtil.getProperty("system.properties", "user.login.lockNum"));
		
		if (user != null) {
			//if("L".equals(user.getStatus())){
			if("L".equals(user.getState())){
				status = "0";
				remarks = "该账号已锁定,请联系管理员";
			}else if( userLoginErrorInfoSV.getErrorNum(userId) >= LoginErrorMaxNum){
				//大于最大错误次数
				status = "0";
				remarks = "该账号已锁定";
			}else if ("N".equalsIgnoreCase(user.getState()) && user.getPassword().equals(EncryptUtil.md5(password))) {
				 status = "1";
				 //登录成功就解锁
				 userLoginErrorInfoSV.removeLoginErrorTime(userId);
			} else {
				//记录密码错误次数
				status = "0";
				remarks = "";
				
				UserLoginErrorInfo errorInfo = userLoginErrorInfoSV.addLoginErrorTime(userId);
				if(errorInfo.getLoginErrorNum() == LoginErrorMaxNum){
					//1表示需要人工解锁
					if("1".equals(PropertiesUtil.getProperty("system.properties", "user.login.shouldUnLock"))){
						//更改用户状态---错了三次锁定账号
						user.setState("L");
						this.userDAO.saveOrUpdate(user);
					}
					remarks = "密码错误 : 账号已锁定,请联系管理员!";
				}else{
					remarks = "密码错误 :已错误"+errorInfo.getLoginErrorNum()+"次,错误"+LoginErrorMaxNum+"次将锁定账号!";
				}
			}
		} else {
			status = "0";
			remarks = "用户不存在";
		}
		result.put("remarks", remarks);
		result.put("status", status);

		UserLoginLog log = new UserLoginLog();
		log.setUserId(userId);
		log.setIpAddress(ip);
		log.setMacAddress(mac);
		log.setPlatform(Constants.Platform.PC);
		log.setStatus(status);
		log.setRemarks(remarks);
		log.setLoginTime(DateUtil.format(new Date(),
				Constants.DATETIME_MASK));
		//userLoginLogSV.saveOrUpdate(log);
		return result;
	}

	/**
	 * 移动端登录
	 * @param userId
	 * @param password
	 *            用户密码
	 * @param ip
	 *            客户端IP地址
	 * @param mac
	 *            客户端MAC地址
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> loginMB(String userId, String password, String ip, String mac) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		User user = this.queryById(userId);
		result.put("user", user);
		String remarks = null, status = null;
		//错几次锁定
		int LoginErrorMaxNum = Integer.parseInt(PropertiesUtil.getProperty("system.properties", "user.login.lockNum"));

		if (user != null) {
			if("L".equals(user.getStatus())){
				status = "0";
				remarks = "该账号已锁定,请联系管理员";
			}else if( userLoginErrorInfoSV.getErrorNum(userId) >= LoginErrorMaxNum){
				//大于最大错误次数
				status = "0";
				remarks = "该账号已锁定";
			}else if (user.getPassword().equals(EncryptUtil.md5(password))) {
				status = "1";
				//登录成功就解锁
				userLoginErrorInfoSV.removeLoginErrorTime(userId);
			} else {
				//记录密码错误次数
				status = "0";
				remarks = "";
				UserLoginErrorInfo errorInfo = userLoginErrorInfoSV.addLoginErrorTime(userId);
				if(errorInfo.getLoginErrorNum() == LoginErrorMaxNum){
					//1表示需要人工解锁
					if("1".equals(PropertiesUtil.getProperty("system.properties", "user.login.shouldUnLock"))){
						//更改用户状态---错了三次锁定账号
						user.setState("L");
						this.userDAO.saveOrUpdate(user);
					}
					remarks = "密码错误 : 账号已锁定,请联系管理员!";
				}else{
					remarks = "密码错误 :已错误"+errorInfo.getLoginErrorNum()+"次,错误"+LoginErrorMaxNum+"次将锁定账号!";
				}
			}
		} else {
			status = "0";
			remarks = "用户不存在";
		}
		result.put("remarks", remarks);
		result.put("status", status);
		UserLoginLog log = new UserLoginLog();
		log.setUserId(userId);
		log.setIpAddress(ip);
		log.setMacAddress(mac);
		log.setPlatform(Constants.Platform.MOBILE);
		log.setStatus(status);
		log.setRemarks(remarks);
		log.setLoginTime(DateUtil.format(new Date(),
				Constants.DATETIME_MASK));
		userLoginLogSV.saveOrUpdate(log);
		return result;
	}

	public void loginout(String userId, String platform) throws Exception {
		
		UserLoginLog log = userLoginLogSV.lastLogin(userId, platform);
		if (log != null) {
			log.setLoginoutTime(DateUtil.format(new Date(),
					Constants.DATETIME_MASK));
			userLoginLogSV.saveOrUpdate(log);
		}
	}

	@Override
	public void resetPassword(String userId, String newPassword) throws Exception {
		
		User user = this.queryById(userId);
		if (user == null) {
			ExceptionUtil.throwException("用户不存在，重置失败！");
		}
		
		String operatorId = HttpUtil.getCurrentUser().getUserId();
		user.setLastPassword(user.getPassword());
		user.setPassword(EncryptUtil.md5(newPassword));
		user.setWrongPinNum(0);
		user.setConWrongPinNum(0);
		user.setUpdateUser(operatorId);
		user.setUpdateTime(DateUtil.getNowTimestamp());

		this.update(user);

		platformLogSV.saveOperateLog("修改密码--operatorId",
				SysConstants.PlatformLogType.MODIFY_PASSWORD, operatorId);// 日志记录
	}
	
	@Override
	public void resetPassword(String userId,String oldPassword, String newPassword) throws Exception {
		
		User user = this.queryById(userId);
		if (user == null) {
			ExceptionUtil.throwException("用户不存在，重置失败！");
		}
		
		if(!user.getPassword().equals(EncryptUtil.md5(oldPassword))){
			ExceptionUtil.throwException("原密码错误");
		}
		
		String operatorId = HttpUtil.getCurrentUser().getUserId();
		user.setLastPassword(user.getPassword());
		user.setPassword(EncryptUtil.md5(newPassword));
		user.setWrongPinNum(0);
		user.setConWrongPinNum(0);
		user.setUpdateUser(operatorId);
		user.setUpdateTime(DateUtil.getNowTimestamp());

		this.update(user);

		platformLogSV.saveOperateLog("修改密码--本人",
				SysConstants.PlatformLogType.MODIFY_PASSWORD, operatorId);// 日志记录
	}

	@Override
	public void modifyPwdPC(String userId, String oldPassword, String newPassword)
			throws Exception {
		
		User user = this.queryById(userId);// 获取用户信息
		String passwordNow = user.getPassword();// 获取此用户现在登录的密码
		oldPassword = EncryptUtil.md5(oldPassword);// 获取传进来的原密码
		if (passwordNow.equals(oldPassword)) {
			user.setLastPassword(oldPassword);// 把传进来的原密码赋值给此用户"上次登录密码"
			user.setPassword(EncryptUtil.md5(newPassword));
			user.setWrongPinNum(0);
			user.setConWrongPinNum(0);
			this.update(user);
		} else {
			ExceptionUtil.throwException("原始密码不正确！");
		}
	}
	
	@Override
	public int modifyPwdMB(String userId, String oldPassword, String newPassword) {
		int flag = 0;//用户不存在
		try {
			User user = this.queryById(userId);
			if (user != null) {
				if (oldPassword.equals(user.getPassword())) {
					user.setPassword(newPassword);
					user.setLastPassword(oldPassword);

					String configFP = "/properties/cf/system.properties";
					int pwdExpiryDays = Integer.parseInt(PropertiesUtil
							.getProperty(configFP, "user.pwd.expiryDays"));
					long curTime = new Date().getTime();
					long dayTime = 24 * 60 * 60 * 1000;
					// 计算密码失效日期
					long time = curTime + (dayTime) * pwdExpiryDays;
					String expireDay = DateUtil.format(new Date(time),
							"yyyy-MM-dd");
					user.setPwdExpiryDate(expireDay);
					userDAO.saveOrUpdate(user);
					flag = 1;//修改成功
				} else
					flag = 2;//原密码错误
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return flag;
	}

	@Override
	public boolean onlineCheck(String userId, String platform) throws Exception {
		UserLoginLog loginLog = userLoginLogSV.lastLogin(userId, platform);
		return isOnlineLog(loginLog);
	}

	public boolean isOnlineLog(UserLoginLog loginLog) {
		boolean online = false;
		if (loginLog != null) {

			long time = DateUtil.getDayStart();
			Date ds = new Date(time);
			String dayStart = DateUtil.formatDate(ds,
					Constants.DATETIME_MASK);

			String loginOutTime = loginLog.getLoginoutTime();
			if ((loginOutTime == null || "".equals(loginOutTime))
					&& loginLog.getLoginTime().compareTo(dayStart) > 0) {
				online = true;
			}
		}
		return online;
	}

	@Override
	public List<User> queryUserNOAdmin() {
		// TODO Auto-generated method stub
		
		List<User> users= null;
		try {
			users = this.userDAO.getUsers();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return users;
	}
	
	/**
	 * 获取登录错误的提示
	 * 首次登录错误无提示
	 * 第二次提示:你今日已错误2次,错误3次将锁定账号!
	 * 第三次提示：已锁定
	 * 锁定的账户不会进到这里
	 * @param userId
	 * @return
	 */
	/*public String loginError(String userId) {
		
		SimpleDateFormat formData = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date data = new Date();
		String format = formData.format(new Date());
		//获取24小时前时间
		String oneDayAgo = formData.format(new Date(data.getTime()-1*24*60*60*1000));
		//是因为24小时后自动解锁吗
		UserLoginErrorInfo errorInfo = this.loginErrorInfoDao.getLoginError(userId,oneDayAgo);
		User user = null;
		try {
			 user = this.userDAO.getUserById(userId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		//24小时内不存在登录错误记录
		if(errorInfo == null){
			errorInfo = new UserLoginErrorInfo();
			errorInfo.setUserId(userId);
			errorInfo.setUserName(user.getUserName());
			errorInfo.setLoginErrorTime(format);
			errorInfo.setLoginErrorNum(0);//新建设置为0
			errorInfo.setIpAddress(HttpUtil.getRemoteAddr());
		}
		errorInfo.setLoginErrorNum(errorInfo.getLoginErrorNum()+1);//只要是走到这里，错误次数都要+1
		errorInfo = this.loginErrorInfoDao.saveOrUpdate(errorInfo);
		String resultError = "";
		if(errorInfo.getLoginErrorNum() == 2){
			resultError = "   你今日已错误2次,错误3次将锁定账号!";
		}else if(errorInfo.getLoginErrorNum() == 3){
			resultError = "账号已锁定,请联系管理员!";
			//更改用户状态---错了三次锁定账号
			user.setStatus("L");
			this.userDAO.saveOrUpdate(user);
		}else{
			
		}
		return resultError;*/
		/*System.out.println("断点，写的太重复了xyh");
		if(errorInfo == null){
			//新增一条记录次数为1	
			System.out.println("1次");
			UserLoginErrorInfo errorInfoNew = new UserLoginErrorInfo();
			errorInfoNew.setUserId(userId);
			errorInfoNew.setUserName(user.getUserName());
			errorInfoNew.setLoginErrorTime(format);
			errorInfoNew.setLoginErrorNum(1);
			errorInfoNew.setIpAddress(HttpUtil.getRemoteAddr());
			this.loginErrorInfoDao.saveLoginErrorInfo(errorInfoNew);
			
		}else {
			System.out.println("2次");
			int errorNum = errorInfo.getLoginErrorNum();
			if (errorNum == 0) {
				//错误次数加1
				errorInfo.setLoginErrorTime(format);
				errorInfo.setLoginErrorNum(errorNum+1);
				errorInfo.setIpAddress(HttpUtil.getRemoteAddr());
				this.loginErrorInfoDao.updateErrorInfo(errorInfo);
				return "";
			}else if (errorNum == 1) {
				//错误次数加1
				errorInfo.setLoginErrorTime(format);
				errorInfo.setLoginErrorNum(errorNum+1);
				errorInfo.setIpAddress(HttpUtil.getRemoteAddr());
				this.loginErrorInfoDao.updateErrorInfo(errorInfo);
				return "   你今日已错误2次,错误3次将锁定账号!";
			}else {
				//错误次数加1并且锁定用户状态
				errorInfo.setLoginErrorTime(format);
				errorInfo.setLoginErrorNum(errorNum+1);
				errorInfo.setIpAddress(HttpUtil.getRemoteAddr());
				this.loginErrorInfoDao.updateErrorInfo(errorInfo);
				//更改用户状态
				user.setStatus("L");
				User user2 = this.userDAO.saveOrUpdate(user);
				System.out.println("更新后的用户状态!"+user2);
				return "账号已锁定,请联系管理员!";
			}
			
			
		}*/
	//}

	@Override
	public int updateLock(String userId) {
		try {
			// 解除指定时间内的锁定
			userLoginErrorInfoSV.removeLoginErrorTime(userId);
			// 解除用户身上的锁定
			User user = this.userDAO.getUserById(userId);
			if (user != null && "L".equals(user.getStatus())) {
				user.setState("N");
				this.userDAO.saveOrUpdate(user);
			}
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public List<User> getUserByFullName(String userName) {
		String sql = "from User where userName ?1";

		return userDAO.selectByProperty("userName", userName);
	}
	
}
