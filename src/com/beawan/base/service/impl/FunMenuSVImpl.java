package com.beawan.base.service.impl;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.beawan.base.entity.FunMenu;
import com.beawan.base.dao.IFunMenuDAO;
import com.beawan.base.service.IFunMenuSV;
import com.platform.util.StringUtil;
@Service("funMenuSV")
public class FunMenuSVImpl implements IFunMenuSV {
	
	@Resource
	private IFunMenuDAO funMenuDAO;

	@Override
	public void saveOrUpdate(FunMenu FunMenu) throws Exception {
		
		funMenuDAO.saveOrUpdate(FunMenu);
		
		String parentNo = FunMenu.getParentNo();
		if(!StringUtil.isEmptyString(parentNo)
				&& !"0".equals(parentNo)){
			FunMenu pFunMenu = funMenuDAO.queryByNo(parentNo);
			if(pFunMenu != null){
				pFunMenu.setMenuType("P");
				funMenuDAO.saveOrUpdate(pFunMenu);
			}
		}
	}
	
	@Override
	public void delete(FunMenu FunMenu) throws Exception {
		funMenuDAO.delete(FunMenu);
	}
	
	@Override
	public void deleteByMenuNo(String FunMenuNo) throws Exception {
		FunMenu FunMenu = funMenuDAO.queryByNo(FunMenuNo);
		if(FunMenu != null)
			recursDelete(FunMenu);
	}
	
	public void recursDelete(FunMenu FunMenu) throws Exception{
		
		List<FunMenu> list = funMenuDAO.queryByParentNo(FunMenu.getMenuNo());
		if(!CollectionUtils.isEmpty(list)){
			for(FunMenu m : list){
				recursDelete(m);
			}
		}
		
		funMenuDAO.delete(FunMenu);
	}

	@Override
	public List<FunMenu> paginate(FunMenu queryCondition,
			String sortCondition, int start, int limit) throws Exception {
		StringBuffer queryString = new StringBuffer("from FunMenu as model where model.FunMenuType='P'"); 
		List<Object> params = new ArrayList<Object>();
		if(queryCondition!=null){
			if(!StringUtil.isEmptyString(queryCondition.getMenuNo())){
				queryString.append(" and model.FunMenuNo like ?");
				params.add("%"+queryCondition.getMenuNo()+"%");
			}
			if(!StringUtil.isEmptyString(queryCondition.getMenuName())){
				String name = URLDecoder.decode(queryCondition.getMenuName(), "UTF-8");
				queryString.append(" and model.FunMenuName like ?");
				params.add("%" + name + "%");
			}
		}
		if(!StringUtil.isEmptyString(sortCondition)){
			queryString.append(" order by ").append(sortCondition);
		}
		int startIndex=start*limit;
		
		List<FunMenu> list = funMenuDAO.queryPaginate(queryString.toString(), params.toArray(),startIndex, limit);
		return list;
	}

	@Override
	public int totalCount(FunMenu queryCondition) throws Exception {
		StringBuffer queryString = new StringBuffer("from FunMenu as model where model.FunMenuType='P'"); 
		List<Object> params = new ArrayList<Object>();
		if(queryCondition!=null){
			if(!StringUtil.isEmptyString(queryCondition.getMenuNo())){
				queryString.append(" and model.FunMenuNo like ?");
				params.add("%"+queryCondition.getMenuNo()+"%");
			}
			if(!StringUtil.isEmptyString(queryCondition.getMenuName())){
				String name = URLDecoder.decode(queryCondition.getMenuName(), "UTF-8");
				queryString.append(" and model.FunMenuName like ?");
				params.add("%" + name + "%");
			}
		}
		List<FunMenu> list = funMenuDAO.queryCondition(queryString.toString(),params.toArray());
		if(!CollectionUtils.isEmpty(list)){
			return list.size();
		}else{
			return 0;
		}
		
		
	}

	@Override
	public FunMenu queryByNo(String FunMenuNo) throws Exception {
		return funMenuDAO.queryByNo(FunMenuNo);
	}

	@Override
	public List<FunMenu> queryChildrenMenu(String parentNo) throws Exception {
		return funMenuDAO.queryByParentNo(parentNo);
	}

	@Override
	public List<FunMenu> queryTreeMenu(String parentNo) throws Exception {
		List<FunMenu> childFunMenus = funMenuDAO.queryByParentNo(parentNo);
		if(childFunMenus != null && childFunMenus.size() > 0){
			for(FunMenu FunMenu: childFunMenus){
				dsfChildTree(FunMenu);
			}
		}
		return childFunMenus;
	}
	/**
	 * 递归查询树形FunMenu
	 * @param parentNo
	 */
	private void dsfChildTree(FunMenu parentFunMenu) throws Exception{
		List<FunMenu> childFunMenus = new ArrayList<FunMenu>();
		childFunMenus =  funMenuDAO.queryByParentNo(parentFunMenu.getMenuNo());
		if(childFunMenus == null || childFunMenus.size() <= 0){
			return;
		}
		for(FunMenu FunMenu: childFunMenus){
			dsfChildTree(FunMenu);
		}
		parentFunMenu.setChildMenuList(childFunMenus);
	}

}
