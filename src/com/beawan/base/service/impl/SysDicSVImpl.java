package com.beawan.base.service.impl;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Service;

import com.beawan.base.entity.SysDic;
import com.beawan.base.entity.User;
import com.beawan.base.dao.ISysDicDAO;
import com.beawan.base.dao.ISysTreeDicDAO;
import com.beawan.base.service.ISysDicSV;
import com.beawan.common.SysConstants;
import com.platform.util.BeanUtil;
import com.platform.util.StringUtil;

@Service("sysDicSV")
public class SysDicSVImpl implements ISysDicSV {

	@Resource
	private ISysDicDAO sysDicDAO;
	
	@Resource
	private ISysTreeDicDAO sysTreeDicDAO;
	
	@Override
	public SysDic saveOrUpdate(SysDic dic) throws Exception {
		return sysDicDAO.saveOrUpdate(dic);
	}
	
	@Override
	public SysDic saveDicInfo(String jsondata) throws Exception {
		
		JSONObject json = JSONObject.fromObject(jsondata);
		SysDic dic = (SysDic) JSONObject.toBean(json, SysDic.class);
		//更新
		if(dic.getSdId() != null){
			SysDic oldDic = sysDicDAO.selectByPrimaryKey(dic.getSdId());
			if(oldDic != null){
				BeanUtil.mergeProperties(dic, oldDic, json);
				dic = oldDic;
			}
		}
		
		return sysDicDAO.saveOrUpdate(dic);
	}
	
	@Override
	public void delete(SysDic dic) throws Exception {
		sysDicDAO.deleteEntity(dic);
	}
	
	@Override
	public SysDic findById(long sdId) throws Exception {
		return sysDicDAO.selectByPrimaryKey(sdId);
	}

	@Override
	public List<SysDic> findCertType() throws Exception {
		return sysDicDAO.queryByOptType("STD_ZB_CERT_TYP");
	}
	
	@Override
	public List<SysDic> findByType(String optType) throws Exception {
		return sysDicDAO.queryByOptType(optType);
	}

	@Override
	public SysDic findByEnNameAndType(String enName, String optType) throws Exception {
		return sysDicDAO.queryByEnNameAndOptType(enName, optType);
	}

	@Override
	public List<SysDic> queryByTypes(String optTypes) throws Exception {
		
		String inStr = "'" + optTypes.replace(",", "','") + "'";
		
		String hsql = "from SysDic as model where model.optType in (" + inStr + ")";
		
		return sysDicDAO.select(hsql);
	}

	@Override
	public List<SysDic> queryByTypesOrderByENName(String optTypes) throws Exception {
		String inStr = "'" + optTypes.replace(",", "','") + "'";

		String hsql = "from SysDic as model where model.optType in (" + inStr + ")  order by model.enName asc ";

		return sysDicDAO.select(hsql);
	}

	@Override
	public List<SysDic> findAll() {
		return sysDicDAO.getAll();
	}
	
	@Override
	public String queryMapListByTypes(String optTypes) throws Exception {
		
		String inStr = "'" + optTypes.replace(",", "','") + "'";
		
		String hsql = "from SysDic where optType in (" + inStr + ") order by orderId desc";
		
		List<SysDic> list = sysDicDAO.select(hsql);
		
		Map<String, List<SysDic>> map = new HashMap<String, List<SysDic>>();
		for(SysDic dic : list){
			String optType = dic.getOptType();
			List<SysDic> temp = (List<SysDic>) map.get(optType);
			if(temp == null){
				temp = new ArrayList<SysDic>();
				map.put(optType, temp);
			}
			temp.add(dic);
		}
        for (String val : map.keySet()) {
        	
        	Collections.sort(map.get(val), new Comparator<SysDic>() {
    			@Override
    			public int compare(SysDic arg0, SysDic arg1) {
    				return (int) (arg0.getSdId() - arg1.getSdId());
    			}
    		});
		}
		
		return JSONObject.fromObject(map).toString();
	}
	
	@Override
	public String queryMapListByTypes(String[] optTypes) throws Exception {
		String types = "";
		for(int i=0;i<optTypes.length;i++){
			types += "," + optTypes[i];
		}
		return queryMapListByTypes(types.substring(1));
	}
	
	@Override
	public String queryMapListByTypes(String[] optTypes, User curUser) throws Exception {
		String types = "";
		for(int i=0;i<optTypes.length;i++){
			types += "," + optTypes[i];
		}
		String optTypeStr = types.substring(1);
		String inStr = "'" + optTypeStr.replace(",", "','") + "'";
		
		String hsql = "from SysDic where optType in (" + inStr + ")";
		
		if(optTypeStr.contains(SysConstants.BsDicConstant.STD_SY_COM_SURVEY_TYPE)) {
			if(curUser.getRoleNo().contains("0009")) {
				hsql = "from SysDic where optType in (" + inStr + ") and sdId='5306'";
			}
		}
		
		List<SysDic> list = sysDicDAO.select(hsql);
		
		Map<String, Object> map = new HashMap<String, Object>();
		for(SysDic dic : list){
			String optType = dic.getOptType();
			List<SysDic> temp = (List<SysDic>) map.get(optType);
			if(temp == null){
				temp = new ArrayList<SysDic>();
				map.put(optType, temp);
			}
			temp.add(dic);
		}
		
		return JSONObject.fromObject(map).toString();
	}
	
	public Map<String, Object> queryMapByTypes(String[] optTypes) throws Exception {
		
		String types = "";
		for(int i=0;i<optTypes.length;i++){
			types += ",'" + optTypes[i] + "'";
		}
		
		String inStr = types.substring(1);
		
		String hsql = "from SysDic where optType in (" + inStr + ")";
		
		List<SysDic> list = sysDicDAO.select(hsql);
		
		Map<String, Object> result = new HashMap<String, Object>();
		for(SysDic dic : list){
			String optType = dic.getOptType();
			Map<String, String> dicMap = (Map<String, String>) result.get(optType);
			if(dicMap == null){
				dicMap = new HashMap<String, String>();
				result.put(optType, dicMap);
			}
			dicMap.put(dic.getEnName(), dic.getCnName());
		}
		
		return result;
	}

	@Override
	public List<SysDic> queryDicPaging(SysDic queryCondition,
			String order, int index, int count) throws Exception {
		
		String query = "from SysDic" + getQueryCondition(queryCondition);
		
		if (!StringUtil.isEmptyString(order))
			query += " order by " + order;
		
		return sysDicDAO.selectRange(query, index*count, count);
	}

	@Override
	public long queryDicCount(SysDic queryCondition) throws Exception {
		String query = "select count(*) from SysDic" + getQueryCondition(queryCondition);
		return sysDicDAO.selectCount(query);
	}

	private String getQueryCondition(SysDic queryCondition) throws Exception{
		
		StringBuffer hsql = new StringBuffer(" where 1=1");
		if (queryCondition == null) 
			return hsql.toString();
					
		if (!StringUtil.isEmptyString(queryCondition.getEnName())) {
			hsql.append(" and enName = '")
				.append(queryCondition.getEnName())
				.append("'");
		}
		
		if (!StringUtil.isEmptyString(queryCondition.getOptType())) {
			hsql.append(" and optType = '")
				.append(queryCondition.getOptType())
				.append("'");
		}
		
		if (!StringUtil.isEmptyString(queryCondition.getCnName())) {
			String name = URLDecoder.decode(queryCondition.getCnName(), "UTF-8");
			hsql.append(" and cnName like '%")
				.append(name)
				.append("%'");
		}
		
		if (!StringUtil.isEmptyString(queryCondition.getMemo())) {
			String memo = URLDecoder.decode(queryCondition.getMemo(), "UTF-8");
			hsql.append(" and memo like '%")
				.append(memo)
				.append("%'");
		}
		
		return hsql.toString();
	}
}
