package com.beawan.base.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.beawan.base.entity.TrainInfo;
import com.beawan.base.entity.User;
import com.beawan.base.dao.IUserDAO;
import com.beawan.base.dao.TrainInfoDAO;
import com.beawan.base.qbean.QUser;
import com.beawan.base.service.TrainInfoSV;
import com.beawan.common.Constants;
import com.platform.util.HttpUtil;

@Service("trainInfoSV")
public class TrainInfoSVImpl implements TrainInfoSV{

	@Resource
	private TrainInfoDAO trainInfoDAO;
	@Resource
	private IUserDAO userDAO;
	
	@Override
	public long queryUserCount(QUser queryCondition) throws Exception {

		return 0;
	}
	
	
	@Override
	public Map<String, Object> queryUserPaging(QUser queryCondition,String orderCondition, int index, int count) throws Exception {
		
		Map<String, Object> map = trainInfoDAO.queryAll(queryCondition, orderCondition, index, count);
		
		return map;
		
	}

	@Override
	public TrainInfo saveIllegalRecord(TrainInfo trainInfo ,MultipartFile file) {
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		HttpSession session = HttpUtil.getSession();
		User user = (User) session
				.getAttribute(Constants.SessionKey.SESSION_KEY_USER);
		User user2 = null;
		try {
			user2 = this.userDAO.getUserByName(trainInfo.getTrainPerson());
		} catch (Exception e) {
			e.printStackTrace();
		}
		InputStream ins = null;
		OutputStream os = null;
		String attachmentPath = null;
		try {
			ins = file.getInputStream();
			String fileName = file.getOriginalFilename();
			StringBuffer fname = new StringBuffer(fileName);
			int indexOf = fname.indexOf(".");
			fname.insert(indexOf, "_"+user2.getUserId());
			
			File fileDir = new File("D:\\BeawanDevelop\\workspace\\localFile");
			if(!fileDir.exists()){
				fileDir.mkdirs();
			}
			byte[] byt = new byte[1024];
			int len;
			os = new FileOutputStream(fileDir.getPath()+File.separator+fname.toString());
			while((len = ins.read(byt)) != -1){
				os.write(byt);
			}
			attachmentPath = fileDir.getPath()+File.separator+fname.toString();
			os.flush();
			os.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		} 

		
		trainInfo.setTrainerId(user2.getUserId());
		trainInfo.setRecordPerson(user.getUserName());
		trainInfo.setRecordTime(format.format(new Date()));
		trainInfo.setModPerson("");
		trainInfo.setModTime("");
		trainInfo.setAttachmentPath(attachmentPath);
		trainInfo.setState("1");
		
		return trainInfoDAO.saveIllegalRecord(trainInfo);
	}


	@Override
	public void deleteIllRecord(Long id) {
		
		TrainInfo trainInfo = this.trainInfoDAO.querySingle(id);
		trainInfo.setState("0");
		this.trainInfoDAO.deleteIllRecord(trainInfo);
		
		
	}


	@Override
	public TrainInfo viewIllRecord(Long id) {
		
		TrainInfo trainInfo = this.trainInfoDAO.querySingle(id);
		
		return trainInfo;
	}


	@Override
	public void updateInfo(TrainInfo trainInfoNew) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		TrainInfo trainInfoOld = this.trainInfoDAO.querySingle(trainInfoNew.getId());	
		
		User user1 = null;
		try {
			user1 = this.userDAO.getUserByName(trainInfoNew.getTrainPerson());
		} catch (Exception e) {
			e.printStackTrace();
		}
		trainInfoOld.setTrainerId(user1.getUserId());
		trainInfoOld.setTrainPerson(trainInfoNew.getTrainPerson());
		trainInfoOld.setTrainTime(trainInfoNew.getTrainTime());
		trainInfoOld.setTrainTheme(trainInfoNew.getTrainTheme());
		trainInfoOld.setTrainContent(trainInfoNew.getTrainContent());		
		HttpSession session = HttpUtil.getSession();
		User user = (User) session
				.getAttribute(Constants.SessionKey.SESSION_KEY_USER);
		trainInfoOld.setModPerson(user.getUserName());
		trainInfoOld.setModTime(format.format(new Date()));
         
         System.out.println("修改后的:"+trainInfoOld);
		
		this.trainInfoDAO.updtaInfo(trainInfoOld);
		
		
		
	}


	@Override
	public TrainInfo downloadAttachment(String id) {

		Long lid = Long.valueOf(id);
		TrainInfo trainInfo = this.trainInfoDAO.querySingle(lid);

		return trainInfo;	
	}
	
}
