package com.beawan.base.service.impl;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Service;

import com.beawan.base.entity.PromptMsg;
import com.beawan.base.dao.IPromptMsgDAO;
import com.beawan.base.service.IPromptMsgSV;
import com.platform.util.DateUtil;
import com.platform.util.StringUtil;

@Service("promptMsgSV")
public class PromptMsgSVImpl implements IPromptMsgSV {

	@Resource
	private IPromptMsgDAO promptMsgDAO;

	@Override
	public PromptMsg save(PromptMsg promptMsg) throws Exception {
		promptMsg.setUpdateTime(DateUtil.getNowTimestamp());
		return promptMsgDAO.saveOrUpdate(promptMsg);
	}

	@Override
	public void delete(PromptMsg promptMsg) throws Exception {
		promptMsgDAO.deleteEntity(promptMsg);
	}

	@Override
	public PromptMsg findByCode(String code) throws Exception {
		return promptMsgDAO.selectByPrimaryKey(code);
	}

	@Override
	public List<PromptMsg> findByCodes(String codes) throws Exception {

		String inStr = "'" + codes.replace(",", "','") + "'";

		String hsql = "code in (" + inStr + ")";

		return promptMsgDAO.select(hsql);
	}

	@Override
	public Map<String, PromptMsg> findMapByCodes(String[] codes)
			throws Exception {

		String codeStr = "";
		for (int i = 0; i < codes.length; i++) {
			codeStr += "," + codes[i];
		}

		List<PromptMsg> list = this.findByCodes(codeStr.substring(1));

		Map<String, PromptMsg> map = new HashMap<String, PromptMsg>();
		for (PromptMsg msg : list) {
			map.put(msg.getCode(), msg);
		}

		return map;
	}

	@Override
	public String findMapJsonByCodes(String[] codes) throws Exception {
		return JSONObject.fromObject(this.findMapByCodes(codes)).toString();
	}

	@Override
	public Map<String, Object> findPaging(PromptMsg queryCondition,
			String orderCondition, int pageIndex, int pageSize) throws Exception {
		
		StringBuffer listSql = new StringBuffer("1=1");
		StringBuffer totalSql = new StringBuffer("1=1");
		
		Map<String, Object> params = new HashMap<String, Object>();
		if (queryCondition != null) {
			
			if (!StringUtil.isEmptyString(queryCondition.getCode())) {
				listSql.append(" and code=:code");
				totalSql.append(" and code=:code");
				params.put("code", queryCondition.getCode());
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getName())) {
				listSql.append(" and name like:name");
				totalSql.append(" and name like:name");
				String name = URLDecoder.decode(queryCondition.getName(), "UTF-8");
				params.put("name", "%" + name + "%");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getBusinessNo())) {
				listSql.append(" and businessNo=:businessNo");
				totalSql.append(" and businessNo=:businessNo");
				params.put("businessNo", queryCondition.getBusinessNo());
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getCategory())) {
				listSql.append(" and category=:category");
				totalSql.append(" and category=:category");
				params.put("category", queryCondition.getCategory());
			}
		}
		
		if (!StringUtil.isEmptyString(orderCondition))
			listSql.append(" order by ").append(orderCondition);
		else
			listSql.append(" order by updateTime desc");
		
		Map<String, Object> dataMap = new HashMap<String, Object>();
		int startIndex = pageIndex*pageSize;
		List<PromptMsg> list = promptMsgDAO.selectRange(listSql.toString(), startIndex, pageSize, params);
		
		long total = promptMsgDAO.selectCount(totalSql.toString(), params);
		dataMap.put("total", total);
		dataMap.put("rows", list);
		
		return dataMap;
	}

}
