package com.beawan.base.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.beawan.base.entity.SReportData;
import com.beawan.base.entity.SReportRecord;
import com.beawan.base.dao.ISReportRecordDAO;
import com.beawan.base.service.ISReportDataSV;
import com.beawan.base.service.ISReportRecordSV;
import com.platform.util.StringUtil;

@Service("sReportRecordSV")
public class SReportRecordSVImpl implements ISReportRecordSV {

	@Resource
	private ISReportRecordDAO sReportRecordDAO;
	
	@Resource
	private ISReportDataSV sReportDataSV;
	
	@Override
	public SReportRecord saveOrUpdate(SReportRecord sReportRecord) throws Exception {
		return sReportRecordDAO.saveOrUpdate(sReportRecord);
	}
	
	@Override
	public void delete(SReportRecord sReportRecord) throws Exception {
		
		List<SReportData> list = sReportDataSV.queryByReportNo(sReportRecord.getReportNo());
		if(!CollectionUtils.isEmpty(list)){
			for(SReportData data : list)
				sReportDataSV.delete(data);
		}
		
		sReportRecordDAO.deleteEntity(sReportRecord);
	}
	
	@Override
	public void deleteByNo(String reportNo) throws Exception {
		this.delete(this.queryByNo(reportNo));
	}
	
	@Override
	public SReportRecord queryByNo(String reportNo) throws Exception {
		return sReportRecordDAO.selectByPrimaryKey(reportNo);
	}

	@Override
	public SReportRecord queryUnique(String objectNo, String modelNo) throws Exception {
		String hql = "objectNo=:objectNo and modelNo=:modelNo";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("objectNo", objectNo);
		params.put("modelNo", modelNo);
		return sReportRecordDAO.selectSingle(hql, params);
	}
	
	@Override
	public List<SReportRecord> queryByObjectNo(String objectNo) throws Exception {
		String hql = "objectNo=:objectNo";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("objectNo", objectNo);
		return sReportRecordDAO.select(hql, params);
	}
	
	@Override
	public List<SReportRecord> queryByCondition(SReportRecord condition) throws Exception {
		
		StringBuffer sb = new StringBuffer("from SReportRecord where 1=1");
		
		if(!StringUtil.isEmpty(condition.getReportNo())){
			sb.append(" and reportNo='")
			  .append(condition.getReportNo())
			  .append("'");
		}
		
		if(!StringUtil.isEmpty(condition.getObjectNo())){
			sb.append(" and objectNo='")
			  .append(condition.getObjectNo())
			  .append("'");
		}
		
		if(!StringUtil.isEmpty(condition.getObjectType())){
			sb.append(" and objectType='")
			  .append(condition.getObjectType())
			  .append("'");
		}
		
		if(!StringUtil.isEmpty(condition.getValidFlag())){
			sb.append(" and validFlag='")
			  .append(condition.getValidFlag())
			  .append("'");
		}
		
		if(!StringUtil.isEmpty(condition.getModelNo())){
			sb.append(" and modelNo in ('")
			  .append(condition.getModelNo().replace(",", "','"))
			  .append("')");
		}
			
		return sReportRecordDAO.select(sb.toString());
	}
	
}
