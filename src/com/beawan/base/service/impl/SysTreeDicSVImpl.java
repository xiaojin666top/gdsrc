package com.beawan.base.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.base.dao.ISysTreeDicDAO;
import com.beawan.base.dao.impl.SysTreeDicDAOImpl;
import com.beawan.base.service.ISysTreeDicSV;
import com.beawan.library.bean.TreeData;
import com.platform.util.StringUtil;

@Service("sysTreeDicSV")
public class SysTreeDicSVImpl implements ISysTreeDicSV {

	@Resource
	private ISysTreeDicDAO sysTreeDicDAO;

	@Override
	public TreeData saveOrUpdate(TreeData treeData) {
		return sysTreeDicDAO.saveOrUpdate(treeData);
	}
	
	@Override
	public TreeData findByEnNameAndType(String enName, String optType) {
		return sysTreeDicDAO.geTreeDataByEnName(enName, optType);
	}

	@Override
	public List<TreeData> findByAbvenName(String abvenName, String optType) {
		return sysTreeDicDAO.getTreeDataAbvenName(abvenName, optType);
	}

	@Override
	public List<TreeData> findAll() {
		return sysTreeDicDAO.getAll();
	}

	@Override
	public String getFullNameByLocate(String locate, String optType){
		
		String fullName = "";
		String[] enNames = locate.split(",");
		
		int i = 0;
		if(enNames.length > 2)
			i = 1;
		
		for(;i<enNames.length;i++){
			TreeData treeData = sysTreeDicDAO.geTreeDataByEnName(enNames[i], optType);
			fullName = fullName + " > " + treeData.getCnName();
		}
		
		if(!fullName.equals(""))
			fullName = fullName.substring(2);
		
		return fullName;
	}
	
	@Override
	public String getFullNameByEnName(String enName, String optType){
		
		if(StringUtil.isEmptyString(enName) || StringUtil.isEmptyString(optType))
			return null;
		
		TreeData treeData = sysTreeDicDAO.geTreeDataByEnName(enName, optType);
		if(treeData == null)
			return null;
		else
			return getFullNameByLocate(treeData.getLocate(), optType);
	}
}
