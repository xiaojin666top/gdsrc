package com.beawan.base.service.impl;


import java.net.URLDecoder;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.base.entity.IndusPolicySet;
import com.beawan.base.entity.User;
import com.beawan.base.dao.IIndusPolicySetDAO;
import com.beawan.base.dao.ISysTreeDicDAO;
import com.beawan.base.service.IIndusPolicySetSV;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.library.bean.TreeData;
import com.platform.util.DateUtil;
import com.platform.util.HttpUtil;
import com.platform.util.JdbcUtil;
import com.platform.util.StringUtil;

@Service("indusPolicySetSV")
public class IndusPolicySetSVImpl implements IIndusPolicySetSV {

	@Resource
	private IIndusPolicySetDAO indusPolicySetDAO;
	
	@Resource
	private ISysTreeDicDAO sysTreeDicDAO;
	
	@Override
	public IndusPolicySet saveOrUpdate(IndusPolicySet indusPolicySet) throws Exception {
		
		User currUser = HttpUtil.getCurrentUser();
		String time = DateUtil.format(new Date(), Constants.DATETIME_MASK);
		if(indusPolicySet.getId() != null){
			IndusPolicySet ips = this.queryById(indusPolicySet.getId());
			ips.setIndusCode(indusPolicySet.getIndusCode());
			ips.setIndusPolicy(indusPolicySet.getIndusPolicy());
			ips.setPolicyType(indusPolicySet.getPolicyType());
			ips.setRemarks(indusPolicySet.getRemarks());
			ips.setStatus(indusPolicySet.getStatus());
			ips.setUpdateUser(currUser.getUserId());
			ips.setUpdateTime(time);
			indusPolicySet = ips;
		}else{
			indusPolicySet.setInputUser(currUser.getUserId());
			indusPolicySet.setInputTime(time);
		}
		
		return indusPolicySetDAO.saveOrUpdate(indusPolicySet);
	}
	
	@Override
	public IndusPolicySet queryById(long id) throws Exception {
		return indusPolicySetDAO.selectByPrimaryKey(id);
	}

	@Override
	public void delete(IndusPolicySet indusPolicySet) throws Exception {
		indusPolicySetDAO.deleteEntity(indusPolicySet);
	}
	
	@Override
	public void deleteById(long id) throws Exception {
		indusPolicySetDAO.deleteEntity(queryById(id));
	}
	
	@Override
	public List<Map<String,Object>> queryPaging(IndusPolicySet queryCondition,
			String orderCondition, int pageIndex, int pageSize) throws Exception {
		
		String sql = "SELECT T.*"
				   + " FROM ("
		           + " SELECT ROW_NUMBER() OVER() AS ROW_NUM,IPS.*,TD.CNNAME AS INDUS_NAME"
		           + " FROM GDTCESYS.DG_BS_INDUS_POLICY_SET IPS"
		           + " LEFT JOIN GDTCESYS.DG_BS_TREEDIC TD ON TD.ENNAME = IPS.INDUS_CODE"
		           + " AND TD.OPTTYPE='" + SysConstants.TreeDicConstant.GB_IC_4754_2017 + "'"
		           + " WHERE $"
		           + " ) T WHERE T.ROW_NUM > ? AND T.ROW_NUM <=?";
		
		String queryStr = genQueryStr(queryCondition);
		sql = sql.replace("$", queryStr);
		
		if (!StringUtil.isEmptyString(orderCondition))
			sql = sql.replace("OVER()", "OVER(ORDER BY " + orderCondition + ")");
		else
			sql = sql.replace("OVER()", "OVER(ORDER BY IPS.input_time)");
		int startIndex = pageIndex*pageSize;
		int endIndex = startIndex + pageSize;
		Object[] params = new Object[]{startIndex, endIndex};
		
		return JdbcUtil.query(Constants.DataSource.TCE_DS, sql, params);
	}

	@Override
	public long queryCount(IndusPolicySet queryCondition) throws Exception {
		
		String sql = "SELECT COUNT(*)"
				   + " FROM GDTCESYS.DG_BS_INDUS_POLICY_SET IPS"
		           + " LEFT JOIN GDTCESYS.DG_BS_TREEDIC TD ON TD.ENNAME = IPS.INDUS_CODE"
		           + " AND TD.OPTTYPE='" + SysConstants.TreeDicConstant.GB_IC_4754_2017 + "'"
		           + " WHERE $";
		
		String queryStr = genQueryStr(queryCondition);
		sql = sql.replace("$", queryStr);
		
		return JdbcUtil.queryForLong(Constants.DataSource.TCE_DS, sql, null);
	}

	
	private String genQueryStr(IndusPolicySet queryCondition) throws Exception {
		
		StringBuffer listSql = new StringBuffer("1=1");
		
		if (queryCondition != null) {
			
			if (!StringUtil.isEmptyString(queryCondition.getIndusCode())) {
				listSql.append(" AND IPS.INDUS_CODE = '")
				       .append(queryCondition.getIndusCode())
				       .append("'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getIndusName())) {
				String name = URLDecoder.decode(queryCondition.getIndusName(), "UTF-8");
				listSql.append(" AND TD.CNNAME LIKE '%")
			       .append(name)
			       .append("%'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getIndusPolicy())) {
				listSql.append(" AND IPS.INDUS_POLICY = '")
			       .append(queryCondition.getIndusPolicy())
			       .append("'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getPolicyType())) {
				listSql.append(" AND IPS.POLICY_TYPE = '")
			       .append(queryCondition.getPolicyType())
			       .append("'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getStatus())) {
				listSql.append(" AND IPS.STATUS = '")
			       .append(queryCondition.getStatus())
			       .append("'");
			}
			
		}
		
		return listSql.toString();
	}
	
	@Override
	public IndusPolicySet queryIndusPolicy(TreeData indusObj, String policyType) throws Exception {
		
		if(indusObj == null || indusObj.getLocate() == null)
			return null;
		
		IndusPolicySet ips = null;
		
		String[] codes = indusObj.getLocate().split(",");
		
		String hsql = "from IndusPolicySet where status = '1' and indusCode = ?"
				    + " and policyType = ? order by updateTime desc";
		
		for(int i=codes.length-1; i>0; i--){
			ips = indusPolicySetDAO.selectSingle(hsql, codes[i], policyType);
			if(ips != null)
				break;
		}
		
		return ips;
	}

	@Override
	public IndusPolicySet queryIndusPolicy(String industryCode, String policyType) throws Exception {
		
		String hsql = "from IndusPolicySet where status = '1' and indusCode = ?"
			    + " and policyType = ? order by updateTime desc";
		
		IndusPolicySet ips = indusPolicySetDAO.selectSingle(hsql, industryCode, policyType);
		if(ips == null){
			TreeData indusObj = sysTreeDicDAO.geTreeDataByEnName(industryCode,
					SysConstants.TreeDicConstant.GB_IC_4754_2017);
			ips = this.queryIndusPolicy(indusObj, policyType);
		}
		
		return ips;
	}
	
}
