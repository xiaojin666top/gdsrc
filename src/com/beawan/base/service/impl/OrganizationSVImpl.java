package com.beawan.base.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import com.platform.util.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.beawan.base.entity.Organization;
import com.beawan.base.entity.User;
import com.beawan.base.dao.IOrganizationDAO;
import com.beawan.base.dao.IUserDAO;
import com.beawan.base.dto.OrganizationDto;
import com.beawan.base.qbean.QOrganization;
import com.beawan.base.service.IOrganizationSV;
import com.beawan.common.Constants;
import com.beawan.core.Pagination;

@Service("organizationSV")
public class OrganizationSVImpl implements IOrganizationSV {
	
	@Resource
	private IOrganizationDAO organizationDAO;
	@Resource
	private IUserDAO userDAO;
	
	
	@Override
	public void saveOrUpdate(Organization organization) throws Exception {
		if(StringUtils.isEmpty(organization.getOrganno())){
			ExceptionUtil.throwException("机构编号不能为空！");
		}
		if(StringUtils.isEmpty(organization.getOrganname())){
			ExceptionUtil.throwException("机构名称不能为空！");
		}
		organizationDAO.saveOrUpdate(organization);
	}

	@Override
	public void update(Organization organization) throws Exception {
		organizationDAO.saveOrUpdate(organization);
	}

	@Override
	public void delete(Organization organization) throws Exception {
		List<User> managerList = userDAO.
				getUsers("from User as model where model.orgNo='"+organization.getOrganno()+"'", null);
		if(managerList.size()>0){
			ExceptionUtil.throwException("该机构下包含用户，不允许删除！");
		}
		List<Organization> orglist = organizationDAO.
				queryByCondition("from Organization as model where model.suporganno='"+organization.getOrganno()+"'");
		if(orglist.size()>0){
			ExceptionUtil.throwException("该机构有下属机构，不允许删除！");
		}
		organization.setStates(Constants.DELETE);
		organizationDAO.saveOrUpdate(organization);
	}

	@Override
	public Organization queryByNo(String no) throws Exception {
		return organizationDAO.queryByNo(no);
	}

	@Override
	public List<Organization> paginateQuery(QOrganization queryCondition,
			String sortCondition, int start, int limit) throws Exception {
		StringBuffer queryString = new StringBuffer("from Organization as model where 1=1");
		if(queryCondition!=null){
			if(!StringUtils.isEmpty(queryCondition.getOrgName())){
				String name = URLDecoder.decode(queryCondition.getOrgName(), "UTF-8");
				queryString.append(" and model.organname like '%")
					.append(name).append("%'");
			}
			if(!StringUtils.isEmpty(queryCondition.getOrgNo())){
				queryString.append(" and model.organno like '%")
					.append(queryCondition.getOrgNo()).append("%'");
			}
			if(!StringUtils.isEmpty(queryCondition.getParentNo())){
				queryString.append(" and model.suporganno ='")
					.append(queryCondition.getParentNo()).append("'");
			}
			if(!StringUtils.isEmpty(queryCondition.getStatus())){
				queryString.append(" and model.state =")
					.append(queryCondition.getStatus());
			}
		}
		queryString.append(" ORDER BY model.orgLvl");
		if(!StringUtil.isEmptyString(sortCondition)){
			queryString.append(" ,").append(sortCondition);
		}
		List<Organization> list = organizationDAO.queryPaginate(queryString.toString(), start, limit);
		
		return list;
	}

	@Override
	public int totalCount(QOrganization queryCondition) throws Exception {
		StringBuffer queryString = new StringBuffer("from Organization as model where 1=1");
		if(queryCondition!=null){
			if(!StringUtils.isEmpty(queryCondition.getOrgName())){
				String name = URLDecoder.decode(queryCondition.getOrgName(), "UTF-8");
				queryString.append(" and model.organname like '%")
					.append(name).append("%'");
			}
			if(!StringUtils.isEmpty(queryCondition.getOrgNo())){
				String no = new String(queryCondition.getOrgNo().getBytes("ISO8859-1"), "UTF-8");
				queryString.append(" and model.organno like '%")
					.append(no).append("%'");
			}
			if(!StringUtils.isEmpty(queryCondition.getParentNo())){
				queryString.append(" and model.suporganno ='")
					.append(queryCondition.getParentNo()).append("'");
			}
			if(!StringUtils.isEmpty(queryCondition.getStatus())){
				queryString.append(" and model.state =")
					.append(queryCondition.getStatus());
			}
		}
		List<Organization> list = organizationDAO.queryByCondition(queryString.toString());
		if(list.size()>0){
			return list.size();
		}else{
			return 0;
		}
	}

	@Override
	public List<Organization> queryByCondition(String queryString)
			throws Exception {
		List<Organization> list = organizationDAO.queryByCondition(queryString);
		return list;
	}

	@Override
	public List<Organization> queryBySuporganno(String suporganno)
			throws Exception {
		StringBuffer queryString = new StringBuffer("from Organization as model");
		queryString.append(" where model.suporganno='").append(suporganno).append("'");
		List<Organization> list = organizationDAO.queryByCondition(queryString.toString());
		return list;
	}

	@Override
	public Map<String, String> queryNoAndNameMap() throws Exception {
		List<Organization> list = organizationDAO.queryByCondition("from Organization as model");
		Map<String, String> map = new HashMap<String, String>();
		for (Organization org : list) {
			map.put(org.getOrganno(), org.getOrganshortform());
		}
		return map;
	}
	

	@Override
	public List<Map<String, Object>> queryCmisOrgPaging(
			QOrganization queryCondition, String orderCondition,
			int pageIndex, int pageSize) throws Exception {
		
		String sql = "SELECT T.*"
				   + " FROM ("
		           + " SELECT ROW_NUMBER() OVER() AS ROW_NUM,"
		           + " O.ORGID AS ORG_ID,O.ORGNAME AS ORG_NAME"
		           + " FROM CMIS.ORG_INFO O"
		           + " WHERE $"
		           + " ) T WHERE T.ROW_NUM > ? AND T.ROW_NUM <=?";
		
		String queryStr = genCmisQueryStr(queryCondition);
		sql = sql.replace("$", queryStr);
		int startIndex = pageIndex*pageSize;
		
		Object[] params = new Object[]{startIndex, startIndex+pageSize};
		
		return JdbcUtil.query(Constants.DataSource.TCE_DS, sql, params);
	}

	@Override
	public long queryCmisOrgCount(QOrganization queryCondition)
			throws Exception {
		
		String sql = "SELECT COUNT(*)"
				   + " FROM CMIS.ORG_INFO O"
				   + " WHERE $";
		
		String queryStr = genCmisQueryStr(queryCondition);
		sql = sql.replace("$", queryStr);
		
		return JdbcUtil.queryForLong(Constants.DataSource.TCE_DS, sql, null);
	}
	
	private String genCmisQueryStr(QOrganization queryCondition) throws Exception {
		
		StringBuffer listSql = new StringBuffer("1=1");
		
		if (queryCondition != null) {
			
			if (!StringUtil.isEmptyString(queryCondition.getOrgNo())) {
				listSql.append(" AND O.ORGID = '")
				       .append(queryCondition.getOrgNo())
				       .append("'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getOrgName())) {
				String name = URLDecoder.decode(queryCondition.getOrgName(), "UTF-8");
				listSql.append(" AND O.ORGNAME LIKE '%")
			       .append(name)
			       .append("%'");
			}
		}
		
		return listSql.toString();
	}

	@Override
	public void saveOrUpdateList(File file) throws Exception {
		// TODO Auto-generated method stub
		
		Sheet sheet = null;
		FileInputStream fis = new FileInputStream(file);
		if(file.getName().endsWith(".xls")){
			HSSFWorkbook wb = new HSSFWorkbook(fis);
			sheet = wb.getSheetAt(0);
		}else{
			XSSFWorkbook wb = new XSSFWorkbook(fis);
			sheet = wb.getSheetAt(0);
		}
		//第一行是标题   第二行是表头  
		//表头由左往右分别为  机构编号 机构名称  机构简称  机构等级  上级机构编号   机构法人  开办日期（2020/01/01）  联系电话 地址  邮编  营业执照  传真
//		List<Organization> orgList = new ArrayList<Organization>();
		int orgNum = sheet.getLastRowNum();
		for(int i = 2; i < orgNum; i++) {
			Organization dto = new Organization();
			Row loanInfoRow = sheet.getRow(i);
			System.out.println(i+"---->"+ExcelUtil.getStringValue(loanInfoRow.getCell(0)));
			dto.setOrganno(ExcelUtil.getStringValue(loanInfoRow.getCell(0)));
			dto.setOrganname(ExcelUtil.getCellValue(loanInfoRow.getCell(1)));
			dto.setOrganshortform(ExcelUtil.getStringValue(loanInfoRow.getCell(2)));
			dto.setOrgLvl(ExcelUtil.getStringValue(loanInfoRow.getCell(3)));
			dto.setSuporganno(ExcelUtil.getStringValue(loanInfoRow.getCell(4)));
			dto.setOrganchief(ExcelUtil.getCellValue(loanInfoRow.getCell(5)));
			dto.setLaunchdate(ExcelUtil.getStringValue(loanInfoRow.getCell(6)));
			dto.setTelnum(ExcelUtil.getCellValue(loanInfoRow.getCell(7)));
			dto.setLocate(ExcelUtil.getCellValue(loanInfoRow.getCell(8)));
			dto.setPostcode(ExcelUtil.getStringValue(loanInfoRow.getCell(9)));
			dto.setBsinsLic(ExcelUtil.getStringValue(loanInfoRow.getCell(10)));
			dto.setFax(ExcelUtil.getCellValue(loanInfoRow.getCell(11)));
			dto.setStates(Constants.NORMAL);

			organizationDAO.saveOrUpdate(dto);
		}
		
		
	}

	@Override
	public Pagination<OrganizationDto> paginateOrg(Class<OrganizationDto> clazz, String orgno, String orgName,
			String order, int page, int rows) {
		// TODO Auto-generated method stub
		Map<String,Object> params = new HashMap<String,Object>();
		StringBuilder sql = new StringBuilder("select a.organno organno,a.suporganno suporganno,a.organname organname,a.state state,");
		sql.append("a.organshortform organshortform,a.organchief organchief,a.org_lvl orgLvl,a.telnum telnum,b.organname suporganname");
		sql.append(" from BS_ORG a left join BS_ORG b on a.suporganno=b.organno where 1=1 and (a.STATE='0' or a.STATE='4') ");
		if(orgno!=null && !"".equals(orgno)) {
			sql.append(" and a.organno=:orgno");
			params.put("orgno", orgno);
		}
		if(orgName!=null && !"".equals(orgName)) {
			sql.append(" and a.organname like :orgName");
			params.put("orgName", "%"+orgName+"%");
		}
		sql.append("  order by a.org_lvl");
		if(order!=null && !"".equals(order)) {
			sql.append(","+order);
		}
		String countSql = "select count(*) from ("+sql+") a";
		Pagination<OrganizationDto> pagin = organizationDAO.findSqlPagination(OrganizationDto.class, sql.toString(), countSql, params, page, rows);
		
		return pagin;
	}

	@Override
	public void saveAndmergeOrgs(String orgnos, Organization orgInfo) throws Exception {
		// TODO Auto-generated method stub
		if(orgnos==null || "".equals(orgnos)) {
			return;
		}
		String[] orgnoArr = orgnos.split(",");
		for(String arr : orgnoArr) {
			Organization organ = organizationDAO.queryByNo(arr);
			organ.setStates(Constants.ORG_STATE.MERGED);
			organizationDAO.saveOrUpdate(organ);
		}
		orgInfo.setStates(Constants.ORG_STATE.NEW_MERGE);
		organizationDAO.saveOrUpdate(orgInfo);
	}

	@Override
	public List<OrganizationDto> queryChildTree(String suporganno) throws Exception {
		StringBuffer queryString = new StringBuffer("from Organization as model");
		queryString.append(" where model.suporganno='").append(suporganno).append("'");
		List<Organization> childs = organizationDAO.queryByCondition(queryString.toString());
		if(childs == null || childs.size() <= 0){
			return null;
		}
		List<OrganizationDto> dtos = MapperUtil.trans(childs, OrganizationDto.class);
		for(OrganizationDto child : dtos){
			child.setChildList(queryChildTree(child.getOrganno()));
		}
		return dtos;
	}
	
	
	
	

	@Override
	public Set<String> getChildList(Set<String> list, List<OrganizationDto> orgTreeChild) throws Exception {
		for (OrganizationDto org : orgTreeChild) {
			list.add(org.getOrganno());
			if (org.getChildList() != null && org.getChildList().size() >= 0) {
				getChildList(list, org.getChildList());
			}
		}
		return list;
	}

	@Override
	public Organization queryByOrgNo(String organno) throws Exception {
		StringBuffer queryString = new StringBuffer("from Organization as model");
		queryString.append(" where model.organno='").append(organno).append("'");
		List<Organization> childs = organizationDAO.queryByCondition(queryString.toString());
		if(childs == null || childs.size() <= 0){
			return null;
		}
		return childs.get(0);
	}
	
	
}











