package com.beawan.base.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.base.dao.InduInfoDao;
import com.beawan.base.entity.InduInfo;
import com.beawan.base.service.InduInfoService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.base.entity.InduInfo;

/**
 * @author yzj
 */
@Service("induInfoService")
public class InduInfoServiceImpl extends BaseServiceImpl<InduInfo> implements InduInfoService{
    /**
    * 注入DAO
    */
    @Resource(name = "induInfoDao")
    public void setDao(BaseDao<InduInfo> dao) {
        super.setDao(dao);
    }
    @Resource
    public InduInfoDao induInfoDao;
}
