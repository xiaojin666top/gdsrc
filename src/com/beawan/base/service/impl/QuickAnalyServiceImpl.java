package com.beawan.base.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.core.Pagination;
import com.beawan.corporateloan.entity.LeExaminePoint;
import com.beawan.base.dao.QuickAnalyDao;
import com.beawan.base.entity.QuickAnaly;
import com.beawan.base.entity.SysDic;
import com.beawan.base.service.QuickAnalyService;
import com.beawan.common.Constants;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import com.beawan.base.entity.QuickAnaly;

/**
 * @author yzj
 */
@Service("quickAnalyService")
public class QuickAnalyServiceImpl extends BaseServiceImpl<QuickAnaly> implements QuickAnalyService{
    /**
    * 注入DAO
    */
    @Resource(name = "quickAnalyDao")
    public void setDao(BaseDao<QuickAnaly> dao) {
        super.setDao(dao);
    }
    @Resource
    public QuickAnalyDao quickAnalyDao;
    
    
	@Override
	public Pagination<QuickAnaly> queryPageList(int page, int rows) {
		Map<String, Object> params = new HashMap<>();
        StringBuilder sql = new StringBuilder("select * from GDTCESYS.BS_QUICK_ANALY");
        sql.append(" where status=:status ");
		params.put("status", Constants.NORMAL);
        String count = "select count(1) from (" + sql + ")";
        Pagination<QuickAnaly> pager = quickAnalyDao.findCustSqlPagination(QuickAnaly.class,sql.toString(),count,params,page,rows);
        return pager;
	}

	@Override
	public Map<String, List<QuickAnaly>> queryAnalyByItem(String[] items) {
		String types = "";
		for(int i=0;i<items.length;i++){
			types += "," + items[i];
		}
		String optTypeStr = types.substring(1);
		String inStr = "'" + optTypeStr.replace(",", "','") + "'";
		
		String hsql = "from QuickAnaly where ITEMNAME in (" + inStr + ") and status='0'";
		List<QuickAnaly> list = quickAnalyDao.select(hsql);
		if(CollectionUtils.isEmpty(list)){
			return null;
		}
		Map<String, List<QuickAnaly>> collect = list.stream().collect(Collectors.groupingBy(QuickAnaly::getItemName));
		return collect;
	}
}
