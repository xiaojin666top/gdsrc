package com.beawan.base.service.impl;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.base.entity.Role;
import com.beawan.base.entity.RoleMenuRel;
import com.beawan.base.dao.IRoleDAO;
import com.beawan.base.dao.IRoleMenuRelDAO;
import com.beawan.base.service.IRoleSV;
import com.beawan.common.SysConstants;
import com.platform.util.StringUtil;

@Service("roleSV")
public class RoleSVImpl implements IRoleSV{
	@Resource
	private IRoleDAO roleDAO;
	@Resource
	private IRoleMenuRelDAO roleMenuMapDAO;

	@Override
	public void saveOrUpdate(Role role) throws Exception {
		roleDAO.saveOrUpdate(role);
	}
	
	@Override
	public void delete(String roleNo) throws Exception {
		Role role = roleDAO.queryByNo(roleNo);
		List<RoleMenuRel> mappings = roleMenuMapDAO.queryByRoleNo(roleNo);
		if(mappings != null){
			for (RoleMenuRel roleMenuMapping : mappings) {
				roleMenuMapDAO.delete(roleMenuMapping);
			}
		}
		roleDAO.delete(role);
	}
	
	@Override
	public List<Role> queryByCondition(String queryCondition) throws Exception {
		String sqlString = "from Role as model where 1=1";
		Map<String, Object> params = new HashMap<String, Object>();
		
		if(!StringUtil.isEmptyString(queryCondition)){
			String name = URLDecoder.decode(queryCondition, "UTF-8");
			sqlString += " and model.roleName  like:roleName";
			params.put("roleName","%" + name + "%");
		}
		return roleDAO.queryByCondition(sqlString, params);
	}

	@Override
	public Role queryByNo(String roleNo) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("roleNo",roleNo);
		List<Role> list = roleDAO.queryByCondition("from Role as model where model.roleNo=:roleNo",params);
		if(list.size()>0){
			return (Role)list.get(0);
		}else{
			return null;
		}
	}

	@Override
	public List<Role> query() throws Exception {
		String queryString = "from Role where roleNo <> '" 
						   + SysConstants.UserRole.SUPER_ADMIN + "'"
						   + " order by roleNo";
		return roleDAO.queryByCondition(queryString,null);
	}
	
	@Override
	public List<Role> queryAll() throws Exception {
		String queryString = "from Role";
		return roleDAO.queryByCondition(queryString,null);
	}

	@Override
	public List<Role> paginateQuery(Role queryCondition, String sortCondition,
			int pageIndex, int pageSize) throws Exception {
		StringBuffer sqlString = new StringBuffer("from Role as model where 1=1");
		Map<String, Object> params = new HashMap<String, Object>();
		
		if(!StringUtil.isEmptyString(queryCondition.getRoleNo())){
			String name = URLDecoder.decode(queryCondition.getRoleName(), "UTF-8");
			sqlString.append(" and model.roleName  like:roleName");
			params.put("roleName","%" + name + "%");
		}
		int startIndex = pageIndex*pageSize;
		List<Role> list = roleDAO.queryPaging(sqlString.toString(),params,startIndex, pageSize);
		return list;
	}

	@Override
	public int totalCount(Role queryCondition) throws Exception {
		StringBuffer sqlString = new StringBuffer("from Role as model where 1=1");
		Map<String, Object> params = new HashMap<String, Object>();
		
		if(!StringUtil.isEmptyString(queryCondition.getRoleNo())){
			String name = URLDecoder.decode(queryCondition.getRoleName(), "UTF-8");
			sqlString.append(" and model.roleName  like:roleName");
			params.put("roleName","%" + name + "%");
		}
		List<Role> list = roleDAO.queryByCondition(sqlString.toString(),params);
		return list.size();
	}


	@Override
	public Map<String, String> getRoleNoNameMap() throws Exception {
		Map<String, String> dataMap = new HashMap<String, String>();
		List<Role> list = this.queryAll();
		for (Role role : list) {
			dataMap.put(role.getRoleNo(), role.getRoleName());
		}
		return dataMap;
	}

	@Override
	public List<Role> queryListByNos(String roleNos) throws Exception {
		String[] roleArray = roleNos.split(",");
		String param = "";
		for(int i=0; i<roleArray.length; i++) {
			if(i == 0) {
				param += "(";
			}			
			param += "'" + roleArray[i] + "',";			
			if(i == roleArray.length -1) {
				param = param.substring(0,param.length()-1);
				param += ")";
			}
		}
		List<Role> list = roleDAO.queryByCondition("from Role as model where model.roleNo in" + param,null);

		return list;
	}
}
