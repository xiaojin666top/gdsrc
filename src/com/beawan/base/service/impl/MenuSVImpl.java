package com.beawan.base.service.impl;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.beawan.base.entity.Menu;
import com.beawan.base.dao.IMenuDAO;
import com.beawan.base.service.IMenuSV;
import com.platform.util.StringUtil;
@Service("menuSV")
public class MenuSVImpl implements IMenuSV {
	
	@Resource
	private IMenuDAO menuDAO;

	@Override
	public void saveOrUpdate(Menu menu) throws Exception {
		
		menuDAO.saveOrUpdate(menu);
		
		String parentNo = menu.getParentNo();
		if(!StringUtil.isEmptyString(parentNo)
				&& !"0".equals(parentNo)){
			Menu pmenu = menuDAO.queryByNo(parentNo);
			if(pmenu != null){
				pmenu.setMenuType("P");
				menuDAO.saveOrUpdate(pmenu);
			}
		}
	}
	
	@Override
	public void delete(Menu menu) throws Exception {
		menuDAO.delete(menu);
	}
	
	@Override
	public void deleteByMenuNo(String menuNo) throws Exception {
		Menu menu = menuDAO.queryByNo(menuNo);
		if(menu != null)
			recursDelete(menu);
	}
	/**
	 * 递归删除子菜单
	 * @author: xyh
	 * @date:   2020年5月26日 上午10:15:50   
	 * @param menu
	 * @throws Exception
	 */
	public void recursDelete(Menu menu) throws Exception{
		
		List<Menu> list = menuDAO.queryByParentNo(menu.getMenuNo());
		if(!CollectionUtils.isEmpty(list)){
			for(Menu m : list){
				recursDelete(m);
			}
		}
		
		menuDAO.delete(menu);
	}

	@Override
	public List<Menu> paginate(Menu queryCondition,
			String sortCondition, int start, int limit) throws Exception {
		StringBuffer queryString = new StringBuffer("from Menu as model where model.menuType='P'"); 
		List<Object> params = new ArrayList<Object>();
		if(queryCondition!=null){
			if(!StringUtil.isEmptyString(queryCondition.getMenuNo())){
				queryString.append(" and model.menuNo like ?");
				params.add("%"+queryCondition.getMenuNo()+"%");
			}
			if(!StringUtil.isEmptyString(queryCondition.getMenuName())){
				String name = URLDecoder.decode(queryCondition.getMenuName(), "UTF-8");
				queryString.append(" and model.menuName like ?");
				params.add("%" + name + "%");
			}
		}
		if(!StringUtil.isEmptyString(sortCondition)){
			queryString.append(" order by ").append(sortCondition);
		}
		int startIndex=start*limit;
		
		List<Menu> list = menuDAO.queryPaginate(queryString.toString(), params.toArray(),startIndex, limit);
		return list;
	}

	@Override
	public int totalCount(Menu queryCondition) throws Exception {
		StringBuffer queryString = new StringBuffer("from Menu as model where model.menuType='P'"); 
		List<Object> params = new ArrayList<Object>();
		if(queryCondition!=null){
			if(!StringUtil.isEmptyString(queryCondition.getMenuNo())){
				queryString.append(" and model.menuNo like ?");
				params.add("%"+queryCondition.getMenuNo()+"%");
			}
			if(!StringUtil.isEmptyString(queryCondition.getMenuName())){
				String name = URLDecoder.decode(queryCondition.getMenuName(), "UTF-8");
				queryString.append(" and model.menuName like ?");
				params.add("%" + name + "%");
			}
		}
		List<Menu> list = menuDAO.queryCondition(queryString.toString(),params.toArray());
		if(!CollectionUtils.isEmpty(list)){
			return list.size();
		}else{
			return 0;
		}
		
		
	}

	@Override
	public Menu queryByNo(String menuNo) throws Exception {
		return menuDAO.queryByNo(menuNo);
	}

	@Override
	public List<Menu> queryChildrenMenu(String parentNo) throws Exception {
		return menuDAO.queryByParentNo(parentNo);
	}

	@Override
	public List<Menu> queryTreeMenu(String parentNo) throws Exception {
		List<Menu> childMenus = menuDAO.queryByParentNo(parentNo);
		if(childMenus == null || childMenus.size() <= 0){
			return null;
		}
		for(Menu menu: childMenus){
			menu.setChildMenuList(queryTreeMenu(menu.getMenuNo()));
		}
		return childMenus;
		
		/*List<Menu> childMenus = menuDAO.queryByParentNo(parentNo);
		if(childMenus != null && childMenus.size() > 0){
			for(Menu menu: childMenus){
				dsfChildTree(menu);
			}
		}
		return childMenus;*/
	}
	/**
	 * 递归查询树形menu
	 * @param parentNo
	 */
	/*private void dsfChildTree(Menu parentMenu) throws Exception{
		List<Menu> childMenus = new ArrayList<Menu>();
		childMenus =  menuDAO.queryByParentNo(parentMenu.getMenuNo());
		if(childMenus == null || childMenus.size() <= 0){
			return;
		}
		for(Menu menu: childMenus){
			dsfChildTree(menu);
		}
		parentMenu.setChildMenuList(childMenus);
	}*/

}
