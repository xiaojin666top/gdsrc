package com.beawan.base.service.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.beawan.base.entity.SReportModel;
import com.beawan.base.entity.SReportModelRow;
import com.beawan.base.service.ISReportModelSV;
import com.beawan.common.Constants;
import com.platform.util.JdbcUtil;

@Service("sReportModelSV")
public class SReportModelSVImpl implements ISReportModelSV {
	
	private static final String MODEL_SQL_TEMP = " C.MODELNO AS MODEL_NO,"
					    + "C.MODELNAME AS MODEL_NAME,"
					    + "C.MODELTYPE AS MODEL_TYPE,"
					    + "C.MODELDESCRIBE AS MODEL_DESCRIBE,"
					    + "C.MODELABBR AS MODEL_ABBR,"
					    + "C.MODELCLASS AS MODEL_CLASS,"
					    + "C.ATTRIBUTE1,ATTRIBUTE1,"
					    + "C.DISPLAYMETHOD AS DISPLAY_METHOD,"
					    + "C.HEADERMETHOD AS HEADER_METHOD,"
					    + "C.DELETEFLAG AS DELETE_FLAG,"
					    + "C.REMARK "
					    + " FROM CMIS.REPORT_CATALOG C ";
	
	private static final String MODEL_ROW_SQL_TEMP = " M.MODELNO AS MODEL_NO,"
					    + "M.ROWNO AS ROW_NO,"
					    + "M.ROWNAME AS ROW_NAME,"
					    + "M.ROWSUBJECT AS ROW_SUBJECT,"
					    + "M.DISPLAYORDER AS DISPLAY_ORDER,"
					    + "M.ROWDIMTYPE AS ROW_DIM_TYPE,"
					    + "M.ROWATTRIBUTE AS ROW_ATTRIBUTE,"
					    + "M.COL1DEF AS COL1_DEF,"
					    + "M.COL2DEF AS COL2_DEF,"
					    + "M.COL3DEF AS COL3_DEF,"
					    + "M.COL4DEF AS COL4_DEF,"
					    + "M.STANDARDVALUE AS STANDARD_VALUE,"
					    + "M.DELETEFLAG AS DELETE_FLAG,"
					    + "M.FORMULAEXP1 AS FORMULA_EXP1,"
					    + "M.FORMULAEXP2 AS FORMULA_EXP2"
					    + " FROM CMIS.REPORT_MODEL M ";


	@Override
	public SReportModel queryModelByNo(String modelNo) throws Exception {
		
		String sql = "SELECT" + MODEL_SQL_TEMP + "WHERE C.MODELNO = '" + modelNo + "'";
		
		 List<SReportModel> list = JdbcUtil.executeQuery(sql, 
				 Constants.DataSource.TCE_DS, SReportModel.class);
		 
		 if(!CollectionUtils.isEmpty(list))
			 return list.get(0);
		 else
			 return null;
	}
	
	@Override
	public List<SReportModel> queryModelByClass(String modelClass) throws Exception {
		
		String sql = "SELECT" + MODEL_SQL_TEMP + "WHERE C.MODELCLASS = '" + modelClass + "'";
		return JdbcUtil.executeQuery(sql, 
				 Constants.DataSource.TCE_DS, SReportModel.class);
	}
	
	@Override
	public String queryModelNameByNo(String modelNo) throws Exception {
		
		String sql = "SELECT MODELNAME FROM CMIS.REPORT_CATALOG WHERE MODELNO = '" + modelNo + "'";
		
		String modelName =  JdbcUtil.queryForString(Constants.DataSource.TCE_DS, sql, null);
		 
		return modelName;
	}
	
	@Override
	public List<SReportModelRow> queryModelRowByNo(String modelNo) throws Exception {
		
		String sql = "SELECT" + MODEL_ROW_SQL_TEMP 
				   + " WHERE M.MODELNO = '" + modelNo + "'"
				   + " ORDER BY M.DISPLAYORDER ASC";
		
		 return JdbcUtil.executeQuery(sql, Constants.DataSource.TCE_DS, SReportModelRow.class);
	}
	
}
