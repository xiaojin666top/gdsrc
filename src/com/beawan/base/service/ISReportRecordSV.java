package com.beawan.base.service;

import java.util.List;

import com.beawan.base.entity.SReportRecord;


public interface ISReportRecordSV {
	
	/**
	 * TODO 保存
	 * @param sReportRecord
	 * @return
	 * @throws Exception
	 */
	public SReportRecord saveOrUpdate(SReportRecord sReportRecord) throws Exception;
	
	/**
	 * TODO 删除
	 * @param sReportRecord
	 * @throws Exception
	 */
	public void delete(SReportRecord sReportRecord) throws Exception;
	
	/**
	 * TODO 根据报告编号删除
	 * @param reportNo 报告编号
	 * @throws Exception
	 */
	public void deleteByNo(String reportNo) throws Exception;

	/**
	 * TODO 报表记录查询
	 * @param reportNo 报表编号
	 * @return
	 * @throws Exception
	 */
	public SReportRecord queryByNo(String reportNo) throws Exception;
	
	/**
	 * TODO 报表记录查询
	 * @param objectNo 关联对象编号
	 * @param modelNo 模型编号
	 * @return
	 * @throws Exception
	 */
	public SReportRecord queryUnique(String objectNo, String modelNo) throws Exception;
	
	/**
	 * TODO 报表记录查询
	 * @param objectNo 关联对象编号
	 * @return
	 * @throws Exception
	 */
	public List<SReportRecord> queryByObjectNo(String objectNo) throws Exception;
	
	public List<SReportRecord> queryByCondition(SReportRecord condition) throws Exception;

}
