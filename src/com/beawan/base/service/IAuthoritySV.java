package com.beawan.base.service;

import java.util.List;

import com.beawan.base.entity.RoleMenuRel;
import com.beawan.base.entity.Menu;
/**
 * 权限管理逻辑层接口
 * @author beawan_fengjj
 *
 */
public interface IAuthoritySV {
	/**
	 * 查询所有菜单项
	 * @return
	 * @throws Exception
	 */
	public List<Menu> allMenuList()throws Exception;
	/**
	 * 保存角色与菜单映射信息
	 * @param roleNo
	 * @param menuNoStr
	 * @throws Exception
	 */
	public void saveOrUpdateMap(String roleNo,String menuNoStr)throws Exception;
	/**
	 * 根据角色代码查询菜单映射配置信息
	 * @param roleNo
	 * @return
	 * @throws Exception
	 */
	public List<RoleMenuRel> queryMapByRoleNo(String roleNo)throws Exception;
	/**
	 * 根据角色代码查询主菜单项，包括子菜单（支持多角色）
	 * @param roleNo
	 * @return
	 * @throws Exception
	 */
	public List<Menu> queryPrimaryMenuByRoleNo(String roleNo)throws Exception;
	/**
	 * 根据角色代码和菜单代码查询子菜单信息
	 * @param roleNo
	 * @param menuNo
	 * @return
	 * @throws Exception
	 */
	public Menu querySecondaryMenuByRoleNoAndMenuNo(String roleNo,
			String menuNo)throws Exception;
	
}
