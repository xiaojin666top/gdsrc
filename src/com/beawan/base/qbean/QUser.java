package com.beawan.base.qbean;

import java.io.Serializable;
import java.util.Set;

public class QUser implements Serializable {

	private String userId; // 用户编号
	private String userName; // 用户姓名
	private String orgNo; // 机构号（支行）
	private String orgName; // 机构名称
	private String roleNo; // 角色编号，多角色用逗号分隔
	private String businessLine; // 业务条线，全部0、对公业务1、个人业务2
	private String status; // 状态，N正常、L锁定、C注销
	private String sex; // 性别
	private String birthday; // 生日
	private String signState; // 签到状态

	private String startTime; // 查询起始时间
	private String endTime; // 查询结束时间
	
	private Set<String> orgChilds;//查询的子机构

	/** default constructor */
	public QUser() {
	}

	/** minimal constructor */
	public QUser(String userId) {
		this.userId = userId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOrgNo() {
		return orgNo;
	}

	public void setOrgNo(String orgNo) {
		this.orgNo = orgNo;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getRoleNo() {
		return roleNo;
	}

	public void setRoleNo(String roleNo) {
		this.roleNo = roleNo;
	}

	public String getBusinessLine() {
		return businessLine;
	}

	public void setBusinessLine(String businessLine) {
		this.businessLine = businessLine;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getSignState() {
		return signState;
	}

	public void setSignState(String signState) {
		this.signState = signState;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Set<String> getOrgChilds() {
		return orgChilds;
	}

	public void setOrgChilds(Set<String> orgChilds) {
		this.orgChilds = orgChilds;
	}

}