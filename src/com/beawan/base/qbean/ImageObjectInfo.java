package com.beawan.base.qbean;

import java.util.List;

/**
 * 贷款业务涉及的对象信息类（借款人、保证人、质押物 、抵押物）
 * @author feng-pc
 *
 */
public class ImageObjectInfo {
	
	private String objectName;//对象名称
	
	private String objectNo;//对象编号
	
	private String objectType;//对象类型
	
	private String businType;//业务类型
	
	private int totalImages;//图片数量
	
	List<ImageModel> imageModelList;

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public String getObjectNo() {
		return objectNo;
	}

	public void setObjectNo(String objectNo) {
		this.objectNo = objectNo;
	}

	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getBusinType() {
		return businType;
	}

	public void setBusinType(String businType) {
		this.businType = businType;
	}

	public int getTotalImages() {
		return totalImages;
	}

	public void setTotalImages(int totalImages) {
		this.totalImages = totalImages;
	}

	public List<ImageModel> getImageModelList() {
		return imageModelList;
	}

	public void setImageModelList(List<ImageModel> imageModelList) {
		this.imageModelList = imageModelList;
	}
	
}
