package com.beawan.base.qbean;

import java.util.Map;


public class ImageModel {

	private String groupName;
	
	private String groupCode;
	
	private Map<String, String> moduleMap;
	
	private int totalImages;
	
	private int takedImages;
	
	private String typeFlag;

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public Map<String, String> getModuleMap() {
		return moduleMap;
	}

	public void setModuleMap(Map<String, String> moduleMap) {
		this.moduleMap = moduleMap;
	}

	public int getTotalImages() {
		return totalImages;
	}

	public void setTotalImages(int totalImages) {
		this.totalImages = totalImages;
	}

	public int getTakedImages() {
		return takedImages;
	}

	public void setTakedImages(int takedImages) {
		this.takedImages = takedImages;
	}

	public String getTypeFlag() {
		return typeFlag;
	}

	public void setTypeFlag(String typeFlag) {
		this.typeFlag = typeFlag;
	}
	
	
}
