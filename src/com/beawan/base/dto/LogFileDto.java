package com.beawan.base.dto;

public class LogFileDto {

	private String fileName;// 日志文件名
	private long lastModified;// 上次修改时间
	private String modifiedTime;// 修改时间
	private String modifiedDate;// 修改日期
	private String filePath;// 文件路径
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public long getLastModified() {
		return lastModified;
	}
	public void setLastModified(long lastModified) {
		this.lastModified = lastModified;
	}
	public String getModifiedTime() {
		return modifiedTime;
	}
	public void setModifiedTime(String modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	@Override
	public String toString() {
		return "LogFileDto [fileName=" + fileName + ", lastModified=" + lastModified + ", modifiedTime=" + modifiedTime
				+ ", modifiedDate=" + modifiedDate + ", filePath=" + filePath + "]";
	}
	
}
