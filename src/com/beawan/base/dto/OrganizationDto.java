package com.beawan.base.dto;

import java.util.List;

import com.beawan.base.entity.Organization;

// default package



/**
 * 机构表
 */

public class OrganizationDto{

    // Fields    

     private String organno;//机构码
     private String suporganno;//上级机构码
     private String suporganname;//上级机构名称
     private String locate;//位置属性
     private String organname;//机构名称
     private String organshortform;//机构简称
     private String enname;//英文名，存放是否总部部室，为总部部室时设值为1否则为空
     private Long orderno;//序号
     private String distno;//地区编号
     private String launchdate;//开办日期
     private Long organlevel;//机构级别
     private String fincode;//金融代码
     private Long state;//状态
     private String organchief;//机构负责人
     private String telnum;//联系电话
     private String address;//地址
     private String postcode;//邮编
     private String control;//控制字
     private String artiOrganno;//所属法人机构码
     private String distname;//地区名称
     private String areaDevCateType;//地区发展分类
     private String bankSrrno;//大额行号
     private String bsinsLic;//营业执照
     
     private String orgLvl;//机构等级
     private String fax;//传真
     
     private List<OrganizationDto> childList;
     
     
    // Constructors

    /** default constructor */
    public OrganizationDto() {
    }
    
    public OrganizationDto(Organization org) {
    	this.organno = org.getOrganno();
    	this.organname = org.getOrganname();
    	this.suporganno = org.getSuporganno();
    	this.organshortform = org.getOrganshortform();
    	this.address = org.getAddress();
    	this.postcode = org.getPostcode();
    	this.launchdate = org.getLaunchdate();
    	this.bsinsLic = org.getBsinsLic();
    	this.orgLvl = org.getOrgLvl();
    	this.fax = org.getFax();
    	this.telnum = org.getTelnum();
    	this.organchief = org.getOrganchief();
    }

    /** full constructor */
    public OrganizationDto(String organno, String suporganno, String locate, String organname, String organshortform, String enname, Long orderno, String distno, String launchdate, Long organlevel, String fincode, Long state, String organchief, String telnum, String address, String postcode, String control, String artiOrganno, String distname, String areaDevCateType, String bankSrrno, String bsinsLic, String orgLvl, String fax) {
        this.organno = organno;
        this.suporganno = suporganno;
        this.locate = locate;
        this.organname = organname;
        this.organshortform = organshortform;
        this.enname = enname;
        this.orderno = orderno;
        this.distno = distno;
        this.launchdate = launchdate;
        this.organlevel = organlevel;
        this.fincode = fincode;
        this.state = state;
        this.organchief = organchief;
        this.telnum = telnum;
        this.address = address;
        this.postcode = postcode;
        this.control = control;
        this.artiOrganno = artiOrganno;
        this.distname = distname;
        this.areaDevCateType = areaDevCateType;
        this.bankSrrno = bankSrrno;
        this.bsinsLic = bsinsLic;
        this.orgLvl = orgLvl;
        this.fax = fax;
    }

   
    // Property accessors

    public String getOrganno() {
        return this.organno;
    }
    
    public void setOrganno(String organno) {
        this.organno = organno;
    }

    public String getSuporganno() {
        return this.suporganno;
    }
    
    public void setSuporganno(String suporganno) {
        this.suporganno = suporganno;
    }

    public String getLocate() {
        return this.locate;
    }
    
    public void setLocate(String locate) {
        this.locate = locate;
    }

    public String getOrganname() {
        return this.organname;
    }
    
    public void setOrganname(String organname) {
        this.organname = organname;
    }

    public String getOrganshortform() {
        return this.organshortform;
    }
    
    public void setOrganshortform(String organshortform) {
        this.organshortform = organshortform;
    }

    public String getEnname() {
        return this.enname;
    }
    
    public void setEnname(String enname) {
        this.enname = enname;
    }

    public Long getOrderno() {
        return this.orderno;
    }
    
    public void setOrderno(Long orderno) {
        this.orderno = orderno;
    }

    public String getDistno() {
        return this.distno;
    }
    
    public void setDistno(String distno) {
        this.distno = distno;
    }

    public String getLaunchdate() {
        return this.launchdate;
    }
    
    public void setLaunchdate(String launchdate) {
        this.launchdate = launchdate;
    }

    public Long getOrganlevel() {
        return this.organlevel;
    }
    
    public void setOrganlevel(Long organlevel) {
        this.organlevel = organlevel;
    }

    public String getFincode() {
        return this.fincode;
    }
    
    public void setFincode(String fincode) {
        this.fincode = fincode;
    }

    public Long getState() {
        return this.state;
    }
    
    public void setState(Long state) {
        this.state = state;
    }

    public String getOrganchief() {
        return this.organchief;
    }
    
    public void setOrganchief(String organchief) {
        this.organchief = organchief;
    }

    public String getTelnum() {
        return this.telnum;
    }
    
    public void setTelnum(String telnum) {
        this.telnum = telnum;
    }

    public String getAddress() {
        return this.address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostcode() {
        return this.postcode;
    }
    
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getControl() {
        return this.control;
    }
    
    public void setControl(String control) {
        this.control = control;
    }

    public String getArtiOrganno() {
        return this.artiOrganno;
    }
    
    public void setArtiOrganno(String artiOrganno) {
        this.artiOrganno = artiOrganno;
    }

    public String getDistname() {
        return this.distname;
    }
    
    public void setDistname(String distname) {
        this.distname = distname;
    }

    public String getAreaDevCateType() {
        return this.areaDevCateType;
    }
    
    public void setAreaDevCateType(String areaDevCateType) {
        this.areaDevCateType = areaDevCateType;
    }

    public String getBankSrrno() {
        return this.bankSrrno;
    }
    
    public void setBankSrrno(String bankSrrno) {
        this.bankSrrno = bankSrrno;
    }

    public String getBsinsLic() {
        return this.bsinsLic;
    }
    
    public void setBsinsLic(String bsinsLic) {
        this.bsinsLic = bsinsLic;
    }

    public String getOrgLvl() {
        return this.orgLvl;
    }
    
    public void setOrgLvl(String orgLvl) {
        this.orgLvl = orgLvl;
    }

    public String getFax() {
        return this.fax;
    }
    
    public void setFax(String fax) {
        this.fax = fax;
    }

	public String getSuporganname() {
		return suporganname;
	}

	public void setSuporganname(String suporganname) {
		this.suporganname = suporganname;
	}

    public List<OrganizationDto> getChildList() {
        return childList;
    }

    public void setChildList(List<OrganizationDto> childList) {
        this.childList = childList;
    }
}