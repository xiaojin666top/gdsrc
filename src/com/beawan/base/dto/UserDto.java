package com.beawan.base.dto;


import com.beawan.core.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @ClassName User
 * @Company 杭州碧湾信息技术有限公司
 */
public class UserDto extends BaseEntity implements Serializable{


    private String userId; //用户编号

    private String loginId; //登录名（不用）

    private String userName; //用户姓名
    private String password; //登录密码
    private String mgeOrgNo; //管理机构号（部门）
    private String accOrgNo; //账务机构号（支行）
	private String orgName;//机构名称
    private String orgLvl;//机构等级
    private String roleNo; //角色编号，多角色用逗号分隔
    private String businessLine; //业务条线，全部0、对公业务1、个人业务2
    private String state; //状态，N正常、L锁定、C注销
    private String phone; //手机号码
    private String email; //邮箱
    private String sex; //性别
    private String birthday; //生日
	private String certid;//身份证号码

    private String pwdExpiryDate; //密码失效日期
    private Integer wrongPinNum; //密码错误累计次数
    private String lastPassword; //上一次设置的密码
    private Integer conWrongPinNum; //密码连续输错次数
    private String allowWorkPlat; //允许的应用平台，MB:移动设备，PC:PC电脑，ALL：全部
    private String allowWorkSys; //允许的应用系统
    private String dataAccess; //数据访问权限，0全部数据、1所属机构下数据、2所属用户下数据

    private String signState; //签到状态
    private String inputOrg; //登记机构
    private String inputUser; //登记人编号
    private String inputTime; //登记时间
    private String updateUser; //更新人编号

    /** default constructor */
    public UserDto() {
    }

	/** minimal constructor */
    public UserDto(String userId) {
        this.userId = userId;
    }
    
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getMgeOrgNo() {
		return mgeOrgNo;
	}
	public void setMgeOrgNo(String mgeOrgNo) {
		this.mgeOrgNo = mgeOrgNo;
	}
	public String getAccOrgNo() {
		return accOrgNo;
	}
	public void setAccOrgNo(String accOrgNo) {
		this.accOrgNo = accOrgNo;
	}
	public String getRoleNo() {
		return roleNo;
	}
	public void setRoleNo(String roleNo) {
		this.roleNo = roleNo;
	}
	public String getBusinessLine() {
		return businessLine;
	}
	public void setBusinessLine(String businessLine) {
		this.businessLine = businessLine;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getPwdExpiryDate() {
		return pwdExpiryDate;
	}
	public void setPwdExpiryDate(String pwdExpiryDate) {
		this.pwdExpiryDate = pwdExpiryDate;
	}
	public Integer getWrongPinNum() {
		return wrongPinNum;
	}
	public void setWrongPinNum(Integer wrongPinNum) {
		this.wrongPinNum = wrongPinNum;
	}
	public String getLastPassword() {
		return lastPassword;
	}
	public void setLastPassword(String lastPassword) {
		this.lastPassword = lastPassword;
	}
	public Integer getConWrongPinNum() {
		return conWrongPinNum;
	}
	public void setConWrongPinNum(Integer conWrongPinNum) {
		this.conWrongPinNum = conWrongPinNum;
	}
	public String getAllowWorkPlat() {
		return allowWorkPlat;
	}
	public void setAllowWorkPlat(String allowWorkPlat) {
		this.allowWorkPlat = allowWorkPlat;
	}
	public String getAllowWorkSys() {
		return allowWorkSys;
	}
	public void setAllowWorkSys(String allowWorkSys) {
		this.allowWorkSys = allowWorkSys;
	}
	public String getDataAccess() {
		return dataAccess;
	}
	public void setDataAccess(String dataAccess) {
		this.dataAccess = dataAccess;
	}
	public String getSignState() {
		return signState;
	}
	public void setSignState(String signState) {
		this.signState = signState;
	}
	public String getInputOrg() {
		return inputOrg;
	}
	public void setInputOrg(String inputOrg) {
		this.inputOrg = inputOrg;
	}
	public String getInputUser() {
		return inputUser;
	}
	public void setInputUser(String inputUser) {
		this.inputUser = inputUser;
	}
	public String getInputTime() {
		return inputTime;
	}
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}

	public String getOrgLvl() {
		return orgLvl;
	}

	public void setOrgLvl(String orgLvl) {
		this.orgLvl = orgLvl;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCertid() {
		return certid;
	}

	public void setCertid(String certid) {
		this.certid = certid;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	@Override
	public String toString() {
		return "User{" +
				"userId='" + userId + '\'' +
				", loginId='" + loginId + '\'' +
				", userName='" + userName + '\'' +
				", password='" + password + '\'' +
				", mgeOrgNo='" + mgeOrgNo + '\'' +
				", accOrgNo='" + accOrgNo + '\'' +
				", orgLvl='" + orgLvl + '\'' +
				", roleNo='" + roleNo + '\'' +
				", businessLine='" + businessLine + '\'' +
				", state='" + state + '\'' +
				", phone='" + phone + '\'' +
				", email='" + email + '\'' +
				", sex='" + sex + '\'' +
				", birthday='" + birthday + '\'' +
				", certid='" + certid + '\'' +
				", pwdExpiryDate='" + pwdExpiryDate + '\'' +
				", wrongPinNum=" + wrongPinNum +
				", lastPassword='" + lastPassword + '\'' +
				", conWrongPinNum=" + conWrongPinNum +
				", allowWorkPlat='" + allowWorkPlat + '\'' +
				", allowWorkSys='" + allowWorkSys + '\'' +
				", dataAccess='" + dataAccess + '\'' +
				", signState='" + signState + '\'' +
				", inputOrg='" + inputOrg + '\'' +
				", inputUser='" + inputUser + '\'' +
				", inputTime='" + inputTime + '\'' +
				'}';
	}
}