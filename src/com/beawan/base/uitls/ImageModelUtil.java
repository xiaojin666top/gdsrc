package com.beawan.base.uitls;

import java.util.ArrayList;
import java.util.List;



import org.apache.commons.collections.CollectionUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.beawan.base.qbean.ImageModel;

public class ImageModelUtil {

	/**
	 * 转换模型
	 * @param modelName
	 * @return
	 * @throws Exception
	 */
	public static List<ImageModel> getImageModel(String modelName)throws Exception{
		
		List<ImageModel>  imageModelList = new ArrayList<ImageModel>();
		
		SAXReader saxReader = new SAXReader();
		Document document = saxReader.read(ImageModelUtil.class.getResourceAsStream("/biz/image_menu.xml"));
		// 获取根元素
        Element root = document.getRootElement();
        // 获取所有子元素
        Element modelElement = (Element)root
    			.selectSingleNode("//business[@value='" + modelName + "']");
        @SuppressWarnings("unchecked")
		List<Element> groupList = modelElement.elements("group");
        for (Element group : groupList) {
        	 @SuppressWarnings("unchecked")
			List<Element> subGroupList = group.elements("subGroup");
             if(!CollectionUtils.isEmpty(subGroupList)){
             	ImageModel imageModel = null;
             	for (Element subGroup : subGroupList) {
             		imageModel = new ImageModel();
             		imageModel.setGroupCode(subGroup.attributeValue("value"));
             		imageModel.setGroupName(subGroup.attributeValue("name"));
             		imageModelList.add(imageModel);
     			}
             		
             }
		}
       
		return imageModelList;
	}
	
}
