package com.beawan.base.uitls;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.beawan.survey.custInfo.bean.CompBaseEquity;
import com.beawan.survey.loanInfo.bean.CusLaw;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;









//启信宝获取数据工具类

public class QixinUtil {
   
	/**日志*/
	private  static  Logger logger=LogManager.getLogger(QixinUtil.class);
	
	private  static  String appkey="a0a01d53a236415699fb5f7f0788927f";
	private  static  String appkey2="a0a01d53a236415699fb5f7f0788927f";
	private  static  String realappkey="10ee8db7932640de8aec7814a1da317e";
	/**
	 * 根据企业全名或注册号精确获取企业工商详细信息
	 * @param company 企业全名或企业注册号
	 * @return Map<String,Object>
	 */
	
	public static Map<String, Object> getDetailByName(String  company) {
		
		String result=HttpUtil.sendPost("http://api.qixin.com/APITestService/v2/enterprise/getDetailByName", "appkey="+appkey+"&keyword="+company);
		
		JSONObject jsonResult=JSONObject.fromObject(result);
		
		Map<String, Object> map=new HashMap<String,Object>();
		
		map=(Map<String, Object>)jsonResult.get("data");
			
		return map;
	}
	
	/**
	 * 根据企业全名或注册号或统一社会信用代码获取企业股东及出资信息 
	 * @param company 企业全名或企业注册号或统一社会信用代码
	 * @return Map<String,Object>
	 */
	public static List<CompBaseEquity> getStockholdersListByName(String customerNo,String  company) {
		
						
		String result=HttpUtil.sendPost("http://api.qixin.com/APITestService/enterprise/getStockholdersListByName", "appkey="+appkey+"&keyword="+company);
		JSONObject jsonResult=JSONObject.fromObject(result);		
		JSONObject items=JSONObject.fromObject(jsonResult.get("data"));
		JSONArray array=JSONArray.fromObject(items.get("items"));
		
		List<CompBaseEquity> equitlist=new ArrayList<CompBaseEquity>();
		
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				JSONObject item=JSONObject.fromObject(array.get(i));
				JSONArray shouldcapi=JSONArray.fromObject(item.get("should_capi_items"));
				JSONObject should=JSONObject.fromObject(shouldcapi.get(0));
				JSONArray realcapi=JSONArray.fromObject(item.get("real_capi_items"));
				JSONObject real=JSONObject.fromObject(realcapi.get(0));		
				CompBaseEquity baseEquity=new CompBaseEquity();
				baseEquity.setCustomerNo(customerNo);
				baseEquity.setCurrency("CNY");
				baseEquity.setStockName(item.getString("name"));
				String fundAmount=deChinese((String)should.get("shoud_capi"));
				baseEquity.setFundAmount(fundAmount);
				baseEquity.setFundWay((String)should.get("invest_type"));				
				String invtFactAmt=deChinese((String)real.get("real_capi"));
				baseEquity.setInvtFactAmt(invtFactAmt);
				equitlist.add(baseEquity);
			}
		}			
		return equitlist;
	}
	
	/***
	 * 按企业全名或注册号或统一社会信用代码返回裁判文书列表
	 * @param customerNo
	 * @param company 企业全名或注册号或统一社会信用代码
	 * @return
	 */
	public static List<CusLaw> getLawsuitListByName(String customerNo,String  company) {
		List<CusLaw> lawInfolist=new ArrayList<CusLaw>();
		String result=HttpUtil.sendPost("http://api.qixin.com/APITestService/lawsuit/getLawsuitListByName","appkey="+appkey+"&name="+company+"&skip=0");
		JSONObject jsonResult=JSONObject.fromObject(result);
		JSONObject items=JSONObject.fromObject(jsonResult.get("data"));
		JSONArray array=items.getJSONArray("items");

		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
			JSONObject item=JSONObject.fromObject(array.get(i));
			String id=item.getString("id");
			String lawitem=HttpUtil.sendPost("http://api.qixin.com/APITestService/lawsuit/getLawsuitDetail","id="+id+"&appkey="+appkey);			
			JSONObject jsonLaw=JSONObject.fromObject(lawitem);
			if(jsonLaw.getString("status").equals("200")) {
			CusLaw  lawInfo=new CusLaw(customerNo);
			JSONObject  lawData=jsonLaw.getJSONObject("data");
			lawInfo.setId(id);		
			lawInfo.setCaseNo(toUTF8(lawData.getString("case_no")));			
			lawInfo.setCaseCause(toUTF8(lawData.getString("case_cause")));
			lawInfo.setContent(toUTF8(lawData.getString("content")));
			lawInfo.setDate(lawData.getString("date"));
			lawInfo.setTitle(toUTF8(lawData.getString("title")));
			lawInfo.setType(toUTF8(lawData.getString("type")));
			lawInfolist.add(lawInfo);
			}
			}
		}
		
		return lawInfolist;
	}
	
	
	
	
	
	
	
	
	
	public static void main(String args[]) {
		
		
		
		getLawsuitListByName("111","小米科技有限责任公司");
		
	
	}
	
	
	
	/***
	 * 删除中文字符
	 * @param str
	 * @return
	 */
	public static String deChinese(String str) {
		
		String REGEX_CHINESE = "[\u4e00-\u9fa5]";// 中文正则
		
		// 去除中文
        Pattern pat = Pattern.compile(REGEX_CHINESE);
        Matcher mat = pat.matcher(str);
        str=mat.replaceAll("");
		
		return str;
	}
	
	public static String toUTF8(String str) {		
		try {
			if (str.equals(new String(str.getBytes("GBK"), "GBK"))) {
				str = new String(str.getBytes("GBK"), "UTF-8");
				System.out.println("3");
				return str;
			}
		} catch (Exception exception3) {
		}
		try {
			if (str.equals(new String(str.getBytes("GB2312"), "GB2312"))) {
				str = new String(str.getBytes("GB2312"), "UTF-8");
				System.out.println("1");
				return str;
			}
		} catch (Exception exception) {
		}
		try {
			if (str.equals(new String(str.getBytes("ISO-8859-1"), "ISO-8859-1"))) {
				str = new String(str.getBytes("ISO-8859-1"), "UTF-8");
				System.out.println("2");
				return str;
			}
		} catch (Exception exception1) {
		}
		
		
		return str;
	}


}
