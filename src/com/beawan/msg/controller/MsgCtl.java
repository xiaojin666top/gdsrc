package com.beawan.msg.controller;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.Pagination;
import com.beawan.core.ResultDto;
import com.beawan.msg.entity.TransactionMsg;
import com.beawan.msg.service.IMsgSV;

@Controller
@RequestMapping({ "/base/msg" })
@UserSessionAnnotation
public class MsgCtl extends BaseController{
	
	private static final Logger log = Logger.getLogger(MsgCtl.class);
	
	@Resource
	private IMsgSV msgSV;
	
	/***跳转到菜单管理页面***/
	@RequestMapping("msgMge.do")
	public String msgMge(HttpServletRequest request, HttpServletResponse response){
		return "views/base/msg/msgList";
	}
	
//	getMsgData.json
	@RequestMapping("getMsgData.json")
	@ResponseBody
	public ResultDto getMsgData(HttpServletRequest request, HttpServletResponse response,
			String msgId, String dealCode, @RequestParam(value="page",defaultValue="1")int page,
			@RequestParam(value="rows",defaultValue="10")int rows) {
		ResultDto re = returnFail("");
		try {
			
			Pagination<TransactionMsg> msgPager = msgSV.getMsgPag(msgId, dealCode, page, rows);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setRows(msgPager.getItems());
			re.setTotal(msgPager.getRowsCount());
		}catch(Exception e) {
			e.printStackTrace();
			log.error("获取报文信息异常", e);
		}
		return re;
	}
	
	@RequestMapping("viewMsgData.do")
	public ModelAndView viewMsgData(HttpServletRequest request, HttpServletResponse response, Long id) {
		ModelAndView mav = new ModelAndView("views/base/msg/msgView");
		try {
			TransactionMsg msgInfo = msgSV.getById(id);
			mav.addObject("msgInfo", msgInfo);
		}catch(Exception e) {
			e.printStackTrace();
			log.error("查看报文详情界面", e);
		}
		return mav;
	}
	
	
}
