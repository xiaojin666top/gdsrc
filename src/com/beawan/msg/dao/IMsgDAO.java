package com.beawan.msg.dao;

import com.beawan.core.BaseDao;
import com.beawan.msg.entity.TransactionMsg;

/**
 * 系统菜单持久层接口
 * @author beawan_fengjj
 *
 */
public interface IMsgDAO extends BaseDao<TransactionMsg>{
	

}
