package com.beawan.msg.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.msg.dao.IMsgDAO;
import com.beawan.msg.entity.TransactionMsg;
/**
 * 系统菜单持久层实现
 * @author beawan_fengjj
 *
 */
@Repository("msgDAO")
public class MsgDAOImpl extends BaseDaoImpl<TransactionMsg> implements IMsgDAO {


}
