package com.beawan.msg.dto;

/**
 * CusBase entity. @author MyEclipse Persistence Tools
 */

public class TransCusBase implements java.io.Serializable {

	// Fields

	private String customerNo;
	private String customerName;//客户名称
	private String customerType;//客户类型
	private String certType;//证件类型
	private String certCode;//证件号
	private String managerUserId;//管户人

	// Constructors

	/** default constructor */
	public TransCusBase() {
	}

	/** minimal constructor */
	public TransCusBase(String customerNo) {
		this.customerNo = customerNo;
	}


	public TransCusBase(String customerNo, String customerName, String customerType, String certType, String certCode,
			String managerUserId) {
		super();
		this.customerNo = customerNo;
		this.customerName = customerName;
		this.customerType = customerType;
		this.certType = certType;
		this.certCode = certCode;
		this.managerUserId = managerUserId;
	}

	// Property accessors

	public String getCustomerNo() {
		return this.customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerType() {
		return this.customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getCertType() {
		return this.certType;
	}

	public void setCertType(String certType) {
		this.certType = certType;
	}

	public String getCertCode() {
		return this.certCode;
	}

	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}


	public String getManagerUserId() {
		return this.managerUserId;
	}

	public void setManagerUserId(String managerUserId) {
		this.managerUserId = managerUserId;
	}




}