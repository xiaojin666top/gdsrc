package com.beawan.msg.util;

import org.apache.commons.lang.math.RandomUtils;

import com.beawan.core.ResultDto;
import com.beawan.msg.request.ReqBizHeader;
import com.beawan.msg.request.ReqBody;
import com.beawan.msg.request.ReqHeader;
import com.beawan.msg.request.ReqRequest;
import com.beawan.msg.request.ReqSysHeader;
import com.beawan.msg.request.ReqTransaction;
import com.beawan.msg.response.ResBizBodyData;
import com.beawan.msg.response.ResBizBodyStatus;
import com.beawan.msg.response.ResBizHeader;
import com.beawan.msg.response.ResBody;
import com.beawan.msg.response.ResHeader;
import com.beawan.msg.response.ResResponse;
import com.beawan.msg.response.ResSysHeader;
import com.beawan.msg.response.ResTransaction;
import com.beawan.msg.response.ResponseTransaction;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.platform.util.DateUtil;

/**
 * 用来封装行内msg接口的数据格式
 */
public class MsgUtil {

	/**
	 * 获取请求头中的交易码  用来分发请求
	 * @param requestMsg
	 * @return
	 */
	public static ReqBizHeader getDealCode(String requestMsg) {
		
		Gson gson = new Gson();
		JsonParser parser = new JsonParser();
		JsonElement TransactionStr = parser.parse(requestMsg)
				.getAsJsonObject().get("Transaction");
		JsonObject Header = TransactionStr.getAsJsonObject().get("Header").getAsJsonObject();
		ReqHeader reqHeader = gson.fromJson(Header, ReqHeader.class);
		
		JsonObject BodyJson = TransactionStr.getAsJsonObject().get("Body").getAsJsonObject();
		JsonObject requestJson = BodyJson.get("request").getAsJsonObject();
		ReqBizHeader bizHeader = gson.fromJson(requestJson.get("bizHeader").getAsJsonObject(), ReqBizHeader.class);
		return bizHeader;
	}
	
	/**
	 * 获得请求头中的固定格式
	 * @param resCode   请求结果
	 * @param resText	请求返回的消息
	 * @return
	 */
	public static ReqSysHeader getReqSysHeader(String resCode,String resText) {
		ReqSysHeader sysHeader = new ReqSysHeader();
		sysHeader.setResCode(resCode);
		sysHeader.setResText(resText);
		sysHeader.setMsgId(DateUtil.getNow()+RandomUtils.nextInt(10));
		sysHeader.setMsgDate(DateUtil.getNow());
		return sysHeader;
		
	}
	
	
	/**
	 * 解析msg中的数据内容，T根据不同的请求包，解析成不同对象
	 * @param <T>
	 * @param requestMsg
	 * @param clazz
	 * @return
	 */
	public static <T> ReqTransaction<T> getReqTrans(String requestMsg, Class<T> clazz) {

		ReqTransaction<T> transaction = new ReqTransaction<T>();
		
		ReqBody<T> Body = new ReqBody<T>();
		ReqRequest<T> request = new ReqRequest<T>();
		
		Gson gson = new Gson();
		JsonParser parser = new JsonParser();
		JsonElement TransactionStr = parser.parse(requestMsg)
				.getAsJsonObject().get("Transaction");
		JsonObject Header = TransactionStr.getAsJsonObject().get("Header").getAsJsonObject();
		ReqHeader reqHeader = gson.fromJson(Header, ReqHeader.class);
		
		JsonObject BodyJson = TransactionStr.getAsJsonObject().get("Body").getAsJsonObject();
		JsonObject requestJson = BodyJson.get("request").getAsJsonObject();
		ReqBizHeader bizHeader = gson.fromJson(requestJson.get("bizHeader").getAsJsonObject(), ReqBizHeader.class);
		request.setBizHeader(bizHeader);

		T bizBody = gson.fromJson(requestJson.get("bizBody").getAsJsonObject(), clazz);
		request.setBizBody(bizBody);
		
//		String requestJsonString = requestJson.toString();
//		request = gson.fromJson(requestJsonString, new TypeToken<ReqRequest<T>>() {}.getType());
				
		Body.setRequest(request);
		transaction.setHeader(reqHeader);
		transaction.setBody(Body);
		return transaction;
	}
	
	
	
	public static void setResBody(ResTransaction resTransaction) {
		Gson gson = new Gson();
	}
	
	public static ResponseTransaction resTrans(ReqSysHeader reqSysHeader,String resCode,String resText,String bizResCode,String bizResText) {
		ResponseTransaction trans = new ResponseTransaction();
		
		ResSysHeader resSysHeader = new ResSysHeader();
		if(reqSysHeader != null) {
			resSysHeader.setMsgId(reqSysHeader.getMsgId());
			resSysHeader.setMsgDate(reqSysHeader.getMsgDate());
			resSysHeader.setMsgTime(reqSysHeader.getMsgTime());
			resSysHeader.setServiceCd(reqSysHeader.getServiceCd());
			resSysHeader.setOperation(reqSysHeader.getOperation());
			resSysHeader.setClientCd(reqSysHeader.getClientCd());
			resSysHeader.setServerCd(reqSysHeader.getServerCd());
			resSysHeader.setBizId(reqSysHeader.getBizId());
			resSysHeader.setBizType(reqSysHeader.getBizType());
			resSysHeader.setOrgCode(reqSysHeader.getOrgCode());
			resSysHeader.setVer(reqSysHeader.getVer());
			resSysHeader.setAuthId(reqSysHeader.getAuthId());
			resSysHeader.setAuthPara(reqSysHeader.getAuthPara());
			resSysHeader.setAuthContext(reqSysHeader.getAuthContext());
			resSysHeader.setPinIndex(reqSysHeader.getPinIndex());
			resSysHeader.setPinValue(reqSysHeader.getPinValue());
		}
		//技术响应码
		resSysHeader.setResCode(resCode);
		//技术响应信息
		resSysHeader.setResText(resText);
		//业务响应码
		resSysHeader.setBizResCode(bizResCode);
		//业务响应信息
		resSysHeader.setBizResText(bizResText);
		
		ResTransaction resTransaction = new ResTransaction();
		ResHeader resHeader = new ResHeader();
		resHeader.setSysHeader(resSysHeader);
		resTransaction.setHeader(resHeader);

		ResBody resBody = new ResBody();
		ResResponse response = new ResResponse();
		ResBizHeader bizHeader = new ResBizHeader();
		ResBizBodyData bizBody = new ResBizBodyData();
		
		bizBody.setCode(resCode);
		bizBody.setMsg(resText);
		
		response.setBizHeader(bizHeader);
		response.setBizBody(bizBody);
		resBody.setResponse(response);
		resTransaction.setBody(resBody);
		trans.setTransaction(resTransaction);
		return trans;
		
	}
	
	
	
	public static ResponseTransaction<ResBizBodyStatus> resStatus(ReqSysHeader reqSysHeader,String resCode,String resText,String bizResCode,String bizResText) {
		ResponseTransaction<ResBizBodyStatus> trans = new ResponseTransaction<ResBizBodyStatus>();
		
		ResSysHeader resSysHeader = new ResSysHeader();
		if(reqSysHeader!=null) {
			resSysHeader.setMsgId(reqSysHeader.getMsgId());
			resSysHeader.setMsgDate(reqSysHeader.getMsgDate());
			resSysHeader.setMsgTime(reqSysHeader.getMsgTime());
			resSysHeader.setServiceCd(reqSysHeader.getServiceCd());
			resSysHeader.setOperation(reqSysHeader.getOperation());
			resSysHeader.setClientCd(reqSysHeader.getClientCd());
			resSysHeader.setServerCd(reqSysHeader.getServerCd());
			resSysHeader.setBizId(reqSysHeader.getBizId());
			resSysHeader.setBizType(reqSysHeader.getBizType());
			resSysHeader.setOrgCode(reqSysHeader.getOrgCode());
			resSysHeader.setVer(reqSysHeader.getVer());
			resSysHeader.setAuthId(reqSysHeader.getAuthId());
			resSysHeader.setAuthPara(reqSysHeader.getAuthPara());
			resSysHeader.setAuthContext(reqSysHeader.getAuthContext());
			resSysHeader.setPinIndex(reqSysHeader.getPinIndex());
			resSysHeader.setPinValue(reqSysHeader.getPinValue());
		}
		//技术响应码
		resSysHeader.setResCode(resCode);
		//技术响应信息
		resSysHeader.setResText(resText);
		//业务响应码
		resSysHeader.setBizResCode(bizResCode);
		//业务响应信息
		resSysHeader.setBizResText(bizResText);
		
		ResTransaction<ResBizBodyStatus> resTransaction = new ResTransaction<ResBizBodyStatus>();
		ResHeader resHeader = new ResHeader();
		resHeader.setSysHeader(resSysHeader);
		resTransaction.setHeader(resHeader);

		ResBody<ResBizBodyStatus> resBody = new ResBody<ResBizBodyStatus>();
		ResResponse<ResBizBodyStatus> response = new ResResponse<ResBizBodyStatus>();
		ResBizHeader bizHeader = new ResBizHeader();
		ResBizBodyStatus bizBody = new ResBizBodyStatus(Integer.parseInt(resCode), resText);
		
		bizBody.setCode(Integer.parseInt(resCode));
		bizBody.setMsg(resText);
		
		response.setBizHeader(bizHeader);
		response.setBizBody(bizBody);
		resBody.setResponse(response);
		resTransaction.setBody(resBody);
		trans.setTransaction(resTransaction);
		return trans;
		
	}
}
