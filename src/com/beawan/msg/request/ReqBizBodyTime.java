package com.beawan.msg.request;

public class ReqBizBodyTime {

	/** @Field @customerNo : (客户编号) */
	private String customerNo;
	
	/** @Field @customerNo : (全行统一核心客户号) */
	private String outCustNo;
	
	/** @Field @customerName : (客户名称) */
	private String customerName;
	
	/** @Field @industryCode : (行业代码) */
	private String industryCode;
	
	/** @Field @userHx : (核心客户经理号) */
	private String userHx;
	
	/** @Field @userHx : (核心客户经理名字) */
	private String userName;
	
	/** @Field @name : (报表年份) */
	private String reportYear;
	
	/** @Field @remark : (报表月份) */
	private String reportMonth;

	/** @Field @remark : (是否使用三方数据提供的财报数据) */
	private String isList;
	
	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getOutCustNo() {
		return outCustNo;
	}

	public void setOutCustNo(String outCustNo) {
		this.outCustNo = outCustNo;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public String getUserHx() {
		return userHx;
	}

	public void setUserHx(String userHx) {
		this.userHx = userHx;
	}

	public String getReportYear() {
		return reportYear;
	}

	public void setReportYear(String reportYear) {
		this.reportYear = reportYear;
	}

	public String getReportMonth() {
		return reportMonth;
	}

	public void setReportMonth(String reportMonth) {
		this.reportMonth = reportMonth;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getIsList() {
		return isList;
	}

	public void setIsList(String isList) {
		this.isList = isList;
	}
	
	


	
	
	
}
