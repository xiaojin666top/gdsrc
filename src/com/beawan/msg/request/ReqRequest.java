package com.beawan.msg.request;

public class ReqRequest<T> {

	private ReqBizHeader bizHeader;
	private T bizBody;
	
	
	public ReqBizHeader getBizHeader() {
		return bizHeader;
	}
	public void setBizHeader(ReqBizHeader bizHeader) {
		this.bizHeader = bizHeader;
	}
	public T getBizBody() {
		return bizBody;
	}
	public void setBizBody(T bizBody) {
		this.bizBody = bizBody;
	}
	
	

	
	
}
