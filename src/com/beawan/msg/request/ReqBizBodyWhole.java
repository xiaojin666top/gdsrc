package com.beawan.msg.request;

import java.util.List;


public class ReqBizBodyWhole {

	
	/** @Field @customerNo : (客户编号) */
	private String customerNo;

	/** @Field @customerNo : (全行统一核心客户号) */
	private String outCustNo;
	
	/** @Field @customerName : (客户名称) */
	private String customerName;

	/** @Field @customerName : (客户社会统一信用号) */
	private String creditNo;
	
	/** @Field @industryCode : (行业代码) */
	private String industryCode;
	
	/** @Field @name : (报表年份) */
	private String reportYear;
	
	/** @Field @remark : (报表月份) */
	private String reportMonth;

	/** @Field @userHx : (核心客户经理号) */
	private String userHx;

	/** @Field @userHx : (核心客户经理名字) */
	private String userName;
	
	/** @Field @reportRule : (新旧准则报表) */
	private String reportRule;
	

	
	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getOutCustNo() {
		return outCustNo;
	}

	public void setOutCustNo(String outCustNo) {
		this.outCustNo = outCustNo;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCreditNo() {
		return creditNo;
	}

	public void setCreditNo(String creditNo) {
		this.creditNo = creditNo;
	}

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public String getReportYear() {
		return reportYear;
	}

	public void setReportYear(String reportYear) {
		this.reportYear = reportYear;
	}

	public String getReportMonth() {
		return reportMonth;
	}

	public void setReportMonth(String reportMonth) {
		this.reportMonth = reportMonth;
	}

	public String getUserHx() {
		return userHx;
	}

	public void setUserHx(String userHx) {
		this.userHx = userHx;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getReportRule() {
		return reportRule;
	}

	public void setReportRule(String reportRule) {
		this.reportRule = reportRule;
	}
	
	
	
}
