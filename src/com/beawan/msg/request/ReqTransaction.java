package com.beawan.msg.request;

public class ReqTransaction<T> {

	
	private ReqHeader Header;
	private ReqBody<T> Body;
	public ReqHeader getHeader() {
		return Header;
	}
	public void setHeader(ReqHeader header) {
		Header = header;
	}
	public ReqBody<T> getBody() {
		return Body;
	}
	public void setBody(ReqBody<T> body) {
		Body = body;
	}
	
	
}
