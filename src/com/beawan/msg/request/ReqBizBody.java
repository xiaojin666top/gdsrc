package com.beawan.msg.request;

import java.util.List;


public class ReqBizBody {

	/** @Field @customerId : (客户id) */
	private Long customerId;
	
	/** @Field @customerNo : (客户编号) */
	private String customerNo;
	
	/** @Field @customerName : (客户名称) */
	private String customerName;
	
	/** @Field @industryCode : (行业代码) */
	private String industryCode;
	
	/** @Field @name : (报表年份) */
	private String reportYear;
	
	/** @Field @remark : (报表月份) */
	private String reportMonth;
	
	/** @Field @reportRule : (旧准则报表) */
	private String reportRule;
	
	/** @Field @reportType : (表类型，资产负债表是  balance 利润表  income 现金流量表 cashFlow ) */
	private String reportType;

	/** @Field @reportMould : (合并报表) */
	private String reportMould;
	
	/** @Field @reportUnit : (报表单位) */
	private String reportUnit;
	
	/** @Field @currency : (币种) */
	private String currency;
	
	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public String getReportYear() {
		return reportYear;
	}

	public void setReportYear(String reportYear) {
		this.reportYear = reportYear;
	}

	public String getReportMonth() {
		return reportMonth;
	}

	public void setReportMonth(String reportMonth) {
		this.reportMonth = reportMonth;
	}

	public String getReportRule() {
		return reportRule;
	}

	public void setReportRule(String reportRule) {
		this.reportRule = reportRule;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public String getReportMould() {
		return reportMould;
	}

	public void setReportMould(String reportMould) {
		this.reportMould = reportMould;
	}

	public String getReportUnit() {
		return reportUnit;
	}

	public void setReportUnit(String reportUnit) {
		this.reportUnit = reportUnit;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	
}
