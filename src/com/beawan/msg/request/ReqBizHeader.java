package com.beawan.msg.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReqBizHeader {
	
	@JsonProperty("SERVICE_ID")
	private String SERVICE_ID;
	
	@JsonProperty("REQ_SYS_NO")
	private String REQ_SYS_NO;
	
	@JsonProperty("REQ_TIME")
	private String REQ_TIME;
	
	@JsonProperty("DEALCODE")
	private String DEALCODE;

	@JsonProperty("SERVICE_ID")
	public String getSERVICE_ID() {
		return SERVICE_ID;
	}

	@JsonProperty("SERVICE_ID")
	public void setSERVICE_ID(String sERVICE_ID) {
		SERVICE_ID = sERVICE_ID;
	}

	@JsonProperty("REQ_SYS_NO")
	public String getREQ_SYS_NO() {
		return REQ_SYS_NO;
	}

	@JsonProperty("REQ_SYS_NO")
	public void setREQ_SYS_NO(String rEQ_SYS_NO) {
		REQ_SYS_NO = rEQ_SYS_NO;
	}
	
	@JsonProperty("REQ_TIME")
	public String getREQ_TIME() {
		return REQ_TIME;
	}

	@JsonProperty("REQ_TIME")
	public void setREQ_TIME(String rEQ_TIME) {
		REQ_TIME = rEQ_TIME;
	}

	@JsonProperty("DEALCODE")
	public String getDEALCODE() {
		return DEALCODE;
	}
	
	@JsonProperty("DEALCODE")
	public void setDEALCODE(String dEALCODE) {
		DEALCODE = dEALCODE;
	}


	
	
	
}
