package com.beawan.msg.request;

public class ReqBody<T> {

	private ReqRequest<T> request;

	public ReqRequest<T> getRequest() {
		return request;
	}

	public void setRequest(ReqRequest<T> request) {
		this.request = request;
	}

	
	
}
