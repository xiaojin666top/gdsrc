package com.beawan.msg.response;

public class ResResponse<T> {

	private ResBizHeader bizHeader;
	private T bizBody;
	
	
	public ResBizHeader getBizHeader() {
		return bizHeader;
	}
	public void setBizHeader(ResBizHeader bizHeader) {
		this.bizHeader = bizHeader;
	}
	public T getBizBody() {
		return bizBody;
	}
	public void setBizBody(T bizBody) {
		this.bizBody = bizBody;
	}
	
	

	
	
}
