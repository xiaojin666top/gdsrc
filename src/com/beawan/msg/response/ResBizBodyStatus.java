package com.beawan.msg.response;

public class ResBizBodyStatus {

	private int code;//0成功  1失败
	private String msg;
	
	
	
	public ResBizBodyStatus() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public ResBizBodyStatus(int code, String msg) {
		super();
		this.code = code;
		this.msg = msg;
	}


	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	
}
