package com.beawan.msg.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResTransaction<T> {


	@JsonProperty("Header")
	private ResHeader Header;
	
	@JsonProperty("Body")
	private ResBody<T> Body;
	

	@JsonProperty("Header")
	public ResHeader getHeader() {
		return Header;
	}
	@JsonProperty("Header")
	public void setHeader(ResHeader header) {
		Header = header;
	}

	@JsonProperty("Body")
	public ResBody<T> getBody() {
		return Body;
	}
	@JsonProperty("Body")
	public void setBody(ResBody<T> body) {
		Body = body;
	}
	
	
}
