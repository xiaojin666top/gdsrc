package com.beawan.msg.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseTransaction<T> {

	@JsonProperty("Transaction")
	private ResTransaction<T> Transaction;

	@JsonProperty("Transaction")
	public ResTransaction<T> getTransaction() {
		return Transaction;
	}

	@JsonProperty("Transaction")
	public void setTransaction(ResTransaction<T> transaction) {
		Transaction = transaction;
	}
	
	
}
