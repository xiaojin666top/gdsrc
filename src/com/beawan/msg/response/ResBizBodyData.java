package com.beawan.msg.response;

import java.util.List;


/**
 * 财务分析所有数据内容
 * @author user
 *
 */
public class ResBizBodyData {

	
	private String serNo;
	private Double riskScore;
	private String year;
	private String month;
	private String periods;
	
	private String conclusion;
	private String assetAnaly;
	private String debtAnaly;
	private String incomeAnaly;
	private String upCashFlow;
	private String lowCashFlow;
	private String ratioAnaly;
	private String cashFlowAnaly;
	
	private String code;//0成功 1失败
	private String msg;//信息描述
	
	public String getSerNo() {
		return serNo;
	}
	public void setSerNo(String serNo) {
		this.serNo = serNo;
	}
	public Double getRiskScore() {
		return riskScore;
	}
	public void setRiskScore(Double riskScore) {
		this.riskScore = riskScore;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getPeriods() {
		return periods;
	}
	public void setPeriods(String periods) {
		this.periods = periods;
	}
	public String getConclusion() {
		return conclusion;
	}
	public void setConclusion(String conclusion) {
		this.conclusion = conclusion;
	}
	public String getAssetAnaly() {
		return assetAnaly;
	}
	public void setAssetAnaly(String assetAnaly) {
		this.assetAnaly = assetAnaly;
	}
	public String getDebtAnaly() {
		return debtAnaly;
	}
	public void setDebtAnaly(String debtAnaly) {
		this.debtAnaly = debtAnaly;
	}
	public String getIncomeAnaly() {
		return incomeAnaly;
	}
	public void setIncomeAnaly(String incomeAnaly) {
		this.incomeAnaly = incomeAnaly;
	}
	public String getUpCashFlow() {
		return upCashFlow;
	}
	public void setUpCashFlow(String upCashFlow) {
		this.upCashFlow = upCashFlow;
	}
	public String getLowCashFlow() {
		return lowCashFlow;
	}
	public void setLowCashFlow(String lowCashFlow) {
		this.lowCashFlow = lowCashFlow;
	}
	public String getRatioAnaly() {
		return ratioAnaly;
	}
	public void setRatioAnaly(String ratioAnaly) {
		this.ratioAnaly = ratioAnaly;
	}
	public String getCashFlowAnaly() {
		return cashFlowAnaly;
	}
	public void setCashFlowAnaly(String cashFlowAnaly) {
		this.cashFlowAnaly = cashFlowAnaly;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	
	
	
	
	
	
	
}
