package com.beawan.msg.response;

public class ResBody<T> {

	private ResResponse<T> response;

	public ResResponse<T> getResponse() {
		return response;
	}

	public void setResponse(ResResponse<T> response) {
		this.response = response;
	}

	
	
}
