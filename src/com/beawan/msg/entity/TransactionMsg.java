package com.beawan.msg.entity;

import java.io.Serializable;
import java.sql.Timestamp;

public class TransactionMsg implements Serializable{
	
	private Long id;
	private String msgId;
	private String dealCode;//交易码
	private String dealName;//交易名称
	private String transType;//通讯方式
	private String dataType;//数据类型
	private String requestMsg;//请求报文
	private String responseMsg;//响应报文
	private String reqFrom;//请求源
	private Integer resultCode;//请求结果   0成功  1失败
	private String resultText;//请求结果说明
	private Timestamp requestTime;//请求时间
	
	
	
	public TransactionMsg() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public TransactionMsg(Long id, String msgId, String dealCode, String dealName, String transType, String dataType,
			String requestMsg, String responseMsg, String reqFrom, Integer resultCode, String resultText,
			Timestamp requestTime) {
		super();
		this.id = id;
		this.msgId = msgId;
		this.dealCode = dealCode;
		this.dealName = dealName;
		this.transType = transType;
		this.dataType = dataType;
		this.requestMsg = requestMsg;
		this.responseMsg = responseMsg;
		this.reqFrom = reqFrom;
		this.resultCode = resultCode;
		this.resultText = resultText;
		this.requestTime = requestTime;
	}

	public String getResultText() {
		return resultText;
	}
	public void setResultText(String resultText) {
		this.resultText = resultText;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDealCode() {
		return dealCode;
	}
	public void setDealCode(String dealCode) {
		this.dealCode = dealCode;
	}
	public String getDealName() {
		return dealName;
	}
	public void setDealName(String dealName) {
		this.dealName = dealName;
	}
	public String getTransType() {
		return transType;
	}
	public void setTransType(String transType) {
		this.transType = transType;
	}
	public String getDataType() {
		return dataType;
	}
	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
	public String getRequestMsg() {
		return requestMsg;
	}
	public void setRequestMsg(String requestMsg) {
		this.requestMsg = requestMsg;
	}
	public String getResponseMsg() {
		return responseMsg;
	}
	public void setResponseMsg(String responseMsg) {
		this.responseMsg = responseMsg;
	}
	public String getReqFrom() {
		return reqFrom;
	}
	public void setReqFrom(String reqFrom) {
		this.reqFrom = reqFrom;
	}
	public Integer getResultCode() {
		return resultCode;
	}
	public void setResultCode(Integer resultCode) {
		this.resultCode = resultCode;
	}
	public Timestamp getRequestTime() {
		return requestTime;
	}
	public void setRequestTime(Timestamp requestTime) {
		this.requestTime = requestTime;
	}
	public String getMsgId() {
		return msgId;
	}
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
	
	

}
