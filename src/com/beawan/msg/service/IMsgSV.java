package com.beawan.msg.service;

import com.beawan.core.Pagination;
import com.beawan.msg.entity.TransactionMsg;

public interface IMsgSV {
	
	
	void saveTranscationMsg(TransactionMsg msg);
	
	/**
	 * 查看接受报文详情
	 * @param transName
	 * @param transCode
	 * @param page
	 * @param rows
	 * @return
	 */
	public Pagination<TransactionMsg> getMsgPag(String msgId, String dealCode, int page, int rows);
	
	TransactionMsg getById(Long id);
	
}
