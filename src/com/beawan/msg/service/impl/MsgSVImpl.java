package com.beawan.msg.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.core.Pagination;
import com.beawan.msg.dao.IMsgDAO;
import com.beawan.msg.entity.TransactionMsg;
import com.beawan.msg.service.IMsgSV;

@Service("msgSV")
public class MsgSVImpl implements IMsgSV {
	
	@Resource
	private IMsgDAO msgDAO;

	@Override
	public Pagination<TransactionMsg> getMsgPag(String msgId, String dealCode, int page, int rows) {
		// TODO Auto-generated method stub
		
//		select ID id,MSG_ID msgId,DEAL_CODE dealCode,TRANS_TYPE transType,
//		REQUEST_MSG requestMsg, RESPONSE_MSG responseMsg,REQ_FROM reqFrom,
//		RESULT_CODE resultCode,REQUEST_TIME requestTime,DATA_TYPE dataType
//		 from DG_TX_MSG t
		
		Map<String,Object> params = new HashMap<String,Object>();
		StringBuilder sql = new StringBuilder("select ID id,MSG_ID msgId,DEAL_CODE dealCode,DEAL_NAME dealName,TRANS_TYPE transType,REQUEST_MSG requestMsg, ");
		sql.append("RESPONSE_MSG responseMsg,REQ_FROM reqFrom,RESULT_CODE resultCode,REQUEST_TIME requestTime,DATA_TYPE dataType,RESULT_TEXT resultText");
		sql.append(" from DG_TX_MSG t where 1=1 ");
		if(msgId!=null && !"".equals(msgId)) {
			sql.append(" and t.MSG_ID like :msgId");
			params.put("msgId", "%"+msgId+"%");
		}
		if(dealCode!=null && !"".equals(dealCode)) {
			sql.append(" and t.dealCode like :dealCode");
			params.put("DEAL_CODE", "%"+dealCode+"%");
		}
		String countSql = "select count(*) from ("+sql+") a";
		Pagination<TransactionMsg> pager = msgDAO.findSqlPagination(TransactionMsg.class, sql.toString(), countSql, params, page, rows);
		
		return pager;
		
	}

	@Override
	public void saveTranscationMsg(TransactionMsg msg) {
		// TODO Auto-generated method stub
		msgDAO.saveOrUpdate(msg);
	}

	@Override
	public TransactionMsg getById(Long id) {
		// TODO Auto-generated method stub
		return msgDAO.findByPrimaryKey(id);
	}


}
