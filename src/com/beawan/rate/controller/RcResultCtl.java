package com.beawan.rate.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.rate.entity.RcResult;
import com.beawan.rate.service.IRcQuotaSV;
import com.beawan.rate.service.RcResultService;

/**
 * 利率结果控制器
 * @author 86178
 *
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/model/rcResult" })
public class RcResultCtl extends BaseController {

	private static final Logger log = Logger.getLogger(RcResultCtl.class);

	@Resource
	private RcResultService rcResultService;

	@Resource
	private IRcQuotaSV rcquotaSV;
	
	
	@RequestMapping("rcResultList.do")
	public ModelAndView rcQuotaList(HttpServletRequest request, String serNo){

		ModelAndView mav = new ModelAndView("views/examine/examineRcResult");
		List<RcResult> rcResultList = rcResultService.selectByProperty("serNo", serNo);
		mav.addObject("rcResultList", rcResultList);
		mav.addObject("serNo", serNo);
		return mav;
	}
	
	
	@RequestMapping("syncRcResult.json")
	@ResponseBody
	public ResultDto getRcQuotaList(HttpServletRequest request,String serNo){
		ResultDto re = returnFail();
		try{
			rcResultService.syncRcResult(serNo);
		}catch (Exception e){
			log.error("计算利率异常",e);
			e.printStackTrace();
		}
		return re;
	}
	
	

	
	
}
