package com.beawan.rate.controller;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.rate.entity.RcQuota;
import com.beawan.rate.service.IRcQuotaSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.HttpUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;

/**
 * 指标项管理
 * @author 86178
 *
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/model/rcquota" })
public class RcQuotaCtl extends BaseController {

	private static final Logger log = Logger.getLogger(RcQuotaCtl.class);
	@Resource
	private IRcQuotaSV rcquotaSV;
	
	
	@RequestMapping("rcQuotaList.do")
	public String rcQuotaList(HttpServletRequest request, HttpServletResponse response){
		return "views/base/rate/rcQuotaList";
	}
	
	
	@RequestMapping("getRcQuotaList.do")
	@ResponseBody
	public String getRcQuotaList(HttpServletRequest request, HttpServletResponse response,String search, String order,int page, int rows){
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			RcQuota condition = null;
			if(!StringUtil.isEmptyString(search)){
				condition = JacksonUtil.fromJson(search, RcQuota.class);
			}
			
			long totalCount = rcquotaSV.totalCount(condition);
			List<RcQuota> list =  rcquotaSV.paginate(condition, order, page-1, rows);
			json.put("rows", list);
			json.put("total", totalCount);
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", "查询指标项列表出错！");
			log.error("查询指标项列表出错", e);
		}
		return jRequest.serialize(json, true);
	}
	
	
	@RequestMapping("quotaInfo.do")
	public ModelAndView quotaInfo(HttpServletRequest request, HttpServletResponse response){
		ModelAndView mav = new ModelAndView("/views/base/rate/rcQuota_info");
		try {
			long id = HttpUtil.getAsLong(request, "id");
			RcQuota rcQuota = rcquotaSV.queryById(id);
			mav.addObject("data", rcQuota);
		} catch (Exception e) {
			log.error("查看指标详情系统异常！",e);
			e.printStackTrace();
		}
		return mav;
	}
	
	@RequestMapping("modify.do")
	public ModelAndView modify(HttpServletRequest request, HttpServletResponse response){
		ModelAndView mav = new ModelAndView("/views/base/rate/rcQuota_modeify");
		try {
			long id = HttpUtil.getAsLong(request, "id");
			RcQuota rcQuota = rcquotaSV.queryById(id);
			mav.addObject("data", rcQuota);
		} catch (Exception e) {
			log.error("查看指标详情系统异常！",e);
			e.printStackTrace();
		}
		return mav;
	}
	
	
	/**
	 * 保存
	 * @return
	 */
	@RequestMapping("saveQuota.do")
	@ResponseBody
	public ResultDto saveQuota(HttpServletRequest request, HttpServletResponse response){
		ResultDto re = returnFail("");
		Map<String,Object> model = new HashMap<>();
		try {
			String jsonString = HttpUtil.getAsString(request, "jsonstring");
			jsonString = URLDecoder.decode(jsonString, "utf-8");
			RcQuota rcQuota = JacksonUtil.fromJson(jsonString, RcQuota.class);	
			if(rcQuota==null ) return re;
			List<RcQuota> getRcQuotas=rcquotaSV.queryByCode(rcQuota.getCode());
		   if(rcQuota.getId()!=null) {
 
			   rcquotaSV.update(rcQuota);
			   
		   }else {
			if(getRcQuotas!=null && getRcQuotas.size()>0) {
				re.setMsg("存在指标编码重复，修改失败");
				return re;
			}
			   rcquotaSV.save(rcQuota);
		   }
			
			model.put("save", true);
			model.put("success", true);		
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("success");
			re.setRows(model);
		} catch (Exception e) {
			model.put("msg", "系统异常，请联系管理员！");
			log.error(e);
			e.printStackTrace();
		}
		return re;
	}
	
	
	
}
