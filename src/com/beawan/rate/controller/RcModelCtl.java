package com.beawan.rate.controller;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.rate.entity.RcModel;
import com.beawan.rate.dto.RateModel;
import com.beawan.rate.service.IRcModelSV;
import com.beawan.rate.service.IRcQuotaSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.HttpUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;

/**
 * 指标项管理
 * @author 86178
 *
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/model/rcModel" })
public class RcModelCtl extends BaseController {

	private static final Logger log = Logger.getLogger(RcModelCtl.class);
	@Resource
	private IRcModelSV rcModelSV;
	@Resource
	private IRcQuotaSV rcQuotaSV;
	
	
	@RequestMapping("rcModelList.do")
	public String rcModelList(HttpServletRequest request, HttpServletResponse response){
		return "views/base/rate/rcModelList";
	}
	
	
	@RequestMapping("getRcModelList.do")
	@ResponseBody
	public String getRcModelList(HttpServletRequest request, HttpServletResponse response,String search, String order,int page, int rows){
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			RcModel condition = null;
			if(!StringUtil.isEmptyString(search)){
				condition = JacksonUtil.fromJson(search, RcModel.class);
			}
			
			long totalCount = rcModelSV.totalCount(condition);
			List<RcModel> list =  rcModelSV.paginate(condition, order, page-1, rows);
			json.put("rows", list);
			json.put("total", totalCount);
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", "查询指标项列表出错！");
			log.error("查询指标项列表出错", e);
		}
		return jRequest.serialize(json, true);
	}
	
	
	@RequestMapping("view.do")
	public ModelAndView view(HttpServletRequest request, HttpServletResponse response){
		ModelAndView mav = new ModelAndView("/views/base/rate/rcModel_info");
		try {
			long id = HttpUtil.getAsLong(request, "id");
			RcModel rcModel = rcModelSV.queryById(id);
			mav.addObject("model", rcModel);
		} catch (Exception e) {
			log.error("查看指标详情系统异常！",e);
			e.printStackTrace();
		}
		return mav;
	}
	
	@RequestMapping("modify.do")
	public ModelAndView modify(HttpServletRequest request, HttpServletResponse response){
		ModelAndView mav = new ModelAndView("/views/base/rate/rcModel_modify");
		try {
			long id = HttpUtil.getAsLong(request, "id");
			RcModel rcModel = rcModelSV.queryById(id);
			mav.addObject("model", rcModel);
		} catch (Exception e) {
			log.error("查看指标详情系统异常！",e);
			e.printStackTrace();
		}
		return mav;
	}
	
	@RequestMapping("modify2.do")
	public ModelAndView modify2(HttpServletRequest request, HttpServletResponse response){
		ModelAndView mav = new ModelAndView("/views/base/rate/modify_condition");
		try {
			long id = HttpUtil.getAsLong(request, "id");
			RcModel rcModel = rcModelSV.queryById(id);
			mav.addObject("data", rcModel);
		} catch (Exception e) {
			log.error("查看指标详情系统异常！",e);
			e.printStackTrace();
		}
		return mav;
	}
	
	
	/**
	 * 保存
	 * @return
	 */
	@RequestMapping("save.json")
	@ResponseBody
	public ResultDto save(HttpServletRequest request, HttpServletResponse response){
		ResultDto re = returnFail("");
		Map<String,Object> model = new HashMap<>();
		try {
			String jsonString = HttpUtil.getAsString(request, "jsonstring");
			jsonString = URLDecoder.decode(jsonString, "utf-8");
			RateModel rateModel = JacksonUtil.fromJson(jsonString, RateModel.class);	
			rcModelSV.saveRateModel(rateModel);
			
			model.put("save", true);
			model.put("success", true);		
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("success");
			re.setRows(model);
		} catch (Exception e) {
			model.put("msg", "系统异常，请联系管理员！");
			log.error(e);
			e.printStackTrace();
		}
		return re;
	}
}
