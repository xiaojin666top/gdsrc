package com.beawan.rate.dto;

import com.beawan.rate.entity.RcQuota;

public class RateQuotaDto {	
	private Long id;
	private String code;
	private String name;
	private String type;
	private String isResult;
	private String unit;	
	private String value;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getIsResult() {
		return isResult;
	}
	public void setIsResult(String isResult) {
		this.isResult = isResult;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public RateQuotaDto(Long id, String code, String name, String type, String isResult, String unit, String value) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.type = type;
		this.isResult = isResult;
		this.unit = unit;
		this.value = value;
	}
	
	public RateQuotaDto(RcQuota rcquota) {
		super();
		this.id = rcquota.getId();
		this.code = rcquota.getCode();
		this.name = rcquota.getName();
		this.type = rcquota.getType();
		this.isResult = rcquota.getIsResult();
		this.unit = rcquota.getUnit();
		this.value = rcquota.getValue();
	}
	public RateQuotaDto() {
		super();
	}
	
	
}
