package com.beawan.rate.dto;

import java.util.List;

import com.beawan.rate.entity.RcCondition;

public class RateModel {
	private Long id;
	private String name;
	private List<RcCondition> conditions;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<RcCondition> getConditions() {
		return conditions;
	}
	public void setConditions(List<RcCondition> conditions) {
		this.conditions = conditions;
	}
	
	

}
