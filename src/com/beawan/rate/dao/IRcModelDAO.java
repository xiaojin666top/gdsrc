package com.beawan.rate.dao;

import java.util.List;

import com.beawan.core.BaseDao;
import com.beawan.rate.entity.RcModel;

public interface IRcModelDAO extends BaseDao<RcModel> {

	public List<RcModel> queryPaginate(String queryString, Object[] params, int start, int limit) throws Exception;

	public List<RcModel> queryByCondition(String queryString, Object[] params) throws Exception;

	public RcModel queryById(long id) throws Exception;

	public List<RcModel> queryByStatus(String status) throws Exception;
	
}