package com.beawan.rate.dao;

import com.beawan.core.BaseDao;
import com.beawan.rate.dto.RcDateDto;
import com.beawan.rate.entity.RcResult;

import java.util.List;

/**
 * @author yzj
 */
public interface RcResultDao extends BaseDao<RcResult> {

    /**
     * 根据模型id获取详细模型内容
     * @param modelId
     * @return
     * @throws Exception
     */
    public List<RcDateDto> getRcDateList(String modelId) throws Exception;
}
