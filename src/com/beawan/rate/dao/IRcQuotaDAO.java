package com.beawan.rate.dao;

import java.util.List;

import com.beawan.core.BaseDao;
import com.beawan.rate.entity.RcQuota;

public interface IRcQuotaDAO extends BaseDao<RcQuota> {

	public List<RcQuota> queryPaginate(String queryString, Object[] params, int start, int limit) throws Exception;

	public List<RcQuota> queryByCondition(String queryString, Object[] params) throws Exception;

	public RcQuota queryById(long id) throws Exception;

	public List<RcQuota> queryByCode(String code) throws Exception;
	 
	public List<RcQuota> queryAll() throws Exception;
	public List<RcQuota> queryAllNotResult() throws Exception;

}
