package com.beawan.rate.dao;

import java.util.List;

import com.beawan.core.BaseDao;
import com.beawan.rate.entity.RcCondition;

public interface IRcConditionDAO extends BaseDao<RcCondition> {

	public List<RcCondition> queryPaginate(String queryString, Object[] params, int start, int limit) throws Exception;

	public List<RcCondition> queryByCondition(String queryString, Object[] params) throws Exception;

	public RcCondition queryById(long id) throws Exception;

	public List<RcCondition> queryByQuotaIdAndModelId(Long quotaId,Long modelId) throws Exception;
	
	public List<RcCondition> queryByModelId(Long modelId) throws Exception;
}