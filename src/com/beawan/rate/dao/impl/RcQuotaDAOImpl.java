package com.beawan.rate.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.rate.entity.RcQuota;
import com.beawan.rate.dao.IRcQuotaDAO;

@Repository("rcQuotaDAO")
public class RcQuotaDAOImpl extends  BaseDaoImpl<RcQuota>  implements IRcQuotaDAO{

	@Override
	public List<RcQuota> queryPaginate(String queryString, Object[] params, int start, int limit) throws Exception {
		int index = start < 0 ? 0 : start;
		int count = limit < 0 ? 0 : limit;
		List<RcQuota> list = execQueryRange(RcQuota.class, queryString, index, count, params);
		return list;
	}

	@Override
	public List<RcQuota> queryByCondition(String queryString, Object[] params) throws Exception {
		return execQuery(RcQuota.class,queryString,params);
	}

	@Override
	public RcQuota queryById(long id) throws Exception {
		String queryString = "from RcQuota as model where model.id="+id;
		List<RcQuota> list = select(RcQuota.class, queryString);
		if(list.size()>0){
			return list.get(0);
		}else{
			return null;
		}
	}

	@Override
	public List<RcQuota> queryByCode(String code) throws Exception {
		String queryString = "from RcQuota as model where model.code=?";
		List<RcQuota> list = select(RcQuota.class, queryString,code);
		
		return list;
	
	}

	@Override
	public List<RcQuota> queryAll() throws Exception {
		String queryString = "from RcQuota as model ";
		List<RcQuota> list = select(RcQuota.class, queryString);
		return list;
	}

	@Override
	public List<RcQuota> queryAllNotResult() throws Exception {
		String queryString = "from RcQuota as model where model.isResult =? order by model.order asc";
		List<RcQuota> list = select(RcQuota.class, queryString,new String("0"));
		return list;
	}

	

}