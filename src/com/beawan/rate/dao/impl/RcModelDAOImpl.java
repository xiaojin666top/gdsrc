package com.beawan.rate.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.rate.entity.RcModel;
import com.beawan.rate.dao.IRcModelDAO;


@Repository("rcModelDAO")
public class RcModelDAOImpl extends  BaseDaoImpl<RcModel>  implements IRcModelDAO{

	@Override
	public List<RcModel> queryPaginate(String queryString, Object[] params, int start, int limit) throws Exception {
		int index = start < 0 ? 0 : start;
		int count = limit < 0 ? 0 : limit;
		List<RcModel> list = execQueryRange(RcModel.class, queryString, index, count, params);
		return list;
	}

	@Override
	public List<RcModel> queryByCondition(String queryString, Object[] params) throws Exception {
		return execQuery(RcModel.class,queryString,params);
	}

	@Override
	public RcModel queryById(long id) throws Exception {
		String queryString = "from RcModel as model where model.id="+id;
		List<RcModel> list = select(RcModel.class, queryString);
		if(list.size()>0){
			return list.get(0);
		}else{
			return null;
		}
	}

	@Override
	public List<RcModel> queryByStatus(String status) throws Exception {
		String queryString = "from RcModel as model where model.status=?";
		List<RcModel> list = select(RcModel.class, queryString,status);
		return list;
	
	}

	

}