package com.beawan.rate.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.rate.entity.RcCondition;
import com.beawan.rate.dao.IRcConditionDAO;


@Repository("rcConditonDAO")
public class RcConditionDAPOImpl extends  BaseDaoImpl<RcCondition>  implements IRcConditionDAO{

	@Override
	public List<RcCondition> queryPaginate(String queryString, Object[] params, int start, int limit) throws Exception {
		int index = start < 0 ? 0 : start;
		int count = limit < 0 ? 0 : limit;
		List<RcCondition> list = execQueryRange(RcCondition.class, queryString, index, count, params);
		return list;
	}

	@Override
	public List<RcCondition> queryByCondition(String queryString, Object[] params) throws Exception {
		return execQuery(RcCondition.class,queryString,params);
	}

	@Override
	public RcCondition queryById(long id) throws Exception {
		String queryString = "from RcCondition as model where model.id="+id;
		List<RcCondition> list = select(RcCondition.class, queryString);
		if(list.size()>0){
			return list.get(0);
		}else{
			return null;
		}
	}

	@Override
	public List<RcCondition> queryByQuotaIdAndModelId(Long quotaId,Long modelId) throws Exception {
		String queryString = "from RcCondition as model where model.quotaId=? and model.modelId =?";
		List<RcCondition> list = select(RcCondition.class, queryString,quotaId,modelId);
		return list;
	
	}

	@Override
	public List<RcCondition> queryByModelId(Long modelId) throws Exception {
		String queryString = "from RcCondition as model where model.modelId=?";
		List<RcCondition> list = select(RcCondition.class, queryString,modelId);
		return list;
	}
}

	
