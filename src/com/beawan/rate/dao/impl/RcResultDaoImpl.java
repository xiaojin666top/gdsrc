package com.beawan.rate.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.rate.dao.RcResultDao;
import com.beawan.rate.dto.RcDateDto;
import com.beawan.rate.entity.RcResult;

/**
 * @author yzj
 */
@Repository("rcResultDao")
public class RcResultDaoImpl extends BaseDaoImpl<RcResult> implements RcResultDao{

    @Override
    public List<RcDateDto> getRcDateList(String modelId) throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append("select a.id modelId,a.name modelName,b.quota_id quotaId,c.code quotaCode,")
                .append(" c.name quotaName,c.type quotaType,b.ATTR_VALUE attrValue,")
                .append(" b.UPPER upper,b.down down,b.CONTENT content,b.RESULT_VALUE resultValue")
                .append(" from  RC_MODEL a")
                .append(" join RC_CONDITION b on a.id=b.model_id")
                .append(" join RC_QUOTA c on b.QUOTA_ID=c.id")
                .append(" where 1=1 and a.id=:modelId");
        Map<String, Object> params = new HashMap<>();
        params.put("modelId", modelId);
        List<RcDateDto> rcDateDtos = findCustListBySql(RcDateDto.class, sql, params);
        return rcDateDtos;
    }
}
