package com.beawan.rate.service;

import com.beawan.core.BaseService;
import com.beawan.rate.dto.RcDateDto;
import com.beawan.rate.entity.RcResult;

import java.util.List;

/**
 * @author yzj
 */
public interface RcResultService extends BaseService<RcResult> {


    /**
     * 根据模型id获取详细模型内容
     * @param modelId
     * @return
     * @throws Exception
     */
    List<RcDateDto> getRcDateList(String modelId) throws Exception;

    /**
     * 计算对公利率指标
     * @param serNo
     * @throws Exception
     */
    Double syncRcResult(String serNo) throws Exception;


}
