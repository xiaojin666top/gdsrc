package com.beawan.rate.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.beawan.rate.entity.RcCondition;
import com.beawan.rate.entity.RcModel;
import com.beawan.rate.entity.RcQuota;
import com.beawan.rate.dao.IRcModelDAO;
import com.beawan.rate.dao.IRcQuotaDAO;
import com.beawan.rate.dto.RateModel;
import com.beawan.rate.service.IRcConditionSV;
import com.beawan.rate.service.IRcModelSV;
import com.platform.util.DateUtil;
import com.platform.util.StringUtil;


@Service("rcModelSV")
public class RcModelSVImpl implements IRcModelSV  {

	@Resource
	private IRcModelDAO rcModelDAO;
	
	@Resource
	private IRcConditionSV rcConditionSV;
	@Resource
	private IRcQuotaDAO rcQuotaDAO;

	@Override
	public List<RcModel> paginate(RcModel queryCondition, String sortCondition, int start, int limit) throws Exception {
		StringBuffer queryString = new StringBuffer("from RcModel as model ");
		List<Object> params = new ArrayList<Object>();
		if(queryCondition!=null){
			/**/
		}
		if(!StringUtil.isEmptyString(sortCondition)){
			queryString.append(" order by ").append(sortCondition);
		}
		int startIndex = start * limit;
		return rcModelDAO.queryPaginate(queryString.toString(),params.toArray(), startIndex, limit);
	}

	@Override
	public int totalCount(RcModel queryCondition) throws Exception {
		StringBuffer queryString = new StringBuffer("from RcModel as model ");
		List<Object> params = new ArrayList<Object>();

		List<RcModel> list = rcModelDAO.queryByCondition(queryString.toString(), params.toArray());
		if(!CollectionUtils.isEmpty(list)){
			return list.size();
		}else{
			return 0;
		}
	}

	@Override
	public RcModel queryById(long id) throws Exception {
		RcModel rcModel = rcModelDAO.queryById(id);
		if(rcModel==null) return null;
		List<RcQuota> rcQuotas =  rcQuotaDAO.queryAllNotResult();
		if(rcQuotas==null || rcQuotas.size()==0) return rcModel;
		for(RcQuota rcQuota : rcQuotas) {
			List<RcCondition> rcConditions =rcConditionSV.queryByQuotaIdAndModelId(rcQuota.getId(), rcModel.getId());
			rcQuota.setConditions(rcConditions);
		}
		rcModel.setQuotas(rcQuotas);
	
		return rcModel;
	}

	@Override
	public List<RcModel> queryByStatus(String status) throws Exception {
		// TODO Auto-generated method stub
				List<RcModel> rcQuotas = rcModelDAO.queryByStatus(status);
				return rcQuotas;
	}

	@Override
	public void update(RcModel rcModel) throws Exception {
		rcModelDAO.saveOrUpdate(rcModel);
		
	}

	@Override
	public RcModel save(RcModel rcModel) throws Exception {
		// TODO Auto-generated method stub
		return rcModelDAO.saveOrUpdate(rcModel);
	}

	@Override
	public void saveRateModel(RateModel rateModel) throws Exception {
	   if(rateModel==null) return;
	   if(rateModel.getId()==null) {
		  
		   RcModel model = new RcModel();
		   model.setName(rateModel.getName());
		   model.setCreateTime(DateUtil.getNow());
		   rcModelDAO.saveOrUpdate(model);
		   rcConditionSV.saveRcCondition(rateModel.getConditions(),model.getId());
		   
	   }else {
		   rcConditionSV.deleteConditionByModelId(rateModel.getId());
		  
		   RcModel rcModel= rcModelDAO.queryById(rateModel.getId());
		   rcModel.setName(rateModel.getName());
		  rcModelDAO.saveOrUpdate(rcModel);
		  rcConditionSV.saveRcCondition(rateModel.getConditions(),rcModel.getId());
	   }
		
	}
	
	
	
	
}
