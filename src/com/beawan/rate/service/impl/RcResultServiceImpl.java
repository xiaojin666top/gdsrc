package com.beawan.rate.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.rate.dao.RcResultDao;
import com.beawan.rate.dto.RcDateDto;
import com.beawan.rate.entity.RcResult;
import com.beawan.rate.service.RcResultService;
import com.beawan.scoreCard.AcConstants;
import com.beawan.survey.custInfo.dto.CompBaseDto;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.beawan.task.bean.Task;
import com.beawan.task.dao.ITaskDAO;
import com.platform.util.ExceptionUtil;
import com.platform.util.StringUtil;

/**
 * @author yzj
 */
@Service("rcResultService")
public class RcResultServiceImpl extends BaseServiceImpl<RcResult> implements RcResultService{

    private static Logger log = Logger.getLogger(RcResultServiceImpl.class);
    /**
    * 注入DAO
    */
    @Resource(name = "rcResultDao")
    public void setDao(BaseDao<RcResult> dao) {
        super.setDao(dao);
    }
    @Resource
    public RcResultDao rcResultDao;
    @Resource
    private ICusBaseSV cusBaseSV;
    @Resource
    private ICompBaseSV compBaseSV;
    @Resource
    private ITaskDAO taskDAO;


    @Override
    public List<RcDateDto> getRcDateList(String modelId) throws Exception {
        if(StringUtil.isEmptyString(modelId)){
            ExceptionUtil.throwException("查询模型主键为空，请重试");
        }
        return rcResultDao.getRcDateList(modelId);
    }

    @Override
    public Double syncRcResult(String serNo) throws Exception {

        List<RcResult> rcResultList = new ArrayList<>();
        String modelId = "1";//利率就只有一个模型 --》碧湾利率模型  主键为1
        List<RcDateDto> rcDateList = rcResultDao.getRcDateList(modelId);
        Map<String, List<RcDateDto>> quotaMap = rcDateList.stream().collect(Collectors.groupingBy(RcDateDto::getQuotaName));

        Task task = taskDAO.getTaskBySerNo(serNo);
        String customerNo = task.getCustomerNo();
        CompBaseDto compBaseDto = compBaseSV.findCompBaseByCustNo(customerNo);

        Double baseRate = 0.0;
        Double gradeV = 0.0;
        Double guarwayV = 0.0;
        Double creditHistV = 0.0;
        Double revoleV = 0.0;
        Double amountV = 0.0;
        Double baseAccV = 0.0;
        Double creditAccV = 0.0;

        Integer loanTerm = task.getLoanTerm();
        if(loanTerm ==null || loanTerm <= 1){
            baseRate = 4.35;
        }else if(loanTerm <= 5){
            baseRate = 4.75;
        }else{
            baseRate = 4.9;
        }

        for(Map.Entry<String, List<RcDateDto>> entry : quotaMap.entrySet()){
            String key = entry.getKey();
            List<RcDateDto> list = entry.getValue();
            if(CollectionUtils.isEmpty(list))
                continue;
            switch (key){
                //客户评级
                case Constants.MD_RATE_QUOTA.CUS_GRADE:
                    String creditLevel = compBaseDto.getCreditLevel();
                    list:for(RcDateDto rcDate : list){
                        if(AcConstants.AcQuotaAttrType.SECTION.equals(rcDate.getQuotaType())){
                            if(rcDate.getContent().equals(creditLevel+"类客户")){
                                gradeV = rcDate.getResultValue() / 100;
                                RcResult rc = new RcResult(serNo, key, rcDate.getContent(), creditLevel, rcDate.getResultValue());
                                rcResultList.add(rc);
                                break list;
                            }
                        }
                    }
                    break;
                    //授信金额
                case Constants.MD_RATE_QUOTA.CREDIT_AMOUNT:
                    Double loanNumber = task.getLoanNumber();
                    list:for(RcDateDto rcDate : list) {
                        if (AcConstants.AcQuotaAttrType.VALUE.equals(rcDate.getQuotaType())) {
                            Double upper = rcDate.getUpper();
                            Double down = rcDate.getDown();
                            if(AcConstants.SectionType.LEFT.equals(rcDate.getAttrValue())){
                                if(loanNumber > down && loanNumber <= upper){
                                    amountV = rcDate.getResultValue();
                                    String content = "授信金额大于" + down + "万元，且小于等于" + upper + "万元";
                                    RcResult rc = new RcResult(serNo, key, content, loanNumber.toString(), rcDate.getResultValue());
                                    rcResultList.add(rc);
                                    break list;
                                }
                            }else if (AcConstants.SectionType.RIGHT.equals(rcDate.getAttrValue())){
                                if(loanNumber >= down && loanNumber < upper) {
                                    amountV = rcDate.getResultValue();
                                    String content = "授信金额大于等于" + down + "万元，且小于" + upper + "万元";
                                    RcResult rc = new RcResult(serNo, key, content, loanNumber.toString(), rcDate.getResultValue());
                                    rcResultList.add(rc);
                                    break list;
                                }
                            }else if (AcConstants.SectionType.ALL.equals(rcDate.getAttrValue())){
                                if(loanNumber >= down && loanNumber <= upper) {
                                    amountV = rcDate.getResultValue();
                                    String content = "授信金额大于等于" + down + "万元，且小于等于" + upper + "万元";
                                    RcResult rc = new RcResult(serNo, key, content, loanNumber.toString(), rcDate.getResultValue());
                                    rcResultList.add(rc);
                                    break list;
                                }
                            }
                        }
                    }
                    break;
                    //征信记录
                case Constants.MD_RATE_QUOTA.CREDIT_RECORD:
                    String creditRecord = compBaseDto.getCreditRecord();
                    list:for(RcDateDto rcDate : list) {
                        if (AcConstants.AcQuotaAttrType.SECTION.equals(rcDate.getQuotaType())) {
                            if(rcDate.getContent().equals(creditRecord)){
                                creditHistV = rcDate.getResultValue();
                                RcResult rc = new RcResult(serNo, key, rcDate.getContent(), creditRecord, rcDate.getResultValue());
                                rcResultList.add(rc);
                                break list;
                            }
                        }
                    }
                    break;
                    //客户忠诚度
                case Constants.MD_RATE_QUOTA.CUS_LOYAL:
                    String mbankAccType = compBaseDto.getMbankAccType();
                    String loanHistory = compBaseDto.getLoanHistory();
                    list:for(RcDateDto rcDate : list) {
                        if (AcConstants.AcQuotaAttrType.SECTION.equals(rcDate.getQuotaType())) {
                            if(AcConstants.LOYAL.BASE_ACC.equals(rcDate.getContent())){
                                //非本行基本户
                                if(!"03".equals(mbankAccType)){
                                    baseAccV  = rcDate.getResultValue();
                                    RcResult rc = new RcResult(serNo, key, rcDate.getContent(), mbankAccType, rcDate.getResultValue());
                                    rcResultList.add(rc);
                                }
                            }else if(AcConstants.LOYAL.CREDIT_ACC.equals(rcDate.getContent())){
                                if(AcConstants.LOAN_HISTORY.TRUE.equals(loanHistory)){
                                    creditAccV = rcDate.getResultValue();
                                    RcResult rc = new RcResult(serNo, key, rcDate.getContent(), "是", rcDate.getResultValue());
                                    rcResultList.add(rc);
                                }
                            }
                        }
                    }
                    break;
                    //抵押方式
                case Constants.MD_RATE_QUOTA.GUARWAY:
                    String assureMeans = task.getAssureMeans();
                    list:for(RcDateDto rcDate : list) {
                        if(AcConstants.GUARWAY.OTHER_COMP.equals(assureMeans)){
                            if("保证-公司保证".equals(rcDate.getContent())){
                                guarwayV = rcDate.getResultValue();
                                RcResult rc = new RcResult(serNo, key, rcDate.getContent(), assureMeans, rcDate.getResultValue());
                                rcResultList.add(rc);
                            }
                            break list;
                        }else if(AcConstants.GUARWAY.NORMAL_PERSON.equals(assureMeans)){
                            if("保证-自然人保证".equals(rcDate.getContent())){
                                guarwayV = rcDate.getResultValue();
                                RcResult rc = new RcResult(serNo, key, rcDate.getContent(), assureMeans, rcDate.getResultValue());
                                rcResultList.add(rc);
                            }
                            break list;
                        }else if(AcConstants.GUARWAY.MORTGAGE.equals(assureMeans)){
                            //具体的抵质押
                            if("抵押-其他房地产".equals(rcDate.getContent())){
                                guarwayV = rcDate.getResultValue();
                                RcResult rc = new RcResult(serNo, key, rcDate.getContent(), assureMeans, rcDate.getResultValue());
                                rcResultList.add(rc);
                            }
                            break list;
                        }
                    }
                    break;
                    //是否循环额度
                case Constants.MD_RATE_QUOTA.REVOLV_LIMIT:
                    String revoleCredit = compBaseDto.getRevoleCredit();
                    list:for(RcDateDto rcDate : list) {
                        if (AcConstants.AcQuotaAttrType.SECTION.equals(rcDate.getQuotaType())) {
                            if(AcConstants.REVOLE_CREDIT.TRUE.equals(revoleCredit)){
                                revoleV = rcDate.getResultValue();
                                RcResult rc = new RcResult(serNo, key, rcDate.getContent(), "是", rcDate.getResultValue());
                                rcResultList.add(rc);
                                break list;
                            }else if(AcConstants.REVOLE_CREDIT.FALSE.equals(revoleCredit)){
                                revoleV = rcDate.getResultValue();
                                RcResult rc = new RcResult(serNo, key, rcDate.getContent(), "否", rcDate.getResultValue());
                                rcResultList.add(rc);
                                break list;
                            }
                        }
                    }
                    break;
            }
        }

        if(!CollectionUtils.isEmpty(rcResultList)){
            //先清空
            List<RcResult> list = rcResultDao.selectByProperty("serNo", serNo);
            if(!CollectionUtils.isEmpty(list)) {
                for (RcResult rc : list) {
                    rc.setStatus(Constants.DELETE);
                }
                rcResultDao.batchSaveUpdata(list);
            }
        }
        rcResultDao.batchSaveUpdata(rcResultList);

        //最终计算额度
        /**
         * 指导利率=基本利率*（1+评级调整项）+担保方式调整项+信用记录调整项+循环额度调整系数+授信金额调整项+忠诚度调整项
         */
        Double rate = baseRate * (1 + gradeV) + guarwayV + creditHistV + revoleV + amountV - baseAccV - creditAccV;

        log.info("对公利率计算成功，测算利率为：" + rate + " %");
        task.setDgRate(rate);
        taskDAO.saveOrUpdate(task);

        return rate;

    }
}
