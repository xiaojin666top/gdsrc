package com.beawan.rate.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.beawan.rate.entity.RcQuota;
import com.beawan.rate.dao.IRcQuotaDAO;
import com.beawan.rate.service.IRcQuotaSV;
import com.platform.util.StringUtil;


@Service("rcQuotaSV")
public class RcQuotaSVImpl implements IRcQuotaSV  {

	@Resource
	private IRcQuotaDAO rcQuotaDAO;

	@Override
	public List<RcQuota> paginate(RcQuota queryCondition, String sortCondition, int start, int limit) throws Exception {
		StringBuffer queryString = new StringBuffer("from RcQuota as model where model.isResult='0' ");
		List<Object> params = new ArrayList<Object>();
		if(queryCondition!=null){
			/**/
		}
		if(!StringUtil.isEmptyString(sortCondition)){
			queryString.append(" order by ").append(sortCondition);
		}
		int startIndex = start * limit;
		return rcQuotaDAO.queryPaginate(queryString.toString(),params.toArray(), startIndex, limit);
	}

	@Override
	public int totalCount(RcQuota queryCondition) throws Exception {
		StringBuffer queryString = new StringBuffer("from RcQuota as model ");
		List<Object> params = new ArrayList<Object>();

		List<RcQuota> list = rcQuotaDAO.queryByCondition(queryString.toString(), params.toArray());
		if(!CollectionUtils.isEmpty(list)){
			return list.size();
		}else{
			return 0;
		}
	}

	@Override
	public RcQuota queryById(long id) throws Exception {
		// TODO Auto-generated method stub
		RcQuota rcQuota = rcQuotaDAO.queryById(id);
	
		return rcQuota;
	}

	@Override
	public List<RcQuota> queryByCode(String code) throws Exception {
		// TODO Auto-generated method stub
				List<RcQuota> rcQuotas = rcQuotaDAO.queryByCode(code);
				return rcQuotas;
	}

	@Override
	public void update(RcQuota rcQuota) throws Exception {
		rcQuotaDAO.saveOrUpdate(rcQuota);
		
	}

	@Override
	public RcQuota save(RcQuota rcQuota) throws Exception {
		// TODO Auto-generated method stub
		return rcQuotaDAO.saveOrUpdate(rcQuota);
	}

	@Override
	public List<RcQuota> queryAllNotResult() throws Exception {
		// TODO Auto-generated method stub
		return rcQuotaDAO.queryAllNotResult();
	}
	
	
	
	
}
