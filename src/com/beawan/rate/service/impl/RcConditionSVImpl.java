package com.beawan.rate.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.beawan.rate.entity.RcCondition;
import com.beawan.rate.entity.RcQuota;
import com.beawan.rate.dao.IRcConditionDAO;
import com.beawan.rate.dao.IRcQuotaDAO;
import com.beawan.rate.dto.RateQuotaDto;
import com.beawan.rate.service.IRcConditionSV;
import com.platform.util.StringUtil;


@Service("rcConditionSV")
public class RcConditionSVImpl implements IRcConditionSV  {

	@Resource
	private IRcConditionDAO rcConditionDAO;
	@Resource
	private IRcQuotaDAO rcQuotaDAO;

	@Override
	public List<RcCondition> paginate(RcCondition queryCondition, String sortCondition, int start, int limit) throws Exception {
		StringBuffer queryString = new StringBuffer("from RcCondition as model ");
		List<Object> params = new ArrayList<Object>();
		if(queryCondition!=null){
			/**/
		}
		if(!StringUtil.isEmptyString(sortCondition)){
			queryString.append(" order by ").append(sortCondition);
		}
		int startIndex = start * limit;
		return rcConditionDAO.queryPaginate(queryString.toString(),params.toArray(), startIndex, limit);
	}

	@Override
	public int totalCount(RcCondition queryCondition) throws Exception {
		StringBuffer queryString = new StringBuffer("from RcCondition as model ");
		List<Object> params = new ArrayList<Object>();

		List<RcCondition> list = rcConditionDAO.queryByCondition(queryString.toString(), params.toArray());
		if(!CollectionUtils.isEmpty(list)){
			return list.size();
		}else{
			return 0;
		}
	}

	@Override
	public RcCondition queryById(long id) throws Exception {
		// TODO Auto-generated method stub
		RcCondition rcCondition = rcConditionDAO.queryById(id);
	
		return rcCondition;
	}

	@Override
	public List<RcCondition> queryByQuotaIdAndModelId(Long quotaId,Long modelId) throws Exception {
		// TODO Auto-generated method stub
				List<RcCondition> rcConditions = rcConditionDAO.queryByQuotaIdAndModelId(quotaId, modelId);
				if(rcConditions!=null && rcConditions.size()>0) {
					for(RcCondition rcCondition : rcConditions) {
						RcQuota quota = rcQuotaDAO.queryById(rcCondition.getQuotaId());
						RcQuota result = rcQuotaDAO.queryById(rcCondition.getResultId());
						rcCondition.setCondQuota(new RateQuotaDto(quota));
						rcCondition.setResultQuota(new RateQuotaDto(result));
					}
				}
				return rcConditions;
	}

	@Override
	public void update(RcCondition rcCondition) throws Exception {
		rcConditionDAO.saveOrUpdate(rcCondition);
		
	}

	@Override
	public RcCondition save(RcCondition rcCondition) throws Exception {
		// TODO Auto-generated method stub
		return rcConditionDAO.saveOrUpdate(rcCondition);
	}

	@Override
	public void deleteConditionByModelId(long id) throws Exception {
		// TODO Auto-generated method stub
	

		List<RcCondition> list = rcConditionDAO.queryByModelId(id);
		if(!CollectionUtils.isEmpty(list)){
			for(RcCondition condition :list){
				 rcConditionDAO.deleteEntity(condition);;
			}
			
		}
	}

	@Override
	public void saveRcCondition(List<RcCondition> rcConditions,Long modelId) throws Exception {
		if(CollectionUtils.isEmpty(rcConditions)) return;
		for(RcCondition condition : rcConditions) {
			condition.setModelId(modelId);
			rcConditionDAO.saveOrUpdate(condition);
		}
		
	}
	
	
	
	
}
