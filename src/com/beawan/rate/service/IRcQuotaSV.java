package com.beawan.rate.service;

import java.util.List;

import com.beawan.rate.entity.RcQuota;


public interface IRcQuotaSV {
	
	  public List<RcQuota> paginate(RcQuota queryCondition,String sortCondition,int start ,int limit)throws Exception;
		/**
		 * 获取符合条件的条数
		 * @param queryCondition
		 * @return
		 * @throws Exception
		 */
		public int totalCount(RcQuota queryCondition)throws Exception;
		
		public RcQuota queryById(long id) throws Exception ;
		
		public List<RcQuota> queryByCode(String code) throws Exception ;
		
		public List<RcQuota> queryAllNotResult() throws Exception;
		
		public void update(RcQuota rcQuota) throws Exception;

		public RcQuota save(RcQuota rcQuota) throws Exception;
		
}
