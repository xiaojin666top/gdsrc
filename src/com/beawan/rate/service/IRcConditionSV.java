package com.beawan.rate.service;

import java.util.List;

import com.beawan.rate.entity.RcCondition;

public interface IRcConditionSV {
	public List<RcCondition> paginate(RcCondition queryCondition,String sortCondition,int start ,int limit)throws Exception;
	
	public int totalCount(RcCondition queryCondition)throws Exception;
	
	public RcCondition queryById(long id) throws Exception ;
	
	public List<RcCondition> queryByQuotaIdAndModelId(Long quotaId,Long modelId) throws Exception ;
	
	
	public void update(RcCondition rcModel) throws Exception;

	public RcCondition save(RcCondition rcModel) throws Exception;
	
	public void saveRcCondition(List<RcCondition> rcConditions,Long modelId)throws Exception;
	
	public void deleteConditionByModelId(long id) throws Exception ;
}
