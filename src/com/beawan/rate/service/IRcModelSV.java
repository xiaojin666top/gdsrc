package com.beawan.rate.service;

import java.util.List;

import com.beawan.rate.entity.RcModel;
import com.beawan.rate.dto.RateModel;

public interface IRcModelSV {
	    public List<RcModel> paginate(RcModel queryCondition,String sortCondition,int start ,int limit)throws Exception;
	
		public int totalCount(RcModel queryCondition)throws Exception;
		
		public RcModel queryById(long id) throws Exception ;
		
		public List<RcModel> queryByStatus(String status) throws Exception ;
		
	
		
		public void update(RcModel rcModel) throws Exception;

		public RcModel save(RcModel rcModel) throws Exception;
		
		public void saveRateModel(RateModel rateModel) throws Exception;
		

}
