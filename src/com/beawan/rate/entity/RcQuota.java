package com.beawan.rate.entity;

import java.util.List;

import com.beawan.rate.dto.RateResult;

public class RcQuota {

	// Fields

	private Long id;

	private String code;
	private String name;

	private String type;
	private String isResult;
	private String unit;
	
	
	private String value;
	private List<RcCondition> conditions;
	private RateResult rateResult;
	private Integer order;
	

	public RcQuota() {
		super();
	}

	public String getIsResult() {
		return isResult;
	}

	public void setIsResult(String isResult) {
		this.isResult = isResult;
	}

	
	


	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}

	public RcQuota(Long id, String code, String name, String type, String isResult) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
		this.type = type;
		this.isResult = isResult;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public List<RcCondition> getConditions() {
		return conditions;
	}

	public void setConditions(List<RcCondition> conditions) {
		this.conditions = conditions;
	}

	public RateResult getRateResult() {
		return rateResult;
	}

	public void setRateResult(RateResult rateResult) {
		this.rateResult = rateResult;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	

}
