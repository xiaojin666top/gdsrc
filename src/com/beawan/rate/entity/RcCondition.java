package com.beawan.rate.entity;

import com.beawan.rate.dto.RateQuotaDto;

public class RcCondition {
	private Long id;
	private Long quotaId;
	private String attrValue;
	private Double upper;
	private Double down;
	private String content;
	private Long resultId;
	private Double resultValue;
	private Long modelId;
	
	private RateQuotaDto condQuota;
	private RateQuotaDto resultQuota;
	
	
	public RcCondition() {
		super();
	}

	public RcCondition(Long id, Long quotaId, String attrValue, double upper, double down, String content,
			Long resultId, Long modelId) {
		super();
		this.id = id;
		this.quotaId = quotaId;
		this.attrValue = attrValue;
		this.upper = upper;
		this.down = down;
		this.content = content;
		this.resultId = resultId;
		this.modelId = modelId;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getQuotaId() {
		return quotaId;
	}
	public void setQuotaId(Long quotaId) {
		this.quotaId = quotaId;
	}
	public String getAttrValue() {
		return attrValue;
	}
	public void setAttrValue(String attrValue) {
		this.attrValue = attrValue;
	}
	
	public Double getUpper() {
		return upper;
	}

	public void setUpper(Double upper) {
		this.upper = upper;
	}

	public Double getDown() {
		return down;
	}

	public void setDown(Double down) {
		this.down = down;
	}

	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Long getResultId() {
		return resultId;
	}
	public void setResultId(Long resultId) {
		this.resultId = resultId;
	}

	public Long getModelId() {
		return modelId;
	}

	public void setModelId(Long modelId) {
		this.modelId = modelId;
	}

	public RateQuotaDto getCondQuota() {
		return condQuota;
	}

	public void setCondQuota(RateQuotaDto condQuota) {
		this.condQuota = condQuota;
	}

	public RateQuotaDto getResultQuota() {
		return resultQuota;
	}

	public void setResultQuota(RateQuotaDto resultQuota) {
		this.resultQuota = resultQuota;
	}

	public Double getResultValue() {
		return resultValue;
	}

	public void setResultValue(Double resultValue) {
		this.resultValue = resultValue;
	}
	
	
	
	
}
