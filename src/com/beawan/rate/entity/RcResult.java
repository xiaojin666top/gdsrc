package com.beawan.rate.entity;


import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 利率测算  具体过程结果
 */

@Entity
@Table(name = "RC_RESULT",schema = "GDTCESYS")
public class RcResult extends BaseEntity {

    @Id
  //  @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="RC_RESULT_SEQ")
   // @SequenceGenerator(name="RC_RESULT_SEQ",allocationSize=1,initialValue=1, sequenceName="RC_RESULT_SEQ")
    private Long id;

    @Column(name = "SER_NO")
    private String serNo;//流水号

    @Column(name = "QUOTA_NAME")
    private String quotaName ;//指标名称

    @Column(name = "CONTENT")
    private String content ;//指标名称

    @Column(name = "REAL_VAL")
    private String realVal;//企业实际值

    @Column(name = "FLOTA_VAL")
    private Double flotaVal;//浮动值

    public RcResult() {
    }

    public RcResult(String serNo, String quotaName, String content, String realVal, Double flotaVal) {
        this.serNo = serNo;
        this.quotaName = quotaName;
        this.content = content;
        this.realVal = realVal;
        this.flotaVal = flotaVal;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getQuotaName() {
        return quotaName;
    }

    public void setQuotaName(String quotaName) {
        this.quotaName = quotaName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getRealVal() {
        return realVal;
    }

    public void setRealVal(String realVal) {
        this.realVal = realVal;
    }

    public Double getFlotaVal() {
        return flotaVal;
    }

    public void setFlotaVal(Double flotaVal) {
        this.flotaVal = flotaVal;
    }
}
