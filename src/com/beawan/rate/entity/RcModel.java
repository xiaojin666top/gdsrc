package com.beawan.rate.entity;

import java.util.List;

public class RcModel {

	private Long id;
	private String name;
	private String createTime;
	private String status;
	private String formula;
	
	private List<RcQuota> quotas;
	
	public RcModel() {
		super();
	}
	public RcModel(Long id, String name, String createTime, String status, String formula) {
		super();
		this.id = id;
		this.name = name;
		this.createTime = createTime;
		this.status = status;
		this.formula = formula;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getFormula() {
		return formula;
	}
	public void setFormula(String formula) {
		this.formula = formula;
	}
	public List<RcQuota> getQuotas() {
		return quotas;
	}
	public void setQuotas(List<RcQuota> quotas) {
		this.quotas = quotas;
	}

	
	
	
	
}
