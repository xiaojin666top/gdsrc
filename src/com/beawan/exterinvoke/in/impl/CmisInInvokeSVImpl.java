package com.beawan.exterinvoke.in.impl;

import java.math.BigDecimal;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.beawan.survey.custInfo.dto.CompBaseDto;
import com.platform.util.*;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.beawan.analysis.finansis.service.IFinanasisService;
import com.beawan.base.entity.SReportData;
import com.beawan.base.entity.SReportRecord;
import com.beawan.base.service.ISysTreeDicSV;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.bean.CusFSRecord;
import com.beawan.customer.dao.ICusBaseDAO;
import com.beawan.customer.utils.CustomerUtil;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.beawan.survey.custInfo.bean.CompBase;
import com.beawan.survey.custInfo.bean.PersonBase;
import com.beawan.survey.custInfo.dao.ICompBaseDAO;
import com.beawan.survey.custInfo.dao.IPersonBaseDAO;

/**
 * 与信贷系统相关方法实现
 * 
 * @author beawan_fengjj
 *
 */
@Service("cmisInInvokeSV")
public class CmisInInvokeSVImpl implements ICmisInInvokeSV {
	
	private static DecimalFormat dformat = new DecimalFormat("0.00");
	private static String sourceFormat = "yyyyMMdd";
	private static String targetFormat = "yyyy-MM-dd";
	
	@Resource
	private ICusBaseDAO cusBaseDAO;
	
	@Resource
	private ICompBaseDAO compBaseDAO;
	
	@Resource
	private IPersonBaseDAO personBaseDAO;
	
	@Resource
	private ISysTreeDicSV sysTreeDicSV;
	
	@Resource
	private IFinanasisService finanasisSV;
	
	@Override
	public List<CusBase> queryCustomerPaging(CusBase queryCondition,
			String orderCondition, int pageIndex, int pageSize) throws Exception {
		
		String sql = "SELECT T.CUSTOMERID AS CUSTOMER_NO,"
				   + " T.CUSTOMERNAME AS CUSTOMER_NAME,"
				   + " T.CUSTOMERTYPE AS CUSTOMER_TYPE"
				   + " FROM ("
		           + " SELECT ROW_NUMBER() OVER(ORDER BY INPUTDATE DESC) AS ROW_NUM,C.*"
		           + " FROM CMIS.CUSTOMER_INFO C"
		           + " WHERE $"
		           + " ) T WHERE T.ROW_NUM > ? AND T.ROW_NUM <=?";
		
		String queryStr = genQueryStr(queryCondition);
		sql = sql.replace("$", queryStr);
		
		if (!StringUtil.isEmptyString(orderCondition))
			sql = sql.replace("OVER(ORDER BY INPUTDATE DESC)", "OVER(ORDER BY " + orderCondition + ")");
		
		int startIndex = pageIndex*pageSize;
		int endIndex = startIndex + pageSize;
		Object[] params = new Object[]{startIndex, endIndex};
		
		return JdbcUtil.executeQuery(sql, Constants.DataSource.TCE_DS, params, CusBase.class);
	}
	
	public long queryCustomerCount(CusBase queryCondition) throws Exception {
		String sql = "SELECT COUNT(*) FROM CMIS.CUSTOMER_INFO WHERE $";
		String queryStr = genQueryStr(queryCondition);
		sql = sql.replace("$", queryStr);
		return JdbcUtil.queryForLong(Constants.DataSource.TCE_DS, sql, null);
	}
	
	private String genQueryStr(CusBase queryCondition) throws Exception {
		
		StringBuffer listSql = new StringBuffer("1=1");
		
		if (queryCondition != null) {
			
			if (!StringUtil.isEmptyString(queryCondition.getCustomerNo())) {
				listSql.append(" AND CUSTOMERID = '")
				       .append(queryCondition.getCustomerNo())
				       .append("'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getCustomerName())) {
				String name = URLDecoder.decode(queryCondition.getCustomerName(), "UTF-8");
				listSql.append(" AND CUSTOMERNAME LIKE '%")
			       .append(name)
			       .append("%'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getCustomerType())) {
				if(SysConstants.CusNoType.PERSON_CUS.equals(queryCondition.getCustomerType()))
					listSql.append(" AND CUSTOMERTYPE LIKE '03%'");
				else
					listSql.append(" AND CUSTOMERTYPE NOT LIKE '03%'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getCertType())) {
				listSql.append(" AND CERTTYPE = '")
			       .append(queryCondition.getCertType())
			       .append("'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getCertCode())) {
				listSql.append(" AND CERTID = '")
			       .append(queryCondition.getCertCode())
			       .append("'");
			}
			
			/*if (!StringUtil.isEmptyString(queryCondition.getManagerUserId())) {
				listSql.append(" AND MANAGERUSERID = '")
			       .append(queryCondition.getManagerUserId())
			       .append("'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getManagerOrgId())) {
				listSql.append(" AND MANAGERORGID = '")
			       .append(queryCondition.getManagerOrgId())
			       .append("'");
			}*/
		}
		
		return listSql.toString();
	}
	
	@Override
	public String getCusType(String cusId) throws Exception {
		
		String customerType = null;
		
		CusBase cusBase = this.queryCusBaseInfo(cusId);
		if(cusBase != null && !StringUtil.isEmptyString(cusBase.getCustomerType())){
			String flag = cusBase.getCustomerType().substring(0, 2);
			if("03".equals(flag))
				customerType = SysConstants.CusNoType.PERSON_CUS;
			else if("01".equals(flag) || "02".equals(flag))
				customerType = SysConstants.CusNoType.COMP_CUS;
			else
				customerType = SysConstants.CusNoType.UNKOWN_CUS;
		}
		
		return customerType;
	}

	@Override
	public CusBase queryCusBaseInfo(String cusId)throws Exception {
		
		CusBase cusBase = null;
		
		String sql = "SELECT"
				   + " CUSTOMERID AS CUSTOMER_NO,"
				   + " CUSTOMERNAME AS CUSTOMER_NAME,"
				   + " CUSTOMERTYPE AS CUSTOMER_TYPE,"
				   + " CERTTYPE AS CERT_TYPE,"
				   + " CERTID AS CERT_CODE,"
				   + " INPUTORGID AS INPUT_ORG_ID,"
				   + " INPUTUSERID AS INPUT_USER_ID,"
				   + " INPUTDATE AS INPUT_DATE,"
				   + " MFCUSTOMERID AS MF_CUSTOMER_NO,"
				   + " STATUS AS STATUS,"
				   + " MANAGERUSERID AS MANAGER_USER_ID,"
				   + " MANAGERORGID AS MANAGER_ORG_ID,"
				   + " BLACKSHEETORNOT AS BLACK_SHEET_OR_NOT,"
				   + " CONFIRMORNOT AS CONFIRM_OR_NOT,"
				   + " CLIENTCLASSN AS CLIENT_CLASS_N,"
				   + " CLIENTCLASSM AS CLIENT_CLASS_M,"
				   + " BUSINESSSTATE AS BUSINESS_STATE,"
				   + " UPDATEDATE AS UPDATE_DATE,"
				   + " REMARK AS REMARK"
				   + " FROM CMIS.CUSTOMER_INFO"
				   + " WHERE CUSTOMERID = '" + cusId + "'";
		
		List<CusBase> list = JdbcUtil.executeQuery(sql, Constants.DataSource.TCE_DS, CusBase.class);
		
		if(!CollectionUtils.isEmpty(list)){
			cusBase = list.get(0);
			cusBase.setInputDate(DateUtil.convert(cusBase.getInputDate(),
					sourceFormat, targetFormat));
			cusBase.setUpdateDate(DateUtil.convert(cusBase.getUpdateDate(),
					sourceFormat, targetFormat));
		}
		
		return cusBase;
	}

	@Override
	public CompBaseDto queryCompBaseInfo(String cusId) throws Exception {

		CompBaseDto compBase = null;
		
		String sql = "SELECT"
				   + " CUSTOMERID AS CUSTOMER_NO,"
				   + " ENTERPRISENAME AS CUSTOMER_NAME,"
				   + " SETUPDATE AS FOUND_DATE,"
				   + " ORGNATURE AS COMPANY_NATURE,"
				   + " SCOPE AS COMPANY_SCOPE,"
				   + " CREDITLEVEL AS CREDIT_LEVEL,"
				   + " SUBSTR (INDUSTRYTYPE,2,4) as INDUSTRY_CODE,"
				   + " FICTITIOUSPERSON AS LEGAL_PERSON,"
				   + " FICTITIOUSPERSONID AS LEGAL_PERSON_CODE,"
				   + " MOSTBUSINESS AS MAIN_BUSINESS,"
				   + " PCCURRENCY AS PC_CURRENCY,"
				   + " PAICLUPCAPITAL AS REAL_CAPITAL,"
				   + " RCCURRENCY AS RC_CURRENCY,"
				   + " REGISTERCAPITAL AS REGISTE_CAPITAL,"
				   + " REGISTERADD AS REGISTE_ADDRESS,"
				   + " OFFICEADD AS RUN_ADDRESS,"
				   + " CORPID AS CERTIFICATE_ORGAN_CODE,"
				   + " LICENSENO AS COMPANY_LICENCE_NUM,"
				   + " BASICBANK AS OPEN_BANK,"
				   + " MYACCOUNTTYPE AS MBANK_ACC_TYPE,"
				   + " MYBANKDORM AS IS_MBANK_HOLDER,"
				   + " EMPLOYEENUMBER AS EMPLOYEE_NUM,"
				   + " MANAGEUSERID AS MANAGE_USER,"
				   + " MANAGEORGID AS MANAGE_ORG,"
				   + " OTHERCREDITLEVEL AS OUT_CREDIT_LEVEL,"
				   + " FINANCEBELONG AS FINANCE_BELONG"
				   + " FROM CMIS.ENT_INFO"
				   + " WHERE CUSTOMERID = '" + cusId + "'";
		
		List<CompBase> list = JdbcUtil.executeQuery(sql, Constants.DataSource.TCE_DS, CompBase.class);
		
		if(!CollectionUtils.isEmpty(list)){
			compBase = MapperUtil.trans(list.get(0), CompBaseDto.class);
			compBase.setFoundDate(DateUtil.convert(compBase.getFoundDate(),
					sourceFormat, targetFormat));
			compBase.setIndustryType(sysTreeDicSV.getFullNameByEnName(
					compBase.getIndustryCode(), SysConstants.TreeDicConstant.GB_IC_4754_2017));
		}
		
		return compBase;
	}

	@Override
	public PersonBase queryPersonBase(String cusId) throws Exception {
		
		PersonBase personBase = null;
		
		String sql = "SELECT"
				   + " CUSTOMERID AS CUSTOMER_NO,"
				   + " FULLNAME AS NAME,"
				   + " SEX AS SEX,"
				   + " CERTTYPE AS CERT_TYPE,"
				   + " CERTID AS CERT_CODE,"
				   + " COUNTRY AS COUNTRY,"
				   + " NATIONALITY AS NATIONALITY,"
				   + " BIRTHDAY AS BIRTH_DATE,"
				   + " NATIVEPLACE AS BIRTH_PLACE,"
				   + " NATIVEADD AS DOMIC_PLACE,"
				   + " FAMILYADD AS LIVING_PLACE,"
				   + " FAMILYADD AS FAMILY_ADDR,"
				   + " MOBILETELEPHONE AS MOBILE_PHONE,"
				   + " MARRIAGE AS MARITAL_STATUS,"
				   + " EDUEXPERIENCE AS EDUCATION,"
				   + " WORKCORP AS WORK_UNIT,"
				   + " HEADSHIP AS POSITION,"
				   + " OCCUPATION AS OCCUPATION,"
				   + " POSITION AS TITLE,"
				   + " YEARINCOME AS ANNUAL_INCOME,"
				   + " CREDITLEVEL AS CREDIT_LEVEL"
				   + " FROM CMIS.IND_INFO "
				   + " WHERE CUSTOMERID = '" + cusId + "'";
		
		List<PersonBase> list = JdbcUtil.executeQuery(sql, Constants.DataSource.TCE_DS,
				PersonBase.class);
		
		if(!CollectionUtils.isEmpty(list)){
			personBase = list.get(0);
			personBase.setBirthday(DateUtil.convert(personBase.getBirthday(),sourceFormat, targetFormat));
		}
		
		return personBase;
	}
	
	@Override
	public void syncCusInfo(final String cusId) throws Exception {
		
		CusBase cusBase = this.queryCusBaseInfo(cusId);
		if(cusBase == null) return;
		
		if(isStockCustomer(cusId))
			cusBase.setBusinessState(SysConstants.CusBusiStatus.STOCK);
		else
			cusBase.setBusinessState(SysConstants.CusBusiStatus.NEW);
		
		cusBase.setSyncTime(DateUtil.formatDate(new Date(), Constants.DATETIME_MASK));
		
		cusBaseDAO.saveOrUpdate(cusBase);
		
		String customerType = cusBase.getCustomerType();
		if(customerType == null || customerType.length() < 2)
			return;
		
		String cusFlag = cusBase.getCustomerType().substring(0, 2);
		
		//对公客户（公司和集团）
		if("01".equals(cusFlag) || "02".equals(cusFlag)){

			CompBaseDto compBase = this.queryCompBaseInfo(cusId);
			if(compBase == null){
				compBase = new CompBaseDto();
				compBase.setCustomerNo(cusBase.getCustomerNo());
				compBase.setCustomerName(cusBase.getCustomerName());
			}
			
			compBase.setCertType(cusBase.getCertType());
			compBase.setCertCode(cusBase.getCertCode());
			compBase.setManageOrg(cusBase.getManagerOrgId());
			compBase.setManageUser(cusBase.getManagerUserId());
			compBase.setStockCustomer(cusBase.getBusinessState());
			
			CompBase oldCompBase = compBaseDAO.queryByCustNo(cusId);
			if(oldCompBase != null){
				BeanUtil.mergeProperties(compBase, oldCompBase, null, true, true);
				compBase.setCustomerNo(null);
			}else {
				oldCompBase = MapperUtil.trans(compBase, CompBase.class);
			}
			
			compBaseDAO.saveOrUpdate(oldCompBase);
			
		}else if("03".equals(cusFlag)){//自然人客户
			
			PersonBase personBase = this.queryPersonBase(cusId);
			if(personBase == null){
				personBase = new PersonBase();
				personBase.setCustomerNo(cusId);
				personBase.setName(cusBase.getCustomerName());
			}
			
			personBase.setCertType(cusBase.getCertType());
			personBase.setCertCode(cusBase.getCertCode());
			personBase.setStockCustomer(cusBase.getBusinessState());
			
			PersonBase oldPersonBase = personBaseDAO.queryByCustNo(cusId);
			if(oldPersonBase != null){
				BeanUtil.mergeProperties(personBase, oldPersonBase, null, true, true);
				personBase.setCustomerNo(null);
			}else
				oldPersonBase = personBase;
			
			personBaseDAO.saveOrUpdate(oldPersonBase);
		}
		
		CustomerUtil.asynCusFSFromCmis(cusId, 0, 0);
	}
	
	/**
	 * TODO 判断是否为存量客户
	 * @param cusId 客户号
	 * @return
	 * @throws Exception
	 */
	private boolean isStockCustomer(String cusId) throws Exception{
		
		String sql = "SELECT COUNT(*) FROM CMIS.BUSINESS_DUEBILL WHERE CUSTOMERID = '" + cusId + "'";
		//从借据表查询客户借据信息
		int count = JdbcUtil.queryForInt(Constants.DataSource.TCE_DS, sql, null);
		//存在借据信息则说明是存量客户
		if(count > 0)
			return true;
		else
			return false;
	}
	
	@Override
	public String queryOrgName(String orgId, boolean isShortName)
			throws Exception {
		
		String orgName = null;
		
		String sql = "SELECT ORGNAME,ORGABBREVI FROM CMIS.ORG_INFO WHERE ORGID = '" + orgId + "'";
		List<Map<String,Object>> list = JdbcUtil.executeQuery(sql, Constants.DataSource.TCE_DS);
		if(!CollectionUtils.isEmpty(list)){
			String fullName = (String) list.get(0).get("orgname");
			String shortName = (String) list.get(0).get("orgabbrevi");
			if(!isShortName)
				orgName = fullName;
			else{
				if(shortName != null && !"".equals(shortName))
					orgName = shortName;
				else{
					String flag1 = "江苏射阳农村商业银行股份有限公司";
					String flag2 = "江苏射阳农村商业银行";
					if(fullName.contains(flag1))
						orgName = fullName.replace(flag1, "");
					else if(fullName.contains(flag2))
						orgName = fullName.replace(flag2, "");
					else
						orgName = fullName;
				}
			}
		}
		
		return orgName;
	}

	@Override
	public String queryOrgName(String orgId) throws Exception {
		return queryOrgName(orgId, true);
	}
	
	@Override
	public double queryLoanBalance(String cusId, String businessType) throws Exception {
		
		String sql = "SELECT BALANCE FROM CMIS.BUSINESS_DUEBILL WHERE CUSTOMERID = '" + cusId + "'";
		
		if(!StringUtil.isEmptyString(businessType))
			sql += " AND BUSINESSTYPE = '" + businessType + "'";
		
		double balance = 0.0;
		List<Object[]> objs = JdbcUtil.execQuery(sql, Constants.DataSource.TCE_DS);
		if(!CollectionUtils.isEmpty(objs)){
			for(int i=0;i<objs.size();i++){
				Object bal = objs.get(i)[0];
				if(bal != null)
					balance += ((BigDecimal)bal).doubleValue()/10000;
			}
		}
		
		return balance;
	}
	
	private String CUS_FS_SQL = "SELECT "
							   + "CUSTOMERID AS CUSTOMER_ID,"
							   + "RECORDNO AS RECORD_NO,"
							   + "REPORTDATE AS REPORT_DATE,"
							   + "REPORTSCOPE AS REPORT_SCOPE,"
							   + "REPORTPERIOD AS REPORT_PERIOD,"
							   + "REPORTCURRENCY AS REPORT_CURRENCY,"
							   + "REPORTUNIT AS REPORT_UNIT,"
							   + "REPORTSTATUS AS REPORT_STATUS,"
							   + "REPORTFLAG AS REPORT_FLAG,"
							   + "REPORTOPINION AS REPORT_OPINION,"
							   + "AUDITFLAG AS AUDIT_FLAG,"
							   + "AUDITOFFICE AS AUDIT_OFFICE,"
							   + "AUDITOPINION AS AUDIT_OPINION,"
							   + "INPUTDATE AS INPUT_DATE,"
							   + "ORGID AS ORG_ID,"
							   + "USERID AS USER_ID,"
							   + "REMARK AS REMARK,"
							   + "REPORTLOCKED AS REPORT_LOCKED,"
							   + "MODELCLASS AS MODEL_CLASS,"
							   + "FINISHDATE AS FINISH_DATE"
							   + " FROM CMIS.CUSTOMER_FSRECORD"
							   + " WHERE $ ORDER BY REPORTDATE DESC";
	
	@Override
	public List<CusFSRecord> queryCusFSRecord(String customerNo, String date) throws Exception {
		
		CusFSRecord condition = new CusFSRecord();
		condition.setCustomerId(customerNo);
		condition.setReportDate(date);
		
		return this.queryCusFSRecord(condition);
	}
	
	@Override
	public List<CusFSRecord> queryCusFSRecord(CusFSRecord condition) throws Exception {
		
		StringBuffer sb = new StringBuffer("1=1  AND REPORTDATE >= '201612'");
		
		if(condition != null){
			
			if(!StringUtil.isEmpty(condition.getCustomerId()))
				sb.append(" AND CUSTOMERID = '")
				  .append(condition.getCustomerId())
				  .append("'");
			
			if(!StringUtil.isEmpty(condition.getReportDate()))
				sb.append(" AND REPORTDATE = '")
				  .append(condition.getReportDate().replace("-", ""))
				  .append("'");
			
			if(!StringUtil.isEmpty(condition.getModelClass()))
				sb.append(" AND MODELCLASS = '")
				  .append(condition.getModelClass())
				  .append("'");
			
			if(!StringUtil.isEmpty(condition.getReportScope()))
				sb.append(" AND REPORTSCOPE = '")
				  .append(condition.getReportScope())
				  .append("'");
		}
		
		String sql = CUS_FS_SQL.replace("$", sb.toString());

		//涉及到了CMIS下的表查询，先关掉
		//List<CusFSRecord> list = JdbcUtil.executeQuery(sql,Constants.DataSource.TCE_DS, CusFSRecord.class);
		List<CusFSRecord> list = null;
		if(!CollectionUtils.isEmpty(list)){
			//转换时间格式
			for(CusFSRecord record : list){
				record.setReportDate(DateUtil.convertNoException(record.getReportDate(), 
						this.getCmisTimeFormat(record.getReportDate()), "yyyy-MM"));
				record.setInputDate(DateUtil.convertNoException(record.getInputDate(), 
						this.getCmisTimeFormat(record.getInputDate()), "yyyy-MM-dd"));
				record.setFinishDate(DateUtil.convertNoException(record.getFinishDate(), 
						this.getCmisTimeFormat(record.getFinishDate()), "yyyy-MM-dd"));
			}
		}
		
		return list;
	}
	
	@Override
	public List<SReportRecord> queryReportRecord(String cusFSRecordNo) throws Exception {
		
//		String sql = "SELECT "
//				   + "REPORTNO AS REPORT_NO,"
//				   + "OBJECTTYPE AS OBJECT_TYPE,"
//				   + "OBJECTNO AS OBJECT_NO,"
//				   + "MODELNO AS MODEL_NO,"
//				   + "REPORTNAME AS REPORT_NAME,"
//				   + "INPUTTIME AS INPUT_TIME,"
//				   + "ORGID AS ORG_ID,"
//				   + "USERID AS USER_ID,"
//				   + "UPDATETIME AS UPDATE_TIME,"
//				   + "VALIDFLAG AS VALID_FLAG"
//				   + " FROM CMIS.REPORT_RECORD"
//				   + " WHERE OBJECTTYPE = 'CustomerReport' AND OBJECTNO = ?0"
//				   + " ORDER BY UPDATETIME DESC";
		
		String sql = "SELECT "
				   + "REPORTNO AS reportNo,"
				   + "OBJECTTYPE AS objectType,"
				   + "OBJECTNO AS objectNo,"
				   + "MODELNO AS modelNo,"
				   + "REPORTNAME AS reportName,"
				   + "INPUTTIME AS inputTime,"
				   + "ORGID AS orgId,"
				   + "USERID AS userId,"
				   + "UPDATETIME AS updateDate,"
				   + "VALIDFLAG AS validFlag"
				   + " FROM CMIS.REPORT_RECORD"
				   + " WHERE OBJECTNO = ?0 AND OBJECTTYPE = 'CustomerReport'" 
				   + " ORDER BY UPDATETIME DESC";
		
		String[] params = new String[]{cusFSRecordNo};
		
		List<SReportRecord> list = cusBaseDAO.findCustListBySql(SReportRecord.class, sql, cusFSRecordNo);
//		List<SReportRecord> list = JdbcUtil.executeQuery(sql,
//				Constants.DataSource.TCE_DS, params, SReportRecord.class);
		
		if(!CollectionUtils.isEmpty(list)){
			//转换时间格式
			for(SReportRecord record : list){
				record.setInputTime(DateUtil.convertNoException(record.getInputTime(), 
						this.getCmisTimeFormat(record.getInputTime()), Constants.DATETIME_MASK));
				record.setUpdateTime(record.getUpdateTime());
			}
		}
		
		return list;
	}
	
	private String getCmisTimeFormat(String timeStr){
		
		String format = null;
		if(timeStr != null){
			int length = timeStr.length();
			switch(length){
				case 6: format = "yyyyMM";break;
				case 8: format = "yyyyMMdd";break;
				case 14: format = "yyyyMMddHHmmss";break;
			}
		}
		return format;
	}
	
	@Override
	public List<SReportData> queryReportData(String reportNo) throws Exception {
		
		String sql = "SELECT "
				   + "REPORTNO AS REPORT_NO,"
				   + "ROWNO AS ROW_NO,"
				   + "ROWNAME AS ROW_NAME,"
				   + "ROWSUBJECT AS ROW_SUBJECT,"
				   + "DISPLAYORDER AS DISPLAY_ORDER,"
				   + "ROWDIMTYPE AS ROW_DIM_TYPE,"
				   + "ROWATTRIBUTE AS ROW_ATTRIBUTE,"
				   + "COL1VALUE AS COL1_VALUE,"
				   + "COL2VALUE AS COL2_VALUE,"
				   + "COL3VALUE AS COL3_VALUE,"
				   + "COL4VALUE AS COL4_VALUE,"
				   + "STANDARDVALUE AS STANDARD_VALUE"
				   + " FROM CMIS.REPORT_DATA"
				   + " WHERE REPORTNO = ?"
				   + " ORDER BY DISPLAYORDER ASC";
		
		String[] params = new String[]{reportNo};
		
		List<SReportData> list = JdbcUtil.executeQuery(sql,
				Constants.DataSource.TCE_DS, params, SReportData.class);
		
		return list;
	}

	//涉及CMIS库，暂时返回null
	@Override
	public List<String> getLastCreditInfo(String customerId) throws Exception {
		
		String sql = "SELECT "
				+"C.CUSTOMERID,C.OCCURDATE,T.TYPENAME AS BUSINESSTYPE,"
				+"C.CREDITCYCLE,C.BUSINESSSUM,C.TERMMONTH,C.BUSINESSRATE,"
				+"C.BAILRATIO,C.BAILSUM,DP.CNNAME AS PAYCYC,C.PURPOSE,"
				+"C.PUTOUTDATE,C.MATURITY,DV.CNNAME AS VOUCHTYPE,C.LOWRISK,"
				+"C.ACTUALPUTOUTSUM,C.BALANCE,C.NORMALBALANCE,"
				+"C.TATIMES,C.LCATIMES,DC.CNNAME AS CLASSIFYRESULT "
				+"FROM CMIS.BUSINESS_CONTRACT C "
				+"LEFT JOIN CMIS.BUSINESS_TYPE T ON T.TYPENO = C.BUSINESSTYPE "
				+"LEFT JOIN GDTCESYS.BS_DIC DP ON DP.OPTTYPE='STD_SY_JB00019' AND DP.ENNAME = C.PAYCYC "
				+"LEFT JOIN GDTCESYS.BS_DIC DV ON DV.OPTTYPE='STD_SY_JB00015' AND DV.ENNAME = C.VOUCHTYPE "
				+"LEFT JOIN GDTCESYS.BS_DIC DC ON DC.OPTTYPE='STD_SY_JB00029' AND DC.ENNAME = C.CLASSIFYRESULT "
				+"WHERE C.CUSTOMERID = '" + customerId + "' AND C.OCCURDATE IS NOT NULL ORDER BY C.OCCURDATE DESC";
		
		//List<Map<String, Object>> list = JdbcUtil.executeQuery(sql, Constants.DataSource.TCE_DS);//切换CMIS数据库，
		List<Map<String, Object>> list = null;
		if(CollectionUtils.isEmpty(list))
			return null;
		
		List<String> infoList = new ArrayList<String>();
		
		String date = (String) list.get(0).get("occurdate");
		
		for(int i=0;i<list.size();i++){
			
			Map<String, Object> map = list.get(i);
			String occurdate = (String) map.get("occurdate");
			if(!date.equals(occurdate))
				break;
			
			StringBuffer sb = new StringBuffer();
				
			sb.append((i+1) + "、");
			
			String businesstype = (String) map.get("businesstype");
			sb.append(businesstype);
			
			String lowrisk = (String) map.get("lowrisk");
			if("Y".equals(lowrisk))
				sb.append("，低风险业务");
			else
				sb.append("，非低风险业务");
			
			Object businesssum = map.get("businesssum");
			sb.append("，授信金额" + dformat.format(Double.parseDouble(businesssum.toString())/10000) + "万元");
			
			String creditcycle = (String) map.get("creditcycle");
			if("Y".equals(creditcycle))
				sb.append("，循环使用");
			else
				sb.append("，一次性使用");
			
			Object termmonth = map.get("termmonth");
			sb.append("，授信期限" + Integer.parseInt(termmonth.toString()) + "个月");
			
			Object businessrate = map.get("businessrate");
			if(businessrate != null)
				sb.append("，年利率" + dformat.format(Double.parseDouble(businessrate.toString())) + "%");
			
			Object bailsum = map.get("bailsum");
			if(bailsum != null){
				sb.append("，保证金" + dformat.format(Double.parseDouble(bailsum.toString())/10000) + "万元");
				Object bailratio = map.get("bailratio");
				if(bailratio != null)
					sb.append("（" + dformat.format(Double.parseDouble(bailratio.toString())) + "%）");
			}
			
			String vouchtype = (String) map.get("vouchtype");
			sb.append("，担保方式为" + vouchtype);
			
			String purpose = (String) map.get("purpose");
			if(purpose != null)
				sb.append("，授信用途为" + purpose);
			
			String paycyc = (String) map.get("paycyc");
			if(paycyc != null)
				sb.append("，还款方式为" + paycyc);
			
			String putoutdate = (String) map.get("putoutdate");
			if(putoutdate != null){
				String tempDate = DateUtil.convert(putoutdate, sourceFormat, targetFormat);
				sb.append("，" + tempDate + "生效");
			}
			
			String maturity = (String) map.get("maturity");
			if(maturity != null){
				String tempDate = DateUtil.convert(maturity, sourceFormat, targetFormat);
				sb.append("，" + tempDate + "到期");
			}
			
			Object actualputoutsum = map.get("actualputoutsum");
			if(actualputoutsum != null)
				sb.append("，实际出账金额" + dformat.format(Double.parseDouble(actualputoutsum.toString())/10000) + "万元");
			
			Object balance = map.get("balance");
			if(balance != null)
				sb.append("，当前余额" + dformat.format(Double.parseDouble(balance.toString())/10000) + "万元");
			
			Object normalbalance = map.get("normalbalance");
			if(normalbalance != null && !normalbalance.equals(balance))
				sb.append("，其中正常余额" + dformat.format(Double.parseDouble(normalbalance.toString())/10000) + "万元");
			
			Object tatimes = map.get("tatimes");
			if(tatimes != null){
				int num = Integer.parseInt(tatimes.toString());
				if(num != 0)
					sb.append("，累计欠款" + num + "期");
			}
			
			Object lcatimes = map.get("lcatimes");
			if(lcatimes != null){
				int num = (int)Double.parseDouble(lcatimes.toString());
				if(num != 0)
					sb.append("，连续欠款" + num + "期");
			}
			
			String classifyresult = (String) map.get("classifyresult");
			if(classifyresult != null)
				sb.append("，五级分类" + classifyresult + "。");
			
			infoList.add(sb.toString());
		}
		
		return infoList;
	}
	
}
