package com.beawan.exterinvoke.in.impl;


import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.beawan.common.Constants;
import com.beawan.exterinvoke.bean.CmisDic;
import com.beawan.exterinvoke.in.ICmisCommOutInvokeSV;
import com.platform.util.JdbcUtil;

/**
 * 
 *  调用信贷系统相关实现
 *  @author beawan_fengjj
 * 
 */
@Service("cmisCommOutInvokeSV")
public class CmisCommOutInvokeSVImpl implements ICmisCommOutInvokeSV{
	
	private static final Logger log = Logger.getLogger(CmisCommOutInvokeSVImpl.class);
	
//	private static final String INTERFACE_PROPS = "/properties/cf/interface.properties";
	
//	private String ip = PropertiesUtil.getProperty(INTERFACE_PROPS, "gxp.credit.ip");
//    
//	private int port = Integer.valueOf(PropertiesUtil.getProperty(INTERFACE_PROPS, "gxp.credit.port"));
	
//	@Resource
//	private ICmisXfOutInvokeSV cmisXfOutInvokeSV;
//	@Resource
//	private ICmisWdOutInvokeSV cmisWdOutInvokeSV;
//	@Resource
//	private ICmisXdOutInvokeSV cmisXdOutInvokeSV;
//	@Resource
//	private ICmisKdOutInvokeSV cmisKdOutInvokeSV;
//	
//	@Resource
//	private IMessageTrandInfoSV trandInfoSV;
//	
//	@Resource
//	private ICustomerDAO customerDAO;

//	@Override
//	public Customer saveOrUpdateCustomer(String custId) throws Exception {
//		
//		StringBuffer sqlString = new StringBuffer();
//		sqlString.append("SELECT B.CUS_ID AS CUS_ID,")
//				 .append("B.CUS_NAME AS CUS_NAME,")
//				 .append("B.CERT_TYPE AS CERT_TYPE,")
//				 .append("B.CERT_CODE AS CERT_CODE,")
//				 .append("B.CUS_TYPE AS CUS_TYPE,")
//				 .append("I.INDIV_SEX AS INDIV_SEX,")
//				 .append("I.INDIV_COUNTRY AS INDIV_COUNTRY,")
//				 .append("I.CUS_CHANGQI_YN AS CUS_CHANGQI_YN,")
//				 .append("I.INDIV_ID_EXP_DT AS INDIV_ID_EXP_DT,")
//				 .append("I.AGRI_FLG AS AGRI_FLG,")
//				 .append("I.CCR_TYPE AS CCR_TYPE,")
//				 .append("I.NEW_PRODUCT AS NEW_PRODUCT,")
//				 .append("I.INDIV_NTN AS INDIV_NTN,")
//				 .append("I.INDIV_HOUH_REG_ADD AS INDIV_HOUH_REG_ADD,")
//				 .append("I.INDIV_DT_OF_BIRTH AS INDIV_DT_OF_BIRTH,")
//				 .append("I.INDIV_EDT AS INDIV_EDT,")
//				 .append("I.INDIV_DGR AS INDIV_DGR,")
//				 .append("I.INDIV_MAR_ST AS INDIV_MAR_ST,")
//				 .append("I.INDIV_HEAL_ST AS INDIV_HEAL_ST,")
//				 .append("I.COM_INIT_LOAN_DATE AS COM_INIT_LOAN_DATE,")
//				 .append("I.CRD_GRADE AS CRD_GRADE,")
//				 .append("I.LOAN_CARD_FLG AS LOAN_CARD_FLG,")
//				 .append("I.LOAN_CARD_ID AS LOAN_CARD_ID,")
//				 .append("I.LOAN_CARD_EFF_FLG AS LOAN_CARD_EFF_FLG,")
//				 .append("I.LOAN_CARD_AUDIT_DT AS LOAN_CARD_AUDIT_DT,")
//				 .append("I.AREA_CODE AS AREA_CODE,")
//				 .append("I.AREA_NAME AS AREA_NAME,")
//				 .append("I.POST_ADDR AS POST_ADDR,")
//				 .append("I.INDIV_RSD_TOYEARS AS INDIV_RSD_TOYEARS,")
//				 .append("I.INDIV_RSD_ST AS INDIV_RSD_ST,")
//				 .append("I.INDIV_ZIP_CODE AS INDIV_ZIP_CODE,")
//				 .append("I.MOBILE AS MOBILE,")
//				 .append("I.ENGAGE_TYPE AS ENGAGE_TYPE,")
//				 .append("I.INDIV_OCC AS INDIV_OCC,")
//				 .append("I.INDIV_COM_NAME AS INDIV_COM_NAME,")
//				 .append("I.INDIV_COM_FLD AS INDIV_COM_FLD,")
//				 .append("I.INDIV_COM_FLD_NAME AS INDIV_COM_FLD_NAME,")
//				 .append("I.INDIV_COM_ADDR AS INDIV_COM_ADDR,")
//				 .append("I.INDIV_COM_PHN AS INDIV_COM_PHN,")
//				 .append("I.INDIV_WORK_JOB_Y AS INDIV_WORK_JOB_Y,")
//				 .append("I.WORK_JOB_YEARS AS WORK_JOB_YEARS,")
//				 .append("I.INDIV_COM_JOB_TTL AS INDIV_COM_JOB_TTL,")
//				 .append("I.INDIV_CRTFCTN AS INDIV_CRTFCTN,")
//				 .append("I.INCOME_CUR_TYPE AS INCOME_CUR_TYPE,")
//				 .append("I.INDIV_ANN_INCM AS INDIV_ANN_INCM,")
//				 .append("I.FARMILY_INCM AS FARMILY_INCM,")
//				 .append("I.PHONE AS PHONE,")
//				 .append("I.INDIV_RSD_ADDR AS INDIV_RSD_ADDR,")
//				 .append("I.INPUT_DATE AS INPUT_DATE,")
//				 .append("I.CUS_STATUS AS CUS_STATUS")
//				 .append(" FROM CUS_BASE B LEFT JOIN CUS_INDIV I ON B.INNER_CUS_ID = I.INNER_CUS_ID")
//				 .append(" WHERE B.CUS_ID=?");
//		Object[] params = {custId};
//		List<Customer> list = JdbcUtil.executeQuery(sqlString.toString(),Constants.DataSource.LS_CS,params,Customer.class);
//		if(list!=null && list.size()>0){
//			Customer customer =  customerDAO.saveOrUpdate(list.get(0));
//			return customer;
//		}else{
//			return null;
//		}
//	}
//	
//	@Override
//	public String queryCusId(String certCode)throws Exception{
//		
//		String sql = "SELECT CUS_ID FROM CUS_BASE WHERE CERT_CODE = '" + certCode + "'";
//		
//		String cusId =  JdbcUtil.queryForString(Constants.DataSource.LS_CS, sql, null);
//		
//		return cusId;
//	}
//	
//	@Override
//	public String queryCusBaseInfo(String custId,String baseInfo)throws Exception{
//		
//		StringBuffer sqlString = new StringBuffer();
//		sqlString.append("SELECT B.")
//				 .append(baseInfo)
//				 .append(" FROM CUS_BASE B")
//				 .append(" WHERE B.CUS_ID=?");
//		Object[] params = {custId};
//		String result =  JdbcUtil.queryForString(Constants.DataSource.LS_CS, sqlString.toString(), params);
//		return result;
//	}
//	
//	@Override
//	public List<XdZxReportRel> queryXdZxReportRel(String surveySerno)throws Exception{
//		Object[] params = {surveySerno};
//		String sqlString = "SELECT * FROM XD_ZX_REPORT_REL Z WHERE Z.SURVEY_SERNO=?";
//		return JdbcUtil.executeQuery(sqlString, Constants.DataSource.LS_CS, params, XdZxReportRel.class);
//	}
//	
//	@Override
//	public Map<String, Object> querySurveyInfo(String surveySerno,String loanType)throws Exception{
//		
//		log.info("从信贷系统查询调查信息-开始");
//		
//		Map<String, Object> dataMap = new HashMap<String, Object>();
//		//综合消费贷
//		if(ExtConstants.CreditSysLoanType.COMP_CONSU_LOAN.equals(loanType)){
//			dataMap =  cmisXfOutInvokeSV.queryCompConsuLoanSurveyInfo(surveySerno);
//		//汽车消费贷
//		}else if(ExtConstants.CreditSysLoanType.CAR_CONSU_LOAN.equals(loanType)){
//			dataMap = cmisXfOutInvokeSV.queryCarConsuLoanSurveyInfo(surveySerno);
//		//创易贷
//		}else if(ExtConstants.CreditSysLoanType.START_BUSIN_LOAN.equals(loanType)){
//			dataMap = cmisXfOutInvokeSV.queryStartBusinLoanSurveyInfo(surveySerno);
//		//经营性微贷
//		}else if(ExtConstants.CreditSysLoanType.MICRO_BUSIN_LOAN.equals(loanType)){
//			dataMap = cmisWdOutInvokeSV.queryMicroBusinLoanSurvey(surveySerno);
//		//经营性小贷
//		}else if(ExtConstants.CreditSysLoanType.SMALL_BUSIN_LOAN.equals(loanType)){
//			dataMap = cmisXdOutInvokeSV.querySmallBusinLoanSurveyInfo(surveySerno);
//		}else if(ExtConstants.CreditSysLoanType.QUICK_BUSIN_LOAN.equals(loanType)){
//			dataMap = cmisKdOutInvokeSV.queryQuickBusinLoanSurveyInfo(surveySerno);
//		}
//		//小贷客户调查信息
//		CusSurvey cusSurvey = this.queryCustSurvey(surveySerno);
//		if(cusSurvey!=null){
//			dataMap.put("xdCusSurvey", cusSurvey);
//		}
//		//征信查询关联记录
//		dataMap.put("xdZxReportRelList",this.queryXdZxReportRel(surveySerno));
//		
//		log.info("从信贷系统查询调查信息-结束");
//		
//		return dataMap;
//	}
//	
//	@Override
//	public CusSurvey queryCustSurvey(String surveySerno){
//		CusSurvey custSurvey = null;
//		try{
//			Object[] params = {surveySerno};
//			String sqlString = "SELECT * FROM XD_CUS_SURVEY X WHERE X.SURVEY_SERNO=?";
//			List<CusSurvey> custSurveyList = JdbcUtil.executeQuery(sqlString, Constants.DataSource.LS_CS, params, CusSurvey.class);
//			if(!CollectionUtils.isEmpty(custSurveyList)){
//				custSurvey = custSurveyList.get(0);
//			}
//		}catch(Exception e){
//			log.error("小贷调查主表信息查询异常", e);
//		}
//		return custSurvey;
//	}
//	
//	@Deprecated
//	@Override
//	public void syncSurveyInfo(String surveySerno,String loanType) throws Exception {
//		
//		//综合消费贷
//		if(ExtConstants.CreditSysLoanType.COMP_CONSU_LOAN.equals(loanType)){
//			cmisXfOutInvokeSV.syncCompConsuLoanSurveyInfo(surveySerno);
//		//汽车消费贷
//		}else if(ExtConstants.CreditSysLoanType.CAR_CONSU_LOAN.equals(loanType)){
//			cmisXfOutInvokeSV.syncCarConsuLoanSurveyInfo(surveySerno);
//		//创意贷
//		}else if(ExtConstants.CreditSysLoanType.START_BUSIN_LOAN.equals(loanType)){
//			cmisXfOutInvokeSV.syncStartBusinLoanSurveyInfo(surveySerno);
//		//经营性微贷
//		}else if(ExtConstants.CreditSysLoanType.MICRO_BUSIN_LOAN.equals(loanType)){
//			cmisWdOutInvokeSV.syncMicroBusinLoanSurvey(surveySerno);
//		//经营性小贷
//		}else if(ExtConstants.CreditSysLoanType.SMALL_BUSIN_LOAN.equals(loanType)){
//			cmisXdOutInvokeSV.syncSmallBusinLoanSurveyInfo(surveySerno);
//		}
//	}
//
//	@Override
//	public double getLoanAccumBal(String cusId, String loanType,
//			String guarWayMain) throws Exception {
//		
//		String sql = "SELECT A.LOAN_BALANCE AS BALANCE FROM ACC_LOAN A"
//				   + " WHERE A.CUS_ID = '" + cusId + "'"
//				   + " AND A.MANAGER_BR_ID = '016000' AND A.ACCOUNT_STATUS IN ('0','1','2','3','4')";
//		
//		if(!StringUtil.isEmptyString(loanType)){
//			//消费性贷款
//			if(LaConstants.LoanType.PERSON_CONSU.equals(loanType)
//					|| LaConstants.LoanType.PERSON_CAR.equals(loanType)){
//				sql += " AND A.BIZ_TYPE IN ('022019','022005','022006','022007','022008','022034')";
//			}
//			//经营性贷款
//			else if(LaConstants.LoanType.PERSON_BUSINESS.equals(loanType)){
//				sql += " AND A.BIZ_TYPE IN ('023001','023003','023004')";
//			}
//		}
//		
//		if(!StringUtil.isEmptyString(guarWayMain)){
//			String guarWayCondition = "";
//			String[] guarWayArray = guarWayMain.split(",");
//			if(guarWayArray.length>0){
//				for (int i=0;i<guarWayArray.length;i++) {
//					guarWayCondition += "'" +guarWayArray[i] + "',";
//				}
//			}
//			if(guarWayCondition.length()>0){
//				guarWayCondition = guarWayCondition.substring(0,guarWayCondition.length()-1);
//			}
//			sql += " AND A.ASSURE_MEANS_MAIN IN(" + guarWayCondition + ")";
//		}
//		
//		double accumBalanSum = 0.0;
//		List<Map<String, Object>> results = JdbcUtil.executeQuery(sql, Constants.DataSource.LS_CS);
//		if(!CollectionUtils.isEmpty(results)){
//			for(Map<String, Object> result : results){
//				BigDecimal bal = (BigDecimal) result.get("balance");
//				if(bal != null)
//					accumBalanSum += bal.doubleValue();
//			}
//		}
//		
//		return accumBalanSum;
//	}
//	
//	@Override
//	public double getLoanAccBalByGuarWay(LaLoanMain loanMain, AppInfo appInfo) throws Exception{
//		
//		// 担保方式相同的台账余额
//		double loanBalanSum = 0.0;
//		
//		String guarWay = appInfo.getGuarTypeMain();
//		//抵质押归为同一类
//		if("10,20".contains(guarWay))
//			guarWay = "10,20";
//		loanBalanSum = getLoanAccumBal(loanMain.getCustomerNo(), loanMain.getLoanType(), guarWay);
//
//		//存量周转
//		if(Constants.ApplyType.CLZZ.equals(loanMain.getApplyType())){
//			
//			if(loanBalanSum > appInfo.getSuggAmt())
//				loanBalanSum = loanBalanSum - appInfo.getSuggAmt();//总余额-需要周转的金额
//			else
//				loanBalanSum = 0;
//			
//		}else if(Constants.ApplyType.CLZZ_AND_XZ.equals(loanMain.getApplyType())){
//			
//			String sql = "SELECT BCSX_AMT FROM XD_CUS_SURVEY WHERE SURVEY_SERNO = ?";
//			//存量周转金额
//			double clzzAmt = JdbcUtil.queryForDouble(Constants.DataSource.LS_CS, sql,
//					new String[]{loanMain.getSerialNumber()});
//			
//			if(loanBalanSum > clzzAmt)
//				loanBalanSum = loanBalanSum - clzzAmt;//总余额-需要周转的金额
//			else
//				loanBalanSum = 0;
//		}
//		
//		return loanBalanSum;
//	}
//	
//	
//	@Override
//	public List<Map<String,Object>> getLoanAccByCusId(String cusId)throws Exception{
//		StringBuffer sql = new StringBuffer("SELECT A.CUS_NAME,A.LOAN_BALANCE AS BALANCE,A.PRD_NAME,")
//				.append("S1.CNNAME AS ASSURE_MEANS_MAIN,(A.REALITY_IR_Y)*100 AS REALITY_IR_Y,")
//				.append("A.LOAN_START_DATE,A.LOAN_END_DATE,S2.CNNAME AS ACCOUNT_STATUS")
//				.append( " FROM ACC_LOAN A")
//				.append( " LEFT JOIN S_DIC S1 ON A.ASSURE_MEANS_MAIN=S1.ENNAME AND S1.OPTTYPE='STD_ZB_ASSURE_MEANS'")
//				.append( " LEFT JOIN S_DIC S2 ON A.ACCOUNT_STATUS=S2.ENNAME AND S2.OPTTYPE='STD_ZB_ACC_STATUS'")
//				.append( " WHERE A.CUS_ID = '" + cusId + "'")
//				.append(" AND A.ACCOUNT_STATUS IN ('1','2','3','4')");
//		List<Map<String, Object>> results = JdbcUtil.executeQuery(sql.toString(), Constants.DataSource.LS_CS);
//		return results;
//	}
//	
//	@Override
//	public List<Map<String,Object>> getLoanAccByCertCode(String certNo)throws Exception{
//		
//		StringBuffer sql = new StringBuffer("SELECT A.LOAN_BALANCE AS BALANCE,A.PRD_NAME,")
//		.append("S1.CNNAME AS ASSURE_MEANS_MAIN,(A.REALITY_IR_Y)*100 AS REALITY_IR_Y,")
//		.append("A.LOAN_START_DATE,A.LOAN_END_DATE,S2.CNNAME AS ACCOUNT_STATUS")
//		.append( " FROM ACC_LOAN A")
//		.append( " LEFT JOIN CUS_BASE C ON A.CUS_ID=C.CUS_ID")
//		.append( " LEFT JOIN S_DIC S1 ON A.ASSURE_MEANS_MAIN=S1.ENNAME AND S1.OPTTYPE='STD_ZB_ASSURE_MEANS'")
//		.append( " LEFT JOIN S_DIC S2 ON A.ACCOUNT_STATUS=S2.ENNAME AND S2.OPTTYPE='STD_ZB_ACC_STATUS'")
//		.append( " WHERE C.CERT_CODE = '" + certNo + "'")
//		.append(" AND A.ACCOUNT_STATUS IN ('1','2','3','4')");
//		List<Map<String, Object>> results = JdbcUtil.executeQuery(sql.toString(), Constants.DataSource.LS_CS);
//		return results;
//	}
//	
//
//	@Override
//	public String getNoCreditLoanMaxLife(String cusId) throws Exception {
//		StringBuffer queryString = new StringBuffer(
//				"SELECT A.LOAN_END_DATE AS LOAN_END_DATE FROM ACC_LOAN A WHERE A.CUS_ID='")
//				.append(cusId)
//				.append("' AND  A.LOAN_BALANCE!=0 AND ASSURE_MEANS_MAIN!='00' ORDER BY A.LOAN_END_DATE DESC");
//		List<Map<String, Object>> list = JdbcUtil.executeQuery(queryString.toString(), Constants.DataSource.LS_CS);
//		if(!CollectionUtils.isEmpty(list)){
//			String loanEndDate = list.get(0).get("loanEndDate").toString();
//			return loanEndDate;
//		}else{
//			return null;
//		}
//	}
//
//	@Override
//	public int getNormalTurnoverNum(String cusId) throws Exception {
//		StringBuffer queryString = new StringBuffer();
//		queryString.append("SELECT COUNT(*) AS NUM FROM ACC_LOAN A WHERE A.CUS_ID='")
//				.append(cusId)
//				.append("' AND  A.ACCOUNT_STATUS='0'");
//		int turnoverNum = JdbcUtil.queryForInt(Constants.DataSource.LS_CS, queryString.toString(), null);
//		return turnoverNum;
//	}

	@Override
	public List<CmisDic> getDicListByType(String opttype) throws Exception {
		StringBuffer queryString = new StringBuffer();
		queryString.append("SELECT S.* FROM S_DIC S WHERE S.OPTTYPE='").append(opttype).append("'");
		List<CmisDic> list = JdbcUtil.executeQuery(queryString.toString() , Constants.DataSource.TCE_DS, CmisDic.class);
		return list;
	}
	
//	@Override
//	public String getDicCnNameByType(String opttype,String enName)throws Exception{
//		StringBuffer queryString = new StringBuffer();
//		queryString.append("SELECT S.CNNAME FROM S_DIC S WHERE S.OPTTYPE='").append(opttype)
//			.append("' AND S.ENNAME='").append(enName).append("' ORDER BY S.ORDERID ASC");
//		String cnName = JdbcUtil.queryForString(Constants.DataSource.LS_CS, queryString.toString(), null);
//		return cnName;
//	}
//
//	@Override
//	public Map<String, String> getDicMapByType(String opttype) throws Exception {
//		Map<String, String> dataMap = new HashMap<String, String>();
//		List<CmisDic> list = this.getDicListByType(opttype);
//		for (CmisDic cmisDic : list) {
//			dataMap.put(cmisDic.getEnname(), cmisDic.getCnname());
//		}
//		return dataMap;
//	}
//
//	@Override
//	public void submitApprResult(String serno, String cusId, String approveResult,
//			ApprovalRecord apprRecord) throws Exception {
//		
//		SAXReader saxReader = new SAXReader();
//		Document document = saxReader.read(MessageUtil.class.getResourceAsStream("/message/credit/preCreditResultSendMsg.xml"));
//		// 获取根元素
//        Element root = document.getRootElement();
//        
//        Element headElement = root.element("head");
//        
//        String prcscd = PropertiesUtil.getProperty(INTERFACE_PROPS, "credit.submitApprResult.TranCode");
//        
//        String trandt = DateUtil.format(DateUtil.getSystemTime(), Constants.DATE_MASK2);
//        
//        String seqnum = UUID.randomUUID().toString().replace("-", "");
//        		
//        MessageUtil.setMessagePublic(headElement, prcscd, trandt, seqnum);
//        
//        Element bodyElement = root.element("body");
//        
//        Element listElement = bodyElement.element("list");
//        
//        String optionFlag = ExtConstants.ApprResultSubmitType.SUBMIT;
//        String approveStatus = "";
//        
//        //审批通过
//        if(ApprovalConstants.ApprovalStatus.APPROVE_PASS.equals(approveResult)){
//        	approveStatus = "997";
//        	if(apprRecord!=null){
//            	Element recordElement = listElement.element("record");
//            	Element sernoElement =  recordElement.element("survey_serno");
//            	sernoElement.addText(serno);
//            	if(!StringUtil.isEmptyString(cusId)){
//	            	Element cusIdElement =  recordElement.element("cus_id");
//	            	cusIdElement.addText(cusId);
//            	}
//            	Element amountElement =  recordElement.element("apply_amount");
//            	if(apprRecord.getApprovalSum()!=null){
//            		amountElement.addText(String.valueOf(apprRecord.getApprovalSum()));
//            		recordElement.element("cur_use_amt").addText(
//            				String.valueOf(apprRecord.getApprovalSum()));
//            	}
//            	if(apprRecord.getApprovalInteRate()!=null){
//            		Element rateElement =  recordElement.element("reality_ir_y");
//                	rateElement.addText(String.valueOf(apprRecord.getApprovalInteRate()));
//            	}
//            	if(apprRecord.getApprovalLife()!=null){
//            		Element termElement =  recordElement.element("apply_term");
//                	termElement.addText(String.valueOf(apprRecord.getApprovalLife()));
//            	}
//            	
//            	Element repayTypeElement =  recordElement.element("repay_type");
//            	repayTypeElement.addText(apprRecord.getApprovalRepayWay());
//            	Element guarWaysElement =  recordElement.element("guar_ways");
//            	guarWaysElement.addText(apprRecord.getApprovalGuarWay());
//            	Element limitTypeElement =  recordElement.element("limit_type");
//            	limitTypeElement.addText(apprRecord.getApprovalLimitType());
//            	Element startDateElement =  recordElement.element("loan_start_date");
//            	String startDate = DateUtil.format(DateUtil.getSystemTime(), Constants.DATE_MASK);
//            	startDateElement.addText(startDate);
//            	Element endDateElement =  recordElement.element("loan_end_date");
//            	double loanLimit = apprRecord.getApprovalLife()!=null ? apprRecord.getApprovalLife() : 0;
//            	String endDate =  DateUtil.format(
//            			DateUtil.addDay(DateUtil.addMonth(DateUtil.getSystemTime(), (int)loanLimit), -1), Constants.DATE_MASK);
//            	endDateElement.addText(endDate);
//            }
//        //打回
//        } else if(ApprovalConstants.ApprovalStatus.APPROVE_TURN_DOWN.equals(approveResult)){
//        	approveStatus = "992";
//        //否决
//        }else if(ApprovalConstants.ApprovalStatus.APPROVE_REFUSE.equals(approveResult)){
//        	approveStatus = "998";
//        }else{
//        	//作废
//        	if(ExtConstants.ApprResultSubmitType.CANCEL.equals(approveResult))
//        		optionFlag = ExtConstants.ApprResultSubmitType.CANCEL;
//        }
//        
//        Element surveySernoElement = bodyElement.element("survey_serno");
//        surveySernoElement.addText(serno);
//        Element approveStatusElement = bodyElement.element("approve_status");
//        approveStatusElement.addText(approveStatus);
//        Element optionFlagElement = bodyElement.element("option_flag");
//        optionFlagElement.addText(optionFlag);
//        
//        String mainMessage = document.asXML();
//        
//        String reqData = mainMessage;
//        
//        String repData = TCPIPSocketUtil.requestServer(ip, port, reqData);//接收的数据
//        
//        //保存报文记录
//        trandInfoSV.saveMessageRecord(prcscd, "信贷系统提交审批结果", 
//        		StringUtil.getTagVaule(repData, "retcod"), 
//        		StringUtil.getTagVaule(repData, "erortx"), repData, mainMessage);
//        
//        MessageUtil.parseCommResultXML(repData);
//	}
//
//	@Override
//	public CreditApprInfo getCreditApprInfo(String cusId) throws Exception {
//		CreditApprInfo creditApprInfo = null;
//		StringBuffer queryString = new StringBuffer();
//		queryString.append("SELECT A.LOAN_AMOUNT AS LOAN_AMOUNT,")
//				.append("A.LOAN_BALANCE AS LOAN_BALANCE,")
//				.append("S.ENNAME AS ASSURE_MEANS_MAIN")
//				.append(" FROM ACC_LOAN A LEFT JOIN S_DIC S ON A.ASSURE_MEANS_MAIN = S.ENNAME")
//				.append(" WHERE A.LOAN_BALANCE>0 AND A.CUS_ID=?");
//		List<Map<String, Object>> list = JdbcUtil.query(Constants.DataSource.LS_CS, queryString.toString(),new Object[]{cusId});
//		if(!CollectionUtils.isEmpty(list)){
//			creditApprInfo = new CreditApprInfo();
//			double lastDqCreditAmt = 0;
//			double lastDqFinaBala = 0;
//			String lastDqGuarWay = "";
//			for (Map<String, Object> map : list) {
//				lastDqCreditAmt += Double.valueOf(map.get("loanAmount").toString());
//				lastDqFinaBala += Double.valueOf(map.get("loanBalance").toString());
//				lastDqGuarWay += map.get("assureMeansMain").toString() + "、";
//			}
//			creditApprInfo.setLastDqCreditAmt(lastDqCreditAmt);
//			creditApprInfo.setLastDqFinaBala(lastDqFinaBala);
//			lastDqGuarWay = lastDqGuarWay.substring(0,lastDqGuarWay.length()-1);
//			creditApprInfo.setLastDqGuarWay(lastDqGuarWay);
//		}
//		return creditApprInfo;
//	}
//
//	@Override
//	public User getUserInfoFromXD(String userNo) throws Exception {
//		StringBuffer queryString = new StringBuffer();
//		queryString.append("SELECT U.ACTORNO AS USER_NO,U.ACTORNAME AS REAL_NAME,")
//				.append("U.TELNUM AS PHONE,U.ORGID AS INSTITUTION_NO")
//				.append(" FROM S_USER U")
//				.append(" WHERE U.ACTORNO=?");
//		List<User> list = JdbcUtil.executeQuery(queryString.toString(), Constants.DataSource.LS_CS, new Object[]{userNo}, User.class);
//		if(!CollectionUtils.isEmpty(list)){
//			return list.get(0);
//		}else{
//			return null;
//		}
//	}
//
//	@Override
//	public List<Map<String, Object>> lousQueryFromHS(String cusId,
//			String settleFlag) throws Exception {
//		SAXReader saxReader = new SAXReader();
//		Document reqDoc = saxReader.read(MessageUtil.class.getResourceAsStream("/message/credit/lousQueryMsg.xml"));
//		// 获取根元素
//        Element reqRoot = reqDoc.getRootElement();
//		
//        Element bodyElement = reqRoot.element("body");
//        Element cusIdElement = bodyElement.element("cus_id");
//        Element flagElement = bodyElement.element("settle_flag");
//        
//        /*String flag = "";
//        if(settleFlag.equals("0"))
//        	flag = "所有";
//        else if(settleFlag.equals("1"))
//        	flag = "结清";
//        else flag = "未结清";*/
//        
//        cusIdElement.addText(cusId);
//        flagElement.addText(settleFlag);
//        
//        List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
//        Map<String, Object> map = null;
//        
//        try {
//	        String reqData = reqDoc.asXML();
//	        String repData = TCPIPSocketUtil.requestServer(ip,port,reqData);//接收的数据 
//	        
//        	Document repDoc = DocumentHelper.parseText(repData);        
//            Element repRoot = repDoc.getRootElement();
//            //Element repDataElement = repRoot.element("Data");
//            Element pubElement = repRoot.element("Pub");
//            String retrce = pubElement.element("errorCode").getTextTrim();
//            
//            if(retrce.equals("9999")) {
//            	System.out.println(pubElement.element("errorMsg").getTextTrim());
//            	log.error(pubElement.element("errorMsg").getTextTrim());
//            	return null;
//            }
//            
//            Element ResElement = repRoot.element("Res");
//            Element listElement = ResElement.element("RealTimeAmtList");
//            List<Element> elements = listElement.elements("MX");
//            
//            if(elements == null || elements.size() < 1) {
//            	System.out.println("list为空");
//            	return null;
//            }
//            
//            for(Element element : elements) {
//            	map = new HashMap<String, Object>();
//            	map.put("bill_id", element.element("bill_id").getText());
//            	map.put("total_term", element.element("total_term").getText());
//            	map.put("curr_term", element.element("curr_term").getText());
//            	map.put("balance", element.element("balance").getText());
//                list.add(map);
//            }
//		} catch (Exception e) {
//			log.error(e);
//			return null;
//		}
//        
//		return list;
//	}
//
//	@Override
//	public List<Map<String, Object>> debitQueryFromHS(String billId,
//			String startDate, String endDate) throws Exception {
//		SAXReader saxReader = new SAXReader();
//		Document reqDoc = saxReader.read(MessageUtil.class.getResourceAsStream("/message/credit/debitQueryMsg.xml"));
//		// 获取根元素
//        Element reqRoot = reqDoc.getRootElement();
//		
//        Element bodyElement = reqRoot.element("body");
//        Element billIdElement = bodyElement.element("bill_id");
//        Element startDateElement = bodyElement.element("start_date");
//        Element endDateElement = bodyElement.element("end_date");
//        
//        billIdElement.addText(billId);
//        startDateElement.addText(startDate);
//        endDateElement.addText(endDate);
//        
//        String reqData = reqDoc.asXML();        
//        String repData = TCPIPSocketUtil.requestServer(ip,port,reqData);//接收的数据        
//        
//        List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
//        Map<String, Object> map = null;
//        
//        try {
//        	Document repDoc = DocumentHelper.parseText(repData);        
//            Element repRoot = repDoc.getRootElement();
//            Element pubElement = repRoot.element("Pub");
//            String retrce = pubElement.element("errorCode").getTextTrim();
//            
//            if(retrce.equals("9999")) {
//            	System.out.println(pubElement.element("errorMsg").getTextTrim());
//            	log.error(pubElement.element("errorMsg").getTextTrim());
//            	return null;
//            }
//            
//            Element ResElement = repRoot.element("Res");
//            Element listElement = ResElement.element("RealTimeAmtList");
//            List<Element> elements = listElement.elements("MX");
//            
//            if(elements == null || elements.size() < 1) {
//            	System.out.println("list为空");
//            	return null;
//            }
//            
//            for(Element recordElement : elements) {
//            	map = new HashMap<String, Object>();
//            	map.put("occ_date", recordElement.element("occ_date").getText());
//            	map.put("term_num", recordElement.element("term_num").getText());
//            	map.put("amt", recordElement.element("amt").getText());
//            	map.put("days", recordElement.element("days").getText());
//                list.add(map);
//            }
//		} catch (Exception e) {
//			log.error(e);
//			return null;
//		}
//        
//		return list;
//	}
//
//	@Override
//	public List<Map<String, Object>> repayQueryFromHS(String billId,
//			String startDate, String endDate) throws Exception {
//		SAXReader saxReader = new SAXReader();
//		Document reqDoc = saxReader.read(MessageUtil.class.getResourceAsStream("/message/credit/repayQueryMsg.xml"));
//		// 获取根元素
//        Element reqRoot = reqDoc.getRootElement();
//		
//        Element bodyElement = reqRoot.element("body");
//        Element billIdElement = bodyElement.element("bill_id");
//        Element startDateElement = bodyElement.element("start_date");
//        Element endDateElement = bodyElement.element("end_date");
//        
//        billIdElement.addText(billId);
//        startDateElement.addText(startDate);
//        endDateElement.addText(endDate);
//        
//        String reqData = reqDoc.asXML();        
//        String repData = TCPIPSocketUtil.requestServer(ip,port,reqData);//接收的数据        
//        
//        List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
//        Map<String, Object> map = null;
//        
//        try {
//        	Document repDoc = DocumentHelper.parseText(repData);        
//            Element repRoot = repDoc.getRootElement();
//            Element pubElement = repRoot.element("Pub");
//            String retrce = pubElement.element("errorCode").getTextTrim();
//            
//            if(retrce.equals("9999")) {
//            	System.out.println(pubElement.element("errorMsg").getTextTrim());
//            	log.error(pubElement.element("errorMsg").getTextTrim());
//            	return null;
//            }
//            
//            Element ResElement = repRoot.element("Res");
//            Element listElement = ResElement.element("RealTimeAmtList");
//            List<Element> elements = listElement.elements("MX");
//            
//            if(elements == null || elements.size() < 1) {
//            	System.out.println("list为空");
//            	return null;
//            }
//            
//            for(Element recordElement : elements) {
//            	map = new HashMap<String, Object>();
//            	map.put("term_num", recordElement.element("term_num").getText());
//            	map.put("repay_date", recordElement.element("repay_date").getText());
//            	map.put("is_third", recordElement.element("is_third").getText());
//                list.add(map);
//            }
//		} catch (Exception e) {
//			log.error(e);
//			return null;
//		}
//        
//		return list;
//	}
//
//	@Override
//	public boolean isFk(String serNo) throws Exception {
//		
//		String sql = "SELECT COUNT(*) FROM CTR_LOAN_CONT CTR,ACC_LOAN ACC " 
//				   + "WHERE  CTR.CONT_NO = ACC.CONT_NO AND CTR.SURVEY_SERNO = ? "
//				   + "AND ACC.ACCOUNT_STATUS IS NOT NULL AND ACC.ACCOUNT_STATUS <> '6'";
//		
//		int num = JdbcUtil.queryForInt(Constants.DataSource.LS_CS, sql, new Object[]{serNo});
//		
//		if(num > 0)
//			return true;
//		else
//			return false;
//	}
//
//	@Override
//	public void submitModifyApprResult(String serno,ApprResultEditRecord editRecord) throws Exception {
//		
//		SAXReader saxReader = new SAXReader();
//		Document document = saxReader.read(MessageUtil.class.getResourceAsStream("/message/credit/znsdsp0002SendMsg.xml"));
//		// 获取根元素
//        Element root = document.getRootElement();
//        
//        Element headElement = root.element("head");
//        
//        String prcscd = PropertiesUtil.getProperty(INTERFACE_PROPS, "credit.submitModifyApprResult.TranCode");
//        
//        String trandt = DateUtil.format(DateUtil.getSystemTime(), Constants.DATE_MASK2);
//        
//        String seqnum = UUID.randomUUID().toString().replace("-", "");
//        		
//        MessageUtil.setMessagePublic(headElement, prcscd, trandt, seqnum);
//        
//        Element bodyElement = root.element("body");
//        
//        //审批通过
//    	if(editRecord!=null){
//        	Element sernoElement =  bodyElement.element("survey_serno");
//        	sernoElement.addText(serno);
//        	Element amountElement =  bodyElement.element("apply_amount");
//        	if(editRecord.getNowAmt()!=null){
//        		amountElement.addText(String.valueOf(editRecord.getNowAmt()));
//        		bodyElement.element("cur_use_amt").addText(
//        				String.valueOf(editRecord.getNowAmt()));
//        	}
//        	if(editRecord.getNowRate()!=null){
//        		Element rateElement =  bodyElement.element("reality_ir_y");
//            	rateElement.addText(String.valueOf(editRecord.getNowRate()));
//        	}
//        	if(editRecord.getNowLimit()!=null){
//        		Element termElement =  bodyElement.element("apply_term");
//            	termElement.addText(String.valueOf(editRecord.getNowLimit()));
//        	}
//        	
//        	Element repayTypeElement =  bodyElement.element("repay_type");
//        	repayTypeElement.addText(editRecord.getNowRepayWay());
//        	Element limitTypeElement =  bodyElement.element("limit_type");
//        	limitTypeElement.addText(editRecord.getNowLimitType());
//        	Element startDateElement =  bodyElement.element("loan_start_date");
//        	String startDate = DateUtil.format(DateUtil.getSystemTime(), Constants.DATE_MASK);
//        	startDateElement.addText(startDate);
//        	Element endDateElement =  bodyElement.element("loan_end_date");
//        	double loanLimit = editRecord.getNowLimit()!=null ? editRecord.getNowLimit() : 0;
//        	String endDate =  DateUtil.format(
//        			DateUtil.addDay(DateUtil.addMonth(DateUtil.getSystemTime(), (int)loanLimit), -1), Constants.DATE_MASK);
//        	endDateElement.addText(endDate);
//        }
//        String mainMessage = document.asXML();
//        
//        String reqData = mainMessage;
//        
//        String repData = TCPIPSocketUtil.requestServer(ip, port, reqData);//接收的数据
//        
//        //保存报文记录
//        trandInfoSV.saveMessageRecord(prcscd, "信贷系统提交修改审批结果", 
//        		StringUtil.getTagVaule(repData, "retcod"), 
//        		StringUtil.getTagVaule(repData, "erortx"), repData, mainMessage);
//        
//        MessageUtil.parseCommResultXML(repData);
//	}
//
//	@Override
//	public double getHouseEvaluAmtByYgjSerno(String ygjSerno) throws Exception {
//		String sql = "SELECT TOTAL FROM XD_WD_GUARANTYJUDGE T WHERE T.SERNO = ?";
//		double houseEvaluAmt = JdbcUtil.queryForDouble(Constants.DataSource.LS_CS, sql, new Object[]{ygjSerno});
//		return houseEvaluAmt;
//	}
//
//	@Override
//	public List<Map<String, Object>> getHouseEvaluInfosByYgjSernos(String ygjSernos)
//			throws Exception {
//		String sql = "SELECT * FROM XD_WD_GUARANTYJUDGE T WHERE T.SERNO IN (" + ygjSernos + ")";
//		List<Map<String, Object>> list  = JdbcUtil.executeQuery(sql, Constants.DataSource.LS_CS);
//		return list;
//	}
	
}
