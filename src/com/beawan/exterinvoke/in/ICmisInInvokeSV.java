package com.beawan.exterinvoke.in;

import java.util.List;

import com.beawan.base.entity.SReportData;
import com.beawan.base.entity.SReportRecord;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.bean.CusFSRecord;
import com.beawan.survey.custInfo.bean.PersonBase;
import com.beawan.survey.custInfo.dto.CompBaseDto;

/**
 * 与信贷系统相关接口
 * @author beawan_fengjj
 */
public interface ICmisInInvokeSV {
	
	/**
	 * @TODO 客户信息分页查询
	 * @param queryCondition 查询条件
	 * @param orderCondition 排序条件
	 * @param index 起始游标
	 * @param count 每页数量
	 * @return
	 * @throws Exception
	 */
	public List<CusBase> queryCustomerPaging(CusBase queryCondition,
			String orderCondition, int pageIndex, int pageSize) throws Exception;
	
	/**
	 * @TODO 查询客户信息记录数
	 * @param queryCondition
	 * @return
	 * @throws Exception
	 */
	public long queryCustomerCount(CusBase queryCondition) throws Exception;
	
	/**
	 * 获取客户类型
	 * @param cusId
	 * @return
	 * @throws Exception
	 */
	public String getCusType(String cusId)throws Exception;
	
	/**
	 * TODO 查询客户基本信息
	 * @param cusId
	 * @return
	 * @throws Exception
	 */
	public CusBase queryCusBaseInfo(String cusId)throws Exception;
	
	/**
	 * TODO 查询公司客户基本信息
	 * @param cusId
	 * @return
	 * @throws Exception
	 */
	public CompBaseDto queryCompBaseInfo(String cusId)throws Exception;
	
	/**
	 * TODO 查询个人客户基本信息
	 * @param cusId
	 * @return
	 * @throws Exception
	 */
	public PersonBase queryPersonBase(String cusId)throws Exception;
	
	/**
	 * TODO 从信贷同步客户信息
	 * @param cusId 客户号
	 * @throws Exception
	 */
	public void syncCusInfo(String cusId) throws Exception;
	
	/**
	 * TODO 根据机构号查询机构名称
	 * @param orgId 机构号
	 * @param isShortName 是否查询简称（默认为简称，若机构无简称则返回全称）
	 * @return
	 * @throws Exception
	 */
	public String queryOrgName(String orgId, boolean isShortName) throws Exception;
	
	/**
	 * TODO 根据机构号查询机构名称
	 * @param orgId 机构号
	 * @return 默认返回机构简称
	 * @throws Exception
	 */
	public String queryOrgName(String orgId) throws Exception;
	
	/**
	 * TODO 查询贷款余额
	 * @param cusId 客户号
	 * @param businessType 信贷业务类型编号
	 * @return
	 * @throws Exception
	 */
	public double queryLoanBalance(String cusId, String businessType) throws Exception;
	
	/**
	 * TODO 查询客户财务报表记录
	 * @param customerNo 客户号
	 * @param date 报表日期
	 * @return
	 * @throws Exception
	 */
	public List<CusFSRecord> queryCusFSRecord(String customerNo, String date) throws Exception;
	
	/**
	 * TODO 查询客户财务报表记录
	 * @param condition 查询条件对象
	 * @return
	 * @throws Exception
	 */
	public List<CusFSRecord> queryCusFSRecord(CusFSRecord condition) throws Exception;
	
	/**
	 * TODO 查询报表记录
	 * @param cusFSRecordNo 客户财务报表记录编号
	 * @return
	 * @throws Exception
	 */
	public List<SReportRecord> queryReportRecord(String cusFSRecordNo) throws Exception;
	
	/**
	 * TODO 查询报表数据
	 * @param reportNo 报表编号
	 * @return
	 * @throws Exception
	 */
	public List<SReportData> queryReportData(String reportNo) throws Exception;
	
	/**
	 * TODO 获得上一次客户授信信息
	 * @param customerId 客户号
	 * @return 返回描述信息列表，可能存在多个产品
	 * @throws Exception
	 */
	public List<String> getLastCreditInfo(String customerId) throws Exception;
	
}
