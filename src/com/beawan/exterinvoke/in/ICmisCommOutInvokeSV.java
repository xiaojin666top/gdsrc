package com.beawan.exterinvoke.in;

import java.util.List;

import com.beawan.exterinvoke.bean.CmisDic;

/**
 * 
 *  调用信贷系统公共方法接口
 *  @author beawan_fengjj
 * 
 */
public interface ICmisCommOutInvokeSV {
	
	/**
	 * 根据客户号从信贷系统查询客户信息
	 * @param custId 客户号
	 * @return
	 * @throws Exception
	 */
//	public Customer saveOrUpdateCustomer(String custId)throws Exception;
	
	/**
	 * 查询XdZxReportRel小贷客户调查与征信报告关系表
	 * @param surveySerno
	 * @return
	 * @throws Exception
	 */
//	public List<XdZxReportRel> queryXdZxReportRel(String surveySerno)throws Exception;
	
	/**
	 * 查询客户基本信息
	 * @param custId 客户号
	 * @param baseInfo 信贷系统CUS_BASE表中的字段
	 * @return
	 * @throws Exception
	 */
//	public String queryCusBaseInfo(String custId,String baseInfo)throws Exception;
	
	/**
	 * 根据证件号码查询客户号
	 * @param certCode 证件号码
	 * @return
	 * @throws Exception
	 */
//	public String queryCusId(String certCode)throws Exception;
	
	/**
	 * 查询调查信息
	 * @param surveySerno 业务流水号
	 * @param loanType 贷款类型
	 * @throws ExceptioloanTypen
	 */
//	public Map<String, Object> querySurveyInfo(String surveySerno,String loanType)throws Exception;
	
	/**
	 * 查询小贷客户调查信息
	 * @param surveySerno
	 * @return
	 */
//	public CusSurvey queryCustSurvey(String surveySerno);
	
	/**
	 * 同步调查信息
	 * @param surveySerno 业务流水号
	 * @param loanType 贷款类型
	 * @throws ExceptioloanTypen
	 */
	@Deprecated
//	public void syncSurveyInfo(String surveySerno,String loanType)throws Exception;
	
	/**
	 * 根据客户号查询担保方式相同的贷款余额之和（单位：元）
	 * @param cusId 客户编号
	 * @param loanType 贷款类型SC：经营贷，PW：消费贷，CM：车贷
	 * @param guarWay 担保方式（为空时不作为查询条件）
	 * @param applyType 申请类型
	 * @param loanAmt 申请金额
	 * @return
	 * @throws Exception
	 */
//	public double getLoanAccBalByGuarWay(LaLoanMain loanMain, AppInfo appInfo)throws Exception;
	
	/**
	 * 根据客户号查询贷款余额之和（单位：元）
	 * @param cusId 客户编号
	 * @param loanType 业务类型（为空时不作为查询条件）
	 * @param guarWay 担保方式（为空时不作为查询条件）
	 * @return
	 * @throws Exception
	 */
//	public double getLoanAccumBal(String cusId, String loanType, String guarWay)throws Exception;
	
	/**
	 * 根据客户号查询现有融资情况
	 * @param cusId
	 * @return
	 * @throws Exception
	 */
//	public List<Map<String,Object>> getLoanAccByCusId(String cusId)throws Exception;
	
	/**
	 * 根据客户证件号查询现有融资情况
	 * @param certNo
	 * @return
	 * @throws Exception
	 */
//	public List<Map<String,Object>> getLoanAccByCertCode(String certNo)throws Exception;
	
	
	/**
	 * 获取非信用方式发放贷款的最长到期日
	 * @param cusId
	 * @return
	 * @throws Exception
	 */
//	public String getNoCreditLoanMaxLife(String cusId)throws Exception;
	
	/**
	 * 获取正常周转次数
	 * @param cusId
	 * @return
	 * @throws Exception
	 */
//	public int getNormalTurnoverNum(String cusId)throws Exception;
	
	/**
	 * 根据字典项类型查询字典项对象列表
	 * @param opttype
	 * @return
	 * @throws Exception
	 */
	public List<CmisDic> getDicListByType(String opttype)throws Exception;
	
	/**
	 * 根据字典项类型和代号查询字典项中文名
	 * @param opttype 字典项类型
	 * @param enName 代号
	 * @return
	 * @throws Exception
	 */
//	public String getDicCnNameByType(String opttype,String enName)throws Exception;
	
	
	/**
	 * 根据字典项类型查询字典项Map集合（KEY：编号，VALUE：名称）
	 * @param opttype
	 * @return
	 * @throws Exception
	 */
//	public Map<String,String> getDicMapByType(String opttype)throws Exception;
	
	/**
	 * 提交审批结果
	 * @param serno 调查报告流水号
	 * @param cusId 客户号码
	 * @param approveResult 审批结果
	 * @param apprRecord 贷款要素
	 * @throws Exception
	 */
//	public void submitApprResult(String serno,String cusId,String approveResult, 
//			ApprovalRecord apprRecord)throws Exception;
//	
	/**
	 * 查询上期批准最高额授信额度及现有融资余额
	 * @param cusId
	 * @return
	 * @throws Exception
	 */
//	public CreditApprInfo getCreditApprInfo(String cusId)throws Exception;
	
	/**
	 * 根据用户编号从信贷系统查询用户信息
	 * @param userNo
	 * @return
	 * @throws Exception
	 */
//	public User getUserInfoFromXD(String userNo)throws Exception;
	
	/**
	 * 从核算系统中查询借据
	 * @param cusId 客户号
	 * @param settleFlag 结清标识
	 * @return
	 * @throws Exception
	 */
//	public List<Map<String, Object>> lousQueryFromHS(String cusId, String settleFlag)throws Exception;

	/**
	 * 从核算系统中查询欠息记录
	 * @param billId 借据号
	 * @param startDate 起始日期
	 * @param endDate 截止日期
	 * @return
	 * @throws Exception
	 */
//	public List<Map<String, Object>> debitQueryFromHS(String billId, String startDate, String endDate)throws Exception;

	/**
	 * 从核算系统中查询还款记录
	 * @param billId 借据号
	 * @param startDate 起始日期
	 * @param endDate 截止日期
	 * @return
	 * @throws Exception
	 */
//	public List<Map<String, Object>> repayQueryFromHS(String billId, String startDate, String endDate)throws Exception;
	
	
	/**
	 * 判断是否放款
	 * @param serNo
	 * @return
	 * @throws Exception
	 */
//	public boolean isFk(String serNo) throws Exception;
	
	
	/**
	 * 提交修改审批结果
	 * @param serno 调查报告流水号
	 * @param apprRecord 贷款要素
	 * @throws Exception
	 */
//	public void submitModifyApprResult(String serno,ApprResultEditRecord editRecord)throws Exception;
	
	/**
	 * 根据云估计流水号查询房屋估价
	 * @param ygjSerno
	 * @return
	 * @throws Exception
	 */
//	public double getHouseEvaluAmtByYgjSerno(String ygjSerno)throws Exception;
	
	/**
	 * 根据云估计流水号查询房屋信息
	 * @param ygjSernos
	 * @return
	 * @throws Exception
	 */
//	public List<Map<String, Object>> getHouseEvaluInfosByYgjSernos(String ygjSernos)throws Exception;
	
}
