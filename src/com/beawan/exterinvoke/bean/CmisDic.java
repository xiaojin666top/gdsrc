package com.beawan.exterinvoke.bean;
/**
 * 信贷系统字典项信息
 * @author beawan_fengjj
 *
 */
public class CmisDic {
	
	private String pk1;//主键
	
	private String enname;//字典项名称
	
	private String cnname;//字典项中文名称
	
	private String opttype;//字典项类型
	
	private String memo;//字典项说明
	
	private String flag;//字典项标识
	
	private String levels;//字典项等级
	
	private Long orderid;//字典项排序编号

	public String getPk1() {
		return pk1;
	}

	public void setPk1(String pk1) {
		this.pk1 = pk1;
	}

	public String getEnname() {
		return enname;
	}

	public void setEnname(String enname) {
		this.enname = enname;
	}

	public String getCnname() {
		return cnname;
	}

	public void setCnname(String cnname) {
		this.cnname = cnname;
	}

	public String getOpttype() {
		return opttype;
	}

	public void setOpttype(String opttype) {
		this.opttype = opttype;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getLevels() {
		return levels;
	}

	public void setLevels(String levels) {
		this.levels = levels;
	}

	public Long getOrderid() {
		return orderid;
	}

	public void setOrderid(Long orderid) {
		this.orderid = orderid;
	}

}
