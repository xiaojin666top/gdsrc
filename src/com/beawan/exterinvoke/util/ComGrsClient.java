package com.beawan.exterinvoke.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.io.IOUtils;

import com.beawan.common.Constants;
import com.geor.grs.client.GrsClient;
import com.geor.grs.client.GrsClientDefault;
import com.platform.util.PropertiesUtil;

public class ComGrsClient {
	
	public static String FOLDER_HOME;
	
	public static String CATALOG; // 分类名
	
	public static String HOSTNAME; //FTP服务器hostname
	
	public static int PORT; //FTP服务器端口
	
	private static LinkedBlockingQueue<GrsClient> grsClientQueue;
	
	private static int queueMaxSize = 20; //队列最大值
	private static int queueCount = 0; //队列元素计数器
	
	static {
		FOLDER_HOME = PropertiesUtil.getProperty(Constants.SYSTEM_PROPS_PATH, "image.temp.path");
		File folder = new File(FOLDER_HOME);
		if(!folder.exists())
			folder.mkdirs();
		CATALOG = PropertiesUtil.getProperty(Constants.SYSTEM_PROPS_PATH, "image.server.catalog");
		HOSTNAME = PropertiesUtil.getProperty(Constants.SYSTEM_PROPS_PATH, "image.server.hostname");
		PORT = Integer.valueOf(PropertiesUtil.getProperty(Constants.SYSTEM_PROPS_PATH, "image.server.port"));
		
		grsClientQueue = new LinkedBlockingQueue<GrsClient>(queueMaxSize);
	}
	
	private static synchronized GrsClient getGrsClient() throws InterruptedException {
		GrsClient grsClient;
		if(grsClientQueue.isEmpty() && queueCount < queueMaxSize){
			grsClient = new GrsClientDefault();
			grsClient.setHost(HOSTNAME, PORT);
			queueCount++;
		}else
			grsClient = grsClientQueue.take();
		return grsClient;
	}
	
	/**
	 * TODO 上传文件至影像平台
	 * @param filePath  文件绝对路径全称
	 * @return 返回文件资源编号
	 * @throws Exception
	 */
	public static String putFile(String filePath) throws Exception {
		GrsClient grsClient = getGrsClient();
		String resourceId = grsClient.putFile(CATALOG, filePath);
		//回收放入队列中
		grsClientQueue.add(grsClient);
		return resourceId;
	}
	
	/**
	 * TODO 上传文件至影像平台
	 * @param file 文件对象
	 * @return 返回文件资源编号
	 * @throws Exception
	 */
	public static String putFile(File file) throws Exception {
		return putFile(file.getAbsolutePath());
	}
	
	/**
	 * TODO 上传文件至影像平台
	 * @param inputStream 文件输入流对象
	 * @return 返回文件资源编号
	 * @throws Exception
	 */
	public static String putFile(InputStream inputStream) throws Exception {
		GrsClient grsClient = getGrsClient();
		String resourceId = grsClient.putFile(CATALOG, inputStream);
		//回收放入队列中
		grsClientQueue.add(grsClient);
		return resourceId;
	}
	
	/**
	 * TODO 从影像平台下载文件
	 * @param resourceId 资源编号
	 * @param filePath 本地存放绝对路径
	 * @return 返回文件存储绝对路径
	 * @throws Exception
	 */
	public static String getFile(String resourceId, String filePath) throws Exception {
		GrsClient grsClient = getGrsClient();
		String fileAbsPath = grsClient.getFile(resourceId, filePath);
		//回收放入队列中
		grsClientQueue.add(grsClient);
		return fileAbsPath;
	}
	
	public static String getFile(String resourceId) throws Exception {
		return getFile(resourceId, FOLDER_HOME+resourceId);
	}
	
	/**
	 * TODO 从影像平台下载文件
	 * @param resourceId 资源编号
	 * @param outputStream 输出流编号
	 * @return 
	 * @throws Exception
	 */
	public static void getFile(String resourceId, OutputStream outputStream) throws Exception {
		GrsClient grsClient = getGrsClient();
		grsClient.getFile(resourceId, outputStream);
		//回收放入队列中
		grsClientQueue.add(grsClient);
	}

	public static String putFileTest(File file) throws IOException{
		
		String resourceId = UUID.randomUUID().toString();
		
		File newFile = new File(FOLDER_HOME + resourceId);
		if(!newFile.exists())
			newFile.createNewFile();
		InputStream input = new FileInputStream(file);
		OutputStream output = new FileOutputStream(newFile);
		
		IOUtils.copy(input, output);
		
		return resourceId;
	}
	
	public static String getFileTest(String resourceId) throws IOException{
		
		return FOLDER_HOME + resourceId;
	}
	
}
