package com.beawan.exterinvoke.util;

import java.util.Map;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.beawan.exterinvoke.bean.FncStatMapModel;
import com.platform.util.JacksonUtil;

public class FncStatMapUtil {

	//private static FncStatMapModel fncStatMapModel;
	
	private static Map<String, ?> finaModelMap;
	
	private static Map<String, ?> finaModelSubjectMap;

	/**
	 * 在调查报告中显示的主要财务科目指标
	 */
	private static Map<String, ?> finaModelImportMap;
	
	private static void loadData(){
		
		try{
			
			SAXReader saxReader = new SAXReader();
			Document document = saxReader.read(FncStatMapUtil.class
					.getResourceAsStream("/biz/fina_report.xml"));
			// 获取根元素
		    Element root = document.getRootElement();
		    
		    /*Element modelElement = (Element) root.selectSingleNode("model[@name='fnc_stat']");
		    String jsonString = modelElement.getTextTrim();
		    FncStatMapUtil.fncStatMapModel = JacksonUtil.fromJson(jsonString, FncStatMapModel.class);*/
		    
		    Element modelElement = (Element) root.selectSingleNode("model[@name='fina_model_map']");
		    String jsonString = modelElement.getTextTrim();
		    finaModelMap = JacksonUtil.fromJson(jsonString, Map.class);
		    
		    modelElement = (Element) root.selectSingleNode("model[@name='fina_model_sub_map']");
		    jsonString = modelElement.getTextTrim();
		    finaModelSubjectMap = JacksonUtil.fromJson(jsonString, Map.class);

			modelElement = (Element) root.selectSingleNode("model[@name='fina_model_import_map']");
			jsonString = modelElement.getTextTrim();
			finaModelImportMap = JacksonUtil.fromJson(jsonString, Map.class);

		}catch(Exception e){
			e.printStackTrace();
		}
	}


	/**
	 * 获取整个数据报文段
	 * @return
	 * @throws Exception
	 */
	public static Map<String, ?> getFinaModelSubjectMap() throws Exception{
		if(finaModelSubjectMap == null)
			loadData();
	    return finaModelSubjectMap;
	}
	
	public static Map<String, ?> getFinaModelMap() throws Exception{
		if(finaModelMap == null)
			loadData();
		return finaModelMap;
	}

	public static Map<String, ?> getFinaModelImportMap() throws Exception{
		if(finaModelImportMap == null)
			loadData();
		return finaModelImportMap;
	}

	/**
	 * 根据modelclass和报表类型获取对应字段
	 * @param modelClass
	 * @param reportType
	 * @return
	 */
	public static String getModelNo(String modelClass, String reportType){
		if(finaModelMap == null)
			loadData();
		return (String)((Map)finaModelMap.get(modelClass)).get(reportType);
	}
	
	public static Map<String,String> getModelSubjectMap(String modelClass, String reportType){
		if(finaModelSubjectMap == null)
			loadData();
		return (Map<String, String>) ((Map)finaModelSubjectMap.get(modelClass)).get(reportType);
	}

	public static Map<String,String> getImportMap(String modelClass, String reportType){
		if(finaModelImportMap == null)
			loadData();
		return (Map<String, String>) ((Map)finaModelImportMap.get(modelClass)).get(reportType);
	}
	
	public static void main(String[] args) throws Exception {
		//System.out.println(getModelNo("010", "name"));
		System.out.println(getModelSubjectMap("010", "balance"));
	}
	
}
