package com.beawan.loanAfter.timer;

import java.net.URLDecoder;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import com.beawan.dtmp.service.DtmpService;
import com.beawan.loanAfter.service.FcTaskService;
import com.beawan.loanAfter.service.LfTaskService;
import com.platform.util.DateUtil;

/**
 * 创建贷后任务定事情
 */
public class LfTaskTimer {
	
	private static final Logger log = Logger.getLogger(LfTaskTimer.class);
	

	@Resource
	private DtmpService dtmpService;
	@Resource
	private LfTaskService lfTaskService;
    @Resource
    private FcTaskService fcTaskService;
	
	public void createLfTask(){
		try {
			log.info(DateUtil.getNowYmdStr()+"------------客户数据及贷后任务生成跑批任务启动");
			log.info("开始同步客户数据");
			dtmpService.syncCustBase();
			log.info("同步客户数据成功");
			
			log.info("开始同步贷后客户数据");
			dtmpService.syncLfCustInfo();
			log.info("同步贷后客户数据成功");
			
			log.info("开始更新贷后客户数据");
			dtmpService.updateLfCustInfo();
			log.info("更新代后客户数据成功");
			//3个月到期
//			lfTaskService.syncFXThreeMonthTask(3);
//			log.info("创建三个月到期任务成功");
			//首检任务
			lfTaskService.syncFifteenDayTask(null);
			log.info("创建15日首检任务成功");
//			//日常任务
			lfTaskService.syncGHDayTask();
			log.info("创建管护日常任务成功");
			//风险预警任务
			lfTaskService.syncFXWarningTask(null);
			log.info("创建风险预警任务成功");

			log.info(DateUtil.getNowYmdStr()+"------------客户数据及贷后任务生成跑批任务结束");
		} catch (Exception e) {
			log.error("创建贷后任务失败！异常原因：", e);
			e.printStackTrace();
		}
	}
	
	public void createFXThreeMonthTask(){
		try {
			//3个月到期
			lfTaskService.syncFXThreeMonthTask2(null);
			log.info("创建三个月到期任务成功");
		}catch(Exception e){
			log.info("到期检查任务创建失败!");
			e.printStackTrace();
		}
	}
	
	public void createFiveClassTask(){
		int year = new Date().getYear();
		int month = new Date().getMonth();
		String taskName = year+"年";
		if(month >=0 && month <=3){
			taskName += "第一季度五级分类任务";
		}else if(month <=6){
			taskName += "第二季度五级分类任务";
		}else if(month <=9){
			taskName += "第三季度五级分类任务";
		}else if(month <=12){
			taskName += "第四季度五级分类任务";
		}
		try {
			fcTaskService.batchFcTask(taskName, "DGZJ", "TIMER");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	
}
