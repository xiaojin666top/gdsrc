package com.beawan.loanAfter.service;

import java.util.List;

import com.beawan.core.BaseService;
import com.beawan.loanAfter.entity.LfRiskWarningInfo;

/**
 * @author yzj
 */
public interface LfRiskWarningInfoService extends BaseService<LfRiskWarningInfo> {
	
	/**
	 * 批量保存
	 * @param saveList
	 * @throws Exception
	 */
	void batchSaveUpdata(List<LfRiskWarningInfo> saveList)throws Exception;
}
