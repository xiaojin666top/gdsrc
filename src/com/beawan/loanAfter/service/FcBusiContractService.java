package com.beawan.loanAfter.service;

import java.util.List;

import com.beawan.core.BaseService;
import com.beawan.loanAfter.entity.FcBusiContract;

/**
 * @author yzj
 */
public interface FcBusiContractService extends BaseService<FcBusiContract> {
	
	/**
	 * 根据taskCustId 获取贷款信息
	 * @param taskCustId
	 * @return
	 */
	List<FcBusiContract> getByTaskCustId(Integer taskCustId);
}
