package com.beawan.loanAfter.service;

import java.util.List;

import com.beawan.core.BaseService;
import com.beawan.core.Pagination;
import com.beawan.loanAfter.dto.LfClassifyVo;
import com.beawan.loanAfter.dto.LfCustDto;
import com.beawan.loanAfter.entity.LfCustInfo;

/**
 * @author yzj
 */
public interface LfCustInfoService extends BaseService<LfCustInfo> {
	/**
	 * 
	 *@Description 获取客户分类的分页信息
	 *@param page
	 *@param pageSize
	 *@return
	 *@throws Exception
	 * @author xyh
	 */
	Pagination<LfCustDto> getLfCustPage(int page, int pageSize, LfClassifyVo condition)throws Exception;
	
	/**
	 * 
	 *@Description 获取客户分类信息列表，用于生成excel
	 *@return
	 *@throws Exception
	 * @author xyh
	 */
	List<LfCustDto> getLfCustList()throws Exception;
}
