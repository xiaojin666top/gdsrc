package com.beawan.loanAfter.service;

import java.util.List;

import com.beawan.core.BaseService;
import com.beawan.loanAfter.entity.LfSituationCheck;

/**
 * @author yzj
 */
public interface LfSituationCheckService extends BaseService<LfSituationCheck> {
	/**
	 * 
	 *@Description 批量插入数据
	 *@param taskId 任务号
	 *@param typeId 插入数据类型 ---- RG_LF_JY_01
	 *@param typeName 插入数据名 ----- 贷后-经营情况-外部环境
	 *@throws Exception
	 * @author xyh
	 */
	//void insertLfSituationcheck(Integer taskId,String typeId,String typeName)throws Exception;
	/**
	 * 批量保存
	 * @param saveList
	 * @throws Exception
	 */
	void batchSaveUpdata(List<LfSituationCheck> saveList)throws Exception;
	/**
	 * 
	 *@Description 根据贷后任务id和检查大类获取数据
	 *@param typeFirstNo
	 *@param taskId
	 *@throws Exception
	 * @author xyh
	 */
	List<LfSituationCheck> queryByTypeFirstLike(String typeFirstNo,Integer taskId)throws Exception;
}
