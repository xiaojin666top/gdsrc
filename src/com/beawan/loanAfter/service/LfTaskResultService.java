package com.beawan.loanAfter.service;

import com.beawan.core.BaseService;
import com.beawan.loanAfter.entity.LfTaskResult;

/**
 * @author yzj
 */
public interface LfTaskResultService extends BaseService<LfTaskResult> {
}
