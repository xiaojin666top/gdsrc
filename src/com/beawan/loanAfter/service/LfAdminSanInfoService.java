package com.beawan.loanAfter.service;

import java.util.List;

import com.beawan.core.BaseService;
import com.beawan.loanAfter.entity.LfAdminSanInfo;

/**
 * @author yzj
 */
public interface LfAdminSanInfoService extends BaseService<LfAdminSanInfo> {
	/**
	 * 批量保存
	 * @param saveList
	 * @throws Exception
	 */
	void batchSaveUpdata(List<LfAdminSanInfo> saveList)throws Exception;
}
