package com.beawan.loanAfter.service;

import java.util.List;

import com.beawan.core.BaseService;
import com.beawan.loanAfter.entity.LfPaymentOfFees;

/**
 * @author yzj
 */
public interface LfPaymentOfFeesService extends BaseService<LfPaymentOfFees> {
	/**
	 * 
	 *@Description
	 *@param saveList
	 *@throws Exception
	 * @author xyh
	 */
	void batchSaveUpdata(List<LfPaymentOfFees> saveList)throws Exception;
}
