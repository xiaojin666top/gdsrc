package com.beawan.loanAfter.service;

import com.beawan.core.BaseService;
import com.beawan.core.Pagination;
import com.beawan.loanAfter.entity.LfRiskSort;

/**
 * @author yzj
 */
public interface LfRiskSortService extends BaseService<LfRiskSort> {
	/**
     *获取相应分类中可用的风险要点（状态正常）	
     *
     */	
	Pagination<LfRiskSort> queryPageList(String riskNo, int page, int rows) throws Exception;
}
