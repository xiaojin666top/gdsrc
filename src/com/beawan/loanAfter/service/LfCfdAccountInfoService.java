package com.beawan.loanAfter.service;

import java.util.List;

import com.beawan.core.BaseService;
import com.beawan.loanAfter.entity.LfCfdAccountInfo;

/**
 * @author yzj
 */
public interface LfCfdAccountInfoService extends BaseService<LfCfdAccountInfo> {
	/**
	 * 批量保存
	 * @param saveList
	 * @throws Exception
	 */
	void batchSaveUpdata(List<LfCfdAccountInfo> saveList)throws Exception;
	
	/**
	 * 同步查冻扣账户
	 * @param taskId		贷后任务号
	 * @param mfCustNo		核心客户号
	 * @throws Exception
	 */
	void syncCfdAccount(Integer taskId, String mfCustNo) throws Exception;
}
