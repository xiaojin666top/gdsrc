package com.beawan.loanAfter.service;

import com.beawan.core.BaseService;
import com.beawan.loanAfter.entity.LfCreditSelfBank;

/**
 * @author yzj
 */
public interface LfCreditSelfBankService extends BaseService<LfCreditSelfBank> {
}
