package com.beawan.loanAfter.service;

import java.util.List;

import com.beawan.core.BaseService;
import com.beawan.core.Pagination;
import com.beawan.loanAfter.dto.CmisLargeAffirmDto;
import com.beawan.loanAfter.dto.CmisSmallAffirmDto;
import com.beawan.loanAfter.dto.CmisWorkPaperDto;
import com.beawan.loanAfter.entity.FcTask;

/**
 * @author yzj
 */
public interface FcTaskService extends BaseService<FcTask> {
	
	/**
	 * 获取贷后五级分类任务列表
	 * @param posi
	 * @param page
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	Pagination<FcTask> getFcTaskList(Integer posi, String taskName, String createrName, int page, int pageSize) throws Exception;
	
	/**
	 * 创建五级分类任务
	 * @param taskName	任务名称
	 * @param user		操作人
	 * @throws Exception
	 */
	void batchFcTask(String taskName, String userId, String userName) throws Exception;
	
	/**
	 * 获取五级分类工作底稿数据
	 * 
	 */
	CmisWorkPaperDto getCmisCustInfoFull(String xdCustNo) throws Exception;
	
	/**
	 * 获取五级分类工作底稿数据，数据转换
	 * 
	 */
	List<String> getCmisSpecialAuth(CmisWorkPaperDto cmisCustFullDto) throws Exception;

	/**
	 * 获取五级分类   大额认定表
	 * @param full		工作底稿中的部分数据
	 * @return
	 * @throws Exception
	 */
	CmisLargeAffirmDto getCmisLargeAffirm(CmisWorkPaperDto full) throws Exception;

	/**
	 * 获取五级分类   小额认定表
	 * @param full
	 * @throws Exception
	 */
	CmisSmallAffirmDto getCmisSmallAffirm(CmisWorkPaperDto full) throws Exception;
	/**
	 * 生成五级分类工作底稿，五级分类认定表
	 * @param full
	 * @return 
	 * @throws Exception
	 */
	String generateReport(String taskName, String customerName, String xdCustNo) throws Exception;

}
