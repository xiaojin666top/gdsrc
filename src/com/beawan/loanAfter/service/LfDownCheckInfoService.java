package com.beawan.loanAfter.service;

import com.beawan.core.BaseService;
import com.beawan.loanAfter.entity.LfDownCheckInfo;

/**
 * @author yzj
 */
public interface LfDownCheckInfoService extends BaseService<LfDownCheckInfo> {
}
