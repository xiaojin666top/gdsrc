package com.beawan.loanAfter.service;

import java.util.List;

import com.beawan.core.BaseService;
import com.beawan.loanAfter.entity.LfProjectInfo;

/**
 * @author yzj
 */
public interface LfProjectInfoService extends BaseService<LfProjectInfo> {
	
	void batchSaveUpdata(List<LfProjectInfo> saveList)throws Exception;
}
