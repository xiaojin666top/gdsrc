package com.beawan.loanAfter.service;

import com.beawan.core.BaseService;
import com.beawan.loanAfter.entity.LfLocalResult;

/**
 * @author yzj
 */
public interface LfLocalResultService extends BaseService<LfLocalResult> {
}
