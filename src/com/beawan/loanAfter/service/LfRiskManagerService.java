package com.beawan.loanAfter.service;

import com.beawan.core.BaseService;
import com.beawan.core.Pagination;
import com.beawan.loanAfter.entity.LfRiskManager;

/**
 * @author yzj
 */
public interface LfRiskManagerService extends BaseService<LfRiskManager> {

	/**
	 * 分页获取机构下设定的风险经理
	 * @param userNo
	 * @param userName
	 * @param orgName
	 * @param page
	 * @param rows
	 * @return
	 */
	Pagination<LfRiskManager> getRiskManagerPager(String userNo, String userName, String orgName, int page, int rows);
}
