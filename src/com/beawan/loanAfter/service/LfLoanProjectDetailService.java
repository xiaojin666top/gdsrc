package com.beawan.loanAfter.service;

import java.util.List;

import com.beawan.core.BaseService;
import com.beawan.loanAfter.entity.LfLoanProjectDetail;

/**
 * @author yzj
 */
public interface LfLoanProjectDetailService extends BaseService<LfLoanProjectDetail> {
	void batchSaveUpdata(List<LfLoanProjectDetail> saveList)throws Exception;
}
