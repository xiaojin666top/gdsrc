package com.beawan.loanAfter.service;

import java.util.Map;

public interface LfReportService {
	/**
	 * 
	 *@Description 获取调查报告信息
	 *@param taskId
	 *@return
	 *@throws Exception
	 * @author xyh
	 */
	Map<String, Object> getReportInfo(Integer taskId)throws Exception;
}
