package com.beawan.loanAfter.service;

import com.beawan.core.BaseService;
import com.beawan.loanAfter.entity.LfLoanProjectSummary;

/**
 * @author yzj
 */
public interface LfLoanProjectSummaryService extends BaseService<LfLoanProjectSummary> {
}
