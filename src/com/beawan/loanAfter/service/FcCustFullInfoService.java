package com.beawan.loanAfter.service;

import java.util.Map;

import com.beawan.core.BaseService;
import com.beawan.loanAfter.dto.FcAnalysisDto;
import com.beawan.loanAfter.dto.FcCustDto;
import com.beawan.loanAfter.entity.FcCustFullInfo;

/**
 * @author yzj
 */
public interface FcCustFullInfoService extends BaseService<FcCustFullInfo> {
	
	/**
	 * 获取企业基本信息
	 * @param taskCustId
	 * @return
	 */
	FcCustDto getByTaskCustId(Integer taskCustId);
	
	/**
	 * 获取分析数据
	 * @param taskCustId
	 * @return
	 */
	FcAnalysisDto getFcAnalysis(Integer taskCustId);
	
	/**
	 * 获取客户存贷款信息
	 * @param taskCustId
	 * @return
	 */
	String getLoanAnaly(Integer taskCustId);
	
	/**
	 * 获取 财务比率分析信息
	 * @param taskCustId
	 * @return
	 */
	String getFinaAnaly(Integer taskCustId);
	
	/**
	 * 获取 五级分类报告数据
	 * @param taskCustId
	 * @return
	 */
	Map<String, Object> getFcReportData(Integer taskCustId) throws Exception ;
}
