package com.beawan.loanAfter.service;

import java.util.List;

import com.beawan.core.BaseService;
import com.beawan.core.Pagination;
import com.beawan.loanAfter.dto.LfCustDto;
import com.beawan.loanAfter.dto.LfTaskDto;
import com.beawan.loanAfter.entity.LfCustInfo;
import com.beawan.loanAfter.entity.LfTask;

/**
 * @author yzj
 */
public interface LfTaskService extends BaseService<LfTask> {
	
	/**
	 * 
	 *@Description 贷后任务分页
	 *@param taskTypes 任务类型
	 *@param customerNo 客户编号
	 *@param customerName 客户名字
	 *@param userNo 风险经理用户编号
	 *@param orgNo 机构编号
	 *@param taskPosition 任务状态
	 *@param page 页码
	 *@param pageSize 每页条数
	 *@return
	 *@throws Exception
	 * @author xyh
	 */
	Pagination<LfTaskDto> getLfTaskPage(List<String> taskTypes, String customerNo, String customerName, String userName,
			String orgName, String riskUserName, String riskUserNo, String userNo, String orgNo, Integer taskPosition,
			int page, int pageSize) throws Exception;
    /**
     * 提交贷后任务
     * 同时设置贷后客户的类型等信息
     * @param lfTask
     * @throws Exception
     */
    void submitLfTaskInfo(LfTask lfTask,String userId)throws Exception;

    //=========================================任务跑批
    /**
     * 初始化操作-----只有项目初次上线的时候才会用到
     * 手动触发
     * @throws Exception
     */
    void syncInitLfTask()throws Exception;

    /**
     * 15日首检------管户和风险经理都在这里生成
     * @param beforeDay   跑前几天的首检----初始设定为5天
     * @throws Exception
     */
    void syncFifteenDayTask(Integer beforeDay)throws Exception;

    /**
     * 管户客户经理日常贷后任务
     * @throws Exception
     */
    void syncGHDayTask()throws Exception;
    
    public void syncGHDayTask2() throws Exception;
    
    /**
     *@Description 创建一个贷后任务
     *@param intervalDay
     *@param lfCustInfo
     *@throws Exception
     * @author xyh
     */
    void createOneTask(Integer intervalDay,LfCustInfo lfCustInfo)throws Exception;

    /**
     * 原先是每天跑一次
     * 风险经理3个月到期检查
     * @throws Exception
     */
    @Deprecated
    void syncFXThreeMonthTask(Integer afterMonth)throws Exception;
    

    /**
     * 风险经理3个月到期检查
     * 每月25日开始跑   下个月   1号到月底之间到期的授信合同，过期时间以授信时间
     * @throws Exception
     */
    void syncFXThreeMonthTask2(Integer nextMonth)throws Exception;
    
    /**
     * 风险经理日常贷后  根据预警项生成任务
     * 生成存在风险预警信息的贷后任务
     * @param date 跑批日期  若为空  则取前一天
     */
    void syncFXWarningTask(String date) throws Exception;
    
    

    /**
     * 插入15日首检的核查项信息
     * @param taskId
     * @throws Exception
     */
    void insertFifteenCheck(Integer taskId)throws Exception;
    
    
    /**
     *手动创建任务
     *@param lfCustDto 
     *@throws Exception
     */
    void createManuallyOneTask(String customerNo)throws Exception;
    
    
    
    /////////贷后任务开始，插入初始化数据
    /**
     * 同步企业基本工商数据   来自企查查
     * //判断当前客户是否同步企查查数据，若无  则同步，若已经同步过，则忽略这步操作
     * @param custNo
     * @throws Exception
     */
    void syncComFullDetail(String custNo) throws Exception;

    /**
     * 从客户工商信息的行政处罚中同步数据到贷后行政处罚
     * 必须先执行上面的 syncComFullDetail 方法
     * @param taskId
     * @param custNo
     * @throws Exception
     */
    void syncPently(Integer taskId, String custNo) throws Exception;
    
    
    /**
     * 同步 裁判文书案件数量   是否被执行人  是否失信人被执行名单  共三块数据
     * 注意：在诉讼中的案件 和 裁判文书有点不一样，一个诉讼案件可能会有很多个裁判文书  后期修改吧 GGG
     * 必须先执行上面的 syncComFullDetail 方法
     * @param taskId
     * @param custNo
     * @throws Exception
     */
    void syncLocalResult(Integer taskId, String custNo) throws Exception;
    
    
    /**
     * 
     *@Description 插入通用类型的数据
     *@param taskId
     *@throws Exception
     * @author xyh
     */
    void insertNomalModel(Integer taskId)throws Exception;
    /**
     * 
     *@Description 插入行业是 制造业 的行业模版数据
     *@param taskId
     *@throws Exception
     * @author xyh
     */
    void insertCModel(Integer taskId)throws Exception;
    /**
     * 
     *@Description 插入行业是 建筑业 的行业模版数据
     *@param taskId
     *@throws Exception
     * @author xyh
     */
    void insertEModel(Integer taskId)throws Exception;
    /**
     * 
     *@Description 插入行业是 批发零售业 的行业模版数据
     *@param taskId
     *@throws Exception
     * @author xyh
     */
    void insertFModel(Integer taskId)throws Exception;
    /**
     * 
     *@Description 插入行业是  房地产行业  的行业模版数据
     *@param taskId
     *@throws Exception
     * @author xyh
     */
    void insertKModel(Integer taskId)throws Exception;
    
    /**
     * 
     *@Description 插入1000w以下 的模版数据
     *@param taskId
     *@throws Exception
     * @author xyh
     */
    void insertDownModel(Integer taskId)throws Exception;
    
    /**
     * 同步客户在我行融资情况（用信情况）
     * @param taskId
     * @throws Exception
     */
    public void insertCreditSelfBankInfo(Integer taskId)throws Exception;
    
    /**
     * GGG -- >内容较多
     * 同步贷后 能获取到的所有数据  
     * 特别是非现场贷后部分
     * @param taskId
     * @param custName
     */
    void syncLfFullData(Integer taskId, String custName) throws Exception ;
    
    /**
     * 同步客户预警信息
     * @param	taskId  贷后任务号
     * @param	custName 客户名称
     * @param	date 日期    
     * ***/
    void syncWarnInfo(Integer taskId, String custName, String date) throws Exception;
    

    /**
     * 同步客户预警信息
     * @param	taskId  贷后任务号
     * @param	custName 客户名称
     * @param	startDate 开始日期    
     * @param	endDate 结束日期   
     * ***/
    void syncWarnInfo(Integer taskId, String custName, String startDate, String endDate) throws Exception;
    
    
    /**
     * 获取贷后任务数据用于数据导出
     * @param	taskId  贷后任务号
     * @param	custName 客户名称
     * ***/
	List<LfTaskDto> getLfTaskList(List<String> taskTypes, String customerNo, String customerName, String userName,
			String orgName, String riskUserName, String userNo, String orgNo, Integer taskPosition) throws Exception;
	
	
	/**
     * 手动创建管护日常任务
     * @param	customerNo 客户名称
	 * @return 
     * ***/
	String createGHDayTask(String customerNo) throws Exception;
	
	/**
     * 手动创建到期任务
     * @param	customerNo 客户名称
	 * @return 
     * ***/
	void createManuallyStopTask(String customerNo) throws Exception;
}
