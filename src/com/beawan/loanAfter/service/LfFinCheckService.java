package com.beawan.loanAfter.service;

import java.util.List;

import com.beawan.core.BaseService;
import com.beawan.loanAfter.entity.LfFinCheck;

/**
 * 贷后检查   财务状况检查
 * @author yzj
 */
public interface LfFinCheckService extends BaseService<LfFinCheck> {

	/**
	 * 批量保存
	 * @param saveList
	 * @throws Exception
	 */
	void batchSaveUpdata(List<LfFinCheck> saveList)throws Exception;
}
