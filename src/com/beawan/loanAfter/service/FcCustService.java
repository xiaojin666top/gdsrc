package com.beawan.loanAfter.service;

import com.beawan.core.BaseService;
import com.beawan.core.Pagination;
import com.beawan.loanAfter.dto.LfCustDto;
import com.beawan.loanAfter.entity.FcCust;

/**
 * @author yzj
 */
public interface FcCustService extends BaseService<FcCust> {
	
	/**
	 * 获取当前五级分类客户名单   分页
	 * @param taskId	五级分类任务号
	 * @param custName	客户名字  模糊查询条件
	 * @param state		客户报告状态
	 * @return
	 * @throws Exception
	 */
	Pagination<LfCustDto> getFcCustList(Integer taskId, String custName, String userName, int page, int pageSize) throws Exception;
}
