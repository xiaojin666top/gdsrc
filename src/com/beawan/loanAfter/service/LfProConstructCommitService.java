package com.beawan.loanAfter.service;

import com.beawan.core.BaseService;
import com.beawan.loanAfter.entity.LfProConstructCommit;

/**
 * @author yzj
 */
public interface LfProConstructCommitService extends BaseService<LfProConstructCommit> {
}
