package com.beawan.loanAfter.service;

import java.util.List;

import com.beawan.core.BaseService;
import com.beawan.loanAfter.entity.LfInternalControlInfo;

/**
 * @author yzj
 */
public interface LfInternalControlInfoService extends BaseService<LfInternalControlInfo> {
	
	/**
	 * 在自动生成贷后任务时，同步我行内控名单信息
	 * 暂时拿到的都是黑名单客户 
	 * 黑名单客户并不是只有一条数据，会有很多条，每天有加入黑名单的原因
	 * @param taskId		贷后任务号
	 * @param xdCustNo		客户信贷号
	 * @param lastTime		任务生成时间		yyyyMMdd格式
	 * @param month			生成时间向前推m个月 	默认6个月
	 */
	void syncInternalControl(Integer taskId, String xdCustNo, String lastTime, Integer month) throws Exception;
	
	/**
	 * 批量保存内控名单列表
	 * @param saveList
	 * @throws Exception
	 */
	void batchSaveUpdata(List<LfInternalControlInfo> saveList)throws Exception;
}
