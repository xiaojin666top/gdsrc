package com.beawan.loanAfter.service;

import java.util.List;

import com.beawan.core.BaseService;
import com.beawan.loanAfter.entity.FcFinaData;

/**
 * @author yzj
 */
public interface FcFinaDataService extends BaseService<FcFinaData> {
	
	/**
	 * 获取 财务比率分析表
	 * @param taskCustId
	 * @return
	 */
	List<FcFinaData> getFinaRate(Integer taskCustId);
	
	/**
	 * 获取 现金流量分析表
	 * @param taskCustId
	 * @return
	 */
	List<FcFinaData> getCashFlow(Integer taskCustId);
}
