package com.beawan.loanAfter.service;

import com.beawan.core.BaseService;
import com.beawan.loanAfter.entity.LfFifteenDayCheck;

import java.util.List;

/**
 * @author yzj
 */
public interface LfFifteenDayCheckService extends BaseService<LfFifteenDayCheck> {

    void batchSaveUpdata(List<LfFifteenDayCheck> saveList)throws Exception;
}
