package com.beawan.loanAfter.service;

import com.beawan.core.BaseService;
import com.beawan.loanAfter.entity.LfChangeCheck;

import java.util.List;

/**
 * @author yzj
 */
public interface LfChangeCheckService extends BaseService<LfChangeCheck> {
    void batchSaveUpdata(List<LfChangeCheck> saveList)throws Exception;
}
