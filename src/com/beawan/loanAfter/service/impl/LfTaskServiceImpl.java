package com.beawan.loanAfter.service.impl;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.beawan.base.dao.ISysDicDAO;
import com.beawan.base.entity.SysDic;
import com.beawan.base.entity.User;
import com.beawan.base.service.IOrganizationSV;
import com.beawan.base.service.IUserSV;
import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.core.Pagination;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.dao.ICusBaseDAO;
import com.beawan.dtmp.dao.BusinessContractDao;
import com.beawan.dtmp.dao.BusinessDuebillDao;
import com.beawan.dtmp.dao.CustomerInfoDao;
import com.beawan.dtmp.dao.EntInfoDao;
import com.beawan.dtmp.dao.LnlnslnsDao;
import com.beawan.dtmp.dto.CustLoanDto;
import com.beawan.dtmp.entity.BusinessContract;
import com.beawan.dtmp.entity.BusinessDuebill;
import com.beawan.dtmp.entity.CustomerInfo;
import com.beawan.dtmp.entity.EntInfo;
import com.beawan.dtmp.entity.Lnlnslns;
import com.beawan.loanAfter.dao.LfAdminSanInfoDao;
import com.beawan.loanAfter.dao.LfChangeCheckDao;
import com.beawan.loanAfter.dao.LfCreditSelfBankDao;
import com.beawan.loanAfter.dao.LfCustInfoDao;
import com.beawan.loanAfter.dao.LfFifteenDayCheckDao;
import com.beawan.loanAfter.dao.LfFinCheckDao;
import com.beawan.loanAfter.dao.LfLoanProjectDetailDao;
import com.beawan.loanAfter.dao.LfLoanProjectSummaryDao;
import com.beawan.loanAfter.dao.LfLocalResultDao;
import com.beawan.loanAfter.dao.LfPaymentOfFeesDao;
import com.beawan.loanAfter.dao.LfRiskWarningInfoDao;
import com.beawan.loanAfter.dao.LfSituationCheckDao;
import com.beawan.loanAfter.dao.LfTaskDao;
import com.beawan.loanAfter.dto.LfTaskDto;
import com.beawan.loanAfter.entity.LfAdminSanInfo;
import com.beawan.loanAfter.entity.LfChangeCheck;
import com.beawan.loanAfter.entity.LfCreditSelfBank;
import com.beawan.loanAfter.entity.LfCustInfo;
import com.beawan.loanAfter.entity.LfFifteenDayCheck;
import com.beawan.loanAfter.entity.LfFinCheck;
import com.beawan.loanAfter.entity.LfLoanProjectDetail;
import com.beawan.loanAfter.entity.LfLoanProjectSummary;
import com.beawan.loanAfter.entity.LfLocalResult;
import com.beawan.loanAfter.entity.LfPaymentOfFees;
import com.beawan.loanAfter.entity.LfRiskManager;
import com.beawan.loanAfter.entity.LfRiskWarningInfo;
import com.beawan.loanAfter.entity.LfSituationCheck;
import com.beawan.loanAfter.entity.LfTask;
import com.beawan.loanAfter.service.LfCfdAccountInfoService;
import com.beawan.loanAfter.service.LfInternalControlInfoService;
import com.beawan.loanAfter.service.LfRiskManagerService;
import com.beawan.loanAfter.service.LfTaskService;
import com.beawan.qcc.service.SynchronizationQCCService;
import com.beawan.survey.custInfo.bean.CompPenalty;
import com.beawan.survey.custInfo.bean.CompShiXinItems;
import com.beawan.survey.custInfo.bean.CompZhiXingItems;
import com.beawan.survey.custInfo.dao.CompJudgmentDao;
import com.beawan.survey.custInfo.dao.CompPenaltyDao;
import com.beawan.survey.custInfo.dao.CompShiXinItemsDao;
import com.beawan.survey.custInfo.dao.CompZhiXingItemsDao;
import com.beawan.survey.custInfo.entity.CompJudgment;
import com.platform.util.DateUtil;
import com.platform.util.ExceptionUtil;
import com.platform.util.RgnshUtils;
import com.platform.util.StringUtil;

/**
 * @author yzj
 */
@Service("lfTaskService")
public class LfTaskServiceImpl extends BaseServiceImpl<LfTask> implements LfTaskService{
	

	private static final Logger log = Logger.getLogger(LfTaskServiceImpl.class);
    /**
    * 注入DAO
    */
    @Resource(name = "lfTaskDao")
    public void setDao(BaseDao<LfTask> dao) {
        super.setDao(dao);
    }
    @Resource
    public LfTaskDao lfTaskDao;
    @Resource
    public LfCustInfoDao lfCustInfoDao;
    @Resource
    private ICusBaseDAO cusBaseDAO;
    @Resource
    private LfFifteenDayCheckDao lfFifteenDayCheckDao;
    @Resource
    private ISysDicDAO sysDicDAO;
    @Resource
    private LfChangeCheckDao lfChangeCheckDao;
    @Resource
    private LfFinCheckDao lfFinCheckDao;
    @Resource
    private LfSituationCheckDao lfSituationCheckDao;
    @Resource
    private LfPaymentOfFeesDao lfPaymentOfFeesDao;
    @Resource
    private LfLoanProjectSummaryDao lfLoanProjectSummaryDao;
    @Resource
    private LfLoanProjectDetailDao lfLoanProjectDetailDao;
    @Resource
    private LfCreditSelfBankDao lfCreditSelfBankDao;
    
    //核心（cbod）中的放款档--这个最好不出现在这里
    @Resource
    private LnlnslnsDao lnlnslnsDao;
    @Resource
    private EntInfoDao entInfoDao;
    @Resource
    private CustomerInfoDao customerInfoDao;
    @Resource
    private BusinessContractDao businessContractDao;
    @Resource
    private BusinessDuebillDao businessDuebillDao;
    
    
    @Resource
    private LfRiskWarningInfoDao lfRiskWarningInfoDao;
    @Resource
    private LfInternalControlInfoService lfInternalControlInfoService;
    @Resource
    private LfCfdAccountInfoService lfCfdAccountInfoService;
	@Resource
	private SynchronizationQCCService synchronizationQCCService;
	@Resource
	private CompPenaltyDao compPenaltyDao;
    @Resource
    private LfAdminSanInfoDao lfAdminSanInfoDao;
    @Resource
    private CompJudgmentDao compJudgmentDao;
    @Resource
    private CompZhiXingItemsDao compZhiXingItemsDao;
    @Resource
    private CompShiXinItemsDao compShiXinItemsDao;
    @Resource
    private LfLocalResultDao lfLocalResultDao;

    @Resource
    private LfRiskManagerService lfRiskManagerService;
	@Resource
	private IUserSV userSV;
    
    
    
    


    @Override
    public Pagination<LfTaskDto> getLfTaskPage(List<String> taskTypes,String customerNo,String customerName, String userName,
    		String orgName, String riskUserName,String riskUserNo,String userNo,String orgNo,Integer taskPosition, int page, int pageSize) throws Exception {
    	//123
        StringBuffer sql = new StringBuffer();
        Map<String,Object> params = new HashMap<>();
        
        sql.append(" select a.id,c.XD_CUSTOMER_NO as xdCustNo,c.customer_no as customerNo,a.TASK_NAME taskName,")
                .append(" a.TASK_TYPE as taskType,a.TASK_POSITION as taskPosition, a.USER_NO as userNo,")
                .append(" b.USER_NAME as userName,d.ORGANNAME, a.CREATE_TIME as createTime,")
                .append(" a.CUSTOMER_NAME as customerName,  a.LOAN_BALANCE as loanBalance ,")
                .append(" a.INDUSTRY_TYPE as industryType,a.MARTUITY,a.RISKUSER,a.RISKUSERNAME")
                .append(" from LF_TASK a  ")
                .append(" left join CUS_BASE c on c.CUSTOMER_NO=a.CUSTOMER_NO")
                .append(" join BS_USER_INFO b ON a.user_no=b.user_id")
                .append(" left join BS_org d on b.ACC_ORG_NO=d.ORGANNO")
                .append(" where a.status= '"+Constants.NORMAL+"' ");
                //.append(" where a.status=:status ");

//                .append(" a.LOAN_BALANCE as loanBalance ,a.INDUSTRY_TYPE as industryType ")
//                .append(" from LF_TASK a ")
//                .append(" join CUS_BASE c on c.XD_CUSTOMER_NO=a.CUSTOMER_NO ")
//                .append(" join BS_USER_INFO b ON a.user_no=b.user_id ")
//                .append(" where a.status=:status ");
        params.put("status",Constants.NORMAL);
        if(!StringUtil.isEmptyString(customerNo)){
            //sql.append(" and a.CUSTOMER_NO=:customerNo ");
            sql.append(" and a.CUSTOMER_NO='"+customerNo+"'");
            params.put("customerNo",customerNo);
        }
        if(!StringUtil.isEmptyString(customerName)){
            sql.append(" and c.CUSTOMER_NAME like '%"+customerName+"%' ");
            params.put("customerName","%"+customerName+"%");
        }
        if(!StringUtil.isEmptyString(userName)){
            sql.append(" and b.USER_NAME like '%"+userName+"%' ");
            params.put("userName","%"+userName+"%");
        }
        if(!StringUtil.isEmptyString(orgName)){
            sql.append(" and d.ORGANNAME like '%"+orgName+"%' ");
            params.put("orgName","%"+orgName+"%");
        }
        if(!StringUtil.isEmptyString(riskUserName)){
            sql.append(" and a.RISKUSERNAME like '%"+riskUserName+"%' ");
            params.put("riskUserName","%"+riskUserName+"%");
        }
        if(taskPosition != null){
            sql.append(" and a.TASK_POSITION='"+taskPosition+"' ");
            params.put("taskPosition",taskPosition);
        }
        if(!StringUtil.isEmptyString(userNo)){
            sql.append(" and a.USER_NO= '"+userNo+"' ");
            params.put("userNo",userNo);
        }
        if(!StringUtil.isEmptyString(riskUserNo)){
            sql.append(" and a.RISKUSER= '"+riskUserNo+"' ");
            params.put("riskUserNo",riskUserNo);
        }
        if(!StringUtil.isEmptyString(orgNo)){
            sql.append(" and b.ACC_ORG_NO= '"+orgNo+"' ");
            params.put("orgNo",orgNo);
        }
        //in 查询的使用：1.括号在sql中   2.直接把list放入param中，会自动拼接
        if(!CollectionUtils.isEmpty(taskTypes)){
            for (int i = 0; i < taskTypes.size(); i++) {
                String type = taskTypes.get(i);
                //sql.append(" and a.TASK_TYPE in (:taskTypes)");
                sql.append(" and a.TASK_TYPE = '"+type+"' ");
            }


            params.put("taskTypes", taskTypes);
        }
        sql.append(" order by a.CREATE_TIME desc ");
        String count = " select count(*) from ("+sql.toString()+") tb1";
        params.clear();
        Pagination<LfTaskDto> pager = lfTaskDao.findCustSqlPagination(LfTaskDto.class,sql.toString(),count,params,page,pageSize);
        return pager;
    }


    @Override
    public void submitLfTaskInfo(LfTask lfTask,String userId) throws Exception {
    	
    	LfTask saveLfTask = lfTaskDao.findByPrimaryKey(lfTask.getId());
    	
        if(lfTask == null || RgnshUtils.isEmptyInteger(lfTask.getId())){
            ExceptionUtil.throwException("传输数据为空，提交失败");
        }
        if(StringUtil.isEmptyString(lfTask.getRiskPoint())&&StringUtil.isEmptyString(saveLfTask.getRiskPoint())
        		|| StringUtil.isEmptyString(lfTask.getRiskClassiReason())&&StringUtil.isEmptyString(saveLfTask.getRiskClassiReason())
        		|| StringUtil.isEmptyString(lfTask.getCreditPoint())&&StringUtil.isEmptyString(saveLfTask.getCreditPoint())
        		|| StringUtil.isEmptyString(lfTask.getCreditClassiReason())&&StringUtil.isEmptyString(saveLfTask.getCreditClassiReason())
        		){
        	ExceptionUtil.throwException("请先填写客户分类原因");
        }
       
        if(!StringUtils.isEmpty(lfTask.getCreditClassi())){
        	saveLfTask.setCreditClassi(lfTask.getCreditClassi()); 
        }
        if(!StringUtils.isEmpty(lfTask.getCreditSecondClassi()) && lfTask.getCreditClassi().equals("8") ){
        	saveLfTask.setCreditSecondClassi(lfTask.getCreditSecondClassi());
        }else{
        	saveLfTask.setCreditSecondClassi("");
        }
        if(!StringUtils.isEmpty(lfTask.getCreditPoint())){
        	saveLfTask.setCreditPoint(lfTask.getCreditPoint());
        }
        if(!StringUtils.isEmpty(lfTask.getCreditClassiReason())){
        	saveLfTask.setCreditClassiReason(lfTask.getCreditClassiReason());
        }
        //设置为贷后结束的贷后任务
        saveLfTask.setTaskPosition(Constants.LF_TASK_POSITION.CHECKED_TASK);
        saveLfTask.setUpdater(userId);
        saveLfTask.setUpdateTime(DateUtil.getNowTimestamp());
        lfTaskDao.saveOrUpdate(saveLfTask);

        //同步到贷后客户信息上
        LfCustInfo lfCustInfo =lfCustInfoDao.selectSingleByProperty("customerNo",saveLfTask.getCustomerNo());
        if(lfCustInfo == null){
            //客户信息不完整
            ExceptionUtil.throwException("客户信息不完整，提交失败");
        }
        lfCustInfo.setRiskClassi(saveLfTask.getRiskClassi());
        lfCustInfo.setCreditClassi(saveLfTask.getCreditClassi());
        lfCustInfo.setUpdater(userId);
        lfCustInfo.setUpdateTime(DateUtil.getNowTimestamp());
        lfCustInfoDao.saveOrUpdate(lfCustInfo);
    }

    //========================跑批任务
    @Override
    public void syncInitLfTask() throws Exception {
    	///////弃用-已经提供现在的客户分类名单
        //查询出所有is_add_cust为1的客户-----因为所有客户都是新进来的，所以都标识为新增，都要进行客户的初始认定
        //认定风险等级是为了做日常贷后，系统初始化的那天如果有15日首检，按这个客户的init就可以不做
        //所以这个接口可以放在首检的跑批之后
        List<LfCustInfo> lfCustInfoList = lfCustInfoDao.selectByProperty("isAddCust","1");
        for(LfCustInfo lfCustInfo:lfCustInfoList){
            //获取客户基本信息
            CusBase cusBase = cusBaseDAO.selectSingleByProperty("customerNo",lfCustInfo.getCustomerNo());
            //初始化客户风险分类和客户授信分类的贷后任务
            LfTask lfTask = createNewTask(lfCustInfo,cusBase.getCustomerName(),
            		Constants.LF_TASK_TYPE.INIT_TASK,cusBase.getManagerUserId());
            //同步企业信息
            lfTask = syncLfCustBaseInfo(lfTask,cusBase.getXdCustomerNo());
            lfTaskDao.saveOrUpdate(lfTask);

            lfCustInfo.setIsAddCust("0");
            lfCustInfoDao.saveOrUpdate(lfCustInfo);
        }
    }

    @Override
    public void syncFifteenDayTask(Integer beforeDay) throws Exception {
        //查询出所有的带增量标记的客户
        List<LfCustInfo> lfCustInfoList = lfCustInfoDao.selectByProperty("isAddCust","1");
        if(CollectionUtils.isEmpty(lfCustInfoList)){
            return;
        }
        if(beforeDay == null){
            beforeDay = -5;
        }
        Date shouldDay = DateUtil.addDay(new Date(),beforeDay);
        String shouldDayStr = DateUtil.format(shouldDay,Constants.DATE_MASK2);
        for(LfCustInfo lfCustInfo:lfCustInfoList){
        	//判断是否是指定时间发放的贷款（默认5天前）
            if(StringUtil.isEmptyString(lfCustInfo.getLastStartDate()) || !shouldDayStr.equals(lfCustInfo.getLastStartDate())){
                continue;
            }
            //获取客户基本信息
            CusBase cusBase = cusBaseDAO.selectSingleByProperty("customerNo",lfCustInfo.getCustomerNo());
//            Lnlnslns lnlnslns = lnlnslnsDao.getDtmpSingleByProperty("lnLnAcctNo",lfCustInfo.getLastLnLnAcctNo());
            BusinessDuebill businessDuebill = businessDuebillDao.getDtmpSingleByProperty("serialno", lfCustInfo.getLastLnLnAcctNo());
            //管户客户经理15日首检
            LfTask lfTask = createNewTask(lfCustInfo,cusBase.getCustomerName(),
            		Constants.LF_TASK_TYPE.GH_FIFTEEN_DAY_TASK,cusBase.getManagerUserId());
            
            //设置一下生成首检报告的客户信息和贷款信息
            lfTask.setCustomerCreNum(cusBase.getCertCode());
            lfTask.setFirstBalance(businessDuebill.getBusinesssum().setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
//            lfTask.setFirstTerm(lnlnslns.getLnLnMthsN());
            //同步企业信息
            lfTask = syncLfCustBaseInfo(lfTask,cusBase.getXdCustomerNo());
            lfTask = lfTaskDao.saveOrUpdate(lfTask);
            
            //同步企业工商信息
            this.syncComFullDetail(cusBase.getCustomerNo());
            this.syncPently(lfTask.getId(), cusBase.getCustomerNo());
            this.syncLocalResult(lfTask.getId(), cusBase.getCustomerNo());
            
            //添加如果有风险信号，给专职也生成一个贷后任务
            CustomerInfo cust = customerInfoDao.getDtmpSingleByProperty("customername", cusBase.getCustomerName());
    		String xdCustNo = null;
    		if(cust!=null){
    			xdCustNo = cust.getCustomerid();
    		}
            List<LfRiskWarningInfo> warnList = lfTaskDao.getWarnInfoList(cusBase.getCustomerName(), xdCustNo, lfCustInfo.getLastStartDate(), shouldDayStr);
            if(!CollectionUtils.isEmpty(warnList)){
                LfTask fxTask = createNewTask(lfCustInfo,cusBase.getCustomerName(),
                		Constants.LF_TASK_TYPE.FX_FIFTEEN_DAY_TASK,cusBase.getManagerUserId());
                //设置一下生成首检报告的客户信息和贷款信息
                //指定处理任务在专职风险经理
                User user = userSV.queryById(cusBase.getManagerUserId());
                if(user!=null){
                String accOrgNo = user.getAccOrgNo();//获取管护在用户所在机构号
    	            if(!StringUtils.isEmpty(accOrgNo)){
    	                LfRiskManager manager = lfRiskManagerService.selectSingleByProperty("organno", accOrgNo);
    	                if(manager==null){
    	                	log.info(cusBase.getCustomerName() + "管护客户经理是：" + user.getUserName() + "--所在机构为： " + user.getAccOrgNo() + "---该机构下暂未指定风险经理");
    	                }else{
    	                	fxTask.setRiskUser(manager.getRiskuser());
    	                	fxTask.setRiskUserName(manager.getRiskusername());
    	                }
    	            }
                }
                
                fxTask.setCustomerCreNum(cusBase.getCertCode());
                fxTask.setFirstBalance(businessDuebill.getBusinesssum().setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
//                fxTask.setFirstTerm(lnlnslns.getLnLnMthsN());
                fxTask = lfTaskDao.saveOrUpdate(fxTask);
                
        		Map<String, Object> params = new HashMap<>();
        		params.put("taskId", fxTask.getId());
        		for(LfRiskWarningInfo warn : warnList){
        			params.put("riskNo", warn.getRiskNo());
        			List<LfRiskWarningInfo> existList = lfRiskWarningInfoDao.selectByProperty(params);
        			if(!CollectionUtils.isEmpty(existList)){
        				log.info(warn.getRiskNo() + "--预警内容：" +warn.getRiskName() + "在贷后任务号为： " + fxTask.getId() + "--已经存在，跳过该预警项。");
        				continue;
        			}
        			warn.setTaskId(fxTask.getId());
        			lfRiskWarningInfoDao.saveOrUpdate(warn);
        		}	
    		}
            lfCustInfo.setIsAddCust("0");
            lfCustInfoDao.saveOrUpdate(lfCustInfo);
        }
        log.info("贷后首检任务处理完成");
    }

    @Override
    public void syncGHDayTask() throws Exception {
        //获取余额大于0的贷后客户
        List<LfCustInfo> lfCustInfoList = getShouldAfterCust(null);
        if(CollectionUtils.isEmpty(lfCustInfoList)){
            return;
        }
        for(LfCustInfo lfCustInfo:lfCustInfoList){
        	try{
	            //根据风险类型，获取贷后间隔期
	            if(Constants.LF_CUST_RISK_CLASSI.NOMAL_TYPE.equals(lfCustInfo.getRiskClassi())){
	                createDayTask(-90,lfCustInfo);
	            }else if(Constants.LF_CUST_RISK_CLASSI.FOLLOW_TYPE.equals(lfCustInfo.getRiskClassi()) || Constants.LF_CUST_RISK_CLASSI.FX_ONE_TYPE.equals(lfCustInfo.getRiskClassi())){
	                createDayTask(-30,lfCustInfo);
	            }else if(Constants.LF_CUST_RISK_CLASSI.FX_ONE_TYPE.equals(lfCustInfo.getRiskClassi())){
	                createDayTask(-7,lfCustInfo);
	            }
        	}catch(Exception e){
        		e.printStackTrace();
        		log.info("创建贷后任务异常，异常原因：", e);
        	}
        }
    }

    @Override
    public String createGHDayTask(String customerNo) throws Exception {
    	String info = "该客户授信金额小于0，不可创建日常任务";
    	//获取余额大于0的贷后客户
        List<LfCustInfo> lfCustInfoList = getShouldAfterCust(customerNo);
        if(CollectionUtils.isEmpty(lfCustInfoList)){
            return info;
        }
        
        for(LfCustInfo lfCustInfo:lfCustInfoList){
        	try{
	            //根据风险类型，获取贷后间隔期
	            if(Constants.LF_CUST_RISK_CLASSI.NOMAL_TYPE.equals(lfCustInfo.getRiskClassi())){
	                 info = createDayTask(-90,lfCustInfo);
	            }else if(Constants.LF_CUST_RISK_CLASSI.FOLLOW_TYPE.equals(lfCustInfo.getRiskClassi()) || Constants.LF_CUST_RISK_CLASSI.FX_ONE_TYPE.equals(lfCustInfo.getRiskClassi())){
	                 info = createDayTask(-30,lfCustInfo);
	            }else if(Constants.LF_CUST_RISK_CLASSI.FX_ONE_TYPE.equals(lfCustInfo.getRiskClassi())){
	                 info = createDayTask(-7,lfCustInfo);
	            }else if(Constants.LF_CUST_RISK_CLASSI.FX_TWO_TYPE.equals(lfCustInfo.getRiskClassi())){
	            	info="该客户分类的分类为:风险二级，无法创建日常任务";
	            }else if(Constants.LF_CUST_RISK_CLASSI.FX_THREE_TYPE.equals(lfCustInfo.getRiskClassi())){
	            	info="该客户分类的分类为:风险三级，无法创建日常任务";
	            }else{
	            	info="该客户分类的分类为:未分类，无法创建日常任务";
	            }
        	}catch(Exception e){
        		e.printStackTrace();
        		log.info("创建贷后任务异常，异常原因：", e);
        		info = "系统异常，请联系管理员";
        	}
        }
        return info;
    }
    
    @Override
    public void syncGHDayTask2() throws Exception {
        //获取余额大于0的贷后客户
//        List<LfCustInfo> lfCustInfoList = getShouldAfterCust();
//        if(CollectionUtils.isEmpty(lfCustInfoList)){
//            return;
//        }
        LfCustInfo lfCustInfo = lfCustInfoDao.selectSingleByProperty("customerNo", "CM20201016166134");
//        for(LfCustInfo lfCustInfo:lfCustInfoList){
            //根据风险类型，获取贷后间隔期
        if(Constants.LF_CUST_RISK_CLASSI.NOMAL_TYPE.equals(lfCustInfo.getRiskClassi())){
            createDayTask(-90,lfCustInfo);
        }else if(Constants.LF_CUST_RISK_CLASSI.FOLLOW_TYPE.equals(lfCustInfo.getRiskClassi()) || Constants.LF_CUST_RISK_CLASSI.FX_ONE_TYPE.equals(lfCustInfo.getRiskClassi())){
            createDayTask(-30,lfCustInfo);
        }else if(Constants.LF_CUST_RISK_CLASSI.FX_ONE_TYPE.equals(lfCustInfo.getRiskClassi())){
            createDayTask(-7,lfCustInfo);
        }
//        }
    }
    
    
    @Override
	public void createOneTask(Integer intervalDay, LfCustInfo lfCustInfo) throws Exception {
    	createDayTask(intervalDay,lfCustInfo);
	}

	/**
     * 创建指定间隔时间的贷后任务
     * @param intervalDay 间隔时间，天
     * @param lfCustInfo 需要进行的贷后客户
     * @throws Exception
     */
    private String createDayTask(Integer intervalDay,LfCustInfo lfCustInfo)throws Exception{
        //获取当前时间的指定前几天的日期
        Date beforeDate = DateUtil.addDay(new Date(),intervalDay);
        //查询获取贷后任务创建时间在beforeDate和今天之间的日常贷后任务
        StringBuffer sql = new StringBuffer(" from LfTask where 1=1 ");
        sql.append(" and createTime >:headTime and createTime <:endTIme ");
        sql.append(" and status=:status ");
        sql.append(" and taskType=:taskType");
        sql.append(" and customerNo=:customerNo ");
        Map<String,Object>param = new HashMap<>();
        param.put("headTime",beforeDate);
        param.put("endTIme",new Date());
        param.put("status",Constants.NORMAL);
        param.put("taskType",Constants.LF_TASK_TYPE.GH_DAY_TASK);
        param.put("customerNo",lfCustInfo.getCustomerNo());
        //如果存在，直接返回
        List<LfTask>lfTaskList = lfTaskDao.select(sql.toString(),param);
        if(CollectionUtils.isEmpty(lfTaskList)){
            //获取客户基本信息
            CusBase cusBase = cusBaseDAO.selectSingleByProperty("customerNo",lfCustInfo.getCustomerNo());
            
            System.out.println("开始对贷后任务中的数据进行同步，当前进行的客户编号为:【" + cusBase.getCustomerNo() + "】,客户名称为：【"+ cusBase.getCustomerName() + "】");
            log.info("开始对贷后任务中的数据进行同步，当前进行的客户编号为:【" + cusBase.getCustomerNo() + "】,客户名称为：【"+ cusBase.getCustomerName() + "】");
            //管户客户经理的日常贷后任务
            LfTask lfTask = createNewTask(lfCustInfo,cusBase.getCustomerName(),
            		Constants.LF_TASK_TYPE.GH_DAY_TASK,cusBase.getManagerUserId());
            //同步企业信息
            lfTask = syncLfCustBaseInfo(lfTask,cusBase.getXdCustomerNo());
            lfTask = lfTaskDao.saveOrUpdate(lfTask);

            //同步 不同时间间隔前到今天的预警信息
            this.syncWarnInfo(lfTask.getId(), cusBase.getCustomerName(), 
            		DateUtil.format(DateUtil.addDay(new Date(), intervalDay),Constants.DATE_MASK2), 
            		DateUtil.format(new Date(), Constants.DATE_MASK2));
            
            //同步客户黑名单信息
            lfInternalControlInfoService.syncInternalControl(lfTask.getId(), 
            		cusBase.getXdCustomerNo(), DateUtil.format(new Date(), Constants.DATE_MASK2), 6);
            //同步查冻扣账户
            lfCfdAccountInfoService.syncCfdAccount(lfTask.getId(), cusBase.getMfCustomerNo());
            //同步企业工商信息
            this.syncComFullDetail(cusBase.getCustomerNo());
            this.syncPently(lfTask.getId(), cusBase.getCustomerNo());
            this.syncLocalResult(lfTask.getId(), cusBase.getCustomerNo());
            System.out.println("客户编号为:【" + cusBase.getCustomerNo() + "】,客户名称为：【"+ cusBase.getCustomerName() + "】数据同步成功");
            log.info("客户编号为:【" + cusBase.getCustomerNo() + "】,客户名称为：【"+ cusBase.getCustomerName() + "】数据同步成功");
            return "客户编号为:【" + cusBase.getCustomerNo() + "】,客户名称为：【"+ cusBase.getCustomerName() + "】,创建任务成功";
        }else{
        	CusBase cusBase = cusBaseDAO.selectSingleByProperty("customerNo",lfCustInfo.getCustomerNo());
        	log.info("客户编号为:【" + cusBase.getCustomerNo() + "】,客户名称为：【"+ cusBase.getCustomerName() + "】，已在任务中");
        	return "客户名称为：【"+ cusBase.getCustomerName() + "】，已在任务中";
        }
    }

    @Override
    public void syncFXThreeMonthTask(Integer afterMonth) throws Exception {
        List<LfCustInfo> lfCustInfoList = getShouldAfterCust(null);
        if(CollectionUtils.isEmpty(lfCustInfoList)){
            return;
        }
        if(afterMonth == null){
            afterMonth = 3;
        }
        Date shouldMonth = DateUtil.addMonth(new Date(),afterMonth);
        String shouldMonthStr = DateUtil.format(shouldMonth,Constants.DATE_MASK2);
        for(LfCustInfo lfCustInfo:lfCustInfoList){
            //最近一笔贷款到期日存在，而且是今天后的3个月到期，那么生成到期检查任务
            if(StringUtil.isEmptyString(lfCustInfo.getLastStopDate()) || !shouldMonthStr.equals(lfCustInfo.getLastStopDate())){
                continue;
            }
            //获取客户基本信息
            CusBase cusBase = cusBaseDAO.selectSingleByProperty("customerNo",lfCustInfo.getCustomerNo());
            //风险客户经理三个月到期检查
            LfTask lfTask = createNewTask(lfCustInfo,cusBase.getCustomerName(),
            		Constants.LF_TASK_TYPE.FX_THREE_MONTH_TASK,cusBase.getManagerUserId());
            //同步企业信息
            lfTask = syncLfCustBaseInfo(lfTask,cusBase.getXdCustomerNo());
            lfTask = lfTaskDao.saveOrUpdate(lfTask);
            
            //同步三个月前到今天的预警信息
            this.syncWarnInfo(lfTask.getId(), cusBase.getCustomerName(), 
            		DateUtil.format(DateUtil.addMonth(new Date(), -afterMonth),Constants.DATE_MASK2), 
            		DateUtil.format(new Date(), Constants.DATE_MASK2));
            
            //同步客户黑名单信息
            lfInternalControlInfoService.syncInternalControl(lfTask.getId(), 
            		cusBase.getXdCustomerNo(), DateUtil.format(new Date(), Constants.DATE_MASK2), 6);
            //同步查冻扣账户
            lfCfdAccountInfoService.syncCfdAccount(lfTask.getId(), cusBase.getMfCustomerNo());
            //同步企业工商信息
            this.syncComFullDetail(cusBase.getCustomerNo());
            this.syncPently(lfTask.getId(), cusBase.getCustomerNo());
            this.syncLocalResult(lfTask.getId(), cusBase.getCustomerNo());
        }
    }
    
    
    @Override
    public void syncFXThreeMonthTask2(Integer nextMonth)throws Exception {

    	//当前设定2个月后授信到期
    	if(nextMonth == null)
    		nextMonth = 2;
    	/**
    	 * 每月25日开始跑   下个月   1号到月底之间到期的授信合同，过期时间以授信时间
    	 * 数据源为信贷系统的 BUSINESS_CONTRACT 授信业务合同表
    	 */
    	String firstDay = DateUtil.getNextMonthFirstDay(nextMonth, null);
    	String lastDay = DateUtil.getNextMonthLastDay(nextMonth, null);
    	
    	String taskYear = firstDay.substring(0, 4); 
    	String taskMonth = firstDay.substring(4, 6); 
    	//风险客户经理2个月到期检查
    	List<BusinessContract> matuityConListByTerm = businessContractDao
    			.getMatuityConListByTerm(firstDay, lastDay, null);
    	if(CollectionUtils.isEmpty(matuityConListByTerm)){
    		log.warn(nextMonth + "个月后授信到期合同数据为空，请核查具体原因");
    		return;
    	}
    	Map<String, List<BusinessContract>> collect = matuityConListByTerm.stream()
    			.collect(Collectors.groupingBy(BusinessContract::getCustomerid));
    	
    	int index = 0;
    	int qccIndex = 0;
    	for(Map.Entry<String, List<BusinessContract>> entry : collect.entrySet()){
    		String xdCustNo = entry.getKey();
    		List<BusinessContract> contractList = entry.getValue();
    		
    		CusBase cusBase = cusBaseDAO.selectSingleByProperty("xdCustomerNo", xdCustNo);
    		if(cusBase == null){
    			log.error("信贷号为： " + xdCustNo + "的客户，在对公系统的cusBase表中不存在，请核查");
    			continue;
    		}
    		/*if(!cusBase.getCustomerNo().equals("CM20201016449997") && 
    				!cusBase.getCustomerNo().equals("CM20201016049307")){
    			continue;
    		}*/
    		LfCustInfo lfCustInfo = lfCustInfoDao.selectSingleByProperty("customerNo", cusBase.getCustomerNo());
        	LfTask lfTask = new LfTask();
        	lfTask.setTaskName(taskYear + "年" + taskMonth + "月份授信到期任务");
            lfTask.setCustomerNo(cusBase.getCustomerNo());//贷后客户编号
            lfTask.setTaskType(Constants.LF_TASK_TYPE.FX_THREE_MONTH_TASK); //贷后任务类型
            lfTask.setUserNo(cusBase.getManagerUserId());//贷后客户经理
            lfTask.setCustomerName(cusBase.getCustomerName());
            //同步企业信息
            lfTask = syncLfCustBaseInfo(lfTask,cusBase.getXdCustomerNo());
            //指定处理任务在专职风险经理
            User user = userSV.queryById(cusBase.getManagerUserId());
            if(user!=null){
            String accOrgNo = user.getAccOrgNo();//获取管护在用户所在机构号
	            if(!StringUtils.isEmpty(accOrgNo)){
	                LfRiskManager manager = lfRiskManagerService.selectSingleByProperty("organno", accOrgNo);
	                if(manager==null){
	                	log.info(cusBase.getCustomerName() + "管护客户经理是：" + user.getUserName() + "--所在机构为： " + user.getAccOrgNo() + "---该机构下暂未指定风险经理");
	                }else{
	                	lfTask.setRiskUser(manager.getRiskuser());
	                	lfTask.setRiskUserName(manager.getRiskusername());
	                }
	            }
            }

            BusinessContract max = contractList.stream().max(Comparator.comparing(BusinessContract::getMaturity)).get();
//            MARTUITY
            lfTask.setMartuity(max.getMaturity());
            //在到期检查任务中，这个金额 为授信到期金额
            double sum = contractList.stream().mapToDouble(BusinessContract::getBusinesssum).sum();
            lfTask.setLoanBalance(sum);//  2个月后授信到期 金额之和
            
            lfTask.setIndustryType(lfCustInfo.getIndustryType());//行业
            lfTask.setCreditClassi(lfCustInfo.getCreditClassi());//客户分类
            lfTask.setRiskClassi(lfCustInfo.getRiskClassi());//风险分类
            
            lfTask.setHasSync(0);//设置为没有同步过数据
            lfTask.setTaskPosition(Constants.LF_TASK_POSITION.CHECKING_TASK);
            lfTask.setCreater(Constants.SYS_SHORT_NAME);
            lfTask.setUpdater(Constants.SYS_SHORT_NAME);
            lfTask.setUpdateTime(DateUtil.getNowTimestamp());
            lfTask = lfTaskDao.saveOrUpdate(lfTask);
            
            log.info("第"+(++index)+"条贷后到期客户------其中信贷号为： " + xdCustNo + "--------客户名称："+cusBase.getCustomerName()+taskMonth+"月份的到期任务创建成功");
            //同步三个月前到今天的预警信息
            this.syncWarnInfo(lfTask.getId(), cusBase.getCustomerName(), 
            		DateUtil.format(DateUtil.addMonth(new Date(), -nextMonth),Constants.DATE_MASK2), 
            		DateUtil.format(new Date(), Constants.DATE_MASK2));
            log.info("同步预警信息成功");
            //同步客户黑名单信息
            lfInternalControlInfoService.syncInternalControl(lfTask.getId(), 
            		cusBase.getXdCustomerNo(), DateUtil.format(new Date(), Constants.DATE_MASK2), 6);
            log.info("同步黑名单信息成功");
            //同步查冻扣账户
            lfCfdAccountInfoService.syncCfdAccount(lfTask.getId(), cusBase.getMfCustomerNo());
            log.info("同步查冻扣信息成功");
            //同步企业工商信息
            try{
            	this.syncComFullDetail(cusBase.getCustomerNo());
                log.info("同步企查查工商信息成功");
            }catch(Exception e){
            	qccIndex++;
            	log.info("第" + qccIndex + "条," + cusBase.getCustomerNo() +"----"+ cusBase.getCustomerName() + "同步企查查工商信息失败");
            	
            }
            
            this.syncPently(lfTask.getId(), cusBase.getCustomerNo());
            log.info("同步企查查行政处罚信息成功");
            this.syncLocalResult(lfTask.getId(), cusBase.getCustomerNo());
            log.info("同步企查查司法诉讼信息成功");
            
    	}
    	
    	log.info(taskMonth+"月份的到期任务创建成功");
//        LfTask lfTask = createNewTask(lfCustInfo,cusBase.getCustomerName(),
//        		Constants.LF_TASK_TYPE.FX_THREE_MONTH_TASK,cusBase.getManagerUserId());
    }
    
    @Override
    public void syncFXWarningTask(String date) throws Exception{
    	//跑批任务是是第二天的凌晨
    	//1. 获取跑批前一天的时间内  所有存在风险的客户名单    包括2个预警系统数据 和 30个OAP预警系统数据
    	if(StringUtils.isEmpty(date))
    		date = DateUtil.format(DateUtil.addDay(new Date(), -1), Constants.DATE_MASK2);
    	//获取存在预警的客户号列表及管护人id
    	List<LfTaskDto> warnCustList = lfTaskDao.getWarnCust(date);
    	if(CollectionUtils.isEmpty(warnCustList)){
    		log.info("今日无客户出现预警信息，创建---"+date+"---预警任务结束");
    		return;
    	}
    	
    	//2. 根据客户名单  去生成风险经理的贷后风险预警任务
    	//若该客户已经存在贷后风险预警任务，则在该风险预警的任务中，增加预警信息项
    	//若客户不存在贷后风险预警任务，则新增贷后风险预警任务    并同步预警信息项
    	for(LfTaskDto dto : warnCustList){
    		Map<String, Object> params = new HashMap<>();
    		params.put("customerNo", dto.getCustomerNo());
    		params.put("taskType", Constants.LF_TASK_TYPE.FX_YJ_TASK);
    		params.put("taskPosition", Constants.LF_TASK_POSITION.CHECKING_TASK);
    		//查询这个客户有没有检查中的风险预警任务
    		List<LfTask> pendingTaskList = lfTaskDao.selectByProperty(params);
    		LfTask lfTask;
    		//若 当前客户不存在 未处理任务
    		if(CollectionUtils.isEmpty(pendingTaskList)){
    			//获取贷后客户信息
    			LfCustInfo lfCustInfo = lfCustInfoDao.selectSingleByProperty("customerNo", dto.getCustomerNo());
    			if(lfCustInfo == null ||  lfCustInfo.getLastLoanSum() == 0){
    				log.info("创建贷后预警任务时，当前客户无贷后客户信息或 当前客户在我行授信金额为 0 元，客户号为："
    						+ dto.getCustomerNo() + "----不创建预警任务");
    				continue;
    			}
    			
    			lfTask = new LfTask();
                lfTask.setCustomerNo(dto.getCustomerNo());//贷后客户编号
                lfTask.setTaskType(Constants.LF_TASK_TYPE.FX_YJ_TASK); //贷后任务类型
                lfTask.setUserNo(dto.getUserNo());//贷后管护客户经理
                lfTask.setCustomerName(dto.getCustomerName());
                //指定处理任务在专职风险经理
                User user = userSV.queryById(dto.getUserNo());
                String accOrgNo = user.getAccOrgNo();//获取管护用户所在机构号
                if(!StringUtils.isEmpty(accOrgNo)){
                    LfRiskManager manager = lfRiskManagerService.selectSingleByProperty("organno", accOrgNo);
                    if(manager==null){
                    	log.info(dto.getCustomerName() + "--管护客户经理是：" + user.getUserName() + "--所在机构为： " + user.getAccOrgNo() + "---该机构下暂未指定风险经理");
                    }else{
                    	lfTask.setRiskUser(manager.getRiskuser());
                    	lfTask.setRiskUserName(manager.getRiskusername());
                    }
                }

                //在到期检查任务中，这个金额 为授信到期金额
//                double sum = contractList.stream().mapToDouble(BusinessContract::getBusinesssum).sum();
                lfTask.setLoanBalance(lfCustInfo.getLastLoanSum());//  客户授信金额
                
                lfTask.setIndustryType(lfCustInfo.getIndustryType());//行业
                lfTask.setCreditClassi(lfCustInfo.getCreditClassi());//客户分类
                lfTask.setRiskClassi(lfCustInfo.getRiskClassi());//风险分类
                
                lfTask.setHasSync(0);//设置为没有同步过数据
                lfTask.setTaskPosition(Constants.LF_TASK_POSITION.CHECKING_TASK);
                lfTask.setCreater(Constants.SYS_SHORT_NAME);
                lfTask.setUpdater(Constants.SYS_SHORT_NAME);
                lfTask.setUpdateTime(DateUtil.getNowTimestamp());
                lfTask = lfTaskDao.saveOrUpdate(lfTask);
//    			123
	    		

    			//获取客户基本信息
                CusBase cusBase = cusBaseDAO.selectSingleByProperty("customerNo",lfCustInfo.getCustomerNo());
                //同步企业信息
                lfTask = syncLfCustBaseInfo(lfTask,cusBase.getXdCustomerNo());
                lfTask = lfTaskDao.saveOrUpdate(lfTask);
	            //同步客户黑名单信息
	            lfInternalControlInfoService.syncInternalControl(lfTask.getId(), 
	            		cusBase.getXdCustomerNo(), DateUtil.format(new Date(), Constants.DATE_MASK2), 6);
	            //同步查冻扣账户
	            lfCfdAccountInfoService.syncCfdAccount(lfTask.getId(), cusBase.getMfCustomerNo());
	            
	            //同步企业工商信息
	            this.syncComFullDetail(cusBase.getCustomerNo());
	            this.syncPently(lfTask.getId(), cusBase.getCustomerNo());
	            this.syncLocalResult(lfTask.getId(), cusBase.getCustomerNo());
    		}else{
    			//存在未处理任务
    			lfTask = pendingTaskList.get(0);
    		}
    		
    		//插入风险预警
    		this.syncWarnInfo(lfTask.getId(), lfTask.getCustomerName(), date);
    	}
    }

    
    
    /**
     * 获取贷款余额大于0的客户列表
     * 大于0 表示需要贷后
     * @return
     */
    private List<LfCustInfo> getShouldAfterCust(String customerNo){
        StringBuffer sqlString = new StringBuffer(" from LfCustInfo where 1=1 ");
        sqlString.append(" and lastLoanSum >:lastLoanSum ")
                .append(" and status=:status ");
        Map<String,Object> params = new HashMap<>();
        if(!StringUtils.isEmpty(customerNo)){
        	sqlString.append(" and customerNo=:customerNo ");
        	params.put("customerNo",customerNo);
        }
        params.put("lastLoanSum",0.0);
        params.put("status",Constants.NORMAL);
        List<LfCustInfo> lfCustInfoList = lfCustInfoDao.select(sqlString.toString(),params);
        return lfCustInfoList;
    }

    /**
     * 
     *@Description 构造一个贷后任务（行业和金额用于区分贷后使用的调查模版）
     *@param lfCustInfo 贷后客户信息（取出客户编号、行业、金额、风险分类、客户分类）
     *@param customerName 客户姓名
     *@param taskType 贷后任务类型
     *@param userNo 贷后经理编号
     *@return
     * @author xyh
     */
    private LfTask createNewTask(LfCustInfo lfCustInfo,String customerName, String taskType,String userNo){
        LfTask lfTask = new LfTask();
        lfTask.setCustomerNo(lfCustInfo.getCustomerNo());//贷后客户编号
        lfTask.setTaskType(taskType); //贷后任务类型
        lfTask.setUserNo(userNo);//贷后客户经理
        lfTask.setCustomerName(customerName);
        lfTask.setLoanBalance(lfCustInfo.getLastLoanSum());//金额
        lfTask.setIndustryType(lfCustInfo.getIndustryType());//行业
        lfTask.setCreditClassi(lfCustInfo.getCreditClassi());//客户分类
        lfTask.setRiskClassi(lfCustInfo.getRiskClassi());//风险分类
        lfTask.setMartuity( lfCustInfo.getLastStopDate());//到期日期
        lfTask.setHasSync(0);//设置为没有同步过数据
        lfTask.setTaskPosition(Constants.LF_TASK_POSITION.CHECKING_TASK);
        lfTask.setCreater(Constants.SYS_SHORT_NAME);
        lfTask.setUpdater(Constants.SYS_SHORT_NAME);
        lfTask.setUpdateTime(DateUtil.getNowTimestamp());
        return lfTask;
    }
    

    /**
     * 为这笔首检的贷后任务，插入核查内容
     * @param taskId
     */
    @Override
    public void insertFifteenCheck(Integer taskId)throws Exception{
        if(RgnshUtils.isEmptyInteger(taskId)){
            return;
        }
        //获取首检核查类型
        List<SysDic> checkTypeList = sysDicDAO.queryByOptType(Constants.LF_SYS_DIC.FIFTEEN_DAY_CHECK);
        if(CollectionUtils.isEmpty(checkTypeList)){
            ExceptionUtil.throwException("未获取到首检核查项");
        }
        List<LfFifteenDayCheck> saveList = new ArrayList<>();
        for(SysDic sysDic:checkTypeList){
            LfFifteenDayCheck lfFifteenDayCheck = new LfFifteenDayCheck(taskId,sysDic.getCnName(),sysDic.getEnName());
            lfFifteenDayCheck.setCreater(Constants.SYS_SHORT_NAME);
            lfFifteenDayCheck.setUpdater(Constants.SYS_SHORT_NAME);
            lfFifteenDayCheck.setUpdateTime(DateUtil.getNowTimestamp());
            saveList.add(lfFifteenDayCheck);
        }
        lfFifteenDayCheckDao.batchSaveUpdata(saveList);
        //设置已经同步过数据
      	setTaskHadSync(taskId);
    }
    

    @Override
    public void createManuallyOneTask(String customerNo) throws Exception {
    	
		LfTask lfTask;
		
		//获取贷后客户信息
		LfCustInfo lfCustInfo = lfCustInfoDao.selectSingleByProperty("customerNo", customerNo);
		
		//获取客户基本信息
        CusBase cusBase = cusBaseDAO.selectSingleByProperty("customerNo",lfCustInfo.getCustomerNo());
        
		lfTask = createNewTask(lfCustInfo,cusBase.getCustomerName(),
				Constants.LF_TASK_TYPE.CREATE_MANUALLY_TASK, cusBase.getManagerUserId());
		
		//同步企业信息
        lfTask = syncLfCustBaseInfo(lfTask,cusBase.getXdCustomerNo());

		lfTask = lfTaskDao.saveOrUpdate(lfTask);
		//同步三个月前到今天的预警信息
        this.syncWarnInfo(lfTask.getId(), cusBase.getCustomerName(), 
        		DateUtil.format(DateUtil.addMonth(new Date(), -3),Constants.DATE_MASK2), 
        		DateUtil.format(new Date(), Constants.DATE_MASK2));
		
        //同步客户黑名单信息
        lfInternalControlInfoService.syncInternalControl(lfTask.getId(), 
        		cusBase.getXdCustomerNo(), DateUtil.format(new Date(), Constants.DATE_MASK2), 6);
        //同步查冻扣账户 GGG
        lfCfdAccountInfoService.syncCfdAccount(lfTask.getId(), cusBase.getMfCustomerNo());
        
        //同步企业工商信息
        this.syncComFullDetail(cusBase.getCustomerNo());
        this.syncPently(lfTask.getId(), cusBase.getCustomerNo());
        this.syncLocalResult(lfTask.getId(), cusBase.getCustomerNo());
		
    }
    
    
    
    //=====================根据行业模版，同步数据

    @Override
	public void insertNomalModel(Integer taskId) throws Exception {
    	if(RgnshUtils.isEmptyInteger(taskId)){
            return;
        }
    	//客户基本信息变化情况
		insertChangeCheck(taskId, Constants.LF_SYS_DIC.LF_CHANGE_CUST);
		//融资及对外担保变化情况
		insertChangeCheck(taskId, Constants.LF_SYS_DIC.LF_CHANGE_FINANCE);
		insertChangeCheck(taskId, Constants.LF_SYS_DIC.LF_CHANGE_GUAT);
		//财务状况检查
		insertLfFincheck(taskId,Constants.LF_SYS_DIC.LF_FIN_CHECK);
		//经营情况检查
		insertLfSituationcheck(taskId, "RG_LF_JY_01", "外部环境");
		insertLfSituationcheck(taskId, "RG_LF_JY_02", "现场检查情况");
		//担保情况检查
		insertLfSituationcheck(taskId, "RG_LF_DB_01", "保证人的信用状况");
		insertLfSituationcheck(taskId, "RG_LF_DB_02", "保证人的经营情况");
		insertLfSituationcheck(taskId, "RG_LF_DB_03", "担保人的担保能力");
		insertLfSituationcheck(taskId, "RG_LF_DB_04", "担保人对外担保情况");
		
		//设置已经同步过数据
		setTaskHadSync(taskId);
	}
    
	@Override
	public void insertCModel(Integer taskId) throws Exception {
		if (RgnshUtils.isEmptyInteger(taskId)) {
			return;
		}
		// 客户基本信息变化情况
		insertChangeCheck(taskId, Constants.LF_SYS_DIC.LF_CHANGE_CUST);
		// 融资及对外担保变化情况
		insertChangeCheck(taskId, Constants.LF_SYS_DIC.LF_CHANGE_FINANCE);
		insertChangeCheck(taskId, Constants.LF_SYS_DIC.LF_CHANGE_GUAT);
		//费用缴纳情况分析
		insertFinFees(taskId);
		// 财务状况检查
		insertLfFincheck(taskId,Constants.LF_SYS_DIC.LF_FIN_CHECK);
		//生产经营情况
		insertLfSituationcheck(taskId, "RG_LF_JY_03", "市场环境");
		insertLfSituationcheck(taskId, "RG_LF_JY_04", "经营管理");
		insertLfSituationcheck(taskId, "RG_LF_JY_05", "投资情况");
		insertLfSituationcheck(taskId, "RG_LF_JY_06", "生产情况");
		//其他现场检查情况
		insertLfSituationcheck(taskId, "RG_LF_QT_01", "其他现场检查");
		// 担保情况检查
		insertLfSituationcheck(taskId, "RG_LF_DB_01", "保证人的信用状况");
		insertLfSituationcheck(taskId, "RG_LF_DB_02", "保证人的经营情况");
		insertLfSituationcheck(taskId, "RG_LF_DB_03", "担保人的担保能力");
		insertLfSituationcheck(taskId, "RG_LF_DB_04", "担保人对外担保情况");
		
		//设置已经同步过数据
		setTaskHadSync(taskId);
	}

	@Override
	public void insertEModel(Integer taskId) throws Exception {
		if(RgnshUtils.isEmptyInteger(taskId)){
            return;
        }
    	//客户基本信息变化情况
		insertChangeCheck(taskId, Constants.LF_SYS_DIC.LF_CHANGE_CUST);
		//融资及对外担保变化情况
		insertChangeCheck(taskId, Constants.LF_SYS_DIC.LF_CHANGE_FINANCE);
		insertChangeCheck(taskId, Constants.LF_SYS_DIC.LF_CHANGE_GUAT);
		//财务状况检查
		insertLfFincheck(taskId,Constants.LF_SYS_DIC.LF_FIN_CHECK_JZ);
		
		//申贷项目概况
		insertLoanProject(taskId);
		
		
		//项目存在的主要风险
		insertLfSituationcheck(taskId, "RG_LF_XM_01", "项目存在的主要风险");
		//其他现场检查情况
		insertLfSituationcheck(taskId, "RG_LF_QT_01", "其他现场检查");
		//担保情况检查
		insertLfSituationcheck(taskId, "RG_LF_DB_01", "保证人的信用状况");
		insertLfSituationcheck(taskId, "RG_LF_DB_02", "保证人的经营情况");
		insertLfSituationcheck(taskId, "RG_LF_DB_03", "担保人的担保能力");
		insertLfSituationcheck(taskId, "RG_LF_DB_04", "担保人对外担保情况");
		
		//设置已经同步过数据
		setTaskHadSync(taskId);
	}

	@Override
	public void insertFModel(Integer taskId) throws Exception {
		if(RgnshUtils.isEmptyInteger(taskId)){
            return;
        }
    	//客户基本信息变化情况
		insertChangeCheck(taskId, Constants.LF_SYS_DIC.LF_CHANGE_CUST);
		//融资及对外担保变化情况
		insertChangeCheck(taskId, Constants.LF_SYS_DIC.LF_CHANGE_FINANCE);
		insertChangeCheck(taskId, Constants.LF_SYS_DIC.LF_CHANGE_GUAT);
		//财务状况检查
		insertLfFincheck(taskId,Constants.LF_SYS_DIC.LF_FIN_CHECK_LS);
		//经营情况检查
		insertLfSituationcheck(taskId, "RG_LF_JY_01", "外部环境");
		//经营风险
		insertLfSituationcheck(taskId, "RG_LF_JY_07", "经营风险");
		
		//其他现场检查情况
		insertLfSituationcheck(taskId, "RG_LF_QT_01", "其他现场检查");
		//担保情况检查
		insertLfSituationcheck(taskId, "RG_LF_DB_01", "保证人的信用状况");
		insertLfSituationcheck(taskId, "RG_LF_DB_02", "保证人的经营情况");
		insertLfSituationcheck(taskId, "RG_LF_DB_03", "担保人的担保能力");
		insertLfSituationcheck(taskId, "RG_LF_DB_04", "担保人对外担保情况");
		
		//设置已经同步过数据
		setTaskHadSync(taskId);
		
	}
	
	@Override
	public void insertKModel(Integer taskId) throws Exception {
		if(RgnshUtils.isEmptyInteger(taskId)){
            return;
        }
    	//客户基本信息变化情况
		insertChangeCheck(taskId, Constants.LF_SYS_DIC.LF_CHANGE_CUST);
		//融资及对外担保变化情况
		insertChangeCheck(taskId, Constants.LF_SYS_DIC.LF_CHANGE_FINANCE);
		insertChangeCheck(taskId, Constants.LF_SYS_DIC.LF_CHANGE_GUAT);
		//其他现场检查情况
		insertLfSituationcheck(taskId, "RG_LF_QT_02", "其他现场检查");
		//担保情况检查
		insertLfSituationcheck(taskId, "RG_LF_DB_01", "保证人的信用状况");
		insertLfSituationcheck(taskId, "RG_LF_DB_02", "保证人的经营情况");
		insertLfSituationcheck(taskId, "RG_LF_DB_03", "担保人的担保能力");
		insertLfSituationcheck(taskId, "RG_LF_DB_04", "担保人对外担保情况");	
		
		//设置已经同步过数据
		setTaskHadSync(taskId);
	}
	
	@Override
	public void insertDownModel(Integer taskId) throws Exception {
		//同步一下生成报告需要的信息---同步到贷后任务信息表中
		//同步我行用信情况
		insertCreditSelfBankInfo(taskId);
		// 生产经营情况
		insertLfSituationcheck(taskId, "RG_LF_JY_08", "生产经营情况");
		// 建筑类
		insertLfSituationcheck(taskId, "RG_LF_JZ_01", "建筑类");
		// 财务状况
		insertLfSituationcheck(taskId, "RG_LF_FIN_01", "财务状况");

		// 担保情况检查
		insertLfSituationcheck(taskId, "RG_LF_DB_01", "保证人的信用状况");
		insertLfSituationcheck(taskId, "RG_LF_DB_02", "保证人的经营情况");
		insertLfSituationcheck(taskId, "RG_LF_DB_03", "担保人的担保能力");
		insertLfSituationcheck(taskId, "RG_LF_DB_04", "担保人对外担保情况");
		// 设置已经同步过数据
		setTaskHadSync(taskId);
	}

	/**
	 * 
	 *@Description 设置为已同步
	 *@param taskId
	 *@throws Exception
	 * @author xyh
	 */
	private void setTaskHadSync(Integer taskId)throws Exception{
		LfTask lfTask = lfTaskDao.findByPrimaryKey(taskId);
		lfTask.setHasSync(1);
		lfTaskDao.saveOrUpdate(lfTask);
	}
	

	/**
     * 插入变动核查信息-----每一笔需要现场调查的贷后都会进入到这里
     * 如果需要同步上一次的变动核查信息，就不走这个接口
     * @param taskId  贷后任务编号
     * @param changeType 变动类型
     * @throws Exception
     */
    private void insertChangeCheck(Integer taskId,String changeType)throws Exception{
    	 //获取客户信息变动检查项
        List<SysDic> changeCustList = sysDicDAO.queryByOptType(changeType);
        if(CollectionUtils.isEmpty(changeCustList)){
            ExceptionUtil.throwException("未获取到变动核查项");
        }
        List<LfChangeCheck> saveList = new ArrayList<>();
        for(SysDic sysDic:changeCustList){
			LfChangeCheck lfChangeCheck = new LfChangeCheck(taskId, sysDic.getCnName(), sysDic.getEnName(), changeType);
			if (Constants.LF_SYS_DIC.LF_CHANGE_CUST.equals(changeType) && ("01".equals(sysDic.getEnName())
					|| "02".equals(sysDic.getEnName()) || "03".equals(sysDic.getEnName()))) {
				//同步以下客户基本信息中的前三项，实际控制人、主要管理人、实收资本
				LfTask lfTask = lfTaskDao.findByPrimaryKey(taskId);
				CusBase cusBase = cusBaseDAO.findByPrimaryKey(lfTask.getCustomerNo());
				EntInfo entInfo = entInfoDao.getAfInfoByMfCustomerId(cusBase.getMfCustomerNo());
				switch (sysDic.getEnName()) {
				case "01":
				case "02":
					lfChangeCheck.setCreditSituation(entInfo.getFicitiousPerson());
					break;
				case "03":
					NumberFormat nf = new DecimalFormat("#,###.##");
					String paiclupCapital = nf.format(entInfo.getPaiclupCapital());
					lfChangeCheck.setCreditSituation(paiclupCapital);
					break;
				default:
					break;
				}
			}
            lfChangeCheck.setCreater(Constants.SYS_SHORT_NAME);
            lfChangeCheck.setUpdater(Constants.SYS_SHORT_NAME);
            lfChangeCheck.setUpdateTime(DateUtil.getNowTimestamp());
            saveList.add(lfChangeCheck);
        }
        lfChangeCheckDao.batchSaveUpdata(saveList);
    }
    /**
     * 
     *@Description 财务状况检查
     *@param taskId
     *@throws Exception
     * @author xyh
     */
    private void insertLfFincheck(Integer taskId,String finCheckType) throws Exception {
    	//获取客户信息变动检查项
        List<SysDic> finCheckList = sysDicDAO.queryByOptType(finCheckType);
        if(CollectionUtils.isEmpty(finCheckList)){
            ExceptionUtil.throwException("未获取到贷后检查财务状况检查细项");
        }
		
        List<LfFinCheck> saveList = new ArrayList<>();
        for(SysDic sysDic:finCheckList){
        	LfFinCheck lfFinCheck = new LfFinCheck();
        	lfFinCheck.setTaskId(taskId);
        	lfFinCheck.setSubjectId(sysDic.getEnName());
        	lfFinCheck.setSubjectName(sysDic.getCnName());
        	//其中一些描述不能以否定为默认值，使用sysdic中的flag字段来设置默认值
        	lfFinCheck.setHasChage(sysDic.getFlag());
        	//lfFinCheck.setHasChage("0");//默认为否
        	lfFinCheck.setCreater(Constants.SYS_SHORT_NAME);
        	lfFinCheck.setUpdater(Constants.SYS_SHORT_NAME);
        	lfFinCheck.setUpdateTime(DateUtil.getNowTimestamp());
        	saveList.add(lfFinCheck);
        }
        lfFinCheckDao.batchSaveUpdata(saveList);
    }
    
	/**
	 * 
	 *@Description 批量插入数据
	 *@param taskId 任务号
	 *@param typeId 插入数据类型 ---- RG_LF_JY_01
	 *@param typeName 插入数据名 ----- 贷后-经营情况-外部环境
	 *@throws Exception
	 * @author xyh
	 */
	private void insertLfSituationcheck(Integer taskId,String typeId,String typeName)throws Exception{
		//获取客户信息变动检查项
        List<SysDic> finCheckList = sysDicDAO.queryByOptType(typeId);
        if(CollectionUtils.isEmpty(finCheckList)){
            ExceptionUtil.throwException("未获取到"+typeName+"细项");
        }
		List<LfSituationCheck> saveList = new ArrayList<>();
		for(SysDic sysDic:finCheckList){
			LfSituationCheck lfSituationCheck = new LfSituationCheck();
			lfSituationCheck.setTaskId(taskId);
			lfSituationCheck.setTypeFirstNo(typeId);
			lfSituationCheck.setTypeFirstName(typeName);
			lfSituationCheck.setSituationEnName(sysDic.getEnName());
			lfSituationCheck.setSituationCnName(sysDic.getCnName());
			//其中一些描述不能以否定为默认值，使用sysdic中的flag字段来设置默认值
			lfSituationCheck.setHasChage(sysDic.getFlag());
			//lfSituationCheck.setHasChage("0");//默认设置否
			lfSituationCheck.setCreater(Constants.SYS_SHORT_NAME);
			lfSituationCheck.setUpdater(Constants.SYS_SHORT_NAME);
			lfSituationCheck.setUpdateTime(DateUtil.getNowTimestamp());
			saveList.add(lfSituationCheck);
		}
		lfSituationCheckDao.batchSaveUpdata(saveList);
	}
	
	/**
	 * 
	 * @Description 财务状况-费用缴纳
	 * @param taskId
	 * @throws Exception
	 * @author xyh
	 */
	private void insertFinFees(Integer taskId) throws Exception {
		// 获取财务状况-费用缴纳
		List<SysDic> finCheckList = sysDicDAO.queryByOptType("RG_LF_FIN_FEE");
		if (CollectionUtils.isEmpty(finCheckList)) {
			ExceptionUtil.throwException("未获取到财务状况-费用缴纳细项");
		}

		List<LfPaymentOfFees> saveList = new ArrayList<>();
		for (SysDic sysDic : finCheckList) {
			LfPaymentOfFees lfPaymentOfFees = new LfPaymentOfFees();
			lfPaymentOfFees.setTaskId(taskId);
			lfPaymentOfFees.setFeeTypeNo(sysDic.getEnName());
			lfPaymentOfFees.setFeeTypeName(sysDic.getCnName());
			//这两个默认值字段，没有语义上需要设置其他默认值的情况
			lfPaymentOfFees.setIsNormal("1");//默认正常
			lfPaymentOfFees.setIsDown("0");//默认未大幅度下降
			lfPaymentOfFees.setCreater(Constants.SYS_SHORT_NAME);
			lfPaymentOfFees.setUpdater(Constants.SYS_SHORT_NAME);
			lfPaymentOfFees.setUpdateTime(DateUtil.getNowTimestamp());
			saveList.add(lfPaymentOfFees);
		}
		lfPaymentOfFeesDao.batchSaveUpdata(saveList);
	}
	
	private void insertLoanProject(Integer taskId)throws Exception{
		//插入申请贷款概况
		LfLoanProjectSummary summary = new LfLoanProjectSummary();
		summary.setTaskId(taskId);
		summary.setCreater(Constants.SYS_SHORT_NAME);
		summary.setUpdater(Constants.SYS_SHORT_NAME);
		summary.setUpdateTime(DateUtil.getNowTimestamp());
		lfLoanProjectSummaryDao.saveOrUpdate(summary);
		
		List<SysDic> finCheckList = sysDicDAO.queryByOptType("RG_LF_LJ");
		if (CollectionUtils.isEmpty(finCheckList)) {
			ExceptionUtil.throwException("未获取到申贷项目概况细项");
		}
		List<LfLoanProjectDetail> saveList = new ArrayList<>();
		for (SysDic sysDic : finCheckList) {
			LfLoanProjectDetail lfLoanProjectDetail = new LfLoanProjectDetail();
			lfLoanProjectDetail.setTaskId(taskId);
			lfLoanProjectDetail.setDetailTypeNo(sysDic.getEnName());
			lfLoanProjectDetail.setDetailTypeName(sysDic.getCnName());
			lfLoanProjectDetail.setCreater(Constants.SYS_SHORT_NAME);
			lfLoanProjectDetail.setUpdater(Constants.SYS_SHORT_NAME);
			lfLoanProjectDetail.setUpdateTime(DateUtil.getNowTimestamp());
			saveList.add(lfLoanProjectDetail);
		}
		lfLoanProjectDetailDao.batchSaveUpdata(saveList);
	}

	@Override
	public void syncLfFullData(Integer taskId, String custName) throws Exception {
		
	}

	@Override
	public void syncWarnInfo(Integer taskId, String custName, String date) throws Exception {
		CustomerInfo cust = customerInfoDao.getDtmpSingleByProperty("customername", custName);
		String xdCustNo = null;
		if(cust!=null){
			xdCustNo = cust.getCustomerid();
		}
		List<LfRiskWarningInfo> warnList = lfTaskDao.getWarnInfoList(custName, xdCustNo, date);
		if(CollectionUtils.isEmpty(warnList)){
			return;
		}
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		for(LfRiskWarningInfo warn : warnList){
			params.put("riskNo", warn.getRiskNo());
			List<LfRiskWarningInfo> existList = lfRiskWarningInfoDao.selectByProperty(params);
			if(!CollectionUtils.isEmpty(existList)){
				log.info(warn.getRiskNo() + "--预警内容：" +warn.getRiskName() + "在贷后任务号为： " + taskId + "--已经存在，跳过该预警项。");
				continue;
			}
			warn.setTaskId(taskId);
			lfRiskWarningInfoDao.saveOrUpdate(warn);
		}	
	}
	
	public void syncWarnInfo(Integer taskId, String custName, String startDate, String endDate) throws Exception{
		CustomerInfo cust = customerInfoDao.getDtmpSingleByProperty("customername", custName);
		String xdCustNo = null;
		if(cust!=null){
			xdCustNo = cust.getCustomerid();
		}
		List<LfRiskWarningInfo> warnList = lfTaskDao.getWarnInfoList(custName, xdCustNo, startDate, endDate);
		if(CollectionUtils.isEmpty(warnList)){
			return;
		}
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		for(LfRiskWarningInfo warn : warnList){
			params.put("riskNo", warn.getRiskNo());
			List<LfRiskWarningInfo> existList = lfRiskWarningInfoDao.selectByProperty(params);
			if(!CollectionUtils.isEmpty(existList)){
				log.info(warn.getRiskNo() + "--预警内容：" +warn.getRiskName() + "在贷后任务号为： " + taskId + "--已经存在，跳过该预警项。");
				continue;
			}
			warn.setTaskId(taskId);
			lfRiskWarningInfoDao.saveOrUpdate(warn);
		}	
	}
	
	
	/**
	 * @Description 插入本行用心情况
	 * @param taskId
	 * @author xyh
	 */
	public void insertCreditSelfBankInfo(Integer taskId)throws Exception{
		//获取到贷后客户的核心号
		LfTask lfTask = lfTaskDao.selectByPrimaryKey(taskId);
		CusBase cusBase = cusBaseDAO.selectSingleByProperty("customerNo", lfTask.getCustomerNo());
		//获取用信情况
		List<CustLoanDto> custLoanDtos = lnlnslnsDao.getCustLoan(cusBase.getMfCustomerNo(), null);
		if(CollectionUtils.isEmpty(custLoanDtos)){
			return;
		}
		List<LfCreditSelfBank> saveList = new ArrayList<>();
		for(CustLoanDto dto : custLoanDtos){
			LfCreditSelfBank lfCreditSelfBank = new LfCreditSelfBank(taskId, dto.getTypename(), 
					dto.getBalance(), dto.getFrstAlfdDt(), dto.getDueDt(),
					dto.getPurpose(), dto.getVouchtype());
			lfCreditSelfBank.setCreater(Constants.SYS_SHORT_NAME);
			lfCreditSelfBank.setUpdater(Constants.SYS_SHORT_NAME);
			lfCreditSelfBank.setUpdateTime(DateUtil.getNowTimestamp());
			saveList.add(lfCreditSelfBank);
		}
		lfCreditSelfBankDao.batchSaveUpdata(saveList);
	}

	@Override
	public void syncComFullDetail(String custNo) throws Exception {
		//获取客户基本信息
        CusBase cusBase = cusBaseDAO.selectSingleByProperty("customerNo", custNo);
        if(!StringUtils.isEmpty(cusBase.getQccSyncTime())){
        	log.info("客户号为："+custNo + "的工商数据已同步，企查查更新时间为：" + cusBase.getQccSyncTime());
        	return;
        }
        //同步企查查数据  --》这部分数据贷前通用
		synchronizationQCCService.syncQccAllData(cusBase.getCustomerNo());
		cusBase.setQccSyncTime(DateUtil.getNowY_m_dStr());
		cusBaseDAO.saveOrUpdate(cusBase);
	}
	
	@Override
	public void syncPently(Integer taskId, String custNo) throws Exception {
		if(taskId==null || taskId == 0){
			log.error("贷后任务号为空，同步行政数据失败");
			return;
		}
		if(StringUtil.isEmptyString(custNo)){
			log.error("客户号为空，同步行政数据失败");
			return;
		}
		try{
			List<CompPenalty> penaltyList = compPenaltyDao.selectByProperty("customerNo", custNo);
			if(CollectionUtils.isEmpty(penaltyList)){
				log.info("客户号为："+custNo + "暂无行政处罚信息（数据来源:企查查）");
				return;
			}
	//		List<LfAdminSanInfo> lfAdminSanInfoList = lfAdminSanInfoService.selectByProperty("taskId", taskId);
			for(CompPenalty penalty : penaltyList){
				LfAdminSanInfo sanInfo = new LfAdminSanInfo();
				sanInfo.setTaskId(taskId);
				sanInfo.setOrgName(penalty.getOfficeName());
				sanInfo.setSubjectMatter(penalty.getPenaltyType());
				sanInfo.setContent(penalty.getContent());
				lfAdminSanInfoDao.saveOrUpdate(sanInfo);
			}
			
//			//裁判文书列表
//			List<CompJudgment> judgList = compJudgmentDao.selectByProperty("customerNo", custNo);
//			List<CompZhiXingItems> zhixingList = compZhiXingItemsDao.selectByProperty("customerNo", custNo);
//			List<CompShiXinItems> shixinList = compShiXinItemsDao.selectByProperty("customerNo", custNo);
//			
//			LfLocalResult localResult = new LfLocalResult();
//			localResult.setTaskId(taskId);
//			localResult.setCaseCount(CollectionUtils.isEmpty(judgList)?0:judgList.size());
//			localResult.setIsEnforce(CollectionUtils.isEmpty(zhixingList)?Constants.FALSE:Constants.TRUE);
//			localResult.setIsBreachTrust(CollectionUtils.isEmpty(shixinList)?Constants.FALSE:Constants.TRUE);
//			lfLocalResultDao.saveOrUpdate(localResult);
			
			log.info("客户号为："+custNo + " 行政处罚及司法信息同步成功（数据来源:企查查）");
		}catch(Exception e){
			e.printStackTrace();
			log.error("客户号为："+custNo + " 行政处罚信息同步异常", e);
		}
	}

	@Override
	public void syncLocalResult(Integer taskId, String custNo) throws Exception {
		if(taskId==null || taskId == 0){
			log.error("贷后任务号为空，同步司法诉讼数据失败");
			return;
		}
		if(StringUtil.isEmptyString(custNo)){
			log.error("客户号为空，同步司法诉讼数据失败");
			return;
		}
		try{
			//裁判文书列表
			List<CompJudgment> judgList = compJudgmentDao.selectByProperty("customerNo", custNo);
			List<CompZhiXingItems> zhixingList = compZhiXingItemsDao.selectByProperty("customerNo", custNo);
			List<CompShiXinItems> shixinList = compShiXinItemsDao.selectByProperty("customerNo", custNo);
			
			LfLocalResult localResult = new LfLocalResult();
			localResult.setTaskId(taskId);
			localResult.setCaseCount(CollectionUtils.isEmpty(judgList)?0:judgList.size());
			localResult.setIsEnforce(CollectionUtils.isEmpty(zhixingList)?Constants.FALSE:Constants.TRUE);
			localResult.setIsBreachTrust(CollectionUtils.isEmpty(shixinList)?Constants.FALSE:Constants.TRUE);
			
			lfLocalResultDao.saveOrUpdate(localResult);
		}catch(Exception e){
			e.printStackTrace();
			log.error("客户号为："+custNo + " 司法诉讼信息同步异常", e);
		}
		
	}
	
	  @Override
	  public List<LfTaskDto> getLfTaskList(List<String> taskTypes,String customerNo,String customerName,String userName,
	    		String orgName, String riskUserName,String userNo,String orgNo,Integer taskPosition) throws Exception {
	        StringBuffer sql = new StringBuffer();
	        Map<String,Object> params = new HashMap<>();
	        
	        sql.append(" select a.id,c.XD_CUSTOMER_NO as xdCustNo,c.customer_no customerNo,")
	                .append(" a.TASK_TYPE as taskType,a.TASK_POSITION as taskPosition, a.USER_NO as userNo,")
	                .append(" b.USER_NAME as userName,d.ORGANNAME, a.CREATE_TIME as createTime,")
	                .append(" a.CUSTOMER_NAME as customerName,  a.LOAN_BALANCE as loanBalance ,")
	                .append(" a.INDUSTRY_TYPE as industryType,a.MARTUITY,a.RISKUSER,a.RISKUSERNAME")
	                .append(" from LF_TASK a  ")
	                .append(" left join CUS_BASE c on c.CUSTOMER_NO=a.CUSTOMER_NO")
	                .append(" join BS_USER_INFO b ON a.user_no=b.user_id")
	                .append(" left join GDTCESYS.BS_org d on b.ACC_ORG_NO=d.ORGANNO")
	                .append(" where a.status=:status ");

	        params.put("status",Constants.NORMAL);
	        if(!StringUtil.isEmptyString(customerNo)){
	            sql.append(" and a.CUSTOMER_NO=:customerNo ");
	            params.put("customerNo",customerNo);
	        }
	        if(!StringUtil.isEmptyString(customerName)){
	            sql.append(" and c.CUSTOMER_NAME like:customerName ");
	            params.put("customerName","%"+customerName+"%");
	        }
	        
	        if(!StringUtil.isEmptyString(userName)){
	            sql.append(" and b.USER_NAME like:userName ");
	            params.put("userName","%"+userName+"%");
	        }
	        if(!StringUtil.isEmptyString(orgName)){
	            sql.append(" and d.ORGANNAME like:orgName ");
	            params.put("orgName","%"+orgName+"%");
	        }
	        if(!StringUtil.isEmptyString(riskUserName)){
	            sql.append(" and a.RISKUSERNAME like:riskUserName ");
	            params.put("riskUserName","%"+riskUserName+"%");
	        }
	        if(taskPosition != null){
	            sql.append(" and a.TASK_POSITION=:taskPosition");
	            params.put("taskPosition",taskPosition);
	        }
	        if(!StringUtil.isEmptyString(userNo)){
	            sql.append(" and a.RISKUSER=:userNo ");
	            params.put("userNo",userNo);
	        }
	        if(!StringUtil.isEmptyString(orgNo)){
	            sql.append(" and b.ACC_ORG_NO=:orgNo ");
	            params.put("orgNo",orgNo);
	        }
	        //in 查询的使用：1.括号在sql中   2.直接把list放入param中，会自动拼接
	        if(!CollectionUtils.isEmpty(taskTypes)){
	        	sql.append(" and a.TASK_TYPE in (:taskTypes)");
	        	params.put("taskTypes", taskTypes);
	        }
	        sql.append(" order by a.CREATE_TIME desc ");
	       
	       List<LfTaskDto> pager = lfTaskDao.findCustListBySql(LfTaskDto.class,sql.toString(),params);
	        return pager;
	    }
	  
	  public LfTask syncLfCustBaseInfo(LfTask task,String xdCustNo){
		  List<LfTask> lfTaskList = lfTaskDao.getLfCustBaseInfo(xdCustNo);
		  if(!CollectionUtils.isEmpty(lfTaskList)){
			  LfTask lfTask = lfTaskList.get(0);
			  task.setCustomerCreNum(lfTask.getCustomerCreNum());
			  task.setRegisterAddress(lfTask.getRegisterAddress());
			  task.setRunAddress(lfTask.getRunAddress());
			  task.setControlPerson(lfTask.getControlPerson());
			  task.setLegalPerson(lfTask.getLegalPerson());
			  task.setRunScope(lfTask.getRunScope());
			  task.setFiveClass(lfTask.getFiveClass());
			  task.setAllAmount(lfTask.getAllAmount());
		  }
		  return task;
		  
	  }
	  
	  
	  @Override
	  public void createManuallyStopTask(String customerNo) throws Exception {
		  
			LfTask lfTask;
			
			SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
			Date date = new Date();
			String currDate = sf.format(date);
			String taskName = currDate.substring(2, 4) + "年" + currDate.substring(4, 6) + "月"
					+ currDate.substring(6, 8) + "号手工创建";
			
	        
			//获取贷后客户信息
			LfCustInfo lfCustInfo = lfCustInfoDao.selectSingleByProperty("customerNo", customerNo);
			
			//获取客户基本信息
	        CusBase cusBase = cusBaseDAO.selectSingleByProperty("customerNo",lfCustInfo.getCustomerNo());
	       
			lfTask = createNewTask(lfCustInfo,cusBase.getCustomerName(),
					Constants.LF_TASK_TYPE.FX_THREE_MONTH_TASK, cusBase.getManagerUserId());
			lfTask.setTaskName(taskName);
			
			//同步企业信息
	        lfTask = syncLfCustBaseInfo(lfTask,cusBase.getXdCustomerNo());

			lfTask = lfTaskDao.saveOrUpdate(lfTask);
			//同步三个月前到今天的预警信息
	        this.syncWarnInfo(lfTask.getId(), cusBase.getCustomerName(), 
	        		DateUtil.format(DateUtil.addMonth(new Date(), -3),Constants.DATE_MASK2), 
	        		DateUtil.format(new Date(), Constants.DATE_MASK2));
	        
	      //指定处理任务在专职风险经理
            User user = userSV.queryById(cusBase.getManagerUserId());
            if(user!=null){
            String accOrgNo = user.getAccOrgNo();//获取管护在用户所在机构号
	            if(!StringUtils.isEmpty(accOrgNo)){
	                LfRiskManager manager = lfRiskManagerService.selectSingleByProperty("organno", accOrgNo);
	                if(manager==null){
	                	log.info(cusBase.getCustomerName() + "管护客户经理是：" + user.getUserName() + "--所在机构为： " + user.getAccOrgNo() + "---该机构下暂未指定风险经理");
	                }else{
	                	lfTask.setRiskUser(manager.getRiskuser());
	                	lfTask.setRiskUserName(manager.getRiskusername());
	                }
	            }
            }

	        //同步客户黑名单信息
	        lfInternalControlInfoService.syncInternalControl(lfTask.getId(), 
	        		cusBase.getXdCustomerNo(), DateUtil.format(new Date(), Constants.DATE_MASK2), 6);
	        //同步查冻扣账户 GGG
	        lfCfdAccountInfoService.syncCfdAccount(lfTask.getId(), cusBase.getMfCustomerNo());
	        
	        //同步企业工商信息
	        this.syncComFullDetail(cusBase.getCustomerNo());
	        this.syncPently(lfTask.getId(), cusBase.getCustomerNo());
	        this.syncLocalResult(lfTask.getId(), cusBase.getCustomerNo());
			
	    }  

}
