package com.beawan.loanAfter.service.impl;

import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.beawan.loanAfter.dao.LfCfdAccountInfoDao;
import com.beawan.loanAfter.entity.LfCfdAccountInfo;
import com.beawan.loanAfter.entity.LfInternalControlInfo;
import com.beawan.loanAfter.service.LfCfdAccountInfoService;
import com.platform.util.DateUtil;

/**
 * @author yzj
 */
@Service("lfCfdAccountInfoService")
public class LfCfdAccountInfoServiceImpl extends BaseServiceImpl<LfCfdAccountInfo> implements LfCfdAccountInfoService{

    private static final Logger log = Logger.getLogger(LfCfdAccountInfoServiceImpl.class);
	
	/**
    * 注入DAO
    */
    @Resource(name = "lfCfdAccountInfoDao")
    public void setDao(BaseDao<LfCfdAccountInfo> dao) {
        super.setDao(dao);
    }
    @Resource
    public LfCfdAccountInfoDao lfCfdAccountInfoDao;
	@Override
	public void batchSaveUpdata(List<LfCfdAccountInfo> saveList) throws Exception {
		lfCfdAccountInfoDao.batchSaveUpdata(saveList);
		
	}
	@Override
	public void syncCfdAccount(Integer taskId, String mfCustNo) throws Exception {
		if(taskId==null || taskId==0){
			log.warn("同步查冻扣账户时，任务号号为空，同步失败");
			return ;
		}
		if(StringUtils.isEmpty(mfCustNo)){
			log.warn("同步查冻扣账户时，核心客户号为空，同步失败");
			return ;
		}
		Map<String, Object> params = new HashMap<>();
		params.put("mfCustNo", mfCustNo);
		List<LfCfdAccountInfo> cfdList = new ArrayList();
		//获取 活期 事故档
		StringBuilder saSql = new StringBuilder();
		saSql.append("select A.SA_ACCT_NO accountNum,B.SA_ACCD_COD saAccdCod,D.CODE_VALUE_REFE cfdType, ")
		.append("B.SA_ACCD_DT saAccdDt,B.SA_ACCD_AMT saAccdAmt,B.SA_BRANCH_NAME cfdOrg,B.SA_BRANCH_TYPE,'SA' accType ")
		.append("from CBOD.SAACNACN A ")
		.append("join CBOD.SAACNEVT B on A.SA_ACCT_NO=B.FK_SAACN_KEY ")
		.append("JOIN CBOD.SAACNAMT C ON A.SA_ACCT_NO=C.FK_SAACN_KEY ")
		.append("LEFT JOIN CMIS.F_COM_CODE_VALUE D ON B.SA_ACCD_COD=D.CODE_VAL ")
		.append("WHERE B.SA_PROCESS_FLAG='N' ")
		.append("AND C.SA_DDP_ACCT_STS='01' ")
		.append("and B.SA_ACCD_COD in ('2110','2120','2310','2330','6010') ")
		.append("AND D.CODE_NO='ODS0556' ")
		.append("AND A.SA_CUST_NO=:mfCustNo ");
		List<LfCfdAccountInfo> saList = lfCfdAccountInfoDao.findCustListBySql(LfCfdAccountInfo.class, saSql, params);

		//获取 定期 事故档  --》经手动查询  数据都是空的
		StringBuilder tdSql = new StringBuilder();
		tdSql.append("select A.TD_TD_ACCT_NO accountNum,B.TD_ACCD_COD,D.CODE_VALUE_REFE cfdType, ")
		.append("B.TD_BRANCH_NAME cfdOrg,TD_FRZN_AMT saAccdAmt,'TD' accType ")
		.append("from CBOD.TDACNACN A ")
		.append("join CBOD.TDACNEVT B on A.TD_TD_ACCT_NO=B.FK_TDACN_KEY ")
		.append("LEFT JOIN CMIS.F_COM_CODE_VALUE D ON B.TD_ACCD_COD=D.CODE_VAL ")
		.append("WHERE B.TD_PROCESS_FLAG='N' AND A.TD_ACCT_STS='01' ")
		.append("and B.TD_ACCD_COD in ('2003','2004','2006','2007','2031','2047','2048','2062','2063','2065','2066') ")
		.append("AND D.CODE_NO='ODS0595' ")
		.append("AND A.TD_CUST_NO=:mfCustNo ");
		List<LfCfdAccountInfo> tdList = lfCfdAccountInfoDao.findCustListBySql(LfCfdAccountInfo.class, tdSql, params);
		
		if(saList!=null){
			cfdList.addAll(saList);
		}
		if(tdList!=null){
			cfdList.addAll(tdList);
		}
		//保存到对公系统中
		if(cfdList.size()==0){
			log.info("任务号：" + taskId + "查冻扣信息为空，同步数据完成");
		}
		for(LfCfdAccountInfo account : cfdList){
			account.setTaskId(taskId);
			account.setStatus(Constants.NORMAL);
			account.setCreater("DGZJ");
			account.setCreateTime(DateUtil.getNowTimestamp());
		}
		this.batchSaveUpdata(cfdList);
		log.info("任务号：" + taskId + "查冻扣信息，同步完成");
		
	}
}
