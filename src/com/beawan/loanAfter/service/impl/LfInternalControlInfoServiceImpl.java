package com.beawan.loanAfter.service.impl;

import com.beawan.base.dao.ISysDicDAO;
import com.beawan.base.entity.SysDic;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.beawan.loanAfter.controller.LoanAfterController;
import com.beawan.loanAfter.dao.LfInternalControlInfoDao;
import com.beawan.loanAfter.entity.LfInternalControlInfo;
import com.beawan.loanAfter.service.LfInternalControlInfoService;
import com.ibm.db2.jcc.a.a;
import com.platform.util.DateUtil;

/**
 * @author yzj
 */
@Service("lfInternalControlInfoService")
public class LfInternalControlInfoServiceImpl extends BaseServiceImpl<LfInternalControlInfo> implements LfInternalControlInfoService{
	

    private static final Logger log = Logger.getLogger(LfInternalControlInfoServiceImpl.class);
	
    /**
    * 注入DAO
    */
    @Resource(name = "lfInternalControlInfoDao")
    public void setDao(BaseDao<LfInternalControlInfo> dao) {
        super.setDao(dao);
    }
    @Resource
    public LfInternalControlInfoDao lfInternalControlInfoDao;

	@Resource
	private ISysDicDAO sysDicDAO;
	@Override
	public void batchSaveUpdata(List<LfInternalControlInfo> saveList) throws Exception {
		lfInternalControlInfoDao.batchSaveUpdata(saveList);
		
	}
	
	
	@Override
	public void syncInternalControl(Integer taskId, String xdCustNo, String lastTime, Integer month) throws Exception {
		if(taskId==null || taskId==0){
			log.warn("同步黑名单客户时，任务号号为空，同步失败");
			return ;
		}
		if(StringUtils.isEmpty(xdCustNo)){
			log.warn("同步黑名单客户时，信贷客户号为空，同步失败");
			return ;
		}
		Map<String, Object> params = new HashMap<>();
		StringBuilder sql = new StringBuilder();
		sql.append(" select '黑名单' listName,INLISTREASON addListReason,INPUTDATE addListTime,")
			.append(" SERIALNO,CUSTOMERNAME,SECTIONTYPE,INPUTORGID,INPUTUSERID")
			.append(" from CMIS.CUSTOMER_SPECIAL ")
			.append(" WHERE BLACKSHEETORNOT='Y' AND CONFIRMORNOT='Y'")
			.append(" AND CUSTOMERID =:xdCustNo ")
			.append(" and INPUTDATE >= :startTime")
			.append(" AND INPUTDATE <= :lastTime");
		if(month==null || month==0)
			month = 6;
		Date startDate = DateUtil.addMonth(DateUtil.parse(lastTime, Constants.DATE_MASK2), -month);
		String startTime = DateUtil.format(startDate, Constants.DATE_MASK2);
		
		params.put("xdCustNo", xdCustNo);
		params.put("startTime", startTime);
		params.put("lastTime", lastTime);
		
		List<LfInternalControlInfo> lfInternalCtls = lfInternalControlInfoDao
				.findCustListBySql(LfInternalControlInfo.class, sql, params);
		if(CollectionUtils.isEmpty(lfInternalCtls)){
			log.info("任务号为：" + taskId + "---信贷客户号为：" + xdCustNo +"---不再内控黑名单中,数据同步完成");
			return;
		}
		for(LfInternalControlInfo inctl : lfInternalCtls){
			SysDic inDic = sysDicDAO.queryByEnNameAndOptType(inctl.getAddListReason()
					, SysConstants.BsDicConstant.RG_IN_LIST_REASON);
			if(inDic!=null && !StringUtils.isEmpty(inDic.getCnName())){
				inctl.setAddListReason(inDic.getCnName());
			}
			inctl.setTaskId(taskId);
		}
		lfInternalControlInfoDao.batchSaveUpdata(lfInternalCtls);
	}
	
}
