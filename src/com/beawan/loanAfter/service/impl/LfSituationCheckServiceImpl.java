package com.beawan.loanAfter.service.impl;

import com.beawan.base.dao.ISysDicDAO;
import com.beawan.base.entity.SysDic;
import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.beawan.loanAfter.dao.LfSituationCheckDao;
import com.beawan.loanAfter.entity.LfSituationCheck;
import com.beawan.loanAfter.service.LfSituationCheckService;
import com.platform.util.DateUtil;
import com.platform.util.ExceptionUtil;
import com.platform.util.RgnshUtils;

/**
 * @author yzj
 */
@Service("lfSituationCheckService")
public class LfSituationCheckServiceImpl extends BaseServiceImpl<LfSituationCheck> implements LfSituationCheckService{
    /**
    * 注入DAO
    */
    @Resource(name = "lfSituationCheckDao")
    public void setDao(BaseDao<LfSituationCheck> dao) {
        super.setDao(dao);
    }
    @Resource
    public LfSituationCheckDao lfSituationCheckDao;

	@Override
	public void batchSaveUpdata(List<LfSituationCheck> saveList) throws Exception {
		lfSituationCheckDao.batchSaveUpdata(saveList);
		
	}

	@Override
	public List<LfSituationCheck> queryByTypeFirstLike(String typeFirstNo, Integer taskId) throws Exception {
		return lfSituationCheckDao.queryByTypeFirstLike(typeFirstNo, taskId);
		/*StringBuffer sql = new StringBuffer(" from LfSituationCheck where 1=1 ");
		sql.append(" and typeFirstNo like:typeFirstNo")
		   .append(" and taskId =:taskId")
		   .append(" and status =:status");
		Map<String,Object>param = new HashMap<>();
		param.put("typeFirstNo","%"+typeFirstNo+"%");
		param.put("taskId",taskId);
        param.put("status",Constants.NORMAL);
        List<LfSituationCheck> lfSituationCheckList = lfSituationCheckDao.select(sql.toString(), param);
       return lfSituationCheckList;*/
	}
}
