package com.beawan.loanAfter.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.core.Pagination;
import com.beawan.loanAfter.dao.FcCustDao;
import com.beawan.loanAfter.dto.LfCustDto;
import com.beawan.loanAfter.entity.FcCust;
import com.beawan.loanAfter.service.FcCustService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

/**
 * @author yzj
 */
@Service("fcCustService")
public class FcCustServiceImpl extends BaseServiceImpl<FcCust> implements FcCustService{
    /**
    * 注入DAO
    */
    @Resource(name = "fcCustDao")
    public void setDao(BaseDao<FcCust> dao) {
        super.setDao(dao);
    }
    @Resource
    public FcCustDao fcCustDao;
    
	@Override
	public Pagination<LfCustDto> getFcCustList(Integer taskId, String custName, String userName, int page, int pageSize) throws Exception {
		
		Pagination<LfCustDto> pager = fcCustDao.getFcCustList(taskId, custName, userName, page, pageSize);
		return pager;
	}
}
