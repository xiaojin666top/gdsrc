package com.beawan.loanAfter.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.loanAfter.dao.LfFinCheckDao;
import com.beawan.loanAfter.entity.LfFinCheck;
import com.beawan.loanAfter.service.LfFinCheckService;

/**
 * @author yzj
 */
@Service("lfFinCheckService")
public class LfFinCheckServiceImpl extends BaseServiceImpl<LfFinCheck> implements LfFinCheckService{
    /**
    * 注入DAO
    */
    @Resource(name = "lfFinCheckDao")
    public void setDao(BaseDao<LfFinCheck> dao) {
        super.setDao(dao);
    }
    @Resource
    public LfFinCheckDao lfFinCheckDao;
	@Override
	public void batchSaveUpdata(List<LfFinCheck> saveList) throws Exception {
		lfFinCheckDao.batchSaveUpdata(saveList);
	}
}
