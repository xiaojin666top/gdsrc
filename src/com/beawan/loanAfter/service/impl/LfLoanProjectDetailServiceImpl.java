package com.beawan.loanAfter.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.Resource;

import com.beawan.loanAfter.dao.LfLoanProjectDetailDao;
import com.beawan.loanAfter.entity.LfLoanProjectDetail;
import com.beawan.loanAfter.service.LfLoanProjectDetailService;

/**
 * @author yzj
 */
@Service("lfLoanProjectDetailService")
public class LfLoanProjectDetailServiceImpl extends BaseServiceImpl<LfLoanProjectDetail> implements LfLoanProjectDetailService{
    /**
    * 注入DAO
    */
    @Resource(name = "lfLoanProjectDetailDao")
    public void setDao(BaseDao<LfLoanProjectDetail> dao) {
        super.setDao(dao);
    }
    @Resource
    public LfLoanProjectDetailDao lfLoanProjectDetailDao;
	@Override
	public void batchSaveUpdata(List<LfLoanProjectDetail> saveList) throws Exception {
		lfLoanProjectDetailDao.batchSaveUpdata(saveList);
	}
}
