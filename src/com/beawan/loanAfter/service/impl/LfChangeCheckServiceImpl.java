package com.beawan.loanAfter.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.loanAfter.dao.LfChangeCheckDao;
import com.beawan.loanAfter.service.LfChangeCheckService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.loanAfter.entity.LfChangeCheck;

import java.util.List;

/**
 * @author yzj
 */
@Service("lfChangeCheckService")
public class LfChangeCheckServiceImpl extends BaseServiceImpl<LfChangeCheck> implements LfChangeCheckService{
    /**
    * 注入DAO
    */
    @Resource(name = "lfChangeCheckDao")
    public void setDao(BaseDao<LfChangeCheck> dao) {
        super.setDao(dao);
    }
    @Resource
    public LfChangeCheckDao lfChangeCheckDao;

    @Override
    public void batchSaveUpdata(List<LfChangeCheck> saveList) throws Exception {
        lfChangeCheckDao.batchSaveUpdata(saveList);
    }
}
