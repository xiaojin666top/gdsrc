package com.beawan.loanAfter.service.impl;

import com.beawan.base.dao.ISysDicDAO;
import com.beawan.base.entity.SysDic;
import com.beawan.common.SysConstants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.loanAfter.dao.FcBusiContractDao;
import com.beawan.loanAfter.dao.FcCustDao;
import com.beawan.loanAfter.dao.FcCustFullInfoDao;
import com.beawan.loanAfter.dao.FcFinaDataDao;
import com.beawan.loanAfter.dto.FcAnalysisDto;
import com.beawan.loanAfter.dto.FcCustDto;
import com.beawan.loanAfter.entity.FcBusiContract;
import com.beawan.loanAfter.entity.FcCust;
import com.beawan.loanAfter.entity.FcCustFullInfo;
import com.beawan.loanAfter.entity.FcFinaData;
import com.beawan.loanAfter.service.FcCustFullInfoService;
import com.beawan.survey.custInfo.bean.CompBaseEquity;
import com.beawan.survey.custInfo.dao.ICompBaseEquityDAO;
import com.platform.util.MapperUtil;
import com.platform.util.StringUtil;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Resource;

/**
 * @author yzj
 */
@Service("fcCustFullInfoService")
public class FcCustFullInfoServiceImpl extends BaseServiceImpl<FcCustFullInfo> implements FcCustFullInfoService{
    /**
    * 注入DAO
    */
    @Resource(name = "fcCustFullInfoDao")
    public void setDao(BaseDao<FcCustFullInfo> dao) {
        super.setDao(dao);
    }
    @Resource
    public FcCustFullInfoDao fcCustFullInfoDao;
    @Resource
    public FcBusiContractDao fcBusiContractDao;
    @Resource
    public FcFinaDataDao fcFinaDataDao;
    @Resource
	private ISysDicDAO ISysDicDAO;
    @Resource
	private ICompBaseEquityDAO compBaseEquityDAO;
    @Resource
    public FcCustDao fcCustDao;
    
    public FcCustDto getByTaskCustId(Integer taskCustId){
    	FcCustFullInfo entity = fcCustFullInfoDao.selectSingleByProperty("taskCustId", taskCustId);
    	if(entity == null) return null;
    	return MapperUtil.trans(entity, FcCustDto.class);
    }

	@Override
	public FcAnalysisDto getFcAnalysis(Integer taskCustId) {
		FcCustFullInfo entity = fcCustFullInfoDao.selectSingleByProperty("taskCustId", taskCustId);
    	if(entity == null) return null;
    	return MapperUtil.trans(entity, FcAnalysisDto.class);
	}

	@Override
	public String getLoanAnaly(Integer taskCustId) {
		FcCustFullInfo entity = fcCustFullInfoDao.selectSingleByProperty("taskCustId", taskCustId);
		if(entity == null || StringUtil.isEmptyString(entity.getReportDate())) 
			return "该企业在我行暂无存款、贷款信息！";
		String result = "信贷关系史："+ entity.getCustomerName() +"在本行拥有以下存款帐户："
				+ entity.getDeposit() + "，截止" + entity.getReportDate();
		double depositSum = entity.getDepositSum();
		if(depositSum >= 10000d){
			result += "，在我行存款余额为：" + String.format("%.2f", depositSum/10000) + "万元，";
		}else{
			result += "，在我行存款余额为：" + depositSum + "元，";
		}
		double loanSum = entity.getLoanSum();
		if(loanSum >= 10000d){
			result += "在我行贷款余额为：" + String.format("%.2f", loanSum/10000) + "万元。";
		}else{
			result += "在我行贷款余额为：" + loanSum + "元。";
		}
		return result;
	}
	
	@Override
	public String getFinaAnaly(Integer taskCustId) {
		FcCustFullInfo entity = fcCustFullInfoDao.selectSingleByProperty("taskCustId", taskCustId);
		if(entity == null || StringUtil.isEmptyString(entity.getFinaDate())) 
			return "该企业暂无财务分析信息！";
		String finaDate  = entity.getFinaDate();
		String result = "至"+ finaDate.substring(0, 4) +"年"+ finaDate.substring(4, 6) +"月末，";
		double balance = entity.getBalance();
		if(balance >= 10000d){
			result += "该公司各类资产总额" + String.format("%.2f", balance/10000) + "万元，";
		}else{
			result += "该公司各类资产总额" + balance + "元，";
		}
		double debt = entity.getDebt();
		if(debt >= 10000d){
			result += "负债总额" + String.format("%.2f", debt/10000) + "万元，";
		}else{
			result += "负债总额" + debt + "元，";
		}
		double owners = entity.getOwners();
		if(owners >= 10000d){
			result += "所有者权益" + String.format("%.2f", owners/10000) + "万元，";
		}else{
			result += "所有者权益" + owners + "元，";
		}
		double sales = entity.getSales();
		if(sales >= 10000d){
			result += "至报告期销售收入" + String.format("%.2f", sales/10000) + "万元，";
		}else{
			result += "至报告期销售收入" + sales + "元，";
		}
		double profits = entity.getSales();
		if(profits >= 10000d){
			result += "利润总额" + String.format("%.2f", profits/10000) + "万元。";
		}else{
			result += "利润总额" + profits + "元。";
		}
		return result;
	}

	@Override
	public Map<String, Object> getFcReportData(Integer taskCustId) throws Exception {
		Map<String, Object> dataMap = new HashMap<>();
		FcCustFullInfo entity = fcCustFullInfoDao.selectSingleByProperty("taskCustId", taskCustId);
		
		dataMap.put("customerName", entity.getCustomerName());
		String startDate = entity.getStartDate();
		if(!StringUtil.isEmptyString(startDate)){//1993-02-17 00:00:00
			entity.setStartDate(startDate.substring(0, 4)+"年"+startDate.substring(5, 7)+"月"+startDate.substring(8, 10)+"日");
		}
		if(!StringUtil.isEmptyString(entity.getEntType())){
			SysDic entType = 
				ISysDicDAO.queryByEnNameAndOptType(entity.getEntType(), SysConstants.BsDicConstant.STD_SY_GB00015);
			entity.setEntType(entType.getCnName());
		}
		if(!StringUtil.isEmptyString(entity.getFiveClass())){
			SysDic fiveClassType = 
				ISysDicDAO.queryByEnNameAndOptType(entity.getFiveClass(), SysConstants.BsDicConstant.STD_ZB_FIVE_SORT);
			entity.setFiveClass(fiveClassType.getCnName());
		}
		entity.setDepositSum(entity.getDepositSum()/10000d);
		entity.setLoanSum(entity.getLoanSum()/10000d);
		entity.setBalance(entity.getBalance()/10000d);
		entity.setDebt(entity.getDebt()/10000d);
		entity.setOwners(entity.getOwners()/10000d);
		entity.setSales(entity.getSales()/10000d);
		entity.setProfits(entity.getProfits()/10000d);
		String finaDate = entity.getFinaDate();
		if(!StringUtil.isEmptyString(finaDate)){//202002
			entity.setFinaDate(finaDate.substring(0, 4)+"年"+startDate.substring(5, 7)+"月");
		}
		dataMap.put("finaYear", Integer.parseInt(finaDate.substring(0, 4)));
		dataMap.put("baseInfo", entity);
		// 股权结构
		String equityInfo = "";
		FcCust fcCust = fcCustDao.findByPrimaryKey(taskCustId);
		if(!Objects.isNull(fcCust)){
			List<CompBaseEquity> equityList =compBaseEquityDAO.queryByCustNo(fcCust.getCustomerNo());
			for(CompBaseEquity equity : equityList){
				equityInfo += "股东为" + equity.getStockName() + "出资"+ equity.getFundAmount() + "万元，占比"+ equity.getFundRate() +"；";
			}
		}
		dataMap.put("equity", equityInfo);
		// 贷款明细
		List<FcBusiContract> loanInfoList = fcBusiContractDao.getByTaskCustId(taskCustId);
		// 提取出担保方式并去重  distinct()
		String guaWay = "";
		if(!CollectionUtils.isEmpty(loanInfoList)){
			List<String> vouchTypeList = 
					loanInfoList.stream().map(FcBusiContract::getVouchType).distinct().collect(Collectors.toList());
			for(String vouchType : vouchTypeList){
				guaWay += vouchType + "、";
			}
			guaWay = guaWay.substring(0, guaWay.length()-1);
		}
		dataMap.put("guaWay", guaWay);
		dataMap.put("loanInfo", loanInfoList);
		// 财务比率分析表
		List<FcFinaData> finaRateList = fcFinaDataDao.getFinaRate(taskCustId);
		dataMap.put("finaRate", finaRateList);
		// 现金流量分析表 
		List<FcFinaData> cashFlowLsit = fcFinaDataDao.getCashFlow(taskCustId);
		dataMap.put("cashFlow", cashFlowLsit);
		
		return dataMap;
	}
}
