package com.beawan.loanAfter.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.loanAfter.dao.LfCreditSelfBankDao;
import com.beawan.loanAfter.entity.LfCreditSelfBank;
import com.beawan.loanAfter.service.LfCreditSelfBankService;

/**
 * @author yzj
 */
@Service("lfCreditSelfBankService")
public class LfCreditSelfBankServiceImpl extends BaseServiceImpl<LfCreditSelfBank> implements LfCreditSelfBankService{
    /**
    * 注入DAO
    */
    @Resource(name = "lfCreditSelfBankDao")
    public void setDao(BaseDao<LfCreditSelfBank> dao) {
        super.setDao(dao);
    }
    @Resource
    public LfCreditSelfBankDao lfCreditSelfBankDao;
}
