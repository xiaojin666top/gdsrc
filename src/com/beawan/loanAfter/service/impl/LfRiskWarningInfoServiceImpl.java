package com.beawan.loanAfter.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.Resource;

import com.beawan.loanAfter.dao.LfRiskWarningInfoDao;
import com.beawan.loanAfter.entity.LfRiskWarningInfo;
import com.beawan.loanAfter.service.LfRiskWarningInfoService;

/**
 * @author yzj
 */
@Service("lfRiskWarningInfoService")
public class LfRiskWarningInfoServiceImpl extends BaseServiceImpl<LfRiskWarningInfo> implements LfRiskWarningInfoService{
    /**
    * 注入DAO
    */
    @Resource(name = "lfRiskWarningInfoDao")
    public void setDao(BaseDao<LfRiskWarningInfo> dao) {
        super.setDao(dao);
    }
    @Resource
    public LfRiskWarningInfoDao lfRiskWarningInfoDao;
	@Override
	public void batchSaveUpdata(List<LfRiskWarningInfo> saveList) throws Exception {
		lfRiskWarningInfoDao.batchSaveUpdata(saveList);
	}
}
