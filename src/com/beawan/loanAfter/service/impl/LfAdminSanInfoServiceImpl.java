package com.beawan.loanAfter.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.Resource;

import com.beawan.loanAfter.dao.LfAdminSanInfoDao;
import com.beawan.loanAfter.entity.LfAdminSanInfo;
import com.beawan.loanAfter.service.LfAdminSanInfoService;

/**
 * @author yzj
 */
@Service("lfAdminSanInfoService")
public class LfAdminSanInfoServiceImpl extends BaseServiceImpl<LfAdminSanInfo> implements LfAdminSanInfoService{
    /**
    * 注入DAO
    */
    @Resource(name = "lfAdminSanInfoDao")
    public void setDao(BaseDao<LfAdminSanInfo> dao) {
        super.setDao(dao);
    }
    @Resource
    public LfAdminSanInfoDao lfAdminSanInfoDao;
	@Override
	public void batchSaveUpdata(List<LfAdminSanInfo> saveList) throws Exception {
		lfAdminSanInfoDao.batchSaveUpdata(saveList);
		
	}
}
