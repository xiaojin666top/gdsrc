package com.beawan.loanAfter.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.Resource;

import com.beawan.loanAfter.dao.LfPaymentOfFeesDao;
import com.beawan.loanAfter.entity.LfPaymentOfFees;
import com.beawan.loanAfter.service.LfPaymentOfFeesService;

/**
 * @author yzj
 */
@Service("lfPaymentOfFeesService")
public class LfPaymentOfFeesServiceImpl extends BaseServiceImpl<LfPaymentOfFees> implements LfPaymentOfFeesService{
    /**
    * 注入DAO
    */
    @Resource(name = "lfPaymentOfFeesDao")
    public void setDao(BaseDao<LfPaymentOfFees> dao) {
        super.setDao(dao);
    }
    @Resource
    public LfPaymentOfFeesDao lfPaymentOfFeesDao;
    
	@Override
	public void batchSaveUpdata(List<LfPaymentOfFees> saveList) throws Exception {
		lfPaymentOfFeesDao.batchSaveUpdata(saveList);
	}
}
