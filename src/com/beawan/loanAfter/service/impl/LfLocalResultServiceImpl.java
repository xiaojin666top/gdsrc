package com.beawan.loanAfter.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.beawan.loanAfter.dao.LfLocalResultDao;
import com.beawan.loanAfter.entity.LfLocalResult;
import com.beawan.loanAfter.service.LfLocalResultService;

/**
 * @author yzj
 */
@Service("lfLocalResultService")
public class LfLocalResultServiceImpl extends BaseServiceImpl<LfLocalResult> implements LfLocalResultService{
    /**
    * 注入DAO
    */
    @Resource(name = "lfLocalResultDao")
    public void setDao(BaseDao<LfLocalResult> dao) {
        super.setDao(dao);
    }
    @Resource
    public LfLocalResultDao lfLocalResultDao;
}
