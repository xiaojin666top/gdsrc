package com.beawan.loanAfter.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.loanAfter.dao.FcBusiContractDao;
import com.beawan.loanAfter.entity.FcBusiContract;
import com.beawan.loanAfter.service.FcBusiContractService;
import org.springframework.stereotype.Service;
import java.util.List;

import javax.annotation.Resource;

/**
 * @author yzj
 */
@Service("fcBusiContractService")
public class FcBusiContractServiceImpl extends BaseServiceImpl<FcBusiContract> implements FcBusiContractService{
    /**
    * 注入DAO
    */
    @Resource(name = "fcBusiContractDao")
    public void setDao(BaseDao<FcBusiContract> dao) {
        super.setDao(dao);
    }
    @Resource
    public FcBusiContractDao fcBusiContractDao;
    
	@Override
	public List<FcBusiContract> getByTaskCustId(Integer taskCustId) {
		return fcBusiContractDao.getByTaskCustId(taskCustId);
	}
    
    
}
