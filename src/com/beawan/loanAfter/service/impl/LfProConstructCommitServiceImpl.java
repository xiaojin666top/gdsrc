package com.beawan.loanAfter.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.loanAfter.dao.LfProConstructCommitDao;
import com.beawan.loanAfter.entity.LfProConstructCommit;
import com.beawan.loanAfter.service.LfProConstructCommitService;

/**
 * @author yzj
 */
@Service("lfProConstructCommitService")
public class LfProConstructCommitServiceImpl extends BaseServiceImpl<LfProConstructCommit> implements LfProConstructCommitService{
    /**
    * 注入DAO
    */
    @Resource(name = "lfProConstructCommitDao")
    public void setDao(BaseDao<LfProConstructCommit> dao) {
        super.setDao(dao);
    }
    @Resource
    public LfProConstructCommitDao lfProConstructCommitDao;
}
