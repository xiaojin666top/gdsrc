package com.beawan.loanAfter.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.loanAfter.dao.LfProjectInfoDao;
import com.beawan.loanAfter.entity.LfProjectInfo;
import com.beawan.loanAfter.service.LfProjectInfoService;

/**
 * @author yzj
 */
@Service("lfProjectInfoService")
public class LfProjectInfoServiceImpl extends BaseServiceImpl<LfProjectInfo> implements LfProjectInfoService{
    /**
    * 注入DAO
    */
    @Resource(name = "lfProjectInfoDao")
    public void setDao(BaseDao<LfProjectInfo> dao) {
        super.setDao(dao);
    }
    @Resource
    public LfProjectInfoDao lfProjectInfoDao;
	@Override
	public void batchSaveUpdata(List<LfProjectInfo> saveList) throws Exception {
		lfProjectInfoDao.batchSaveUpdata(saveList);
	}
}
