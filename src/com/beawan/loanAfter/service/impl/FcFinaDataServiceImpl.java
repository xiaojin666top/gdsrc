package com.beawan.loanAfter.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.loanAfter.dao.FcFinaDataDao;
import com.beawan.loanAfter.entity.FcFinaData;
import com.beawan.loanAfter.service.FcFinaDataService;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.Resource;

/**
 * @author yzj
 */
@Service("fcFinaDataService")
public class FcFinaDataServiceImpl extends BaseServiceImpl<FcFinaData> implements FcFinaDataService{
    /**
    * 注入DAO
    */
    @Resource(name = "fcFinaDataDao")
    public void setDao(BaseDao<FcFinaData> dao) {
        super.setDao(dao);
    }
    @Resource
    public FcFinaDataDao fcFinaDataDao;
    
	@Override
	public List<FcFinaData> getFinaRate(Integer taskCustId) {
		return fcFinaDataDao.getFinaRate(taskCustId);
	}
	@Override
	public List<FcFinaData> getCashFlow(Integer taskCustId) {
		return fcFinaDataDao.getCashFlow(taskCustId);
	}
    
}
