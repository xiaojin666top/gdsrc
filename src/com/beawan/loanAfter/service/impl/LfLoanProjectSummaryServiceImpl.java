package com.beawan.loanAfter.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.beawan.loanAfter.dao.LfLoanProjectSummaryDao;
import com.beawan.loanAfter.entity.LfLoanProjectSummary;
import com.beawan.loanAfter.service.LfLoanProjectSummaryService;

/**
 * @author yzj
 */
@Service("lfLoanProjectSummaryService")
public class LfLoanProjectSummaryServiceImpl extends BaseServiceImpl<LfLoanProjectSummary> implements LfLoanProjectSummaryService{
    /**
    * 注入DAO
    */
    @Resource(name = "lfLoanProjectSummaryDao")
    public void setDao(BaseDao<LfLoanProjectSummary> dao) {
        super.setDao(dao);
    }
    @Resource
    public LfLoanProjectSummaryDao lfLoanProjectSummaryDao;
}
