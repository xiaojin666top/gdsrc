package com.beawan.loanAfter.service.impl;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.beawan.common.Constants;
import com.beawan.common.config.AppConfig;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.core.Pagination;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.dao.ICusBaseDAO;
import com.beawan.dtmp.dto.CmisBillDto;
import com.beawan.dtmp.dto.CmisContractDto;
import com.beawan.dtmp.dto.FmContractDto;
import com.beawan.dtmp.dto.GuarBaseDto;
import com.beawan.dtmp.dto.StyBalDto;
import com.beawan.dtmp.service.DtmpService;
import com.beawan.loanAfter.dao.FcBusiContractDao;
import com.beawan.loanAfter.dao.FcCustDao;
import com.beawan.loanAfter.dao.FcCustFullInfoDao;
import com.beawan.loanAfter.dao.FcFinaDataDao;
import com.beawan.loanAfter.dao.FcTaskDao;
import com.beawan.loanAfter.dao.LfCustInfoDao;
import com.beawan.loanAfter.dto.CmisCustBaseDto;
import com.beawan.loanAfter.dto.CmisCustChargeDto;
import com.beawan.loanAfter.dto.CmisCustStockDto;
import com.beawan.loanAfter.dto.CmisLargeAffirmDto;
import com.beawan.loanAfter.dto.CmisSmallAffirmDto;
import com.beawan.loanAfter.dto.CmisSpecialAuthDto;
import com.beawan.loanAfter.dto.CmisWorkPaperDto;
import com.beawan.loanAfter.entity.FcBusiContract;
import com.beawan.loanAfter.entity.FcCust;
import com.beawan.loanAfter.entity.FcCustFullInfo;
import com.beawan.loanAfter.entity.FcFinaData;
import com.beawan.loanAfter.entity.FcTask;
import com.beawan.loanAfter.entity.LfCustInfo;
import com.beawan.loanAfter.service.FcTaskService;
import com.beawan.qcc.service.SynchronizationQCCService;
import com.beawan.survey.custInfo.dto.CompBaseDto;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.platform.util.DateUtil;
import com.platform.util.FreeMarkerWordUtil;
import com.platform.util.MapperUtil;
import com.platform.util.StringUtil;

/**
 * @author yzj
 */
@Service("fcTaskService")
public class FcTaskServiceImpl extends BaseServiceImpl<FcTask> implements FcTaskService{

    private static final Logger log = Logger.getLogger(FcTaskServiceImpl.class);
    /**
     * 注入DAO
     */
     @Resource(name = "fcTaskDao")
     public void setDao(BaseDao<FcTask> dao) {
         super.setDao(dao);
     }
    @Resource
    public FcTaskDao fcTaskDao;
    @Resource
    public FcCustDao fcCustDao;//五级分类任务对应客户名单
    @Resource
    public LfCustInfoDao lfCustInfoDao;//贷后任务客户名单
	@Resource
	private ICusBaseDAO cusBaseDAO;
	@Resource
	private FcBusiContractDao fcBusiContractDao;
    @Resource
    public FcFinaDataDao fcFinaDataDao;
    @Resource
    public FcCustFullInfoDao fcCustFullInfoDao;	
    @Resource
    private ICompBaseSV compBaseSV;//客户详细信息
	@Resource
	private SynchronizationQCCService synchronizationQCCService;
	@Resource
	private DtmpService dtmpService;
    
    
	@Override
	public Pagination<FcTask> getFcTaskList(Integer posi, String taskName, String createrName, int page, int pageSize) throws Exception {
		
		Map<String, Object> params = new HashMap<>();
		StringBuilder sql = new StringBuilder();
		sql.append("select ID,TASK_NAME taskName,CREATER_NAME createrName,TASK_POSITION taskPosition,CREATE_TIME createTime ")
			.append("from GDTCESYS.LFC_TASK where STATUS='0'");
		if(Objects.nonNull(posi)){
			sql.append(" and TASK_POSITION=:posi");
			params.put("posi", posi);
		}
		if(!StringUtils.isEmpty(taskName)){
			sql.append(" and TASK_NAME like :taskName");
			params.put("taskName", "%" + taskName + "%");
		}
		if(!StringUtils.isEmpty(createrName)){
			sql.append(" and CREATER_NAME like :createrName");
			params.put("createrName", "%" + createrName + "%");
		}
		String count = "select count(1) from (" + sql + ") a";
		sql.append(" order by CREATE_TIME desc");
		Pagination<FcTask> pager = fcTaskDao.findCustSqlPagination(FcTask.class, sql, count, params, page, pageSize);
		return pager;
	}


	@Override
	public void batchFcTask(String taskName, String userId, String userName) throws Exception {
		FcTask task = new FcTask();
		task.setTaskName(taskName);
		task.setTaskPosition(Constants.LFC_TASK_POSITION.PENDING_TASK);
		task.setCreateTime(DateUtil.getNowTimestamp());
		task.setCreater(userId);
		task.setCreaterName(userName);
		task.setUpdater(userId);
		task.setUpdateTime(DateUtil.getNowTimestamp());
		task = fcTaskDao.saveOrUpdate(task);
		
		//创建本次任务中包括的客户列表
		List<LfCustInfo> all = lfCustInfoDao.getAll();
//		List<LfCustInfo> all = lfCustInfoDao.selectByProperty("customerNo", "CM20201016850992");
//		StringBuffer sql = new StringBuffer();
//		Map<String, Object> params = new HashMap<>();
//		sql.append("")
//			.append("")
//		 List<LfCustInfo> all =lfCustInfoDao.select(sql.toString(), params);
//		List<String> cus = new ArrayList<>();
//		cus.add("CM20201016490199");
//		cus.add("CM20201016017512");
//		cus.add("CM20201016115736");
//		cus.add("CM20201016900624");
//		cus.add("CM20201016102184");
//		for (String string : cus) {
//			Map<String,  Object> parmas = new HashMap<>();
//			parmas.put("customerNo", string);
//			List<LfCustInfo> al = lfCustInfoDao.selectByProperty(parmas);
//			all.addAll(al);
//		}
		
		if(CollectionUtils.isEmpty(all)){
			log.error("创建五级分类时，贷后客户名单为空！！");
			return;
		}
//		List<FcCust> taskCustList = new ArrayList();
		//创建五级分类任务的客户清单
		int index = 0;
		
		for(LfCustInfo custInfo : all){
			try{
				//判断当前客户是否在我行还有贷款
				Double lastLoanSum = custInfo.getLastLoanSum();
				if(lastLoanSum <= 0d){
					log.info("===========对公客户号：" + custInfo.getCustomerNo() + "======无我行授信金额，五级分类跳过该客户。");
					continue;
				}
				index++;
				log.info("=======第===="+index+"===个五级分类客户，客户号：" + custInfo.getCustomerNo() + "======在我行授信金额为"+lastLoanSum+"，开始创建该客户的五级分类任务。");
				FcCust fcCust = new FcCust(task.getId(),
						custInfo.getCustomerNo(), Constants.LFC_TASK_CUST_STATE.PENDING);
				fcCust = fcCustDao.saveOrUpdate(fcCust);
				
				//对公客户号
				String custNo = custInfo.getCustomerNo();
				//对客户五级分类报告在信息进行赋值
		        CusBase cusBase = cusBaseDAO.selectByPrimaryKey(custNo);
		        //客户名称
		        String customerName = cusBase.getCustomerName();
				//核心客户号
				String mfCustNo = cusBase.getMfCustomerNo();
				//信贷客户号
				String xdCustNo = cusBase.getXdCustomerNo();
		        CompBaseDto compBase = compBaseSV.findCompBaseByCustNo(custNo);
		        if(StringUtil.isEmpty(cusBase.getQccSyncTime()) || compBase == null){
		        	//进行企查查数据同步
		        	log.info("=========开始同步企查查信息==========");
					synchronizationQCCService.syncQccAllData(custNo);
					compBase = compBaseSV.findCompBaseByCustNo(custNo);
					cusBase.setQccSyncTime(DateUtil.getNowY_m_dStr());
					cusBaseDAO.saveOrUpdate(cusBase);
		        }
				FcCustFullInfo full = new FcCustFullInfo();
				full.setTaskCustId(fcCust.getId());
				full.setCustomerName(compBase.getCustomerName());
				full.setStartDate(compBase.getFoundDate());
				full.setAddress(compBase.getRunAddress());
				full.setRegistCapi(compBase.getRegisterCapital()/10000+"万元"+compBase.getRcCurrency());
				full.setRecCap(compBase.getRealCapital()/10000+ "万元"+ compBase.getPcCurrency());
				full.setOperName(compBase.getLegalPerson());
				//格式化
				full.setEntType(compBase.getCompanyNature());
				full.setWorks(compBase.getEmployeeNum());
				full.setInduCode(compBase.getIndustryCode());
				full.setInduName(compBase.getIndustryType());
				full.setScope(compBase.getRunScope());
				
				full.setReportDate(DateUtil.formatDate(new Date(), "yyyy年MM月dd日"));
				List<StyBalDto> depositList = dtmpService.getDepositByCust(mfCustNo);
				if(!CollectionUtils.isEmpty(depositList)){
					String deposit = null;
					StringBuilder deposits = new StringBuilder();
					Double depositSum = 0d;
					for(StyBalDto dto : depositList){
						deposits.append(",").append(dto.getStyName());
						if(dto.getBalVal()!=null)
							depositSum += dto.getBalVal();
					}
					if(deposits.length()>1){
						deposit = deposits.substring(1);
					}
					full.setDeposit(deposit);
					full.setDepositSum(depositSum);
				}
				//获取并保存客户贷款合同信息
				List<FcBusiContract> contractList = dtmpService.getBusiContractByCust(xdCustNo);
				if(!CollectionUtils.isEmpty(contractList)){
					for(FcBusiContract contract : contractList){
						contract.setTaskCustId(fcCust.getId());
					}
					fcBusiContractDao.batchSaveUpdata(contractList);
				}
	//			分组操作放到取数地方
	//			if(!CollectionUtils.isEmpty(contractList)){
	//				Map<String, List<FcBusiContract>> collect = contractList.stream().collect(Collectors.groupingBy(FcBusiContract::getArtificialNo));
	//				for(Map.Entry<String, List<FcBusiContract>> entry : collect.entrySet()){
	//					
	//				}
	//			}
	        	log.info("=========开始同步行内系统的客户财务数据==========");
				//----------------------------开始获取财务数据
				//1.获取当期主要科目
				String currFinaDate = dtmpService.getCustFinaDate(xdCustNo);
				if(!StringUtils.isEmpty(currFinaDate)){
					full.setFinaDate(currFinaDate);
					int currYear = Integer.parseInt(currFinaDate.substring(0, 4));
					String oneYear = (currYear-1) + "12";//上年末
					String twoYear = (currYear-2) + "12";//上上年末
					List<BigDecimal> mainSubList = dtmpService.getCustFinaCurrMainSubject(xdCustNo, currFinaDate);
					if(!CollectionUtils.isEmpty(mainSubList)){
						full.setBalance(mainSubList.get(0).doubleValue());
						full.setDebt(mainSubList.get(1).doubleValue());
						full.setOwners(mainSubList.get(2).doubleValue());
						full.setSales(mainSubList.get(3).doubleValue());
						full.setProfits(mainSubList.get(4).doubleValue());
					}
					//2.获取最近3期比率指标
					List<FcFinaData> ratioFinaData = dtmpService.getRatioFinaData(xdCustNo, currFinaDate, oneYear, twoYear);
					if(!CollectionUtils.isEmpty(ratioFinaData)){
						for(FcFinaData data : ratioFinaData){
							data.setTaskCustId(fcCust.getId());
							fcFinaDataDao.saveOrUpdate(data);
						}
						
					}
					//3.获取最近3期现金流情况
					List<FcFinaData> cashFlowFinaData = dtmpService.getCashFlowFinaData(xdCustNo, currFinaDate, oneYear, twoYear);
					if(!CollectionUtils.isEmpty(cashFlowFinaData)){
						for(FcFinaData data : cashFlowFinaData){
							data.setTaskCustId(fcCust.getId());
							fcFinaDataDao.saveOrUpdate(data);
						}
						//抽出最近一期在现金流指标
						full.setCashflowSum(cashFlowFinaData.get(0).getCurrVal());
						full.setCashflowOperate(cashFlowFinaData.get(1).getCurrVal());
						full.setCashflowInvest(cashFlowFinaData.get(2).getCurrVal());
						full.setCashflowFinanc(cashFlowFinaData.get(3).getCurrVal());
					}
					fcCustFullInfoDao.saveOrUpdate(full);
				}

	        	log.info("=========开始创建五级分类的工作底稿和认定表==========");
				//生成五级分类工作底稿及五级分类认定表
				generateReport(taskName,customerName,xdCustNo);
				log.info("===========对公客户号：" + custInfo.getCustomerNo() + "======五级分类任务创建成功。");
			}catch(Exception e){
				log.error("在任务号为：" + task.getId() + ",客户号为：" + custInfo.getCustomerNo() + "创建五级分类任务时出现异常:" , e);
			}
			if(index ==50){
				break;
			}
		}
		//GGG 上面可能还要同步获取一些数据
		log.info("五级分类任务：" + taskName + "-----创建完成。");
				
	}
	
	@Override
	public String generateReport(String taskName,String customerName,String xdCustNo) throws Exception{
		
		Map<String, Object> dataMap = new HashMap<>();
		CmisWorkPaperDto cmisCustFullDto = getCmisCustInfoFull(xdCustNo);			
		List<String> cmisSpecial = getCmisSpecialAuth(cmisCustFullDto);
		CmisLargeAffirmDto cmisLargeAffirmDto = getCmisLargeAffirm(cmisCustFullDto);
		List<CmisContractDto> contractListCompress = dtmpService.getContractListCompress(cmisLargeAffirmDto.getContractList());
		List<Double> contractListSum = dtmpService.getContractListSum(contractListCompress);
		CmisSmallAffirmDto cmisSmallAffirmDto = getCmisSmallAffirm(cmisCustFullDto);
		
		dataMap.put("cmis", cmisCustFullDto);
		dataMap.put("special", cmisSpecial);
		dataMap.put("cmisLager", cmisLargeAffirmDto);
		dataMap.put("contractList", contractListCompress);
		dataMap.put("contractSum", contractListSum);
		dataMap.put("cmisSmall", cmisSmallAffirmDto);
		
		
		
		String fiveClassPath = AppConfig.Cfg.FIVE_CLASS_PATH;// 五级分类报告地址
		String fiveClassDirPath = fiveClassPath + taskName + "/" + customerName;
		File fiveClassDir = new File(fiveClassDirPath);
		if(!fiveClassDir.exists()){
			fiveClassDir.mkdirs();
		}
		try{
			//生成五级分类工作底稿
			File workingPaperFile = new File(fiveClassDir, customerName + "五级分类工作底稿.xls");
			FreeMarkerWordUtil.createWord(dataMap, "loan_statistics/lf/workingPaper.htm", fiveClassDir.getPath(), workingPaperFile.getName());
		} catch (Exception e) {
			log.error("生成五级分类工作底稿文件异常：", e);
		}
		//贷款金额合计大于五百万则生成大额分类认定表，如果小于五百万则生成小额分类认定表
		if(contractListSum != null){
			if(contractListSum.get(0) >= 5000000){
				
				try{
					//生成五级分类大额企事业单位贷款分类认定表
					File identificationTableFile = new File(fiveClassDir, customerName + "五级分类大额分类认定表.xls");
					FreeMarkerWordUtil.createWord(dataMap, "loan_statistics/lf/LagerFinaTable.htm", fiveClassDir.getPath(), identificationTableFile.getName());
					return customerName + "五级分类大额分类认定表.xls";
				} catch (Exception e) {
					log.error("生成五级分类大额分类认定表文件异常：", e);
					return customerName + "五级分类大额分类认定表.xls";
				}
				
			}else{
				try{
					//生成五级分类小额企事业单位贷款分类认定表
					File identificationTableFile = new File(fiveClassDir, customerName + "五级分类小额分类认定表.xls");
					FreeMarkerWordUtil.createWord(dataMap, "loan_statistics/lf/SmallFinaTable.htm", fiveClassDir.getPath(), identificationTableFile.getName());
					return customerName + "五级分类小额分类认定表.xls";
				} catch (Exception e) {
					log.error("生成五级分类小额分类认定表文件异常：", e);
					return customerName + "五级分类小额分类认定表.xls";
				}
			}
		}else{
			
			try{
				//生成五级分类大额企事业单位贷款分类认定表
				File identificationTableFile = new File(fiveClassDir, customerName + "五级分类认定表.xls");
				FreeMarkerWordUtil.createWord(dataMap, "loan_statistics/lf/SmallFinaTable.htm", fiveClassDir.getPath(), identificationTableFile.getName());
				return customerName + "五级分类认定表.xls";
			} catch (Exception e) {
				log.error("生成五级分类大额分类认定表文件异常：", e);
				return customerName + "五级分类认定表.xls";
			}
		}
	}

	@Override
	public CmisWorkPaperDto getCmisCustInfoFull(String xdCustNo) throws Exception {
		CmisWorkPaperDto full = new CmisWorkPaperDto();
		full.setXdCustNo(xdCustNo);
		full.setConfirmDate(DateUtil.format(new Date(), "yyyy年MM月dd日"));
		
		CmisCustBaseDto baseInfo = fcTaskDao.getCustBaseInfo(xdCustNo);
		
		if(baseInfo!=null){
			if(baseInfo.getCustomerName() != null){
				full.setCustomerName(baseInfo.getCustomerName());
			}
			if(baseInfo.getAddress() != null){
				full.setAddress(baseInfo.getAddress());
			}else if(baseInfo.getRegistAddr() != null){
				full.setAddress(baseInfo.getRegistAddr());
			}
		}
		
		List<CmisSpecialAuthDto> authList = this.formatAuth(fcTaskDao.getSpecialAuth(xdCustNo));
		List<CmisCustChargeDto> chargeList = fcTaskDao.getChargeList(xdCustNo);
		
		//设置基本信息
		full.setCustBaseDto(baseInfo);
		//设置特殊行业许可证
		full.setSpecialAuthList(authList);
		//设置企业法人和财务负责人信息
		if(!CollectionUtils.isEmpty(chargeList)){
			Map<String, List<CmisCustChargeDto>> chargeMap = chargeList.stream().collect(Collectors.groupingBy(CmisCustChargeDto::getRelationShip));
			List<CmisCustChargeDto> lagerList = chargeMap.get(Constants.RELATIONSHIP.LAGER);
			List<CmisCustChargeDto> finList = chargeMap.get(Constants.RELATIONSHIP.FINANCE);
			if(!CollectionUtils.isEmpty(lagerList)){
				full.setLeger(lagerList.get(0).getCustomerName());
				full.setLagerId(lagerList.get(0).getCertId());
			}
			if(!CollectionUtils.isEmpty(finList)){
				full.setFinancer(finList.get(0).getCustomerName());
				full.setFinancerId(finList.get(0).getCertId());
			}
			chargeMap.remove(Constants.RELATIONSHIP.LAGER);
			chargeMap.remove(Constants.RELATIONSHIP.FINANCE);
			
			//设置资本构成
			List<CmisCustChargeDto> compositList = new ArrayList(); 
			for(List<CmisCustChargeDto> list : chargeMap.values()){
				compositList.addAll(list);
			}
			compositList.sort((a,b) -> (int)(a.getInvestmentProp() - b.getInvestmentProp()));
			full.setChargeList(compositList);
		}
		//设置近三期财报
		String currFinaDate = dtmpService.getCustFinaDate(xdCustNo);
		int currYear = Integer.parseInt(currFinaDate.substring(0, 4));
		String oneYear = (currYear-1) + "12";//上年末
		String twoYear = (currYear-2) + "12";//上上年末
		//设置报告中显示财报时间
		full.setCurrDate(currFinaDate);
		full.setOneYearDate(oneYear);
		full.setTwoYearDate(twoYear);
		List<FcFinaData> manuscriptFinaData = dtmpService.getManuscriptFinaData(xdCustNo, currFinaDate, oneYear, twoYear);
		full.setManuscriptFinaData(manuscriptFinaData);
		
		//设置担保人财报
		GuarBaseDto guarInfo = dtmpService.getGuarInfo(xdCustNo);
		if(guarInfo!=null){
			String guarCustNo = guarInfo.getGuarCustNo();
			//设置近三期财报
			String guarCurrFinaDate = dtmpService.getCustFinaDate(guarCustNo);
			if (!StringUtil.isEmpty(guarCurrFinaDate)) {
				int guarCurrYear = Integer.parseInt(guarCurrFinaDate.substring(0, 4));
				String guarOneYear = (guarCurrYear-1) + "12";//上年末
				String guarTwoYear = (guarCurrYear-2) + "12";//上上年末
				List<FcFinaData> guarFinaData = dtmpService.getCmGuarFinaData(xdCustNo, guarCurrFinaDate, guarOneYear, guarTwoYear);
				full.setGuarCurrFinaDate(guarCurrFinaDate);
				full.setGuarFinaData(guarFinaData);
				full.setGuarOneYear(guarOneYear);
				full.setGuarTwoYear(guarTwoYear);
			}
		}
		
		FmContractDto contractInfo = dtmpService.getContractInfo(xdCustNo);
		//设置授信申请、担保信息
		if(contractInfo!=null){
			full.setFmContractDto(contractInfo);
			
			//业务合同流水号--》获取借据表信息  即获取用信信息
			String serialNo = contractInfo.getSerialNo();
			List<CmisBillDto> billInfo = dtmpService.getBillInfo(serialNo);
			full.setBillInfo(billInfo);
		}
		
		return full;
	}
	
	/**
	 * 
	 * 特殊行业认证    ---》数据格式统一，最后只需要取   certName  certOrg 就行
	 * 根据认证类型取 证书名称和认证单位信息：
	 * 当认证类型是 01时： 取 certName  certOrg
	 * 当认证类型是 010时： 取 certName  无认证单位
	 * 当认证类型是 02时： 取 certName  nameRending
	 * 当认证类型是 020时： 取 tradeName  无认证单位
	 * @param authList
	 * @return
	 */
	private List<CmisSpecialAuthDto> formatAuth(List<CmisSpecialAuthDto> authList){
		if(CollectionUtils.isEmpty(authList)){
			return authList;
		}
		for(CmisSpecialAuthDto auth : authList){
			if("02".equals(auth.getCertType())){
				auth.setCertOrg(auth.getNameRending());
			}else if("020".equals(auth.getCertType())){
				auth.setCertName(auth.getTradeName());
			}
		}
		return authList;
	}

	/**
	 * 修整五级分类工作底稿中的特殊行业经营许可证信息
	 * @param cmisCustFullDto
	 * @return
	 */
	@Override
	public List<String> getCmisSpecialAuth(CmisWorkPaperDto cmisCustFullDto) throws Exception {
		List<String> result = new ArrayList<String>();
		List<CmisSpecialAuthDto> specialAuthList = cmisCustFullDto.getSpecialAuthList();
		
		String certName ="";
		String certId ="";
		String validDate = "";
		String certOrg = "";
		if(!CollectionUtils.isEmpty(specialAuthList)){
			for(CmisSpecialAuthDto specialAuth : specialAuthList){
                if (!StringUtil.isEmpty(specialAuth.getCertName())) {
                	certName = certName + specialAuth.getCertName() + ",";
				}
				
                if (!StringUtil.isEmpty(specialAuth.getCertId())) {
                	certId  = certId + specialAuth.getCertId() + ",";
				}
				
                if (!StringUtil.isEmpty(specialAuth.getValidDate())) {
                	validDate = validDate + specialAuth.getValidDate() + ",";
				}
				
				if (!StringUtil.isEmpty(specialAuth.getCertOrg())) {
					certOrg = certOrg + specialAuth.getCertOrg() + ",";
				}
				
			}
			if (!StringUtil.isEmpty(certName)) {
				certName = certName.substring(0, certName.length()-1);
			}
			
            if (!StringUtil.isEmpty(certId)) {
            	certId = certId.substring(0, certId.length()-1);
			}
			
            if (!StringUtil.isEmpty(validDate)) {
            	validDate = validDate.substring(0, validDate.length()-1);
			}
			
			if (!StringUtil.isEmpty(certOrg)) {
				certOrg = certOrg.substring(0, certOrg.length()-1);
			}
				
		}
		result.add(certName);
		result.add(certId);
		result.add(validDate);
		result.add(certOrg);
		
		return result;
	}
	



	@Override
	public CmisLargeAffirmDto getCmisLargeAffirm(CmisWorkPaperDto full) throws Exception {
		CmisLargeAffirmDto lgAffirm = MapperUtil.trans(full, CmisLargeAffirmDto.class);
		String xdCustNo = full.getXdCustNo();
		
		//设置合同信息
		List<CmisContractDto> contractList = dtmpService.getContractList(xdCustNo);
		if(!CollectionUtils.isEmpty(contractList)){
			for(CmisContractDto contract : contractList){
				if(contract.getVouchtype() != null){
	            	if(contract.getVouchtype().contains("A")){
	            		contract.setVouchtype("质押");
	            	}else if(contract.getVouchtype().contains("B")){
	            		contract.setVouchtype("抵押");
	            	}else if(contract.getVouchtype().contains("C")){
	            		contract.setVouchtype("保证");
	            	}else if(contract.getVouchtype().contains("D")){
	            		contract.setVouchtype("信用/免担保");
	            	}else if(contract.getVouchtype().contains("E")){
	            		contract.setVouchtype("组合担保");
	            	}else if(contract.getVouchtype().contains("F")){
	            		contract.setVouchtype("纸质押");
	            	}else if(contract.getVouchtype().contains("G")){
	            		contract.setVouchtype("票据贴现");
	            	}else if(contract.getVouchtype().contains("H")){
	            		contract.setVouchtype("保证金");
	            	}else if(contract.getVouchtype().contains("Z")){
	            		contract.setVouchtype("其他");
	            	}else{
	            		contract.setVouchtype("未说明");
	            	}
            	}
			}
		}
			
		lgAffirm.setContractList(contractList);
		

		//设置固定资产
		CmisCustStockDto custStock = dtmpService.getCustStock(xdCustNo);
		lgAffirm.setStockDto(custStock);

		return lgAffirm;
	}
	
	


	@Override
	public CmisSmallAffirmDto getCmisSmallAffirm(CmisWorkPaperDto full) throws Exception {
		if(full==null){
			return null;
		}
		
		String xdCustNo = full.getXdCustNo();
		List<FcFinaData> manuscriptFinaData = full.getManuscriptFinaData();
		
		String currDate = full.getCurrDate();
		String oneYearDate = full.getOneYearDate();
		String twoYearDate = full.getTwoYearDate();
		
		CmisSmallAffirmDto smAffirm = new CmisSmallAffirmDto();
		
		smAffirm.setCustomername(full.getCustomerName());
		smAffirm.setOrgnature(full.getCustBaseDto().getOrgnature());
		smAffirm.setMostbusiness(full.getCustBaseDto().getMostbusiness());
		
		List<FcFinaData> smallFinaData = dtmpService.getSmallFinaData(xdCustNo, currDate, oneYearDate, twoYearDate);
		smAffirm.setSmallFinaData(smallFinaData);
		smAffirm.setCurrDate(currDate);
		smAffirm.setOneYearDate(oneYearDate);
		smAffirm.setTwoYearDate(twoYearDate);
		//获取全部小额认定表中所需要的数据
		CmisContractDto lastOneContractInfo = dtmpService.getLastOneContractInfo(xdCustNo);
		if(lastOneContractInfo != null){
			smAffirm.setContractSum(lastOneContractInfo.getContractSum());
			smAffirm.setOccurdate(lastOneContractInfo.getOccurdate());
			smAffirm.setMaturity(lastOneContractInfo.getMaturity());
			smAffirm.setVouchtype(lastOneContractInfo.getVouchtype());
		}
		return smAffirm;
	}

}
