package com.beawan.loanAfter.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.core.Pagination;
import com.beawan.loanAfter.dao.LfCustInfoDao;
import com.beawan.loanAfter.dto.LfClassifyVo;
import com.beawan.loanAfter.dto.LfCustDto;
import com.beawan.loanAfter.entity.LfCustInfo;
import com.beawan.loanAfter.service.LfCustInfoService;
import com.platform.util.StringUtil;

/**
 * @author yzj
 */
@Service("lfCustInfoService")
public class LfCustInfoServiceImpl extends BaseServiceImpl<LfCustInfo> implements LfCustInfoService{
    /**
    * 注入DAO
    */
    @Resource(name = "lfCustInfoDao")
    public void setDao(BaseDao<LfCustInfo> dao) {
        super.setDao(dao);
    }
    @Resource
    public LfCustInfoDao lfCustInfoDao;
    
	@Override
	public Pagination<LfCustDto> getLfCustPage(int page, int pageSize, LfClassifyVo condition) throws Exception {
		StringBuffer sql = new StringBuffer();
        Map<String,Object> params = new HashMap<>();
        sql.append(" select a.CUSTOMER_NO as customerNo,b.CUSTOMER_NAME as customerName,a.LAST_LOAN_SUM as loanBalance,")
        .append(" a.RISK_CLASSI as riskClassi,a.CREDIT_CLASSI as creditClassi, a.UPDATE_TIME as updateTime, ")
        .append(" b.MANAGER_USER_ID as managerUserId,c.USER_NAME as userName,d.ORGANSHORTFORM orgName ")
        .append(" from LF_CUST_INFO a ")
        .append(" join CUS_BASE b on b.CUSTOMER_NO=a.CUSTOMER_NO ")
        .append(" join BS_USER_INFO c on b.MANAGER_USER_ID=c.USER_ID ")
        .append(" join BS_ORG d on d.ORGANNO=b.MANAGER_ORG_ID ")
        .append(" where a.status='"+Constants.NORMAL+"' ");
        params.put("status",Constants.NORMAL);
        if(Objects.nonNull(condition)){
        	if(!StringUtil.isEmptyString(condition.getManagerName())){
            	sql.append(" and c.USER_NAME like '%"+condition.getManagerName()+"%' ");
            	params.put("managerName","%"+condition.getManagerName()+"%");
            }
            if(!StringUtil.isEmptyString(condition.getCustomerName())){
            	sql.append(" and b.CUSTOMER_NAME '%"+condition.getCustomerName()+"%' ");
            	params.put("customerName","%"+condition.getCustomerName()+"%");
            }
            if(!StringUtil.isEmptyString(condition.getRiskClassi())){
            	sql.append(" and a.RISK_CLASSI = '"+condition.getRiskClassi()+"' ");
            	params.put("riskClassi", condition.getRiskClassi());
            }
            if(!StringUtil.isEmptyString(condition.getCreditClassi())){
            	sql.append(" and a.CREDIT_CLASSI = '"+condition.getCreditClassi()+"' ");
            	params.put("creditClassi", condition.getCreditClassi());
            }
        }
        String count = " select count(*) from ("+sql.toString()+") tb2";
        sql.append(" order by a.UPDATE_TIME desc ");
        params.clear();
        return lfCustInfoDao.findCustSqlPagination(LfCustDto.class, sql.toString(), count, params, page, pageSize);
	}

	@Override
	public List<LfCustDto> getLfCustList() throws Exception {
		StringBuffer sql = new StringBuffer();
        Map<String,Object> params = new HashMap<>();
        sql.append(" select a.CUSTOMER_NO as customerNo,b.CUSTOMER_NAME as customerName,")
        .append(" a.RISK_CLASSI as riskClassi,a.CREDIT_CLASSI as creditClassi, a.UPDATE_TIME as updateTime, ")
        .append(" b.MANAGER_USER_ID as managerUserId,c.USER_NAME as userName ")
        .append(" from LF_CUST_INFO a ")
        .append(" join CUS_BASE b on b.CUSTOMER_NO=a.CUSTOMER_NO ")
        .append(" join BS_USER_INFO c on b.MANAGER_USER_ID=c.USER_ID ")
        .append(" where a.status='"+Constants.NORMAL+"' ");
        params.put("status",Constants.NORMAL);
        params.clear();
        List<LfCustDto> result = lfCustInfoDao.findCustListBySql(LfCustDto.class, sql.toString(), params);
		return result;
	}

}
