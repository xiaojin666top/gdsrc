package com.beawan.loanAfter.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.core.Pagination;
import com.beawan.loanAfter.dao.LfRiskManagerDao;
import com.beawan.loanAfter.entity.LfRiskManager;
import com.beawan.loanAfter.service.LfRiskManagerService;

/**
 * @author yzj
 */
@Service("lfRiskManagerService")
public class LfRiskManagerServiceImpl extends BaseServiceImpl<LfRiskManager> implements LfRiskManagerService{
    /**
    * 注入DAO
    */
    @Resource(name = "lfRiskManagerDao")
    public void setDao(BaseDao<LfRiskManager> dao) {
        super.setDao(dao);
    }
    @Resource
    public LfRiskManagerDao lfRiskManagerDao;
    
	@Override
	public Pagination<LfRiskManager> getRiskManagerPager(String userNo, String userName, String orgName, int page, int rows) {
		
		Map<String, Object> params = new HashMap<>();
		StringBuilder sql = new StringBuilder("select * from GDTCESYS.LF_RISK_MANAGER where status=:status");
		params.put("status", Constants.NORMAL);
		if(!StringUtils.isEmpty(userNo)){
			sql.append(" and RISKUSER = :userNo ");
			params.put("userNo", userNo);
		}
		if(!StringUtils.isEmpty(userName)){
			sql.append(" and RISKUSERNAME like :userName ");
			params.put("userName", "%"+userName+"%");
		}
		if(!StringUtils.isEmpty(orgName)){
			sql.append(" and ORGANNAME like :orgName ");
			params.put("orgName", "%"+orgName+"%");
		}
		sql.append(" order by RISKUSER");
		Pagination<LfRiskManager> pager = lfRiskManagerDao.findCustSqlPagination(LfRiskManager.class, sql, params, page, rows);
		return pager;
	}
}
