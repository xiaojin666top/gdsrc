package com.beawan.loanAfter.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.loanAfter.dao.LfFifteenDayCheckDao;
import com.beawan.loanAfter.entity.LfFifteenDayCheck;
import com.beawan.loanAfter.service.LfFifteenDayCheckService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
 * @author yzj
 */
@Service("lfFifteenDayCheckService")
public class LfFifteenDayCheckServiceImpl extends BaseServiceImpl<LfFifteenDayCheck> implements LfFifteenDayCheckService{
    /**
    * 注入DAO
    */
    @Resource(name = "lfFifteenDayCheckDao")
    public void setDao(BaseDao<LfFifteenDayCheck> dao) {
        super.setDao(dao);
    }
    @Resource
    public LfFifteenDayCheckDao lfFifteenDayCheckDao;

    @Override
    public void batchSaveUpdata(List<LfFifteenDayCheck> saveList) throws Exception {
        lfFifteenDayCheckDao.batchSaveUpdata(saveList);
    }
}
