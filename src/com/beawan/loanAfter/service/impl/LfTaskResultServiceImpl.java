package com.beawan.loanAfter.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.loanAfter.dao.LfTaskResultDao;
import com.beawan.loanAfter.service.LfTaskResultService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.loanAfter.entity.LfTaskResult;

/**
 * @author yzj
 */
@Service("lfTaskResultService")
public class LfTaskResultServiceImpl extends BaseServiceImpl<LfTaskResult> implements LfTaskResultService{
    /**
    * 注入DAO
    */
    @Resource(name = "lfTaskResultDao")
    public void setDao(BaseDao<LfTaskResult> dao) {
        super.setDao(dao);
    }
    @Resource
    public LfTaskResultDao lfTaskResultDao;
}
