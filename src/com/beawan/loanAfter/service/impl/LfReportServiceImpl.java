package com.beawan.loanAfter.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.beawan.base.entity.Organization;
import com.beawan.base.entity.User;
import com.beawan.base.service.IOrganizationSV;
import com.beawan.base.service.IUserSV;
import com.beawan.common.Constants;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.loanAfter.dao.LfAdminSanInfoDao;
import com.beawan.loanAfter.dao.LfCfdAccountInfoDao;
import com.beawan.loanAfter.dao.LfChangeCheckDao;
import com.beawan.loanAfter.dao.LfCreditSelfBankDao;
import com.beawan.loanAfter.dao.LfDownCheckInfoDao;
import com.beawan.loanAfter.dao.LfFifteenDayCheckDao;
import com.beawan.loanAfter.dao.LfFinCheckDao;
import com.beawan.loanAfter.dao.LfInternalControlInfoDao;
import com.beawan.loanAfter.dao.LfLoanProjectDetailDao;
import com.beawan.loanAfter.dao.LfLoanProjectSummaryDao;
import com.beawan.loanAfter.dao.LfLocalResultDao;
import com.beawan.loanAfter.dao.LfPaymentOfFeesDao;
import com.beawan.loanAfter.dao.LfProConstructCommitDao;
import com.beawan.loanAfter.dao.LfProjectInfoDao;
import com.beawan.loanAfter.dao.LfRiskWarningInfoDao;
import com.beawan.loanAfter.dao.LfSituationCheckDao;
import com.beawan.loanAfter.dao.LfTaskDao;
import com.beawan.loanAfter.dao.LfTaskResultDao;
import com.beawan.loanAfter.entity.LfAdminSanInfo;
import com.beawan.loanAfter.entity.LfCfdAccountInfo;
import com.beawan.loanAfter.entity.LfChangeCheck;
import com.beawan.loanAfter.entity.LfCreditSelfBank;
import com.beawan.loanAfter.entity.LfDownCheckInfo;
import com.beawan.loanAfter.entity.LfFifteenDayCheck;
import com.beawan.loanAfter.entity.LfFinCheck;
import com.beawan.loanAfter.entity.LfInternalControlInfo;
import com.beawan.loanAfter.entity.LfLoanProjectDetail;
import com.beawan.loanAfter.entity.LfLoanProjectSummary;
import com.beawan.loanAfter.entity.LfLocalResult;
import com.beawan.loanAfter.entity.LfPaymentOfFees;
import com.beawan.loanAfter.entity.LfProConstructCommit;
import com.beawan.loanAfter.entity.LfProjectInfo;
import com.beawan.loanAfter.entity.LfRiskWarningInfo;
import com.beawan.loanAfter.entity.LfSituationCheck;
import com.beawan.loanAfter.entity.LfTask;
import com.beawan.loanAfter.entity.LfTaskResult;
import com.beawan.loanAfter.service.LfReportService;
import com.beawan.survey.custInfo.dto.CompBaseDto;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.platform.util.ExceptionUtil;
import com.platform.util.StringUtil;

@Service("lfReportService")
public class LfReportServiceImpl implements LfReportService {
	// 风险预警信息
	@Resource
	private LfRiskWarningInfoDao lfRiskWarningInfoDao;
	@Resource
	private LfLocalResultDao lfLocalResultDao;
	@Resource
	private LfAdminSanInfoDao lfAdminSanInfoDao;
	@Resource
	private LfCfdAccountInfoDao lfCfdAccountInfoDao;
	@Resource
	private LfInternalControlInfoDao lfInternalControlInfoDao;
	@Resource
	private LfChangeCheckDao lfChangeCheckDao;
	@Resource
	private LfTaskResultDao lfTaskResultDao;
	@Resource
	private LfFinCheckDao lfFinCheckDao;
	@Resource
	public LfSituationCheckDao lfSituationCheckDao;
	@Resource
	private LfTaskDao lfTaskDao;
	@Resource
	private LfProConstructCommitDao lfProConstructCommitDao;
	@Resource
	private LfPaymentOfFeesDao lfPaymentOfFeesDao;
	@Resource
	private LfProjectInfoDao lfProjectInfoDao;
	@Resource
	private LfLoanProjectSummaryDao lfLoanProjectSummaryDao;
	@Resource
	private LfLoanProjectDetailDao lfLoanProjectDetailDao;
	@Resource
	private LfDownCheckInfoDao lfDownCheckInfoDao;
	@Resource
	private LfCreditSelfBankDao lfCreditSelfBankDao;
	@Resource
	private LfFifteenDayCheckDao lfFifteenDayCheckDao;
	@Resource
	private ICusBaseSV cusBaseSV;
	@Resource
	private IUserSV userSV;
	@Resource
	private IOrganizationSV organizationSV;
	
	
	

	@Override
	public Map<String, Object> getReportInfo(Integer taskId) throws Exception {
		Map<String, Object> result = new HashMap<>();
		// 贷后任务信息
		LfTask lfTask = lfTaskDao.findByPrimaryKey(taskId);
		
		CusBase cusBase = cusBaseSV.queryByCusNo(lfTask.getCustomerNo());
		Organization organization = organizationSV.queryByNo(cusBase.getManagerOrgId());
		if (lfTask == null || lfTask.getLoanBalance() == null) {
			ExceptionUtil.throwException("贷后任务数据异常！");
		}
		User checkUser = userSV.queryById(lfTask.getUserNo());
		result.put("checkUserName", checkUser.getUserName());
		result.put("organizationName", organization.getOrganname());
		result.put("lfTask", lfTask);
		// 根据（任务类型/行业/金额）区分模版
		switch (lfTask.getTaskType()) {
		case Constants.LF_TASK_TYPE.GH_FIFTEEN_DAY_TASK:// 管户15日首检
		case Constants.LF_TASK_TYPE.FX_FIFTEEN_DAY_TASK:// 风险15日首检
			result.put("reportPath", "loan_statistics/lf/fifteen_day_task_report.htm");
			result.put("lfTaskResult", getLfTaskResult(taskId));
			result.put("fifteenDayCheck", getFifteenDayCheck(taskId));
			break;
		case Constants.LF_TASK_TYPE.GH_DAY_TASK:// 管户日常任务
		case Constants.LF_TASK_TYPE.FX_THREE_MONTH_TASK:// 风险3个月到期检查
		case Constants.LF_TASK_TYPE.FX_YJ_TASK:// 风险预警检查
		case Constants.LF_TASK_TYPE.CREATE_MANUALLY_TASK:// 创建特殊贷后任务
			if (lfTask.getLoanBalance() <= 10000000d) {
				result.put("reportPath", "loan_statistics/lf/lf_down_report.htm");
				result.putAll(getDownMapInfo(taskId));
			} else {
				if(StringUtil.isEmptyString(lfTask.getIndustryType())){
					result.put("reportPath", "loan_statistics/lf/lf_nomal_report.htm");
					result.putAll(getNomalMapInfo(taskId));
				}else if (lfTask.getIndustryType().contains("C")) {
					// 制造业
					result.put("reportPath", "loan_statistics/lf/lf_C_report.htm");
					result.putAll(getCMapInfo(taskId));
				} else if (lfTask.getIndustryType().contains("E")) {
					// 建筑业
					result.put("reportPath", "loan_statistics/lf/lf_E_report.htm");
					result.putAll(getEMapInfo(taskId));
				} else if (lfTask.getIndustryType().contains("F")) {
					// 批发零售
					result.put("reportPath", "loan_statistics/lf/lf_F_report.htm");
					result.putAll(getFMapInfo(taskId));
				} else if (lfTask.getIndustryType().contains("K")) {
					// 房地产业
					result.put("reportPath", "loan_statistics/lf/lf_K_report.htm");
					result.putAll(getKMapInfo(taskId));
				} else {
					// 报告模版
					result.put("reportPath", "loan_statistics/lf/lf_nomal_report.htm");
					result.putAll(getNomalMapInfo(taskId));
				}
			}
			break;
		}
		return result;
	}
	
	private Map<String, Object> getDownMapInfo(Integer taskId) throws Exception {
		Map<String, Object> result = new HashMap<>();
			
		result.put("creditSelfBank", getCreditSelfBank(taskId));
		
		// 获取到客户风险预警信息
		result.put("riskInfo", getRiskInfo("FXYJ",taskId));
		//获取oap信息
		result.put("oapInfo", getRiskInfo("OAP",taskId));
		
		//1000w以下特有结论
		result.put("downCheckInfo", getDownCheckInfo(taskId));
		// 现场调查结论性的话在这里
		result.put("lfTaskResult", getLfTaskResult(taskId));
		//生产经营情况
		result.put("jySituation", getSituationCheck("RG_LF_JY_08", taskId));
		//建筑类
		result.put("jzSituation", getSituationCheck("RG_LF_JZ_01", taskId));
		//财务状况
		result.put("finSituation", getSituationCheck("RG_LF_FIN_01", taskId));
		// 担保情况
		result.put("dbSituation", getSituationCheckGroup("RG_LF_DB", taskId));
		return result;
	}

	private Map<String, Object> getNomalMapInfo(Integer taskId) throws Exception {
		Map<String, Object> result = new HashMap<>();
		// 获取到客户风险预警信息
		result.put("riskInfo", getRiskInfo("FXYJ",taskId));
		// 司法涉诉信息和一些结论性信息
		result.put("localResult", getLocalResult(taskId));
		// 获取行政处罚信息
		result.put("adminSanInfo", getAdminSanInfo(taskId));
		// 账户查冻扣信息列表
		result.put("cfdAccountInfo", getCfdAccountInfo(taskId));
		// 内控名单列表
		result.put("internalControlInfo", getInternalControlInfo(taskId));

		// 现场调查结论性的话在这里
		result.put("lfTaskResult", getLfTaskResult(taskId));
		// 客户基本信息变化情况
		result.put("custChangeCheck", getCustChangeCheck(taskId));
		// 融资信息变化情况
		result.put("financeChangeCheck", getFinanceChangeCheck(taskId));
		// 对外担保信息变化情况
		result.put("guatChangeCheck", getGuatChangeCheck(taskId));
		// 财务状况检查
		result.put("finCheckInfo", getFinCheckInfo(taskId));
		// 经营情况
		result.put("jySituation", getSituationCheckGroup("RG_LF_JY", taskId));
		// 担保情况
		result.put("dbSituation", getSituationCheckGroup("RG_LF_DB", taskId));
		return result;
	}

	private Map<String, Object> getCMapInfo(Integer taskId) throws Exception {
		Map<String, Object> result = new HashMap<>();
		// 获取到客户风险预警信息
		result.put("riskInfo", getRiskInfo("FXYJ",taskId));
		// 司法涉诉信息和一些结论性信息
		result.put("localResult", getLocalResult(taskId));
		// 获取行政处罚信息
		result.put("adminSanInfo", getAdminSanInfo(taskId));
		// 账户查冻扣信息列表
		result.put("cfdAccountInfo", getCfdAccountInfo(taskId));
		// 内控名单列表
		result.put("internalControlInfo", getInternalControlInfo(taskId));

		// 现场调查结论性的话在这里
		result.put("lfTaskResult", getLfTaskResult(taskId));
		// 客户基本信息变化情况
		result.put("custChangeCheck", getCustChangeCheck(taskId));
		// 融资信息变化情况
		result.put("financeChangeCheck", getFinanceChangeCheck(taskId));
		// 对外担保信息变化情况
		result.put("guatChangeCheck", getGuatChangeCheck(taskId));
		// 缴纳税费情况
		result.put("paymentOfFees", getPaymentOfFees(taskId));
		// 财务状况检查
		result.put("finCheckInfo", getFinCheckInfo(taskId));
		// 经营情况
		result.put("jySituation", getSituationCheckGroup("RG_LF_JY", taskId));
		// 其他现场调查情况
		result.put("qtSituation", getSituationCheck("RG_LF_QT", taskId));
		// 担保情况
		result.put("dbSituation", getSituationCheckGroup("RG_LF_DB", taskId));
		return result;
	}

	private Map<String, Object> getEMapInfo(Integer taskId) throws Exception {
		Map<String, Object> result = new HashMap<>();
		// 获取到客户风险预警信息
		result.put("riskInfo", getRiskInfo("FXYJ",taskId));
		// 司法涉诉信息和一些结论性信息
		result.put("localResult", getLocalResult(taskId));
		// 获取行政处罚信息
		result.put("adminSanInfo", getAdminSanInfo(taskId));
		// 账户查冻扣信息列表
		result.put("cfdAccountInfo", getCfdAccountInfo(taskId));
		// 内控名单列表
		result.put("internalControlInfo", getInternalControlInfo(taskId));

		// 现场调查结论性的话在这里
		result.put("lfTaskResult", getLfTaskResult(taskId));
		// 客户基本信息变化情况
		result.put("custChangeCheck", getCustChangeCheck(taskId));
		// 融资信息变化情况
		result.put("financeChangeCheck", getFinanceChangeCheck(taskId));
		// 对外担保信息变化情况
		result.put("guatChangeCheck", getGuatChangeCheck(taskId));
		// 财务状况检查
		result.put("finCheckInfo", getFinCheckInfo(taskId));
		// 经营情况检查---项目存在的主要风险
		result.put("xmSituation", getSituationCheck("RG_LF_XM", taskId));
		// 在建或已完工未完全回款的主要项目情况
		result.put("projectInfo", getProjectInfo(taskId));
		result.put("loanProjectDetail", getLoanProjectDetail(taskId));
		result.put("loanProjectSummary", getLoanProjectSummary(taskId));
		// 其他现场调查情况
		result.put("qtSituation", getSituationCheck("RG_LF_QT", taskId));
		// 担保情况
		result.put("dbSituation", getSituationCheckGroup("RG_LF_DB", taskId));
		return result;
	}

	private Map<String, Object> getFMapInfo(Integer taskId) throws Exception {
		Map<String, Object> result = new HashMap<>();
		// 获取到客户风险预警信息
		result.put("riskInfo", getRiskInfo("FXYJ",taskId));
		// 司法涉诉信息和一些结论性信息
		result.put("localResult", getLocalResult(taskId));
		// 获取行政处罚信息
		result.put("adminSanInfo", getAdminSanInfo(taskId));
		// 账户查冻扣信息列表
		result.put("cfdAccountInfo", getCfdAccountInfo(taskId));
		// 内控名单列表
		result.put("internalControlInfo", getInternalControlInfo(taskId));

		// 现场调查结论性的话在这里
		result.put("lfTaskResult", getLfTaskResult(taskId));
		// 客户基本信息变化情况
		result.put("custChangeCheck", getCustChangeCheck(taskId));
		// 融资信息变化情况
		result.put("financeChangeCheck", getFinanceChangeCheck(taskId));
		// 对外担保信息变化情况
		result.put("guatChangeCheck", getGuatChangeCheck(taskId));
		// 财务状况检查
		result.put("finCheckInfo", getFinCheckInfo(taskId));
		// 经营情况
		result.put("jySituation", getSituationCheckGroup("RG_LF_JY", taskId));
		// 其他现场调查情况
		result.put("qtSituation", getSituationCheck("RG_LF_QT", taskId));
		// 担保情况
		result.put("dbSituation", getSituationCheckGroup("RG_LF_DB", taskId));
		return result;
	}

	private Map<String, Object> getKMapInfo(Integer taskId) throws Exception {
		Map<String, Object> result = new HashMap<>();
		// 获取到客户风险预警信息
		result.put("riskInfo", getRiskInfo("FXYJ",taskId));
		// 司法涉诉信息和一些结论性信息
		result.put("localResult", getLocalResult(taskId));
		// 获取行政处罚信息
		result.put("adminSanInfo", getAdminSanInfo(taskId));
		// 账户查冻扣信息列表
		result.put("cfdAccountInfo", getCfdAccountInfo(taskId));
		// 内控名单列表
		result.put("internalControlInfo", getInternalControlInfo(taskId));

		// 现场调查结论性的话在这里
		result.put("lfTaskResult", getLfTaskResult(taskId));
		// 客户基本信息变化情况
		result.put("custChangeCheck", getCustChangeCheck(taskId));
		// 融资信息变化情况
		result.put("financeChangeCheck", getFinanceChangeCheck(taskId));
		// 对外担保信息变化情况
		result.put("guatChangeCheck", getGuatChangeCheck(taskId));
		// 项目建设、投产情况检查
		result.put("proConstructCommit", getProConstructCommit(taskId));
		// 其他现场调查情况
		result.put("qtSituation", getSituationCheck("RG_LF_QT", taskId));
		// 担保情况
		result.put("dbSituation", getSituationCheckGroup("RG_LF_DB", taskId));
		return result;
	}

	/**
	 * 
	 * @Description 获取预警信息
	 * @param taskId
	 * @return
	 * @throws Exception
	 * @author xyh
	 */
	private List<LfRiskWarningInfo> getRiskInfo(String type,Integer taskId) throws Exception {
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		params.put("riskFrom", type);// 查询风险预警系统中的数据
		List<LfRiskWarningInfo> lfRiskWarningInfoList = lfRiskWarningInfoDao.selectByProperty(params);
		return lfRiskWarningInfoList;
	}

	/**
	 * 
	 * @Description 返回非现场调查的结论性信息
	 * @param taskId
	 * @return
	 * @throws Exception
	 * @author xyh
	 */
	private LfLocalResult getLocalResult(Integer taskId) throws Exception {
		LfLocalResult lfLocalResult = lfLocalResultDao.selectSingleByProperty("taskId", taskId);
		return lfLocalResult;
	}

	/**
	 * 
	 * @Description 获取行政处罚信息
	 * @param taskId
	 * @return
	 * @throws Exception
	 * @author xyh
	 */
	private List<LfAdminSanInfo> getAdminSanInfo(Integer taskId) throws Exception {
		List<LfAdminSanInfo> lfAdminSanInfoList = lfAdminSanInfoDao.selectByProperty("taskId", taskId);
		return lfAdminSanInfoList;
	}

	/**
	 * 
	 * @Description 获取查冻扣账户信息
	 * @param taskId
	 * @return
	 * @throws Exception
	 * @author xyh
	 */
	private List<LfCfdAccountInfo> getCfdAccountInfo(Integer taskId) throws Exception {
		List<LfCfdAccountInfo> lfCfdAccountInfoList = lfCfdAccountInfoDao.selectByProperty("taskId", taskId);
		return lfCfdAccountInfoList;
	}

	/**
	 * 
	 * @Description 获取内控名单信息
	 * @param taskId
	 * @return
	 * @throws Exception
	 * @author xyh
	 */
	private List<LfInternalControlInfo> getInternalControlInfo(Integer taskId) throws Exception {
		List<LfInternalControlInfo> lfInternalControlInfoList = lfInternalControlInfoDao.selectByProperty("taskId",
				taskId);
		return lfInternalControlInfoList;
	}

	/**
	 * 
	 * @Description 获取客户信息变动情况
	 * @param taskId
	 * @return
	 * @throws Exception
	 * @author xyh
	 */
	private List<LfChangeCheck> getCustChangeCheck(Integer taskId) throws Exception {
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		params.put("checkType", Constants.LF_SYS_DIC.LF_CHANGE_CUST);
		List<LfChangeCheck> lfChangeCheckList = lfChangeCheckDao.selectByProperty(params);
		return lfChangeCheckList;
	}

	/**
	 * 
	 * @Description 获取贷后任务结果信息
	 * @param taskId
	 * @return
	 * @throws Exception
	 * @author xyh
	 */
	private LfTaskResult getLfTaskResult(Integer taskId) throws Exception {
		LfTaskResult lfTaskResult = lfTaskResultDao.selectSingleByProperty("taskId", taskId);
		return lfTaskResult;
	}

	/**
	 * 
	 * @Description 获取财务变动信息
	 * @param taskId
	 * @return
	 * @throws Exception
	 * @author xyh
	 */
	private List<LfChangeCheck> getFinanceChangeCheck(Integer taskId) throws Exception {
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		params.put("checkType", Constants.LF_SYS_DIC.LF_CHANGE_FINANCE);
		List<LfChangeCheck> lfChangeCheckList = lfChangeCheckDao.selectByProperty(params);
		return lfChangeCheckList;
	}

	/**
	 * 
	 * @Description 获取对外担保变动信息
	 * @param taskId
	 * @return
	 * @throws Exception
	 * @author xyh
	 */
	private List<LfChangeCheck> getGuatChangeCheck(Integer taskId) throws Exception {
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		params.put("checkType", Constants.LF_SYS_DIC.LF_CHANGE_GUAT);
		List<LfChangeCheck> lfChangeCheckList = lfChangeCheckDao.selectByProperty(params);
		return lfChangeCheckList;
	}

	/**
	 * 
	 * @Description 获取财务状况检查信息
	 * @param taskId
	 * @return
	 * @throws Exception
	 * @author xyh
	 */
	private List<LfFinCheck> getFinCheckInfo(Integer taskId) throws Exception {
		List<LfFinCheck> lfFinCheckList = lfFinCheckDao.selectByProperty("taskId", taskId);
		return lfFinCheckList;
	}

	/**
	 * 
	 * @Description
	 * @param type
	 * @param taskId
	 * @return
	 * @throws Exception
	 * @author xyh
	 */
	private Map<String, List<LfSituationCheck>> getSituationCheckGroup(String type, Integer taskId) throws Exception {
		List<LfSituationCheck> lfSituationCheckList = lfSituationCheckDao.queryByTypeFirstLike(type, taskId);
		Map<String, List<LfSituationCheck>> collect = lfSituationCheckList.stream()
				.collect(Collectors.groupingBy(LfSituationCheck::getTypeFirstName));
		return collect;
	}

	/**
	 * 
	 * @Description 获取检查情况 在分组前的信息
	 * @param type
	 * @param taskId
	 * @return
	 * @throws Exception
	 * @author xyh
	 */
	private List<LfSituationCheck> getSituationCheck(String type, Integer taskId) throws Exception {
		List<LfSituationCheck> lfSituationCheckList = lfSituationCheckDao.queryByTypeFirstLike(type, taskId);
		return lfSituationCheckList;
	}

	/**
	 * 
	 * @Description 项目建设和投产信息
	 * @param taskId
	 * @return
	 * @throws Exception
	 * @author xyh
	 */
	private LfProConstructCommit getProConstructCommit(Integer taskId) throws Exception {
		LfProConstructCommit lfProConstructCommit = lfProConstructCommitDao.selectSingleByProperty("taskId", taskId);
		return lfProConstructCommit;
	}

	/**
	 * 
	 * @Description 缴纳税费情况
	 * @param taskId
	 * @return
	 * @throws Exception
	 * @author xyh
	 */
	private List<LfPaymentOfFees> getPaymentOfFees(Integer taskId) throws Exception {
		List<LfPaymentOfFees> lfPaymentOfFeeList = lfPaymentOfFeesDao.selectByProperty("taskId", taskId);
		return lfPaymentOfFeeList;
	}

	/**
	 * 
	 * @Description 获取在建或已完工未完全回款的主要项目情况
	 * @param taskId
	 * @return
	 * @throws Exception
	 * @author xyh
	 */
	private List<LfProjectInfo> getProjectInfo(Integer taskId) throws Exception {
		List<LfProjectInfo> lfProjectInfoList = lfProjectInfoDao.selectByProperty("taskId", taskId);
		return lfProjectInfoList;
	}

	/**
	 * 
	 * @Description 获取申贷项目详细信息
	 * @param taskId
	 * @return
	 * @throws Exception
	 * @author xyh
	 */
	private List<LfLoanProjectDetail> getLoanProjectDetail(Integer taskId) throws Exception {
		List<LfLoanProjectDetail> lfLoanProjectDetailList = lfLoanProjectDetailDao.selectByProperty("taskId", taskId);
		return lfLoanProjectDetailList;
	}

	/**
	 * 
	 * @Description 获取申贷项目结论信息
	 * @param taskId
	 * @return
	 * @throws Exception
	 * @author xyh
	 */
	private LfLoanProjectSummary getLoanProjectSummary(Integer taskId) throws Exception {
		LfLoanProjectSummary lfLoanProjectSummary = lfLoanProjectSummaryDao.selectSingleByProperty("taskId", taskId);
		return lfLoanProjectSummary;
	}
	/**
	 * 
	 *@Description 1000w以下的结论
	 *@param taskId
	 *@return
	 *@throws Exception
	 * @author xyh
	 */
	private LfDownCheckInfo getDownCheckInfo(Integer taskId) throws Exception {
		LfDownCheckInfo lfDownCheckInfo = lfDownCheckInfoDao.selectSingleByProperty("taskId", taskId);
		return lfDownCheckInfo;
	}
	 
	private List<LfCreditSelfBank> getCreditSelfBank(Integer taskId) throws Exception {
		List<LfCreditSelfBank> lfCreditSelfBankList = lfCreditSelfBankDao.selectByProperty("taskId", taskId);
		if(!CollectionUtils.isEmpty(lfCreditSelfBankList)){
			for(LfCreditSelfBank lfcsb : lfCreditSelfBankList){
				if(lfcsb.getGuateeWay() != null){
		        	if(lfcsb.getGuateeWay().contains("A")){
		        		lfcsb.setGuateeWay("质押");
		        	}else if(lfcsb.getGuateeWay().contains("B")){
		        		lfcsb.setGuateeWay("抵押");
		        	}else if(lfcsb.getGuateeWay().contains("C")){
		        		lfcsb.setGuateeWay("保证");
		        	}else if(lfcsb.getGuateeWay().contains("D")){
		        		lfcsb.setGuateeWay("信用/免担保");
		        	}else if(lfcsb.getGuateeWay().contains("E")){
		        		lfcsb.setGuateeWay("组合担保");
		        	}else if(lfcsb.getGuateeWay().contains("F")){
		        		lfcsb.setGuateeWay("纸质押");
		        	}else if(lfcsb.getGuateeWay().contains("G")){
		        		lfcsb.setGuateeWay("票据贴现");
		        	}else if(lfcsb.getGuateeWay().contains("H")){
		        		lfcsb.setGuateeWay("保证金");
		        	}else if(lfcsb.getGuateeWay().contains("Z")){
		        		lfcsb.setGuateeWay("其他");
		        	}else{
		        		lfcsb.setGuateeWay("未说明");
		        	}
	        	}
	        }
		}
		return lfCreditSelfBankList;
	}
	
	
	private List<LfFifteenDayCheck> getFifteenDayCheck(Integer taskId) throws Exception {
		List<LfFifteenDayCheck> lfFifteenDayCheckList = lfFifteenDayCheckDao.selectByProperty("taskId",taskId);
		return lfFifteenDayCheckList;
	}
}
