package com.beawan.loanAfter.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.loanAfter.dao.LfDownCheckInfoDao;
import com.beawan.loanAfter.entity.LfDownCheckInfo;
import com.beawan.loanAfter.service.LfDownCheckInfoService;

/**
 * @author yzj
 */
@Service("lfDownCheckInfoService")
public class LfDownCheckInfoServiceImpl extends BaseServiceImpl<LfDownCheckInfo> implements LfDownCheckInfoService{
    /**
    * 注入DAO
    */
    @Resource(name = "lfDownCheckInfoDao")
    public void setDao(BaseDao<LfDownCheckInfo> dao) {
        super.setDao(dao);
    }
    @Resource
    public LfDownCheckInfoDao lfDownCheckInfoDao;
}
