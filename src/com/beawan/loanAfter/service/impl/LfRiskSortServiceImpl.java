package com.beawan.loanAfter.service.impl;

import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.core.Pagination;
import com.beawan.loanAfter.dao.LfRiskSortDao;
import com.beawan.loanAfter.entity.LfRiskSort;
import com.beawan.loanAfter.service.LfRiskSortService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import com.beawan.loanAfter.entity.LfRiskSort;

/**
 * @author yzj
 */
@Service("lfRiskSortService")
public class LfRiskSortServiceImpl extends BaseServiceImpl<LfRiskSort> implements LfRiskSortService{
    /**
    * 注入DAO
    */
    @Resource(name = "lfRiskSortDao")
    public void setDao(BaseDao<LfRiskSort> dao) {
        super.setDao(dao);
    }
    @Resource
    public LfRiskSortDao lfRiskSortDao;

    /**
     *获取相应分类中可用的风险要点（状态正常）	
     *
     */
    @Override
    public Pagination<LfRiskSort> queryPageList(String riskNo,int page, int rows) throws Exception {
        Map<String, Object> params = new HashMap<>();
        StringBuilder sql = new StringBuilder("select id,RISK_NO as riskNo,RISK_CONTENT as riskContent,RISK_POINT as riskPoint");
        sql.append(" from LF_RISK_SORT ");
        sql.append(" where 1=1 and status=:status and RISK_NO=:riskNo ");
        sql.append(" order by RISK_CONTENT desc, id desc ");
        params.put("riskNo", riskNo);
        params.put("status", Constants.NORMAL);
        String count = "select count(1) from (" + sql + ")";
        Pagination<LfRiskSort> pager = lfRiskSortDao.findCustSqlPagination(LfRiskSort.class,sql.toString(),count,params,page,rows);
        return pager;
    }
}
