package com.beawan.loanAfter.dto;

import java.sql.Timestamp;

import javax.persistence.Column;

/**
 * @Author: xyh
 * @Date: 24/08/2020
 * @Description:
 */
public class LfTaskDto {
    private Integer id;
    private String taskName;//任务名称
    private String customerNo;//客户编号--对公系统的客户编号
    private String xdCustNo;//信贷系统  客户编号
    private String customerName;//客户名称
    private String taskType;//任务类型
    private Integer taskPosition;//任务所处阶段
    private String userNo;//客户经理号---以信贷系统的账号为用户编号
    private String organname;//机构名称
    private String userName;//客户经理名称
    private Double loanBalance;//贷款余额
    private String industryType;//行业
    private Timestamp createTime;//创建时间
    private String martuity;	//到期时间

    private String riskUser;//对于专职贷后风险经理，需要指定风险经理
    private String riskUserName;//风险经理名称

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getXdCustNo() {
		return xdCustNo;
	}

	public void setXdCustNo(String xdCustNo) {
		this.xdCustNo = xdCustNo;
	}

	public String getTaskType() {
        return taskType;
    }

    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    public Integer getTaskPosition() {
        return taskPosition;
    }

    public void setTaskPosition(Integer taskPosition) {
        this.taskPosition = taskPosition;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOrganname() {
		return organname;
	}

	public void setOrganname(String organname) {
		this.organname = organname;
	}

	public Double getLoanBalance() {
		return loanBalance;
	}

	public void setLoanBalance(Double loanBalance) {
		this.loanBalance = loanBalance;
	}

	public String getIndustryType() {
		return industryType;
	}

	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}

	public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

	public String getMartuity() {
		return martuity;
	}

	public void setMartuity(String martuity) {
		this.martuity = martuity;
	}

	public String getRiskUser() {
		return riskUser;
	}

	public void setRiskUser(String riskUser) {
		this.riskUser = riskUser;
	}

	public String getRiskUserName() {
		return riskUserName;
	}

	public void setRiskUserName(String riskUserName) {
		this.riskUserName = riskUserName;
	}
}
