package com.beawan.loanAfter.dto;

import java.util.List;

import com.beawan.dtmp.dto.CmisBillDto;
import com.beawan.dtmp.dto.FmContractDto;
import com.beawan.loanAfter.entity.FcFinaData;

/**
 * 五级分类工作底稿  所需要的全部数据
 * @author yuzhejia
 *
 */
public class CmisWorkPaperDto {

    private String xdCustNo;//借款人信贷号  -->以下数据都是通过信贷号获取
	
	private String classifyOrg;//贷款分类单位
	private String classifyOperater;//分类操作员
    private String confirmDate;//分类时间
    private String customerName;//借款人名称
    private String address;//通讯地址
    
    //五级分类工作底稿、及认定表中  所需要的基础数据
    private CmisCustBaseDto custBaseDto;
    
    //特殊行业许可证
    private List<CmisSpecialAuthDto> specialAuthList;
    
    //资本构成
    private List<CmisCustChargeDto> chargeList;
    
    //贷款客户主体财务指标
    private List<FcFinaData> manuscriptFinaData;
    
    private String currDate;//财报当期
    private String oneYearDate;//去年
    private String twoYearDate;//前年
    

    //担保人财务指标
    private List<FcFinaData> guarFinaData;
    
    private String guarCurrFinaDate;//担保人财报当期
    private String guarOneYear;//担保人去年
    private String guarTwoYear;//担保人前年
    

    private String leger;//法定代表人
    private String lagerId;//法定代表人身份证号码
    private String financer;//财务负责人
    private String financerId;//财务负责人身份证号码
    
    private String flowWater;//贷款卡的流水账单-->暂时不取
    
    private FmContractDto fmContractDto;//授信业务资料-->申请、担保信息
    
    List<CmisBillDto> billInfo;//借据信息  最近两笔使用的额度和时间

	public String getXdCustNo() {
		return xdCustNo;
	}

	public void setXdCustNo(String xdCustNo) {
		this.xdCustNo = xdCustNo;
	}

	public String getClassifyOrg() {
		return classifyOrg;
	}

	public void setClassifyOrg(String classifyOrg) {
		this.classifyOrg = classifyOrg;
	}

	public String getClassifyOperater() {
		return classifyOperater;
	}

	public void setClassifyOperater(String classifyOperater) {
		this.classifyOperater = classifyOperater;
	}

	public String getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(String confirmDate) {

		this.confirmDate = confirmDate;
		
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public CmisCustBaseDto getCustBaseDto() {
		return custBaseDto;
	}

	public void setCustBaseDto(CmisCustBaseDto custBaseDto) {
		this.custBaseDto = custBaseDto;
	}

	public List<CmisSpecialAuthDto> getSpecialAuthList() {
		return specialAuthList;
	}

	public void setSpecialAuthList(List<CmisSpecialAuthDto> specialAuthList) {
		this.specialAuthList = specialAuthList;
	}

	public List<CmisCustChargeDto> getChargeList() {
		return chargeList;
	}

	public void setChargeList(List<CmisCustChargeDto> chargeList) {
		this.chargeList = chargeList;
	}

	public String getLeger() {
		return leger;
	}

	public void setLeger(String leger) {
		this.leger = leger;
	}

	public String getLagerId() {
		return lagerId;
	}

	public void setLagerId(String lagerId) {
		this.lagerId = lagerId;
	}

	public String getFinancer() {
		return financer;
	}

	public void setFinancer(String financer) {
		this.financer = financer;
	}

	public String getFinancerId() {
		return financerId;
	}

	public void setFinancerId(String financerId) {
		this.financerId = financerId;
	}

	public String getFlowWater() {
		return flowWater;
	}

	public void setFlowWater(String flowWater) {
		this.flowWater = flowWater;
	}

	public List<FcFinaData> getManuscriptFinaData() {
		return manuscriptFinaData;
	}

	public void setManuscriptFinaData(List<FcFinaData> manuscriptFinaData) {
		this.manuscriptFinaData = manuscriptFinaData;
	}

	public String getCurrDate() {
		return currDate;
	}

	public void setCurrDate(String currDate) {
		
		this.currDate = currDate;
		
	}

	public String getOneYearDate() {
		return oneYearDate;
	}

	public void setOneYearDate(String oneYearDate) {
		this.oneYearDate = oneYearDate;
	}

	public String getTwoYearDate() {
		return twoYearDate;
	}

	public void setTwoYearDate(String twoYearDate) {
		this.twoYearDate = twoYearDate;
		
	}

	public List<FcFinaData> getGuarFinaData() {
		return guarFinaData;
	}

	public void setGuarFinaData(List<FcFinaData> guarFinaData) {
		this.guarFinaData = guarFinaData;
	}

	public String getGuarCurrFinaDate() {
		return guarCurrFinaDate;
	}

	public void setGuarCurrFinaDate(String guarCurrFinaDate) {
		
		this.guarCurrFinaDate = guarCurrFinaDate;
	}

	public String getGuarOneYear() {
		return guarOneYear;
	}

	public void setGuarOneYear(String guarOneYear) {
		
		this.guarOneYear = guarOneYear;
	}

	public String getGuarTwoYear() {
		return guarTwoYear;
	}

	public void setGuarTwoYear(String guarTwoYear) {
		this.guarTwoYear = guarTwoYear;		
	}

	public FmContractDto getFmContractDto() {
		return fmContractDto;
	}

	public void setFmContractDto(FmContractDto fmContractDto) {
		this.fmContractDto = fmContractDto;
	}

	public List<CmisBillDto> getBillInfo() {
		return billInfo;
	}

	public void setBillInfo(List<CmisBillDto> billInfo) {
		this.billInfo = billInfo;
	}

    
}
