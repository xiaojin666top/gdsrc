package com.beawan.loanAfter.dto;

/**
 * 五级分类工作底稿、及认定表中  所需要的基础数据
 * @author yuzhejia
 *
 */
public class CmisCustBaseDto {

    private String customerName;//借款人名称
    private String customerId;//借款人编号
    private String address;//通讯地址
    
    //--------------------营业执照信息
    private String busiLic;//营业执照
    private Double registCapi;//注册资本
    private String customerType;//企业类型
    private String registAddr;//注册地
    private String bsValidTerm;//有效期
    private String mot;//年检情况

    //--------------------税务登记证
    private String taxNo;//税务登记证号码
    private String operateWay;//经营方式
    private String taxValidTerm;//税务登记证有效期

    //--------------------组织机构号
    private String corpId;//组织机构号
    private String orgnature;//组织机构类型   ---》国有企业  集体企业....
    private String orgValidTerm;//有效期
    
    private String loancardNo;//贷款卡编号
    private String workNum;//企业员工数
    
    private String setupDate;//公司成立时间
    private String mostbusiness;//主营业务
    private String basicbank;//基本开户行
    private String officeadd;//办公地址
    private String fictitiousperson;//法人代表

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBusiLic() {
		return busiLic;
	}

	public void setBusiLic(String busiLic) {
		this.busiLic = busiLic;
	}

	public Double getRegistCapi() {
		return registCapi;
	}

	public void setRegistCapi(Double registCapi) {
		this.registCapi = registCapi;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getRegistAddr() {
		return registAddr;
	}

	public void setRegistAddr(String registAddr) {
		this.registAddr = registAddr;
	}

	public String getBsValidTerm() {
		return bsValidTerm;
	}

	public void setBsValidTerm(String bsValidTerm) {
		this.bsValidTerm = bsValidTerm;
	}

	public String getMot() {
		return mot;
	}

	public void setMot(String mot) {
		this.mot = mot;
	}

	public String getTaxNo() {
		return taxNo;
	}

	public void setTaxNo(String taxNo) {
		this.taxNo = taxNo;
	}

	public String getOperateWay() {
		return operateWay;
	}

	public void setOperateWay(String operateWay) {
		this.operateWay = operateWay;
	}

	public String getTaxValidTerm() {
		return taxValidTerm;
	}

	public void setTaxValidTerm(String taxValidTerm) {
		this.taxValidTerm = taxValidTerm;
	}

	public String getWorkNum() {
		return workNum;
	}

	public void setWorkNum(String workNum) {
		this.workNum = workNum;
	}

	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	public String getOrgnature() {
		return orgnature;
	}

	public void setOrgnature(String orgnature) {
		this.orgnature = orgnature;
	}

	public String getOrgValidTerm() {
		return orgValidTerm;
	}

	public void setOrgValidTerm(String orgValidTerm) {
		this.orgValidTerm = orgValidTerm;
	}

	public String getLoancardNo() {
		return loancardNo;
	}

	public void setLoancardNo(String loancardNo) {
		this.loancardNo = loancardNo;
	}

	public String getBasicbank() {
		return basicbank;
	}

	public void setBasicbank(String basicbank) {
		this.basicbank = basicbank;
	}

	public String getOfficeadd() {
		return officeadd;
	}

	public void setOfficeadd(String officeadd) {
		this.officeadd = officeadd;
	}

	public String getFictitiousperson() {
		return fictitiousperson;
	}

	public void setFictitiousperson(String fictitiousperson) {
		this.fictitiousperson = fictitiousperson;
	}
    
    
    
	public String getSetupDate() {
		return setupDate;
	}

	public void setSetupDate(String setupDate) {
		this.setupDate = setupDate;
	}

	public String getMostbusiness() {
		return mostbusiness;
	}

	public void setMostbusiness(String mostbusiness) {
		this.mostbusiness = mostbusiness;
	}

	private String StringToDate(String str){
		String result = "";
		String year = str.substring(0, 4) + "/";    	
    	if(str.length() > 6){
    		String month = str.substring(4, 6) + "/";
    		String day = str.substring(6, 8);
    		 result = year + month + day;
    	}else{
    		 String month = str.substring(4, 6);
    		 result = year + month;
    	}
    	return result;
    	
    }
    	
    
}
