package com.beawan.loanAfter.dto;

/**
 * 特殊行业认证    
 * 根据认证类型取 证书名称和认证单位信息：
 * 当认证类型是 01时： 取 certName  certOrg
 * 当认证类型是 010时： 取 certName  无认证单位
 * 当认证类型是 02时： 取 certName  nameRending
 * 当认证类型是 020时： 取 tradeName  无认证单位
 * 
 * @author yuzhejia
 *
 */
public class CmisSpecialAuthDto {

	private String customerId;//客户信贷号
    private String certType;//证件类型 
    private String authDate;//认证时间
    private String certName;//证书名称
    private String certId;//证书号
    private String certResult;//认证结果
    private String certOrg;//认证单位
    private String validDate;//有效日期
    private String tradeName;//所在行业
    private String nameRending;//认定单位名称
    
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCertType() {
		return certType;
	}
	public void setCertType(String certType) {
		this.certType = certType;
	}
	public String getAuthDate() {
		return authDate;
	}
	public void setAuthDate(String authDate) {
		if(authDate.length()==6||authDate.length()==8){
			this.authDate = StringToDate(authDate);
		}else{
			this.authDate = authDate;
		}
	}
	public String getCertName() {
		return certName;
	}
	public void setCertName(String certName) {
		this.certName = certName;
	}
	public String getCertId() {
		return certId;
	}
	public void setCertId(String certId) {
		this.certId = certId;
	}
	public String getCertResult() {
		return certResult;
	}
	public void setCertResult(String certResult) {
		this.certResult = certResult;
	}
	public String getCertOrg() {
		return certOrg;
	}
	public void setCertOrg(String certOrg) {
		this.certOrg = certOrg;
	}
	public String getValidDate() {
		return validDate;
	}
	public void setValidDate(String validDate) {
		
		if(validDate.length()==6||validDate.length()==8){
			this.validDate = StringToDate(validDate);
		}else{
			this.validDate = validDate;
		}
		
	}
	public String getTradeName() {
		return tradeName;
	}
	public void setTradeName(String tradeName) {
		this.tradeName = tradeName;
	}
	public String getNameRending() {
		return nameRending;
	}
	public void setNameRending(String nameRending) {
		this.nameRending = nameRending;
	}
    
    
	private String StringToDate(String str){
		
		String year = str.substring(0, 4) + "年";
    	String month = str.substring(4, 6) + "月";
    	if(str.length() > 6){
    		String day = str.substring(6, 8) + "日";
    		String result = year + month + day;
    	}
    	String result = year + month;
    	return result;
    	
    }
    
}
