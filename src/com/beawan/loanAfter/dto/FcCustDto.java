package com.beawan.loanAfter.dto;

public class FcCustDto {

	private String customerName; //客户名称
    private String startDate;	 //成立时间
    private String operName;	 //法人
    private String entType;	 	 //企业类型
    private String registCapi;	 //注册资本
    private String recCap;		 //实收资本
    private String areaCovered;  //占地面积
    private Integer works;		 //工人人数
    private String address;		 //经营地址
    private String scope;		 //经营范围
    private String induCode;	 //行业代码
    private String induName;	 //行业名称
    
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getOperName() {
		return operName;
	}
	public void setOperName(String operName) {
		this.operName = operName;
	}
	public String getEntType() {
		return entType;
	}
	public void setEntType(String entType) {
		this.entType = entType;
	}
	public String getRegistCapi() {
		return registCapi;
	}
	public void setRegistCapi(String registCapi) {
		this.registCapi = registCapi;
	}
	public String getRecCap() {
		return recCap;
	}
	public void setRecCap(String recCap) {
		this.recCap = recCap;
	}
	public String getAreaCovered() {
		return areaCovered;
	}
	public void setAreaCovered(String areaCovered) {
		this.areaCovered = areaCovered;
	}
	public Integer getWorks() {
		return works;
	}
	public void setWorks(Integer works) {
		this.works = works;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public String getInduCode() {
		return induCode;
	}
	public void setInduCode(String induCode) {
		this.induCode = induCode;
	}
	public String getInduName() {
		return induName;
	}
	public void setInduName(String induName) {
		this.induName = induName;
	}
}
