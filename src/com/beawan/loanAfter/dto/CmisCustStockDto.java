package com.beawan.loanAfter.dto;


/**
 * 客户 固定资产情况
 * @author yuzhejia
 *
 */
public class CmisCustStockDto {

	private String xdCustNo;//信贷客户号
	//土地使用权   1
	private Integer landActualNum;//实际数量
	private Integer landCertifNum;//已办证数量
	private Double landAssessVal;//评估价值
	private String landMortgageStatus;//抵押状态--》是和否   --》默认有抵押无的东西，直接写是 
	//房产   2
	private Integer houseActualNum;//实际数量
	private Integer houseCertifNum;//已办证数量
	private Double houseAssessVal;//评估价值
	private String houseMortgageStatus;//抵押状态--》是和否   --》默认有抵押无的东西，直接写是 
	//设备及交通工具  3
	private Integer equipActualNum;//实际数量
	private Integer equipCertifNum;//已办证数量
	private Double equipAssessVal;//评估价值
	private String equipMortgageStatus;//抵押状态--》是和否   --》默认有抵押无的东西，直接写是 
	public String getXdCustNo() {
		return xdCustNo;
	}
	public void setXdCustNo(String xdCustNo) {
		this.xdCustNo = xdCustNo;
	}
	public Integer getLandActualNum() {
		return landActualNum;
	}
	public void setLandActualNum(Integer landActualNum) {
		this.landActualNum = landActualNum;
	}
	public Integer getLandCertifNum() {
		return landCertifNum;
	}
	public void setLandCertifNum(Integer landCertifNum) {
		this.landCertifNum = landCertifNum;
	}
	public Double getLandAssessVal() {
		return landAssessVal;
	}
	public void setLandAssessVal(Double landAssessVal) {
		this.landAssessVal = landAssessVal;
	}
	public String getLandMortgageStatus() {
		return landMortgageStatus;
	}
	public void setLandMortgageStatus(String landMortgageStatus) {
		this.landMortgageStatus = landMortgageStatus;
	}
	public Integer getHouseActualNum() {
		return houseActualNum;
	}
	public void setHouseActualNum(Integer houseActualNum) {
		this.houseActualNum = houseActualNum;
	}
	public Integer getHouseCertifNum() {
		return houseCertifNum;
	}
	public void setHouseCertifNum(Integer houseCertifNum) {
		this.houseCertifNum = houseCertifNum;
	}
	public Double getHouseAssessVal() {
		return houseAssessVal;
	}
	public void setHouseAssessVal(Double houseAssessVal) {
		this.houseAssessVal = houseAssessVal;
	}
	public String getHouseMortgageStatus() {
		return houseMortgageStatus;
	}
	public void setHouseMortgageStatus(String houseMortgageStatus) {
		this.houseMortgageStatus = houseMortgageStatus;
	}
	public Integer getEquipActualNum() {
		return equipActualNum;
	}
	public void setEquipActualNum(Integer equipActualNum) {
		this.equipActualNum = equipActualNum;
	}
	public Integer getEquipCertifNum() {
		return equipCertifNum;
	}
	public void setEquipCertifNum(Integer equipCertifNum) {
		this.equipCertifNum = equipCertifNum;
	}
	public Double getEquipAssessVal() {
		return equipAssessVal;
	}
	public void setEquipAssessVal(Double equipAssessVal) {
		this.equipAssessVal = equipAssessVal;
	}
	public String getEquipMortgageStatus() {
		return equipMortgageStatus;
	}
	public void setEquipMortgageStatus(String equipMortgageStatus) {
		this.equipMortgageStatus = equipMortgageStatus;
	}
	
	
}
