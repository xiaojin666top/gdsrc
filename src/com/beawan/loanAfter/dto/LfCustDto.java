package com.beawan.loanAfter.dto;

import java.sql.Timestamp;

import javax.persistence.Column;

/**
 * 
 * @Author: xyh
 * @Date: 2020年9月25日
 * @Description: 贷后客户信息，用于显示客户分类界面，生成贷后客户分类台帐
 */
public class LfCustDto {
	private String customerNo;
	private String customerName;
	private String riskClassi;//风险分类
	private String creditClassi;//授信分类
	private Timestamp updateTime;
	private String managerUserId;//管户客户经理编号
	private String userName; //管户客户经理名字
	private String orgName; //管户经理机构简称
	private Double loanBalance;//贷款余额
	
	private Integer id;
    private Integer state;//当前客户五级分类报告是否完成   1未完成   2完成
	private Integer taskId;//五级分类任务号
    
    
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getTaskId() {
		return taskId;
	}
	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getRiskClassi() {
		return riskClassi;
	}
	public void setRiskClassi(String riskClassi) {
		this.riskClassi = riskClassi;
	}
	public String getCreditClassi() {
		return creditClassi;
	}
	public void setCreditClassi(String creditClassi) {
		this.creditClassi = creditClassi;
	}
	public Timestamp getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}
	public String getManagerUserId() {
		return managerUserId;
	}
	public void setManagerUserId(String managerUserId) {
		this.managerUserId = managerUserId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public Double getLoanBalance() {
		return loanBalance;
	}
	public void setLoanBalance(Double loanBalance) {
		this.loanBalance = loanBalance;
	}
	
}
