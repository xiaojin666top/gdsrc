package com.beawan.loanAfter.dto;

public class FcAnalysisDto {
	
	private String profitAnaly;//盈利能力分析
    private String operateAnaly;//营运能力分析
    private String debtAnaly;//偿债能力分析
    private String cashflowAnaly;//现金流分析
    private String induRisk;//行业风险
    private String operateRisk;//经营风险
    private String managerRisk;//管理风险
    private String sponsorAnaly;//保证人分析
    private String collateralAnaly;//抵押物分析
    private String classifyReason;//分类理由
    private String fiveClass;//五级分类结果
    
	public String getProfitAnaly() {
		return profitAnaly;
	}
	public void setProfitAnaly(String profitAnaly) {
		this.profitAnaly = profitAnaly;
	}
	public String getOperateAnaly() {
		return operateAnaly;
	}
	public void setOperateAnaly(String operateAnaly) {
		this.operateAnaly = operateAnaly;
	}
	public String getDebtAnaly() {
		return debtAnaly;
	}
	public void setDebtAnaly(String debtAnaly) {
		this.debtAnaly = debtAnaly;
	}
	public String getCashflowAnaly() {
		return cashflowAnaly;
	}
	public void setCashflowAnaly(String cashflowAnaly) {
		this.cashflowAnaly = cashflowAnaly;
	}
	public String getInduRisk() {
		return induRisk;
	}
	public void setInduRisk(String induRisk) {
		this.induRisk = induRisk;
	}
	public String getOperateRisk() {
		return operateRisk;
	}
	public void setOperateRisk(String operateRisk) {
		this.operateRisk = operateRisk;
	}
	public String getManagerRisk() {
		return managerRisk;
	}
	public void setManagerRisk(String managerRisk) {
		this.managerRisk = managerRisk;
	}
	public String getSponsorAnaly() {
		return sponsorAnaly;
	}
	public void setSponsorAnaly(String sponsorAnaly) {
		this.sponsorAnaly = sponsorAnaly;
	}
	public String getCollateralAnaly() {
		return collateralAnaly;
	}
	public void setCollateralAnaly(String collateralAnaly) {
		this.collateralAnaly = collateralAnaly;
	}
	public String getClassifyReason() {
		return classifyReason;
	}
	public void setClassifyReason(String classifyReason) {
		this.classifyReason = classifyReason;
	}
	public String getFiveClass() {
		return fiveClass;
	}
	public void setFiveClass(String fiveClass) {
		this.fiveClass = fiveClass;
	}
	
}
