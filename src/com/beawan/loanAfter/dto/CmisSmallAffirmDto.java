package com.beawan.loanAfter.dto;

import java.util.List;

import com.beawan.dtmp.dto.CmisBillDto;
import com.beawan.dtmp.dto.CmisContractDto;
import com.beawan.dtmp.dto.FmContractDto;
import com.beawan.loanAfter.entity.FcFinaData;

/**
 * 五级分类   小额企事业认定表中  所需要的全部数据
 * @author yuzhejia
 *
 */
public class CmisSmallAffirmDto {

    private String xdCustNo;//借款人信贷号  -->以下数据都是通过信贷号获取
	
    private String customername;//客户名称
    private String orgnature;//组织机构类型   ---》国有企业  集体企业....
    private String mostbusiness;//主营业务
	private Double contractSum;//合同金额
	private String occurdate;//签订合同日期 yyyyMMdd
	private String maturity;//到期日期 yyyyMMdd
	private String vouchtype;//担保方式
    
    
    //贷款客户主体财务指标
    private List<FcFinaData> smallFinaData;
    
    private String currDate;//财报当期
    private String oneYearDate;//去年
    private String twoYearDate;//前年
    
    
	public String getXdCustNo() {
		return xdCustNo;
	}
	public void setXdCustNo(String xdCustNo) {
		this.xdCustNo = xdCustNo;
	}
	public String getCustomername() {
		return customername;
	}
	public void setCustomername(String customername) {
		this.customername = customername;
	}
	public String getOrgnature() {
		return orgnature;
	}
	public void setOrgnature(String orgnature) {
		this.orgnature = orgnature;
	}
	public String getMostbusiness() {
		return mostbusiness;
	}
	public void setMostbusiness(String mostbusiness) {
		this.mostbusiness = mostbusiness;
	}
	public Double getContractSum() {
		return contractSum;
	}
	public void setContractSum(Double contractSum) {
		this.contractSum = contractSum;
	}
	public String getOccurdate() {
		return occurdate;
	}
	public void setOccurdate(String occurdate) {
		this.occurdate = occurdate;
	}
	public String getMaturity() {
		return maturity;
	}
	public void setMaturity(String maturity) {
		this.maturity = maturity;
	}
	public String getVouchtype() {
		return vouchtype;
	}
	public void setVouchtype(String vouchtype) {
		this.vouchtype = vouchtype;
	}
	
	public List<FcFinaData> getSmallFinaData() {
		return smallFinaData;
	}
	public void setSmallFinaData(List<FcFinaData> smallFinaData) {
		this.smallFinaData = smallFinaData;
	}
	public String getCurrDate() {
		return currDate;
	}
	public void setCurrDate(String currDate) {
		this.currDate = currDate;
	}
	public String getOneYearDate() {
		return oneYearDate;
	}
	public void setOneYearDate(String oneYearDate) {
		this.oneYearDate = oneYearDate;
	}
	public String getTwoYearDate() {
		return twoYearDate;
	}
	public void setTwoYearDate(String twoYearDate) {
		this.twoYearDate = twoYearDate;
	}
    
 
    
}
