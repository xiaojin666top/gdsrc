package com.beawan.loanAfter.dto;

/**
 * 五级分类工作底稿、及认定表中  高管信息 及 资本构成
 * @author yuzhejia
 *
 */
public class CmisCustChargeDto {

    private String ship;//关系 中文名
    private String relationShip;//关系代码   0100法人  0103财务  50开头资本构成
    private String customerName;//关系人名称（个人或企业）
    private String certId;//证件号码
    private Double investmentSum;//实际出资金额
    private Double investmentProp;//占比 单位%
    private String telephone;//电话
    
    
	public String getShip() {
		return ship;
	}
	public void setShip(String ship) {
		this.ship = ship;
	}
	public String getRelationShip() {
		return relationShip;
	}
	public void setRelationShip(String relationShip) {
		this.relationShip = relationShip;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCertId() {
		return certId;
	}
	public void setCertId(String certId) {
		this.certId = certId;
	}
	public Double getInvestmentSum() {
		return investmentSum;
	}
	public void setInvestmentSum(Double investmentSum) {
		this.investmentSum = investmentSum;
	}
	public Double getInvestmentProp() {
		return investmentProp;
	}
	public void setInvestmentProp(Double investmentProp) {
		this.investmentProp = investmentProp;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
    
    
    
    
    
}
