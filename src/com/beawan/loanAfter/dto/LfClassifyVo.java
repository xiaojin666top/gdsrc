package com.beawan.loanAfter.dto;

/**
 * 贷后客户分类 查询条件
 * @author User
 *
 */
public class LfClassifyVo {
	
	private String managerName; //客户经理
	private String customerName;//客户名称
	private String riskClassi;//风险分类
	private String creditClassi;//授信分类
	
	public String getManagerName() {
		return managerName;
	}
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getRiskClassi() {
		return riskClassi;
	}
	public void setRiskClassi(String riskClassi) {
		this.riskClassi = riskClassi;
	}
	public String getCreditClassi() {
		return creditClassi;
	}
	public void setCreditClassi(String creditClassi) {
		this.creditClassi = creditClassi;
	}
}

