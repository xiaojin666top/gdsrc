package com.beawan.loanAfter.dto;

import java.util.HashMap;
import java.util.Map;

public class LfConstants {
	
	private static Map<String, String> riskNoMap = new HashMap<String, String>();
	private static Map<String, String> ficeNoMap = new HashMap<String, String>();
	private static Map<String, String> stageNoMap = new HashMap<String, String>();
	/*
	 * 风险分类字典
	 */
	public static String getLfriskNametoriskNo(String riskName){
		
		riskNoMap.put("正常类","01");
		riskNoMap.put("关注类","02");
		riskNoMap.put("风险一级","03");
		riskNoMap.put("风险二级","04");
		riskNoMap.put("风险三级","05");
		riskNoMap.put("增加授信","06");
		riskNoMap.put("维持授信","07");
		riskNoMap.put("减少授信","08");
		riskNoMap.put("退出授信","09");

		return riskNoMap.get(riskName);
	}
	
	/**
	 * 五级分类类别字典
	 * @param fiveNo
	 * @return
	 */
	public static String getLfFiveNotoFiveName(String fiveNo){
		
		ficeNoMap.put("QL01","正常");
		ficeNoMap.put("QL02","关注");
		ficeNoMap.put("QL03","次级");
		ficeNoMap.put("QL04","可疑");
		ficeNoMap.put("QL05","损失");

		return ficeNoMap.get(fiveNo);
	
	}
	/**
	 * 五级分类类别字典
	 * @param fiveNo
	 * @return
	 */
	public static String gettaskStatetoName(String stageNo){
		
		stageNoMap.put("400","审查分配");
		stageNoMap.put("401","待审查");
		stageNoMap.put("402","审查通过");
		stageNoMap.put("302","已提交");
		stageNoMap.put("301","已提交");
		stageNoMap.put("204","已提交");
		return stageNoMap.get(stageNo);
	
	}
	
	

}
