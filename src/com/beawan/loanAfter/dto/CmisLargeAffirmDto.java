package com.beawan.loanAfter.dto;

import java.util.List;

import com.beawan.dtmp.dto.CmisBillDto;
import com.beawan.dtmp.dto.CmisContractDto;
import com.beawan.dtmp.dto.FmContractDto;
import com.beawan.loanAfter.entity.FcFinaData;

/**
 * 五级分类   大额认定表中  所需要的全部数据
 * @author yuzhejia
 *
 */
public class CmisLargeAffirmDto {

    private String xdCustNo;//借款人信贷号  -->以下数据都是通过信贷号获取
	
	private String classifyOrg;//贷款分类单位
	private String classifyOperater;//分类操作员
    private String confirmDate;//分类时间
    private String customerName;//借款人名称
    private String address;//通讯地址
    
    //五级分类工作底稿、及认定表中  所需要的基础数据
    private CmisCustBaseDto custBaseDto;
    
    //贷款客户主体财务指标
    private List<FcFinaData> manuscriptFinaData;
    
    private String currDate;//财报当期
    private String oneYearDate;//去年
    private String twoYearDate;//前年
    
    private List<CmisContractDto> contractList;//合同信息

    
    private CmisCustStockDto stockDto;//客户固定资产
    
	public String getXdCustNo() {
		return xdCustNo;
	}

	public void setXdCustNo(String xdCustNo) {
		this.xdCustNo = xdCustNo;
	}

	public String getClassifyOrg() {
		return classifyOrg;
	}

	public void setClassifyOrg(String classifyOrg) {
		this.classifyOrg = classifyOrg;
	}

	public String getClassifyOperater() {
		return classifyOperater;
	}

	public void setClassifyOperater(String classifyOperater) {
		this.classifyOperater = classifyOperater;
	}

	public String getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(String confirmDate) {
		this.confirmDate = confirmDate;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public CmisCustBaseDto getCustBaseDto() {
		return custBaseDto;
	}

	public void setCustBaseDto(CmisCustBaseDto custBaseDto) {
		this.custBaseDto = custBaseDto;
	}

	public List<FcFinaData> getManuscriptFinaData() {
		return manuscriptFinaData;
	}

	public void setManuscriptFinaData(List<FcFinaData> manuscriptFinaData) {
		this.manuscriptFinaData = manuscriptFinaData;
	}

	public String getCurrDate() {
		return currDate;
	}

	public void setCurrDate(String currDate) {
		this.currDate = currDate;
	}

	public String getOneYearDate() {
		return oneYearDate;
	}

	public void setOneYearDate(String oneYearDate) {
		this.oneYearDate = oneYearDate;
	}

	public String getTwoYearDate() {
		return twoYearDate;
	}

	public void setTwoYearDate(String twoYearDate) {
		this.twoYearDate = twoYearDate;
	}

	public List<CmisContractDto> getContractList() {
		return contractList;
	}

	public void setContractList(List<CmisContractDto> contractList) {
		this.contractList = contractList;
	}

	public CmisCustStockDto getStockDto() {
		return stockDto;
	}


	public void setStockDto(CmisCustStockDto stockDto) {
		this.stockDto = stockDto;
	}


    
}
