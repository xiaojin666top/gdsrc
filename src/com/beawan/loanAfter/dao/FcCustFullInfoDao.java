package com.beawan.loanAfter.dao;

import com.beawan.core.BaseDao;
import com.beawan.loanAfter.entity.FcCustFullInfo;

/**
 * @author yzj
 */
public interface FcCustFullInfoDao extends BaseDao<FcCustFullInfo> {
}
