package com.beawan.loanAfter.dao;

import com.beawan.core.BaseDao;
import com.beawan.loanAfter.entity.LfCreditSelfBank;

/**
 * @author yzj
 */
public interface LfCreditSelfBankDao extends BaseDao<LfCreditSelfBank> {
}
