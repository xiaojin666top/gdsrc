package com.beawan.loanAfter.dao;

import java.util.List;

import com.beawan.core.BaseDao;
import com.beawan.core.Pagination;
import com.beawan.loanAfter.dto.LfCustDto;
import com.beawan.loanAfter.entity.FcCust;

/**
 * @author yzj
 */
public interface FcCustDao extends BaseDao<FcCust> {
	
	/**
	 * 批量保存，没有效果
	 * @param list
	 * @param batchSize
	 * @throws Exception
	 */
	void batchSaveFcCust(List<FcCust> list, int batchSize) throws Exception;

	/**
	 * 分页获取当前五级分类对应的客户列表
	 * @param taskId
	 * @param custName
	 * @param userName
	 * @return
	 */
	Pagination<LfCustDto> getFcCustList(Integer taskId, String custName, String userName, int page, int pageSize);
}
