package com.beawan.loanAfter.dao;

import com.beawan.core.BaseDao;
import com.beawan.loanAfter.entity.LfCustInfo;

/**
 * @author yzj
 */
public interface LfCustInfoDao extends BaseDao<LfCustInfo> {
}
