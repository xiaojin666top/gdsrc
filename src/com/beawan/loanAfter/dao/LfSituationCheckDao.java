package com.beawan.loanAfter.dao;

import java.util.List;

import com.beawan.core.BaseDao;
import com.beawan.loanAfter.entity.LfSituationCheck;

/**
 * @author yzj
 */
public interface LfSituationCheckDao extends BaseDao<LfSituationCheck> {
	List<LfSituationCheck> queryByTypeFirstLike(String typeFirstNo, Integer taskId) throws Exception;
}
