package com.beawan.loanAfter.dao;

import com.beawan.core.BaseDao;
import com.beawan.loanAfter.entity.LfFinCheck;

/**
 * @author yzj
 */
public interface LfFinCheckDao extends BaseDao<LfFinCheck> {
}
