package com.beawan.loanAfter.dao;

import com.beawan.core.BaseDao;
import com.beawan.loanAfter.entity.LfChangeCheck;

/**
 * @author yzj
 */
public interface LfChangeCheckDao extends BaseDao<LfChangeCheck> {
}
