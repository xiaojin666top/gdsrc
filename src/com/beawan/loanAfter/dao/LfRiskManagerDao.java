package com.beawan.loanAfter.dao;

import com.beawan.core.BaseDao;
import com.beawan.loanAfter.entity.LfRiskManager;

/**
 * @author yzj
 */
public interface LfRiskManagerDao extends BaseDao<LfRiskManager> {
}
