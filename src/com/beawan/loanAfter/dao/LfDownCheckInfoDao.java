package com.beawan.loanAfter.dao;

import com.beawan.core.BaseDao;
import com.beawan.loanAfter.entity.LfDownCheckInfo;

/**
 * @author yzj
 */
public interface LfDownCheckInfoDao extends BaseDao<LfDownCheckInfo> {
}
