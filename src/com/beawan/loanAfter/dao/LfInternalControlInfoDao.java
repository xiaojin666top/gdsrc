package com.beawan.loanAfter.dao;

import com.beawan.core.BaseDao;
import com.beawan.loanAfter.entity.LfInternalControlInfo;

/**
 * @author yzj
 */
public interface LfInternalControlInfoDao extends BaseDao<LfInternalControlInfo> {
}
