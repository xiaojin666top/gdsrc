package com.beawan.loanAfter.dao;

import com.beawan.core.BaseDao;
import com.beawan.loanAfter.entity.LfCfdAccountInfo;

/**
 * @author yzj
 */
public interface LfCfdAccountInfoDao extends BaseDao<LfCfdAccountInfo> {
}
