package com.beawan.loanAfter.dao;

import java.util.List;

import com.beawan.core.BaseDao;
import com.beawan.loanAfter.entity.FcBusiContract;

/**
 * @author yzj
 */
public interface FcBusiContractDao extends BaseDao<FcBusiContract> {
	
	/**
	 * 根据taskCustId 获取贷款信息
	 * @param taskCustId
	 * @return
	 */
	List<FcBusiContract> getByTaskCustId(Integer taskCustId);
}
