package com.beawan.loanAfter.dao;

import com.beawan.core.BaseDao;
import com.beawan.loanAfter.entity.LfFifteenDayCheck;

/**
 * @author yzj
 */
public interface LfFifteenDayCheckDao extends BaseDao<LfFifteenDayCheck> {
}
