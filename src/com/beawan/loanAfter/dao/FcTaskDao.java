package com.beawan.loanAfter.dao;

import java.util.List;

import com.beawan.core.BaseDao;
import com.beawan.loanAfter.dto.CmisCustBaseDto;
import com.beawan.loanAfter.dto.CmisCustChargeDto;
import com.beawan.loanAfter.dto.CmisSpecialAuthDto;
import com.beawan.loanAfter.entity.FcTask;

/**
 * @author yzj
 */
public interface FcTaskDao extends BaseDao<FcTask> {
	
	//五级分类需要从CMIS中获取的数据
	
	/**
	 * 获取企业基本信息
	 * @param xdCustNo
	 * @return
	 * @throws Exception
	 */
	CmisCustBaseDto getCustBaseInfo(String xdCustNo) throws Exception;
	
	/**
	 * 获取特殊行业的一些许可证
	 * 根据认证类型取 证书名称和认证单位信息：
	 * 当认证类型是 01时： 取 certName  certOrg
	 * 当认证类型是 010时： 取 certName  无认证单位
	 * 当认证类型是 02时： 取 certName  nameRending
	 * 当认证类型是 020时： 取 tradeName  无认证单位
	 * @param xdCustNo
	 */
	List<CmisSpecialAuthDto> getSpecialAuth(String xdCustNo) throws Exception;
	
	/**
	 * 获得企业管理层、资本构成信息（业务层需要加工）
	 * 高管信息包括（法人和财务）
	 * 资本构成的ship是以52开头
	 * @param xdCustNo
	 * @return
	 * @throws Exception
	 */
	List<CmisCustChargeDto> getChargeList(String xdCustNo) throws Exception;
}
