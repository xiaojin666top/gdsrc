package com.beawan.loanAfter.dao;

import java.util.List;
import java.util.Map;

import com.beawan.core.BaseDao;
import com.beawan.loanAfter.entity.LfRiskWarningInfo;
import com.beawan.loanAfter.entity.LfTask;
import com.beawan.loanAfter.dto.LfTaskDto;

/**
 * @author yzj
 */
public interface LfTaskDao extends BaseDao<LfTask> {
	
	/**
	 * 获取 预警客户信息名单
	 * @return
	 * @throws Exception
	 */
	public List<LfTaskDto> getWarnCust(String date) throws Exception;

	/**
	 * 获取客户详细预警信息列表
	 * @param custName
	 * @Param xdCustNo 信贷客户号
	 * @param date
	 * @return
	 */
	public List<LfRiskWarningInfo> getWarnInfoList(String custName, String xdCustNo, String date);

	/**
	 * 获取客户详细预警信息列表
	 * @param custName
	 * @Param xdCustNo 信贷客户号
	 * @param date
	 * @return
	 */
	public List<LfRiskWarningInfo> getWarnInfoList(String custName, String xdCustNo, String startDate, String endDate);
	
	
	/**
	 * 获取客户信息
	 * @Param xdCustNo 信贷客户号
	 * @return
	 */
	public List<LfTask> getLfCustBaseInfo(String xdCustNo);
	
}
