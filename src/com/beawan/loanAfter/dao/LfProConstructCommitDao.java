package com.beawan.loanAfter.dao;

import com.beawan.core.BaseDao;
import com.beawan.loanAfter.entity.LfProConstructCommit;

/**
 * @author yzj
 */
public interface LfProConstructCommitDao extends BaseDao<LfProConstructCommit> {
}
