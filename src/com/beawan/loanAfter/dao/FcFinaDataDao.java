package com.beawan.loanAfter.dao;

import java.util.List;

import com.beawan.core.BaseDao;
import com.beawan.loanAfter.entity.FcFinaData;

/**
 * @author yzj
 */
public interface FcFinaDataDao extends BaseDao<FcFinaData> {
	/**
	 * 获取 财务比率分析表
	 * @param taskCustId
	 * @return
	 */
	List<FcFinaData> getFinaRate(Integer taskCustId);
	
	/**
	 * 获取 现金流量分析表
	 * @param taskCustId
	 * @return
	 */
	List<FcFinaData> getCashFlow(Integer taskCustId);
}
