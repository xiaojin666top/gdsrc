package com.beawan.loanAfter.dao;

import com.beawan.core.BaseDao;
import com.beawan.loanAfter.entity.LfLocalResult;

/**
 * @author yzj
 */
public interface LfLocalResultDao extends BaseDao<LfLocalResult> {
}
