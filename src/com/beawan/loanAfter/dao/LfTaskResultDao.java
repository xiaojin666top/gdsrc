package com.beawan.loanAfter.dao;

import com.beawan.core.BaseDao;
import com.beawan.loanAfter.entity.LfTaskResult;

/**
 * @author yzj
 */
public interface LfTaskResultDao extends BaseDao<LfTaskResult> {
}
