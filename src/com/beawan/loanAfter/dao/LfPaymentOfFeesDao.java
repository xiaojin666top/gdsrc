package com.beawan.loanAfter.dao;

import com.beawan.core.BaseDao;
import com.beawan.loanAfter.entity.LfPaymentOfFees;

/**
 * @author yzj
 */
public interface LfPaymentOfFeesDao extends BaseDao<LfPaymentOfFees> {
}
