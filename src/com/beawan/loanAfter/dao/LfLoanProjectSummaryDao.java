package com.beawan.loanAfter.dao;

import com.beawan.core.BaseDao;
import com.beawan.loanAfter.entity.LfLoanProjectSummary;

/**
 * @author yzj
 */
public interface LfLoanProjectSummaryDao extends BaseDao<LfLoanProjectSummary> {
}
