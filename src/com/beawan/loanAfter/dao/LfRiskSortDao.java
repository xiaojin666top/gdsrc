package com.beawan.loanAfter.dao;

import com.beawan.core.BaseDao;
import com.beawan.core.Pagination;
import com.beawan.loanAfter.entity.LfRiskSort;

/**
 * @author yzj
 */
public interface LfRiskSortDao extends BaseDao<LfRiskSort> {

}
