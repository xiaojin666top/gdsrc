package com.beawan.loanAfter.dao;

import com.beawan.core.BaseDao;
import com.beawan.loanAfter.entity.LfAdminSanInfo;

/**
 * @author yzj
 */
public interface LfAdminSanInfoDao extends BaseDao<LfAdminSanInfo> {
}
