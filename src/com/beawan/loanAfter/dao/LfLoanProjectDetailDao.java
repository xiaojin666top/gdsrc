package com.beawan.loanAfter.dao;

import com.beawan.core.BaseDao;
import com.beawan.loanAfter.entity.LfLoanProjectDetail;

/**
 * @author yzj
 */
public interface LfLoanProjectDetailDao extends BaseDao<LfLoanProjectDetail> {
}
