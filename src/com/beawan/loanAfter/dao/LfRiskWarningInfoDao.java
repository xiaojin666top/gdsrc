package com.beawan.loanAfter.dao;

import com.beawan.core.BaseDao;
import com.beawan.loanAfter.entity.LfRiskWarningInfo;

/**
 * @author yzj
 */
public interface LfRiskWarningInfoDao extends BaseDao<LfRiskWarningInfo> {
}
