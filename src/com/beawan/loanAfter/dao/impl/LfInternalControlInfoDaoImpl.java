package com.beawan.loanAfter.dao.impl;

import com.beawan.core.BaseDaoImpl;
import org.springframework.stereotype.Repository;

import com.beawan.loanAfter.dao.LfInternalControlInfoDao;
import com.beawan.loanAfter.entity.LfInternalControlInfo;

/**
 * @author yzj
 */
@Repository("lfInternalControlInfoDao")
public class LfInternalControlInfoDaoImpl extends BaseDaoImpl<LfInternalControlInfo> implements LfInternalControlInfoDao{

}
