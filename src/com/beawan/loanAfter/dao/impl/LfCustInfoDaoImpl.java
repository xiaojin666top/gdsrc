package com.beawan.loanAfter.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.loanAfter.dao.LfCustInfoDao;
import com.beawan.loanAfter.entity.LfCustInfo;
import org.springframework.stereotype.Repository;

/**
 * @author yzj
 */
@Repository("lfCustInfoDao")
public class LfCustInfoDaoImpl extends BaseDaoImpl<LfCustInfo> implements LfCustInfoDao{

}
