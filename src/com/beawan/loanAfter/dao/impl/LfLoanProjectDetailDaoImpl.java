package com.beawan.loanAfter.dao.impl;

import com.beawan.core.BaseDaoImpl;
import org.springframework.stereotype.Repository;

import com.beawan.loanAfter.dao.LfLoanProjectDetailDao;
import com.beawan.loanAfter.entity.LfLoanProjectDetail;

/**
 * @author yzj
 */
@Repository("lfLoanProjectDetailDao")
public class LfLoanProjectDetailDaoImpl extends BaseDaoImpl<LfLoanProjectDetail> implements LfLoanProjectDetailDao{

}
