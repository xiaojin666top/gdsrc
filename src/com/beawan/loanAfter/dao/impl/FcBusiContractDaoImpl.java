package com.beawan.loanAfter.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.loanAfter.dao.FcBusiContractDao;
import com.beawan.loanAfter.entity.FcBusiContract;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

/**
 * @author yzj
 */
@Repository("fcBusiContractDao")
public class FcBusiContractDaoImpl extends BaseDaoImpl<FcBusiContract> implements FcBusiContractDao{

	@Override
	public List<FcBusiContract> getByTaskCustId(Integer taskCustId) {
		List<FcBusiContract> entityList = this.selectByProperty("taskCustId", taskCustId, "SERIALNO ASC");
		if(CollectionUtils.isEmpty(entityList) || entityList.size() <= 1) return entityList;
		
		List<FcBusiContract> resultList = new ArrayList<>();
		// entityList 去重操作
		FcBusiContract origin = entityList.get(0);
		resultList.add(origin);
		String guarantyName = origin.getGuarantyName() + ",";
		for(int i=1; i<entityList.size(); i++){
			FcBusiContract entity = entityList.get(i);
			if(entity.getSerialNo().equals(origin.getSerialNo())){
				guarantyName += entity.getGuarantyName() + ",";
				origin.setGuarantyName(guarantyName.substring(0, guarantyName.length()-1));
			}else{
				resultList.add(entity);
				origin = entity;
			}
		}
		return resultList;
	}

}
