package com.beawan.loanAfter.dao.impl;

import com.beawan.common.Constants;
import com.beawan.core.BaseDaoImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.beawan.loanAfter.dao.LfSituationCheckDao;
import com.beawan.loanAfter.entity.LfSituationCheck;

/**
 * @author yzj
 */
@Repository("lfSituationCheckDao")
public class LfSituationCheckDaoImpl extends BaseDaoImpl<LfSituationCheck> implements LfSituationCheckDao{

	@Override
	public List<LfSituationCheck> queryByTypeFirstLike(String typeFirstNo, Integer taskId) throws Exception {
		StringBuffer sql = new StringBuffer(" from LfSituationCheck where 1=1 ");
		sql.append(" and typeFirstNo like:typeFirstNo")
		   .append(" and taskId =:taskId")
		   .append(" and status =:status");
		Map<String,Object>param = new HashMap<>();
		param.put("typeFirstNo","%"+typeFirstNo+"%");
		param.put("taskId",taskId);
        param.put("status",Constants.NORMAL);
        List<LfSituationCheck> lfSituationCheckList = select(sql.toString(), param);
        return lfSituationCheckList;
	}

}
