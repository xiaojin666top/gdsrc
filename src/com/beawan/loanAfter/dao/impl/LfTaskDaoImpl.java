package com.beawan.loanAfter.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.beawan.core.BaseDaoImpl;
import com.beawan.loanAfter.dao.LfTaskDao;
import com.beawan.loanAfter.dto.LfCustDto;
import com.beawan.loanAfter.dto.LfTaskDto;
import com.beawan.loanAfter.entity.LfRiskWarningInfo;
import com.beawan.loanAfter.entity.LfTask;

/**
 * @author yzj
 */
@Repository("lfTaskDao")
public class LfTaskDaoImpl extends BaseDaoImpl<LfTask> implements LfTaskDao{

	@Override
	public List<LfTaskDto> getWarnCust(String date) throws Exception {
		//获取预警系统客户名单
//    	date = "20191201";
    	StringBuilder sql = new StringBuilder();
	    	sql.append("select C.CUSTOMER_NO customerNo,C.CUSTOMER_NAME customerName,C.MANAGER_USER_ID userNo from (")
	    	.append(" select CUSTOMERID customerId from KHFXYJ.ALARM_INFO where INPUTDATE=:date")
	    	.append(" union")
	    	.append(" select OBJECT_CODE customerId")
	    	.append(" from KHFXYJ2.WM_WARNING_SIGNAL_TB where WARNING_TIME=:date and (ISRELIEVE!='是' or ISRELIEVE is null)")
	    	.append(" ) A")
	    	.append(" JOIN CMIS.CUSTOMER_INFO B ON B.CUSTOMERID=A.CUSTOMERID")
	    	.append(" JOIN GDTCESYS.CUS_BASE C ON C.MF_CUSTOMER_NO=B.MFCUSTOMERID");
    	Map<String, Object> params = new HashMap<>();
    	params.put("date", date);
    			
    	List<LfTaskDto> warnList = findCustListBySql(LfTaskDto.class, sql.toString(), params);
    	
    	//获取OAP系统客户名单
    	//但是OAP系统里面竟然会没有客户编号，只有客户名称，这个是如皋科技部自己设计的表~~
    	//所以先查询出 当日所有存在预警信息的客户名单 --》而且包含了企业和个人  我们不管，直接用查出来的客户名字去cus_base表 查出客户编号
    	//覆盖下key -value
//    	params.put("date", "20180302");
    	StringBuilder oapSql = new StringBuilder();
    	oapSql.append("SELECT B.CUSTOMER_NO customerNo,B.CUSTOMER_NAME customerName,B.MANAGER_USER_ID userNo FROM ( ")
    		.append("select F1.JKQY JKQY from CMIS.RGFXJC_00005 F1 where F1.STAT_DATE=:date UNION ")
    		.append("select F2.QYMC JKQY from CMIS.RGFXJC_00006 F2 where F2.STAT_DATE=:date UNION ")
    		.append("select F3.JKRMC JKQY from CMIS.RGFXJC_00010 F3 where F3.STAT_DATE=:date UNION ")
    		.append("select F4.CUST_NAME JKQY from CMIS.RGFXJC_00014 F4 where F4.STAT_DATE=:date UNION ")
    		.append("select F5.CUST_NAME JKQY from CMIS.RGFXJC_00015 F5 where F5.STAT_DATE=:date UNION ")
    		.append("select F6.CUST_NAME JKQY from CMIS.RGFXJC_00016 F6 where F6.STAT_DATE=:date UNION ")
    		.append("select F7.CUST_NAME JKQY from CMIS.RGFXJC_00020 F7 where F7.STAT_DATE=:date UNION ")
    		.append("select F8.CUST_NAME JKQY from CMIS.RGFXJC_00029 F8 where F8.STAT_DATE=:date UNION ")
    		.append("select F9.CUST_NAME JKQY from CMIS.RGFXJC_00030 F9 where F9.STAT_DATE=:date UNION ")
    		.append("select F10.CUST_NAME JKQY from CMIS.RGFXJC_00040 F10 where F10.STAT_DATE=:date UNION ")
    		.append("select F11.CUST_NAME JKQY from CMIS.RGFXJC_00074 F11 where F11.STAT_DATE=:date UNION ")
    		.append("select F12.JKRMC JKQY from CMIS.RGFXJC_00078 F12 where F12.STAT_DATE=:date UNION ")
    		.append("select F13.JKRMC JKQY from CMIS.RGFXJC_00079 F13 where F13.STAT_DATE=:date UNION ")
    		.append("select F14.JKRMC JKQY from CMIS.RGFXJC_00080 F14 where F14.STAT_DATE=:date UNION ")
    		.append("select F15.JKRMC JKQY from CMIS.RGFXJC_00081 F15 where F15.STAT_DATE=:date UNION ")
    		.append("select F16.CUST_NAME JKQY from CMIS.RGFXJC_00083 F16 where F16.STAT_DATE=:date UNION ")
    		.append("select F17.JKRMC JKQY from CMIS.RGFXJC_00084 F17 where F17.STAT_DATE=:date UNION ")
    		.append("select F18.CUST_NAME JKQY from CMIS.RGFXJC_00086 F18 where F18.STAT_DATE=:date UNION ")
    		.append("select F19.CUST_NAME JKQY from CMIS.RGFXJC_00087 F19 where F19.STAT_DATE=:date UNION ")
    		.append("select F20.CUST_NAME1 JKQY from CMIS.RGFXJC_00102 F20 where F20.STAT_DATE=:date  UNION ")
    		.append("select F21.CUST_NAME2 JKQY from CMIS.RGFXJC_00102 F21 where F21.STAT_DATE=:date UNION ")
    		.append("select F22.CUST_NAME JKQY from CMIS.RGFXJC_00105 F22 where F22.STAT_DATE=:date UNION ")
    		.append("select F23.CUST_NAME JKQY from CMIS.RGFXJC_00106 F23 where F23.STAT_DATE=:date UNION ")
    		.append("select F24.CUST_NAME JKQY from CMIS.RGFXJC_00108 F24 where F24.STAT_DATE=:date UNION ")
    		.append("select F25.CUST_NAME JKQY from CMIS.RGFXJC_00109 F25 where F25.STAT_DATE=:date UNION ")
    		.append("select F26.CUST_NAME JKQY from CMIS.RGFXJC_00110 F26 where F26.STAT_DATE=:date UNION ")
    		.append("select F27.CUST_NAME JKQY from CMIS.RGFXJC_00111 F27 where F27.STAT_DATE=:date UNION ")
    		.append("select F28.CUST_NAME JKQY from CMIS.RGFXJC_00112 F28 where F28.STAT_DATE=:date UNION ")
    		.append("select F29.CUST_NAME JKQY from CMIS.RGFXJC_00113 F29 where F29.STAT_DATE=:date UNION ")
    		.append("select F30.CUST_NAME JKQY from CMIS.RGFXJC_00114 F30 where F30.STAT_DATE=:date UNION ")
    		.append("select F31.CUST_NAME JKQY from CMIS.RGFXJC_00115 F31 where F31.STAT_DATE=:date  ")
    		.append(") A join GDTCESYS.CUS_BASE B ON A.JKQY=B.CUSTOMER_NAME");
    	List<LfTaskDto> oapList = findCustListBySql(LfTaskDto.class, oapSql.toString(), params);
    	//合并两个结果集
    	if(CollectionUtils.isEmpty(warnList) && CollectionUtils.isEmpty(oapList)){
    		return null;
    	}else if(!CollectionUtils.isEmpty(warnList) && CollectionUtils.isEmpty(oapList)){
    		return warnList;
    	}else if(CollectionUtils.isEmpty(warnList) && !CollectionUtils.isEmpty(oapList)){
    		return oapList;
    	}else {
        	warnList.addAll(oapList);
    		return warnList;
    	}
	}

	@Override
	public List<LfRiskWarningInfo> getWarnInfoList(String custName, String xdCustNo, String date) {
		List<LfRiskWarningInfo> warnList = new ArrayList<>();
		
		Map<String, Object> params = new HashMap<>();
		params.put("custName", custName);
		System.out.println(custName);
		params.put("date", date);
		StringBuilder alarmInfoSQL = new StringBuilder("select ALARMINFODESC riskName,'ALARM_INFO' riskNo,'FXYJ' riskFrom ")
					.append(" from KHFXYJ.ALARM_INFO where CUSTOMERNAME=:custName and INPUTDATE=:date");
		List<LfRiskWarningInfo> alarmList = findCustListBySql(LfRiskWarningInfo.class, alarmInfoSQL, params);

		StringBuilder wmSQL = new StringBuilder("select SIGNAL_NAME riskName,'WM_WARNING_SIGNAL_TB' riskNo,'FXYJ' riskFrom ")
					.append(" from KHFXYJ2.WM_WARNING_SIGNAL_TB where OBJECT_CODE=?0 and WARNING_TIME=?1");
		List<LfRiskWarningInfo> wmList = findCustListBySql(LfRiskWarningInfo.class, wmSQL, xdCustNo, date);

		StringBuilder RGFXJC_00005_SQL = new StringBuilder("select DISTINCT('借款企业或其法人夫妇本行历史有不良记录（含核销不良）') AS riskName,'RGFXJC_00005' riskNo, ")
					.append(" 'OAP' riskFrom from CMIS.RGFXJC_00005 where JKQY=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg005List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00005_SQL, params);

		StringBuilder RGFXJC_00006_SQL = new StringBuilder("select DISTINCT('企业与法定代表人或主要股东及其配偶同贷') AS riskName,'RGFXJC_00006' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00006 where QYMC=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg006List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00006_SQL, params);
		
		StringBuilder RGFXJC_00010_SQL = new StringBuilder("select DISTINCT('借款人或其配偶本行历史有不良记录（含核销不良）') AS riskName,'RGFXJC_00010' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00010 where JKRMC=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg010List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00010_SQL, params);

		StringBuilder RGFXJC_00014_SQL = new StringBuilder("select DISTINCT('违反信贷政策-贷款资金用于投资理财') AS riskName,'RGFXJC_00014' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00014 where CUST_NAME=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg014List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00014_SQL, params);

		StringBuilder RGFXJC_00015_SQL = new StringBuilder("select DISTINCT('违规开展票据业务-贷款资金用于银行承兑汇票保证金') AS riskName,'RGFXJC_00015' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00015 where CUST_NAME=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg015List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00015_SQL, params);

		StringBuilder RGFXJC_00016_SQL = new StringBuilder("select DISTINCT('违反房地产行业政策-信贷资金用于购房') AS riskName,'RGFXJC_00016' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00016 where CUST_NAME=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg016List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00016_SQL, params);

		StringBuilder RGFXJC_00020_SQL = new StringBuilder("select DISTINCT('违反信贷管理--冒名贷款') AS riskName,'RGFXJC_00020' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00020 where CUST_NAME=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg020List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00020_SQL, params);

		StringBuilder RGFXJC_00029_SQL = new StringBuilder("select DISTINCT('以贷还贷，以贷收息') AS riskName,'RGFXJC_00029' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00029 where CUST_NAME=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg029List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00029_SQL, params);

		StringBuilder RGFXJC_00030_SQL = new StringBuilder("select DISTINCT('单一客户为多人还贷2次及以上') AS riskName,'RGFXJC_00030' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00030 where CUST_NAME=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg030List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00030_SQL, params);

		StringBuilder RGFXJC_00040_SQL = new StringBuilder("select DISTINCT('贴现资金转手前手人') AS riskName,'RGFXJC_00040' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00040 where CUST_NAME=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg040List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00040_SQL, params);

		StringBuilder RGFXJC_00074_SQL = new StringBuilder("select DISTINCT('向房地产企业发放流动资金贷款') AS riskName,'RGFXJC_00074' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00074 where CUST_NAME=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg074List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00074_SQL, params);

		StringBuilder RGFXJC_00078_SQL = new StringBuilder("select DISTINCT('新增贷款的担保人有核销史') AS riskName,'RGFXJC_00078' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00078 where JKRMC=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg078List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00078_SQL, params);

		StringBuilder RGFXJC_00079_SQL = new StringBuilder("select DISTINCT('新增贷款的担保人为黑名单客户') AS riskName,'RGFXJC_00079' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00079 where JKRMC=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg079List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00079_SQL, params);

		StringBuilder RGFXJC_00080_SQL = new StringBuilder("select DISTINCT('借款企业法人夫妻及主要股东在本行有借款') AS riskName,'RGFXJC_00080' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00080 where JKRMC=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg080List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00080_SQL, params);

		StringBuilder RGFXJC_00081_SQL = new StringBuilder("select DISTINCT('查询是否向黑名单客户发放贷款') AS riskName,'RGFXJC_00081' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00081 where JKRMC=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg081List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00081_SQL, params);

		StringBuilder RGFXJC_00083_SQL = new StringBuilder("select DISTINCT('以流动资金贷款名义发放固定资产贷款') AS riskName,'RGFXJC_00083' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00083 where CUST_NAME=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg083List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00083_SQL, params);

		StringBuilder RGFXJC_00084_SQL = new StringBuilder("select DISTINCT('贷款资金流入企业法定代表人个人存款账户') AS riskName,'RGFXJC_00084' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00084 where JKRMC=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg084List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00084_SQL, params);

		StringBuilder RGFXJC_00086_SQL = new StringBuilder("select DISTINCT('对公贷款资金入股市') AS riskName,'RGFXJC_00086' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00086 where CUST_NAME=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg086List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00086_SQL, params);

		StringBuilder RGFXJC_00087_SQL = new StringBuilder("select DISTINCT('贷款资金转房地产、典当行、小额贷款公司、担保公司') AS riskName,'RGFXJC_00087' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00087 where CUST_NAME=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg087List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00087_SQL, params);

		StringBuilder RGFXJC_00102_SQL = new StringBuilder("select DISTINCT('借款人互为担保人') AS riskName,'RGFXJC_00102' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00102 where CUST_NAME1=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg102List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00102_SQL, params);

		StringBuilder RGFXJC_00105_SQL = new StringBuilder("select DISTINCT('连续3个月还款资金来源不同') AS riskName,'RGFXJC_00105' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00105 where CUST_NAME=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg105List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00105_SQL, params);

		StringBuilder RGFXJC_00106_SQL = new StringBuilder("select DISTINCT('连续3个月用现金还款') AS riskName,'RGFXJC_00106' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00106 where CUST_NAME=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg106List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00106_SQL, params);

		StringBuilder RGFXJC_00108_SQL = new StringBuilder("select DISTINCT('企业疑似停发工资2个月') AS riskName,'RGFXJC_00108' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00108 where CUST_NAME=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg108List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00108_SQL, params);

		StringBuilder RGFXJC_00109_SQL = new StringBuilder("select DISTINCT('企业疑似停缴电费2个月') AS riskName,'RGFXJC_00109' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00109 where CUST_NAME=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg109List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00109_SQL, params);

		StringBuilder RGFXJC_00110_SQL = new StringBuilder("select DISTINCT('企业疑似停缴税费2个月') AS riskName,'RGFXJC_00110' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00110 where CUST_NAME=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg110List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00110_SQL, params);

		StringBuilder RGFXJC_00111_SQL = new StringBuilder("select DISTINCT('企业代发工资季度同比下降超过30%') AS riskName,'RGFXJC_00111' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00111 where CUST_NAME=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg111List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00111_SQL, params);

		StringBuilder RGFXJC_00112_SQL = new StringBuilder("select DISTINCT('企业缴纳电费季度同比下降超过30%') AS riskName,'RGFXJC_00112' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00112 where CUST_NAME=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg112List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00112_SQL, params);

		StringBuilder RGFXJC_00113_SQL = new StringBuilder("select DISTINCT('企业缴纳税费季度同比下降超过30%') AS riskName,'RGFXJC_00113' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00113 where CUST_NAME=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg113List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00113_SQL, params);

		StringBuilder RGFXJC_00114_SQL = new StringBuilder("select DISTINCT('企业货款归行率较低') AS riskName,'RGFXJC_00114' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00114 where CUST_NAME=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg114List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00114_SQL, params);

		StringBuilder RGFXJC_00115_SQL = new StringBuilder("select DISTINCT('企业货款归行率季度同比下降超过50%') AS riskName,'RGFXJC_00115' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00115 where CUST_NAME=:custName and STAT_DATE=:date");
		List<LfRiskWarningInfo> rg115List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00115_SQL, params);

		this.mix(warnList, alarmList);
		this.mix(warnList, wmList);
		this.mix(warnList, rg005List);
		this.mix(warnList, rg006List);
		this.mix(warnList, rg010List);
		this.mix(warnList, rg014List);
		this.mix(warnList, rg015List);
		this.mix(warnList, rg016List);
		this.mix(warnList, rg020List);
		this.mix(warnList, rg029List);
		this.mix(warnList, rg030List);
		this.mix(warnList, rg040List);
		this.mix(warnList, rg074List);
		this.mix(warnList, rg078List);
		this.mix(warnList, rg079List);
		this.mix(warnList, rg080List);
		this.mix(warnList, rg081List);
		this.mix(warnList, rg083List);
		this.mix(warnList, rg084List);
		this.mix(warnList, rg086List);
		this.mix(warnList, rg087List);
		this.mix(warnList, rg102List);
		this.mix(warnList, rg105List);
		this.mix(warnList, rg106List);
		this.mix(warnList, rg108List);
		this.mix(warnList, rg109List);
		this.mix(warnList, rg110List);
		this.mix(warnList, rg111List);
		this.mix(warnList, rg112List);
		this.mix(warnList, rg113List);
		this.mix(warnList, rg114List);
		this.mix(warnList, rg115List);
		
		return warnList;
	}
	
	
	

	@Override
	public List<LfRiskWarningInfo> getWarnInfoList(String custName, String xdCustNo, String startDate, String endDate) {
		List<LfRiskWarningInfo> warnList = new ArrayList<>();
		
		Map<String, Object> params = new HashMap<>();
		params.put("custName", custName);
		params.put("startDate", startDate);
		params.put("endDate", endDate);
		StringBuilder alarmInfoSQL = new StringBuilder("select ALARMINFODESC riskName,'ALARM_INFO' riskNo,'FXYJ' riskFrom ")
					.append(" from KHFXYJ.ALARM_INFO where CUSTOMERNAME=:custName and INPUTDATE>=:startDate and INPUTDATE<=:endDate");
		List<LfRiskWarningInfo> alarmList = findCustListBySql(LfRiskWarningInfo.class, alarmInfoSQL, params);

		StringBuilder wmSQL = new StringBuilder("select SIGNAL_NAME riskName,'WM_WARNING_SIGNAL_TB' riskNo,'FXYJ' riskFrom ")
					.append(" from KHFXYJ2.WM_WARNING_SIGNAL_TB where OBJECT_CODE=?0 and WARNING_TIME>=?1 and WARNING_TIME<=?2");
		List<LfRiskWarningInfo> wmList = findCustListBySql(LfRiskWarningInfo.class, wmSQL, xdCustNo, startDate, endDate);

		StringBuilder RGFXJC_00005_SQL = new StringBuilder("select DISTINCT('借款企业或其法人夫妇本行历史有不良记录（含核销不良）') AS riskName,'RGFXJC_00005' riskNo, ")
					.append(" 'OAP' riskFrom from CMIS.RGFXJC_00005 where JKQY=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg005List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00005_SQL, params);

		StringBuilder RGFXJC_00006_SQL = new StringBuilder("select DISTINCT('企业与法定代表人或主要股东及其配偶同贷') AS riskName,'RGFXJC_00006' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00006 where QYMC=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg006List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00006_SQL, params);
		
		StringBuilder RGFXJC_00010_SQL = new StringBuilder("select DISTINCT('借款人或其配偶本行历史有不良记录（含核销不良）') AS riskName,'RGFXJC_00010' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00010 where JKRMC=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg010List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00010_SQL, params);

		StringBuilder RGFXJC_00014_SQL = new StringBuilder("select DISTINCT('违反信贷政策-贷款资金用于投资理财') AS riskName,'RGFXJC_00014' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00014 where CUST_NAME=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg014List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00014_SQL, params);

		StringBuilder RGFXJC_00015_SQL = new StringBuilder("select DISTINCT('违规开展票据业务-贷款资金用于银行承兑汇票保证金') AS riskName,'RGFXJC_00015' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00015 where CUST_NAME=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg015List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00015_SQL, params);

		StringBuilder RGFXJC_00016_SQL = new StringBuilder("select DISTINCT('违反房地产行业政策-信贷资金用于购房') AS riskName,'RGFXJC_00016' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00016 where CUST_NAME=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg016List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00016_SQL, params);

		StringBuilder RGFXJC_00020_SQL = new StringBuilder("select DISTINCT('违反信贷管理--冒名贷款') AS riskName,'RGFXJC_00020' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00020 where CUST_NAME=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg020List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00020_SQL, params);

		StringBuilder RGFXJC_00029_SQL = new StringBuilder("select DISTINCT('以贷还贷，以贷收息') AS riskName,'RGFXJC_00029' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00029 where CUST_NAME=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg029List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00029_SQL, params);

		StringBuilder RGFXJC_00030_SQL = new StringBuilder("select DISTINCT('单一客户为多人还贷2次及以上') AS riskName,'RGFXJC_00030' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00030 where CUST_NAME=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg030List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00030_SQL, params);

		StringBuilder RGFXJC_00040_SQL = new StringBuilder("select DISTINCT('贴现资金转手前手人') AS riskName,'RGFXJC_00040' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00040 where CUST_NAME=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg040List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00040_SQL, params);

		StringBuilder RGFXJC_00074_SQL = new StringBuilder("select DISTINCT('向房地产企业发放流动资金贷款') AS riskName,'RGFXJC_00074' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00074 where CUST_NAME=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg074List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00074_SQL, params);

		StringBuilder RGFXJC_00078_SQL = new StringBuilder("select DISTINCT('新增贷款的担保人有核销史') AS riskName,'RGFXJC_00078' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00078 where JKRMC=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg078List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00078_SQL, params);

		StringBuilder RGFXJC_00079_SQL = new StringBuilder("select DISTINCT('新增贷款的担保人为黑名单客户') AS riskName,'RGFXJC_00079' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00079 where JKRMC=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg079List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00079_SQL, params);

		StringBuilder RGFXJC_00080_SQL = new StringBuilder("select DISTINCT('借款企业法人夫妻及主要股东在本行有借款') AS riskName,'RGFXJC_00080' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00080 where JKRMC=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg080List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00080_SQL, params);

		StringBuilder RGFXJC_00081_SQL = new StringBuilder("select DISTINCT('查询是否向黑名单客户发放贷款') AS riskName,'RGFXJC_00081' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00081 where JKRMC=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg081List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00081_SQL, params);

		StringBuilder RGFXJC_00083_SQL = new StringBuilder("select DISTINCT('以流动资金贷款名义发放固定资产贷款') AS riskName,'RGFXJC_00083' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00083 where CUST_NAME=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg083List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00083_SQL, params);

		StringBuilder RGFXJC_00084_SQL = new StringBuilder("select DISTINCT('贷款资金流入企业法定代表人个人存款账户') AS riskName,'RGFXJC_00084' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00084 where JKRMC=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg084List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00084_SQL, params);

		StringBuilder RGFXJC_00086_SQL = new StringBuilder("select DISTINCT('对公贷款资金入股市') AS riskName,'RGFXJC_00086' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00086 where CUST_NAME=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg086List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00086_SQL, params);

		StringBuilder RGFXJC_00087_SQL = new StringBuilder("select DISTINCT('贷款资金转房地产、典当行、小额贷款公司、担保公司') AS riskName,'RGFXJC_00087' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00087 where CUST_NAME=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg087List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00087_SQL, params);

		StringBuilder RGFXJC_00102_SQL = new StringBuilder("select DISTINCT('借款人互为担保人') AS riskName,'RGFXJC_00102' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00102 where CUST_NAME1=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg102List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00102_SQL, params);

		StringBuilder RGFXJC_00105_SQL = new StringBuilder("select DISTINCT('连续3个月还款资金来源不同') AS riskName,'RGFXJC_00105' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00105 where CUST_NAME=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg105List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00105_SQL, params);

		StringBuilder RGFXJC_00106_SQL = new StringBuilder("select DISTINCT('连续3个月用现金还款') AS riskName,'RGFXJC_00106' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00106 where CUST_NAME=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg106List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00106_SQL, params);

		StringBuilder RGFXJC_00108_SQL = new StringBuilder("select DISTINCT('企业疑似停发工资2个月') AS riskName,'RGFXJC_00108' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00108 where CUST_NAME=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg108List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00108_SQL, params);

		StringBuilder RGFXJC_00109_SQL = new StringBuilder("select DISTINCT('企业疑似停缴电费2个月') AS riskName,'RGFXJC_00109' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00109 where CUST_NAME=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg109List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00109_SQL, params);

		StringBuilder RGFXJC_00110_SQL = new StringBuilder("select DISTINCT('企业疑似停缴税费2个月') AS riskName,'RGFXJC_00110' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00110 where CUST_NAME=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg110List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00110_SQL, params);

		StringBuilder RGFXJC_00111_SQL = new StringBuilder("select DISTINCT('企业代发工资季度同比下降超过30%') AS riskName,'RGFXJC_00111' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00111 where CUST_NAME=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg111List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00111_SQL, params);

		StringBuilder RGFXJC_00112_SQL = new StringBuilder("select DISTINCT('企业缴纳电费季度同比下降超过30%') AS riskName,'RGFXJC_00112' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00112 where CUST_NAME=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg112List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00112_SQL, params);

		StringBuilder RGFXJC_00113_SQL = new StringBuilder("select DISTINCT('企业缴纳税费季度同比下降超过30%') AS riskName,'RGFXJC_00113' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00113 where CUST_NAME=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg113List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00113_SQL, params);

		StringBuilder RGFXJC_00114_SQL = new StringBuilder("select DISTINCT('企业货款归行率较低') AS riskName,'RGFXJC_00114' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00114 where CUST_NAME=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg114List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00114_SQL, params);

		StringBuilder RGFXJC_00115_SQL = new StringBuilder("select DISTINCT('企业货款归行率季度同比下降超过50%') AS riskName,'RGFXJC_00115' riskNo,")
					.append(" 'OAP' riskFrom  from CMIS.RGFXJC_00115 where CUST_NAME=:custName and STAT_DATE>=:startDate and STAT_DATE<=:endDate");
		List<LfRiskWarningInfo> rg115List = findCustListBySql(LfRiskWarningInfo.class, RGFXJC_00115_SQL, params);

		this.mix(warnList, alarmList);
		this.mix(warnList, wmList);
		this.mix(warnList, rg005List);
		this.mix(warnList, rg006List);
		this.mix(warnList, rg010List);
		this.mix(warnList, rg014List);
		this.mix(warnList, rg015List);
		this.mix(warnList, rg016List);
		this.mix(warnList, rg020List);
		this.mix(warnList, rg029List);
		this.mix(warnList, rg030List);
		this.mix(warnList, rg040List);
		this.mix(warnList, rg074List);
		this.mix(warnList, rg078List);
		this.mix(warnList, rg079List);
		this.mix(warnList, rg080List);
		this.mix(warnList, rg081List);
		this.mix(warnList, rg083List);
		this.mix(warnList, rg084List);
		this.mix(warnList, rg086List);
		this.mix(warnList, rg087List);
		this.mix(warnList, rg102List);
		this.mix(warnList, rg105List);
		this.mix(warnList, rg106List);
		this.mix(warnList, rg108List);
		this.mix(warnList, rg109List);
		this.mix(warnList, rg110List);
		this.mix(warnList, rg111List);
		this.mix(warnList, rg112List);
		this.mix(warnList, rg113List);
		this.mix(warnList, rg114List);
		this.mix(warnList, rg115List);
		
		return warnList;
	}
	
	private void mix(List<LfRiskWarningInfo> tatget, List<LfRiskWarningInfo> newList){
		if(CollectionUtils.isEmpty(newList))
			return;
		tatget.addAll(newList);
	}

	@Override
	public List<LfTask> getLfCustBaseInfo(String xdCustNo) {
		
		StringBuilder sql = new StringBuilder();
		sql.append("select A.LICENSENO AS customerCreNum,A.REGISTERADD AS registerAddress,A.OFFICEADD AS runAddress, ")
		.append("A.FICTITIOUSPERSON AS legalPerson,A.MOSTBUSINESS AS runScope ")
		.append("from CMIS.ENT_INFO A ")
		.append("WHERE A.CUSTOMERID=?0");
 
		return this.findCustListBySql(LfTask.class, sql.toString(), xdCustNo);
	}

}
