package com.beawan.loanAfter.dao.impl;

import com.beawan.common.Constants;
import com.beawan.core.BaseDaoImpl;
import com.beawan.loanAfter.dao.FcFinaDataDao;
import com.beawan.loanAfter.entity.FcFinaData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;

/**
 * @author yzj
 */
@Repository("fcFinaDataDao")
public class FcFinaDataDaoImpl extends BaseDaoImpl<FcFinaData> implements FcFinaDataDao{

	@Override
	public List<FcFinaData> getFinaRate(Integer taskCustId) {
		String query = "MOLD = :mold And TASK_CUST_ID= :taskCustId AND STATUS= :status ORDER BY SORT_INDEX";
		Map<String, Object> params = new HashMap<>();
		params.put("mold", Constants.FINA_MOLD.RATIO);
		params.put("taskCustId", taskCustId);
		params.put("status", Constants.NORMAL);
		List<FcFinaData> list = this.select(query, params);
		if(CollectionUtils.isEmpty(list)){
			return list;
		}
		for(FcFinaData data : list){
			if(data.getItemName()!=null){
				if(data.getItemName().contains("（%）")){
					data.setCurrVal(data.getCurrVal() * 100);
					data.setOneYearVal(data.getOneYearVal() * 100);
					data.setTwoYearVal(data.getTwoYearVal() * 100);
				}
			}
		}
		return list;
	}

	@Override
	public List<FcFinaData> getCashFlow(Integer taskCustId) {
		String query = "MOLD = :mold And TASK_CUST_ID= :taskCustId AND STATUS= :status ORDER BY SORT_INDEX";
		Map<String, Object> params = new HashMap<>();
		params.put("mold", Constants.FINA_MOLD.CASHFLOW);
		params.put("taskCustId", taskCustId);
		params.put("status", Constants.NORMAL);
		return this.select(query, params);
	}

}
