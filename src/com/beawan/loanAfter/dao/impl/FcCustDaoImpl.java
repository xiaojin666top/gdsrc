package com.beawan.loanAfter.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.core.Pagination;
import com.beawan.loanAfter.dao.FcCustDao;
import com.beawan.loanAfter.dto.LfCustDto;
import com.beawan.loanAfter.entity.FcCust;

/**
 * @author yzj
 */
@Repository("fcCustDao")
public class FcCustDaoImpl extends BaseDaoImpl<FcCust> implements FcCustDao{
	protected static final Logger log = LoggerFactory.getLogger(FcCustDaoImpl.class);

	@PersistenceUnit
	EntityManagerFactory emf;
	EntityManager em;
	
	@Override
	public void batchSaveFcCust(List<FcCust> list, int batchSize) throws Exception {
		em = emf.createEntityManager();
		EntityTransaction transaction = em.getTransaction();
		transaction.begin();
		Session session = (Session)em.getDelegate();
		session.setFlushMode(FlushMode.MANUAL);
		for(int i = 0; i < list.size(); i++){
			/** 将要批量插入的实例转换为managed(托管)状态 */
			session.merge(list.get(i));
			if(i % batchSize == (batchSize - 1) || i==(list.size() - 1)){
				LOGGER.info("第" + (i/batchSize) + "次刷新entityManager!");
				session.flush();
				session.clear();
				transaction.commit();
			}
		}
		if(em!=null && em.isOpen()){
			em.close();
		}
		log.info("批量保存操作完成");
		
	}

	@Override
	public Pagination<LfCustDto> getFcCustList(Integer taskId, String custName
			, String userName, int page, int pageSize) {
		
		Map<String, Object> params = new HashMap<>();
		StringBuilder sql = new StringBuilder();
		sql.append("select A.ID id,A.CUSTOMER_NO customerNo,A.TASK_ID taskId,A.STATE state, ")
		.append("B.CUSTOMER_NAME customerName,C.USER_ID managerUserId,C.USER_NAME userName ")
		.append("from GDTCESYS.LFC_TASK_CUST A ")
		.append("join GDTCESYS.CUS_BASE B ON A.CUSTOMER_NO=B.CUSTOMER_NO ")
		.append("JOIN GDTCESYS.BS_USER_INFO C ON B.MANAGER_USER_ID=C.USER_ID ")
		.append("WHERE A.TASK_ID= :taskId ");
		
		params.put("taskId", taskId);
		
		if(!StringUtils.isEmpty(custName)){
			sql.append(" AND B.CUSTOMER_NAME LIKE :custName ");
			params.put("custName", "%"+custName+"%");
		}
		if(!StringUtils.isEmpty(userName)){
			sql.append(" AND C.USER_NAME LIKE :userName ");
			params.put("userName", "%"+userName+"%");
		}
		String count = "select count(1) from (" + sql + ")";
		sql.append("ORDER BY A.STATE,C.USER_NAME,B.CUSTOMER_NAME");
		return findCustSqlPagination(LfCustDto.class, sql, count, params, page, pageSize);
		
	}

}
