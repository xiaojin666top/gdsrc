package com.beawan.loanAfter.dao.impl;

import com.beawan.core.BaseDaoImpl;
import org.springframework.stereotype.Repository;

import com.beawan.loanAfter.dao.LfProjectInfoDao;
import com.beawan.loanAfter.entity.LfProjectInfo;

/**
 * @author yzj
 */
@Repository("lfProjectInfoDao")
public class LfProjectInfoDaoImpl extends BaseDaoImpl<LfProjectInfo> implements LfProjectInfoDao{

}
