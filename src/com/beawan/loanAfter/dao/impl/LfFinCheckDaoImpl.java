package com.beawan.loanAfter.dao.impl;

import com.beawan.core.BaseDaoImpl;
import org.springframework.stereotype.Repository;

import com.beawan.loanAfter.dao.LfFinCheckDao;
import com.beawan.loanAfter.entity.LfFinCheck;

/**
 * @author yzj
 */
@Repository("lfFinCheckDao")
public class LfFinCheckDaoImpl extends BaseDaoImpl<LfFinCheck> implements LfFinCheckDao{

}
