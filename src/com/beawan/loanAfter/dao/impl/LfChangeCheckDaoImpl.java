package com.beawan.loanAfter.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.loanAfter.dao.LfChangeCheckDao;
import org.springframework.stereotype.Repository;
import com.beawan.loanAfter.entity.LfChangeCheck;

/**
 * @author yzj
 */
@Repository("lfChangeCheckDao")
public class LfChangeCheckDaoImpl extends BaseDaoImpl<LfChangeCheck> implements LfChangeCheckDao{

}
