package com.beawan.loanAfter.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.loanAfter.dao.LfProConstructCommitDao;
import com.beawan.loanAfter.entity.LfProConstructCommit;

/**
 * @author yzj
 */
@Repository("lfProConstructCommitDao")
public class LfProConstructCommitDaoImpl extends BaseDaoImpl<LfProConstructCommit> implements LfProConstructCommitDao{

}
