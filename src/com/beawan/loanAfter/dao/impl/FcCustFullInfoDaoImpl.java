package com.beawan.loanAfter.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.loanAfter.dao.FcCustFullInfoDao;
import com.beawan.loanAfter.entity.FcCustFullInfo;
import org.springframework.stereotype.Repository;
import com.beawan.loanAfter.entity.FcCustFullInfo;

/**
 * @author yzj
 */
@Repository("fcCustFullInfoDao")
public class FcCustFullInfoDaoImpl extends BaseDaoImpl<FcCustFullInfo> implements FcCustFullInfoDao{

}
