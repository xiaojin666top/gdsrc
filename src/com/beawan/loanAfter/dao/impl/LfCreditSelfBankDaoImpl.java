package com.beawan.loanAfter.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.loanAfter.dao.LfCreditSelfBankDao;
import com.beawan.loanAfter.entity.LfCreditSelfBank;

/**
 * @author yzj
 */
@Repository("lfCreditSelfBankDao")
public class LfCreditSelfBankDaoImpl extends BaseDaoImpl<LfCreditSelfBank> implements LfCreditSelfBankDao{

}
