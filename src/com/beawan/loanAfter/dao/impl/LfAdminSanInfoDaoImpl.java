package com.beawan.loanAfter.dao.impl;

import com.beawan.core.BaseDaoImpl;
import org.springframework.stereotype.Repository;

import com.beawan.loanAfter.dao.LfAdminSanInfoDao;
import com.beawan.loanAfter.entity.LfAdminSanInfo;

/**
 * @author yzj
 */
@Repository("lfAdminSanInfoDao")
public class LfAdminSanInfoDaoImpl extends BaseDaoImpl<LfAdminSanInfo> implements LfAdminSanInfoDao{

}
