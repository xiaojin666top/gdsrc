package com.beawan.loanAfter.dao.impl;

import com.beawan.common.Constants;
import com.beawan.core.BaseDaoImpl;
import com.beawan.core.Pagination;
import com.beawan.corporateloan.entity.LeExaminePoint;
import com.beawan.loanAfter.dao.LfRiskSortDao;
import com.beawan.loanAfter.entity.LfRiskSort;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;
import com.beawan.loanAfter.entity.LfRiskSort;

/**
 * @author yzj
 */
@Repository("lfRiskSortDao")
public class LfRiskSortDaoImpl extends BaseDaoImpl<LfRiskSort> implements LfRiskSortDao{
	
}
