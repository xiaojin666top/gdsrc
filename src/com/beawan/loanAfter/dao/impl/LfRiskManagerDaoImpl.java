package com.beawan.loanAfter.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.loanAfter.dao.LfRiskManagerDao;
import com.beawan.loanAfter.entity.LfRiskManager;
import org.springframework.stereotype.Repository;
import com.beawan.loanAfter.entity.LfRiskManager;

/**
 * @author yzj
 */
@Repository("lfRiskManagerDao")
public class LfRiskManagerDaoImpl extends BaseDaoImpl<LfRiskManager> implements LfRiskManagerDao{

}
