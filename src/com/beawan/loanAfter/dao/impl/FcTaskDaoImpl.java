package com.beawan.loanAfter.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.beawan.core.BaseDaoImpl;
import com.beawan.loanAfter.dao.FcTaskDao;
import com.beawan.loanAfter.dto.CmisCustBaseDto;
import com.beawan.loanAfter.dto.CmisCustChargeDto;
import com.beawan.loanAfter.dto.CmisSpecialAuthDto;
import com.beawan.loanAfter.entity.FcTask;

/**
 * @author yzj
 */
@Repository("fcTaskDao")
public class FcTaskDaoImpl extends BaseDaoImpl<FcTask> implements FcTaskDao{

	@Override
	public CmisCustBaseDto getCustBaseInfo(String xdCustNo) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("select A.CUSTOMERNAME customerName, A.CUSTOMERID customerId,B.FICTITIOUSPERSON fictitiousperson,")
		.append("C.CODE_VALUE_REFE customerType,B.REGISTERCAPITAL registCapi,B.OFFICEADD officeadd,  ")
		.append("B.REGISTERADD registAddr,B.OFFICEADD address,B.LICENSEDATE bsValidTerm,B.LICENSECHECK mot,B.SETUPDATE setupDate ,")
		.append("B.TAXNO taxNo,B.CORPID corpId,B.LICENSENO busiLic,B.LOANCARDNO loancardNo, ")
		.append("D.CODE_VALUE_REFE orgnature,B.LICENSEMATURITY orgValidTerm,B.BASICBANK,B.MOSTBUSINESS mostbusiness ")
		.append("from cmis.customer_info A ")
		.append("join cmis.ent_info B on A.CUSTOMERID=B.CUSTOMERID ")
		.append("join cmis.F_COM_CODE_VALUE C ON A.CUSTOMERTYPE=C.CODE_VAL ")
		.append("JOIN CMIS.F_COM_CODE_VALUE D ON B.ORGNATURE=D.CODE_VAL ")
		.append("where A.CUSTOMERID=:xdCustNo AND C.CODE_NO='ODS0001' ")
		.append("AND D.CODE_NO='GB00015' ");
		Map<String, Object> params = new HashMap();
		params.put("xdCustNo", xdCustNo);
		List<CmisCustBaseDto> list = findCustListBySql(CmisCustBaseDto.class, sql, params);
		if(CollectionUtils.isEmpty(list)){
			return null;
		}
		return list.get(0);
	}

	@Override
	public List<CmisSpecialAuthDto> getSpecialAuth(String xdCustNo) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("select A.CUSTOMERID customerId,A.CERTTYPE certType,A.AUTHDATE authDate, ")
		.append("A.CERTNAME certName,A.CERTID certId,A.CERTRESULT certResult,A.CERTORG certOrg, ")
		.append("A.VALIDDATE validDate,A.TRADENAME tradeName,A.NAME_RENDING nameRending ")
		.append("from cmis.ent_auth A ")
		.append("where A.CUSTOMERID=:xdCustNo order by CERTTYPE ");
		Map<String, Object> params = new HashMap();
		params.put("xdCustNo", xdCustNo);
		return findCustListBySql(CmisSpecialAuthDto.class, sql, params);
	}

	@Override
	public List<CmisCustChargeDto> getChargeList(String xdCustNo) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("select B.CODE_VALUE_REFE ship,A.CUSTOMERNAME customerName,A.CERTID certId,")
		.append("A.RELATIONSHIP relationShip,A.INVESTMENTSUM ,A.INVESTMENTPROP,A.TELEPHONE ")
		.append("from cmis.CUSTOMER_RELATIVE A ")
		.append("join CMIS.F_COM_CODE_VALUE B ON A.RELATIONSHIP=B.CODE_VAL ")
		.append("where CUSTOMERID=:xdCustNo and ( ")
		.append("RELATIONSHIP='0103' or RELATIONSHIP='0100' or RELATIONSHIP like '52%') ")
		.append("AND B.CODE_NO='GB00016' ");
		Map<String, Object> params = new HashMap();
		params.put("xdCustNo", xdCustNo);
		return findCustListBySql(CmisCustChargeDto.class, sql, params);
		
	}

}
