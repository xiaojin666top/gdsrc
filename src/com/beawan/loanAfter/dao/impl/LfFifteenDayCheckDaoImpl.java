package com.beawan.loanAfter.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.loanAfter.dao.LfFifteenDayCheckDao;
import org.springframework.stereotype.Repository;
import com.beawan.loanAfter.entity.LfFifteenDayCheck;

/**
 * @author yzj
 */
@Repository("lfFifteenDayCheckDao")
public class LfFifteenDayCheckDaoImpl extends BaseDaoImpl<LfFifteenDayCheck> implements LfFifteenDayCheckDao{

}
