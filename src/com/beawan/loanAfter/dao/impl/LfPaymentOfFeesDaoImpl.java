package com.beawan.loanAfter.dao.impl;

import com.beawan.core.BaseDaoImpl;
import org.springframework.stereotype.Repository;

import com.beawan.loanAfter.dao.LfPaymentOfFeesDao;
import com.beawan.loanAfter.entity.LfPaymentOfFees;

/**
 * @author yzj
 */
@Repository("lfPaymentOfFeesDao")
public class LfPaymentOfFeesDaoImpl extends BaseDaoImpl<LfPaymentOfFees> implements LfPaymentOfFeesDao{

}
