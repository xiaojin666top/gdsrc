package com.beawan.loanAfter.dao.impl;

import com.beawan.core.BaseDaoImpl;
import org.springframework.stereotype.Repository;

import com.beawan.loanAfter.dao.LfCfdAccountInfoDao;
import com.beawan.loanAfter.entity.LfCfdAccountInfo;

/**
 * @author yzj
 */
@Repository("lfCfdAccountInfoDao")
public class LfCfdAccountInfoDaoImpl extends BaseDaoImpl<LfCfdAccountInfo> implements LfCfdAccountInfoDao{

}
