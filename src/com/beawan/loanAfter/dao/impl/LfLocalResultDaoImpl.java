package com.beawan.loanAfter.dao.impl;

import com.beawan.core.BaseDaoImpl;
import org.springframework.stereotype.Repository;

import com.beawan.loanAfter.dao.LfLocalResultDao;
import com.beawan.loanAfter.entity.LfLocalResult;

/**
 * @author yzj
 */
@Repository("lfLocalResultDao")
public class LfLocalResultDaoImpl extends BaseDaoImpl<LfLocalResult> implements LfLocalResultDao{

}
