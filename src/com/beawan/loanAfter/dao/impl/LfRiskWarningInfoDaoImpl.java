package com.beawan.loanAfter.dao.impl;

import com.beawan.core.BaseDaoImpl;
import org.springframework.stereotype.Repository;

import com.beawan.loanAfter.dao.LfRiskWarningInfoDao;
import com.beawan.loanAfter.entity.LfRiskWarningInfo;

/**
 * @author yzj
 */
@Repository("lfRiskWarningInfoDao")
public class LfRiskWarningInfoDaoImpl extends BaseDaoImpl<LfRiskWarningInfo> implements LfRiskWarningInfoDao{

}
