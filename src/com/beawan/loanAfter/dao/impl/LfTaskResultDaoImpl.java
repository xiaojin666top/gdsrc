package com.beawan.loanAfter.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.loanAfter.dao.LfTaskResultDao;
import org.springframework.stereotype.Repository;
import com.beawan.loanAfter.entity.LfTaskResult;

/**
 * @author yzj
 */
@Repository("lfTaskResultDao")
public class LfTaskResultDaoImpl extends BaseDaoImpl<LfTaskResult> implements LfTaskResultDao{

}
