package com.beawan.loanAfter.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.loanAfter.dao.LfDownCheckInfoDao;
import com.beawan.loanAfter.entity.LfDownCheckInfo;

/**
 * @author yzj
 */
@Repository("lfDownCheckInfoDao")
public class LfDownCheckInfoDaoImpl extends BaseDaoImpl<LfDownCheckInfo> implements LfDownCheckInfoDao{

}
