package com.beawan.loanAfter.dao.impl;

import com.beawan.core.BaseDaoImpl;
import org.springframework.stereotype.Repository;

import com.beawan.loanAfter.dao.LfLoanProjectSummaryDao;
import com.beawan.loanAfter.entity.LfLoanProjectSummary;

/**
 * @author yzj
 */
@Repository("lfLoanProjectSummaryDao")
public class LfLoanProjectSummaryDaoImpl extends BaseDaoImpl<LfLoanProjectSummary> implements LfLoanProjectSummaryDao{

}
