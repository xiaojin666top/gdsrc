package com.beawan.loanAfter.dao;

import com.beawan.core.BaseDao;
import com.beawan.loanAfter.entity.LfProjectInfo;

/**
 * @author yzj
 */
public interface LfProjectInfoDao extends BaseDao<LfProjectInfo> {
}
