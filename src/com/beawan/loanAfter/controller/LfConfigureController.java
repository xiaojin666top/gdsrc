package com.beawan.loanAfter.controller;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.base.dto.UserDto;
import com.beawan.base.entity.User;
import com.beawan.base.service.ISysDicSV;
import com.beawan.base.service.IUserSV;
import com.beawan.common.Constants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.Pagination;
import com.beawan.core.ResultDto;
import com.beawan.loanAfter.dto.LfConstants;
import com.beawan.loanAfter.entity.LfRiskManager;
import com.beawan.loanAfter.entity.LfRiskSort;
import com.beawan.loanAfter.service.LfRiskManagerService;
import com.beawan.loanAfter.service.LfRiskSortService;
import com.platform.util.DateUtil;
import com.platform.util.HttpUtil;

/**
 * @Author: yuzhejia
 * @Date: 13/12/2020
 * @Description: 贷后配置管理控制器
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/loanAfter/config/" })
public class LfConfigureController extends BaseController {
    private static final Logger log = Logger.getLogger(LfConfigureController.class);

    @Resource
    private LfRiskManagerService lfRiskManagerService;
	@Resource
	private IUserSV userSV;
	@Resource
	private LfRiskSortService lfRiskSortService;
	@Resource
	private ISysDicSV sysDicSV;
    
    
    /**
     * 跳转到 设置机构下的专职风险 界面
     * @return
     */
    @RequestMapping("riskManager.do")
	public ModelAndView riskManager() {
		ModelAndView mav = new ModelAndView("/views/loanAfter/config/riskManager");
		return mav;
	}


    /**
     * 保存机构下风险经理    --》系统上没有入口  直接手动新增
     * @param customerNo
     * @param customerName
     * @param page
     * @param rows
     * @return
     */
	@RequestMapping("saveRiskManager.json")
	@ResponseBody
	public ResultDto saveRiskManager() {
		ResultDto resultDto = returnFail();
		try {
//			孟建- F060401
			LfRiskManager m1 = new LfRiskManager("320622011", "江苏如皋农村商业银行白蒲支行", "白蒲支行", "F060401", "孟建");
			LfRiskManager m2 = new LfRiskManager("320622008", "江苏如皋农村商业银行林梓支行", "林梓支行", "F060401", "孟建");
			LfRiskManager m3 = new LfRiskManager("320622012", "江苏如皋农村商业银行奚斜支行", "奚斜支行", "F060401", "孟建");
			LfRiskManager m4 = new LfRiskManager("320622009", "江苏如皋农村商业银行新姚支行", "新姚支行", "F060401", "孟建");
			LfRiskManager m5 = new LfRiskManager("320622999", "江苏如皋农村商业银行勇敢支行", "勇敢支行", "F060401", "孟建");
			LfRiskManager m6 = new LfRiskManager("320622013", "江苏如皋农村商业银行花园支行", "花园支行", "F060401", "孟建");
			LfRiskManager m7 = new LfRiskManager("320622014", "江苏如皋农村商业银行下原支行", "下原支行", "F060401", "孟建");
			LfRiskManager m8 = new LfRiskManager("320622045", "江苏如皋农村商业银行马塘支行", "马塘支行", "F060401", "孟建");
//			陈丽- F060816
			LfRiskManager c1 = new LfRiskManager("320622047", "江苏如皋农村商业银行场北支行", "场北支行", "F060816", "陈丽");
			LfRiskManager c2 = new LfRiskManager("320622048", "江苏如皋农村商业银行磨头支行", "磨头支行", "F060816", "陈丽");
			LfRiskManager c3 = new LfRiskManager("320622025", "江苏如皋农村商业银行吴窑支行", "吴窑支行", "F060816", "陈丽");
			LfRiskManager c4 = new LfRiskManager("320622028", "江苏如皋农村商业银行长庄支行", "长庄支行", "F060816", "陈丽");
			LfRiskManager c5 = new LfRiskManager("320622050", "江苏如皋农村商业银行大明支行", "大明支行", "F060816", "陈丽");
//			范小平- F061315
			LfRiskManager f1 = new LfRiskManager("320622034", "江苏如皋农村商业银行搬经支行", "搬经支行", "F061315", "范小平");
			LfRiskManager f2 = new LfRiskManager("320622033", "江苏如皋农村商业银行常青支行", "常青支行", "F061315", "范小平");
			LfRiskManager f3 = new LfRiskManager("320622032", "江苏如皋农村商业银行高明支行", "高明支行", "F061315", "范小平");
			LfRiskManager f4 = new LfRiskManager("320622035", "江苏如皋农村商业银行加力支行", "加力支行", "F061315", "范小平");
			LfRiskManager f5 = new LfRiskManager("320622036", "江苏如皋农村商业银行夏堡支行", "夏堡支行", "F061315", "范小平");
			LfRiskManager f6 = new LfRiskManager("320622039", "江苏如皋农村商业银行何庄支行", "何庄支行", "F061315", "范小平");
			LfRiskManager f7 = new LfRiskManager("320622043", "江苏如皋农村商业银行中山路支行", "中山路支行", "F061315", "范小平");
//			许华- F060621
			LfRiskManager x1 = new LfRiskManager("320622040", "江苏如皋农村商业银行袁桥支行", "袁桥支行", "F060621", "许华");
			LfRiskManager x2 = new LfRiskManager("320622038", "江苏如皋农村商业银行柴东支行", "柴东支行", "F060621", "许华");
			LfRiskManager x3 = new LfRiskManager("320622041", "江苏如皋农村商业银行戴庄支行", "戴庄支行", "F060621", "许华");
			LfRiskManager x4 = new LfRiskManager("320622062", "江苏如皋农村商业银行环北支行", "环北支行", "F060621", "许华");
			LfRiskManager x5 = new LfRiskManager("320622042", "江苏如皋农村商业银行开发区支行", "开发区支行", "F060621", "许华");
			LfRiskManager x6 = new LfRiskManager("320622052", "江苏如皋农村商业银行兴隆支行", "兴隆支行", "F060621", "许华");
//			臧明- F061301
			LfRiskManager z1 = new LfRiskManager("320622017", "江苏如皋农村商业银行九华支行", "九华支行", "F061301", "臧明");
			LfRiskManager z2 = new LfRiskManager("320622015", "江苏如皋农村商业银行龙舌支行", "龙舌支行", "F061301", "臧明");
			LfRiskManager z3 = new LfRiskManager("320622023", "江苏如皋农村商业银行郭园支行", "郭园支行", "F061301", "臧明");
			LfRiskManager z4 = new LfRiskManager("320622018", "江苏如皋农村商业银行江防支行", "江防支行", "F061301", "臧明");
			LfRiskManager z5 = new LfRiskManager("320622020", "江苏如皋农村商业银行如皋港支行", "如皋港支行", "F061301", "臧明");
			LfRiskManager z6 = new LfRiskManager("320622019", "江苏如皋农村商业银行长江支行", "长江支行", "F061301", "臧明");
//			丛尤华- F061832
			LfRiskManager y1 = new LfRiskManager("320622029", "江苏如皋农村商业银行高井支行", "高井支行", "F061832", "丛尤华");
			LfRiskManager y2 = new LfRiskManager("320622024", "江苏如皋农村商业银行石北支行", "石北支行", "F061832", "丛尤华");
			LfRiskManager y3 = new LfRiskManager("320622022", "江苏如皋农村商业银行石庄支行", "石庄支行", "F061832", "丛尤华");
			LfRiskManager y4 = new LfRiskManager("320622026", "江苏如皋农村商业银行葛市支行", "葛市支行", "F061832", "丛尤华");
			LfRiskManager y5 = new LfRiskManager("320622030", "江苏如皋农村商业银行黄市支行", "黄市支行", "F061832", "丛尤华");
			LfRiskManager y6 = new LfRiskManager("320622027", "江苏如皋农村商业银行江安支行", "江安支行", "F061832", "丛尤华");
			LfRiskManager y7 = new LfRiskManager("320622031", "江苏如皋农村商业银行胜利支行", "胜利支行", "F061832", "丛尤华");
//			陈莉萍- F063033
			LfRiskManager l1 = new LfRiskManager("320622006", "江苏如皋农村商业银行丁西支行", "丁堰支行", "F063033", "陈莉萍");
			LfRiskManager l2 = new LfRiskManager("320622004", "江苏如皋农村商业银行丁北支行", "丁北支行", "F063033", "陈莉萍");
			LfRiskManager l3 = new LfRiskManager("320622003", "江苏如皋农村商业银行东陈支行", "东陈支行", "F063033", "陈莉萍");
			LfRiskManager l4 = new LfRiskManager("320622001", "江苏如皋农村商业银行南凌支行", "南凌支行", "F063033", "陈莉萍");
			LfRiskManager l5 = new LfRiskManager("320622002", "江苏如皋农村商业银行雪岸支行", "雪岸支行", "F063033", "陈莉萍");
			LfRiskManager l6 = new LfRiskManager("320622064", "江苏如皋农村商业银行高新支行", "高新支行", "F063033", "陈莉萍");
			LfRiskManager l7 = new LfRiskManager("320622053", "江苏如皋农村商业银行软件园支行", "软件园支行", "F063033", "陈莉萍");
			LfRiskManager l8 = new LfRiskManager("320622049", "江苏如皋农村商业银行桃园支行", "桃园支行", "F063033", "陈莉萍");
//			高翠红- F062016
			LfRiskManager g1 = new LfRiskManager("320622037", "江苏如皋农村商业银行城东支行", "城东支行", "F062016", "高翠红");
			LfRiskManager g2 = new LfRiskManager("320622068", "江苏如皋农村商业银行股份有限公司海阳支行", "海阳支行", "F062016", "高翠红");
			LfRiskManager g3 = new LfRiskManager("320622044", "江苏如皋农村商业银行如城支行", "如城支行", "F062016", "高翠红");
			LfRiskManager g4 = new LfRiskManager("320622055", "江苏如皋农村商业银行城南支行", "城南支行", "F062016", "高翠红");
//			徐丽华- F060326
			LfRiskManager h1 = new LfRiskManager("320622912", "江苏如皋农村商业银行公司业务部", "公司业务部", "F060326", "徐丽华");
			LfRiskManager h2 = new LfRiskManager("320622065", "江苏如皋农村商业银行白蒲支行", "中如建筑支行", "F060326", "徐丽华");

			lfRiskManagerService.saveOrUpdate(m1);
			lfRiskManagerService.saveOrUpdate(m2);
			lfRiskManagerService.saveOrUpdate(m3);
			lfRiskManagerService.saveOrUpdate(m4);
			lfRiskManagerService.saveOrUpdate(m5);
			lfRiskManagerService.saveOrUpdate(m6);
			lfRiskManagerService.saveOrUpdate(m7);
			lfRiskManagerService.saveOrUpdate(m8);
			lfRiskManagerService.saveOrUpdate(c1);
			lfRiskManagerService.saveOrUpdate(c2);
			lfRiskManagerService.saveOrUpdate(c3);
			lfRiskManagerService.saveOrUpdate(c4);
			lfRiskManagerService.saveOrUpdate(c5);
			lfRiskManagerService.saveOrUpdate(f1);
			lfRiskManagerService.saveOrUpdate(f2);
			lfRiskManagerService.saveOrUpdate(f3);
			lfRiskManagerService.saveOrUpdate(f4);
			lfRiskManagerService.saveOrUpdate(f5);
			lfRiskManagerService.saveOrUpdate(f6);
			lfRiskManagerService.saveOrUpdate(f7);
			lfRiskManagerService.saveOrUpdate(x1);
			lfRiskManagerService.saveOrUpdate(x2);
			lfRiskManagerService.saveOrUpdate(x3);
			lfRiskManagerService.saveOrUpdate(x4);
			lfRiskManagerService.saveOrUpdate(x5);
			lfRiskManagerService.saveOrUpdate(x6);
			lfRiskManagerService.saveOrUpdate(z1);
			lfRiskManagerService.saveOrUpdate(z2);
			lfRiskManagerService.saveOrUpdate(z3);
			lfRiskManagerService.saveOrUpdate(z4);
			lfRiskManagerService.saveOrUpdate(z5);
			lfRiskManagerService.saveOrUpdate(z6);
			lfRiskManagerService.saveOrUpdate(y1);
			lfRiskManagerService.saveOrUpdate(y2);
			lfRiskManagerService.saveOrUpdate(y3);
			lfRiskManagerService.saveOrUpdate(y4);
			lfRiskManagerService.saveOrUpdate(y5);
			lfRiskManagerService.saveOrUpdate(y6);
			lfRiskManagerService.saveOrUpdate(y7);
			lfRiskManagerService.saveOrUpdate(l1);
			lfRiskManagerService.saveOrUpdate(l2);
			lfRiskManagerService.saveOrUpdate(l3);
			lfRiskManagerService.saveOrUpdate(l4);
			lfRiskManagerService.saveOrUpdate(l5);
			lfRiskManagerService.saveOrUpdate(l6);
			lfRiskManagerService.saveOrUpdate(l7);
			lfRiskManagerService.saveOrUpdate(l8);
			lfRiskManagerService.saveOrUpdate(g1);
			lfRiskManagerService.saveOrUpdate(g2);
			lfRiskManagerService.saveOrUpdate(g3);
			lfRiskManagerService.saveOrUpdate(g4);
			lfRiskManagerService.saveOrUpdate(h1);
			lfRiskManagerService.saveOrUpdate(h2);
			resultDto = returnSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("保存机构下指定风险经理失败！", e);
			resultDto.setMsg("保存机构下指定风险经理失败");
		}
		return resultDto;
	}


	/**
	 * 分页获取  机构下指定的风险经理
	 * @param userNo
	 * @param userName
	 * @param orgName
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping("getRiskManagerList.json")
	@ResponseBody
	public ResultDto getRiskManagerList(String userNo, String userName, String orgName, int page, int rows) {
		ResultDto re = returnFail();
		try {
			Pagination<LfRiskManager> pager = lfRiskManagerService.getRiskManagerPager(userNo, userName, orgName, page, rows);
			re = returnSuccess();
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取机构下指定的风险经理失败！", e);
			re.setMsg("获取机构下指定的风险经理失败");
		}
		return re;
	}
	
	@RequestMapping("selectUser.do")
	public ModelAndView selectUser(Integer id) {
		ModelAndView mav =  new ModelAndView("views/loanAfter/config/query_user");
		mav.addObject("id", id);
		return mav;
	}
	
	@RequestMapping("choseUser.do")
	public ModelAndView choseUser(Integer id) {
		ModelAndView mav =  new ModelAndView("views/loanAfter/config/chose_user");
		mav.addObject("id", id);
		return mav;
	}
	
	/**
	 * 获取专职风险经理列表
	 * @param page
	 * @param rows
	 * @param userName
	 * @param userNo
	 * @return
	 */
	@RequestMapping("queryRiskUserList.json")
	@ResponseBody
	public ResultDto queryRiskUserList( @RequestParam(value="page",defaultValue="1")int page,
									  @RequestParam(value="rows",defaultValue="10")int rows,
									  String userName, String userNo) {
		ResultDto re = returnFail();
		try {
			Pagination<UserDto> pager = userSV.queryUserPaging(Constants.ROLE.LF_RISK_USER, userName, userNo, page, rows);
			re = returnSuccess();
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 指定机构下在风险经理
	 * @param customerNo
	 * @param customerName
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping("appointRiskManager.json")
	@ResponseBody
	public ResultDto appointRiskManager(Integer id, String userNo, String userName) {
		ResultDto resultDto = returnFail();
		if(id==null){
			resultDto.setMsg("数据信息异常，请重试");
			return resultDto;
		}
		try {
			LfRiskManager manager = lfRiskManagerService.findByPrimaryKey(id);
			manager.setRiskuser(userNo);
			manager.setRiskusername(userName);
			lfRiskManagerService.saveOrUpdate(manager);
			
			resultDto = returnSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("指定机构下在风险经理失败！", e);
			resultDto.setMsg("指定机构下在风险经理失败");
		}
		return resultDto;
	}
   
	
	@RequestMapping("aaa.json")
	@ResponseBody
	public ResultDto aaa(String id, String userNo, String userName) {
		ResultDto resultDto = returnFail();
		try {
			resultDto = returnSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取待调查贷后任务失败！", e);
			resultDto.setMsg("获取待调查贷后任务失败");
		}
		return resultDto;
	}
	
	
	

	/**
	 * 跳转到 分类别 客户风险分类列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("lfRiskDetail.do")
	public String lfRiskDetail(HttpServletRequest request, HttpServletResponse response) {
		return "views/loanAfter/lfRiskDetail";
	}
	
	
	/**
	 * 跳转到 分类别 客户风险分类列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("lfCreditDetail.do")
	public String lfCreditDetail(HttpServletRequest request, HttpServletResponse response) {
		return "views/loanAfter/lfCreditDetail";
	}

	/**
	 * 跳转到 分类别 客户风险分类列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("lfReviewPoint.do")
	public ModelAndView lfReviewPoint(HttpServletRequest request, HttpServletResponse response, String sort) {
		ModelAndView mav = new ModelAndView("views/loanAfter/lfReviewPoint");
		mav.addObject("sort", sort);
		return mav;
	}
	
	/**
	 * 跳转到 分类别 客户授信跟踪管理列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("lfCreditPoint.do")
	public ModelAndView lfCreditPoint(HttpServletRequest request, HttpServletResponse response, String sort) {
		ModelAndView mav = new ModelAndView("views/loanAfter/lfCreditPoint");
		mav.addObject("sort", sort);
		return mav;
	}
	

	/** 获取 客户风险分类内容列表 **/
	@RequestMapping("getLfReviewPoint.json")
	@ResponseBody
	public ResultDto getReviewPoint(HttpServletRequest request, HttpServletResponse response,String sort,
			   @RequestParam(value="page",defaultValue="1")int page,
			   @RequestParam(value="rows",defaultValue="10")int rows) {
		ResultDto resultDto = returnFail("获取审查内容列表失败");
		
		try {
			Pagination<LfRiskSort> pager = lfRiskSortService.queryPageList(sort,page,rows);
			resultDto = returnSuccess();
			resultDto.setRows(pager.getItems());
			resultDto.setTotal(pager.getRowsCount());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultDto;
	}
	
	/**
	 * 跳转到 新增客户风险分类内容或授信管理分类
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("addLfReviewPoint.do")
	public ModelAndView addReviewPoint(HttpServletRequest request, HttpServletResponse response,
									   Integer id,String sort) {
			ModelAndView mav = new ModelAndView();
		if(sort.equals("01") || sort.equals("02") || sort.equals("03") || sort.equals("04") || sort.equals("05")){
			mav = new ModelAndView("views/loanAfter/addLfReviewPoint");//风险分类标准
		}else{
			mav = new ModelAndView("views/loanAfter/addLfCreditPoint");//授信管理分类
		}
		if(id!=null && !"".equals(id) && id!=0){
			LfRiskSort lfRiskSort = lfRiskSortService.findByPrimaryKey(id);
			if(lfRiskSort==null){
				log.error("修改核查项内容数据获取异常！");
			}
			mav.addObject("id", lfRiskSort.getId());
			mav.addObject("riskContent", lfRiskSort.getRiskContent());
			mav.addObject("riskPoint", lfRiskSort.getRiskPoint());

		}

		String sortName = "";
		if(Constants.LF_RISK_CLASSIFY.NORMAL.equals(sort)){
			sortName = "正常类";
		}else if(Constants.LF_RISK_CLASSIFY.ATTENTION.equals(sort)){
			sortName = "关注类";
		}else if(Constants.LF_RISK_CLASSIFY.RISK_ONE.equals(sort)){
			sortName = "风险一类";
		}else if(Constants.LF_RISK_CLASSIFY.RISK_TWO.equals(sort)){
			sortName = "风险二类";
		}else if(Constants.LF_RISK_CLASSIFY.RISK_THREE.equals(sort)){
			sortName = "风险三类";
		}else if(Constants.LF_RISK_CLASSIFY.INCREASE_CREDIT.equals(sort)){
			sortName = "增加授信";
		}else if(Constants.LF_RISK_CLASSIFY.KEEP_CREDIT.equals(sort)){
			sortName = "维持授信";
		}else if(Constants.LF_RISK_CLASSIFY.REDUCE_CREDIT.equals(sort)){
			sortName = "减少授信";
		}else if(Constants.LF_RISK_CLASSIFY.QUIT_CREDIT.equals(sort)){
			sortName = "退出授信";
		}
		mav.addObject("sort", sort);
		mav.addObject("sortName", sortName);
		return mav;
	}
	
	/**
	 * 保存客户风险分类标准要点
	 * @return
	 */
	@RequestMapping(value = "saveLfReviewPoint.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto saveLfReviewPoint(LfRiskSort lfRiskSort) {
		ResultDto re = returnFail("保存失败");
		if(lfRiskSort==null){
			re.setMsg("传输数据异常！请重试");
			return re;
		}
		User user = HttpUtil.getCurrentUser();
		try {
			Integer id = lfRiskSort.getId();
			if(id != null){
				lfRiskSort.setUpdater(user.getUserId());
				lfRiskSort.setUpdateTime(DateUtil.getNowTimestamp());
			}else{
				lfRiskSort.setCreater(user.getUserId());
				lfRiskSort.setCreateTime(DateUtil.getNowTimestamp());
				lfRiskSort.setUpdater(user.getUserId());
				lfRiskSort.setUpdateTime(DateUtil.getNowTimestamp());
			}
			lfRiskSort.setRiskContent(URLDecoder.decode(URLDecoder.decode(lfRiskSort.getRiskContent(), "UTF-8")));
			lfRiskSort.setRiskPoint(URLDecoder.decode(URLDecoder.decode(lfRiskSort.getRiskPoint(), "UTF-8")));
			lfRiskSortService.saveOrUpdate(lfRiskSort);
			
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}



	/** 删除客户风险分类标准内容 **/
	@RequestMapping("deleteLfReviewPoint.json")
	@ResponseBody
	public ResultDto deleteLfReviewPoint(Integer id) {
		ResultDto re = returnFail("获取客户风险分类标准内容列表失败");
		if(id==null || "".equals(id)){
			re.setMsg("参数传输为空，请重试");
			return re;
		}
		try {
			LfRiskSort lfRiskSort = lfRiskSortService.findByPrimaryKey(id);
			if(lfRiskSort==null){
				re.setMsg("删除数据异常！请重试");
				log.error("删除客户风险分类标准内容数据失败！通过主键无法获取数据！");
				return re;
			}
			lfRiskSort.setStatus(Integer.parseInt(Constants.STATE_DELETE));
			lfRiskSortService.saveOrUpdate(lfRiskSort);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 根据风险分类 获取分类标准信息
	 * @param no
	 * @return
	 */
	@RequestMapping("getRiskSortByNo.json")
	@ResponseBody
	public ResultDto getRiskSortByNo(String no) {
		ResultDto re = returnFail("获取客户风险分类标准内容列表失败");
	
		if(no==null || "".equals(no)){
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			return re;
		}
		//通过分类名称获取分类序号
		//no = LfConstants.getLfriskNametoriskNo(no);
		try {
			List<LfRiskSort> lfRiskSort = lfRiskSortService.selectByProperty("riskNo", no);
			if(no.equals(Constants.LF_RISK_CLASSIFY.ATTENTION)){
				if(!CollectionUtils.isEmpty(lfRiskSort)){
					Map<String, List<LfRiskSort>> map = lfRiskSort.stream().collect(Collectors.groupingBy(LfRiskSort::getRiskContent));
					re.setRows(map);
					re.setCode(ResultDto.RESULT_CODE_SUCCESS);
					return re;
				}
			}else if(no.equals(Constants.LF_RISK_CLASSIFY.REDUCE_CREDIT)){
				if(!CollectionUtils.isEmpty(lfRiskSort)){
					Map<String, List<LfRiskSort>> map = lfRiskSort.stream().collect(Collectors.groupingBy(LfRiskSort::getRiskContent));
					re.setRows(map);
					re.setCode(ResultDto.RESULT_CODE_SUCCESS);
					return re;
				}
			}
			re.setMsg("获取客户风险分类标准列表成功");
			re.setRows(lfRiskSort);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
}
