package com.beawan.loanAfter.controller;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.analysis.finansis.utils.CodeUtil;
import com.beawan.base.entity.User;
import com.beawan.base.service.ISysDicSV;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.common.config.AppConfig;
import com.beawan.core.BaseController;
import com.beawan.core.Pagination;
import com.beawan.core.ResultDto;
import com.beawan.corporateloan.dto.LcTaskRiskDto;
import com.beawan.corporateloan.entity.LcTask;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.dtmp.dto.CmisBillDto;
import com.beawan.dtmp.dto.CmisContractDto;
import com.beawan.dtmp.dto.FmContractDto;
import com.beawan.dtmp.service.DtmpService;
import com.beawan.loanAfter.dto.CmisCustBaseDto;
import com.beawan.loanAfter.dto.CmisCustChargeDto;
import com.beawan.loanAfter.dto.CmisCustStockDto;
import com.beawan.loanAfter.dto.CmisLargeAffirmDto;
import com.beawan.loanAfter.dto.CmisSmallAffirmDto;
import com.beawan.loanAfter.dto.CmisWorkPaperDto;
import com.beawan.loanAfter.dto.CmisSpecialAuthDto;
import com.beawan.loanAfter.dto.FcAnalysisDto;
import com.beawan.loanAfter.dto.FcCustDto;
import com.beawan.loanAfter.dto.LfCustDto;
import com.beawan.loanAfter.entity.FcBusiContract;
import com.beawan.loanAfter.entity.FcCust;
import com.beawan.loanAfter.entity.FcCustFullInfo;
import com.beawan.loanAfter.entity.FcFinaData;
import com.beawan.loanAfter.entity.FcTask;
import com.beawan.loanAfter.service.FcBusiContractService;
import com.beawan.loanAfter.service.FcCustFullInfoService;
import com.beawan.loanAfter.service.FcCustService;
import com.beawan.loanAfter.service.FcFinaDataService;
import com.beawan.loanAfter.service.FcTaskService;
import com.beawan.survey.custInfo.bean.CompBaseEquity;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.platform.util.DateUtil;
import com.platform.util.FreeMarkerWordUtil;
import com.platform.util.HttpUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.RgnshUtils;
import com.platform.util.ZipCompressUtil;

/**
 * @Author: yuzhejia
 * @Date: 26/10/2020
 * @Description: 贷后--五级分类控制器
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/fiveClass/" })
public class FiveClassController extends BaseController {
    private static final Logger log = Logger.getLogger(FiveClassController.class);

    @Resource
    private FcTaskService fcTaskService;
    @Resource
    private FcCustService fcCustService;
    @Resource
    private FcCustFullInfoService fcCustFullInfoService;
    @Resource
	private ISysDicSV sysDicSV;
    @Resource
	private FcBusiContractService fcBusiContractService;
    @Resource
	private FcFinaDataService fcFinaDataService;
	@Resource
	private ICompBaseSV compBaseSV;
	@Resource
	private ICusBaseSV cusBaseSV;
	@Resource
	private DtmpService dtmpService;
	
	
    /**
     * 页面跳转 -->待处理五级分类任务列表
     * @return
     */
    @RequestMapping("pendingFcTask.do")
	public ModelAndView pendingFcTask() {
    	ModelAndView mav = new ModelAndView("/views/loanAfter/fiveClass/pendingFcTask");
    	return mav;
    }
    
    /**
     * 页面跳转 -->待处理五级分类任务列表
     * @return
     */
    @RequestMapping("FinishedFcTask.do")
	public ModelAndView pendedFcTask() {
    	ModelAndView mav = new ModelAndView("/views/loanAfter/fiveClass/FinishedFcTask");
    	return mav;
    }
    
  


    /**
     * 页面跳转 -->待处理五级分类任务详情  客户列表
     * @return
     */
    @RequestMapping("fcCustList.do")
	public ModelAndView fcCustList(Integer taskId) {
    	ModelAndView mav = new ModelAndView("/views/loanAfter/fiveClass/FcCustList");
    	FcTask task = fcTaskService.findByPrimaryKey(taskId);
    	mav.addObject("task", task);
    	return mav;
    }

    /**
     * 页面跳转 -->五级分类报告界面
     * @return
     */
    @RequestMapping("classfiyReportIndex.do")
	public ModelAndView classfiyReportIndex(String taskCustId) {
    	ModelAndView mav = new ModelAndView("/views/loanAfter/fiveClass/classify_report_index");
    	mav.addObject("taskCustId", taskCustId);
    	return mav;
    }
    
    
    
    //=========================获取数据 .json==========================

    /**
     * 
     *@Description 获取待处理五级分类任务列表
     *@param page
     *@param rows
     *@return
     * @author yuzhejia
     */
	@RequestMapping("getPendingFcTaskList.json")
	@ResponseBody
	public ResultDto getPendingFcTaskList(String taskName, String createrName, int page, int rows) {
		ResultDto resultDto = returnFail();
		try {
			Pagination<FcTask> pager = fcTaskService.getFcTaskList(
					Constants.LFC_TASK_POSITION.PENDING_TASK, taskName, createrName, page, rows);
			
			resultDto = returnSuccess();
			resultDto.setRows(pager.getItems());
			resultDto.setTotal(pager.getRowsCount());
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取贷后五级分类任务失败！", e);
			resultDto.setMsg("获取贷后五级分类任务失败");
		}
		return resultDto;
	}
	
	/**
     * 
     *@Description 获取待处理五级分类任务列表
     *@param page
     *@param rows
     *@return
     * @author yuzhejia
     */
	@RequestMapping("getFinishedFcTaskList.json")
	@ResponseBody
	public ResultDto getFinishedFcTaskList(String taskName, String createrName, int page, int rows) {
		ResultDto resultDto = returnFail();
		try {
			Pagination<FcTask> pager = fcTaskService.getFcTaskList(
					Constants.LFC_TASK_POSITION.FINISHED_TASK, taskName, createrName, page, rows);
			
			resultDto = returnSuccess();
			resultDto.setRows(pager.getItems());
			resultDto.setTotal(pager.getRowsCount());
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取贷后五级分类任务失败！", e);
			resultDto.setMsg("获取贷后五级分类任务失败");
		}
		return resultDto;
	}
	
	/**
	 * 手动触发五级分类任务 --》季度
	 * @param taskName
	 * @return
	 */
	@RequestMapping("createLfcTask.json")
	@ResponseBody
	public ResultDto createLfcTask() {
		try {
			int year = new Date().getYear();
			int month = new Date().getMonth();
			String taskName = year+"年";
			if(month >=0 && month <=3){
				taskName += "第一季度五级分类任务";
			}else if(month <=6){
				taskName += "第二季度五级分类任务";
			}else if(month <=9){
				taskName += "第三季度五级分类任务";
			}else if(month <=12){
				taskName += "第四季度五级分类任务";
			}
			fcTaskService.batchFcTask("2020年第四季度五级分类任务", "DGZJ", "TIMER");
			
			return returnSuccess();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return returnFail();
		
	}

	/**
	 * 保存五级分类任务
	 * @param taskName
	 * @return
	 */
	@RequestMapping("saveFcTask.json")
	@ResponseBody
	public ResultDto saveFcTask(String taskName) {
		ResultDto re = returnFail();
		if(StringUtils.isEmpty(taskName)){
			re.setMsg("任务名称为空，请输入任务名后重试");
			return re;
		}
		User user = HttpUtil.getCurrentUser();
		try {
			List<FcTask> existList = fcTaskService.selectByProperty("taskName", taskName);
			if(!CollectionUtils.isEmpty(existList)){
				re.setMsg("任务名称已存在，请修改后重试");
				return re;
			}
			fcTaskService.batchFcTask(taskName, user.getUserId(), user.getUserName());
			
			re = returnSuccess();
			re.setMsg("创建五级分类任务成功");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("保存五级分类任务失败！", e);
			re.setMsg("保存五级分类任务失败");
		}
		return re;
	}
	
	/**
	 * 保存五级分类任务
	 * @param taskId
	 * @return
	 */
	@RequestMapping("submitLfcTask.json")
	@ResponseBody
	public ResultDto submitLfcTask(HttpServletRequest request, Integer id){
		ResultDto re = returnFail();
		if(RgnshUtils.isEmptyInteger(id)){
			re.setMsg("任务信息异常，请重试");
			return re;
		}
		User currentUser = HttpUtil.getCurrentUser(request);
		try{
			FcTask task = fcTaskService.findByPrimaryKey(id);
			if(task==null){
				re.setMsg("当前任务不存在，请重试");
				log.error("当前任务不存在，请重试，任务id为：" + id);
				return re;
			}
			System.out.println("currentUser.getUserId()="+currentUser.getUserId());
			//提交到
			task.setTaskPosition(Constants.LFC_TASK_POSITION.FINISHED_TASK);
			task.setUpdater(currentUser.getUserId());
			task.setUpdateTime(DateUtil.getNowTimestamp());
			fcTaskService.saveOrUpdate(task);
			re = returnSuccess();
			re.setMsg("五级分类审查任务提交");
		}catch (Exception e){
			e.printStackTrace();
		}
		return re;
	}
	
	
	/**
	 * 分页获取五级分类详情
	 * @param taskId
	 * @param custName
	 * @param userName
	 * @param page
	 * @param pageSize
	 * @return
	 */
	@RequestMapping("getFcTaskCustList.json")
	@ResponseBody
	public ResultDto getFcTaskCustList(Integer taskId, String custName, String userName,
			@RequestParam(value="page", defaultValue = "1") int page,
			@RequestParam(value="rows", defaultValue = "10") int pageSize) {
		ResultDto re = returnFail();
		if(Objects.isNull(taskId)){
			re.setMsg("任务名称为空，请输入任务名后重试");
			return re;
		}
		try {
			Pagination<LfCustDto> pager = fcCustService.getFcCustList(taskId, custName, userName, page, pageSize);
			re = returnSuccess();
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());
			re.setMsg("获取五级分类任务详情成功");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取五级分类任务详情失败！", e);
			re.setMsg("获取五级分类任务详情失败");
		}
		return re;
	}
	
	
	//-------------------------五级分类报告
    /**
     * 跳转 客户基本情况
     * @param taskId
     * @param customerNo
     * @return
     */
    @RequestMapping("fcCustInfo.do")
  	public ModelAndView fcCustInfo(Integer taskCustId) {
      	ModelAndView mav = new ModelAndView("/views/loanAfter/fiveClass/fcCustInfo");
  		mav.addObject("taskCustId", taskCustId);
		try {
			FcCustDto result = fcCustFullInfoService.getByTaskCustId(taskCustId);
	      	mav.addObject("compBase", result);
	      	//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.STD_SY_GB00015};
			mav.addObject("dicData", sysDicSV.queryMapListByTypes(optTypes));
		} catch (Exception e) {
			e.printStackTrace();
		}
      	return mav;
    }
	
    /**
     * 跳转 股权结构情况
     * @param taskCustId
     * @return
     */
    @RequestMapping("fcCustEquity.do")
	public ModelAndView fcCustEquity(Integer taskCustId) {
    	ModelAndView mav = new ModelAndView("/views/loanAfter/fiveClass/fcCustEquity");
    	FcCust fcCust = fcCustService.selectByPrimaryKey(taskCustId);
  		mav.addObject("taskCustId", taskCustId);
  		mav.addObject("customerNo", fcCust.getCustomerNo());
    	return mav;
    }
    
    /**
     * 跳转 企业目前贷款情况
     * @param taskCustId
     * @return
     */
    @RequestMapping("fcLoanInfo.do")
	public ModelAndView fcLoanInfo(Integer taskCustId) {
    	ModelAndView mav = new ModelAndView("/views/loanAfter/fiveClass/fcLoanInfo");
  		mav.addObject("taskCustId", taskCustId);
    	try {
      		mav.addObject("loanAnaly", fcCustFullInfoService.getLoanAnaly(taskCustId));
	      	mav.addObject("taskCustId", taskCustId);
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return mav;
    }
    
    /**
     * 跳转 财务比率分析
     * @param taskCustId
     * @return
     */
    @RequestMapping("fcFinaRate.do")
	public ModelAndView fcFinaRate(Integer taskCustId) {
    	ModelAndView mav = new ModelAndView("/views/loanAfter/fiveClass/fcFinaRate");
  		mav.addObject("taskCustId", taskCustId);
    	try {
    		FcAnalysisDto result = fcCustFullInfoService.getFcAnalysis(taskCustId);
	      	if(result != null){
	      		mav.addObject("profitAnaly", result.getProfitAnaly());
	      		mav.addObject("operateAnaly", result.getOperateAnaly());
	      		mav.addObject("debtAnaly", result.getDebtAnaly());
	      	}
	      	FcCustFullInfo fcCustFullInfo =fcCustFullInfoService.selectSingleByProperty("taskCustId", taskCustId);
	      	String finaDate = fcCustFullInfo.getFinaDate();
	      	if(StringUtils.isEmpty(finaDate)){
	      		mav.addObject("nowYear", "当期值");
				mav.addObject("oneYear", "上年末");
				mav.addObject("twoYear", "上上年末");
	      	}else{
		      	int currYear = Integer.parseInt(finaDate.substring(0, 4));
		      	String nowYear = currYear + "年" + finaDate.substring(4, 6)+"月";
				String oneYear = (currYear-1) + "年12月";//上年末
				String twoYear = (currYear-2) + "年12月";//上上年末
				mav.addObject("nowYear", nowYear);
				mav.addObject("oneYear", oneYear);
				mav.addObject("twoYear", twoYear);
	      	}
			mav.addObject("finaAnaly", fcCustFullInfoService.getFinaAnaly(taskCustId));
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return mav;
    }
    
    /**
     * 跳转 现金流分析
     * @param taskCustId
     * @return
     */
    @RequestMapping("fcCashFlow.do")
	public ModelAndView fcCashFlow(Integer taskCustId) {
    	ModelAndView mav = new ModelAndView("/views/loanAfter/fiveClass/fcCashFlow");
  		mav.addObject("taskCustId", taskCustId);
    	try {
    		FcAnalysisDto result = fcCustFullInfoService.getFcAnalysis(taskCustId);
	      	if(result != null){
	      		mav.addObject("cashflowAnaly", result.getCashflowAnaly());
	      	}
	      	FcCustFullInfo fcCustFullInfo =fcCustFullInfoService.selectSingleByProperty("taskCustId", taskCustId);
	      	String finaDate = fcCustFullInfo.getFinaDate();
	      	if(StringUtils.isEmpty(finaDate)){
	      		mav.addObject("nowYear", "当期值");
				mav.addObject("oneYear", "上年末");
				mav.addObject("twoYear", "上上年末");
	      	}else{
		      	int currYear = Integer.parseInt(finaDate.substring(0, 4));
		      	String nowYear = currYear + "年" + finaDate.substring(4, 6)+"月";
				String oneYear = (currYear-1) + "年12月";//上年末
				String twoYear = (currYear-2) + "年12月";//上上年末
				mav.addObject("nowYear", nowYear);
				mav.addObject("oneYear", oneYear);
				mav.addObject("twoYear", twoYear);
	      	}
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return mav;
    }
    
    /**
     * 跳转 风险分析
     * @param taskCustId
     * @return
     */
    @RequestMapping("fcRisk.do")
	public ModelAndView fcRisk(Integer taskCustId) {
    	ModelAndView mav = new ModelAndView("/views/loanAfter/fiveClass/fcRisk");
  		mav.addObject("taskCustId", taskCustId);
    	try {
    		FcAnalysisDto result = fcCustFullInfoService.getFcAnalysis(taskCustId);
	      	if(result != null){
	      		mav.addObject("induRisk", result.getInduRisk());
	      		mav.addObject("operateRisk", result.getOperateRisk());
	      		mav.addObject("managerRisk", result.getManagerRisk());
	      	}
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return mav;
    }
    
    /**
     * 跳转 担保分析
     * @param taskCustId
     * @return
     */
    @RequestMapping("fcGuarantee.do")
	public ModelAndView fcGuarantee(Integer taskCustId) {
    	ModelAndView mav = new ModelAndView("/views/loanAfter/fiveClass/fcGuarantee");
  		mav.addObject("taskCustId", taskCustId);
    	try {
    		FcAnalysisDto result = fcCustFullInfoService.getFcAnalysis(taskCustId);
	      	if(result != null){
	      		mav.addObject("sponsorAnaly", result.getSponsorAnaly());
	      		mav.addObject("collateralAnaly", result.getCollateralAnaly());
	      	}
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return mav;
    }
    
    /**
     * 跳转 分类结论
     * @param taskCustId
     * @return
     */
    @RequestMapping("fcResult.do")
	public ModelAndView fcResult(Integer taskCustId) {
    	ModelAndView mav = new ModelAndView("/views/loanAfter/fiveClass/fcResult");
		try{
	  		mav.addObject("taskCustId", taskCustId);
	  		FcAnalysisDto result = fcCustFullInfoService.getFcAnalysis(taskCustId);
	      	if(result != null){
	      		mav.addObject("classifyReason", result.getClassifyReason());
	      		mav.addObject("fiveClass", result.getFiveClass());
	      	}
			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.STD_ZB_FIVE_SORT};
			String dicData = sysDicSV.queryMapListByTypes(optTypes);
	  		mav.addObject("dicData", dicData);
		}catch(Exception e){
			e.printStackTrace();
		}
    	return mav;
    }
    
    /**
     * 保存场地面积
     * @param taskCustId
     * @param areaCovered
     * @return
     */
    @RequestMapping("saveFullBaseInfo.json")
    @ResponseBody
    public ResultDto saveFullBaseInfo(Integer taskCustId, String areaCovered){
        ResultDto re = returnFail("保存数据失败！");
        try {
        	
        	FcCustFullInfo full = fcCustFullInfoService.selectSingleByProperty("taskCustId", taskCustId);
        	if(full==null){
        		re.setMsg("客户基本信息异常，请刷新后重试");
        		return re;
        	}
        	full.setAreaCovered(areaCovered);
        	fcCustFullInfoService.saveOrUpdate(full);
            re.setMsg("保存数据成功！");
            re.setCode(Constants.NORMAL);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }
    
    /**
     * 获取 股权结构信息
     * @param taskCustId
     * @return
     */
    @RequestMapping("getCustEquity.json")
	@ResponseBody
	public ResultDto getCustEquity(Integer taskCustId) {
		ResultDto re = returnFail("获取企业股权情况信息失败");
		List<CompBaseEquity> dataGrid = null;//股东列表
		try {
			FcCust fcCust = fcCustService.selectByPrimaryKey(taskCustId);
			dataGrid = compBaseSV.findCompEquityByCustNo(fcCust.getCustomerNo());
			re = returnSuccess();
			re.setRows(dataGrid);
			re.setMsg("获取企业股权情况信息成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
    
    /**
     * 获取 贷款信息列表
     * @param taskCustId
     * @return
     */
    @RequestMapping("getFcLoanList.json")
    @ResponseBody
    public ResultDto getFcLoanList(Integer taskCustId){
        ResultDto re = returnFail("获取数据失败！");
        try {
            List<FcBusiContract> list = fcBusiContractService.getByTaskCustId(taskCustId);
            re.setMsg("获取数据成功！");
            re.setCode(Constants.NORMAL);
            re.setRows(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }
    
    /**
     * 获取  财务比率分析表
     * @param taskCustId
     * @return
     */
    @RequestMapping("getFinaData.json")
    @ResponseBody
    public ResultDto getFinaData(Integer taskCustId){
        ResultDto re = returnFail("获取数据失败！");
        try {
            List<FcFinaData> list = fcFinaDataService.getFinaRate(taskCustId);
            re.setMsg("获取数据成功！");
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setRows(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }
    
    /**
     * 获取  现金流分析表
     * @param taskCustId
     * @return
     */
    @RequestMapping("getCashFlowList.json")
    @ResponseBody
    public ResultDto getCashFlowList(Integer taskCustId){
        ResultDto re = returnFail("获取数据失败！");
        try {
            List<FcFinaData> list = fcFinaDataService.getCashFlow(taskCustId);
            re.setMsg("获取数据成功！");
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setRows(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }
    
   /**
    * 保存 财务比率分析
    * @param request
    * @param taskCustId
    * @param jsonData
    * @return
    */
    @RequestMapping("saveFinaRateAnaly.json")
    @ResponseBody
    public ResultDto saveFinaRateAnaly(HttpServletRequest request,Integer taskCustId, String jsonData){
        ResultDto re = returnFail("保存失败！");
        try {
        	FcAnalysisDto source = JacksonUtil.fromJson(jsonData, FcAnalysisDto.class);
            if (source == null) return returnFail("传输数据内容为空，请重试!");
            
            User user = HttpUtil.getCurrentUser(request);
            FcCustFullInfo target = fcCustFullInfoService.selectSingleByProperty("taskCustId", taskCustId);
            if(target == null){
            	target = new FcCustFullInfo();
            	target.setCreater(user.getUserId());
            	target.setCreateTime(DateUtil.getNowTimestamp());
            }
            target.setProfitAnaly(source.getProfitAnaly());
            target.setOperateAnaly(source.getOperateAnaly());
            target.setDebtAnaly(source.getDebtAnaly());
            
            target.setUpdater(user.getUserId());
            target.setUpdateTime(DateUtil.getNowTimestamp());
            fcCustFullInfoService.saveOrUpdate(target);
            
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }
    
    /**
     * 保存 现金流分析
     * @param request
     * @param taskCustId
     * @param cashflowAnaly
     * @return
     */
    @RequestMapping("saveCashflowAnaly.json")
    @ResponseBody
    public ResultDto saveCashflowAnaly(HttpServletRequest request,Integer taskCustId, String cashflowAnaly){
        ResultDto re = returnFail("保存失败！");
        try {
            User user = HttpUtil.getCurrentUser(request);
            FcCustFullInfo target = fcCustFullInfoService.selectSingleByProperty("taskCustId", taskCustId);
            if(target == null){
            	target = new FcCustFullInfo();
            	target.setCreater(user.getUserId());
            	target.setCreateTime(DateUtil.getNowTimestamp());
            }
            target.setCashflowAnaly(cashflowAnaly);
            
            target.setUpdater(user.getUserId());
            target.setUpdateTime(DateUtil.getNowTimestamp());
            fcCustFullInfoService.saveOrUpdate(target);
            
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }
    
    /**
     * 保存 风险分析
     * @param request
     * @param taskCustId
     * @param jsonData
     * @return
     */
    @RequestMapping("saveRiskflowAnaly.json")
    @ResponseBody
    public ResultDto saveRiskflowAnaly(HttpServletRequest request,Integer taskCustId, String jsonData){
        ResultDto re = returnFail("保存失败！");
        try {
        	FcAnalysisDto source = JacksonUtil.fromJson(jsonData, FcAnalysisDto.class);
            if (source == null) return returnFail("传输数据内容为空，请重试!");
            
            User user = HttpUtil.getCurrentUser(request);
            FcCustFullInfo target = fcCustFullInfoService.selectSingleByProperty("taskCustId", taskCustId);
            if(target == null){
            	target = new FcCustFullInfo();
            	target.setCreater(user.getUserId());
            	target.setCreateTime(DateUtil.getNowTimestamp());
            }
            target.setInduRisk(source.getInduRisk());
            target.setOperateRisk(source.getOperateRisk());
            target.setManagerRisk(source.getManagerRisk());
            
            target.setUpdater(user.getUserId());
            target.setUpdateTime(DateUtil.getNowTimestamp());
            fcCustFullInfoService.saveOrUpdate(target);
            
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }
    
    /**
     * 保存 担保分析
     * @param request
     * @param taskCustId
     * @param jsonData
     * @return
     */
    @RequestMapping("saveGuaAnaly.json")
    @ResponseBody
    public ResultDto saveGuaAnaly(HttpServletRequest request,Integer taskCustId, String jsonData){
        ResultDto re = returnFail("保存失败！");
        try {
        	FcAnalysisDto source = JacksonUtil.fromJson(jsonData, FcAnalysisDto.class);
            if (source == null) return returnFail("传输数据内容为空，请重试!");
            
            User user = HttpUtil.getCurrentUser(request);
            FcCustFullInfo target = fcCustFullInfoService.selectSingleByProperty("taskCustId", taskCustId);
            if(target == null){
            	target = new FcCustFullInfo();
            	target.setCreater(user.getUserId());
            	target.setCreateTime(DateUtil.getNowTimestamp());
            }
            target.setSponsorAnaly(source.getSponsorAnaly());
            target.setCollateralAnaly(source.getCollateralAnaly());
            
            target.setUpdater(user.getUserId());
            target.setUpdateTime(DateUtil.getNowTimestamp());
            fcCustFullInfoService.saveOrUpdate(target);
            
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }
    
    /**
     * 保存 分类结果
     * @param request
     * @param taskCustId
     * @param jsonData
     * @return
     */
    @RequestMapping("saveResultAnaly.json")
    @ResponseBody
    public ResultDto saveResultAnaly(HttpServletRequest request,Integer taskCustId, String jsonData){
        ResultDto re = returnFail("保存失败！");
        try {
        	FcAnalysisDto source = JacksonUtil.fromJson(jsonData, FcAnalysisDto.class);
            if (source == null) return returnFail("传输数据内容为空，请重试!");
            
            User user = HttpUtil.getCurrentUser(request);
            FcCustFullInfo target = fcCustFullInfoService.selectSingleByProperty("taskCustId", taskCustId);
            if(target == null){
            	target = new FcCustFullInfo();
            	target.setCreater(user.getUserId());
            	target.setCreateTime(DateUtil.getNowTimestamp());
            }
            target.setClassifyReason(source.getClassifyReason());
            target.setFiveClass(source.getFiveClass());
            
            target.setUpdater(user.getUserId());
            target.setUpdateTime(DateUtil.getNowTimestamp());
            fcCustFullInfoService.saveOrUpdate(target);
            
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }
    
    /**
     * 跳转 预览报告
     * @param taskCustId
     * @return
     */
    @RequestMapping("viewFcReport.do")
	public ModelAndView viewFcReport(Integer taskCustId) {
    	ModelAndView mav = new ModelAndView("/views/loanAfter/fiveClass/fcReport");
    	try{
	    	Map<String, Object> dataMap = fcCustFullInfoService.getFcReportData(taskCustId);
	    	mav.addObject("data", dataMap);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
    	return mav;
    }
    
    /**
     * 下载 贷前调查报告
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("downloadFcReport.json")
    public ResponseEntity<byte[]> downloadLcReport(Integer taskCustId) {
        ResponseEntity<byte[]> result = null;
        try {
        	Map<String, Object> dataMap = fcCustFullInfoService.getFcReportData(taskCustId);
            dataMap.put("data", dataMap);
            
            
            FcCust fcCust = fcCustService.findByPrimaryKey(taskCustId);
            Integer taskId = fcCust.getTaskId();
            FcTask task = fcTaskService.findByPrimaryKey(taskId);
            String taskName = task.getTaskName();
            
            //父级路径为/GDTCESYS/appdata/fiveClass/"任务名"/"用户名"/
            String destPath = AppConfig.Cfg.FIVE_CLASS_PATH + taskName + "/" + dataMap.get("customerName");
            String fileName = dataMap.get("customerName") + "五级分类报告.doc";
            
            File file = new File(destPath , fileName);
            if(!file.getParentFile().exists()){
                file.getParentFile().mkdirs();
                }
            
            String templateName = "loan_statistics/lf/fcReport.htm";
            
            FreeMarkerWordUtil.createWord(dataMap, templateName, destPath, file.getName());
            HttpHeaders headers = new HttpHeaders();
            headers.setContentDispositionFormData("attachment", CodeUtil.parseGBK(fileName));
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            result = new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file), headers, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            log.error("下载五级分类报告文件异常：", e);
        }
        return result;
    }
    
    /**
     * 下载 工作底稿
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("downloadLfWorkingPaper.json")
    public ResponseEntity<byte[]> downloadLfWorkingPaper(HttpServletRequest request,
			 HttpServletResponse response, Integer taskCustId) throws Exception {
    	
		ResponseEntity<byte[]> result = null;
		try{
				
			
			FcCust fcCust = fcCustService.findByPrimaryKey(taskCustId);
	        Integer taskId = fcCust.getTaskId();
	        FcTask task = fcTaskService.findByPrimaryKey(taskId);
	        String taskName = task.getTaskName();
	        FcCustDto fcCustDto = fcCustFullInfoService.getByTaskCustId(taskCustId);
			String customerName = fcCustDto.getCustomerName();
			CusBase cusBase = cusBaseSV.queryByCusNo(fcCust.getCustomerNo());
		    String xdCustNo = cusBase.getXdCustomerNo();
			
			//父级路径为/GDTCESYS/appdata/fiveClass/"任务名"/"用户名"/
	        String destPath = AppConfig.Cfg.FIVE_CLASS_PATH + taskName + "/" + customerName;
	        String fileName = customerName + "五级分类工作底稿.xls";
	       
	        File file = new File(destPath , fileName);        
//	        if(!file.exists()){
	        	fcTaskService.generateReport(taskName, customerName, xdCustNo);
	        	file = new File(destPath , fileName);
//	        }
	        HttpHeaders headers = new HttpHeaders();
	        headers.setContentDispositionFormData("attachment", CodeUtil.parseGBK(fileName));
	        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
	        result = new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file), headers, HttpStatus.OK);
	        
		} catch (Exception e) {
			log.error("下载五级分类工作底稿文件异常：", e);
		}
		
		return result;
	}
    
    
    /**
     * 跳转 工作底稿
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("LfWorkingPaper.do")
    public ModelAndView LfWorkingPaper(Integer taskCustId) throws Exception {
    	ModelAndView mav = new ModelAndView("/views/loanAfter/fiveClass/workingPaper");
		try{
				
			
			FcCust fcCust = fcCustService.findByPrimaryKey(taskCustId);
			CusBase cusBase = cusBaseSV.queryByCusNo(fcCust.getCustomerNo());
		    String xdCustNo = cusBase.getXdCustomerNo();

			CmisWorkPaperDto cmisCustFullDto = fcTaskService.getCmisCustInfoFull(xdCustNo);			
			List<String> cmisSpecial = fcTaskService.getCmisSpecialAuth(cmisCustFullDto);
			CmisLargeAffirmDto cmisLargeAffirmDto = fcTaskService.getCmisLargeAffirm(cmisCustFullDto);
			List<CmisContractDto> contractListCompress = dtmpService.getContractListCompress(cmisLargeAffirmDto.getContractList());
			List<Double> contractListSum = dtmpService.getContractListSum(contractListCompress);
			CmisSmallAffirmDto cmisSmallAffirmDto = fcTaskService.getCmisSmallAffirm(cmisCustFullDto);
			
	        mav.addObject("cmis", cmisCustFullDto);
	        mav.addObject("special", cmisSpecial);
	        mav.addObject("cmisLager", cmisLargeAffirmDto);
	        mav.addObject("contractList", contractListCompress);
	        mav.addObject("contractSum", contractListSum);
	        mav.addObject("cmisSmall", cmisSmallAffirmDto);
		} catch (Exception e) {
			log.error("下载五级分类工作底稿文件异常：", e);
		}
		
		return mav;
	}
    
    /**
     * 下载 分类认定表
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("downloadLfIdentificationTable.json")
    public ResponseEntity<byte[]> downloadLfIdentificationTable(HttpServletRequest request,
			 HttpServletResponse response, Integer taskCustId) throws Exception {
    	
		ResponseEntity<byte[]> result = null;
		//模板数据
		try{
						
			FcCust fcCust = fcCustService.findByPrimaryKey(taskCustId);
	        Integer taskId = fcCust.getTaskId();
	        FcTask task = fcTaskService.findByPrimaryKey(taskId);
	        String taskName = task.getTaskName();
	        CusBase cusBase = cusBaseSV.queryByCusNo(fcCust.getCustomerNo());
	        String customerName = cusBase.getCustomerName();
	        String xdCustNo = cusBase.getXdCustomerNo();
			//父级路径为/GDTCESYS/appdata/fiveClass/"任务名"/"用户名"/
			String destPath = AppConfig.Cfg.FIVE_CLASS_PATH + taskName + "/" + customerName;
			File destPathDir = new File(destPath);
			
			String fileName = customerName + "五级分类小额分类认定表.xls";
			File file = new File(destPathDir, fileName);
			
			if(!file.exists()){
				fileName = customerName + "五级分类大额分类认定表.xls";
				file = new File(destPathDir, fileName);
			}
			
			if(!file.exists()){
				fileName = customerName + "五级分类认定表.xls";
				file = new File(destPathDir, fileName);
			}
			
			if(!file.exists()){
				fileName = fcTaskService.generateReport(taskName, customerName, xdCustNo);
	        	file = new File(destPath , fileName);
	        }
			
			HttpHeaders headers = new HttpHeaders();
	        headers.setContentDispositionFormData("attachment", CodeUtil.parseGBK(fileName));
	        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
	        result = new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file), headers, HttpStatus.OK);
		
				
			
		} catch (Exception e) {
			log.error("下载五级分类分类认定表文件异常：", e);
		}
		
		return result;
	}
    
    /**
     * 下载 五级分类压缩文件
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("downloadZip.json")
    public ResponseEntity<byte[]> downloadZip(HttpServletRequest request,
			 HttpServletResponse response, Integer taskId) throws Exception {
    	
		ResponseEntity<byte[]> result = null;

		try{
			
			FcTask task = fcTaskService.findByPrimaryKey(taskId);
			String taskName = task.getTaskName();
			
			String sourcefilePath = AppConfig.Cfg.FIVE_CLASS_PATH + taskName + "/";// 五级分类报告
			String zipFileName = sourcefilePath + taskName + "_FIVE_CLASS.zip";
						
			List<FcCust> fcCustList = fcCustService.selectByProperty("taskId", taskId);
						
			List<String> specifiedFiles = new ArrayList<String>();
			for(int i = 0; i < fcCustList.size(); i++){
			CusBase cusBase = cusBaseSV.queryByCusNo(fcCustList.get(i).getCustomerNo());
				specifiedFiles.add(cusBase.getCustomerName());
			}

			ZipCompressUtil.specifiedFilesZip(sourcefilePath, zipFileName, specifiedFiles);
			
			File zipfile = new File(zipFileName);
		
			HttpHeaders headers = new HttpHeaders();
			headers.setContentDispositionFormData("attachment", CodeUtil.parseGBK(taskName + "压缩文件.zip"));
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			result = new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(zipfile),
			headers, HttpStatus.OK);
		
		} catch (Exception e) {
			log.error("下载五级分类任务报告压缩文件异常：", e);
		}
		
		return result;
	}
    
    
    /**
     *  更新财报数据
     * @param request
     * @param taskCustId
     * @return
     */
    @RequestMapping("updateFinaData.json")
    @ResponseBody
    public ResultDto updateFinaData(HttpServletRequest request,Integer taskCustId){
        ResultDto re = returnFail("更新失败！");
        try {
        	
        	FcCustFullInfo full = fcCustFullInfoService.selectSingleByProperty("taskCustId", taskCustId);
        	CusBase cusBase = cusBaseSV.queryByCusName(full.getCustomerName());
        	String xdCustNo = cusBase.getXdCustomerNo();
        	String currFinaDate = dtmpService.getCustFinaDate(xdCustNo);
        	if(!StringUtils.isEmpty(currFinaDate)){
				full.setFinaDate(currFinaDate);
				int currYear = Integer.parseInt(currFinaDate.substring(0, 4));
				String oneYear = (currYear-1) + "12";//上年末
				String twoYear = (currYear-2) + "12";//上上年末
				List<BigDecimal> mainSubList = dtmpService.getCustFinaCurrMainSubject(xdCustNo, currFinaDate);
				if(!CollectionUtils.isEmpty(mainSubList)){
					full.setBalance(mainSubList.get(0).doubleValue());
					full.setDebt(mainSubList.get(1).doubleValue());
					full.setOwners(mainSubList.get(2).doubleValue());
					full.setSales(mainSubList.get(3).doubleValue());
					full.setProfits(mainSubList.get(4).doubleValue());
				}
				
				//删除原来的数据
				List<FcFinaData> existList = fcFinaDataService.selectByProperty("taskCustId", taskCustId);
				if(!CollectionUtils.isEmpty(existList)){
					for(FcFinaData fc: existList){
						fc.setStatus(Constants.DELETE);
						fcFinaDataService.saveOrUpdate(fc);
					}
				}
				
				//2.获取最近3期比率指标
				List<FcFinaData> ratioFinaData = dtmpService.getRatioFinaData(xdCustNo, currFinaDate, oneYear, twoYear);
				if(!CollectionUtils.isEmpty(ratioFinaData)){
					for(FcFinaData data : ratioFinaData){
						data.setTaskCustId(taskCustId);
						fcFinaDataService.saveOrUpdate(data);
					}
					
				}
		
				//3.获取最近3期现金流情况
				List<FcFinaData> cashFlowFinaData = dtmpService.getCashFlowFinaData(xdCustNo, currFinaDate, oneYear, twoYear);
				if(!CollectionUtils.isEmpty(cashFlowFinaData)){
					for(FcFinaData data : cashFlowFinaData){
						data.setTaskCustId(taskCustId);
						fcFinaDataService.saveOrUpdate(data);
					}
					//抽出最近一期在现金流指标
					full.setCashflowSum(cashFlowFinaData.get(0).getCurrVal());
					full.setCashflowOperate(cashFlowFinaData.get(1).getCurrVal());
					full.setCashflowInvest(cashFlowFinaData.get(2).getCurrVal());
					full.setCashflowFinanc(cashFlowFinaData.get(3).getCurrVal());
				}
				fcCustFullInfoService.saveOrUpdate(full);
        	}
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("更新成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
         return re;
    }
    
}
