package com.beawan.loanAfter.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.HandlerMapping;

import com.beawan.analysis.finansis.utils.CodeUtil;
import com.beawan.base.dto.RandomizingID;
import com.beawan.base.entity.User;
import com.beawan.common.BusinessException;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.common.aopLog.LoanAopLog;
import com.beawan.common.config.AppConfig;
import com.beawan.core.BaseController;
import com.beawan.core.Pagination;
import com.beawan.core.ResultDto;
import com.beawan.corporateloan.dto.LcManagerRiskStatDto;
import com.beawan.corporateloan.dto.LcRiskStatDto;
import com.beawan.corporateloan.dto.LcTaskStatDto;
import com.beawan.corporateloan.entity.LrCustInfo;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.dtmp.service.DtmpService;
import com.beawan.loanAfter.dto.LfClassifyVo;
import com.beawan.loanAfter.dto.LfCustDto;
import com.beawan.loanAfter.dto.LfTaskDto;
import com.beawan.loanAfter.entity.LfAdminSanInfo;
import com.beawan.loanAfter.entity.LfCfdAccountInfo;
import com.beawan.loanAfter.entity.LfChangeCheck;
import com.beawan.loanAfter.entity.LfCreditSelfBank;
import com.beawan.loanAfter.entity.LfCustInfo;
import com.beawan.loanAfter.entity.LfDownCheckInfo;
import com.beawan.loanAfter.entity.LfFifteenDayCheck;
import com.beawan.loanAfter.entity.LfFinCheck;
import com.beawan.loanAfter.entity.LfInternalControlInfo;
import com.beawan.loanAfter.entity.LfLoanProjectDetail;
import com.beawan.loanAfter.entity.LfLoanProjectSummary;
import com.beawan.loanAfter.entity.LfLocalResult;
import com.beawan.loanAfter.entity.LfPaymentOfFees;
import com.beawan.loanAfter.entity.LfProConstructCommit;
import com.beawan.loanAfter.entity.LfProjectInfo;
import com.beawan.loanAfter.entity.LfRiskManager;
import com.beawan.loanAfter.entity.LfRiskWarningInfo;
import com.beawan.loanAfter.entity.LfSituationCheck;
import com.beawan.loanAfter.entity.LfTask;
import com.beawan.loanAfter.entity.LfTaskResult;
import com.beawan.loanAfter.service.LfAdminSanInfoService;
import com.beawan.loanAfter.service.LfCfdAccountInfoService;
import com.beawan.loanAfter.service.LfChangeCheckService;
import com.beawan.loanAfter.service.LfCreditSelfBankService;
import com.beawan.loanAfter.service.LfCustInfoService;
import com.beawan.loanAfter.service.LfDownCheckInfoService;
import com.beawan.loanAfter.service.LfFifteenDayCheckService;
import com.beawan.loanAfter.service.LfFinCheckService;
import com.beawan.loanAfter.service.LfInternalControlInfoService;
import com.beawan.loanAfter.service.LfLoanProjectDetailService;
import com.beawan.loanAfter.service.LfLoanProjectSummaryService;
import com.beawan.loanAfter.service.LfLocalResultService;
import com.beawan.loanAfter.service.LfPaymentOfFeesService;
import com.beawan.loanAfter.service.LfProConstructCommitService;
import com.beawan.loanAfter.service.LfProjectInfoService;
import com.beawan.loanAfter.service.LfReportService;
import com.beawan.loanAfter.service.LfRiskWarningInfoService;
import com.beawan.loanAfter.service.LfSituationCheckService;
import com.beawan.loanAfter.service.LfTaskResultService;
import com.beawan.loanAfter.service.LfTaskService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.platform.util.DateUtil;
import com.platform.util.ExcelUtil;
import com.platform.util.ExceptionUtil;
import com.platform.util.FreeMarkerWordUtil;
import com.platform.util.GsonUtil;
import com.platform.util.HttpUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.PropertiesUtil;
import com.platform.util.RgnshUtils;
import com.platform.util.StringUtil;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Collections;

/**
 * @Author: xyh
 * @Date: 24/08/2020
 * @Description: 贷后控制器
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/loanAfter/" })
public class LoanAfterController extends BaseController {
    private static final Logger log = Logger.getLogger(LoanAfterController.class);

    @Resource
    private LfTaskService lfTaskService;
    @Resource
    private ICusBaseSV cusBaseSV;
    @Resource
    private LfFifteenDayCheckService lfFifteenDayCheckService;
    @Resource
    private LfTaskResultService lfTaskResultService;
    @Resource
    private LfChangeCheckService lfChangeCheckService;
    @Resource
    private LfFinCheckService lfFinCheckService;
    @Resource
    private LfSituationCheckService lfSituationCheckService;
    @Resource
    private LfRiskWarningInfoService lfRiskWarningInfoService;
    @Resource
    private LfAdminSanInfoService lfAdminSanInfoService;
    @Resource
    private LfCfdAccountInfoService lfCfdAccountInfoService;
    @Resource
    private LfInternalControlInfoService lfInternalControlInfoService;
    @Resource
    private LfLocalResultService lfLocalResultService;
    @Resource
    private LfPaymentOfFeesService lfPaymentOfFeesService;
    @Resource
    private LfProjectInfoService lfProjectInfoService;
    @Resource
	private LfLoanProjectSummaryService lfLoanProjectSummaryService;
    @Resource
    private LfLoanProjectDetailService lfLoanProjectDetailService;
    @Resource
	private LfProConstructCommitService lfProConstructCommitService;
    @Resource
	private LfDownCheckInfoService lfDownCheckInfoService;
    @Resource
    private LfCreditSelfBankService lfCreditSelfBankService;
    @Resource
	private LfReportService lfReportService;
    @Resource
    private LfCustInfoService lfCustInfoService;
    @Resource
    private DtmpService dtmpService;
    
    


   
    //=========================获取数据 .json==========================

    /**
     * 
     *@Description 获取调查中任务------首检
     *@param customerNo
     *@param customerName
     *@param page
     *@param rows
     *@return
     * @author xyh
     */
	@RequestMapping("getFisrtCheckingTaskList.json")
	@ResponseBody
	public ResultDto getFisrtCheckingTaskList(String customerNo, String customerName, String userName, 
			String orgName, String riskUserName, int page, int rows) {
		ResultDto resultDto = returnFail();
		User curUser = HttpUtil.getCurrentUser();
		String userNo = null;
		String orgNo = null;
		String riskUserNo = null;
		try {
			String userDA = curUser.getDataAccess();
			if (SysConstants.UserDataAccess.PERSONAL.equals(userDA)) {
				
				String roleNo = curUser.getRoleNo();
				String [] role = roleNo.split(",");
				for(int i = 0; i<role.length; i++){
					if(role[i].contains(Constants.ROLE.LF_RISK_USER)){
						riskUserNo = curUser.getUserId();
						break;
					}
				}
				if(riskUserNo == null)userNo = curUser.getUserId();
				
			} else if (SysConstants.UserDataAccess.ORG.equals(userDA)) {
				orgNo = curUser.getAccOrgNo();
			}
			
			String roleNo = curUser.getRoleNo();
			List<String> taskTypes = new ArrayList<>();
			//根据当前登录用户在角色   判断是否 管护经理 还是  存在专职风险经理角色
			//当用户贷后管理员时，则能看贷后和管护的首检任务
			//当用户是专职风险经理时，则只能看到专职风险经理任务
			//当用户是不是贷后管理员、且不是专职风险经理时，则，为管护客户经理
			if(roleNo.contains(Constants.ROLE.LF_CONFIG_MANAGER)){
				taskTypes.add(Constants.LF_TASK_TYPE.GH_FIFTEEN_DAY_TASK);
			}else if(roleNo.contains(Constants.ROLE.LF_RISK_USER)){
				taskTypes.add(Constants.LF_TASK_TYPE.FX_FIFTEEN_DAY_TASK);
			}else{
				taskTypes.add(Constants.LF_TASK_TYPE.GH_FIFTEEN_DAY_TASK);
			}
			
			Pagination<LfTaskDto> pager = lfTaskService.getLfTaskPage(taskTypes, customerNo, customerName,
					userName, orgName, riskUserName, riskUserNo,userNo,
					orgNo, Constants.LF_TASK_POSITION.CHECKING_TASK, page, rows);
			resultDto = returnSuccess();
			resultDto.setRows(pager.getItems());
			resultDto.setTotal(pager.getRowsCount());
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取待调查贷后任务失败！", e);
			resultDto.setMsg("获取待调查贷后任务失败");
		}
		return resultDto;
	}
	
	
	
	/**
     * 
     *@Description 获取调查中任务------首检
     *@param customerNo
     *@param customerName
     *@param page
     *@param rows
     *@return
     * @author xyh
     */
	@RequestMapping("getFisrtRiskCheckingTaskList.json")
	@ResponseBody
	public ResultDto getFisrtRiskCheckingTaskList(String customerNo, String customerName, String userName, 
			String orgName, String riskUserName, int page, int rows) {
		ResultDto resultDto = returnFail();
		User curUser = HttpUtil.getCurrentUser();
		String userNo = null;
		String orgNo = null;
		String riskUserNo = null;
		try {
			String userDA = curUser.getDataAccess();
			if (SysConstants.UserDataAccess.PERSONAL.equals(userDA)) {
				
				String roleNo = curUser.getRoleNo();
				String [] role = roleNo.split(",");
				for(int i = 0; i<role.length; i++){
					if(role[i].contains(Constants.ROLE.LF_RISK_USER)){
						riskUserNo = curUser.getUserId();
						break;
					}
				}
				if(riskUserNo == null)userNo = curUser.getUserId();
				
			} else if (SysConstants.UserDataAccess.ORG.equals(userDA)) {
				orgNo = curUser.getAccOrgNo();
			}
			
			String roleNo = curUser.getRoleNo();
			List<String> taskTypes = new ArrayList<>();
			//根据当前登录用户在角色   判断是否 管护经理 还是  存在专职风险经理角色
			//当用户贷后管理员时，则能看贷后和管护的首检任务
			//当用户是专职风险经理时，则只能看到专职风险经理任务
			//当用户是不是贷后管理员、且不是专职风险经理时，则，为管护客户经理
			if(roleNo.contains(Constants.ROLE.LF_CONFIG_MANAGER)){
				taskTypes.add(Constants.LF_TASK_TYPE.FX_FIFTEEN_DAY_TASK);
			}else if(roleNo.contains(Constants.ROLE.LF_RISK_USER)){
				taskTypes.add(Constants.LF_TASK_TYPE.FX_FIFTEEN_DAY_TASK);
			}else{
				taskTypes.add(Constants.LF_TASK_TYPE.GH_FIFTEEN_DAY_TASK);
			}
			
			Pagination<LfTaskDto> pager = lfTaskService.getLfTaskPage(taskTypes, customerNo, customerName,
					userName, orgName, riskUserName, riskUserNo,userNo,
					orgNo, Constants.LF_TASK_POSITION.CHECKING_TASK, page, rows);
			resultDto = returnSuccess();
			resultDto.setRows(pager.getItems());
			resultDto.setTotal(pager.getRowsCount());
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取待调查贷后任务失败！", e);
			resultDto.setMsg("获取待调查贷后任务失败");
		}
		return resultDto;
	}
	
	
    /**
     * 
     *@Description 获取调查中任务------管户日常
     *@param customerNo
     *@param customerName
     *@param page
     *@param rows
     *@return
     * @author xyh
     */
	@RequestMapping("getDaylyCheckingTaskList.json")
	@ResponseBody
	public ResultDto getDaylyCheckingTaskList(String customerNo, String customerName, String userName, 
			String orgName, String riskUserName, int page, int rows) {
		ResultDto resultDto = returnFail();
		User curUser = HttpUtil.getCurrentUser();
		String userNo = null;
		String orgNo = null;
		String riskUserNo = null;
		try {
			String userDA = curUser.getDataAccess();
			if (SysConstants.UserDataAccess.PERSONAL.equals(userDA)) {
				
				String roleNo = curUser.getRoleNo();
				String [] role = roleNo.split(",");
				for(int i = 0; i<role.length; i++){
					if(role[i].contains(Constants.ROLE.LF_RISK_USER)){
						riskUserNo = curUser.getUserId();
						break;
					}
				}
				if(riskUserNo == null)userNo = curUser.getUserId();
				
			} else if (SysConstants.UserDataAccess.ORG.equals(userDA)) {
				orgNo = curUser.getAccOrgNo();
			}
			
			List<String> taskTypes = new ArrayList<>();
			taskTypes.add(Constants.LF_TASK_TYPE.GH_DAY_TASK);
			Pagination<LfTaskDto> pager = lfTaskService.getLfTaskPage(taskTypes, customerNo, customerName,
					userName, orgName, riskUserName, riskUserNo, userNo,
					orgNo, Constants.LF_TASK_POSITION.CHECKING_TASK, page, rows);
			resultDto = returnSuccess();
			resultDto.setRows(pager.getItems());
			resultDto.setTotal(pager.getRowsCount());
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取待调查贷后任务失败！", e);
			resultDto.setMsg("获取待调查贷后任务失败");
		}
		return resultDto;
	}
    
    /**
     * 
     *@Description 获取调查中任务------3个月到期检查
     *@param customerNo
     *@param customerName
     *@param page
     *@param rows
     *@return
     * @author xyh
     */
    @RequestMapping("getStopCheckingTaskList.json")
    @ResponseBody
    public ResultDto getStopCheckingTaskList(String customerNo,String customerName, String userName,
    		String orgName, String riskUserName,
    		int page, int rows) {
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
        String userNo = null;
        String orgNo = null;
        String riskUserNo = null;
		try {
			String userDA = curUser.getDataAccess();
			if (SysConstants.UserDataAccess.PERSONAL.equals(userDA)) {
				
				String roleNo = curUser.getRoleNo();
				String [] role = roleNo.split(",");
				for(int i = 0; i<role.length; i++){
					if(role[i].contains(Constants.ROLE.LF_RISK_USER)){
						riskUserNo = curUser.getUserId();
						break;
					}
				}
				if(riskUserNo == null)userNo = curUser.getUserId();
				
			} else if (SysConstants.UserDataAccess.ORG.equals(userDA)) {
				orgNo = curUser.getAccOrgNo();
			}
			
            List<String> taskTypes = new ArrayList<>();
            taskTypes.add(Constants.LF_TASK_TYPE.FX_THREE_MONTH_TASK);
            Pagination<LfTaskDto> pager = lfTaskService.getLfTaskPage(taskTypes,customerNo,customerName,
            		userName, orgName, riskUserName,riskUserNo,
            		userNo, orgNo,Constants.LF_TASK_POSITION.CHECKING_TASK,page,rows);
            resultDto = returnSuccess();
            resultDto.setRows(pager.getItems());
            resultDto.setTotal(pager.getRowsCount());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取待调查贷后任务失败！",e);
            resultDto.setMsg("获取待调查贷后任务失败");
        }
        return resultDto;
    }
    
    /**
     * 
     *@Description 获取调查中任务------风险预警任务
     *@param customerNo
     *@param customerName
     *@param page
     *@param rows
     *@return
     * @author xyh
     */
    @RequestMapping("getRiskWarnCheckingTaskList.json")
    @ResponseBody
    public ResultDto getRiskWarnCheckingTaskList(String customerNo,String customerName, String userName,
    		String orgName, String riskUserName,int page, int rows) {
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
        String userNo = null;
        String orgNo = null;
        String riskUserNo = null;
		try {
			String userDA = curUser.getDataAccess();
			if (SysConstants.UserDataAccess.PERSONAL.equals(userDA)) {
				
				String roleNo = curUser.getRoleNo();
				String [] role = roleNo.split(",");
				for(int i = 0; i<role.length; i++){
					if(role[i].contains(Constants.ROLE.LF_RISK_USER)){
						riskUserNo = curUser.getUserId();
						break;
					}
				}
				if(riskUserNo == null)userNo = curUser.getUserId();
				
			} else if (SysConstants.UserDataAccess.ORG.equals(userDA)) {
				orgNo = curUser.getAccOrgNo();
			}
            List<String> taskTypes = new ArrayList<>();
            taskTypes.add(Constants.LF_TASK_TYPE.FX_YJ_TASK);
            Pagination<LfTaskDto> pager = lfTaskService.getLfTaskPage(taskTypes,customerNo,customerName,
            		userName, orgName, riskUserName,riskUserNo,
            		userNo, orgNo,Constants.LF_TASK_POSITION.CHECKING_TASK,page,rows);
            resultDto = returnSuccess();
            resultDto.setRows(pager.getItems());
            resultDto.setTotal(pager.getRowsCount());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取待调查贷后任务失败！",e);
            resultDto.setMsg("获取待调查贷后任务失败");
        }
        return resultDto;
    }
    
    /**
     * 
     *@Description 获取调查中任务------手动创建
     *@param customerNo
     *@param customerName
     *@param page
     *@param rows
     *@return
     * @author lk
     */
	@RequestMapping("getCreateManuallyCheckingTaskList.json")
	@ResponseBody
	public ResultDto getCreateManuallyCheckingTaskList(String customerNo, String customerName, String userName, 
			String orgName, String riskUserName, int page, int rows) {
		ResultDto resultDto = returnFail();
		User curUser = HttpUtil.getCurrentUser();
		String userNo = null;
		String orgNo = null;
		String riskUserNo = null;
		try {
			String userDA = curUser.getDataAccess();
			if (SysConstants.UserDataAccess.PERSONAL.equals(userDA)) {
					
				String roleNo = curUser.getRoleNo();
				String [] role = roleNo.split(",");
				for(int i = 0; i<role.length; i++){
					if(role[i].contains(Constants.ROLE.LF_RISK_USER)){
						riskUserNo = curUser.getUserId();
						break;
					}
				}
				if(riskUserNo == null)
					userNo = curUser.getUserId();
					
			} else if (SysConstants.UserDataAccess.ORG.equals(userDA)) {
				orgNo = curUser.getAccOrgNo();
			}
			List<String> taskTypes = new ArrayList<>();
			taskTypes.add(Constants.LF_TASK_TYPE.CREATE_MANUALLY_TASK);
			
//			Pagination<LfTaskDto> getLfTaskPage(List<String> taskTypes, String customerNo, String customerName, String userName,
//					String orgName, String riskUserName, String riskUserNo, String userNo, String orgNo, Integer taskPosition,
			
			Pagination<LfTaskDto> pager = lfTaskService.getLfTaskPage(taskTypes, customerNo, customerName,
					userName, orgName, riskUserName,riskUserNo, userNo,
					orgNo, Constants.LF_TASK_POSITION.CHECKING_TASK, page, rows);
			resultDto = returnSuccess();
			resultDto.setRows(pager.getItems());
			resultDto.setTotal(pager.getRowsCount());
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取待调查贷后任务失败！", e);
			resultDto.setMsg("获取待调查贷后任务失败");
		}
		return resultDto;
	}
    
    
    
    /**
     * 
     *@Description 获取调查结束任务------首检
     *@param customerNo
     *@param customerName
     *@param page
     *@param rows
     *@return
     * @author xyh
     */
    @RequestMapping("getFisrtCheckedTaskList.json")
    @ResponseBody
    public ResultDto getFisrtCheckedTaskList(String customerNo,String customerName,
    		String userName, String orgName, String riskUserName,int page, int rows) {
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
		String userNo = null;
		String orgNo = null;
		String riskUserNo = null;
		try {
			String userDA = curUser.getDataAccess();
			if (SysConstants.UserDataAccess.PERSONAL.equals(userDA)) {
				
				String roleNo = curUser.getRoleNo();
				String [] role = roleNo.split(",");
				for(int i = 0; i<role.length; i++){
					if(role[i].contains(Constants.ROLE.LF_RISK_USER)){
						riskUserNo = curUser.getUserId();
						break;
					}
				}
				if(riskUserNo == null)userNo = curUser.getUserId();
				
			} else if (SysConstants.UserDataAccess.ORG.equals(userDA)) {
				orgNo = curUser.getAccOrgNo();
			}
			
			String roleNo = curUser.getRoleNo();
			List<String> taskTypes = new ArrayList<>();
			//根据当前登录用户在角色   判断是否 管护经理 还是  存在专职风险经理角色
			//当用户贷后管理员时，则能看贷后和管护的首检任务
			//当用户是专职风险经理时，则只能看到专职风险经理任务
			//当用户是不是贷后管理员、且不是专职风险经理时，则，为管护客户经理
			if(roleNo.contains(Constants.ROLE.LF_CONFIG_MANAGER)){
				taskTypes.add(Constants.LF_TASK_TYPE.GH_FIFTEEN_DAY_TASK);
			}else if(roleNo.contains(Constants.ROLE.LF_RISK_USER)){
				taskTypes.add(Constants.LF_TASK_TYPE.FX_FIFTEEN_DAY_TASK);
			}else{
				taskTypes.add(Constants.LF_TASK_TYPE.GH_FIFTEEN_DAY_TASK);
			}
			
            Pagination<LfTaskDto> pager = lfTaskService.getLfTaskPage(taskTypes,customerNo,customerName,
            		userName, orgName, riskUserName,riskUserNo,userNo, orgNo,Constants.LF_TASK_POSITION.CHECKED_TASK,page,rows);
            resultDto = returnSuccess();
            resultDto.setRows(pager.getItems());
            resultDto.setTotal(pager.getRowsCount());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取结束调查贷后任务失败！",e);
            resultDto.setMsg("获取结束调查贷后任务失败！");
        }
        return resultDto;
    }
    
    
    /**
     * 
     *@Description 获取调查结束任务------首检
     *@param customerNo
     *@param customerName
     *@param page
     *@param rows
     *@return
     * @author xyh
     */
    @RequestMapping("getFisrtRiskCheckedTaskList.json")
    @ResponseBody
    public ResultDto getFisrtRiskCheckedTaskList(String customerNo,String customerName,
    		String userName, String orgName, String riskUserName,int page, int rows) {
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
		String userNo = null;
		String orgNo = null;
		String riskUserNo = null;
		try {
			String userDA = curUser.getDataAccess();
			if (SysConstants.UserDataAccess.PERSONAL.equals(userDA)) {
				
				String roleNo = curUser.getRoleNo();
				String [] role = roleNo.split(",");
				for(int i = 0; i<role.length; i++){
					if(role[i].contains(Constants.ROLE.LF_RISK_USER)){
						riskUserNo = curUser.getUserId();
						break;
					}
				}
				if(riskUserNo == null)userNo = curUser.getUserId();
				
			} else if (SysConstants.UserDataAccess.ORG.equals(userDA)) {
				orgNo = curUser.getAccOrgNo();
			}
			
			String roleNo = curUser.getRoleNo();
			List<String> taskTypes = new ArrayList<>();
			//根据当前登录用户在角色   判断是否 管护经理 还是  存在专职风险经理角色
			//当用户贷后管理员时，则能看贷后和管护的首检任务
			//当用户是专职风险经理时，则只能看到专职风险经理任务
			//当用户是不是贷后管理员、且不是专职风险经理时，则，为管护客户经理
			if(roleNo.contains(Constants.ROLE.LF_CONFIG_MANAGER)){
				taskTypes.add(Constants.LF_TASK_TYPE.FX_FIFTEEN_DAY_TASK);
			}else if(roleNo.contains(Constants.ROLE.LF_RISK_USER)){
				taskTypes.add(Constants.LF_TASK_TYPE.FX_FIFTEEN_DAY_TASK);
			}else{
				taskTypes.add(Constants.LF_TASK_TYPE.GH_FIFTEEN_DAY_TASK);
			}
			
            Pagination<LfTaskDto> pager = lfTaskService.getLfTaskPage(taskTypes,customerNo,customerName,
            		userName, orgName, riskUserName,riskUserNo,userNo, orgNo,Constants.LF_TASK_POSITION.CHECKED_TASK,page,rows);
            resultDto = returnSuccess();
            resultDto.setRows(pager.getItems());
            resultDto.setTotal(pager.getRowsCount());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取结束调查贷后任务失败！",e);
            resultDto.setMsg("获取结束调查贷后任务失败！");
        }
        return resultDto;
    }
    /**
     * 
     *@Description 获取调查结束任务------管户日常
     *@param customerNo
     *@param customerName
     *@param page
     *@param rows
     *@return
     * @author xyh
     */
    @RequestMapping("getDaylyCheckedTaskList.json")
    @ResponseBody
    public ResultDto getDaylyCheckedTaskList(String customerNo,String customerName, String userName,
    		String orgName, String riskUserName,int page, int rows) {
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
        String userNo = null;
        String orgNo = null;
        String riskUserNo = null;
		try {
			String userDA = curUser.getDataAccess();
			if (SysConstants.UserDataAccess.PERSONAL.equals(userDA)) {
					
				String roleNo = curUser.getRoleNo();
				String [] role = roleNo.split(",");
				for(int i = 0; i<role.length; i++){
					if(role[i].contains(Constants.ROLE.LF_RISK_USER)){
						riskUserNo = curUser.getUserId();
						break;
					}
				}
				if(riskUserNo == null)userNo = curUser.getUserId();
					
			} else if (SysConstants.UserDataAccess.ORG.equals(userDA)) {
				orgNo = curUser.getAccOrgNo();
			}
            List<String> taskTypes = new ArrayList<>();
            taskTypes.add(Constants.LF_TASK_TYPE.GH_DAY_TASK);
            Pagination<LfTaskDto> pager = lfTaskService.getLfTaskPage(taskTypes,customerNo,customerName,
            		userName, orgName, riskUserName,riskUserNo,userNo, orgNo,Constants.LF_TASK_POSITION.CHECKED_TASK,page,rows);
            resultDto = returnSuccess();
            resultDto.setRows(pager.getItems());
            resultDto.setTotal(pager.getRowsCount());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取结束调查贷后任务失败！",e);
            resultDto.setMsg("获取结束调查贷后任务失败！");
        }
        return resultDto;
    }
    
    /**
     * 
     *@Description 获取调查结束任务------3个月到期检查
     *@param customerNo
     *@param customerName
     *@param page
     *@param rows
     *@return
     * @author xyh
     */
    @RequestMapping("getStopCheckedTaskList.json")
    @ResponseBody
    public ResultDto getStopCheckedTaskList(String customerNo,String customerName, String userName,
    		String orgName, String riskUserName,int page, int rows) {
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
        String userNo = null;
        String orgNo = null;
        String riskUserNo = null;
		try {
			String userDA = curUser.getDataAccess();
			if (SysConstants.UserDataAccess.PERSONAL.equals(userDA)) {
					
				String roleNo = curUser.getRoleNo();
				String [] role = roleNo.split(",");
				for(int i = 0; i<role.length; i++){
					if(role[i].contains(Constants.ROLE.LF_RISK_USER)){
						riskUserNo = curUser.getUserId();
						break;
					}
				}
				if(riskUserNo == null)userNo = curUser.getUserId();
					
			} else if (SysConstants.UserDataAccess.ORG.equals(userDA)) {
				orgNo = curUser.getAccOrgNo();
			}
            List<String> taskTypes = new ArrayList<>();
            taskTypes.add(Constants.LF_TASK_TYPE.FX_THREE_MONTH_TASK);
            Pagination<LfTaskDto> pager = lfTaskService.getLfTaskPage(taskTypes,customerNo,customerName,
            		userName, orgName, riskUserName,riskUserNo,userNo, orgNo,Constants.LF_TASK_POSITION.CHECKED_TASK,page,rows);
            resultDto = returnSuccess();
            resultDto.setRows(pager.getItems());
            resultDto.setTotal(pager.getRowsCount());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取结束调查贷后任务失败！",e);
            resultDto.setMsg("获取结束调查贷后任务失败！");
        }
        return resultDto;
    }
    
    /**
     * 
     *@Description 获取调查结束任务------风险预警任务
     *@param customerNo
     *@param customerName
     *@param page
     *@param rows
     *@return
     * @author xyh
     */
    @RequestMapping("getRiskWarnCheckedTaskList.json")
    @ResponseBody
    public ResultDto getRiskWarnCheckedTaskList(String customerNo,String customerName, String userName, 
    		String orgName, String riskUserName,int page, int rows) {
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
        String userNo = null;
        String orgNo = null;
        String riskUserNo = null;
		try {
			String userDA = curUser.getDataAccess();
			if (SysConstants.UserDataAccess.PERSONAL.equals(userDA)) {
					
				String roleNo = curUser.getRoleNo();
				String [] role = roleNo.split(",");
				for(int i = 0; i<role.length; i++){
					if(role[i].contains(Constants.ROLE.LF_RISK_USER)){
						riskUserNo = curUser.getUserId();
						break;
					}
				}
				if(riskUserNo == null)userNo = curUser.getUserId();
					
			} else if (SysConstants.UserDataAccess.ORG.equals(userDA)) {
				orgNo = curUser.getAccOrgNo();
			}
            List<String> taskTypes = new ArrayList<>();
            taskTypes.add(Constants.LF_TASK_TYPE.FX_YJ_TASK);
            Pagination<LfTaskDto> pager = lfTaskService.getLfTaskPage(taskTypes,customerNo,customerName,
            		userName, orgName, riskUserName,riskUserNo,userNo, orgNo,Constants.LF_TASK_POSITION.CHECKED_TASK,page,rows);
            resultDto = returnSuccess();
            resultDto.setRows(pager.getItems());
            resultDto.setTotal(pager.getRowsCount());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取结束调查贷后任务失败！",e);
            resultDto.setMsg("获取结束调查贷后任务失败！");
        }
        return resultDto;
    }
    
    /**
     * 
     *@Description 获取调查结束任务------手动创建任务
     *@param customerNo
     *@param customerName
     *@param page
     *@param rows
     *@return
     * @author lk
     */
    @RequestMapping("getCreateManuallyCheckedTaskList.json")
    @ResponseBody
    public ResultDto getCreateManuallyCheckedTaskList(String customerNo,String customerName, String userName,
    		String orgName, String riskUserName,int page, int rows) {
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
        String userNo = null;
        String orgNo = null;
        String riskUserNo = null;
		try {
			String userDA = curUser.getDataAccess();
			if (SysConstants.UserDataAccess.PERSONAL.equals(userDA)) {
					
				String roleNo = curUser.getRoleNo();
				String [] role = roleNo.split(",");
				for(int i = 0; i<role.length; i++){
					if(role[i].contains(Constants.ROLE.LF_RISK_USER)){
						riskUserNo = curUser.getUserId();
						break;
					}
				}
				if(riskUserNo == null)userNo = curUser.getUserId();
					
			} else if (SysConstants.UserDataAccess.ORG.equals(userDA)) {
				orgNo = curUser.getAccOrgNo();
			}
            List<String> taskTypes = new ArrayList<>();
            taskTypes.add(Constants.LF_TASK_TYPE.CREATE_MANUALLY_TASK);
            Pagination<LfTaskDto> pager = lfTaskService.getLfTaskPage(taskTypes,customerNo,customerName,
            		userName, orgName, riskUserName,riskUserNo,userNo, orgNo,Constants.LF_TASK_POSITION.CHECKED_TASK,page,rows);
            resultDto = returnSuccess();
            resultDto.setRows(pager.getItems());
            resultDto.setTotal(pager.getRowsCount());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取结束调查贷后任务失败！",e);
            resultDto.setMsg("获取结束调查贷后任务失败！");
        }
        return resultDto;
    }
   
    /**
	 * 
	 *@Description 手动创建单个特殊贷后任务
	 *@return
	 * @author lk
	 */
	@RequestMapping("saveCreateManuallyOneTask.json")
	@ResponseBody
	public ResultDto saveCreateManuallyOneTask(String customerNo){
		ResultDto re = returnFail("创建特殊贷后任务失败");
		try {
			
			lfTaskService.createManuallyOneTask(customerNo);
			
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("创建特殊贷后任务成功");
		} catch (Exception e) {
			log.error("创建特殊贷后任务异常", e);
			re.setMsg(e.getMessage());
		}
		return re;
	}
	
    
    
    /**
     * 保存客户分类信息
     * @param lfTaskStr
     * @return
     */
    @RequestMapping("saveCustClassi.json")
    @ResponseBody
    public ResultDto saveCustClassi(String lfTaskStr,String flag) {
        ResultDto resultDto = returnFail();
        if(StringUtil.isEmptyString(lfTaskStr)){
            resultDto = returnFail("传输数据为空，保存失败");
        }
        User curUser = HttpUtil.getCurrentUser();
        try {
            LfTask lfTask = GsonUtil.GsonToBean(lfTaskStr,LfTask.class);
            if(lfTask == null || RgnshUtils.isEmptyInteger(lfTask.getId())){
                resultDto = returnFail("传输数据为空，保存失败");
            }
            LfTask saveLfTask = lfTaskService.findByPrimaryKey(lfTask.getId());
            
            if(flag.equals("01")){ //01 表示从客户分类中客户风险页保存传来的值。（为了保护该保存操作不会将其他页传来的值被覆盖）
	            if(!StringUtils.isEmpty(lfTask.getRiskClassi())){
	            	saveLfTask.setRiskClassi(lfTask.getRiskClassi());
	            }
	            if(!StringUtils.isEmpty(lfTask.getRiskSecondClassi()) && lfTask.getRiskClassi().equals("02")){
	            	saveLfTask.setRiskSecondClassi(lfTask.getRiskSecondClassi());
	            }else{
	            	saveLfTask.setRiskSecondClassi("");
	            }
	            if(!StringUtils.isEmpty(lfTask.getRiskPoint())){
	            	saveLfTask.setRiskPoint(lfTask.getRiskPoint());
	            }
	            if(!StringUtils.isEmpty(lfTask.getRiskClassiReason())){
	            	saveLfTask.setRiskClassiReason(lfTask.getRiskClassiReason());
	            }
            }else{// 表示从客户分类中客户授信页保存传来的值。
            	 if(!StringUtils.isEmpty(lfTask.getCreditClassi())){
  	            	saveLfTask.setCreditClassi(lfTask.getCreditClassi()); 
  	            }
	            if(!StringUtils.isEmpty(lfTask.getCreditSecondClassi()) && lfTask.getCreditClassi().equals("08") ){
	            	saveLfTask.setCreditSecondClassi(lfTask.getCreditSecondClassi());
	            }else{
	            	saveLfTask.setCreditSecondClassi("");
	            }
	            if(!StringUtils.isEmpty(lfTask.getCreditPoint())){
	            	saveLfTask.setCreditPoint(lfTask.getCreditPoint());
	            }
	            if(!StringUtils.isEmpty(lfTask.getCreditClassiReason())){
	            	saveLfTask.setCreditClassiReason(lfTask.getCreditClassiReason());
	            }
            }
            saveLfTask.setUpdater(curUser.getUserId());
            saveLfTask.setUpdateTime(DateUtil.getNowTimestamp());
            lfTaskService.saveOrUpdate(saveLfTask);
            resultDto = returnSuccess();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存贷后客户客户分类信息失败！",e);
            resultDto.setMsg("保存贷后客户客户分类信息失败");
        }
        return resultDto;
    }

    /**
     * 提交贷后任务--贷后任务完成
     * @param lfTaskStr
     * @return
     */
    @RequestMapping("submitLfTaskOk.json")
    @ResponseBody
    public ResultDto submitLfTaskOk(String lfTaskStr) {
        ResultDto resultDto = returnFail();
        if(StringUtil.isEmptyString(lfTaskStr)){
            resultDto = returnFail("传输数据为空，保存失败");
        }
        User curUser = HttpUtil.getCurrentUser();
        try {
            LfTask lfTask = GsonUtil.GsonToBean(lfTaskStr,LfTask.class);
            lfTaskService.submitLfTaskInfo(lfTask,curUser.getUserId());
            resultDto = returnSuccess();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存贷后客户客户分类信息失败！",e);
            resultDto.setMsg(e.getMessage());
        }
        return resultDto;
    }

    /**
     * 获取15日首检核查列表
     * @param taskId
     * @return
     */
    @RequestMapping("getFifteenDayCheckList.json")
    @ResponseBody
    public ResultDto getFifteenDayCheckList(Integer taskId) {
        ResultDto resultDto = returnFail();
        try {
            List<LfFifteenDayCheck> lfFifteenDayCheckList = lfFifteenDayCheckService.selectByProperty("taskId",taskId);
            resultDto = returnSuccess();
            resultDto.setRows(lfFifteenDayCheckList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取首检核查列表失败！",e);
            resultDto.setMsg("获取首检核查列表失败！");
        }
        return resultDto;
    }

    /**
     * 保存15日首检核查列表
     * @param fifteenDayCheckList
     * @return
     */
    @RequestMapping("saveFifteenDayCheckList.json")
    @ResponseBody
    public ResultDto saveFifteenDayCheckList(String fifteenDayCheckList){
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
        try {
            List<LfFifteenDayCheck> saveList = JacksonUtil.fromJson(fifteenDayCheckList,new TypeReference<List<LfFifteenDayCheck>>() {});
            for(LfFifteenDayCheck lfFifteenDayCheck : saveList){
                lfFifteenDayCheck.setUpdater(curUser.getUserId());
                lfFifteenDayCheck.setUpdateTime(DateUtil.getNowTimestamp());
            }
            lfFifteenDayCheckService.batchSaveUpdata(saveList);
            resultDto = returnSuccess();

        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存首检核查列表失败！",e);
            resultDto.setMsg("系统异常：保存首检核查列表失败");
        }
        return resultDto;
    }

    /**
     * 保存首检贷后检查结论
     * @param fifteenDayResult 结论
     * @param taskId
     * @return
     */
    @RequestMapping("saveFifteenDayResult.json")
    @ResponseBody
    public ResultDto saveFifteenDayResult(String fifteenDayResult,Integer taskId){
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
        try {
            LfTaskResult lfTaskResult = lfTaskResultService.selectSingleByProperty("taskId",taskId);
            if(lfTaskResult == null){
                lfTaskResult = new LfTaskResult();
                lfTaskResult.setCreater(curUser.getUserId());
                lfTaskResult.setTaskId(taskId);
            }
            lfTaskResult.setFifteenDayResult(fifteenDayResult);
            lfTaskResult.setUpdater(curUser.getUserId());
            lfTaskResult.setUpdateTime(DateUtil.getNowTimestamp());
            lfTaskResultService.saveOrUpdate(lfTaskResult);
            resultDto = returnSuccess();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存首检结论信息失败！",e);
            resultDto.setMsg("系统异常：保存首检结论信息失败");
        }
        return resultDto;
    }
    /**
     * 获取客户变动信息检查列表
     * @param taskId
     * @return
     */
    @RequestMapping("getCustChangeCheckList.json")
    @ResponseBody
    public ResultDto getCustChangeCheckList(Integer taskId) {
        ResultDto resultDto = returnFail();
        try {
            Map<String,Object> params = new HashMap<>();
            params.put("taskId",taskId);
            params.put("checkType",Constants.LF_SYS_DIC.LF_CHANGE_CUST);
            List<LfChangeCheck> lfChangeCheckList = lfChangeCheckService.selectByProperty(params);
            resultDto = returnSuccess();
            resultDto.setRows(lfChangeCheckList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取客户变动信息检查列表失败！",e);
            resultDto.setMsg("系统异常：获取客户变动信息检查列表失败");
        }
        return resultDto;
    }
    
    /**
     * 获取融资变动
     * @param taskId
     * @return
     */
    @RequestMapping("getFinanceChangeCheckList.json")
    @ResponseBody
    public ResultDto getFinanceChangeCheckList(Integer taskId) {
        ResultDto resultDto = returnFail();
        try {
            Map<String,Object> params = new HashMap<>();
            params.put("taskId",taskId);
            params.put("checkType",Constants.LF_SYS_DIC.LF_CHANGE_FINANCE);
            List<LfChangeCheck> lfChangeCheckList = lfChangeCheckService.selectByProperty(params);
            resultDto = returnSuccess();
            resultDto.setRows(lfChangeCheckList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取融资变动信息检查列表失败！",e);
            resultDto.setMsg("系统异常：获取融资变动信息检查列表失败");
        }
        return resultDto;
    }
    /**
     * 获取对外担保变动信息
     * @param taskId
     * @return
     */
    @RequestMapping("getGuatChangeCheckList.json")
    @ResponseBody
    public ResultDto getGuatChangeCheckList(Integer taskId) {
        ResultDto resultDto = returnFail();
        try {
            Map<String,Object> params = new HashMap<>();
            params.put("taskId",taskId);
            params.put("checkType",Constants.LF_SYS_DIC.LF_CHANGE_GUAT);
            List<LfChangeCheck> lfChangeCheckList = lfChangeCheckService.selectByProperty(params);
            resultDto = returnSuccess();
            resultDto.setRows(lfChangeCheckList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取对外担保变动信息检查列表失败！",e);
            resultDto.setMsg("系统异常：获取对外担保变动信息检查列表失败");
        }
        return resultDto;
    }



    /**
     * 保存变动信息核查列表
     * @param changeCheckList
     * @return
     */
    @RequestMapping("saveChangeCheckList.json")
    @ResponseBody
    public ResultDto saveChangeCheckList(String changeCheckList){
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
        try {
            List<LfChangeCheck> saveList = JacksonUtil.fromJson(changeCheckList,new TypeReference<List<LfChangeCheck>>() {});
            for(LfChangeCheck lfChangeCheck : saveList){
                lfChangeCheck.setUpdater(curUser.getUserId());
                lfChangeCheck.setUpdateTime(DateUtil.getNowTimestamp());
            }
            lfChangeCheckService.batchSaveUpdata(saveList);
            resultDto = returnSuccess();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存变动信息检查列表失败！",e);
            resultDto.setMsg("系统异常：保存变动信息检查列表失败");
        }
        return resultDto;
    }
    
    /**
     * 保存首检贷后检查结论
     * @param fifteenDayResult 结论
     * @param taskId
     * @return
     */
    @RequestMapping("saveCustChangeReason.json")
    @ResponseBody
    public ResultDto saveCustChangeReason(String custChangeReason,Integer taskId){
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
        try {
            LfTaskResult lfTaskResult = lfTaskResultService.selectSingleByProperty("taskId",taskId);
            if(lfTaskResult == null){
                lfTaskResult = new LfTaskResult();
                lfTaskResult.setCreater(curUser.getUserId());
                lfTaskResult.setTaskId(taskId);
            }
            lfTaskResult.setCustChangeReason(custChangeReason);
            lfTaskResult.setUpdater(curUser.getUserId());
            lfTaskResult.setUpdateTime(DateUtil.getNowTimestamp());
            lfTaskResultService.saveOrUpdate(lfTaskResult);
            resultDto = returnSuccess();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存客户基本信息变化原因失败！",e);
            resultDto.setMsg("系统异常：保存客户基本信息变化原因失败！");
        }
        return resultDto;
    }
    
    /**
     * 获取财务状况检查细项列表
     * @param taskId
     * @return
     */
    @RequestMapping("getFinCheckList.json")
    @ResponseBody
    public ResultDto getFinCheckList(Integer taskId) {
        ResultDto resultDto = returnFail();
        try {
            Map<String,Object> params = new HashMap<>();
            params.put("taskId",taskId);
            List<LfFinCheck> lfFinCheckList = lfFinCheckService.selectByProperty(params);
            resultDto = returnSuccess();
            resultDto.setRows(lfFinCheckList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取财务状况检查细项信息列表失败！",e);
            resultDto.setMsg("系统异常：获取财务状况检查细项信息列表失败！");
        }
        return resultDto;
    }



    /**
     * 保存财务状况检查细项信息列表
     * @param changeCheckList
     * @return
     */
    @RequestMapping("saveFinCheckList.json")
    @ResponseBody
    public ResultDto saveFinCheckList(String finCheckList){
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
        try {
            List<LfFinCheck> saveList = JacksonUtil.fromJson(finCheckList,new TypeReference<List<LfFinCheck>>() {});
            for(LfFinCheck lfFinCheck : saveList){
            	lfFinCheck.setUpdater(curUser.getUserId());
            	lfFinCheck.setUpdateTime(DateUtil.getNowTimestamp());
            }
            lfFinCheckService.batchSaveUpdata(saveList);
            resultDto = returnSuccess();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存财务状况检查细项列表失败！",e);
            resultDto.setMsg("系统异常：保存财务状况检查细项列表失败");
        }
        return resultDto;
    }
    
    /**
     * 
     *@Description 获取经营检查信息列表
     *@param taskId
     *@return
     * @author xyh
     */
    @RequestMapping("getJYSituationCheckList.json")
    @ResponseBody
    public ResultDto getJYSituationCheckList(Integer taskId) {
        ResultDto resultDto = returnFail();
        try {
            List<LfSituationCheck> lfSituationCheckList = lfSituationCheckService.queryByTypeFirstLike("RG_LF_JY", taskId);
            resultDto = returnSuccess();
            resultDto.setRows(lfSituationCheckList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取经营情况检查列表失败！",e);
            resultDto.setMsg("系统异常：获取经营情况检查列表失败！");
        }
        return resultDto;
    }
    
    /**
     * 
     *@Description 保存检查信息列表
     *@param situationCheckList
     *@return
     * @author xyh
     */
    @RequestMapping("saveSituationCheckList.json")
    @ResponseBody
    public ResultDto saveSituationCheckList(String situationCheckList){
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
        try {
            List<LfSituationCheck> saveList = JacksonUtil.fromJson(situationCheckList,new TypeReference<List<LfSituationCheck>>() {});
            for(LfSituationCheck lfSituationCheck : saveList){
            	lfSituationCheck.setUpdater(curUser.getUserId());
            	lfSituationCheck.setUpdateTime(DateUtil.getNowTimestamp());
            }
            lfSituationCheckService.batchSaveUpdata(saveList);
            resultDto = returnSuccess();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存情况检查细项列表失败！",e);
            resultDto.setMsg("系统异常：保存情况检查细项列表失败");
        }
        return resultDto;
    }
    
    /**
     * 
     *@Description 获取担保情况检查列表
     *@param taskId
     *@return
     * @author xyh
     */
    @RequestMapping("getDBSituationCheckList.json")
    @ResponseBody
    public ResultDto getDBSituationCheckList(Integer taskId) {
        ResultDto resultDto = returnFail();
        try {
            List<LfSituationCheck> lfSituationCheckList = lfSituationCheckService.queryByTypeFirstLike("RG_LF_DB", taskId);
            resultDto = returnSuccess();
            resultDto.setRows(lfSituationCheckList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取担保情况检查列表失败！",e);
            resultDto.setMsg("系统异常：获取担保情况检查列表失败！");
        }
        return resultDto;
    }
    
    /**
     * 
     *@Description 保存风险评价信息
     *@param taskResult
     *@return
     * @author xyh
     */
    @RequestMapping("saveRiskEvaluation.json")
    @ResponseBody
    public ResultDto saveRiskEvaluation(String taskResult ){
    	ResultDto resultDto = returnFail();
    	User curUser = HttpUtil.getCurrentUser();
        try {
        	LfTaskResult lfTaskResult = JacksonUtil.fromJson(taskResult, LfTaskResult.class);
        	if(lfTaskResult == null || RgnshUtils.isEmptyInteger(lfTaskResult.getTaskId())){
        		ExceptionUtil.throwException("数据不完整，保存失败");
        	}
        	LfTaskResult saveTaskResult = lfTaskResultService.selectSingleByProperty("taskId", lfTaskResult.getTaskId());
        	if(saveTaskResult == null){
        		saveTaskResult = new LfTaskResult();
        		saveTaskResult.setTaskId(lfTaskResult.getTaskId());
        		saveTaskResult.setCreater(curUser.getUserId());
        		saveTaskResult.setUpdater(curUser.getUserId());
        		saveTaskResult.setUpdateTime(DateUtil.getNowTimestamp());
        	}
        	saveTaskResult.setUnLocalRiskSummary(lfTaskResult.getUnLocalRiskSummary());
        	saveTaskResult.setLocalRiskManaging(lfTaskResult.getLocalRiskManaging());
        	saveTaskResult.setLocalRiskBusiness(lfTaskResult.getLocalRiskBusiness());
        	saveTaskResult.setLocalRiskFinancial(lfTaskResult.getLocalRiskFinancial());
        	saveTaskResult.setLocalRiskGuarantee(lfTaskResult.getLocalRiskGuarantee());
        	saveTaskResult.setPreventiveMeasures(lfTaskResult.getPreventiveMeasures());
        	lfTaskResultService.saveOrUpdate(saveTaskResult);
        	resultDto = returnSuccess();
        } catch(BusinessException bException){
        	resultDto.setMsg(bException.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            log.error("获取风险评价信息失败！",e);
            resultDto.setMsg("系统异常：获取风险评价信息失败！");
        }
        return resultDto;
    }
    
    /**
     * 
     *@Description 获取风险信号列表
     *@param taskId
     *@return
     * @author xyh
     */
    @RequestMapping("getRiskWarningInfoList.json")
    @ResponseBody
    public ResultDto getRiskWarningInfoList(Integer taskId,String noOAP) {
        ResultDto resultDto = returnFail();
        try {
        	
        	Map<String, Object> params = new HashMap<>();
        	params.put("taskId", taskId);
        	if(!StringUtils.isEmpty(noOAP) && "yes".equals(noOAP)){
        		params.put("riskFrom", "FXYJ");//查询风险预警系统中的数据
        	}
        	//params.put("riskFrom", "FXYJ");//查询风险预警系统中的数据
            List<LfRiskWarningInfo> lfRiskWarningInfoList = lfRiskWarningInfoService.selectByProperty(params);
            resultDto = returnSuccess();
            resultDto.setRows(lfRiskWarningInfoList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取客户风险信号列表失败！",e);
            resultDto.setMsg("系统异常：获取客户风险信号列表失败！");
        }
        return resultDto;
    }
    /**
     * 
     *@Description 获取OAP的预警信息
     *@param taskId
     *@return
     * @author xyh
     */
    @RequestMapping("getOAPWarningInfoList.json")
    @ResponseBody
    public ResultDto getOAPWarningInfoList(Integer taskId) {
        ResultDto resultDto = returnFail();
        try {
        	
        	Map<String, Object> params = new HashMap<>();
        	params.put("taskId", taskId);
        	params.put("riskFrom", "OAP");//查询风险预警系统中的数据
            List<LfRiskWarningInfo> lfRiskWarningInfoList = lfRiskWarningInfoService.selectByProperty(params);
            resultDto = returnSuccess();
            resultDto.setRows(lfRiskWarningInfoList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取客户风险信号列表失败！",e);
            resultDto.setMsg("系统异常：获取客户风险信号列表失败！");
        }
        return resultDto;
    }
    
    
    
    /**
     * 
     *@Description 保存风险信号列表
     *@param riskWarningInfoList
     *@return
     * @author xyh
     */
    @RequestMapping("saveRiskWarningInfoList.json")
    @ResponseBody
    public ResultDto saveRiskWarningInfoList(String riskWarningInfoList){
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
        try {
            List<LfRiskWarningInfo> saveList = JacksonUtil.fromJson(riskWarningInfoList,new TypeReference<List<LfRiskWarningInfo>>() {});
            for(LfRiskWarningInfo lfRiskWarningInfo : saveList){
            	lfRiskWarningInfo.setUpdater(curUser.getUserId());
            	lfRiskWarningInfo.setUpdateTime(DateUtil.getNowTimestamp());
            }
            lfRiskWarningInfoService.batchSaveUpdata(saveList);
            resultDto = returnSuccess();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存客户风险信号列表失败！",e);
            resultDto.setMsg("系统异常：保存客户风险信号列表失败");
        }
        return resultDto;
    }
    
    /**
     * 
     *@Description 行政处罚信息
     *@param taskId
     *@return
     * @author xyh
     */
    @RequestMapping("getAdminSanInfoList.json")
    @ResponseBody
    public ResultDto getAdminSanInfoList(Integer taskId) {
        ResultDto resultDto = returnFail();
        try {
            List<LfAdminSanInfo> lfAdminSanInfoList = lfAdminSanInfoService.selectByProperty("taskId", taskId);
            resultDto = returnSuccess();
            resultDto.setRows(lfAdminSanInfoList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取行政处罚信息列表失败！",e);
            resultDto.setMsg("系统异常：获取行政处罚信息列表失败！");
        }
        return resultDto;
    }
    
    /**
     * 
     *@Description 保存行政处罚信息列表
     *@param riskWarningInfoList
     *@return
     * @author xyh
     */
    @RequestMapping("saveAdminSanInfoList.json")
    @ResponseBody
    public ResultDto saveAdminSanInfoList(String adminSanInfoList){
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
        try {
            List<LfAdminSanInfo> saveList = JacksonUtil.fromJson(adminSanInfoList,new TypeReference<List<LfAdminSanInfo>>() {});
            for(LfAdminSanInfo lfRiskWarningInfo : saveList){
            	lfRiskWarningInfo.setUpdater(curUser.getUserId());
            	lfRiskWarningInfo.setUpdateTime(DateUtil.getNowTimestamp());
            }
            lfAdminSanInfoService.batchSaveUpdata(saveList);
            resultDto = returnSuccess();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存行政处罚信息列表失败！",e);
            resultDto.setMsg("系统异常：保存行政处罚信息列表失败");
        }
        return resultDto;
    }
    
    /**
     * 
     *@Description 删除新政处罚信息
     *@param id
     *@return
     * @author xyh
     */
    @RequestMapping("delAdminSanInfo.json")
    @ResponseBody
    public ResultDto delAdminSanInfo(Integer id){
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
        try {
        	LfAdminSanInfo lfAdminSanInfo = lfAdminSanInfoService.findByPrimaryKey(id);
        	if(lfAdminSanInfo == null){
        		ExceptionUtil.throwException("未查询到数据，数据编号为："+ id);
        	}
        	lfAdminSanInfo.setUpdater(curUser.getUserId());
        	lfAdminSanInfo.setUpdateTime(DateUtil.getNowTimestamp());
        	lfAdminSanInfo.setStatus(Constants.DELETE);
        	lfAdminSanInfoService.saveOrUpdate(lfAdminSanInfo);
            resultDto = returnSuccess();
        }catch(BusinessException be){
        	log.error("运行异常:",be);
            resultDto.setMsg("运行异常：" + be.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            log.error("保存行政处罚信息列表失败！",e);
            resultDto.setMsg("系统异常：保存行政处罚信息列表失败");
        }
        return resultDto;
    }
    
    /**
     * 
     *@Description 获取账户查冻扣信息
     *@param taskId
     *@return
     * @author xyh
     */
    @RequestMapping("getCfdAccountInfoList.json")
    @ResponseBody
    public ResultDto getCfdAccountInfoList(Integer taskId) {
        ResultDto resultDto = returnFail();
        try {
            List<LfCfdAccountInfo> lfCfdAccountInfoList = lfCfdAccountInfoService.selectByProperty("taskId", taskId);
            resultDto = returnSuccess();
            resultDto.setRows(lfCfdAccountInfoList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取账户查冻扣信息列表失败！",e);
            resultDto.setMsg("系统异常：获取账户查冻扣信息列表失败！");
        }
        return resultDto;
    }
    
    /**
     * 
     *@Description 保存账户查冻扣
     *@param riskWarningInfoList
     *@return
     * @author xyh
     */
    @RequestMapping("saveCfdAccountInfoList.json")
    @ResponseBody
    public ResultDto saveCfdAccountInfoList(String cfdAccountInfoList){
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
        try {
            List<LfCfdAccountInfo> saveList = JacksonUtil.fromJson(cfdAccountInfoList,new TypeReference<List<LfCfdAccountInfo>>() {});
            for(LfCfdAccountInfo lfRiskWarningInfo : saveList){
            	lfRiskWarningInfo.setUpdater(curUser.getUserId());
            	lfRiskWarningInfo.setUpdateTime(DateUtil.getNowTimestamp());
            }
            lfCfdAccountInfoService.batchSaveUpdata(saveList);
            resultDto = returnSuccess();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存账户查冻扣列表失败！",e);
            resultDto.setMsg("系统异常：保存账户查冻扣列表失败");
        }
        return resultDto;
    }
    
    /**
     * 
     *@Description 删除账户查冻扣信息
     *@param cfdAccountInfoList
     *@return
     * @author xyh
     */
    @RequestMapping("delCfdAccountInfo.json")
    @ResponseBody
    public ResultDto delCfdAccountInfoList(Integer id){
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
        try {
        	LfCfdAccountInfo lfCfdAccountInfo = lfCfdAccountInfoService.findByPrimaryKey(id);
        	if(lfCfdAccountInfo == null){
        		ExceptionUtil.throwException("未查询到数据，查询id为："+id);
        	}
        	lfCfdAccountInfo.setUpdater(curUser.getUserId());
        	lfCfdAccountInfo.setUpdateTime(DateUtil.getNowTimestamp());
        	lfCfdAccountInfo.setStatus(Constants.DELETE);
        	lfCfdAccountInfoService.saveOrUpdate(lfCfdAccountInfo);
            resultDto = returnSuccess();
        }catch (BusinessException be) {
            log.error("运行异常",be);
            resultDto.setMsg("运行异常：" + be.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存账户查冻扣列表失败！",e);
            resultDto.setMsg("系统异常：保存账户查冻扣列表失败");
        }
        return resultDto;
    }
    
    
    /**
     * 
     *@Description 获取内控名单
     *@param taskId
     *@return
     * @author xyh
     */
    @RequestMapping("getLfInternalControlInfoList.json")
    @ResponseBody
    public ResultDto getLfInternalControlInfoList(Integer taskId) {
        ResultDto resultDto = returnFail();
        try {
            List<LfInternalControlInfo> lfInternalControlInfoList = lfInternalControlInfoService.selectByProperty("taskId", taskId);
            resultDto = returnSuccess();
            resultDto.setRows(lfInternalControlInfoList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取内控名单列表失败！",e);
            resultDto.setMsg("系统异常：获取内控名单列表失败！");
        }
        return resultDto;
    }
    
    /**
     * 
     *@Description 保存内控名单信息
     *@param riskWarningInfoList
     *@return
     * @author xyh
     */
    @RequestMapping("saveInternalControlInfoList.json")
    @ResponseBody
    public ResultDto saveInternalControlInfoList(String internalControlInfoList){
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
        try {
            List<LfInternalControlInfo> saveList = JacksonUtil.fromJson(internalControlInfoList,new TypeReference<List<LfInternalControlInfo>>() {});
            for(LfInternalControlInfo lfRiskWarningInfo : saveList){
            	lfRiskWarningInfo.setUpdater(curUser.getUserId());
            	lfRiskWarningInfo.setUpdateTime(DateUtil.getNowTimestamp());
            }
            lfInternalControlInfoService.batchSaveUpdata(saveList);
            resultDto = returnSuccess();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存内控名单信息列表失败！",e);
            resultDto.setMsg("系统异常：保存内控名单信息列表失败");
        }
        return resultDto;
    }
    /**
     * 
     *@Description 删除内控名单信息
     *@param id
     *@return
     * @author xyh
     */
    @RequestMapping("delInternalControlInfo.json")
    @ResponseBody
    public ResultDto delInternalControlInfo(Integer id){
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
        try {
        	LfInternalControlInfo lfInternalControlInfo = lfInternalControlInfoService.findByPrimaryKey(id);
        	if(lfInternalControlInfo == null){
        		ExceptionUtil.throwException("未查询到数据，查询id为："+id);
        	}
        	lfInternalControlInfo.setUpdater(curUser.getUserId());
        	lfInternalControlInfo.setUpdateTime(DateUtil.getNowTimestamp());
        	lfInternalControlInfo.setStatus(Constants.DELETE);
        	lfInternalControlInfoService.saveOrUpdate(lfInternalControlInfo);
            resultDto = returnSuccess();
        }catch(BusinessException be){
        	 log.error("运行异常",be);
             resultDto.setMsg("运行异常：" + be.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除内控名单信息失败！",e);
            resultDto.setMsg("系统异常：删除内控名单信息失败");
        }
        return resultDto;
    }
    
    /**
     * 
     *@Description 保存非现场调查的信息
     *@param localResult
     *@return
     * @author xyh
     */
    @RequestMapping("saveLocalResult.json")
    @ResponseBody
    public ResultDto saveLocalResult(String localResult){
    	ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
        try {
        	LfLocalResult lfLocalResult = JacksonUtil.fromJson(localResult, LfLocalResult.class);
        	if(lfLocalResult == null){
        		ExceptionUtil.throwException("保存数据为空");
        	}
        	LfLocalResult saveLocalResult = lfLocalResultService.selectSingleByProperty("taskId",lfLocalResult.getTaskId());
        	if(saveLocalResult == null){
        		saveLocalResult = new LfLocalResult();
        		saveLocalResult.setTaskId(lfLocalResult.getTaskId());
        		saveLocalResult.setCreater(curUser.getUserId());
        	}
        	saveLocalResult.setCaseCount(lfLocalResult.getCaseCount());
        	saveLocalResult.setIsEnforce(lfLocalResult.getIsEnforce());
        	saveLocalResult.setIsBreachTrust(lfLocalResult.getIsBreachTrust());
        	saveLocalResult.setLocalOption(lfLocalResult.getLocalOption());
        	saveLocalResult.setOtherInfo(lfLocalResult.getOtherInfo());
        	saveLocalResult.setUpdater(curUser.getUserId());
        	saveLocalResult.setUpdateTime(DateUtil.getNowTimestamp());
        	lfLocalResultService.saveOrUpdate(saveLocalResult);
            resultDto = returnSuccess();
        }catch(BusinessException be){
        	 log.error("运行异常",be);
             resultDto.setMsg("运行异常：" + be.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存失败！",e);
            resultDto.setMsg("系统异常：保存失败");
        }
        return resultDto;
    }
    
    
    /**
     * 
     *@Description 获取费用缴纳情况分析
     *@param taskId
     *@return
     * @author xyh
     */
    @RequestMapping("getLfPaymentOfFeesList.json")
    @ResponseBody
    public ResultDto getLfPaymentOfFeesList(Integer taskId) {
        ResultDto resultDto = returnFail();
        try {
        	List<LfPaymentOfFees> lfPaymentOfFeeList = lfPaymentOfFeesService.selectByProperty("taskId", taskId);
        	resultDto = returnSuccess();
            resultDto.setRows(lfPaymentOfFeeList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取费用缴纳情况分析列表失败！",e);
            resultDto.setMsg("系统异常：获取费用缴纳情况分析失败！");
        }
        return resultDto;
    }
    
    /**
     * 
     *@Description 保存费用缴纳情况分析列表
     *@param riskWarningInfoList
     *@return
     * @author xyh
     */
    @RequestMapping("saveLfPaymentOfFeesList.json")
    @ResponseBody
    public ResultDto saveLfPaymentOfFeesList(String paymentOfFeesList){
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
        try {
        	List<LfPaymentOfFees> saveList = JacksonUtil.fromJson(paymentOfFeesList,new TypeReference<List<LfPaymentOfFees>>() {});
        	for(LfPaymentOfFees lfPaymentOfFees : saveList){
        		lfPaymentOfFees.setUpdater(curUser.getUserId());
        		lfPaymentOfFees.setUpdateTime(DateUtil.getNowTimestamp());
            }
        	lfPaymentOfFeesService.batchSaveUpdata(saveList);
        	resultDto = returnSuccess();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存费用缴纳情况分析列表失败！",e);
            resultDto.setMsg("系统异常：保存费用缴纳情况分析列表失败");
        }
        return resultDto;
    }
    
    
    @RequestMapping("getQTSituationCheckList.json")
    @ResponseBody
    public ResultDto getQTSituationCheckList(Integer taskId) {
        ResultDto resultDto = returnFail();
        try {
            List<LfSituationCheck> lfSituationCheckList = lfSituationCheckService.queryByTypeFirstLike("RG_LF_QT", taskId);
            resultDto = returnSuccess();
            resultDto.setRows(lfSituationCheckList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取其他现场检查列表失败！",e);
            resultDto.setMsg("系统异常：获取其他现场检查列表失败！");
        }
        return resultDto;
    }
    
    @RequestMapping("saveSituationQTRe.json")
    @ResponseBody
    public ResultDto saveSituationQTRe(String taskResult ){
    	ResultDto resultDto = returnFail();
    	User curUser = HttpUtil.getCurrentUser();
        try {
        	LfTaskResult lfTaskResult = JacksonUtil.fromJson(taskResult, LfTaskResult.class);
        	if(lfTaskResult == null || RgnshUtils.isEmptyInteger(lfTaskResult.getTaskId())){
        		ExceptionUtil.throwException("数据不完整，保存失败");
        	}
        	LfTaskResult saveTaskResult = lfTaskResultService.selectSingleByProperty("taskId", lfTaskResult.getTaskId());
        	if(saveTaskResult == null){
        		saveTaskResult = new LfTaskResult();
        		saveTaskResult.setTaskId(lfTaskResult.getTaskId());
        		saveTaskResult.setCreater(curUser.getUserId());
        		saveTaskResult.setUpdater(curUser.getUserId());
        		saveTaskResult.setUpdateTime(DateUtil.getNowTimestamp());
        	}
        	saveTaskResult.setAnalysisReason(lfTaskResult.getAnalysisReason());
        	saveTaskResult.setOtherCheckInfo(lfTaskResult.getOtherCheckInfo());
        	saveTaskResult.setVerificativeInfo(lfTaskResult.getVerificativeInfo());
        	lfTaskResultService.saveOrUpdate(saveTaskResult);
        	resultDto = returnSuccess();
        } catch(BusinessException bException){
        	resultDto.setMsg(bException.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            log.error("获取其他现场检查结论失败！",e);
            resultDto.setMsg("系统异常：获取其他现场检查结论失败！");
        }
        return resultDto;
    }
    
    @RequestMapping("getXMSituationCheckList.json")
    @ResponseBody
    public ResultDto getXMSituationCheckList(Integer taskId) {
        ResultDto resultDto = returnFail();
        try {
            List<LfSituationCheck> lfSituationCheckList = lfSituationCheckService.queryByTypeFirstLike("RG_LF_XM", taskId);
            resultDto = returnSuccess();
            resultDto.setRows(lfSituationCheckList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取项目存在的主要风险列表失败！",e);
            resultDto.setMsg("系统异常：获取项目存在的主要风险列表失败！");
        }
        return resultDto;
    }
    
    @RequestMapping("getProjectInfoList.json")
    @ResponseBody
    public ResultDto getProjectInfoList(Integer taskId){
    	ResultDto resultDto = returnFail();
    	 try {
    		 List<LfProjectInfo> lfProjectInfoList = lfProjectInfoService.selectByProperty("taskId", taskId);
    		 resultDto = returnSuccess();
             resultDto.setRows(lfProjectInfoList);
         } catch (Exception e) {
             e.printStackTrace();
             log.error("获取在建或已完工为完全回款的主要项目情况列表失败！",e);
             resultDto.setMsg("系统异常：获取在建或已完工为完全回款的主要项目情况列表失败！");
         }
         return resultDto;
    }
    
    @RequestMapping("saveProjectInfoList.json")
    @ResponseBody
    public ResultDto saveProjectInfoList(String projectInfoList){
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
        try {
            List<LfProjectInfo> saveList = JacksonUtil.fromJson(projectInfoList,new TypeReference<List<LfProjectInfo>>() {});
            for(LfProjectInfo lfProjectInfo : saveList){
            	lfProjectInfo.setUpdater(curUser.getUserId());
            	lfProjectInfo.setUpdateTime(DateUtil.getNowTimestamp());
            }
            lfProjectInfoService.batchSaveUpdata(saveList);
            resultDto = returnSuccess();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存在建或已完工为完全回款的主要项目情况列表失败！",e);
            resultDto.setMsg("系统异常：保存在建或已完工为完全回款的主要项目情况列表失败");
        }
        return resultDto;
    }
    
    /**
     * 
     *@Description 删除账户查冻扣信息
     *@param cfdAccountInfoList
     *@return
     * @author xyh
     */
    @RequestMapping("delProjectInfoList.json")
    @ResponseBody
    public ResultDto delProjectInfoList(Integer id){
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
        try {
        	LfProjectInfo lfProjectInfo = lfProjectInfoService.findByPrimaryKey(id);
        	if(lfProjectInfo == null){
        		ExceptionUtil.throwException("未查询到数据，查询id为："+id);
        	}
        	lfProjectInfo.setUpdater(curUser.getUserId());
        	lfProjectInfo.setUpdateTime(DateUtil.getNowTimestamp());
        	lfProjectInfo.setStatus(Constants.DELETE);
        	lfProjectInfoService.saveOrUpdate(lfProjectInfo);
            resultDto = returnSuccess();
        }catch (BusinessException be) {
            log.error("运行异常",be);
            resultDto.setMsg("运行异常：" + be.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除在建或已完工为完全回款的主要项目情况失败！",e);
            resultDto.setMsg("系统异常：删除在建或已完工为完全回款的主要项目情况失败");
        }
        return resultDto;
    }
    
    @RequestMapping("getLoanProjectDetailList.json")
    @ResponseBody
    public ResultDto getLoanProjectDetailList(Integer taskId){
    	ResultDto resultDto = returnFail();
    	 try {
    		 List<LfLoanProjectDetail> lfLoanProjectDetailList = lfLoanProjectDetailService.selectByProperty("taskId", taskId);
    		 resultDto = returnSuccess();
             resultDto.setRows(lfLoanProjectDetailList);
         } catch (Exception e) {
             e.printStackTrace();
             log.error("获取申贷项目概况细项失败！",e);
             resultDto.setMsg("系统异常：获取申贷项目概况细项失败！");
         }
         return resultDto;
    }
    
    @RequestMapping("saveLoanProjectDetailList.json")
    @ResponseBody
    public ResultDto saveLoanProjectDetailList(String loanProjectDetailList){
        ResultDto resultDto = returnFail();
        User curUser = HttpUtil.getCurrentUser();
        try {
            List<LfLoanProjectDetail> saveList = JacksonUtil.fromJson(loanProjectDetailList,new TypeReference<List<LfLoanProjectDetail>>() {});
            for(LfLoanProjectDetail lfLoanProjectDetail : saveList){
            	lfLoanProjectDetail.setUpdater(curUser.getUserId());
            	lfLoanProjectDetail.setUpdateTime(DateUtil.getNowTimestamp());
            }
            lfLoanProjectDetailService.batchSaveUpdata(saveList);
            resultDto = returnSuccess();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("保存申贷项目概况细项失败！",e);
            resultDto.setMsg("系统异常：保存申贷项目概况细项失败");
        }
        return resultDto;
    }
    
    @RequestMapping("saveLoanProjectSummary.json")
    @ResponseBody
    public ResultDto saveLoanProjectSummary(String loanProjectSummary){
    	ResultDto resultDto = returnFail();
    	User curUser = HttpUtil.getCurrentUser();
        try {
        	LfLoanProjectSummary lfLoanProjectSummary = JacksonUtil.fromJson(loanProjectSummary, LfLoanProjectSummary.class);
        	if(lfLoanProjectSummary == null || RgnshUtils.isEmptyInteger(lfLoanProjectSummary.getTaskId())){
        		ExceptionUtil.throwException("数据不完整，保存失败");
        	}
        	LfLoanProjectSummary saveEntity = lfLoanProjectSummaryService.selectSingleByProperty("taskId", lfLoanProjectSummary.getTaskId());
        	if(saveEntity == null){
        		saveEntity = new LfLoanProjectSummary();
        		saveEntity.setTaskId(lfLoanProjectSummary.getTaskId());
        		saveEntity.setCreater(curUser.getUserId());
        		saveEntity.setUpdater(curUser.getUserId());
        		saveEntity.setUpdateTime(DateUtil.getNowTimestamp());
        	}
        	saveEntity.setProjectSiteCondition(lfLoanProjectSummary.getProjectSiteCondition());
        	saveEntity.setIntroduceOwner(lfLoanProjectSummary.getIntroduceOwner());
        	lfLoanProjectSummaryService.saveOrUpdate(saveEntity);
        	resultDto = returnSuccess();
        } catch(BusinessException bException){
        	resultDto.setMsg(bException.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            log.error("获取其他现场检查结论失败！",e);
            resultDto.setMsg("系统异常：获取其他现场检查结论失败！");
        }
        return resultDto;
    }
    
    
    @RequestMapping("saveProConstructCommit.json")
    @ResponseBody
    public ResultDto saveProConstructCommit(String proConstructCommit){
    	ResultDto resultDto = returnFail();
    	User curUser = HttpUtil.getCurrentUser();
        try {
        	LfProConstructCommit lfProConstructCommit = JacksonUtil.fromJson(proConstructCommit,LfProConstructCommit.class);
        	if(lfProConstructCommit == null || RgnshUtils.isEmptyInteger(lfProConstructCommit.getTaskId())){
        		ExceptionUtil.throwException("数据不完整，保存失败");
        	}
        	LfProConstructCommit saveEntity = lfProConstructCommitService.selectSingleByProperty("taskId", lfProConstructCommit.getTaskId());
        	if(saveEntity == null){
        		saveEntity = new LfProConstructCommit();
        		saveEntity.setTaskId(lfProConstructCommit.getTaskId());
        		saveEntity.setCreater(curUser.getUserId());
        		saveEntity.setUpdater(curUser.getUserId());
        		saveEntity.setUpdateTime(DateUtil.getNowTimestamp());
        	}
        	saveEntity.setTotalInvestment(lfProConstructCommit.getTotalInvestment());
        	saveEntity.setConstructionPeriod(lfProConstructCommit.getConstructionPeriod());
        	saveEntity.setOwnFundsInvested(lfProConstructCommit.getOwnFundsInvested());
        	saveEntity.setProjectProgress(lfProConstructCommit.getProjectProgress());
        	saveEntity.setExternalFundsInvested(lfProConstructCommit.getExternalFundsInvested());
        	saveEntity.setOtherFundsInvested(lfProConstructCommit.getOtherFundsInvested());
        	saveEntity.setProInvestmentGap(lfProConstructCommit.getProInvestmentGap());
        	saveEntity.setProspectForecast(lfProConstructCommit.getProspectForecast());
        	saveEntity.setOverallSituation(lfProConstructCommit.getOverallSituation());
        	lfProConstructCommitService.saveOrUpdate(saveEntity);
        	resultDto = returnSuccess();
        } catch(BusinessException bException){
        	resultDto.setMsg(bException.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            log.error("获取其他现场检查结论失败！",e);
            resultDto.setMsg("系统异常：获取其他现场检查结论失败！");
        }
        return resultDto;
    }
    
    
    @RequestMapping("getFinDownSituationCheckList.json")
    @ResponseBody
    public ResultDto getFinDownSituationCheckList(Integer taskId) {
        ResultDto resultDto = returnFail();
        try {
            List<LfSituationCheck> lfSituationCheckList = lfSituationCheckService.queryByTypeFirstLike("RG_LF_FIN_01", taskId);
            resultDto = returnSuccess();
            resultDto.setRows(lfSituationCheckList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取其他现场检查列表失败！",e);
            resultDto.setMsg("系统异常：获取其他现场检查列表失败！");
        }
        return resultDto;
    }
    
    @RequestMapping("getJZDownSituationCheckList.json")
    @ResponseBody
    public ResultDto getJZDownSituationCheckList(Integer taskId) {
        ResultDto resultDto = returnFail();
        try {
            List<LfSituationCheck> lfSituationCheckList = lfSituationCheckService.queryByTypeFirstLike("RG_LF_JZ_01", taskId);
            resultDto = returnSuccess();
            resultDto.setRows(lfSituationCheckList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取其他现场检查列表失败！",e);
            resultDto.setMsg("系统异常：获取其他现场检查列表失败！");
        }
        return resultDto;
    }
    
    @RequestMapping("getJYDownSituationCheckList.json")
    @ResponseBody
    public ResultDto getJYDownSituationCheckList(Integer taskId) {
        ResultDto resultDto = returnFail();
        try {
            List<LfSituationCheck> lfSituationCheckList = lfSituationCheckService.queryByTypeFirstLike("RG_LF_JY_08", taskId);
            resultDto = returnSuccess();
            resultDto.setRows(lfSituationCheckList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取其他现场检查列表失败！",e);
            resultDto.setMsg("系统异常：获取其他现场检查列表失败！");
        }
        return resultDto;
    }
    
    @RequestMapping("saveDownCheckInfoJY.json")
    @ResponseBody
    public ResultDto saveDownCheckInfoJY(String taskResult){
    	ResultDto resultDto = returnFail();
    	User curUser = HttpUtil.getCurrentUser();
        try {
        	
        	LfDownCheckInfo lfDownCheckInfo = JacksonUtil.fromJson(taskResult, LfDownCheckInfo.class);
        	if(lfDownCheckInfo == null || RgnshUtils.isEmptyInteger(lfDownCheckInfo.getTaskId())){
        		ExceptionUtil.throwException("数据不完整，保存失败");
        	}
        	LfDownCheckInfo saveEntity = lfDownCheckInfoService.selectSingleByProperty("taskId", lfDownCheckInfo.getTaskId());
        	if(saveEntity == null){
        		saveEntity = new LfDownCheckInfo();
        		saveEntity.setTaskId(lfDownCheckInfo.getTaskId());
        		saveEntity.setCreater(curUser.getUserId());
        		saveEntity.setUpdater(curUser.getUserId());
        		saveEntity.setUpdateTime(DateUtil.getNowTimestamp());
        	}
        	saveEntity.setBusinessInfo(lfDownCheckInfo.getBusinessInfo());
        	lfDownCheckInfoService.saveOrUpdate(saveEntity);
        	resultDto = returnSuccess();
        } catch(BusinessException bException){
        	resultDto.setMsg(bException.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            log.error("获取其他现场检查结论失败！",e);
            resultDto.setMsg("系统异常：获取其他现场检查结论失败！");
        }
        return resultDto;
    }
    
    @RequestMapping("saveDownCheckInfoJZ.json")
    @ResponseBody
    public ResultDto saveDownCheckInfoJZ(String taskResult){
    	ResultDto resultDto = returnFail();
    	User curUser = HttpUtil.getCurrentUser();
        try {
        	
        	LfDownCheckInfo lfDownCheckInfo = JacksonUtil.fromJson(taskResult, LfDownCheckInfo.class);
        	if(lfDownCheckInfo == null || RgnshUtils.isEmptyInteger(lfDownCheckInfo.getTaskId())){
        		ExceptionUtil.throwException("数据不完整，保存失败");
        	}
        	LfDownCheckInfo saveEntity = lfDownCheckInfoService.selectSingleByProperty("taskId", lfDownCheckInfo.getTaskId());
        	if(saveEntity == null){
        		saveEntity = new LfDownCheckInfo();
        		saveEntity.setTaskId(lfDownCheckInfo.getTaskId());
        		saveEntity.setCreater(curUser.getUserId());
        		saveEntity.setUpdater(curUser.getUserId());
        		saveEntity.setUpdateTime(DateUtil.getNowTimestamp());
        	}
        	saveEntity.setBuildInfo(lfDownCheckInfo.getBuildInfo());
        	lfDownCheckInfoService.saveOrUpdate(saveEntity);
        	resultDto = returnSuccess();
        } catch(BusinessException bException){
        	resultDto.setMsg(bException.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            log.error("获取其他现场检查结论失败！",e);
            resultDto.setMsg("系统异常：获取其他现场检查结论失败！");
        }
        return resultDto;
    }
    
    @RequestMapping("saveDownCheckInfoBase.json")
    @ResponseBody
    public ResultDto saveDownCheckInfoBase(String taskResult){
    	ResultDto resultDto = returnFail();
    	User curUser = HttpUtil.getCurrentUser();
        try {
        	
        	LfDownCheckInfo lfDownCheckInfo = JacksonUtil.fromJson(taskResult, LfDownCheckInfo.class);
        	if(lfDownCheckInfo == null || RgnshUtils.isEmptyInteger(lfDownCheckInfo.getTaskId())){
        		ExceptionUtil.throwException("数据不完整，保存失败");
        	}
        	LfDownCheckInfo saveEntity = lfDownCheckInfoService.selectSingleByProperty("taskId", lfDownCheckInfo.getTaskId());
        	if(saveEntity == null){
        		saveEntity = new LfDownCheckInfo();
        		saveEntity.setTaskId(lfDownCheckInfo.getTaskId());
        		saveEntity.setCreater(curUser.getUserId());
        		saveEntity.setUpdater(curUser.getUserId());
        		saveEntity.setUpdateTime(DateUtil.getNowTimestamp());
        	}
        	saveEntity.setBaseMainChange(lfDownCheckInfo.getBaseMainChange());
        	saveEntity.setCredentiasCheck(lfDownCheckInfo.getCredentiasCheck());
        	saveEntity.setCheckInfo(lfDownCheckInfo.getCheckInfo());
        	lfDownCheckInfoService.saveOrUpdate(saveEntity);
        	resultDto = returnSuccess();
        } catch(BusinessException bException){
        	resultDto.setMsg(bException.getMessage());
        }catch (Exception e) {
            e.printStackTrace();
            log.error("获取其他现场检查结论失败！",e);
            resultDto.setMsg("系统异常：获取其他现场检查结论失败！");
        }
        return resultDto;
    }
    
    /**
     * 
     *@Description 获得本行用信情况
     *@param taskId
     *@return
     * @author xyh
     */
    @RequestMapping("getCreditSelfBankList.json")
    @ResponseBody
    public ResultDto getCreditSelfBankList(Integer taskId) {
        ResultDto resultDto = returnFail();
        try {
            List<LfCreditSelfBank> lfCreditSelfBankList = lfCreditSelfBankService.selectByProperty("taskId", taskId);
        	if(!CollectionUtils.isEmpty(lfCreditSelfBankList)){
	            for(LfCreditSelfBank lfcsb : lfCreditSelfBankList){
	            	if(lfcsb.getGuateeWay() != null){
		            	if(lfcsb.getGuateeWay().contains("A")){
		            		lfcsb.setGuateeWay("质押");
		            	}else if(lfcsb.getGuateeWay().contains("B")){
		            		lfcsb.setGuateeWay("抵押");
		            	}else if(lfcsb.getGuateeWay().contains("C")){
		            		lfcsb.setGuateeWay("保证");
		            	}else if(lfcsb.getGuateeWay().contains("D")){
		            		lfcsb.setGuateeWay("信用/免担保");
		            	}else if(lfcsb.getGuateeWay().contains("E")){
		            		lfcsb.setGuateeWay("组合担保");
		            	}else if(lfcsb.getGuateeWay().contains("F")){
		            		lfcsb.setGuateeWay("纸质押");
		            	}else if(lfcsb.getGuateeWay().contains("G")){
		            		lfcsb.setGuateeWay("票据贴现");
		            	}else if(lfcsb.getGuateeWay().contains("H")){
		            		lfcsb.setGuateeWay("保证金");
		            	}else if(lfcsb.getGuateeWay().contains("Z")){
		            		lfcsb.setGuateeWay("其他");
		            	}else{
		            		lfcsb.setGuateeWay("未说明");
		            	}
	            	}
	            }
        	}
            resultDto = returnSuccess();
            resultDto.setRows(lfCreditSelfBankList);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取本行用信情况列表失败！",e);
            resultDto.setMsg("系统异常：获取本行用信情况列表失败！");
        }
        return resultDto;
    }
    
    ///***************报告********************//
    
    /**
     * 
     *@Description 下载贷后调查报告
     *@param request
     *@param taskId
     *@return
     * @author xyh
     */
    @RequestMapping("downloadLfReport.json")
    @LoanAopLog(operationName="下载贷后检查报告")
    public ResponseEntity<byte[]> downloadLcReport(HttpServletRequest request,Integer taskId) {
        ResponseEntity<byte[]> result = null;
        //模板数据
        Map<String, Object> dataMap = new HashMap<>();
        if(RgnshUtils.isEmptyInteger(taskId)){
            log.error("参数信息为空，请排查原因");
            return result;
        }
        try {
            Map<String, Object> lfInfos = lfReportService.getReportInfo(taskId);
            dataMap.put("lfInfo", lfInfos);
            //获取调查报告的报告地址
            String templateName = lfInfos.get("reportPath").toString();
            LfTask lfTask = (LfTask) lfInfos.get("lfTask");
            
            String fileName = lfTask.getCustomerName() + "贷后调查报告.doc";
            String destPath = AppConfig.Cfg.REPORT_PATH
                    + DateUtil.formatDate(new Date(), "yyyy-MM" );
            File file = new File(destPath , fileName);
            if(!file.getParentFile().exists())
                file.getParentFile().mkdirs();
            FreeMarkerWordUtil.createWord(dataMap, templateName, destPath, file.getName());
            HttpHeaders headers = new HttpHeaders();
            headers.setContentDispositionFormData("attachment", CodeUtil.parseGBK(fileName));
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            result = new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("下载贷前调查报告文件异常：", e);
        }
        return result;
    }
    
    /**
     * 
     *@Description 客户分类
     *@param page
     *@param rows
     *@return
     * @author xyh
     */
    @RequestMapping("getCustClassiList.json")
	@ResponseBody
	@LoanAopLog(operationName="查询客户分类列表")
	public ResultDto getCustClassiList(int page, int rows,LfClassifyVo condition) {
		ResultDto resultDto = returnFail();	
		try {
			Pagination<LfCustDto> pager = lfCustInfoService.getLfCustPage(page, rows, condition);
			resultDto = returnSuccess();
			resultDto.setRows(pager.getItems());
			resultDto.setTotal(pager.getRowsCount());
		} catch (Exception e) {
			e.printStackTrace();
			log.error("获取待调查贷后任务失败！", e);
			resultDto.setMsg("获取待调查贷后任务失败");
		}
		return resultDto;
	}
    
    
    @RequestMapping("downCustClassiExcel.json")
    @LoanAopLog(operationName="下载贷后客户分类台帐")
    public ResponseEntity<byte[]> downCustClassiExcel() {
        ResponseEntity<byte[]> result = null;
        //模板数据
        try {
        	//获取所有的客户分类数据
        	List<LfCustDto> excelDatas = lfCustInfoService.getLfCustList();
        	if(!CollectionUtils.isEmpty(excelDatas)){
	        	for(LfCustDto lfCust : excelDatas){
	        		
		            if(("01").equals(lfCust.getRiskClassi())){
		            	lfCust.setRiskClassi("正常类");
		            }else if(("02").equals(lfCust.getRiskClassi())){
		            	lfCust.setRiskClassi("关注类");
		            }else if(("03").equals(lfCust.getRiskClassi())){
		            	lfCust.setRiskClassi("风险一级");
		            }else if(("04").equals(lfCust.getRiskClassi())){
		            	lfCust.setRiskClassi("风险二级");
		            }else if(("05").equals(lfCust.getRiskClassi())){
		            	lfCust.setRiskClassi("风险三级");
		            }
		           
	        		
	            	if(("06").equals(lfCust.getCreditClassi())){
	            		lfCust.setCreditClassi("增加授信");
	            	}else if(("07").equals(lfCust.getCreditClassi())){
	            		lfCust.setCreditClassi("维持授信");
	            	}else if(("08").equals(lfCust.getCreditClassi())){
	            		lfCust.setCreditClassi("减少授信");
	            	}else if(("09").equals(lfCust.getCreditClassi())){
	            		lfCust.setCreditClassi("退出授信");
	            	}
	            }
        	}
        	//创建一个xls的excel文件
        	HSSFWorkbook workbook = new HSSFWorkbook();
        	//创建一个sheet页
        	HSSFSheet sheet = workbook.createSheet("客户分类台帐");
        	//设置sheet页默认行高
        	sheet.setDefaultRowHeightInPoints(20);
        	//第一行----标题
        	String[] heads ={"客户姓名","风险分类","授信分类"};
            ExcelUtil.createHeadRow(0,ExcelUtil.getHeadCellStyle(workbook), sheet,heads);
            //内容行通用的样式
            CellStyle cellStyle = ExcelUtil.getCellStyle(workbook);
            //下面是不是可以加一个根据字段名获取数据
            for(int i = 0 ;i<excelDatas.size() ;i++){
            	Row row = sheet.createRow(i+1);
            	Cell cell1 = row.createCell(0);
            	cell1.setCellValue(excelDatas.get(i).getCustomerName());
            	cell1.setCellStyle(cellStyle);
            	
            	Cell cell2= row.createCell(1);
            	cell2.setCellValue(excelDatas.get(i).getRiskClassi());
            	cell2.setCellStyle(cellStyle);
            	
            	Cell cell3 = row.createCell(2);
            	cell3.setCellValue(excelDatas.get(i).getCreditClassi());
            	cell3.setCellStyle(cellStyle);
            }
            //设定每行的宽度
            sheet.setColumnWidth(0,20*512);
    		sheet.setColumnWidth(1,10*512);
    		sheet.setColumnWidth(2, 10*512);
    		
    		String dir = PropertiesUtil.getProperty(Constants.SYSTEM_PROPS_PATH, "data.temp.path");//本地缓存地址
            File exl = new File(dir + "客户分类台帐.xls");
            if(!exl.getParentFile().exists())
            	exl.getParentFile().mkdirs();
            FileOutputStream fileOut = new FileOutputStream(exl);
			workbook.write(fileOut);// 将workbook中的 内容写入fileOut
			fileOut.flush();
			fileOut.close();
			HttpHeaders headers = new HttpHeaders();
	        headers.setContentDispositionFormData("attachment", CodeUtil.parseGBK("客户分类台帐.xls"));
	        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
	        result = new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(exl), headers, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("下载贷前调查报告文件异常：", e);
        }
        return result;
    }
    
    
    /**
     * 导入存量的客户分类
     *@Description
     *@param request
     *@param response
     *@param dataFile
     *@return
     * @author xyh
     */
    @RequestMapping("uploadExcel.json")
	@ResponseBody
	@LoanAopLog(operationName="导入贷后客户风险分类")
	public ResultDto uploadExcel(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "file", required = false) MultipartFile dataFile) {
		ResultDto re = returnFail("上传失败，请重试！");
		// 专门给ie和火狐下使用 uploadify插件
		request.setAttribute(HandlerMapping.PRODUCIBLE_MEDIA_TYPES_ATTRIBUTE, null);
		RandomizingID random = new RandomizingID("ID", "yyyyMMddHHmmss", 4, false);
		User sessionUser = HttpUtil.getCurrentUser(request);
		try {
			String fileName = dataFile.getOriginalFilename().toString();
			// 流水号或者时间
			File target = new File("D:\\RG_PRO\\file\\", random.genNewId() + ".xls");
			if (!target.exists()) {
				target.mkdirs();
			}
			dataFile.transferTo(target);
			// 分析excel
			InputStream excelInputStream = new FileInputStream(target);

			Workbook wb = null;
			if (fileName.endsWith(".xls")) {
				wb = new HSSFWorkbook(excelInputStream);
			} else if (fileName.endsWith(".xlsx")) {
				wb = new XSSFWorkbook(excelInputStream);
			}
			Sheet sheet = wb.getSheetAt(0);
			Row firstRow = sheet.getRow(0);

			int totalRows = sheet.getPhysicalNumberOfRows();
			int rowCells = firstRow.getPhysicalNumberOfCells();

			// 客户名称 列
			int custCol = -1;
			// 客户分类
			int classCol = -1;
			for (int i = 0; i < rowCells; i++) {
				if ("客户名称".equals(firstRow.getCell(i).getStringCellValue())) {
					custCol = i;
				} else if ("客户分类".equals(firstRow.getCell(i).getStringCellValue())) {
					classCol = i;
				}
			}
			if (custCol == -1) {
				ExceptionUtil.throwException("未找到客户名称列");
			}
			if (classCol == -1) {
				ExceptionUtil.throwException("未找到客户分类列");
			}
			
			for (int i = 1; i < totalRows; i++) {
				String custName = sheet.getRow(i).getCell(custCol).getStringCellValue();
				String custClass = sheet.getRow(i).getCell(classCol).getStringCellValue();
				//获取当前客户
				CusBase entity = cusBaseSV.queryByCusName(custName);
				if(entity == null){
					log.info("["+custName+"]不存在，导入失败请检查");
					continue;
				}
				//获取当前客户的贷后客户信息
				LfCustInfo lfCustInfo = lfCustInfoService.selectSingleByProperty("customerNo",entity.getCustomerNo());
				if(lfCustInfo==null){
					log.info("["+custName+"]贷后客户信息不存在，导入失败请检查");
					continue;
				}
				if(!StringUtil.isEmptyString(lfCustInfo.getRiskClassi())){
					continue;
				}
				String riskClass = "";
				switch (custClass) {
				case "正常":
					riskClass = "01";
					break;
				case "关注":
					riskClass = "02";
					break;
				case "风险一级":
					riskClass = "03";
					break;
				case "风险二级":
					riskClass = "04";
					break;
				case "风险三级":
					riskClass = "05";
					break;
				}
				if(StringUtil.isEmptyString(riskClass)){
					ExceptionUtil.throwException("["+custName+"]风险分类为空");
				}
				lfCustInfo.setRiskClassi(riskClass);
				lfCustInfo.setUpdater(sessionUser.getUserId());
				lfCustInfo.setUpdateTime(DateUtil.getNowTimestamp());
				lfCustInfoService.saveOrUpdate(lfCustInfo);
				
				re.setCode(ResultDto.RESULT_CODE_SUCCESS);
				re.setMsg("数据导入成功");
			}
		}catch(BusinessException e){
			log.error(e.getMessage());
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
    
    /**
	 * 导入存量的客户分类
	 *@Description
	 *@param request
	 *@param response
	 *@param dataFile
	 *@return
	 * @author lk
	 */
	@RequestMapping("uploadAddTaskExcel.json")
	@ResponseBody
	@LoanAopLog(operationName="导入创建特殊贷后任务")
	public ResultDto uploadAddTaskExcel(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "file", required = false) MultipartFile dataFile) {
		ResultDto re = returnFail("上传失败，请重试！");
		// 专门给ie和火狐下使用 uploadify插件
		request.setAttribute(HandlerMapping.PRODUCIBLE_MEDIA_TYPES_ATTRIBUTE, null);
		RandomizingID random = new RandomizingID("ID", "yyyyMMddHHmmss", 4, false);
		try {
			String fileName = dataFile.getOriginalFilename().toString();
			// 流水号或者时间
			File target = new File("D:\\RG_PRO\\file\\", random.genNewId() + ".xls");
			if (!target.exists()) {
				target.mkdirs();
			}
			dataFile.transferTo(target);
			// 分析excel
			InputStream excelInputStream = new FileInputStream(target);
	
			Workbook wb = null;
			if (fileName.endsWith(".xls")) {
				wb = new HSSFWorkbook(excelInputStream);
			} else if (fileName.endsWith(".xlsx")) {
				wb = new XSSFWorkbook(excelInputStream);
			}
			Sheet sheet = wb.getSheetAt(0);
			Row firstRow = sheet.getRow(0);
	
			int totalRows = sheet.getPhysicalNumberOfRows();
			int rowCells = firstRow.getPhysicalNumberOfCells();
	
			// 客户名称 列
			int custNameCol = -1;
			// 客户编号 列
			int xdCustNoCol = -1;
			for (int i = 0; i < rowCells; i++) {
				if ("客户名称".equals(firstRow.getCell(i).getStringCellValue())) {
					custNameCol = i;
				} else if ("信贷客户号".equals(firstRow.getCell(i).getStringCellValue())) {
					xdCustNoCol = i;
				}
			}
			if (custNameCol == -1 && xdCustNoCol == -1) {
				ExceptionUtil.throwException("未找到客户名称列且未找到信贷客户号列");	
			}
			
			if (xdCustNoCol != -1) {
				for (int i = 1; i < totalRows; i++) {
					String xdCustomerNo = sheet.getRow(i).getCell(xdCustNoCol).getStringCellValue();
					//获取当前用户
					CusBase entity = cusBaseSV.queryByXdCusNo(xdCustomerNo);
					if(entity == null){
						log.info("该信贷客户号["+xdCustomerNo+"]不存在，导入失败请检查");
						continue;
					}
					
					//获取当前客户的贷后客户信息
					LfCustInfo lfCustInfo = lfCustInfoService.selectSingleByProperty("customerNo",entity.getCustomerNo());
					if(lfCustInfo==null){
						log.info("该信贷客户号["+xdCustomerNo+"]的贷后客户信息不存在，导入失败请检查");
						continue;
					}
					
					lfTaskService.createManuallyOneTask(lfCustInfo.getCustomerNo());
					
					re.setCode(ResultDto.RESULT_CODE_SUCCESS);
					re.setMsg("任务创建成功");
				}
			}else if (custNameCol != -1) {
				for (int i = 1; i < totalRows; i++) {
					String custName = sheet.getRow(i).getCell(custNameCol).getStringCellValue();
					//获取当前客户
					CusBase entity = cusBaseSV.queryByCusName(custName);
					if(entity == null){
						log.info("["+custName+"]不存在，导入失败请检查");
						continue;
					}
					//获取当前客户的贷后客户信息
					LfCustInfo lfCustInfo = lfCustInfoService.selectSingleByProperty("customerNo",entity.getCustomerNo());
					if(lfCustInfo==null){
						log.info("["+custName+"]贷后客户信息不存在，导入失败请检查");
						continue;
					}
					
					lfTaskService.createManuallyOneTask(lfCustInfo.getCustomerNo());
					
					re.setCode(ResultDto.RESULT_CODE_SUCCESS);
					re.setMsg("任务创建成功");
				}
			}
			
		}catch(BusinessException e){
			log.error(e.getMessage());
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	@RequestMapping("createDaoqi.json")
	@ResponseBody
	public ResultDto createDaoqi(HttpServletRequest request) {
		ResultDto re = returnFail("上传失败，请重试！");
		try{
			lfTaskService.syncFXThreeMonthTask2(null);
			re = returnSuccess();
		}catch(Exception e){
			e.printStackTrace();
		}
		return re;
	
	}
	
	

	/**
	 * 重新分配风险经理
	 * @param customerNo
	 * @param customerName
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping("choseRiskManager.json")
	@ResponseBody
	public ResultDto choseRiskManager(Integer id, String userNo, String userName) {
		ResultDto resultDto = returnFail();
		if(id==null){
			resultDto.setMsg("数据信息异常，请重试");
			return resultDto;
		}
		try {
			User curUser = HttpUtil.getCurrentUser();
			LfTask lfTask = lfTaskService.findByPrimaryKey(id);
			lfTask.setRiskUser(userNo);
			lfTask.setRiskUserName(userName);
			lfTask.setUpdater(curUser.getUserId());
			lfTask.setUpdateTime(DateUtil.getNowTimestamp());
			lfTaskService.saveOrUpdate(lfTask);
			resultDto = returnSuccess();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("更换选择专职风险经理失败！", e);
			resultDto.setMsg("更换选择专职风险经理失败");
		}
		return resultDto;
	}
	
	/**
	 * 贷后任务清单 导出Excel
	 * @param fileType 表的类型，如
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "exportTask.do", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<byte[]> exportTask(String customerNo, String customerName,String userName,
			String orgName, String riskUserName,String fileType) throws Exception{
		ResponseEntity<byte[]> result = null;
		User curUser = HttpUtil.getCurrentUser();
	    String userNo = null;
	    String orgNo = null;
	    try {
	    	String userDA = curUser.getDataAccess();
	    	if(SysConstants.UserDataAccess.PERSONAL.equals(userDA)) {
	    		userNo = curUser.getUserId();
	        }else if(SysConstants.UserDataAccess.ORG.equals(userDA)) {
	            orgNo = curUser.getAccOrgNo();
	        }
	    	
	    	customerName = URLDecoder.decode(customerName,"utf-8");
	    	userName = URLDecoder.decode(userName,"utf-8");
	    	orgName = URLDecoder.decode(orgName,"utf-8");
	    	riskUserName = URLDecoder.decode(riskUserName,"utf-8");
			Map<String, Object> dataMap = this.getExcelData(customerNo,customerName,
					userName,orgName,riskUserName,userNo,orgNo,fileType);
	
			
			String templateName = (String) dataMap.get("templateName");
			String fileName = dataMap.get("titleName") + ".xls";
			String destPath = AppConfig.Cfg.FIVE_CLASS_PATH
					+ DateUtil.formatDate(new Date(), "yyyy-MM-dd" );
			File file = new File(destPath , fileName);
			if(!file.getParentFile().exists())
				file.getParentFile().mkdirs();
			FreeMarkerWordUtil.createWord(dataMap, templateName, destPath, file.getName());

			HttpHeaders headers = new HttpHeaders();
			headers.setContentDispositionFormData("attachment", CodeUtil.parseGBK(fileName));
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			result = new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file), headers, HttpStatus.OK);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 获取 Excel 数据集
	 * @param fileType Excel文件类型
	 * @return
	 * @throws Exception 
	 */
	public Map<String, Object> getExcelData(String customerNo, String customerName,String userName,
			String orgName, String riskUserName,String userNo,String orgNo,String fileType) throws Exception {
		String templateName = "", titleName = "";
		Map<String, Object> dataMap = new HashMap<>();
		if (Constants.LF_TASK_TYPE.DAY_TASK_LIST.equals(fileType)){			// 日常贷后任务清单----待处理任务
			templateName = "loan_statistics/lf/dayTaskList.htm";
			titleName = "日常贷后待处理任务清单";
			List<String> taskTypes = new ArrayList<>();
	        taskTypes.add(Constants.LF_TASK_TYPE.GH_DAY_TASK);
	        List<LfTaskDto> lfTaskList = lfTaskService.getLfTaskList(taskTypes,customerNo,
	        		customerName,userName,orgName,riskUserName,userNo,
	        		orgNo,Constants.LF_TASK_POSITION.CHECKING_TASK);
	        if(!CollectionUtils.isEmpty(lfTaskList)){
            	lfTaskList = this.transIndustryType(lfTaskList);
            }
	        dataMap.put("fileType",fileType);
	        dataMap.put("lfTaskList", lfTaskList);
	        
		}else if (Constants.LF_TASK_TYPE.FIFTEEN_DAY_TASK_LIST.equals(fileType)){		// 15日首检贷后任务清单----待处理任务
			templateName = "loan_statistics/lf/fifteenDayTaskList.htm";
			titleName = "15日首检贷后待处理任务清单";
			List<String> taskTypes = new ArrayList<>();
			taskTypes.add(Constants.LF_TASK_TYPE.FX_FIFTEEN_DAY_TASK);
            taskTypes.add(Constants.LF_TASK_TYPE.GH_FIFTEEN_DAY_TASK);
            List<LfTaskDto> lfTaskList = lfTaskService.getLfTaskList(taskTypes,customerNo,
            		customerName,userName,orgName,riskUserName,userNo,
            		orgNo,Constants.LF_TASK_POSITION.CHECKING_TASK);
            if(!CollectionUtils.isEmpty(lfTaskList)){
            	lfTaskList = this.transIndustryType(lfTaskList);
            }
	        dataMap.put("fileType",fileType);
            dataMap.put("lfTaskList", lfTaskList);
	        
		}else if (Constants.LF_TASK_TYPE.THREE_MONTH_TASK_LIST.equals(fileType)){	// 到期检查贷后任务清单----待处理任务
			templateName = "loan_statistics/lf/stopCheckingTaskList.htm";
			titleName = "到期检查贷后待处理任务清单";
			List<String> taskTypes = new ArrayList<>();
			taskTypes.add(Constants.LF_TASK_TYPE.FX_THREE_MONTH_TASK);
			List<LfTaskDto> lfTaskList = lfTaskService.getLfTaskList(taskTypes,customerNo,
					customerName,userName,orgName,riskUserName,userNo,
					orgNo,Constants.LF_TASK_POSITION.CHECKING_TASK);
			if(!CollectionUtils.isEmpty(lfTaskList)){
            	lfTaskList = this.transIndustryType(lfTaskList);
            }
	        dataMap.put("fileType",fileType);
			dataMap.put("lfTaskList", lfTaskList);
		        
		}else if (Constants.LF_TASK_TYPE.FX_YJ_TASK_LIST.equals(fileType)){	// 风险信号检查贷后任务清单----待处理任务
			templateName = "loan_statistics/lf/fxyjTaskList.htm";
			titleName = "风险信号检查贷后待处理任务清单";
			List<String> taskTypes = new ArrayList<>();
			taskTypes.add(Constants.LF_TASK_TYPE.FX_YJ_TASK);
			List<LfTaskDto> lfTaskList = lfTaskService.getLfTaskList(taskTypes,customerNo,
					customerName,userName,orgName,riskUserName,userNo,
					orgNo,Constants.LF_TASK_POSITION.CHECKING_TASK);
			if(!CollectionUtils.isEmpty(lfTaskList)){
            	lfTaskList = this.transIndustryType(lfTaskList);
            }
	        dataMap.put("fileType",fileType);
			dataMap.put("lfTaskList", lfTaskList);
		        
		}else if (Constants.LF_TASK_TYPE.CREATE_MANUALLY_LIST.equals(fileType)){	// 手动创建贷后任务清单----待处理任务
			templateName = "loan_statistics/lf/createManuallyList.htm";
			titleName = "手动创建贷后待处理任务清单";
			List<String> taskTypes = new ArrayList<>();
			taskTypes.add(Constants.LF_TASK_TYPE.CREATE_MANUALLY_TASK);
			List<LfTaskDto> lfTaskList = lfTaskService.getLfTaskList(taskTypes,customerNo,
					customerName,userName,orgName,riskUserName,userNo,
					orgNo,Constants.LF_TASK_POSITION.CHECKING_TASK);
			if(!CollectionUtils.isEmpty(lfTaskList)){
            	lfTaskList = this.transIndustryType(lfTaskList);
            }
	        dataMap.put("fileType",fileType);
			dataMap.put("lfTaskList", lfTaskList);
			
		}else if (Constants.LF_TASK_TYPE.DAY_TASK_LIST_OVER.equals(fileType)){			// 日常贷后任务清单----检查结束任务
			templateName = "loan_statistics/lf/dayTaskList.htm";
			titleName = "日常贷后检查结束任务清单";
			List<String> taskTypes = new ArrayList<>();
	        taskTypes.add(Constants.LF_TASK_TYPE.GH_DAY_TASK);
	        List<LfTaskDto> lfTaskList = lfTaskService.getLfTaskList(taskTypes,customerNo,
	        		customerName,userName,orgName,riskUserName,userNo,
	        		orgNo,Constants.LF_TASK_POSITION.CHECKED_TASK);
	        if(!CollectionUtils.isEmpty(lfTaskList)){
            	lfTaskList = this.transIndustryType(lfTaskList);
            }
	        dataMap.put("fileType",fileType);
	        dataMap.put("lfTaskList", lfTaskList);
	        
		}else if (Constants.LF_TASK_TYPE.FIFTEEN_DAY_TASK_LIST_OVER.equals(fileType)){		// 15日首检贷后任务清单----检查结束任务
			templateName = "loan_statistics/lf/fifteenDayTaskList.htm";
			titleName = "15日首检贷后检查结束任务清单";
			List<String> taskTypes = new ArrayList<>();
			taskTypes.add(Constants.LF_TASK_TYPE.FX_FIFTEEN_DAY_TASK);
            taskTypes.add(Constants.LF_TASK_TYPE.GH_FIFTEEN_DAY_TASK);
            List<LfTaskDto> lfTaskList = lfTaskService.getLfTaskList(taskTypes,customerNo,
            		customerName,userName,orgName,riskUserName,userNo,
            		orgNo,Constants.LF_TASK_POSITION.CHECKED_TASK);
            if(!CollectionUtils.isEmpty(lfTaskList)){
            	lfTaskList = this.transIndustryType(lfTaskList);
            }
	        dataMap.put("fileType",fileType);
            dataMap.put("lfTaskList", lfTaskList);
	        
		}else if (Constants.LF_TASK_TYPE.THREE_MONTH_TASK_LIST_OVER.equals(fileType)){	// 到期检查贷后任务清单----检查结束任务
			templateName = "loan_statistics/lf/stopCheckingTaskList.htm";
			titleName = "到期检查贷后检查结束任务清单";
			List<String> taskTypes = new ArrayList<>();
			taskTypes.add(Constants.LF_TASK_TYPE.FX_THREE_MONTH_TASK);
			List<LfTaskDto> lfTaskList = lfTaskService.getLfTaskList(taskTypes,customerNo,
					customerName,userName,orgName,riskUserName,userNo,
					orgNo,Constants.LF_TASK_POSITION.CHECKED_TASK);
			if(!CollectionUtils.isEmpty(lfTaskList)){
            	lfTaskList = this.transIndustryType(lfTaskList);
            }
	        dataMap.put("fileType",fileType);
			dataMap.put("lfTaskList", lfTaskList);
		        
		}else if (Constants.LF_TASK_TYPE.FX_YJ_TASK_LIST_OVER.equals(fileType)){	// 风险信号检查贷后任务清单----检查结束任务
			templateName = "loan_statistics/lf/fxyjTaskList.htm";
			titleName = "风险信号检查贷后检查结束任务清单";
			List<String> taskTypes = new ArrayList<>();
			taskTypes.add(Constants.LF_TASK_TYPE.FX_YJ_TASK);
			List<LfTaskDto> lfTaskList = lfTaskService.getLfTaskList(taskTypes,customerNo,
					customerName,userName,orgName,riskUserName,userNo,
					orgNo,Constants.LF_TASK_POSITION.CHECKED_TASK);
			if(!CollectionUtils.isEmpty(lfTaskList)){
            	lfTaskList = this.transIndustryType(lfTaskList);
            }
	        dataMap.put("fileType",fileType);
			dataMap.put("lfTaskList", lfTaskList);
		        
		}else if (Constants.LF_TASK_TYPE.CREATE_MANUALLY_LIST_OVER.equals(fileType)){	// 手动创建贷后任务清单----检查结束任务
			templateName = "loan_statistics/lf/createManuallyList.htm";
			titleName = "手动创建贷后检查结束任务清单";
			List<String> taskTypes = new ArrayList<>();
			taskTypes.add(Constants.LF_TASK_TYPE.CREATE_MANUALLY_TASK);
			List<LfTaskDto> lfTaskList = lfTaskService.getLfTaskList(taskTypes,customerNo,
					customerName,userName,orgName,riskUserName,userNo,
					orgNo,Constants.LF_TASK_POSITION.CHECKED_TASK);
			if(!CollectionUtils.isEmpty(lfTaskList)){
            	lfTaskList = this.transIndustryType(lfTaskList);
            }
	        dataMap.put("fileType",fileType);
			dataMap.put("lfTaskList", lfTaskList);       
		}
		
		dataMap.put("titleName", titleName);
		dataMap.put("templateName", templateName);
		return dataMap;
	}
	
	public  List<LfTaskDto> transIndustryType(List<LfTaskDto> lfTaskList){
		for(LfTaskDto lfTask : lfTaskList){
			if(!StringUtils.isEmpty(lfTask.getIndustryType())){
				if(lfTask.getIndustryType().contains("A")){
					lfTask.setIndustryType("农、林、牧、渔业");
				}else if(lfTask.getIndustryType().contains("B")){
					lfTask.setIndustryType("采矿业");
				}else if(lfTask.getIndustryType().contains("C")){
					lfTask.setIndustryType("制造业");
				}else if(lfTask.getIndustryType().contains("D")){
					lfTask.setIndustryType("电力、燃气及水的生产和供应业");
				}else if(lfTask.getIndustryType().contains("E")){
					lfTask.setIndustryType("建筑业");
				}else if(lfTask.getIndustryType().contains("F")){
					lfTask.setIndustryType("批发和零售业");
				}else if(lfTask.getIndustryType().contains("G")){
					lfTask.setIndustryType("交通运输、仓储和邮政业");
				}else if(lfTask.getIndustryType().contains("H")){
					lfTask.setIndustryType("住宿和餐饮业");
				}else if(lfTask.getIndustryType().contains("I")){
					lfTask.setIndustryType("住宿和餐饮业");
				}else if(lfTask.getIndustryType().contains("J")){
					lfTask.setIndustryType("金融业");
				}else if(lfTask.getIndustryType().contains("K")){
					lfTask.setIndustryType("房地产业");
				}else if(lfTask.getIndustryType().contains("L")){
					lfTask.setIndustryType("租赁和商务服务业");
				}else if(lfTask.getIndustryType().contains("M")){
					lfTask.setIndustryType("科学研究和技术服务业");
				}else if(lfTask.getIndustryType().contains("N")){
					lfTask.setIndustryType("水利、环境和公共设施管理业");
				}else if(lfTask.getIndustryType().contains("O")){
					lfTask.setIndustryType("居民服务、修理和其他服务业");
				}else if(lfTask.getIndustryType().contains("P")){
					lfTask.setIndustryType("教育");
				}else if(lfTask.getIndustryType().contains("Q")){
					lfTask.setIndustryType("卫生和社会工作");
				}else if(lfTask.getIndustryType().contains("R")){
					lfTask.setIndustryType("文化、体育和娱乐业");
				}else if(lfTask.getIndustryType().contains("S")){
					lfTask.setIndustryType("公共管理、社会保障和社会组织");
				}else if(lfTask.getIndustryType().contains("T")){
					lfTask.setIndustryType("国际组织");
				}else if(lfTask.getIndustryType().contains("X")){
					lfTask.setIndustryType("未提供");
				}else if(lfTask.getIndustryType().contains("Z")){
					lfTask.setIndustryType("未知");
				}
			}else{
				lfTask.setIndustryType("无");
			}
		}
		return lfTaskList;
	}
	
	/**
	 * 同步当前任务所有的查冻扣账户信息
	 * @return
	 */
	@RequestMapping("/syncCfdAccount.json")
	@ResponseBody
	public ResultDto syncCfdAccount(){
		ResultDto re =returnFail();
		try{
			List<LfTask> all = lfTaskService.getAll();
			if(CollectionUtils.isEmpty(all)){
				re.setMsg("贷后任务为空");
				return re;
			}
			for(LfTask lfTask : all){
				String customerNo = lfTask.getCustomerNo();
				CusBase cusBase = cusBaseSV.queryByCusNo(customerNo);
				if(cusBase==null || StringUtils.isEmpty(cusBase.getMfCustomerNo())){
					log.info("当初客户信息为空或不存在和核心号");
					continue;
				}
			    //同步查冻扣账户
			    lfCfdAccountInfoService.syncCfdAccount(lfTask.getId(), cusBase.getMfCustomerNo());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return re;
	}
	
	
	
	/**
	 * 
	 * 手动创建日常任务的接口
	 * 
	 * @return
	 */
	@RequestMapping("creatDayTask.json")
	@ResponseBody
	public ResultDto creatDayTask(HttpServletRequest request, String customerName){
		ResultDto re = returnFail();
		if(StringUtil.isEmptyString(customerName)){
			return returnFail("请将信息补充完整再提交！");
		}
		
		try{
			CusBase cusBase = cusBaseSV.queryByCusName(customerName);
			if(cusBase==null){
				return returnFail("客户名称为：["+customerName+"]在信贷系统中不存在，请检查客户名称是否正确");
			}
			String info = lfTaskService.createGHDayTask(cusBase.getCustomerNo());
			
			if(info.contains("创建任务成功")){
				re = returnSuccess();
				re.setMsg(info);
			}else{
				return returnFail(info);
			}
		}catch(Exception e){
			e.printStackTrace();
			
		}
		return re;
	}
	
	
	/**
	 * 
	 * 手动创建到期任务的接口
	 * 
	 * @return
	 */
	@RequestMapping("creatStopCheckTask.json")
	@ResponseBody
	public ResultDto creatStopCheckTask(HttpServletRequest request, String customerName){
		ResultDto re = returnFail();
		if(StringUtil.isEmptyString(customerName)){
			return returnFail("请将信息补充完整再提交！");
		}
		
		try{
			CusBase cusBase = cusBaseSV.queryByCusName(customerName);
			if(cusBase==null){
				return returnFail("客户名称为：["+customerName+"]在信贷系统中不存在，请检查客户名称是否正确");
			}
			lfTaskService.createManuallyStopTask(cusBase.getCustomerNo());
			re = returnSuccess();
			re.setMsg("创建成功");
		}catch(Exception e){
			e.printStackTrace();
			return returnFail("系统异常，创建任务失败！");
		}
		return re;
	}



}
