package com.beawan.loanAfter.controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.beawan.customer.bean.CusBase;
import com.platform.util.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.analysis.finansis.utils.CodeUtil;
import com.beawan.base.entity.User;
import com.beawan.base.service.ISysDicSV;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.common.config.AppConfig;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.corporateloan.dto.LcManagerRiskStatDto;
import com.beawan.corporateloan.dto.LcRiskStatDto;
import com.beawan.corporateloan.dto.LcTaskStatDto;
import com.beawan.loanAfter.entity.LfDownCheckInfo;
import com.beawan.loanAfter.entity.LfLoanProjectSummary;
import com.beawan.loanAfter.entity.LfLocalResult;
import com.beawan.loanAfter.entity.LfProConstructCommit;
import com.beawan.loanAfter.entity.LfRiskWarningInfo;
import com.beawan.loanAfter.entity.LfTask;
import com.beawan.loanAfter.entity.LfTaskResult;
import com.beawan.loanAfter.service.LfCustInfoService;
import com.beawan.loanAfter.service.LfDownCheckInfoService;
import com.beawan.loanAfter.service.LfFinCheckService;
import com.beawan.loanAfter.service.LfLoanProjectSummaryService;
import com.beawan.loanAfter.service.LfLocalResultService;
import com.beawan.loanAfter.service.LfProConstructCommitService;
import com.beawan.loanAfter.service.LfReportService;
import com.beawan.loanAfter.service.LfRiskWarningInfoService;
import com.beawan.loanAfter.service.LfSituationCheckService;
import com.beawan.loanAfter.service.LfTaskResultService;
import com.beawan.loanAfter.service.LfTaskService;

/**	
 * 贷后页面跳转controller
 * 
 * @author xyh
 *
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/loanAfter/" })
public class LoanAfterPageController extends BaseController {
	private static final Logger log = Logger.getLogger(LoanAfterController.class);
	@Resource
	private LfTaskService lfTaskService;
	@Resource
	private LfTaskResultService lfTaskResultService;
	@Resource
	private LfLocalResultService lfLocalResultService;
	@Resource
	private LfCustInfoService lfCustInfoService;
	@Resource
	private LfFinCheckService lfFinCheckService;
	@Resource
	private LfSituationCheckService lfSituationCheckService;
	@Resource
	private LfLoanProjectSummaryService lfLoanProjectSummaryService;
	@Resource
	private LfProConstructCommitService lfProConstructCommitService;
	@Resource
	private LfDownCheckInfoService lfDownCheckInfoService;
	@Resource
	private LfReportService lfReportService;
	@Resource
	private ISysDicSV sysDicSV;
	@Resource
	private LfRiskWarningInfoService lfRiskWarningInfoService;
	/**
	 * 
	 *@Description 首检任务界面--检查中任务（管户经理）
	 *@return
	 * @author xyh
	 */
	@RequestMapping("fisrtCheckingTask.do")
	public ModelAndView fisrtCheckingTask() {
		User curUser = HttpUtil.getCurrentUser();
		ModelAndView mav = new ModelAndView("views/loanAfter/fisrtCheckingTask");
		mav.addObject("roleNo", curUser.getRoleNo());
		return mav;
	}
	
	
	/**
	 * 
	 *@Description 首检任务界面--检查中任务（风险经理）
	 *@return
	 * @author xyh
	 */
	@RequestMapping("fisrtRiskCheckingTask.do")
	public ModelAndView fisrRisktCheckingTask() {
		User curUser = HttpUtil.getCurrentUser();
		ModelAndView mav = new ModelAndView("views/loanAfter/fisrtRiskCheckingTask");
		mav.addObject("roleNo", curUser.getRoleNo());
		return mav;
	}
	
	/**
	 * 
	 *@Description 日常检查任务界面--检查中任务
	 *@return
	 * @author xyh
	 */
	@RequestMapping("daylyCheckingTask.do")
	public ModelAndView daylyCheckingTask() {
		ModelAndView mav = new ModelAndView("views/loanAfter/daylyCheckingTask");
		return mav;
	}
	
	/**
	 * 
	 *@Description 到期检查------检查中任务
	 *@return
	 * @author xyh
	 */
	@RequestMapping("stopCheckingTask.do")
	public ModelAndView stopCheckingTask() {
		ModelAndView mav = new ModelAndView("views/loanAfter/stopCheckingTask");
		return mav;
	}
	
	
	/**
	 * 
	 *@Description 风险预警贷后任务界面-----检查中任务
	 *@return
	 * @author xyh
	 */
	@RequestMapping("riskWarnCheckingTask.do")
	public ModelAndView riskWarnCheckingTask() {
		ModelAndView mav = new ModelAndView("views/loanAfter/riskWarnCheckingTask");
		return mav;
	}
	
	/**
	 * 
	 *@Description 特殊贷后任务界面--检查中任务
	 *@return
	 * @author lk
	 */
	@RequestMapping("createManuallyCheckingTask.do")
	public ModelAndView createManuallyCheckingTask() {
		ModelAndView mav = new ModelAndView("views/loanAfter/createManuallyCheckingTask");
		return mav;
	}
	
	
	/**
	 * 
	 *@Description 首检任务界面----检查结束任务(管户经理)
	 *@return
	 * @author xyh
	 */
	@RequestMapping("fisrtCheckedTask.do")
	public ModelAndView fisrtCheckedTask() {
		ModelAndView mav = new ModelAndView("views/loanAfter/fisrtCheckedTask");
		return mav;
	}
	
	/**
	 * 
	 *@Description 首检任务界面----检查结束任务(风险经理)
	 *@return
	 * @author xyh
	 */
	@RequestMapping("fisrtRiskCheckedTask.do")
	public ModelAndView fisrtRiskCheckedTask() {
		ModelAndView mav = new ModelAndView("views/loanAfter/fisrtRiskCheckedTask");
		return mav;
	}
	
	/**
	 * 
	 *@Description 日常检查任务界面----检查结束任务
	 *@return
	 * @author xyh
	 */
	@RequestMapping("daylyCheckedTask.do")
	public ModelAndView daylyCheckedTask() {
		ModelAndView mav = new ModelAndView("views/loanAfter/daylyCheckedTask");
		return mav;
	}
	/**
	 * 
	 *@Description 到期检查------检查结束任务
	 *@return
	 * @author xyh
	 */
	@RequestMapping("stopCheckedTask.do")
	public ModelAndView stopCheckedTask() {
		ModelAndView mav = new ModelAndView("views/loanAfter/stopCheckedTask");
		return mav;
	}
	/**
	 * 
	 *@Description 风险预警贷后任务界面-----检查结束任务
	 *@return
	 * @author xyh
	 */
	@RequestMapping("riskWarnCheckedTask.do")
	public ModelAndView riskWarnCheckedTask() {
		ModelAndView mav = new ModelAndView("views/loanAfter/riskWarnCheckedTask");
		return mav;
	}
	
	
	/**
	 * 
	 *@Description 风险预警贷后任务界面-----检查结束任务
	 *@return
	 * @author lk
	 */
	@RequestMapping("createManuallyCheckedTask.do")
	public ModelAndView createManuallyCheckedTask() {
		ModelAndView mav = new ModelAndView("views/loanAfter/createManuallyCheckedTask");
		return mav;
	}
	
	/**
	 * 
	 *@Description 风险预警贷后任务界面-----检查结束任务
	 *@return
	 * @author lk
	 */
	@RequestMapping("createTask.do")
	@ResponseBody
	public ResultDto syncCustInfo(HttpServletRequest request, HttpServletResponse response,String custNo){
		ResultDto re = returnFail("创建特殊贷后任务失败");
		try {
		
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("创建特殊贷后任务成功");
		} catch (Exception e) {
			log.error("创建特殊贷后任务异常", e);
			re.setMsg(e.getMessage());
		}
		return re;
	}
	
	
	/**
	 * 跳转到贷后检查界面 根据调查报告的不同，跳转到不同的界面
	 * 
	 * @return
	 */
	@RequestMapping("startCheckPage.do")
	public ModelAndView startCheckPage(Integer taskId) {
		// 默认跳转到初始化任务的界面
		String jumpPage = "views/loanAfter/init_task_index";
		ModelAndView mav = new ModelAndView(jumpPage);
		try {
			// 根据id获取贷后任务
			LfTask lfTask = lfTaskService.findByPrimaryKey(taskId);
//			String customerNo = lfTask.getCustomerNo();
			//判断当前客户是否同步企查查数据
//			lfTaskService.syncComFullDetail(customerNo);
			
			if (lfTask != null) {
				switch (lfTask.getTaskType()) {
				case Constants.LF_TASK_TYPE.INIT_TASK:// 初始化任务
					jumpPage = "views/loanAfter/init_task_index";
					mav = new ModelAndView(jumpPage);
					break;
				case Constants.LF_TASK_TYPE.GH_FIFTEEN_DAY_TASK:// 管户15日首检
					jumpPage = "views/loanAfter/fifteen_day_task_index";
					if(RgnshUtils.isEmptyInteger(lfTask.getHasSync())){
						lfTaskService.insertFifteenCheck(lfTask.getId());
					}
					mav = new ModelAndView(jumpPage);
					break;
				case Constants.LF_TASK_TYPE.FX_FIFTEEN_DAY_TASK:// 风险15日首检
					jumpPage = "views/loanAfter/fifteen_day_risk_task_index";
					if(RgnshUtils.isEmptyInteger(lfTask.getHasSync())){
						lfTaskService.insertFifteenCheck(lfTask.getId());
					}
					mav = new ModelAndView(jumpPage);
					break;
				case Constants.LF_TASK_TYPE.GH_DAY_TASK:// 管户日常任务
				case Constants.LF_TASK_TYPE.CREATE_MANUALLY_TASK://手动创建任务
				case Constants.LF_TASK_TYPE.FX_THREE_MONTH_TASK:// 风险3个月到期检查
				case Constants.LF_TASK_TYPE.FX_YJ_TASK:// 风险预警检查
					//获取客户所属行业，设置行业模版
					if(lfTask.getLoanBalance()== null || lfTask.getLoanBalance()<=10000000d){
						jumpPage = "views/loanAfter/daily_task_index_down";
						mav = new ModelAndView(jumpPage);
						//为空或者为0，需要同步数据
						if(RgnshUtils.isEmptyInteger(lfTask.getHasSync())){
							lfTaskService.insertDownModel(lfTask.getId());
						}
						int FXYJSum = 0;
						int OAPSum = 0;
						Map<String, Object> params = new HashMap<>();
						params.put("taskId", taskId);
		        		params.put("riskFrom", "FXYJ");//查询风险预警系统中的数据
		        		List<LfRiskWarningInfo> lfFXYJRiskWarningInfoList = lfRiskWarningInfoService.selectByProperty(params);
		        		if(!CollectionUtils.isEmpty(lfFXYJRiskWarningInfoList)){
		        			FXYJSum = lfFXYJRiskWarningInfoList.size();
		        		}
		        		mav.addObject("FXYJSum",FXYJSum);
		        		Map<String, Object> param = new HashMap<>();
		        		param.put("taskId", taskId);
		        		param.put("riskFrom", "OAP");//查询OAP预警系统中的数据
		        		List<LfRiskWarningInfo> lfOAPRiskWarningInfoList = lfRiskWarningInfoService.selectByProperty(param);
		        		if(!CollectionUtils.isEmpty(lfOAPRiskWarningInfoList)){
		        			OAPSum = lfOAPRiskWarningInfoList.size();
		        		}
		        		mav.addObject("OAPSum",OAPSum);
					}else {
						jumpPage = chooseModel(lfTask.getIndustryType(),lfTask);
						mav = new ModelAndView(jumpPage);
						int riskSum = 0;
						Map<String, Object> params = new HashMap<>();
						params.put("taskId", taskId);
		        		List<LfRiskWarningInfo> lfRiskWarningInfoList = lfRiskWarningInfoService.selectByProperty(params);
		        		if(!CollectionUtils.isEmpty(lfRiskWarningInfoList)){
		        			riskSum = lfRiskWarningInfoList.size();
		        		}
		        		mav.addObject("riskSum",riskSum);
					}
					break;
				}
			}
			
			mav.addObject("custName",lfTask.getCustomerName());
			// 将任务直接整个返回过去
			String customerNo = lfTask.getCustomerNo();
			mav.addObject("customerNo", lfTask.getCustomerNo());
			mav.addObject("taskId", lfTask.getId());
		} catch (Exception e) {
			e.printStackTrace();
			log.error("跳转贷后界面失败");
		}
		return mav;
	}
	/**
	 * 
	 *@Description 跳转具体的行业模版，同步不同模版的数据
	 *@param induType
	 *@param lfTask
	 *@return
	 *@throws Exception
	 * @author xyh
	 */
	private String chooseModel(String induType,LfTask lfTask)throws Exception{
		String jumpPage = "views/loanAfter/daily_task_index";
		if(!StringUtil.isEmptyString(induType)&&induType.contains("C")){
			//制造业
			jumpPage = "views/loanAfter/daily_task_index_C";
			if(RgnshUtils.isEmptyInteger(lfTask.getHasSync())){
				lfTaskService.insertCModel(lfTask.getId());
			}
		}else if(!StringUtil.isEmptyString(induType)&&induType.contains("E")){
			//建筑业
			jumpPage = "views/loanAfter/daily_task_index_E";
			if(RgnshUtils.isEmptyInteger(lfTask.getHasSync())){
				lfTaskService.insertEModel(lfTask.getId());
			}
		}else if(!StringUtil.isEmptyString(induType)&&induType.contains("F")){
			//批发零售
			jumpPage = "views/loanAfter/daily_task_index_F";
			if(RgnshUtils.isEmptyInteger(lfTask.getHasSync())){
				lfTaskService.insertFModel(lfTask.getId());
			}
		}else if(!StringUtil.isEmptyString(induType)&&induType.contains("K")){
			//房地产业
			jumpPage = "views/loanAfter/daily_task_index_K";
			if(RgnshUtils.isEmptyInteger(lfTask.getHasSync())){
				lfTaskService.insertKModel(lfTask.getId());
			}
		}else{
			if(RgnshUtils.isEmptyInteger(lfTask.getHasSync())){
				lfTaskService.insertNomalModel(lfTask.getId());
			}
		}
		return jumpPage;
	}
	
	
	/**
	 * 跳转到客户分类的界面
	 * @param taskId
	 
	 * @return
	 */
	@RequestMapping("goCustClassi.do")
	public ModelAndView goCustClassi(Integer taskId) {
		ModelAndView mav = new ModelAndView("views/loanAfter/commons/lfCustClassiDetail");
		//LfTask lfTask = lfTaskService.selectByPrimaryKey(taskId);
		mav.addObject("taskId", taskId);
		return mav;
	}
	
	/**
	 * 跳转到客户风险分类的界面
	 * @param taskId
	 
	 * @return
	 */
	@RequestMapping("lfRiskClassi.do")
	public ModelAndView lfRiskClassi(Integer taskId) {
		ModelAndView mav = new ModelAndView("views/loanAfter/commons/lfRiskClassi");
		LfTask lfTask = lfTaskService.selectByPrimaryKey(taskId);
		mav.addObject("lfTask", lfTask);
		return mav;
	}

	/**
	 * 跳转到客户授信跟踪分类的界面
	 * @param taskId

	 * @return
	 */
	@RequestMapping("lfCreditClassi.do")
	public ModelAndView lfCreditClassi(Integer taskId) {
		ModelAndView mav = new ModelAndView("views/loanAfter/commons/lfCreditClassi");
		LfTask lfTask = lfTaskService.selectByPrimaryKey(taskId);
		mav.addObject("lfTask", lfTask);
		return mav;
	}


	/**
	 * 15日首检贷款信息界面
	 * 
	 * @param taskId
	 * @return
	 */
	@RequestMapping("goFifteenDayLoanInfo.do")
	public ModelAndView goFifteenDayLoanInfo(Integer taskId) {
		ModelAndView mav = new ModelAndView("views/loanAfter/commons/fifteen_day_loan_info");
		try {
			// 获取任务信息
			LfTask lfTask = lfTaskService.selectByPrimaryKey(taskId);
			mav.addObject("lfTask", lfTask);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("系统异常：",e);
		}
		return mav;
	}

	/**
	 * 15日首检核查项界面
	 * 
	 * @param taskId
	 * @return
	 */
	@RequestMapping("goFifteenDayCheck.do")
	public ModelAndView goFifteenDayCheck(Integer taskId) {
		ModelAndView mav = new ModelAndView("views/loanAfter/commons/fifteen_day_check");
		LfTaskResult lfTaskResult = lfTaskResultService.selectSingleByProperty("taskId", taskId);
		if (lfTaskResult != null) {
			mav.addObject("fifteenDayResult", lfTaskResult.getFifteenDayResult());
		}
		mav.addObject("taskId", taskId);
		return mav;
	}

	/**
	 * 跳转界面：现场调查-客户基础信息变动检查界面
	 * 
	 * @param taskId
	 * @return
	 */
	@RequestMapping("goCustChangeCheck.do")
	public ModelAndView goCustChangeCheck(Integer taskId) {
		ModelAndView mav = new ModelAndView("views/loanAfter/commons/cust_change_check");
		//获取到贷后任务的整体的结果表
		LfTaskResult lfTaskResult = lfTaskResultService.selectSingleByProperty("taskId", taskId);
		if (lfTaskResult != null) {
			mav.addObject("custChangeReason", lfTaskResult.getCustChangeReason());
		}
		mav.addObject("taskId", taskId);
		return mav;
	}
	
	/**
	 * 跳转到融资变动和担保信息变动的界面
	 * @param taskId
	 * @param
	 * @return
	 */
	@RequestMapping("goFinAndGuaChangeCheck.do")
	public ModelAndView goFinAndGuaChangeCheck(Integer taskId) {
		ModelAndView mav = new ModelAndView("views/loanAfter/commons/fin_and_gua_change_check");
		mav.addObject("taskId", taskId);
		return mav;
	}
	
	/**
	 * 跳转到财务状况检查的界面
	 * @param taskId
	 * @param
	 * @return
	 */
	@RequestMapping("goFinCheck.do")
	public ModelAndView goFinCheck(Integer taskId){
		ModelAndView mav = new ModelAndView("views/loanAfter/commons/fin_check");
		mav.addObject("taskId", taskId);
		return mav;
	}
	
	/**
	 * 
	 *@Description 跳转到经营情况检查界面
	 *@param taskId
	 *@param
	 *@return
	 * @author xyh
	 */
	@RequestMapping("goJYSituationCheck.do")
	public ModelAndView goJYSituationCheck(Integer taskId){
		ModelAndView mav = new ModelAndView("views/loanAfter/commons/situation_jy_check");
		LfTaskResult lfTaskResult = lfTaskResultService.selectSingleByProperty("taskId", taskId);
		mav.addObject("lfTaskResult", lfTaskResult);
		mav.addObject("taskId", taskId);
		return mav;
	}
	
	/**
	 * 
	 *@Description 跳转到担保情况检查界面
	 *@param taskId
	 *@param
	 *@return
	 * @author xyh
	 */
	@RequestMapping("goDBSituationCheck.do")
	public ModelAndView goDBSituationCheck(Integer taskId){
		ModelAndView mav = new ModelAndView("views/loanAfter/commons/situation_db_check");
		mav.addObject("taskId", taskId);
		return mav;
	}
	
	/**
	 * 
	 *@Description 跳转到风险评价界面
	 *@param taskId
	 *@param
	 *@return
	 * @author xyh
	 */
	@RequestMapping("goRiskEvaluation.do")
	public ModelAndView goRiskEvaluation(Integer taskId){
		ModelAndView mav = new ModelAndView("views/loanAfter/commons/risk_evaluation");
		LfTaskResult lfTaskResult = lfTaskResultService.selectSingleByProperty("taskId", taskId);
		mav.addObject("lfTaskResult", lfTaskResult);
		mav.addObject("taskId", taskId);
		return mav;
	}
	
	/**
	 * 
	 *@Description 风险预警信息
	 *@param taskId
	 *@param
	 *@return
	 * @author xyh
	 */
	@RequestMapping("goRiskWarningPage.do")
	public ModelAndView goRiskWarningPage(Integer taskId,String noOAP){
		ModelAndView mav = new ModelAndView("views/loanAfter/commons/risk_warning_info");
		mav.addObject("taskId", taskId);
		mav.addObject("noOAP", noOAP);
		return mav;
	}
	
	@RequestMapping("goOAPWarningPage.do")
	public ModelAndView goOAPWarningPage(Integer taskId){
		ModelAndView mav = new ModelAndView("views/loanAfter/commons/oap_warning_info");
		mav.addObject("taskId", taskId);
		return mav;
	}
	
	/**
	 * 
	 *@Description 行政处罚信息
	 *@param taskId
	 *@param
	 *@return
	 * @author xyh
	 */
	@RequestMapping("goAdminSanInfo.do")
	public ModelAndView goAdminSanInfo(Integer taskId){
		ModelAndView mav = new ModelAndView("views/loanAfter/commons/admin_san_info");
		mav.addObject("taskId", taskId);
		return mav;
	}
	
	/**
	 * 
	 *@Description 账户查、冻、扣信息
	 *@param taskId
	 *@param
	 *@return
	 * @author xyh
	 */
	@RequestMapping("goCfdAccountInfo.do")
	public ModelAndView goCfdAccountInfo(Integer taskId){
		ModelAndView mav = new ModelAndView("views/loanAfter/commons/cfd_account_info");
		mav.addObject("taskId", taskId);
		return mav;
	}
	
	/**
	 *  
	 *@Description 内控名单信息
	 *@param taskId
	 *@param
	 *@return
	 * @author xyh
	 */
	@RequestMapping("goInternalControlInfo.do")
	public ModelAndView goInternalControlInfo(Integer taskId){
		ModelAndView mav = new ModelAndView("views/loanAfter/commons/internal_control_info");
		mav.addObject("taskId", taskId);
		return mav;
	}
	/**
	 * 
	 *@Description  司法涉诉信息和其他信息一起录入
	 *@param taskId
	 *@param
	 *@return
	 * @author xyh
	 */
	@RequestMapping("goLocalResult.do")
	public ModelAndView goLocalResult(Integer taskId){
		ModelAndView mav = new ModelAndView("views/loanAfter/commons/local_result");
		LfLocalResult lfLocalResult = lfLocalResultService.selectSingleByProperty("taskId", taskId);
		mav.addObject("lfLocalResult", lfLocalResult);
		mav.addObject("taskId", taskId);
		return mav;
	}
	
	//=================进入其他行业各自的模版中
	/**
	 * 
	 *@Description  司法涉诉信息和其他信息一起录入
	 *@param taskId
	 *@param
	 *@return
	 * @author xyh
	 */
	@RequestMapping("goPaymentOfFees.do")
	public ModelAndView goPaymentOfFees(Integer taskId){
		ModelAndView mav = new ModelAndView("views/loanAfter/specials/payment_of_fees");
		mav.addObject("taskId", taskId);
		return mav;
	}
	/**
	 * 
	 *@Description 其他现场检查情况
	 *@param taskId
	 *@param
	 *@return
	 * @author xyh
	 */
	@RequestMapping("goQTSituationCheck.do")
	public ModelAndView goQTSituationCheck(Integer taskId){
		ModelAndView mav = new ModelAndView("views/loanAfter/specials/situation_qt_check");
		LfTaskResult lfTaskResult = lfTaskResultService.selectSingleByProperty("taskId", taskId);
		mav.addObject("lfTaskResult", lfTaskResult);
		mav.addObject("taskId", taskId);
		return mav;
	}
	
	/**
	 * 
	 *@Description 项目存在的主要风险
	 *@param taskId
	 *@param
	 *@return
	 * @author xyh
	 */
	@RequestMapping("goXMSituationCheck.do")
	public ModelAndView goXMSituationCheck(Integer taskId){
		ModelAndView mav = new ModelAndView("views/loanAfter/specials/situation_xm_check");
		mav.addObject("taskId", taskId);
		return mav;
	}
	
	/**
	 * 
	 *@Description 在建或已完工为完全回款的主要项目情况
	 *@param taskId
	 *@param
	 *@return
	 * @author xyh
	 */
	@RequestMapping("goProjectInfo.do")
	public ModelAndView goProjectInfo(Integer taskId){
		ModelAndView mav = new ModelAndView("views/loanAfter/specials/project_info");
		mav.addObject("taskId", taskId);
		return mav;
	}
	
	@RequestMapping("goLoanProjectAll.do")
	public ModelAndView goLoanProjectAll(Integer taskId){
		ModelAndView mav = new ModelAndView("views/loanAfter/specials/loan_project_all");
		LfLoanProjectSummary lfLoanProjectSummary = lfLoanProjectSummaryService.selectSingleByProperty("taskId", taskId);
		mav.addObject("lfLoanProjectSummary",lfLoanProjectSummary);
		mav.addObject("taskId", taskId);
		return mav;
	}
	
	@RequestMapping("goProjectConstructCommit.do")
	public ModelAndView goProjectConstructCommit(Integer taskId){
		ModelAndView mav = new ModelAndView("views/loanAfter/specials/project_construct_commit");
		LfProConstructCommit lfProConstructCommit = lfProConstructCommitService.selectSingleByProperty("taskId", taskId);
		mav.addObject("lfProConstructCommit",lfProConstructCommit);
		mav.addObject("taskId", taskId);
		return mav;
	}
	
	@RequestMapping("goFinDownSituationCheck.do")
	public ModelAndView goFinDownSituationCheck(Integer taskId){
		ModelAndView mav = new ModelAndView("views/loanAfter/specials/situation_fin_check_down");
		mav.addObject("taskId", taskId);
		return mav;
	}
	
	@RequestMapping("goJZDownSituationCheck.do")
	public ModelAndView goJZDownSituationCheck(Integer taskId){
		ModelAndView mav = new ModelAndView("views/loanAfter/specials/situation_jz_check_down");
		LfDownCheckInfo lfDownCheckInfo = lfDownCheckInfoService.selectSingleByProperty("taskId", taskId);
		mav.addObject("lfDownCheckInfo", lfDownCheckInfo);
		mav.addObject("taskId", taskId);
		return mav;
	}
	
	@RequestMapping("goJYDownSituationCheck.do")
	public ModelAndView goJYDownSituationCheck(Integer taskId){
		ModelAndView mav = new ModelAndView("views/loanAfter/specials/situation_jy_check_down");
		LfDownCheckInfo lfDownCheckInfo = lfDownCheckInfoService.selectSingleByProperty("taskId", taskId);
		mav.addObject("lfDownCheckInfo", lfDownCheckInfo);
		mav.addObject("taskId", taskId);
		return mav;
	}
	
	
	@RequestMapping("goDownCheckBase.do")
	public ModelAndView goDownCheckBase(Integer taskId){
		ModelAndView mav = new ModelAndView("views/loanAfter/specials/base_info_check_down");
		LfDownCheckInfo lfDownCheckInfo = lfDownCheckInfoService.selectSingleByProperty("taskId", taskId);
		mav.addObject("lfDownCheckInfo", lfDownCheckInfo);
		mav.addObject("taskId", taskId);
		return mav;
	}
	
	/**
	 * 
	 *@Description 贷后基础客户信息界面，是否有需要做修改操作？
	 *@param taskId
	 *@param
	 *@return
	 * @author xyh
	 */
	@RequestMapping("goLfCustBaseInfo.do")
	public ModelAndView goLfCustBaseInfo(Integer taskId){
		ModelAndView mav = new ModelAndView("views/loanAfter/specials/lf_cust_base_info");
		LfTask lfTask = lfTaskService.findByPrimaryKey(taskId);
		

		

		mav.addObject("lfTask", lfTask);
		mav.addObject("taskId", taskId);
		return mav;
	}
	
	
	/**
	 * 
	 *@Description 查看调查报告--------这个用于测试
	 *@param taskId
	 *@return
	 * @author xyh
	 */
	@RequestMapping("goLfTaskReport.do")
	public ModelAndView goLfTaskReport(Integer taskId){
		ModelAndView mav = new ModelAndView("views/loanAfter/report/lf_nomal_report");
		try {
			Map<String, Object> lfInfo = lfReportService.getReportInfo(taskId);
			mav.addObject("lfInfo", lfInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
	
	@RequestMapping("custClassiPage.do")
	public ModelAndView custClassiPage(Integer taskId){
		ModelAndView mav = new ModelAndView("views/loanAfter/custClassiPage");
		return mav;
	}


	/**
	 * 健康体检的跳转
	 * @param request
	 * @param customerName
	 * @return
	 */
	@RequestMapping("healthyTest.do")
	@ResponseBody
	public ModelAndView healthyTest(HttpServletRequest request, String customerName) throws Exception {
		ModelAndView mav = new ModelAndView("views/loanAfter/healthyTest");
		mav.addObject("custName",customerName);
		if(customerName.contains("*")){
			return mav;
		}else{
			String t = getStringDate();
			int x = (int) (Math.random() * 900) + 100;
			String serial = t + x;
			String url = "http://http://49.234.78.216:8081/review/saveRwTaskNotMav";
			Map<String, Object> params = new HashMap<>();
			params.put("seriaNo",serial);
			params.put("customerName",customerName);
			String result = HttpClientUtil.httpGet(url,params);
		}
		return mav;
	}


	/**
	 * 获取现在时间
	 *
	 * @return返回字符串格式yyyyMMddHHmmss
	 */
	public static String getStringDate() {
		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmm");
		String dateString = formatter.format(currentTime);
		System.out.println("TIME:::" + dateString);
		return dateString;
	}



}
