package com.beawan.loanAfter.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * @Author: xyh
 * @Date: 25/08/2020
 * @Description: 15日首检核查信息表
 */
@Entity
@Table(name = "LF_FIFTEEN_DAY_CHECK",schema = "GDTCESYS")
public class LfFifteenDayCheck extends BaseEntity {
    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LF_FIFTEEN_DAY_CHECK_SEQ")
   // @SequenceGenerator(name="LF_FIFTEEN_DAY_CHECK_SEQ",allocationSize=1,initialValue=1, sequenceName="LF_FIFTEEN_DAY_CHECK_SEQ")
    private Integer id;
    @Column(name = "TASK_ID")
    private Integer taskId;//贷后任务id
    @Column(name = "CHECK_NAME")
    private String checkName;//核查名称
    @Column(name = "CHECK_TYPE")
    private String checkType;//核查列表
    @Column(name = "CHECK_SITUATION")
    private String checkSituation;//核查情况
    @Column(name = "CHECK_REMARK")
    private String checkRemark;//核查备注

    public LfFifteenDayCheck() {
    }

    public LfFifteenDayCheck(Integer taskId, String checkName, String checkType) {
        this.taskId = taskId;
        this.checkName = checkName;
        this.checkType = checkType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getCheckName() {
        return checkName;
    }

    public void setCheckName(String checkName) {
        this.checkName = checkName;
    }

    public String getCheckType() {
        return checkType;
    }

    public void setCheckType(String checkType) {
        this.checkType = checkType;
    }

    public String getCheckSituation() {
        return checkSituation;
    }

    public void setCheckSituation(String checkSituation) {
        this.checkSituation = checkSituation;
    }

    public String getCheckRemark() {
        return checkRemark;
    }

    public void setCheckRemark(String checkRemark) {
        this.checkRemark = checkRemark;
    }
}
