package com.beawan.loanAfter.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * @Author: xyh
 * @Date: 19/08/2020
 * @Description: 贷后任务列表
 * 这个表会冗余很多信息
 * 客户信息（10个字段）：新建一张表意义不大，只是用于生成一下报告，其他的客户数据又对接企查查来实时更新
 * 
 * 
 */
@Entity
@Table(name = "LF_TASK",schema = "GDTCESYS")
public class LfTask extends BaseEntity {
    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LF_TASK_SEQ")
   // @SequenceGenerator(name="LF_TASK_SEQ",allocationSize=1,initialValue=1, sequenceName="LF_TASK_SEQ")
    private Integer id;
    @Column(name = "CUSTOMER_NO")
    private String customerNo;//客户编号--对公系统的客户编号
    /**
     * 01：初始化认定任务----对客户进行认定工作
     * 02：管户客户经理日常贷后任务----如果客户的未结清金额大于0 按客户风险分类进行贷后检查
     * 03：管户客户经理15日首检----客户未结清金额大于0，且有新增标记的
     * 04：风险经理15日首检 ---- 这个待定     未结清金额大于0，且有新增标记，且有风险信号的
     * 05：风险经理3个月授信到期检查 -----客户身上最近到期日
     * 06：风险经理风险信号贷后 ----  有风险信号
     */
    @Column(name = "TASK_TYPE")
    private String taskType;//任务类型

    @Column(name = "TASK_NAME")
    private String taskName;//任务名称
    
    @Column(name = "TASK_POSITION")
    private Integer taskPosition;//任务所处阶段
    
    @Column(name = "HAS_SYNC")
    private Integer hasSync;//是否已经同步完成数据 1：已同步

    @Column(name = "USER_NO")
    private String userNo;//贷后检查客户经理号---以信贷系统的账号为用户编号
    
    /**
     * 风险分类：
     *  1 ：正常类
     *  2：关注类
     *  3：风险一级
     *  4：风险二级
     *  5：风险三级
     */
    @Column(name = "RISK_CLASSI")
    private String riskClassi;
    
    @Column(name = "RISK_SECOND_CLASSI")
    private String riskSecondClassi;
  
    @Column(name = "RISK_POINT")
    private String riskPoint;
    
    /**
     * 风险分类原因
     */
    @Column(name = "RISK_CLASSI_REASON")
    private String riskClassiReason;
    
    /**
     * 授信分类：
     * 6：增、
     * 7：持、
     * 8：减、
     * 9：退
     *
     */
    @Column(name = "CREDIT_CLASSI")
    private String creditClassi;
    
    @Column(name = "CREDIT_SECOND_CLASSI")
    private String creditSecondClassi;
  
    @Column(name = "CREDIT_POINT")
    private String creditPoint; 
    /**
     * 授信分类原因
     */
    @Column(name = "CREDIT_CLASSI_REASON")
    private String creditClassiReason;
    
    
    @Column(name = "CUSTOMER_NAME")
    private String customerName;//客户名字
    @Column(name = "CUSTOMER_CRE_NUM")
    private String customerCreNum;//证件号码
    @Column(name = "ALL_AMOUNT")
    private Double allAmount;//授信总金额  
    @Column(name = "REGISTE_ADDRESS")
	private String registerAddress;//注册地址
    @Column(name = "RUN_ADDRESS")
	private String runAddress;//经营地址
    @Column(name = "LEGAL_PERSON")
	private String legalPerson;//法定代表人
    @Column(name = "CONTROL_PERSON")
	private String controlPerson;//实际控制人
    @Column(name = "RUN_SCOPE")
	private String runScope;//经营范围
    @Column(name = "FIVE_CLASS")
    private String fiveClass;//五级分类
    
    
    @Column(name = "FIRST_BALANCE")
    private Double firstBalance;//首检贷后 对应的余额
    @Column(name = "FIRST_TERM")
    private Integer firstTerm;//首检贷后 对应的期限
    
    //这两个字段是客户信息的冗余，用于调查的时候分金额，分行业的调查模版
    @Column(name = "LOAN_BALANCE")
    private Double loanBalance;//贷款总余额
    @Column(name = "INDUSTRY_TYPE")
    private String industryType;//行业分类
    
    @Column(name = "MARTUITY")
    private String martuity;//到期任务中    月内 最近一笔授信到期时间
    
    @Column(name = "RISKUSER")
    private String riskUser;//对于专职贷后风险经理，需要指定风险经理

    @Column(name = "RISKUSERNAME")
    private String riskUserName;//风险经理名称
    
    
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getTaskType() {
		return taskType;
	}
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	public Integer getTaskPosition() {
		return taskPosition;
	}
	public void setTaskPosition(Integer taskPosition) {
		this.taskPosition = taskPosition;
	}
	public Integer getHasSync() {
		return hasSync;
	}
	public void setHasSync(Integer hasSync) {
		this.hasSync = hasSync;
	}
	public String getUserNo() {
		return userNo;
	}
	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}
	public String getRiskClassi() {
		return riskClassi;
	}
	public void setRiskClassi(String riskClassi) {
		this.riskClassi = riskClassi;
	}
	public String getRiskSecondClassi() {
		return riskSecondClassi;
	}
	public void setRiskSecondClassi(String riskSecondClassi) {
		this.riskSecondClassi = riskSecondClassi;
	}
	public String getRiskPoint() {
		return riskPoint;
	}
	public void setRiskPoint(String riskPoint) {
		this.riskPoint = riskPoint;
	}
	public String getRiskClassiReason() {
		return riskClassiReason;
	}
	public void setRiskClassiReason(String riskClassiReason) {
		this.riskClassiReason = riskClassiReason;
	}
	public String getCreditClassi() {
		return creditClassi;
	}
	public void setCreditClassi(String creditClassi) {
		this.creditClassi = creditClassi;
	}
	public String getCreditSecondClassi() {
		return creditSecondClassi;
	}
	public void setCreditSecondClassi(String creditSecondClassi) {
		this.creditSecondClassi = creditSecondClassi;
	}
	public String getCreditPoint() {
		return creditPoint;
	}
	public void setCreditPoint(String creditPoint) {
		this.creditPoint = creditPoint;
	}
	public String getCreditClassiReason() {
		return creditClassiReason;
	}
	public void setCreditClassiReason(String creditClassiReason) {
		this.creditClassiReason = creditClassiReason;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerCreNum() {
		return customerCreNum;
	}
	public void setCustomerCreNum(String customerCreNum) {
		this.customerCreNum = customerCreNum;
	}
	public Double getAllAmount() {
		return allAmount;
	}
	public void setAllAmount(Double allAmount) {
		this.allAmount = allAmount;
	}
	public String getRegisterAddress() {
		return registerAddress;
	}
	public void setRegisterAddress(String registerAddress) {
		this.registerAddress = registerAddress;
	}
	public String getRunAddress() {
		return runAddress;
	}
	public void setRunAddress(String runAddress) {
		this.runAddress = runAddress;
	}
	public String getLegalPerson() {
		return legalPerson;
	}
	public void setLegalPerson(String legalPerson) {
		this.legalPerson = legalPerson;
	}
	public String getControlPerson() {
		return controlPerson;
	}
	public void setControlPerson(String controlPerson) {
		this.controlPerson = controlPerson;
	}
	public String getRunScope() {
		return runScope;
	}
	public void setRunScope(String runScope) {
		this.runScope = runScope;
	}
	public String getFiveClass() {
		return fiveClass;
	}
	public void setFiveClass(String fiveClass) {
		this.fiveClass = fiveClass;
	}
	public Double getFirstBalance() {
		return firstBalance;
	}
	public void setFirstBalance(Double firstBalance) {
		this.firstBalance = firstBalance;
	}
	public Integer getFirstTerm() {
		return firstTerm;
	}
	public void setFirstTerm(Integer firstTerm) {
		this.firstTerm = firstTerm;
	}
	public Double getLoanBalance() {
		return loanBalance;
	}
	public void setLoanBalance(Double loanBalance) {
		this.loanBalance = loanBalance;
	}
	public String getIndustryType() {
		return industryType;
	}
	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getMartuity() {
		return martuity;
	}
	public void setMartuity(String martuity) {
		this.martuity = martuity;
	}
	public String getRiskUser() {
		return riskUser;
	}
	public void setRiskUser(String riskUser) {
		this.riskUser = riskUser;
	}
	public String getRiskUserName() {
		return riskUserName;
	}
	public void setRiskUserName(String riskUserName) {
		this.riskUserName = riskUserName;
	}
	
}
