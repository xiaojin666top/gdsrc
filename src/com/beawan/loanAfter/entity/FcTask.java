package com.beawan.loanAfter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;

/**
 * 五级分类任务
 * @author yuzhejia
 *
 */
@Entity
@Table(name = "LFC_TASK",schema = "GDTCESYS")
public class FcTask extends BaseEntity {
	@Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LFC_TASK_SEQ")
   // @SequenceGenerator(name="LFC_TASK_SEQ",allocationSize=1,initialValue=1, sequenceName="LFC_TASK_SEQ")
    private Integer id;//taskId

    @Column(name = "TASK_NAME")
    private String taskName;//五级分类任务名称
    
    @Column(name = "TASK_POSITION")
    private Integer taskPosition;//五级分类任务所处阶段      1： 新建（待处理）    99：完成（提交）
    
    @Column(name = "CREATER_NAME")
    private String createrName;//创建任务 人名称

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}


	public Integer getTaskPosition() {
		return taskPosition;
	}

	public void setTaskPosition(Integer taskPosition) {
		this.taskPosition = taskPosition;
	}

	public String getCreaterName() {
		return createrName;
	}

	public void setCreaterName(String createrName) {
		this.createrName = createrName;
	}
    
    
    
    
}
