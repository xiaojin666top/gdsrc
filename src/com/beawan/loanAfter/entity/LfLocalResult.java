package com.beawan.loanAfter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;

/**
 * 非现场贷后中的司法诉讼及其他信息
 * @author User
 *
 */

@Entity
@Table(name = "LF_LOCAL_RESULT", schema = "GDTCESYS")
public class LfLocalResult extends BaseEntity{
	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LF_LOCAL_RESULT_SEQ")
	//@SequenceGenerator(name = "LF_LOCAL_RESULT_SEQ", allocationSize = 1, initialValue = 1, sequenceName = "LF_LOCAL_RESULT_SEQ")
	private Integer id;
	@Column(name = "TASK_ID")
	private Integer taskId;// 贷后任务id
	@Column(name = "CASE_COUNT")
	private Integer caseCount;//诉讼中的案件个数
	@Column(name = "IS_ENFORCE")
	private Integer isEnforce;//是否是法院强制执行   	0否   1是
	@Column(name = "IS_BREACH_TRUST")
	private Integer isBreachTrust;//是否是失信人名单	0否   1是
	@Column(name = "LOCAL_OPTION")
	private String localOption;//说明
	@Column(name = "OTHER_INFO")
	private String otherInfo;//其他信息
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getTaskId() {
		return taskId;
	}
	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}
	public Integer getCaseCount() {
		return caseCount;
	}
	public void setCaseCount(Integer caseCount) {
		this.caseCount = caseCount;
	}
	public Integer getIsEnforce() {
		return isEnforce;
	}
	public void setIsEnforce(Integer isEnforce) {
		this.isEnforce = isEnforce;
	}
	public Integer getIsBreachTrust() {
		return isBreachTrust;
	}
	public void setIsBreachTrust(Integer isBreachTrust) {
		this.isBreachTrust = isBreachTrust;
	}
	public String getLocalOption() {
		return localOption;
	}
	public void setLocalOption(String localOption) {
		this.localOption = localOption;
	}
	public String getOtherInfo() {
		return otherInfo;
	}
	public void setOtherInfo(String otherInfo) {
		this.otherInfo = otherInfo;
	}
	
}
