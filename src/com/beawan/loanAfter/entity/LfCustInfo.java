package com.beawan.loanAfter.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * @Author: xyh
 * @Date: 19/08/2020
 * @Description: 贷后特有的客户信息表 --- 每天会被同步，所以任何需要显示在报告中的信息，都不可以从这里取
 *               仅用于生成贷后任务
 */
@Entity
@Table(name = "LF_CUST_INFO",schema = "GDTCESYS")
public class LfCustInfo extends BaseEntity {
    @Id
  //  @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LF_CUST_INFO_SEQ")
   // @SequenceGenerator(name="LF_CUST_INFO_SEQ",allocationSize=1,initialValue=1, sequenceName="LF_CUST_INFO_SEQ")
    private Integer id;
    @Column(name = "CUSTOMER_NO")
    private String customerNo;//客户编号--对公系统的客户编号
    @Column(name = "INDUSTRY_TYPE")
    private String industryType;//行业分类---先取entInfo中的信息
    
    @Column(name = "LAST_LOAN_SUM")
    private Double lastLoanSum;//贷款金额
    @Column(name = "LAST_LOAN_BALANCE")
    private Double lastLoanBalance;//贷款余额  --->目前失效，不需要使用

    /**
     * 风险分类：
     *  01 ：正常类
     *  02：关注类
     *  03：风险一级
     *  04：风险二级
     *  05：风险三级
     */
    @Column(name = "RISK_CLASSI")
    private String riskClassi;
    /**
     * 授信分类：
     * 01：增加、
     * 02：维持、
     * 03：减少、
     * 04：退出
     *
     */
    @Column(name = "CREDIT_CLASSI")
    private String creditClassi;
    /**
     * 最近一笔贷款结束时间
     * 用于三个月授信到期
     */
    @Column(name = "LAST_STOP_DATE")
    private String lastStopDate;
    /**
     * 最近一笔放款日期
     * 用于首检----首检是放款5天以后  5-15天内首检结束
     */
    @Column(name = "LAST_START_DATE")
    private String lastStartDate;

    /**
     *  0 否新增
     *  1 是新增
     *  new这个客户的时候，统一默认是 0
     *  然后会在跑批放款台账的时候，判断是否为新增
     *  如果放款台账中未结清金额大于上次同步金额，为新增
     */
    @Column(name = "IS_ADD_CUST")
    private String isAddCust;//标识是否为增量的客户-----包括两种，增量，存量增量，如果是增量，会需要做首检
    @Column(name = "LAST_LN_LN_ACCT_NO")
    private String lastLnLnAcctNo;//最近一笔 信贷业务合同编号


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }
    
    public String getIndustryType() {
		return industryType;
	}

	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}

	public Double getLastLoanSum() {
        return lastLoanSum;
    }

    public void setLastLoanSum(Double lastLoanSum) {
        this.lastLoanSum = lastLoanSum;
    }

    public Double getLastLoanBalance() {
        return lastLoanBalance;
    }

    public void setLastLoanBalance(Double lastLoanBalance) {
        this.lastLoanBalance = lastLoanBalance;
    }

    public String getIsAddCust() {
        return isAddCust;
    }

    public void setIsAddCust(String isAddCust) {
        this.isAddCust = isAddCust;
    }

    public String getRiskClassi() {
        return riskClassi;
    }

    public void setRiskClassi(String riskClassi) {
        this.riskClassi = riskClassi;
    }

    public String getCreditClassi() {
        return creditClassi;
    }

    public void setCreditClassi(String creditClassi) {
        this.creditClassi = creditClassi;
    }

    public String getLastStopDate() {
        return lastStopDate;
    }

    public void setLastStopDate(String lastStopDate) {
        this.lastStopDate = lastStopDate;
    }

    public String getLastStartDate() {
        return lastStartDate;
    }

    public void setLastStartDate(String lastStartDate) {
        this.lastStartDate = lastStartDate;
    }

    public String getLastLnLnAcctNo() {
        return lastLnLnAcctNo;
    }

    public void setLastLnLnAcctNo(String lastLnLnAcctNo) {
        this.lastLnLnAcctNo = lastLnLnAcctNo;
    }
}
