package com.beawan.loanAfter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;
/**
 * 
 * @Author: xyh
 * @Date: 2020年9月4日
 * @Description:贷后内控名单
 */
@Entity
@Table(name = "LF_INTERNAL_CONTROL_INFO",schema = "GDTCESYS")
public class LfInternalControlInfo extends BaseEntity{
	
	@Id
  //  @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LF_INTERNAL_CONTROL_INFO_SEQ")
   // @SequenceGenerator(name="LF_INTERNAL_CONTROL_INFO_SEQ",allocationSize=1,initialValue=1, sequenceName="LF_INTERNAL_CONTROL_INFO_SEQ")
    private Integer id;
	
    @Column(name = "TASK_ID")
    private Integer taskId;//贷后任务id
    
    @Column(name = "LIST_NAME")
    private String listName;//名单名称
    
    @Column(name = "ADD_LIST_TIME")
    private String addListTime;//加入名单时间
    
    @Column(name = "ADD_LIST_REASON")
    private String addListReason;//加入名单原因
    
    @Column(name = "CHECK_OPTION")
    private String checkOption;//核查情况

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public String getListName() {
		return listName;
	}

	public void setListName(String listName) {
		this.listName = listName;
	}

	public String getAddListTime() {
		return addListTime;
	}

	public void setAddListTime(String addListTime) {
		this.addListTime = addListTime;
	}

	public String getAddListReason() {
		return addListReason;
	}

	public void setAddListReason(String addListReason) {
		this.addListReason = addListReason;
	}

	public String getCheckOption() {
		return checkOption;
	}

	public void setCheckOption(String checkOption) {
		this.checkOption = checkOption;
	}  
    
}
