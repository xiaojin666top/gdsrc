package com.beawan.loanAfter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;

/**
 * 五级分类任务详情 --》任务对应的客户详细在五级分类报告
 * @author yuzhejia
 *
 */
@Entity
@Table(name = "LFC_CUST_FULL_INFO",schema = "GDTCESYS")
public class FcCustFullInfo extends BaseEntity {
	@Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LFC_CUST_FULL_INFO_SEQ")
   // @SequenceGenerator(name="LFC_CUST_FULL_INFO_SEQ",allocationSize=1,initialValue=1, sequenceName="LFC_CUST_FULL_INFO_SEQ")
    private Integer id;

    @Column(name = "TASK_CUST_ID")
    private Integer taskCustId;// 五级分类任务客户表id
    @Column(name = "CUSTOMER_NAME")
    private String customerName;//客户名称

    @Column(name = "START_DATE")
    private String startDate;//成立时间
    @Column(name = "ADDRESS")
    private String address;//经营地址
    @Column(name = "REGIST_CAPI")
    private String registCapi;//注册资本
    @Column(name = "REC_CAP")
    private String recCap;//实收资本
    @Column(name = "OPER_NAME")
    private String operName;//法人
    @Column(name = "ENT_TYPE")
    private String entType;//企业类型
    @Column(name = "AREA_COVERED")
    private String areaCovered;//占地面积
    @Column(name = "WORKS")
    private Integer works;//工人人数
    @Column(name = "INDU_CODE")
    private String induCode;//行业代码
    @Column(name = "INDU_NAME")
    private String induName;//行业名称
    @Column(name = "SCOPE")
    private String scope;//经营范围

    @Column(name = "REPORT_DATE")
    private String reportDate;//取数时间：yyyy年MM月dd日
    @Column(name = "DEPOSIT")
    private String deposit;//存款账户，多个账户用逗号连接
    @Column(name = "DEPOSIT_SUM")
    private Double depositSum=0.00d;//存款总额
    @Column(name = "LOAN_SUM")
    private Double loanSum=0.00d;//贷款总额
    
    //财务数据
    @Column(name = "FINA_DATE")
    private String finaDate;//财报时间 :yyyyMM
    @Column(name = "BALANCE")
    private Double balance=0.00d;//资产总额
    @Column(name = "DEBT")
    private Double debt=0.00d;//负债总额
    @Column(name = "OWNERS")
    private Double owners=0.00d;//所有者权益
    @Column(name = "SALES")
    private Double sales=0.00d;//销售收入
    @Column(name = "PROFITS")
    private Double profits=0.00d;//利润
    
    @Column(name = "PROFIT_ANALY")
    private String profitAnaly;//盈利能力分析
    @Column(name = "OPERATE_ANALY")
    private String operateAnaly;//营运能力分析
    @Column(name = "DEBT_ANALY")
    private String debtAnaly;//偿债能力分析
    
    @Column(name = "CASHFLOW_SUM")
    private Double cashflowSum;//总现金流
    @Column(name = "CASHFLOW_OPERATE")
    private Double cashflowOperate;//经营活动现金流
    @Column(name = "CASHFLOW_INVEST")
    private Double cashflowInvest;//投资性现金流
    @Column(name = "CASHFLOW_FINANC")
    private Double cashflowFinanc;//筹资性现金流
    @Column(name = "CASHFLOW_ANALY")
    private String cashflowAnaly;//现金流分析
    //非财务分析  风险分析
    @Column(name = "INDU_RISK")
    private String induRisk;//行业风险
    @Column(name = "OPERATE_RISK")
    private String operateRisk;//经营风险
    @Column(name = "MANAGER_RISK")
    private String managerRisk;//管理风险
    //担保分析
    @Column(name = "GUARATER")
    private String guarater;//担保方式
    @Column(name = "SPONSOR_ANALY")
    private String sponsorAnaly;//保证人分析
    @Column(name = "COLLATERAL_ANALY")
    private String collateralAnaly;//抵押物分析
    
    @Column(name = "CLASSIFY_REASON")
    private String classifyReason;//分类理由
    @Column(name = "FIVE_CLASS")
    private String fiveClass;//五级分类结果
    
    
	public FcCustFullInfo() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTaskCustId() {
		return taskCustId;
	}

	public void setTaskCustId(Integer taskCustId) {
		this.taskCustId = taskCustId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getRegistCapi() {
		return registCapi;
	}

	public void setRegistCapi(String registCapi) {
		this.registCapi = registCapi;
	}

	public String getRecCap() {
		return recCap;
	}

	public void setRecCap(String recCap) {
		this.recCap = recCap;
	}

	public String getOperName() {
		return operName;
	}

	public void setOperName(String operName) {
		this.operName = operName;
	}

	public String getEntType() {
		return entType;
	}

	public void setEntType(String entType) {
		this.entType = entType;
	}

	public String getAreaCovered() {
		return areaCovered;
	}

	public void setAreaCovered(String areaCovered) {
		this.areaCovered = areaCovered;
	}

	public Integer getWorks() {
		return works;
	}

	public void setWorks(Integer works) {
		this.works = works;
	}

	public String getInduCode() {
		return induCode;
	}

	public void setInduCode(String induCode) {
		this.induCode = induCode;
	}

	public String getInduName() {
		return induName;
	}

	public void setInduName(String induName) {
		this.induName = induName;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getReportDate() {
		return reportDate;
	}

	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}

	public String getDeposit() {
		return deposit;
	}

	public void setDeposit(String deposit) {
		this.deposit = deposit;
	}

	public Double getDepositSum() {
		return depositSum;
	}

	public void setDepositSum(Double depositSum) {
		this.depositSum = depositSum;
	}

	public Double getLoanSum() {
		return loanSum;
	}

	public void setLoanSum(Double loanSum) {
		this.loanSum = loanSum;
	}

	public String getFinaDate() {
		return finaDate;
	}

	public void setFinaDate(String finaDate) {
		this.finaDate = finaDate;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public Double getDebt() {
		return debt;
	}

	public void setDebt(Double debt) {
		this.debt = debt;
	}

	public Double getOwners() {
		return owners;
	}

	public void setOwners(Double owners) {
		this.owners = owners;
	}

	public Double getSales() {
		return sales;
	}

	public void setSales(Double sales) {
		this.sales = sales;
	}

	public Double getProfits() {
		return profits;
	}

	public void setProfits(Double profits) {
		this.profits = profits;
	}

	public String getProfitAnaly() {
		return profitAnaly;
	}

	public void setProfitAnaly(String profitAnaly) {
		this.profitAnaly = profitAnaly;
	}

	public String getOperateAnaly() {
		return operateAnaly;
	}

	public void setOperateAnaly(String operateAnaly) {
		this.operateAnaly = operateAnaly;
	}

	public String getDebtAnaly() {
		return debtAnaly;
	}

	public void setDebtAnaly(String debtAnaly) {
		this.debtAnaly = debtAnaly;
	}

	public Double getCashflowSum() {
		return cashflowSum;
	}

	public void setCashflowSum(Double cashflowSum) {
		this.cashflowSum = cashflowSum;
	}

	public Double getCashflowOperate() {
		return cashflowOperate;
	}

	public void setCashflowOperate(Double cashflowOperate) {
		this.cashflowOperate = cashflowOperate;
	}

	public Double getCashflowInvest() {
		return cashflowInvest;
	}

	public void setCashflowInvest(Double cashflowInvest) {
		this.cashflowInvest = cashflowInvest;
	}

	public Double getCashflowFinanc() {
		return cashflowFinanc;
	}

	public void setCashflowFinanc(Double cashflowFinanc) {
		this.cashflowFinanc = cashflowFinanc;
	}

	public String getCashflowAnaly() {
		return cashflowAnaly;
	}

	public void setCashflowAnaly(String cashflowAnaly) {
		this.cashflowAnaly = cashflowAnaly;
	}

	public String getInduRisk() {
		return induRisk;
	}

	public void setInduRisk(String induRisk) {
		this.induRisk = induRisk;
	}

	public String getOperateRisk() {
		return operateRisk;
	}

	public void setOperateRisk(String operateRisk) {
		this.operateRisk = operateRisk;
	}

	public String getManagerRisk() {
		return managerRisk;
	}

	public void setManagerRisk(String managerRisk) {
		this.managerRisk = managerRisk;
	}

	public String getGuarater() {
		return guarater;
	}

	public void setGuarater(String guarater) {
		this.guarater = guarater;
	}

	public String getSponsorAnaly() {
		return sponsorAnaly;
	}

	public void setSponsorAnaly(String sponsorAnaly) {
		this.sponsorAnaly = sponsorAnaly;
	}

	public String getCollateralAnaly() {
		return collateralAnaly;
	}

	public void setCollateralAnaly(String collateralAnaly) {
		this.collateralAnaly = collateralAnaly;
	}

	public String getClassifyReason() {
		return classifyReason;
	}

	public void setClassifyReason(String classifyReason) {
		this.classifyReason = classifyReason;
	}

	public String getFiveClass() {
		return fiveClass;
	}

	public void setFiveClass(String fiveClass) {
		this.fiveClass = fiveClass;
	}
}
