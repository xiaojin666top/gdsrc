package com.beawan.loanAfter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.omg.CORBA.PRIVATE_MEMBER;

import com.beawan.core.BaseEntity;

/**
 * 贷后财务状况检查
 * @author xyh
 *
 */
@Entity
@Table(name = "LF_FIN_CHECK",schema = "GDTCESYS")
public class LfFinCheck extends BaseEntity {
	@Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LF_FIN_CHECK_SEQ")
  //  @SequenceGenerator(name="LF_FIN_CHECK_SEQ",allocationSize=1,initialValue=1, sequenceName="LF_FIN_CHECK_SEQ")
    private Integer id;
	
    @Column(name = "TASK_ID")
    private Integer taskId;//贷后任务id
    
    @Column(name = "SUBJECT_ID")
    private String subjectId;//科目编号
    
    @Column(name = "SUBJECT_NAME")
    private String subjectName;//科目名称
    
    @Column(name = "CHECKING_AMOUNT")
    private Double checkingAmount;//检查时财务数据
    
    @Column(name = "HAS_CHANGE")
    private String hasChage;//是否有差异
    
    @Column(name = "CHANGE_REASON")
    private String changeReason;//差异原因

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public String getSubjectId() {
		return subjectId;
	}

	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public Double getCheckingAmount() {
		return checkingAmount;
	}

	public void setCheckingAmount(Double checkingAmount) {
		this.checkingAmount = checkingAmount;
	}

	public String getHasChage() {
		return hasChage;
	}

	public void setHasChage(String hasChage) {
		this.hasChage = hasChage;
	}

	public String getChangeReason() {
		return changeReason;
	}

	public void setChangeReason(String changeReason) {
		this.changeReason = changeReason;
	}
    
}
