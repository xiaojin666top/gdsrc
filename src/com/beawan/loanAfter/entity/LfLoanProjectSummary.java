package com.beawan.loanAfter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;
/**
 * 
 * @Author: xyh
 * @Date: 2020年9月9日
 * @Description: 申贷项目概况-----可能会不唯一
 */
@Entity
@Table(name = "LF_LOAN_PROJECT_SUMMARY", schema = "GDTCESYS")
public class LfLoanProjectSummary extends BaseEntity{
	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LF_LOAN_PROJECT_SUMMARY_SEQ")
	//@SequenceGenerator(name = "LF_LOAN_PROJECT_SUMMARY_SEQ", allocationSize = 1, initialValue = 1, sequenceName = "LF_LOAN_PROJECT_SUMMARY_SEQ")
	private Integer id;
	@Column(name = "TASK_ID")
	private Integer taskId;// 贷后任务id
	@Column(name = "PROJECT_SITE_CONDITION")
	private String projectSiteCondition;//项目现场情况
	@Column(name = "INTRODUCE_OWNER")
	private String introduceOwner;//业主方介绍
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getTaskId() {
		return taskId;
	}
	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}
	public String getProjectSiteCondition() {
		return projectSiteCondition;
	}
	public void setProjectSiteCondition(String projectSiteCondition) {
		this.projectSiteCondition = projectSiteCondition;
	}
	public String getIntroduceOwner() {
		return introduceOwner;
	}
	public void setIntroduceOwner(String introduceOwner) {
		this.introduceOwner = introduceOwner;
	}
}
