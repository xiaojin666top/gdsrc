package com.beawan.loanAfter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;



/**
 * @Author: xyh
 * @Date: 2020年9月3日
 * @Description:情况检查-----经营情况、担保情况
 */
@Entity
@Table(name = "LF_SITUATION_CHECK", schema = "GDTCESYS")
public class LfSituationCheck extends BaseEntity {

	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LF_SITUATION_CHECK_SEQ")
	//@SequenceGenerator(name = "LF_SITUATION_CHECK_SEQ", allocationSize = 1, initialValue = 1, sequenceName = "LF_SITUATION_CHECK_SEQ")
	private Integer id;
	@Column(name = "TASK_ID")
	private Integer taskId;// 贷后任务id
	/**
	 * JY-01 表示是：经营情况下的外部环境
	 */
	@Column(name = "TYPE_FIRST_NO")
	private String typeFirstNo;
	
	@Column(name = "TYPE_FIRST_NAME")
	private String typeFirstName;
	
	@Column(name = "SITUATION_ENNAME")
	private String situationEnName;
	
	@Column(name = "SITUATION_CNNAME")
	private String situationCnName;
	
	@Column(name = "HAS_CHANGE")
    private String hasChage;//是否有差异
    
    @Column(name = "CHANGE_REMARK")
    private String changeRemark;//差异原因

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public String getTypeFirstNo() {
		return typeFirstNo;
	}

	public void setTypeFirstNo(String typeFirstNo) {
		this.typeFirstNo = typeFirstNo;
	}

	public String getTypeFirstName() {
		return typeFirstName;
	}

	public void setTypeFirstName(String typeFirstName) {
		this.typeFirstName = typeFirstName;
	}

	public String getSituationEnName() {
		return situationEnName;
	}

	public void setSituationEnName(String situationEnName) {
		this.situationEnName = situationEnName;
	}

	public String getSituationCnName() {
		return situationCnName;
	}

	public void setSituationCnName(String situationCnName) {
		this.situationCnName = situationCnName;
	}

	public String getHasChage() {
		return hasChage;
	}

	public void setHasChage(String hasChage) {
		this.hasChage = hasChage;
	}

	public String getChangeRemark() {
		return changeRemark;
	}

	public void setChangeRemark(String changeRemark) {
		this.changeRemark = changeRemark;
	}

	
	
	
	
	
	
	
	

}
