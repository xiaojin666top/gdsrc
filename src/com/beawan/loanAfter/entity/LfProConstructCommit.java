package com.beawan.loanAfter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;
/**
 * 
 * @Author: xyh
 * @Date: 2020年9月23日
 * @Description:项目建设和投产信息
 */
@Entity
@Table(name = "LF_PRO_CONSTRUCT_COMMIT", schema = "GDTCESYS")
public class LfProConstructCommit extends BaseEntity{
	@Id
//	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LF_PRO_CONSTRUCT_COMMIT_SEQ")
	//@SequenceGenerator(name = "LF_PRO_CONSTRUCT_COMMIT_SEQ", allocationSize = 1, initialValue = 1, sequenceName = "LF_PRO_CONSTRUCT_COMMIT_SEQ")
	private Integer id;
	@Column(name = "TASK_ID")
	private Integer taskId;// 贷后任务id
	
	@Column(name = "TOTAL_INVESTMENT")
	private Double totalInvestment;//项目总投资
	
	@Column(name = "CONSTRUCTION_PERIOD")
	private Integer constructionPeriod;//项目总建设期
	
	@Column(name = "OWN_FUNDS_INVESTED")
	private Double ownFundsInvested;//自有资金投入
	
	@Column(name = "PROJECT_PROGRESS")
	private Double projectProgress;//项目进度(%)
	
	@Column(name = "EXTERNAL_FUNDS_INVESTED")
	private Double externalFundsInvested;//外部融资已投入
	
	@Column(name = "OTHER_FUNDS_INVESTED")
	private Double otherFundsInvested;//其他集资已投入
	
	@Column(name = "PRO_INVESTMENT_GAP")
	private String proInvestmentGap;//项目投资差距
	
	@Column(name = "PRO_SPRCT_FORECAST")
	private String prospectForecast;//项目前景预测
	
	@Column(name = "OVERALL_SITUATION")
	private String overallSituation;//项目投资整体情况

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public Double getTotalInvestment() {
		return totalInvestment;
	}

	public void setTotalInvestment(Double totalInvestment) {
		this.totalInvestment = totalInvestment;
	}

	public Integer getConstructionPeriod() {
		return constructionPeriod;
	}

	public void setConstructionPeriod(Integer constructionPeriod) {
		this.constructionPeriod = constructionPeriod;
	}

	public Double getOwnFundsInvested() {
		return ownFundsInvested;
	}

	public void setOwnFundsInvested(Double ownFundsInvested) {
		this.ownFundsInvested = ownFundsInvested;
	}

	public Double getProjectProgress() {
		return projectProgress;
	}

	public void setProjectProgress(Double projectProgress) {
		this.projectProgress = projectProgress;
	}

	public Double getExternalFundsInvested() {
		return externalFundsInvested;
	}

	public void setExternalFundsInvested(Double externalFundsInvested) {
		this.externalFundsInvested = externalFundsInvested;
	}

	public Double getOtherFundsInvested() {
		return otherFundsInvested;
	}

	public void setOtherFundsInvested(Double otherFundsInvested) {
		this.otherFundsInvested = otherFundsInvested;
	}

	public String getProInvestmentGap() {
		return proInvestmentGap;
	}

	public void setProInvestmentGap(String proInvestmentGap) {
		this.proInvestmentGap = proInvestmentGap;
	}

	public String getProspectForecast() {
		return prospectForecast;
	}

	public void setProspectForecast(String prospectForecast) {
		this.prospectForecast = prospectForecast;
	}

	public String getOverallSituation() {
		return overallSituation;
	}

	public void setOverallSituation(String overallSituation) {
		this.overallSituation = overallSituation;
	}
	
	
	
	
}
