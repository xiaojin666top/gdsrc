package com.beawan.loanAfter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;

/**
 * 贷后客户风险分类标准 
 * @author lk
 *
 */
@Entity
@Table(name = "LF_RISK_SORT",schema = "GDTCESYS")
public class LfRiskSort extends BaseEntity{
	
	@Id
  //  @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LF_RISK_SORT_SEQ")
  //  @SequenceGenerator(name="LF_RISK_SORT_SEQ",allocationSize=1,initialValue=1, sequenceName="LF_RISK_SORT_SEQ")
    private Integer id;
	
	@Column(name = "RISK_NO")
	private String riskNo;//客户风险分类标准号：01：正常类; 02:关注类; 03:风险一级; 04:风险二级; 05:风险三级。（与客户授信跟踪管理同数据库）
						//客户授信跟踪管理分类号：06:增加授信； 07：维持授信；08：减少授信；09：退出授信
	@Column(name = "RISK_CONTENT")
	private String riskContent;//风险点所属小类
	
	@Column(name = "RISK_POINT")
	private String riskPoint;//风险点

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRiskNo() {
		return riskNo;
	}

	public void setRiskNo(String riskNo) {
		this.riskNo = riskNo;
	}

	public String getRiskContent() {
		return riskContent;
	}

	public void setRiskContent(String riskContent) {
		this.riskContent = riskContent;
	}

	public String getRiskPoint() {
		return riskPoint;
	}

	public void setRiskPoint(String riskPoint) {
		this.riskPoint = riskPoint;
	}
	
	
	
	

}
