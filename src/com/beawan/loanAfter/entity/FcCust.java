package com.beawan.loanAfter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;

/**
 * 五级分类任务详情 --》任务对应的客户
 * @author yuzhejia
 *
 */
@Entity
@Table(name = "LFC_TASK_CUST",schema = "GDTCESYS")
public class FcCust extends BaseEntity {
	@Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LFC_TASK_CUST_SEQ")
  //  @SequenceGenerator(name="LFC_TASK_CUST_SEQ",allocationSize=1,initialValue=1, sequenceName="LFC_TASK_CUST_SEQ")
    private Integer id;//taskCustId

    @Column(name = "TASK_ID")
    private Integer taskId;//五级分类任务id
    
    @Column(name = "CUSTOMER_NO")
    private String customerNo;//客户编号--对公系统的客户编号
    
    @Column(name = "STATE")
    private Integer state;//当前客户五级分类报告是否完成   1未完成   2完成

    
    
	public FcCust() {
		super();
	}
	

	public FcCust(Integer taskId, String customerNo, Integer state) {
		super();
		this.taskId = taskId;
		this.customerNo = customerNo;
		this.state = state;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}
    

    
    
    
}
