package com.beawan.loanAfter.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * @Author: xyh
 * @Date: 25/08/2020
 * @Description: 贷后检查结论信息表
 */
@Entity
@Table(name = "LF_TASK_RESULT",schema = "GDTCESYS")
public class LfTaskResult extends BaseEntity {
    @Id
  //  @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LF_TASK_RESULT_SEQ")
  //  @SequenceGenerator(name="LF_TASK_RESULT_SEQ",allocationSize=1,initialValue=1, sequenceName="LF_TASK_RESULT_SEQ")
    private Integer id;
    
    @Column(name = "TASK_ID")
    private Integer taskId;
    
    @Column(name = "FIFTEEN_DAY_RESULT")
    private String fifteenDayResult;//15日首检结论
    
    @Column(name = "CUST_CHANGE_REASON")
    private String custChangeReason;//客户基本信息变化情况的变化原因
    
    @Column(name = "UN_LOCAL_RISK_SUMMARY")
    private String unLocalRiskSummary;//非现场风险信号概述
    
    @Column(name = "LOCAL_RISK_MANAGING")
    private String localRiskManaging;//现场贷后管理风险
    
    @Column(name = "LOCAL_RISK_BUSINESS")
    private String localRiskBusiness;//现场贷后经营风险
    
    @Column(name = "LOCAL_RISK_FINANCIAL")
    private String localRiskFinancial;//现场贷后财务风险
    
    @Column(name = "LOCAL_RISK_GUARANTEE")
    private String localRiskGuarantee;//现场贷后担保风险
    
    @Column(name = "PREVENTIVE_MEASURES")
    private String preventiveMeasures;//防范措施
    
    @Column(name = "OHTER_CHECK_INFO")
    private String otherCheckInfo;//发现的其他检查情况
    
    @Column(name = "ANALYSIS_REASON")
    private String analysisReason;//分析说明
    
    @Column(name = "VERIFICATIVE_INFO")
    private String verificativeInfo;//授信条件落实情况
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getFifteenDayResult() {
        return fifteenDayResult;
    }

    public void setFifteenDayResult(String fifteenDayResult) {
        this.fifteenDayResult = fifteenDayResult;
    }

	public String getCustChangeReason() {
		return custChangeReason;
	}

	public void setCustChangeReason(String custChangeReason) {
		this.custChangeReason = custChangeReason;
	}

	public String getUnLocalRiskSummary() {
		return unLocalRiskSummary;
	}

	public void setUnLocalRiskSummary(String unLocalRiskSummary) {
		this.unLocalRiskSummary = unLocalRiskSummary;
	}

	public String getLocalRiskManaging() {
		return localRiskManaging;
	}

	public void setLocalRiskManaging(String localRiskManaging) {
		this.localRiskManaging = localRiskManaging;
	}

	public String getLocalRiskBusiness() {
		return localRiskBusiness;
	}

	public void setLocalRiskBusiness(String localRiskBusiness) {
		this.localRiskBusiness = localRiskBusiness;
	}

	public String getLocalRiskFinancial() {
		return localRiskFinancial;
	}

	public void setLocalRiskFinancial(String localRiskFinancial) {
		this.localRiskFinancial = localRiskFinancial;
	}

	public String getLocalRiskGuarantee() {
		return localRiskGuarantee;
	}

	public void setLocalRiskGuarantee(String localRiskGuarantee) {
		this.localRiskGuarantee = localRiskGuarantee;
	}

	public String getPreventiveMeasures() {
		return preventiveMeasures;
	}

	public void setPreventiveMeasures(String preventiveMeasures) {
		this.preventiveMeasures = preventiveMeasures;
	}

	public String getOtherCheckInfo() {
		return otherCheckInfo;
	}

	public void setOtherCheckInfo(String otherCheckInfo) {
		this.otherCheckInfo = otherCheckInfo;
	}

	public String getAnalysisReason() {
		return analysisReason;
	}

	public void setAnalysisReason(String analysisReason) {
		this.analysisReason = analysisReason;
	}

	public String getVerificativeInfo() {
		return verificativeInfo;
	}

	public void setVerificativeInfo(String verificativeInfo) {
		this.verificativeInfo = verificativeInfo;
	}
}
