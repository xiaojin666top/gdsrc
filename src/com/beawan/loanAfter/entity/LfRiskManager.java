package com.beawan.loanAfter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;

/**
 * 贷后  机构下风险经理配置
 * @author yuzhejia
 *
 */
@Entity
@Table(name = "LF_RISK_MANAGER",schema = "GDTCESYS")
public class LfRiskManager extends BaseEntity {
	@Id
 //   @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LF_RISK_MANAGER_SEQ")
   // @SequenceGenerator(name="LF_RISK_MANAGER_SEQ",allocationSize=1,initialValue=1, sequenceName="LF_RISK_MANAGER_SEQ")
    private Integer id;//

    @Column(name = "ORGANNO")
    private String organno;//机构码
    
    @Column(name = "ORGANNAME")
    private String organname;//机构名称
    
    @Column(name = "ORGANSHORTFORM")
    private String organshortform;//机构简称
    
    @Column(name = "RISKUSER")
    private String riskuser;//风险经理信贷号
    
    @Column(name = "RISKUSERNAME")
    private String riskusername;//风险经理 名称
    
    
	public LfRiskManager() {
		super();
	}
	


	public LfRiskManager(String organno, String organname, String organshortform, String riskuser,
			String riskusername) {
		super();
		this.organno = organno;
		this.organname = organname;
		this.organshortform = organshortform;
		this.riskuser = riskuser;
		this.riskusername = riskusername;
	}



	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getOrganno() {
		return organno;
	}


	public void setOrganno(String organno) {
		this.organno = organno;
	}


	public String getOrganname() {
		return organname;
	}


	public void setOrganname(String organname) {
		this.organname = organname;
	}


	public String getOrganshortform() {
		return organshortform;
	}


	public void setOrganshortform(String organshortform) {
		this.organshortform = organshortform;
	}


	public String getRiskuser() {
		return riskuser;
	}


	public void setRiskuser(String riskuser) {
		this.riskuser = riskuser;
	}


	public String getRiskusername() {
		return riskusername;
	}


	public void setRiskusername(String riskusername) {
		this.riskusername = riskusername;
	}
	


    
    
    
}
