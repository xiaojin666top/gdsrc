package com.beawan.loanAfter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;

/**
 * 
 * @Author: xyh
 * @Date: 2020年9月17日
 * @Description: 我行融资情况(所有信息直接和任务关联)
 */
@Entity
@Table(name = "LF_CREDIT_SELF_BANK",schema = "GDTCESYS")
public class LfCreditSelfBank extends BaseEntity{
	@Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LF_CREDIT_SELF_BANK_SEQ")
    //@SequenceGenerator(name="LF_CREDIT_SELF_BANK_SEQ",allocationSize=1,initialValue=1, sequenceName="LF_CREDIT_SELF_BANK_SEQ")
    private Integer id;
    @Column(name = "TASK_ID")
    private Integer taskId;//贷后任务id
    
    @Column(name = "PRODUCT_NAME")
    private String productName;//贷款品种
    
    @Column(name = "LOAN_BALANCE")
    private Double loanBalance;//贷款余额
    
    @Column(name = "START_DATE")
    private String startDate;//借款日期
    
    @Column(name = "END_DATE")
    private String endDate;//到期日期
    
    @Column(name = "LOAN_USE")
    private String loanUse;//贷款用途
    
    @Column(name = "GUATEE_WAY")
    private String guateeWay;//担保方式

	public LfCreditSelfBank() {
		super();
	}
	public LfCreditSelfBank(Integer taskId, String productName, Double loanBalance, String startDate, String endDate,
			String loanUse, String guateeWay) {
		super();
		this.taskId = taskId;
		this.productName = productName;
		this.loanBalance = loanBalance;
		this.startDate = startDate;
		this.endDate = endDate;
		this.loanUse = loanUse;
		this.guateeWay = guateeWay;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getTaskId() {
		return taskId;
	}
	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Double getLoanBalance() {
		return loanBalance;
	}
	public void setLoanBalance(Double loanBalance) {
		this.loanBalance = loanBalance;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getLoanUse() {
		return loanUse;
	}
	public void setLoanUse(String loanUse) {
		this.loanUse = loanUse;
	}
	public String getGuateeWay() {
		return guateeWay;
	}
	public void setGuateeWay(String guateeWay) {
		this.guateeWay = guateeWay;
	}
    
}
