package com.beawan.loanAfter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;
/**
 * 
 * @Author: xyh
 * @Date: 2020年9月9日
 * @Description: 申贷项目 细项
 *    与LfLoanProjectSummary一对多
 *    
 */
@Entity
@Table(name = "LF_LOAN_PROJECT_DETAIL", schema = "GDTCESYS")
public class LfLoanProjectDetail extends BaseEntity {
	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LF_LOAN_PROJECT_DETAIL_SEQ")
	//@SequenceGenerator(name = "LF_LOAN_PROJECT_DETAIL_SEQ", allocationSize = 1, initialValue = 1, sequenceName = "LF_LOAN_PROJECT_DETAIL_SEQ")
	private Integer id;
	@Column(name = "TASK_ID")
	private Integer taskId;// 贷后任务id
	
	@Column(name = "DETAIL_TYPE_NO")
	private String detailTypeNo;//类别
	@Column(name = "DETAIL_TYPE_NAME")
	private String detailTypeName;//类别
	
	@Column(name = "DETAIL_OPTION")
	private String detailOption;//检查情况
	
	@Column(name = "DETAIL_REMARK")
	private String detailRemark;//备注
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getTaskId() {
		return taskId;
	}
	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}
	public String getDetailTypeNo() {
		return detailTypeNo;
	}
	public void setDetailTypeNo(String detailTypeNo) {
		this.detailTypeNo = detailTypeNo;
	}
	public String getDetailTypeName() {
		return detailTypeName;
	}
	public void setDetailTypeName(String detailTypeName) {
		this.detailTypeName = detailTypeName;
	}
	public String getDetailOption() {
		return detailOption;
	}
	public void setDetailOption(String detailOption) {
		this.detailOption = detailOption;
	}
	public String getDetailRemark() {
		return detailRemark;
	}
	public void setDetailRemark(String detailRemark) {
		this.detailRemark = detailRemark;
	}
	
}
