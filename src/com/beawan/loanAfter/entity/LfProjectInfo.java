package com.beawan.loanAfter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;


/**
 * 
 * @Author: xyh
 * @Date: 2020年9月9日
 * @Description: 在建或已完工未完全回款的主要项目情况
 */
@Entity
@Table(name = "LF_PROJECT_INFO", schema = "GDTCESYS")
public class LfProjectInfo extends BaseEntity {

	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LF_PROJECT_INFO_SEQ")
	//@SequenceGenerator(name = "LF_PROJECT_INFO_SEQ", allocationSize = 1, initialValue = 1, sequenceName = "LF_PROJECT_INFO_SEQ")
	private Integer id;
	@Column(name = "TASK_ID")
	private Integer taskId;// 贷后任务id
	
	@Column(name = "PROJECT_NAME")
	private String projectName;//项目名称
	
	@Column(name = "FIRST_PARTY")
	private String firstParty;//甲方
	
	@Column(name = "PROJECT_TYPE")
	private String projectType;//项目类型
	
	@Column(name = "CONTRACT_SUM")
	private Double contractSum;//合同总价
	
	@Column(name = "RECEIVED_PROPORTION")
	private Integer receivedProportion;//已回款比例(%)

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getFirstParty() {
		return firstParty;
	}

	public void setFirstParty(String firstParty) {
		this.firstParty = firstParty;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public Double getContractSum() {
		return contractSum;
	}

	public void setContractSum(Double contractSum) {
		this.contractSum = contractSum;
	}

	public Integer getReceivedProportion() {
		return receivedProportion;
	}

	public void setReceivedProportion(Integer receivedProportion) {
		this.receivedProportion = receivedProportion;
	}
}
