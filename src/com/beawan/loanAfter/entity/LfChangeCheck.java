package com.beawan.loanAfter.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * @Author: xyh
 * @Date: 26/08/2020
 * @Description: 贷后现场调查变化情况信息表
 */
@Entity
@Table(name = "LF_CHANGE_CHECK",schema = "GDTCESYS")
public class LfChangeCheck extends BaseEntity {
    @Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LF_CHANGE_CHECK_SEQ")
    //@SequenceGenerator(name="LF_CHANGE_CHECK_SEQ",allocationSize=1,initialValue=1, sequenceName="LF_CHANGE_CHECK_SEQ")
    private Integer id;
    @Column(name = "TASK_ID")
    private Integer taskId;//贷后任务id
    @Column(name = "CHECK_CNNAME")
    private String checkCnName;//核查名称
    @Column(name = "CHECK_ENNAME")
    private String checkEnName;//核查类型
    @Column(name = "CHECK_TYPE")
    private String checkType;//变动类型：客户信息、融资情况、对外担保
    @Column(name = "CREDIT_SITUATION")
    private String creditSituation;//授信时的情况
    @Column(name = "LF_SITUATION")
    private String lfSituation;//贷后检查时的情
    @Column(name = "CHANGE_REASON")
    private String changeReason;//变动原因

    public LfChangeCheck() {
    }

    public LfChangeCheck(Integer taskId, String checkCnName, String checkEnName, String checkType) {
        this.taskId = taskId;
        this.checkCnName = checkCnName;
        this.checkEnName = checkEnName;
        this.checkType = checkType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getCheckCnName() {
        return checkCnName;
    }

    public void setCheckCnName(String checkCnName) {
        this.checkCnName = checkCnName;
    }

    public String getCheckEnName() {
        return checkEnName;
    }

    public void setCheckEnName(String checkEnName) {
        this.checkEnName = checkEnName;
    }

    public String getCheckType() {
        return checkType;
    }

    public void setCheckType(String checkType) {
        this.checkType = checkType;
    }

    public String getCreditSituation() {
        return creditSituation;
    }

    public void setCreditSituation(String creditSituation) {
        this.creditSituation = creditSituation;
    }

    public String getLfSituation() {
        return lfSituation;
    }

    public void setLfSituation(String lfSituation) {
        this.lfSituation = lfSituation;
    }

    public String getChangeReason() {
        return changeReason;
    }

    public void setChangeReason(String changeReason) {
        this.changeReason = changeReason;
    }
}
