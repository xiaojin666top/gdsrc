package com.beawan.loanAfter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;

/**
 * 五级分类任务详情 --》任务对应的客户 -->贷款合同信息
 * @author yuzhejia
 *
 */
@Entity
@Table(name = "LFC_BUSI_CONTRACT",schema = "GDTCESYS")
public class FcBusiContract extends BaseEntity {
	@Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LFC_BUSI_CONTRACT_SEQ")
    //@SequenceGenerator(name="LFC_BUSI_CONTRACT_SEQ",allocationSize=1,initialValue=1, sequenceName="LFC_BUSI_CONTRACT_SEQ")
    private Integer id;
	@Column(name = "TASK_CUST_ID")
    private Integer taskCustId;// 五级分类任务客户表id
	
    @Column(name = "SERIALNO")
    private String serialNo;//合同号
    @Column(name = "ARTIFICIALNO")
    private String artificialNo;// 手工编号
    @Column(name = "VOUCHTYPE")
    private String vouchType;// 担保类型
    @Column(name = "BALANCE")
    private Double balance;// 余额
    @Column(name = "START_DATE")
    private String startDate;// 合同开始时间
    @Column(name = "END_DATE")
    private String endDate;// 合同结束时间
    @Column(name = "GUARANTYNAME")
    private String guarantyName;// 抵押物名称

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTaskCustId() {
		return taskCustId;
	}

	public void setTaskCustId(Integer taskCustId) {
		this.taskCustId = taskCustId;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getArtificialNo() {
		return artificialNo;
	}

	public void setArtificialNo(String artificialNo) {
		this.artificialNo = artificialNo;
	}

	public String getVouchType() {
		return vouchType;
	}

	public void setVouchType(String vouchType) {
		this.vouchType = vouchType;
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = balance;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getGuarantyName() {
		return guarantyName;
	}

	public void setGuarantyName(String guarantyName) {
		this.guarantyName = guarantyName;
	}
    
    
}
