package com.beawan.loanAfter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;

/**
 * 五级分类任务详情 --》任务对应的客户 -->财报数据
 * @author yuzhejia
 *
 */
@Entity
@Table(name = "LFC_FINA_DATA",schema = "GDTCESYS")
public class FcFinaData extends BaseEntity {
	@Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LFC_FINA_DATA_SEQ")
   // @SequenceGenerator(name="LFC_FINA_DATA_SEQ",allocationSize=1,initialValue=1, sequenceName="LFC_FINA_DATA_SEQ")
    private Integer id;
	@Column(name = "TASK_CUST_ID")
    private Integer taskCustId;// 五级分类任务客户表id
	
    @Column(name = "MOLD")
    private String mold;//---》Constants.FINA_MOLD   01比率指标   02现金流
    
    @Column(name = "ITEM_NAME")
    private String itemName;// 科目名
    @Column(name = "TWO_YEAR_VAL")
    private Double twoYearVal;// 上上年末值
    @Column(name = "ONE_YEAR_VAL")
    private Double oneYearVal;// 上年末值
    @Column(name = "CURR_VAL")
    private Double currVal;// 当期值

    @Column(name = "SORT_INDEX")
    private Integer sortIndex;//排序
    
    
    
    
	public FcFinaData() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public FcFinaData(String mold, String itemName) {
		super();
		this.mold = mold;
		this.itemName = itemName;
	}

	

	public FcFinaData(String mold, String itemName, Double twoYearVal, Double oneYearVal, Double currVal, Integer sortIndex) {
		super();
		this.mold = mold;
		this.itemName = itemName;
		this.twoYearVal = twoYearVal;
		this.oneYearVal = oneYearVal;
		this.currVal = currVal;
		this.sortIndex = sortIndex;
	}


	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getTaskCustId() {
		return taskCustId;
	}
	public void setTaskCustId(Integer taskCustId) {
		this.taskCustId = taskCustId;
	}
	public String getMold() {
		return mold;
	}
	public void setMold(String mold) {
		this.mold = mold;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getTwoYearVal() {
		return twoYearVal;
	}
	public void setTwoYearVal(Double twoYearVal) {
		this.twoYearVal = twoYearVal;
	}
	public Double getOneYearVal() {
		return oneYearVal;
	}
	public void setOneYearVal(Double oneYearVal) {
		this.oneYearVal = oneYearVal;
	}
	public Double getCurrVal() {
		return currVal;
	}
	public void setCurrVal(Double currVal) {
		this.currVal = currVal;
	}


	public Integer getSortIndex() {
		return sortIndex;
	}


	public void setSortIndex(Integer sortIndex) {
		this.sortIndex = sortIndex;
	}
    
	
    
}
