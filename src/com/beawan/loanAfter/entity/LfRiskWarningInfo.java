package com.beawan.loanAfter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;



/**
 * @Author: xyh
 * @Date: 2020年9月3日
 * @Description:非现场贷后---风险预警信息
 */
@Entity
@Table(name = "LF_RISK_WARNING_INFO", schema = "GDTCESYS")
public class LfRiskWarningInfo extends BaseEntity {

	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LF_RISK_WARNING_INFO_SEQ")
	//@SequenceGenerator(name = "LF_RISK_WARNING_INFO_SEQ", allocationSize = 1, initialValue = 1, sequenceName = "LF_RISK_WARNING_INFO_SEQ")
	private Integer id;
	
	@Column(name = "TASK_ID")
	private Integer taskId;// 贷后任务id
	
	@Column(name = "RISK_NO")
	private String riskNo;
	
	@Column(name = "RISK_NAME")
	private String riskName;
	
	@Column(name = "CHECK_RESULT")
	private String checkResult;
	
	@Column(name = "CHECK_OPTION")
	private String checkOption;
	
	@Column(name = "RISK_FROM")
	private String riskFrom;//信号来源：风险预警系统，OAP系统
	
	@Column(name = "HAS_RELIEVE")
	private String hasRelieve;//是否已经解除
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public String getRiskNo() {
		return riskNo;
	}

	public void setRiskNo(String riskNo) {
		this.riskNo = riskNo;
	}

	public String getRiskName() {
		return riskName;
	}

	public void setRiskName(String riskName) {
		this.riskName = riskName;
	}

	public String getCheckResult() {
		return checkResult;
	}

	public void setCheckResult(String checkResult) {
		this.checkResult = checkResult;
	}

	public String getCheckOption() {
		return checkOption;
	}

	public void setCheckOption(String checkOption) {
		this.checkOption = checkOption;
	}

	public String getRiskFrom() {
		return riskFrom;
	}

	public void setRiskFrom(String riskFrom) {
		this.riskFrom = riskFrom;
	}

	public String getHasRelieve() {
		return hasRelieve;
	}

	public void setHasRelieve(String hasRelieve) {
		this.hasRelieve = hasRelieve;
	}
	
	
}
