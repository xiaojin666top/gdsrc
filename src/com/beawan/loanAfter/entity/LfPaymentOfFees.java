package com.beawan.loanAfter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;
/**
 * 
 * @Author: xyh
 * @Date: 2020年9月8日
 * @Description:费用缴纳情况
 */
@Entity
@Table(name = "LF_PAYMENT_OF_FEES", schema = "GDTCESYS")
public class LfPaymentOfFees extends BaseEntity{
	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LF_PAYMENT_OF_FEES_SEQ")
	//@SequenceGenerator(name = "LF_PAYMENT_OF_FEES_SEQ", allocationSize = 1, initialValue = 1, sequenceName = "LF_PAYMENT_OF_FEES_SEQ")
	private Integer id;
	@Column(name = "TASK_ID")
	private Integer taskId;// 贷后任务id
	
	@Column(name = "FEE_TYPE_NO")
	private String feeTypeNo;//费用类别
	@Column(name = "FEE_TYPE_NAME")
	private String feeTypeName;//费用名称
	@Column(name = "IS_NORMAL")
	private String isNormal;//是否正常
	@Column(name = "IS_DOWN")
	private String isDown;//与授信时是否大幅下降
	@Column(name = "ANOMALY_ANALYSIS")
	private String anomalyAnalysis;//异常分析
	@Column(name = "REMARK")
	private String remark;//备注说明
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getTaskId() {
		return taskId;
	}
	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}
	public String getFeeTypeNo() {
		return feeTypeNo;
	}
	public void setFeeTypeNo(String feeTypeNo) {
		this.feeTypeNo = feeTypeNo;
	}
	public String getFeeTypeName() {
		return feeTypeName;
	}
	public void setFeeTypeName(String feeTypeName) {
		this.feeTypeName = feeTypeName;
	}
	public String getIsNormal() {
		return isNormal;
	}
	public void setIsNormal(String isNormal) {
		this.isNormal = isNormal;
	}
	public String getIsDown() {
		return isDown;
	}
	public void setIsDown(String isDown) {
		this.isDown = isDown;
	}
	public String getAnomalyAnalysis() {
		return anomalyAnalysis;
	}
	public void setAnomalyAnalysis(String anomalyAnalysis) {
		this.anomalyAnalysis = anomalyAnalysis;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
}
