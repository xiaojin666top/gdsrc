package com.beawan.loanAfter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


import com.beawan.core.BaseEntity;

/**
 * 
 * @Author: xyh
 * @Date: 2020年9月10日
 * @Description: 1000w以下的特有检查情况结论
 */
@Entity
@Table(name = "LF_DOWN_CHECK_INFO", schema = "GDTCESYS")
public class LfDownCheckInfo extends BaseEntity {
	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LF_DOWN_CHECK_INFO_SEQ")
	//@SequenceGenerator(name = "LF_DOWN_CHECK_INFO_SEQ", allocationSize = 1, initialValue = 1, sequenceName = "LF_DOWN_CHECK_INFO_SEQ")
	private Integer id;
	@Column(name = "TASK_ID")
	private Integer taskId;// 贷后任务id
	
	@Column(name = "BASE_MAIN_CHANGE")
	private String baseMainChange;//借款期间重大变化事项
	@Column(name = "CREDENTIAS_CHECK")
	private String credentiasCheck;//企业资格证书年检情况
	@Column(name = "BUSINESS_INFO")
	private String businessInfo;//生产经营情况的其他情况
	@Column(name = "BUILD_INFO")
	private String buildInfo;//建筑类的其他情况
	@Column(name = "CHECK_INFO")
	private String checkInfo;//授信条件落实情况

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public String getBaseMainChange() {
		return baseMainChange;
	}

	public void setBaseMainChange(String baseMainChange) {
		this.baseMainChange = baseMainChange;
	}

	public String getCheckInfo() {
		return checkInfo;
	}

	public void setCheckInfo(String checkInfo) {
		this.checkInfo = checkInfo;
	}

	public String getCredentiasCheck() {
		return credentiasCheck;
	}

	public void setCredentiasCheck(String credentiasCheck) {
		this.credentiasCheck = credentiasCheck;
	}

	public String getBusinessInfo() {
		return businessInfo;
	}

	public void setBusinessInfo(String businessInfo) {
		this.businessInfo = businessInfo;
	}

	public String getBuildInfo() {
		return buildInfo;
	}

	public void setBuildInfo(String buildInfo) {
		this.buildInfo = buildInfo;
	}
	
	
	
	
}
