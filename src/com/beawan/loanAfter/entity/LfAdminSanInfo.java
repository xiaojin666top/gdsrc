package com.beawan.loanAfter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;

/**
 * 
 * @Author: xyh
 * @Date: 2020年9月4日
 * @Description: 贷后行政处罚信息
 */
@Entity
@Table(name = "LF_ADMIN_SAN_INFO",schema = "GDTCESYS")
public class LfAdminSanInfo extends BaseEntity{
	@Id
  //  @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LF_ADMIN_SAN_INFO_SEQ")
   // @SequenceGenerator(name="LF_ADMIN_SAN_INFO_SEQ",allocationSize=1,initialValue=1, sequenceName="LF_ADMIN_SAN_INFO_SEQ")
    private Integer id;
	
    @Column(name = "TASK_ID")
    private Integer taskId;//贷后任务id
    
    @Column(name = "ORG_NAME")
    private String orgName;//处罚机构
    
    @Column(name = "SUBJECT_MATTER")
    private String subjectMatter;//行政处罚事由
    
    @Column(name = "AMOUNT")
    private Double amount;//行政处罚金额
    
    @Column(name = "CONTENT")
    private String content;//行政处罚事由
    
    @Column(name = "CHECK_OPTION")
    private String checkOption;//核查情况

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getSubjectMatter() {
		return subjectMatter;
	}

	public void setSubjectMatter(String subjectMatter) {
		this.subjectMatter = subjectMatter;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCheckOption() {
		return checkOption;
	}

	public void setCheckOption(String checkOption) {
		this.checkOption = checkOption;
	}
}
