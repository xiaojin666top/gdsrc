package com.beawan.loanAfter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;
/**
 * 
 * @Author: xyh
 * @Date: 2020年9月4日
 * @Description:账户查、冻、扣信息
 */
@Entity
@Table(name = "LF_CFD_ACCOUNT_INFO",schema = "GDTCESYS")
public class LfCfdAccountInfo extends BaseEntity{
	@Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LF_CFD_ACCOUNT_INFO_SEQ")
    //@SequenceGenerator(name="LF_CFD_ACCOUNT_INFO_SEQ",allocationSize=1,initialValue=1, sequenceName="LF_CFD_ACCOUNT_INFO_SEQ")
    private Integer id;
	
    @Column(name = "TASK_ID")
    private Integer taskId;//贷后任务id
    @Column(name = "ACCOUNT_NUM")
    private String accountNum;//帐号

    @Column(name = "SA_ACCD_COD")
    private String saAccdCod;//事故类型代码
    @Column(name = "SA_ACCD_DT")
    private String saAccdDt;//事故发生时间
    @Column(name = "SA_ACCD_AMT")
    private String saAccdAmt;//事故金额
    @Column(name = "ACC_TYPE")
    private String accType;//账户类型   SA 活期    TD 定期
    
    @Column(name = "CFD_ORG")
    private String cfdOrg;//查冻扣机构
    @Column(name = "CFD_TYPE")
    private String cfdType;//查冻扣类型
    @Column(name = "CFD_REASON")
    private String cfdReason;//原因
    @Column(name = "CHECK_OPTION")
    private String checkOption;//核查情况
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getTaskId() {
		return taskId;
	}
	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}
	public String getAccountNum() {
		return accountNum;
	}
	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}
	public String getCfdOrg() {
		return cfdOrg;
	}
	public void setCfdOrg(String cfdOrg) {
		this.cfdOrg = cfdOrg;
	}
	public String getCfdType() {
		return cfdType;
	}
	public void setCfdType(String cfdType) {
		this.cfdType = cfdType;
	}
	public String getCfdReason() {
		return cfdReason;
	}
	public void setCfdReason(String cfdReason) {
		this.cfdReason = cfdReason;
	}
	public String getCheckOption() {
		return checkOption;
	}
	public void setCheckOption(String checkOption) {
		this.checkOption = checkOption;
	}
	public String getSaAccdCod() {
		return saAccdCod;
	}
	public void setSaAccdCod(String saAccdCod) {
		this.saAccdCod = saAccdCod;
	}
	public String getSaAccdDt() {
		return saAccdDt;
	}
	public void setSaAccdDt(String saAccdDt) {
		this.saAccdDt = saAccdDt;
	}
	public String getSaAccdAmt() {
		return saAccdAmt;
	}
	public void setSaAccdAmt(String saAccdAmt) {
		this.saAccdAmt = saAccdAmt;
	}
	public String getAccType() {
		return accType;
	}
	public void setAccType(String accType) {
		this.accType = accType;
	}
	
    
}
