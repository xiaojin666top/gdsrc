package com.beawan.quotaModel.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.quotaModel.bean.Formula;
import com.beawan.quotaModel.dao.IFormulaDAO;

@Repository("formulaDAO")
public class FormulaDAOImpl extends BaseDaoImpl<Formula> implements IFormulaDAO{

}
