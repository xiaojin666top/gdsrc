package com.beawan.quotaModel.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.quotaModel.bean.FormulaParamRel;
import com.beawan.quotaModel.dao.IFormulaParamRelDAO;

@Repository("formulaParamRelDAO")
public class FormulaParamRelDAOImpl extends BaseDaoImpl<FormulaParamRel> implements IFormulaParamRelDAO{

}
