package com.beawan.quotaModel.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.quotaModel.bean.Model;
import com.beawan.quotaModel.dao.IModelDAO;

@Repository("modelDAO")
public class ModelDAOImpl extends BaseDaoImpl<Model> implements IModelDAO{

}
