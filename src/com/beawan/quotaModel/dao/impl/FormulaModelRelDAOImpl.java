package com.beawan.quotaModel.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.quotaModel.bean.FormulaModelRel;
import com.beawan.quotaModel.dao.IFormulaModelRelDAO;

@Repository("formulaModelRelDAO")
public class FormulaModelRelDAOImpl extends BaseDaoImpl<FormulaModelRel> implements IFormulaModelRelDAO{

}
