package com.beawan.quotaModel.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.quotaModel.bean.Param;
import com.beawan.quotaModel.dao.IParamDAO;

@Repository("paramDAO")
public class ParamDAOImpl extends BaseDaoImpl<Param> implements IParamDAO{


}
