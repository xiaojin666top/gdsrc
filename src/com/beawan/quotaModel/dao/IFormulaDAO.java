package com.beawan.quotaModel.dao;

import com.beawan.core.BaseDao;
import com.beawan.quotaModel.bean.Formula;

public interface IFormulaDAO extends BaseDao<Formula> {

}
