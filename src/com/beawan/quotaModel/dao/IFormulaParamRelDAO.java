package com.beawan.quotaModel.dao;

import com.beawan.core.BaseDao;
import com.beawan.quotaModel.bean.FormulaParamRel;

public interface IFormulaParamRelDAO extends BaseDao<FormulaParamRel> {

}
