package com.beawan.quotaModel.dao;

import com.beawan.core.BaseDao;
import com.beawan.quotaModel.bean.Param;

public interface IParamDAO extends BaseDao<Param> {
}
