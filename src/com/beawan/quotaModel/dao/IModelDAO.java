package com.beawan.quotaModel.dao;

import com.beawan.core.BaseDao;
import com.beawan.quotaModel.bean.Model;

public interface IModelDAO extends BaseDao<Model> {

}
