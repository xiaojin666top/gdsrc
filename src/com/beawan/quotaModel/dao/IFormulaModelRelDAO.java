package com.beawan.quotaModel.dao;

import com.beawan.core.BaseDao;
import com.beawan.quotaModel.bean.FormulaModelRel;

public interface IFormulaModelRelDAO extends BaseDao<FormulaModelRel> {

}
