package com.beawan.quotaModel.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.quotaModel.bean.Param;
import com.beawan.quotaModel.service.IParamSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;

@Controller
@RequestMapping({ "/quotaModel" })
@UserSessionAnnotation
public class QuotaModelCtl {
	
	private static final Logger log = Logger.getLogger(QuotaModelCtl.class);
	
	@Resource
	private IParamSV paramSV;
	
	/**
	 * 跳转到公式管理页面 
	 * /quotaModel/quotaFormulaMge.do 
	 **/
	@RequestMapping("/quotaFormulaMge.do")
	public String viewAdminQuota(HttpServletRequest request,HttpServletResponse response) {
		return "views/model/loanLimit/formulaList";
	}
	
	/**
	 * 跳转到新增公式页面
	 * @return
	 */
	@RequestMapping("/addFormulaPage.do")
	public String addQuotaPage() {
		return "views/model/loanLimit/add_formula";
	}
	
	/**
	 * 跳转到参数管理页面 
	 * /quotaModel/quotaParamMge.do 
	 **/
	@RequestMapping("quotaParamMge.do")
	public String menuMge(HttpServletRequest request, HttpServletResponse response){
		return "views/model/loanLimit/paramList";
	}
	
	/**
	 * TODO 根据参数编号获得参数列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getParamChildren.json")
	@ResponseBody
	public String getChildren(HttpServletRequest request, HttpServletResponse response) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<Param> dataGrid = null;
		try {
			String parentNo = request.getParameter("id");
			if(StringUtil.isEmptyString(parentNo))
				parentNo = "0";
			dataGrid = paramSV.queryTreeMenu(parentNo);
		} catch (Exception e) {
			log.error("得到子菜单列表异常", e);
		}
		
		return jRequest.serialize(dataGrid, true);
	}
	
	/**
	 * @Description 保存菜单
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveParam.json")
	@ResponseBody
	public String saveMenu(HttpServletRequest request, HttpServletResponse response) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);
		
		try {
			String jsondata = request.getParameter("jsondata");
			Param param = JacksonUtil.fromJson(jsondata, Param.class);
			paramSV.saveParam(param);
			
			json.put("result", true);
		} catch (Exception e) {
			json.put("msg", "系统异常，请联系管理员！");
			log.error("保存菜单异常", e);
		}
		
		return jRequest.serialize(json, true);
	}
	
}
