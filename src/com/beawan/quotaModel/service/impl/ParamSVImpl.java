package com.beawan.quotaModel.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.base.entity.User;
import com.beawan.quotaModel.bean.Param;
import com.beawan.quotaModel.dao.IParamDAO;
import com.beawan.quotaModel.service.IParamSV;
import com.platform.util.DateUtil;
import com.platform.util.ExceptionUtil;
import com.platform.util.HttpUtil;
import com.platform.util.StringUtil;

@Service("paramSV")
public class ParamSVImpl implements IParamSV{

	@Resource
	private IParamDAO paramDAO;
	
	@Override
	public void saveParam(Param param) throws Exception {
		
		//检查是否重复
		if(StringUtil.isEmptyString(param.getParamNo()) 
				|| StringUtil.isEmptyString(param.getParamName()) ){
			ExceptionUtil.throwException("参数编号或参数名称为空！保存失败。");
		}
		
		List<Param> list = paramDAO.selectByProperty("paramNo", param.getParamNo());
		if(list.size()>0){
			ExceptionUtil.throwException("参数编号重复！保存失败。");
		}
		
		User currentUser = HttpUtil.getCurrentUser();
		param.setCreateTime(DateUtil.getServerTime());
		param.setCreateUserNo(currentUser.getUserId());
		param.setCreateUserName(currentUser.getUserName());
		param.setUpdateTime(DateUtil.getServerTime());
		param.setUpdateUserName(currentUser.getUserName());
		param.setUpdateUserNo(currentUser.getUserId());
		paramDAO.saveOrUpdate(param);
		
		/*String parentNo = param.getParentNo();
		if(!StringUtil.isEmptyString(parentNo)
				&& !"0".equals(parentNo)){
			//Param pParam = paramDAO.queryByNo(parentNo);
			List<Param> pList = paramDAO.selectByProperty("paramNo", parentNo);
			Param pParam = pList.get(0);
			if(pParam != null){
				//pParam.setMenuType("P");
				paramDAO.saveOrUpdate(pParam);
			}
		}*/
	}
	
	@Override
	public List<Param> queryTreeMenu(String parentNo) throws Exception {
		List<Param> childParams = paramDAO.selectByProperty("parentNo", parentNo);
		if(childParams != null && childParams.size() > 0){
			for(Param param: childParams){
				dsfChildTree(param);
			}
		}
		return childParams;
	}
	/**
	 * 递归查询树形menu
	 * @param parentNo
	 */
	private void dsfChildTree(Param parentParam) throws Exception{
		List<Param> childParams = new ArrayList<Param>();
		//childMenus =  menuDAO.queryByParentNo(parentMenu.getMenuNo());
		childParams = paramDAO.selectByProperty("parentNo", parentParam.getParamNo());
		if(childParams == null || childParams.size() <= 0){
			return;
		}
		for(Param param: childParams){
			dsfChildTree(param);
		}
		parentParam.setChildParamList(childParams);
	}
	
}
