package com.beawan.quotaModel.service;

import java.util.List;

import com.beawan.quotaModel.bean.Param;

public interface IParamSV{
	void saveParam(Param param)throws Exception;
	List<Param> queryTreeMenu(String paramNo)throws Exception;
}
