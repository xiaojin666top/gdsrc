package com.beawan.quotaModel.bean;

import java.util.List;

public class Param {
	private long id;
	private String parentNo;//父节点
	private String paramNo;//参数编号
	private String paramName;//参数名称

	private String createTime;
	private String createUserNo;
	private String createUserName;
	
	private String updateTime;
	private String updateUserNo;
	private String updateUserName;
	
	private List<Param> childParamList;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public List<Param> getChildParamList() {
		return childParamList;
	}
	public void setChildParamList(List<Param> childParamList) {
		this.childParamList = childParamList;
	}
	public String getParentNo() {
		return parentNo;
	}
	public void setParentNo(String parentNo) {
		this.parentNo = parentNo;
	}
	public String getParamNo() {
		return paramNo;
	}
	public void setParamNo(String paramNo) {
		this.paramNo = paramNo;
	}
	public String getParamName() {
		return paramName;
	}
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getCreateUserNo() {
		return createUserNo;
	}
	public void setCreateUserNo(String createUserNo) {
		this.createUserNo = createUserNo;
	}
	public String getCreateUserName() {
		return createUserName;
	}
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getUpdateUserNo() {
		return updateUserNo;
	}
	public void setUpdateUserNo(String updateUserNo) {
		this.updateUserNo = updateUserNo;
	}
	public String getUpdateUserName() {
		return updateUserName;
	}
	public void setUpdateUserName(String updateUserName) {
		this.updateUserName = updateUserName;
	}
	
	
}
