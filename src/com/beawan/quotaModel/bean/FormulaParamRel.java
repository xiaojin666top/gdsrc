package com.beawan.quotaModel.bean;

public class FormulaParamRel {
	private long id;
	private String formulaNo;//公式编号
	private String paramNo;//参数编号

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getParamNo() {
		return paramNo;
	}
	public void setParamNo(String paramNo) {
		this.paramNo = paramNo;
	}
	public String getFormulaNo() {
		return formulaNo;
	}
	public void setFormulaNo(String formulaNo) {
		this.formulaNo = formulaNo;
	}
}
