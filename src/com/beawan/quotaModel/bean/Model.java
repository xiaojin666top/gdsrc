package com.beawan.quotaModel.bean;

public class Model {
	private long id;
	private String modelNo;//模型编号
	private String modelDesc;//模型描述
	private String modelName;//模型名称

	private String createTime;
	private String createUserNo;
	private String createUserName;
	
	private String updateTime;
	private String updateUserNo;
	private String updateUserName;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getModelNo() {
		return modelNo;
	}
	public void setModelNo(String modelNo) {
		this.modelNo = modelNo;
	}
	public String getModelDesc() {
		return modelDesc;
	}
	public void setModelDesc(String modelDesc) {
		this.modelDesc = modelDesc;
	}
	public String getModelName() {
		return modelName;
	}
	public void setModelName(String modelName) {
		this.modelName = modelName;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getCreateUserNo() {
		return createUserNo;
	}
	public void setCreateUserNo(String createUserNo) {
		this.createUserNo = createUserNo;
	}
	public String getCreateUserName() {
		return createUserName;
	}
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getUpdateUserNo() {
		return updateUserNo;
	}
	public void setUpdateUserNo(String updateUserNo) {
		this.updateUserNo = updateUserNo;
	}
	public String getUpdateUserName() {
		return updateUserName;
	}
	public void setUpdateUserName(String updateUserName) {
		this.updateUserName = updateUserName;
	}
	
	
}
