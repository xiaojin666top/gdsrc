package com.beawan.quotaModel.bean;

public class FormulaModelRel {
	private long id;
	private String formulaNo;//公式编号
	private String modelNo;//模型编号

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getModelNo() {
		return modelNo;
	}
	public void setModelNo(String modelNo) {
		this.modelNo = modelNo;
	}
	public String getFormulaNo() {
		return formulaNo;
	}
	public void setFormulaNo(String formulaNo) {
		this.formulaNo = formulaNo;
	}
}
