package com.beawan.quotaModel.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.nfunk.jep.JEP;

import com.beawan.common.BusinessException;
import com.platform.util.BeanUtil;
import com.platform.util.StringUtil;

public class ExpressionUtil {

	/* 传入expression直接得到结果 */
	public static Object eval(Map map, Expression expression) throws BusinessException {
		JEP jep = getJEP(map);
		return workOutKey(jep, map, expression.getConditionExpr(), expression.getFormulaExpr(),
				expression.getEvaluate()).get(expression.getEvaluate());
	}

	/* 直接得出结果 */
	public static Object eval(Map map, String expression) throws BusinessException {
		JEP jep = getJEP(map);
		return workOutSingle(jep, expression);
	}

	/* 通过object当数据源，返回的map */
	public static Map workOutListMap(Object object, Expression expression) throws BusinessException {
		Map<String, Object> map = BeanUtil.convertObject2Map(object);
		JEP jep = getJEP(map);
		return workOutKey(jep, map, expression.getConditionExpr(), expression.getFormulaExpr(),
				expression.getEvaluate());
	}

	/* 传入map和表达式可以解析 */
	public static Map workOutListMap(Map map, Expression expression) throws BusinessException {
		JEP jep = getJEP(map);
		return workOutKey(jep, map, expression.getConditionExpr(), expression.getFormulaExpr(),
				expression.getEvaluate());
	}

	/* 直接指定map和表达式和结果 */
	public static Map workOutListMap(Map map, String formulaExpr, String evaluate) throws BusinessException {
		JEP jep = getJEP(map);
		return workOutKey(jep, map, null, formulaExpr, evaluate);
	}

	/*
	 * @description:
	 * 
	 * @author: wzy
	 * 
	 * @param
	 * 
	 * @return
	 * 
	 * @createtime: 2019/12/4
	 */
	public static Map workOutListMap(Map map, String conditionExpr, String formulaExpr, String evaluate)
			throws BusinessException {

		JEP jep = getJEP(map);
		return workOutKey(jep, map, conditionExpr, formulaExpr, evaluate);

	}

	/**
	 * 计算出表达式并填充
	 * 
	 * @param jep
	 * @param map
	 * @param conditionExpr
	 * @param formulaExpr
	 * @param evaluate
	 * @throws BusinessException
	 */
	private static Map workOutKey(JEP jep, Map map, String conditionExpr, String formulaExpr, String evaluate)
			throws BusinessException {
		// 如果没有条件
		if (!StringUtil.isNotEmptyString(conditionExpr)) {
			map.put(evaluate, workOutSingle(jep, formulaExpr));
			// 如果有条件 且条件为true
		} else if (workOutBool(jep, conditionExpr)) {
			map.put(evaluate, workOutSingle(jep, formulaExpr));
		}

		return map;
	}

	/**
	 * 判断条件表达式
	 * 
	 * @param jep
	 * @param expression
	 * @return
	 * @throws BusinessException
	 */
	private static boolean workOutBool(JEP jep, String expression) throws BusinessException {

		return (Double) workOutSingle(jep, expression) > 0;
	}

	/**
	 * 计算表达式的值
	 * 
	 * @param jep
	 * @param expression
	 * @return
	 * @throws BusinessException
	 */
	private static Object workOutSingle(JEP jep, String expression) throws BusinessException {
		Object result = null;
		jep.parseExpression(expression);

		result = jep.getValueAsObject();
		if (result == null) {
			throw new BusinessException("公式表达式解析失败,参数不合法");
		}
		return result;
	}

	private static JEP initJEP() {
		JEP jep = new JEP();

		jep.addStandardFunctions();
		jep.addFunction("max", new MaxFunction());
		jep.addFunction("min", new MinFunction());
		return jep;
	}

	/**
	 * 获取填充好变量的JEP对象
	 * 
	 * @param param
	 * @return
	 */
	private static JEP getJEP(Map param) {

		JEP jep = initJEP();
		Set<Map.Entry> set = param.entrySet();
		for (Map.Entry entry : set) {
			Object entryValue = entry.getValue();
			String entryKey = (String) entry.getKey();
			if (entryValue != null && !"".equals(entryValue)) {
				jep.addVariable(entryKey, entryValue);
			}
		}
		return jep;

	}

	public static void main(String[] args) throws Exception {
		Map<String, Object> map = new HashMap<>();
		map.put("a", 0.045);
		map.put("c", 12.123143214124);

		/*Expression expression = new Expression();
		expression.setConditionExpr(null);
		expression.setFormulaExpr("min(max(a+c,c),a)");
		expression.setEvaluate("b");*/

		//System.out.println(eval(map, "a*(1-0.05)"));
		System.out.println(eval(map, "max(max(a+c,c),a)"));
	}

}
