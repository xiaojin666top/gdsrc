package com.beawan.quotaModel.utils;

public class Expression {
	/*条件*/
	private  String conditionExpr;
	/*计算公式*/
	private String formulaExpr;
	/*计算结果*/
	private String evaluate;

	public Expression() {
	}

	public Expression(String conditionExpr, String formulaExpr, String evaluate) {
		super();
		this.conditionExpr = conditionExpr;
		this.formulaExpr = formulaExpr;
		this.evaluate = evaluate;
	}
	public String getConditionExpr() {
		return conditionExpr;
	}
	public void setConditionExpr(String conditionExpr) {
		this.conditionExpr = conditionExpr;
	}
	public String getFormulaExpr() {
		return formulaExpr;
	}
	public void setFormulaExpr(String formulaExpr) {
		this.formulaExpr = formulaExpr;
	}
	public String getEvaluate() {
		return evaluate;
	}
	public void setEvaluate(String evaluate) {
		this.evaluate = evaluate;
	}

}
