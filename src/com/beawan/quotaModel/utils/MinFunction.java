package com.beawan.quotaModel.utils;

import org.nfunk.jep.ParseException;
import org.nfunk.jep.function.PostfixMathCommandI;

import java.util.Stack;

/**
 * @Author: wzy
 * @CreateTime: 2019-12-05
 * @Description:
 */
public class MinFunction implements PostfixMathCommandI {
    @Override
    public void run(Stack stack) throws ParseException {
        Object param2 = stack.pop();
        Object param1=stack.pop();
        if(Double.parseDouble(param1.toString())<Double.parseDouble(param2.toString())){
            stack.push(param1);
        }else{
            stack.push(param2);
        }
    }

    @Override
    public int getNumberOfParameters() {
        return 2;
    }

    @Override
    public void setCurNumberOfParameters(int i) {

    }

    @Override
    public boolean checkNumberOfParameters(int i) {
        return true;
    }
}

