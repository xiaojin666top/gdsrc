package com.beawan.core;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation= Propagation.REQUIRED,readOnly=false,rollbackFor={Exception.class})
public class BaseServiceImpl<T> implements BaseService<T> {

    /**
     * 注入BaseDao
     */
    private BaseDao<T> dao;
    @Resource
    public void setDao(BaseDao<T> dao) {
        this.dao = dao;
    }

	public BaseDao<T> getDao() {
		return dao;
	}

	@Override
	public T findByPrimaryKey(Object id) {
		return dao.findByPrimaryKey(id);
	}

	@Override
	public List<T> getAll() {
		return dao.getAll();
	}

	@Override
	public T saveOrUpdate(T entity) {
		return dao.saveOrUpdate(entity);
	}

	@Override
	public void deleteEntity(T entity) {
		dao.deleteEntity(entity);
	}

	@Override
	public void refresh(T entity) {
		dao.refresh(entity);
	}

	@Override
	public void flush() {
		dao.flush();
	}

	@Override
	public List<T> select(String query, Object... args) {
		return dao.select(query,args);
	}

	@Override
	public List<T> select(String query, Map<String, Object> params) {
		return dao.select(query, params);
	}

	@Override
	public List<T> selectByNative(String query, Object... args) {
		return dao.selectByNative(query, args);
	}

	@Override
	public List<T> selectByProperty(String property, Object value) {
		Map<String, Object> params =  new HashMap<>();
		params.put(property, value);
		//增加默认状态码 0的过滤   原来的数据表 没有继承BaseEntity   调用该方法会出错
    	return dao.selectByProperty(params);
//		return dao.selectByProperty(property, value);
	}

	@Override
	public List<T> selectByProperty(Map<String, Object> params){
    	return dao.selectByProperty(params);
	}
	
	@Override
	public List<T> selectByProperty(Map<String, Object> params, String orderType){
		return dao.selectByProperty(params, orderType);
	}
	

	@Override
	public List<T> selectByProperty(String property, Object value, String orderType) {
		return dao.selectByProperty(property, value, orderType);
	}
	@Override
	public T selectByPrimaryKey(Object key) {
		return dao.selectByPrimaryKey(key);
	}

	@Override
	public List<T> selectDistinct(String query, Object... args) {
		return dao.selectDistinct(query, args);
	}

	@Override
	public List<T> selectDistinctByNative(String query, Object... args) {
		return dao.selectDistinctByNative(query, args);
	}

	@Override
	public List<T> selectDistinctByProperty(String property, Object value) {
		return dao.selectDistinctByProperty(property, value);
	}

	@Override
	public T selectSingle(String query, Map<String, Object> params) {
		return dao.selectSingle(query, params);
	}

	@Override
	public T selectSingle(String query, Object... args) {
		return dao.selectSingle(query, args);
	}

	@Override
	public T selectSingleByNative(String query, Object... args) {
		return dao.selectSingleByNative(query, args);
	}

	@Override
	public T selectSingleByProperty(String property, Object value) {
		return dao.selectSingleByProperty(property, value);
	}

	@Override
	public List<T> selectRange(String query, int index, int count, Object... args) {
		return dao.selectRange(query, index, count, args);
	}

	@Override
	public List<T> selectRange(String query, int index, int count, Map<String, Object> params) {
		return dao.selectRange(query, index, count, params);
	}

	@Override
	public List<T> selectRangeByNative(String query, int index, int count, Object... args) {
		return dao.selectRangeByNative(query, index, count, args);
	}

	@Override
	public List<T> selectRangeByProperty(String property, Object value, int index, int count) {
		return dao.selectRangeByProperty(property, value, index, count);
	}

	@Override
	public long selectCount(String query, Object... args) {
		return selectCount(query, args);
	}

	@Override
	public long selectCount(String query, Map<String, Object> params) {
		return dao.selectCount(query, params);
	}

	@Override
	public long selectCountByNative(String query, Object... args) {
		return selectCountByNative(query, args);
	}

	@Override
	public long selectCountByProperty(String property, Object value) {
		return selectCountByProperty(property, value);
	}

	@Override
	public boolean isExist(String query, Object... args) {
		return isExist(query, args);
	}

	@Override
	public boolean isExistByNative(String query, Object... args) {
		return isExistByNative(query, args);
	}

	@Override
	public boolean isExistByProperty(String property, Object value) {
		return isExistByProperty(property, value);
	}



	/*********************************************************/
	/***
	 * 以下针对行内下发数据  创建的方法
	 * 因为对公系统的表全部包含creater createtime updater updatetime status
	 * 以上所有接口默认会添加status=0（非删除状态）的查询条件
	 *
	 * 信贷系统下发的数据并不是这样  所以一下方法没有status=0的约束条件
	 * 所有的查询方法名 带 dtmp关键字 以get开头
	 */
	@Override
	public T getDtmpById(Object id) {
		return dao.getDtmpById(id);
	}

	@Override
	public T getDtmpSingleByProperty(String property, Object value) {
		return dao.getDtmpSingleByProperty(property, value);
	}

	@Override
	public List<T> getDtmpByProperty(String property, Object value) {
		return dao.getDtmpByProperty(property, value);
	}

	@Override
	public List<T> getDtmpByProperty(Map<String, Object> params) {
		return dao.getDtmpByProperty(params);
	}

	@Override
	public List<T> getDtmpAll() {
		return dao.getDtmpAll();
	}
}
