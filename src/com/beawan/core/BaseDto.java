package com.beawan.core;

import com.beawan.common.Constants;
import com.platform.util.DateUtil;

import java.sql.Timestamp;

/**
 * @author zxh
 * @date 2020/7/9 10:06
 */
public class BaseDto {

    private String creater;
    private Timestamp createTime;
    private String updater;
    private Timestamp updateTime;
    private Integer status;//0正常   1删除

    public BaseDto() {
        /*this.createTime = DateUtil.getNowTimestamp();*/
        this.status = Integer.parseInt(Constants.STATE_NORMAL);//0正常   1删除
    }

    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public String getUpdater() {
        return updater;
    }

    public void setUpdater(String updater) {
        this.updater = updater;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
