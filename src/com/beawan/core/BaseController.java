package com.beawan.core;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.util.Date;

/** 
 * 
 * <p>Title: BaseController</p>
 * <p>Description: </p>
 * <p>Company: </p>
 * @author    
 * @date   
 */
public class BaseController {

	protected ModelAndView returnLoginView(){
		return new ModelAndView("login");
	}


	protected ResultDto resultForApi(Integer code, String msg) {
		return resultForApi(code, null,msg);
	}


	protected ResultDto resultForApi(Integer code, Object obj,String msg) {
		ResultDto rd = new ResultDto();
		rd.setCode(code);
		rd.setRows(obj);
		rd.setMsg(msg);
		return rd;
	}

	/**
	 * 返回结果成功结果
	 * 
	 * @return
	 */
	protected ResultDto returnSuccess() {
		return resultForApi(ResultDto.RESULT_CODE_SUCCESS,"success");
	}

	protected ResultDto returnFail() {
		return new ResultDto(ResultDto.RESULT_CODE_FAIL);
	}

	protected ResultDto returnFail(String str) {
		return new ResultDto(ResultDto.RESULT_CODE_FAIL, str);
	}

	protected ResultDto returnReLogin(String str) {
		return new ResultDto(ResultDto.RESULT_CODE_RELOGIN, str);
	}

	@InitBinder
	private void dateBinder( WebDataBinder binder ) {
		SimpleDateFormat dateFormat = new SimpleDateFormat( "dd-MM-yyyy" );
		CustomDateEditor editor = new CustomDateEditor( dateFormat, true );
		binder.registerCustomEditor( Date.class, editor );
	}
}
