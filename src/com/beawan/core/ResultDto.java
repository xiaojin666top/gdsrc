package com.beawan.core;

public class ResultDto {

    public static final int RESULT_CODE_SUCCESS = 0;
    public static final int RESULT_CODE_FAIL = 1;
    public static final int RESULT_CODE_ERROR = 2;
    public static final int RESULT_CODE_RELOGIN = 99;
    private Integer code = RESULT_CODE_SUCCESS; //操作信息

    private Object rows;//存放对象

    private Long total=0L;

    String msg="";//存放一些要返回前台的信息
    private long page=1;

    public Integer getCode() {
        return code;
    }

    public void setRows(Object rows) {
        this.rows = rows;
    }

    public Object getRows() {
        return rows;
    }

    public void setCode(Integer code ) {
        this.code = code;
    }


    public ResultDto() {
    }

    public ResultDto(Integer code) {
        this.code = code;
    }

    public ResultDto(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public Long getTotal() {
        return total;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getPage() {
        return page;
    }

    public void setPage(long page) {
        this.page = page;
    }
}
