package com.beawan.core;

import com.beawan.common.Constants;
import com.platform.util.DateUtil;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.sql.Timestamp;
/**
 * 构造方法中对创建时间进行了设置，并对状态设置为正常
 * 
 * @ClassName: BaseEntity
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author xyh
 * @date 16 Jun 2020
 *
 */
@MappedSuperclass
public class BaseEntity {

    @Column(name = "CREATER")
    private String creater;

    @Column(name = "CREATE_TIME")
    private Timestamp createTime;

    @Column(name = "UPDATER")
    private String updater;

    @Column(name = "UPDATE_TIME")
    private Timestamp updateTime;

    @Column(name = "STATUS")
    private Integer status;//0正常   1删除

    public BaseEntity() {
        this.createTime = DateUtil.getNowTimestamp();
        this.status = Integer.parseInt(Constants.STATE_NORMAL);//0正常   1删除
    }


    public String getCreater() {
        return creater;
    }

    public void setCreater(String creater) {
        this.creater = creater;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public String getUpdater() {
        return updater;
    }

    public void setUpdater(String updater) {
        this.updater = updater;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
