package com.beawan.core;

import javax.persistence.Query;

import com.beawan.survey.guarantee.entity.CompFinanceTransfer;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface BaseDao<T> {
  
	/**
	 * 主键查询
	 * @param id
	 * @return
	 */
	public T findByPrimaryKey(Object id);
	
	/**
	 * 查询所有
	 * @return
	 */
	public List<T> getAll();
	/**
	 * 保存或更新
	 * @param entity
	 * @return
	 */
	public T saveOrUpdate(T entity);

	public boolean batchSaveUpdata(List<T> list);

	public boolean batchSaveUpdata(List<T> list, int batchSize);
	
	public void deleteEntity(T entity);
	
	public void refresh(T entity);

	public void flush();
	/**
	 * 查询（占位符采用物理顺序的方式）
	 * @param query
	 * @param args
	 * @return
	 */
	public List<T> select(String query, Object... args);
	/**
	 * 查询（占位符采用参数命名方式）
	 * @param query
	 * @param params
	 * @return
	 */
	public List<T> select(String query, Map<String,Object> params);

	public List<T> selectByNative(String query, Object... args);

	public List<T> selectByProperty(String property, Object value);

	public List<T> selectByProperty(Map<String, Object> params);

	public List<T> selectByProperty(Map<String, Object> params, String orderType);
	
	public List<T> selectByProperty(String property, Object value , String orderType);
	
	public T selectByPrimaryKey(Object key);

	public List<T> selectDistinct(String query, Object... args);

	public List<T> selectDistinctByNative(String query, Object... args);

	public List<T> selectDistinctByProperty(String property, Object value);

	public T selectSingle(String query, Map<String,Object> params);
	
	public T selectSingle(String query, Object... args);

	public T selectSingleByNative(String query, Object... args);

	public T selectSingleByProperty(String property, Object value);
	/**
	 * 分页查询（占位符采用物理顺序的方式）
	 * @param query
	 * @param index
	 * @param count
	 * @param args
	 * @return
	 */
	public List<T> selectRange(String query, int index, int count, Object... args);

	/**
	 * 分页查询（占位符采用参数命名方式）
	 * @param query
	 * @param index
	 * @param count
	 * @return
	 */
	public List<T> selectRange(String query, int index, int count,  Map<String,Object> params);
	
	public List<T> selectRangeByNative(String query, int index, int count, Object... args);

	public List<T> selectRangeByProperty(String property, Object value, int index, int count);

	public long selectCount(String query, Object... args);
	
	public long selectCount(String query, Map<String,Object> params);

	public long selectCountByNative(String query, Object... args);

	public long selectCountByProperty(String property, Object value);

	public <T> long selectMax(Class<T> cls, String query, Map<String, Object> args);

	public boolean isExist(String query, Object... args);

	public boolean isExistByNative(String query, Object... args);

	public boolean isExistByProperty(String property, Object value);
	
	public <T> T findCustOneBySql(Class<T> beanClass, CharSequence queryStr, Object... params);
	
	public <T> List<T> findCustOneListBySql(Class<T> beanClass, CharSequence queryStr, Object... params);
	
	public <T> List<T> findCustListBySql(Class<T> beanClass, CharSequence queryStr, Object... params);
	
	public <T> List<T> findCustListBySql(Class<T> beanClass, CharSequence queryStr, Map<String,Object> params);

	public Integer findCustSingeBySql(CharSequence queryStr, Map<String,Object> params);

	public <T> Pagination<T> findCustSqlPagination(Class beanClass,CharSequence queryString, final CharSequence countString,
            final Map<String, Object> params, int pageIndex, int pageSize);

	/**
	 * 重载  自动组装总数查询语句
	 * @param beanClass
	 * @param queryString
	 * @param params
	 * @param pageIndex
	 * @param pageSize
	 * @param <T>
	 * @return
	 */
	public <T> Pagination<T> findCustSqlPagination(Class beanClass,CharSequence queryString,
												   final Map<String, Object> params, int pageIndex, int pageSize);
	
	public <T> Pagination<T> findSqlPagination(Class beanClass, CharSequence queryString, final CharSequence countString,
            final Map<String, Object> params, int pageIndex, int pageSize);

	/***
	 * 以下针对行内下发数据  创建的方法
	 * 因为对公系统的表全部包含creater createtime updater updatetime status
	 * 以上所有接口默认会添加status=0（非删除状态）的查询条件
	 *
	 * 信贷系统下发的数据并不是这样  所以以下方法没有status=0的约束条件
	 * 所有的查询方法名 带 dtmp关键字 以get开头
	 */

	public T getDtmpById(Object id);

	public T getDtmpSingleByProperty(String property, Object value);

	public List<T> getDtmpByProperty(String property, Object value);

	public List<T> getDtmpByProperty(Map<String, Object> params);

	public List<T> getDtmpAll();

}