package com.beawan.web.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class UserSession {
	protected String loggedUserId;
	protected String loggedUserName;
	protected String loggedUserOrgId;
	protected String loggedUserOrgName;
	protected String taskId;
	protected List<String> powers;
	protected List<String> roles;

	public static void clearSession(HttpServletRequest request) {
		request.getSession().removeAttribute("USERSESSION");
	}

	public static UserSession setUserSession(HttpServletRequest request) {
		UserSession session = new UserSession();
		request.getSession().setAttribute("USERSESSION", session);
		return session;
	}

	public static UserSession getUserSession(HttpServletRequest request) {
		UserSession session = null;
		try {
			session = (UserSession) request.getSession().getAttribute("USERSESSION");
		} catch (Exception ex) {
			session = null;
		}
		return session;
	}

	public String getLoggedUserId() {
		return this.loggedUserId;
	}

	public void setLoggedUserId(String loggedUserId) {
		this.loggedUserId = loggedUserId;
	}

	public String getLoggedUserName() {
		return loggedUserName;
	}

	public void setLoggedUserName(String loggedUserName) {
		this.loggedUserName = loggedUserName;
	}

	public List<String> getPowers() {
		return powers;
	}

	public void setPowers(List<String> powers) {
		this.powers = powers;
	}

	public boolean hasPower(String power) {
		if (this.powers != null) {
			return this.powers.contains(power);
		} else {
			return false;
		}
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

	public boolean hasRole(String role) {
		if (this.roles != null) {
			return this.roles.contains(role);
		} else {
			return false;
		}
	}

	public String getLoggedUserOrgName() {
		return loggedUserOrgName;
	}

	public void setLoggedUserOrgName(String loggedUserOrgName) {
		this.loggedUserOrgName = loggedUserOrgName;
	}

	public String getLoggedUserOrgId() {
		return loggedUserOrgId;
	}

	public void setLoggedUserOrgId(String loggedUserOrgId) {
		this.loggedUserOrgId = loggedUserOrgId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
	
}