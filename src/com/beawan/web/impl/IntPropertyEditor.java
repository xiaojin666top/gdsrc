package com.beawan.web.impl;

import org.springframework.beans.propertyeditors.PropertiesEditor;

public class IntPropertyEditor extends PropertiesEditor {
	@Override
	public String getAsText() {
		Object obj = this.getValue();
		if (obj != null) {
			return String.valueOf((Integer)obj);
		}
		return null;
	}

	@Override
	public void setAsText(String text) {
		try {
			this.setValue(Integer.parseInt(text));
		} catch (Exception e) {
			this.setValue(0);
		}
	}
}