package com.beawan.web.impl;

import org.springframework.beans.propertyeditors.PropertiesEditor;

public class DoublePropertyEditor extends PropertiesEditor {
	@Override
	public String getAsText() {
		Object obj = this.getValue();
		if (obj != null) {
			return String.valueOf((Double) obj);
		}
		return null;
	}

	@Override
	public void setAsText(String text) {
		try {
			this.setValue(Double.parseDouble(text));
		} catch (Exception e) {
			this.setValue(0);
		}
	}
}