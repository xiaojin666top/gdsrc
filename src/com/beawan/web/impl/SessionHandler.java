package com.beawan.web.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
/**
 * 这是什么东西？
 * @ClassName:  SessionHandler   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: xyh
 * @date:   2020年5月20日 下午2:52:33   
 *
 */
public class SessionHandler extends HandlerInterceptorAdapter {

	protected static final Logger LOG = LoggerFactory.getLogger(SessionHandler.class);
	//protected String ignoreUrls = "index.do,install.do,login.do,home.do,wap/,bbs/";
	protected String ignoreUrls = "index.do,install.do,login.do,wap/,bbs/";
	protected boolean ignoreLogin = false;

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		super.afterCompletion(request, response, handler, ex);
	}

	@Override
	public void afterConcurrentHandlingStarted(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		super.afterConcurrentHandlingStarted(request, response, handler);
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		super.postHandle(request, response, handler, modelAndView);
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		UserSession userSession = UserSession.getUserSession(request);
		if (!this.ignoreLogin) {
			if (userSession == null || StringUtils.isBlank(userSession.getLoggedUserId()))
				if (StringUtils.isNotBlank(this.ignoreUrls)) {
					String[] urls = this.ignoreUrls.split(",");
					boolean isContain = false;
					for (String url : urls) {
						if (request.getRequestURI().toLowerCase().contains(url.toLowerCase())) {
							isContain = true;
							break;
						}
					}
					if (!isContain) {
						//response.sendRedirect(request.getServletContext().getContextPath() + "/login.do");
						response.setStatus(302);
						response.setHeader("location", "login.do");
						return false;
					}
				}
		}
		request.setAttribute("userSession", userSession);
		return super.preHandle(request, response, handler);
	}
	
	public String getIgnoreUrls() {
		return ignoreUrls;
	}

	public void setIgnoreUrls(String ignoreUrls) {
		this.ignoreUrls = ignoreUrls;
	}

	public boolean isIgnoreLogin() {
		return ignoreLogin;
	}

	public void setIgnoreLogin(boolean ignoreLogin) {
		this.ignoreLogin = ignoreLogin;
	}
}