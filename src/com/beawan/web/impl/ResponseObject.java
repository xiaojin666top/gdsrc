package com.beawan.web.impl;

import java.io.Serializable;

public class ResponseObject implements Serializable {
	private static final long serialVersionUID = -1708831882944151133L;

	public static String STATUS_UNKNOWN = "unknown";
	public static String STATUS_SUCCESS = "success";
	public static String STATUS_FAILURE = "failure";
	public static String STATUS_NOLOGIN = "nologin";

	protected String status = ResponseObject.STATUS_SUCCESS;

	protected String message = "操作成功";

	protected Object model = null;

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getModel() {
		return this.model;
	}

	public void setModel(Object model) {
		this.model = model;
	}
}