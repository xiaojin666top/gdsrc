package com.beawan.web.impl;

import java.io.IOException;
import java.io.Serializable;

import com.beawan.web.IResponseSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

@SuppressWarnings({ "unchecked" })
public class JsonResponseSerializer<T> implements Serializable, IResponseSerializer<T> {
	private static final long serialVersionUID = -7752891182586911767L;

	protected boolean isSuccess = true;

	protected String message;

	protected T data;

	@JsonProperty(value = "isSuccess")
	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public String serialize() {
		return this.serialize(false);
	}

	public String serialize(boolean dataOnly) {
		JsonObjectMapper objectMapper = new JsonObjectMapper();
		try {
			if (dataOnly) {
				return objectMapper.writeValueAsString(this.data);
			} else {
				return objectMapper.writeValueAsString(this);
			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	public JsonResponseSerializer<T> deserialize(String json) throws JsonParseException, JsonMappingException, IOException {
		JsonObjectMapper objectMapper = new JsonObjectMapper();
		JsonResponseSerializer<T> jsonResponseSerializer = objectMapper.readValue(json, this.getClass());
		this.isSuccess = jsonResponseSerializer.isSuccess;
		this.message = jsonResponseSerializer.message;
		this.data = jsonResponseSerializer.data;
		return jsonResponseSerializer;
	}

	public static <T> JsonResponseSerializer<T> deserialize(Class<T> cls, String json) throws JsonParseException, JsonMappingException, IOException {
		JsonObjectMapper objectMapper = new JsonObjectMapper();
		return (JsonResponseSerializer<T>) objectMapper.readValue(json, cls);
	}
}