package com.beawan.web.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.propertyeditors.PropertiesEditor;

public class DatePropertyEditor extends PropertiesEditor {
	protected static SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	protected static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	@Override
	public String getAsText() {
		Object obj = this.getValue(); 
		if (obj != null) {
			return DatePropertyEditor.timeFormat.format((Date) obj)!=null?DatePropertyEditor.dateFormat.format((Date) obj):DatePropertyEditor.timeFormat.format((Date) obj);
		}
		return null;
	}

	@Override
	public void setAsText(String text) {
		try {
			if (text.length() > 10) {
				this.setValue(DatePropertyEditor.timeFormat.parse(text));
			} else {
				this.setValue(DatePropertyEditor.dateFormat.parse(text));
			}
		} catch (ParseException e) {
			this.setValue(null);
		}
	}
}