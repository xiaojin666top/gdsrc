package com.beawan.web.impl;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.beawan.web.IRequestSerializer;
import com.beawan.web.IResponseSerializer;
import com.platform.util.StringUtil;

public class JsonRequestSerializer implements IRequestSerializer {
	protected static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	protected static DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	protected HttpServletRequest request;
	protected HttpServletResponse response;

	public JsonRequestSerializer(HttpServletRequest request, HttpServletResponse response) {
		this.request = request;
		this.response = response;
	}

	public String getString(String key, String defaultValue) {
		String value = this.request.getParameter(key);
		if (StringUtil.isNotBlank(value)) {
			return value;
		}
		return defaultValue;
	}

	public int getInt(String key, int defaultValue) {
		String value = this.request.getParameter(key);
		if (StringUtil.isNotBlank(value)) {
			return Integer.parseInt(value);
		}
		return defaultValue;
	}

	public long getLong(String key, long defaultValue) {
		String value = this.request.getParameter(key);
		if (StringUtil.isNotBlank(value)) {
			return Long.parseLong(value);
		}
		return defaultValue;
	}

	public double getDouble(String key, double defaultValue) {
		String value = this.request.getParameter(key);
		if (StringUtil.isNotBlank(value)) {
			return Double.parseDouble(value);
		}
		return defaultValue;
	}

	public float getFloat(String key, float defaultValue) {
		String value = this.request.getParameter(key);
		if (StringUtil.isNotBlank(value)) {
			return Float.parseFloat(value);
		}
		return defaultValue;
	}

	public boolean getBoolean(String key, boolean defaultValue) {
		String value = this.request.getParameter(key);
		if (StringUtil.isNotBlank(value)) {
			return Boolean.parseBoolean(value);
		}
		return defaultValue;
	}

	public Date getDate(String key, Date defaultValue) {
		String value = this.request.getParameter(key);
		if (StringUtil.isNotBlank(value)) {
			try {
				if (value.length() > 10) {
					return JsonRequestSerializer.dateTimeFormat.parse(value);
				} else {
					return JsonRequestSerializer.dateFormat.parse(value);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return defaultValue;
	}

	public void response(String json) {
		this.response.setCharacterEncoding("UTF-8");
		this.response.setContentType("text/json;charset=UTF-8");
		try {
			this.response.getOutputStream().print(json);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public <T> IResponseSerializer<T> getResponseSerializer(boolean isSuccess, T data, String message) {
		IResponseSerializer<T> jResponse = new JsonResponseSerializer<T>();
		jResponse.setSuccess(isSuccess);
		jResponse.setData(data);
		jResponse.setMessage(message);
		return jResponse;
	}

	public <T> String serialize(boolean isSuccess, T data, String message, boolean dataOnly) {
		IResponseSerializer<T> jResponse = this.getResponseSerializer(isSuccess, data, message);
		return jResponse.serialize(dataOnly);
	}

	public <T> String serialize(T data, boolean dataOnly) {
		return this.serialize(true, data, null, dataOnly);
	}

	public <T> String serialize(T data) {
		return this.serialize(true, data, null, false);
	}

	public <T> String serialize(boolean isSuccess, String message) {
		return this.serialize(isSuccess, null, message, false);
	}

	public String serialize() {
		return this.serialize(true, null, null, false);
	}
}