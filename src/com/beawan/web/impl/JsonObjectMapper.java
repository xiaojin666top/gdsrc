package com.beawan.web.impl;

import java.text.SimpleDateFormat;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate4.Hibernate4Module;

public class JsonObjectMapper extends ObjectMapper {
	private static final long serialVersionUID = 6364759089689090527L;

	public JsonObjectMapper() {
		this.registerModule(new Hibernate4Module());
		this.setSerializationInclusion(Include.ALWAYS);
		this.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
	}
}