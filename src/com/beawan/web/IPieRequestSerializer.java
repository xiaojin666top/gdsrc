package com.beawan.web;

import java.util.Collection;

@SuppressWarnings({ "rawtypes" })
public interface IPieRequestSerializer extends IRequestSerializer {
	public String getName();
	
	public double getY();
	

	public <T extends Collection> IPieResponseSerializer<T> getResponse(T PieItems);

	public <T extends Collection> String serialize(T PieItem);
}