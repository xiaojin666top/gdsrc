package com.beawan.web;

import java.util.Collection;

@SuppressWarnings({ "rawtypes" })
public interface IPieResponseSerializer<T extends Collection> extends IResponseSerializer<T> {
	
}