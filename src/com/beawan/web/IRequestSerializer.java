package com.beawan.web;

import java.util.Date;

public interface IRequestSerializer {
	public String getString(String key, String defaultValue);

	public int getInt(String key, int defaultValue);

	public long getLong(String key, long defaultValue);

	public double getDouble(String key, double defaultValue);

	public float getFloat(String key, float defaultValue);

	public boolean getBoolean(String key, boolean defaultValue);
	
	public Date getDate(String key, Date defaultValue);

	public void response(String json);

	public <T> IResponseSerializer<T> getResponseSerializer(boolean isSuccess, T data, String message);
	
	public <T> String serialize(T data, boolean dataOnly);
	
	public <T> String serialize(T data);
	
	public <T> String serialize(boolean isSuccess, String message);

	public <T> String serialize(boolean isSuccess, T data, String message, boolean dataOnly);
}