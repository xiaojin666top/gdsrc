package com.beawan.web;

import com.fasterxml.jackson.annotation.JsonProperty;

public interface IResponseSerializer<T> {
	@JsonProperty(value = "isSuccess")
	public boolean isSuccess();

	public void setSuccess(boolean isSuccess);

	public String getMessage();

	public void setMessage(String message);

	public T getData();

	public void setData(T data);

	public String serialize();
	
	public String serialize(boolean dataOnly);
}