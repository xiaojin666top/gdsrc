package com.beawan.dtmp.timer;

import java.util.Date;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.beawan.core.ResultDto;
import com.beawan.dtmp.service.DtmpService;
import com.beawan.loanAfter.service.FcTaskService;
import com.beawan.loanAfter.service.LfTaskService;

/**
 * 创建贷后任务定事情
 */
public class DtmpSyncTimer {
	
	private static final Logger log = Logger.getLogger(DtmpSyncTimer.class);
	

	@Resource
	private DtmpService dtmpService;
	
    /**
     * 同步信贷系统中的用户信息  ----》 主要是所在机构的问题
     */
    public void syncUserInfo() {
		try {
			if(dtmpService.syncUserInfo()){
				log.info("用户基本系统同步成功");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.error("用户基本信息同步失败", e);
		}
	}
    
	
}
