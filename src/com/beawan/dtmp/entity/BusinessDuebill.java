package com.beawan.dtmp.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * 
 * @Author: 俞哲嘉
 * @Date: 2021年1月9日
 * @Description:业务借据信息
 */
@Entity
@Table(name = "BUSINESS_DUEBILL",schema = "CMIS")
public class BusinessDuebill {
	
	@Id
    @Column(name = "SERIALNO")
    private String serialno;
	@Column(name = "CUSTOMERID")
	private String customerid;//客户编号
	@Column(name = "CUSTOMERNAME")
	private String customername;//客户名称
	@Column(name = "BUSINESSSUM")
	private BigDecimal businesssum;//贷款金额
	@Column(name = "BUSINESSRATE")
	private BigDecimal businessrate;//贷款利率
    @Column(name = "PUTOUTDATE")
    private String putoutdate;	//发放日起
    @Column(name = "MATURITY")
    private String maturity;	//到期日期
    

    public String getCustomerid() {
		return customerid;
	}
	public String getSerialno() {
		return serialno;
	}
	public void setSerialno(String serialno) {
		this.serialno = serialno;
	}
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	public String getCustomername() {
		return customername;
	}
	public void setCustomername(String customername) {
		this.customername = customername;
	}
	public BigDecimal getBusinesssum() {
		return businesssum;
	}
	public void setBusinesssum(BigDecimal businesssum) {
		this.businesssum = businesssum;
	}
	public BigDecimal getBusinessrate() {
		return businessrate;
	}
	public void setBusinessrate(BigDecimal businessrate) {
		this.businessrate = businessrate;
	}
	public String getPutoutdate() {
		return putoutdate;
	}
	public void setPutoutdate(String putoutdate) {
		this.putoutdate = putoutdate;
	}
	public String getMaturity() {
		return maturity;
	}
	public void setMaturity(String maturity) {
		this.maturity = maturity;
	}
    
    
	
	
	
	
}
