package com.beawan.dtmp.entity;
// default package

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * cmis:信贷机构表
 */

@Entity
@Table(name = "ORG_INFO",schema = "CMIS")
public class OrgInfo {

    // Fields
    @Id
    @Column(name = "ORGID")
    private String orgid;
    @Column(name = "SORTNO")
    private String sortno;
    @Column(name = "ORGNAME")
    private String orgname;//
    @Column(name = "ORGLEVEL")
    private String orglevel;//
    @Column(name = "ORGPROPERTY")
    private String orgproperty;//
    @Column(name = "RELATIVEORGID")
    private String relativeorgid;//
    @Column(name = "MAINFRAMEORGID")
    private String mainframeorgid;//网点号
    @Column(name = "STATUS")
    private String status;//
    @Column(name = "PRINCIPAL")
    private String principal;//负责人
    @Column(name = "ORGTEL")
    private String orgtel;//联系方式
    @Column(name = "UPDATEUSER")
    private String updateuser;//
    @Column(name = "UPDATETIME")
    private String updatetime;//
    @Column(name = "ISCREDIT")
    private String iscredit;//
    @Column(name = "CORPORATEORGNAME")
    private String corporateorgname;//法人机构名称
    @Column(name = "ORGABBREVI")
    private String orgabbrevi;//
    @Column(name = "FAXNO")
    private String faxno;//邮编
    @Column(name = "ARGADDRESS")
    private String argaddress;//控制字

	/** default constructor */
    public OrgInfo() {
    }

    public String getOrgid() {
        return orgid;
    }

    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }

    public String getSortno() {
        return sortno;
    }

    public void setSortno(String sortno) {
        this.sortno = sortno;
    }

    public String getOrgname() {
        return orgname;
    }

    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    public String getOrglevel() {
        return orglevel;
    }

    public void setOrglevel(String orglevel) {
        this.orglevel = orglevel;
    }

    public String getOrgproperty() {
        return orgproperty;
    }

    public void setOrgproperty(String orgproperty) {
        this.orgproperty = orgproperty;
    }

    public String getRelativeorgid() {
        return relativeorgid;
    }

    public void setRelativeorgid(String relativeorgid) {
        this.relativeorgid = relativeorgid;
    }

    public String getMainframeorgid() {
        return mainframeorgid;
    }

    public void setMainframeorgid(String mainframeorgid) {
        this.mainframeorgid = mainframeorgid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getOrgtel() {
        return orgtel;
    }

    public void setOrgtel(String orgtel) {
        this.orgtel = orgtel;
    }

    public String getUpdateuser() {
        return updateuser;
    }

    public void setUpdateuser(String updateuser) {
        this.updateuser = updateuser;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getIscredit() {
        return iscredit;
    }

    public void setIscredit(String iscredit) {
        this.iscredit = iscredit;
    }

    public String getCorporateorgname() {
        return corporateorgname;
    }

    public void setCorporateorgname(String corporateorgname) {
        this.corporateorgname = corporateorgname;
    }

    public String getOrgabbrevi() {
        return orgabbrevi;
    }

    public void setOrgabbrevi(String orgabbrevi) {
        this.orgabbrevi = orgabbrevi;
    }

    public String getFaxno() {
        return faxno;
    }

    public void setFaxno(String faxno) {
        this.faxno = faxno;
    }

    public String getArgaddress() {
        return argaddress;
    }

    public void setArgaddress(String argaddress) {
        this.argaddress = argaddress;
    }
}