package com.beawan.dtmp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * 
 * @Author: xyh
 * @Date: 2020年9月7日
 * @Description:企业基本信息
 */
@Entity
@Table(name = "ENT_INFO",schema = "CMIS")
public class EntInfo {
	@Id
    @Column(name = "CUSTOMERID")
    private String customerid;
	@Column(name = "INDUSTRYTYPE")
	private String industryType;//行业类型
	
	@Column(name = "FICTITIOUSPERSON")
	private String ficitiousPerson;//法人代表
	
	@Column(name = "SETUPDATE")
	private String setupdate;//成立时间
	@Column(name = "OFFICEADD")
	private String officeadd;//实际经营地址
	@Column(name = "REGISTERCAPITAL")
	private Double registercapital;//注册资本
	@Column(name = "RCCURRENCY")
	private String rccurrency;//注册资本币种
	@Column(name = "MYBANKDORM")
	private String mybankdorm;// 是否本行股东  N Y
	
	@Column(name = "PAICLUPCAPITAL")
	private Double paiclupCapital;//实收资本
	@Column(name = "PCCURRENCY")
	private String pccurrency;//实收资本币种
	
	@Column(name = "FINANCEBELONG")
	private String financebelong;//财报所属
	
	
	public String getCustomerid() {
		return customerid;
	}
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	public String getIndustryType() {
		return industryType;
	}
	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}
	public String getFicitiousPerson() {
		return ficitiousPerson;
	}
	public void setFicitiousPerson(String ficitiousPerson) {
		this.ficitiousPerson = ficitiousPerson;
	}
	public Double getPaiclupCapital() {
		return paiclupCapital;
	}
	public void setPaiclupCapital(Double paiclupCapital) {
		this.paiclupCapital = paiclupCapital;
	}
	public String getPccurrency() {
		return pccurrency;
	}
	public void setPccurrency(String pccurrency) {
		this.pccurrency = pccurrency;
	}
	public String getOfficeadd() {
		return officeadd;
	}
	public void setOfficeadd(String officeadd) {
		this.officeadd = officeadd;
	}
	public Double getRegistercapital() {
		return registercapital;
	}
	public void setRegistercapital(Double registercapital) {
		this.registercapital = registercapital;
	}
	public String getMybankdorm() {
		return mybankdorm;
	}
	public void setMybankdorm(String mybankdorm) {
		this.mybankdorm = mybankdorm;
	}
	public String getRccurrency() {
		return rccurrency;
	}
	public void setRccurrency(String rccurrency) {
		this.rccurrency = rccurrency;
	}
	public String getSetupdate() {
		return setupdate;
	}
	public void setSetupdate(String setupdate) {
		this.setupdate = setupdate;
	}
	public String getFinancebelong() {
		return financebelong;
	}
	public void setFinancebelong(String financebelong) {
		this.financebelong = financebelong;
	}
	
}
