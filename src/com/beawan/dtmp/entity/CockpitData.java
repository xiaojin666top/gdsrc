package com.beawan.dtmp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;

/**
 * 数据驾驶舱（公司）界面数据
 * @author User
 *
 */
@Entity
@Table(name = "COCKPIT_DATA", schema = "GDTCESYS")
public class CockpitData extends BaseEntity{

	@Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COCKPIT_DATA")
   // @SequenceGenerator(name = "COCKPIT_DATA", allocationSize = 1, initialValue = 1, sequenceName = "COCKPIT_DATA_SEQ")
    private Integer id;
	@Column(name = "LOAN_SUM")
	private String loanSum; // 对公贷款总额
	@Column(name = "DEPOSIT_SUM")
	private String depositSum; // 对公存款总额
	@Column(name = "DEPOSIT_BY_TYPE")
	private String depositByType; // 按存款结构统计存款(对公、储蓄、保证金)
	@Column(name = "DEPOSIT_BY_ORG")
	private String depositByOrg; // 按各支行统计对公存款
	@Column(name = "DEPOSIT_BY_MONTH")
	private String depositByMonth; // 按月份统计对公存款(近5个月)
	@Column(name = "LOAN_BY_CUST_NUM")
	private String loanByCustNum; // 按户数统计对公贷款
	@Column(name = "LOAN_BY_GUA_TYPE")
	private String loanByGuaType; // 按担保方式统计对公贷款
	@Column(name = "LOAN_BY_CLASSIFY")
	private String loanByClassify; // 按五级分类统计对公贷款
	@Column(name = "LOAN_BY_CREDIT")
	private String loanByCredit; // 按授信统计对公贷款
	@Column(name = "LOAN_ROTE")
	private String loanRote; // 对公贷款执行利率
	@Column(name = "SYCN_DATE")
	private String sycnDate; // 数据同步时间:yyyyMMdd
	
	public String getLoanSum() {
		return loanSum;
	}
	public void setLoanSum(String loanSum) {
		this.loanSum = loanSum;
	}
	public String getDepositSum() {
		return depositSum;
	}
	public void setDepositSum(String depositSum) {
		this.depositSum = depositSum;
	}
	public String getDepositByType() {
		return depositByType;
	}
	public void setDepositByType(String depositByType) {
		this.depositByType = depositByType;
	}
	public String getDepositByOrg() {
		return depositByOrg;
	}
	public void setDepositByOrg(String depositByOrg) {
		this.depositByOrg = depositByOrg;
	}
	public String getDepositByMonth() {
		return depositByMonth;
	}
	public void setDepositByMonth(String depositByMonth) {
		this.depositByMonth = depositByMonth;
	}
	public String getLoanByCustNum() {
		return loanByCustNum;
	}
	public void setLoanByCustNum(String loanByCustNum) {
		this.loanByCustNum = loanByCustNum;
	}
	public String getLoanByGuaType() {
		return loanByGuaType;
	}
	public void setLoanByGuaType(String loanByGuaType) {
		this.loanByGuaType = loanByGuaType;
	}
	public String getLoanByClassify() {
		return loanByClassify;
	}
	public void setLoanByClassify(String loanByClassify) {
		this.loanByClassify = loanByClassify;
	}
	public String getLoanByCredit() {
		return loanByCredit;
	}
	public void setLoanByCredit(String loanByCredit) {
		this.loanByCredit = loanByCredit;
	}
	public String getLoanRote() {
		return loanRote;
	}
	public void setLoanRote(String loanRote) {
		this.loanRote = loanRote;
	}
	public String getSycnDate() {
		return sycnDate;
	}
	public void setSycnDate(String sycnDate) {
		this.sycnDate = sycnDate;
	}
}
