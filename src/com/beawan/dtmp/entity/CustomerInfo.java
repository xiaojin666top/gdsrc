package com.beawan.dtmp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Author: xyh
 * @Date: 19/08/2020
 * @Description: cmis中的Customer_Info
 *               全量客户，包括个人客户，公司类客户
 *               字段只取需要的，其他的都忽略
 */
@Entity
@Table(name = "CUSTOMER_INFO",schema = "CMIS")
public class CustomerInfo {
    @Id
    @Column(name = "CUSTOMERID")
    private String customerid;

    @Column(name = "CUSTOMERNAME")
    private String customername;//客户名称

    @Column(name = "CUSTOMERTYPE")
    private String customertype;//客户类型

    @Column(name = "CERTTYPE")
    private String certtype;//证件类型

    @Column(name = "CERTID")
    private String certid;//证件号

    @Column(name = "INPUTORGID")
    private String inputorgid;//登记机构

    @Column(name = "INPUTUSERID")
    private String inputuserid;//登记人

    @Column(name = "INPUTDATE")
    private String inputdate;//登记日期

    @Column(name = "MFCUSTOMERID")
    private String mfcustomerid;//核心客户号

    @Column(name = "STATUS")
    private String status;//状态

    @Column(name = "MANAGERUSERID")
    private String manageruserid;//管户人

    @Column(name = "MANAGERORGID")
    private String managerorgid;//管户机构ID

    @Column(name = "BLACKSHEETORNOT")
    private String blacksheetornot;//是否黑名客户

    @Column(name = "CONFIRMORNOT")
    private String confirmornot;//是否生效

    @Column(name = "CLIENTCLASSN")
    private String clientclassn;//当前客户分类

    @Column(name = "CLIENTCLASSM")
    private String clientclassm;//客户分类调整

    @Column(name = "BUSINESSSTATE")
    private String businessstate;//存量字段标志

    @Column(name = "UPDATEDATE")
    private String updatedate;//更新日期

    
    
    public CustomerInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CustomerInfo(String customername, String updatedate) {
		super();
		this.customername = customername;
		this.updatedate = updatedate;
	}

	public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getCustomertype() {
        return customertype;
    }

    public void setCustomertype(String customertype) {
        this.customertype = customertype;
    }

    public String getCerttype() {
        return certtype;
    }

    public void setCerttype(String certtype) {
        this.certtype = certtype;
    }

    public String getCertid() {
        return certid;
    }

    public void setCertid(String certid) {
        this.certid = certid;
    }

    public String getInputorgid() {
        return inputorgid;
    }

    public void setInputorgid(String inputorgid) {
        this.inputorgid = inputorgid;
    }

    public String getInputuserid() {
        return inputuserid;
    }

    public void setInputuserid(String inputuserid) {
        this.inputuserid = inputuserid;
    }

    public String getInputdate() {
        return inputdate;
    }

    public void setInputdate(String inputdate) {
        this.inputdate = inputdate;
    }

    public String getMfcustomerid() {
        return mfcustomerid;
    }

    public void setMfcustomerid(String mfcustomerid) {
        this.mfcustomerid = mfcustomerid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getManageruserid() {
        return manageruserid;
    }

    public void setManageruserid(String manageruserid) {
        this.manageruserid = manageruserid;
    }

    public String getManagerorgid() {
        return managerorgid;
    }

    public void setManagerorgid(String managerorgid) {
        this.managerorgid = managerorgid;
    }

    public String getBlacksheetornot() {
        return blacksheetornot;
    }

    public void setBlacksheetornot(String blacksheetornot) {
        this.blacksheetornot = blacksheetornot;
    }

    public String getConfirmornot() {
        return confirmornot;
    }

    public void setConfirmornot(String confirmornot) {
        this.confirmornot = confirmornot;
    }

    public String getClientclassn() {
        return clientclassn;
    }

    public void setClientclassn(String clientclassn) {
        this.clientclassn = clientclassn;
    }

    public String getClientclassm() {
        return clientclassm;
    }

    public void setClientclassm(String clientclassm) {
        this.clientclassm = clientclassm;
    }

    public String getBusinessstate() {
        return businessstate;
    }

    public void setBusinessstate(String businessstate) {
        this.businessstate = businessstate;
    }

    public String getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(String updatedate) {
        this.updatedate = updatedate;
    }
}
