package com.beawan.dtmp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Author: xyh
 * @Date: 21/10/2020
 * @Description: CBOD（核心系统） 定期存款主档
 */
@Entity
@Table(name = "TDACNACN",schema = "CBOD")
public class Tdacnacn {
	
	@Id
    @Column(name = "TD_TD_ACCT_NO")
    private String tdAcctNo;// 定存帐号
    @Column(name = "TD_CUST_NO")
    private String tdCustNo; // 客户编号
    @Column(name = "TD_CUST_NAME")
    private Double tdCustName;// 客户名称
    @Column(name = "TD_ACTU_AMT")
    private Double tdActuAmt;// 实际金额
    @Column(name = "TD_CERT_TYP")
    private String tdCertTyp; // 证件种类
    @Column(name = "TD_CERT_ID")
    private Double tdCertId;// 证件号码
    
	public String getTdAcctNo() {
		return tdAcctNo;
	}
	public void setTdAcctNo(String tdAcctNo) {
		this.tdAcctNo = tdAcctNo;
	}
	public String getTdCustNo() {
		return tdCustNo;
	}
	public void setTdCustNo(String tdCustNo) {
		this.tdCustNo = tdCustNo;
	}
	public Double getTdCustName() {
		return tdCustName;
	}
	public void setTdCustName(Double tdCustName) {
		this.tdCustName = tdCustName;
	}
	public Double getTdActuAmt() {
		return tdActuAmt;
	}
	public void setTdActuAmt(Double tdActuAmt) {
		this.tdActuAmt = tdActuAmt;
	}
	public String getTdCertTyp() {
		return tdCertTyp;
	}
	public void setTdCertTyp(String tdCertTyp) {
		this.tdCertTyp = tdCertTyp;
	}
	public Double getTdCertId() {
		return tdCertId;
	}
	public void setTdCertId(Double tdCertId) {
		this.tdCertId = tdCertId;
	}
}
