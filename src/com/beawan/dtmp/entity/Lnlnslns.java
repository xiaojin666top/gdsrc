package com.beawan.dtmp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Author: xyh
 * @Date: 19/08/2020
 * @Description: CBOD（核心系统）中的放款主档
 */
@Entity
@Table(name = "LNLNSLNS",schema = "CBOD")
public class Lnlnslns {
    @Id
    @Column(name = "LN_LN_ACCT_NO")
    private String lnLnAcctNo;
    @Column(name = "LN_CUST_NO")
    private String lnCustNo;
    @Column(name = "LN_TOTL_LN_AMT_HYPO_AMT")
    private Double lnTotlLnAmtHypoAmt;//贷款总金额
    @Column(name = "LN_LN_BAL")
    private Double lnLnBal;//贷款余额
    @Column(name = "LN_FRST_ALFD_DT_N")
    private String lnFrstAlfdDtN;//放款日期
    @Column(name = "LN_DUE_DT_N")
    private String lnDueDtN;//到期日期
    @Column(name = "LN_LN_MTHS_N")
    private Integer lnLnMthsN;//贷款期限
    @Column(name = "LN_APCL_FLG")
    private String lnApclFlg;//呆账核销标记

    public String getLnLnAcctNo() {
        return lnLnAcctNo;
    }

    public void setLnLnAcctNo(String lnLnAcctNo) {
        this.lnLnAcctNo = lnLnAcctNo;
    }

    public String getLnCustNo() {
        return lnCustNo;
    }

    public void setLnCustNo(String lnCustNo) {
        this.lnCustNo = lnCustNo;
    }

    public Double getLnTotlLnAmtHypoAmt() {
        return lnTotlLnAmtHypoAmt;
    }

    public void setLnTotlLnAmtHypoAmt(Double lnTotlLnAmtHypoAmt) {
        this.lnTotlLnAmtHypoAmt = lnTotlLnAmtHypoAmt;
    }

    public Double getLnLnBal() {
        return lnLnBal;
    }

    public void setLnLnBal(Double lnLnBal) {
        this.lnLnBal = lnLnBal;
    }

    public String getLnFrstAlfdDtN() {
        return lnFrstAlfdDtN;
    }

    public void setLnFrstAlfdDtN(String lnFrstAlfdDtN) {
        this.lnFrstAlfdDtN = lnFrstAlfdDtN;
    }

    public String getLnDueDtN() {
        return lnDueDtN;
    }

    public void setLnDueDtN(String lnDueDtN) {
        this.lnDueDtN = lnDueDtN;
    }

    public Integer getLnLnMthsN() {
        return lnLnMthsN;
    }

    public void setLnLnMthsN(Integer lnLnMthsN) {
        this.lnLnMthsN = lnLnMthsN;
    }

    public String getLnApclFlg() {
        return lnApclFlg;
    }

    public void setLnApclFlg(String lnApclFlg) {
        this.lnApclFlg = lnApclFlg;
    }
}
