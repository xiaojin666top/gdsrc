package com.beawan.dtmp.entity;
// default package

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * cims:信贷用户表
 */

@Entity
@Table(name = "USER_INFO",schema = "CMIS")
public class UserInfo {

    // Fields
    @Id
    @Column(name = "USERID")
    private String userid;
    @Column(name = "LOGINID")
    private String loginid;

    @Column(name = "USERNAME")
    private String username;
    @Column(name = "BELONGORG")
    private String belongorg;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "CERTTYPE")
    private String certtype;
    @Column(name = "CERTID")
    private String certid;
    @Column(name = "MOBILETEL")
    private String mobiletel;

    @Column(name = "INPUTORG")
    private String inputorg;
    @Column(name = "INPUTUSER")
    private String inputuser;
    @Column(name = "INPUTTIME")
    private String inputtime;

    @Column(name = "UPDATEUSER")
    private String updateuser;
    @Column(name = "UPDATETIME")
    private String updatetime;

    public UserInfo() {
    }


    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getLoginid() {
        return loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBelongorg() {
        return belongorg;
    }

    public void setBelongorg(String belongorg) {
        this.belongorg = belongorg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCerttype() {
        return certtype;
    }

    public void setCerttype(String certtype) {
        this.certtype = certtype;
    }

    public String getCertid() {
        return certid;
    }

    public void setCertid(String certid) {
        this.certid = certid;
    }

    public String getMobiletel() {
        return mobiletel;
    }

    public void setMobiletel(String mobiletel) {
        this.mobiletel = mobiletel;
    }

    public String getUpdateuser() {
        return updateuser;
    }

    public void setUpdateuser(String updateuser) {
        this.updateuser = updateuser;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getInputorg() {
        return inputorg;
    }

    public void setInputorg(String inputorg) {
        this.inputorg = inputorg;
    }

    public String getInputuser() {
        return inputuser;
    }

    public void setInputuser(String inputuser) {
        this.inputuser = inputuser;
    }

    public String getInputtime() {
        return inputtime;
    }

    public void setInputtime(String inputtime) {
        this.inputtime = inputtime;
    }
}