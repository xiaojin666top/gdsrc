package com.beawan.dtmp.entity;

/**
 * 
 * @Author: 俞哲嘉
 * @Date: 2020年9月7日
 * @Description:ods下的字典表
 */
public class FComCodeValue{
	
    private String codeNo;//字典分组
	private String codeVal;//字典项
	private String codeValueRefe;//中文说明

	public String getCodeNo() {
		return codeNo;
	}

	public void setCodeNo(String codeNo) {
		this.codeNo = codeNo;
	}

	public String getCodeVal() {
		return codeVal;
	}

	public void setCodeVal(String codeVal) {
		this.codeVal = codeVal;
	}

	public String getCodeValueRefe() {
		return codeValueRefe;
	}

	public void setCodeValueRefe(String codeValueRefe) {
		this.codeValueRefe = codeValueRefe;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codeNo == null) ? 0 : codeNo.hashCode());
		result = prime * result + ((codeVal == null) ? 0 : codeVal.hashCode());
		result = prime * result + ((codeValueRefe == null) ? 0 : codeValueRefe.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FComCodeValue other = (FComCodeValue) obj;
		if (codeNo == null) {
			if (other.codeNo != null)
				return false;
		} else if (!codeNo.equals(other.codeNo))
			return false;
		if (codeVal == null) {
			if (other.codeVal != null)
				return false;
		} else if (!codeVal.equals(other.codeVal))
			return false;
		if (codeValueRefe == null) {
			if (other.codeValueRefe != null)
				return false;
		} else if (!codeValueRefe.equals(other.codeValueRefe))
			return false;
		return true;
	}

	
	
	
}
