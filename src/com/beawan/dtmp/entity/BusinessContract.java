package com.beawan.dtmp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * 
 * @Author: 俞哲嘉
 * @Date: 2020年9月7日
 * @Description:业务合同信息
 */
@Entity
@Table(name = "BUSINESS_CONTRACT",schema = "CMIS")
public class BusinessContract {
	@Id
    @Column(name = "SERIALNO")
    private String serialno;
	@Column(name = "CUSTOMERID")
	private String customerid;//客户编号
	
	@Column(name = "CUSTOMERNAME")
	private String customername;//客户名称
	

	@Column(name = "OCCURDATE")
	private String occurdate;//发生日期
	
	@Column(name = "OCCURTYPE")
	private String occurtype;//发生类型   
	@Column(name = "BUSINESSSUM")
	private Double businesssum;//贷款金额
	
    @Column(name = "PAYCYC")
    private String paycyc;//还款方式
    @Column(name = "PUTOUTDATE")
    private String putoutdate;	//约定发放日
    @Column(name = "MATURITY")
    private String maturity;	//到期日期
    @Column(name = "PAYDEADLINE")
    private String paydeadline;	//合同签订到期截止日期
    
    
	public String getSerialno() {
		return serialno;
	}
	public void setSerialno(String serialno) {
		this.serialno = serialno;
	}
	public String getCustomerid() {
		return customerid;
	}
	public void setCustomerid(String customerid) {
		this.customerid = customerid;
	}
	public String getCustomername() {
		return customername;
	}
	public void setCustomername(String customername) {
		this.customername = customername;
	}
	
	public String getOccurdate() {
		return occurdate;
	}
	public void setOccurdate(String occurdate) {
		this.occurdate = occurdate;
	}
	public String getOccurtype() {
		return occurtype;
	}
	public void setOccurtype(String occurtype) {
		this.occurtype = occurtype;
	}
	public Double getBusinesssum() {
		return businesssum;
	}
	public void setBusinesssum(Double businesssum) {
		this.businesssum = businesssum;
	}
	public String getPaycyc() {
		return paycyc;
	}
	public void setPaycyc(String paycyc) {
		this.paycyc = paycyc;
	}
	public String getPutoutdate() {
		return putoutdate;
	}
	public void setPutoutdate(String putoutdate) {
		this.putoutdate = putoutdate;
	}
	public String getMaturity() {
		return maturity;
	}
	public void setMaturity(String maturity) {
		this.maturity = maturity;
	}
	public String getPaydeadline() {
		return paydeadline;
	}
	public void setPaydeadline(String paydeadline) {
		this.paydeadline = paydeadline;
	}
	
	
	
	
}
