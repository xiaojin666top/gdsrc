package com.beawan.dtmp.controller;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.customer.service.ICusFSRecordSV;
import com.beawan.dtmp.dto.StyBalDto;
import com.beawan.dtmp.dto.BalByRateDto;
import com.beawan.dtmp.dto.CustSpreadByBalDto;
import com.beawan.dtmp.dto.DepositByDsDto;
import com.beawan.dtmp.dto.DepositByOrgDto;
import com.beawan.dtmp.service.CockpitDataService;
import com.beawan.dtmp.service.DtmpService;
import com.beawan.loanAfter.service.LfCustInfoService;
import com.beawan.loanAfter.service.LfTaskService;


/**
 * 报表统计下的---》数据驾驶舱
 * 对行内系统数据进行统计
 * 包括cmis和CBOD 等 
 * 控制器
 */
@Controller
@RequestMapping({ "/store" })
@UserSessionAnnotation
public class DataStoreCtl extends BaseController{
	
	private static final Logger log = Logger.getLogger(DataStoreCtl.class);


	@Resource
	private DtmpService dtmpService;
	@Resource
	private LfTaskService lfTaskService;
	@Resource
	private ICusBaseSV cusBaseSV;
	@Resource
	private ICusFSRecordSV cusFSRecordSV;
	@Resource
	private LfCustInfoService lfCustInfoService;
	@Resource
	private CockpitDataService cockpitDataService;

	
	/**
	 * A大类  存款情况
	 * 存款总额  
	 */
	@RequestMapping("getDeposit.json")
	@ResponseBody
	public ResultDto getDeposit() {
		ResultDto re = returnFail("获取存款总额失败");
		try {
			Double deposit = cockpitDataService.getPublicDeposit();
			re.setRows(deposit);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("获取数据成功");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * A大类  存款情况
	 * 分支行 定期、活期存款总额  
	 */
	@RequestMapping("getDepositByOrg.json")
	@ResponseBody
	public ResultDto getDepositByOrg() {
		ResultDto re = returnFail("获取存款总额失败");
		try {
			List<DepositByOrgDto> depositByOrg = cockpitDataService.getDepositByOrg();
			re.setRows(depositByOrg);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("获取数据成功");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * A大类  存款情况
	 * 分存款结构：对公、储蓄、保证金 存款总额  
	 */
	@RequestMapping("getDepositByType.json")
	@ResponseBody
	public ResultDto getDepositByType() {
		ResultDto re = returnFail("获取存款总额失败!");
		try {
			List<StyBalDto> result = cockpitDataService.getDepositByType();
			re.setRows(result);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("获取数据成功");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * A大类  存款情况
	 * 分定期、活期 统计对公存款 近几个月变化情况 
	 */
	@RequestMapping("getDepositByMonth.json")
	@ResponseBody
	public ResultDto getDepositByMonth() {
		ResultDto re = returnFail("获取近几个月对公存款数据失败!");
		try {
			List<DepositByDsDto> result = cockpitDataService.getDepositByMonth();
			re.setRows(result);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("获取数据成功");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * B大类  贷款情况
	 * 获取 分金额的统计贷款户数分布
	 * @return
	 */
	@RequestMapping("getCustNumSpreadByBalance.json")
	@ResponseBody
	public ResultDto getCustNumSpreadByBalance() {
		ResultDto re = returnFail("统计分金额的贷款户数分布失败");
		try {
			CustSpreadByBalDto rows = cockpitDataService.getCustSpreadByBal();
			re.setRows(rows);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("获取成功");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
		return re;
	}
	

	/***
	 * 获取贷款总额
	 * @return
	 */
	@RequestMapping("getLoanBal.json")
	@ResponseBody
	public ResultDto getLoanBal() {
		ResultDto re = returnFail("统计分金额的贷款户数分布失败");
		try {
			Double loanBal = cockpitDataService.getLoanBal();
			re.setRows(loanBal);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("获取贷款总金额成功");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 统计按担保类型的贷款情况
	 * @return
	 */
	@RequestMapping("getBalStry.json")
	@ResponseBody
	public ResultDto getBalStry() {
		ResultDto re = returnFail("统计按担保类型的贷款情况失败");
		try {
			List<StyBalDto> balStry = cockpitDataService.getBalStry();
			re.setRows(balStry);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("获取数据成功");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
		return re;
	}
	

	/**
	 * 贷款五级分类情况
	 * @return
	 */
	@RequestMapping("getBalByClassifyResult.json")
	@ResponseBody
	public ResultDto getBalByClassifyResult() {
		ResultDto re = returnFail("统计五级分类情况失败");
		try {
			List<StyBalDto> classifyResult = cockpitDataService.getBalByClassifyResult();
			re.setRows(classifyResult);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("获取数据成功");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 按贷款利率区间 统计各区间的贷款总额
	 * @return
	 */
	@RequestMapping("getBalByRate.json")
	@ResponseBody
	public ResultDto getBalByRate() {
		ResultDto re = returnFail("统计按利率的贷款情况失败");
		try {
			BalByRateDto rate = cockpitDataService.getBalByRate();
			re.setRows(rate);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("获取数据成功");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
		return re;
	}
	
	@RequestMapping("aaa.json")
	@ResponseBody
	public ResultDto aaa() {
		ResultDto re = returnFail("统计分金额的贷款户数分布失败");
		try {
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
		return re;
	}
}
