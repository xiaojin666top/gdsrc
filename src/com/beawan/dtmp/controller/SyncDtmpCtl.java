package com.beawan.dtmp.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.common.Constants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.customer.service.ICusFSRecordSV;
import com.beawan.dtmp.service.DtmpService;
import com.beawan.loanAfter.service.LfCustInfoService;
import com.beawan.loanAfter.service.LfTaskService;
import com.beawan.survey.custInfo.entity.CompJudgment;
import com.beawan.survey.custInfo.service.CompJudgmentService;
import com.platform.util.DateUtil;
import com.platform.util.JdbcUtil;


/**
 * 同步行内系统数据到对公系统
 * 包括cmis和CBOD
 * 控制器
 */
@Controller
@RequestMapping({ "/dtmp" })
@UserSessionAnnotation
public class SyncDtmpCtl extends BaseController{
	
	private static final Logger log = Logger.getLogger(SyncDtmpCtl.class);


	@Resource
	private DtmpService dtmpService;
	@Resource
	private LfTaskService lfTaskService;
	@Resource
	private ICusBaseSV cusBaseSV;
	@Resource
	private ICusFSRecordSV cusFSRecordSV;
	@Resource
	private LfCustInfoService lfCustInfoService;
	@Resource
	private CompJudgmentService compJudgmentService;

	/**
	 * 同步cmis中的机构数据
	 * @return
	 */
	@RequestMapping("syncOrg.do")
	@ResponseBody
	public ResultDto syncOrg() {
		ResultDto re = returnFail("同步机构数据失败");
		try {
			if(dtmpService.syncOrgInfo()){
				re = returnSuccess();
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 同步cmis中的用户数据
	 * @return
	 */
	@RequestMapping("syncUserInfo.do")
	@ResponseBody
	public ResultDto syncUserInfo() {
		ResultDto re = returnFail("同步用户数据失败");
		try {
			if(dtmpService.syncUserInfo()){
				re = returnSuccess();
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 同步企业数据
	 * @return
	 */
	@RequestMapping("syncCusBase.do")
	@ResponseBody
	public ResultDto syncCusBase(){
		ResultDto re = returnFail("同步企业客户数据失败");
		try{
			dtmpService.syncCustBase();
//			dtmpService.syncLfCustInfo();
			re = returnSuccess();
			re.setMsg("同步数据成功");
		}catch (Exception e){
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
		return re;
	}
	
	
	/**
	 * 同步企业财报
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("syncCusFSRecord.json")
	@ResponseBody
	public Map<String, Object> syncCusFSRecord(HttpServletRequest request,
			HttpServletResponse response) {
		
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("success", false);
		
		try {
			
			long timeStart = new Date().getTime();
			
			List<CusBase> cusList = cusBaseSV.queryAll();
			System.out.println("客户总数量为:" +cusList.size());
			for(int i = 6749; i < cusList.size(); i++){
				CusBase cusBase = cusList.get(i);
				System.out.println("开始同步第:" +i +"---客户名称为："+cusBase.getCustomerName());
				cusFSRecordSV.syncFSFromCmisByCusNoWhole(cusBase.getCustomerNo(), cusBase.getXdCustomerNo());
			}
			
			long timeEnd = new Date().getTime();
			
			json.put("seconds", (timeEnd-timeStart)/1000);
			json.put("success", true);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return json;
	}

	

	@RequestMapping("syncLfCustInfo.do")
	@ResponseBody
	public ResultDto syncLfCustInfo(){
		ResultDto re = returnFail("同步贷后客户数据失败");
		try{
			//dtmpService.syncLfCustInfo();
			dtmpService.updateLfCustInfo();
			re = returnSuccess();
		}catch (Exception e){
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
		return re;
	}

	@RequestMapping("syncLfTask.do")
	@ResponseBody
	public ResultDto syncLfTask(Integer day){
		ResultDto re = returnFail("同步客户信息数据");
		
		
		try {
			
			
//			log.info(DateUtil.getNowYmdStr()+"------------客户数据及贷后任务生成跑批任务启动");
//			
//			log.info("开始同步客户数据");
			 dtmpService.syncCustBase();
//			 dtmpService.syncCustBaseByType("CM20201016102140");
//			log.info("同步客户数据成功");
////
//			log.info("开始同步贷后客户数据");
//			dtmpService.syncLfCustInfo();
//			log.info("同步贷后客户数据成功");
//			
//			log.info("开始更新贷后客户数据");
//			dtmpService.updateLfCustInfo();
//			log.info("更新代后客户数据成功");
//			
			//首检任务
//			lfTaskService.syncFifteenDayTask(-5);
//			log.info("创建15日首检任务成功");
//			
//			//日常任务
//			lfTaskService.syncGHDayTask();
//			log.info("创建管护日常任务成功");
			
			//2个月到期  每个月25日生成 2个月后到期在任务
//			lfTaskService.syncFXThreeMonthTask2(null);
//			lfTaskService.syncFXWarningTask(null);
			/*
			
			
			//3个月到期
			lfTaskService.syncFXThreeMonthTask(3);
			log.info("创建三个月到期任务成功");
			
			//日常任务
			lfTaskService.syncGHDayTask();
			log.info("创建管护日常任务成功");
			//风险预警任务
			lfTaskService.syncFXWarningTask(null);
			log.info("创建风险预警任务成功");*/
			
		} catch (Exception e) {
			log.error("创建贷后任务失败！异常原因：", e);
			e.printStackTrace();
		}
		
		
		
		
//		try{
			/*log.info("开始同步客户数据");
			dtmpService.syncCustBase();
			log.info("同步客户数据成功");
			
			log.info("开始同步贷后客户数据");
			dtmpService.syncLfCustInfo();
			log.info("同步贷后客户数据成功");
			
			log.info("开始更新贷后客户数据");
//			LfCustInfo lfCustInfo = lfCustInfoService.selectSingleByProperty("customerNo", "11111111111111");
//			dtmpService.updateOneLfCust(lfCustInfo);
			dtmpService.updateLfCustInfo();
			log.info("更新代后客户数据成功");
//			lfTaskService.createOneTask(-90,lfCustInfo);
			//3个月到期
			lfTaskService.syncFXThreeMonthTask(3);
			log.info("创建三个月到期预警任务成功");
			//首检任务
			lfTaskService.syncFifteenDayTask(5);
			log.info("创建首检任务成功");*/
			//日常任务
//			lfTaskService.syncGHDayTask();
			//测试特殊的一笔任务
//			lfTaskService.syncGHDayTask2();
			
//			log.info("创建日常任务成功");
			//风险预警任务
//			lfTaskService.syncFXWarningTask(null);
//			log.info("创建风险预警任务成功");
			
//			lfCustInfoService.syncGHFifteenDayTask(-5);
//			lfCustInfoService.syncInitLfTask();
//			
//			re = returnSuccess();
//		}catch (Exception e){
//			log.error(e.getMessage(), e);
//			e.printStackTrace();
//		}
		return re;
	}
	
	
	@RequestMapping("testProcedures.do")
	@ResponseBody
	public ResultDto testProcedures(){
		ResultDto re = returnFail("测试存储过程失败");
		try{
			

			List<CompJudgment> judgList = compJudgmentService.selectByProperty("customerNo", "CM20201016166134");
			//调用存储过程示例
//			List<Map<String, Object>> results = JdbcUtil.executeQuery("call CMIS.RGFX00005('洪谬宰捧径窑扫厂')", Constants.DataSource.TCE_DS);
			
			re = returnSuccess();
			re.setRows(judgList);
		}catch (Exception e){
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
		return re;
	}
	
	
	@RequestMapping("testFXYJ.do")
	@ResponseBody
	public ResultDto testFXYJ(){
		ResultDto re = returnFail("");
		try{
			//调用存储过程示例
			lfTaskService.syncFXWarningTask("20180302");
			
			re = returnSuccess();
		}catch (Exception e){
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
		return re;
	}

}
