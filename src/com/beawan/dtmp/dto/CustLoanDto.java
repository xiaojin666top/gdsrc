package com.beawan.dtmp.dto;


/**
 * 客户在我行用信情况
 * 通过核心客户号就行查询
 * 数据内容包含放款挡  借据挡  业务类型表 ods的字典表
 */
public class CustLoanDto {
	private String loanNo;//贷款编号
    private String mfCustomerId;//核心客户号
    private String creType;//证件类型
    private String creNum;//证件号码
    private String custName;//客户名字
    private String typename;//贷款品种
    private Double amount;//贷款总金额
    private Double balance;//贷款余额
    private String frstAlfdDt;//借款日期
    private String dueDt;//到期日期
    private String purpose;//贷款用途
    private String vouchtype;//担保方式
    private Double loanRate;//利率
    private String fiveClass;//五级分类
	public String getLoanNo() {
		return loanNo;
	}
	public void setLoanNo(String loanNo) {
		this.loanNo = loanNo;
	}
	public String getMfCustomerId() {
		return mfCustomerId;
	}
	public void setMfCustomerId(String mfCustomerId) {
		this.mfCustomerId = mfCustomerId;
	}
	public String getCreType() {
		return creType;
	}
	public void setCreType(String creType) {
		this.creType = creType;
	}
	public String getCreNum() {
		return creNum;
	}
	public void setCreNum(String creNum) {
		this.creNum = creNum;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getTypename() {
		return typename;
	}
	public void setTypename(String typename) {
		this.typename = typename;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public String getFrstAlfdDt() {
		return frstAlfdDt;
	}
	public void setFrstAlfdDt(String frstAlfdDt) {
		this.frstAlfdDt = frstAlfdDt;
	}
	public String getDueDt() {
		return dueDt;
	}
	public void setDueDt(String dueDt) {
		this.dueDt = dueDt;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public String getVouchtype() {
		return vouchtype;
	}
	public void setVouchtype(String vouchtype) {
		this.vouchtype = vouchtype;
	}
	public Double getLoanRate() {
		return loanRate;
	}
	public void setLoanRate(Double loanRate) {
		this.loanRate = loanRate;
	}
	public String getFiveClass() {
		return fiveClass;
	}
	public void setFiveClass(String fiveClass) {
		this.fiveClass = fiveClass;
	}

    
}
