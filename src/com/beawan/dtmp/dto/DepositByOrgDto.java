package com.beawan.dtmp.dto;

public class DepositByOrgDto {
	
	private String orgName; // 支行名称
	private Double tdDeposit; // 定期存款总额（万元）
	private Double saDeposit; // 活期存款总额（万元）
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
	public Double getTdDeposit() {
		return tdDeposit;
	}
	public void setTdDeposit(Double tdDeposit) {
		this.tdDeposit = tdDeposit;
	}
	public Double getSaDeposit() {
		return saDeposit;
	}
	public void setSaDeposit(Double saDeposit) {
		this.saDeposit = saDeposit;
	}

	
}
