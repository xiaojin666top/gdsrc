package com.beawan.dtmp.dto;

/**
 * 担保人编号 等信息
 * @author yuzhejia
 *
 */
public class GuarBaseDto {

	private String custType;//客户编号  对公客户都是 01 02开头
	private String custNo;//企业的信贷号
	private String guarCustNo;//担保人的信贷号
	private String guarCustName;//担保人名称
	
	public String getCustType() {
		return custType;
	}
	public void setCustType(String custType) {
		this.custType = custType;
	}
	public String getCustNo() {
		return custNo;
	}
	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}
	public String getGuarCustNo() {
		return guarCustNo;
	}
	public void setGuarCustNo(String guarCustNo) {
		this.guarCustNo = guarCustNo;
	}
	public String getGuarCustName() {
		return guarCustName;
	}
	public void setGuarCustName(String guarCustName) {
		this.guarCustName = guarCustName;
	}
	
	
}
