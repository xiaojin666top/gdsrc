package com.beawan.dtmp.dto;

/**
 * 授信业务背景资料（原始基础数据）--》授信申请、担保信息等
 * @author User
 *
 */
public class CmisContractDto {

	//业务合同及申请信息
	private String serialNo;//合同流水号
	private String relativeSerialno;//申请流水号
	
	private String vouchtype;//担保方式
	private String purpose;//用途
	private String purposeadd;//用途补充，这个直接理解成实际用途
	private Double businesssum;//申请金额
	private Double contractSum;//合同金额
	private Double balance;//当前余额
	private String paysource;//还款来源
	private Integer termmonth;//还款期限（按月）
	private String occurdate;//签订合同日期 yyyyMMdd
	private String maturity;//到期日期 yyyyMMdd
	private String customername;//客户名称
	private String customerId;//客户信贷号
	private String creditcondition;//授信使用条件
	private Double overduebalance;//逾期金额
	private Double interestbalance1;//表内欠息金额
	private Double interestbalance2;//表外欠息金额
	
	private String classifyResult;//五级分类结果
	private String classifyDate;//最新五级分类时间
	
	
	
	//业务合同和担保合同是一对多的关系
	//担保合同及担保物信息
	private String guarType;//担保类型  01开头保证    05抵押   06质押
	private String guarantorName;//担保人名称
	private String contracttype;//担保合同类型
	private String enddate;//担保期限
	private Double guarantyValue;//担保金额
	private String guarantyType ;//担保物类型
	private String guarantyName;//担保物名称
	private String guarantyregOrg;//担保物登记机关
	private Double confirmValue;//担保物价值
	private Double guarantyRate;//担保比率
	
	//处理后的担保合同及担保信息
	private String guarantorNameGather;//担保人名称集合
	private String guarantyNameGather;//担保物名称集合
	private Double confirmValueGather;//担保物价值集合
	private Double gurantyRateGather;//抵质押 计算公式：抵质押率  =  合同金额 ÷ ∑﹙担保物价值﹚
	private String registerOrg;//是否有权部门登记  是或否
	
	
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public String getRelativeSerialno() {
		return relativeSerialno;
	}
	public void setRelativeSerialno(String relativeSerialno) {
		this.relativeSerialno = relativeSerialno;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public Double getBusinesssum() {
		return businesssum;
	}
	public void setBusinesssum(Double businesssum) {
		this.businesssum = businesssum;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public String getPaysource() {
		return paysource;
	}
	public void setPaysource(String paysource) {
		this.paysource = paysource;
	}
	public Integer getTermmonth() {
		return termmonth;
	}
	public void setTermmonth(Integer termmonth) {
		this.termmonth = termmonth;
	}
	public String getMaturity() {
		return maturity;
	}
	public void setMaturity(String maturity) {
		if(maturity.length()==6||maturity.length()==8){
			this.maturity = StringToDate(maturity);
		}else{
			this.maturity = maturity;
		}
		//this.maturity = maturity;
	}
	public String getCustomername() {
		return customername;
	}
	public void setCustomername(String customername) {
		this.customername = customername;
	}
	public String getVouchtype() {
		return vouchtype;
	}
	public void setVouchtype(String vouchtype) {
		this.vouchtype = vouchtype;
	}
	public String getPurposeadd() {
		return purposeadd;
	}
	public void setPurposeadd(String purposeadd) {
		this.purposeadd = purposeadd;
	}
	public Double getContractSum() {
		return contractSum;
	}
	public void setContractSum(Double contractSum) {
		this.contractSum = contractSum;
	}
	public String getOccurdate() {
		return occurdate;
	}
	public void setOccurdate(String occurdate) {
		if(occurdate.length()==6||occurdate.length()==8){
			this.occurdate = StringToDate(occurdate);
		}else{
			this.occurdate = occurdate;
		}
		
		//this.occurdate = occurdate;
	}
	public Double getOverduebalance() {
		return overduebalance;
	}
	public void setOverduebalance(Double overduebalance) {
		this.overduebalance = overduebalance;
	}
	public Double getInterestbalance1() {
		return interestbalance1;
	}
	public void setInterestbalance1(Double interestbalance1) {
		this.interestbalance1 = interestbalance1;
	}
	public Double getInterestbalance2() {
		return interestbalance2;
	}
	public void setInterestbalance2(Double interestbalance2) {
		this.interestbalance2 = interestbalance2;
	}
	public String getGuarantorName() {
		return guarantorName;
	}
	public void setGuarantorName(String guarantorName) {
		this.guarantorName = guarantorName;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCreditcondition() {
		return creditcondition;
	}
	public void setCreditcondition(String creditcondition) {
		this.creditcondition = creditcondition;
	}
	public String getGuarType() {
		return guarType;
	}
	public void setGuarType(String guarType) {
		this.guarType = guarType;
	}
	public String getContracttype() {
		return contracttype;
	}
	public void setContracttype(String contracttype) {
		this.contracttype = contracttype;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public Double getGuarantyValue() {
		return guarantyValue;
	}
	public void setGuarantyValue(Double guarantyValue) {
		this.guarantyValue = guarantyValue;
	}
	public String getGuarantyType() {
		return guarantyType;
	}
	public void setGuarantyType(String guarantyType) {
		this.guarantyType = guarantyType;
	}
	public String getGuarantyName() {
		return guarantyName;
	}
	public void setGuarantyName(String guarantyName) {
		this.guarantyName = guarantyName;
	}
	public String getGuarantyregOrg() {
		return guarantyregOrg;
	}
	public void setGuarantyregOrg(String guarantyregOrg) {
		this.guarantyregOrg = guarantyregOrg;
	}
	public Double getConfirmValue() {
		return confirmValue;
	}
	public void setConfirmValue(Double confirmValue) {
		this.confirmValue = confirmValue;
	}
	public Double getGuarantyRate() {
		return guarantyRate;
	}
	public void setGuarantyRate(Double guarantyRate) {
		this.guarantyRate = guarantyRate;
	}
	public String getClassifyResult() {
		return classifyResult;
	}
	public void setClassifyResult(String classifyResult) {
		this.classifyResult = classifyResult;
	}
	public String getClassifyDate() {
		return classifyDate;
	}
	public void setClassifyDate(String classifyDate) {
		this.classifyDate = classifyDate;
	}
	public String getGuarantorNameGather() {
		return guarantorNameGather;
	}
	public void setGuarantorNameGather(String guarantorNameGather) {
		this.guarantorNameGather = guarantorNameGather;
	}
	public String getGuarantyNameGather() {
		return guarantyNameGather;
	}
	public void setGuarantyNameGather(String guarantyNameGather) {
		this.guarantyNameGather = guarantyNameGather;
	}
	public Double getConfirmValueGather() {
		return confirmValueGather;
	}
	public void setConfirmValueGather(Double confirmValueGather) {
		this.confirmValueGather = confirmValueGather;
	}
	public Double getGurantyRateGather() {
		return gurantyRateGather;
	}
	public void setGurantyRateGather(Double gurantyRateGather) {
		this.gurantyRateGather = gurantyRateGather;
	}
	public String getRegisterOrg() {
		return registerOrg;
	}
	public void setRegisterOrg(String registerOrg) {
		this.registerOrg = registerOrg;
	}
	
	private String StringToDate(String str){
		
		String result="";
		String year = str.substring(0, 4) + "/";   	
    	if(str.length() > 6){
    		String month = str.substring(4, 6) + "/";
    		String day = str.substring(6, 8);
    		result = year + month + day;
    	}else{
    		String month = str.substring(4, 6);
    		result = year + month;
    		}
    	return result;
    	
    }
	
}
