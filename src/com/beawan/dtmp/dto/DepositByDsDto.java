package com.beawan.dtmp.dto;

public class DepositByDsDto {
	
	private String endMonth; // 截止月份 202010
	private Double tdDeposit; // 定期存款总额（万元）
	private Double saDeposit; // 活期存款总额（万元）
	
	public DepositByDsDto(){}
	public DepositByDsDto(String endMonth, Double tdDeposit, Double saDeposit) {
		super();
		this.endMonth = endMonth;
		this.tdDeposit = tdDeposit;
		this.saDeposit = saDeposit;
	}
	
	public String getEndMonth() {
		return endMonth;
	}
	public void setEndMonth(String endMonth) {
		this.endMonth = endMonth;
	}
	public Double getTdDeposit() {
		return tdDeposit;
	}
	public void setTdDeposit(Double tdDeposit) {
		this.tdDeposit = tdDeposit;
	}
	public Double getSaDeposit() {
		return saDeposit;
	}
	public void setSaDeposit(Double saDeposit) {
		this.saDeposit = saDeposit;
	}
}
