package com.beawan.dtmp.dto;

/****
 * 统计单一对公贷款余额  客户数量分布情况
 */
public class CustSpreadByBalDto {

    private Integer lvNum1;//1千万以下 户数
    private Double lvBal1;//1千万以下贷款 总额
    private Integer lvNum2;//1千万到3千万 户数
    private Double lvBal2;
    private Integer lvNum3;//3千万到5千万 户数
    private Double lvBal3;
    private Integer lvNum4;//5千万到1亿 户数
    private Double lvBal4;
    private Integer lvNum5;//1亿户数
    private Double lvBal5;

    public Integer getLvNum1() {
        return lvNum1;
    }

    public void setLvNum1(Integer lvNum1) {
        this.lvNum1 = lvNum1;
    }

    public Double getLvBal1() {
        return lvBal1;
    }

    public void setLvBal1(Double lvBal1) {
        this.lvBal1 = lvBal1;
    }

    public Integer getLvNum2() {
        return lvNum2;
    }

    public void setLvNum2(Integer lvNum2) {
        this.lvNum2 = lvNum2;
    }

    public Double getLvBal2() {
        return lvBal2;
    }

    public void setLvBal2(Double lvBal2) {
        this.lvBal2 = lvBal2;
    }

    public Integer getLvNum3() {
        return lvNum3;
    }

    public void setLvNum3(Integer lvNum3) {
        this.lvNum3 = lvNum3;
    }

    public Double getLvBal3() {
        return lvBal3;
    }

    public void setLvBal3(Double lvBal3) {
        this.lvBal3 = lvBal3;
    }

    public Integer getLvNum4() {
        return lvNum4;
    }

    public void setLvNum4(Integer lvNum4) {
        this.lvNum4 = lvNum4;
    }

    public Double getLvBal4() {
        return lvBal4;
    }

    public void setLvBal4(Double lvBal4) {
        this.lvBal4 = lvBal4;
    }

    public Integer getLvNum5() {
        return lvNum5;
    }

    public void setLvNum5(Integer lvNum5) {
        this.lvNum5 = lvNum5;
    }

    public Double getLvBal5() {
        return lvBal5;
    }

    public void setLvBal5(Double lvBal5) {
        this.lvBal5 = lvBal5;
    }
}
