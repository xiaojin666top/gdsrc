package com.beawan.dtmp.dto;

/**
 * 合同 借据表
 * @author yuzhejia
 *
 */
public class ContractBillDto {

	private String serialNo;//票据流水号
	private String relativeserialno2;//合同号
	private String customerId;//客户信贷号
	private String occurtype;//发生类型
	private Double businesssum;//借据金额
	private String putoutDate;//发放时间
	
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public String getRelativeserialno2() {
		return relativeserialno2;
	}
	public void setRelativeserialno2(String relativeserialno2) {
		this.relativeserialno2 = relativeserialno2;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getOccurtype() {
		return occurtype;
	}
	public void setOccurtype(String occurtype) {
		this.occurtype = occurtype;
	}
	public Double getBusinesssum() {
		return businesssum;
	}
	public void setBusinesssum(Double businesssum) {
		this.businesssum = businesssum;
	}
	public String getPutoutDate() {
		return putoutDate;
	}
	public void setPutoutDate(String putoutDate) {
		this.putoutDate = putoutDate;
	}
	
	
	
	
}
