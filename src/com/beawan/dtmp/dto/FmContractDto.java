package com.beawan.dtmp.dto;

/**
 * 格式化规整后的数据存储对象，基础输出类型源：----->>> CmisContractDto.java
 * 授信业务背景资料（原始基础数据）--》授信申请、担保信息等
 * @author yuzhejia
 *
 */
public class FmContractDto {

	//业务合同及申请信息
	private String serialNo;//合同流水号
	private String relativeSerialno;//申请流水号
	private String purpose;//用途
	private Double businesssum;//申请金额
	private String paysource;//还款来源
	private Integer termmonth;//还款期限（按月）
	private String maturity;//到期日期 yyyyMMdd
	private String classifyResult;//五级分类结果
	private String classifyDate;//最新五级分类时间
	
	private String creditcondition;//授信使用条件
	
	//保证方式 G 质押 P 抵押 M
	private String gContracttype;//合同类型
	private String gEnddate;//担保期限
	private Double gValue;//担保金额
	//质押
	private String pName ;//质物名称
	private Double pValue;//质物金额
	private String pEnddate;//质物期限
	//抵押
	private String mContracttype;//质押物合同类型
	private Double mValueGroud;//抵押物-土地价值
	private Double mValueHouse;//抵押物-房屋价值
	private Double mValueEquip;//抵押物-设备价值
	private Double mValueVehicle;//抵押物-运输工具价值
	private Double mValueOther;//抵押物-其他价值
	private String mRegisterOrg;//是否有权部门登记  是或否
	
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public String getRelativeSerialno() {
		return relativeSerialno;
	}
	public void setRelativeSerialno(String relativeSerialno) {
		this.relativeSerialno = relativeSerialno;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	public Double getBusinesssum() {
		return businesssum;
	}
	public void setBusinesssum(Double businesssum) {
		this.businesssum = businesssum;
	}
	public String getPaysource() {
		return paysource;
	}
	public void setPaysource(String paysource) {
		this.paysource = paysource;
	}
	public Integer getTermmonth() {
		return termmonth;
	}
	public void setTermmonth(Integer termmonth) {
		this.termmonth = termmonth;
	}
	public String getMaturity() {
		return maturity;
	}
	public void setMaturity(String maturity) {
		this.maturity = maturity;
	}
	public String getCreditcondition() {
		return creditcondition;
	}
	public void setCreditcondition(String creditcondition) {
		this.creditcondition = creditcondition;
	}
	
	public String getClassifyResult() {
		return classifyResult;
	}
	public void setClassifyResult(String classifyResult) {
		this.classifyResult = classifyResult;
	}
	public String getClassifyDate() {
		return classifyDate;
	}
	public void setClassifyDate(String classifyDate) {
		this.classifyDate = classifyDate;
	}
	public String getgContracttype() {
		return gContracttype;
	}
	public void setgContracttype(String gContracttype) {
		this.gContracttype = gContracttype;
	}
	public String getgEnddate() {
		return gEnddate;
	}
	public void setgEnddate(String gEnddate) {
		this.gEnddate = gEnddate;
	}
	public Double getgValue() {
		return gValue;
	}
	public void setgValue(Double gValue) {
		this.gValue = gValue;
	}
	public String getpName() {
		return pName;
	}
	public void setpName(String pName) {
		this.pName = pName;
	}
	
	public Double getpValue() {
		return pValue;
	}
	public void setpValue(Double pValue) {
		this.pValue = pValue;
	}
	public String getpEnddate() {
		return pEnddate;
	}
	public void setpEnddate(String pEnddate) {
		this.pEnddate = pEnddate;
	}
	public String getmContracttype() {
		return mContracttype;
	}
	public void setmContracttype(String mContracttype) {
		this.mContracttype = mContracttype;
	}
	public Double getmValueGroud() {
		return mValueGroud;
	}
	public void setmValueGroud(Double mValueGroud) {
		this.mValueGroud = mValueGroud;
	}
	public Double getmValueHouse() {
		return mValueHouse;
	}
	public void setmValueHouse(Double mValueHouse) {
		this.mValueHouse = mValueHouse;
	}
	public Double getmValueEquip() {
		return mValueEquip;
	}
	public void setmValueEquip(Double mValueEquip) {
		this.mValueEquip = mValueEquip;
	}
	public Double getmValueVehicle() {
		return mValueVehicle;
	}
	public void setmValueVehicle(Double mValueVehicle) {
		this.mValueVehicle = mValueVehicle;
	}
	public Double getmValueOther() {
		return mValueOther;
	}
	public void setmValueOther(Double mValueOther) {
		this.mValueOther = mValueOther;
	}
	public String getmRegisterOrg() {
		return mRegisterOrg;
	}
	public void setmRegisterOrg(String mRegisterOrg) {
		this.mRegisterOrg = mRegisterOrg;
	}
	

	

}
