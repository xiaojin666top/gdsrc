package com.beawan.dtmp.dto;

/**
 * 按  styName 统计客户贷款余额的 总数
 * 包括担保方式、二级行业、五级分类
 */
public class StyBalDto {

    private String styName;//包括担保方式、二级行业、五级分类
    private Double balVal;//贷款余额  总金额
    private Double rate;//占比

    public StyBalDto(){}
    public StyBalDto(String styName, Double balVal) {
		super();
		this.styName = styName;
		this.balVal = balVal;
	}
    
	public StyBalDto(String styName, Double balVal, Double rate) {
		super();
		this.styName = styName;
		this.balVal = balVal;
		this.rate = rate;
	}

	public String getStyName() {
        return styName;
    }

    public void setStyName(String styName) {
        this.styName = styName;
    }

    public Double getBalVal() {
        return balVal;
    }

    public void setBalVal(Double balVal) {
        this.balVal = balVal;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }
}
