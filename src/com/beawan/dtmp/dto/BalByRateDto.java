package com.beawan.dtmp.dto;

/****
 * 统计贷款利率 对应 对公贷款余额  分布情况
 * 单位：亿元
 */
public class BalByRateDto {

    private Double lvBal1;//5%以下
    private Double lvBal2;//5%-6%
    private Double lvBal3;//6%-7%以下
    private Double lvBal4;//7%-8%以下
    private Double lvBal5;//8%以上
    
	public Double getLvBal1() {
		return lvBal1;
	}
	public void setLvBal1(Double lvBal1) {
		this.lvBal1 = lvBal1;
	}
	public Double getLvBal2() {
		return lvBal2;
	}
	public void setLvBal2(Double lvBal2) {
		this.lvBal2 = lvBal2;
	}
	public Double getLvBal3() {
		return lvBal3;
	}
	public void setLvBal3(Double lvBal3) {
		this.lvBal3 = lvBal3;
	}
	public Double getLvBal4() {
		return lvBal4;
	}
	public void setLvBal4(Double lvBal4) {
		this.lvBal4 = lvBal4;
	}
	public Double getLvBal5() {
		return lvBal5;
	}
	public void setLvBal5(Double lvBal5) {
		this.lvBal5 = lvBal5;
	}

    
}
