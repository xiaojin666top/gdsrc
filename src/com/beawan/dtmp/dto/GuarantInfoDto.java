package com.beawan.dtmp.dto;

/**
 * 押品 信息
 * @author yuzhejia
 *
 */
public class GuarantInfoDto {

	private String guarantytype;//担保详细类型
	private String guarantystatus;//担保物状态
	private String ownerid;//所有者id
	private String evalmethod;//评估方式---------ODS0131
	private Double confirmvalue;//认定价值
	private String guarantyregorg;//抵押等级机关
	
	public String getGuarantytype() {
		return guarantytype;
	}
	public void setGuarantytype(String guarantytype) {
		this.guarantytype = guarantytype;
	}
	public String getGuarantystatus() {
		return guarantystatus;
	}
	public void setGuarantystatus(String guarantystatus) {
		this.guarantystatus = guarantystatus;
	}
	public String getOwnerid() {
		return ownerid;
	}
	public void setOwnerid(String ownerid) {
		this.ownerid = ownerid;
	}
	public String getEvalmethod() {
		return evalmethod;
	}
	public void setEvalmethod(String evalmethod) {
		this.evalmethod = evalmethod;
	}
	public Double getConfirmvalue() {
		return confirmvalue;
	}
	public void setConfirmvalue(Double confirmvalue) {
		this.confirmvalue = confirmvalue;
	}
	public String getGuarantyregorg() {
		return guarantyregorg;
	}
	public void setGuarantyregorg(String guarantyregorg) {
		this.guarantyregorg = guarantyregorg;
	}
	
	
}
