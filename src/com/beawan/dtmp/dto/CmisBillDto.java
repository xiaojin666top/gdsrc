package com.beawan.dtmp.dto;

/**
 * 合同对应借据信息---->即用信数据
 * @author yuzhejia
 *
 */
public class CmisBillDto {

	private String relativeserialno2;//合同号
	private Double businesssum;//借据金额
	private String putoutDate;//发放时间
	
	
	public String getRelativeserialno2() {
		return relativeserialno2;
	}
	public void setRelativeserialno2(String relativeserialno2) {
		this.relativeserialno2 = relativeserialno2;
	}
	public Double getBusinesssum() {
		return businesssum;
	}
	public void setBusinesssum(Double businesssum) {
		this.businesssum = businesssum;
	}
	public String getPutoutDate() {
		return putoutDate;
	}
	public void setPutoutDate(String putoutDate) {
		if(putoutDate.length()==6||putoutDate.length()==8){
			this.putoutDate = StringToDate(putoutDate);
		}else{
			this.putoutDate = putoutDate;
		}
	}
	
	private String StringToDate(String str){
		String result = "";
    	String year = str.substring(0, 4) + "/";
    	
    	if(str.length() > 6){
    		String month = str.substring(4, 6) + "/";
    		String day = str.substring(6, 8);
    		result = year + month + day;
    	}else{
    		String month = str.substring(4, 6);
    		result = year + month;
    	}
    	return result;
    }
}
