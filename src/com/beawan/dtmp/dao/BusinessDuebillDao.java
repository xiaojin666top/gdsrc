package com.beawan.dtmp.dao;

import com.beawan.core.BaseDao;
import com.beawan.dtmp.entity.BusinessDuebill;

/**
 * @author yzj
 */
public interface BusinessDuebillDao extends BaseDao<BusinessDuebill> {
}
