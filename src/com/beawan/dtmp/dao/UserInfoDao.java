package com.beawan.dtmp.dao;

import com.beawan.core.BaseDao;
import com.beawan.dtmp.entity.UserInfo;

/**
 * @author yzj
 */
public interface UserInfoDao extends BaseDao<UserInfo> {
}
