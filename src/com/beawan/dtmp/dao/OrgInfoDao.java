package com.beawan.dtmp.dao;

import com.beawan.core.BaseDao;
import com.beawan.dtmp.entity.OrgInfo;

/**
 * @author yzj
 */
public interface OrgInfoDao extends BaseDao<OrgInfo> {
}
