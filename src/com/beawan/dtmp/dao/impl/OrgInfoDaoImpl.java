package com.beawan.dtmp.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.dtmp.dao.OrgInfoDao;
import com.beawan.dtmp.entity.OrgInfo;

/**
 * @author yzj
 */
@Repository("orgInfoDao")
public class OrgInfoDaoImpl extends BaseDaoImpl<OrgInfo> implements OrgInfoDao{

}
