package com.beawan.dtmp.dao.impl;

import com.beawan.common.Constants;
import com.beawan.core.BaseDaoImpl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.stereotype.Repository;
import com.beawan.dtmp.dao.TdacnacnDao;
import com.beawan.dtmp.dto.DepositByOrgDto;
import com.beawan.dtmp.dto.StyBalDto;
import com.beawan.dtmp.entity.Tdacnacn;
import com.platform.util.StringUtil;

/**
 * @author yzj
 */
@Repository("tdacnacnDao")
public class TdacnacnDaoImpl extends BaseDaoImpl<Tdacnacn> implements TdacnacnDao{

	@Override
	public Double getTotalDeposit(String depositType) throws Exception {
		String condition = "";
		StringBuilder sql = new StringBuilder();
		if(Constants.DEPOSIT_TYPE.PUBLIC_DEPOSIT.equals(depositType)){// 对公存款
			condition = " AND (SUBSTR(B.GL_ACC_NO,1,4) IN ('2001','2002','2006','2011','2012','2013') OR SUBSTR(B.GL_ACC_NO,1,6) IN ('200610', '200520'))";
		}else if (Constants.DEPOSIT_TYPE.SAVINGS_DEPOSIT.equals(depositType)){// 储蓄存款
			condition = " AND (SUBSTR(B.GL_ACC_NO,1,4) IN ('2003', '2004') OR SUBSTR(B.GL_ACC_NO,1,6) IN ('200505', '200515', '200525', '200530'))";
		}
		sql.append("SELECT SUM(ACCT_BAL)/10000 totalDeposit FROM (")
			.append(" SELECT SUM(A.TD_ACTU_AMT) ACCT_BAL FROM CBOD.TDACNACN A")
			.append(" LEFT JOIN CMIS.F_PRD_DEP_GL B ON B.DEP_TYP=A.TD_DEP_TYP AND B.DEP_PRD_N=A.TD_DEP_PRD_N")
			.append(" WHERE A.TD_ACTU_AMT>0 AND A.TD_BELONG_INSTN_COD='320622051' AND B.ACCT_TYP='TD'");
		sql.append(condition).append(" UNION ALL")
			.append(" SELECT SUM(C.SA_ACCT_BAL) ACCT_BAL FROM CBOD.SAACNACN A")
			.append(" LEFT JOIN CMIS.F_PRD_DEP_GL B ON B.DEP_TYP=A.SA_DEP_TYP")
			.append(" LEFT JOIN CBOD.SAACNAMT C ON C.FK_SAACN_KEY=A.SA_ACCT_NO")
			.append(" WHERE C.SA_ACCT_BAL>0 AND B.ACCT_TYP='SA' AND A.SA_BELONG_INSTN_COD='320622051'");
		sql.append(condition).append(")");
		if (Constants.DEPOSIT_TYPE.MARGIN_DEPOSIT.equals(depositType)){ // 保证金存款：只有活期
			sql.setLength(0);// 清空sql
			sql.append("SELECT SUM(C.SA_ACCT_BAL)/10000 totalDeposit FROM CBOD.SAACNACN A")
				.append(" LEFT JOIN CMIS.F_PRD_DEP_GL B ON B.DEP_TYP=A.SA_DEP_TYP")
				.append(" LEFT JOIN CBOD.SAACNAMT C ON C.FK_SAACN_KEY=A.SA_ACCT_NO")
				.append(" WHERE C.SA_ACCT_BAL>0 AND B.ACCT_TYP='SA' AND SUBSTR(B.GL_ACC_NO,1,4)='2014'")
				.append(" AND A.SA_BELONG_INSTN_COD='320622051'");
		} 
		BigDecimal result = this.findCustOneBySql(BigDecimal.class, sql.toString());
		if(Objects.isNull(result)) return 0d;
		return result.doubleValue();
	}

	@Override
	public List<DepositByOrgDto> getDepositByOrg() throws Exception {
		StringBuilder sql = new StringBuilder("SELECT AA.ORGNAME orgName, AA.DQCK/10000 tdDeposit, BB.HQCK/10000 saDeposit FROM")
            .append(" (SELECT (CASE WHEN (C.ORGABBREVI IS NULL) THEN '其他支行' ELSE C.ORGABBREVI END) ORGNAME, SUM(A.TD_ACTU_AMT) DQCK")
            .append(" FROM CBOD.TDACNACN A")
            .append(" LEFT JOIN CMIS.F_PRD_DEP_GL B ON B.DEP_TYP=A.TD_DEP_TYP AND B.DEP_PRD_N=A.TD_DEP_PRD_N")
            .append(" LEFT JOIN CMIS.ORG_INFO C ON C.ORGID=A.TD_BELONG_INSTN_COD")
            .append(" WHERE A.TD_ACTU_AMT>0 AND B.ACCT_TYP='TD'")
            .append(" AND (SUBSTR(B.GL_ACC_NO,1,4) IN ('2001','2002','2006','2011','2012','2013') OR SUBSTR(B.GL_ACC_NO,1,6) IN ('200610', '200520'))")
            .append(" GROUP BY C.ORGABBREVI) AA LEFT JOIN (")
            .append(" SELECT (CASE WHEN (C.ORGABBREVI IS NULL) THEN '其他支行' ELSE C.ORGABBREVI END) ORGNAME, SUM(D.SA_ACCT_BAL) HQCK")
            .append(" FROM CBOD.SAACNACN A")
            .append(" LEFT JOIN CMIS.F_PRD_DEP_GL B ON B.DEP_TYP=A.SA_DEP_TYP")
            .append(" LEFT JOIN CMIS.ORG_INFO C ON C.ORGID=A.SA_BELONG_INSTN_COD")
            .append(" LEFT JOIN CBOD.SAACNAMT D ON D.FK_SAACN_KEY=A.SA_ACCT_NO")
            .append(" WHERE D.SA_ACCT_BAL>0 AND B.ACCT_TYP='SA'")
            .append(" AND (SUBSTR(B.GL_ACC_NO,1,4) IN ('2001','2002','2006','2011','2012','2013') OR SUBSTR(B.GL_ACC_NO,1,6) IN ('200610', '200520'))")
            .append(" GROUP BY C.ORGABBREVI) BB ON AA.ORGNAME=BB.ORGNAME");
		return this.findCustListBySql(DepositByOrgDto.class, sql.toString());
	}

	@Override
	public Double getTdDeposit(String endDate) throws Exception {
		StringBuilder sql = new StringBuilder("SELECT SUM(TD_ACCT_BAL)/100000000 FROM (")
            .append(" SELECT C.FK_TDACN_KEY, C.TD_TX_DT, C.TD_TX_TM, C.TD_ACCT_BAL,")
            .append(" ROW_NUMBER() OVER(PARTITION BY C.FK_TDACN_KEY ORDER BY C.TD_TX_DT DESC,C.TD_TX_TM DESC) AS rowNo")
            .append(" FROM CBOD.TDACNACN A")
            .append(" LEFT JOIN CMIS.F_PRD_DEP_GL B ON B.DEP_TYP=A.TD_DEP_TYP AND B.DEP_PRD_N=A.TD_DEP_PRD_N")
            .append(" LEFT JOIN CBOD.TDACNINT C ON C.FK_TDACN_KEY=A.TD_TD_ACCT_NO")
            .append(" WHERE C.TD_TX_DT<= ?0 AND C.TD_PRT_FLG IS NULL AND C.TD_ACCT_BAL>0 AND B.ACCT_TYP='TD'")
            .append(" AND (SUBSTR(B.GL_ACC_NO,1,4) IN ('2001','2002','2006','2011','2012','2013') OR SUBSTR(B.GL_ACC_NO,1,6) IN ('200610', '200520'))")
            .append(" AND A.TD_BELONG_INSTN_COD='320622051') WHERE ROWNO=1");
		BigDecimal result = this.findCustOneBySql(BigDecimal.class, sql.toString(), endDate);
		if (result == null) return 0d;
		return result.doubleValue();
	}

	@Override
	public Double getSaDeposit(String endDate) throws Exception {
		StringBuilder sql = new StringBuilder("SELECT SUM(SA_DDP_ACCT_BAL)/100000000 FROM(")
            .append(" SELECT C.FK_SAACN_KEY, C.SA_TX_DT, C.SA_TX_TM, C.SA_DDP_ACCT_BAL,")
            .append(" ROW_NUMBER() OVER(PARTITION BY C.FK_SAACN_KEY ORDER BY C.SA_TX_DT DESC, C.SA_TX_TM DESC) AS rowNo")
            .append(" FROM CBOD.SAACNACN A")
            .append(" LEFT JOIN CMIS.F_PRD_DEP_GL B ON B.DEP_TYP=A.SA_DEP_TYP")
            .append(" LEFT JOIN CBOD.SAACNTXN C ON C.FK_SAACN_KEY=A.SA_ACCT_NO")
            .append(" WHERE B.ACCT_TYP='SA' AND (SUBSTR(B.GL_ACC_NO,1,4) IN ('2001','2002','2006','2011','2012','2013') OR SUBSTR(B.GL_ACC_NO,1,6) IN ('200610', '200520'))")
            .append(" AND C.SA_TX_DT<= ?0 AND C.SA_DDP_ACCT_BAL>0")
            .append(" AND A.SA_BELONG_INSTN_COD='320622051') WHERE rowNo=1");
		BigDecimal result = this.findCustOneBySql(BigDecimal.class, sql.toString(), endDate);
		if (result == null) return 0d;
		return result.doubleValue();
	}

	@Override
	public List<StyBalDto> getSaDepositByCustNo(String mfCustno) {
		if(StringUtil.isEmpty(mfCustno)) return null;
		
		StringBuilder sql = new StringBuilder("select A.SA_ACCT_NO styName, ")
			.append("B.SA_ACCT_BAL balVal from CBOD.SAACNACN A ")
			.append("join CBOD.SAACNAMT B ON A.SA_ACCT_NO=B.FK_SAACN_KEY where SA_CUST_NO=:mfCustno");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("mfCustno", mfCustno);
		
		return this.findCustListBySql(StyBalDto.class, sql.toString(), params);
	}

	@Override
	public List<StyBalDto> getTdDepositByCustNo(String mfCustno) {
		if(StringUtil.isEmpty(mfCustno)) return null;
		
		StringBuilder sql = new StringBuilder("select TD_TD_ACCT_NO styName,TD_ACTU_AMT balVal ")
			.append("from CBOD.TDACNACN WHERE TD_CUST_NO=:mfCustno");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("mfCustno", mfCustno);
		
		return this.findCustListBySql(StyBalDto.class, sql.toString(), params);
	}

	
}
