package com.beawan.dtmp.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.dtmp.dao.UserInfoDao;
import com.beawan.dtmp.entity.UserInfo;

/**
 * @author yzj
 */
@Repository("userInfoDao")
public class UserInfoDaoImpl extends BaseDaoImpl<UserInfo> implements UserInfoDao{

}
