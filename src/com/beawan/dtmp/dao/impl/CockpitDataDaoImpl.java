package com.beawan.dtmp.dao.impl;

import com.beawan.common.Constants;
import com.beawan.core.BaseDaoImpl;
import com.beawan.dtmp.dao.CockpitDataDao;
import com.beawan.dtmp.entity.CockpitData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import com.beawan.dtmp.entity.CockpitData;

/**
 * @author yzj
 */
@Repository("cockpitDataDao")
public class CockpitDataDaoImpl extends BaseDaoImpl<CockpitData> implements CockpitDataDao{

	@Override
	public CockpitData getLastCockpitData() throws Exception {
		String query = "STATUS = :status ORDER BY SYCN_DATE";
		Map<String, Object> params = new HashMap<>();
		params.put("status", Constants.NORMAL);
		List<CockpitData> resultLsit = this.select(query, params);
		if(CollectionUtils.isEmpty(resultLsit)) return null;
		return resultLsit.get(0);
	}

	
}
