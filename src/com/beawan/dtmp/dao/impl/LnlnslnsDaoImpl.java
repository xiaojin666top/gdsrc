package com.beawan.dtmp.dao.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.dtmp.dao.LnlnslnsDao;
import com.beawan.dtmp.dto.BalByRateDto;
import com.beawan.dtmp.dto.CmisBillDto;
import com.beawan.dtmp.dto.CmisContractDto;
import com.beawan.dtmp.dto.CustLoanDto;
import com.beawan.dtmp.dto.CustSpreadByBalDto;
import com.beawan.dtmp.dto.GuarBaseDto;
import com.beawan.dtmp.dto.GuarantInfoDto;
import com.beawan.dtmp.dto.StyBalDto;
import com.beawan.dtmp.entity.Lnlnslns;
import com.beawan.loanAfter.entity.FcBusiContract;
import com.beawan.survey.guarantee.entity.CompFinanceTransfer;
import com.beawan.survey.guarantee.entity.GuaAccounts;
import com.beawan.survey.guarantee.entity.GuaChattel;
import com.beawan.survey.guarantee.entity.GuaCompany;
import com.beawan.survey.guarantee.entity.GuaDepositReceipt;
import com.beawan.survey.guarantee.entity.GuaEstate;
import com.beawan.survey.guarantee.entity.GuaPerson;
import com.google.common.collect.Maps;
import com.platform.util.StringUtil;

/**
 * @author yzj
 */
@Repository("lnlnslnsDao")
public class LnlnslnsDaoImpl extends BaseDaoImpl<Lnlnslns> implements LnlnslnsDao{

    @Override
    public List<CustLoanDto> getCustLoan(String mfCustomerId,List<String> fiveClassList) throws Exception {
        StringBuilder sql = new StringBuilder();
        Map<String, Object> params = new HashMap<>();
        sql.append("SELECT LN_LN_ACCT_NO AS loanNo, LN_CUST_NO AS mfCustomerId, ")
        .append(" LN_CERT_TYP AS creType, LN_CERT_ID  AS creNum, LN_CUST_NAME AS custName,")
        .append(" typename  as typename,LN_TOTL_LN_AMT_HYPO_AMT AS amount,LN_LN_BAL as balance,")
        .append(" to_date(case when LN_FRST_ALFD_DT_N<10000101 then 10000101 else  LN_FRST_ALFD_DT_N  end,'yyyymmdd')  AS frstAlfdDt,")
        .append(" to_date(case when  LN_DUE_DT_N<10000101 then 10000101 else LN_DUE_DT_N  end,'yyyymmdd') AS dueDt,")
        .append(" g.purpose as purpose, CI_LN_STY as vouchtype,LN_FRST_INTC_INTR as loanRate, e.CLASSIFYRESULT as fiveClass")
        .append(" FROM CBOD.LNLNSLNS A")
        .append(" LEFT OUTER JOIN CBOD.CICILCRF D ON  (A.LN_CUST_NO=D.FK_CICIL_KEY AND A.LN_CRLMT_NO=D.CI_CRLMT_NO)")
        .append(" LEFT OUTER JOIN CMIS.BUSINESS_DUEBILL E ON A.LN_LN_ACCT_NO=E.SERIALNO")
        .append(" LEFT OUTER JOIN CMIS.business_contract  g ON g.SERIALNO=e.RELATIVESERIALNO2")
        .append(" LEFT OUTER JOIN CMIS.business_type l ON  e.businesstype=l.typeno ")
        .append(" where ln_ln_bal >:balance")
        .append(" and ln_apcl_flg<>:aoclFlg")
        .append(" and LN_CUST_TYP =:custTyp");
        //.append(" AND e.CLASSIFYRESULT IN ('QL01','QL02')");
        params.put("balance", "0");
        params.put("aoclFlg", "Y");
        params.put("custTyp", "2");//客户类型为2---对公客户
        if(!StringUtil.isEmptyString(mfCustomerId)){
        	sql.append(" and LN_CUST_NO=:mfCustomerId");
        	params.put("mfCustomerId", mfCustomerId);
        }
        if(!CollectionUtils.isEmpty(fiveClassList)){
        	String types = "";
    		for(String fiveClass:fiveClassList){
    			types += ",'" + fiveClass + "'";
    		}
    		String inStr = types.substring(1);
    		sql.append(" and e.CLASSIFYRESULT IN:inStr");
        	params.put("inStr", inStr);
        }
        return this.findCustListBySql(CustLoanDto.class, sql.toString(), params);
    }
    
    @Override
    public Double getLoanBal() throws Exception {
        //LN_CUST_TYP 1 对私 2对公  公司部 320622051
        String sql = "SELECT SUM(LN_LN_BAL)/10000 bal FROM CBOD.LNLNSLNS A WHERE A.LN_CUST_TYP='2' AND A.LN_BELONG_INSTN_COD='320622051'";
        return this.findCustOneBySql(BigDecimal.class, sql.toString()).doubleValue();
    }


    @Override
    public CustSpreadByBalDto getCustSpreadByBal(Integer unit) throws Exception {
        StringBuilder sql = new StringBuilder();
        if(unit==null || unit<=0){
            unit = 100000000;
        }
        
        sql.append("select sum(case when (bal<=10000000) then 1 else 0 end) as lvNum1,")
                .append("sum(case when (bal<=10000000) then bal else 0 end /"+unit+") as lvBal1, ")
                .append("sum(case when (bal>10000000 and bal<=30000000) then 1 else 0 end) as lvNum2, ")
                .append("sum(case when (bal>10000000 and bal<=30000000) then bal else 0 end /"+unit+") as lvBal2, ")
                .append("sum(case when (bal>30000000 and bal<=50000000) then 1 else 0 end) as lvNum3, ")
                .append("sum(case when (bal>30000000 and bal<=50000000) then bal else 0 end /"+unit+") as lvBal3, ")
                .append("sum(case when (bal>50000000 and bal<=100000000) then 1 else 0 end) as lvNum4, ")
                .append("sum(case when (bal>50000000 and bal<=100000000) then bal else 0 end /"+unit+") as lvBal4, ")
                .append("sum(case when (bal>100000000) then 1 else 0 end) as lvNum5, ")
                .append("sum(case when (bal>100000000) then bal else 0 end /"+unit+") as lvBal5 ")
                .append("from (SELECT LN_CUST_NO,sum(LN_LN_BAL) bal FROM CBOD.LNLNSLNS A ")
                .append("WHERE A.LN_CUST_TYP='2' AND A.LN_BELONG_INSTN_COD='320622051' GROUP BY LN_CUST_NO) ln_data ");
        List<CustSpreadByBalDto> spreadList = findCustListBySql(CustSpreadByBalDto.class, sql.toString());
        if(CollectionUtils.isEmpty(spreadList)){
            System.out.println("应该不会出现这种情况，查询统计信息为空");
            return null;
        }
        return spreadList.get(0);
    }

    @Override
    public List<StyBalDto> getBalStry() throws Exception {
        StringBuilder sql = new StringBuilder();
        sql.append("select  CASE CI_LN_STY ")
                .append("WHEN 'A' THEN '质押' ")
                .append("WHEN 'B' THEN '抵押' ")
                .append("WHEN 'C' THEN '保证' ")
                .append("WHEN 'D' THEN '信用/免担保' END as styName,sum(LN_LN_BAL)/100000000 balVal ")
                .append("from CBOD.lnlnslns lns ")
                .append("join CBOD.CICILCRF cici on lns.LN_CUST_NO=cici.FK_CICIL_KEY ")
                .append("AND lns.LN_CRLMT_NO=cici.CI_CRLMT_NO ")
                .append("WHERE lns.LN_BELONG_INSTN_COD='320622051' group by cici.CI_LN_STY");
        List<StyBalDto> list = findCustListBySql(StyBalDto.class, sql.toString());
        return list;
    }

	@Override
	public List<StyBalDto> getBalByClassifyResult() throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT case B.CLASSIFYRESULT ")
			.append("WHEN 'QL01'    THEN '正常' ")
			.append("WHEN 'QL02'    THEN '关注' ")
			.append("WHEN 'QL03'    THEN '次级' ")
			.append("WHEN 'QL04'    THEN '可疑' ")
			.append("WHEN 'QL05'    THEN '损失' ")
			.append("WHEN 'QL06'    THEN '未分类' ")
			.append("WHEN 'X'       THEN '未说明'  END AS styName,")
			.append("sum(LN_LN_BAL)/100000000 balVal  FROM CBOD.lnlnslns A ")
			.append("JOIN CMIS.BUSINESS_DUEBILL B ON A.LN_LN_ACCT_NO=B.SERIALNO ")
			.append("where A.LN_CUST_TYP = '2' AND A.LN_BELONG_INSTN_COD='320622051' ")
			.append("GROUP BY B.CLASSIFYRESULT");
		List<StyBalDto> list = findCustListBySql(StyBalDto.class, sql.toString());
		return list;
	}

	@Override
	public BalByRateDto getBalByRate() throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("select sum(CASE WHEN LN_FRST_INTC_INTR<=5 THEN LN_LN_BAL else 0 END)/100000000 lvBal1,")
			.append("sum(CASE WHEN LN_FRST_INTC_INTR>5 AND LN_FRST_INTC_INTR<=6 THEN LN_LN_BAL ELSE 0 END)/100000000 lvBal2, ")
			.append("sum(CASE WHEN LN_FRST_INTC_INTR>6 AND LN_FRST_INTC_INTR<=7 THEN LN_LN_BAL ELSE 0 END)/100000000 lvBal3, ")
			.append("sum(CASE WHEN LN_FRST_INTC_INTR>7 AND LN_FRST_INTC_INTR<=8 THEN LN_LN_BAL ELSE 0 END)/100000000 lvBal4, ")
			.append("sum(CASE WHEN LN_FRST_INTC_INTR>8 THEN LN_LN_BAL ELSE 0 END)/100000000 lvBal5 ")
			.append("from CBOD.lnlnslns A ")
			.append("where A.LN_CUST_TYP='2' AND A.ln_ln_bal>0 and  A.LN_APCL_FLG<>'Y' ")
			.append("AND A.LN_BELONG_INSTN_COD='320622051' and A.ln_busn_typ not in ('680')");
		List<BalByRateDto> list = findCustListBySql(BalByRateDto.class, sql.toString());
        if(CollectionUtils.isEmpty(list)){
            System.out.println("应该不会出现这种情况，查询统计信息为空");
            return null;
        }
        return list.get(0);
	}

	@Override
	public List<FcBusiContract> getBusiContractByCust(String xdCustNo) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("select A.CUSTOMERNAME,A.SERIALNO,A.ARTIFICIALNO,")
			.append("D.CODE_VALUE_REFE vouchType,A.BALANCE,A.PUTOUTDATE startDate, ")
			.append("A.MATURITY endDate,C.GUARANTYNAME ")
			.append("from CMIS.BUSINESS_CONTRACT zz ")
			.append("join  CMIS.lineapply_relative ll on zz.SERIALNO=ll.objectno ")
			.append("join CMIS.BUSINESS_CONTRACT A on ll.SERIALNO=A.SERIALNO ")
			.append("LEFT JOIN cmis.GUARANTY_RELATIVE B ON A.SERIALNO=B.OBJECTNO ")
			.append("LEFT JOIN cmis.GUARANTY_INFO C ON B.GUARANTYID=C.GUARANTYID ")
			.append("join cmis.F_COM_CODE_VALUE D on A.VOUCHTYPE=D.CODE_VAL ")
			.append("where D.CODE_NO='JB00015' AND A.BALANCE>0 AND ll.flag='01' and ll.effectflag='01' ")
			.append("and A.CUSTOMERID= :xdCustNo ");
		Map<String, Object> params = new HashMap<>();
		params.put("xdCustNo", xdCustNo);
		return findCustListBySql(FcBusiContract.class, sql, params);
	}

	@Override
	public String getCustFinaDate(String xdCustNo) {
		StringBuilder sql = new StringBuilder();
		sql.append("select max(REPORTDATE) from cmis.CUSTOMER_FSRECORD where CUSTOMERID= ?0");
		return findCustOneBySql(String.class, sql, xdCustNo);
	}

	@Override
	public List<BigDecimal> getCustFinaCurrMainSubject(String xdCustNo, String date) {
		StringBuilder sql = new StringBuilder();
		sql.append("select C.COL2VALUE from (")
			.append("SELECT CF.*,row_number() over(partition by CF.CUSTOMERID,CF.REPORTDATE order by CF.REPORTSTATUS desc) as rn ")
			.append("FROM cmis.CUSTOMER_FSRECORD CF ) A ")
			.append("join cmis.REPORT_RECORD B ON A.RECORDNO=B.OBJECTNO ")
			.append("JOIN cmis.REPORT_DATA C ON B.REPORTNO=C.REPORTNO ")
			.append("WHERE A.CUSTOMERID= ?0 AND A.REPORTDATE= ?1 ")
			.append("AND C.ROWSUBJECT IN ('165','246','266','301','329') ")
			.append("AND A.RN=1 ORDER BY C.ROWSUBJECT");
		//资产总计 165		负债合计 246	所有者权益 266	营业收入 301 	利润总额  329
		//新旧报表准则对应的字段subjectno针对以上大科目都是相同的
		return findCustOneListBySql(BigDecimal.class, sql, xdCustNo, date);
	}
	
	
	/**
	 * 获取营业收入
	 * */
	@Override
	public Double getfnValue(String customerNo, String date) {
		StringBuilder sql = new StringBuilder();
		sql.append("select C.COL2_VALUE ")
			//.append("SELECT CF.*,row_number() over(partition by CF.CUSTOMER_ID,CF.REPORT_DATE order by CF.REPORT_STATUS desc) as rn ")
			.append("FROM GDTCESYS.CUS_FSRECORD A ")
			.append("join GDTCESYS.CUS_REPORT_RECORD B ON A.RECORD_NO=B.OBJECT_NO ")
			.append("JOIN GDTCESYS.CUS_REPORT_DATA C ON B.REPORT_NO=C.REPORT_NO ")
			.append("WHERE A.CUSTOMER_ID= ?0 AND A.REPORT_DATE= ?1 ")
			.append("AND C.ROW_SUBJECT = '301' ");
			//.append("AND A.RN=1 ORDER BY C.ROW_SUBJECT");
		//资产总计 165		负债合计 246	所有者权益 266	营业收入 301 	利润总额  329
		//新旧报表准则对应的字段subjectno针对以上大科目都是相同的
		return findCustOneBySql(Double.class, sql, customerNo, date);
	}

	
	@Override
	public List<BigDecimal> getRatioFinaData(String xdCustNo, String currDate, String oneYearDate, String twoYearDate) {
		/**
		 * 600		流动比率
		 * 612		资产负债率
		 * 642		销售利润率   
		 * 650		应收账款周转率
		 * 652		存货周转率
		 * 915		资产利润率
		 * */
		String[] subjectNos = new String[]{"600","612","642","650","652","915"};
		String finaDataSql = this.getFinaDataSql(xdCustNo, currDate, oneYearDate, twoYearDate, subjectNos);
		return findCustOneListBySql(BigDecimal.class, finaDataSql, null);
	}
	

	@Override
	public List<BigDecimal> getCashFlowFinaData(String xdCustNo, String currDate, String oneYearDate, String twoYearDate) {
		String[] subjectNos = new String[]{"418","442","460","464"};
		String finaDataSql = this.getFinaDataSql(xdCustNo, currDate, oneYearDate, twoYearDate, subjectNos);
		return findCustOneListBySql(BigDecimal.class, finaDataSql, null);
	}
    
	@Override
	public List<BigDecimal> getManuscriptFinaData(String xdCustNo, String currDate, String oneYearDate,
			String twoYearDate) {
		//以下指标分别对应  销售收入 净利润   现金流指标  比率指标
		String[] subjectNos = new String[]{"301","333","418","442","460","464","600","612","642","650","652","915"};
		String finaDataSql = this.getFinaDataSql(xdCustNo, currDate, oneYearDate, twoYearDate, subjectNos);
		return findCustOneListBySql(BigDecimal.class, finaDataSql, null);
	}
	
	/**
	 * 获取财报中在某个值  可以通过subjectno获取
	 * @param subjectNos
	 * @return
	 */
	private String getFinaDataSql(String custNo, String currDate, String oneYearDate, String twoYearDate, String[] subjectNos){
		StringBuilder subStr = new StringBuilder(); 
		for(String str : subjectNos){
			subStr.append(",'").append(str).append("'");
		}
		String subSql = subStr.substring(1);
		StringBuilder sql = new StringBuilder("");
		sql.append("select C.COL2VALUE from ( SELECT CF.*, ")
			.append("row_number() over(partition by CF.CUSTOMERID,CF.REPORTDATE order by CF.REPORTSTATUS desc) as rn  ")
			.append("FROM CMIS.CUSTOMER_FSRECORD CF) A ")
			.append("join CMIS.REPORT_RECORD B ON A.RECORDNO=B.OBJECTNO ")
			.append("JOIN CMIS.REPORT_DATA C ON B.REPORTNO=C.REPORTNO ")
			.append("WHERE A.CUSTOMERID='"+custNo+"' AND ")
			.append("(A.REPORTDATE='"+currDate+"' or A.REPORTDATE='"+oneYearDate+"' or A.REPORTDATE='"+twoYearDate+"') ")
			.append("AND C.ROWSUBJECT IN  ("+subSql+") ")
			.append("AND A.RN=1 AND B.MODELNO != '0030' AND B.MODELNO != '0130' ORDER BY A.REPORTDATE desc,C.ROWSUBJECT ");
		//0030 0130是手工录入的现金流量表   --> 直接排除在外
		return sql.toString();
	}

	
	
	//资产总计 165		负债合计 246	营业收入 301 	利润总额  329
	@Override
	public List<BigDecimal> getGuarFinaData(String xdCustNo, String currDate, String oneYearDate, String twoYearDate) {
		String[] subjectNos = new String[]{"165","246","301","329"};
		String finaDataSql = this.getFinaDataSql(xdCustNo, currDate, oneYearDate, twoYearDate, subjectNos);
		return findCustOneListBySql(BigDecimal.class, finaDataSql, null);
	}
	
	@Override
	public GuarBaseDto getGuarCustInfo(String xdCustNo) throws Exception {
		StringBuilder sql = new StringBuilder();
		
		sql.append("select B.CUSTOMERTYPE custType,A.CUSTOMERID custNo,A.GUARANTORID guarCustNo, ")
		.append("A.GUARANTORNAME guarCustName from CMIS.GUARANTY_CONTRACT A ")
		.append("JOIN CMIS.CUSTOMER_INFO B ON A.CUSTOMERID=B.CUSTOMERID ")
		.append("WHERE A.GUARANTYTYPE LIKE '01%'  and A.CUSTOMERID=:xdCustNo ")
		.append("order by  A.SERIALNO DESC FETCH FIRST 1 ROWS ONLY ");
		Map<String, Object> params = new HashMap<>();
		params.put("xdCustNo", xdCustNo);
		List<GuarBaseDto> list = findCustListBySql(GuarBaseDto.class, sql, params);
		if(CollectionUtils.isEmpty(list))
			return null;
		return list.get(0);
	}

	@Override
	public List<CmisContractDto> getContractInfo(String xdCustNo) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT BC.*,F.CODE_VALUE_REFE contracttype,D.GUARANTYTYPE guarType,D.ENDDATE enddate,D.GUARANTYVALUE guarantyValue, ")
		.append("E.GUARANTYTYPE guarantyType, E.GUARANTYNAME guarantyName,E.GUARANTYREGORG guarantyregOrg,E.CONFIRMVALUE confirmValue,E.GUARANTYRATE guarantyRate ")
		.append("FROM ( ")
		.append("select A.SERIALNO serialNo,A.RELATIVESERIALNO relativeSerialno,B.PURPOSE purpose,B.BUSINESSSUM businesssum, ")
		.append("FCV.CODE_VALUE_REFE classifyResult, A.CLASSIFYDATE classifyDate, ")
		.append("B.PAYSOURCE paysource,B.TERMMONTH termmonth,B.MATURITY maturity,B.CUSTOMERNAME,B.CUSTOMERID,A.CREDITCONDITION creditcondition ")
		.append("from cmis.BUSINESS_CONTRACT zz ")
		.append("join  CMIS.lineapply_relative ll on zz.SERIALNO=ll.objectno ")
		.append("join CMIS.BUSINESS_CONTRACT A on ll.SERIALNO=A.SERIALNO ")
		.append("JOIN cmis.BUSINESS_APPLY B ON A.RELATIVESERIALNO=B.SERIALNO ")
		.append("left join CMIS.F_COM_CODE_VALUE FCV ON FCV.CODE_VAL=A.CLASSIFYRESULT ")
		.append("where A.CUSTOMERID =:xdCustNo  AND FCV.CODE_NO='JB00029' AND ll.flag='01' and ll.effectflag='01' ")
		.append("order by  A.OCCURDATE DESC FETCH FIRST 1 ROWS ONLY) AS BC ")
		.append("LEFT JOIN CMIS.GUARANTY_RELATIVE C ON BC.SERIALNO=C.OBJECTNO ")
		.append("LEFT JOIN CMIS.GUARANTY_CONTRACT D ON C.CONTRACTNO=D.SERIALNO ")
		.append("LEFT JOIN CMIS.GUARANTY_INFO E ON C.GUARANTYID=E.GUARANTYID ")
		.append("LEFT JOIN CMIS.F_COM_CODE_VALUE F ON D.CONTRACTTYPE=F.CODE_VAL AND F.CODE_NO='ODS0073' ");
		Map<String, Object> params = new HashMap<>();
		params.put("xdCustNo", xdCustNo);

		return findCustListBySql(CmisContractDto.class, sql, params);
		
	}	

	@Override
	public List<CmisBillDto> getBillInfo(String serialNo) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("select BUSINESSSUM businesssum,PUTOUTDATE putoutDate from CMIS.BUSINESS_DUEBILL ")
		.append("WHERE RELATIVESERIALNO2=:serialNo ")
		.append("order by PUTOUTDATE desc fetch first 2 rows only ");
		Map<String, Object> params = new HashMap<>();
		params.put("serialNo", serialNo);
		
		return findCustListBySql(CmisBillDto.class, sql, params);
	}


	@Override
	public List<CmisContractDto> getContractList(String xdCustNo) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT ZZ.*,F.CODE_VALUE_REFE contracttype,D.GUARANTYTYPE guarType,D.ENDDATE enddate, ")
		.append("D.GUARANTYVALUE guarantyValue,D.GUARANTORNAME guarantorName,E.GUARANTYTYPE guarantyType,  ")
		.append("E.GUARANTYNAME guarantyName,E.GUARANTYREGORG guarantyregOrg,E.CONFIRMVALUE confirmValue,E.GUARANTYRATE guarantyRate ")
		.append("FROM ( select A.SERIALNO serialNo,A.OCCURDATE occurdate,A.RELATIVESERIALNO relativeSerialno,A.BALANCE balance, ")
		.append("B.PURPOSE purpose,A.PURPOSEADD purposeadd,B.BUSINESSSUM businesssum,A.OVERDUEBALANCE overduebalance, ")
		.append("A.INTERESTBALANCE1  interestbalance1, A.INTERESTBALANCE2 interestbalance2,FCV.CODE_VALUE_REFE classifyResult, ")
		.append("A.CLASSIFYDATE classifyDate, A.VOUCHTYPE vouchtype,A.BUSINESSSUM contractSum,B.PAYSOURCE paysource, ")
		.append("B.TERMMONTH termmonth,B.MATURITY maturity,B.CUSTOMERNAME,B.CUSTOMERID,A.CREDITCONDITION creditcondition ")
		.append("from cmis.BUSINESS_CONTRACT A ")
		.append("JOIN cmis.BUSINESS_APPLY B ON A.RELATIVESERIALNO=B.SERIALNO ")
		.append("left join CMIS.F_COM_CODE_VALUE FCV ON FCV.CODE_VAL=A.CLASSIFYRESULT ")
		.append("where A.CUSTOMERID =:xdCustNo  AND FCV.CODE_NO='JB00029' ) AS ZZ ")
		.append("join CMIS.lineapply_relative ll on ZZ.SERIALNO=ll.objectno ")
		.append("join CMIS.BUSINESS_CONTRACT BC on ll.SERIALNO=BC.SERIALNO ")
		.append("LEFT JOIN CMIS.GUARANTY_RELATIVE C ON BC.SERIALNO=C.OBJECTNO ")
		.append("LEFT JOIN CMIS.GUARANTY_CONTRACT D ON C.CONTRACTNO=D.SERIALNO ")
		.append("LEFT JOIN CMIS.GUARANTY_INFO E ON C.GUARANTYID=E.GUARANTYID ")
		.append("LEFT JOIN CMIS.F_COM_CODE_VALUE F ON D.CONTRACTTYPE=F.CODE_VAL ")
		.append("WHERE F.CODE_NO='ODS0073' AND ll.flag='01' and ll.effectflag='01'");
		Map<String, Object> params = new HashMap<>();
		params.put("xdCustNo", xdCustNo);

		return findCustListBySql(CmisContractDto.class, sql, params);
	}

	@Override
	public List<GuarantInfoDto> getCustGuarantInfo(String xdCustNo) {
		StringBuilder sql = new StringBuilder();
		sql.append("select GUARANTYTYPE guarantytype,GUARANTYSTATUS guarantystatus,OWNERID ownerid, ")
		.append("EVALMETHOD evalmethod,CONFIRMVALUE confirmvalue,GUARANTYREGORG guarantyregorg  ")
		.append("from CMIS.GUARANTY_INFO A ")
		.append("where A.OWNERID =:xdCustNo ");
		Map<String, Object> params = new HashMap<>();
		params.put("xdCustNo", xdCustNo);

		return findCustListBySql(GuarantInfoDto.class, sql, params);
	}

	@Override
	public CmisContractDto getLastOneContractInfo(String xdCustNo) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("select GUARANTYTYPE guarantytype,GUARANTYSTATUS guarantystatus,OWNERID ownerid, ")
		.append("EVALMETHOD evalmethod,CONFIRMVALUE confirmvalue,GUARANTYREGORG guarantyregorg  ")
		.append("from CMIS.GUARANTY_INFO A ")
		.append("where A.OWNERID =:xdCustNo ");
		Map<String, Object> params = new HashMap<>();
		params.put("xdCustNo", xdCustNo);

		List<CmisContractDto> list = findCustListBySql(CmisContractDto.class, sql, params);
		if(CollectionUtils.isEmpty(list)){
			return null;
		}
		return list.get(0);
	}	

	@Override
	public List<BigDecimal> getSmallFinaData(String xdCustNo, String currDate, String oneYearDate,
			String twoYearDate) {
		//以下指标分别对应  资产总计 负债合计  资产负债率  营业利润 净利润 销售利润率  净现金流量
		String[] subjectNos = new String[]{"165","246","612","329","333","642","418"};
		String finaDataSql = this.getFinaDataSql(xdCustNo, currDate, oneYearDate, twoYearDate, subjectNos);
		return findCustOneListBySql(BigDecimal.class, finaDataSql, null);
	}
	
	/**
	 * 获取担保企业信息
	 * */
	@Override
	public List<GuaCompany> getGuaCompany(String xdCustNo) {
		
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Calendar calender = Calendar.getInstance();
		calender.add(Calendar.MONTH, -1);//一个月前
		Date dateNow = calender.getTime();
		String date = format.format(dateNow);  
		
		//date = "0";
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT B.GUARANTORNAME AS companyName,B.GUARANTYVALUE AS guaranteeAmt,B.GUARANTYTYPE ")
			.append("AS guaranteeWay,E.SETUPDATE AS foundDate,E.REGISTERCAPITAL AS regCapital, ")
			.append("E.PAICLUPCAPITAL AS realCapital,E.FICTITIOUSPERSON AS legalPerson,E.REGISTERADD AS address, ")
			.append("E.MOSTBUSINESS AS mainBusiness,E.MANAGEINFO AS runInfo ")
			.append("FROM  cmis.GUARANTY_RELATIVE A  ")
			.append("JOIN cmis.GUARANTY_CONTRACT B ON B.SERIALNO=A.CONTRACTNO   ")
			.append("JOIN (SELECT * FROM cmis.BUSINESS_CONTRACT WHERE  CUSTOMERID= ?0 ")
			.append("AND OCCURDATE>= ?1 order by  OCCURDATE DESC FETCH FIRST 1 ROWS ONLY) AS C ON C.SERIALNO=A.OBJECTNO ")
			.append("JOIN cmis.ENT_INFO E ON B.GUARANTORID=E.CUSTOMERID ");
		
		return findCustListBySql(GuaCompany.class, sql, xdCustNo, date);
		
	}
	
	
	/**
	 * 获取担保企业三年财务信息
	 * */
	@Override
	public List<CompFinanceTransfer> getCompFinnaceTransfer(String xdCustNo) {
		StringBuilder maxSql = new StringBuilder();
		maxSql.append("select max(REPORTDATE) from cmis.CUSTOMER_FSRECORD where CUSTOMERID=?0");
		String maxDate = findCustOneBySql(String.class, maxSql, xdCustNo);
		if(StringUtils.isEmpty(maxDate)){
			return null;
		}
		
		int currYear = Integer.parseInt(maxDate.substring(0, 4));
		String oneYear = (currYear-1) + "12";//上年末
		String twoYear = (currYear-2) + "12";//上上年末
		
		
		StringBuilder sql = new StringBuilder();
		sql.append("select C.COL2VALUE as value, C.ROWSUBJECT as rowSubject,A.REPORTDATE as date ")
			.append("FROM cmis.CUSTOMER_FSRECORD A  ")
			.append("join cmis.REPORT_RECORD B ON A.RECORDNO=B.OBJECTNO  ")
			.append("JOIN cmis.REPORT_DATA C ON B.REPORTNO=C.REPORTNO ")
			.append("WHERE A.CUSTOMERID= ?0 ")
			.append("AND C.ROWSUBJECT IN ('165','246','266','301','329','612') ")
			.append("AND A.REPORTDATE IN ('")
			.append(maxDate).append("','")
			.append(oneYear).append("','")
			.append(twoYear).append("')")
			;
		//资产总计 165		负债合计 246	所有者权益（净资产） 266	营业收入 301 	利润总额  329      资产负债率 612
		//新旧报表准则对应的字段subjectno针对以上大科目都是相同的
		return findCustListBySql(CompFinanceTransfer.class, sql, xdCustNo);
	}
	
	
	/**
	 * 获取自然人担保的个人信息
	 * 
	 * @param xdCustNo
	 * @return
	 */
	@Override
	public List<GuaPerson> getGuaPerson(String xdCustNo) {
		
		Map<String, Object> params = new HashMap<>();
		params.put("customerid", xdCustNo);
		
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Calendar calender = Calendar.getInstance();
		calender.add(Calendar.MONTH, -1);//一个月前
		Date dateNow = calender.getTime();
		String date = format.format(dateNow); 
		params.put("date", date);
		//date = "19950101";
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT B.GUARANTORNAME AS name,F.SEX AS sex,B.GUARANTYVALUE AS guaranteeAmt,B.GUARANTYTYPE ")
			.append("AS guaranteeWay,F.CERTID18 AS certCode,F.NATIVEADD AS address, ")
			.append("F.WORKCORP AS workUnitPost,F.MOBILETELEPHONE AS mobilePhone,F.YEARINCOME AS lastYearIncome, ")
			.append("F.TOTALASSETS AS assetsSum,F.FAMILYPUREASSET AS netAssets ")
			.append("FROM  cmis.GUARANTY_RELATIVE A  ")
			.append("JOIN cmis.GUARANTY_CONTRACT B ON B.SERIALNO=A.CONTRACTNO  ")
			.append("JOIN (SELECT * FROM cmis.BUSINESS_CONTRACT WHERE  CUSTOMERID= :customerid ")
			.append("AND OCCURDATE>= :date order by  OCCURDATE DESC FETCH FIRST 1 ROWS ONLY) AS C ON C.SERIALNO=A.OBJECTNO  ")
			.append("JOIN (SELECT * FROM cmis.IND_INFO ")
			.append( "WHERE CUSTOMERID IN (SELECT CUSTOMERID FROM cmis.CUSTOMER_INFO WHERE CUSTOMERTYPE LIKE '03%')) AS F ")
			.append( "ON B.GUARANTORID=F.CUSTOMERID ");
		
		return findCustListBySql(GuaPerson.class, sql, params);
		
	}
	
	/**
	 * 获取房地产抵押信息
	 * 
	 * @param xdCustNo
	 * @return
	 */
	@Override
	public List<GuaEstate> getGuaEstate(String xdCustNo) {
		
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Calendar calender = Calendar.getInstance();
		calender.add(Calendar.MONTH, -1);//一个月前
		Date dateNow = calender.getTime();
		String date = format.format(dateNow);  
		//date = "0";
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT B.GUARANTORNAME AS mortgagor,D.GUARANTYTYPE AS mortgagorType,D.GUARANTYLOCATION AS location,")
			.append("D.GUARANTYRIGHTID AS certificateNo,D.BEGINDATE AS certificateDate,D.CONFIRMVALUE AS estimatedValue, ")
			.append("D.EVALORGNAME AS evaluationAgency,D.RENTORNOT AS useOrRent,D.ISASSURE AS isHandleInsurance, ")
			.append("D.HOSEVALNETVALUE AS loanAmount,D.GUARANTYRATE AS discountRate ")
			.append("FROM  cmis.GUARANTY_RELATIVE A ")
			.append("JOIN cmis.GUARANTY_CONTRACT B ON B.SERIALNO=A.CONTRACTNO ")
			.append("JOIN (SELECT * FROM cmis.BUSINESS_CONTRACT WHERE  CUSTOMERID= ?0 ")
			.append("AND OCCURDATE>= ?1 order by  OCCURDATE DESC FETCH FIRST 1 ROWS ONLY) AS C ON C.SERIALNO=A.OBJECTNO ")
			.append("JOIN (SELECT * FROM cmis.GUARANTY_INFO ")
			.append( "WHERE GUARANTYTYPE LIKE '0202%' OR GUARANTYTYPE LIKE '0201%')AS D ")
			.append( "ON D.GUARANTYID=A.GUARANTYID ");
		//0201开头和0202开头  抵押-房地产
		return findCustListBySql(GuaEstate.class, sql, xdCustNo, date);
		
	}
	
	/**
	 * 获取应收账款质押信息
	 * 
	 * @param xdCustNo
	 * @return
	 */
	@Override
	public List<GuaAccounts> getGuaAccounts(String xdCustNo) {
		
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Calendar calender = Calendar.getInstance();
		calender.add(Calendar.MONTH, -1);//一个月前
		Date dateNow = calender.getTime();
		String date = format.format(dateNow);  
		//date = "0";
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT D.OWNERNAME AS customerName,D.CONFIRMVALUE AS amount,D.COMPLETEYEAR AS accountsAge,D.REMARK AS remark ")
			.append("FROM  cmis.GUARANTY_RELATIVE A ")
			.append("JOIN cmis.GUARANTY_CONTRACT B ON B.SERIALNO=A.CONTRACTNO ")
			.append("JOIN (SELECT * FROM cmis.BUSINESS_CONTRACT WHERE  CUSTOMERID= ?0 ")
			.append("AND OCCURDATE>= ?1 order by  OCCURDATE DESC FETCH FIRST 1 ROWS ONLY) AS C ON C.SERIALNO=A.OBJECTNO ")
			.append("JOIN (SELECT * FROM cmis.GUARANTY_INFO ")
			.append( "WHERE GUARANTYTYPE ='040300')AS D ")
			.append( "ON D.GUARANTYID=A.GUARANTYID ");
		
		//040300 质押-应收账款
		return findCustListBySql(GuaAccounts.class, sql, xdCustNo, date);
		
	}
	
	/**
	 * 获取动产抵押信息
	 * 
	 * @param xdCustNo
	 * @return
	 */
	@Override
	public List<GuaChattel> getGuaChattel(String xdCustNo) {
		
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Calendar calender = Calendar.getInstance();
		calender.add(Calendar.MONTH, -1);//一个月前
		Date dateNow = calender.getTime();
		String date = format.format(dateNow);  
		//date = "0";
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT D.GUARANTYNAME AS chattelName,D.GUARANTYAMOUNT AS number,D.OWNERNAME AS ownership,")
			.append("D.GUARANTYPRICE AS originalValue,D.CONFIRMVALUE AS estimatedValue,D.EVALORGNAME AS evaluationAgency, ")
			.append("D.RENTORNOT AS useOrRent,D.ISASSURE AS isHandleInsurance, ")
			.append("D.GUARANTYLOCATION AS storageLocation ")
			.append("FROM  cmis.GUARANTY_RELATIVE A ")
			.append("JOIN cmis.GUARANTY_CONTRACT B ON B.SERIALNO=A.CONTRACTNO ")
			.append("JOIN (SELECT * FROM cmis.BUSINESS_CONTRACT WHERE  CUSTOMERID= ?0 ")
			.append("AND OCCURDATE>= ?1 order by  OCCURDATE DESC FETCH FIRST 1 ROWS ONLY) AS C ON C.SERIALNO=A.OBJECTNO ")
			.append("JOIN (SELECT * FROM cmis.GUARANTY_INFO ")
			.append( "WHERE GUARANTYTYPE  IN ('020500','020600','020720','020740','020740010','020740020','020800','020850','020850','020850010','020850020'))AS D ")
			.append( "ON D.GUARANTYID=A.GUARANTYID ");
		//02开头为抵押   020500  汽车 ,020600 船舶,020720 其他交通工具,020740 抵押-设备抵押，
		//020740010 通用设备,020740020 专用设备,020800 存货，020850  林木资产,020850 抵押-其他抵押
		//020850010 其他资源性资产,020850020 其他
		return findCustListBySql(GuaChattel.class, sql, xdCustNo, date);
		
	}
	
	
	
	/**
	 * 获取应收账款质押信息
	 * 
	 * @param xdCustNo
	 * @return
	 */
	@Override
	public List<GuaDepositReceipt> getGuaDepositReceipt(String xdCustNo) {
		
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		Calendar calender = Calendar.getInstance();
		calender.add(Calendar.MONTH, -1);//一个月前
		Date dateNow = calender.getTime();
		String date = format.format(dateNow);  
		//date = "0";
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT D.OWNERNAME AS owner,D.CONFIRMVALUE AS amount,D.GUARANTYLOCATION AS bank,D.GUARANTYRATE AS pledgeRate,D.REMARK AS remark ")
			.append("FROM cmis.GUARANTY_RELATIVE A ")
			.append("JOIN cmis.GUARANTY_CONTRACT B ON B.SERIALNO=A.CONTRACTNO ")
			.append("JOIN (SELECT * FROM cmis.BUSINESS_CONTRACT WHERE  CUSTOMERID= ?0 ")
			.append("AND OCCURDATE>= ?1 order by  OCCURDATE DESC FETCH FIRST 1 ROWS ONLY) AS C ON C.SERIALNO=A.OBJECTNO ")
			.append("JOIN (SELECT * FROM cmis.GUARANTY_INFO ")
			.append( "WHERE GUARANTYTYPE ='040100' OR GUARANTYTYPE='040150') AS D ")
			.append( "ON D.GUARANTYID=A.GUARANTYID ");
		
		//040100 质押-本行存单   040150 质押-他行存单
		return findCustListBySql(GuaDepositReceipt.class, sql, xdCustNo, date);
		
	}
    
}
