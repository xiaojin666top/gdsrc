package com.beawan.dtmp.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.dtmp.dao.EntInfoDao;
import com.beawan.dtmp.entity.EntInfo;

/**
 * @author yzj
 */
@Repository("entInfoDao")
public class EntInfoDaoImpl extends BaseDaoImpl<EntInfo> implements EntInfoDao{

	@Override
	public EntInfo getInduByMfCustomerId(String mfCustomerId) throws Exception {
		StringBuilder sql = new StringBuilder();
        Map<String, Object> params = new HashMap<>();
        sql.append(" select  a.CUSTOMERID as customerid,a.INDUSTRYTYPE as industryType from CMIS.ENT_INFO a ")
        .append(" left join  CMIS.CUSTOMER_INFO b on  a.CUSTOMERID = b.CUSTOMERID ")
        .append(" where b.MFCUSTOMERID =:MfCustomerId ");
        params.put("MfCustomerId", mfCustomerId);
        List<EntInfo> entInfos = this.findCustListBySql(EntInfo.class, sql.toString(), params);
        if(!CollectionUtils.isEmpty(entInfos)){
        	return entInfos.get(0);
        }
        return null;
	}

	@Override
	public EntInfo getAfInfoByMfCustomerId(String mfCustomerId) throws Exception {
		StringBuilder sql = new StringBuilder();
        Map<String, Object> params = new HashMap<>();
        sql.append(" select  a.CUSTOMERID as customerid,a.FICTITIOUSPERSON as ficitiousPerson,a.PAICLUPCAPITAL as paiclupCapital from CMIS.ENT_INFO a ")
        .append(" left join  CMIS.CUSTOMER_INFO b on  a.CUSTOMERID = b.CUSTOMERID ")
        .append(" where b.MFCUSTOMERID =:MfCustomerId ");
        params.put("MfCustomerId", mfCustomerId);
        List<EntInfo> entInfos = this.findCustListBySql(EntInfo.class, sql.toString(), params);
        if(!CollectionUtils.isEmpty(entInfos)){
        	return entInfos.get(0);
        }
        return null;
	}
	
}
