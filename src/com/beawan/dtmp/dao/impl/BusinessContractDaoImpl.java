package com.beawan.dtmp.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.dtmp.dao.BusinessContractDao;
import com.beawan.dtmp.dto.ContractBillDto;
import com.beawan.dtmp.entity.BusinessContract;
import com.beawan.survey.custInfo.bean.CompConnetCm;
import com.beawan.survey.custInfo.bean.CompFinancingExtGuara;

/**
 * @author yzj
 */
@Repository("businessContractDao")
public class BusinessContractDaoImpl extends BaseDaoImpl<BusinessContract> implements BusinessContractDao{

	@Override
	public List<BusinessContract> getMatuityConListByTerm(String monthStart, String monthEnd, String xdCustNo) {
		StringBuilder sql = new StringBuilder();
		Map<String, Object> params = new HashMap<>();
		sql.append(" SELECT A.SERIALNO,A.CUSTOMERID,A.CUSTOMERNAME,A.OCCURDATE,A.OCCURTYPE,A.BUSINESSSUM,")
			.append(" A.PAYCYC,A.PUTOUTDATE,A.MATURITY,A.PAYDEADLINE,B.INDUSTRYTYPE")
			.append(" FROM CMIS.BUSINESS_CONTRACT A ")
			.append(" join CMIS.ENT_INFO B ON A.CUSTOMERID=B.CUSTOMERID")
			.append(" WHERE 1=1  ");
		if(!StringUtils.isEmpty(xdCustNo)){
			sql.append("  and A.CUSTOMERID =:xdCustNo ");
			params.put("xdCustNo", xdCustNo);
		}
		if(!StringUtils.isEmpty(monthStart)){
			sql.append("  and A.MATURITY >=:monthStart ");
			params.put("monthStart", monthStart);
		}
		if(!StringUtils.isEmpty(monthEnd)){
			sql.append("  AND A.MATURITY <=:monthEnd ");
			params.put("monthEnd", monthEnd);
		}
		sql.append(" AND A.FLAG5='1000' AND A.BUSINESSTYPE LIKE '3%' AND A.BUSINESSSUM>0 ");
//		//FLAG5 1000代表 --->  审批通过的申请
		return findCustListBySql(BusinessContract.class, sql.toString(), params);
	}
	

	@Override
	public List<CompFinancingExtGuara> getGuarantyContract(String xdCustNo, String endTime) {
		Map<String, Object> params = new HashMap<>();
		StringBuilder sql = new StringBuilder();
		sql.append("select  A.SERIALNO,A.GUARANTYTYPE antiGuaranteeWay,B.ENTERPRISENAME guaranteeName,'如皋农商行' financialOrg,E.TYPENAME businessType, ")
			.append("A.GUARANTYVALUE guaranteeAmt,A.GUARANTYBALANCEVALUE guaranteeBal,D.CLASSIFYRESULT fiveClass ")
			.append("from cmis.GUARANTY_CONTRACT A ")
			.append("JOIN CMIS.ENT_INFO B ON A.CUSTOMERID=B.CUSTOMERID ")
			.append("join CMIS.GUARANTY_RELATIVE C ON A.SERIALNO=C.CONTRACTNO ")
			.append("JOIN CMIS.BUSINESS_CONTRACT D ON C.OBJECTNO=D.SERIALNO ")
			.append("JOIN CMIS.BUSINESS_TYPE E ON D.BUSINESSTYPE=E.TYPENO ")
			.append("where A.CONTRACTSTATUS = :contractStatus ");
		params.put("contractStatus", "020");
		if(!StringUtils.isEmpty(xdCustNo)){
			sql.append(" AND A.GUARANTORID = :xdCustNo ");
			params.put("xdCustNo", xdCustNo);
		}
		if(!StringUtils.isEmpty(endTime)){
			sql.append(" AND A.ENDDATE >=:endTime ");
			params.put("endTime", endTime);
		}
		sql.append(" GROUP BY A.SERIALNO,A.GUARANTYTYPE,B.ENTERPRISENAME,E.TYPENAME,A.GUARANTYVALUE,")
			.append("A.GUARANTYBALANCEVALUE,D.CLASSIFYRESULT");
		return findCustListBySql(CompFinancingExtGuara.class, sql.toString(), params);
	}

	@Override
	public List<CompConnetCm> getCustomerRelative(String xdCustNo) {
		Map<String, Object> params = new HashMap<>();
		StringBuilder sql = new StringBuilder();
		/*sql.append("select D.ENTERPRISENAME companyName,D.SETUPDATE foundDate,D.FICTITIOUSPERSON legalPerson, ")
			.append("D.REGISTERCAPITAL registerCapital,D.PAICLUPCAPITAL realCapital,")
			.append("A.RELATIONSHIP relationship,C.CODE_VALUE_REFE,D.MOSTBUSINESS mainBusiness,D.LIMIT ")
			.append("from cmis.CUSTOMER_RELATIVE A ")
			.append("JOIN CMIS.ENT_INFO B ON A.CUSTOMERID=B.CUSTOMERID ")
			.append("JOIN CMIS.F_COM_CODE_VALUE C ON A.RELATIONSHIP=C.CODE_VAL ")
			.append("JOIN CMIS.ENT_INFO D ON A.RELATIVEID=D.CUSTOMERID ")
			.append("where (EFFECTFLAG!='N' OR EFFECTFLAG IS NULL) AND C.CODE_NO='GB00016' ");
		*/
		
		sql.append("select D.ENTERPRISENAME companyName,D.SETUPDATE foundDate,D.FICTITIOUSPERSON legalPerson, ")
		.append("D.REGISTERCAPITAL registerCapital,D.PAICLUPCAPITAL realCapital,")
		.append("A.RELATIONSHIP relationship,D.MOSTBUSINESS mainBusiness,D.LIMIT ")
		.append("from cmis.GROUP_RELATIVE A ")
		.append("JOIN CMIS.ENT_INFO B ON A.CUSTOMERID=B.CUSTOMERID ")
		.append("JOIN CMIS.ENT_INFO D ON A.RELATIVEID=D.CUSTOMERID ");
		sql.append(" AND A.CUSTOMERID=:xdCustNo ");
		params.put("xdCustNo", xdCustNo);
		
		return findCustListBySql(CompConnetCm.class, sql.toString(), params);
	}


	@Override
	public List<ContractBillDto> getDuebill(String xdCustNo) {
		Map<String, Object> params = new HashMap<>();
		StringBuilder sql = new StringBuilder();
		sql.append("select D.SERIALNO,A.OCCURTYPE,A.CUSTOMERID,D.RELATIVESERIALNO2,D.BUSINESSSUM,D.PUTOUTDATE ")
			.append("from  CMIS.BUSINESS_CONTRACT A ")
			.append("JOIN CMIS.LINEAPPLY_RELATIVE B ON A.SERIALNO=B.OBJECTNO AND B.FLAG='01' AND B.EFFECTFLAG='01' ")
			.append("JOIN CMIS.BUSINESS_CONTRACT C ON C.SERIALNO=B.SERIALNO ")
			.append("JOIN CMIS.BUSINESS_DUEBILL D ON C.SERIALNO=D.RELATIVESERIALNO2 ")
			.append("WHERE (A.OCCURTYPE = '090' OR A.OCCURTYPE = '110' ) ")
			.append("AND A.CUSTOMERID=:xdCustNo ")
			.append("order by D.PUTOUTDATE desc ");
		params.put("xdCustNo", xdCustNo);
		return findCustListBySql(ContractBillDto.class, sql.toString(), params);
	}

}
