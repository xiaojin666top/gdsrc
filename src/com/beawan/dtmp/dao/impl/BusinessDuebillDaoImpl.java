package com.beawan.dtmp.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.dtmp.dao.BusinessDuebillDao;
import com.beawan.dtmp.entity.BusinessDuebill;
import org.springframework.stereotype.Repository;
import com.beawan.dtmp.entity.BusinessDuebill;

/**
 * @author yzj
 */
@Repository("businessDuebillDao")
public class BusinessDuebillDaoImpl extends BaseDaoImpl<BusinessDuebill> implements BusinessDuebillDao{

}
