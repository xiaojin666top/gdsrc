package com.beawan.dtmp.dao.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.dtmp.dao.CustomerInfoDao;
import com.beawan.dtmp.entity.CustomerInfo;
import com.beawan.dtmp.entity.FComCodeValue;
import com.beawan.survey.custInfo.bean.CompWmWarnSignal;
import com.platform.util.DateUtil;

/**
 * @author yzj
 */
@Repository("customerInfoDao")
public class CustomerInfoDaoImpl extends BaseDaoImpl<CustomerInfo> implements CustomerInfoDao{

	@Override
	public String getXdCustNo(String mfCustomerId) throws Exception {
		String sql = "select CUSTOMERID from CMIS.CUSTOMER_INFO where MFCUSTOMERID=?1";
		List<String> list = findCustListBySql(String.class, sql, mfCustomerId);
		if(CollectionUtils.isEmpty(list))
			return null;
		return list.get(0);
	}

	@Override
	public List<CompWmWarnSignal> getWmWarningSignal(String xdCustNo) throws Exception {
		StringBuilder sql = new StringBuilder();
		Map<String, Object> params = new HashMap<>();
		Date startDate = DateUtil.addYear(new Date(), -2);
		String format = DateUtil.format(startDate, "yyyyMMdd");
		params.put("startDate", format);
		params.put("xdCustNo", xdCustNo);
		sql.append("select inputTime,alarminfoDesc,case warnLevel when 1 then '黄色' ")
			.append("when 2 then '橙色' when 3 then '红色' else '无' end warnLevel, falseCause from ( ")
			.append("select INPUTDATE inputTime,ALARMINFODESC alarminfoDesc,ALARMLEVEL warnLevel,'' falseCause  from KHFXYJ.ALARM_INFO ")
			.append("where INPUTDATE>=:startDate AND CUSTOMERID=:xdCustNo union ")
			.append("select WARNING_TIME inputTime,SIGNAL_NAME alarminfoDesc,WARNING_LEVEL warnLevel,FALSE_CAUSE falseCause ")
			.append("from KHFXYJ2.WM_WARNING_SIGNAL_TB ")
			.append("where WARNING_TIME>=:startDate AND OBJECT_CODE=:xdCustNo ")
			.append("and (ISRELIEVE!='是' or ISRELIEVE is null) ")
			.append(") A ");
		return findCustListBySql(CompWmWarnSignal.class, sql, params);
	}

	@Override
	public List<FComCodeValue> getFComCodeValue(String codeNo, String codeVal) {
		StringBuilder sql = new StringBuilder();
		Map<String, Object> params = new HashMap<>();
		sql.append("select CODE_NO codeNo,CODE_VAL codeVal,CODE_VALUE_REFE codeValueRefe from cmis.F_COM_CODE_VALUE ")
			.append(" where 1=1");
		if(!StringUtils.isEmpty(codeNo)){
			sql.append(" AND CODE_NO=:codeNo");
			params.put("codeNo", codeNo);
		}
		if(!StringUtils.isEmpty(codeVal)){
			sql.append(" AND CODE_VAL=:codeVal");
			params.put("codeVal", codeVal);
		}
		return findCustListBySql(FComCodeValue.class, sql, params);
	}

	@Override
	public String getRealContoller(String xdCustNo) throws Exception {
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT B.CUSTOMERNAME customername FROM GDTCESYS.CUS_BASE A  ")
			.append("JOIN CMIS.customer_relative b ON A.XD_CUSTOMER_NO=B.CUSTOMERID ")
			.append("JOIN CMIS.F_COM_CODE_VALUE C ON B.RELATIONSHIP=C.CODE_VAL ")
			.append("WHERE C.CODE_NO='GB00016' AND B.RELATIONSHIP IN ('F0010','0109','5109') ")
			.append("AND (B.EFFECTFLAG!='N' OR B.EFFECTFLAG IS NULL)  ") 
			.append("AND A.CUSTOMER_NO = :xdCustNo ");
		Map<String, Object> params = new HashMap<>();
		params.put("xdCustNo", xdCustNo);
        System.out.println("CUSTOMER_NO:" + xdCustNo );
		List<CustomerInfo> custInfo = findCustListBySql(CustomerInfo.class, sql.toString(), params);
		if(CollectionUtils.isEmpty(custInfo)){
			return null;
		}
		return custInfo.get(0).getCustomername();
	}

}
