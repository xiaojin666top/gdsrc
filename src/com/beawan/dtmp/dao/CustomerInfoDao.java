package com.beawan.dtmp.dao;

import java.util.List;

import com.beawan.core.BaseDao;
import com.beawan.dtmp.entity.CustomerInfo;
import com.beawan.dtmp.entity.FComCodeValue;
import com.beawan.survey.custInfo.bean.CompWmWarnSignal;

/**
 * @author yzj
 */
public interface CustomerInfoDao extends BaseDao<CustomerInfo> {
	
	/**
	 * 根据核心客户好获取信贷客户号
	 * @param mfCustomerId
	 * @return
	 * @throws Exception
	 */
	public String getXdCustNo(String mfCustomerId) throws Exception;

	/**
	 * 根据信贷客户号获取企业
	 * @param xdCustNo
	 * @return
	 */
	public List<CompWmWarnSignal> getWmWarningSignal(String xdCustNo) throws Exception;
	
	/**
	 * ODS字典大类
	 * 大类中的具体编号
	 * @param codeNo
	 * @param codeVal
	 * @return
	 */
	public List<FComCodeValue> getFComCodeValue(String codeNo, String codeVal);
	
	/**
	 * 根据企业信贷客户号，获取企业实际控制人信息
	 * @param xdCustNo
	 * @return
	 * @throws Exception
	 */
	public String getRealContoller(String xdCustNo) throws Exception;
}
