package com.beawan.dtmp.dao;

import java.util.List;

import com.beawan.core.BaseDao;
import com.beawan.dtmp.dto.DepositByOrgDto;
import com.beawan.dtmp.dto.StyBalDto;
import com.beawan.dtmp.entity.Tdacnacn;

/**
 * @author yzj
 */
public interface TdacnacnDao extends BaseDao<Tdacnacn> {
	
	/**
	 * 存款总额(定期+活期)
	 * @param depositType 存款类型：对公、储蓄、保证金
	 * @return
	 * @throws Exception
	 */
    Double getTotalDeposit(String depositType) throws Exception;
    
    /**
     * 分支行统计对公存款(定期、活期)
     * @return
     * @throws Exception
     */
    List<DepositByOrgDto> getDepositByOrg() throws Exception;
    
    /**
     * 获取 定期存款(亿元)
     * @param endDate "yyyyMMdd" 每月底
     * @return
     * @throws Exception
     */
    Double getTdDeposit(String endDate) throws Exception;
    
    /**
     * 获取 活期存款(亿元)
     * @param endDate yyyyMMdd
     * @return
     * @throws Exception
     */
    Double getSaDeposit(String endDate) throws Exception;

    /**
     * 根据客户号获取客户活期账号及余额
     * @param mfCustno
     * @return
     */
	List<StyBalDto> getSaDepositByCustNo(String mfCustno);
	/**
     * 根据客户号获取客户定期账号及余额
     * @param mfCustno
     * @return
     */
	List<StyBalDto> getTdDepositByCustNo(String mfCustno);
    
    
}
