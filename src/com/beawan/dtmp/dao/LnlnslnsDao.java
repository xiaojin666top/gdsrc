package com.beawan.dtmp.dao;

import java.math.BigDecimal;
import java.util.List;

import com.beawan.core.BaseDao;
import com.beawan.dtmp.dto.BalByRateDto;
import com.beawan.dtmp.dto.CmisBillDto;
import com.beawan.dtmp.dto.CmisContractDto;
import com.beawan.dtmp.dto.CustLoanDto;
import com.beawan.dtmp.dto.CustSpreadByBalDto;
import com.beawan.dtmp.dto.GuarBaseDto;
import com.beawan.dtmp.dto.GuarantInfoDto;
import com.beawan.dtmp.dto.StyBalDto;
import com.beawan.dtmp.entity.Lnlnslns;
import com.beawan.loanAfter.entity.FcBusiContract;
import com.beawan.survey.guarantee.entity.CompFinanceTransfer;
import com.beawan.survey.guarantee.entity.GuaAccounts;
import com.beawan.survey.guarantee.entity.GuaChattel;
import com.beawan.survey.guarantee.entity.GuaCompany;
import com.beawan.survey.guarantee.entity.GuaDepositReceipt;
import com.beawan.survey.guarantee.entity.GuaEstate;
import com.beawan.survey.guarantee.entity.GuaPerson;

/**
 * @author yzj
 */
public interface LnlnslnsDao extends BaseDao<Lnlnslns> {

    /**
     * 从下发数据的信贷系统
     * 获取客户在我行授信情况数据
     * @param mfCustomerId      核心客户号
     * @return
     * @throws Exception
     */
    public List<CustLoanDto> getCustLoan(String mfCustomerId,List<String> fiveClassList) throws Exception;
    
    /**
     * 贷款总额
     * @return
     * @throws Exception
     */
    Double getLoanBal() throws Exception;
    
    /***
     * 统计当前时点 分金额的统计贷款户数 或贷款余额的总数
     * @return
     * @param unit      查询单位，原始数据为元  默认除以100000000（亿）
     * @throws Exception
     */
    CustSpreadByBalDto getCustSpreadByBal(Integer unit) throws Exception;

    /**
     * 按担保方式统计贷款总额
     * @return
     */
    List<StyBalDto> getBalStry() throws Exception;

    /**
     * 按五级分类统计贷款总额
     * @return
     * @throws Exception
     */
	List<StyBalDto> getBalByClassifyResult() throws Exception;


    /**
     * 统计贷款利率分布情况
     * @return
     * @throws Exception
     */
	BalByRateDto getBalByRate() throws Exception;

	
	//-----------------------贷后五级分类接口数据
	/**
	 * 获取客户贷款 合同及担保物信息
	 * @param xdCustNo
	 * @return
	 */
	List<FcBusiContract> getBusiContractByCust(String xdCustNo) throws Exception;

	 /**
     * 根据信贷客户号获取企业最新一期的财报年月
     * @param xdCustNo
     * @return
     * @throws Exception
     */
	String getCustFinaDate(String xdCustNo);

	/**
     * 获取客户当期 的 资产负债、利润表的主要指标
     * @param xdCustNo
     * @return
     * @throws Exception
     */
	List<BigDecimal> getCustFinaCurrMainSubject(String xdCustNo, String date);

	 /**
     * 获取近三年比例指标数据 
     * 按照时间升序    指标升序
	 * 600		流动比率
	 * 612		资产负债率
	 * 642		销售利润率   
	 * 650		应收账款周转率
	 * 652		存货周转率
	 * 915		资产利润率
     * @param xdCustNo
     * @return
     * @throws Exception
     */
	List<BigDecimal> getRatioFinaData(String xdCustNo, String currDate, String oneYearDate, String twoYearDate);


	 /**
     * 获取近三年现金流数据
     * 按照时间升序    指标升序
	 * 418		净现金流量
	 * 442		经营性净现金流量
	 * 460		投资活动净现金流量
	 * 464		筹资活动净现金流量
     * @param xdCustNo
     * @return
     * @throws Exception
     */
	List<BigDecimal> getCashFlowFinaData(String xdCustNo, String currDate, String oneYearDate, String twoYearDate);
	

	 /**
    * 获取近三年工作底稿中需要的财务数据
    * 按照时间升序    指标升序
	 * 418		净现金流量
	 * 442		经营性净现金流量
	 * 460		投资活动净现金流量
	 * 464		筹资活动净现金流量
    * @param xdCustNo
    * @return
    * @throws Exception
    */
	List<BigDecimal> getManuscriptFinaData(String xdCustNo, String currDate, String oneYearDate, String twoYearDate);
	

	 /**
   * 获取近三年工作底稿中需要的 担保人财务数据
   * 按照时间升序    指标升序
	 * 418		净现金流量
	 * 442		经营性净现金流量
	 * 460		投资活动净现金流量
	 * 464		筹资活动净现金流量
   * @param xdCustNo
   * @return
   * @throws Exception
   */
	List<BigDecimal> getGuarFinaData(String xdCustNo, String currDate, String oneYearDate, String twoYearDate);
	
	/**
	 * 获取担保人编号（担保人信贷号）
	 * @param xdCustNo
	 * @return
	 */
	GuarBaseDto getGuarCustInfo(String xdCustNo) throws Exception;
	

	/**
	 * 获取最近一笔合同 极其关联信息--->工作底稿部分
	 * @param xdCustNo
	 * @throws Exception
	 */
	List<CmisContractDto> getContractInfo(String xdCustNo) throws Exception;
	
	/**
	 * 通过合同流水号  获取借据信息--》用信情况
	 * 获取最近2笔数据     包括用信时间、金额
	 * @param serialNo
	 * @return
	 * @throws Exception
	 */
	List<CmisBillDto> getBillInfo(String serialNo) throws Exception;
	

	/**
	 * 获取 大额 五级分类认定表中的合同信息表
	 * @param xdCustNo
	 * @return
	 * @throws Exception
	 */
	List<CmisContractDto> getContractList(String xdCustNo) throws Exception;

	/**
	 * 大额 获取当前客户的所有抵押物信息
	 * @param xdCustNo
	 */
	List<GuarantInfoDto> getCustGuarantInfo(String xdCustNo);
	
	/**
	 * 小额 五级分类认定表   最新一笔合同基本信息
	 * @param xdCustNo
	 * @return
	 * @throws Exception
	 */
	CmisContractDto getLastOneContractInfo(String xdCustNo) throws Exception;
	


	 /**
  * 获取近三年小额企事业党委  财务数据
  * 按照时间升序    指标升序
	 	资产总计 165	
		负债合计 246
		营业利润 329	
		净利润   333
		净现金流量 418	
		资产负债率 612
		销售利润率 642	
  * @param xdCustNo
  * @return
  * @throws Exception
  */
	List<BigDecimal> getSmallFinaData(String xdCustNo, String currDate, String oneYearDate, String twoYearDate);
	
		/**
	 * 获取营业收入（信贷系统中获取）
	 * 
	 * @param xdCustNo
	 * @param date
	 * @return
	 */
	Double getfnValue(String customerNo, String date);
	/**
	 * 获取担保公司信息（信贷系统中获取）
	 * 
	 * @param xdCustNo
	 * @return
	 */
	List<GuaCompany> getGuaCompany(String xdCustNo);
	
	/**
	 * 获取担保公司财务状况信息（信贷系统中获取）
	 * 
	 * @param xdCustNo
	 * @return
	 */
	List<CompFinanceTransfer> getCompFinnaceTransfer(String xdCustNo);
	
	/**
	 * 获取自然人担保的个人信息（信贷系统中获取）
	 * 
	 * @param xdCustNo
	 * @return
	 */
	List<GuaPerson> getGuaPerson(String xdCustNo);
	
	/**
	 * 获取房地产抵押信息（信贷系统中获取）
	 * 
	 * @param xdCustNo
	 * @return
	 */
	List<GuaEstate> getGuaEstate(String xdCustNo);
	
	
	/**
	 * 获取动产抵押信息（信贷系统中获取）
	 * 
	 * @param xdCustNo
	 * @return
	 */
	List<GuaChattel> getGuaChattel(String xdCustNo);
	
	
	/**
	 * 获取应收账款质押信息（信贷系统中获取）
	 * 
	 * @param xdCustNo
	 * @return
	 */
	List<GuaAccounts> getGuaAccounts(String xdCustNo);
	
	/**
	 * 获取存单质押信息（信贷系统中获取）
	 * 
	 * @param xdCustNo
	 * @return
	 */
	List<GuaDepositReceipt> getGuaDepositReceipt(String xdCustNo);

	
	
	
}
