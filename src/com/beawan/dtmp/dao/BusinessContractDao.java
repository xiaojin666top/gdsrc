package com.beawan.dtmp.dao;

import java.util.List;

import com.beawan.core.BaseDao;
import com.beawan.dtmp.dto.ContractBillDto;
import com.beawan.dtmp.entity.BusinessContract;
import com.beawan.survey.custInfo.bean.CompConnetCm;
import com.beawan.survey.custInfo.bean.CompFinancingExtGuara;

/**
 * @author yzj
 */
public interface BusinessContractDao extends BaseDao<BusinessContract> {
	
	/**
	 * 获取  在 start 到 end时间段内  授信到期的业务合同列表
	 * @param monthStart
	 * @param monthEnd
	 * @param xdCustNo	信贷客户号
	 * @return
	 */
	List<BusinessContract> getMatuityConListByTerm(String monthStart, String monthEnd, String xdCustNo);
	
	/**
	 * 获取企业在担保信息    
	 * @param xdCustNo
	 * @param endTime	yyyyMMdd 担保合同到期时间在endTime之后在，及还未到期在对外担保
	 * @return
	 */
	List<CompFinancingExtGuara> getGuarantyContract(String xdCustNo, String endTime);

	/**
	 * 获取企业关联关系
	 * @param xdCustNo
	 * @return
	 */
	List<CompConnetCm> getCustomerRelative(String xdCustNo);
	
	/**
	 * 获取当前客户在需要首检在票据清单
	 * 主合同发生类型为 090 110
	 * @param xdCustNo
	 * @return
	 */
	List<ContractBillDto> getDuebill(String xdCustNo);
}
