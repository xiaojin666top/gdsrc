package com.beawan.dtmp.dao;

import com.beawan.core.BaseDao;
import com.beawan.dtmp.entity.EntInfo;

/**
 * @author yzj
 */
public interface EntInfoDao extends BaseDao<EntInfo> {
	/**
	 * 
	 *@Description 根据核心客户号获取行业
	 *@param mfCustomerId
	 *@return
	 *@throws Exception
	 * @author xyh
	 */
	EntInfo getInduByMfCustomerId(String mfCustomerId)throws Exception;
	/**
	 * 
	 *@Description 根据核心客户号获取贷后同步所需数据
	 *@param mfCustomerId
	 *@return
	 *@throws Exception
	 * @author xyh
	 */
	EntInfo getAfInfoByMfCustomerId(String mfCustomerId)throws Exception;
}
