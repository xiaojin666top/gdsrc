package com.beawan.dtmp.dao;

import com.beawan.core.BaseDao;
import com.beawan.dtmp.entity.CockpitData;

/**
 * @author yzj
 */
public interface CockpitDataDao extends BaseDao<CockpitData> {
	
	/**
     * 从本地获取 最新的 驾驶舱数据
     * @return
     * @throws Exception
     */
    CockpitData getLastCockpitData() throws Exception;
}
