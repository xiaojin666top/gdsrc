package com.beawan.dtmp.service;

import com.beawan.core.BaseService;
import com.beawan.dtmp.entity.BusinessDuebill;

/**
 * @author yzj
 */
public interface BusinessDuebillService extends BaseService<BusinessDuebill> {
}
