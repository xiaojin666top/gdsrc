package com.beawan.dtmp.service;

import java.math.BigDecimal;
import java.util.List;

import com.beawan.dtmp.dto.BalByRateDto;
import com.beawan.dtmp.dto.CmisBillDto;
import com.beawan.dtmp.dto.CmisContractDto;
import com.beawan.dtmp.dto.CustLoanDto;
import com.beawan.dtmp.dto.CustSpreadByBalDto;
import com.beawan.dtmp.dto.DepositByDsDto;
import com.beawan.dtmp.dto.DepositByOrgDto;
import com.beawan.dtmp.dto.FmContractDto;
import com.beawan.dtmp.dto.GuarBaseDto;
import com.beawan.dtmp.dto.StyBalDto;
import com.beawan.dtmp.entity.Lnlnslns;
import com.beawan.loanAfter.dto.CmisCustStockDto;
import com.beawan.loanAfter.entity.FcBusiContract;
import com.beawan.loanAfter.entity.FcFinaData;
import com.beawan.loanAfter.entity.LfCustInfo;
import com.beawan.survey.guarantee.entity.GuaCompany;

/**
 * @Author: xyh
 * @Date: 19/08/2020
 * @Description:
 * syncCustBase 一定要在 syncLfCustInfo之前执行
 */
public interface DtmpService {

    /**
     * 同步cmis中的机构数据
     * @return
     * @throws Exception
     */
    boolean syncOrgInfo() throws Exception;

    /**
     * 同步cmis中的用户数据
     * @return
     * @throws Exception
     */
    boolean syncUserInfo() throws Exception;

    /**
     * 同步风险预警信息到贷前中
     * @param xdCustNo 		信贷客户号
     * @param taskId		贷前任务号
     * @return
     * @throws Exception
     */
    boolean syncWmWarning(Long taskId, String xdCustNo) throws Exception;
    
    /**
     * 同步cmis中的企业信息  --》 到 对公CUS_BASE 表中   即   cmis.customer_info ---->  GDTCESYS.cus_base GDTCESYS_cm_base
     * cmis中customertype 为0120：小企业，02：集团客户，0110：一般企业
     * 其他类型不考虑
     * @throws Exception
     */
    void syncCustBase()throws Exception;

    /**
     * 同步贷后客户信息  同步cmis.customer_info --->  GDTCESYS.LF_CUST_INFO
     * 只会同步cmis中核心号不为空的客户
     * @throws Exception
     */
    void syncLfCustInfo()throws Exception;

    /**
     * 更新贷后客户信息----每天会更新一下这个客户的贷款情况
     * 更新客户贷款余额、所在行业
     * @throws Exception
     */
    void updateLfCustInfo()throws Exception;
    /**
     * 
     *@Description （极其重要核心方法）更新一个贷后客户的信息
     *--包括贷款余额、授信时间等重要信息
     *@param lfCustInfo
     *@throws Exception
     * @author xyh
     */
    void updateOneLfCust(LfCustInfo lfCustInfo)throws Exception;

    /**
     * 从信贷系统中获取客户在我行授信情况（只有查询，不做任何业务上的报错操作）
     * @param mfCustomerId       核心客户号
     * @throws Exception
     */
    List<CustLoanDto> getCustLoan(String mfCustomerId,List<String> fiveClassList) throws Exception;

    /**
     * 根据核心放款帐中的编号获取该笔放款帐上的信息
     * @param lnLnAcctNo
     * @return
     * @throws Exception
     */
    Lnlnslns getLoanByLnLnAcctNo(String lnLnAcctNo)throws Exception;

    /********************************** 统计数据驾驶舱 **********************************/
    /*/////////////////////////////////////存款信息/////////////////////////////////////////*/
    /**
     * 对公存款总额（万元）
     * @return
     * @throws Exception
     */
    Double getPublicDeposit() throws Exception;
    
    /**
     * 储蓄存款总额（万元）
     * @return
     * @throws Exception
     */
    Double getSavingsDeposit() throws Exception;
    
    
    /**
     * 保证金存款总额（万元）
     * @return
     * @throws Exception
     */
    Double getMarginDeposit() throws Exception;
    
    /**
     * 分支行统计对公存款(定期、活期)
     * @return
     * @throws Exception
     */
    List<DepositByOrgDto> getDepositByOrg() throws Exception;
    
    /**
     * 分存款种类统计存款总额
     * @return
     * @throws Exception
     */
    List<StyBalDto> getDepositByType() throws Exception;
    
    /**
     * 分定期、活期 统计对公存款 近五个月变化情况
     * @return
     * @throws Exception
     */
    List<DepositByDsDto> getDepositByMonth() throws Exception;
    /*/////////////////////////////////////贷款信息/////////////////////////////////////////*/

    /**
     * 贷款总额
     * @return
     * @throws Exception
     */
    Double getLoanBal() throws Exception;

    /***
     * 统计当前时点 分金额的统计贷款户数分布
     * @return
     * @throws Exception
     */
    CustSpreadByBalDto getCustSpreadByBal() throws Exception;


    /**
     * 统计 按担保方式贷款情况
     * @return
     * @throws Exception
     */
    List<StyBalDto> getBalStry() throws Exception;
    
    /**
     * 统计按五级分类  贷款情况
     */
    List<StyBalDto> getBalByClassifyResult() throws Exception; 
    
    /**
     * 统计贷款利率分布情况
     * @return
     * @throws Exception
     */
    BalByRateDto getBalByRate() throws Exception;
    
    /**
     * 从CMIS同步驾驶舱数据(定时任务)
     * @throws Exception
     */
    void syncCockpitData() throws Exception;
    
    
    //-------------------获取存款 、贷款信息\
    
    /***
     * 获取客户在存款账号及存款金额
     * @param mfCustno		核心客户号
     * @return
     * @throws Exception
     */
    List<StyBalDto> getDepositByCust(String mfCustno) throws Exception;
    
    /**
     * 获取客户的贷款合同信息 包含抵押物信息等
     * @param xdCustNo
     * @return
     * @throws Exception
     */
    List<FcBusiContract> getBusiContractByCust(String xdCustNo) throws Exception;
    
    /**
     * 根据信贷客户号获取企业最新一期的财报年月
     * @param xdCustNo
     * @return
     * @throws Exception
     */
    String getCustFinaDate(String xdCustNo) throws Exception;
    
    /**
     * 获取客户当期 的 资产负债、利润表的主要指标 且按顺序取值就行
     * //资产总计 165		负债合计 246	所有者权益 266	营业收入 301 	利润总额  329
     * @param xdCustNo
     * @param date		期数
     * @return
     * @throws Exception
     */
    List<BigDecimal> getCustFinaCurrMainSubject(String xdCustNo, String date) throws Exception;

    /**
     * 获取近三年比例指标数据 
     * @return
     * @throws Exception
     */
    List<FcFinaData> getRatioFinaData(String xdCustNo, String currDate, String oneYearDate, String twoYearDate) throws Exception;

    /**
     * 获取近三年现金流 数据 
     * 净现金流量  经营性净现金流量	投资活动净现金流量	筹资活动净现金流量
     * @return
     * @throws Exception
     */
    List<FcFinaData> getCashFlowFinaData(String xdCustNo, String currDate, String oneYearDate, String twoYearDate) throws Exception;

    /**
     * 获取近三年 五级分类工作底稿中的财务 数据 
     * 根据模板已进行排序操作
     * 排序顺序为：
     *  1.销  售	
		2净利润	
		3.流动比率	
		4.资产负债率	
		5.销售利润率	
		6.资产利润率	
		7.应收账款周转率	
		8.存货周转率	
		9.总净现金流量	
		10.经营活动净现金流量	
		11.投资活动净现金流量	
		12.筹资活动净现金流量	
     * @return
     * @throws Exception
     */
    List<FcFinaData> getManuscriptFinaData(String xdCustNo, String currDate, String oneYearDate,
			String twoYearDate) throws Exception;
    
    /**
     * 获取近三年 五级分类工作底稿中的  公司类型担保人财务 数据 
     * 根据模板已进行排序操作
     * @return
     * @throws Exception
     */
    List<FcFinaData> getCmGuarFinaData(String xdCustNo, String currDate, String oneYearDate,
			String twoYearDate) throws Exception;
    
    /**
     * 获取担保人的近三期财报数据
     * @param xdCustNo
     */
    GuarBaseDto getGuarInfo(String xdCustNo) throws Exception;
    
    /**
     * 获取企业申请  担保信息
     * @param xdCustNo
     * @throws Exception
     */
    FmContractDto getContractInfo(String xdCustNo) throws Exception;
    

	/**
	 * 通过合同流水号  获取借据信息--》用信情况
	 * 获取最近2笔数据     包括用信时间、金额
	 * @param serialNo
	 * @return
	 * @throws Exception
	 */
	List<CmisBillDto> getBillInfo(String serialNo) throws Exception;
	
	/**
	 * 获取 大额 五级分类认定表中的合同信息表
	 * @param xdCustNo
	 * @return
	 * @throws Exception
	 */
	List<CmisContractDto> getContractList(String xdCustNo) throws Exception;
	
	/**
	 * 压缩  五级分类认定表中的合同信息
	 * 将一对多信息整合为一条记录
	 * @param ContractDtoList
	 * @return
	 * @throws Exception
	 */
	List<CmisContractDto> getContractListCompress(List<CmisContractDto>	contractList) throws Exception;
	
	/**
	 * 认定表中的合同信息
	 * 合计部分数据整合
	 * @param ContractDtoList
	 * @return
	 * @throws Exception
	 */
	List<Double> getContractListSum(List<CmisContractDto> contractListCompress)throws Exception;
	
	/**
	 * 获取大额 五级分类认定表中 客户固定资产
	 * 数据从押品信息表获取
	 * @param xdCustNo
	 * @return
	 * @throws Exception
	 */
	CmisCustStockDto getCustStock(String xdCustNo) throws Exception;
	
	/**
	 * 小额 五级分类认定表   最新一笔合同基本信息
	 * @param xdCustNo
	 * @return
	 * @throws Exception
	 */
	CmisContractDto getLastOneContractInfo(String xdCustNo) throws Exception;
	
	/**
	 * 获取小额企事业单位在 财务指标
	 * @param xdCustNo
	 * @param currDate
	 * @param oneYearDate
	 * @param twoYearDate
	 * @return
	 * @throws Exception
	 */
	public List<FcFinaData> getSmallFinaData(String xdCustNo, String currDate, String oneYearDate,
			String twoYearDate) throws Exception ;
	
	/**
	 * 获取营业收入
	 * 
	 * @param xdCustNo
	 * @param date
	 * @return
	 * @throws Exception
	 */
	Double getfnValue(String customerNo, String date) throws Exception;
	
	/**
	 * 获取担保企业财务状况信息（从财务报表中获取）
	 * 
	 * @param serNo
	 * @return
	 */
	void syncCompFinanceInfo(String serNo);
	
	/**
	 * 同步获取担保企业信息（从信贷系统中获取）
	 * 
	 * @param xdCustNo
	 * @param serNo
	 * @return
	 */
	void syncGuaCompany(String xdCustNo, String serNo);
	
	/**
	 * 同步获取担保自然人担保信息（从信贷系统中获取）
	 * 
	 * @param xdCustNo
	 * @param serNo
	 * @return
	 */
	void syncGuaPerson(String xdCustNo, String serNo);
	
	/**
	 * 同步获取房地产抵押担保信息（从信贷系统中获取）
	 * 
	 * @param xdCustNo
	 * @param serNo
	 * @return
	 */
	void syncGuaEstate(String xdCustNo, String serNo);
	
	
	/**
	 * 同步获取动产抵押担保信息（从信贷系统中获取）
	 * 
	 * @param xdCustNo
	 * @param serNo
	 * @return
	 */
	void syncGuaChattel(String xdCustNo, String serNo);
	
	
	/**
	 * 同步获取应收账款质押担保信息（从信贷系统中获取）
	 * 
	 * @param xdCustNo
	 * @param serNo
	 * @return
	 */
	void syncGuaAccounts(String xdCustNo, String serNo);

	/**
	 * 同步获取存单质押担保信息（从信贷系统中获取）
	 * 
	 * @param xdCustNo
	 * @param serNo
	 * @return
	 */
	void syncGuaDepositReceipt(String xdCustNo, String serNo);

	/**
	 * 同步贷款企业主体对外担保情况（从信贷系统中获取）
	 * @param xdCustNo
	 * @param taskId
	 */
	void syncExtGuara(String xdCustNo, Long taskId, String customerNo);
	
	/**
	 * 同步公司类关联企业信息
	 * @param xdCustNo		信贷客户号
	 * @param customerNo	对公客户号
	 */
	void syncCompConnetCm(String xdCustNo, String customerNo);
	/**
	 * 同步获取担保公司财务数据
	 * @param xdCustNo		信贷客户号
	 * @param customerNo	对公客户号
	 */
	void saveCompFinanceInfo(String XdCustNo, GuaCompany guaCompany);
	
}
