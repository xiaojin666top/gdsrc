package com.beawan.dtmp.service;

import com.beawan.core.BaseService;
import com.beawan.dtmp.entity.EntInfo;

/**
 * @author yzj
 */
public interface EntInfoService extends BaseService<EntInfo> {
}
