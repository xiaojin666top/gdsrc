package com.beawan.dtmp.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.dtmp.dao.BusinessDuebillDao;
import com.beawan.dtmp.entity.BusinessDuebill;
import com.beawan.dtmp.service.BusinessDuebillService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.dtmp.entity.BusinessDuebill;

/**
 * @author yzj
 */
@Service("businessDuebillService")
public class BusinessDuebillServiceImpl extends BaseServiceImpl<BusinessDuebill> implements BusinessDuebillService{
    /**
    * 注入DAO
    */
    @Resource(name = "businessDuebillDao")
    public void setDao(BaseDao<BusinessDuebill> dao) {
        super.setDao(dao);
    }
    @Resource
    public BusinessDuebillDao businessDuebillDao;
}
