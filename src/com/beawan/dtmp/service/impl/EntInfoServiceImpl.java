package com.beawan.dtmp.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.beawan.dtmp.dao.EntInfoDao;
import com.beawan.dtmp.entity.EntInfo;
import com.beawan.dtmp.service.EntInfoService;

/**
 * @author yzj
 */
@Service("entInfoService")
public class EntInfoServiceImpl extends BaseServiceImpl<EntInfo> implements EntInfoService{
    /**
    * 注入DAO
    */
    @Resource(name = "entInfoDao")
    public void setDao(BaseDao<EntInfo> dao) {
        super.setDao(dao);
    }
    @Resource
    public EntInfoDao entInfoDao;
}
