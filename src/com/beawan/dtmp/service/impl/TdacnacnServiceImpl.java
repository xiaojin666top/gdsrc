package com.beawan.dtmp.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

import com.beawan.dtmp.dao.TdacnacnDao;
import com.beawan.dtmp.entity.Tdacnacn;
import com.beawan.dtmp.service.TdacnacnService;

/**
 * @author yzj
 */
@Service("tdacnacnService")
public class TdacnacnServiceImpl extends BaseServiceImpl<Tdacnacn> implements TdacnacnService{
    /**
    * 注入DAO
    */
    @Resource(name = "tdacnacnDao")
    public void setDao(BaseDao<Tdacnacn> dao) {
        super.setDao(dao);
    }
    @Resource
    public TdacnacnDao tdacnacnDao;
}
