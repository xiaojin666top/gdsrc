package com.beawan.dtmp.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.beawan.base.dao.IOrganizationDAO;
import com.beawan.base.dao.IUserDAO;
import com.beawan.base.entity.Organization;
import com.beawan.base.entity.QuickAnaly;
import com.beawan.base.entity.User;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.dao.ICusBaseDAO;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.dtmp.dao.BusinessContractDao;
import com.beawan.dtmp.dao.CockpitDataDao;
import com.beawan.dtmp.dao.CustomerInfoDao;
import com.beawan.dtmp.dao.EntInfoDao;
import com.beawan.dtmp.dao.LnlnslnsDao;
import com.beawan.dtmp.dao.OrgInfoDao;
import com.beawan.dtmp.dao.TdacnacnDao;
import com.beawan.dtmp.dao.UserInfoDao;
import com.beawan.dtmp.dto.BalByRateDto;
import com.beawan.dtmp.dto.CmisBillDto;
import com.beawan.dtmp.dto.CmisContractDto;
import com.beawan.dtmp.dto.ContractBillDto;
import com.beawan.dtmp.dto.CustLoanDto;
import com.beawan.dtmp.dto.CustSpreadByBalDto;
import com.beawan.dtmp.dto.DepositByDsDto;
import com.beawan.dtmp.dto.DepositByOrgDto;
import com.beawan.dtmp.dto.FmContractDto;
import com.beawan.dtmp.dto.GuarBaseDto;
import com.beawan.dtmp.dto.GuarantInfoDto;
import com.beawan.dtmp.dto.StyBalDto;
import com.beawan.dtmp.entity.BusinessContract;
import com.beawan.dtmp.entity.CockpitData;
import com.beawan.dtmp.entity.CustomerInfo;
import com.beawan.dtmp.entity.EntInfo;
import com.beawan.dtmp.entity.FComCodeValue;
import com.beawan.dtmp.entity.Lnlnslns;
import com.beawan.dtmp.entity.OrgInfo;
import com.beawan.dtmp.entity.UserInfo;
import com.beawan.dtmp.service.DtmpService;
import com.beawan.loanAfter.dao.LfCustInfoDao;
import com.beawan.loanAfter.dto.CmisCustStockDto;
import com.beawan.loanAfter.entity.FcBusiContract;
import com.beawan.loanAfter.entity.FcFinaData;
import com.beawan.loanAfter.entity.LfCustInfo;
import com.beawan.qcc.service.SynchronizationQCCService;
import com.beawan.survey.custInfo.bean.CompBase;
import com.beawan.survey.custInfo.bean.CompConnetCm;
import com.beawan.survey.custInfo.bean.CompFinancingExtGuara;
import com.beawan.survey.custInfo.bean.CompWmWarnSignal;
import com.beawan.survey.custInfo.dao.CompWmWarnSignalDao;
import com.beawan.survey.custInfo.dao.ICompBaseDAO;
import com.beawan.survey.custInfo.dao.ICompConnectDAO;
import com.beawan.survey.custInfo.dao.ICompFinancingExtGuaraDAO;
import com.beawan.survey.custInfo.dto.CompBaseDto;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.beawan.survey.guarantee.entity.CompFinanceInfo;
import com.beawan.survey.guarantee.entity.CompFinanceTransfer;
import com.beawan.survey.guarantee.entity.GuaAccounts;
import com.beawan.survey.guarantee.entity.GuaChattel;
import com.beawan.survey.guarantee.entity.GuaCompany;
import com.beawan.survey.guarantee.entity.GuaDepositReceipt;
import com.beawan.survey.guarantee.entity.GuaEstate;
import com.beawan.survey.guarantee.entity.GuaPerson;
import com.beawan.survey.guarantee.service.CompFinanceInfoService;
import com.beawan.survey.guarantee.service.GuaAccountsService;
import com.beawan.survey.guarantee.service.GuaChattelService;
import com.beawan.survey.guarantee.service.GuaCompanyService;
import com.beawan.survey.guarantee.service.GuaDepositReceiptService;
import com.beawan.survey.guarantee.service.GuaEstateService;
import com.beawan.survey.guarantee.service.GuaPersonService;
import com.google.common.collect.Lists;
import com.platform.util.DateUtil;
import com.platform.util.EncryptUtil;
import com.platform.util.ExceptionUtil;
import com.platform.util.HttpUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;

/**
 * @Author: xyh
 * @Date: 19/08/2020
 * @Description:
 */
@Service("dtmpService")
@Transactional(propagation= Propagation.REQUIRED,readOnly=false,rollbackFor={Exception.class})
public class DtmpServiceImpl implements DtmpService {

	private static final Logger log = Logger.getLogger(DtmpServiceImpl.class);
    //cmis中的dao
    @Resource
    public CustomerInfoDao customerInfoDao;//cmis客户库
    @Resource
    public OrgInfoDao orgInfoDao;//cmis机构
    @Resource
    public UserInfoDao userInfoDao;//cmis用户
    @Resource
    public LnlnslnsDao lnlnslnsDao;//cbod中的放款档
    @Resource
    public EntInfoDao entInfoDao;//企业基础信息
    @Resource
    public TdacnacnDao tdacnacnDao;//cbod中的定期存款主档
    @Resource
    public CockpitDataDao cockpitDataDao;// 驾驶舱数据
    @Resource
    public BusinessContractDao businessContractDao;// 业务合同

    //对公系统中的dao
    @Resource
    private ICusBaseDAO cusBaseDAO;
    @Resource
    private LfCustInfoDao lfCustInfoDao;
    @Resource
    private IOrganizationDAO organizationDAO;
    @Resource
    private IUserDAO userDAO;
    @Resource
    private ICompBaseDAO compBaseDAO;

    @Resource
    private CompWmWarnSignalDao compWmWarnSignalDao;

    @Resource 
    private CompFinanceInfoService compFinanceInfoService;
    @Resource 
    private GuaCompanyService guaCompanyService;
    @Resource
	private ICusBaseSV cusBaseSV;
    @Resource 
    private GuaPersonService guaPersonService;
    @Resource 
    private GuaEstateService guaEstateService;
    @Resource 
    private GuaAccountsService guaAccountsService;
    @Resource 
    private GuaDepositReceiptService guaDepositReceiptService;
    @Resource 
    private GuaChattelService guaChattelService;
    @Resource
    private ICompFinancingExtGuaraDAO compFinancingExtGuaraDAO;
    @Resource
    private ICompConnectDAO compConnectDAO;
    @Resource
    private SynchronizationQCCService synchronizationQCCService;
    @Resource
    private ICompBaseSV compBaseSV;//客户详细信息
    
    
    

    @Override
    public boolean syncOrgInfo() throws Exception{
        List<OrgInfo> targetList = orgInfoDao.getDtmpAll();
        List<Organization> ownList = new ArrayList<>();

        if(targetList==null || targetList.size()==0){
            ExceptionUtil.throwException("信贷系统中机构表数据为空，同步失败");
            return false;
        }
        for(OrgInfo target : targetList) {
            Organization own = new Organization();
            own.setStates(Constants.NORMAL);
            own.setOrganno(target.getOrgid());
            own.setSuporganno(target.getRelativeorgid());
            own.setLocate(target.getArgaddress());
            own.setOrganname(target.getOrgname());
            own.setOrganshortform(target.getOrgabbrevi());
            own.setOrderno(target.getSortno());
            own.setOrganchief(target.getPrincipal());
            own.setTelnum(target.getOrgtel());
            own.setFax(target.getFaxno());
            own.setOrganlevel(target.getOrglevel());
            ownList.add(own);
        }
        return organizationDAO.batchSaveUpdata(ownList);
    }


    @Override
    public boolean syncUserInfo() throws Exception {
        List<UserInfo> targetList = userInfoDao.getDtmpAll();
        List<User> ownList = new ArrayList<>();

        if(targetList==null || targetList.size()==0){
            ExceptionUtil.throwException("信贷系统中用户表数据为空，同步失败");
            return false;
        }
        for(UserInfo tar : targetList) {
        	User own = userDAO.selectSingleByProperty("userId", tar.getUserid());
        	if(own == null){
        		own = new User();
                own.setUserId(tar.getUserid());
                own.setLoginId(tar.getLoginid());
                own.setPassword(EncryptUtil.md5("123456"));
                own.setUserName(tar.getUsername());
                own.setBusinessLine("1");//业务条线，全部0、对公业务1、个人业务2
                //
                own.setState(SysConstants.UserStatus.NORMAL);
                own.setSignState("online");//新增用户默认在岗
                own.setWrongPinNum(0);
                own.setConWrongPinNum(0);
        	}
            own.setAccOrgNo(tar.getBelongorg());
            own.setPhone(tar.getMobiletel());
            own.setCertid(tar.getCertid());
            own.setInputOrg(tar.getInputorg());
            own.setInputUser(tar.getInputuser());
            own.setInputTime(tar.getInputtime());
            own.setUpdater(tar.getUpdateuser());
            ownList.add(own);
        }
        return userDAO.batchSaveUpdata(ownList);
    }

    @Override
    public boolean syncWmWarning(Long taskId, String xdCustNo) throws Exception {
    	List<CompWmWarnSignal> list = customerInfoDao.getWmWarningSignal(xdCustNo);
    	if(CollectionUtils.isEmpty(list)){
    		log.info("贷前任务号为： "+ taskId + "------信贷客户号为:" + xdCustNo +"---不存在风险预警信息");
    		return true;
    	}
    	for(CompWmWarnSignal signal : list){
    		signal.setTaskId(taskId);
        	compWmWarnSignalDao.saveOrUpdate(signal);
    	}
    	return true;
    }


    @Override
    public void syncCustBase() throws Exception {
        //同步小企业客户
        sycnCustBaseByType("0120");
        //同步集团客户
        sycnCustBaseByType("02");
        //同步一般企业
        sycnCustBaseByType("0110");
    }
    

    /**
     * 根据客户类型进行同步
     * @param customerType
     * @throws Exception
     */
    void sycnCustBaseByType(String customerType)throws Exception{
        //获取cmis中的公司类客户
        List<CustomerInfo> customerInfoList = customerInfoDao.getDtmpByProperty("customertype",customerType);
        //循环遍历
        if(!CollectionUtils.isEmpty(customerInfoList)){
        	//客户名称相同  去重 
        	customerInfoList = customerInfoList.stream().sorted(Comparator.comparing(CustomerInfo::getCustomername ,
        			Comparator.nullsLast(Comparator.naturalOrder()))
    				.thenComparing(CustomerInfo::getUpdatedate, Comparator.nullsLast(Comparator.naturalOrder())).reversed())
    			.collect(Collectors.collectingAndThen(Collectors.toCollection(()-> 
    			new TreeSet<>(Comparator.comparing(CustomerInfo::getCustomername,Comparator.nullsLast(Comparator.naturalOrder())))), ArrayList::new));
        	
        	
            for(CustomerInfo customerInfo : customerInfoList){
                //核心号为空或者是客户名字是空的直接跳过
                if(StringUtil.isEmptyString(customerInfo.getMfcustomerid()) || StringUtil.isEmptyString(customerInfo.getCustomername())){
                    continue;
                }
                
                //信贷系统中  客户详细信息
                EntInfo entInfo = entInfoDao.getDtmpSingleByProperty("customerid", customerInfo.getCustomerid());
                CusBase cusBase = cusBaseDAO.selectSingleByProperty("mfCustomerNo",customerInfo.getMfcustomerid());
                if(cusBase == null){
                    cusBase = cusBaseDAO.selectSingleByProperty("customerName",customerInfo.getCustomername());
                }
                if(cusBase == null){
                    cusBase = new CusBase();
                    cusBase.setCustomerNo(StringUtil.genRandomNo());
                    cusBase.setCreater("cmis");
                }else{
                	//走到这里说明  至少可以通过核心号或者客户名称查到
                }
                cusBase.setCustomerName(customerInfo.getCustomername());
                cusBase.setCustomerType(customerInfo.getCustomertype());
                cusBase.setCertType(customerInfo.getCerttype());
                cusBase.setCertCode(customerInfo.getCertid());
                cusBase.setInputOrgId(customerInfo.getInputorgid());
                cusBase.setInputUserId(customerInfo.getInputuserid());
                cusBase.setInputDate(customerInfo.getInputdate());
                cusBase.setMfCustomerNo(customerInfo.getMfcustomerid());
                cusBase.setXdCustomerNo(customerInfo.getCustomerid());
                cusBase.setState(customerInfo.getStatus());
                cusBase.setManagerUserId(customerInfo.getManageruserid());
                cusBase.setManagerOrgId(customerInfo.getManagerorgid());
                cusBase.setBlackSheetOrNot(customerInfo.getBlacksheetornot());
                cusBase.setConfirmOrNot(customerInfo.getConfirmornot());
                cusBase.setClientClassN(customerInfo.getClientclassn());
                cusBase.setClientClassM(customerInfo.getClientclassm());
                cusBase.setBusinessState(customerInfo.getBusinessstate());
                cusBase.setUpdateDate(customerInfo.getUpdatedate());
                cusBase.setSyncTime(DateUtil.formatDate(new Date(), Constants.DATETIME_MASK));
                cusBase.setUpdater("cmis");
                cusBase.setUpdateTime(DateUtil.getNowTimestamp());
                cusBase = cusBaseDAO.saveOrUpdate(cusBase);
                
                CompBase compBase = compBaseDAO.selectSingleByProperty("customerNo", cusBase.getCustomerNo());
                if(compBase==null){
                	compBase = new CompBase(cusBase.getCustomerNo());
                }
                //从关联关系表中获取实际控制人  -- 》可能为空
                String realContoller = customerInfoDao.getRealContoller(customerInfo.getCustomerid());
                //同步信贷系统中  客户有的数据
                if(StringUtil.isEmptyString(compBase.getControlPerson())){
                    compBase.setControlPerson(realContoller);
                }
                
                //1.实收资本  注册资本
                if(entInfo!=null){
                	//同步企业财报类型
                	String financebelong = entInfo.getFinancebelong();
                	if("010".equals(financebelong) || "020".equals(financebelong)){
                		compBase.setFinanceBelong(financebelong);
                	}
                	
                	
	                compBase.setRealCapital(entInfo.getPaiclupCapital());
	                if("CNY".equals(entInfo.getPccurrency())){
	                	compBase.setPcCurrency("人民币");
	                }else{
	                	compBase.setPcCurrency(entInfo.getPccurrency());
	                }
	                //注册资本
	                compBase.setRegisterCapital(entInfo.getRegistercapital());
	                if("CNY".equals(entInfo.getRccurrency())){
	                	compBase.setRcCurrency("人民币");
	                }else{
	                	compBase.setRcCurrency(entInfo.getRccurrency());
	                }
	                //公司成立日期
	                String setupdate = entInfo.getSetupdate();
	                if(!StringUtils.isEmpty(setupdate) && setupdate.length()==8){
	                	String foundDate = setupdate.substring(0,4)
	                			+"-"+setupdate.substring(4,6)
	                			+"-"+setupdate.substring(6,8)
	                			+" 00:00:00";
	                	compBase.setFoundDate(foundDate);
	                }
	                
	                //实际经营地址
	                String officeadd = entInfo.getOfficeadd();
	                if(!StringUtils.isEmpty(officeadd)){
	                    compBase.setRunAddress(officeadd);
	                }
	                String mybankdorm = entInfo.getMybankdorm();
	                //同步是否我行股东信息
	                if(!StringUtils.isEmpty(mybankdorm) && !mybankdorm.equals("X"))
	                	compBase.setIsMbankHolder(mybankdorm);
	                //同步行业信息
	                String industryType = entInfo.getIndustryType();
	                if(!StringUtils.isEmpty(industryType)){
	                	List<FComCodeValue> induTypeList = customerInfoDao.getFComCodeValue("GB00006", "industryType");
	                	if(!CollectionUtils.isEmpty(induTypeList)){
	                		String codeValueRefe = induTypeList.get(0).getCodeValueRefe();
	                    	//这边信贷系统存的是完整4级行业代码，第一级大行业是英文，需要去掉
	                    	industryType = industryType.substring(1);
	                    	compBase.setIndustryCode(industryType);
	                    	compBase.setIndustryType(codeValueRefe);
	                	}
	                	
	                }
                }
                compBaseDAO.saveOrUpdate(compBase);
                System.out.println(customerInfo.getCustomername() + "数据同步成功");
            }
        }
    }

    @Override
    public void syncLfCustInfo() throws Exception {
        //同步小企业客户
        sycnLfCustInfoByType("0120");
        //同步集团客户
        sycnLfCustInfoByType("02");
        //同步一般企业
        sycnLfCustInfoByType("0110");
    }


    /**
     * 根据客户类型进行同步 贷后客户名单----有过核心号的认为是贷后名单之一
     * @param customerType
     * @throws Exception
     */
    void sycnLfCustInfoByType(String customerType)throws Exception{
        //获取cmis中的公司类客户
        List<CustomerInfo> customerInfoList = customerInfoDao.getDtmpByProperty("customertype",customerType);
        if(!CollectionUtils.isEmpty(customerInfoList)){
            for(CustomerInfo customerInfo : customerInfoList){
                if(StringUtil.isEmptyString(customerInfo.getMfcustomerid())){
                    //核心号为空，认定不可能会有贷后任务，核心号是放款档中的查询依据
                    continue;
                }
                //根据核心号去cusBase中去查询，如果cusBase中不存在，虽然不存在肯定是哪里出了问题，不过还是跳过
                CusBase cusBase = cusBaseDAO.selectSingleByProperty("mfCustomerNo",customerInfo.getMfcustomerid());
                if(cusBase == null || StringUtil.isEmptyString(cusBase.getCustomerNo())){
                    continue;
                }
                LfCustInfo lfCustInfo = lfCustInfoDao.selectSingleByProperty("customerNo",cusBase.getCustomerNo());
                if(lfCustInfo == null){
                    lfCustInfo = new LfCustInfo();
                    lfCustInfo.setCustomerNo(cusBase.getCustomerNo());
                    lfCustInfo.setLastLoanSum(0d);
                    lfCustInfo.setLastLoanBalance(0d);
                    lfCustInfo.setIsAddCust("0");
                    lfCustInfo.setCreater("cmis");
                    lfCustInfo.setUpdater("cmis");
                    lfCustInfo.setUpdateTime(DateUtil.getNowTimestamp());
                    lfCustInfoDao.saveOrUpdate(lfCustInfo);
                }
                
            }
        }
    }

    @Override
    public void updateLfCustInfo() throws Exception{
        //获取所有的贷后客户
        StringBuilder sql = new StringBuilder(" from LfCustInfo where 1=1 ");
        sql.append(" and status=:status ");
        Map<String, Object> params = new HashMap<>();
        params.put("status",Constants.NORMAL);
        List<LfCustInfo> lfCustInfoList = lfCustInfoDao.select(sql.toString(),params);
        if(CollectionUtils.isEmpty(lfCustInfoList)){
        	log.error("贷后客户信息列表为空-------->严重异常---，请核查");
            return;
        }
        for (int i = 0; i < lfCustInfoList.size(); i++){
        	LfCustInfo lfCustInfo = lfCustInfoList.get(i);
        	updateOneLfCust(lfCustInfo);
        	log.info("当前更新贷后客户授信信息进度为：" + lfCustInfo.getCustomerNo() 
        	+ "-------(" + (i+1) + "/" + lfCustInfoList.size() + ")" );
        }
    }
    

    @Override
	public void updateOneLfCust(LfCustInfo lfCustInfo) throws Exception {
    	 //获取cusBase的信息
        CusBase cusBase = cusBaseDAO.selectSingleByProperty("customerNo", lfCustInfo.getCustomerNo());
        if(cusBase == null){
            return;
        }
        String xdCustomerNo = cusBase.getXdCustomerNo();
        if(xdCustomerNo.isEmpty()){
        	log.error("更新客户贷后状态时，当前客户的信贷系统号为空！其对公客户号为：======"+lfCustInfo.getCustomerNo());
        	return ;
        }
        //原先从   cbod下的  lnlnslns表 中取用信数据，该取法是错误，
        //现在是从cmis.business_contract取数据   授信总金额
        //获取信贷号为xdCustNo的 当前存在的授信 列表
        BigDecimal amt = new BigDecimal(0);//计算贷款金额
        List<BusinessContract> contractList = businessContractDao.getMatuityConListByTerm(null, null, xdCustomerNo);
        //若 授信合同列表为空，则直接更新 lfCustInfo中的授信总额为0
        if(CollectionUtils.isEmpty(contractList)){
        	lfCustInfo.setLastLoanSum(0d);
        	lfCustInfoDao.saveOrUpdate(lfCustInfo);
        	return;
        }
        for(BusinessContract contract : contractList){
        	Double businesssum = contract.getBusinesssum();
        	if(businesssum==null || businesssum <= 0){
        		continue;
        	}
        	amt = amt.add(new BigDecimal(businesssum));
        }
        //若当前客户授信 金额为 0 ，  则对其内容进行值零操作
        if(amt.compareTo(BigDecimal.ZERO) != 1){
            //把以前的存量客户恢复初始状态
            if("cbod".equals(lfCustInfo.getUpdater())){
                lfCustInfo.setLastLoanSum(0d);
                lfCustInfo.setLastLoanBalance(0d);
                lfCustInfo.setIsAddCust("0");
                lfCustInfo.setUpdater("cmis");
                lfCustInfo.setUpdateTime(DateUtil.getNowTimestamp());
                lfCustInfo.setLastStopDate(null);
                lfCustInfo.setLastStartDate(null);
            }
            lfCustInfoDao.saveOrUpdate(lfCustInfo);
            //授信金额小于等于0时，表示这个贷后客户没有存量贷款
            return;
        }
        //这里开始表示是这个客户需要做贷后
        EntInfo entInfo = entInfoDao.getInduByMfCustomerId(cusBase.getMfCustomerNo());
        log.info("获取cmis下的ent_info 数据成功");
        if(entInfo != null){//设置这个客户的行业编号----非国标
        	lfCustInfo.setIndustryType(entInfo.getIndustryType());
        }
        
        //开始判断当前客户在最新一笔用信（票据）信息   由发生时间 倒叙   给首检任务用的
        List<ContractBillDto> duebillList = businessContractDao.getDuebill(xdCustomerNo);
        if(!CollectionUtils.isEmpty(duebillList)){
        	ContractBillDto contractBillDto = duebillList.get(0);
        	String serialNo = contractBillDto.getSerialNo();
        	if(serialNo!=null && !serialNo.equals(lfCustInfo.getLastLnLnAcctNo())){
                lfCustInfo.setIsAddCust("1");
                lfCustInfo.setLastLnLnAcctNo(serialNo);
                lfCustInfo.setLastStartDate(contractBillDto.getPutoutDate());
        	}
        }
        
        /*//原先只通过授信合同的日期进行判断，现在通过合同对应的票据进行判断
        //判断当前客户是否是  新增或存量增量类型 
        //首先获取最新一期在授信信息
        BusinessContract max = contractList.stream().max(Comparator.comparing(BusinessContract::getOccurdate)).get();
        String serialno = max.getSerialno();
        String occurdate = max.getOccurdate();
        if( !serialno.equals(lfCustInfo.getLastLnLnAcctNo()) 
        		&& (Constants.OCCURTYPE.FIRST.equals(max.getOccurtype())
        		|| Constants.OCCURTYPE.INCRE.equals(max.getOccurtype()))){
            //授信发生类型为首次或增量
            //标记这个人为新增
            lfCustInfo.setIsAddCust("1");
            lfCustInfo.setLastLnLnAcctNo(max.getSerialno());
            lfCustInfo.setLastStartDate(occurdate);
        }*/
        
        //更新
        
        
        lfCustInfo.setLastLoanSum(amt.doubleValue());
        lfCustInfo.setUpdater("cbod");
        lfCustInfo.setUpdateTime(DateUtil.getNowTimestamp());
        lfCustInfoDao.saveOrUpdate(lfCustInfo);
	}


	@Override
    public List<CustLoanDto> getCustLoan(String mfCustomerId,List<String> fiveClassList) throws Exception {
        /**
         * 1.先从下发的数据表 LNLNSLNS(放款挡) BUSINESS_CONTRACT(借据表) BUSINESS_TYPE(业务品种)
         * 连表获取客户在我行信贷业务的情况
         */
        return lnlnslnsDao.getCustLoan(mfCustomerId,fiveClassList);

    }

    @Override
    public Lnlnslns getLoanByLnLnAcctNo(String lnLnAcctNo) throws Exception {
        Lnlnslns lnlnslns = lnlnslnsDao.getDtmpSingleByProperty("lnLnAcctNo",lnLnAcctNo);
        return lnlnslns;
    }
    
    @Override
    public Double getLoanBal() throws Exception {
        return lnlnslnsDao.getLoanBal();
    }

    @Override
    public CustSpreadByBalDto getCustSpreadByBal() throws Exception {
        CustSpreadByBalDto spread = lnlnslnsDao.getCustSpreadByBal(null);
        return spread;
    }

    @Override
    public List<StyBalDto> getBalStry() throws Exception {
        return lnlnslnsDao.getBalStry();
    }


	@Override
	public List<StyBalDto> getBalByClassifyResult() throws Exception {
		List<StyBalDto> classifyResult = lnlnslnsDao.getBalByClassifyResult();
		if(CollectionUtils.isEmpty(classifyResult))
			return null;
		//去掉五级分类为空的分组
		ListIterator<StyBalDto> it = classifyResult.listIterator();
		while(it.hasNext()){
			StyBalDto dto = it.next();
			if(StringUtil.isEmptyString(dto.getStyName())){
				it.remove();
				break;
			}
		}
		return classifyResult;
	}


	@Override
	public BalByRateDto getBalByRate() throws Exception {
		BalByRateDto data = lnlnslnsDao.getBalByRate();
		return data;
	}


	/*/////////////////////////////////////存款信息/////////////////////////////////////////*/
	@Override
	public Double getPublicDeposit() throws Exception {
		return tdacnacnDao.getTotalDeposit(Constants.DEPOSIT_TYPE.PUBLIC_DEPOSIT);
	}


	@Override
	public Double getSavingsDeposit() throws Exception {
		return tdacnacnDao.getTotalDeposit(Constants.DEPOSIT_TYPE.SAVINGS_DEPOSIT);
	}


	@Override
	public Double getMarginDeposit() throws Exception {
		return tdacnacnDao.getTotalDeposit(Constants.DEPOSIT_TYPE.MARGIN_DEPOSIT);
	}


	@Override
	public List<DepositByOrgDto> getDepositByOrg() throws Exception {
		return tdacnacnDao.getDepositByOrg();
	}


	@Override
	public List<StyBalDto> getDepositByType() throws Exception {
		List<StyBalDto> result = new ArrayList<>();
		result.add(new StyBalDto("保证金存款", tdacnacnDao.getTotalDeposit(Constants.DEPOSIT_TYPE.MARGIN_DEPOSIT)));
		result.add(new StyBalDto("对公存款", tdacnacnDao.getTotalDeposit(Constants.DEPOSIT_TYPE.PUBLIC_DEPOSIT)));
		result.add(new StyBalDto("储蓄存款", tdacnacnDao.getTotalDeposit(Constants.DEPOSIT_TYPE.SAVINGS_DEPOSIT)));
		return result;
	}

	@Override
	public List<DepositByDsDto> getDepositByMonth() throws Exception {
		List<DepositByDsDto> result = new ArrayList<>();
		// 近五个月数据(不包括当月)
		for(int i=5; i>0; i--){
			String endDate = DateUtil.getLastEndData(i).replaceAll("-", "");//yyyy-MM-dd
			Double tdDeposit = tdacnacnDao.getTdDeposit(endDate);
			Double saDeposit = tdacnacnDao.getSaDeposit(endDate);
			result.add(new DepositByDsDto(endDate.substring(0, 6), tdDeposit, saDeposit));
		}
		return result;
	}

	@Override
	public void syncCockpitData()  throws Exception{
		try {
			// 数据同步之前删除原有数据
			List<CockpitData> entityList = cockpitDataDao.getAll();
			if(!CollectionUtils.isEmpty(entityList)){
				for(CockpitData entity :entityList){
					entity.setStatus(Constants.DELETE);
					entity.setUpdater("SYSTEM");
					entity.setUpdateTime(DateUtil.getNowTimestamp());
					cockpitDataDao.saveOrUpdate(entity);
				}
			}
			
			CockpitData entity = new CockpitData();
			entity.setSycnDate(DateUtil.getNowYmdStr());
			entity.setLoanSum(String.valueOf(lnlnslnsDao.getLoanBal()));
			entity.setDepositSum(String.valueOf(tdacnacnDao.getTotalDeposit(Constants.DEPOSIT_TYPE.PUBLIC_DEPOSIT)));
			entity.setDepositByType(JacksonUtil.serialize(this.getDepositByType()));
			entity.setDepositByOrg(JacksonUtil.serialize(this.getDepositByOrg()));
			entity.setLoanByCustNum(JacksonUtil.serialize(lnlnslnsDao.getCustSpreadByBal(null)));
			entity.setLoanByGuaType(JacksonUtil.serialize(lnlnslnsDao.getBalStry()));
			entity.setLoanByClassify(JacksonUtil.serialize(this.getBalByClassifyResult()));
			entity.setLoanRote(JacksonUtil.serialize(lnlnslnsDao.getBalByRate()));
			//entity.setLoanByCredit(JacksonUtil.serialize());
			entity.setDepositByMonth(JacksonUtil.serialize(this.getDepositByMonth()));
			
			entity.setCreater("CMIS");
			entity.setUpdater("CMIS");
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			entity.setCreateTime(DateUtil.getNowTimestamp());
			entity.setStatus(Constants.NORMAL);
			cockpitDataDao.saveOrUpdate(entity);
		} catch (Exception e) {
			log.error("驾驶舱定时器：同步驾驶舱数据出错！", e);
			e.printStackTrace();
		} 
	}


	@Override
	public List<StyBalDto> getDepositByCust(String mfCustno) throws Exception {
		List<StyBalDto> saList = tdacnacnDao.getSaDepositByCustNo(mfCustno);
		List<StyBalDto> tdList = tdacnacnDao.getTdDepositByCustNo(mfCustno);
		if(saList==null)
			return tdList;
		if(tdList == null)
			return saList;
		saList.addAll(tdList);
		return saList;
	}


	@Override
	public List<FcBusiContract> getBusiContractByCust(String xdCustNo) throws Exception {
		return lnlnslnsDao.getBusiContractByCust(xdCustNo);
	}


	@Override
	public String getCustFinaDate(String xdCustNo) throws Exception {
		return lnlnslnsDao.getCustFinaDate(xdCustNo);
	}


	@Override
	public List<BigDecimal> getCustFinaCurrMainSubject(String xdCustNo, String date) throws Exception {
		return lnlnslnsDao.getCustFinaCurrMainSubject(xdCustNo, date);
	}


	@Override
	public Double getfnValue(String customerNo, String date) throws Exception {
		return lnlnslnsDao.getfnValue(customerNo, date);
	}


	@Override
	public List<FcFinaData> getRatioFinaData(String xdCustNo, String currDate, String oneYearDate, String twoYearDate)
			throws Exception {
		List<FcFinaData> finaDataList = new ArrayList<>();
		List<BigDecimal> ratioFinaDatas = lnlnslnsDao.getRatioFinaData(xdCustNo, currDate, oneYearDate, twoYearDate);
		if(CollectionUtils.isEmpty(ratioFinaDatas)){
			return null;
		}
		FcFinaData data1 = new FcFinaData(Constants.FINA_MOLD.RATIO, "流动比率（倍）", 0d, 0d, 0d, 5);
		FcFinaData data2 = new FcFinaData(Constants.FINA_MOLD.RATIO, "资产负债率（%）", 0d, 0d, 0d, 6);
		FcFinaData data3 = new FcFinaData(Constants.FINA_MOLD.RATIO, "销售利润率（%）", 0d, 0d, 0d, 1);
		FcFinaData data4 = new FcFinaData(Constants.FINA_MOLD.RATIO, "应收帐款周转率（次/年）", 0d, 0d, 0d, 3);
		FcFinaData data5 = new FcFinaData(Constants.FINA_MOLD.RATIO, "存货周转率（次/年）", 0d, 0d, 0d, 4);
		FcFinaData data6 = new FcFinaData(Constants.FINA_MOLD.RATIO, "资产利润率（%）", 0d, 0d, 0d, 2);
		//有数据在期数
		int size = ratioFinaDatas.size()/6;
		for(int i = 0; i < size; i++){
			if(i==0){
				data1.setCurrVal(ratioFinaDatas.get((i*6)+0).doubleValue());
				data2.setCurrVal(ratioFinaDatas.get((i*6)+1).doubleValue());
				data3.setCurrVal(ratioFinaDatas.get((i*6)+2).doubleValue());
				data4.setCurrVal(ratioFinaDatas.get((i*6)+3).doubleValue());
				data5.setCurrVal(ratioFinaDatas.get((i*6)+4).doubleValue());
				data6.setCurrVal(ratioFinaDatas.get((i*6)+5).doubleValue());
			}else if(i==1){
				data1.setOneYearVal(ratioFinaDatas.get((i*6)+0).doubleValue());
				data2.setOneYearVal(ratioFinaDatas.get((i*6)+1).doubleValue());
				data3.setOneYearVal(ratioFinaDatas.get((i*6)+2).doubleValue());
				data4.setOneYearVal(ratioFinaDatas.get((i*6)+3).doubleValue());
				data5.setOneYearVal(ratioFinaDatas.get((i*6)+4).doubleValue());
				data6.setOneYearVal(ratioFinaDatas.get((i*6)+5).doubleValue());
			}else if(i==2){
				data1.setTwoYearVal(ratioFinaDatas.get((i*6)+0).doubleValue());
				data2.setTwoYearVal(ratioFinaDatas.get((i*6)+1).doubleValue());
				data3.setTwoYearVal(ratioFinaDatas.get((i*6)+2).doubleValue());
				data4.setTwoYearVal(ratioFinaDatas.get((i*6)+3).doubleValue());
				data5.setTwoYearVal(ratioFinaDatas.get((i*6)+4).doubleValue());
				data6.setTwoYearVal(ratioFinaDatas.get((i*6)+5).doubleValue());
			}
		}
		finaDataList.add(data1);
		finaDataList.add(data2);
		finaDataList.add(data3);
		finaDataList.add(data4);
		finaDataList.add(data5);
		finaDataList.add(data6);
		return finaDataList;
	}

	@Override
	public List<FcFinaData> getCashFlowFinaData(String xdCustNo, String currDate, String oneYearDate,
			String twoYearDate) throws Exception {
		List<FcFinaData> finaDataList = new ArrayList<>();
		List<BigDecimal> ratioFinaDatas = lnlnslnsDao.getCashFlowFinaData(xdCustNo, currDate, oneYearDate, twoYearDate);
		if(CollectionUtils.isEmpty(ratioFinaDatas)){
			return null;
		}
		FcFinaData data1 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "净现金流量", 0d, 0d, 0d, 1);
		FcFinaData data2 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "经营性净现金流量", 0d, 0d, 0d, 2);
		FcFinaData data3 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "投资活动净现金流量", 0d, 0d, 0d, 3);
		FcFinaData data4 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "筹资活动净现金流量", 0d, 0d, 0d, 4);
		//有数据在期数
		int size = ratioFinaDatas.size()/4;
		for(int i = 0; i < size; i++){
			if(i==0){
				data1.setCurrVal(ratioFinaDatas.get((i*4)+0).doubleValue());
				data2.setCurrVal(ratioFinaDatas.get((i*4)+1).doubleValue());
				data3.setCurrVal(ratioFinaDatas.get((i*4)+2).doubleValue());
				data4.setCurrVal(ratioFinaDatas.get((i*4)+3).doubleValue());
			}else if(i==1){
				data1.setOneYearVal(ratioFinaDatas.get((i*4)+0).doubleValue());
				data2.setOneYearVal(ratioFinaDatas.get((i*4)+1).doubleValue());
				data3.setOneYearVal(ratioFinaDatas.get((i*4)+2).doubleValue());
				data4.setOneYearVal(ratioFinaDatas.get((i*4)+3).doubleValue());
			}else if(i==2){
				data1.setTwoYearVal(ratioFinaDatas.get((i*4)+0).doubleValue());
				data2.setTwoYearVal(ratioFinaDatas.get((i*4)+1).doubleValue());
				data3.setTwoYearVal(ratioFinaDatas.get((i*4)+2).doubleValue());
				data4.setTwoYearVal(ratioFinaDatas.get((i*4)+3).doubleValue());
			}
		}
		finaDataList.add(data1);
		finaDataList.add(data2);
		finaDataList.add(data3);
		finaDataList.add(data4);
		return finaDataList;
	}

	@Override
	public List<FcFinaData> getManuscriptFinaData(String xdCustNo, String currDate, String oneYearDate,
			String twoYearDate) throws Exception {
		List<BigDecimal> manuFinaDatas = lnlnslnsDao.getManuscriptFinaData(xdCustNo, currDate, oneYearDate, twoYearDate);
		if(CollectionUtils.isEmpty(manuFinaDatas)){
			return null;
		}
		FcFinaData data1 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "销售", 0d, 0d, 0d, 1);
		FcFinaData data2 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "净利润", 0d, 0d, 0d, 2);
		
		FcFinaData data3 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "净现金流量", 0d, 0d, 0d, 9);
		FcFinaData data4 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "经营性净现金流量", 0d, 0d, 0d, 10);
		FcFinaData data5 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "投资活动净现金流量", 0d, 0d, 0d, 11);
		FcFinaData data6 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "筹资活动净现金流量", 0d, 0d, 0d, 12);
		
		FcFinaData data7 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "流动比率", 0d, 0d, 0d, 3);
		FcFinaData data8 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "资产负债率", 0d, 0d, 0d, 4);
		FcFinaData data9 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "销售利润率", 0d, 0d, 0d, 5);
		FcFinaData data10 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "应收账款周转率", 0d, 0d, 0d, 7);
		FcFinaData data11 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "存货周转率", 0d, 0d, 0d, 8);
		FcFinaData data12 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "资产利润率", 0d, 0d, 0d, 6);
		//有数据在期数
		int size = manuFinaDatas.size()/12;
		for(int i = 0; i < size; i++){
			if(i==0){
				data1.setCurrVal(manuFinaDatas.get((i*12)+0).doubleValue()/10000);
				data2.setCurrVal(manuFinaDatas.get((i*12)+1).doubleValue()/10000);
				data3.setCurrVal(manuFinaDatas.get((i*12)+2).doubleValue()/10000);
				data4.setCurrVal(manuFinaDatas.get((i*12)+3).doubleValue()/10000);
				data5.setCurrVal(manuFinaDatas.get((i*12)+4).doubleValue()/10000);
				data6.setCurrVal(manuFinaDatas.get((i*12)+5).doubleValue()/10000);
				data7.setCurrVal(manuFinaDatas.get((i*12)+6).doubleValue());
				data8.setCurrVal(manuFinaDatas.get((i*12)+7).doubleValue());
				data9.setCurrVal(manuFinaDatas.get((i*12)+8).doubleValue());
				data10.setCurrVal(manuFinaDatas.get((i*12)+9).doubleValue());
				data11.setCurrVal(manuFinaDatas.get((i*12)+10).doubleValue());
				data12.setCurrVal(manuFinaDatas.get((i*12)+11).doubleValue());
			}else if(i==1){
				data1.setOneYearVal(manuFinaDatas.get((i*12)+0).doubleValue()/10000);
				data2.setOneYearVal(manuFinaDatas.get((i*12)+1).doubleValue()/10000);
				data3.setOneYearVal(manuFinaDatas.get((i*12)+2).doubleValue()/10000);
				data4.setOneYearVal(manuFinaDatas.get((i*12)+3).doubleValue()/10000);
				data5.setOneYearVal(manuFinaDatas.get((i*12)+4).doubleValue()/10000);
				data6.setOneYearVal(manuFinaDatas.get((i*12)+5).doubleValue()/10000);
				data7.setOneYearVal(manuFinaDatas.get((i*12)+6).doubleValue());
				data8.setOneYearVal(manuFinaDatas.get((i*12)+7).doubleValue());
				data9.setOneYearVal(manuFinaDatas.get((i*12)+8).doubleValue());
				data10.setOneYearVal(manuFinaDatas.get((i*12)+9).doubleValue());
				data11.setOneYearVal(manuFinaDatas.get((i*12)+10).doubleValue());
				data12.setOneYearVal(manuFinaDatas.get((i*12)+11).doubleValue());
			}else if(i==2){
				data1.setTwoYearVal(manuFinaDatas.get((i*12)+0).doubleValue()/10000);
				data2.setTwoYearVal(manuFinaDatas.get((i*12)+1).doubleValue()/10000);
				data3.setTwoYearVal(manuFinaDatas.get((i*12)+2).doubleValue()/10000);
				data4.setTwoYearVal(manuFinaDatas.get((i*12)+3).doubleValue()/10000);
				data5.setTwoYearVal(manuFinaDatas.get((i*12)+4).doubleValue()/10000);
				data6.setTwoYearVal(manuFinaDatas.get((i*12)+5).doubleValue()/10000);
				data7.setTwoYearVal(manuFinaDatas.get((i*12)+6).doubleValue());
				data8.setTwoYearVal(manuFinaDatas.get((i*12)+7).doubleValue());
				data9.setTwoYearVal(manuFinaDatas.get((i*12)+8).doubleValue());
				data10.setTwoYearVal(manuFinaDatas.get((i*12)+9).doubleValue());
				data11.setTwoYearVal(manuFinaDatas.get((i*12)+10).doubleValue());
				data12.setTwoYearVal(manuFinaDatas.get((i*12)+11).doubleValue());
			}
		}
		List<FcFinaData> finaDataList = Lists.newArrayList(data1,data2,data3,data4,data5,data6,data7,data8,data9,data10,data11,data12);
		finaDataList.sort((a,b) -> a.getSortIndex()-b.getSortIndex());
		return finaDataList;
	}

	@Override
	public List<FcFinaData> getCmGuarFinaData(String xdCustNo, String currDate, String oneYearDate,
			String twoYearDate) throws Exception {
		List<FcFinaData> finaDataList = new ArrayList<>();
		List<BigDecimal> ratioFinaDatas = lnlnslnsDao.getCashFlowFinaData(xdCustNo, currDate, oneYearDate, twoYearDate);
		if(CollectionUtils.isEmpty(ratioFinaDatas)){
			return null;
		}
		
		//资产总计 165		负债合计 246	所有者权益 266	营业收入 301 	利润总额  329
		FcFinaData data1 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "资产总计", 0d, 0d, 0d, 1);
		FcFinaData data2 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "负债合计", 0d, 0d, 0d, 2);
		FcFinaData data3 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "营业收入", 0d, 0d, 0d, 3);
		FcFinaData data4 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "利润总额", 0d, 0d, 0d, 4);
		//有数据在期数
		int size = ratioFinaDatas.size()/4;
		for(int i = 0; i < size; i++){
			if(i==0){
				data1.setCurrVal(ratioFinaDatas.get((i*4)+0).doubleValue());
				data2.setCurrVal(ratioFinaDatas.get((i*4)+1).doubleValue());
				data3.setCurrVal(ratioFinaDatas.get((i*4)+2).doubleValue());
				data4.setCurrVal(ratioFinaDatas.get((i*4)+3).doubleValue());
			}else if(i==1){
				data1.setOneYearVal(ratioFinaDatas.get((i*4)+0).doubleValue());
				data2.setOneYearVal(ratioFinaDatas.get((i*4)+1).doubleValue());
				data3.setOneYearVal(ratioFinaDatas.get((i*4)+2).doubleValue());
				data4.setOneYearVal(ratioFinaDatas.get((i*4)+3).doubleValue());
			}else if(i==2){
				data1.setTwoYearVal(ratioFinaDatas.get((i*4)+0).doubleValue());
				data2.setTwoYearVal(ratioFinaDatas.get((i*4)+1).doubleValue());
				data3.setTwoYearVal(ratioFinaDatas.get((i*4)+2).doubleValue());
				data4.setTwoYearVal(ratioFinaDatas.get((i*4)+3).doubleValue());
			}
		}
		finaDataList.add(data1);
		finaDataList.add(data2);
		finaDataList.add(data3);
		finaDataList.add(data4);
		return finaDataList;
	}
	
	@Override
	public GuarBaseDto getGuarInfo(String xdCustNo) throws Exception{
		return lnlnslnsDao.getGuarCustInfo(xdCustNo);
//		if(guarCustInfo==null){
//			log.info("信贷客户号为：【"+ xdCustNo + "】不存在担保人信息。");
//			return;
//		}
//		String custType = guarCustInfo.getCustType();
////		//对公对私客户类型   对应的财报都是放同一个表的
//		List<FcFinaData> cmGuarFinaData = this.getCmGuarFinaData(xdCustNo, currDate, oneYearDate, twoYearDate);
	}


	@Override
	public FmContractDto getContractInfo(String xdCustNo) throws Exception {
		FmContractDto dto = new FmContractDto();
		List<CmisContractDto> contractList = lnlnslnsDao.getContractInfo(xdCustNo);
		if(CollectionUtils.isEmpty(contractList)){
			log.info("信贷客户号为：【"+ xdCustNo + "】不存在授信业务背景资料信息。");
			return null;
		}
		//授信申请信息
		CmisContractDto cmis = contractList.get(0);
		dto.setBusinesssum(cmis.getBusinesssum());
		dto.setSerialNo(cmis.getSerialNo());
		dto.setRelativeSerialno(cmis.getRelativeSerialno());
		dto.setPurpose(cmis.getPurpose());
		dto.setPaysource(cmis.getPaysource());
		dto.setMaturity(cmis.getMaturity());
		dto.setCreditcondition(cmis.getCreditcondition());
		dto.setClassifyResult(cmis.getClassifyResult());
		dto.setClassifyDate(cmis.getClassifyDate());
		
		Map<String, List<CmisContractDto>> map = contractList.stream()
				.filter(e -> e!=null)
				.filter(e -> !StringUtils.isEmpty(e.getGuarType()))
				.collect(Collectors.groupingBy(CmisContractDto::getGuarType));
		for(Map.Entry<String, List<CmisContractDto>> entry : map.entrySet()){
			String key = entry.getKey();
			List<CmisContractDto> value = entry.getValue();
			if(key.startsWith("01")){//保证
				CmisContractDto guar = value.get(0);
				dto.setgContracttype(guar.getContracttype());
				dto.setgEnddate(guar.getEnddate());
				dto.setgValue(guar.getGuarantyValue());
			}else if(key.startsWith("05")){//抵押
				Double mValueGroud = 0d;//抵押物-土地价值
				Double mValueHouse = 0d;//抵押物-房屋价值
				Double mValueEquip = 0d;//抵押物-设备价值
				Double mValueVehicle = 0d;//抵押物-运输工具价值
				Double mValueOther = 0d;//抵押物-其他价值
				String mRegisterOrg = "否";
				for(CmisContractDto mort : value){
					String guarantyType = mort.getGuarantyType();
					Double confirmValue = mort.getConfirmValue();
					String guarantyregOrg = mort.getGuarantyregOrg();
					//判断是否存在登记机关
					if(!StringUtils.isEmpty(guarantyregOrg)){
						mRegisterOrg = "是";
					}
					//010开头有一整套  抵押类型。   020开头也有。  用或符号连接
					if(guarantyType.startsWith("010010") || guarantyType.startsWith("020100") || guarantyType.startsWith("020200")){
						//抵押-房产
						mValueHouse += confirmValue;
					}else if(guarantyType.startsWith("010020") || guarantyType.startsWith("020300") || guarantyType.startsWith("020400")){
						//抵押-土地
						mValueGroud += confirmValue;
					}else if(guarantyType.startsWith("010030") || guarantyType.startsWith("020500")
							|| guarantyType.startsWith("020600")|| guarantyType.startsWith("020720")){
						//抵押-交通运输工具
						mValueVehicle += confirmValue;
					}else if(guarantyType.startsWith("010040") || guarantyType.startsWith("020740")){
						//抵押-设备
						mValueEquip += confirmValue;
					}else {
						//抵押-其他
						mValueOther += confirmValue;
					}
					dto.setmContracttype(mort.getContracttype());
				}
				dto.setmValueEquip(mValueEquip);
				dto.setmValueGroud(mValueGroud);
				dto.setmValueHouse(mValueHouse);
				dto.setmValueOther(mValueOther);
				dto.setmValueVehicle(mValueVehicle);
				dto.setmRegisterOrg(mRegisterOrg);
			}else if(key.startsWith("06")){//质押
				List<String> pledgeList = new ArrayList();
				Double guarValueSum = 0d;
				for(CmisContractDto pled : value){
					String guarantyName = pled.getGuarantyName();
					Double confirmValue = pled.getConfirmValue();
					guarValueSum += confirmValue;
					if(!pledgeList.contains(guarantyName)){
						pledgeList.add(guarantyName);
					}
					//默认取最后一次的期限
					dto.setpEnddate(pled.getEnddate());
				}
				dto.setpValue(guarValueSum);
				dto.setpName(StringUtils.join(pledgeList,","));
			}else{//其他担保方式
				log.warn("信贷客户号为：【"+ xdCustNo + "】 担保类型：【"+ key +"】----跳过该担保类型数据");
				continue;
			}
		}
		return dto;
	}
	

	/**
	 * 通过合同流水号  获取借据信息--》用信情况
	 * 获取最近2笔数据     包括用信时间、金额
	 * @param serialNo
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<CmisBillDto> getBillInfo(String serialNo) throws Exception {
		return lnlnslnsDao.getBillInfo(serialNo);
	}


	@Override
	public List<CmisContractDto> getContractList(String xdCustNo) throws Exception {
		return lnlnslnsDao.getContractList(xdCustNo);
	}

	
	
	
	/**
	 * 压缩  五级分类认定表中的合同信息
	 * 将一对多信息整合为一条记录
	 * @param ContractDtoList
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<CmisContractDto> getContractListCompress(List<CmisContractDto> contractList) throws Exception{
		if(CollectionUtils.isEmpty(contractList)){
			log.info("待压缩的合同列表为空");
			return null;
		}
		
		List<CmisContractDto> resultList = new ArrayList<CmisContractDto>();
		
		Map<String, List<CmisContractDto>> map = contractList.stream().collect(Collectors.groupingBy(CmisContractDto::getSerialNo));
		for(Map.Entry<String, List<CmisContractDto>> entry : map.entrySet()){
			
			List<CmisContractDto> value = entry.getValue();
			CmisContractDto commonPart = value.get(0);
			
			CmisContractDto result = new CmisContractDto();
			result.setSerialNo(commonPart.getSerialNo());
			result.setVouchtype(commonPart.getVouchtype());
			result.setContractSum(commonPart.getContractSum());
			result.setBalance(commonPart.getBalance());
			result.setOccurdate(commonPart.getOccurdate());
			result.setMaturity(commonPart.getMaturity());
			result.setPurpose(commonPart.getPurpose());
			result.setPurposeadd(commonPart.getPurposeadd());
			result.setOverduebalance(commonPart.getOverduebalance());
			result.setInterestbalance1(commonPart.getInterestbalance1());
			
			List<String> guarantorNameList = new ArrayList<String>();
			List<String> guarantyNameList = new ArrayList<String>();
			Double confirmValueGather = 0d;
			Double gurantyRateGather = 0d;
			Double gurantyRateDenominator = 0d;
			String registerOrg = "否";
			
			for(CmisContractDto mort : value){
				String guarantyregOrg = mort.getGuarantyregOrg();
				String guarantorName = mort.getGuarantorName();
				String guarantyName = mort.getGuarantyName();
				//判断是否存在登记机关
				if(!StringUtils.isEmpty(guarantyregOrg)){
					registerOrg = "是";
				}
				//担保人名称
				if(!guarantorNameList.contains(guarantorName)){
					guarantorNameList.add(guarantorName);
				}
				//担保物名称
				if(!guarantyNameList.contains(guarantyName)){
					guarantyNameList.add(guarantyName);
				}
				
				if(mort.getConfirmValue() != null){
					//担保物总价值
					confirmValueGather += mort.getConfirmValue();
					//抵质押率比率分母
					gurantyRateDenominator += mort.getConfirmValue();
				}
			}
			//gurantyRateGather = (double) (Math.round((result.getContractSum() / gurantyRateDenominator)*100))/100;
			gurantyRateGather = result.getContractSum() / gurantyRateDenominator;
			result.setGuarantorNameGather(StringUtils.join(guarantorNameList,","));
			result.setGuarantyNameGather(StringUtils.join(guarantyNameList,","));
			result.setConfirmValueGather(confirmValueGather);
			result.setGurantyRateGather(gurantyRateGather);
			result.setRegisterOrg(registerOrg);
			resultList.add(result);
		}
		return resultList;
	}
	
	/**
	 * 认定表中的合同信息
	 * 合计部分数据整合
	 * @param ContractDtoList
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<Double> getContractListSum(List<CmisContractDto> contractListCompress)throws Exception{
		if(CollectionUtils.isEmpty(contractListCompress)){
			log.info("压缩的合同列表为空");
			return null;
		}
		Double contractSumAdd = 0d;
		Double balanceSum = 0d;
		Double confirmValueSum = 0d;
		Double gurantyRateGatherSum = 0d;
		Double overdueBalanceSum = 0d;
		Double interestBalanceSum = 0d;
		List<Double> sumResult = new ArrayList<Double>();
		for(CmisContractDto more : contractListCompress){
			contractSumAdd += more.getContractSum()==null ? 0 : more.getContractSum();
			
			balanceSum += more.getBalance()==null ? 0 : more.getBalance();
			confirmValueSum += more.getConfirmValueGather()==null ? 0 : more.getConfirmValueGather();
			overdueBalanceSum += more.getOverduebalance()==null ? 0 : more.getOverduebalance();
			interestBalanceSum += more.getInterestbalance1()==null ? 0 : more.getInterestbalance1();
		}
		gurantyRateGatherSum = contractSumAdd / confirmValueSum;
		
		sumResult.add(contractSumAdd);
		sumResult.add(balanceSum);
		sumResult.add(confirmValueSum);
		sumResult.add(gurantyRateGatherSum);
		sumResult.add(overdueBalanceSum);
		sumResult.add(interestBalanceSum);
		
		return sumResult;
	}
	


	@Override
	public CmisCustStockDto getCustStock(String xdCustNo) throws Exception {
		// TODO Auto-generated method stub
		//获取客户的所有抵押物信息
		List<GuarantInfoDto> guarantInfoList = lnlnslnsDao.getCustGuarantInfo(xdCustNo);
		if(CollectionUtils.isEmpty(guarantInfoList))
			return null;
		
		CmisCustStockDto stockDto = new CmisCustStockDto();
		stockDto.setXdCustNo(xdCustNo);
		
		Integer landActualNum = 0;//实际数量
		Integer landCertifNum = 0;//已办证数量
		Double landAssessVal = 0d;//评估价值
		//房产   2
		Integer houseActualNum = 0;//实际数量
		Integer houseCertifNum = 0;//已办证数量
		Double houseAssessVal = 0d;//评估价值
		//设备及交通工具  3
		Integer equipActualNum = 0;//实际数量
		Integer equipCertifNum = 0;//已办证数量
		Double equipAssessVal = 0d;//评估价值
		
		
		for(GuarantInfoDto guarInfo : guarantInfoList){
			String guarantyType = guarInfo.getGuarantytype();
			Double confirmValue = guarInfo.getConfirmvalue();
			String guarantyregorg = guarInfo.getGuarantyregorg();
			//010开头有一整套  抵押类型。   020开头也有。  用或符号连接
			if(guarantyType.startsWith("010010") || guarantyType.startsWith("020100") || guarantyType.startsWith("020200")){
				//抵押-房产
				houseActualNum++;
				houseAssessVal += confirmValue;
				if(!StringUtils.isEmpty(guarantyregorg)){
					houseCertifNum ++;
				}
			}else if(guarantyType.startsWith("010020") || guarantyType.startsWith("020300") || guarantyType.startsWith("020400")){
				//抵押-土地
				landActualNum++;
				landAssessVal += confirmValue;
				if(!StringUtils.isEmpty(guarantyregorg)){
					landCertifNum ++;
				}
			}else if(guarantyType.startsWith("010030") || guarantyType.startsWith("020500")
					|| guarantyType.startsWith("020600")|| guarantyType.startsWith("020720")
					|| guarantyType.startsWith("010040") || guarantyType.startsWith("020740")){
				//抵押-设备 和   抵押-交通运输工具
				equipActualNum++;
				equipAssessVal += confirmValue;
				if(!StringUtils.isEmpty(guarantyregorg)){
					equipCertifNum ++;
				}
			}else {
				//抵押-其他 -------->暂时不需要这块数据
			}
		}
		
		stockDto.setLandActualNum(landActualNum);
		stockDto.setLandCertifNum(landCertifNum);
		stockDto.setLandAssessVal(landAssessVal);
		stockDto.setLandMortgageStatus(landActualNum > 0 ? "是" : "否" );
		stockDto.setHouseActualNum(houseActualNum);
		stockDto.setHouseCertifNum(houseCertifNum);
		stockDto.setHouseAssessVal(houseAssessVal);
		stockDto.setHouseMortgageStatus(houseActualNum > 0 ? "是" : "否" );
		stockDto.setEquipActualNum(equipActualNum);
		stockDto.setEquipCertifNum(equipCertifNum);
		stockDto.setEquipAssessVal(equipAssessVal);
		stockDto.setEquipMortgageStatus(equipActualNum > 0 ? "是" : "否" );
		
		return stockDto;
	}


	@Override
	public CmisContractDto getLastOneContractInfo(String xdCustNo) throws Exception {
		return lnlnslnsDao.getLastOneContractInfo(xdCustNo);
	}
	

	@Override
	public List<FcFinaData> getSmallFinaData(String xdCustNo, String currDate, String oneYearDate,
			String twoYearDate) throws Exception {
		List<FcFinaData> finaDataList = new ArrayList<>();
		List<BigDecimal> ratioFinaDatas = lnlnslnsDao.getSmallFinaData(xdCustNo, currDate, oneYearDate, twoYearDate);
		if(CollectionUtils.isEmpty(ratioFinaDatas)){
			return null;
		}
		FcFinaData data1 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "资产总计", 0d, 0d, 0d, 1);
		FcFinaData data2 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "负债合计", 0d, 0d, 0d, 2);
		FcFinaData data3 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "营业利润", 0d, 0d, 0d, 3);
		FcFinaData data4 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "净利润", 0d, 0d, 0d, 4);
		FcFinaData data5 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "净现金流量", 0d, 0d, 0d, 5);
		FcFinaData data6 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "资产负债率", 0d, 0d, 0d, 6);
		FcFinaData data7 = new FcFinaData(Constants.FINA_MOLD.CASHFLOW, "销售利润率", 0d, 0d, 0d, 7);
		//有数据在期数
		int size = ratioFinaDatas.size()/7;
		for(int i = 0; i < size; i++){
			if(i==0){
				data1.setCurrVal(ratioFinaDatas.get((i*7)+0).doubleValue());
				data2.setCurrVal(ratioFinaDatas.get((i*7)+1).doubleValue());
				data3.setCurrVal(ratioFinaDatas.get((i*7)+2).doubleValue());
				data4.setCurrVal(ratioFinaDatas.get((i*7)+3).doubleValue());
				data5.setCurrVal(ratioFinaDatas.get((i*7)+4).doubleValue());
				data6.setCurrVal(ratioFinaDatas.get((i*7)+5).doubleValue());
				data7.setCurrVal(ratioFinaDatas.get((i*7)+6).doubleValue());
			}else if(i==1){
				data1.setOneYearVal(ratioFinaDatas.get((i*7)+0).doubleValue());
				data2.setOneYearVal(ratioFinaDatas.get((i*7)+1).doubleValue());
				data3.setOneYearVal(ratioFinaDatas.get((i*7)+2).doubleValue());
				data4.setOneYearVal(ratioFinaDatas.get((i*7)+3).doubleValue());
				data5.setOneYearVal(ratioFinaDatas.get((i*7)+4).doubleValue());
				data6.setOneYearVal(ratioFinaDatas.get((i*7)+5).doubleValue());
				data7.setOneYearVal(ratioFinaDatas.get((i*7)+6).doubleValue());
			}else if(i==2){
				data1.setTwoYearVal(ratioFinaDatas.get((i*7)+0).doubleValue());
				data2.setTwoYearVal(ratioFinaDatas.get((i*7)+1).doubleValue());
				data3.setTwoYearVal(ratioFinaDatas.get((i*7)+2).doubleValue());
				data4.setTwoYearVal(ratioFinaDatas.get((i*7)+3).doubleValue());
				data5.setTwoYearVal(ratioFinaDatas.get((i*7)+4).doubleValue());
				data6.setTwoYearVal(ratioFinaDatas.get((i*7)+5).doubleValue());
				data7.setTwoYearVal(ratioFinaDatas.get((i*7)+6).doubleValue());
			}
		}
		finaDataList.add(data1);
		finaDataList.add(data2);
		finaDataList.add(data3);
		finaDataList.add(data4);
		finaDataList.add(data5);
		finaDataList.add(data6);
		finaDataList.add(data7);
		return finaDataList;
	}
	
	@Override
	public void syncCompFinanceInfo(String serNo){
		//获取该流水任务下所有担保企业
		List<GuaCompany> guaCompanyList = guaCompanyService.selectByProperty("serNo", serNo);
		
			if(!CollectionUtils.isEmpty(guaCompanyList)){
				for(GuaCompany guaCompany : guaCompanyList){
					//获取担保企业的企业担保信息(为了获取担保企业的XdCustNo)
					CusBase cusBase = cusBaseSV.queryByCusName(guaCompany.getCompanyName());
					//获取担保企业的财务报表中的相关数据
					this.saveCompFinanceInfo(cusBase.getXdCustomerNo(), guaCompany);
				}
		
			}
	}
	
	@Override
	public void saveCompFinanceInfo(String XdCustNo,GuaCompany guaCompany){
		
		List<CompFinanceTransfer> compFinnaceTransferList = lnlnslnsDao.getCompFinnaceTransfer(XdCustNo);
		Map<String, Object> params = new HashMap<>();
		params.put("id", guaCompany.getId());
		if(!CollectionUtils.isEmpty(compFinnaceTransferList) && guaCompany != null){
			Map<String, List<CompFinanceTransfer>> map = compFinnaceTransferList.stream()
					.collect(Collectors.groupingBy(CompFinanceTransfer::getDate));
			for(String key : map.keySet()){
				List<CompFinanceTransfer> list = map.get(key);
				if(CollectionUtils.isEmpty(list)){
					continue;
				}
				CompFinanceInfo compFinanceInfo = new CompFinanceInfo();
				for(CompFinanceTransfer transfer : list){
					if(transfer.getRowSubject().equals("165")){
						compFinanceInfo.setAssetsSum(transfer.getValue());//总资产
					}else if(transfer.getRowSubject().equals("246")){
						compFinanceInfo.setDebtSum(transfer.getValue());//总负债
					}else if(transfer.getRowSubject().equals("266")){
						compFinanceInfo.setNetAssets(transfer.getValue());//净资产
					}else if(transfer.getRowSubject().equals("301")){
						compFinanceInfo.setSalesRevenue(transfer.getValue());//营业收入
					}else if(transfer.getRowSubject().equals("329")){
						compFinanceInfo.setNetProfit(transfer.getValue());//利润总额
					}else if(transfer.getRowSubject().equals("612")){
						compFinanceInfo.setAssetDebtRate(transfer.getValue());//资产负债率
					}	
				}
				compFinanceInfo.setSerNo(guaCompany.getSerNo());
				compFinanceInfo.setGuaCompanyId(guaCompany.getId());
				compFinanceInfo.setYears(key);
				compFinanceInfo.setCreater("cmis");
				compFinanceInfo.setCreateTime(DateUtil.getNowTimestamp());
				compFinanceInfo.setUpdater("CMIS");
				compFinanceInfo.setUpdateTime(DateUtil.getNowTimestamp());
				compFinanceInfoService.saveOrUpdate(compFinanceInfo);
				
			}
			
		}
	}
	
	
	@Override
	public void syncGuaCompany(String xdCustNo,String serNo){
		
		List<GuaCompany> guaCompanyList = lnlnslnsDao.getGuaCompany(xdCustNo);
		
		if(!CollectionUtils.isEmpty(guaCompanyList)){
			for(GuaCompany guaCompany : guaCompanyList){
				
				guaCompany.setSerNo(serNo);
				//数据库为'是否保险'字段(GUARANTYTYPE),存入代码值为01开头:保证,05开头:抵押,06开头:质押,X:未说明
				if(guaCompany.getGuaranteeWay().contains("01")){
					guaCompany.setGuaranteeWay("C");//保证
				}else if(guaCompany.getGuaranteeWay().contains("05")){
					guaCompany.setGuaranteeWay("B");//抵押
				}else if(guaCompany.getGuaranteeWay().contains("06")){
					guaCompany.setGuaranteeWay("A");//质押
				}else{
					guaCompany.setGuaranteeWay("Z");//其他
				}
				
				guaCompany.setCreater("cmis");
				guaCompany.setCreateTime(DateUtil.getNowTimestamp());
				guaCompany.setUpdater("CMIS");
				guaCompany.setUpdateTime(DateUtil.getNowTimestamp());
				
				guaCompanyService.saveOrUpdate(guaCompany);
				
			}
		}
		
	}
	
	@Override
	public void syncGuaPerson(String xdCustNo,String serNo){
		
	
		List<GuaPerson> guaPersonList = lnlnslnsDao.getGuaPerson(xdCustNo);
		
		if(!CollectionUtils.isEmpty(guaPersonList)){
			for(GuaPerson guaPerson : guaPersonList){
				
				guaPerson.setSerNo(serNo);
				
				//数据库为'是否保险'字段(GUARANTYTYPE),存入代码值为01开头:保证,05开头:抵押,06开头:质押,X:未说明
				if(guaPerson.getGuaranteeWay().contains("01")){
					guaPerson.setGuaranteeWay("C");//保证
				}else if(guaPerson.getGuaranteeWay().contains("05")){
					guaPerson.setGuaranteeWay("B");//抵押
				}else if(guaPerson.getGuaranteeWay().contains("06")){
					guaPerson.setGuaranteeWay("A");//质押
				}else{
					guaPerson.setGuaranteeWay("Z");//其他
				}
				
				guaPerson.setCreater("cmis");
				guaPerson.setCreateTime(DateUtil.getNowTimestamp());
				guaPerson.setUpdater("CMIS");
				guaPerson.setUpdateTime(DateUtil.getNowTimestamp());
				
				guaPersonService.saveOrUpdate(guaPerson);
				
			}
		}
		
	}
	

	@Override
	public void syncGuaEstate(String xdCustNo,String serNo){
	
		List<GuaEstate> guaEstateList = lnlnslnsDao.getGuaEstate(xdCustNo);
		
		if(!CollectionUtils.isEmpty(guaEstateList)){
			for(GuaEstate guaEstate : guaEstateList){			
				guaEstate.setSerNo(serNo);
				//数据库为'是否保险'字段(ISASSURE),存入代码值为Y:是,N:否,X:空值未提供代码值
				if(guaEstate.getIsHandleInsurance().contains("Y")){
					guaEstate.setIsHandleInsurance("是");
				}else if(guaEstate.getIsHandleInsurance().contains("N")){
					guaEstate.setIsHandleInsurance("否");
				}else{
					guaEstate.setIsHandleInsurance("未知状态");
				}
				//数据库为'是否出租'字段(RENTORNOT),存入代码值为Y:是,N:否,X:空值未提供代码值
				if(guaEstate.getUseOrRent().contains("Y")){
					guaEstate.getUseOrRent().contains("出租");
				}else if(guaEstate.getUseOrRent().contains("N")){
					guaEstate.getUseOrRent().contains("自用");
				}else{
					guaEstate.getUseOrRent().contains("未知");
				}
				
				guaEstate.setCreater("cmis");
				guaEstate.setCreateTime(DateUtil.getNowTimestamp());
				guaEstate.setUpdater("CMIS");
				guaEstate.setUpdateTime(DateUtil.getNowTimestamp());
				
				guaEstateService.saveOrUpdate(guaEstate);
				
			}
		}
	}
	
	@Override
	public void syncGuaChattel(String xdCustNo,String serNo){
	
		List<GuaChattel> guaChattelList = lnlnslnsDao.getGuaChattel(xdCustNo);
		
		if(!CollectionUtils.isEmpty(guaChattelList)){
			for(GuaChattel guaChattel : guaChattelList){			
				guaChattel.setSerNo(serNo);
				//数据库为'是否保险'字段(ISASSURE),存入代码值为Y:是,N:否,X:空值未提供代码值
				if(guaChattel.getIsHandleInsurance().contains("Y")){
					guaChattel.setIsHandleInsurance("是");
				}else if(guaChattel.getIsHandleInsurance().contains("N")){
					guaChattel.setIsHandleInsurance("否");
				}else{
					guaChattel.setIsHandleInsurance("未知状态");
				}
			
				guaChattel.setCreater("cmis");
				guaChattel.setCreateTime(DateUtil.getNowTimestamp());
				guaChattel.setUpdater("CMIS");
				guaChattel.setUpdateTime(DateUtil.getNowTimestamp());
				
				guaChattelService.saveOrUpdate(guaChattel);
				
			}
		}
	}
	
	@Override
	public void syncGuaAccounts(String xdCustNo,String serNo){
	
		List<GuaAccounts> guaAccountsList = lnlnslnsDao.getGuaAccounts(xdCustNo);
		
		if(!CollectionUtils.isEmpty(guaAccountsList)){
			for(GuaAccounts guaAccounts : guaAccountsList){			
				guaAccounts.setSerNo(serNo);
				
				guaAccounts.setNetAmount(guaAccounts.getAmount());//金额和净额存入同值
				guaAccounts.setCreater("cmis");
				guaAccounts.setCreateTime(DateUtil.getNowTimestamp());
				guaAccounts.setUpdater("CMIS");
				guaAccounts.setUpdateTime(DateUtil.getNowTimestamp());
				
				guaAccountsService.saveOrUpdate(guaAccounts);
				
			}
		}
	}
	
	@Override
	public void syncGuaDepositReceipt(String xdCustNo,String serNo){
	
		List<GuaDepositReceipt> guaDepositReceiptList = lnlnslnsDao.getGuaDepositReceipt(xdCustNo);
		
		if(!CollectionUtils.isEmpty(guaDepositReceiptList)){
			for(GuaDepositReceipt guaDepositReceipt : guaDepositReceiptList){			
				guaDepositReceipt.setSerNo(serNo);
				
				guaDepositReceipt.setCreater("cmis");
				guaDepositReceipt.setCreateTime(DateUtil.getNowTimestamp());
				guaDepositReceipt.setUpdater("CMIS");
				guaDepositReceipt.setUpdateTime(DateUtil.getNowTimestamp());
				
				guaDepositReceiptService.saveOrUpdate(guaDepositReceipt);
				
			}
		}
	}


	@Override
	public void syncExtGuara(String xdCustNo, Long taskId, String customerNo) {
		
		String endTime = DateUtil.getNowYmdStr();
		if(StringUtils.isEmpty(xdCustNo)){
			log.info("对公客户号为：----" + customerNo + "-----不存在信贷客户号，同步对外担保信息结束。");
		}
		List<CompFinancingExtGuara> guarantyContract = businessContractDao.getGuarantyContract(xdCustNo, endTime);
		if(CollectionUtils.isEmpty(guarantyContract)){
			log.info("信贷系统中，客户号为：" + xdCustNo + "无对外担保信息");
			return ;
		}
		for(CompFinancingExtGuara extGuara : guarantyContract){
			Double guaranteeAmt = extGuara.getGuaranteeAmt();
			if(guaranteeAmt!=null){
				extGuara.setGuaranteeAmt(guaranteeAmt/10000);
			}
			Double guaranteeBal = extGuara.getGuaranteeBal();
			if(guaranteeBal!=null){
				extGuara.setGuaranteeBal(guaranteeBal/10000);
			}
			extGuara.setFiveClass(this.formatClassifyresult(extGuara.getFiveClass()));
			extGuara.setAntiGuaranteeWay(this.formatGuarType(extGuara.getAntiGuaranteeWay()));
			extGuara.setTaskId(taskId);
			extGuara.setCustomerNo(customerNo);
			compFinancingExtGuaraDAO.saveOrUpdate(extGuara);
		}
	}
	
	/**
	 * 将信贷在五级分类字典转化为为本地在字典
	 * @param classify
	 * @return
	 */
	private String formatClassifyresult(String classify){
		if(!StringUtils.isEmpty(classify)){
			if(classify.startsWith("QL01")){
				return "10";
			}else if(classify.startsWith("QL02")){
				return "20";
			}else if(classify.startsWith("QL03")){
				return "30";
			}else if(classify.startsWith("QL04")){
				return "40";
			}else if(classify.startsWith("QL05")){
				return "50";
			}else {
				return "00";
			}
		}
		return "00";
	}

	/**
	 * 将信贷在担保方式字典转化为为本地字典
	 * @param classify
	 * @return
	 */
	private String formatGuarType(String guarType){
		if(StringUtils.isEmpty(guarType)){
			return null;
		}
		if(guarType.startsWith("010")){
			return "C";
		}else if(guarType.startsWith("050")){
			return "B";
		}else if(guarType.startsWith("060")){
			return "A";
		}else {
			return "Z";
		}
		
	}


	@Override
	public void syncCompConnetCm(String xdCustNo, String customerNo) {
		if(StringUtil.isEmptyString(customerNo)){
			log.error("对公客户号为空，同步关联企业失败");
			return ;
		}
		if(StringUtil.isEmptyString(xdCustNo)){
			log.error("信贷客户号为空，同步关联企业失败");
			return ;
		}
		
		List<CompConnetCm> connectList = businessContractDao.getCustomerRelative(xdCustNo);
		if(CollectionUtils.isEmpty(connectList)){
			log.info("信贷系统中，信贷客户号为：" + xdCustNo + "无公司类关联信息");
			return ;
		}
		Map<String, Object> params = new HashMap<>();
		params.put("customerNo", customerNo);
		CompBase compBase = null;
		for(CompConnetCm conn : connectList){
			//集团授信 关联企业都是没有工商信息
			//同步企查查数据
			try {

				String companyName = conn.getCompanyName();
				if(StringUtil.isEmptyString(companyName)){
					log.info("信贷系统中，关联企业名称为空");
					continue;
				}
				CompBase cm = compBaseDAO.queryByCustomerName(companyName);
				String cmNo = null;
				//判断当前客户是否在信贷系统中
				if(cm==null){
					User user = HttpUtil.getCurrentUser();
					CusBase cusBase = new CusBase();
					cusBase.setCustomerName(companyName);
					cusBase.setCustomerNo(StringUtil.genRandomNo());
					cusBase.setInputUserId(user.getUserId());
					cusBase.setInputOrgId(user.getAccOrgNo());
					cusBase.setManagerUserId(user.getUserId());
					cusBase.setManagerOrgId(user.getAccOrgNo());
					cusBase.setInputDate(DateUtil.getNowYmdStr());
					cusBase.setUpdateDate(DateUtil.getNowYmdStr());
					cusBase.setStatus(Constants.NORMAL);
					cusBase.setCreater(user.getUserId());
					cusBase.setUpdater(user.getUserId());
					cusBase.setCreateTime(DateUtil.getNowTimestamp());
					cusBase.setUpdateTime(DateUtil.getNowTimestamp());
					cusBaseSV.save(cusBase);
					
					cmNo = cusBase.getCustomerNo();
					//同步企查查数据
				}else {
					cmNo = cm.getCustomerNo();
				}
				
				synchronizationQCCService.syncQccAllData(cmNo);
				
				
			} catch (Exception e) {
				log.error("获取集团授信数据，同步企查查数据时异常，异常原因：" + e.getMessage());
				e.printStackTrace();
			}

			compBase = compBaseDAO.queryByCustomerName(conn.getCompanyName());
			if(compBase != null){
				params.put("companyName", conn.getCompanyName());
				List<CompConnetCm> existList = compConnectDAO.selectByProperty(params);
				
				String foundDate = "";
				if(!StringUtils.isEmpty(compBase.getFoundDate())){
					foundDate = compBase.getFoundDate();
					if(foundDate.length() > 10){
						foundDate = foundDate.substring(0, 10);
					}
				}
				if(!CollectionUtils.isEmpty(existList)){
					CompConnetCm exist = existList.get(0);
					exist.setLegalPerson(compBase.getLegalPerson());
					exist.setMainBusiness(compBase.getMainBusiness());
					exist.setRelationship(conn.getRelationship());
					exist.setRegisterCapital(compBase.getRegisterCapital()/10000);
					exist.setRealCapital(compBase.getRealCapital()/10000);
					exist.setFoundDate(foundDate);
					//exist.setUpdateTime(DateUtil.getNowTimestamp());
					compConnectDAO.saveOrUpdate(exist);
				}else{
					conn.setCustomerNo(customerNo);
					conn.setLegalPerson(compBase.getLegalPerson());
					conn.setMainBusiness(compBase.getMainBusiness());
	//				conn.setRelationship(conn.getRelationship());
					conn.setRegisterCapital(compBase.getRegisterCapital()/10000);
					conn.setRealCapital(compBase.getRealCapital()/10000);
					conn.setFoundDate(foundDate);
					//conn.setUpdateTime(DateUtil.getNowTimestamp());
					compConnectDAO.saveOrUpdate(conn);
				}
				
			}
		}
	}
}






