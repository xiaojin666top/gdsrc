package com.beawan.dtmp.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.dtmp.dao.BusinessContractDao;
import com.beawan.dtmp.entity.BusinessContract;
import com.beawan.dtmp.service.BusinessContractService;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.Resource;
import com.beawan.dtmp.entity.BusinessContract;

/**
 * @author yzj
 */
@Service("businessContractService")
public class BusinessContractServiceImpl extends BaseServiceImpl<BusinessContract> implements BusinessContractService{
    /**
    * 注入DAO
    */
    @Resource(name = "businessContractDao")
    public void setDao(BaseDao<BusinessContract> dao) {
        super.setDao(dao);
    }
    @Resource
    public BusinessContractDao businessContractDao;
    
	@Override
	public List<BusinessContract> getMatuityConListByTerm(String monthStart, String monthEnd) {
		// TODO Auto-generated method stub
		return businessContractDao.getMatuityConListByTerm(monthStart, monthEnd, null);
	}
}
