package com.beawan.dtmp.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.dtmp.dao.CockpitDataDao;
import com.beawan.dtmp.dto.BalByRateDto;
import com.beawan.dtmp.dto.CustSpreadByBalDto;
import com.beawan.dtmp.dto.DepositByDsDto;
import com.beawan.dtmp.dto.DepositByOrgDto;
import com.beawan.dtmp.dto.StyBalDto;
import com.beawan.dtmp.entity.CockpitData;
import com.beawan.dtmp.service.CockpitDataService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;

import org.springframework.stereotype.Service;

import java.util.List;

import javax.annotation.Resource;

/**
 * @author yzj
 */
@Service("cockpitDataService")
public class CockpitDataServiceImpl extends BaseServiceImpl<CockpitData> implements CockpitDataService{
    /**
    * 注入DAO
    */
    @Resource(name = "cockpitDataDao")
    public void setDao(BaseDao<CockpitData> dao) {
        super.setDao(dao);
    }
    @Resource
    public CockpitDataDao cockpitDataDao;
    
	@Override
	public CockpitData getLastCockpitData() throws Exception {
		return cockpitDataDao.getLastCockpitData();
	}

	@Override
	public Double getPublicDeposit() throws Exception {
		CockpitData entity = cockpitDataDao.getLastCockpitData();
		if(entity == null || StringUtil.isEmptyString(entity.getDepositSum())) return 0.00d;
		return Double.parseDouble(entity.getDepositSum());
	}

	@Override
	public List<DepositByOrgDto> getDepositByOrg() throws Exception {
		CockpitData entity = cockpitDataDao.getLastCockpitData();
		if(entity == null || StringUtil.isEmptyString(entity.getDepositByOrg())) return null;
		return JacksonUtil.fromJson(entity.getDepositByOrg(),new TypeReference<List<DepositByOrgDto>>() {});
	}

	@Override
	public List<StyBalDto> getDepositByType() throws Exception {
		CockpitData entity = cockpitDataDao.getLastCockpitData();
		if(entity == null || StringUtil.isEmptyString(entity.getDepositByType())) return null;
		return JacksonUtil.fromJson(entity.getDepositByType(),new TypeReference<List<StyBalDto>>() {});
	}

	@Override
	public List<DepositByDsDto> getDepositByMonth() throws Exception {
		CockpitData entity = cockpitDataDao.getLastCockpitData();
		if(entity == null || StringUtil.isEmptyString(entity.getDepositByMonth())) return null;
		return JacksonUtil.fromJson(entity.getDepositByMonth(),new TypeReference<List<DepositByDsDto>>() {}); 
	}

	@Override
	public Double getLoanBal() throws Exception {
		CockpitData entity = cockpitDataDao.getLastCockpitData();
		if(entity == null || StringUtil.isEmptyString(entity.getLoanSum())) return 0.00d;
		return Double.parseDouble(entity.getLoanSum());
	}

	@Override
	public CustSpreadByBalDto getCustSpreadByBal() throws Exception {
		CockpitData entity = cockpitDataDao.getLastCockpitData();
		if(entity == null || StringUtil.isEmptyString(entity.getLoanByCustNum())) return null;
		return JacksonUtil.fromJson(entity.getLoanByCustNum(), CustSpreadByBalDto.class); 
	}

	@Override
	public List<StyBalDto> getBalStry() throws Exception {
		CockpitData entity = cockpitDataDao.getLastCockpitData();
		if(entity == null || StringUtil.isEmptyString(entity.getLoanByGuaType())) return null;
		return JacksonUtil.fromJson(entity.getLoanByGuaType(),new TypeReference<List<StyBalDto>>() {});
	}

	@Override
	public List<StyBalDto> getBalByClassifyResult() throws Exception {
		CockpitData entity = cockpitDataDao.getLastCockpitData();
		if(entity == null || StringUtil.isEmptyString(entity.getLoanByClassify())) return null;
		return JacksonUtil.fromJson(entity.getLoanByClassify(),new TypeReference<List<StyBalDto>>() {});
	}

	@Override
	public BalByRateDto getBalByRate() throws Exception {
		CockpitData entity = cockpitDataDao.getLastCockpitData();
		if(entity == null || StringUtil.isEmptyString(entity.getLoanRote())) return null;
		return JacksonUtil.fromJson(entity.getLoanRote(), BalByRateDto.class); 
	}

	@Override
	public List<StyBalDto> getLoanByCredit() throws Exception {
		CockpitData entity = cockpitDataDao.getLastCockpitData();
		if(entity == null || StringUtil.isEmptyString(entity.getLoanByCredit())) return null;
		return JacksonUtil.fromJson(entity.getLoanByCredit(),new TypeReference<List<StyBalDto>>() {});
	}
}
