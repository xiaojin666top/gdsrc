package com.beawan.dtmp.service;

import java.util.List;

import com.beawan.core.BaseService;
import com.beawan.dtmp.entity.BusinessContract;

/**
 * @author yzj
 */
public interface BusinessContractService extends BaseService<BusinessContract> {
	
	/**
	 * 获取  在 start 到 end时间段内  授信到期的业务合同列表
	 * @param monthStart
	 * @param monthEnd
	 * @return
	 */
	List<BusinessContract> getMatuityConListByTerm(String monthStart, String monthEnd);
}
