package com.beawan.dtmp.service;

import java.util.List;

import com.beawan.core.BaseService;
import com.beawan.dtmp.dto.BalByRateDto;
import com.beawan.dtmp.dto.CustSpreadByBalDto;
import com.beawan.dtmp.dto.DepositByDsDto;
import com.beawan.dtmp.dto.DepositByOrgDto;
import com.beawan.dtmp.dto.StyBalDto;
import com.beawan.dtmp.entity.CockpitData;

/**
 * @author yzj
 */
public interface CockpitDataService extends BaseService<CockpitData> {
	
	/**
     * 从本地获取 最新的 驾驶舱数据
     * @return
     * @throws Exception
     */
    CockpitData getLastCockpitData() throws Exception;
    
    /********************************** 获取数据驾驶舱 **********************************/
    /*/////////////////////////////////////存款信息/////////////////////////////////////////*/
    /**
     * 对公存款总额（万元）
     * @return
     * @throws Exception
     */
    Double getPublicDeposit() throws Exception;
    
    /**
     * 分支行统计对公存款(定期、活期)
     * @return
     * @throws Exception
     */
    List<DepositByOrgDto> getDepositByOrg() throws Exception;
    
    /**
     * 分存款种类统计存款总额
     * @return
     * @throws Exception
     */
    List<StyBalDto> getDepositByType() throws Exception;
    
    /**
     * 分定期、活期 统计对公存款 近五个月变化情况
     * @return
     * @throws Exception
     */
    List<DepositByDsDto> getDepositByMonth() throws Exception;
    /*/////////////////////////////////////贷款信息/////////////////////////////////////////*/

    /**
     * 贷款总额
     * @return
     * @throws Exception
     */
    Double getLoanBal() throws Exception;

    /***
     * 统计当前时点 分金额的统计贷款户数分布
     * @return
     * @throws Exception
     */
    CustSpreadByBalDto getCustSpreadByBal() throws Exception;


    /**
     * 统计 按担保方式贷款情况
     * @return
     * @throws Exception
     */
    List<StyBalDto> getBalStry() throws Exception;
    
    /**
     * 统计按五级分类  贷款情况
     */
    List<StyBalDto> getBalByClassifyResult() throws Exception; 
    
    /**
     * 统计贷款利率分布情况
     * @return
     * @throws Exception
     */
    BalByRateDto getBalByRate() throws Exception;
    
    /**
     * 按授信统计对公贷款
     * @return
     * @throws Exception
     */
    List<StyBalDto> getLoanByCredit() throws Exception; 
}
