package com.beawan.survey.comm;

import java.io.Serializable;
import java.util.List;

/**
 * @ClassName TreeItemBean
 * @Description TODO(构造easyUI的树形结构，与数据库无关联)
 * @author czc
 * @Date 2017年5月11日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public class TreeItemBean implements Serializable {

	/** @Field @serialVersionUID : TODO(这里用一句话描述这个字段的作用) */
	private static final long serialVersionUID = -519559798338536749L;

	/** @Field @id : TODO(唯一标识 id) */
	private String id;

	/** @Field @text : TODO(文本描述) */
	private String text;

	/** @Field @state : TODO(状态) */
	private String state;

	/** @Field @checked : TODO(是否展开选中) */
	private String checked;

	/** @Field @attributes : TODO(贡献描述) */
	private String attributes;

	/** @Field @children : TODO(子节点) */
	private List<TreeItemBean> children;

	public TreeItemBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TreeItemBean(String id, String text) {
		super();
		this.id = id;
		this.text = text;
		this.state = "closed";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getChecked() {
		return checked;
	}

	public void setChecked(String checked) {
		this.checked = checked;
	}

	public String getAttributes() {
		return attributes;
	}

	public void setAttributes(String attributes) {
		this.attributes = attributes;
	}

	public List<TreeItemBean> getChildren() {
		return children;
	}

	public void setChildren(List<TreeItemBean> children) {
		this.children = children;
	}
}
