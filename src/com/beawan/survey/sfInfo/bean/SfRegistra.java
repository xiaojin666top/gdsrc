package com.beawan.survey.sfInfo.bean;



/**
 * TycSfRegistra generated by hbm2java
 */
public class SfRegistra  implements java.io.Serializable {


     private Long id;
     private Long taskId;
     private String litigant;
     private String filingDate;
     private String litigantGids;
     private String caseStatus;
     private String source;
     private String content;
     private String caseType;
     private String sourceUrl;
     private String defendant;
     private String juge;
     private String startTime;
     private Long tableId;
     private String department;
     private String area;
     private String plaintiff;
     private String assistant;
     private String court;
     private String caseNo;
     private String caseReason;
     private String closeDate;
     private String third;
     private String createTime;
     private Long cid;
     private Integer status;
     
    public SfRegistra() {
    }

	
    public SfRegistra(Long id) {
        this.id = id;
    }
    public SfRegistra(Long id, String litigant, String filingDate, String litigantGids, String caseStatus, String source, String content, String caseType, String sourceUrl, String defendant, String juge, String startTime, Long tableId, String department, String area, String plaintiff, String assistant, String court, String caseNo, String caseReason, String closeDate, String third, String createTime, Long cid) {
       this.id = id;
       this.litigant = litigant;
       this.filingDate = filingDate;
       this.litigantGids = litigantGids;
       this.caseStatus = caseStatus;
       this.source = source;
       this.content = content;
       this.caseType = caseType;
       this.sourceUrl = sourceUrl;
       this.defendant = defendant;
       this.juge = juge;
       this.startTime = startTime;
       this.tableId = tableId;
       this.department = department;
       this.area = area;
       this.plaintiff = plaintiff;
       this.assistant = assistant;
       this.court = court;
       this.caseNo = caseNo;
       this.caseReason = caseReason;
       this.closeDate = closeDate;
       this.third = third;
       this.createTime = createTime;
       this.cid = cid;
    }
   
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    public String getLitigant() {
        return this.litigant;
    }
    
    public void setLitigant(String litigant) {
        this.litigant = litigant;
    }
    public String getFilingDate() {
        return this.filingDate;
    }
    
    public void setFilingDate(String filingDate) {
        this.filingDate = filingDate;
    }
    public String getLitigantGids() {
        return this.litigantGids;
    }
    
    public void setLitigantGids(String litigantGids) {
        this.litigantGids = litigantGids;
    }
    public String getCaseStatus() {
        return this.caseStatus;
    }
    
    public void setCaseStatus(String caseStatus) {
        this.caseStatus = caseStatus;
    }
    public String getSource() {
        return this.source;
    }
    
    public void setSource(String source) {
        this.source = source;
    }
    public String getContent() {
        return this.content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }
    public String getCaseType() {
        return this.caseType;
    }
    
    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }
    public String getSourceUrl() {
        return this.sourceUrl;
    }
    
    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }
    public String getDefendant() {
        return this.defendant;
    }
    
    public void setDefendant(String defendant) {
        this.defendant = defendant;
    }
    public String getJuge() {
        return this.juge;
    }
    
    public void setJuge(String juge) {
        this.juge = juge;
    }
    public String getStartTime() {
        return this.startTime;
    }
    
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
    public Long getTableId() {
        return this.tableId;
    }
    
    public void setTableId(Long tableId) {
        this.tableId = tableId;
    }
    public String getDepartment() {
        return this.department;
    }
    
    public void setDepartment(String department) {
        this.department = department;
    }
    public String getArea() {
        return this.area;
    }
    
    public void setArea(String area) {
        this.area = area;
    }
    public String getPlaintiff() {
        return this.plaintiff;
    }
    
    public void setPlaintiff(String plaintiff) {
        this.plaintiff = plaintiff;
    }
    public String getAssistant() {
        return this.assistant;
    }
    
    public void setAssistant(String assistant) {
        this.assistant = assistant;
    }
    public String getCourt() {
        return this.court;
    }
    
    public void setCourt(String court) {
        this.court = court;
    }
    public String getCaseNo() {
        return this.caseNo;
    }
    
    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }
    public String getCaseReason() {
        return this.caseReason;
    }
    
    public void setCaseReason(String caseReason) {
        this.caseReason = caseReason;
    }
    public String getCloseDate() {
        return this.closeDate;
    }
    
    public void setCloseDate(String closeDate) {
        this.closeDate = closeDate;
    }
    public String getThird() {
        return this.third;
    }
    
    public void setThird(String third) {
        this.third = third;
    }
    public String getCreateTime() {
        return this.createTime;
    }
    
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
    public Long getCid() {
        return this.cid;
    }
    
    public void setCid(Long cid) {
        this.cid = cid;
    }


	public Long getTaskId() {
		return taskId;
	}


	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}


	public Integer getStatus() {
		return status;
	}


	public void setStatus(Integer status) {
		this.status = status;
	}


}


