package com.beawan.survey.sfInfo.bean;



/**
 * TycSfNoticeCourtHear generated by hbm2java
 */
public class SfNoticeCourtHear  implements java.io.Serializable {


     private Long id;
     private Long taskId;
     private String startDate;
     private String courtroom;
     private String caseReason;
     private String court;
     private String litigant;
     private Long tableId;
     private String judge;
     private String contractors;
     private String caseNo;
     private Integer status;
     
     
    public SfNoticeCourtHear() {
    }

	
    public SfNoticeCourtHear(Long id) {
        this.id = id;
    }
    public SfNoticeCourtHear(Long id, String startDate, String courtroom, String caseReason, String court, String litigant, Long tableId, String judge, String contractors, String caseNo) {
       this.id = id;
       this.startDate = startDate;
       this.courtroom = courtroom;
       this.caseReason = caseReason;
       this.court = court;
       this.litigant = litigant;
       this.tableId = tableId;
       this.judge = judge;
       this.contractors = contractors;
       this.caseNo = caseNo;
    }
   
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    public String getStartDate() {
        return this.startDate;
    }
    
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
    public String getCourtroom() {
        return this.courtroom;
    }
    
    public void setCourtroom(String courtroom) {
        this.courtroom = courtroom;
    }
    public String getCaseReason() {
        return this.caseReason;
    }
    
    public void setCaseReason(String caseReason) {
        this.caseReason = caseReason;
    }
    public String getCourt() {
        return this.court;
    }
    
    public void setCourt(String court) {
        this.court = court;
    }
    public String getLitigant() {
        return this.litigant;
    }
    
    public void setLitigant(String litigant) {
        this.litigant = litigant;
    }
    public Long getTableId() {
        return this.tableId;
    }
    
    public void setTableId(Long tableId) {
        this.tableId = tableId;
    }
    public String getJudge() {
        return this.judge;
    }
    
    public void setJudge(String judge) {
        this.judge = judge;
    }
    public String getContractors() {
        return this.contractors;
    }
    
    public void setContractors(String contractors) {
        this.contractors = contractors;
    }
    public String getCaseNo() {
        return this.caseNo;
    }
    
    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }


	public Long getTaskId() {
		return taskId;
	}


	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}


	public Integer getStatus() {
		return status;
	}


	public void setStatus(Integer status) {
		this.status = status;
	}


}


