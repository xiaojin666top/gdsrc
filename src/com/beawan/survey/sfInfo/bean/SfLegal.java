package com.beawan.survey.sfInfo.bean;

/**
 * 企业 法律诉讼
 * @author yzj
 *
 */
public class SfLegal {
	private Long id;
	private Long taskId;//任务id 作为关联
	
    private Long splitGids;
    private String plaintiffs;
    private String court;
    private String casereason;
    private String url;
    private String caseno;
    private Long relatId;
    private String title;
    private String abstracts;
    private String submittime;
    private String lawsuitUrl;
    private String casetype;
    private String doctype;
    private String agent;
    private String thirdParties;
    private String defendants;
    private Integer status;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getTaskId() {
		return taskId;
	}
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	public Long getSplitGids() {
		return splitGids;
	}
	public void setSplitGids(Long splitGids) {
		this.splitGids = splitGids;
	}
	public String getPlaintiffs() {
		return plaintiffs;
	}
	public void setPlaintiffs(String plaintiffs) {
		this.plaintiffs = plaintiffs;
	}
	public String getCourt() {
		return court;
	}
	public void setCourt(String court) {
		this.court = court;
	}
	public String getCasereason() {
		return casereason;
	}
	public void setCasereason(String casereason) {
		this.casereason = casereason;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getCaseno() {
		return caseno;
	}
	public void setCaseno(String caseno) {
		this.caseno = caseno;
	}
	public Long getRelatId() {
		return relatId;
	}
	public void setRelatId(Long relatId) {
		this.relatId = relatId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAbstracts() {
		return abstracts;
	}
	public void setAbstracts(String abstracts) {
		this.abstracts = abstracts;
	}
	public String getSubmittime() {
		return submittime;
	}
	public void setSubmittime(String submittime) {
		this.submittime = submittime;
	}
	public String getLawsuitUrl() {
		return lawsuitUrl;
	}
	public void setLawsuitUrl(String lawsuitUrl) {
		this.lawsuitUrl = lawsuitUrl;
	}
	public String getCasetype() {
		return casetype;
	}
	public void setCasetype(String casetype) {
		this.casetype = casetype;
	}
	public String getDoctype() {
		return doctype;
	}
	public void setDoctype(String doctype) {
		this.doctype = doctype;
	}
	public String getAgent() {
		return agent;
	}
	public void setAgent(String agent) {
		this.agent = agent;
	}
	public String getThirdParties() {
		return thirdParties;
	}
	public void setThirdParties(String thirdParties) {
		this.thirdParties = thirdParties;
	}
	public String getDefendants() {
		return defendants;
	}
	public void setDefendants(String defendants) {
		this.defendants = defendants;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
    
    
    
}
