package com.beawan.survey.sfInfo.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.custInfo.bean.CompChangeRecord;
import com.beawan.survey.custInfo.service.ICompChangeRecordSV;
import com.beawan.survey.sfInfo.bean.SfLegal;
import com.beawan.survey.sfInfo.service.ISfLegalSV;


@Service("sfLegalSV")
public class SfLegalSVImpl extends BaseServiceImpl<SfLegal> implements ISfLegalSV{
	/**
	    * 注入DAO
	    */
	    @Resource(name = "sfLegalDAO")
	    public void setDao(BaseDao<SfLegal> dao) {
	        super.setDao(dao);
	    }
	
}
