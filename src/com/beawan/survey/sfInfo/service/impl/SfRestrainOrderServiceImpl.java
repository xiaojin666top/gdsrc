package com.beawan.survey.sfInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.sfInfo.dao.SfRestrainOrderDao;
import com.beawan.survey.sfInfo.bean.SfRestrainOrder;
import com.beawan.survey.sfInfo.service.SfRestrainOrderService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.sfInfo.bean.SfRestrainOrder;

/**
 * @author yzj
 */
@Service("sfRestrainOrderService")
public class SfRestrainOrderServiceImpl extends BaseServiceImpl<SfRestrainOrder> implements SfRestrainOrderService{
    /**
    * 注入DAO
    */
    @Resource(name = "sfRestrainOrderDao")
    public void setDao(BaseDao<SfRestrainOrder> dao) {
        super.setDao(dao);
    }
    @Resource
    public SfRestrainOrderDao sfRestrainOrderDao;
}
