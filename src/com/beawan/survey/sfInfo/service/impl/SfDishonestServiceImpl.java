package com.beawan.survey.sfInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.sfInfo.dao.SfDishonestDao;
import com.beawan.survey.sfInfo.bean.SfDishonest;
import com.beawan.survey.sfInfo.service.SfDishonestService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.sfInfo.bean.SfDishonest;

/**
 * @author yzj
 */
@Service("sfDishonestService")
public class SfDishonestServiceImpl extends BaseServiceImpl<SfDishonest> implements SfDishonestService{
    /**
    * 注入DAO
    */
    @Resource(name = "sfDishonestDao")
    public void setDao(BaseDao<SfDishonest> dao) {
        super.setDao(dao);
    }
    @Resource
    public SfDishonestDao sfDishonestDao;
}
