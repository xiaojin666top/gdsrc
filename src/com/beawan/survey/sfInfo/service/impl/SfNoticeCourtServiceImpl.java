package com.beawan.survey.sfInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.sfInfo.dao.SfNoticeCourtDao;
import com.beawan.survey.sfInfo.bean.SfNoticeCourt;
import com.beawan.survey.sfInfo.service.SfNoticeCourtService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.sfInfo.bean.SfNoticeCourt;

/**
 * @author yzj
 */
@Service("sfNoticeCourtService")
public class SfNoticeCourtServiceImpl extends BaseServiceImpl<SfNoticeCourt> implements SfNoticeCourtService{
    /**
    * 注入DAO
    */
    @Resource(name = "sfNoticeCourtDao")
    public void setDao(BaseDao<SfNoticeCourt> dao) {
        super.setDao(dao);
    }
    @Resource
    public SfNoticeCourtDao sfNoticeCourtDao;
}
