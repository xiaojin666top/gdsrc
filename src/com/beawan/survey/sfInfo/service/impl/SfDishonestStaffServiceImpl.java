package com.beawan.survey.sfInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.sfInfo.dao.SfDishonestStaffDao;
import com.beawan.survey.sfInfo.bean.SfDishonestStaff;
import com.beawan.survey.sfInfo.service.SfDishonestStaffService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.sfInfo.bean.SfDishonestStaff;

/**
 * @author yzj
 */
@Service("sfDishonestStaffService")
public class SfDishonestStaffServiceImpl extends BaseServiceImpl<SfDishonestStaff> implements SfDishonestStaffService{
    /**
    * 注入DAO
    */
    @Resource(name = "sfDishonestStaffDao")
    public void setDao(BaseDao<SfDishonestStaff> dao) {
        super.setDao(dao);
    }
    @Resource
    public SfDishonestStaffDao sfDishonestStaffDao;
}
