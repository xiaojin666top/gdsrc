package com.beawan.survey.sfInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.sfInfo.dao.SfPerComRelDao;
import com.beawan.survey.sfInfo.bean.SfPerComRel;
import com.beawan.survey.sfInfo.service.SfPerComRelService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.sfInfo.bean.SfPerComRel;

/**
 * @author yzj
 */
@Service("sfPerComRelService")
public class SfPerComRelServiceImpl extends BaseServiceImpl<SfPerComRel> implements SfPerComRelService{
    /**
    * 注入DAO
    */
    @Resource(name = "sfPerComRelDao")
    public void setDao(BaseDao<SfPerComRel> dao) {
        super.setDao(dao);
    }
    @Resource
    public SfPerComRelDao sfPerComRelDao;
}
