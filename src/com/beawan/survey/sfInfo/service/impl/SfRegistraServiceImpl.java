package com.beawan.survey.sfInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.sfInfo.dao.SfRegistraDao;
import com.beawan.survey.sfInfo.bean.SfRegistra;
import com.beawan.survey.sfInfo.service.SfRegistraService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.sfInfo.bean.SfRegistra;

/**
 * @author yzj
 */
@Service("sfRegistraService")
public class SfRegistraServiceImpl extends BaseServiceImpl<SfRegistra> implements SfRegistraService{
    /**
    * 注入DAO
    */
    @Resource(name = "sfRegistraDao")
    public void setDao(BaseDao<SfRegistra> dao) {
        super.setDao(dao);
    }
    @Resource
    public SfRegistraDao sfRegistraDao;
}
