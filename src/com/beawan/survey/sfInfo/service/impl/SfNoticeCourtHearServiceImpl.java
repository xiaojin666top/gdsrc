package com.beawan.survey.sfInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.sfInfo.dao.SfNoticeCourtHearDao;
import com.beawan.survey.sfInfo.bean.SfNoticeCourtHear;
import com.beawan.survey.sfInfo.service.SfNoticeCourtHearService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.sfInfo.bean.SfNoticeCourtHear;

/**
 * @author yzj
 */
@Service("sfNoticeCourtHearService")
public class SfNoticeCourtHearServiceImpl extends BaseServiceImpl<SfNoticeCourtHear> implements SfNoticeCourtHearService{
    /**
    * 注入DAO
    */
    @Resource(name = "sfNoticeCourtHearDao")
    public void setDao(BaseDao<SfNoticeCourtHear> dao) {
        super.setDao(dao);
    }
    @Resource
    public SfNoticeCourtHearDao sfNoticeCourtHearDao;
}
