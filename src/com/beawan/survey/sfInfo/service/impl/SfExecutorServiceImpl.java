package com.beawan.survey.sfInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.sfInfo.dao.ISfExecutorDao;
import com.beawan.survey.sfInfo.bean.SfExecutor;
import com.beawan.survey.sfInfo.service.ISfExecutorService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.sfInfo.bean.SfExecutor;

/**
 * @author yzj
 */
@Service("sfExecutorService")
public class SfExecutorServiceImpl extends BaseServiceImpl<SfExecutor> implements ISfExecutorService{
    /**
    * 注入DAO
    */
    @Resource(name = "sfExecutorDao")
    public void setDao(BaseDao<SfExecutor> dao) {
        super.setDao(dao);
    }
    @Resource
    public ISfExecutorDao sfExecutorDao;
}
