package com.beawan.survey.sfInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.sfInfo.dao.SfNoticeAnnounceDao;
import com.beawan.survey.sfInfo.bean.SfNoticeAnnounce;
import com.beawan.survey.sfInfo.service.SfNoticeAnnounceService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.sfInfo.bean.SfNoticeAnnounce;

/**
 * @author yzj
 */
@Service("sfNoticeAnnounceService")
public class SfNoticeAnnounceServiceImpl extends BaseServiceImpl<SfNoticeAnnounce> implements SfNoticeAnnounceService{
    /**
    * 注入DAO
    */
    @Resource(name = "sfNoticeAnnounceDao")
    public void setDao(BaseDao<SfNoticeAnnounce> dao) {
        super.setDao(dao);
    }
    @Resource
    public SfNoticeAnnounceDao sfNoticeAnnounceDao;
}
