package com.beawan.survey.sfInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.sfInfo.dao.SfFinalCaseDao;
import com.beawan.survey.sfInfo.bean.SfFinalCase;
import com.beawan.survey.sfInfo.service.SfFinalCaseService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.sfInfo.bean.SfFinalCase;

/**
 * @author yzj
 */
@Service("sfFinalCaseService")
public class SfFinalCaseServiceImpl extends BaseServiceImpl<SfFinalCase> implements SfFinalCaseService{
    /**
    * 注入DAO
    */
    @Resource(name = "sfFinalCaseDao")
    public void setDao(BaseDao<SfFinalCase> dao) {
        super.setDao(dao);
    }
    @Resource
    public SfFinalCaseDao sfFinalCaseDao;
}
