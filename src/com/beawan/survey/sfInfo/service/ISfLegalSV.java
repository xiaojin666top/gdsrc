package com.beawan.survey.sfInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.sfInfo.bean.SfLegal;

/**
 * 
 * @author beawan_yuzj
 *
 */
public interface ISfLegalSV extends BaseService<SfLegal>{
	
}
