package com.beawan.survey.sfInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.sfInfo.bean.SfDishonest;

/**
 * @author yzj
 */
public interface SfDishonestService extends BaseService<SfDishonest> {
}
