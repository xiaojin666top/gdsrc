package com.beawan.survey.sfInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.sfInfo.bean.SfFinalCase;

/**
 * @author yzj
 */
public interface SfFinalCaseService extends BaseService<SfFinalCase> {
}
