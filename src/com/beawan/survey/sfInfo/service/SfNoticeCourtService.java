package com.beawan.survey.sfInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.sfInfo.bean.SfNoticeCourt;

/**
 * @author yzj
 */
public interface SfNoticeCourtService extends BaseService<SfNoticeCourt> {
}
