package com.beawan.survey.sfInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.sfInfo.bean.SfNoticeAnnounce;

/**
 * @author yzj
 */
public interface SfNoticeAnnounceService extends BaseService<SfNoticeAnnounce> {
}
