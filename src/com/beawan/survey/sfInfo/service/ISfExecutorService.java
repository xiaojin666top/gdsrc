package com.beawan.survey.sfInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.sfInfo.bean.SfExecutor;

/**
 * @author yzj
 */
public interface ISfExecutorService extends BaseService<SfExecutor> {
}
