package com.beawan.survey.sfInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.sfInfo.bean.SfPerComRel;

/**
 * @author yzj
 */
public interface SfPerComRelService extends BaseService<SfPerComRel> {
}
