package com.beawan.survey.sfInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.sfInfo.bean.SfRestrainOrder;

/**
 * @author yzj
 */
public interface SfRestrainOrderService extends BaseService<SfRestrainOrder> {
}
