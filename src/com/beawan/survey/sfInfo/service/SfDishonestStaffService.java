package com.beawan.survey.sfInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.sfInfo.bean.SfDishonestStaff;

/**
 * @author yzj
 */
public interface SfDishonestStaffService extends BaseService<SfDishonestStaff> {
}
