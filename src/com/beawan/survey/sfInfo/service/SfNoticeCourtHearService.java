package com.beawan.survey.sfInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.sfInfo.bean.SfNoticeCourtHear;

/**
 * @author yzj
 */
public interface SfNoticeCourtHearService extends BaseService<SfNoticeCourtHear> {
}
