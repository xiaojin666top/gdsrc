package com.beawan.survey.sfInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.sfInfo.bean.SfRegistra;

/**
 * @author yzj
 */
public interface SfRegistraService extends BaseService<SfRegistra> {
}
