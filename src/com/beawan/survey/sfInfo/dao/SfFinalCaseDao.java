package com.beawan.survey.sfInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.sfInfo.bean.SfFinalCase;

/**
 * @author yzj
 */
public interface SfFinalCaseDao extends BaseDao<SfFinalCase> {
}
