package com.beawan.survey.sfInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.sfInfo.bean.SfDishonestStaff;

/**
 * @author yzj
 */
public interface SfDishonestStaffDao extends BaseDao<SfDishonestStaff> {
}
