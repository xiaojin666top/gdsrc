package com.beawan.survey.sfInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.sfInfo.bean.SfExecutor;

/**
 * @author yzj
 */
public interface ISfExecutorDao extends BaseDao<SfExecutor> {
}
