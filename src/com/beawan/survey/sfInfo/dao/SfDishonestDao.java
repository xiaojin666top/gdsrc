package com.beawan.survey.sfInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.sfInfo.bean.SfDishonest;

/**
 * @author yzj
 */
public interface SfDishonestDao extends BaseDao<SfDishonest> {
}
