package com.beawan.survey.sfInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.sfInfo.dao.SfRestrainOrderDao;
import com.beawan.survey.sfInfo.bean.SfRestrainOrder;
import org.springframework.stereotype.Repository;
import com.beawan.survey.sfInfo.bean.SfRestrainOrder;

/**
 * @author yzj
 */
@Repository("sfRestrainOrderDao")
public class SfRestrainOrderDaoImpl extends BaseDaoImpl<SfRestrainOrder> implements SfRestrainOrderDao{

}
