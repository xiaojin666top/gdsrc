package com.beawan.survey.sfInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.sfInfo.dao.SfFinalCaseDao;
import com.beawan.survey.sfInfo.bean.SfFinalCase;
import org.springframework.stereotype.Repository;
import com.beawan.survey.sfInfo.bean.SfFinalCase;

/**
 * @author yzj
 */
@Repository("sfFinalCaseDao")
public class SfFinalCaseDaoImpl extends BaseDaoImpl<SfFinalCase> implements SfFinalCaseDao{

}
