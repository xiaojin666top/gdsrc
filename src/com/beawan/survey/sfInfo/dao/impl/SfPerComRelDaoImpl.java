package com.beawan.survey.sfInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.sfInfo.dao.SfPerComRelDao;
import com.beawan.survey.sfInfo.bean.SfPerComRel;
import org.springframework.stereotype.Repository;
import com.beawan.survey.sfInfo.bean.SfPerComRel;

/**
 * @author yzj
 */
@Repository("sfPerComRelDao")
public class SfPerComRelDaoImpl extends BaseDaoImpl<SfPerComRel> implements SfPerComRelDao{

}
