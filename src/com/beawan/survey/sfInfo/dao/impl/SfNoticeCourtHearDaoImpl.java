package com.beawan.survey.sfInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.sfInfo.dao.SfNoticeCourtHearDao;
import com.beawan.survey.sfInfo.bean.SfNoticeCourtHear;
import org.springframework.stereotype.Repository;
import com.beawan.survey.sfInfo.bean.SfNoticeCourtHear;

/**
 * @author yzj
 */
@Repository("sfNoticeCourtHearDao")
public class SfNoticeCourtHearDaoImpl extends BaseDaoImpl<SfNoticeCourtHear> implements SfNoticeCourtHearDao{

}
