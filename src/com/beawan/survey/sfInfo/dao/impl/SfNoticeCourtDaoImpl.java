package com.beawan.survey.sfInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.sfInfo.dao.SfNoticeCourtDao;
import com.beawan.survey.sfInfo.bean.SfNoticeCourt;
import org.springframework.stereotype.Repository;
import com.beawan.survey.sfInfo.bean.SfNoticeCourt;

/**
 * @author yzj
 */
@Repository("sfNoticeCourtDao")
public class SfNoticeCourtDaoImpl extends BaseDaoImpl<SfNoticeCourt> implements SfNoticeCourtDao{

}
