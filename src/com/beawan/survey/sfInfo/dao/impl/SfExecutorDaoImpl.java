package com.beawan.survey.sfInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.sfInfo.dao.ISfExecutorDao;
import com.beawan.survey.sfInfo.bean.SfExecutor;
import org.springframework.stereotype.Repository;
import com.beawan.survey.sfInfo.bean.SfExecutor;

/**
 * @author yzj
 */
@Repository("sfExecutorDao")
public class SfExecutorDaoImpl extends BaseDaoImpl<SfExecutor> implements ISfExecutorDao{

}
