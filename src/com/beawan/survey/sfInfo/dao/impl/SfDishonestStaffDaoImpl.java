package com.beawan.survey.sfInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.sfInfo.dao.SfDishonestStaffDao;
import com.beawan.survey.sfInfo.bean.SfDishonestStaff;
import org.springframework.stereotype.Repository;
import com.beawan.survey.sfInfo.bean.SfDishonestStaff;

/**
 * @author yzj
 */
@Repository("sfDishonestStaffDao")
public class SfDishonestStaffDaoImpl extends BaseDaoImpl<SfDishonestStaff> implements SfDishonestStaffDao{

}
