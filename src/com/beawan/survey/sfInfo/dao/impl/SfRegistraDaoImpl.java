package com.beawan.survey.sfInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.sfInfo.dao.SfRegistraDao;
import com.beawan.survey.sfInfo.bean.SfRegistra;
import org.springframework.stereotype.Repository;
import com.beawan.survey.sfInfo.bean.SfRegistra;

/**
 * @author yzj
 */
@Repository("sfRegistraDao")
public class SfRegistraDaoImpl extends BaseDaoImpl<SfRegistra> implements SfRegistraDao{

}
