package com.beawan.survey.sfInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.sfInfo.dao.SfDishonestDao;
import com.beawan.survey.sfInfo.bean.SfDishonest;
import org.springframework.stereotype.Repository;
import com.beawan.survey.sfInfo.bean.SfDishonest;

/**
 * @author yzj
 */
@Repository("sfDishonestDao")
public class SfDishonestDaoImpl extends BaseDaoImpl<SfDishonest> implements SfDishonestDao{

}
