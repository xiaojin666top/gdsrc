package com.beawan.survey.sfInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.sfInfo.bean.SfNoticeCourtHear;

/**
 * @author yzj
 */
public interface SfNoticeCourtHearDao extends BaseDao<SfNoticeCourtHear> {
}
