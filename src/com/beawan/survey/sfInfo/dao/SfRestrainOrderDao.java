package com.beawan.survey.sfInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.sfInfo.bean.SfRestrainOrder;

/**
 * @author yzj
 */
public interface SfRestrainOrderDao extends BaseDao<SfRestrainOrder> {
}
