package com.beawan.survey.sfInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.sfInfo.bean.SfRegistra;

/**
 * @author yzj
 */
public interface SfRegistraDao extends BaseDao<SfRegistra> {
}
