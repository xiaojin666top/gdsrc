package com.beawan.survey.sfInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.sfInfo.bean.SfNoticeCourt;

/**
 * @author yzj
 */
public interface SfNoticeCourtDao extends BaseDao<SfNoticeCourt> {
}
