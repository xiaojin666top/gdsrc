package com.beawan.survey.sfInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.sfInfo.bean.SfPerComRel;

/**
 * @author yzj
 */
public interface SfPerComRelDao extends BaseDao<SfPerComRel> {
}
