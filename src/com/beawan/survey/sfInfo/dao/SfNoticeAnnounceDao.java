package com.beawan.survey.sfInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.sfInfo.bean.SfNoticeAnnounce;

/**
 * @author yzj
 */
public interface SfNoticeAnnounceDao extends BaseDao<SfNoticeAnnounce> {
}
