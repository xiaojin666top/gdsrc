package com.beawan.survey.sfInfo.dto;

/**
 * 司法信息统计
 * @author yuzj
 *
 */
public class SfStatDto {

	private String sfName;//司法类型名字
	private Long numCount;//司法信息条数
	private String url;
	
	
	
	public SfStatDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public SfStatDto(String sfName, Long numCount) {
		super();
		this.sfName = sfName;
		this.numCount = numCount;
	}

	public SfStatDto(String sfName, Long numCount, String url) {
		super();
		this.sfName = sfName;
		this.numCount = numCount;
		this.url = url;
	}



	public String getSfName() {
		return sfName;
	}
	public void setSfName(String sfName) {
		this.sfName = sfName;
	}
	public Long getNumCount() {
		return numCount;
	}
	public void setNumCount(Long numCount) {
		this.numCount = numCount;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
