package com.beawan.survey.sfInfo.dto;

import java.util.List;

import com.beawan.survey.sfInfo.bean.SfPerComRel;

/**
 * TycSfNoticeCourt generated by hbm2java
 */
public class SfNoticeCourtDto  implements java.io.Serializable {


     private Long id;
     private Long taskId;
     private Long tableId;
     private Long announceId;
     private String bltnno;
     private String bltnstate;
     private String bltntype;
     private String bltntypename;
     private String caseno;
     private String content;
     private String courtcode;
     private String dealgrade;
     private String dealgradename;
     private String judge;
     private String judgephone;
     private String mobilephone;
     private String party1;
     private String party2;
     private String province;
     private String publishdate;
     private String publishpage;
     private String reason;
     private Integer status;

     private List<SfPerComRel> perComRels;
     
    public SfNoticeCourtDto() {
    }

	
    public SfNoticeCourtDto(Long id) {
        this.id = id;
    }
    public SfNoticeCourtDto(Long id, Long tableId, Long announceId, String bltnno, String bltnstate, String bltntype, String bltntypename, String caseno, String content, String courtcode, String dealgrade, String dealgradename, String judge, String judgephone, String mobilephone, String party1, String party2, String province, String publishdate, String publishpage, String reason) {
       this.id = id;
       this.tableId = tableId;
       this.announceId = announceId;
       this.bltnno = bltnno;
       this.bltnstate = bltnstate;
       this.bltntype = bltntype;
       this.bltntypename = bltntypename;
       this.caseno = caseno;
       this.content = content;
       this.courtcode = courtcode;
       this.dealgrade = dealgrade;
       this.dealgradename = dealgradename;
       this.judge = judge;
       this.judgephone = judgephone;
       this.mobilephone = mobilephone;
       this.party1 = party1;
       this.party2 = party2;
       this.province = province;
       this.publishdate = publishdate;
       this.publishpage = publishpage;
       this.reason = reason;
    }
   
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    public Long getTableId() {
        return this.tableId;
    }
    
    public void setTableId(Long tableId) {
        this.tableId = tableId;
    }
    public Long getAnnounceId() {
        return this.announceId;
    }
    
    public void setAnnounceId(Long announceId) {
        this.announceId = announceId;
    }
    public String getBltnno() {
        return this.bltnno;
    }
    
    public void setBltnno(String bltnno) {
        this.bltnno = bltnno;
    }
    public String getBltnstate() {
        return this.bltnstate;
    }
    
    public void setBltnstate(String bltnstate) {
        this.bltnstate = bltnstate;
    }
    public String getBltntype() {
        return this.bltntype;
    }
    
    public void setBltntype(String bltntype) {
        this.bltntype = bltntype;
    }
    public String getBltntypename() {
        return this.bltntypename;
    }
    
    public void setBltntypename(String bltntypename) {
        this.bltntypename = bltntypename;
    }
    public String getCaseno() {
        return this.caseno;
    }
    
    public void setCaseno(String caseno) {
        this.caseno = caseno;
    }
    public String getContent() {
        return this.content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }
    public String getCourtcode() {
        return this.courtcode;
    }
    
    public void setCourtcode(String courtcode) {
        this.courtcode = courtcode;
    }
    public String getDealgrade() {
        return this.dealgrade;
    }
    
    public void setDealgrade(String dealgrade) {
        this.dealgrade = dealgrade;
    }
    public String getDealgradename() {
        return this.dealgradename;
    }
    
    public void setDealgradename(String dealgradename) {
        this.dealgradename = dealgradename;
    }
    public String getJudge() {
        return this.judge;
    }
    
    public void setJudge(String judge) {
        this.judge = judge;
    }
    public String getJudgephone() {
        return this.judgephone;
    }
    
    public void setJudgephone(String judgephone) {
        this.judgephone = judgephone;
    }
    public String getMobilephone() {
        return this.mobilephone;
    }
    
    public void setMobilephone(String mobilephone) {
        this.mobilephone = mobilephone;
    }
    public String getParty1() {
        return this.party1;
    }
    
    public void setParty1(String party1) {
        this.party1 = party1;
    }
    public String getParty2() {
        return this.party2;
    }
    
    public void setParty2(String party2) {
        this.party2 = party2;
    }
    public String getProvince() {
        return this.province;
    }
    
    public void setProvince(String province) {
        this.province = province;
    }
    public String getPublishdate() {
        return this.publishdate;
    }
    
    public void setPublishdate(String publishdate) {
        this.publishdate = publishdate;
    }
    public String getPublishpage() {
        return this.publishpage;
    }
    
    public void setPublishpage(String publishpage) {
        this.publishpage = publishpage;
    }
    public String getReason() {
        return this.reason;
    }
    
    public void setReason(String reason) {
        this.reason = reason;
    }


	public Long getTaskId() {
		return taskId;
	}


	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}


	public Integer getStatus() {
		return status;
	}


	public void setStatus(Integer status) {
		this.status = status;
	}


	public List<SfPerComRel> getPerComRels() {
		return perComRels;
	}


	public void setPerComRels(List<SfPerComRel> perComRels) {
		this.perComRels = perComRels;
	}


}


