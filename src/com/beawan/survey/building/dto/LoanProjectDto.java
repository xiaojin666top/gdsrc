package com.beawan.survey.building.dto;

import javax.persistence.Column;

/**
 * 申贷项目DTO
 * @author zxh
 * @date 2020/8/3 20:09
 */
public class LoanProjectDto {

    private Integer id;
    private String serNo;
    private String projectName;// 项目名称
    private double contractAmt;// 合同金额
    private String paymentWay;// 付款方式
    private double plannedAmt;// 需投入金额
    private double investedAmt;// 已投入金额
    private String fundsSource;// 资金来源
    private String schedule;// 项目进度

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public double getContractAmt() {
        return contractAmt;
    }

    public void setContractAmt(double contractAmt) {
        this.contractAmt = contractAmt;
    }

    public String getPaymentWay() {
        return paymentWay;
    }

    public void setPaymentWay(String paymentWay) {
        this.paymentWay = paymentWay;
    }

    public double getPlannedAmt() {
        return plannedAmt;
    }

    public void setPlannedAmt(double plannedAmt) {
        this.plannedAmt = plannedAmt;
    }

    public double getInvestedAmt() {
        return investedAmt;
    }

    public void setInvestedAmt(double investedAmt) {
        this.investedAmt = investedAmt;
    }

    public String getFundsSource() {
        return fundsSource;
    }

    public void setFundsSource(String fundsSource) {
        this.fundsSource = fundsSource;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }
}
