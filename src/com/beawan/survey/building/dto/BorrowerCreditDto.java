package com.beawan.survey.building.dto;

import javax.persistence.Column;

/**
 * 借款人授信情况DTO
 * @author zxh
 * @date 2020/8/4 14:12
 */
public class BorrowerCreditDto {

    private Long id;
    private String customerNo;
    private String financialOrg; // 金融银行
    private String businessType; // 授信品种
    private Double creditAmt; // 授信额度
    private Double usedAmt; // 用信额度
    private Integer creditTerm; // 授信期限(月)
    private String guaranteeType; // 担保方式
    private String loanProject; // 申贷项目

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getFinancialOrg() {
        return financialOrg;
    }

    public void setFinancialOrg(String financialOrg) {
        this.financialOrg = financialOrg;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public Double getCreditAmt() {
        return creditAmt;
    }

    public void setCreditAmt(Double creditAmt) {
        this.creditAmt = creditAmt;
    }

    public Double getUsedAmt() {
        return usedAmt;
    }

    public void setUsedAmt(Double usedAmt) {
        this.usedAmt = usedAmt;
    }

    public Integer getCreditTerm() {
        return creditTerm;
    }

    public void setCreditTerm(Integer creditTerm) {
        this.creditTerm = creditTerm;
    }

    public String getGuaranteeType() {
        return guaranteeType;
    }

    public void setGuaranteeType(String guaranteeType) {
        this.guaranteeType = guaranteeType;
    }

    public String getLoanProject() {
        return loanProject;
    }

    public void setLoanProject(String loanProject) {
        this.loanProject = loanProject;
    }
}
