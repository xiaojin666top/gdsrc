package com.beawan.survey.building.service;

import com.beawan.core.BaseService;
import com.beawan.survey.building.entity.PlanProject;

/**
 * @author yzj
 */
public interface PlanProjectService extends BaseService<PlanProject> {
}
