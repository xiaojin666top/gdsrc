package com.beawan.survey.building.service;

import com.beawan.core.BaseService;
import com.beawan.survey.building.entity.ManageInfo;

/**
 * @author yzj
 */
public interface ManageInfoService extends BaseService<ManageInfo> {
}
