package com.beawan.survey.building.service;

import com.beawan.core.BaseService;
import com.beawan.survey.building.entity.MultiBusiness;

/**
 * @author yzj
 */
public interface MultiBusinessService extends BaseService<MultiBusiness> {
}
