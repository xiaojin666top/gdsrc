package com.beawan.survey.building.service;

import com.beawan.core.BaseService;
import com.beawan.survey.building.entity.BusinessInfo;

/**
 * @author yzj
 */
public interface BusinessInfoService extends BaseService<BusinessInfo> {
}
