package com.beawan.survey.building.service;

import com.beawan.core.BaseService;
import com.beawan.survey.building.entity.FinishProject;

/**
 * @author yzj
 */
public interface FinishProjectService extends BaseService<FinishProject> {
}
