package com.beawan.survey.building.service;

import com.beawan.core.BaseService;
import com.beawan.survey.building.entity.InvestProject;

/**
 * @author yzj
 */
public interface InvestProjectService extends BaseService<InvestProject> {
}
