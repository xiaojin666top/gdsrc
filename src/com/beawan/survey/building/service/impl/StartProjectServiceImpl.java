package com.beawan.survey.building.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.building.dao.StartProjectDao;
import com.beawan.survey.building.entity.StartProject;
import com.beawan.survey.building.service.StartProjectService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.building.entity.StartProject;

/**
 * @author yzj
 */
@Service("startProjectService")
public class StartProjectServiceImpl extends BaseServiceImpl<StartProject> implements StartProjectService{
    /**
    * 注入DAO
    */
    @Resource(name = "startProjectDao")
    public void setDao(BaseDao<StartProject> dao) {
        super.setDao(dao);
    }
    @Resource
    public StartProjectDao startProjectDao;
}
