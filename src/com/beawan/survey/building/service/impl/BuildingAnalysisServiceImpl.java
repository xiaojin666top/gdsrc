package com.beawan.survey.building.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.building.dao.BuildingAnalysisDao;
import com.beawan.survey.building.service.BuildingAnalysisService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.building.entity.BuildingAnalysis;

/**
 * @author yzj
 */
@Service("buildingAnalysisService")
public class BuildingAnalysisServiceImpl extends BaseServiceImpl<BuildingAnalysis> implements BuildingAnalysisService{
    /**
    * 注入DAO
    */
    @Resource(name = "buildingAnalysisDao")
    public void setDao(BaseDao<BuildingAnalysis> dao) {
        super.setDao(dao);
    }
    @Resource
    public BuildingAnalysisDao buildingAnalysisDao;
}
