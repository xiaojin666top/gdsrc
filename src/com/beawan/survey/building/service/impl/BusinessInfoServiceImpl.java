package com.beawan.survey.building.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.building.dao.BusinessInfoDao;
import com.beawan.survey.building.entity.BusinessInfo;
import com.beawan.survey.building.service.BusinessInfoService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.building.entity.BusinessInfo;

/**
 * @author yzj
 */
@Service("businessInfoService")
public class BusinessInfoServiceImpl extends BaseServiceImpl<BusinessInfo> implements BusinessInfoService{
    /**
    * 注入DAO
    */
    @Resource(name = "businessInfoDao")
    public void setDao(BaseDao<BusinessInfo> dao) {
        super.setDao(dao);
    }
    @Resource
    public BusinessInfoDao businessInfoDao;
}
