package com.beawan.survey.building.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.building.dao.FinishProjectDao;
import com.beawan.survey.building.entity.FinishProject;
import com.beawan.survey.building.service.FinishProjectService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.building.entity.FinishProject;

/**
 * @author yzj
 */
@Service("finishProjectService")
public class FinishProjectServiceImpl extends BaseServiceImpl<FinishProject> implements FinishProjectService{
    /**
    * 注入DAO
    */
    @Resource(name = "finishProjectDao")
    public void setDao(BaseDao<FinishProject> dao) {
        super.setDao(dao);
    }
    @Resource
    public FinishProjectDao finishProjectDao;
}
