package com.beawan.survey.building.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.building.dao.CreditInfoDao;
import com.beawan.survey.building.service.CreditInfoService;
import com.beawan.survey.custInfo.bean.CompFinancingBank;
import com.beawan.task.bean.Task;
import com.beawan.task.dao.ITaskDAO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zxh
 * @date 2020/8/4 11:31
 */
@Service("creditInfoService")
public class CreditInfoServiceImpl extends BaseServiceImpl<CompFinancingBank> implements CreditInfoService {

    /**
     * 注入DAO
     */
    @Resource(name = "creditInfoDao")
    public void setDao(BaseDao<CompFinancingBank> dao) {
        super.setDao(dao);
    }
    @Resource
    public CreditInfoDao creditInfoDao;
    @Resource
    protected ITaskDAO taskDAO;

    @Override
    public List<CompFinancingBank> selectBySerNo(String serNo, String type) {
        Task task = taskDAO.getTaskBySerNo(serNo);
        if (task == null) return null;

        Map<String, Object> params = new HashMap<>();
        params.put("taskId", task.getId());
        params.put("customerNo", task.getCustomerNo());
        params.put("type", type);
        return creditInfoDao.selectByProperty(params);
    }
}
