package com.beawan.survey.building.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.building.dao.ManageInfoDao;
import com.beawan.survey.building.entity.ManageInfo;
import com.beawan.survey.building.service.ManageInfoService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.building.entity.ManageInfo;

/**
 * @author yzj
 */
@Service("manageInfoService")
public class ManageInfoServiceImpl extends BaseServiceImpl<ManageInfo> implements ManageInfoService{
    /**
    * 注入DAO
    */
    @Resource(name = "manageInfoDao")
    public void setDao(BaseDao<ManageInfo> dao) {
        super.setDao(dao);
    }
    @Resource
    public ManageInfoDao manageInfoDao;
}
