package com.beawan.survey.building.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.building.dao.InvestProjectDao;
import com.beawan.survey.building.entity.InvestProject;
import com.beawan.survey.building.service.InvestProjectService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.building.entity.InvestProject;

/**
 * @author yzj
 */
@Service("investProjectService")
public class InvestProjectServiceImpl extends BaseServiceImpl<InvestProject> implements InvestProjectService{
    /**
    * 注入DAO
    */
    @Resource(name = "investProjectDao")
    public void setDao(BaseDao<InvestProject> dao) {
        super.setDao(dao);
    }
    @Resource
    public InvestProjectDao investProjectDao;
}
