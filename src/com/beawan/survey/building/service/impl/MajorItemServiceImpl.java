package com.beawan.survey.building.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.building.dao.MajorItemDao;
import com.beawan.survey.building.entity.MajorItem;
import com.beawan.survey.building.service.MajorItemService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.building.entity.MajorItem;

/**
 * @author yzj
 */
@Service("majorItemService")
public class MajorItemServiceImpl extends BaseServiceImpl<MajorItem> implements MajorItemService{
    /**
    * 注入DAO
    */
    @Resource(name = "majorItemDao")
    public void setDao(BaseDao<MajorItem> dao) {
        super.setDao(dao);
    }
    @Resource
    public MajorItemDao majorItemDao;
}
