package com.beawan.survey.building.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.building.dao.PlanProjectDao;
import com.beawan.survey.building.entity.PlanProject;
import com.beawan.survey.building.service.PlanProjectService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.building.entity.PlanProject;

/**
 * @author yzj
 */
@Service("planProjectService")
public class PlanProjectServiceImpl extends BaseServiceImpl<PlanProject> implements PlanProjectService{
    /**
    * 注入DAO
    */
    @Resource(name = "planProjectDao")
    public void setDao(BaseDao<PlanProject> dao) {
        super.setDao(dao);
    }
    @Resource
    public PlanProjectDao planProjectDao;
}
