package com.beawan.survey.building.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.building.dao.MultiBusinessDao;
import com.beawan.survey.building.entity.MultiBusiness;
import com.beawan.survey.building.service.MultiBusinessService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.building.entity.MultiBusiness;

/**
 * @author yzj
 */
@Service("multiBusinessService")
public class MultiBusinessServiceImpl extends BaseServiceImpl<MultiBusiness> implements MultiBusinessService{
    /**
    * 注入DAO
    */
    @Resource(name = "multiBusinessDao")
    public void setDao(BaseDao<MultiBusiness> dao) {
        super.setDao(dao);
    }
    @Resource
    public MultiBusinessDao multiBusinessDao;
}
