package com.beawan.survey.building.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.building.dao.LoanProjectDao;
import com.beawan.survey.building.entity.LoanProject;
import com.beawan.survey.building.service.LoanProjectService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.building.entity.LoanProject;

/**
 * @author yzj
 */
@Service("loanProjectService")
public class LoanProjectServiceImpl extends BaseServiceImpl<LoanProject> implements LoanProjectService{
    /**
    * 注入DAO
    */
    @Resource(name = "loanProjectDao")
    public void setDao(BaseDao<LoanProject> dao) {
        super.setDao(dao);
    }
    @Resource
    public LoanProjectDao loanProjectDao;
}
