package com.beawan.survey.building.service;

import com.beawan.core.BaseService;
import com.beawan.survey.building.entity.MajorItem;

/**
 * @author yzj
 */
public interface MajorItemService extends BaseService<MajorItem> {
}
