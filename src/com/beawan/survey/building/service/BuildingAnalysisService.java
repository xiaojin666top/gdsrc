package com.beawan.survey.building.service;

import com.beawan.core.BaseService;
import com.beawan.survey.building.entity.BuildingAnalysis;

/**
 * @author yzj
 */
public interface BuildingAnalysisService extends BaseService<BuildingAnalysis> {
}
