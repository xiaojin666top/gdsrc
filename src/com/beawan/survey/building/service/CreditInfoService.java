package com.beawan.survey.building.service;

import com.beawan.core.BaseService;
import com.beawan.survey.custInfo.bean.CompFinancingBank;

import java.util.List;

/**
 * 企业授信情况（建筑业）
 * @author zxh
 * @date 2020/8/4 11:30
 */
public interface CreditInfoService extends BaseService<CompFinancingBank> {

    /**
     * 通过 serNo 获取 企业授信情况
     * @param serNo 任务流水号
     * @return
     */
    public List<CompFinancingBank> selectBySerNo(String serNo, String type);
}
