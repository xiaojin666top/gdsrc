package com.beawan.survey.building.service;

import com.beawan.core.BaseService;
import com.beawan.survey.building.entity.LoanProject;

/**
 * @author yzj
 */
public interface LoanProjectService extends BaseService<LoanProject> {
}
