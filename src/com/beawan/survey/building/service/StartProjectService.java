package com.beawan.survey.building.service;

import com.beawan.core.BaseService;
import com.beawan.survey.building.entity.StartProject;

/**
 * @author yzj
 */
public interface StartProjectService extends BaseService<StartProject> {
}
