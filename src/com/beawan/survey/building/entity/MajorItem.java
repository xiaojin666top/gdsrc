package com.beawan.survey.building.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 企业重大事项（建筑业）
 * @author zxh
 * @date 2020/8/4 20:47
 */
@Entity
@Table(name = "CM_MAJOR_ITEM", schema = "GDTCESYS")
public class MajorItem  extends BaseEntity {

    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_MAJOR_ITEM")
   // @SequenceGenerator(name = "CM_MAJOR_ITEM", allocationSize = 1, initialValue = 1, sequenceName = "CM_MAJOR_ITEM_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;// 任务流水号
    @Column(name = "ESCAPE_DEBT")
    private String escapeDebt;// 是否逃债废债企业
    @Column(name = "BAD_ESCAPE_DEBT")
    private String badEscapeDebt;// 逃债废债详情
    @Column(name = "DEBT")
    private String debt;// 有无拖欠情况
    @Column(name = "BAD_DEBT")
    private String badDebt;// 恶意拖欠详情
    @Column(name = "LITIGATION")
    private String litigation;// 有无法律诉讼
    @Column(name = "BAD_LITIGATION")
    private String badLitigation;// 法律诉讼详情
    @Column(name = "MARKET_PERFOR")
    private String marketPerformance;// 有无不良市场表现
    @Column(name = "BAD_MARKET_PERFOR")
    private String badMarketPerformance;// 不良表现详情
    @Column(name = "ENVIRONMENT")
    private String environment;// 有无不良环保行为
    @Column(name = "BAD_ENVIRONMENT")
    private String badEnvironment;// 不良环保详情
    @Column(name = "BUSINESS")
    private String business;// 有无不良经营情况
    @Column(name = "BAD_BUSINESS")
    private String badBusiness;// 不良经营详情
    @Column(name = "TRANSACTION")
    private String transaction;// 有无不良交易情况
    @Column(name = "BAD_TRANSACT")
    private String badTransaction;// 不良交易详情
    @Column(name = "INVESTMENT")
    private String investment;// 有无投融资情况
    @Column(name = "BAD_INVEST")
    private String badInvestment;// 投资融资详情
    @Column(name = "REORGANNIZE")
    private String reorganization;// 有无重组情况
    @Column(name = "BAD_REORGAN")
    private String badReorganization;// 重大重组详情
    @Column(name = "REWARD")
    private String reward;// 有无奖罚情况
    @Column(name = "BAD_REWARD")
    private String badReward;// 重大奖罚详情
    @Column(name = "RISK_INFO")
    private String riskInfo;// 预警情况
    @Column(name = "DEBT_INFO")
    private String debtInfo;// 负债情况

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getEscapeDebt() {
        return escapeDebt;
    }

    public void setEscapeDebt(String escapeDebt) {
        this.escapeDebt = escapeDebt;
    }

    public String getBadEscapeDebt() {
        return badEscapeDebt;
    }

    public void setBadEscapeDebt(String badEscapeDebt) {
        this.badEscapeDebt = badEscapeDebt;
    }

    public String getDebt() {
        return debt;
    }

    public void setDebt(String debt) {
        this.debt = debt;
    }

    public String getBadDebt() {
        return badDebt;
    }

    public void setBadDebt(String badDebt) {
        this.badDebt = badDebt;
    }

    public String getLitigation() {
        return litigation;
    }

    public void setLitigation(String litigation) {
        this.litigation = litigation;
    }

    public String getBadLitigation() {
        return badLitigation;
    }

    public void setBadLitigation(String badLitigation) {
        this.badLitigation = badLitigation;
    }

    public String getMarketPerformance() {
        return marketPerformance;
    }

    public void setMarketPerformance(String marketPerformance) {
        this.marketPerformance = marketPerformance;
    }

    public String getBadMarketPerformance() {
        return badMarketPerformance;
    }

    public void setBadMarketPerformance(String badMarketPerformance) {
        this.badMarketPerformance = badMarketPerformance;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getBadEnvironment() {
        return badEnvironment;
    }

    public void setBadEnvironment(String badEnvironment) {
        this.badEnvironment = badEnvironment;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public String getBadBusiness() {
        return badBusiness;
    }

    public void setBadBusiness(String badBusiness) {
        this.badBusiness = badBusiness;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public String getBadTransaction() {
        return badTransaction;
    }

    public void setBadTransaction(String badTransaction) {
        this.badTransaction = badTransaction;
    }

    public String getInvestment() {
        return investment;
    }

    public void setInvestment(String investment) {
        this.investment = investment;
    }

    public String getBadInvestment() {
        return badInvestment;
    }

    public void setBadInvestment(String badInvestment) {
        this.badInvestment = badInvestment;
    }

    public String getReorganization() {
        return reorganization;
    }

    public void setReorganization(String reorganization) {
        this.reorganization = reorganization;
    }

    public String getBadReorganization() {
        return badReorganization;
    }

    public void setBadReorganization(String badReorganization) {
        this.badReorganization = badReorganization;
    }

    public String getReward() {
        return reward;
    }

    public void setReward(String reward) {
        this.reward = reward;
    }

    public String getBadReward() {
        return badReward;
    }

    public void setBadReward(String badReward) {
        this.badReward = badReward;
    }

    public String getRiskInfo() {
        return riskInfo;
    }

    public void setRiskInfo(String riskInfo) {
        this.riskInfo = riskInfo;
    }

    public String getDebtInfo() {
        return debtInfo;
    }

    public void setDebtInfo(String debtInfo) {
        this.debtInfo = debtInfo;
    }
}