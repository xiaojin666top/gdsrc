package com.beawan.survey.building.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 多元化投资项目(建筑业)
 * @author zxh
 * @date 2020/8/1 16:25
 */
@Entity
@Table(name = "CM_INVEST_PROJECT", schema = "GDTCESYS")
public class InvestProject  extends BaseEntity {

    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_INVEST_PROJECT")
   // @SequenceGenerator(name = "CM_INVEST_PROJECT", allocationSize = 1, initialValue = 1, sequenceName = "CM_INVEST_PROJECT_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;// 任务流水号
    @Column(name = "PROJECT_NAME")
    private String projectName;// 项目名称
    @Column(name = "INDUSTRY")
    private String industry;// 所属行业
    @Column(name = "INDUS_POSITION")
    private String indusPosition;// 所属行业地位
    @Column(name = "INDUS_MATURITY")
    private String indusMaturity;// 行业成熟度
    @Column(name = "INDUS_ACCESS")
    private String indusAccess;// 授信政策行业准入

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getIndusPosition() {
        return indusPosition;
    }

    public void setIndusPosition(String indusPosition) {
        this.indusPosition = indusPosition;
    }

    public String getIndusMaturity() {
        return indusMaturity;
    }

    public void setIndusMaturity(String indusMaturity) {
        this.indusMaturity = indusMaturity;
    }

    public String getIndusAccess() {
        return indusAccess;
    }

    public void setIndusAccess(String indusAccess) {
        this.indusAccess = indusAccess;
    }
}