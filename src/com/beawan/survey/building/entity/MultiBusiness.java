package com.beawan.survey.building.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * 多元业务信息(建筑业)
 * @author zxh
 * @date 2020/8/1 16:12
 */
@Entity
@Table(name = "CM_MULTI_BUSINESS", schema = "GDTCESYS")
public class MultiBusiness  extends BaseEntity {

    @Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="CM_MULTI_BUSINESS")
   // @SequenceGenerator(name="CM_MULTI_BUSINESS",allocationSize=1,initialValue=1, sequenceName="CM_MULTI_BUSINESS_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;// 任务流水号
    @Column(name = "BUSINESS_NAME")
    private String businessName;// 业务名称
    @Column(name = "INVEST_TIME")
    private String investTime;// 初始投资时间
    @Column(name = "DOMAIN")
    private String domain;// 产品应用领域
    @Column(name = "MARKET_DEMAND")
    private String marketDemand;// 产品市场需求
    @Column(name = "MAIN_CUST")
    private String mainCustomer;// 主要客户
    @Column(name = "BUSINESS_AREA")
    private String businessArea;// 业务区域
    @Column(name = "RATIO")
    private double ratio;// 上年度营收占比

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getInvestTime() {
        return investTime;
    }

    public void setInvestTime(String investTime) {
        this.investTime = investTime;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getMarketDemand() {
        return marketDemand;
    }

    public void setMarketDemand(String marketDemand) {
        this.marketDemand = marketDemand;
    }

    public String getMainCustomer() {
        return mainCustomer;
    }

    public void setMainCustomer(String mainCustomer) {
        this.mainCustomer = mainCustomer;
    }

    public String getBusinessArea() {
        return businessArea;
    }

    public void setBusinessArea(String businessArea) {
        this.businessArea = businessArea;
    }

    public double getRatio() {
        return ratio;
    }

    public void setRatio(double ratio) {
        this.ratio = ratio;
    }
}
