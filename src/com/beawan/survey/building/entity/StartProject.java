package com.beawan.survey.building.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * 在建项目情况
 * @author zxh
 * @date 2020/8/3 13:53
 */
@Entity
@Table(name = "CM_START_PROJECT", schema = "GDTCESYS")
public class StartProject extends BaseEntity {

    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_START_PROJECT")
    //@SequenceGenerator(name = "CM_START_PROJECT", allocationSize = 1, initialValue = 1, sequenceName = "CM_START_PROJECT_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;// 任务流水号
    @Column(name = "PROJECT_NAME")
    private String projectName;// 项目名称
    @Column(name = "OWNER_NAME")
    private String ownerName;//业主名称
    @Column(name = "IS_LINKED_COMP")
    private String isLinkedComp;// 是否关联企业
    @Column(name = "START_DATE")
    private String startDate;// 开工日期
    @Column(name = "FINISH_DATE")
    private String finishDate;// 计划完工日期
    @Column(name = "SCHEDULE")
    private double schedule;// 施工进度（%）
    @Column(name = "PRICE_SUM")
    private double priceSum;// 合同总价
    @Column(name = "RECEIVED_AMT")
    private double receivedAmt;// 已收工程款
    @Column(name = "SETTLE_WAY")
    private String settleWay;// 结算方式
    @Column(name = "SETTLE_CYCLE")
    private String settleCycle;// 结算周期

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getIsLinkedComp() {
        return isLinkedComp;
    }

    public void setIsLinkedComp(String isLinkedComp) {
        this.isLinkedComp = isLinkedComp;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    public double getSchedule() {
        return schedule;
    }

    public void setSchedule(double schedule) {
        this.schedule = schedule;
    }

    public double getPriceSum() {
        return priceSum;
    }

    public void setPriceSum(double priceSum) {
        this.priceSum = priceSum;
    }

    public double getReceivedAmt() {
        return receivedAmt;
    }

    public void setReceivedAmt(double receivedAmt) {
        this.receivedAmt = receivedAmt;
    }

    public String getSettleWay() {
        return settleWay;
    }

    public void setSettleWay(String settleWay) {
        this.settleWay = settleWay;
    }

    public String getSettleCycle() {
        return settleCycle;
    }

    public void setSettleCycle(String settleCycle) {
        this.settleCycle = settleCycle;
    }
}
