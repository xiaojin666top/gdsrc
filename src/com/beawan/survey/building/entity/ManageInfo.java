package com.beawan.survey.building.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 内部管理情况（建筑业）
 * @author zxh
 * @date 2020/8/3 10:07
 */
@Entity
@Table(name = "CM_MANAGE_INFO", schema = "GDTCESYS")
public class ManageInfo  extends BaseEntity {

    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_MANAGE_INFO")
   // @SequenceGenerator(name = "CM_MANAGE_INFO", allocationSize = 1, initialValue = 1, sequenceName = "CM_MANAGE_INFO_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;// 任务流水号
    @Column(name = "INDUS_INFO")
    private String industryInfo;// 行业情况
    @Column(name = "DEPART_INFO")
    private String departmentInfo;// 治理结构/部门设置
    @Column(name = "STAFF_STABLE")
    private String staffStability;// 人员稳定性
    @Column(name = "FINANCE_INFO")
    private String financeInfo;// 财务管理情况
    @Column(name = "SITE_MANAGE")
    private String siteManage;// 施工现场管理

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getIndustryInfo() {
        return industryInfo;
    }

    public void setIndustryInfo(String industryInfo) {
        this.industryInfo = industryInfo;
    }

    public String getDepartmentInfo() {
        return departmentInfo;
    }

    public void setDepartmentInfo(String departmentInfo) {
        this.departmentInfo = departmentInfo;
    }

    public String getStaffStability() {
        return staffStability;
    }

    public void setStaffStability(String staffStability) {
        this.staffStability = staffStability;
    }

    public String getFinanceInfo() {
        return financeInfo;
    }

    public void setFinanceInfo(String financeInfo) {
        this.financeInfo = financeInfo;
    }

    public String getSiteManage() {
        return siteManage;
    }

    public void setSiteManage(String siteManage) {
        this.siteManage = siteManage;
    }
}