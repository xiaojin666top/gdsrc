package com.beawan.survey.building.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * 已中标未开工项目情况
 * @author zxh
 * @date 2020/8/3 13:54
 */
@Entity
@Table(name = "CM_PLAN_PROJECT", schema = "GDTCESYS")
public class PlanProject extends BaseEntity {

    @Id
  //  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_PLAN_PROJECT")
   // @SequenceGenerator(name = "CM_PLAN_PROJECT", allocationSize = 1, initialValue = 1, sequenceName = "CM_PLAN_PROJECT_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;// 任务流水号
    @Column(name = "PROJECT_NAME")
    private String projectName;// 中标项目名称
    @Column(name = "OWNER_NAME")
    private String ownerName;// 业主名称
    @Column(name = "IS_LINKED_COMP")
    private String isLinkedComp;// 是否关联企业
    @Column(name = "START_DATE")
    private String startDate;// 计划开工日期
    @Column(name = "FINISH_DATE")
    private String finishDate;// 计划完工日期
    @Column(name = "BID_AMT")
    private double bidAmt;// 中标金额

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getIsLinkedComp() {
        return isLinkedComp;
    }

    public void setIsLinkedComp(String isLinkedComp) {
        this.isLinkedComp = isLinkedComp;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    public double getBidAmt() {
        return bidAmt;
    }

    public void setBidAmt(double bidAmt) {
        this.bidAmt = bidAmt;
    }
}