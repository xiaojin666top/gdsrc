package com.beawan.survey.building.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 企业经营情况（建筑业）
 * @author zxh
 * @date 2020/8/3 10:40
 */
@Entity
@Table(name = "CM_BUSINESS_INFO", schema = "GDTCESYS")
public class BusinessInfo  extends BaseEntity {

    @Id
  //  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_BUSINESS_INFO")
  //  @SequenceGenerator(name = "CM_BUSINESS_INFO", allocationSize = 1, initialValue = 1, sequenceName = "CM_BUSINESS_INFO_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;// 任务流水号
    @Column(name = "UNDERTAKE_WAY")
    private String undertakeWay; // 承接方式
    @Column(name = "AWARDS")
    private String awards; // 获奖情况
    @Column(name = "CONSTR_TYPE")
    private String constructionType; // 施工类型
    @Column(name = "CONSTR_AREA")
    private String constructionArea; // 施工区域
    @Column(name = "TECH_STAFF")
    private String technicalStaff; // 技术人员
    @Column(name = "EQUIPMENT")
    private String equipment; // 工程设备
    @Column(name = "CONSTR_SCALE")
    private String constructionScale; // 施工规模
    @Column(name = "PLAN")
    private String plan; // 经营计划
    @Column(name = "REMARK")
    private String remark; // 分析评价/备注

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUndertakeWay() {
        return undertakeWay;
    }

    public void setUndertakeWay(String undertakeWay) {
        this.undertakeWay = undertakeWay;
    }

    public String getAwards() {
        return awards;
    }

    public void setAwards(String awards) {
        this.awards = awards;
    }

    public String getConstructionType() {
        return constructionType;
    }

    public void setConstructionType(String constructionType) {
        this.constructionType = constructionType;
    }

    public String getConstructionArea() {
        return constructionArea;
    }

    public void setConstructionArea(String constructionArea) {
        this.constructionArea = constructionArea;
    }

    public String getTechnicalStaff() {
        return technicalStaff;
    }

    public void setTechnicalStaff(String technicalStaff) {
        this.technicalStaff = technicalStaff;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public String getConstructionScale() {
        return constructionScale;
    }

    public void setConstructionScale(String constructionScale) {
        this.constructionScale = constructionScale;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}