package com.beawan.survey.building.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 申贷项目
 * @author zxh
 * @date 2020/8/3 19:45
 */
@Entity
@Table(name = "CM_LOAN_PROJECT", schema = "GDTCESYS")
public class LoanProject extends BaseEntity {

    @Id
 //   @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_LOAN_PROJECT")
  //  @SequenceGenerator(name = "CM_LOAN_PROJECT", allocationSize = 1, initialValue = 1, sequenceName = "CM_LOAN_PROJECT_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;// 任务流水号
    @Column(name = "PROJECT_NAME")
    private String projectName;// 项目名称
    @Column(name = "SUPPLY_WAY")
    private String supplyWay;// 材料供应方式
    @Column(name = "CONTRACT_AMT")
    private double contractAmt;// 合同金额
    @Column(name = "PAY_WAY")
    private String paymentWay;// 付款方式
    @Column(name = "PLAN_AMT")
    private double plannedAmt;// 需投入金额
    @Column(name = "INVEST_AMT")
    private double investedAmt;// 已投入金额
    @Column(name = "MATERIAL_COST")
    private double materialCost;// 物料费
    @Column(name = "MATERIAL_PAY_WAY")
    private String materialsPayWay;// 物料支付方式
    @Column(name = "LABOR_COST")
    private double laborCost;// 人工费
    @Column(name = "OTHER_COST")
    private double otherCost;// 其他费用
    @Column(name = "FUNDS_SOURCE")
    private String fundsSource;// 资金来源
    @Column(name = "SCHEDULE")
    private String schedule;// 项目进度
    @Column(name = "PAY_INFO")
    private String paymentInfo;// 工程付款情况
    @Column(name = "SITE_INFO")
    private String siteInfo;// 项目现场情况

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getSupplyWay() {
        return supplyWay;
    }

    public void setSupplyWay(String supplyWay) {
        this.supplyWay = supplyWay;
    }

    public double getContractAmt() {
        return contractAmt;
    }

    public void setContractAmt(double contractAmt) {
        this.contractAmt = contractAmt;
    }

    public String getPaymentWay() {
        return paymentWay;
    }

    public void setPaymentWay(String paymentWay) {
        this.paymentWay = paymentWay;
    }

    public double getPlannedAmt() {
        return plannedAmt;
    }

    public void setPlannedAmt(double plannedAmt) {
        this.plannedAmt = plannedAmt;
    }

    public double getInvestedAmt() {
        return investedAmt;
    }

    public void setInvestedAmt(double investedAmt) {
        this.investedAmt = investedAmt;
    }

    public double getMaterialCost() {
        return materialCost;
    }

    public void setMaterialCost(double materialCost) {
        this.materialCost = materialCost;
    }

    public String getMaterialsPayWay() {
        return materialsPayWay;
    }

    public void setMaterialsPayWay(String materialsPayWay) {
        this.materialsPayWay = materialsPayWay;
    }

    public double getLaborCost() {
        return laborCost;
    }

    public void setLaborCost(double laborCost) {
        this.laborCost = laborCost;
    }

    public double getOtherCost() {
        return otherCost;
    }

    public void setOtherCost(double otherCost) {
        this.otherCost = otherCost;
    }

    public String getFundsSource() {
        return fundsSource;
    }

    public void setFundsSource(String fundsSource) {
        this.fundsSource = fundsSource;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getPaymentInfo() {
        return paymentInfo;
    }

    public void setPaymentInfo(String paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public String getSiteInfo() {
        return siteInfo;
    }

    public void setSiteInfo(String siteInfo) {
        this.siteInfo = siteInfo;
    }
}