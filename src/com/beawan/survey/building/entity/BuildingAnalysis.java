package com.beawan.survey.building.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;

/**
 * 建筑业 分析情况说明
 * @author User
 *
 */
@Entity
@Table(name = "CM_BUILD_ANALY",schema = "GDTCESYS")
public class BuildingAnalysis extends BaseEntity{

	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="CM_BUILD_ANALY")
	//@SequenceGenerator(name="CM_BUILD_ANALY",allocationSize=1,initialValue=1, sequenceName="CM_BUILD_ANALY_SEQ")
	private Long id;
	@Column(name = "SER_NO")
	private String serNo; //业务流水号
	@Column(name = "MULTI_BUSINESS")
	private String multiBusiness; //多元业务情况
	@Column(name = "INVEST_PROJECT")
	private String investProject; //投资项目情况
	@Column(name = "FINISH_PROJECT")
	private String finishProject; //完工项目情况
	@Column(name = "START_PROJECT")
	private String startProject; //在建项目情况
	@Column(name = "PLAN_PROJECT")
	private String planProject; //中标项目情况
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSerNo() {
		return serNo;
	}
	public void setSerNo(String serNo) {
		this.serNo = serNo;
	}
	public String getMultiBusiness() {
		return multiBusiness;
	}
	public void setMultiBusiness(String multiBusiness) {
		this.multiBusiness = multiBusiness;
	}
	public String getInvestProject() {
		return investProject;
	}
	public void setInvestProject(String investProject) {
		this.investProject = investProject;
	}
	public String getFinishProject() {
		return finishProject;
	}
	public void setFinishProject(String finishProject) {
		this.finishProject = finishProject;
	}
	public String getStartProject() {
		return startProject;
	}
	public void setStartProject(String startProject) {
		this.startProject = startProject;
	}
	public String getPlanProject() {
		return planProject;
	}
	public void setPlanProject(String planProject) {
		this.planProject = planProject;
	}
}
