package com.beawan.survey.building.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;
import java.sql.Timestamp;
/**
 * 近三年完工项目情况
 * @author zxh
 * @date 2020/8/3 13:51
 */
@Entity
@Table(name = "CM_FINISH_PROJECT", schema = "GDTCESYS")
public class FinishProject  extends BaseEntity {

    @Id
  //  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_FINISH_PROJECT")
   // @SequenceGenerator(name = "CM_FINISH_PROJECT", allocationSize = 1, initialValue = 1, sequenceName = "CM_FINISH_PROJECT_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;// 任务流水号
    @Column(name = "PROJECT_NAME")
    private String projectName;// 项目名称
    @Column(name = "EMPLOYER")
    private String employer;// 发包方
    @Column(name = "CONTRACTOR")
    private String contractor;// 总承包方
    @Column(name = "START_DATE")
    private String startDate;// 开工日期
    @Column(name = "FINISH_DATE")
    private String finishDate;// 竣工日期
    @Column(name = "PRICE_SUM")
    private double priceSum;// 合同总价
    @Column(name = "SCHEDULE")
    private String schedule;// 工程进度
    @Column(name = "SETTLED_AMT")
    private double settledAmt;// 已结算工程款
    @Column(name = "RECEIVABLE_AMT")
    private double receivableAmt;// 应收账款
    @Column(name = "PAYABLE_AMT")
    private double payableAmt; // 应付账款

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public String getContractor() {
        return contractor;
    }

    public void setContractor(String contractor) {
        this.contractor = contractor;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    public double getPriceSum() {
        return priceSum;
    }

    public void setPriceSum(double priceSum) {
        this.priceSum = priceSum;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public double getSettledAmt() {
        return settledAmt;
    }

    public void setSettledAmt(double settledAmt) {
        this.settledAmt = settledAmt;
    }

    public double getReceivableAmt() {
        return receivableAmt;
    }

    public void setReceivableAmt(double receivableAmt) {
        this.receivableAmt = receivableAmt;
    }

    public double getPayableAmt() {
        return payableAmt;
    }

    public void setPayableAmt(double payableAmt) {
        this.payableAmt = payableAmt;
    }
}