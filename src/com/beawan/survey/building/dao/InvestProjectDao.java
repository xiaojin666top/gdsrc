package com.beawan.survey.building.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.building.entity.InvestProject;

/**
 * @author yzj
 */
public interface InvestProjectDao extends BaseDao<InvestProject> {
}
