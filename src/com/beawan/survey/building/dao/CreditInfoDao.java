package com.beawan.survey.building.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.bean.CompFinancingBank;

/**
 * 企业授信情况（建筑业）
 * @author zxh
 * @date 2020/8/4 11:26
 */
public interface CreditInfoDao extends BaseDao<CompFinancingBank> {
}
