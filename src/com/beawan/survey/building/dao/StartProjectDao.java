package com.beawan.survey.building.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.building.entity.StartProject;

/**
 * @author yzj
 */
public interface StartProjectDao extends BaseDao<StartProject> {
}
