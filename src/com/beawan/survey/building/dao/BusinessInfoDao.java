package com.beawan.survey.building.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.building.entity.BusinessInfo;

/**
 * @author yzj
 */
public interface BusinessInfoDao extends BaseDao<BusinessInfo> {
}
