package com.beawan.survey.building.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.building.entity.PlanProject;

/**
 * @author yzj
 */
public interface PlanProjectDao extends BaseDao<PlanProject> {
}
