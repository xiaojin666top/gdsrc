package com.beawan.survey.building.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.building.entity.FinishProject;

/**
 * @author yzj
 */
public interface FinishProjectDao extends BaseDao<FinishProject> {
}
