package com.beawan.survey.building.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.building.entity.LoanProject;

/**
 * @author yzj
 */
public interface LoanProjectDao extends BaseDao<LoanProject> {
}
