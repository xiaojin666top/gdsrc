package com.beawan.survey.building.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.building.entity.ManageInfo;

/**
 * @author yzj
 */
public interface ManageInfoDao extends BaseDao<ManageInfo> {
}
