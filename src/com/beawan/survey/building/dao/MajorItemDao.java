package com.beawan.survey.building.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.building.entity.MajorItem;

/**
 * @author yzj
 */
public interface MajorItemDao extends BaseDao<MajorItem> {
}
