package com.beawan.survey.building.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.building.dao.InvestProjectDao;
import com.beawan.survey.building.entity.InvestProject;
import org.springframework.stereotype.Repository;
import com.beawan.survey.building.entity.InvestProject;

/**
 * @author yzj
 */
@Repository("investProjectDao")
public class InvestProjectDaoImpl extends BaseDaoImpl<InvestProject> implements InvestProjectDao{

}
