package com.beawan.survey.building.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.building.dao.ManageInfoDao;
import com.beawan.survey.building.entity.ManageInfo;
import org.springframework.stereotype.Repository;
import com.beawan.survey.building.entity.ManageInfo;

/**
 * @author yzj
 */
@Repository("manageInfoDao")
public class ManageInfoDaoImpl extends BaseDaoImpl<ManageInfo> implements ManageInfoDao{

}
