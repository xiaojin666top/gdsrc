package com.beawan.survey.building.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.building.dao.BusinessInfoDao;
import com.beawan.survey.building.entity.BusinessInfo;
import org.springframework.stereotype.Repository;
import com.beawan.survey.building.entity.BusinessInfo;

/**
 * @author yzj
 */
@Repository("businessInfoDao")
public class BusinessInfoDaoImpl extends BaseDaoImpl<BusinessInfo> implements BusinessInfoDao{

}
