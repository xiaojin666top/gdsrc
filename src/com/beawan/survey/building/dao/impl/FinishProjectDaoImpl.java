package com.beawan.survey.building.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.building.dao.FinishProjectDao;
import com.beawan.survey.building.entity.FinishProject;
import org.springframework.stereotype.Repository;
import com.beawan.survey.building.entity.FinishProject;

/**
 * @author yzj
 */
@Repository("finishProjectDao")
public class FinishProjectDaoImpl extends BaseDaoImpl<FinishProject> implements FinishProjectDao{

}
