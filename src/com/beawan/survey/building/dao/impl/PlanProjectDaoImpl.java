package com.beawan.survey.building.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.building.dao.PlanProjectDao;
import com.beawan.survey.building.entity.PlanProject;
import org.springframework.stereotype.Repository;
import com.beawan.survey.building.entity.PlanProject;

/**
 * @author yzj
 */
@Repository("planProjectDao")
public class PlanProjectDaoImpl extends BaseDaoImpl<PlanProject> implements PlanProjectDao{

}
