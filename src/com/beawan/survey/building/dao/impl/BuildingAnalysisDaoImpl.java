package com.beawan.survey.building.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.building.dao.BuildingAnalysisDao;
import org.springframework.stereotype.Repository;
import com.beawan.survey.building.entity.BuildingAnalysis;

/**
 * @author yzj
 */
@Repository("buildingAnalysisDao")
public class BuildingAnalysisDaoImpl extends BaseDaoImpl<BuildingAnalysis> implements BuildingAnalysisDao{

}
