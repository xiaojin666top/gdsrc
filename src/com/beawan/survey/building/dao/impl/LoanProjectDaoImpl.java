package com.beawan.survey.building.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.building.dao.LoanProjectDao;
import com.beawan.survey.building.entity.LoanProject;
import org.springframework.stereotype.Repository;
import com.beawan.survey.building.entity.LoanProject;

/**
 * @author yzj
 */
@Repository("loanProjectDao")
public class LoanProjectDaoImpl extends BaseDaoImpl<LoanProject> implements LoanProjectDao{

}
