package com.beawan.survey.building.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.building.dao.MultiBusinessDao;
import com.beawan.survey.building.entity.MultiBusiness;
import org.springframework.stereotype.Repository;
import com.beawan.survey.building.entity.MultiBusiness;

/**
 * @author yzj
 */
@Repository("multiBusinessDao")
public class MultiBusinessDaoImpl extends BaseDaoImpl<MultiBusiness> implements MultiBusinessDao{

}
