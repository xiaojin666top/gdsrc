package com.beawan.survey.building.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.building.dao.CreditInfoDao;
import com.beawan.survey.custInfo.bean.CompFinancingBank;
import org.springframework.stereotype.Repository;

/**
 * @author zxh
 * @date 2020/8/4 11:27
 */
@Repository("creditInfoDao")
public class CreditInfoDaoImpl extends BaseDaoImpl<CompFinancingBank> implements CreditInfoDao {
}
