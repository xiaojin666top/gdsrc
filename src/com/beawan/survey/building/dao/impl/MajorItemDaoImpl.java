package com.beawan.survey.building.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.building.dao.MajorItemDao;
import com.beawan.survey.building.entity.MajorItem;
import org.springframework.stereotype.Repository;
import com.beawan.survey.building.entity.MajorItem;

/**
 * @author yzj
 */
@Repository("majorItemDao")
public class MajorItemDaoImpl extends BaseDaoImpl<MajorItem> implements MajorItemDao{

}
