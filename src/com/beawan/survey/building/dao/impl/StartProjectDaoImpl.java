package com.beawan.survey.building.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.building.dao.StartProjectDao;
import com.beawan.survey.building.entity.StartProject;
import org.springframework.stereotype.Repository;
import com.beawan.survey.building.entity.StartProject;

/**
 * @author yzj
 */
@Repository("startProjectDao")
public class StartProjectDaoImpl extends BaseDaoImpl<StartProject> implements StartProjectDao{

}
