package com.beawan.survey.building.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.building.entity.MultiBusiness;

/**
 * @author yzj
 */
public interface MultiBusinessDao extends BaseDao<MultiBusiness> {
}
