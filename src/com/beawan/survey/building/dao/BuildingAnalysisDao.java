package com.beawan.survey.building.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.building.entity.BuildingAnalysis;

/**
 * @author yzj
 */
public interface BuildingAnalysisDao extends BaseDao<BuildingAnalysis> {
}
