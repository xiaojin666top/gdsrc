package com.beawan.survey.building.controller;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.base.entity.User;
import com.beawan.common.Constants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.survey.building.dto.LoanProjectDto;
import com.beawan.survey.building.entity.BuildingAnalysis;
import com.beawan.survey.building.entity.FinishProject;
import com.beawan.survey.building.entity.LoanProject;
import com.beawan.survey.building.entity.PlanProject;
import com.beawan.survey.building.entity.StartProject;
import com.beawan.survey.building.service.BuildingAnalysisService;
import com.beawan.survey.building.service.FinishProjectService;
import com.beawan.survey.building.service.LoanProjectService;
import com.beawan.survey.building.service.PlanProjectService;
import com.beawan.survey.building.service.StartProjectService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.platform.util.DateUtil;
import com.platform.util.HttpUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.MapperUtil;

/**
 * 项目概况 控制器(建筑业)
 * @author zxh
 * @date 2020/8/3 11:31
 */
@Controller
@UserSessionAnnotation
@RequestMapping({"/survey/building/"})
public class ProjectOverviewController extends BaseController {

    private static final Logger log = Logger.getLogger(ProjectOverviewController.class);

    @Resource
    private FinishProjectService finishProjectService;
    @Resource
    private StartProjectService startProjectService;
    @Resource
    private PlanProjectService planProjectService;
    @Resource
    private LoanProjectService loanProjectService;
    @Resource
    private BuildingAnalysisService buildingAnalysisService;

    /**
     * 跳转到 完工项目情况
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("finishProject.do")
    public ModelAndView finishProject(String serNo) {
        ModelAndView mv = new ModelAndView("views/survey/building/finishProject");
        try {
        	mv.addObject("serNo", serNo);
        	BuildingAnalysis entity =
        			buildingAnalysisService.selectSingleByProperty("serNo", serNo);
        	if(entity != null){
        		mv.addObject("finishProject", entity.getFinishProject());
        	}
		} catch (Exception e) {
			e.printStackTrace();
		}
        return mv;
    }

    /**
     * 跳转到 在建项目情况
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("startProject.do")
    public ModelAndView startProject(String serNo) {
        ModelAndView mv = new ModelAndView("views/survey/building/startProject");
        try {
        	mv.addObject("serNo", serNo);
        	BuildingAnalysis entity =
        			buildingAnalysisService.selectSingleByProperty("serNo", serNo);
        	if(entity != null){
        		mv.addObject("startProject", entity.getStartProject());
        	}
		} catch (Exception e) {
			e.printStackTrace();
		}
        return mv;
    }

    /**
     * 跳转到 已中标未开工项目情况
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("planProject.do")
    public ModelAndView planProject(String serNo) {
        ModelAndView mv = new ModelAndView("views/survey/building/planProject");
        try {
        	mv.addObject("serNo", serNo);
        	BuildingAnalysis entity =
        			buildingAnalysisService.selectSingleByProperty("serNo", serNo);
        	if(entity != null){
        		mv.addObject("planProject", entity.getPlanProject());
        	}
		} catch (Exception e) {
			e.printStackTrace();
		}
        return mv;
    }

    /**
     * 获取 完工项目列表
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("getFinishProjectList.json")
    @ResponseBody
    public ResultDto getFinishProjectList(String serNo) {
        ResultDto re = returnFail("获取数据失败");
        try {
            List<FinishProject> list = finishProjectService.selectByProperty("serNo", serNo);

            re.setMsg("获取数据成功");
            re.setCode(Constants.NORMAL);
            re.setRows(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 保存/修改 完工项目信息
     * @param request
     * @return
     */
    @RequestMapping("saveFinishProject.json")
    @ResponseBody
    public ResultDto saveFinishProject(HttpServletRequest request) {
        ResultDto re = returnFail("保存失败");
        String jsonData = request.getParameter("jsonData");
        try {
            FinishProject project = JacksonUtil.fromJson(jsonData, FinishProject.class);
            if (project == null) {
                re.setMsg("请先填写相关信息！");
                return re;
            }

            User user = HttpUtil.getCurrentUser(request);
            if (project.getId() == null) {
                project.setCreater(user.getUserId());
                project.setCreateTime(DateUtil.getNowTimestamp());
            }
            project.setStatus(Constants.NORMAL);
            project.setUpdater(user.getUserId());
            project.setUpdateTime(DateUtil.getNowTimestamp());
            finishProjectService.saveOrUpdate(project);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 删除 完工项目信息
     * @param request
     * @param id
     * @return
     */
    @RequestMapping("deleteFinishProject.json")
    @ResponseBody
    public ResultDto deleteFinishProject(HttpServletRequest request, Integer id) {
        ResultDto re = returnFail("删除数据异常！");
        try {
            User user = HttpUtil.getCurrentUser(request);
            FinishProject project = finishProjectService.selectByPrimaryKey(id);
            project.setStatus(Constants.DELETE);
            project.setUpdater(user.getUserId());
            project.setUpdateTime(DateUtil.getNowTimestamp());
            finishProjectService.saveOrUpdate(project);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("删除数据成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 获取 在建项目列表
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("getStartProjectList.json")
    @ResponseBody
    public ResultDto getStartProjectList(String serNo) {
        ResultDto re = returnFail("获取数据失败");
        try {
            List<StartProject> list = startProjectService.selectByProperty("serNo", serNo);

            re.setMsg("获取数据成功");
            re.setCode(Constants.NORMAL);
            re.setRows(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 保存/修改 在建项目信息
     * @param request
     * @return
     */
    @RequestMapping("saveStartProject.json")
    @ResponseBody
    public ResultDto saveStartProject(HttpServletRequest request) {
        ResultDto re = returnFail("保存失败");
        String jsonData = request.getParameter("jsonData");
        try {
            StartProject project = JacksonUtil.fromJson(jsonData, StartProject.class);
            if (project == null) {
                re.setMsg("请先填写相关信息！");
                return re;
            }

            User user = HttpUtil.getCurrentUser(request);
            if (project.getId() == null) {
                project.setCreater(user.getUserId());
                project.setCreateTime(DateUtil.getNowTimestamp());
            }
            project.setStatus(Constants.NORMAL);
            project.setUpdater(user.getUserId());
            project.setUpdateTime(DateUtil.getNowTimestamp());
            startProjectService.saveOrUpdate(project);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 删除 在建项目信息
     * @param request
     * @param id
     * @return
     */
    @RequestMapping("deleteStartProject.json")
    @ResponseBody
    public ResultDto deleteStartProject(HttpServletRequest request, Integer id) {
        ResultDto re = returnFail("删除数据异常！");
        try {
            User user = HttpUtil.getCurrentUser(request);
            StartProject project = startProjectService.selectByPrimaryKey(id);
            project.setStatus(Constants.DELETE);
            project.setUpdater(user.getUserId());
            project.setUpdateTime(DateUtil.getNowTimestamp());
            startProjectService.saveOrUpdate(project);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("删除数据成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 获取 中标项目列表
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("getPlanProjectList.json")
    @ResponseBody
    public ResultDto getPlanProjectList(String serNo) {
        ResultDto re = returnFail("获取数据失败");
        try {
            List<PlanProject> list = planProjectService.selectByProperty("serNo", serNo);

            re.setMsg("获取数据成功");
            re.setCode(Constants.NORMAL);
            re.setRows(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 保存/修改 中标项目信息
     * @param request
     * @return
     */
    @RequestMapping("savePlanProject.json")
    @ResponseBody
    public ResultDto savePlanProject(HttpServletRequest request, String jsonData) {
        ResultDto re = returnFail("保存失败");
        try {
            List<PlanProject> entityList 
                    = JacksonUtil.fromJson(jsonData, new TypeReference<List<PlanProject>>() {});
            if (CollectionUtils.isEmpty(entityList))
                return returnFail("请填写相关信息！");
            
            User user = HttpUtil.getCurrentUser(request);
            Timestamp nowTimestamp = DateUtil.getNowTimestamp();
            for (PlanProject entity : entityList) {
                if (entity.getId() == null) {
                    entity.setCreater(user.getUserId());
                    entity.setCreateTime(nowTimestamp);
                }
                entity.setStatus(Constants.NORMAL);
                entity.setUpdater(user.getUserId());
                entity.setUpdateTime(nowTimestamp);
                planProjectService.saveOrUpdate(entity);
            }

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 删除 中标项目信息
     * @param request
     * @param id
     * @return
     */
    @RequestMapping("deletePlanProject.json")
    @ResponseBody
    public ResultDto deletePlanProject(HttpServletRequest request, Integer id) {
        ResultDto re = returnFail("删除数据异常！");
        try {
            User user = HttpUtil.getCurrentUser(request);
            PlanProject project = planProjectService.selectByPrimaryKey(id);
            project.setStatus(Constants.DELETE);
            project.setUpdater(user.getUserId());
            project.setUpdateTime(DateUtil.getNowTimestamp());
            planProjectService.saveOrUpdate(project);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("删除数据成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 跳转到 申贷项目情况
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("loanProject.do")
    public ModelAndView loanProject(String serNo) {
        ModelAndView mv = new ModelAndView("views/survey/building/loanProject");
        mv.addObject("serNo", serNo);
        return mv;
    }

    /**
     * 新增/查看 申贷项目详情
     * @param serNo 任务流水号
     * @param id 主键
     * @return
     */
    @RequestMapping("loanProjectInfo.do")
    public ModelAndView loanProjectInfo(String serNo,
            @RequestParam(value = "id", required = false) Integer id) {
        ModelAndView mv = new ModelAndView("views/survey/building/loanProjectInfo");
        LoanProject project = loanProjectService.selectByPrimaryKey(id);
        mv.addObject("project", project);
        mv.addObject("serNo", serNo);
        return mv;
    }

    /**
     * 获取 申贷项目列表
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("getLoanProjectList.do")
    @ResponseBody
    public ResultDto getLoanProjectList(String serNo) {
        ResultDto re = returnFail("获取数据失败");
        try {
            List<LoanProject> list = loanProjectService.selectByProperty("serNo", serNo);
            re.setMsg("获取数据成功");
            re.setCode(Constants.NORMAL);
            re.setRows(MapperUtil.trans(list, LoanProjectDto.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 删除 申贷项目信息
     * @param request
     * @param id 主键
     * @return
     */
    @RequestMapping("removeLoanProject.do")
    @ResponseBody
    public ResultDto removeLoanProject(HttpServletRequest request, Integer id) {
        ResultDto re = returnFail("删除数据异常！");
        try {
            User user = HttpUtil.getCurrentUser(request);
            LoanProject project = loanProjectService.selectByPrimaryKey(id);
            project.setStatus(Constants.DELETE);
            project.setUpdater(user.getUserId());
            project.setUpdateTime(DateUtil.getNowTimestamp());
            loanProjectService.saveOrUpdate(project);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("删除数据成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 保存/修改 申贷项目信息
     *
     * @param request
     * @return
     */
    @RequestMapping("saveLoanProject.do")
    @ResponseBody
    public ResultDto saveLoanProject(HttpServletRequest request) {
        ResultDto re = returnFail("保存失败");
        String jsonData = request.getParameter("jsonData");
        try {
            LoanProject project = JacksonUtil.fromJson(jsonData, LoanProject.class);
            if (project == null) {
                re.setMsg("请先填写相关信息！");
                return re;
            }

            User user = HttpUtil.getCurrentUser(request);
            if (project.getId() == null) {
                project.setCreater(user.getUserId());
                project.setCreateTime(DateUtil.getNowTimestamp());
            }
            project.setStatus(Constants.NORMAL);
            project.setUpdater(user.getUserId());
            project.setUpdateTime(DateUtil.getNowTimestamp());
            loanProjectService.saveOrUpdate(project);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }
}
