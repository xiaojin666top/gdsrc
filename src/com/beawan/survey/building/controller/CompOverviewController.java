package com.beawan.survey.building.controller;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.base.entity.User;
import com.beawan.common.Constants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.survey.building.entity.BuildingAnalysis;
import com.beawan.survey.building.entity.BusinessInfo;
import com.beawan.survey.building.entity.InvestProject;
import com.beawan.survey.building.entity.ManageInfo;
import com.beawan.survey.building.entity.MultiBusiness;
import com.beawan.survey.building.service.BuildingAnalysisService;
import com.beawan.survey.building.service.BusinessInfoService;
import com.beawan.survey.building.service.InvestProjectService;
import com.beawan.survey.building.service.ManageInfoService;
import com.beawan.survey.building.service.MultiBusinessService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.platform.util.DateUtil;
import com.platform.util.HttpUtil;
import com.platform.util.JacksonUtil;

/**
 * 企业概况 控制器(建筑业)
 * @author zxh
 * @date 2020/8/1 16:08
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/survey/building/" })
public class CompOverviewController  extends BaseController {

    private static final Logger log = Logger.getLogger(CompOverviewController.class);

    @Resource
    private MultiBusinessService multiBusinessService;
    @Resource
    private InvestProjectService investProjectService;
    @Resource
    private BusinessInfoService businessInfoService;
    @Resource
    private ManageInfoService manageInfoService;
    @Resource
    private BuildingAnalysisService buildingAnalysisService;

    /**
     * 跳转到 多元业务情况
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("multipleBusiness.do")
    public ModelAndView multipleBusiness(String serNo){
        ModelAndView mv = new ModelAndView("views/survey/building/multipleBusiness");
        try {
        	mv.addObject("serNo", serNo);
        	BuildingAnalysis entity =
        			buildingAnalysisService.selectSingleByProperty("serNo", serNo);
        	if(entity != null){
        		mv.addObject("multiBusiness", entity.getMultiBusiness());
        	}
		} catch (Exception e) {
			e.printStackTrace();
		}
        return mv;
    }

    /**
     * 跳转到 多元化投资项目
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("investProject.do")
    public ModelAndView investProject(String serNo){
        ModelAndView mv = new ModelAndView("views/survey/building/investProject");
        try {
        	mv.addObject("serNo", serNo);
        	BuildingAnalysis entity =
        			buildingAnalysisService.selectSingleByProperty("serNo", serNo);
        	if(entity != null){
        		mv.addObject("investProject", entity.getInvestProject());
        	}
		} catch (Exception e) {
			e.printStackTrace();
		}
        return mv;
    }

    /**
     * 获取 多元业务信息列表
     * @param serNo
     * @return
     */
    @RequestMapping("getBusinessList.json")
    @ResponseBody
    public ResultDto getBusinessList(String serNo){
        ResultDto re = returnFail("获取数据失败");
        try {
            List<MultiBusiness> list = multiBusinessService.selectByProperty("serNo", serNo);

            re.setMsg("获取数据成功");
            re.setCode(Constants.NORMAL);
            re.setRows(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 保存/修改 多元业务信息
     * @param request
     * @return
     */
    @RequestMapping("saveBusiness.json")
    @ResponseBody
    public ResultDto saveBusiness(HttpServletRequest request, String jsonArray) {
        ResultDto re = returnFail("保存失败");
        try {
        	List<MultiBusiness> entityList =
                    JacksonUtil.fromJson(jsonArray, new TypeReference<List<MultiBusiness>>() {});
            if (CollectionUtils.isEmpty(entityList))
                return returnFail("请填写相关信息");

            User user = HttpUtil.getCurrentUser(request);
            Timestamp nowTimestamp = DateUtil.getNowTimestamp();
            for (MultiBusiness entity : entityList) {
                if (entity.getId() == null){
                    entity.setCreater(user.getUserId());
                    entity.setCreateTime(nowTimestamp);
                }
                entity.setStatus(Constants.NORMAL);
                entity.setUpdater(user.getUserId());
                entity.setUpdateTime(nowTimestamp);
                multiBusinessService.saveOrUpdate(entity);
            }

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 删除 多元业务信息
     * @param request
     * @param id
     * @return
     */
    @RequestMapping("deleteMultiBusiness.json")
    @ResponseBody
    public ResultDto deleteMultiBusiness(HttpServletRequest request, Integer id){
        ResultDto re = returnFail("删除数据异常！");
        try{
            User user = HttpUtil.getCurrentUser(request);
            MultiBusiness business = multiBusinessService.selectByPrimaryKey(id);
            business.setStatus(Constants.DELETE);
            business.setUpdater(user.getUserId());
            business.setUpdateTime(DateUtil.getNowTimestamp());
            multiBusinessService.saveOrUpdate(business);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("删除数据成功！");
        }catch (Exception e){
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 获取 多元化投资项目列表
     * @param serNo
     * @return
     */
    @RequestMapping("getProjectList.json")
    @ResponseBody
    public ResultDto getProjectList(String serNo){
        ResultDto re = returnFail("获取数据失败");
        try {
            List<InvestProject> list = investProjectService.selectByProperty("serNo", serNo);

            re.setMsg("获取数据成功");
            re.setCode(Constants.NORMAL);
            re.setRows(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 保存/修改 多元化投资项目
     * @param request
     * @return
     */
    @RequestMapping("saveProject.json")
    @ResponseBody
    public ResultDto saveProject(HttpServletRequest request, String jsonArray) {
        ResultDto re = returnFail("保存失败");
        try {
            List<InvestProject> entityList =
                    JacksonUtil.fromJson(jsonArray, new TypeReference<List<InvestProject>>() {});
            if(CollectionUtils.isEmpty(entityList))
                return returnFail("请先填写相关信息！");

            User user = HttpUtil.getCurrentUser(request);
            Timestamp nowTimestamp = DateUtil.getNowTimestamp();
            for (InvestProject entity : entityList) {
                if (entity.getId() == null){
                    entity.setCreater(user.getUserId());
                    entity.setCreateTime(nowTimestamp);
                }
                entity.setStatus(Constants.NORMAL);
                entity.setUpdater(user.getUserId());
                entity.setUpdateTime(nowTimestamp);
                investProjectService.saveOrUpdate(entity);
            }

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 删除 多元业务信息
     * @param request
     * @param id
     * @return
     */
    @RequestMapping("deleteProject.json")
    @ResponseBody
    public ResultDto deleteProject(HttpServletRequest request, Integer id){
        ResultDto re = returnFail("删除数据异常！");
        try{
            User user = HttpUtil.getCurrentUser(request);
            InvestProject project = investProjectService.selectByPrimaryKey(id);
            project.setStatus(Constants.DELETE);
            project.setUpdater(user.getUserId());
            project.setUpdateTime(DateUtil.getNowTimestamp());
            investProjectService.saveOrUpdate(project);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("删除数据成功！");
        }catch (Exception e){
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 跳转到 内部管理情况
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("innerManage.do")
    public ModelAndView innerManage(String serNo){
        ModelAndView mv = new ModelAndView("views/survey/building/innerManage");
        ManageInfo manageInfo = manageInfoService.selectSingleByProperty("serNo", serNo);
        mv.addObject("serNo", serNo);
        mv.addObject("manage", manageInfo);
        return mv;
    }

    /**
     * 跳转到 企业经营情况
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("businessInfo.do")
    public ModelAndView businessInfo(String serNo){
        ModelAndView mv = new ModelAndView("views/survey/building/businessInfo");
        BusinessInfo businessInfo = businessInfoService.selectSingleByProperty("serNo", serNo);
        mv.addObject("serNo", serNo);
        mv.addObject("businessInfo", businessInfo);
        return mv;
    }

    /**
     * 保存/修改 企业经营情况
     * @param request
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("saveBusinessInfo.do")
    @ResponseBody
    public ResultDto saveBusinessInfo(HttpServletRequest request, String serNo) {
        ResultDto re = returnFail("保存失败");
        String jsonData = request.getParameter("businessInfo");
        try {
            BusinessInfo businessInfo = JacksonUtil.fromJson(jsonData, BusinessInfo.class);
            if (businessInfo == null) {
                re.setMsg("请先填写相关信息！");
                return re;
            }

            BusinessInfo oldInfo = businessInfoService.selectSingleByProperty("serNo", serNo);
            if (oldInfo != null)  {
                businessInfo.setId(oldInfo.getId());
                businessInfo.setSerNo(oldInfo.getSerNo());
            }

            User user = HttpUtil.getCurrentUser(request);
            if(businessInfo.getId() == null){
                businessInfo.setCreater(user.getUserId());
                businessInfo.setCreateTime(DateUtil.getNowTimestamp());
            }
            businessInfo.setUpdater(user.getUserId());
            businessInfo.setUpdateTime(DateUtil.getNowTimestamp());
            businessInfo.setStatus(Constants.NORMAL);
            businessInfoService.saveOrUpdate(businessInfo);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 保存/修改 内部管理情况
     * @param request
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("saveManageInfo.do")
    @ResponseBody
    public ResultDto saveManageInfo(HttpServletRequest request, String serNo) {
        ResultDto re = returnFail("保存失败");
        String jsonData = request.getParameter("manageInfo");
        try {
            ManageInfo manageInfo = JacksonUtil.fromJson(jsonData, ManageInfo.class);
            if (manageInfo == null) {
                re.setMsg("请先填写相关信息！");
                return re;
            }

            ManageInfo oldInfo = manageInfoService.selectSingleByProperty("serNo", serNo);
            if (oldInfo != null)  {
                manageInfo.setId(oldInfo.getId());
                manageInfo.setSerNo(oldInfo.getSerNo());
            }

            User user = HttpUtil.getCurrentUser(request);
            if(manageInfo.getId() == null){
                manageInfo.setCreater(user.getUserId());
                manageInfo.setCreateTime(DateUtil.getNowTimestamp());
            }
            manageInfo.setUpdater(user.getUserId());
            manageInfo.setUpdateTime(DateUtil.getNowTimestamp());
            manageInfo.setStatus(Constants.NORMAL);
            manageInfoService.saveOrUpdate(manageInfo);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }
    
    /**
	 * 保存 建筑业 分析说明信息
	 * @param serNo 流水号
	 * @return
	 */
	@RequestMapping(value = "saveBulidingAnalysis.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto saveBulidingAnalysis(HttpServletRequest request,String serNo){
		ResultDto re = returnFail("保存失败");
		String multiBusiness = request.getParameter("multiBusiness");
		String investProject = request.getParameter("investProject");
		String finishProject = request.getParameter("finishProject");
		String startProject = request.getParameter("startProject");
		String planProject = request.getParameter("planProject");
		try {
			BuildingAnalysis entity = 
					buildingAnalysisService.selectSingleByProperty("serNo", serNo);	
			
			User user = HttpUtil.getCurrentUser(request);
			Timestamp nowTime = DateUtil.getNowTimestamp();
			if (entity == null){
				entity = new BuildingAnalysis();
				entity.setSerNo(serNo);
				entity.setCreater(user.getUserId());
				entity.setCreateTime(nowTime);
			}
			if(multiBusiness != null) entity.setMultiBusiness(multiBusiness);
			if(investProject != null) entity.setInvestProject(investProject);
			if(finishProject != null) entity.setFinishProject(finishProject);
			if(startProject != null) entity.setStartProject(startProject);
			if(planProject != null) entity.setPlanProject(planProject);

			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(nowTime);
			buildingAnalysisService.saveOrUpdate(entity);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

}
