package com.beawan.survey.building.controller;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.base.entity.User;
import com.beawan.base.service.ISysDicSV;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.survey.building.dto.BorrowerCreditDto;
import com.beawan.survey.building.dto.RelatedPartyCreditDto;
import com.beawan.survey.building.entity.MajorItem;
import com.beawan.survey.building.service.CreditInfoService;
import com.beawan.survey.building.service.MajorItemService;
import com.beawan.survey.custInfo.bean.CompFinancing;
import com.beawan.survey.custInfo.bean.CompFinancingBank;
import com.beawan.survey.custInfo.service.ICompFinancingSV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.fasterxml.jackson.core.type.TypeReference;
import com.platform.util.DateUtil;
import com.platform.util.HttpUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.MapperUtil;

/**
 * 融资负债情况 控制器(建筑业)
 * @author zxh
 * @date 2020/8/4 10:10
 */
@Controller
@UserSessionAnnotation
@RequestMapping({"/survey/building/"})
public class FinancingDebtController  extends BaseController {

    private static final Logger log = Logger.getLogger(FinancingDebtController.class);

    @Resource
    private ISysDicSV sysDicSV;
    @Resource
    protected ITaskSV taskSV;
    @Resource
    private CreditInfoService creditInfoService;
    @Resource
    private MajorItemService majorItemService;
    @Resource
	private ICompFinancingSV compFinancingSV;

    /**
     * 跳转到 企业授信情况
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("compCredit.do")
    public ModelAndView compCredit(String serNo) {
        ModelAndView mv = new ModelAndView("views/survey/building/compCredit");
        mv.addObject("serNo", serNo);
        return mv;
    }

    /**
     * 跳转到 企业授信相应界面
     * @param serNo
     * @param type
     * @return
     */
    @RequestMapping("toCreditInfo.do")
    public ModelAndView toCreditInfo(String serNo, String type){
        ModelAndView mav = new ModelAndView();
        try {
        	mav.addObject("serNo", serNo);
            //加载字典数据
            String[] optTypes = {SysConstants.BsDicConstant.STD_SY_GUAR_TYPE};
            mav.addObject("dicData", sysDicSV.queryMapListByTypes(optTypes));
            
            Task task = taskSV.getTaskBySerNo(serNo);
            if (task != null){
                CompFinancing financing = 
                		compFinancingSV.findCompFinancingByTIdAndNo(task.getId(), task.getCustomerNo());
                mav.addObject("customerNo", task.getCustomerNo());
                mav.addObject("taskId", task.getId());
                mav.addObject("financing", financing);
            }
            
            if (Constants.LB_PAGE_TYPE.BORROWER_CREDIT.equals(type)){
                mav.setViewName("views/survey/building/borrowerCredit");
            }else if (Constants.LB_PAGE_TYPE.LINKED_PARTY_CREDIT.equals(type)){
                mav.setViewName("views/survey/building/linkedPartyCredit");
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }

    /**
     * 跳转到 企业重大事项
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("majorItem.do")
    public ModelAndView majorItem(String serNo) {
        ModelAndView mv = new ModelAndView("views/survey/building/majorItem");
        MajorItem majorItem = majorItemService.selectSingleByProperty("serNo", serNo);
        mv.addObject("serNo", serNo);
        mv.addObject("majorItem", majorItem);
        return mv;
    }


    /**
     * 获取 授信情况列表（借款人、关联方）
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("getCreditInfoList.json")
    @ResponseBody
    public ResultDto getCreditInfoList(String serNo, String type) {
        ResultDto re = returnFail("获取数据失败");
        try {
            List<CompFinancingBank> list = creditInfoService.selectBySerNo(serNo, type);
            if (Constants.CM_CREDIT.BORROWER.equals(type)){
                re.setRows(MapperUtil.trans(list, BorrowerCreditDto.class));
            }else if (Constants.CM_CREDIT.RELATED_PARTY.equals(type)) {
                re.setRows(MapperUtil.trans(list, RelatedPartyCreditDto.class));
            }
            re.setMsg("获取数据成功");
            re.setCode(Constants.NORMAL);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 保存/修改 授信情况（借款人、关联方）
     * @param request
     * @return
     */
    @RequestMapping("saveCreditInfo.json")
    @ResponseBody
    public ResultDto saveCreditInfo(HttpServletRequest request, String jsonData) {
        ResultDto re = returnFail("保存失败!");
        try {
        	List<CompFinancingBank> entityList = 
        			JacksonUtil.fromJson(jsonData, new TypeReference<List<CompFinancingBank>>(){});
            if(CollectionUtils.isEmpty(entityList))
            	return returnFail("请先填写相关信息！");

            User user = HttpUtil.getCurrentUser(request);
            Timestamp nowTimestamp = DateUtil.getNowTimestamp();
            for(CompFinancingBank entity : entityList){
            	if (entity.getId() == null){
            		entity.setCreater(user.getUserId());
            		entity.setCreateTime(nowTimestamp);
                }
            	entity.setStatus(Constants.NORMAL);
            	entity.setUpdater(user.getUserId());
            	entity.setUpdateTime(nowTimestamp);
                creditInfoService.saveOrUpdate(entity);
            }

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 删除 授信情况（借款人、关联方）
     * @param request
     * @param id
     * @return
     */
    @RequestMapping("deleteCreditInfo.json")
    @ResponseBody
    public ResultDto removeCreditInfo(HttpServletRequest request, Long id){
        ResultDto re = returnFail("删除数据异常！");
        try{
            User user = HttpUtil.getCurrentUser(request);
            CompFinancingBank creditInfo = creditInfoService.selectByPrimaryKey(id);
            creditInfo.setStatus(Constants.DELETE);
            creditInfo.setUpdater(user.getUserId());
            creditInfo.setUpdateTime(DateUtil.getNowTimestamp());
            creditInfoService.saveOrUpdate(creditInfo);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("删除数据成功！");
        }catch (Exception e){
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 保存/修改 企业重大事项
     * @param request
     * @return
     */
    @RequestMapping("saveMajorItem.do")
    @ResponseBody
    public ResultDto saveMajorItem(HttpServletRequest request){
        ResultDto re = returnFail("保存失败");
        String dataJson = request.getParameter("dataJson");
        try {
            MajorItem majorItem = JacksonUtil.fromJson(dataJson, MajorItem.class);
            if (majorItem == null) {
                re.setMsg("传输数据内容为空，请重试");
                return re;
            }

            User user = HttpUtil.getCurrentUser(request);
            if(majorItem.getId() == null){
                majorItem.setCreater(user.getUserId());
                majorItem.setCreateTime(DateUtil.getNowTimestamp());
            }
            majorItem.setUpdater(user.getUserId());
            majorItem.setUpdateTime(DateUtil.getNowTimestamp());
            majorItem.setStatus(Constants.NORMAL);
            majorItemService.saveOrUpdate(majorItem);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }
    
    /**
	 * 保存 借款人、关联方授信情况
	 * @param taskId 任务主键
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping(value = "saveFinancingAnalysis.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto saveFinancingAnalysis(HttpServletRequest request, long taskId, String customerNo){
		ResultDto re = returnFail("保存失败");
		String borrowerCredit = request.getParameter("borrowerCredit");
		String linkedPartyCredit = request.getParameter("linkedPartyCredit");
		try {
			CompFinancing entity = compFinancingSV.findCompFinancingByTIdAndNo(taskId, customerNo);
			User user = HttpUtil.getCurrentUser(request);
			if (entity == null){
				entity = new CompFinancing();
				entity.setTaskId(taskId);
				entity.setCustomerNo(customerNo);
				entity.setCreater(user.getUserId());
				entity.setCreateTime(DateUtil.getNowTimestamp());
			}
			if(borrowerCredit != null) entity.setBorrowerCredit(borrowerCredit);
			if(linkedPartyCredit != null) entity.setLinkedPartyCredit(linkedPartyCredit);
			
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			compFinancingSV.saveCompFinancing(entity);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
}
