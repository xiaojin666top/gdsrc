package com.beawan.survey.loanInfo.bean;

// default package

/**
 * FixedProjPlan entity. @author MyEclipse Persistence Tools
 */

public class FixedProjPlan implements java.io.Serializable {

	// Fields

	private Long id;
	private Long taskId;
	private String time;//时间计划
	private String conent;//建设内容

	// Constructors

	/** default constructor */
	public FixedProjPlan() {
	}

	/** minimal constructor */
	public FixedProjPlan(Long id) {
		this.id = id;
	}

	/** full constructor */
	public FixedProjPlan(Long id, Long taskId, String time, String conent) {
		this.id = id;
		this.taskId = taskId;
		this.time = time;
		this.conent = conent;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getTime() {
		return this.time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getConent() {
		return this.conent;
	}

	public void setConent(String conent) {
		this.conent = conent;
	}

}