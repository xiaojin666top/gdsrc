package com.beawan.survey.loanInfo.bean;

// default package

/**
 * CounterGuaraCom entity. @author MyEclipse Persistence Tools
 */

public class CounterGuaraCom implements java.io.Serializable {

	// Fields

	private Long id;
	private String comName; // 企业名称
	private String relationship; // 关系
	private Double registeCapital; // 注册资本（万元）
	private Double totalAssets; // 总资产（万元）
	private Double netAssets; // 净资产（万元）
	private Double totalDebts; // 总负债（万元）
	private Double extGuaraAmt; // 对外担保（万元）
	private String isMBankCus; // 是否为本行客户
	private String guaraAbility; // 担保额度测算
	private String remarks; // 其他说明
	private Long guaraId;

	// Constructors

	/** default constructor */
	public CounterGuaraCom() {
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getComName() {
		return this.comName;
	}

	public void setComName(String comName) {
		this.comName = comName;
	}

	public String getRelationship() {
		return this.relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public Double getRegisteCapital() {
		return this.registeCapital;
	}

	public void setRegisteCapital(Double registeCapital) {
		this.registeCapital = registeCapital;
	}

	public Double getTotalAssets() {
		return this.totalAssets;
	}

	public void setTotalAssets(Double totalAssets) {
		this.totalAssets = totalAssets;
	}

	public Double getNetAssets() {
		return this.netAssets;
	}

	public void setNetAssets(Double netAssets) {
		this.netAssets = netAssets;
	}

	public Double getTotalDebts() {
		return this.totalDebts;
	}

	public void setTotalDebts(Double totalDebts) {
		this.totalDebts = totalDebts;
	}

	public Double getExtGuaraAmt() {
		return this.extGuaraAmt;
	}

	public void setExtGuaraAmt(Double extGuaraAmt) {
		this.extGuaraAmt = extGuaraAmt;
	}

	public String getGuaraAbility() {
		return this.guaraAbility;
	}

	public void setGuaraAbility(String guaraAbility) {
		this.guaraAbility = guaraAbility;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getGuaraId() {
		return guaraId;
	}

	public void setGuaraId(Long guaraId) {
		this.guaraId = guaraId;
	}

	public String getIsMBankCus() {
		return isMBankCus;
	}

	public void setIsMBankCus(String isMBankCus) {
		this.isMBankCus = isMBankCus;
	}

}