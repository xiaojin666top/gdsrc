package com.beawan.survey.loanInfo.bean;

// default package

/**
 * FixedProjCashFow entity. @author MyEclipse Persistence Tools
 */

public class FixedProjCashFow implements java.io.Serializable {

	// Fields

	private Long id;
	private Long taskId;
	private String itemCode;//项目编号
	private String itemName;//项目名称
	private String stage;//阶段（建设期、经营期）
	private String year;//年份
	private Double itemValue;//项目金额

	// Constructors

	/** default constructor */
	public FixedProjCashFow() {
	}

	/** minimal constructor */
	public FixedProjCashFow(Long id) {
		this.id = id;
	}

	/** full constructor */
	public FixedProjCashFow(Long id, Long taskId, String itemCode,
			String itemName, String stage, String year, Double itemValue) {
		this.id = id;
		this.taskId = taskId;
		this.itemCode = itemCode;
		this.itemName = itemName;
		this.stage = stage;
		this.year = year;
		this.itemValue = itemValue;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getItemCode() {
		return this.itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return this.itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getStage() {
		return this.stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public String getYear() {
		return this.year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public Double getItemValue() {
		return this.itemValue;
	}

	public void setItemValue(Double itemValue) {
		this.itemValue = itemValue;
	}

}