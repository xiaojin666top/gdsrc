package com.beawan.survey.loanInfo.bean;
// default package



/**
 * Guarantor entity. @author MyEclipse Persistence Tools
 */

public class Guarantor  implements java.io.Serializable {


    // Fields    

     private Long id; //保证人ID
     private String cusNo; //保证人客户号
     private String cusName; //保证人名称
     private String guaraType; //保证人类型 STD_SY_GUARANTOR_TYPE
     private Double guaraAmt; //担保金额
     private String guaraForm; //保证形式 STD_GUAR_FORM
     private String relation; //与借款人关系
     private String guaraRateType; //保证人种类STD_RATE_GUR
     private String remarks; //备注
     private Long taskId; //任务ID


    // Constructors

    /** default constructor */
    public Guarantor() {
    }

	/** minimal constructor */
    public Guarantor(Long id) {
        this.id = id;
    }
    
    /** full constructor */
    public Guarantor(Long id, String cusNo, String cusName, String guaraType, Double guaraAmt, String guaraForm, String relation, String remarks, Long taskId) {
        this.id = id;
        this.cusNo = cusNo;
        this.cusName = cusName;
        this.guaraType = guaraType;
        this.guaraAmt = guaraAmt;
        this.guaraForm = guaraForm;
        this.relation = relation;
        this.remarks = remarks;
        this.taskId = taskId;
    }

   
    // Property accessors

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public String getCusNo() {
        return this.cusNo;
    }
    
    public void setCusNo(String cusNo) {
        this.cusNo = cusNo;
    }

    public String getCusName() {
        return this.cusName;
    }
    
    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getGuaraType() {
        return this.guaraType;
    }
    
    public void setGuaraType(String guaraType) {
        this.guaraType = guaraType;
    }

    public Double getGuaraAmt() {
        return this.guaraAmt;
    }
    
    public void setGuaraAmt(Double guaraAmt) {
        this.guaraAmt = guaraAmt;
    }

    public String getGuaraForm() {
        return this.guaraForm;
    }
    
    public void setGuaraForm(String guaraForm) {
        this.guaraForm = guaraForm;
    }

    public String getRelation() {
        return this.relation;
    }
    
    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getRemarks() {
        return this.remarks;
    }
    
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Long getTaskId() {
        return this.taskId;
    }
    
    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

	public String getGuaraRateType() {
		return guaraRateType;
	}

	public void setGuaraRateType(String guaraRateType) {
		this.guaraRateType = guaraRateType;
	}
   

    






}