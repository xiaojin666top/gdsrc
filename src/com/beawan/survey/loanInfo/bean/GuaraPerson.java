package com.beawan.survey.loanInfo.bean;

import com.beawan.survey.custInfo.bean.PersonBase;

// default package

/**
 * GuaraPerson entity. @author MyEclipse Persistence Tools
 */

public class GuaraPerson implements java.io.Serializable {

	// Fields

	private Long guaraId;
	private String overallAnalysis;//综合分析评价

	private Guarantor guarantor;
	private PersonBase personBase;

	// Constructors

	/** default constructor */
	public GuaraPerson() {
	}

	// Property accessors

	public Long getGuaraId() {
		return this.guaraId;
	}

	public void setGuaraId(Long guaraId) {
		this.guaraId = guaraId;
	}

	public String getOverallAnalysis() {
		return this.overallAnalysis;
	}

	public void setOverallAnalysis(String overallAnalysis) {
		this.overallAnalysis = overallAnalysis;
	}

	public Guarantor getGuarantor() {
		return guarantor;
	}

	public void setGuarantor(Guarantor guarantor) {
		this.guarantor = guarantor;
	}

	public PersonBase getPersonBase() {
		return personBase;
	}

	public void setPersonBase(PersonBase personBase) {
		this.personBase = personBase;
	}

}