package com.beawan.survey.loanInfo.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 效益预测 分析
 * 包括还款来源分析、还款计划分析
 */

@Entity
@Table(name = "BIZ_BENEFIT_ANALY",schema = "GDTCESYS")
public class BenefitAnaly extends BaseEntity implements java.io.Serializable {
	
	@Id
  //  @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="BIZ_BENEFIT_ANALY")
 //   @SequenceGenerator(name="BIZ_BENEFIT_ANALY",allocationSize=1,initialValue=1, sequenceName="BIZ_BENEFIT_ANALY_SEQ")
	private Integer id; 
	
	@Column(name = "TASK_ID")
	private Long taskId;

	@Column(name = "BENEFIT_ANALY")
	private String benefitAnaly; //效益预测分析

	@Column(name = "FIRST_PAY_SOURCE")
	private String firstPaySource; //第一还款来源
	
	@Column(name = "SECOND_PAY_SOURCE")
	private String secondPaySource; // 第二还款来源分析
	
	@Column(name = "PAY_PLAN")
	private String payPlan; // 还款计划
	

	// Constructors

	/** default constructor */
	public BenefitAnaly() {
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Long getTaskId() {
		return taskId;
	}


	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}


	public String getBenefitAnaly() {
		return benefitAnaly;
	}


	public void setBenefitAnaly(String benefitAnaly) {
		this.benefitAnaly = benefitAnaly;
	}


	public String getFirstPaySource() {
		return firstPaySource;
	}


	public void setFirstPaySource(String firstPaySource) {
		this.firstPaySource = firstPaySource;
	}


	public String getSecondPaySource() {
		return secondPaySource;
	}


	public void setSecondPaySource(String secondPaySource) {
		this.secondPaySource = secondPaySource;
	}


	public String getPayPlan() {
		return payPlan;
	}


	public void setPayPlan(String payPlan) {
		this.payPlan = payPlan;
	}



	
	

}