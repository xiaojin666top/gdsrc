package com.beawan.survey.loanInfo.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 本次授信申请情况   授信申请方案及贷款用途
 */

@Entity
@Table(name = "BIZ_APPLY",schema = "GDTCESYS")
public class ApplyInfo extends BaseEntity implements java.io.Serializable {

	@Id
	@Column(name = "TASK_ID")
	private Long taskId;
	@Column(name = "APPLY_INFO")
	private String applyInfo; // 授信申请方案分析
	@Column(name = "APPLY_REASON")
	private String applyReason; // 授信理由分析
	@Column(name = "AMT_ANALYSIS")
	private String amtAnalysis; // 额度测算与分析
	@Column(name = "REPAYMENT_ABILITY")
	private String repaymentAbility; // 还款能力分析
	@Column(name = "RATE_ANALYSIS")
	private String rateAnalysis; // 利费率测算分析
	@Column(name = "INTEREST_INCOME")
	private String interestIncome; // 利息收益测算分析
	@Column(name = "UN_INTEREST_INCOME")
	private String unInterestIncome; // 非利息收益测算分析

	@Column(name = "INCOME_GROWTH_RATE")
	private Double incomeGrowthRate = 0.0; // 本年度销售收入预计增长率
	@Column(name = "OTHER_CHANNEL_CAPITAL")
	private Double otherChannelCapital = 0.0; // 借款人从其他渠道获得的资金（万元）
	@Column(name = "MY_BANK_LIQUID_LOAN_BAL")
	private Double myBankLiquidLoanBal = 0.0; //我行流动资金贷款余额（万元）
	
	@Column(name = "ADJUST_QUOTA")
	private Double adjustQuota;//缺口调整值
	@Column(name = "ADJUST_QUOTA_REASON")
	private String adjustQuotaReason;//调整流动资金缺口测算结果理由
	

	// Constructors

	/** default constructor */
	public ApplyInfo() {
	}

	// Property accessors

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getApplyInfo() {
		return this.applyInfo;
	}

	public void setApplyInfo(String applyInfo) {
		this.applyInfo = applyInfo;
	}

	public String getApplyReason() {
		return this.applyReason;
	}

	public void setApplyReason(String applyReason) {
		this.applyReason = applyReason;
	}

	public String getAmtAnalysis() {
		return this.amtAnalysis;
	}

	public void setAmtAnalysis(String amtAnalysis) {
		this.amtAnalysis = amtAnalysis;
	}

	public String getRepaymentAbility() {
		return this.repaymentAbility;
	}

	public void setRepaymentAbility(String repaymentAbility) {
		this.repaymentAbility = repaymentAbility;
	}

	public String getRateAnalysis() {
		return this.rateAnalysis;
	}

	public void setRateAnalysis(String rateAnalysis) {
		this.rateAnalysis = rateAnalysis;
	}

	public String getInterestIncome() {
		return this.interestIncome;
	}

	public void setInterestIncome(String interestIncome) {
		this.interestIncome = interestIncome;
	}

	public String getUnInterestIncome() {
		return this.unInterestIncome;
	}

	public void setUnInterestIncome(String unInterestIncome) {
		this.unInterestIncome = unInterestIncome;
	}

	public Double getIncomeGrowthRate() {
		return incomeGrowthRate;
	}

	public void setIncomeGrowthRate(Double incomeGrowthRate) {
		this.incomeGrowthRate = incomeGrowthRate;
	}

	public Double getOtherChannelCapital() {
		return otherChannelCapital;
	}

	public void setOtherChannelCapital(Double otherChannelCapital) {
		this.otherChannelCapital = otherChannelCapital;
	}

	public Double getMyBankLiquidLoanBal() {
		return myBankLiquidLoanBal;
	}

	public void setMyBankLiquidLoanBal(Double myBankLiquidLoanBal) {
		this.myBankLiquidLoanBal = myBankLiquidLoanBal;
	}

	public Double getAdjustQuota() {
		return adjustQuota;
	}

	public void setAdjustQuota(Double adjustQuota) {
		this.adjustQuota = adjustQuota;
	}

	public String getAdjustQuotaReason() {
		return adjustQuotaReason;
	}

	public void setAdjustQuotaReason(String adjustQuotaReason) {
		this.adjustQuotaReason = adjustQuotaReason;
	}
	
	

}