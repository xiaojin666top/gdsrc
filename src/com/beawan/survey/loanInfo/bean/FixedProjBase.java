package com.beawan.survey.loanInfo.bean;

// default package

/**
 * FixedProjBase entity. @author MyEclipse Persistence Tools
 */

public class FixedProjBase implements java.io.Serializable {

	// Fields

	private Long taskId;
	private String projName;
	private String fixedProjType; //固定资产贷款类型
	private String projAddr; //建设地点
	private Double consCycle; //建设周期
	private Double invesScale; //投资规模
	private Double expectedIncome; //预计收益
	private String projBackground; //项目建设背景及必要性
	private String conentAndScale; //项目建设内容及规模
	private String placeAndTraffic; //项目位置及交通运输情况
	private String materialAndPower; //项目原材料、燃料、动力情况
	private String processAndTech; //工艺技术评价
	private String environment; //环境保护评价
	private String approvalCompliance; //项目审批及合规性
	private String investCapitalUsedDate; //投资资金使用情况统计截止日期
	private String investCapitalUsed; //投资资金使用情况分析说明
	private String investEstimation; //项目投资估算分析说明
	private String financingPlan; //项目筹资计划分析说明
	private String marketSad; //市场供求分析
	private String marketPosition; //产品市场定位
	private String materialAndPrice; //原材料及产品价格分析
	private String marketCompete; //市场竞争力分析
	private String cashFlowAnalysis; //现金流测算分析
	private String solvencyAnalysis; //项目偿债能力综合评价
	private String consEndYear; //建设期截止年份

	// Constructors

	/** default constructor */
	public FixedProjBase() {
	}

	// Property accessors

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getProjName() {
		return this.projName;
	}

	public void setProjName(String projName) {
		this.projName = projName;
	}

	public String getFixedProjType() {
		return this.fixedProjType;
	}

	public void setFixedProjType(String fixedProjType) {
		this.fixedProjType = fixedProjType;
	}

	public String getProjAddr() {
		return this.projAddr;
	}

	public void setProjAddr(String projAddr) {
		this.projAddr = projAddr;
	}

	public Double getConsCycle() {
		return this.consCycle;
	}

	public void setConsCycle(Double consCycle) {
		this.consCycle = consCycle;
	}

	public Double getInvesScale() {
		return this.invesScale;
	}

	public void setInvesScale(Double invesScale) {
		this.invesScale = invesScale;
	}

	public Double getExpectedIncome() {
		return this.expectedIncome;
	}

	public void setExpectedIncome(Double expectedIncome) {
		this.expectedIncome = expectedIncome;
	}

	public String getProjBackground() {
		return this.projBackground;
	}

	public void setProjBackground(String projBackground) {
		this.projBackground = projBackground;
	}

	public String getConentAndScale() {
		return this.conentAndScale;
	}

	public void setConentAndScale(String conentAndScale) {
		this.conentAndScale = conentAndScale;
	}

	public String getPlaceAndTraffic() {
		return this.placeAndTraffic;
	}

	public void setPlaceAndTraffic(String placeAndTraffic) {
		this.placeAndTraffic = placeAndTraffic;
	}

	public String getMaterialAndPower() {
		return this.materialAndPower;
	}

	public void setMaterialAndPower(String materialAndPower) {
		this.materialAndPower = materialAndPower;
	}

	public String getProcessAndTech() {
		return this.processAndTech;
	}

	public void setProcessAndTech(String processAndTech) {
		this.processAndTech = processAndTech;
	}

	public String getEnvironment() {
		return this.environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public String getApprovalCompliance() {
		return this.approvalCompliance;
	}

	public void setApprovalCompliance(String approvalCompliance) {
		this.approvalCompliance = approvalCompliance;
	}

	public String getInvestCapitalUsedDate() {
		return this.investCapitalUsedDate;
	}

	public void setInvestCapitalUsedDate(String investCapitalUsedDate) {
		this.investCapitalUsedDate = investCapitalUsedDate;
	}

	public String getInvestCapitalUsed() {
		return this.investCapitalUsed;
	}

	public void setInvestCapitalUsed(String investCapitalUsed) {
		this.investCapitalUsed = investCapitalUsed;
	}

	public String getInvestEstimation() {
		return this.investEstimation;
	}

	public void setInvestEstimation(String investEstimation) {
		this.investEstimation = investEstimation;
	}

	public String getFinancingPlan() {
		return this.financingPlan;
	}

	public void setFinancingPlan(String financingPlan) {
		this.financingPlan = financingPlan;
	}

	public String getMarketSad() {
		return this.marketSad;
	}

	public void setMarketSad(String marketSad) {
		this.marketSad = marketSad;
	}

	public String getMarketPosition() {
		return this.marketPosition;
	}

	public void setMarketPosition(String marketPosition) {
		this.marketPosition = marketPosition;
	}

	public String getMaterialAndPrice() {
		return this.materialAndPrice;
	}

	public void setMaterialAndPrice(String materialAndPrice) {
		this.materialAndPrice = materialAndPrice;
	}

	public String getMarketCompete() {
		return this.marketCompete;
	}

	public void setMarketCompete(String marketCompete) {
		this.marketCompete = marketCompete;
	}

	public String getCashFlowAnalysis() {
		return this.cashFlowAnalysis;
	}

	public void setCashFlowAnalysis(String cashFlowAnalysis) {
		this.cashFlowAnalysis = cashFlowAnalysis;
	}

	public String getSolvencyAnalysis() {
		return this.solvencyAnalysis;
	}

	public void setSolvencyAnalysis(String solvencyAnalysis) {
		this.solvencyAnalysis = solvencyAnalysis;
	}

	public String getConsEndYear() {
		return consEndYear;
	}

	public void setConsEndYear(String consEndYear) {
		this.consEndYear = consEndYear;
	}

}