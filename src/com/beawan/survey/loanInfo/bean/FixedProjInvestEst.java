package com.beawan.survey.loanInfo.bean;

// default package

/**
 * FixedProjInvestEst entity. @author MyEclipse Persistence Tools
 */

public class FixedProjInvestEst implements java.io.Serializable {

	// Fields

	private Long id;
	private Long taskId;
	private String itemCode; // 项目编号
	private String itemName; // 项目名称
	private Double measureAmt; // 调查测算金额
	private Double feasibleAmt; // 可研金额
	private String difference; // 对比差异
	private String differReason; // 对比差异原因

	// Constructors

	/** default constructor */
	public FixedProjInvestEst() {
	}

	/** minimal constructor */
	public FixedProjInvestEst(Long id) {
		this.id = id;
	}

	/** full constructor */
	public FixedProjInvestEst(Long id, Long taskId, String itemCode,
			String itemName, Double measureAmt, Double feasibleAmt,
			String difference, String differReason) {
		this.id = id;
		this.taskId = taskId;
		this.itemCode = itemCode;
		this.itemName = itemName;
		this.measureAmt = measureAmt;
		this.feasibleAmt = feasibleAmt;
		this.difference = difference;
		this.differReason = differReason;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getItemCode() {
		return this.itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return this.itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Double getMeasureAmt() {
		return this.measureAmt;
	}

	public void setMeasureAmt(Double measureAmt) {
		this.measureAmt = measureAmt;
	}

	public Double getFeasibleAmt() {
		return this.feasibleAmt;
	}

	public void setFeasibleAmt(Double feasibleAmt) {
		this.feasibleAmt = feasibleAmt;
	}

	public String getDifference() {
		return this.difference;
	}

	public void setDifference(String difference) {
		this.difference = difference;
	}

	public String getDifferReason() {
		return this.differReason;
	}

	public void setDifferReason(String differReason) {
		this.differReason = differReason;
	}

}