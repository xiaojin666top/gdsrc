package com.beawan.survey.loanInfo.bean;

import java.util.List;

import com.beawan.survey.custInfo.bean.CompBase;

// default package

/**
 * GuaraProCom entity. @author MyEclipse Persistence Tools
 */

public class GuaraProCom implements java.io.Serializable {

	// Fields

	private Long guaraId; // 保证人ID
	private Double guaraAmtSingle; // 单户担保金额
	private String counterGuaraCom; // 保证反担保分析说明
	private String counterGuaraMort; // 抵押反担保分析说明
	private String overallAnalysis; // 综合分析评价

	private Guarantor guarantor;
	private CompBase compBase;
	private List<CounterGuaraCom> counterGuaraComs;
	private List<CounterGuaraMort> counterGuaraMorts;
	// Constructors

	/** default constructor */
	public GuaraProCom() {
	}

	// Property accessors

	public Long getGuaraId() {
		return this.guaraId;
	}

	public void setGuaraId(Long guaraId) {
		this.guaraId = guaraId;
	}

	public Double getGuaraAmtSingle() {
		return this.guaraAmtSingle;
	}

	public void setGuaraAmtSingle(Double guaraAmtSingle) {
		this.guaraAmtSingle = guaraAmtSingle;
	}

	public String getCounterGuaraCom() {
		return counterGuaraCom;
	}

	public void setCounterGuaraCom(String counterGuaraCom) {
		this.counterGuaraCom = counterGuaraCom;
	}

	public String getCounterGuaraMort() {
		return counterGuaraMort;
	}

	public void setCounterGuaraMort(String counterGuaraMort) {
		this.counterGuaraMort = counterGuaraMort;
	}

	public String getOverallAnalysis() {
		return overallAnalysis;
	}

	public void setOverallAnalysis(String overallAnalysis) {
		this.overallAnalysis = overallAnalysis;
	}

	public Guarantor getGuarantor() {
		return guarantor;
	}

	public void setGuarantor(Guarantor guarantor) {
		this.guarantor = guarantor;
	}

	public CompBase getCompBase() {
		return compBase;
	}

	public void setCompBase(CompBase compBase) {
		this.compBase = compBase;
	}

	public List<CounterGuaraCom> getCounterGuaraComs() {
		return counterGuaraComs;
	}

	public void setCounterGuaraComs(List<CounterGuaraCom> counterGuaraComs) {
		this.counterGuaraComs = counterGuaraComs;
	}

	public List<CounterGuaraMort> getCounterGuaraMorts() {
		return counterGuaraMorts;
	}

	public void setCounterGuaraMorts(List<CounterGuaraMort> counterGuaraMorts) {
		this.counterGuaraMorts = counterGuaraMorts;
	}

	
}