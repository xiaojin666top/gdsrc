package com.beawan.survey.loanInfo.bean;
// default package



/**
 * ApplyRate entity. @author MyEclipse Persistence Tools
 */

@Deprecated
public class ApplyRate  implements java.io.Serializable {


    // Fields    

     private Long id;
     private String businessType; //业务品种
     private Double currIntValue; //本期利率值
     private Double currFloatRatio; //本期利 率浮比
     private Double currRate; //本期费率
     private Double lastIntValue;//上期利率值
     private Double lastFloatRatio; //上期利 率浮比
     private Double lastRate; //上期费率
     private Long taskId;


    // Constructors

    /** default constructor */
    public ApplyRate() {
    }

	/** minimal constructor */
    public ApplyRate(Long id) {
        this.id = id;
    }
    
    /** full constructor */
    public ApplyRate(Long id, String businessType, Double currIntValue, Double currFloatRatio, Double currRate, Double lastIntValue, Double lastFloatRatio, Double lastRate, Long taskId) {
        this.id = id;
        this.businessType = businessType;
        this.currIntValue = currIntValue;
        this.currFloatRatio = currFloatRatio;
        this.currRate = currRate;
        this.lastIntValue = lastIntValue;
        this.lastFloatRatio = lastFloatRatio;
        this.lastRate = lastRate;
        this.taskId = taskId;
    }

   
    // Property accessors

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public String getBusinessType() {
        return this.businessType;
    }
    
    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public Double getCurrIntValue() {
        return this.currIntValue;
    }
    
    public void setCurrIntValue(Double currIntValue) {
        this.currIntValue = currIntValue;
    }

    public Double getCurrFloatRatio() {
        return this.currFloatRatio;
    }
    
    public void setCurrFloatRatio(Double currFloatRatio) {
        this.currFloatRatio = currFloatRatio;
    }

    public Double getCurrRate() {
        return this.currRate;
    }
    
    public void setCurrRate(Double currRate) {
        this.currRate = currRate;
    }

    public Double getLastIntValue() {
        return this.lastIntValue;
    }
    
    public void setLastIntValue(Double lastIntValue) {
        this.lastIntValue = lastIntValue;
    }

    public Double getLastFloatRatio() {
        return this.lastFloatRatio;
    }
    
    public void setLastFloatRatio(Double lastFloatRatio) {
        this.lastFloatRatio = lastFloatRatio;
    }

    public Double getLastRate() {
        return this.lastRate;
    }
    
    public void setLastRate(Double lastRate) {
        this.lastRate = lastRate;
    }

    public Long getTaskId() {
        return this.taskId;
    }
    
    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }
   








}