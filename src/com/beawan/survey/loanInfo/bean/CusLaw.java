package com.beawan.survey.loanInfo.bean;

import java.io.Serializable;

public class CusLaw implements Serializable{
	
	private  String   id;
	private  String customerNo;
	private  String type;//类型
	private  String title;//标题
	private  String date;//提交时间
	private  String content;//文书内容
	private  String caseNo;//案号
	private  String caseCause;//案由
	
	public  CusLaw() {		
	}
	
	public CusLaw(String customerNo, String type, String title, String date, String content, String caseNo,
			String caseCause) {
		this.customerNo = customerNo;
		this.type = type;
		this.title = title;
		this.date = date;
		this.content = content;
		this.caseNo = caseNo;
		this.caseCause = caseCause;
	}
	
	public CusLaw(String customerNo) {
		this.customerNo = customerNo;
	}
		
	

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

	public String getCaseNo() {
		return caseNo;
	}

	public void setCaseNo(String caseNo) {
		this.caseNo = caseNo;
	}

	public String getCaseCause() {
		return caseCause;
	}

	public void setCaseCause(String caseCause) {
		this.caseCause = caseCause;
	}
	
	
	
	
}
