package com.beawan.survey.loanInfo.bean;

// default package

/**
 * CounterGuaraMort entity. @author MyEclipse Persistence Tools
 */

public class CounterGuaraMort implements java.io.Serializable {

	// Fields

	private Long id;
	private String mortName; //抵质押物名称
	private String address; //位置
	private String ownership; //权属
	private String mortType; //性质
	private Double area; //面积（㎡）
	private Double evaluationValue; //评估价（万元）
	private Double mortAmt; //已设定抵押金额（万元）
	private String remarks; //其他说明
	private Long guaraId;

	// Constructors

	public Long getGuaraId() {
		return guaraId;
	}

	public void setGuaraId(Long guaraId) {
		this.guaraId = guaraId;
	}

	/** default constructor */
	public CounterGuaraMort() {
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMortName() {
		return this.mortName;
	}

	public void setMortName(String mortName) {
		this.mortName = mortName;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getOwnership() {
		return this.ownership;
	}

	public void setOwnership(String ownership) {
		this.ownership = ownership;
	}

	public String getMortType() {
		return this.mortType;
	}

	public void setMortType(String mortType) {
		this.mortType = mortType;
	}

	public Double getArea() {
		return this.area;
	}

	public void setArea(Double area) {
		this.area = area;
	}

	public Double getEvaluationValue() {
		return this.evaluationValue;
	}

	public void setEvaluationValue(Double evaluationValue) {
		this.evaluationValue = evaluationValue;
	}

	public Double getMortAmt() {
		return this.mortAmt;
	}

	public void setMortAmt(Double mortAmt) {
		this.mortAmt = mortAmt;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	

}