package com.beawan.survey.loanInfo.bean;

// default package

/**
 * MortgageInfo entity. @author MyEclipse Persistence Tools
 */

public class MortgageInfo implements java.io.Serializable {

	// Fields

	private Long id;
	private String serial; // 押品编号
	private String name; // 抵押物名称
	private String address; // 地址
	private String owner; // 所有权人
	private String warrantCode;
	private String unit; // 单位
	private Double number; // 数量
	private Double evaluationValue; // 评估（预估）价格
	private Double confirmValue; // 资产确认价值
	private Double mortAmt; // 抵押作价
	private String remarks; // 备注
	private String mortType; // 抵押物种类STD_RATE_MOR
	private Long taskId;

	// Constructors

	/** default constructor */
	public MortgageInfo() {
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getOwner() {
		return this.owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getWarrantCode() {
		return this.warrantCode;
	}

	public void setWarrantCode(String warrantCode) {
		this.warrantCode = warrantCode;
	}

	public String getUnit() {
		return this.unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Double getNumber() {
		return this.number;
	}

	public void setNumber(Double number) {
		this.number = number;
	}

	public Double getEvaluationValue() {
		return this.evaluationValue;
	}

	public void setEvaluationValue(Double evaluationValue) {
		this.evaluationValue = evaluationValue;
	}

	public Double getConfirmValue() {
		return this.confirmValue;
	}

	public void setConfirmValue(Double confirmValue) {
		this.confirmValue = confirmValue;
	}

	public Double getMortAmt() {
		return this.mortAmt;
	}

	public void setMortAmt(Double mortAmt) {
		this.mortAmt = mortAmt;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getMortType() {
		return mortType;
	}

	public void setMortType(String mortType) {
		this.mortType = mortType;
	}

	

}