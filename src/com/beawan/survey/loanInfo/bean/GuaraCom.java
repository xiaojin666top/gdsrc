package com.beawan.survey.loanInfo.bean;

import java.util.List;
import java.util.Map;

import com.beawan.survey.custInfo.bean.CompBase;
import com.beawan.survey.custInfo.bean.CompBaseDesc;
import com.beawan.survey.custInfo.bean.CompBaseEquity;
import com.beawan.survey.custInfo.dto.CompBaseDto;

// default package

/**
 * GuaraCom entity. @author MyEclipse Persistence Tools
 */

public class GuaraCom implements java.io.Serializable {

	// Fields

	private Long guaraId;
	private String finaRepYear; // 财报年月
	private String buninessInfo; //经营情况说明分析
	private String ratioAnalysis; //财务比率指标分析
	private String financeAnalysis; //财务情况总体说明分析
	private String financingInfo; //企业融资及对外担保情况说明分析
	private String guaraAbility; //担保能力分析说明
	private String overallAnalysis; //综合分析评价

	private Guarantor guarantor;
	private CompBaseDto compBase;
	private CompBaseDesc compBaseDesc;
	private List<CompBaseEquity> compBaseEquities;
	
	private List<String> finaDateList;
	private List<Map<String, Double>> financeData; //财务数据
	
	/** default constructor */
	public GuaraCom() {
	}

	public Long getGuaraId() {
		return guaraId;
	}

	public void setGuaraId(Long guaraId) {
		this.guaraId = guaraId;
	}

	public String getFinaRepYear() {
		return finaRepYear;
	}

	public void setFinaRepYear(String finaRepYear) {
		this.finaRepYear = finaRepYear;
	}

	public String getBuninessInfo() {
		return buninessInfo;
	}

	public void setBuninessInfo(String buninessInfo) {
		this.buninessInfo = buninessInfo;
	}

	public String getRatioAnalysis() {
		return ratioAnalysis;
	}

	public void setRatioAnalysis(String ratioAnalysis) {
		this.ratioAnalysis = ratioAnalysis;
	}

	public String getFinanceAnalysis() {
		return financeAnalysis;
	}

	public void setFinanceAnalysis(String financeAnalysis) {
		this.financeAnalysis = financeAnalysis;
	}

	public String getFinancingInfo() {
		return financingInfo;
	}

	public void setFinancingInfo(String financingInfo) {
		this.financingInfo = financingInfo;
	}

	public String getGuaraAbility() {
		return guaraAbility;
	}

	public void setGuaraAbility(String guaraAbility) {
		this.guaraAbility = guaraAbility;
	}

	public String getOverallAnalysis() {
		return overallAnalysis;
	}

	public void setOverallAnalysis(String overallAnalysis) {
		this.overallAnalysis = overallAnalysis;
	}

	public Guarantor getGuarantor() {
		return guarantor;
	}

	public void setGuarantor(Guarantor guarantor) {
		this.guarantor = guarantor;
	}

	public CompBaseDto getCompBase() {
		return compBase;
	}

	public void setCompBase(CompBaseDto compBase) {
		this.compBase = compBase;
	}

	public CompBaseDesc getCompBaseDesc() {
		return compBaseDesc;
	}

	public void setCompBaseDesc(CompBaseDesc compBaseDesc) {
		this.compBaseDesc = compBaseDesc;
	}

	public List<CompBaseEquity> getCompBaseEquities() {
		return compBaseEquities;
	}

	public void setCompBaseEquities(List<CompBaseEquity> compBaseEquities) {
		this.compBaseEquities = compBaseEquities;
	}

	public List<String> getFinaDateList() {
		return finaDateList;
	}

	public void setFinaDateList(List<String> finaDateList) {
		this.finaDateList = finaDateList;
	}

	public List<Map<String, Double>> getFinanceData() {
		return financeData;
	}

	public void setFinanceData(List<Map<String, Double>> financeData) {
		this.financeData = financeData;
	}

}