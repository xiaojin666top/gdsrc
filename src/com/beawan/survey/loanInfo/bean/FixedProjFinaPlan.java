package com.beawan.survey.loanInfo.bean;

// default package

/**
 * FixedProjFinaPlan entity. @author MyEclipse Persistence Tools
 */

public class FixedProjFinaPlan implements java.io.Serializable {

	// Fields

	private Long id;
	private Long taskId;
	private String itemCode; // 项目编号
	private String itemName; // 项目名称
	private String financingPlan; // 筹资计划说明
	private Double receiveAmt; // 已到位资金
	private String investForm; // 出资方式
	private Double unReceiveAmt; // 未到位资金
	private String unInvestForm; // 未到位资金计划出资方式
	private String planReceiveDate; // 未到位资金计划到位时间

	// Constructors

	/** default constructor */
	public FixedProjFinaPlan() {
	}

	/** minimal constructor */
	public FixedProjFinaPlan(Long id) {
		this.id = id;
	}

	/** full constructor */
	public FixedProjFinaPlan(Long id, Long taskId, String itemCode,
			String itemName, String financingPlan, Double receiveAmt,
			String investForm, Double unReceiveAmt, String unInvestForm,
			String planReceiveDate) {
		this.id = id;
		this.taskId = taskId;
		this.itemCode = itemCode;
		this.itemName = itemName;
		this.financingPlan = financingPlan;
		this.receiveAmt = receiveAmt;
		this.investForm = investForm;
		this.unReceiveAmt = unReceiveAmt;
		this.unInvestForm = unInvestForm;
		this.planReceiveDate = planReceiveDate;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getItemCode() {
		return this.itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return this.itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getFinancingPlan() {
		return this.financingPlan;
	}

	public void setFinancingPlan(String financingPlan) {
		this.financingPlan = financingPlan;
	}

	public Double getReceiveAmt() {
		return this.receiveAmt;
	}

	public void setReceiveAmt(Double receiveAmt) {
		this.receiveAmt = receiveAmt;
	}

	public String getInvestForm() {
		return this.investForm;
	}

	public void setInvestForm(String investForm) {
		this.investForm = investForm;
	}

	public Double getUnReceiveAmt() {
		return this.unReceiveAmt;
	}

	public void setUnReceiveAmt(Double unReceiveAmt) {
		this.unReceiveAmt = unReceiveAmt;
	}

	public String getUnInvestForm() {
		return this.unInvestForm;
	}

	public void setUnInvestForm(String unInvestForm) {
		this.unInvestForm = unInvestForm;
	}

	public String getPlanReceiveDate() {
		return this.planReceiveDate;
	}

	public void setPlanReceiveDate(String planReceiveDate) {
		this.planReceiveDate = planReceiveDate;
	}

}