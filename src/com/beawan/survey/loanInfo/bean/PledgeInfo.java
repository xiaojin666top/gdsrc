package com.beawan.survey.loanInfo.bean;

// default package

/**
 * PledgeInfo entity. @author MyEclipse Persistence Tools
 */

public class PledgeInfo implements java.io.Serializable {

	// Fields

	private Long id;
	private String serial; // 押品编号
	private String name; // 质押物名称
	private String owner; // 所有权人
	private String warrantCode;
	private String unit; // 单位
	private Double number; // 数量
	private Double confirmValue; // 资产确认价值
	private Double pledAmt; // 抵押作价
	private String remarks; // 备注
	private String pledType;//质押物种类 STD_RATE_POL
	private Long taskId;

	// Constructors

	/** default constructor */
	public PledgeInfo() {
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOwner() {
		return this.owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getWarrantCode() {
		return this.warrantCode;
	}

	public void setWarrantCode(String warrantCode) {
		this.warrantCode = warrantCode;
	}

	public String getUnit() {
		return this.unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Double getNumber() {
		return this.number;
	}

	public void setNumber(Double number) {
		this.number = number;
	}

	public Double getConfirmValue() {
		return this.confirmValue;
	}

	public void setConfirmValue(Double confirmValue) {
		this.confirmValue = confirmValue;
	}

	public Double getPledAmt() {
		return this.pledAmt;
	}

	public void setPledAmt(Double pledAmt) {
		this.pledAmt = pledAmt;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getPledType() {
		return pledType;
	}

	public void setPledType(String pledType) {
		this.pledType = pledType;
	}

	
}