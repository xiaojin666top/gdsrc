package com.beawan.survey.loanInfo.bean;

import java.io.Serializable;

public class Rate implements Serializable{

	private Long taskId;
	private String cdRate;
	private String way;
	private String model;
	
	
	public Rate() {
		super();
	}


	public Long getTaskId() {
		return taskId;
	}


	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}


	public String getCdRate() {
		return cdRate;
	}


	public void setCdRate(String cdRate) {
		this.cdRate = cdRate;
	}


	public String getWay() {
		return way;
	}


	public void setWay(String way) {
		this.way = way;
	}


	public String getModel() {
		return model;
	}


	public void setModel(String model) {
		this.model = model;
	}
	
	
	
}
