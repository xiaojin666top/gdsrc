package com.beawan.survey.loanInfo.bean;
// default package

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;

/**
 * ApplyScheme entity. 本次授信申请方案
 */

@Entity
@Table(name = "BIZ_APPLY_SCHEME", schema = "GDTCESYS")
public class ApplyScheme extends BaseEntity implements java.io.Serializable {

    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BIZ_APPLY_SCHEME_SEQ")
    //@SequenceGenerator(name = "BIZ_APPLY_SCHEME_SEQ",allocationSize=1,initialValue=1, sequenceName = "BIZ_APPLY_SCHEME_SEQ")
    private Long id;
    @Column(name = "BUSINESS_TYPE")
    private String businessType;//业务品种
    @Column(name = "CREDIT_AMT")
    private Double creditAmt;//授信额度
    @Column(name = "CURRENCY")
    private String currency;//币种
    @Column(name = "CREDIT_AMT_TYPE")
    private String creditAmtType; //额度使用方式（一次性/循环）
    @Column(name = "CREDIT_TERM")
    private Integer creditTerm;//授信额度期限(月)
    @Column(name = "CREDIT_RATE")
    private Double creditRate;//利率/费率
    @Column(name = "GRAUATEE_WAY")
    private String grauateeWay;//担保方式
    @Column(name = "ADD_GRAUATEE")
    private String addGrauatee;//追加担保
    @Column(name = "USE_DESC")
    private String useDesc;//授信用途
    @Column(name = "PAYMENT_TYPE")
    private String paymentType;//支付方式
    @Column(name = "REMARKS")
    private String remarks;//其他说明
    @Column(name = "TASK_ID")
    private Long taskId;


    // Constructors

    /**
     * default constructor
     */
    public ApplyScheme() {
    }

    /**
     * minimal constructor
     */
    public ApplyScheme(Long id) {
        this.id = id;
    }

    /**
     * full constructor
     */
    public ApplyScheme(Long id, String businessType, Double creditAmt, String currency, String creditAmtType, Integer creditTerm, Double creditRate, String grauateeWay, String useDesc, String paymentType, String remarks, Long taskId) {
        this.id = id;
        this.businessType = businessType;
        this.creditAmt = creditAmt;
        this.currency = currency;
        this.creditAmtType = creditAmtType;
        this.creditTerm = creditTerm;
        this.creditRate = creditRate;
        this.grauateeWay = grauateeWay;
        this.useDesc = useDesc;
        this.paymentType = paymentType;
        this.remarks = remarks;
        this.taskId = taskId;
    }


    // Property accessors

    public Long getId() {
        return this.id;
    }
    
    public String getAddGrauatee() {
		return addGrauatee;
	}

	public void setAddGrauatee(String addGrauatee) {
		this.addGrauatee = addGrauatee;
	}

	public void setId(Long id) {
        this.id = id;
    }

    public String getBusinessType() {
        return this.businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public Double getCreditAmt() {
        return this.creditAmt;
    }

    public void setCreditAmt(Double creditAmt) {
        this.creditAmt = creditAmt;
    }

    public String getCurrency() {
        return this.currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCreditAmtType() {
        return this.creditAmtType;
    }

    public void setCreditAmtType(String creditAmtType) {
        this.creditAmtType = creditAmtType;
    }

    public Integer getCreditTerm() {
        return this.creditTerm;
    }

    public void setCreditTerm(Integer creditTerm) {
        this.creditTerm = creditTerm;
    }

    public Double getCreditRate() {
        return this.creditRate;
    }

    public void setCreditRate(Double creditRate) {
        this.creditRate = creditRate;
    }

    public String getGrauateeWay() {
        return this.grauateeWay;
    }

    public void setGrauateeWay(String grauateeWay) {
        this.grauateeWay = grauateeWay;
    }

    public String getUseDesc() {
        return this.useDesc;
    }

    public void setUseDesc(String useDesc) {
        this.useDesc = useDesc;
    }

    public String getPaymentType() {
        return this.paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getRemarks() {
        return this.remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Long getTaskId() {
        return this.taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }


}