package com.beawan.survey.loanInfo.bean;

// default package

/**
 * FixedProjInvest entity. @author MyEclipse Persistence Tools
 */

public class FixedProjInvest implements java.io.Serializable {

	// Fields

	private Long id;
	private Long taskId;
	private String itemCode; // 项目代码
	private String itemName; // 项目名称
	private Double planInvestAmt; // 计划总投入资金
	private Double investedAmt; // 工程已投入
	private Double paidAmt; // 已支付资金

	// Constructors

	/** default constructor */
	public FixedProjInvest() {
	}

	/** minimal constructor */
	public FixedProjInvest(Long id) {
		this.id = id;
	}

	/** full constructor */
	public FixedProjInvest(Long id, Long taskId, String itemCode,
			String itemName, Double planInvestAmt, Double investedAmt,
			Double paidAmt) {
		this.id = id;
		this.taskId = taskId;
		this.itemCode = itemCode;
		this.itemName = itemName;
		this.planInvestAmt = planInvestAmt;
		this.investedAmt = investedAmt;
		this.paidAmt = paidAmt;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getItemCode() {
		return this.itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return this.itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Double getPlanInvestAmt() {
		return this.planInvestAmt;
	}

	public void setPlanInvestAmt(Double planInvestAmt) {
		this.planInvestAmt = planInvestAmt;
	}

	public Double getInvestedAmt() {
		return this.investedAmt;
	}

	public void setInvestedAmt(Double investedAmt) {
		this.investedAmt = investedAmt;
	}

	public Double getPaidAmt() {
		return this.paidAmt;
	}

	public void setPaidAmt(Double paidAmt) {
		this.paidAmt = paidAmt;
	}

}