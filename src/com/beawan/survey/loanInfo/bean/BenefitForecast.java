package com.beawan.survey.loanInfo.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 效益预测表
 */

@Entity
@Table(name = "BIZ_BENEFIT_FORECAST",schema = "GDTCESYS")
public class BenefitForecast extends BaseEntity implements java.io.Serializable {
	
	@Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="BIZ_BENEFIT_FORECAST")
  //  @SequenceGenerator(name="BIZ_BENEFIT_FORECAST",allocationSize=1,initialValue=1, sequenceName="BIZ_BENEFIT_FORECAST_SEQ")
	private Integer id; 
	
	@Column(name = "TASK_ID")
	private Long taskId;

	@Column(name = "START_YEAR")
	private Integer startYear; //效益预测开始年

	@Column(name = "YEAR_LENGTH")
	private Integer yearLength; //效益年份
	
	@Column(name = "TABLE_COLS")
	private String tableCols; // 表头数据   json格式
	@Column(name = "TABLE_DATA")
	private String tableData; // 行数据 json格式
	

	// Constructors

	/** default constructor */
	public BenefitForecast() {
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Long getTaskId() {
		return taskId;
	}


	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}


	public Integer getStartYear() {
		return startYear;
	}


	public void setStartYear(Integer startYear) {
		this.startYear = startYear;
	}


	public String getTableCols() {
		return tableCols;
	}


	public void setTableCols(String tableCols) {
		this.tableCols = tableCols;
	}


	public String getTableData() {
		return tableData;
	}


	public void setTableData(String tableData) {
		this.tableData = tableData;
	}


	public Integer getYearLength() {
		return yearLength;
	}


	public void setYearLength(Integer yearLength) {
		this.yearLength = yearLength;
	}

	
	

}