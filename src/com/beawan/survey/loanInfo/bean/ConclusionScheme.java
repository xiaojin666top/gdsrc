package com.beawan.survey.loanInfo.bean;
// default package



/**
 * ConclusionSchema entity. @author MyEclipse Persistence Tools
 */

public class ConclusionScheme  implements java.io.Serializable {


    // Fields    

     private Long id;
     private String businessType;//业务品种
     private Double creditAmt;//授信额度
     private String currency;//币种
     private String currencyStatus;//币种是否可调剂
     private String creditAmtType;//额度使用方式（一次性/循环）
     private Integer creditTerm;//授信额度期限(月)
     private Integer sinBizTerm;//单笔业务期限(月)
     private Double creditRate;//利率/费率
     private Double floatRatio;//利率/费率浮动比例
     private String grauateeWay;//担保方式
     private String paymentType;//支付方式
     private String remarks;
     private Long taskId;


    // Constructors

    /** default constructor */
    public ConclusionScheme() {
    }

	/** minimal constructor */
    public ConclusionScheme(Long id) {
        this.id = id;
    }
    
    /** full constructor */
    public ConclusionScheme(Long id, String businessType, Double creditAmt, String currency, String currencyStatus, String creditAmtType, Integer creditTerm, Integer sinBizTerm, Double creditRate, Double floatRatio, String grauateeWay, String paymentType, String remarks, Long taskId) {
        this.id = id;
        this.businessType = businessType;
        this.creditAmt = creditAmt;
        this.currency = currency;
        this.currencyStatus = currencyStatus;
        this.creditAmtType = creditAmtType;
        this.creditTerm = creditTerm;
        this.sinBizTerm = sinBizTerm;
        this.creditRate = creditRate;
        this.floatRatio = floatRatio;
        this.grauateeWay = grauateeWay;
        this.paymentType = paymentType;
        this.remarks = remarks;
        this.taskId = taskId;
    }

   
    // Property accessors

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public String getBusinessType() {
        return this.businessType;
    }
    
    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public Double getCreditAmt() {
        return this.creditAmt;
    }
    
    public void setCreditAmt(Double creditAmt) {
        this.creditAmt = creditAmt;
    }

    public String getCurrency() {
        return this.currency;
    }
    
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencyStatus() {
        return this.currencyStatus;
    }
    
    public void setCurrencyStatus(String currencyStatus) {
        this.currencyStatus = currencyStatus;
    }

    public String getCreditAmtType() {
        return this.creditAmtType;
    }
    
    public void setCreditAmtType(String creditAmtType) {
        this.creditAmtType = creditAmtType;
    }

    public Integer getCreditTerm() {
        return this.creditTerm;
    }
    
    public void setCreditTerm(Integer creditTerm) {
        this.creditTerm = creditTerm;
    }

    public Integer getSinBizTerm() {
        return this.sinBizTerm;
    }
    
    public void setSinBizTerm(Integer sinBizTerm) {
        this.sinBizTerm = sinBizTerm;
    }

    public Double getCreditRate() {
        return this.creditRate;
    }
    
    public void setCreditRate(Double creditRate) {
        this.creditRate = creditRate;
    }

    public Double getFloatRatio() {
        return this.floatRatio;
    }
    
    public void setFloatRatio(Double floatRatio) {
        this.floatRatio = floatRatio;
    }

    public String getGrauateeWay() {
        return this.grauateeWay;
    }
    
    public void setGrauateeWay(String grauateeWay) {
        this.grauateeWay = grauateeWay;
    }

    public String getPaymentType() {
        return this.paymentType;
    }
    
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getRemarks() {
        return this.remarks;
    }
    
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Long getTaskId() {
        return this.taskId;
    }
    
    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }
   








}