package com.beawan.survey.loanInfo.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;
import java.security.Timestamp;
import java.util.Date;

/**
 * RiskAnalysis entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CM_RISK_ANALYSIS", schema = "GDTCESYS")
public class RiskAnalysis extends BaseEntity implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY/*, generator = "CM_RISK_ANALYSIS"*/)
    //@SequenceGenerator(name = "CM_RISK_ANALYSIS",allocationSize=1,initialValue=1, sequenceName = "CM_RISK_ANALYSIS_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;// 任务流水号
    @Column(name = "INDUSTRY_RISK")
    private String industryRisk; //行业风险
    @Column(name = "WARNING_RISK")
    private String warningRisk; //风险预警信息
    
    @Column(name = "BUSINESS_RISK")
    private String businessRisk; //经营风险
    @Column(name = "FINANCIAL_RISK")
    private String financialRisk; //财务风险
    @Column(name = "GUARANTEE_RISK")
    private String guaranteeRisk; //担保风险
    @Column(name = "OTHER_RISK")
    private String otherRisk; //其他风险

/*    @Column(name = "STATUS")  //父类BaseEntity中已经有了
    private Integer status;
    @Column(name = "CREATER")
    private String  creater;
    @Column(name = "CREATE_TIME")
    private Timestamp  createTime;
    @Column(name = "UPDATER")
    private String  updater;
    @Column(name = "UPDATE_TIME")
    private Timestamp  updateTime;*/

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getIndustryRisk() {
        return industryRisk;
    }

    public void setIndustryRisk(String industryRisk) {
        this.industryRisk = industryRisk;
    }
    
    public String getWarningRisk() {
		return warningRisk;
	}

	public void setWarningRisk(String warningRisk) {
		this.warningRisk = warningRisk;
	}

	public String getBusinessRisk() {
        return businessRisk;
    }

    public void setBusinessRisk(String businessRisk) {
        this.businessRisk = businessRisk;
    }

    public String getFinancialRisk() {
        return financialRisk;
    }

    public void setFinancialRisk(String financialRisk) {
        this.financialRisk = financialRisk;
    }

    public String getGuaranteeRisk() {
        return guaranteeRisk;
    }

    public void setGuaranteeRisk(String guaranteeRisk) {
        this.guaranteeRisk = guaranteeRisk;
    }

    public String getOtherRisk() {
        return otherRisk;
    }

    public void setOtherRisk(String otherRisk) {
        this.otherRisk = otherRisk;
    }
}