package com.beawan.survey.loanInfo.bean;

// default package

/**
 * EstaProjBase entity. @author MyEclipse Persistence Tools
 */

public class EstaProjBase implements java.io.Serializable {

	// Fields

	private Long taskId;
	private String customerNo;
	private String projName;//项目名称
	private Double floorSpace;//占地面积
	private Double grossAreaTotal;//建筑总面积
	private Double withoutFlotArea;//不计容面积
	private Double flotRatio;//容积率
	private Double floorPrice;//楼面价
	private String projAddr;//建设地点
	private String invesScale;//投资规模
	private String consCycle;//建设周期
	private String fundsSource;//资金来源
	private String feasiStudyRepo;//可研报告
	private String projApproval;//项目批文
	private String projSiteSelection;//项目选址意见书
	private String envirImpactAssess;//环评
	private String bulidLandLice;//建设用地规划许可证
	private String natiLandUse;//国有土地使用证
	private String buildProjPlanLice;//建设工程规划许可证
	private String buildProjImplLice;//建设工程施工许可证
	private String otherRequInfo;//其他所需资料
	private String projConsCont;//工程施工合同
	private String projImplProgress;//项目实施进度
	private String presalePremitColleTime;//预售许可证领取时间
	private String presaleIncomeInfo;//取得预售收入情况
	private String investedFundsSource;//已投入资金来源
	private String efficMeasure;//效益测算
	private String repaymentPlan;//还款计划
	private String otherSituatInfo;//其他需说明事项
	private String projOverallEval;//项目情况作总体说明分析

	// Constructors

	/** default constructor */
	public EstaProjBase() {
	}

	// Property accessors

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getCustomerNo() {
		return this.customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getProjName() {
		return this.projName;
	}

	public void setProjName(String projName) {
		this.projName = projName;
	}

	public Double getFloorSpace() {
		return this.floorSpace;
	}

	public void setFloorSpace(Double floorSpace) {
		this.floorSpace = floorSpace;
	}

	public Double getGrossAreaTotal() {
		return this.grossAreaTotal;
	}

	public void setGrossAreaTotal(Double grossAreaTotal) {
		this.grossAreaTotal = grossAreaTotal;
	}

	public Double getWithoutFlotArea() {
		return this.withoutFlotArea;
	}

	public void setWithoutFlotArea(Double withoutFlotArea) {
		this.withoutFlotArea = withoutFlotArea;
	}

	public Double getFlotRatio() {
		return this.flotRatio;
	}

	public void setFlotRatio(Double flotRatio) {
		this.flotRatio = flotRatio;
	}

	public Double getFloorPrice() {
		return this.floorPrice;
	}

	public void setFloorPrice(Double floorPrice) {
		this.floorPrice = floorPrice;
	}

	public String getProjAddr() {
		return this.projAddr;
	}

	public void setProjAddr(String projAddr) {
		this.projAddr = projAddr;
	}

	public String getInvesScale() {
		return this.invesScale;
	}

	public void setInvesScale(String invesScale) {
		this.invesScale = invesScale;
	}

	public String getConsCycle() {
		return this.consCycle;
	}

	public void setConsCycle(String consCycle) {
		this.consCycle = consCycle;
	}

	public String getFundsSource() {
		return this.fundsSource;
	}

	public void setFundsSource(String fundsSource) {
		this.fundsSource = fundsSource;
	}

	public String getFeasiStudyRepo() {
		return this.feasiStudyRepo;
	}

	public void setFeasiStudyRepo(String feasiStudyRepo) {
		this.feasiStudyRepo = feasiStudyRepo;
	}

	public String getProjApproval() {
		return this.projApproval;
	}

	public void setProjApproval(String projApproval) {
		this.projApproval = projApproval;
	}

	public String getProjSiteSelection() {
		return this.projSiteSelection;
	}

	public void setProjSiteSelection(String projSiteSelection) {
		this.projSiteSelection = projSiteSelection;
	}

	public String getEnvirImpactAssess() {
		return this.envirImpactAssess;
	}

	public void setEnvirImpactAssess(String envirImpactAssess) {
		this.envirImpactAssess = envirImpactAssess;
	}

	public String getBulidLandLice() {
		return this.bulidLandLice;
	}

	public void setBulidLandLice(String bulidLandLice) {
		this.bulidLandLice = bulidLandLice;
	}

	public String getNatiLandUse() {
		return this.natiLandUse;
	}

	public void setNatiLandUse(String natiLandUse) {
		this.natiLandUse = natiLandUse;
	}

	public String getBuildProjPlanLice() {
		return this.buildProjPlanLice;
	}

	public void setBuildProjPlanLice(String buildProjPlanLice) {
		this.buildProjPlanLice = buildProjPlanLice;
	}

	public String getBuildProjImplLice() {
		return this.buildProjImplLice;
	}

	public void setBuildProjImplLice(String buildProjImplLice) {
		this.buildProjImplLice = buildProjImplLice;
	}

	public String getOtherRequInfo() {
		return this.otherRequInfo;
	}

	public void setOtherRequInfo(String otherRequInfo) {
		this.otherRequInfo = otherRequInfo;
	}

	public String getProjConsCont() {
		return this.projConsCont;
	}

	public void setProjConsCont(String projConsCont) {
		this.projConsCont = projConsCont;
	}

	public String getProjImplProgress() {
		return this.projImplProgress;
	}

	public void setProjImplProgress(String projImplProgress) {
		this.projImplProgress = projImplProgress;
	}

	public String getPresalePremitColleTime() {
		return this.presalePremitColleTime;
	}

	public void setPresalePremitColleTime(String presalePremitColleTime) {
		this.presalePremitColleTime = presalePremitColleTime;
	}

	public String getPresaleIncomeInfo() {
		return this.presaleIncomeInfo;
	}

	public void setPresaleIncomeInfo(String presaleIncomeInfo) {
		this.presaleIncomeInfo = presaleIncomeInfo;
	}

	public String getInvestedFundsSource() {
		return this.investedFundsSource;
	}

	public void setInvestedFundsSource(String investedFundsSource) {
		this.investedFundsSource = investedFundsSource;
	}

	public String getEfficMeasure() {
		return this.efficMeasure;
	}

	public void setEfficMeasure(String efficMeasure) {
		this.efficMeasure = efficMeasure;
	}

	public String getRepaymentPlan() {
		return this.repaymentPlan;
	}

	public void setRepaymentPlan(String repaymentPlan) {
		this.repaymentPlan = repaymentPlan;
	}

	public String getOtherSituatInfo() {
		return this.otherSituatInfo;
	}

	public void setOtherSituatInfo(String otherSituatInfo) {
		this.otherSituatInfo = otherSituatInfo;
	}

	public String getProjOverallEval() {
		return this.projOverallEval;
	}

	public void setProjOverallEval(String projOverallEval) {
		this.projOverallEval = projOverallEval;
	}

}