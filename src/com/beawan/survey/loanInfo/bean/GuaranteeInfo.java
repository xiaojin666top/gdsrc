package com.beawan.survey.loanInfo.bean;
// default package



/**
 * GuaranteeInfo entity. @author MyEclipse Persistence Tools
 */

public class GuaranteeInfo  implements java.io.Serializable {


    // Fields    

     private Long taskId;
     private String guaraType;//担保方式
     private String guarantorAnalysis;//保证人说明分析
     private String mortgageAnalysis;//抵押说明分析
     private String pledgeAnalysis;//质押说明分析
     private String otherAnalysis;//其他担保方式说明分析
     private String changeAnalysis;//担保变化说明分析


    // Constructors

    /** default constructor */
    public GuaranteeInfo() {
    }

	/** minimal constructor */
    public GuaranteeInfo(Long taskId) {
        this.taskId = taskId;
    }
    
    /** full constructor */
    public GuaranteeInfo(Long taskId, String guaraType, String guarantorAnalysis, String mortgageAnalysis, String pledgeAnalysis, String otherAnalysis, String changeAnalysis) {
        this.taskId = taskId;
        this.guaraType = guaraType;
        this.guarantorAnalysis = guarantorAnalysis;
        this.mortgageAnalysis = mortgageAnalysis;
        this.pledgeAnalysis = pledgeAnalysis;
        this.otherAnalysis = otherAnalysis;
        this.changeAnalysis = changeAnalysis;
    }

   
    // Property accessors

    public Long getTaskId() {
        return this.taskId;
    }
    
    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getGuaraType() {
        return this.guaraType;
    }
    
    public void setGuaraType(String guaraType) {
        this.guaraType = guaraType;
    }

    public String getGuarantorAnalysis() {
        return this.guarantorAnalysis;
    }
    
    public void setGuarantorAnalysis(String guarantorAnalysis) {
        this.guarantorAnalysis = guarantorAnalysis;
    }

    public String getMortgageAnalysis() {
        return this.mortgageAnalysis;
    }
    
    public void setMortgageAnalysis(String mortgageAnalysis) {
        this.mortgageAnalysis = mortgageAnalysis;
    }

    public String getPledgeAnalysis() {
        return this.pledgeAnalysis;
    }
    
    public void setPledgeAnalysis(String pledgeAnalysis) {
        this.pledgeAnalysis = pledgeAnalysis;
    }

    public String getOtherAnalysis() {
        return this.otherAnalysis;
    }
    
    public void setOtherAnalysis(String otherAnalysis) {
        this.otherAnalysis = otherAnalysis;
    }

    public String getChangeAnalysis() {
        return this.changeAnalysis;
    }
    
    public void setChangeAnalysis(String changeAnalysis) {
        this.changeAnalysis = changeAnalysis;
    }


}