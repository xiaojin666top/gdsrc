package com.beawan.survey.loanInfo.bean;
// default package


import com.beawan.core.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Conclusion entity. 综合结论与授信安排
 */

@Entity
@Table(name = "BIZ_CONCLUSION",schema = "GDTCESYS")
public class Conclusion extends BaseEntity implements java.io.Serializable {


    @Id
    @Column(name = "TASK_ID")
    private Long taskId;
    @Column(name = "CONCLUSION")
    private String conclusion; //调查结论
    @Column(name = "CREDIT_SCHEME")
    private String creditScheme; //授信意见/安排

    //以下两个字段没有用到
    @Column(name = "CREDIT_RATING")
    private String creditRating; //授信评级
    @Column(name = "CREDIT_MGE_REQUIRMENT")
    private String creditMgeRequirment; //授信后管理要求


    // Constructors

    /** default constructor */
    public Conclusion() {
    }

	/** minimal constructor */
    public Conclusion(Long taskId) {
        this.taskId = taskId;
    }
    
    /** full constructor */
    public Conclusion(Long taskId, String creditRating, String conclusion, String creditScheme, String creditMgeRequirment) {
        this.taskId = taskId;
        this.creditRating = creditRating;
        this.conclusion = conclusion;
        this.creditScheme = creditScheme;
        this.creditMgeRequirment = creditMgeRequirment;
    }

   
    // Property accessors

    public Long getTaskId() {
        return this.taskId;
    }
    
    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getCreditRating() {
        return this.creditRating;
    }
    
    public void setCreditRating(String creditRating) {
        this.creditRating = creditRating;
    }

    public String getConclusion() {
        return this.conclusion;
    }
    
    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public String getCreditScheme() {
        return this.creditScheme;
    }
    
    public void setCreditScheme(String creditScheme) {
        this.creditScheme = creditScheme;
    }

    public String getCreditMgeRequirment() {
        return this.creditMgeRequirment;
    }
    
    public void setCreditMgeRequirment(String creditMgeRequirment) {
        this.creditMgeRequirment = creditMgeRequirment;
    }
   
}