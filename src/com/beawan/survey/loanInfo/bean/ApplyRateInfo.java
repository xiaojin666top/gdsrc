package com.beawan.survey.loanInfo.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;

/**
 * 授信申请中的利率说明
 * @author yuzhejia
 *
 */
@Entity
@Table(name = "BIZ_APPLY_RATE_INFO",schema = "GDTCESYS")
public class ApplyRateInfo  extends BaseEntity{

	
	@Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="BIZ_APPLY_RATE_INFO_SEQ")
   // @SequenceGenerator(name="BIZ_APPLY_RATE_INFO_SEQ",allocationSize=1,initialValue=1, sequenceName="BIZ_APPLY_RATE_INFO_SEQ")
    private Long id;
    @Column(name = "TASK_ID")
	private Long taskId;
    @Column(name = "TARGET_RATE")
	private Double targetRate; //目标利率
    @Column(name = "MEASURE_RATE")
	private Double measureRate; //测算利率
    @Column(name = "EXECUTE_RATE")
	private Double executeRate; //拟执行利率
    @Column(name = "DISCOUNT_REASON")
	private String discountReason;//优惠理由
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getTaskId() {
		return taskId;
	}
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	public Double getTargetRate() {
		return targetRate;
	}
	public void setTargetRate(Double targetRate) {
		this.targetRate = targetRate;
	}
	public Double getMeasureRate() {
		return measureRate;
	}
	public void setMeasureRate(Double measureRate) {
		this.measureRate = measureRate;
	}
	public Double getExecuteRate() {
		return executeRate;
	}
	public void setExecuteRate(Double executeRate) {
		this.executeRate = executeRate;
	}
	public String getDiscountReason() {
		return discountReason;
	}
	public void setDiscountReason(String discountReason) {
		this.discountReason = discountReason;
	}
    
    



}