package com.beawan.survey.loanInfo.qbean;
/**
 * 征信报告信息
 * @author beawan_fengjj
 *
 */
public class CreditReportInfo {
	
	private String cusName;//客户名称
	
	private String cusType;//客户类型
	
	private String reportUrl;//报告名称

	public String getCusName() {
		return cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public String getCusType() {
		return cusType;
	}

	public void setCusType(String cusType) {
		this.cusType = cusType;
	}

	public String getReportUrl() {
		return reportUrl;
	}

	public void setReportUrl(String reportUrl) {
		this.reportUrl = reportUrl;
	}
	
}
