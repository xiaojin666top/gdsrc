package com.beawan.survey.loanInfo.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.beawan.survey.loanInfo.bean.Guarantor;
import com.beawan.survey.loanInfo.bean.MortgageInfo;
import com.beawan.survey.loanInfo.bean.PledgeInfo;
import com.beawan.survey.loanInfo.qbean.NameAndRate;

public class GetRateUtil {

	public static Double getRateByCusLev(String creditLevel) {
		
		if(null==creditLevel) {
			return 0.8;
		}
		
		if(creditLevel.equals("AAA")) {
			return 0.2;
		}
		
		if(creditLevel.equals("AA")) {
			return 0.3;
		}
		
		if(creditLevel.equals("A")) {
			return 0.4;
		}
		
		if(creditLevel.equals("BBB")) {
			return 0.5;
		}
		
		if(creditLevel.equals("BB")) {
			return 0.6;
		}
		
		if(creditLevel.equals("B")) {
			return 0.7;
		}
		
		return 0.8;
	}
	
	public static  List<NameAndRate> getlistFromGuarator(List<Guarantor> list){
		
		List<NameAndRate> nameList=new ArrayList<NameAndRate>();
		
		for(Guarantor data:list) {
			NameAndRate bean=new NameAndRate();
			String name=data.getGuaraRateType();
			
			if(name.equals("00")) {
				bean.setRate(0.2);
				bean.setName("国营担保公司担保");
			}
			
			if(name.equals("01")) {
				bean.setRate(0.4);
				bean.setName("民营担保公司担保");
			}
			
			if(name.equals("02")) {
				bean.setRate(0.8);
				bean.setName("优质担保人");
			}
			
			if(name.equals("03")) {
				bean.setRate(1.0);
				bean.setName("一般担保人");
			}
			
			nameList.add(bean);
			
		}
		
		return nameList;
	}
	
public static  List<NameAndRate> getlistFromMor(List<MortgageInfo> list){
		
		List<NameAndRate> nameList=new ArrayList<NameAndRate>();
		
		for(MortgageInfo data:list) {
			NameAndRate bean=new NameAndRate();
			String name=data.getMortType();
			
			
			if(name.equals("00")) {
				bean.setRate(0.8);
				bean.setName("其他资产抵押");
			}
			
			if(name.equals("01")) {
				bean.setRate(0.6);
				bean.setName("自有房产二次抵押");
			}
			
			if(name.equals("02")) {
				bean.setRate(1.0);
				bean.setName("第三方房产抵押（二次抵押除外）");
			}			
			
			nameList.add(bean);
			
		}
		
		return nameList;
	}

public static  List<NameAndRate> getlistFromPled(List<PledgeInfo> list){
	
	List<NameAndRate> nameList=new ArrayList<NameAndRate>();
	
	for(PledgeInfo data:list) {
		NameAndRate bean=new NameAndRate();
		String name=data.getPledType();
		
		if(name.equals("00")) {
			bean.setRate(0.0);
			bean.setName("存单质押、理财质押");
		}
				
		if(name.equals("02")) {
			bean.setRate(0.6);
			bean.setName("仓单、收费权、股权质押");
		}
		
		if(name.equals("03")) {
			bean.setRate(0.8);
			bean.setName("其他质押");
		}
		
		nameList.add(bean);
		
	}
	
	return nameList;
}
}
