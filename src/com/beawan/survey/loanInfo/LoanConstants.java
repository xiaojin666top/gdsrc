package com.beawan.survey.loanInfo;

/**
 * 系统常量
 * 
 * 
 */
public class LoanConstants {
	
	
	/**
	 * 房地产项目现金流量项目名
	 */
	public static final String[] ESTA_PROJ_CASH_FLOW_SUBJ = 
		{
			"（1）资金来源（①＋～＋⑦）",
			"①销售收入",
			"②出租收入",
			"③其他收入",
			"④自有资金",
			"⑤我行借款",
			"⑥他行借款",
			"⑦其他负债",
			"（2）资金运用（①＋～＋⑦）",
			"①开发成本费用",
			"②经营成本",
			"③经营税金及附加",
			"④土地增值税",
			"⑤所得税",
			"⑥我行借款本金偿还",
			"⑦他行借款本金偿还",
			"（3）盈余资金",
			"（4）累计盈余资金",
			"（5）期末我行贷款本金余额",
			"（6）期末他行贷款本金余额"
		};
	/**
	 * 房地产项目
	 */
	public static final String[] ESTA_PROJ_BENEFITS_SUBJ = 
		{
			"住宅销售",
			"写字楼销售",
			"商铺销售",
			"其他销售",
			"（1）销售收入合计",
			"（2）开发成本",
			"（3）销售税费",
			"（4）土地增值税",
			"（5）所得税",
			"（6）总成本",
			"（7）净利润",
			"（8）成本净利润率"
		};
	/**
	 * 房地产项目资金缺口估算科目名
	 */
	public static final String[] ESTA_PROJ_FUNDGAP_DETAIL_INVES = 
		{
			"土地成本（按类别列示）",
			"（1）",
			"（2）",
			"前期工程费",
			"建安工程费",
			"基础和配套设施费",
			"其他费用"
		};
	/**
	 * 房地产项目资金筹措科目名
	 */
	public static final String[] ESTA_PROJ_FUNDGAP_DETAIL_RAISE = 
		{
			"实收资本",
			"预售/预租收入",
			"股东借款",
			"其他销售",
			"我行借款",
			"他行借款",
			"质量保证金",
			"其他资金来源"
		};
	/**
	 * 固定资产项目投资估算和筹资评估科目名
	 */
	public static final String[] FIXED_PROJ_INVE_RAISE = 
		{
			"工程费用",
			"",
			"",
			"",
			"工程建设其他费用",
			"",
			"",
			"",
			"预备费",
			"基本预备费",
			"涨价预备费",
			"静态总投资合计",
			"建设期利息",
			"动态总投资",
			"铺底流动资金",
			"工程总投资"
		};
	/**
	 * 固定资产项目投资计划及资金筹措表科目名
	 */
	public static final String[] FIXED_PROJ_FUND_RAISE = 
		{
			"一、资本金",
			"1、",
			"2、",
			"二、银行贷款",
			"其中我行贷款",
			"三、其他资金",
			"1、",
			"2、",
			"合计"
		};
	/**
	 * 固定资产偿债能力测算表科目名
	 */
	public static final String[] FIXED_SOLVEY_MEASU_SUBJ = 
		{
			"1、收入",
			"其中：产品销售（营业）收入",
			"2、成本费用",
			"其中：生产成本",
			"",
			"管理费用",
			"营业费用",
			"财务费用",
			"税金及附加",
			"3、利润总额",
			"4、所得税",
			"5、净利润",
			"6、年初贷款余额",
			"7、可还款现金流（净利润+折旧）",
			"8、年末贷款余额",
			"9、净现金流（净利润+折旧+财务费用）"
		};
	
	
	
	
	/**
	 * 项目资金缺口明细类型
	 * @author beawan_fengjj
	 *
	 */
	public interface FundGapDetailType {

		public static final String DETAIL_INVS = "01";

		public static final String DETAIL_RAISE = "02";

	}
}
