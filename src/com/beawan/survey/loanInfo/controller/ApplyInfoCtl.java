package com.beawan.survey.loanInfo.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.persistence.Column;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpUtils;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.analysis.finansis.utils.CodeUtil;
import com.beawan.base.entity.TableSubjCode;
import com.beawan.base.entity.User;
import com.beawan.base.service.ISysDicSV;
import com.beawan.base.service.ITableSubjCodeSV;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.SysConstants.ExceptionConstant;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.common.util.SystemUtil;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.beawan.rate.dto.RateQuotaDto;
import com.beawan.rate.dto.RateResult;
import com.beawan.rate.entity.RcCondition;
import com.beawan.rate.entity.RcModel;
import com.beawan.rate.entity.RcQuota;
import com.beawan.rate.service.IRcModelSV;
import com.beawan.survey.custInfo.dto.CompBaseDto;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.beawan.survey.loanInfo.bean.ApplyInfo;
import com.beawan.survey.loanInfo.bean.ApplyRate;
import com.beawan.survey.loanInfo.bean.ApplyRateInfo;
import com.beawan.survey.loanInfo.bean.ApplyScheme;
import com.beawan.survey.loanInfo.bean.BenefitAnaly;
import com.beawan.survey.loanInfo.bean.BenefitForecast;
import com.beawan.survey.loanInfo.bean.Guarantor;
import com.beawan.survey.loanInfo.bean.MortgageInfo;
import com.beawan.survey.loanInfo.bean.PledgeInfo;
import com.beawan.survey.loanInfo.bean.Rate;
import com.beawan.survey.loanInfo.qbean.NameAndRate;
import com.beawan.survey.loanInfo.service.ApplyRateInfoService;
import com.beawan.survey.loanInfo.service.IApplyInfoSV;
import com.beawan.survey.loanInfo.service.IGuaraInfoSV;
import com.beawan.survey.loanInfo.util.GetRateUtil;
import com.beawan.task.bean.Task;
import com.beawan.task.controller.TaskCtl;
import com.beawan.task.service.ITaskSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.fasterxml.jackson.core.type.TypeReference;
import com.platform.util.DateUtil;
import com.platform.util.GsonUtil;
import com.platform.util.HttpUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.PropertiesUtil;

import net.sf.json.util.JSONUtils;


/**
 * @ClassName ApplyInfoCtl
 * @Description 本次申请信息
 * @author rain
 * @Date 2018年1月21日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/survey/loan" })
public class ApplyInfoCtl extends BaseController{
	
	private static Logger log = Logger.getLogger(TaskCtl.class);
	
	@Resource
	protected ITaskSV taskSV;

	@Resource
	private IApplyInfoSV applyInfoSV;
	
	@Resource
	private ISysDicSV sysDicSV;
	
	@Resource
	private ITableSubjCodeSV tableSubjCodeSV;
	
	@Resource
	private ICmisInInvokeSV cmisInInvokeSV;
	
	@Resource
	private ICompBaseSV compBaseSV;
	
	@Resource
	private IGuaraInfoSV guaraInfoSV;
	@Resource
	private IRcModelSV rcModelSV;
	@Resource
	private ApplyRateInfoService applyRateInfoService;
	@Resource
	private com.beawan.survey.loanInfo.service.BenefitForecastService benefitForecastService;
	@Resource
	private com.beawan.survey.loanInfo.service.BenefitAnalyService benefitAnalyService;
	
	

	/**
	 * 授信 利率信息页面跳转
	 * 获取申请利率信息
	 * @param taskId
	 * @return
	 */
	@RequestMapping("rateInfo.do")
	public ModelAndView rateInfo(long taskId) {
		ModelAndView mav = new ModelAndView("views/survey/common/loanRateInfo");
		try {
			ApplyRateInfo rate = applyRateInfoService.selectSingleByProperty("taskId", taskId);
			mav.addObject("rate", rate);
			mav.addObject("taskId", taskId);
			
			//加载字典数据
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
	
	
	
	/**
	 * 保存申请利率信息
	 * @param taskId
	 * @param request
	 * @return
	 */
	@RequestMapping("saveApplyRateInfo.json")
	@ResponseBody
	public ResultDto saveApplyRateInfo(ApplyRateInfo rateInfo, HttpServletRequest request) {
		ResultDto re = returnFail("");
		if(rateInfo==null){
			re.setMsg("客户申请利率信息为空，请输入后重试");
			return re;
		}
		ApplyRateInfo entity;
		User user = HttpUtil.getCurrentUser(request);
		try{
			Long id = rateInfo.getId();
			if(id==null || id==0){
				entity = new ApplyRateInfo();
			}else{
				entity = applyRateInfoService.findByPrimaryKey(id);
			}
			entity.setTaskId(rateInfo.getTaskId());
			entity.setTargetRate(rateInfo.getTargetRate());
			entity.setMeasureRate(rateInfo.getMeasureRate());
			entity.setExecuteRate(rateInfo.getExecuteRate());
			entity.setDiscountReason(rateInfo.getDiscountReason());
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			ApplyRateInfo info = applyRateInfoService.saveOrUpdate(entity);
			
			Task task = taskSV.getTaskById(rateInfo.getTaskId());
			task.setLoanRate(rateInfo.getExecuteRate().toString());
			taskSV.saveOrUpdate(task);
			
			re.setRows(info);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
		}
		return re;
	}
	
	
	
	/**
	 * 授信申请信息页面跳转
	 * @param taskId
	 * @return
	 */
	@RequestMapping("applyInfo.do")
	public ModelAndView applyInfo(long taskId) {
		ModelAndView mav = new ModelAndView("views/survey/common/loanApplyInfo");
		try {
			Task task = this.taskSV.getTaskById(taskId);
			mav.addObject("task", task);
			
			ApplyInfo applyInfo = applyInfoSV.findApplyInfoByTaskId(taskId);
			if(applyInfo == null){
				applyInfo = new ApplyInfo();
				applyInfo.setTaskId(taskId);
			}
			mav.addObject("applyInfo", applyInfo);
			//加载字典数据
			String[] optTypes = {
					SysConstants.BsDicConstant.STD_ZB_LIMIT_TYPE,
					SysConstants.BsDicConstant.STD_IQP_PAY_TYPE,
					SysConstants.BsDicConstant.STD_SY_GUAR_TYPE,
					SysConstants.BsDicConstant.RG_PRODUCT_LIST};
			mav.addObject("dicData", sysDicSV.queryMapListByTypes(optTypes));
			
			List<String> infoList = cmisInInvokeSV.getLastCreditInfo(task.getCustomerNo());
			mav.addObject("lastCreditInfoList", infoList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
	
	/**
	 * 保存 授信申请信息
	 * @param taskId 任务主键
	 * @param request 
	 * @return
	 */
	@RequestMapping("saveApplyInfo.json")
	@ResponseBody
	public ResultDto saveApplyInfo(Long taskId, HttpServletRequest request) {
		ResultDto re = returnFail("保存失败！");
		String applyInfo = request.getParameter("applyInfo");
		String applyReason = request.getParameter("applyReason");
		try {
			User user = HttpUtil.getCurrentUser(request);
			ApplyInfo entity = applyInfoSV.findApplyInfoByTaskId(taskId);
			if (entity == null) {
				entity = new ApplyInfo();
				entity.setTaskId(taskId);
				entity.setCreater(user.getUserId());
				entity.setCreateTime(DateUtil.getNowTimestamp());
			}

			if (applyInfo != null) entity.setApplyInfo(applyInfo);
			if (applyReason != null) entity.setApplyReason(applyReason);
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			applyInfoSV.saveApplyInfo(entity);
			
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (DataAccessException e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 获取 授信申请方案列表
	 * @param taskId
	 * @return
	 */
	@RequestMapping(value = "getApplySchemeList.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto getApplyScheme(long taskId) {
		ResultDto re = returnFail("获取数据失败！");
		try {
			List<ApplyScheme> entityList = applyInfoSV.findApplySchemeListByTaskId(taskId);
			re.setMsg("获取数据成功！");
			re.setRows(entityList);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 保存或修改 授信申请方案
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveApplyScheme.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto saveApplyScheme(HttpServletRequest request){
		ResultDto re = returnFail("保存失败");
		String jsonData = request.getParameter("jsonArray");
		try {
			List<ApplyScheme> entityList =
					JacksonUtil.fromJson(jsonData, new TypeReference<List<ApplyScheme>>(){});
			if (CollectionUtils.isEmpty(entityList))
				return returnFail("传输数据内容为空，请重试");

			User user = HttpUtil.getCurrentUser(request);
			Timestamp nowTime = DateUtil.getNowTimestamp();
			Long taskId = null;
			Double sum = 0d;
			Integer maxTerm = 0;
			for (ApplyScheme entity : entityList) {
				taskId = entity.getTaskId();
				Double creditAmt = entity.getCreditAmt();
				if(entity.getCreditTerm() != null){
					if(entity.getCreditTerm() > maxTerm){
						maxTerm = entity.getCreditTerm();
					}
				}
				if(creditAmt!=null){
					sum += creditAmt;
				}
				if(entity.getId() == null){
					entity.setCreater(user.getUserId());
					entity.setCreateTime(nowTime);
				}
				entity.setUpdater(user.getUserId());
				entity.setUpdateTime(nowTime);
				entity.setStatus(Constants.NORMAL);
				applyInfoSV.saveApplyScheme(entity);
			}
			
			Task lbTask = taskSV.getTaskById(taskId);
			if(lbTask==null){
				log.error("当前授信申请任务流水号异常，请核查！");
			}
			lbTask.setLoanTerm(maxTerm);
			lbTask.setLoanNumber(sum);
			taskSV.saveOrUpdate(lbTask);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除 授信申请方案
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "deleteApplyScheme.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto deleteApplyScheme(HttpServletRequest request, Long id) {
		ResultDto re = returnFail("删除失败");
		if (id == null || id == 0)
			return returnFail("传输数据内容异常，请重试");

		User user = HttpUtil.getCurrentUser(request);
		try {
			Long taskId = null;
			ApplyScheme applyScheme = applyInfoSV.findApplySchemeById(id);
			taskId = applyScheme.getTaskId();
			applyScheme.setStatus(Constants.DELETE);
			applyScheme.setUpdater(user.getUserId());
			applyScheme.setUpdateTime(DateUtil.getNowTimestamp());
			applyInfoSV.saveApplyScheme(applyScheme);

			List<ApplyScheme> entityList = applyInfoSV.findApplySchemeListByTaskId(taskId);

			Double sum = 0d;
			Integer maxTerm = 0;
			for (ApplyScheme entity : entityList) {
				
				Double creditAmt = entity.getCreditAmt();
				if(entity.getCreditTerm() != null){
					if(entity.getCreditTerm() > maxTerm){
						maxTerm = entity.getCreditTerm();
					}
				}
				if(creditAmt!=null){
					sum += creditAmt;
				}			
			}
			
			Task lbTask = taskSV.getTaskById(taskId);
			if(lbTask==null){
				log.error("当前授信申请任务流水号异常，请核查！");
			}
			lbTask.setLoanTerm(maxTerm);
			lbTask.setLoanNumber(sum);
			taskSV.saveOrUpdate(lbTask);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * @Description (授信申请利率及收益页面跳转)
	 * @param request
	 * @param response
	 * @param taskId 任务ID
	 * @return
	 */
	@RequestMapping("applyRate2.do")
	public String applyRate(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		
		try {
			
			Task task = this.taskSV.getTaskById(taskId);
			request.setAttribute("task", task);
			
			int loanTerm=task.getLoanTerm();
			request.setAttribute("loanTerm", loanTerm);
			
			double basePrice=0;
			if(loanTerm<=1) {
				basePrice=3.614;
			}else if(loanTerm<=5) {
				basePrice=4.391;
			}else {
				basePrice=4.718;
			}
			request.setAttribute("basePrice", basePrice);
			
			
			String customerNo=task.getCustomerNo();
			CompBaseDto compBase = compBaseSV.findCompBaseByCustNo(customerNo);
			
			String cutsomerLev=compBase.getCreditLevel();
			request.setAttribute("cutsomerLev", cutsomerLev);
			
			double cusRate=GetRateUtil.getRateByCusLev(cutsomerLev);
			request.setAttribute("cusRate", cusRate);
			
			List<NameAndRate> allList=new ArrayList<NameAndRate>();
			
			List<Guarantor> glList=guaraInfoSV.findGuarantorByTaskId(taskId);
			
			
			
			if(glList!=null&&glList.size()>0) {
				List<NameAndRate> guaralist=GetRateUtil.getlistFromGuarator(glList);				
				allList.addAll(guaralist);
			}
			List<MortgageInfo> mList=guaraInfoSV.findMortgageInfoByTaskId(taskId);
			if(mList!=null&&mList.size()>0) {
				List<NameAndRate> morlist=GetRateUtil.getlistFromMor(mList);
				allList.addAll(morlist);
			}
			List<PledgeInfo>  pList=guaraInfoSV.findPledgeInfoByTaskId(taskId);
			if(mList!=null&&mList.size()>0) {
				List<NameAndRate> pledlist=GetRateUtil.getlistFromPled(pList);				
				allList.addAll(pledlist);
			}
			
			if(allList==null||allList.size()==0) {
				NameAndRate bean=new NameAndRate();
				bean.setName("信用贷款");
				bean.setRate(1.2);
				List<NameAndRate> honor=new ArrayList<NameAndRate>();
				honor.add(bean);
				request.setAttribute("honor", honor);
				request.setAttribute("minRate", 1.2);
			}else {
				double min=100;
				for(NameAndRate data:allList) {
					double newData=data.getRate();
					if(newData<min)
						min=newData;					
				}
				request.setAttribute("minRate", min);
				request.setAttribute("allList", allList);				
			}
			
			ApplyInfo  applyInfo = applyInfoSV.findApplyInfoByTaskId(taskId);
			
			if(applyInfo == null ){
				applyInfo = new ApplyInfo();
				applyInfo.setTaskId(taskId);
			}
			request.setAttribute("applyInfo", applyInfo);
			
			//加载字典数据
			String[] optTypes = {
					SysConstants.BsDicRateConstant.STD_RATE_BASE_EDIT,
					SysConstants.BsDicRateConstant.STD_RATE_MODEL_EDIT,
					SysConstants.BsDicRateConstant.STD_RATE_MOR,
					SysConstants.BsDicRateConstant.STD_RATE_POL,
					SysConstants.BsDicRateConstant.STD_RATE_GUR
					};
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
			
			Rate rateInfo=applyInfoSV.selectRateById(taskId);
			if(rateInfo == null ){
				rateInfo = new Rate();
				rateInfo.setTaskId(taskId);
				rateInfo.setWay("0");
				rateInfo.setModel("1");
				rateInfo.setCdRate("0");
			}
			request.setAttribute("rateInfo", rateInfo);
			User user = HttpUtil.getCurrentUser(request);
			request.setAttribute("isTest", user.getRoleNo().contains("0009") ? "0" : "1");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/survey/common/loanApplyRate";
	}
	
	/**
	 * @Description (授信申请利率及收益页面跳转)
	 * @param request
	 * @param response
	 * @param taskId 任务ID
	 * @return
	 */
	@RequestMapping("applyRate.do")
	public String applyRate2(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		
		try {
			
			Task task = this.taskSV.getTaskById(taskId);
			request.setAttribute("task", task);
			
			int loanTerm=task.getLoanTerm();
			request.setAttribute("loanTerm", loanTerm);
			
			
			RcModel rcModel = rcModelSV.queryById(1);
			
			System.out.println(GsonUtil.GsonString(rcModel));
			double basePrice=0;
			if(loanTerm<=1) {
				basePrice=3.614;
			}else if(loanTerm<=5) {
				basePrice=4.391;
			}else {
				basePrice=4.718;
			}
			
			request.setAttribute("basePrice", basePrice);
			String customerNo=task.getCustomerNo();
			CompBaseDto compBase=compBaseSV.findCompBaseByCustNo(customerNo);
			String cutsomerLev=compBase.getCreditLevel();
			Double dbfs = calcluate(rcModel,"DBFS","国营担保公司担保").getValue();
			Double xyjl =calcluate(rcModel,"XYJL","无不良记录").getValue();
			Double xhed = calcluate(rcModel,"XHED","循环额度形式").getValue();
			Double khpj = calcluate(rcModel,"KHPJ","AAA类客户").getValue();
			Double  yksj=   calcluate(rcModel,"YKSJ","1-5年（含）").getValue();
			
			Double sxje = calcluate(rcModel,"SXJE",Double.valueOf(1200)).getValue();
			Double khzc = calcluate(rcModel,"KHZC","基本户开立在本行").getValue();
			
			request.setAttribute("model",rcModel);
			  
			
			
			Double resultRate=basePrice*(1+khpj)+dbfs+xyjl+xhed+yksj+sxje+khzc;
			
			request.setAttribute("resultRate",resultRate);
			
			/*
			 * List<NameAndRate> allList=new ArrayList<NameAndRate>(); List<Guarantor>
			 * glList=guaraInfoSV.findGuarantorByTaskId(taskId);
			 * if(glList!=null&&glList.size()>0) { List<NameAndRate>
			 * guaralist=GetRateUtil.getlistFromGuarator(glList); allList.addAll(guaralist);
			 * } List<MortgageInfo> mList=guaraInfoSV.findMortgageInfoByTaskId(taskId);
			 * if(mList!=null&&mList.size()>0) { List<NameAndRate>
			 * morlist=GetRateUtil.getlistFromMor(mList); allList.addAll(morlist); }
			 * List<PledgeInfo> pList=guaraInfoSV.findPledgeInfoByTaskId(taskId);
			 * if(mList!=null&&mList.size()>0) { List<NameAndRate>
			 * pledlist=GetRateUtil.getlistFromPled(pList); allList.addAll(pledlist); }
			 * 
			 * if(allList==null||allList.size()==0) { NameAndRate bean=new NameAndRate();
			 * bean.setName("信用贷款"); bean.setRate(1.2); List<NameAndRate> honor=new
			 * ArrayList<NameAndRate>(); honor.add(bean); request.setAttribute("honor",
			 * honor); request.setAttribute("minRate", 1.2); }else { double min=100;
			 * for(NameAndRate data:allList) { double newData=data.getRate();
			 * if(newData<min) min=newData; } request.setAttribute("minRate", min);
			 * request.setAttribute("allList", allList); }
			 * 
			 * ApplyInfo applyInfo = applyInfoSV.findApplyInfoByTaskId(taskId);
			 * 
			 * if(applyInfo == null ){ applyInfo = new ApplyInfo();
			 * applyInfo.setTaskId(taskId); } request.setAttribute("applyInfo", applyInfo);
			 * 
			 * //加载字典数据 String[] optTypes = {
			 * SysConstants.BsDicRateConstant.STD_RATE_BASE_EDIT,
			 * SysConstants.BsDicRateConstant.STD_RATE_MODEL_EDIT,
			 * SysConstants.BsDicRateConstant.STD_RATE_MOR,
			 * SysConstants.BsDicRateConstant.STD_RATE_POL,
			 * SysConstants.BsDicRateConstant.STD_RATE_GUR };
			 * request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
			 * 
			 * Rate rateInfo=applyInfoSV.selectRateById(taskId); if(rateInfo == null ){
			 * rateInfo = new Rate(); rateInfo.setTaskId(taskId); rateInfo.setWay("0");
			 * rateInfo.setModel("1"); rateInfo.setCdRate("0"); }
			 * request.setAttribute("rateInfo", rateInfo); User user =
			 * HttpUtil.getCurrentUser(request); request.setAttribute("isTest",
			 * user.getRoleNo().contains("0009") ? "0" : "1");
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/survey/common/loanApplyRate2";
	}
	
	
	private static RateResult calcluate(RcModel rcModel,String conditionName,Object conditionValue) {
		RateResult rateResult = new RateResult();
		if(rcModel==null ) return rateResult;
		List<RcQuota>rcQuotas =rcModel.getQuotas();
		if(rcQuotas==null || rcQuotas.size()==0) return rateResult;
	     for(RcQuota rcQuota : rcQuotas) {
	    	 if(rcQuota.getCode()!=null && rcQuota.getCode().equals(conditionName)) {
	    		 rateResult=getRateResult(rcQuota, conditionName, conditionValue);
	    		 rcQuota.setValue(conditionValue.toString());
	    		 rcQuota.setRateResult(rateResult);
	    		 break;
	    	 }
	     }
	     
		  return rateResult;	
	}
	
	private static RateResult getRateResult(RcQuota rcQuota,String conditionName,Object conditionValue) {
		RateResult rateResult = new RateResult();
		
		if(rcQuota==null) return rateResult;
	     List<RcCondition> rcConditions = rcQuota.getConditions();  
	     if(rcConditions==null || rcConditions.size()==0) return rateResult;
	     RcCondition findCondition = null;
		  if(rcQuota.getType()!=null && rcQuota.getType().equals("S")) {		 
			  for(RcCondition condition : rcConditions) {
				  if(conditionValue.equals(condition.getContent())) {
					  findCondition=condition;
					  break;
				  }
			  }
	
		  }else {			
			  for(RcCondition condition : rcConditions) {
				 if(fit(condition,(Double)conditionValue)){
					 findCondition=condition;
					  break; 
				 }
			  }	  
		  }  
		  if(findCondition==null) return rateResult;
		  RateQuotaDto resultQuota = findCondition.getResultQuota();
		  rateResult.setCode(resultQuota.getCode());
		  rateResult.setName(resultQuota.getName());
		  rateResult.setUnit(resultQuota.getUnit());
		  if(resultQuota.getUnit()!=null && !"null".equals(resultQuota.getUnit())) {
			  rateResult.setValueString(findCondition.getResultValue().toString()+resultQuota.getUnit());
		  }else {
			  rateResult.setValueString(findCondition.getResultValue().toString());
		  }
		 
		  if(resultQuota.getUnit()!=null && resultQuota.getUnit().equals("%")) {
			  rateResult.setValue(findCondition.getResultValue()/100);
		  }else {
			  rateResult.setValue(findCondition.getResultValue());
		  }	  	  
		
		return rateResult;
	}
	
	private static boolean fit(RcCondition condition,Double conditionValue) {
		boolean isfit = false;
		Double downDouble = condition.getDown();
		Double upperDouble = condition.getUpper();
		if(downDouble==null || upperDouble==null) return isfit;
		if(downDouble>upperDouble) {
			Double value = upperDouble;
			upperDouble= downDouble;
			downDouble=value;
		}
		String type="OUT";
		if(conditionValue<=upperDouble&&conditionValue>=downDouble) {
			type="IN";
			if(conditionValue==upperDouble) type="L";
			if(conditionValue==downDouble) type="R";	
		}
		String conditionType= condition.getAttrValue();
		if(conditionType==null || type=="OUT") return isfit;
		if(conditionType=="L") {
			if(type=="IN" || type=="L") {isfit=true;}
		}else if(conditionType=="R") {
			if(type=="IN" || type=="R") {isfit=true;}
		}else {
			if(type=="IN" ) {isfit=true;}
		}
		return isfit;
	}

	
	
	@RequestMapping("saveRateInfo.do")
	@ResponseBody
	public String saveRateInfo(HttpServletRequest request,HttpServletResponse response) {
        
		String rateInfo = request.getParameter("rate");
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			applyInfoSV.saveRateInfo(rateInfo);
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	
	
	
	
	/**
	 * @Description (获取授信申请方案信息列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getApplyRate.do")
	@ResponseBody
	public String getApplyRate(HttpServletRequest request, HttpServletResponse response,
			long taskId,String customerNo) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<ApplyRate> dataGrid = null;
		
		try {
			dataGrid = applyInfoSV.queryApplyRateListByTaskId(taskId);
			if(CollectionUtils.isEmpty(dataGrid))
				dataGrid = applyInfoSV.findApplyRateListByCustomerNo(customerNo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return jRequest.serialize(dataGrid, true);
	}
	
	/**
	 * @Description (保存授信申请方案信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveApplyRate.do")
	@ResponseBody
	public String saveApplyRate(HttpServletRequest request, HttpServletResponse response,
			String jsondata) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			applyInfoSV.saveApplyRate(JacksonUtil.fromJson(jsondata,
					ApplyRate.class));
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (删除授信申请利率信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("deleteApplyRate.do")
	@ResponseBody
	public String deleteApplyRate(HttpServletRequest request, HttpServletResponse response,
			long id) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			applyInfoSV.deleteApplyRateById(id);
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (授信申请额度及还款能力页面跳转)
	 * @param request
	 * @param response
	 * @param taskId 任务ID
	 * @return
	 */
	@RequestMapping("applyAmount.do")
	public String applyAmount(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		
		try {
			
			Task task = this.taskSV.getTaskById(taskId);
			request.setAttribute("task", task);
			
			ApplyInfo  applyInfo = applyInfoSV.findApplyInfoByTaskId(taskId);
			
			if(applyInfo == null ){
				applyInfo = new ApplyInfo();
				applyInfo.setTaskId(taskId);
			}
			request.setAttribute("applyInfo", applyInfo);
			
			List<String> dateListShow = SystemUtil.genFinaDateList(task.getYear(),
					task.getMonth(), false, true);
			request.setAttribute("dateList", dateListShow);
			
			//显示的财报科目列表
			List<TableSubjCode> finaSubjects = tableSubjCodeSV.queryByTableSubjCodes("all", "FINA_AMT");
			request.setAttribute("finaSubjects", finaSubjects);
			
			//额度测算
			Map<String, Object> calResult = applyInfoSV.getCreditAmtCalResult(task, applyInfo);
			request.setAttribute("amtCalFinaData", calResult.get("amtCalFinaData"));
			request.setAttribute("amtCalResult", calResult.get("amtCalResult"));
			
		} catch (Exception e) {
			log.error("授信申请额度及还款能力页面跳转异常：", e);
		}
		
		return "views/survey/common/loanApplyAmount";
	}
	
	/**
	 * @Description 下载流动资金贷款测算excel文件
	 * @param request
	 * @param response
	 * @param serNo 业务流水号
	 * @return
	 * @throws Exception 
	 * @throws IOException
	 */
	@RequestMapping("downloadAmtCalExcel.do")
	public ResponseEntity<byte[]> genAmtCalExcel(HttpServletRequest request,
			HttpServletResponse response, String serNo) throws Exception {
		
		ResponseEntity<byte[]> result = null;
		
		try{
			String path = PropertiesUtil.getProperty(Constants.SYSTEM_PROPS_PATH,
					"data.temp.path");// 文件缓存地址
			File dir = new File(path);
			if(!dir.exists())
				dir.mkdirs();
			File file = new File(dir, serNo + "_ED.xls");
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentDispositionFormData("attachment", CodeUtil.parseGBK("流动资金需求分析表.xls"));
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			
			result = new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file),
					headers, HttpStatus.OK);
		} catch (Exception e) {
			log.error("下载流动资金贷款测算excel文件异常：", e);
		}
		
		return result;
	}
	
	/**
	 * @Description 流动资金贷款测算excel文件
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("genAmtCalExcel.do")
	@ResponseBody
	public String genAmtCalExcel(HttpServletRequest request,
			HttpServletResponse response, long taskId) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);
		
		OutputStream fos = null;
		
		try {
			
			Task task = this.taskSV.getTaskById(taskId);
			request.setAttribute("task", task);
			
			ApplyInfo  applyInfo = applyInfoSV.findApplyInfoByTaskId(taskId);
			
			if(applyInfo == null ){
				applyInfo = new ApplyInfo();
				applyInfo.setTaskId(taskId);
			}
			
			List<String> dateListShow = SystemUtil.genFinaDateList(task.getYear(),
					task.getMonth(), false, true);
			
			//显示的财报科目列表
			List<TableSubjCode> finaSubjects = tableSubjCodeSV.queryByTableSubjCodes("all", "FINA_AMT");
			
			//额度测算
			Map<String, Object> calResult = applyInfoSV.getCreditAmtCalResult(task, applyInfo);
			List<Map<String, Double>> amtCalFinaData = (List<Map<String, Double>>) calResult.get("amtCalFinaData");
			Map<String, Double> amtCalResult = (Map<String, Double>) calResult.get("amtCalResult");
			
			InputStream inputStream = ApplyInfoCtl.class.getResourceAsStream("/template/working_capital.xls");
			Workbook workbook = new HSSFWorkbook(inputStream);
			Sheet sheet = workbook.getSheetAt(0);
			
			Row row1 = sheet.getRow(1);
			Cell cell0 = row1.getCell(0);
			cell0.setCellValue(task.getCustomerName());
			
			Row row2 = sheet.getRow(2);
			for(int i=0; i<dateListShow.size(); i++){
				Cell cell = row2.getCell(i+1);
				cell.setCellValue(dateListShow.get(i));
			}
			
			int rowIndex = 3;
			for(int i=0; i<finaSubjects.size(); i++){
				TableSubjCode subject = finaSubjects.get(i);
				Row row = sheet.getRow(rowIndex++);
				for(int j=0; j<dateListShow.size(); j++){
					Cell cell = row.getCell(j+1);
					
					Double value = amtCalFinaData.get(j).get(subject.getSubjCode());
					if(value != null){
						BigDecimal bd = new BigDecimal(value);
						cell.setCellValue(bd.setScale(2, RoundingMode.HALF_UP).doubleValue());
					}
				}
			}
			
			int lastYear = task.getMonth()==12?task.getYear():task.getYear()-1;
			String[] array = getAmtCalQuota(lastYear);
			
			for(int i=0; i<array.length; i++){
				
				String[] temp = array[i].split("\\|");
				String key = temp[0];
				
				Cell cellName, cellValue;
				if(i%2 == 0){
					Row row = sheet.getRow(++rowIndex);
					cellName = row.getCell(0);
					cellValue = row.getCell(1);
				}else{
					Row row = sheet.getRow(rowIndex);
					cellName = row.getCell(2);
					cellValue = row.getCell(4);
				}
				
				int scale = 2;
				if("incomeGrowthRate,annualProfitRate".contains(key)){
					scale = 4;
					if("annualProfitRate".equals(key))
						cellName.setCellValue(temp[1]);
				}
				
				Double value = amtCalResult.get(key);
				if(value != null){
					BigDecimal bd = new BigDecimal(value);
					value = bd.setScale(scale, RoundingMode.HALF_UP).doubleValue();
					cellValue.setCellValue(value);
				}
				
			}
			
			String path = PropertiesUtil.getProperty(Constants.SYSTEM_PROPS_PATH,
					"data.temp.path");// 文件缓存地址
			File dir = new File(path);
			if(!dir.exists())
				dir.mkdirs();
			File file = new File(dir, task.getSerNo() + "_ED.xls");
			fos = new FileOutputStream(file);
			workbook.write(fos);
			
			json.put("serNo", task.getSerNo());
			json.put("result", true);
		} catch (Exception e) {
			json.put("msg", "生成流动资金需求测算excel文件出错！");
			log.error("生成流动资金需求测算excel文件异常：", e);
		} finally{
			if(fos != null){
				try {
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return jRequest.serialize(json, true);
	}
	
	private String[] getAmtCalQuota(int lastYear){
		String[] arr = {
				"receiveTimes|应收账款周转次数",
				"receiveDays|应收账款周转天数",
				"prePayTimes|预付账款周转次数",
				"prePayDays|预付账款周转天数",
				"inventoryTimes|存货周转次数",
				"inventoryDays|存货周转天数",
				"payTimes|应付账款周转次数",
				"payDays|应付账款周转天数",
				"preReceiveTimes|预收账款周转次数",
				"preReceiveDays|预收账款周转天数",
				"workCapitalTimes|营运资金周转次数",
				"annualProfitRate|" + lastYear + "年度销售利润率",
				"incomeGrowthRate|本年度销售收入预计增长率",
				"workLiquidityCapital|营运资金量",
				"otherChannelCapital|借款人从其他渠道获得的资金",
				"ownLiquidityCapital|自有流动资金",
				"newCreditLimit|新增需求授信额度",
				"newRiskCreditLimit|风险控制新增贷款额度"
		};
		return arr;
	}
	
}
