package com.beawan.survey.loanInfo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.beawan.survey.custInfo.dto.CompBaseDto;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.analysis.finansis.common.FinaRowData;
import com.beawan.analysis.finansis.service.IFinanasisService;
import com.beawan.base.service.ISysDicSV;
import com.beawan.common.SysConstants;
import com.beawan.common.SysConstants.ExceptionConstant;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.common.util.SystemUtil;
import com.beawan.customer.utils.CustomerUtil;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.beawan.survey.custInfo.bean.CompBase;
import com.beawan.survey.custInfo.bean.PersonBase;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.beawan.survey.custInfo.service.ICompFinanceSV;
import com.beawan.survey.loanInfo.bean.CounterGuaraCom;
import com.beawan.survey.loanInfo.bean.CounterGuaraMort;
import com.beawan.survey.loanInfo.bean.GuaraCom;
import com.beawan.survey.loanInfo.bean.GuaraPerson;
import com.beawan.survey.loanInfo.bean.GuaraProCom;
import com.beawan.survey.loanInfo.bean.GuaranteeInfo;
import com.beawan.survey.loanInfo.bean.Guarantor;
import com.beawan.survey.loanInfo.bean.MortgageInfo;
import com.beawan.survey.loanInfo.bean.PledgeInfo;
import com.beawan.survey.loanInfo.service.IGuaraInfoSV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;


/**
 * @ClassName GuaraInfoCtl
 * @Description 担保分析信息控制器
 * @author rain
 * @Date 2018年1月22日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/survey/loan" })
public class GuaraInfoCtl {
	
	private static final Logger log = Logger.getLogger(GuaraInfoCtl.class);
	
	@Resource
	protected ISysDicSV sysDicSV;
	
	@Resource
	protected ITaskSV taskSV;

	@Resource
	private IGuaraInfoSV guaraInfoSV;
	
	@Resource
	private ICompBaseSV compBaseSV;
	
	@Resource
	private ICompFinanceSV compFinanceSV;
	
	@Resource
	private ICmisInInvokeSV cmisInInvokeSV;
	
	@Resource
	private IFinanasisService finanasisSV;
	
	/**
	 * @Description (担保分析页面跳转)
	 * @param request
	 * @param response
	 * @param taskId 任务ID
	 * @return
	 */
	@RequestMapping("guaranteeInfo.do")
	public String guaranteeInfo(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		
		try {
			
			Task task = this.taskSV.getTaskById(taskId);
			request.setAttribute("task", task);
			
			GuaranteeInfo  guaraInfo = guaraInfoSV.findGuaraInfoBytaskId(taskId);
			
			if(guaraInfo == null ){
				guaraInfo = new GuaranteeInfo();
				guaraInfo.setTaskId(taskId);
			}
			request.setAttribute("guaraInfo", guaraInfo);
			
			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.STD_SY_GUAR_TYPE,
					SysConstants.BsDicConstant.STD_GUAR_FORM,
					SysConstants.BsDicConstant.STD_SY_GUARANTOR_TYPE,
					SysConstants.BsDicRateConstant.STD_RATE_GUR,
					SysConstants.BsDicRateConstant.STD_RATE_POL,
					SysConstants.BsDicRateConstant.STD_RATE_MOR,};
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/survey/common/loanGuaraInfo";
	}
	
	/**
	 * @Description (保存担保分析信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveGuaranteeInfo.do")
	@ResponseBody
	public String saveGuaranteeInfo(HttpServletRequest request, HttpServletResponse response,
			String jsondata) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			guaraInfoSV.saveGuaraInfo(JacksonUtil.fromJson(jsondata, GuaranteeInfo.class));
			
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (获取保证人列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getGuarantor.do")
	@ResponseBody
	public String getGuarantor(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<Guarantor> dataGrid = null;
		
		try {
			dataGrid = guaraInfoSV.findGuarantorByTaskId(taskId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return jRequest.serialize(dataGrid, true);
	}
	
	/**
	 * @Description (保存保证人信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveGuarantor.do")
	@ResponseBody
	public String saveGuarantor(HttpServletRequest request, HttpServletResponse response,
			String jsondata) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		String finaRepYear = request.getParameter("finaRepYear");
		
		try {
			
			Guarantor guarantor = guaraInfoSV.saveGuarantor(jsondata);
			if(!StringUtil.isEmptyString(finaRepYear)){
				GuaraCom guaraCom = guaraInfoSV.findGuaraComById(guarantor.getId());
				if(guaraCom == null){
					guaraCom = new GuaraCom();
					guaraCom.setGuaraId(guarantor.getId());
				}
				guaraCom.setFinaRepYear(finaRepYear);
				guaraInfoSV.saveGuaraCom(guaraCom);
			}
			
			json.put("guaraId", guarantor.getId());
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (删除保证人信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("deleteGuarantor.do")
	@ResponseBody
	public String deleteGuarantor(HttpServletRequest request, HttpServletResponse response,
			long id) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			guaraInfoSV.deleteGuarantorById(id);
			
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (获取抵押物信息列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getMortgageInfo.do")
	@ResponseBody
	public String getMortgageInfo(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<MortgageInfo> dataGrid = null;
		
		try {
			dataGrid = guaraInfoSV.findMortgageInfoByTaskId(taskId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return jRequest.serialize(dataGrid, true);
	}
	
	/**
	 * @Description (保存抵押物信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveMortgageInfo.do")
	@ResponseBody
	public String saveMortgageInfo(HttpServletRequest request, HttpServletResponse response,
			String jsondata) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			guaraInfoSV.saveMortgageInfo(JacksonUtil.fromJson(jsondata, MortgageInfo.class));
			
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (删除抵押物信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("deleteMortgageInfo.do")
	@ResponseBody
	public String deleteMortgageInfo(HttpServletRequest request, HttpServletResponse response,
			long id) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			guaraInfoSV.deleteMortgageInfoById(id);
			
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (获取抵押物信息列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getPledgeInfo.do")
	@ResponseBody
	public String getPledgeInfo(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<PledgeInfo> dataGrid = null;
		
		try {
			dataGrid = guaraInfoSV.findPledgeInfoByTaskId(taskId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return jRequest.serialize(dataGrid, true);
	}
	
	/**
	 * @Description (保存质押物信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("savePledgeInfo.do")
	@ResponseBody
	public String savePledgeInfo(HttpServletRequest request, HttpServletResponse response,
			String jsondata) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			guaraInfoSV.savePledgeInfo(JacksonUtil.fromJson(jsondata, PledgeInfo.class));
			
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (删除质押物信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("deletePledgeInfo.do")
	@ResponseBody
	public String deletePledgeInfo(HttpServletRequest request, HttpServletResponse response,
			long id) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			guaraInfoSV.deletePledgeInfoById(id);
			
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description 一般公司担保页面跳转
	 * @param request
	 * @param response
	 * @param guaraId 保证人ID
	 * @return
	 */
	@RequestMapping("guaraCom.do")
	public String guaraCom(HttpServletRequest request, HttpServletResponse response,
			long guaraId) {
		
		try {
			
			Guarantor guarantor = guaraInfoSV.findGuarantorById(guaraId);
			
			request.setAttribute("taskId", guarantor.getTaskId());
			request.setAttribute("customerNo", guarantor.getCusNo());
			request.setAttribute("customerName", guarantor.getCusName());
			request.setAttribute("guaraId", guarantor.getId());
			
			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.STD_SY_GUAR_TYPE,
					SysConstants.BsDicConstant.STD_GUAR_FORM,
					SysConstants.BsDicConstant.STD_SY_GUARANTOR_TYPE};
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
			
			CustomerUtil.asynCusFSFromCmis(guarantor.getCusNo(), 0, 0);
			
		} catch (Exception e) {
			log.error("跳转一般公司担保页面异常：", e);
		}
		
		return "views/survey/common/loanGuaraComIndex";
	}
	
	/**
	 * @Description 一般公司担保-担保信息页面跳转
	 * @param request
	 * @param response
	 * @param guaraId 保证人ID
	 * @return
	 */
	@RequestMapping("guaraComInfo.do")
	public String guaraComInfo(HttpServletRequest request, HttpServletResponse response,
			long guaraId) {
		
		try {
			
			Guarantor guarantor = guaraInfoSV.findGuarantorById(guaraId);
			GuaraCom guaraCom = guaraInfoSV.findGuaraComById(guarantor.getId());
			
			request.setAttribute("guarantor", guarantor);
			request.setAttribute("guaraCom", guaraCom);
			
			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.STD_SY_GUAR_TYPE,
					SysConstants.BsDicConstant.STD_GUAR_FORM,
					SysConstants.BsDicConstant.STD_SY_GUARANTOR_TYPE};
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/survey/common/loanGuaraComInfo";
	}
	
	/**
	 * @Description 一般公司担保-经营信息页面跳转
	 * @param request
	 * @param response
	 * @param guaraId 保证人ID
	 * @return
	 */
	@RequestMapping("guaraComRun.do")
	public String guaraComRun(HttpServletRequest request, HttpServletResponse response,
			long guaraId) {
		
		try {
			
			GuaraCom guaraCom = guaraInfoSV.findGuaraComById(guaraId);
			
			request.setAttribute("guaraId", guaraId);
			request.setAttribute("guaraCom", guaraCom);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/survey/common/loanGuaraComRun";
	}
	
	/**
	 * @Description 一般公司担保-融资及对外担保页面跳转
	 * @param request
	 * @param response
	 * @param guaraId 保证人ID
	 * @return
	 */
	@RequestMapping("guaraComFinancing.do")
	public String guaraComFinancing(HttpServletRequest request, HttpServletResponse response,
			long guaraId) {
		
		try {
			
			GuaraCom guaraCom = guaraInfoSV.findGuaraComById(guaraId);
			
			request.setAttribute("guaraId", guaraId);
			request.setAttribute("guaraCom", guaraCom);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/survey/common/loanGuaraComFinancing";
	}
	
	/**
	 * @Description (保存一般公司担保信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveGuaraCom.do")
	@ResponseBody
	public String saveGuaraCom(HttpServletRequest request, HttpServletResponse response,
			String jsondata) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			
			guaraInfoSV.saveGuaraCom(jsondata);
			
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (跳转页面请求，跳转到财务分析页面)
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("guaraComFinaAnaly.do")
	public String guaraComFinaAnaly(HttpServletRequest request, HttpServletResponse response,
			long guaraId) {
		
		try {
			
			GuaraCom guaraCom = guaraInfoSV.findGuaraComById(guaraId);
			request.setAttribute("guaraCom", guaraCom);
			
			String[] dateArr = guaraCom.getFinaRepYear().split("-");
			int year = Integer.parseInt(dateArr[0]);
			int month = Integer.parseInt(dateArr[1]);
			
			List<String> dateList = SystemUtil.genFinaDateList(year, month, true, false);
			List<String> dateListShow = SystemUtil.genFinaDateList(year, month, true, true);
			request.setAttribute("dateList", JacksonUtil.serialize(dateListShow));
			
			CompBaseDto compBase = compBaseSV.findCompBaseByCustNo(guaraCom.getGuarantor().getCusNo());
			
			//比率指标相关信息
			List<FinaRowData> finaRowData = compFinanceSV.findFinanceIndexs(
					compBase.getCustomerNo(), dateList, compBase.getIndustryCode());
			request.setAttribute("finaRowData", JacksonUtil.serialize(finaRowData));
			
			String ratioAnalysis = guaraCom.getRatioAnalysis();
			if(StringUtil.isEmptyString(ratioAnalysis)){
				//获得系统分析结论
				Map<String, Object> conclusion = finanasisSV.genAnalysisConclusion(
						guaraCom.getGuarantor().getTaskId(),
						guaraCom.getGuarantor().getCusNo(), 
						SysConstants.SysFinaAnalyPart.RATIO_INDEX);
				ratioAnalysis = "盈利能力：" + conclusion.get("RatioOne").toString()
						      + "偿债能力：" + conclusion.get("RatioTwo").toString()
						      + "运营能力：" + conclusion.get("RatioThree").toString()
						      + "增长能力：" + conclusion.get("RatioFour").toString();
				
				guaraCom.setRatioAnalysis(ratioAnalysis);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/survey/common/loanGuaraComFinaIndex";
	}
	
	/**
	 * @Description (专业担保公司页面跳转)
	 * @param request
	 * @param response
	 * @param guaraId 任务ID
	 * @return
	 */
	@RequestMapping("guaraProCom.do")
	public String guaraProCom(HttpServletRequest request, HttpServletResponse response,
			long guaraId) {
		
		try {
			
			Guarantor guarantor = guaraInfoSV.findGuarantorById(guaraId);
			GuaraProCom guaraProCom = guaraInfoSV.findGuaraProComById(guarantor.getId());
			
			request.setAttribute("guarantor", guarantor);
			request.setAttribute("guaraProCom", guaraProCom);
			
			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.STD_SY_GUAR_TYPE,
					SysConstants.BsDicConstant.STD_GUAR_FORM,
					SysConstants.BsDicConstant.STD_SY_GUARANTOR_TYPE,
					SysConstants.BsDicConstant.STD_ZX_YES_NO};
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/survey/common/loanGuaraProCom";
	}
	
	/**
	 * @Description (保存担保公司担保信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveGuaraProCom.do")
	@ResponseBody
	public String saveGuaraProCom(HttpServletRequest request, HttpServletResponse response,
			String jsondata) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			
			guaraInfoSV.saveGuaraProCom(jsondata);
			
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (获取担保公司反担保企业列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getCounterGuaraCom.do")
	@ResponseBody
	public String getCounterGuaraCom(HttpServletRequest request, HttpServletResponse response,
			long guaraId) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<CounterGuaraCom> dataGrid = null;
		
		try {
			dataGrid = guaraInfoSV.findCounterGuaraComByGuaraId(guaraId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return jRequest.serialize(dataGrid, true);
	}
	
	/**
	 * @Description (保存担保公司反担保企业信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveCounterGuaraCom.do")
	@ResponseBody
	public String saveCounterGuaraCom(HttpServletRequest request, HttpServletResponse response,
			String jsondata) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			
			guaraInfoSV.saveCounterGuaraCom(JacksonUtil.fromJson(jsondata, CounterGuaraCom.class));
			
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (删除担保公司反担保企业信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("deleteCounterGuaraCom.do")
	@ResponseBody
	public String deleteCounterGuaraCom(HttpServletRequest request, HttpServletResponse response,
			long id) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			guaraInfoSV.deleteCounterGuaraComById(id);
			
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (获取担保公司反担保抵押物列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getCounterGuaraMort.do")
	@ResponseBody
	public String getCounterGuaraMort(HttpServletRequest request, HttpServletResponse response,
			long guaraId) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<CounterGuaraMort> dataGrid = null;
		
		try {
			dataGrid = guaraInfoSV.findCounterGuaraMortByGuaraId(guaraId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return jRequest.serialize(dataGrid, true);
	}
	
	/**
	 * @Description (保存担保公司反担保抵押物信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveCounterGuaraMort.do")
	@ResponseBody
	public String saveCounterGuaraMort(HttpServletRequest request, HttpServletResponse response,
			String jsondata) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			guaraInfoSV.saveCounterGuaraMort(JacksonUtil.fromJson(jsondata, CounterGuaraMort.class));
			
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (删除担保公司反担保抵押物信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("deleteCounterGuaraMort.do")
	@ResponseBody
	public String deleteCounterGuaraMort(HttpServletRequest request, HttpServletResponse response,
			long id) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			guaraInfoSV.deleteCounterGuaraMortById(id);
			
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (跳转自然人担保页面)
	 * @param request
	 * @param response
	 * @param guaraId 保证人ID
	 * @return
	 */
	@RequestMapping("guaraPerson.do")
	public String guaraPerson(HttpServletRequest request, HttpServletResponse response,
			long guaraId) {
		
		try {
			
			GuaraPerson guaraPerson = guaraInfoSV.findGuaraPersonById(guaraId);
			if(guaraPerson == null){
				guaraPerson = new GuaraPerson();
				guaraPerson.setGuarantor(guaraInfoSV.findGuarantorById(guaraId));
			}
				
			PersonBase personBase = compBaseSV.findPersonBaseByCustNo(
					guaraPerson.getGuarantor().getCusNo());
			guaraPerson.setPersonBase(personBase);
			request.setAttribute("guaraPerson", JacksonUtil.serialize(guaraPerson));
			
			//加载字典数据
			String[] optTypes = {
					SysConstants.BsDicConstant.STD_SY_JB00035,
					SysConstants.BsDicConstant.STD_SY_GB00009,
					SysConstants.BsDicConstant.STD_SY_GB00005, 
					SysConstants.BsDicConstant.STD_SY_GB00002,
					SysConstants.BsDicConstant.STD_SY_ODS0004,
					SysConstants.BsDicConstant.STD_SY_GB00003,
					SysConstants.BsDicConstant.STD_GUAR_FORM};
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
			
		} catch (Exception e) {
			log.error("跳转自然人担保页面异常", e);
		}
		
		return "views/survey/common/loanGuaraPerson";
	}
	
	/**
	 * @Description (保存自然人担保信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveGuaraPerson.do")
	@ResponseBody
	public String saveGuaraPerson(HttpServletRequest request, HttpServletResponse response,
			String guaraPerson) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			
			JSONObject jsondata = JSONObject.fromObject(guaraPerson);
			
			guaraInfoSV.saveGuarantor(jsondata.getString("guarantor"));
			jsondata.remove("guarantor");
			compBaseSV.savePersonBase(jsondata.getString("personBase"));
			jsondata.remove("personBase");
			
			guaraInfoSV.saveGuaraPerson(jsondata.toString());
			
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", "保存自然人担保信息异常！");
			log.error("保存自然人担保信息异常", e);
		}
		
		return jRequest.serialize(json, true);
	}
	
}
