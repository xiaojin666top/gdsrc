package com.beawan.survey.loanInfo.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.base.entity.User;
import com.beawan.common.Constants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.survey.loanInfo.bean.BenefitAnaly;
import com.beawan.survey.loanInfo.bean.BenefitForecast;
import com.beawan.survey.loanInfo.service.BenefitForecastService;
import com.beawan.survey.loanInfo.service.BenefitAnalyService;
import com.platform.util.DateUtil;
import com.platform.util.GsonUtil;
import com.platform.util.HttpUtil;


/**
 * 针对项目类贷款的 效益测算和还款来源分析
 * @author User
 *
 */

@Controller
@UserSessionAnnotation
@RequestMapping({ "/survey/loan" })
public class BenefitCtl extends BaseController{


	@Resource
	private BenefitForecastService benefitForecastService;
	@Resource
	private BenefitAnalyService benefitAnalyService;

	/**
	 * 效益预测  页面跳转
	 * 获取申请利率信息
	 * @param taskId
	 * @return
	 */
	@RequestMapping("benefitForecast.do")
	public ModelAndView benefitForecast(long taskId) {
		ModelAndView mav = new ModelAndView("views/survey/common/benefitForecast");
		mav.addObject("taskId", taskId);
		try {
			List<String> colsList = new ArrayList();
			List<Map<String,Object>> dataList = new ArrayList<>();
			//动态生成表头
			BenefitForecast benefitForecast = benefitForecastService
					.selectSingleByProperty("taskId", taskId);
			
			BenefitAnaly analy = benefitAnalyService.selectSingleByProperty("taskId", taskId);
			mav.addObject("analy", analy);
			
			Integer startYear = null;
			if(benefitForecast==null){
				startYear = DateUtil.getYear(DateUtil.addYear(new Date(), 0));
				String[] keys = Constants.BENEFIT.KEYS;
				String[] newCols = Arrays.copyOfRange(keys, 0, 5);
//				colsList.add(Constants.BENEFIT.ITEM);
				Map<String,Object> map = new HashMap<>();
				map.put(Constants.BENEFIT.ITEM, "");
				for(String cs : newCols){
					colsList.add(cs);
					map.put(cs, "");
				}
				dataList.add(map);
				mav.addObject("startYear", startYear);
				mav.addObject("yearLength", 5);
				mav.addObject("colsList", colsList);
				mav.addObject("colsListStr", GsonUtil.GsonString(colsList));
				mav.addObject("dataList", GsonUtil.GsonString(dataList));
				
				benefitForecast = new BenefitForecast();
				benefitForecast.setTaskId(taskId);
				benefitForecast.setStartYear(startYear);
				benefitForecast.setYearLength(5);
				benefitForecast.setTableCols(GsonUtil.GsonString(colsList));
				benefitForecast.setTableData(GsonUtil.GsonString(dataList));
				benefitForecastService.saveOrUpdate(benefitForecast);
				
				return mav;
			}
			
			startYear = benefitForecast.getStartYear();
			Integer yearLength = benefitForecast.getYearLength();
			
			//动态展示数据
			String cols = benefitForecast.getTableCols();
			String tableData = benefitForecast.getTableData();
			colsList = GsonUtil.GsonToList(cols, String.class);
//			dataList = GsonUtil.GsonToListMaps(tableData);
			mav.addObject("startYear", startYear);
			mav.addObject("yearLength", yearLength);
			mav.addObject("colsList", colsList);
			mav.addObject("colsListStr", GsonUtil.GsonString(colsList));
			mav.addObject("dataList", tableData);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
	
	/**
	 * 保存或变更 效益测算信息
	 * @param benefitData
	 * @param request
	 * @return
	 */
	@RequestMapping("saveBenefitforecast.json")
	@ResponseBody
	public ResultDto saveBenefitforecast(BenefitForecast benefitData, HttpServletRequest request) {
		ResultDto re = returnFail("");
		try {
			BenefitForecast benefitForecast = benefitForecastService
					.selectSingleByProperty("taskId", benefitData.getTaskId());
			if(benefitForecast!=null) {
				benefitForecast.setStatus(Constants.DELETE);
				benefitForecastService.saveOrUpdate(benefitForecast);
			}
			benefitForecastService.saveOrUpdate(benefitData);
			re = returnSuccess();
		}catch(Exception e){
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 变更效益测算开始时间和时长
	 * 并将相同年份的数据同步过来
	 * @param newStartYear
	 * @param taskId
	 * @param newYearLength
	 * @param request
	 * @return
	 */
	@RequestMapping("changeBenefitforecast.json")
	@ResponseBody
	public ResultDto changeBenefitforecast(Integer newStartYear, 
			Long taskId,
			Integer newYearLength, 
			HttpServletRequest request) {
		ResultDto re = returnFail("");
		if(newYearLength==null || newYearLength <0 || newYearLength >=15){
			re.setMsg("预测年份请设置在15年以内");
			return re;
		}
		try {
			//数据库中原始对象
			BenefitForecast benefitForecast = benefitForecastService
					.selectSingleByProperty("taskId", taskId);
			List<String> colsList = GsonUtil.GsonToList(benefitForecast.getTableCols(), String.class);
			int index = Integer.parseInt(colsList.get(0).substring(3)) - 1;
			Integer startYear = benefitForecast.getStartYear();
			int diff = newStartYear-startYear;
			
			String[] keys = Constants.BENEFIT.KEYS;
			String[] newCols = Arrays.copyOfRange(keys, index + diff, index + newYearLength + diff);
			
			List<Map<String, Object>> dataList = GsonUtil.GsonToListMaps(benefitForecast.getTableData());
			
			//将原先填的数据进行转移
			List<Map<String, Object>> newListData = new ArrayList<>();
			for(Map<String, Object> map : dataList){
				Map<String, Object> newMap = new HashMap<>();
				newMap.put(Constants.BENEFIT.ITEM, map.get(Constants.BENEFIT.ITEM));
				for(String newCol : newCols){
					if(map.containsKey(newCol)){
						newMap.put(newCol, map.get(newCol));
					}
				}
				newListData.add(newMap);
			}
			BenefitForecast newbenefitForecast = new BenefitForecast();
			newbenefitForecast.setTableData(GsonUtil.GsonString(newListData));
			newbenefitForecast.setTaskId(taskId);
			newbenefitForecast.setTableCols(GsonUtil.GsonString(newCols));
			newbenefitForecast.setStartYear(newStartYear);
			newbenefitForecast.setYearLength(newYearLength);
			benefitForecastService.saveOrUpdate(newbenefitForecast);
			
			benefitForecast.setStatus(Constants.DELETE);
			benefitForecastService.saveOrUpdate(benefitForecast);
			re = returnSuccess();
		}catch(Exception e){
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 保存效益预测分析
	 * @param benefitId
	 * @param benefitAnaly
	 * @param taskId
	 * @param request
	 * @return
	 */
	@RequestMapping("saveBenefitAnaly.json")
	@ResponseBody
	public ResultDto saveBenefitAnaly(Integer benefitId, String benefitAnaly
			, Long taskId, HttpServletRequest request) {
		ResultDto re = returnFail("保存失败");
		User user = HttpUtil.getCurrentUser(request);
		try{
			BenefitAnaly benefit = benefitAnalyService.selectSingleByProperty("taskId", taskId);
			if(benefit == null){
				benefit = new BenefitAnaly();
				benefit.setTaskId(taskId);
				benefit.setCreater(user.getUserId());
				benefit.setCreateTime(DateUtil.getNowTimestamp());
			}
			benefit.setBenefitAnaly(benefitAnaly);
			benefit.setUpdater(user.getUserId());
			benefit.setUpdateTime(DateUtil.getNowTimestamp());
			benefitAnalyService.saveOrUpdate(benefit);
			
			re = returnSuccess();
		}catch(Exception e){
			e.printStackTrace();
		}
		return re;
	}
	
	

	/**
	 * 保存还款来源分析、还款计划
	 * @param benefitId
	 * @param benefitAnaly
	 * @param taskId
	 * @param request
	 * @return
	 */
	@RequestMapping("savePaySource.json")
	@ResponseBody
	public ResultDto savePaySource(Integer benefitId, String firstPaySource, String secondPaySource, String payPlan
			, Long taskId, HttpServletRequest request) {
		ResultDto re = returnFail("保存失败");
		User user = HttpUtil.getCurrentUser(request);
		try{

			BenefitAnaly benefit = benefitAnalyService.selectSingleByProperty("taskId", taskId);
			if(benefit == null){
				benefit = new BenefitAnaly();
				benefit.setTaskId(taskId);
				benefit.setCreater(user.getUserId());
				benefit.setCreateTime(DateUtil.getNowTimestamp());
			}
			benefit.setFirstPaySource(firstPaySource);
			benefit.setSecondPaySource(secondPaySource);
			benefit.setPayPlan(payPlan);
			benefit.setUpdater(user.getUserId());
			benefit.setUpdateTime(DateUtil.getNowTimestamp());
			benefitAnalyService.saveOrUpdate(benefit);
			re = returnSuccess();
		}catch(Exception e){
			e.printStackTrace();
		}
		return re;
	}
	 
	
	/**
	 * 还款来源和还款计划
	 * @param taskId
	 * @return
	 */
	@RequestMapping("paySourceAndPlan.do")
	public ModelAndView paySourceAndPlan(long taskId) {
		ModelAndView mav = new ModelAndView("views/survey/common/benefitPaySource");
		BenefitAnaly analy = benefitAnalyService.selectSingleByProperty("taskId", taskId);
		mav.addObject("analy", analy);
		mav.addObject("taskId", taskId);
		return mav;
	}
}
