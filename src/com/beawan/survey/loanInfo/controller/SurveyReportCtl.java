package com.beawan.survey.loanInfo.controller;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.analysis.finansis.utils.CodeUtil;
import com.beawan.common.Constants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.survey.loanInfo.service.ISurveyReportSV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.HttpUtil;
import com.platform.util.PropertiesUtil;
import com.platform.util.StringUtil;

/**
 * 调查报告web层
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/survey/report/" })
public class SurveyReportCtl {

	private static final Logger log = Logger.getLogger(SurveyReportCtl.class);

	@Resource
	private ISurveyReportSV surveyReportSV;
	@Resource
	private ITaskSV taskSV;

	/**
	 * @Description 生成调查报告
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("createReport.json")
	@ResponseBody
	public String createReport(HttpServletRequest request, HttpServletResponse response) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);
		
		try {
			
			long taskId = HttpUtil.getAsLong(request, "taskId");
			surveyReportSV.createSurveyReport(taskId);
			
			json.put("result", true);
		} catch (Exception e) {
			log.error("生成授信调查报告异常：", e);
		}
		
		return jRequest.serialize(json, true);
	}

	/**
	 * @Description 从服务器上下载报告
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("getReport.do")
	public ResponseEntity<byte[]> getReport(HttpServletRequest request, HttpServletResponse response) {
		
		ResponseEntity<byte[]> result = null;
		
		try {
			
			Long taskId  = HttpUtil.getAsLong(request, "taskId");
			String previewFlag = HttpUtil.getAsString(request, "preview");
					
			Task task = taskSV.getTaskById(taskId);
			
			String paraDir = PropertiesUtil.getProperty(Constants.SYSTEM_PROPS_PATH, "survey.report.path");// 文件缓存地址
			
			String para = paraDir + "/" + task.getSerNo() + ".doc";
			File file = new File(para);
			if(!file.exists())
				surveyReportSV.createSurveyReport(taskId);
			
			String fileNameTail = "授信调查报告";
			if(!StringUtil.isEmpty(previewFlag))
				fileNameTail += "（预览）";
			
			String namefinal = CodeUtil.parseGBK(task.getCustomerName() + fileNameTail);
			String fileName = new String(namefinal + ".doc");
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentDispositionFormData("attachment", fileName);
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			result = new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file),
					headers, HttpStatus.OK);
			
		} catch (Exception e) {
			log.error("授信调查报告下载异常", e);
		}
		
		return result;
	}

}
