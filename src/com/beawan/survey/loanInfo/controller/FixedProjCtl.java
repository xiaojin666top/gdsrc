package com.beawan.survey.loanInfo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.base.service.ISysDicSV;
import com.beawan.common.SysConstants;
import com.beawan.common.SysConstants.ExceptionConstant;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.survey.loanInfo.bean.FixedProjBase;
import com.beawan.survey.loanInfo.bean.FixedProjCashFow;
import com.beawan.survey.loanInfo.bean.FixedProjFinaPlan;
import com.beawan.survey.loanInfo.bean.FixedProjInvest;
import com.beawan.survey.loanInfo.bean.FixedProjInvestEst;
import com.beawan.survey.loanInfo.bean.FixedProjPlan;
import com.beawan.survey.loanInfo.service.IFixedProjSV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.JacksonUtil;


/**
 * @ClassName FixedProjCtl
 * @Description 固定资产贷款项目信息控制器
 * @author rain
 * @Date 2018年1月22日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/survey/fixed" })
public class FixedProjCtl {
	
	@Resource
	protected ISysDicSV sysDicSV;
	
	@Resource
	protected ITaskSV taskSV;

	@Resource
	private IFixedProjSV fixedProjSV;
	
	/**
	 * @Description (固定资产贷款项目基本信息页面跳转)
	 * @param request
	 * @param response
	 * @param taskId 任务ID
	 * @return
	 */
	@RequestMapping("projBaseInfo.do")
	public String projBaseInfo(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		
		try {
			
			Task task = this.taskSV.getTaskById(taskId);
			request.setAttribute("task", task);
			
			FixedProjBase projBase = fixedProjSV.queryProjBaseByTaskId(taskId);
			
			if(projBase == null ){
				projBase = new FixedProjBase();
				projBase.setTaskId(taskId);
			}
			request.setAttribute("projBase", projBase);
			
			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.STD_ZB_FIXED_PROJ_TYPE};
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/survey/fixedassets/fixedProjBase";
	}
	
	/**
	 * @Description (保存固定资产贷款项目基本信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveFixedProjBase.do")
	@ResponseBody
	public String saveFixedProjBase(HttpServletRequest request, HttpServletResponse response,
			String jsondata) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			
			fixedProjSV.saveProjBase(jsondata);
			
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (固定资产贷款项目进展情况页面跳转)
	 * @param request
	 * @param response
	 * @param taskId 任务ID
	 * @return
	 */
	@RequestMapping("projProgress.do")
	public String projProgress(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		
		try {
			
			Task task = this.taskSV.getTaskById(taskId);
			request.setAttribute("task", task);
			
			FixedProjBase projBase = fixedProjSV.queryProjBaseByTaskId(taskId);
			
			if(projBase == null ){
				projBase = new FixedProjBase();
				projBase.setTaskId(taskId);
			}
			request.setAttribute("projBase", projBase);
			
			List<FixedProjInvest> projInvests = fixedProjSV.queryProjInvestByTaskId(taskId);
			request.setAttribute("projInvests", JacksonUtil.serialize(projInvests));
			
			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.STD_FIXED_INVEST};
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/survey/fixedassets/fixedProjProgress";
	}
	
	/**
	 * @Description (获取项目进度计划列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getProjPlan.do")
	@ResponseBody
	public String getProjPlan(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<FixedProjPlan> dataGrid = null;
		
		try {
			dataGrid = fixedProjSV.queryProjPlanByTaskId(taskId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return jRequest.serialize(dataGrid, true);
	}
	
	/**
	 * @Description (保存项目进度计划列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveProjPlans.do")
	@ResponseBody
	public String saveProjPlans(HttpServletRequest request, HttpServletResponse response,
			long taskId, String jsonArray) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			fixedProjSV.saveProjPlanList(taskId, jsonArray);
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (保存项目建设投资资金使用情况表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveProjInvests.do")
	@ResponseBody
	public String saveProjInvests(HttpServletRequest request, HttpServletResponse response,
			long taskId, String jsonArray) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			fixedProjSV.saveProjInvestList(taskId, jsonArray);
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (固定资产贷款项目投资及筹资情况页面跳转)
	 * @param request
	 * @param response
	 * @param taskId 任务ID
	 * @return
	 */
	@RequestMapping("projInvestAndFina.do")
	public String projInvestAndFina(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		
		try {
			
			Task task = this.taskSV.getTaskById(taskId);
			request.setAttribute("task", task);
			
			FixedProjBase projBase = fixedProjSV.queryProjBaseByTaskId(taskId);
			
			if(projBase == null ){
				projBase = new FixedProjBase();
				projBase.setTaskId(taskId);
			}
			request.setAttribute("projBase", projBase);
			
			List<FixedProjInvestEst> investEsts = fixedProjSV.queryProjInvestEstByTaskId(taskId);
			request.setAttribute("investEsts", JacksonUtil.serialize(investEsts));
			
			List<FixedProjFinaPlan> finaPlans = fixedProjSV.queryProjFinaPlanByTaskId(taskId);
			request.setAttribute("finaPlans", JacksonUtil.serialize(finaPlans));
			
			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.STD_FIXED_INVESTEST,
					SysConstants.BsDicConstant.STD_FINAPLAN_TYPE};
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/survey/fixedassets/fixedProjInvestAndFina";
	}
	
	/**
	 * @Description (保存项目投资估算表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveProjInvestEsts.do")
	@ResponseBody
	public String saveProjInvestEsts(HttpServletRequest request, HttpServletResponse response,
			long taskId, String jsonArray) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			fixedProjSV.saveProjInvestEstList(taskId, jsonArray);
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (保存项目筹资计划表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveProjFinaPlans.do")
	@ResponseBody
	public String saveProjFinaPlans(HttpServletRequest request, HttpServletResponse response,
			long taskId, String jsonArray) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			fixedProjSV.saveProjFinaPlanList(taskId, jsonArray);
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (固定资产贷款项目偿债能力分析页面跳转)
	 * @param request
	 * @param response
	 * @param taskId 任务ID
	 * @return
	 */
	@RequestMapping("projSovency.do")
	public String projSovency(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		
		try {
			
			Task task = this.taskSV.getTaskById(taskId);
			request.setAttribute("task", task);
			
			FixedProjBase projBase = fixedProjSV.queryProjBaseByTaskId(taskId);
			
			if(projBase == null ){
				projBase = new FixedProjBase();
				projBase.setTaskId(taskId);
			}
			request.setAttribute("projBase", projBase);
			
			List<FixedProjCashFow> cashFlows = fixedProjSV.queryProjCashFowByTaskId(taskId);
			request.setAttribute("cashFlows", JacksonUtil.serialize(cashFlows));
			
			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.STD_CASHFLOW_TYPE};
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/survey/fixedassets/fixedProjSovency";
	}
	
	/**
	 * @Description (保存项目现金流测算表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveProjCashFlows.do")
	@ResponseBody
	public String saveProjCashFlows(HttpServletRequest request, HttpServletResponse response,
			long taskId, String jsonArray) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			fixedProjSV.saveProjCashFowList(taskId, jsonArray);
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
}
