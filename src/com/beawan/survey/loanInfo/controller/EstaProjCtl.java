package com.beawan.survey.loanInfo.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.common.SysConstants.ExceptionConstant;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.survey.loanInfo.bean.EstaProjBase;
import com.beawan.survey.loanInfo.service.IEstaProjSV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;


/**
 * @ClassName EstaProjCtl
 * @Description 房地产项目信息控制器
 * @author rain
 * @Date 2018年1月22日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/survey/estate" })
public class EstaProjCtl {
	
	@Resource
	protected ITaskSV taskSV;

	@Resource
	private IEstaProjSV estaProjSV;
	
	/**
	 * @Description (房地产项目基本信息页面跳转)
	 * @param request
	 * @param response
	 * @param taskId 任务ID
	 * @return
	 */
	@RequestMapping("projBaseInfo.do")
	public String projBaseInfo(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		
		try {
			
			Task task = this.taskSV.getTaskById(taskId);
			request.setAttribute("task", task);
			
			EstaProjBase projBase = estaProjSV.findProjBaseByTaskId(taskId);
			
			if(projBase == null ){
				projBase = new EstaProjBase();
				projBase.setTaskId(taskId);
			}
			request.setAttribute("projBase", projBase);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/survey/estate/projBaseInfo";
	}
	
	/**
	 * @Description (房地产项目分析页面跳转)
	 * @param request
	 * @param response
	 * @param taskId 任务ID
	 * @return
	 */
	@RequestMapping("projAnalysis.do")
	public String projAnalysis(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		
		try {
			
			request.setAttribute("taskId", taskId);
			
			EstaProjBase projBase = estaProjSV.findProjBaseByTaskId(taskId);
			
			if(projBase == null ){
				projBase = new EstaProjBase();
				projBase.setTaskId(taskId);
			}
			request.setAttribute("projBase", projBase);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/survey/estate/projAnalysis";
	}
	
	/**
	 * @Description (保存房地产项目基本信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveEstaProjBase.do")
	@ResponseBody
	public String saveEstaProjBase(HttpServletRequest request, HttpServletResponse response,
			String jsondata) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			
			estaProjSV.saveProjBase(jsondata);
			
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
}
