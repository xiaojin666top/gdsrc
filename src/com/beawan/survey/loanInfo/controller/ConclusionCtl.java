package com.beawan.survey.loanInfo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.base.entity.InduInfo;
import com.beawan.base.entity.User;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants.ExceptionConstant;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.customer.service.FinriskResultService;
import com.beawan.survey.custInfo.bean.CompWmWarnSignal;
import com.beawan.survey.custInfo.controller.CompBaseCtl;
import com.beawan.survey.custInfo.entity.CompContribute;
import com.beawan.survey.custInfo.service.CompContributeService;
import com.beawan.survey.custInfo.service.CompWmWarnSignalService;
import com.beawan.survey.loanInfo.bean.Conclusion;
import com.beawan.survey.loanInfo.bean.RiskAnalysis;
import com.beawan.survey.loanInfo.service.IConclusionSV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.DateUtil;
import com.platform.util.HttpUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;

/**
 * @ClassName ConclusionCtl
 * @Description 风险分析及综合结论信息控制器
 * @author rain
 * @Date 2018年1月22日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/survey/loan" })
public class ConclusionCtl extends BaseController {

	private static final Logger log = Logger.getLogger(CompBaseCtl.class);
	@Resource
	protected ITaskSV taskSV;
	@Resource
	private IConclusionSV conclusionSV;
	@Resource
	private CompContributeService contributeService;
    @Resource
    private FinriskResultService finriskResultService;
    @Resource
    private CompWmWarnSignalService compWmWarnSignalService;

	/**
	 * @Description (信贷风险分析页面跳转)
	 * @param request
	 * @param serNo 任务流水号
	 * @return
	 */
	@RequestMapping("riskAnalysis.do")
	public String riskAnalysis(HttpServletRequest request, String serNo) {
		try {
			Task task = this.taskSV.getTaskBySerNo(serNo);
			RiskAnalysis riskAnalysis = conclusionSV.findRiskAnalysisByTaskId(task.getId());
			if(riskAnalysis == null ){
				riskAnalysis = new RiskAnalysis();
				riskAnalysis.setSerNo(serNo);
			}
			if(StringUtil.isEmptyString(riskAnalysis.getIndustryRisk())){
				String industry = task.getIndustry();
				if (industry!=null){
					InduInfo induInfo = finriskResultService.getInduAnalysis(industry);
					if (induInfo!=null){
						riskAnalysis.setIndustryRisk(induInfo.getInduRisk());
					}
				}
			}
			if(StringUtil.isEmptyString(riskAnalysis.getWarningRisk())){
				List<CompWmWarnSignal> warnList = compWmWarnSignalService.selectByProperty("taskId", task.getId());
				StringBuilder warnAnaly = new StringBuilder();
				if(!CollectionUtils.isEmpty(warnList)){
					for(int i = 0; i < warnList.size(); i++){
						CompWmWarnSignal sign = warnList.get(i);
						warnAnaly.append("(").append(i+1).append(")  ").append(sign.getAlarminfoDesc()).append(";");
					}
				}else{
					warnAnaly.append("无预警信号。");
				}
		
				if(warnAnaly.length() > 0){
					warnAnaly.replace(warnAnaly.length() - 1, warnAnaly.length(), "。");
					riskAnalysis.setWarningRisk(warnAnaly.toString());
				}
			}
			conclusionSV.saveRiskAnalysis(riskAnalysis);
			request.setAttribute("serNo", serNo);
			request.setAttribute("risk", riskAnalysis);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "views/survey/common/loanRiskAnalysis";
	}
	
	/**
	 * @Description (保存信贷风险分析信息)
	 * @param request
	 * @param serNo
	 * @return
	 */
	@RequestMapping("saveRiskAnalysis.do")
	@ResponseBody
	public ResultDto saveRiskAnalysis(HttpServletRequest request, String serNo) {

		ResultDto re = returnFail("保存失败！");
		String riskInfo = request.getParameter("riskInfo");
		
		try {
			RiskAnalysis newInfo = JacksonUtil.fromJson(riskInfo, RiskAnalysis.class);
			if (newInfo == null) {
				re.setMsg("传输数据内容为空，请重试");
				return re;
			}

			if (StringUtil.isEmptyString(newInfo.getSerNo()) && !StringUtil.isEmptyString(serNo))
				newInfo.setSerNo(serNo);
			RiskAnalysis oldInfo = conclusionSV.findBySerNo(serNo);
			if (oldInfo != null)  newInfo.setId(oldInfo.getId());

			User user = HttpUtil.getCurrentUser(request);
			if(newInfo.getId() == null){
				newInfo.setCreater(user.getUserId());
				newInfo.setCreateTime(DateUtil.getNowTimestamp());
			}
			newInfo.setUpdater(user.getUserId());
			newInfo.setUpdateTime(DateUtil.getNowTimestamp());
			newInfo.setStatus(Constants.NORMAL);
			conclusionSV.saveRiskAnalysis(newInfo);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (DataAccessException e) {
			re.setMsg(ExceptionConstant.DATA_ACCESS_EXP);
			log.error(ExceptionConstant.DATA_ACCESS_EXP, e);
			e.printStackTrace();
		} catch (Exception e) {
			re.setMsg(ExceptionConstant.UNDEFINED_EXP);
			log.error(ExceptionConstant.UNDEFINED_EXP, e);
			e.printStackTrace();
		}
		return re;
	}
	
	
	/**
	 * @Description (综合结论及授信安排页面跳转)
	 * @param request
	 * @param taskId 任务ID
	 * @return
	 */
	@RequestMapping("conclusion.do")
	public String conclusion(HttpServletRequest request, long taskId) {
		
		try {
			Task task = this.taskSV.getTaskById(taskId);
			request.setAttribute("task", task);
			Conclusion conclusion = conclusionSV.updateAndFindConclusionByTaskId(taskId);
			request.setAttribute("conclusion", conclusion);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "views/survey/common/loanConclusion";
	}
	
	/**
	 * @Description (保存综合结论及授信安排信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveConclusion.do")
	@ResponseBody
	public String saveConclusion(HttpServletRequest request, HttpServletResponse response,
			String jsondata) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			
			conclusionSV.saveConclusion(JacksonUtil.fromJson(jsondata, Conclusion.class));
			
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}


	/** 跳转到 综合贡献度分析 */
	@RequestMapping("contributionAnalysis.do")
	@ResponseBody
	public ModelAndView contributionAnalysis(String serNo, String customerNo){
		ModelAndView mav = new ModelAndView("views/survey/common/compContribution");
		mav.addObject("serNo", serNo);
		try {
			CompContribute result = contributeService.selectSingleByProperty("serNo", serNo);
	        // 本地暂无数据则从CMIS同步
	        if (Objects.isNull(result))
	            result = contributeService.syncContributeFromCMIS(serNo, customerNo);
			mav.addObject("contribution", result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}

	/**
	 * 保存或修改 综合贡献度分析信息
	 * @param request
	 * @param serNo
	 * @return
	 */
	@RequestMapping("saveContributeInfo.json")
	@ResponseBody
	public ResultDto saveContributeInfo(HttpServletRequest request, String serNo){
		ResultDto re = returnFail("保存失败");
		String jsonData = request.getParameter("jsonData");
		try {
			CompContribute source = JacksonUtil.fromJson(jsonData, CompContribute.class);
			if (Objects.isNull(source)) return returnFail("传输数据内容为空，请重试！");
			
			User user = HttpUtil.getCurrentUser(request);
			CompContribute entity = contributeService.selectSingleByProperty("serNo", serNo);
			if(Objects.isNull(entity)){
				entity = new CompContribute();
				entity.setSerNo(serNo);
				entity.setCreater(user.getUserId());
				entity.setCreateTime(DateUtil.getNowTimestamp());
			}
			entity.setBusiness(source.getBusiness());
			entity.setDepositInfo(source.getDepositInfo());
			entity.setIncomeSum(source.getIncomeSum());
			
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			entity.setStatus(Constants.NORMAL);
			contributeService.saveOrUpdate(entity);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
}
