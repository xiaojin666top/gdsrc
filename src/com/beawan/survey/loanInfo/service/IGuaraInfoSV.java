package com.beawan.survey.loanInfo.service;

import java.util.List;

import com.beawan.survey.loanInfo.bean.CounterGuaraCom;
import com.beawan.survey.loanInfo.bean.CounterGuaraMort;
import com.beawan.survey.loanInfo.bean.GuaraCom;
import com.beawan.survey.loanInfo.bean.GuaraPerson;
import com.beawan.survey.loanInfo.bean.GuaraProCom;
import com.beawan.survey.loanInfo.bean.GuaranteeInfo;
import com.beawan.survey.loanInfo.bean.Guarantor;
import com.beawan.survey.loanInfo.bean.MortgageInfo;
import com.beawan.survey.loanInfo.bean.PledgeInfo;

/**
 * @Description 担保信息服务接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IGuaraInfoSV {
	
	/**
	 * @Description (保存保证人信息)
	 * @param guarantor
	 * @return
	 * @throws Exception
	 */
	public Guarantor saveGuarantor(Guarantor guarantor) throws Exception ;
	
	/**
	 * @Description (保存保证人信息)
	 * @param jsondata 保证人信息json串
	 * @return
	 * @throws Exception
	 */
	public Guarantor saveGuarantor(String jsondata) throws Exception ;
	
	/**
	 * @Description (根据任务号删除关联保证人信息)
	 * @param taskId
	 * @throws Exception
	 */
	public void deleteGuarantorByTaskId(long taskId) throws Exception;
	
	/**
	 * @Description (根据ID删除保证人信息)
	 * @param id
	 * @throws Exception
	 */
	public void deleteGuarantorById(long id) throws Exception ;
	
	/**
	 * @Description (根据id查询保证人信息)
	 * @param id 保证人ID
	 * @return Guarantor
	 * @throws Exception
	 */
	public Guarantor findGuarantorById(Long id) throws Exception ;
	
	/**
	 * @Description (根据任务号和客户号查询保证人信息)
	 * @param taskId 任务号
	 * @param customerNo 客户号
	 * @return Guarantor
	 * @throws Exception
	 */
	public Guarantor findGuarantor(long taskId, String customerNo) throws Exception ;
	
	/**
	 * @Description (根据任务号查询保证人列表)
	 * @param taskId 任务号
	 * @return List<Guarantor>
	 * @throws Exception
	 */
	public List<Guarantor> findGuarantorByTaskId(Long taskId) throws Exception;
	
	/**
	 * @Description (根据任务号查询保证人列表)
	 * @param taskId 任务号
	 * @param guaraType 保证人类型
	 * @return List<Guarantor>
	 * @throws Exception
	 */
	public List<Guarantor> findGuarantorByTaskId(Long taskId, String guaraType) throws Exception;
	
	/**
	 * @Description (保存保证人信息)
	 * @param id 保证人ID
	 * @return Guarantor
	 * @throws Exception
	 */
	public void saveGuarantorById(Guarantor guarantor) throws Exception ;
	
	/**
	 * @Description (保存保证人列表)
	 * @param taskId 任务号
	 * @return List<Guarantor>
	 * @throws Exception
	 */
	public void saveGuarantorList(Long taskId,String jsonArray) throws Exception;
	
	/**
	 * @Description (根据任务号查询担保信息)
	 * @param taskId 任务号
	 * @return GuaraInfo
	 * @throws Exception
	 */

	public GuaranteeInfo findGuaraInfoBytaskId(Long taskId) throws Exception;
	
	/**
	 * @Description (保存授信担保信息)
	 * @param taskId 任务号
	 * @return GuaraInfo
	 * @throws Exception
	 */
	public void saveGuaraInfo(GuaranteeInfo guaraInfo) throws Exception;
	
	/**
	 * @Description (根据ID查询一般公司担保信息实体)
	 * @param id 保证人ID
	 * @return GuaraCom
	 * @throws Exception
	 */
	public GuaraCom findGuaraComById(Long id) throws Exception;
	
	/**
	 * @Description (根据任务号和客户号查询一般企业保证人信息)
	 * @param taskId 任务号
	 * @param customerNo 客户号
	 * @return GuaraCom
	 * @throws Exception
	 */
	public GuaraCom findGuaraCom(long taskId, String customerNo) throws Exception ;
	

	/**
	 * @Description (保存一般公司担保信息)
	 * @param guaraCom 
	 * @return 
	 * @throws Exception
	 */
	public void saveGuaraCom(GuaraCom guaraCom) throws Exception;
	
	/**
	 * @Description (保存一般公司担保信息)
	 * @param jsondata 
	 * @return 
	 * @throws Exception
	 */
	public void saveGuaraCom(String jsondata) throws Exception;

	/**
	 * @Description (根据ID查询担保公司担保信息实体)
	 * @param id 保证人ID
	 * @return GuaraProCom
	 * @throws Exception
	 */
	public GuaraProCom findGuaraProComById(Long id) throws Exception;
	
	/**
	 * @Description (保存担保公司担保信息)
	 * @param guaraProCom 担保公司担保信息实体
	 * @return 
	 * @throws Exception
	 */
	public void saveGuaraProCom(GuaraProCom guaraProCom) throws Exception;
	
	/**
	 * @Description (保存担保公司担保信息)
	 * @param jsondata 担保公司担保信息json串
	 * @return 
	 * @throws Exception
	 */
	public void saveGuaraProCom(String jsondata) throws Exception;
	
	/**
	 * @Description (查询自然人担保信息实体)
	 * @param Id 保证人ID
	 * @throws Exception
	 */
	public GuaraPerson findGuaraPersonById(Long id) throws Exception;
	
	/**
	 * @Description (保存自然人担保信息实体)
	 * @param guaraPerson
	 * @throws Exception
	 */
	public void saveGuaraPerson(GuaraPerson guaraPerson) throws Exception;
	
	/**
	 * @Description (保存自然人担保信息)
	 * @param jsondata
	 * @throws Exception
	 */
	public void saveGuaraPerson(String jsondata) throws Exception;
	
	/**
	 * @Description (根据任务号担保公司反担保企业信息列表)
	 * @param guaraId 
	 * @return List<CounterGuaraCom>
	 * @throws Exception
	 */
	public List<CounterGuaraCom> findCounterGuaraComByGuaraId(Long guaraId) throws Exception;
	
	/**
	 * @Description (保存担保公司反担保企业信息)
	 * @param counterGuaraCom
	 * @return 
	 * @throws Exception
	 */
	public CounterGuaraCom saveCounterGuaraCom(CounterGuaraCom counterGuaraCom)throws Exception;
	
	/**
	 * @Description (保存担保公司反担保企业信息列表)
	 * @param guaraId 
	 * @return 
	 * @throws Exception
	 */
	public void saveCounterGuaraComList(Long guaraId,String jsonArray)throws Exception;
	
	/**
	 * @Description (删除担保公司反担保企业信息)
	 * @param id 唯一标识
	 * @return 
	 * @throws Exception
	 */
	public void deleteCounterGuaraComById(long id)throws Exception;
	
	/**
	 * @Description (根据任务号担保公司反担保抵质押物信息列表)
	 * @param guaraId 
	 * @return List<CounterGuaraMort>
	 * @throws Exception
	 */
	public List<CounterGuaraMort> findCounterGuaraMortByGuaraId(Long guaraId) throws Exception;
	
	/**
	 * @Description (保存担保公司反担保抵质押物信息)
	 * @param counterGuaraMort
	 * @return 
	 * @throws Exception
	 */
	public CounterGuaraMort saveCounterGuaraMort(CounterGuaraMort counterGuaraMort)throws Exception;
	
	/**
	 * @Description (保存担保公司反担保抵质押物列表)
	 * @param guaraId 保证人ID
	 * @return 
	 * @throws Exception
	 */
	public void saveCounterGuaraMortList(Long guaraId,String jsonArray)throws Exception;
	/**
	 * @Description (删除担保公司反担保抵质押物信息)
	 * @param id 唯一标识
	 * @return 
	 * @throws Exception
	 */
	public void deleteCounterGuaraMortById(long id)throws Exception;
	
	/**
	 * @Description (根据任务号查询抵押物信息列表)
	 * @param taskId 任务号
	 * @return List<MortgageInfo>
	 * @throws Exception
	 */
	public List<MortgageInfo> findMortgageInfoByTaskId(Long taskId) throws Exception;
	
	/**
	 * @Description (保存抵押物信息)
	 * @param taskId 任务号
	 * @return 
	 * @throws Exception
	 */
	public MortgageInfo saveMortgageInfo(MortgageInfo mortgageInfo)throws Exception;
	
	/**
	 * @Description (保存抵押物信息列表)
	 * @param taskId 任务号
	 * @return 
	 * @throws Exception
	 */
	public void saveMortgageInfoList(Long taskId,String jsonArray)throws Exception;
	
	/**
	 * @Description (删除抵押物信息)
	 * @param id 抵押物标识
	 * @throws Exception
	 */
	public void deleteMortgageInfoById(long id)throws Exception;
	
	/**
	 * @Description (根据任务号删除关联抵押物信息)
	 * @param taskId
	 * @throws Exception
	 */
	public void deleteMortgageByTaskId(long taskId)throws Exception;
	
	/**
	 * @Description (根据任务号查询质押物信息列表)
	 * @param taskId 任务号
	 * @return List<PledgeInfo>
	 * @throws Exception
	 */
	public List<PledgeInfo> findPledgeInfoByTaskId(Long taskId) throws Exception;
	
	/**
	 * TODO 保存质押物信息
	 * @param pledgeInfo 质押物信息实体
	 * @return
	 * @throws Exception
	 */
	public PledgeInfo savePledgeInfo(PledgeInfo pledgeInfo) throws Exception;
	
	/**
	 * @Description (保存质押物信息列表)
	 * @param taskId 任务号
	 * @return 
	 * @throws Exception
	 */
	public void savePledgeInfoList(Long taskId,String jsonArray)throws Exception;
	
	/**
	 * TODO 根据id删除质押物信息
	 * @param id 质押物标识
	 * @throws Exception
	 */
	public void deletePledgeInfoById(long id) throws Exception;
	
	/**
	 * TODO 根据任务号删除关联质押物信息
	 * @param taskId
	 * @throws Exception
	 */
	public void deletePledgeByTaskId(long taskId)throws Exception;
}
