package com.beawan.survey.loanInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.loanInfo.bean.BenefitForecast;

/**
 * @author yzj
 */
public interface BenefitForecastService extends BaseService<BenefitForecast> {
}
