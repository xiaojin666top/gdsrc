package com.beawan.survey.loanInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.loanInfo.bean.ApplyRateInfo;

/**
 * @author yzj
 */
public interface ApplyRateInfoService extends BaseService<ApplyRateInfo> {
}
