package com.beawan.survey.loanInfo.service;

import java.util.List;

import com.beawan.survey.loanInfo.bean.FixedProjBase;
import com.beawan.survey.loanInfo.bean.FixedProjCashFow;
import com.beawan.survey.loanInfo.bean.FixedProjFinaPlan;
import com.beawan.survey.loanInfo.bean.FixedProjInvest;
import com.beawan.survey.loanInfo.bean.FixedProjInvestEst;
import com.beawan.survey.loanInfo.bean.FixedProjPlan;

/**
 * @Description 固定资产贷款项目信息服务接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IFixedProjSV {
	
	/**
	 * 保存项目基本信息
	 * @param fixedProjBase
	 * @throws Exception
	 */
	public void saveProjBase(FixedProjBase fixedProjBase) throws Exception;
	
	/**
	 * 保存项目基本信息
	 * @param jsondata 项目基本信json串
	 * @throws Exception
	 */
	public void saveProjBase(String jsondata) throws Exception;
	
	/**
	 * 根据任务号查询项目基本信息
	 * @param taskId 任务号
	 * @return
	 * @throws Exception
	 */
	public FixedProjBase  queryProjBaseByTaskId(Long taskId) throws Exception;
	
	
	/**
	 * 保存固定资产项目建设计划列表
	 * @param fixedProjBase
	 * @throws Exception
	 */
	public void saveProjPlanList(Long taskId,String  jsonArray) throws Exception;
	
	/**
	 * 根据任务号查询固定资产项目建设计划列表
	 * @param taskId 任务号
	 * @return
	 * @throws Exception
	 */
	public List<FixedProjPlan>  queryProjPlanByTaskId(Long taskId) throws Exception;
	
	
	
	/**
	 * 保存固定资产项目投资情况列表
	 * @param fixedProjBase
	 * @throws Exception
	 */
	public void saveProjInvestList(Long taskId, String  jsonArray) throws Exception;
	
	/**
	 * 根据任务号查询固定资产项目投资情况列表
	 * @param taskId 任务号
	 * @return
	 * @throws Exception
	 */
	public List<FixedProjInvest>  queryProjInvestByTaskId(Long taskId) throws Exception;
	
	
	
	/**
	 * 保存固定资产项目建设投资估算列表
	 * @param fixedProjBase
	 * @throws Exception
	 */
	public void saveProjInvestEstList(Long taskId, String  jsonArray) throws Exception;
	
	/**
	 * 根据任务号查询固定资产项目建设投资估算列表
	 * @param taskId 任务号
	 * @return
	 * @throws Exception
	 */
	public List<FixedProjInvestEst>  queryProjInvestEstByTaskId(Long taskId) throws Exception;
	
	
	/**
	 * 保存固定资产项目建设筹资计划列表
	 * @param fixedProjBase
	 * @throws Exception
	 */
	public void saveProjFinaPlanList(Long taskId,String  jsonArray) throws Exception;
	
	/**
	 * 根据任务号查询固定资产项目建设筹资计划列表
	 * @param taskId 任务号
	 * @return
	 * @throws Exception
	 */
	public List<FixedProjFinaPlan>  queryProjFinaPlanByTaskId(Long taskId) throws Exception;
	
	
	/**
	 * 保存固定资产项目建设现金流测算列表
	 * @param fixedProjBase
	 * @throws Exception
	 */
	public void saveProjCashFowList(Long taskId, String  jsonArray) throws Exception;
	
	/**
	 * 根据任务号查询固定资产项目建设现金流测算列表
	 * @param taskId 任务号
	 * @return
	 * @throws Exception
	 */
	public List<FixedProjCashFow>  queryProjCashFowByTaskId(Long taskId) throws Exception;

}
