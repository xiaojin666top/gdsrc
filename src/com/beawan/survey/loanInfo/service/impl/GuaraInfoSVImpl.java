package com.beawan.survey.loanInfo.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.util.SystemUtil;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.beawan.survey.custInfo.dao.ICompBaseDAO;
import com.beawan.survey.loanInfo.bean.CounterGuaraCom;
import com.beawan.survey.loanInfo.bean.CounterGuaraMort;
import com.beawan.survey.loanInfo.bean.GuaraCom;
import com.beawan.survey.loanInfo.bean.GuaraPerson;
import com.beawan.survey.loanInfo.bean.GuaraProCom;
import com.beawan.survey.loanInfo.bean.GuaranteeInfo;
import com.beawan.survey.loanInfo.bean.Guarantor;
import com.beawan.survey.loanInfo.bean.MortgageInfo;
import com.beawan.survey.loanInfo.bean.PledgeInfo;
import com.beawan.survey.loanInfo.dao.ICounterGuaraComDAO;
import com.beawan.survey.loanInfo.dao.ICounterGuaraMortDAO;
import com.beawan.survey.loanInfo.dao.IGuaraComDAO;
import com.beawan.survey.loanInfo.dao.IGuaraPersonDAO;
import com.beawan.survey.loanInfo.dao.IGuaraProComDAO;
import com.beawan.survey.loanInfo.dao.IGuaranteeInfoDAO;
import com.beawan.survey.loanInfo.dao.IGuarantorDAO;
import com.beawan.survey.loanInfo.dao.IMortgageInfoDAO;
import com.beawan.survey.loanInfo.dao.IPledgeInfoDAO;
import com.beawan.survey.loanInfo.service.IGuaraInfoSV;
import com.platform.util.BeanUtil;
import com.platform.util.JdbcUtil;
import com.platform.util.StringUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


/**
 * @Description 担保信息服务接口实现
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
@Service("guaraInfoSV")
public class GuaraInfoSVImpl implements IGuaraInfoSV {
	
	@Resource
	private IGuaranteeInfoDAO  guaranteeInfoDAO;
	
	@Resource
	private IGuarantorDAO  guarantorDAO;
	
	@Resource
	private IGuaraComDAO  guaraComDAO;
	
	@Resource 
	private IGuaraPersonDAO guaraPersonDAO;
	
	@Resource
	private IGuaraProComDAO guaraProComDAO;
	
	@Resource
	private ICounterGuaraComDAO counterGuaraComDAO;
	
	@Resource
	private ICounterGuaraMortDAO counterGuaraMortDAO;
	
	@Resource
	private IMortgageInfoDAO mortgageInfoDAO;
	
	@Resource
	private IPledgeInfoDAO pledgeInfoDAO;
	
	@Resource
	private ICompBaseDAO compBaseDAO;
	
	@Resource
	private ICmisInInvokeSV cmisInInvokeSV;
	
	@Override
	public Guarantor saveGuarantor(Guarantor guarantor) throws Exception {
		return guarantorDAO.saveOrUpdate(guarantor);
	}
	
	@Override
	public Guarantor saveGuarantor(String jsondata) throws Exception {
		
		if(StringUtil.isEmpty(jsondata))
			return null;
		
		JSONObject json = JSONObject.fromObject(jsondata);
		Guarantor guarantor = (Guarantor) JSONObject.toBean(json,
				Guarantor.class);
		Guarantor oldGuarantor = guarantorDAO.selectByPrimaryKey(guarantor.getId());
		
		if(oldGuarantor != null){
			BeanUtil.mergeProperties(guarantor, oldGuarantor, json);
		}else
			oldGuarantor = guarantor;
		
		oldGuarantor = guarantorDAO.saveOrUpdate(oldGuarantor);
		
		//同步保证人客户信息
		cmisInInvokeSV.syncCusInfo(guarantor.getCusNo());
		
		return oldGuarantor;
	}
	
	@Override
	public void deleteGuarantorByTaskId(long taskId) throws Exception {
		List<Guarantor> guarantors = this.findGuarantorByTaskId(taskId);
		if(!CollectionUtils.isEmpty(guarantors)){
			for(Guarantor guarantor : guarantors){
				this.deleteGuarantorById(guarantor.getId());
			}
		}
	}
	
	@Override
	public void deleteGuarantorById(long id) throws Exception {
		
		Guarantor guarantor = this.findGuarantorById(id);
		String guaraType = guarantor.getGuaraType();
		
		if(SysConstants.GuarantorType.COMP_GUARA.equals(guaraType)){
			//删除公司保证人担保信息
			String sql = "DELETE FROM GDTCESYS.BIZ_GUARA_COM WHERE GUARA_ID = " + id;
			JdbcUtil.execute(sql, Constants.DataSource.TCE_DS);
			
		}else if(SysConstants.GuarantorType.COMP_GUARA.equals(guaraType)){
			//删除担保公司反担保企业信息
			String sql = "DELETE FROM GDTCESYS.BIZ_COUNTER_GUARA_COM WHERE GUARA_ID = " + id;
			JdbcUtil.execute(sql, Constants.DataSource.TCE_DS);
			//删除担保公司反担保抵质押物信息
			sql = "DELETE FROM GDTCESYS.BIZ_COUNTER_GUARA_MORT WHERE GUARA_ID = " + id;
			JdbcUtil.execute(sql, Constants.DataSource.TCE_DS);
			//删除担保公司保证人担保信息
			sql = "DELETE FROM GDTCESYS.BIZ_GUARA_PRO_COM WHERE GUARA_ID = " + id;
			JdbcUtil.execute(sql, Constants.DataSource.TCE_DS);
			
		}else if(SysConstants.GuarantorType.COMP_GUARA.equals(guaraType)){
			//删除个人保证人担保信息
			String sql = "DELETE GDTCESYS.FROM BIZ_GUARA_PERSON WHERE GUARA_ID = " + id;
			JdbcUtil.execute(sql, Constants.DataSource.TCE_DS);
		}
		
		//删除保证人主表信息
		String sql = "DELETE FROM GDTCESYS.BIZ_GUARANTOR WHERE ID = " + id;
		JdbcUtil.execute(sql, Constants.DataSource.TCE_DS);
	}
	
	@Override
	public List<Guarantor> findGuarantorByTaskId(Long taskId) throws Exception {
		
		String query = "taskId=:taskId";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("taskId", taskId);
		
		return guarantorDAO.select(query, params);
	}
	
	@Override
	public List<Guarantor> findGuarantorByTaskId(Long taskId, String guaraType) throws Exception {
		
		String query = "taskId=:taskId";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("taskId", taskId);
		
		if(!StringUtil.isEmptyString(guaraType)){
			query += " and guaraType=:guaraType";
			params.put("guaraType", guaraType);
		}
		
		return guarantorDAO.select(query, params);
	}
	
	public Guarantor findGuarantorById(Long id) throws Exception {
		
		return guarantorDAO.selectByPrimaryKey(id);
	}
	
	public Guarantor findGuarantor(long taskId, String cusNo) throws Exception {
		
		String query = "taskId=:taskId and cusNo=:cusNo";
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("taskId", taskId);
		params.put("cusNo", cusNo);
		
		return guarantorDAO.selectSingle(query, params);
	}
	
	@Override
	public GuaranteeInfo findGuaraInfoBytaskId(Long taskId) throws Exception {
		
		GuaranteeInfo  guaranteeInfo = guaranteeInfoDAO.findByPrimaryKey(taskId);
		return guaranteeInfo;
	}

	@Override
	public void saveGuaraInfo(GuaranteeInfo guaraInfo) throws Exception {
		
		long taskId = guaraInfo.getTaskId();
		String guaraType = guaraInfo.getGuaraType();
		//担保方式不包含抵押则删除抵押物信息
		if(!guaraType.contains(SysConstants.GuaraWay.DY)){
			this.deleteMortgageByTaskId(taskId);
			guaraInfo.setMortgageAnalysis(null);
		}
		
		//担保方式不包含质押则删除质押物信息
		if(!guaraType.contains(SysConstants.GuaraWay.ZY)){
			this.deletePledgeByTaskId(taskId);
			guaraInfo.setPledgeAnalysis(null);
		}
		
		//担保方式不包含保证则删除保证人信息
		if(!guaraType.contains(SysConstants.GuaraWay.BZ)){
			this.deleteGuarantorByTaskId(taskId);
			guaraInfo.setGuarantorAnalysis(null);
		}
		
		guaranteeInfoDAO.saveOrUpdate(guaraInfo);
	}

	@Override
	public void saveGuaraCom(GuaraCom guaraCom) throws Exception {
		
		guaraComDAO.saveOrUpdate(guaraCom);
	}
	
	@Override
	public void saveGuaraCom(String jsondata) throws Exception {
		
		if(StringUtil.isEmpty(jsondata))
			return;
		
		JSONObject json = JSONObject.fromObject(jsondata);
		GuaraCom guaraCom = (GuaraCom) JSONObject.toBean(json,
				GuaraCom.class);
		GuaraCom oldGuaraCom = guaraComDAO.selectByPrimaryKey(guaraCom.getGuaraId());
		
		if(oldGuaraCom != null){
			BeanUtil.mergeProperties(guaraCom, oldGuaraCom, json);
		}else
			oldGuaraCom = guaraCom;
		
		guaraComDAO.saveOrUpdate(oldGuaraCom);
		
		
		if(json.containsKey("guarantor")){
			this.saveGuarantor(json.getString("guarantor"));
		}
	}

	@Override
	public void saveGuaraProCom(GuaraProCom guaraProCom) throws Exception {
		
		guaraProComDAO.saveOrUpdate(guaraProCom);
	}
	
	@Override
	public void saveGuaraProCom(String jsondata) throws Exception {
		
		if(StringUtil.isEmpty(jsondata))
			return;
		
		JSONObject json = JSONObject.fromObject(jsondata);
		GuaraProCom guaraProCom = (GuaraProCom) JSONObject.toBean(json,
				GuaraProCom.class);
		GuaraProCom oldGuaraProCom = guaraProComDAO.selectByPrimaryKey(guaraProCom.getGuaraId());
		
		if(oldGuaraProCom != null){
			BeanUtil.mergeProperties(guaraProCom, oldGuaraProCom, json);
		}else
			oldGuaraProCom = guaraProCom;
		
		guaraProComDAO.saveOrUpdate(oldGuaraProCom);
		
		if(json.containsKey("guarantor")){
			this.saveGuarantor(json.getString("guarantor"));
		}
	}

	@Override
	public void saveGuaraPerson(GuaraPerson guaraPerson) throws Exception {
		
		guaraPersonDAO.saveOrUpdate(guaraPerson);
	}
	
	@Override
	public void saveGuaraPerson(String jsondata) throws Exception {
		
		if(StringUtil.isEmpty(jsondata))
			return;
		
		JSONObject json = JSONObject.fromObject(jsondata);
		GuaraPerson guaraPerson = (GuaraPerson) JSONObject.toBean(json,
				GuaraPerson.class);
		GuaraPerson oldGuaraPerson = guaraPersonDAO.selectByPrimaryKey(guaraPerson.getGuaraId());
		
		if(oldGuaraPerson != null){
			BeanUtil.mergeProperties(guaraPerson, oldGuaraPerson, json);
		}else
			oldGuaraPerson = guaraPerson;
		
		guaraPersonDAO.saveOrUpdate(oldGuaraPerson);
	}

	@Override
	public List<CounterGuaraCom> findCounterGuaraComByGuaraId(Long guaraId) throws Exception {
		
		return counterGuaraComDAO.queryCounterGuaraComByGuaraId(guaraId);
	}
	
	@Override
	public CounterGuaraCom saveCounterGuaraCom(CounterGuaraCom counterGuaraCom)throws Exception {
		return counterGuaraComDAO.saveOrUpdate(counterGuaraCom);
	}

	@Override
	public void saveCounterGuaraComList(Long guaraId,String jsonArray) throws Exception {
		//先删除
		List<CounterGuaraCom> list=counterGuaraComDAO.queryCounterGuaraComByGuaraId(guaraId);
		for(CounterGuaraCom data:list) {
			counterGuaraComDAO.deleteEntity(data);
		}
		
		//后插入
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				JSONObject dataStr = JSONObject.fromObject(array.get(i));
				CounterGuaraCom data=(CounterGuaraCom)JSONObject.toBean(dataStr, CounterGuaraCom.class);
				counterGuaraComDAO.saveOrUpdate(data);
			}
		}
	}

	@Override
	public void deleteCounterGuaraComById(long id)throws Exception {
		counterGuaraComDAO.deleteEntity(counterGuaraComDAO.selectByPrimaryKey(id));
	}
	
	public List<CounterGuaraMort> findCounterGuaraMortByGuaraId(Long guaraId) throws Exception {
		
		return counterGuaraMortDAO.quertCounterGuaraMortListByGuaraId(guaraId);
	}
	
	@Override
	public CounterGuaraMort saveCounterGuaraMort(CounterGuaraMort counterGuaraMort)throws Exception {
		return counterGuaraMortDAO.saveOrUpdate(counterGuaraMort);
	}

	@Override
	public void saveCounterGuaraMortList(Long guaraId, String jsonArray) throws Exception {
	
		//先删除
		List<CounterGuaraMort> list=counterGuaraMortDAO.quertCounterGuaraMortListByGuaraId(guaraId);
		for(CounterGuaraMort data:list) {
			counterGuaraMortDAO.deleteEntity(data);
		}
		
		//后插入
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				JSONObject dataStr = JSONObject.fromObject(array.get(i));
				CounterGuaraMort data=(CounterGuaraMort)JSONObject.toBean(dataStr, CounterGuaraMort.class);
				counterGuaraMortDAO.saveOrUpdate(data);
			}
		}
	}
	
	@Override
	public void deleteCounterGuaraMortById(long id)throws Exception {
		counterGuaraMortDAO.deleteEntity(counterGuaraMortDAO.selectByPrimaryKey(id));
	}

	@Override
	public List<MortgageInfo> findMortgageInfoByTaskId(Long taskId) throws Exception {
		
		return mortgageInfoDAO.queryMortgageInfoListByTaskId(taskId);
	}
	
	@Override
	public MortgageInfo saveMortgageInfo(MortgageInfo mortgageInfo)throws Exception {
		
		//生成押品编号
		if(StringUtil.isEmptyString(mortgageInfo.getSerial()))
			mortgageInfo.setSerial(SystemUtil.generateMortNo());
		
		return mortgageInfoDAO.saveOrUpdate(mortgageInfo);
	}

	@Override
	public void saveMortgageInfoList(Long taskId,String jsonArray) throws Exception {
		
		//先删除
		List<MortgageInfo> list=mortgageInfoDAO.queryMortgageInfoListByTaskId(taskId);
		for(MortgageInfo data:list) {
			mortgageInfoDAO.deleteEntity(data);
		}
		
		//后插入
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				JSONObject dataStr = JSONObject.fromObject(array.get(i));
				MortgageInfo data= (MortgageInfo)JSONObject.toBean(dataStr, MortgageInfo.class);
				this.saveMortgageInfo(data);
			}
		}
	}
	
	@Override
	public void deleteMortgageInfoById(long id)throws Exception {
		mortgageInfoDAO.deleteEntity(mortgageInfoDAO.selectByPrimaryKey(id));
	}
	
	@Override
	public void deleteMortgageByTaskId(long taskId)throws Exception {
		List<MortgageInfo> mortgages = mortgageInfoDAO.queryMortgageInfoListByTaskId(taskId);
		if(!CollectionUtils.isEmpty(mortgages)){
			for(MortgageInfo mortgage : mortgages){
				mortgageInfoDAO.deleteEntity(mortgage);
			}
		}
	}
	
	@Override
	public List<PledgeInfo> findPledgeInfoByTaskId(Long taskId) throws Exception {
		
		return pledgeInfoDAO.queryPledgeInfoListByTaskId(taskId);
	}
	
	@Override
	public PledgeInfo savePledgeInfo(PledgeInfo pledgeInfo) throws Exception {
		
		//生成押品编号
		if(StringUtil.isEmptyString(pledgeInfo.getSerial()))
			pledgeInfo.setSerial(SystemUtil.generateMortNo());
				
		return pledgeInfoDAO.saveOrUpdate(pledgeInfo);
	}

	@Override
	public void savePledgeInfoList(Long taskId,String jsonArray) throws Exception {
		
		//先删除
		List<PledgeInfo> list=pledgeInfoDAO.queryPledgeInfoListByTaskId(taskId);
		for(PledgeInfo data:list) {
			pledgeInfoDAO.deleteEntity(data);
		}
		
		//后插入
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				JSONObject dataStr = JSONObject.fromObject(array.get(i));
				PledgeInfo data=(PledgeInfo)JSONObject.toBean(dataStr, PledgeInfo.class);
				this.savePledgeInfo(data);
			}
		}
	}
	
	@Override
	public void deletePledgeInfoById(long id) throws Exception {
		pledgeInfoDAO.deleteEntity(pledgeInfoDAO.selectByPrimaryKey(id));
	}
	
	@Override
	public void deletePledgeByTaskId(long taskId)throws Exception {
		List<PledgeInfo> pledges = pledgeInfoDAO.queryPledgeInfoListByTaskId(taskId);
		if(!CollectionUtils.isEmpty(pledges)){
			for(PledgeInfo pledge : pledges){
				pledgeInfoDAO.deleteEntity(pledge);
			}
		}
	}

	@Override
	public GuaraCom findGuaraComById(Long id) throws Exception {
		
		GuaraCom guaraCom=guaraComDAO.findByPrimaryKey(id);
		if(guaraCom != null) {
			guaraCom.setGuarantor(this.findGuarantorById(id));
		}
		
		return guaraCom;
	}
	
	public GuaraCom findGuaraCom(long taskId, String cusNo) throws Exception {
		
		GuaraCom guaraCom = null;
		
		Guarantor guarantor = this.findGuarantor(taskId, cusNo);
		if(guarantor != null){
			guaraCom = guaraComDAO.findByPrimaryKey(guarantor.getId());
			guaraCom.setGuarantor(guarantor);
		}
		
		return guaraCom;
	}

	@Override
	public GuaraProCom findGuaraProComById(Long id) throws Exception {
		
		GuaraProCom guaraProCom=guaraProComDAO.findByPrimaryKey(id);
		if(guaraProCom!=null) {
			guaraProCom.setGuarantor(this.findGuarantorById(id));
		}
		
		return guaraProCom;
	}

	@Override
	public GuaraPerson findGuaraPersonById(Long id) throws Exception {
		
		GuaraPerson guaraPerson = guaraPersonDAO.selectByPrimaryKey(id);
		if(guaraPerson != null) {
			guaraPerson.setGuarantor(this.findGuarantorById(id));
		}
		
		return guaraPerson;
	}

	@Override
	public void saveGuarantorById(Guarantor guarantor) throws Exception {
		
		guarantorDAO.saveOrUpdate(guarantor);
	}

	@Override
	public void saveGuarantorList(Long taskId, String jsonArray) throws Exception {
		
		//先删除
		List<Guarantor> list=this.findGuarantorByTaskId(taskId);
		for(Guarantor data:list) {
			guarantorDAO.deleteEntity(data);
		}
		
		//后插入
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				JSONObject dataStr = JSONObject.fromObject(array.get(i));
				Guarantor data=(Guarantor)JSONObject.toBean(dataStr, Guarantor.class);
				guarantorDAO.saveOrUpdate(data);
			}
		}
	}
	
}
