package com.beawan.survey.loanInfo.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.beawan.base.uitls.QixinUtil;
import com.beawan.survey.loanInfo.bean.CusLaw;
import com.beawan.survey.loanInfo.dao.ICusLawDAO;
import com.beawan.survey.loanInfo.service.ICusLawSV;

@Service("cusLawSV")
public class CusLawSVImpl implements ICusLawSV{
	
	@Resource 
	private ICusLawDAO cusLawDao;
	
	@Override
	public List<CusLaw> getLawInfoListByNo(String customerNo) throws Exception{
				 
		List<CusLaw> list=new ArrayList<CusLaw>();
		list=cusLawDao.getLawInfoListByNo(customerNo);

		return list;
	}

	@Override
	public void saveLawInfo(CusLaw data) throws Exception {
		
		cusLawDao.saveOrUpdate(data);
	}

}
