package com.beawan.survey.loanInfo.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.analysis.finansis.service.IFinanasisService;
import com.beawan.common.Constants;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.beawan.survey.loanInfo.bean.ApplyInfo;
import com.beawan.survey.loanInfo.bean.ApplyRate;
import com.beawan.survey.loanInfo.bean.ApplyScheme;
import com.beawan.survey.loanInfo.bean.Guarantor;
import com.beawan.survey.loanInfo.bean.MortgageInfo;
import com.beawan.survey.loanInfo.bean.PledgeInfo;
import com.beawan.survey.loanInfo.bean.Rate;
import com.beawan.survey.loanInfo.dao.IApplyInfoDAO;
import com.beawan.survey.loanInfo.dao.IApplyRateDAO;
import com.beawan.survey.loanInfo.dao.IApplySchemeDAO;
import com.beawan.survey.loanInfo.dao.IRateDAO;
import com.beawan.survey.loanInfo.service.IApplyInfoSV;
import com.beawan.survey.loanInfo.service.IGuaraInfoSV;
import com.beawan.task.bean.Task;
import com.beawan.task.dao.ITaskDAO;
import com.platform.util.JacksonUtil;
import com.platform.util.JdbcUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service("applyInfoSV")
public class ApplyInfoSVImpl implements IApplyInfoSV {
	
	@Resource
	private ITaskDAO taskDAO;
	
	@Resource
	private IApplyInfoDAO applyInfoDAO;
	
	@Resource
	private IApplyRateDAO applyRateDAO;

	@Resource
	private IApplySchemeDAO applySchemaDAO;
	
	@Resource
	private IFinanasisService finanasisSV;
	
	@Resource
	private ICmisInInvokeSV cmisInInvokeSV;
	
	@Resource
	private IGuaraInfoSV guaraInfoSV;
	
	@Resource
	private IRateDAO rateDAO;
	
	@Override
	public ApplyScheme findApplySchemeById(Long id) {
		return applySchemaDAO.findByPrimaryKey(id);
	}

	
	@Override
	public ApplyInfo findApplyInfoByTaskId(Long taskId) throws Exception {
		
		ApplyInfo applyInfo = applyInfoDAO.queryApplyInfoByTaskId(taskId);
		
		return applyInfo;
	}

	@Override
	public void saveApplyInfo(ApplyInfo applyInfo) throws Exception {
		applyInfoDAO.saveOrUpdate(applyInfo);
	}

	@Override
	public List<ApplyScheme> findApplySchemeListByTaskId(Long taskId) throws Exception {
		
		return applySchemaDAO.queryApplySchemeListByTaskId(taskId);
	}
	
	/**
	 * @Description (保存授信申请安排方案记录)
	 * @param applyScheme 授信申请安实体类
	 * @throws Exception
	 */
	public ApplyScheme saveApplyScheme(ApplyScheme applyScheme) throws Exception{
		return applySchemaDAO.saveOrUpdate(applyScheme);
	}

	@Override
	public void saveApplySchemeList(Long taskId,String jsonArray) throws Exception {
		
		//先删除
			List<ApplyScheme>  list=applySchemaDAO.queryApplySchemeListByTaskId(taskId);
			for(ApplyScheme  data:list) {
				applySchemaDAO.deleteEntity(data);
			}
				
			//后插入修改的
			JSONArray array = JSONArray.fromObject(jsonArray);
			if (array != null && array.size() > 0) {
				for (int i = 0; i < array.size(); i++) {
					JSONObject dataStr = JSONObject.fromObject(array.get(i));
					ApplyScheme data = (ApplyScheme) JSONObject.toBean(dataStr,ApplyScheme.class);
					applySchemaDAO.saveOrUpdate(data);
				}
			}
	}
	
	/**
	 * @Description (删除授信申请安排方案记录)
	 * @param id 唯一标识
	 * @throws Exception
	 */
	public void deleteApplySchemeById(long id) throws Exception{
		String sql = "DELETE FROM GDTCESYS.BIZ_APPLY_SCHEME WHERE ID = " + id;
		JdbcUtil.execute(sql, Constants.DataSource.TCE_DS);
	}

	@Override
	public List<ApplyRate> findApplyRateListByCustomerNo(String customerNo) throws Exception {
		
		String sql = "SELECT "
			+"P.PK_FTP_COA,P.EXC_RATE,P.EXC_RISE_RATIO,R.COA_NAME FROM CPDJ.PRP_PRICE_LIST P "
			+"LEFT JOIN CPDJ.PRP_PROD_RMCOA R ON R.PK_FTP_COA = P.PK_FTP_COA "
			+"WHERE P.CUST_CODE = '" + customerNo + "' order by P.MAKEDATE";
		
	
		List<Map<String, Object>> list=JdbcUtil.executeQuery(sql, Constants.DataSource.CPDJ_DS);
		List<ApplyRate> rates=new ArrayList<ApplyRate>();
		if(list.size()>0) {
			for(int i=0;i<list.size();i++) {
				ApplyRate applyRate=new ApplyRate();
				if(i<1) {
				applyRate.setBusinessType(list.get(i).get("coaName").toString());
				applyRate.setCurrIntValue(Double.valueOf(list.get(i).get("excRate").toString()));
				applyRate.setCurrFloatRatio(Double.valueOf(list.get(i).get("excRiseRatio").toString()));
				}else {
					applyRate.setBusinessType(list.get(i).get("coaName").toString());
					applyRate.setCurrIntValue(Double.valueOf(list.get(i).get("excRate").toString()));
					applyRate.setCurrFloatRatio(Double.valueOf(list.get(i).get("excRiseRatio").toString()));
					applyRate.setLastIntValue(Double.valueOf(list.get(i-1).get("excRate").toString()));
					applyRate.setLastFloatRatio(Double.valueOf(list.get(i-1).get("excRiseRatio").toString()));
				}
				rates.add(applyRate);
			}
		}
		return rates;
	}

	@Override
	public void saveApplyRateList(Long taskId, String jsonArray) throws Exception {
		
		//先删除
		List<ApplyRate> list=applyRateDAO.queryApplyRateListByTaskId(taskId);
		for(ApplyRate data:list) {
			applyRateDAO.deleteEntity(data);
		}
		
		//后插入修改的
		JSONArray array=JSONArray.fromObject(jsonArray);
		if (array != null && array.size() > 0) {
			for (int i = 0; i < array.size(); i++) {
				JSONObject dataStr = JSONObject.fromObject(array.get(i));
				ApplyRate data = (ApplyRate) JSONObject.toBean(dataStr,ApplyRate.class);  
				applyRateDAO.saveOrUpdate(data);
			}
		}
	}
	
	/**
	 * @Description (保存本次授信申利费率信息)
	 * @param applyRate 利费率信息实体
	 * @throws Exception
	 */
	public ApplyRate saveApplyRate(ApplyRate applyRate) throws Exception{
		return applyRateDAO.saveOrUpdate(applyRate);
	}
	
	/**
	 * @Description (删除本次授信申利费率信息)
	 * @param id 唯一标识
	 * @throws Exception
	 */
	public void deleteApplyRateById(long id) throws Exception{
		String sql = "DELETE FROM GDTCESYS.BIZ_APPLY_RATE WHERE ID = " + id;
		JdbcUtil.execute(sql, Constants.DataSource.TCE_DS);
	}

	@Override
	public List<ApplyRate> queryApplyRateListByTaskId(Long taskId) throws Exception {
		return applyRateDAO.queryApplyRateListByTaskId(taskId);
	}
	
	@Override
	public Map<String, Object> getCreditAmtCalResult(Task task,
			ApplyInfo applyInfo) throws Exception{
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		if(applyInfo == null)
			applyInfo = new ApplyInfo();
		//查询本行短期流动资金贷款余额
		double bal = cmisInInvokeSV.queryLoanBalance(task.getCustomerNo(), "1010050016037");
		applyInfo.setMyBankLiquidLoanBal(bal);
		
		//财务数据
		List<Map<String, Double>> amtCalFinaData = this.queryAmtCalFianData(
				task.getCustomerNo(), task.getYear(), task.getMonth());
		amtCalFinaData.get(3).put("我行流动资金贷款", bal);
		result.put("amtCalFinaData", amtCalFinaData);
		
		//额度测算结果
		Map<String, Double> amtCalResult = this.calCreditAmt(applyInfo,
				amtCalFinaData, task.getMonth());
		result.put("amtCalResult", amtCalResult);
		
		return result;
	}

	public List<Map<String, Double>> queryAmtCalFianData(String cusId, int finaRepYear,
			int finaRepMonth) throws Exception {
		
		List<Map<String, Double>> balanceList = finanasisSV.queryBalanceSheet(cusId,
				finaRepYear, finaRepMonth);
		List<Map<String, Double>> incomeList = finanasisSV.queryIncomeSheet(cusId,
				finaRepYear, finaRepMonth);
		
		for(int i=0; i<balanceList.size(); i++){
			
			Map<String, Double> balance = balanceList.get(i);
			Map<String, Double> income = incomeList.get(i);
			
			//非流动负债合计
			double longTermDebt = balance.get("负债合计") - balance.get("流动负债合计");
			balance.put("非流动负债合计", longTermDebt);
			//除固定资产外的其他非流动资产
			double otherLongTermAssets = balance.get("资产总计") - balance.get("流动资产合计") - balance.get("固定资产合计");
			balance.put("其他非流动资产", otherLongTermAssets);
			
			balance.putAll(income);
		}
		
		balanceList.remove(balanceList.size()-1);
		
		return balanceList;
	}
	
	public Map<String, Double> calCreditAmt(ApplyInfo applyInfo,
			List<Map<String, Double>> amtCalFinaData, int finaRepMonth){
		
		double incomeGrowthRate = 0.0;
		double otherChannelCapital = 0.0;
		double myBankLiquidLoanBal = 0.0;
		
		if(applyInfo != null){
			if(applyInfo.getIncomeGrowthRate() != null)
				incomeGrowthRate = applyInfo.getIncomeGrowthRate();
			
			if(applyInfo.getOtherChannelCapital() != null)
				otherChannelCapital = applyInfo.getOtherChannelCapital();
			
			if(applyInfo.getMyBankLiquidLoanBal() != null)
				myBankLiquidLoanBal = applyInfo.getMyBankLiquidLoanBal();
		}
		
		Map<String, Double> result = new HashMap<String, Double>();
		result.put("incomeGrowthRate", incomeGrowthRate);
		result.put("otherChannelCapital", otherChannelCapital);
		result.put("myBankLiquidLoanBal", myBankLiquidLoanBal);
		
		int lastYear = finaRepMonth==12?3:2;
		
		//上一完整年度收入
		double income = amtCalFinaData.get(lastYear).get("主营业务收入");
		double cost = amtCalFinaData.get(lastYear).get("主营业务成本");
		double profit = amtCalFinaData.get(lastYear).get("主营业务利润");
		result.put("income", income);
		
		double receive = amtCalFinaData.get(lastYear).get("应收账款");
		double lastReceive = amtCalFinaData.get(lastYear-1).get("应收账款");
		//应收账款周转次数
		double receiveTimes = 0.0;
		//应收账款周转天数
		double receiveDays = 0.0;
		if((receive + lastReceive) != 0.0 && income != 0.0){
			receiveTimes = income/(lastReceive/2 + receive/2);
			receiveDays = 360/receiveTimes;
		}
		result.put("receiveTimes", receiveTimes);
		result.put("receiveDays", receiveDays);
		
		double prePay = amtCalFinaData.get(lastYear).get("预付账款");
		double lastPrePay = amtCalFinaData.get(lastYear-1).get("预付账款");
		//预付账款周转次数
		double prePayTimes = 0.0;
		//预付账款周转天数
		double prePayDays = 0.0;
		if((prePay + lastPrePay) != 0.0 && cost != 0.0){
			prePayTimes = cost/(lastPrePay/2 + prePay/2);
			prePayDays = 360/prePayTimes;
		}
		result.put("prePayTimes", prePayTimes);
		result.put("prePayDays", prePayDays);
		
		double inventory = amtCalFinaData.get(lastYear).get("存货");
		double lastInventory = amtCalFinaData.get(lastYear-1).get("存货");
		//存货周转次数
		double inventoryTimes = 0.0;
		//存货周转天数
		double inventoryDays = 0.0;
		if((inventory + lastInventory) != 0.0 && cost != 0.0){
			inventoryTimes = cost/(lastInventory/2 + inventory/2);
			inventoryDays = 360/inventoryTimes;
		}
		result.put("inventoryTimes", inventoryTimes);
		result.put("inventoryDays", inventoryDays);
		
		double pay = amtCalFinaData.get(lastYear).get("应付账款");
		double lastPay = amtCalFinaData.get(lastYear-1).get("应付账款");
		//应付账款周转次数
		double payTimes = 0.0;
		//应付账款周转天数
		double payDays = 0.0;
		if((pay + lastPay) != 0.0 && cost != 0.0){
			payTimes = cost/(lastPay/2 + pay/2);
			payDays = 360/payTimes;
		}
		result.put("payTimes", payTimes);
		result.put("payDays", payDays);
		
		double preReceive = amtCalFinaData.get(lastYear).get("预收账款");
		double lastPreReceive = amtCalFinaData.get(lastYear-1).get("预收账款");
		//预收账款周转次数
		double preReceiveTimes = 0.0;
		//预收账款周转天数
		double preReceiveDays = 0.0;
		if((preReceive +lastPreReceive) != 0.0 && cost != 0.0){
			preReceiveTimes = cost/(lastPreReceive/2 + preReceive/2);
			preReceiveDays = 360/preReceiveTimes;
		}
		result.put("preReceiveTimes", preReceiveTimes);
		result.put("preReceiveDays", preReceiveDays);
		
		//营运资金周转次数
		double workCapitalTimes = 360/(receiveDays + prePayDays + inventoryDays
				+ payDays + preReceiveDays);
		if(Double.isInfinite(workCapitalTimes) || Double.isNaN(workCapitalTimes))
			workCapitalTimes = 0.0;
		result.put("workCapitalTimes", workCapitalTimes);
		
		//上年度销售利润率
		double annualProfitRate = profit/income;
		if(Double.isInfinite(annualProfitRate) || Double.isNaN(annualProfitRate))
			annualProfitRate = 0.0;
		result.put("annualProfitRate", annualProfitRate);
		
		//营运资金量
		double workLiquidityCapital = (income*(1+incomeGrowthRate)*(1-annualProfitRate))/workCapitalTimes;
		if(Double.isInfinite(workLiquidityCapital) || Double.isNaN(workLiquidityCapital))
			workLiquidityCapital = 0.0;
		result.put("workLiquidityCapital", workLiquidityCapital);
		
		//当期数据
		double totalAssets = amtCalFinaData.get(3).get("资产总计");
		double totalDebts = amtCalFinaData.get(3).get("负债合计");
		double ownerEquity = amtCalFinaData.get(3).get("所有者权益合计");
		
		//非流动负债
		double longTermDebt = amtCalFinaData.get(3).get("非流动负债合计");
		//固定资产
		double fixedAssets = amtCalFinaData.get(3).get("固定资产合计");
		//除固定资产外的其他非流动资产
		double otherLongTermAssets = amtCalFinaData.get(3).get("其他非流动资产");
		double shortTermLoans = amtCalFinaData.get(3).get("短期借款");
		
		result.put("shortTermLoans", shortTermLoans);
		result.put("totalAssets", totalAssets);
		result.put("totalDebts", totalDebts);
		
		//自有流动资金
		double ownLiquidityCapital = ownerEquity + longTermDebt - fixedAssets - otherLongTermAssets;
		result.put("ownLiquidityCapital", ownLiquidityCapital);
		
		//新增需求授信额度
		double newCreditLimit = workLiquidityCapital - ownLiquidityCapital - otherChannelCapital - shortTermLoans;
		result.put("newCreditLimit", newCreditLimit);
		
		//风险控制新增贷款额度
	/*	double newRiskCreditLimit = 0.0;
		if(((totalDebts+newCreditLimit)/(totalAssets+newCreditLimit)) < 0.7)
			newRiskCreditLimit = newCreditLimit;
		else{
			if(((totalAssets*0.7-totalDebts)/0.3)<(shortTermLoans*-1))
				newRiskCreditLimit = myBankLiquidLoanBal*-1;
			else
				newRiskCreditLimit = totalAssets*0.7-totalDebts;
		}
		result.put("newRiskCreditLimit", newRiskCreditLimit);
	*/
		
		//资产负债授信额度
		double deCreditLimit=(0.7*totalAssets-totalDebts)/0.3;
		result.put("deCreditLimit", deCreditLimit);
		
		Long taskId=applyInfo.getTaskId();
		
		
		//最终授信额度
		double  finalLimit=newCreditLimit;
		
		if(deCreditLimit<finalLimit)
			finalLimit=deCreditLimit;
		
		//是否符合简式计算
		double isSim=0;
		try {
			List<Guarantor>  guarantors=guaraInfoSV.findGuarantorByTaskId(taskId);
			List<MortgageInfo> mInfos= guaraInfoSV.findMortgageInfoByTaskId(taskId);
			List<PledgeInfo> pList=guaraInfoSV.findPledgeInfoByTaskId(taskId);
			
			//保证额度
			double guaraLimit=0;
			if(guarantors!=null&&guarantors.size()>0) {
				for(Guarantor data:guarantors) {
					guaraLimit=guaraLimit+data.getGuaraAmt();
				}
			}
			
			//抵押额度
			double mLimit=0;
			double mCredit=0;
			if(mInfos!=null&&mInfos.size()>0) {
				for(MortgageInfo data:mInfos) {
					mLimit=mLimit+data.getMortAmt();
					mCredit=mCredit+data.getConfirmValue();
				}
			}
			
			//质押额度
			double pLimit=0;
			if(pList!=null&&pList.size()>0) {
				for(PledgeInfo data:pList) {
					pLimit=pLimit+data.getPledAmt();
				}
			}
			
			//简氏计算法
			if(pLimit==0&&guaraLimit==0&&mCredit!=0) {
				if(mLimit/mCredit<=0.5) {
					finalLimit=mLimit;
					isSim=1;
				}				
			}else {
				if(mLimit+pLimit+guaraLimit<finalLimit)
					finalLimit=mLimit+pLimit+guaraLimit;
			}
			result.put("guaraLimit", guaraLimit);
			result.put("mLimit", mLimit);
			result.put("pLimit", pLimit);
			result.put("guaranteeLimit", mLimit+pLimit+guaraLimit);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		result.put("newRiskCreditLimit", finalLimit);
		result.put("isSim", isSim);
		
		return result;
	}

	@Override
	public Rate selectRateById(Long taskId) throws Exception {
		
		return rateDAO.selectRateById(taskId);
	}

	@Override
	public void saveRateInfo(String rateInfo) throws Exception {
		
		Rate rate=JacksonUtil.fromJson(rateInfo, Rate.class);
		Rate oldrate=rateDAO.selectRateById(rate.getTaskId());
		
		if(oldrate==null) {
			oldrate=rate;
		}else {
			oldrate.setModel(rate.getModel());
			oldrate.setCdRate(rate.getCdRate());
			oldrate.setWay(rate.getWay());
		}
		
		rateDAO.saveOrUpdate(oldrate);
	}
	
}
