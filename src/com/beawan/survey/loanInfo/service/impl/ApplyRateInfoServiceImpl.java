package com.beawan.survey.loanInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.loanInfo.dao.ApplyRateInfoDao;
import com.beawan.survey.loanInfo.bean.ApplyRateInfo;
import com.beawan.survey.loanInfo.service.ApplyRateInfoService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.loanInfo.bean.ApplyRateInfo;

/**
 * @author yzj
 */
@Service("applyRateInfoService")
public class ApplyRateInfoServiceImpl extends BaseServiceImpl<ApplyRateInfo> implements ApplyRateInfoService{
    /**
    * 注入DAO
    */
    @Resource(name = "applyRateInfoDao")
    public void setDao(BaseDao<ApplyRateInfo> dao) {
        super.setDao(dao);
    }
    @Resource
    public ApplyRateInfoDao applyRateInfoDao;
}
