package com.beawan.survey.loanInfo.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.beawan.survey.custInfo.dto.CompBaseDto;
import com.beawan.survey.custInfo.dto.CompBaseManagerDto;
import com.beawan.survey.custInfo.dto.PersonBaseDto;
import com.platform.util.*;
import org.springframework.stereotype.Service;

import com.beawan.analysis.abnormal.bean.ErrData;
import com.beawan.analysis.abnormal.service.IAbnormalService;
import com.beawan.analysis.finansis.bean.FnFixedAssetsBuilding;
import com.beawan.analysis.finansis.bean.FnFixedAssetsEquipment;
import com.beawan.analysis.finansis.bean.FnFixedAssetsLand;
import com.beawan.analysis.finansis.bean.FnInventory;
import com.beawan.analysis.finansis.bean.FnLongTermPay;
import com.beawan.analysis.finansis.bean.FnOtherPay;
import com.beawan.analysis.finansis.bean.FnOtherReceive;
import com.beawan.analysis.finansis.bean.FnPay;
import com.beawan.analysis.finansis.bean.FnPreReceive;
import com.beawan.analysis.finansis.bean.FnPrepay;
import com.beawan.analysis.finansis.bean.FnReceive;
import com.beawan.analysis.finansis.bean.FnReceiveStat;
import com.beawan.analysis.finansis.common.FinaRowData;
import com.beawan.analysis.finansis.dao.IReportDataDao;
import com.beawan.analysis.finansis.dao.IReportDataTRDao;
import com.beawan.analysis.finansis.service.IFinanasisService;
import com.beawan.analysis.finansis.service.IFnTableService;
import com.beawan.analysis.finansis.utils.InfoSortCaluTotal;
import com.beawan.base.entity.SysDic;
import com.beawan.base.entity.TableSubjCode;
import com.beawan.base.entity.User;
import com.beawan.base.service.ISysDicSV;
import com.beawan.base.service.ITableSubjCodeSV;
import com.beawan.base.service.IUserSV;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.util.SystemUtil;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.customer.service.ICusFSToolSV;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.beawan.survey.custInfo.bean.CompBaseDesc;
import com.beawan.survey.custInfo.bean.CompBaseEquity;
import com.beawan.survey.custInfo.bean.CompConnetCm;
import com.beawan.survey.custInfo.bean.CompFinance;
import com.beawan.survey.custInfo.bean.CompFinanceOperCF;
import com.beawan.survey.custInfo.bean.CompFinanceTax;
import com.beawan.survey.custInfo.bean.CompFinancing;
import com.beawan.survey.custInfo.bean.CompFinancingBank;
import com.beawan.survey.custInfo.bean.CompFinancingExtGuara;
import com.beawan.survey.custInfo.bean.CompRun;
import com.beawan.survey.custInfo.bean.CompRunArea;
import com.beawan.survey.custInfo.bean.CompRunConst;
import com.beawan.survey.custInfo.bean.CompRunConstSPDetail;
import com.beawan.survey.custInfo.bean.CompRunConstSPStruc;
import com.beawan.survey.custInfo.bean.CompRunCustomer;
import com.beawan.survey.custInfo.bean.CompRunEsta;
import com.beawan.survey.custInfo.bean.CompRunEstaDevedProj;
import com.beawan.survey.custInfo.bean.CompRunEstaDevingProj;
import com.beawan.survey.custInfo.bean.CompRunEstaLand;
import com.beawan.survey.custInfo.bean.CompRunFixed;
import com.beawan.survey.custInfo.bean.CompRunGeneral;
import com.beawan.survey.custInfo.bean.CompRunPower;
import com.beawan.survey.custInfo.bean.CompRunPrdSale;
import com.beawan.survey.custInfo.bean.CompRunProduce;
import com.beawan.survey.custInfo.bean.CompRunSupply;
import com.beawan.survey.custInfo.bean.PersonBase;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.beawan.survey.custInfo.service.ICompConnectSV;
import com.beawan.survey.custInfo.service.ICompFinanceSV;
import com.beawan.survey.custInfo.service.ICompFinancingSV;
import com.beawan.survey.custInfo.service.ICompRunSV;
import com.beawan.survey.loanInfo.bean.ApplyInfo;
import com.beawan.survey.loanInfo.bean.ApplyRate;
import com.beawan.survey.loanInfo.bean.ApplyScheme;
import com.beawan.survey.loanInfo.bean.Conclusion;
import com.beawan.survey.loanInfo.bean.CounterGuaraCom;
import com.beawan.survey.loanInfo.bean.CounterGuaraMort;
import com.beawan.survey.loanInfo.bean.EstaProjBase;
import com.beawan.survey.loanInfo.bean.FixedProjBase;
import com.beawan.survey.loanInfo.bean.FixedProjCashFow;
import com.beawan.survey.loanInfo.bean.FixedProjFinaPlan;
import com.beawan.survey.loanInfo.bean.FixedProjInvest;
import com.beawan.survey.loanInfo.bean.FixedProjInvestEst;
import com.beawan.survey.loanInfo.bean.FixedProjPlan;
import com.beawan.survey.loanInfo.bean.GuaraCom;
import com.beawan.survey.loanInfo.bean.GuaraPerson;
import com.beawan.survey.loanInfo.bean.GuaraProCom;
import com.beawan.survey.loanInfo.bean.GuaranteeInfo;
import com.beawan.survey.loanInfo.bean.Guarantor;
import com.beawan.survey.loanInfo.bean.MortgageInfo;
import com.beawan.survey.loanInfo.bean.PledgeInfo;
import com.beawan.survey.loanInfo.bean.RiskAnalysis;
import com.beawan.survey.loanInfo.service.IApplyInfoSV;
import com.beawan.survey.loanInfo.service.IConclusionSV;
import com.beawan.survey.loanInfo.service.IEstaProjSV;
import com.beawan.survey.loanInfo.service.IFixedProjSV;
import com.beawan.survey.loanInfo.service.IGuaraInfoSV;
import com.beawan.survey.loanInfo.service.ISurveyReportSV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;

/**
 * TODO 调查报告相关方法实现
 */
@Service("surveyReportSV")
public class SurveyReportSVImpl implements ISurveyReportSV {

	@Resource
	private ICompBaseSV compBaseSV;
	@Resource
	private ICompRunSV compRunSV;
	@Resource
	private IAbnormalService abnormalSV;
	@Resource
	private ISysDicSV sysDicSV;
	@Resource
	private ICompConnectSV compConnectSV;
	@Resource
	private ICompFinancingSV compFinancingSV;
	@Resource
	private ICompFinanceSV compFinanceSV;
	@Resource
	private IFnTableService fnTableSV;
	@Resource
	private IFixedProjSV fixedProjectSV;
	@Resource
	private IEstaProjSV estateProjSV;
	@Resource
	private IUserSV userSV;
	@Resource
	private ITaskSV taskSV;
	@Resource
	private IReportDataDao reportDataDao;
	@Resource
	private IReportDataTRDao reportDataTRDao;
	@Resource
	private IFinanasisService finanasisSV;
	@Resource
	private IApplyInfoSV applyInfoSV;
	@Resource
	private IGuaraInfoSV guarainfoSV;
	@Resource
	private IConclusionSV conclusionSV;
	@Resource
	private ICusBaseSV cusBaseSV;
	@Resource
	private ITableSubjCodeSV tableSubjCodeSV;
	@Resource
	private ICusFSToolSV cusFSToolSV;
	@Resource
	private ICmisInInvokeSV cmisInInvokeSV;
	
	@Override
	public void createSurveyReport(long taskId) throws Exception {
		
		Task task = taskSV.getTaskById(taskId);
		
		Map<String, Object> dataMap = this.getReportData(task);
		
		String templateFileName = (String) dataMap.get("templateFileName");
		
		String destPath = PropertiesUtil.getProperty(Constants.SYSTEM_PROPS_PATH, "survey.report.path");
		FreeMarkerWordUtil.createWord(dataMap, templateFileName, destPath, task.getSerNo() + ".doc");
	}
	
	@Override
	public Map<String, Object> getReportData(long taskId) throws Exception {
		
		Task task = taskSV.getTaskById(taskId);
		
		return this.getReportData(task);
	}
	
	/**
	 * TODO 获得报告数据集合
	 * @param task 任务对象
	 * @return
	 * @throws Exception
	 */
	private Map<String, Object> getReportData(Task task) throws Exception {
		
		Map<String, Object> dataMap = null;
		
		String businessNo = task.getBusinessNo();
		
		String templateFileName = "";
		
		if (SysConstants.LoanTypeConstant.LOAN_LIQUIDITY_MANUFACT.equals(businessNo)) {					
			
			templateFileName = "loan_manfacturing.xml";
			dataMap = this.getLiquidityData(task);
			
		}else if(SysConstants.LoanTypeConstant.LOAN_LIQUIDITY_AFP.equals(businessNo)) {
			
			templateFileName = "loan_liquidity_afp.xml";
			dataMap = this.getLiquidityData(task);
		
		}else if(SysConstants.LoanTypeConstant.LOAN_LIQUIDITY_TEXTILE.equals(businessNo)) {
			
			templateFileName = "loan_liquidity_textile.xml";
			dataMap = this.getLiquidityData(task);
			
		}else if(SysConstants.LoanTypeConstant.LOAN_LIQUIDITY_WRT.equals(businessNo)) {
			
			templateFileName = "loan_retail.xml";
			dataMap = this.getRetailData(task);
		
		} else if(SysConstants.LoanTypeConstant.LOAN_GENERAL.equals(businessNo)) {
			
			templateFileName = "loan_general.xml";
			dataMap = this.getGeneralData(task);
		
		} else if (SysConstants.LoanTypeConstant.LOAN_LIQUIDITY_CONST.equals(businessNo)) {
			
			templateFileName = "loan_construction.xml";
			dataMap = this.getConstructData(task);
		
		} else if (SysConstants.LoanTypeConstant.LOAN_ESTATE.equals(businessNo)) {
			
			templateFileName = "loan_estate.xml";
			dataMap = this.getEstateData(task);
			
		}else if(SysConstants.LoanTypeConstant.LOAN_FIXED_ASSETS.equals(businessNo)){
			
			templateFileName = "loan_fixedAsset.xml";
			dataMap = this.getFixedAssetData(task);
		}
		
		if(dataMap == null)
			dataMap = new HashMap<String, Object>();
		
		dataMap.put("templateFileName", templateFileName);
		
		return dataMap;
	}
		
	/**
	 * @Description 获得一般流动资金贷款（包括通用制造业、纺织业、农副食品加工业三个模板）报告数据集合
	 * @param task 任务对象
	 * @throws Exception
	 */
	private Map<String, Object> getLiquidityData(Task task) throws Exception{
		
		long taskId = task.getId();
		
		Map<String, Object> dataMap = this.getCommonData(task);
		
		String compCusNo = task.getCustomerNo();
		if(task.getBusinessNo().equals(SysConstants.LoanTypeConstant.LOAN_PERSONAL)) {
			compCusNo = task.getCmBaseId();
		}
		
		CompRun compRun = compRunSV.queryCompRunByTIdAndNo(taskId, compCusNo);
		if(compRun==null) {
			compRun=new CompRun();
		}
		dataMap.put("compRun", compRun);
		
		List<CompRunPower> powers = compRunSV.queryRunPowerByTIdAndNo(taskId, compCusNo);
		dataMap.put("powers", powers);

		List<CompRunSupply> supplies=compRunSV.queryRunSupplyByTIdAndNo(taskId, compCusNo);
		dataMap.put("supplies", supplies);
		
		CompRunProduce produces=compRunSV.queryRunProduceByTIdAndNo(taskId, compCusNo);
		if(produces==null) {
			produces=new CompRunProduce();
		}
		dataMap.put("produces", produces);	
		
		List<CompRunPower> oPowers=compRunSV.queryRunPowerByTIdAndNo(taskId, compCusNo);
		
		List<CompRunPower> powers1=new ArrayList<CompRunPower>();
		List<CompRunPower> powers2=new ArrayList<CompRunPower>();
		List<CompRunPower> powers3=new ArrayList<CompRunPower>();
		List<CompRunPower> powers4=new ArrayList<CompRunPower>();
		List<CompRunPower> powers5=new ArrayList<CompRunPower>();
		if(oPowers==null) {
			oPowers=new ArrayList<CompRunPower>();
		}
		for(CompRunPower data:oPowers) {
			
			String code=data.getItemCode();
			if (code!=null) {
				
			if (code.equals("01")) {
				powers1.add(data);
			}else if(code.equals("02")) {
				powers2.add(data);
			}else if(code.equals("03")){
				powers3.add(data);
			}else if(code.equals("04")){
				powers4.add(data);
			}else if(code.equals("05")){
				powers5.add(data);
			}else;
		
			}
		}
		while(powers1.size()<6) {
			powers1.add(new CompRunPower());
		}
		while(powers2.size()<6) {
			powers2.add(new CompRunPower());
		}
		while(powers3.size()<6) {
			powers3.add(new CompRunPower());
		}
		while(powers4.size()<6) {
			powers4.add(new CompRunPower());
		}
		while(powers5.size()<6) {
			powers5.add(new CompRunPower());
		}
		dataMap.put("powers1", powers1);
		dataMap.put("powers2", powers2);
		dataMap.put("powers3", powers3);
		dataMap.put("powers4", powers4);
		dataMap.put("powers5", powers5);
		
		String powerYearMonth=compRun.getEnergyStatMonth();
		if (null!=powerYearMonth&&!("").equals(powerYearMonth)) {
			String []powerDate=powerYearMonth.split("-");
			List<String> halfyear=new ArrayList<String>();
			int powyear=Integer.parseInt(powerDate[0]);
			int powmonth=Integer.parseInt(powerDate[1]);
			for(int i=0;i<6;i++) {
				int month=powmonth+i-5;
				int year=powyear;
				if (month<1) {
					month=month+12;
					year=year-1;
				}
				halfyear.add(year+"年"+month+"月");
			}
			dataMap.put("halfyear", halfyear);
		}
				
		return dataMap;
	}
	
	/**
	 * @Description 获得通用报告数据集合
	 * @param task 任务对象
	 * @throws Exception
	 */
	private Map<String, Object> getGeneralData(Task task) throws Exception{
		
		long taskId = task.getId();
			
		Map<String, Object> dataMap = this.getCommonData(task);
		
		String compCusNo = task.getCustomerNo();
		if(task.getBusinessNo().equals(SysConstants.LoanTypeConstant.LOAN_PERSONAL)) {
			compCusNo = task.getCmBaseId();
		}
		
		CompRunGeneral compRunGeneral=compRunSV.queryRunGeneralByTIdAndNo(taskId, compCusNo);
		if(compRunGeneral==null) {
			compRunGeneral=new CompRunGeneral();
		}
		dataMap.put("compRunGeneral", compRunGeneral);
		
		List<CompRunPower> powers = compRunSV.queryRunPowerByTIdAndNo(taskId, compCusNo);
		dataMap.put("powers", powers);

		List<CompRunSupply> supplies=compRunSV.queryRunSupplyByTIdAndNo(taskId, compCusNo);
		dataMap.put("supplies", supplies);
		
		CompRunProduce produces=compRunSV.queryRunProduceByTIdAndNo(taskId, compCusNo);
		if(produces==null) {
			produces=new CompRunProduce();
		}
		dataMap.put("produces", produces);	
		
		List<CompRunPower> oPowers=compRunSV.queryRunPowerByTIdAndNo(taskId, compCusNo);
		
		List<CompRunPower> powers1=new ArrayList<CompRunPower>();
		List<CompRunPower> powers2=new ArrayList<CompRunPower>();
		List<CompRunPower> powers3=new ArrayList<CompRunPower>();
		List<CompRunPower> powers4=new ArrayList<CompRunPower>();
		List<CompRunPower> powers5=new ArrayList<CompRunPower>();
		if(oPowers==null) {
			oPowers=new ArrayList<CompRunPower>();
		}
		for(CompRunPower data:oPowers) {
			
			String code=data.getItemCode();
			if (code!=null) {
				
			if (code.equals("01")) {
				powers1.add(data);
			}else if(code.equals("02")) {
				powers2.add(data);
			}else if(code.equals("03")){
				powers3.add(data);
			}else if(code.equals("04")){
				powers4.add(data);
			}else if(code.equals("05")){
				powers5.add(data);
			}else;
		
			}
		}
		while(powers1.size()<6) {
			powers1.add(new CompRunPower());
		}
		while(powers2.size()<6) {
			powers2.add(new CompRunPower());
		}
		while(powers3.size()<6) {
			powers3.add(new CompRunPower());
		}
		while(powers4.size()<6) {
			powers4.add(new CompRunPower());
		}
		while(powers5.size()<6) {
			powers5.add(new CompRunPower());
		}
		dataMap.put("powers1", powers1);
		dataMap.put("powers2", powers2);
		dataMap.put("powers3", powers3);
		dataMap.put("powers4", powers4);
		dataMap.put("powers5", powers5);
		
		String powerYearMonth=compRunGeneral.getEnergyStatMonth();
		if (null!=powerYearMonth&&!("").equals(powerYearMonth)) {
			String []powerDate=powerYearMonth.split("-");
			List<String> halfyear=new ArrayList<String>();
			int powyear=Integer.parseInt(powerDate[0]);
			int powmonth=Integer.parseInt(powerDate[1]);
			for(int i=0;i<6;i++) {
				int month=powmonth+i-5;
				int year=powyear;
				if (month<1) {
					month=month+12;
					year=year-1;
				}
				halfyear.add(year+"年"+month+"月");
			}
			dataMap.put("halfyear", halfyear);
		}
				
		return dataMap;
	}
	
	/**
	 * @Description 获得流动资金贷款（建筑业）报告数据集合
	 * @param task 任务对象
	 * @throws Exception
	 */
	private Map<String, Object> getConstructData(Task task) throws Exception{
		
		long taskId = task.getId();
			
		Map<String, Object> dataMap = this.getCommonData(task);
		
		String compCusNo = task.getCustomerNo();
		if(task.getBusinessNo().equals(SysConstants.LoanTypeConstant.LOAN_PERSONAL)) {
			compCusNo = task.getCmBaseId();
		}
		
		CompRunConst runConst = compRunSV.queryRunConstByTIdAndNo(taskId, compCusNo);
		if(runConst==null) {
			runConst=new CompRunConst();
		}
		List<CompRunConstSPDetail> spDetails=compRunSV.queryRunConstSPDetailByTIdAndNo(taskId, compCusNo);
		List<CompRunConstSPStruc> spStrucs=compRunSV.queryRunConstSPStrucByTIdAndNo(taskId, compCusNo);
		dataMap.put("runConst", runConst);
		dataMap.put("spDetails", spDetails);
		dataMap.put("spStrucs", spStrucs);
		
		return dataMap;
	}
	
	/**
	 * @Description 获得房地产开发贷款报告数据集合
	 * @param task 任务对象
	 * @throws Exception
	 */
	private Map<String, Object> getEstateData(Task task) throws Exception{
		
		long taskId = task.getId();
						
		Map<String, Object> dataMap = this.getCommonData(task);
		
		String compCusNo = task.getCustomerNo();
		if(task.getBusinessNo().equals(SysConstants.LoanTypeConstant.LOAN_PERSONAL)) {
			compCusNo = task.getCmBaseId();
		}
		
		CompRunEsta runEstate = compRunSV.queryRunEstaByTIdAndNo(taskId, compCusNo);
		if(runEstate==null) {
			runEstate=new CompRunEsta();
		}
		dataMap.put("runEstate", runEstate);
		
		InfoSortCaluTotal caluTotal = new InfoSortCaluTotal();
		
		List<CompRunEstaLand> runLands = compRunSV.queryRunEstaLandByTIdAndNo(taskId,compCusNo);
		runLands = caluTotal.sortEatateLand(runLands);
		dataMap.put("runLands", runLands);
		dataMap.put("runLandsRows", runLands.size());
		
		List<CompRunEstaDevedProj> runProjDeveds = compRunSV.queryRunEstaDevedProjByTIdAndNo(taskId, compCusNo);
		runProjDeveds = caluTotal.sortRunProjDevieds(runProjDeveds);
		dataMap.put("runProjDeveds", runProjDeveds);
		dataMap.put("runProjDevedsRows", runProjDeveds.size());
		
		List<CompRunEstaDevingProj> runProjDeveings = compRunSV.queryRunEstaDevingProjByTIdAndNo(taskId, compCusNo);
		runProjDeveings = caluTotal.sortRunProjDevings(runProjDeveings);
		dataMap.put("runProjDeveings", runProjDeveings);
		dataMap.put("runProjDeveingsRows", runProjDeveings.size());
		
		EstaProjBase projBase = estateProjSV.findProjBaseByTaskId(taskId);
		if(projBase==null) {
			projBase=new EstaProjBase();
		}
		dataMap.put("projBase", projBase);
		
		return dataMap;
	}
	
	/**
	 * @Description 获得固定资产贷款报告数据集合
	 * @param task 任务对象
	 * @throws Exception
	 */
	private Map<String, Object> getFixedAssetData(Task task) throws Exception{
		
		long taskId = task.getId();
		
		Map<String, Object> dataMap = this.getCommonData(task);
		
		String compCusNo = task.getCustomerNo();
		if(task.getBusinessNo().equals(SysConstants.LoanTypeConstant.LOAN_PERSONAL)) {
			compCusNo = task.getCmBaseId();
		}
		
		CompRunFixed compRunFixed=compRunSV.queryRunFixedByTIdAndNo(taskId, compCusNo);
		if(compRunFixed==null) {
			compRunFixed=new CompRunFixed();
		}
		dataMap.put("compRunFixed", compRunFixed);
		
		FixedProjBase fProjBase=fixedProjectSV.queryProjBaseByTaskId(taskId);
		if(fProjBase==null) {
			fProjBase=new FixedProjBase();
		}
		dataMap.put("fProjBase", fProjBase);
		
		List<FixedProjPlan> fixedProjPlans=fixedProjectSV.queryProjPlanByTaskId(taskId);
		dataMap.put("fixedProjPlans", fixedProjPlans);
		
		List<FixedProjInvest> fInvests=fixedProjectSV.queryProjInvestByTaskId(taskId);
		if(null==fInvests||fInvests.size()==0) {
			List<SysDic> finmemos=sysDicSV.findByType("STD_FIXED_INVEST");
			for(SysDic data:finmemos) {
				FixedProjInvest fing1=new FixedProjInvest();
				fing1.setItemCode(data.getEnName());
				fing1.setItemName(data.getCnName());
				fing1.setInvestedAmt(0.0);
				fing1.setPaidAmt(0.0);
				fing1.setPlanInvestAmt(0.0);
				fInvests.add(fing1);
			}
		}
		double sum1=0,sum2=0,sum3=0;		
		for(FixedProjInvest data:fInvests) {
			if(null!=data.getPlanInvestAmt()) {
			sum1=sum1+data.getPlanInvestAmt();
			}
			if(null!=data.getInvestedAmt()) {
			sum2=sum2+data.getInvestedAmt();
			}
			if(null!=data.getPaidAmt()) {
			sum3=sum3+data.getPaidAmt();
			}
		}
		FixedProjInvest newinvest=new FixedProjInvest();
		newinvest.setItemName("合计");
		newinvest.setPlanInvestAmt(sum1);
		newinvest.setInvestedAmt(sum2);
		newinvest.setPaidAmt(sum3);
		fInvests.add(newinvest);
		dataMap.put("fInvests", fInvests);
		
		List<FixedProjInvestEst> investEsts=fixedProjectSV.queryProjInvestEstByTaskId(taskId);
		if(null==investEsts||investEsts.size()<=0) {
			List<SysDic> ests=sysDicSV.findByType("STD_FIXED_INVESTEST");
			for(SysDic data:ests) {
				FixedProjInvestEst est=new FixedProjInvestEst();
				est.setItemCode(data.getEnName());
				est.setItemName(data.getCnName());
				investEsts.add(est);
			}
		}
		dataMap.put("investEsts", investEsts);
		
		List<FixedProjFinaPlan> fixedProjFinaPlans=fixedProjectSV.queryProjFinaPlanByTaskId(taskId);
		if(null==fixedProjFinaPlans||fixedProjFinaPlans.size()<=0) {
			List<SysDic> plans=sysDicSV.findByType("STD_FINAPLAN_TYPE");
			for(SysDic data:plans) {
				FixedProjFinaPlan plan=new FixedProjFinaPlan();
				plan.setItemCode(data.getEnName());
				plan.setItemName(data.getCnName());
				fixedProjFinaPlans.add(plan);
			}
		}
		double projsum1=0,projsum2=0;
		for(FixedProjFinaPlan data:fixedProjFinaPlans) {
			double c1=0;
			double c2=0;
			if (data.getReceiveAmt()!=null) {
				c1=data.getReceiveAmt();
			}
			if(data.getUnReceiveAmt()!=null) {
				c2=data.getUnReceiveAmt();
			}
			projsum1=projsum1+c1;
			projsum2=projsum2+c2;
		}
		FixedProjFinaPlan finaPlan=new FixedProjFinaPlan();
		finaPlan.setItemName("合计");
		finaPlan.setReceiveAmt(projsum1);
		finaPlan.setUnReceiveAmt(projsum2);
		fixedProjFinaPlans.add(finaPlan);
		dataMap.put("fixedProjFinaPlans", fixedProjFinaPlans);
		
		String fyear="";
		String fyear1="";
		String fyear2="";
		if(fProjBase.getConsEndYear()!=null&&!fProjBase.equals("")) {
			fyear=fProjBase.getConsEndYear();
			fyear1=DateUtil.dateCountYearNoMonth(fProjBase.getConsEndYear(),1);
			fyear2=DateUtil.dateCountYearNoMonth(fProjBase.getConsEndYear(),2);
		}
		dataMap.put("fyear", fyear);
		dataMap.put("fyear1", fyear1);
		dataMap.put("fyear2", fyear2);
		
		List<FixedProjCashFow> fixedProjCashFows=fixedProjectSV.queryProjCashFowByTaskId(taskId);	
		List<SysDic> cashflowsbs=sysDicSV.findByType("STD_CASHFLOW_TYPE");		
		List<FixedProjCashFow> fixedcash1=new ArrayList<FixedProjCashFow>();
		List<FixedProjCashFow> fixedcash2=new ArrayList<FixedProjCashFow>();
		List<FixedProjCashFow> fixedcash3=new ArrayList<FixedProjCashFow>();		
		for(FixedProjCashFow data:fixedProjCashFows) {
			
			if(data.getYear().equals(fyear)) {
				fixedcash1.add(data);
			}else if(data.getYear().equals(fyear1)){
				fixedcash2.add(data);
			}else if(data.getYear().equals(fyear2)) {
				fixedcash3.add(data);
			}
		}
		dataMap.put("fixedcash1", fixedcash1);
		dataMap.put("fixedcash2", fixedcash2);
		dataMap.put("fixedcash3", fixedcash3);
		dataMap.put("cashflowsbs", cashflowsbs);
		
		return dataMap;
	}
		
	
	/**
	 * @Description 获得流动资金贷款（批发零售业）报告数据集合
	 * @param task 任务对象
	 * @throws Exception
	 */
	private Map<String, Object> getRetailData(Task task) throws Exception{
		
		long taskId = task.getId();
		
		Map<String, Object> dataMap = this.getCommonData(task);
		
		String compCusNo = task.getCustomerNo();
		if(task.getBusinessNo().equals(SysConstants.LoanTypeConstant.LOAN_PERSONAL)) {
			compCusNo = task.getCmBaseId();
		}
		
		CompRun compRun = compRunSV.queryCompRunByTIdAndNo(taskId, compCusNo);
		if(compRun==null) {
			compRun=new CompRun();
		}
		dataMap.put("compRun", compRun);
		
		List<CompRunPower> powers = compRunSV.queryRunPowerByTIdAndNo(taskId, compCusNo);
		dataMap.put("powers", powers);

		List<CompRunSupply> supplies=compRunSV.queryRunSupplyByTIdAndNo(taskId, compCusNo);
		dataMap.put("supplies", supplies);
		
		return dataMap;
	}
	
	/**
	 * @Description 获得报告公共部分数据集合
	 * @param task 调查任务对象
	 * @throws Exception
	 */
	private Map<String, Object> getCommonData(Task task) throws Exception{
		
		long taskId = task.getId();
		
		Map<String, Object> dataMap = new HashMap<String, Object>();
		
		dataMap.put("task", task);

		dataMap.put("finAvail", SysConstants.CusFinaValidFlag.AVAIL);
		
		//企业客户号
		String compCusNo = task.getCustomerNo();
		if(task.getBusinessNo().equals(SysConstants.LoanTypeConstant.LOAN_PERSONAL)) {
			compCusNo = task.getCmBaseId();
		}
		int finaRepYear = task.getYear();
		int finaRepMonth = task.getMonth();
		
		//调查报告中显示的财报科目列表
		List<TableSubjCode> finaSubjects = tableSubjCodeSV.queryByTableSubjCodes("all", "FINA_01");
		dataMap.put("finaSubjects", finaSubjects);
		
		User user = userSV.queryById(task.getUserNo());
		dataMap.put("userName", user.getUserName());

		CompBaseDto compBase = compBaseSV.findCompBaseByCustNo(compCusNo);
		if(null==compBase) {
			compBase=new CompBaseDto();
		}
		dataMap.put("compBase", compBase);

		CompBaseDesc compBaseDesc = compBaseSV.findCompBaseDescByCustNo(compCusNo);
		if(null==compBaseDesc) {
			compBaseDesc=new CompBaseDesc();
		}
		dataMap.put("compBaseDesc", compBaseDesc);

		InfoSortCaluTotal caluTotal = new InfoSortCaluTotal();

		List<CompBaseEquity> equities = compBaseSV.findCompEquityByCustNo(compCusNo);
		equities = caluTotal.sortBaseEquity(equities);
		dataMap.put("equities", equities);

		List<CompBaseManagerDto> managers = compBaseSV.findCompManagerByCustNo(compCusNo);

		for (CompBaseManagerDto manager : managers) {
			PersonBase pBase = compBaseSV.findPersonBaseByCustNo(manager.getPsCusNo());
			manager.setPersonBase(MapperUtil.trans(pBase, PersonBaseDto.class));
		}
		dataMap.put("managers", managers);
		
		List<CompRunArea> runAreas = compRunSV.queryRunAreaByTIdAndNo(taskId, compCusNo);
		dataMap.put("runAreas", runAreas);
		
		List<CompConnetCm> connetCms=compConnectSV.findCompConnectCmByCustNo(compCusNo);
		dataMap.put("connetCms", connetCms);
		
		CompFinancing financing = compFinancingSV.findCompFinancingByTIdAndNo(taskId, compCusNo);
		if(financing==null) {
			financing=new CompFinancing();
		}
		dataMap.put("financing", financing);

		CompFinance finance = compFinanceSV.findCompFinanceByTIdAndNo(taskId, compCusNo);
		if(finance==null) {
			finance=new CompFinance();
			finance.setTaskId(taskId);
			finance.setCustomerNo(compCusNo);
		}
		
		String ratioAnalysis = finance.getRatioAnalysis();
		if(StringUtil.isEmptyString(ratioAnalysis)&&
				SysConstants.CusFinaValidFlag.AVAIL.equals(task.getFinaSyncFlag())){
			//获得系统分析结论(比率指标分析)
			Map<String, Object> conclusion = finanasisSV.genAnalysisConclusion(taskId, compCusNo,
					SysConstants.SysFinaAnalyPart.RATIO_INDEX);
			ratioAnalysis = "盈利能力：" + conclusion.get("RatioOne").toString()
					      + "偿债能力：" + conclusion.get("RatioTwo").toString()
					      + "运营能力：" + conclusion.get("RatioThree").toString()
					      + "增长能力：" + conclusion.get("RatioFour").toString();
			
			finance.setRatioAnalysis(ratioAnalysis);
		}	
		
		
		String cashFlowAnaly = finance.getCashFlowAnaly();
		if(StringUtil.isEmptyString(cashFlowAnaly)&&
				SysConstants.CusFinaValidFlag.AVAIL.equals(task.getFinaSyncFlag())){
			//获得系统分析结论(现金流量分析)
			Map<String, Object> conclusion = finanasisSV.genAnalysisConclusion(taskId, compCusNo,
					SysConstants.SysFinaAnalyPart.CASH_FLOW);
			cashFlowAnaly = conclusion.get("first").toString()
					      + conclusion.get("third").toString()
					      + conclusion.get("four").toString()
					      + conclusion.get("five").toString()
					      + conclusion.get("seven").toString();
			
			finance.setCashFlowAnaly(cashFlowAnaly);
		}
		
		String incomeAnaly = finance.getIncomeAnaly();
		if(StringUtil.isEmptyString(incomeAnaly)&&
				SysConstants.CusFinaValidFlag.AVAIL.equals(task.getFinaSyncFlag())){
			//获得系统分析结论(盈利情况分析)
			Map<String, Object> conclusion = finanasisSV.genAnalysisConclusion(taskId, compCusNo,
					SysConstants.SysFinaAnalyPart.INCOME);
			incomeAnaly = conclusion.get("Incsecond").toString()
						+ conclusion.get("IncsecondThree").toString()
					    + conclusion.get("IncThird").toString()
					    + conclusion.get("IncFour").toString()
					    + conclusion.get("IncFive").toString()
					    + conclusion.get("IncSix").toString();
			
			finance.setIncomeAnaly(incomeAnaly);
		}
		
		dataMap.put("finance", finance);

		
		ApplyInfo applyInfo=applyInfoSV.findApplyInfoByTaskId(taskId);
		if(applyInfo==null) {
			applyInfo=new ApplyInfo();
		}
		dataMap.put("applyInfo", applyInfo);

		List<String> infoList = cmisInInvokeSV.getLastCreditInfo(task.getCustomerNo());
		dataMap.put("lastCreditInfoList", infoList);
		
		
		
		
		//显示的财报科目列表(用于额度计算)
		List<TableSubjCode> applyfinaSubjects = tableSubjCodeSV.queryByTableSubjCodes("all", "FINA_AMT");
		dataMap.put("applyfinaSubjects", applyfinaSubjects);
		
		//额度测算结果
		Map<String, Object> calResult = applyInfoSV.getCreditAmtCalResult(task, applyInfo);
		if(calResult != null) {
			dataMap.put("amtCalFinaData", calResult.get("amtCalFinaData"));
			dataMap.put("amtCalResult", calResult.get("amtCalResult"));
		}
				
				
		List<CompRunCustomer> runCustomers=compRunSV.queryRunCustomerByTIdAndNo(taskId, compCusNo);
		
		dataMap.put("runCustomers", runCustomers);
		
		List<CompRunPrdSale> compRunPrdSales=compRunSV.queryRunPrdSaleByTIdAndNo(taskId, compCusNo);
		dataMap.put("compRunPrdSales", compRunPrdSales);
		
		List<String> prdSaleName=new ArrayList<String>();
		
		for(CompRunPrdSale data:compRunPrdSales) {
			String itemYear=data.getYear();
			if(itemYear.equals(String.valueOf(finaRepYear - 1))) {
				prdSaleName.add(data.getProductName());
			}
		}
		
		dataMap.put("prdSaleName", prdSaleName);
		
		
		List<FnPay> pays = fnTableSV.findPayByTId(taskId);
		ErrData paysErr=abnormalSV.getItemErrorData(taskId, "应付账款");
		pays = caluTotal.sortPays(pays);
		dataMap.put("pays", pays);
		if(null!=paysErr) {
		dataMap.put("paysAnly", paysErr.getExplain());
		}else {
			dataMap.put("paysAnly", "");	
		}
		List<FnPreReceive> preReceives = fnTableSV.findPreReceiveByTId(taskId);
		ErrData preReceivesErr=abnormalSV.getItemErrorData(taskId, "预收账款");
		preReceives = caluTotal.sortPreReceives(preReceives);
		dataMap.put("preReceives", preReceives);
		if(null!=preReceivesErr) {
		dataMap.put("preReceivesAnly", preReceivesErr.getExplain());
		}else {
			dataMap.put("preReceivesAnly", "");
		}
		List<FnReceive> receives = fnTableSV.findReceiveByTId(taskId);
		List<FnReceiveStat> receiveStates=fnTableSV.findReceiveStatByTId(taskId);
		ErrData receivesErr=abnormalSV.getItemErrorData(taskId, "应收账款");
		dataMap.put("receives", receives);
		dataMap.put("receiveStates", receiveStates);
		if(null!=receivesErr) {
		dataMap.put("receivesAnly", receivesErr.getExplain());
		}else {
			dataMap.put("receivesAnly", "");
		}
		List<FnPrepay> prepays = fnTableSV.findPrepayByTId(taskId);
		ErrData prepaysErr=abnormalSV.getItemErrorData(taskId, "预付账款");
		prepays = caluTotal.sortPrepays(prepays);
		dataMap.put("prepays", prepays);
		if(null!=prepaysErr) {
		dataMap.put("prepaysAnly", prepaysErr.getExplain());
		}else {
			dataMap.put("prepaysAnly", "");
		}
		List<FnLongTermPay> longTermPays = fnTableSV.findLongTermPayByTId(taskId);
		ErrData longTermPaysErr=abnormalSV.getItemErrorData(taskId, "长期应付账款");
		dataMap.put("longTermPays", longTermPays);
		if(null!=longTermPaysErr) {
		dataMap.put("longTermPaysAnly", longTermPaysErr.getExplain());
		}else {
			dataMap.put("longTermPaysAnly", "");
		}
		List<FnOtherPay> connOtherPays = fnTableSV.findOtherPayByTId(taskId);
		ErrData connOtherPaysErr=abnormalSV.getItemErrorData(taskId, "其他应付款");
		dataMap.put("connOtherPays", connOtherPays);
		if(null!=connOtherPaysErr) {
		dataMap.put("connOtherPaysAnly", connOtherPaysErr.getExplain());
		}else {
			dataMap.put("connOtherPaysAnly", "");
		}
		
		List<FnOtherReceive> connOtherReceives = fnTableSV.findOtherReceiveByTId(taskId);
		ErrData connOtherReceivesErr=abnormalSV.getItemErrorData(taskId, "其他应收款");
		dataMap.put("connOtherReceives", connOtherReceives);
		if(null!=connOtherReceivesErr) {
		dataMap.put("connOtherReceivesAnly", connOtherReceivesErr.getExplain());
		}else {
			dataMap.put("connOtherReceivesAnly", "");
		}
		List<FnInventory> fnInventories=fnTableSV.findFnInventoryByTId(taskId);
		ErrData fnInventoriesErr=abnormalSV.getItemErrorData(taskId, "存货");
		dataMap.put("fnInventories", fnInventories);
		if(null!=fnInventoriesErr) {
		dataMap.put("fnInventoriesAnly", fnInventoriesErr.getExplain());
		}else {
			dataMap.put("fnInventoriesAnly", "");
		}
		ErrData fnfixedErr=abnormalSV.getItemErrorData(taskId, "固定资产");
		if(null!=fnfixedErr) {
		dataMap.put("fnfixedAnly", fnfixedErr.getExplain());
		}else {
			dataMap.put("fnfixedAnly","");
		}
		List<FnFixedAssetsLand> fnLands=fnTableSV.queryFnFixedAssetsLandByTId(taskId);
		dataMap.put("fnLands", fnLands);
		
		List<FnFixedAssetsBuilding> fnBuildings=fnTableSV.queryFnFixedAssetsBuildingByTId(taskId);
		dataMap.put("fnBuildings", fnBuildings);
		
		List<FnFixedAssetsEquipment> fnequips=fnTableSV.queryFnFixedAssetsEquipmentByTId(taskId);
		dataMap.put("fnequips", fnequips);
		
		List<CompFinancingBank> mfinancingBanks=compFinancingSV.findCompFinaBankByTIdAndNo(taskId, compCusNo, "M");
		dataMap.put("mfinancingBanks", mfinancingBanks);
		
		List<CompFinancingBank> ofinancingBanks=compFinancingSV.findCompFinaBankByTIdAndNo(taskId, compCusNo, "O");
		dataMap.put("ofinancingBanks", ofinancingBanks);
		
		List<CompFinancingExtGuara> financingExtGuaras=compFinancingSV.findCompFinaExtGuaraByTIdAndNo(taskId, compCusNo);
		dataMap.put("financingExtGuaras", financingExtGuaras);
		
		List<ApplyScheme> applySchemes = applyInfoSV.findApplySchemeListByTaskId(taskId);
		dataMap.put("applySchemes", applySchemes);
		
		double schemesSum=0;
		if(applySchemes!=null) {
		for(ApplyScheme data:applySchemes) {
			schemesSum=schemesSum+data.getCreditAmt();
		}
		}
		dataMap.put("schemesSum", schemesSum);
		List<ApplyRate> applyRates=applyInfoSV.queryApplyRateListByTaskId(taskId);
		dataMap.put("applyRates", applyRates);
		GuaranteeInfo guaranteeInfo=guarainfoSV.findGuaraInfoBytaskId(taskId);
		if(guaranteeInfo==null) {
			guaranteeInfo=new GuaranteeInfo(taskId);
		}
		dataMap.put("guaranteeInfo", guaranteeInfo);
		Boolean showBZ=false,showZY=false,showDY=false,showQT=false;
		if(guaranteeInfo!=null&&guaranteeInfo.getGuaraType()!=null) {
		String [] wagArray=guaranteeInfo.getGuaraType().split(",");
		List<String> waylist=new ArrayList<String>();
		for(int i=0;i<wagArray.length;i++) {
			waylist.add(wagArray[i]);
		}
		if(waylist.contains(SysConstants.GuaraWay.BZ)) {
			showBZ=true;
		}
		if(waylist.contains(SysConstants.GuaraWay.ZY)) {
			showZY=true;
		}
		if(waylist.contains(SysConstants.GuaraWay.DY)) {
			showDY=true;
		}
		if(waylist.contains(SysConstants.GuaraWay.QT)) {
			showQT=true;
		}
		dataMap.put("waylist", waylist);
		}
		dataMap.put("showBZ", showBZ);
		dataMap.put("showZY", showZY);
		dataMap.put("showDY", showDY);
		dataMap.put("showQT", showQT);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date dt = sdf.parse(task.getApplicationTime());//将字符串生成Date
        String applyTime = sdf.format(dt);//将给定的 Date 格式化为日期/时间字符串，并将结果添加到给定的 StringBuffer。
		dataMap.put("applyTime", applyTime);
		List<Guarantor> guaranteeInfos=guarainfoSV.findGuarantorByTaskId(taskId);
		List<GuaraCom> guaraComs=new ArrayList<GuaraCom>();
		List<GuaraProCom> guaraProComs=new ArrayList<GuaraProCom>();
		List<GuaraPerson> guaraPersons=new ArrayList<GuaraPerson>();
		if(guaranteeInfos!=null) {
		for(Guarantor guarantor : guaranteeInfos) {
			
			Long guaraId = guarantor.getId();
			String guaraCusNo = guarantor.getCusNo();
			
			if (guarantor.getGuaraType().equals(SysConstants.GuarantorType.COMP_GUARA)) {
				
				//企业基本信息
				CompBaseDto cBase = compBaseSV.findCompBaseByCustNo(guaraCusNo);
				//企业描述信息
				CompBaseDesc cDesc=compBaseSV.findCompBaseDescByCustNo(guaraCusNo);
				//企业股权信息
				List<CompBaseEquity> nequities=compBaseSV.findCompEquityByCustNo(guaraCusNo);
				
				GuaraCom gCom = guarainfoSV.findGuaraComById(guaraId);
				if(null!=cBase && null!=cDesc && null!=nequities && null!=gCom) {
				String[] arr = gCom.getFinaRepYear().split("-");
				int guaraFinaYear = Integer.parseInt(arr[0]);
				int guaraFinaMonth = Integer.parseInt(arr[1]);
				//企业财报数据
				List<Map<String, Double>> finaData = this.genFinaRepData(finaSubjects,
						guaraCusNo, guaraFinaYear, guaraFinaMonth);
				
				List<String> guaraDateList = SystemUtil.genFinaDateList(guaraFinaYear,
						guaraFinaMonth, false, true);
				
				gCom.setCompBase(cBase);
				gCom.setCompBaseDesc(cDesc);
				gCom.setCompBaseEquities(nequities);
				gCom.setGuarantor(guarantor);
				gCom.setFinanceData(finaData);
				gCom.setFinaDateList(guaraDateList);
				
				String guararatioAnalysis = gCom.getRatioAnalysis();
				if(StringUtil.isEmptyString(guararatioAnalysis)){
					//获得系统分析结论
					Map<String, Object> conclusion = finanasisSV.genAnalysisConclusion(
							guarantor.getTaskId(), guaraCusNo, 
							SysConstants.SysFinaAnalyPart.RATIO_INDEX);
					ratioAnalysis = "盈利能力：" + conclusion.get("RatioOne").toString()
							      + "偿债能力：" + conclusion.get("RatioTwo").toString()
							      + "运营能力：" + conclusion.get("RatioThree").toString()
							      + "增长能力：" + conclusion.get("RatioFour").toString();
					
					gCom.setRatioAnalysis(guararatioAnalysis);
				}
				
				guaraComs.add(gCom);
				}
			}else if(guarantor.getGuaraType().equals(SysConstants.GuarantorType.PROF_GUARA)) {
				GuaraProCom gProCom=guarainfoSV.findGuaraProComById(guaraId);
				if(null!=gProCom) {
				gProCom.setGuarantor(guarantor);
				List<CounterGuaraCom> cGuaraComs=guarainfoSV.findCounterGuaraComByGuaraId(guaraId);
				List<CounterGuaraMort> cGuaraMorts=guarainfoSV.findCounterGuaraMortByGuaraId(guaraId);
				gProCom.setCounterGuaraComs(cGuaraComs);
				gProCom.setCounterGuaraMorts(cGuaraMorts);
				guaraProComs.add(gProCom);
				}
			}else if(guarantor.getGuaraType().equals(SysConstants.GuarantorType.PERSON_GUARA)) {
				GuaraPerson gPerson=guarainfoSV.findGuaraPersonById(guaraId);
				if(null!=gPerson) {
				gPerson.setGuarantor(guarantor);
				PersonBase pBase=compBaseSV.findPersonBaseByCustNo(guaraCusNo);
				gPerson.setPersonBase(pBase);
				guaraPersons.add(gPerson);
				}
			}
			else;
			
		}
		}
		dataMap.put("guaraComs", guaraComs);
		dataMap.put("guaraProComs", guaraProComs);
		dataMap.put("guaraPersons", guaraPersons);
		
		
		List<MortgageInfo> mortgageInfos=guarainfoSV.findMortgageInfoByTaskId(taskId);
		List<PledgeInfo> pledgeInfos=guarainfoSV.findPledgeInfoByTaskId(taskId);
	
		dataMap.put("mortgageInfos", mortgageInfos);
		dataMap.put("pledgeInfos", pledgeInfos);
		
		RiskAnalysis rAnalysis=conclusionSV.findRiskAnalysisByTaskId(taskId);
		if(rAnalysis==null) {
			rAnalysis=new RiskAnalysis();
		}
		dataMap.put("rAnalysis", rAnalysis);
		
		Conclusion conclusion=conclusionSV.updateAndFindConclusionByTaskId(taskId);
		if(conclusion==null) {
			conclusion=new Conclusion();
		}
		dataMap.put("conclusion", conclusion);
		
		dataMap.put("year", String.valueOf(finaRepYear));
		dataMap.put("firstYear", String.valueOf(finaRepYear - 3));
		dataMap.put("secondYear", String.valueOf(finaRepYear - 2));
		dataMap.put("thirdYear", String.valueOf(finaRepYear - 1));
		dataMap.put("month", finaRepMonth);
		
		//信贷财报原始数据
		List<Map<String, Double>> cmisBalanceData = cusFSToolSV.findRepDataByType(task.getCustomerNo(),
				finaRepYear, finaRepMonth, SysConstants.CmisFianRepType.BALANCE);
		List<Map<String, Double>> cmisIncomeData = cusFSToolSV.findRepDataByType(task.getCustomerNo(),
				finaRepYear, finaRepMonth, SysConstants.CmisFianRepType.INCOME);
		List<Map<String, Double>> cmisCashFlowData = cusFSToolSV.findRepDataByType(task.getCustomerNo(),
				finaRepYear, finaRepMonth, SysConstants.CmisFianRepType.CASH_FLOW);
		List<Map<String, Double>> cmisRatioIndexData = cusFSToolSV.findRepDataByType(task.getCustomerNo(),
				finaRepYear, finaRepMonth, SysConstants.CmisFianRepType.RATIO_INDEX);
		
		dataMap.put("financeData", this.genFinaRepData(finaSubjects, cmisBalanceData, cmisIncomeData,
				cmisCashFlowData, cmisRatioIndexData));
		
		//查询财务比率指标
		List<FinaRowData> finaIndexs = compFinanceSV.findFinanceIndexs(task.getCustomerNo(),
				task.getIndustry(), cmisRatioIndexData);
		dataMap.put("finaIndexs", finaIndexs);
		
		//现金流量科目相关信息
		List<FinaRowData> cashflows = compFinanceSV.findFinanceCashFlow(cmisCashFlowData);
		dataMap.put("cashflows", cashflows);
		
		//企业经营性现金流回笼信息
		List<CompFinanceOperCF> finaOperCFs = compFinanceSV.findFinanceOperCFByTIdAndNo(taskId,
				task.getCustomerNo());
		
		List<CompFinanceOperCF> mfinaOperCFs=new ArrayList<CompFinanceOperCF>();
		List<CompFinanceOperCF> ofinaOperCFs=new ArrayList<CompFinanceOperCF>();
		
		double msum=0,osum=0;
		if(finaOperCFs!=null&&finaOperCFs.size()>0) {
			for(CompFinanceOperCF data:finaOperCFs) {
				String type=data.getBank();
				if("SYRCB".equals(type)) {
					mfinaOperCFs.add(data);
					if(null!=data.getAmount())
					msum=msum+data.getAmount();
				}else {
					ofinaOperCFs.add(data);
					if(null!=data.getAmount())
					osum=osum+data.getAmount();
				}
			}
			CompFinanceOperCF msumcf=new CompFinanceOperCF();
			msumcf.setAmount(msum);
			mfinaOperCFs.add(msumcf);
			CompFinanceOperCF osumcf=new CompFinanceOperCF();
			osumcf.setAmount(osum);
			ofinaOperCFs.add(osumcf);
		}
		dataMap.put("mfinaOperCFs", mfinaOperCFs);
		dataMap.put("ofinaOperCFs", ofinaOperCFs);
		
		//获取时间（半年）
		List<String> halfdateList = SystemUtil.genFinaDateHalfList(finaRepYear, finaRepMonth);
		dataMap.put("halfdateList", halfdateList);
		
		//缴税信息
		List<CompFinanceTax> financeTaxes = compFinanceSV.findFinanceTaxByTIdAndNo(taskId,
				task.getCustomerNo());
		dataMap.put("financeTaxes", financeTaxes);
		
		//盈利情况
		List<FinaRowData> incomeDatas = compFinanceSV.findFinanceIncome(cmisIncomeData);
		dataMap.put("incomeDatas", incomeDatas);

		if (SysConstants.CusFinaValidFlag.AVAIL.equals(task.getFinaSyncFlag())) {
			
			//系统分析结论
			Map<String, Object> sysAnalysis = finanasisSV.genAnalysisConclusion(taskId, task.getCustomerNo(),
					finaRepYear, finaRepMonth, task.getIndustry(), SysConstants.CusFlag.BORROWER,
					cmisBalanceData, cmisIncomeData, cmisCashFlowData, true,
					SysConstants.SysFinaAnalyPart.ALL);
			dataMap.putAll(sysAnalysis);
		}
		
		//加载字典数据
		String[] optTypes = {
				SysConstants.BsDicConstant.STD_CUS_BIZ_STATUS, 
				SysConstants.BsDicConstant.STD_IS_MB_HOLDER,
				SysConstants.BsDicConstant.STD_SY_GB00015,
				SysConstants.BsDicConstant.STD_MB_ACC_TYPE,
				SysConstants.BsDicConstant.STD_COM_RELATION,
				SysConstants.BsDicConstant.STD_SY_JB00035,
				SysConstants.BsDicConstant.STD_SY_GB00009,
				SysConstants.BsDicConstant.STD_SY_GB00005,
				SysConstants.BsDicConstant.STD_SY_GB00002,
				SysConstants.BsDicConstant.STD_INDUSTRY_POLICY,
				SysConstants.BsDicConstant.STD_OWNERSHIP_NATURE,
				SysConstants.BsDicConstant.STD_LAND_NATURE,
				SysConstants.BsDicConstant.STD_LAND_USE,
				SysConstants.BsDicConstant.STD_ZX_YES_NO,
				SysConstants.BsDicConstant.STD_SY_GUAR_TYPE,
				SysConstants.BsDicConstant.STD_IQP_PAY_TYPE,
				SysConstants.BsDicConstant.STD_COM_EXT_GUARA_REL,
				SysConstants.BsDicConstant.STD_ZB_FIVE_SORT,
				SysConstants.BsDicConstant.STD_ZB_CERT_TYP,
				SysConstants.BsDicConstant.STD_SY_GB00003,
				SysConstants.BsDicConstant.STD_SY_ODS0004,
				SysConstants.BsDicConstant.STD_SY_JB00036,
				SysConstants.BsDicConstant.STD_CONST_QUALI_TYPE,
				SysConstants.BsDicConstant.STD_PROJECT_TAKE_TYPE,
				SysConstants.BsDicConstant.STD_ZB_LIMIT_TYPE,
				SysConstants.BsDicConstant.STD_ACC_AGE_TYPE,
				SysConstants.BsDicConstant.STD_FIXED_INVEST,
				SysConstants.BsDicConstant.STD_FIXED_INVESTEST,
				SysConstants.BsDicConstant.STD_FINAPLAN_TYPE,
				SysConstants.BsDicConstant.STD_SY_GB00009,
				SysConstants.BsDicConstant.STD_SY_JB00015,
				SysConstants.BsDicConstant.STD_SY_JB00029,
				SysConstants.BsDicConstant.STD_ZB_FIXED_PROJ_TYPE,
				SysConstants.BsDicConstant.STD_SY_COM01,
				SysConstants.BsDicConstant.STD_SY_GB00016
				};
		dataMap.put("dicData", sysDicSV.queryMapByTypes(optTypes));
		
		return dataMap;
	}
	
	/**
	 * TODO 生成调查报告中财报数据
	 * @param subjects 要显示的财报科目列表
	 * @param customerNo 客户号
	 * @param finaRepYear 财报年份
	 * @param finaRepMonth 财报月份
	 * @return
	 * @throws Exception
	 */
	private List<Map<String,Double>> genFinaRepData(List<TableSubjCode> subjects,
			String customerNo, int finaRepYear, int finaRepMonth) throws Exception {
		
		List<Map<String, Double>> cmisBalanceData = cusFSToolSV.findRepDataByType(customerNo,
				finaRepYear, finaRepMonth, SysConstants.CmisFianRepType.BALANCE);
		List<Map<String, Double>> cmisIncomeData = cusFSToolSV.findRepDataByType(customerNo,
				finaRepYear, finaRepMonth, SysConstants.CmisFianRepType.INCOME);
		List<Map<String, Double>> cmisCashFlowData = cusFSToolSV.findRepDataByType(customerNo,
				finaRepYear, finaRepMonth, SysConstants.CmisFianRepType.CASH_FLOW);
		List<Map<String, Double>> cmisRatioIndexData = cusFSToolSV.findRepDataByType(customerNo,
				finaRepYear, finaRepMonth, SysConstants.CmisFianRepType.RATIO_INDEX);
		
		return genFinaRepData(subjects, cmisBalanceData, cmisIncomeData,
				cmisCashFlowData, cmisRatioIndexData);
	}
	
	/**
	 * TODO 生成调查报告中财报数据
	 * @param subjects 要显示的财报科目列表
	 * @param balanceData 资产负债表原始数据
	 * @param incomeData 利润表原始数据
	 * @param cashFlowData 现金流量表原始数据
	 * @param ratioIndexData 比率指标表原始数据
	 * @return
	 * @throws Exception
	 */
	private List<Map<String,Double>> genFinaRepData(List<TableSubjCode> subjects,
			List<Map<String,Double>> balanceData, 
			List<Map<String,Double>> incomeData,
			List<Map<String,Double>> cashFlowData,
			List<Map<String,Double>> ratioIndexData) throws Exception {
		
		List<Map<String,Double>> resultList = new ArrayList<Map<String,Double>>();
		
		for(int i=0; i<4; i++){
			
			Map<String,Double> bSheet = balanceData.get(i);
			Map<String,Double> iSheet = incomeData.get(i);
			Map<String,Double> cSheet = cashFlowData.get(i);
			Map<String,Double> rSheet = ratioIndexData.get(i);
			
			Map<String,Double> data = new HashMap<String,Double>();
			
			for(TableSubjCode subject : subjects){
				
				Double value = null;
				
				String subjCode = subject.getCmisSubjCode();
				String subjName = subject.getSubjName();
				
				//资产负债表科目
				if(bSheet.containsKey(subjCode))
					value = bSheet.get(subjCode);
				
				//利润表科目
				else if(iSheet.containsKey(subjCode))
					value = iSheet.get(subjCode);
				
				//指标比率表科目
				else if(rSheet.containsKey(subjCode))
					value = rSheet.get(subjCode);
				
				//现金流量表科目
				else if(cSheet.containsKey(subjCode))
					value = cSheet.get(subjCode);
				
				else if("净现金流".equals(subjName)){
					
					double cashFlow = 0.0;
					//经营净现金流
					Double cf1 = cSheet.get("418");
					if(cf1 != null)
						cashFlow += cf1.doubleValue();
					//投资净现金流
					Double cf2 = cSheet.get("442");
					if(cf2 != null)
						cashFlow += cf2.doubleValue();
					//筹资净现金流
					Double cf3 = cSheet.get("460");
					if(cf3 != null)
						cashFlow += cf3.doubleValue();
					
					value = cashFlow;
					
				}else if("主营业务利润率(%)".equals(subjName)){
					
					Double profit = iSheet.get("308");
					Double income = iSheet.get("301");
					if(profit != null && income != null && income != 0.0)
						value = (profit/income)*100;
					
				}else if("净利润率(%)".equals(subjName)){
					
					Double netProfit = iSheet.get("333");
					Double income = iSheet.get("301");
					if(netProfit != null && income != null && income != 0.0)
						value = (netProfit/income)*100;
				}
				
				data.put(subjName, value);
			}
			
			resultList.add(data);
		}
		
		return resultList;
	}
	
}
