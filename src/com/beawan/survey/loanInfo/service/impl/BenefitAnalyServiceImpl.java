package com.beawan.survey.loanInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.loanInfo.dao.BenefitAnalyDao;
import com.beawan.survey.loanInfo.bean.BenefitAnaly;
import com.beawan.survey.loanInfo.service.BenefitAnalyService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.loanInfo.bean.BenefitAnaly;

/**
 * @author yzj
 */
@Service("benefitAnalyService")
public class BenefitAnalyServiceImpl extends BaseServiceImpl<BenefitAnaly> implements BenefitAnalyService{
    /**
    * 注入DAO
    */
    @Resource(name = "benefitAnalyDao")
    public void setDao(BaseDao<BenefitAnaly> dao) {
        super.setDao(dao);
    }
    @Resource
    public BenefitAnalyDao benefitAnalyDao;
}
