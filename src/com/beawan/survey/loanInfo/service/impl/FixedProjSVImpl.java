package com.beawan.survey.loanInfo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.base.service.ISysDicSV;
import com.beawan.survey.loanInfo.bean.FixedProjBase;
import com.beawan.survey.loanInfo.bean.FixedProjCashFow;
import com.beawan.survey.loanInfo.bean.FixedProjFinaPlan;
import com.beawan.survey.loanInfo.bean.FixedProjInvest;
import com.beawan.survey.loanInfo.bean.FixedProjInvestEst;
import com.beawan.survey.loanInfo.bean.FixedProjPlan;
import com.beawan.survey.loanInfo.dao.IFixedInvestDAO;
import com.beawan.survey.loanInfo.dao.IFixedInvestEstDAO;
import com.beawan.survey.loanInfo.dao.IFixedProjBaseDAO;
import com.beawan.survey.loanInfo.dao.IFixedProjCashFlowDAO;
import com.beawan.survey.loanInfo.dao.IFixedProjFinaPlanDAO;
import com.beawan.survey.loanInfo.dao.IFixedProjPlanDAO;
import com.beawan.survey.loanInfo.service.IFixedProjSV;
import com.platform.util.BeanUtil;
import com.platform.util.StringUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service("fixedProjSV")
public class FixedProjSVImpl implements IFixedProjSV {

	@Resource
	private IFixedProjBaseDAO fixedProjBaseDao;
	
	@Resource
	private IFixedProjCashFlowDAO cashFlowDao;
	
	@Resource
	private IFixedProjPlanDAO planDao;
	
	@Resource 
	private IFixedInvestDAO investDao;
	
	@Resource
	private IFixedInvestEstDAO estDao;
	
	@Resource
	private IFixedProjFinaPlanDAO finaPlanDao;
	
	@Resource
	private ISysDicSV sysDicSV;
	
	@Override
	public void saveProjBase(FixedProjBase fixedProjBase) throws Exception{
		fixedProjBaseDao.saveOrUpdate(fixedProjBase);
	}
	
	@Override
	public void saveProjBase(String jsondata) throws Exception {
		
		if(StringUtil.isEmpty(jsondata))
			return;
		
		JSONObject json = JSONObject.fromObject(jsondata);
		FixedProjBase projBase = (FixedProjBase) JSONObject.toBean(json,
				FixedProjBase.class);
		FixedProjBase oldProjBase = fixedProjBaseDao.selectByPrimaryKey(projBase.getTaskId());
		
		if(oldProjBase != null){
			BeanUtil.mergeProperties(projBase, oldProjBase, json);
		}else
			oldProjBase = projBase;
		
		fixedProjBaseDao.saveOrUpdate(oldProjBase);
	}

	@Override
	public FixedProjBase queryProjBaseByTaskId(Long taskId) throws Exception{
	
		FixedProjBase fProjBase=fixedProjBaseDao.findByPrimaryKey(taskId);
		
		return fProjBase;
	}

	@Override
	public void saveProjPlanList(Long taskId, String jsonArray) throws Exception {
		List<FixedProjPlan> list=planDao.queryFixedProjPlanListByTaskId(taskId);
		for(FixedProjPlan data:list) {
			planDao.deleteEntity(data);
		}
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if (array != null && array.size() > 0) {
			for (int i = 0; i < array.size(); i++) {
				JSONObject dataStr = JSONObject.fromObject(array.get(i));
				FixedProjPlan data = (FixedProjPlan) JSONObject.toBean(dataStr,FixedProjPlan.class);
				planDao.saveOrUpdate(data);
			}
		}
	}

	@Override
	public List<FixedProjPlan> queryProjPlanByTaskId(Long taskId)  throws Exception{
		return planDao.queryFixedProjPlanListByTaskId(taskId);
	}

	@Override
	public void saveProjInvestList(Long taskId, String jsonArray) throws Exception {
		List<FixedProjInvest> list=investDao.queryFixedProjInvestListByTaskId(taskId);
		for(FixedProjInvest data:list) {
			investDao.deleteEntity(data);
		}
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if (array != null && array.size() > 0) {
			for (int i = 0; i < array.size(); i++) {
				JSONObject dataStr = JSONObject.fromObject(array.get(i));
				FixedProjInvest data = (FixedProjInvest) JSONObject.toBean(dataStr,FixedProjInvest.class);
				investDao.saveOrUpdate(data);
			}
		}
	}

	@Override
	public List<FixedProjInvest> queryProjInvestByTaskId(Long taskId) throws Exception {
		return investDao.queryFixedProjInvestListByTaskId(taskId);
	}

	@Override
	public void saveProjInvestEstList(Long taskId, String jsonArray)  throws Exception{
		List<FixedProjInvestEst> list=estDao.queryFixedProjInvestEstListByTaskId(taskId);
		for(FixedProjInvestEst data:list) {
			estDao.deleteEntity(data);
		}
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if (array != null && array.size() > 0) {
			for (int i = 0; i < array.size(); i++) {
				JSONObject dataStr = JSONObject.fromObject(array.get(i));
				FixedProjInvestEst data = (FixedProjInvestEst) JSONObject.toBean(dataStr,FixedProjInvestEst.class);
				estDao.saveOrUpdate(data);
			}
		}
		
	}

	@Override
	public List<FixedProjInvestEst> queryProjInvestEstByTaskId(Long taskId) throws Exception {
//		List<FixedProjInvestEst> list=new ArrayList<FixedProjInvestEst>();
//		List<BsDic> bsDics=sysDicSV.findByType("STD_FIXED_INVESTEST");
//		
//		if(investEsts==null||investEsts.size()<=0) {
//			for(int i=0;i<bsDics.size();i++) {
//					FixedProjInvestEst fixedProjInvestEst=new FixedProjInvestEst();
//					fixedProjInvestEst.setItemCode(bsDics.get(i).getEnName());
//					fixedProjInvestEst.setItemName(bsDics.get(i).getCnName());
//					list.add(fixedProjInvestEst);
//				
//			}
//		}else {
//			return investEsts;
//		}
		
		return estDao.queryFixedProjInvestEstListByTaskId(taskId);
	}

	@Override
	public void saveProjFinaPlanList(Long taskId, String jsonArray)  throws Exception{
		List<FixedProjFinaPlan> list=finaPlanDao.queryFixedProjFinaPlanByTaskId(taskId);
		for(FixedProjFinaPlan data:list) {
			finaPlanDao.deleteEntity(data);
		}
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if (array != null && array.size() > 0) {
			for (int i = 0; i < array.size(); i++) {
				JSONObject dataStr = JSONObject.fromObject(array.get(i));
				FixedProjFinaPlan data = (FixedProjFinaPlan) JSONObject.toBean(dataStr,FixedProjFinaPlan.class);
				finaPlanDao.saveOrUpdate(data);
			}
		}
	}

	@Override
	public List<FixedProjFinaPlan> queryProjFinaPlanByTaskId(Long taskId) throws Exception{
//		List<FixedProjFinaPlan> list=new ArrayList<FixedProjFinaPlan>();
//		List<BsDic> bsDics=sysDicSV.findByType("STD_FINAPLAN_TYPE");
//		
//		if(plans==null||plans.size()<=0) {
//		for(int i=0;i<bsDics.size();i++) {
//				FixedProjFinaPlan plan=new FixedProjFinaPlan();
//				plan.setItemCode(bsDics.get(i).getEnName());
//				plan.setItemName(bsDics.get(i).getCnName());
//				list.add(plan);
//		}
//		
//		}else {
//			return plans;
//		}
		
		
		return finaPlanDao.queryFixedProjFinaPlanByTaskId(taskId);
	}

	@Override
	public void saveProjCashFowList(Long taskId, String jsonArray) throws Exception{
		List<FixedProjCashFow> list=cashFlowDao.queryFixedProjCashFlowListByTaskId(taskId);
		for(FixedProjCashFow data:list) {
			cashFlowDao.deleteEntity(data);
		}
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if (array != null && array.size() > 0) {
			for (int i = 0; i < array.size(); i++) {
				JSONObject dataStr = JSONObject.fromObject(array.get(i));
				FixedProjCashFow data = (FixedProjCashFow) JSONObject.toBean(dataStr,FixedProjCashFow.class);
				cashFlowDao.saveOrUpdate(data);
			}
		}
	}

	@Override
	public List<FixedProjCashFow> queryProjCashFowByTaskId(Long taskId) throws Exception{
		return cashFlowDao.queryFixedProjCashFlowListByTaskId(taskId);
	}

}
