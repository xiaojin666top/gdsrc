package com.beawan.survey.loanInfo.service.impl;

import javax.annotation.Resource;

import com.beawan.base.entity.SysDic;
import com.beawan.base.service.ISysDicSV;
import com.beawan.common.SysConstants;
import com.beawan.survey.guarantee.dao.GuaCompanyDao;
import com.beawan.survey.guarantee.dao.GuaPersonDao;
import com.beawan.survey.guarantee.entity.GuaCompany;
import com.beawan.survey.guarantee.entity.GuaPerson;
import com.beawan.survey.loanInfo.bean.ApplyScheme;
import com.beawan.survey.loanInfo.service.IApplyInfoSV;
import com.beawan.task.bean.Task;
import com.beawan.task.dao.ITaskDAO;
import com.platform.util.StringUtil;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.beawan.survey.loanInfo.bean.Conclusion;
import com.beawan.survey.loanInfo.bean.RiskAnalysis;
import com.beawan.survey.loanInfo.dao.IConclusionDAO;
import com.beawan.survey.loanInfo.dao.IRiskAnalysisDAO;
import com.beawan.survey.loanInfo.service.IConclusionSV;

import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service("conclusionSV")
public class ConclusionSVImpl implements IConclusionSV {
	
	@Resource
	private IConclusionDAO  concluDao;
	@Resource
	private IRiskAnalysisDAO riskDao;
	@Resource
	private ITaskDAO taskDAO;
	@Resource
	private IApplyInfoSV applyInfoSV;
	@Resource
	private ISysDicSV sysDicSV;
	@Resource
	private GuaCompanyDao thirdPartyGuaDao;
	@Resource
	private GuaPersonDao guaPersonDao;

	@Override
	public RiskAnalysis findRiskAnalysisByTaskId(Long taskId) throws Exception {
		Task task = taskDAO.getTaskById(taskId);
		return riskDao.selectSingleByProperty("serNo", task.getSerNo());
	}

	@Override
	public void saveRiskAnalysis(RiskAnalysis riskAnalysisInfo) throws Exception {
		riskDao.saveOrUpdate(riskAnalysisInfo);
	}

	@Override
	public Conclusion updateAndFindConclusionByTaskId(Long taskId) throws Exception {
		Conclusion conclusion=concluDao.findByPrimaryKey(taskId);
		if(conclusion==null){
			conclusion = new Conclusion();
			conclusion.setTaskId(taskId);
		}
		String creditScheme = conclusion.getCreditScheme();
		//当 授信安排已经存在内容是，则不重新生成覆盖
		if(!StringUtils.isEmpty(creditScheme)){
			return conclusion;
		}
//		if(StringUtil.isEmptyString(conclusion.getCreditScheme())){
		//从授信申请列表中获取内容，将表格内容数据转化成文字性报告内容
		//GGG
		List<ApplyScheme> schemeList = applyInfoSV.findApplySchemeListByTaskId(taskId);
		if(schemeList==null || schemeList.size()==0){
			return conclusion;
		}
		Task task = taskDAO.findByPrimaryKey(taskId);
		Collections.sort(schemeList, new Comparator<ApplyScheme>() {
			@Override
			public int compare(ApplyScheme o1, ApplyScheme o2) {
				return (int)(o1.getCreditAmt() - o2.getCreditAmt());
			}
		});
//		经调查，同意对南通宏川化工有限公司授信流动资金贷款4900万元，
//		用于购甲醇、乙二醇等化学品、经营周转等，期限3年，周转使用，
//		由东莞市宏川化工供应链有限公司、广东宏川集团有限公司保证担保，追加实际控制人林海川保证担保，利率执行年利率4.5%。
		String productName = task.getProductName();
		String custName = task.getCustomerName();
		StringBuilder credit = new StringBuilder("经调查，同意对"+ custName + "授信");

//		credit.append(custName);
//		if(schemeList.size()>1){
//			Double sum = 0d;
//			for(ApplyScheme scheme : schemeList){
//				sum += scheme.getCreditAmt();
//			}
//			credit.append("综合授信").append(sum).append("万元。其中，");
//
//		}
		for(ApplyScheme scheme : schemeList){
			String[] grauateeWayArray = scheme.getGrauateeWay().split(",");
			String grauateeWay = "";
			for(int i = 0; i < grauateeWayArray.length; i++){				
				SysDic guarWay = sysDicSV.findByEnNameAndType(grauateeWayArray[i], SysConstants.BsDicConstant.STD_SY_GUAR_TYPE);
				grauateeWay =grauateeWay + guarWay.getCnName() + "，";
			}
			grauateeWay = grauateeWay.substring(0, grauateeWay.length() - 1);
			SysDic prdName = sysDicSV.findByEnNameAndType(scheme.getBusinessType(), SysConstants.BsDicConstant.RG_PRODUCT_LIST);
			DecimalFormat decimalFormat = new DecimalFormat("###################.###########");
			
			credit.append(prdName.getCnName())
					.append(decimalFormat.format(scheme.getCreditAmt()))
//					.append(scheme.getCreditAmt().toString())
//					.append(scheme.getCreditAmtType())
					.append("万元");
			if(!StringUtils.isEmpty(scheme.getUseDesc())){
				credit.append("，用于"+scheme.getUseDesc());
			}
			Integer creditTerm = scheme.getCreditTerm();
			if(creditTerm !=null && creditTerm <12){
				credit.append("，期限" + creditTerm + "个月");
			}else {
				int year = creditTerm / 12;
				int month = creditTerm % 12;
				credit.append("，期限" + year + "年");
				if(month!=0){
					credit.append(month + "个月");
				}
			}
//			，利率执行年利率4.5%
			credit.append("，利率执行年利率").append(scheme.getCreditRate()).append("%");
			String creditAmtType = scheme.getCreditAmtType();
			if(!StringUtils.isEmpty(creditAmtType)){
				credit.append("，").append(creditAmtType);
			}
		}
		List<GuaCompany> list = thirdPartyGuaDao.selectByProperty("serNo", task.getSerNo());
		if(!CollectionUtils.isEmpty(list)){
			credit.append("，由");
			String guaList = "";
			for(GuaCompany guaCm : list){
				String companyName = guaCm.getCompanyName();
				guaList += "、"+companyName;
			}
			String substring = guaList.substring(1);
			substring += "保证担保";
			credit.append(substring);
		}
		List<GuaPerson> guaPersonList = guaPersonDao.selectByProperty("serNo", task.getSerNo());
		if(!CollectionUtils.isEmpty(guaPersonList)){
			String guaPerList = "";
			for(GuaPerson guaPer: guaPersonList){
				guaPerList += "、"+guaPer.getName();
			}
			String substring = guaPerList.substring(1);
			credit.append("，追加自然人").append(substring).append("保证担保。");
		}
		if(!credit.toString().endsWith("。")){
			credit.append("。");
		}
		conclusion.setCreditScheme(credit.toString());
		this.saveConclusion(conclusion);
//		}
		return conclusion;
	}

	@Override
	public void saveConclusion(Conclusion conclusionInfo) throws Exception {
		concluDao.saveOrUpdate(conclusionInfo);
	}

	@Override
	public RiskAnalysis findBySerNo(String serNo) {
		return riskDao.selectSingleByProperty("serNo", serNo);
	}
}
