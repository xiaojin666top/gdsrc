package com.beawan.survey.loanInfo.service.impl;

import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Service;

import com.beawan.survey.loanInfo.bean.EstaProjBase;
import com.beawan.survey.loanInfo.dao.IEstaProjBaseDAO;
import com.beawan.survey.loanInfo.service.IEstaProjSV;
import com.platform.util.BeanUtil;
import com.platform.util.StringUtil;

@Service("eEstaProjSV")
public class EstaProjSVImpl implements IEstaProjSV {
	
	@Resource
	private IEstaProjBaseDAO estaProjBaseDao;
	
	@Override
	public EstaProjBase findProjBaseByTaskId(Long taskId) throws Exception {
		
		EstaProjBase estaProjBase=estaProjBaseDao.findByPrimaryKey(taskId);
		
		if (estaProjBase==null) {
			estaProjBase =new EstaProjBase();
			estaProjBase.setTaskId(taskId);
		}
		
		return estaProjBase;
	}

	@Override
	public void saveProjBase(EstaProjBase estaProjBase) throws Exception {
		estaProjBaseDao.saveOrUpdate(estaProjBase);
	}
	
	@Override
	public void saveProjBase(String jsondata) throws Exception {
		
		if(StringUtil.isEmpty(jsondata))
			return;
		
		JSONObject json = JSONObject.fromObject(jsondata);
		EstaProjBase projBase = (EstaProjBase) JSONObject.toBean(json,
				EstaProjBase.class);
		EstaProjBase oldProjBase = estaProjBaseDao.selectByPrimaryKey(projBase.getTaskId());
		
		if(oldProjBase != null){
			BeanUtil.mergeProperties(projBase, oldProjBase, json);
		}else
			oldProjBase = projBase;
		
		estaProjBaseDao.saveOrUpdate(oldProjBase);
	}
}
