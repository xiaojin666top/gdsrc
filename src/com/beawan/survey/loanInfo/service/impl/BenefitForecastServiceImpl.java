package com.beawan.survey.loanInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.loanInfo.dao.BenefitForecastDao;
import com.beawan.survey.loanInfo.bean.BenefitForecast;
import com.beawan.survey.loanInfo.service.BenefitForecastService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.loanInfo.bean.BenefitForecast;

/**
 * @author yzj
 */
@Service("benefitForecastService")
public class BenefitForecastServiceImpl extends BaseServiceImpl<BenefitForecast> implements BenefitForecastService{
    /**
    * 注入DAO
    */
    @Resource(name = "benefitForecastDao")
    public void setDao(BaseDao<BenefitForecast> dao) {
        super.setDao(dao);
    }
    @Resource
    public BenefitForecastDao benefitForecastDao;
}
