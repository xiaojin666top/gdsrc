package com.beawan.survey.loanInfo.service;

import com.beawan.survey.loanInfo.bean.Conclusion;
import com.beawan.survey.loanInfo.bean.RiskAnalysis;

/**
 * @Description 调查结论信息服务接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IConclusionSV {
	
	/**
	 * @Description (根据任务号查询信贷风险评价情况)
	 * @param taskId 任务号
	 * @return RiskAnalysis
	 * @throws Exception
	 */
	public RiskAnalysis findRiskAnalysisByTaskId(Long taskId) throws Exception;

	/**
	 * 获取风险分析信息
	 * @param serNo
	 * @return
	 */
	RiskAnalysis findBySerNo(String serNo);


	/**
	 * @Description (保存本次授信信贷风险评价情况)
	 * @param riskAnalysisInfo 本次授信信贷风险评价情况
	 * @throws Exception
	 */
	public void saveRiskAnalysis(RiskAnalysis riskAnalysisInfo) throws Exception;
	
	
	/**
	 * @Description (根据任务号查询授信综合结论)
	 * @param taskId 任务号
	 * @return RiskAnalysis
	 * @throws Exception
	 */
	public Conclusion updateAndFindConclusionByTaskId(Long taskId) throws Exception;
	

	/**
	 * @Description (保存本次授信信贷风险评价情况)
	 * @param conclusionInfo 本次授信信贷风险评价情况
	 * @throws Exception
	 */
	public void saveConclusion(Conclusion conclusionInfo) throws Exception;
}
