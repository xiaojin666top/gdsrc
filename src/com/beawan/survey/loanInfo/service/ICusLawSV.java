package com.beawan.survey.loanInfo.service;

import java.util.List;

import com.beawan.survey.loanInfo.bean.CusLaw;

public interface ICusLawSV {

	/***
	 * 从数据库查询裁判文书列表
	 * @param customerNo
	 * @param taskId
	 * @return
	 */
	public List<CusLaw> getLawInfoListByNo(String customerNo) throws Exception ;
	
	
	
	public void saveLawInfo(CusLaw data) throws Exception;
}
