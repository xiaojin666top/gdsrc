package com.beawan.survey.loanInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.loanInfo.bean.BenefitAnaly;

/**
 * @author yzj
 */
public interface BenefitAnalyService extends BaseService<BenefitAnaly> {
}
