package com.beawan.survey.loanInfo.service;

import com.beawan.survey.loanInfo.bean.EstaProjBase;

/**
 * @Description 房地产开发贷款项目信息服务接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IEstaProjSV {
	
	/**
	 * @Description (根据任务号查询房地产项目情况)
	 * @param taskId 任务号
	 * @return 
	 * @throws Exception
	 */
	public EstaProjBase findProjBaseByTaskId(Long taskId) throws Exception;
	

	/**
	 * @Description (保存房地产项目情况)
	 * @param estaProjBase 房地产项目情况实体
	 * @throws Exception
	 */
	public void saveProjBase(EstaProjBase estaProjBase) throws Exception;
	
	/**
	 * @Description (保存房地产项目情况)
	 * @param jsondata 房地产项目基本信息json串
	 * @throws Exception
	 */
	public void saveProjBase(String jsondata) throws Exception ;
}
