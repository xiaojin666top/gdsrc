package com.beawan.survey.loanInfo.service;

import java.util.Map;

/**
 * TODO 调查报告相关方法接口
 */
public interface ISurveyReportSV {
	
	/**
	 * TODO 授信调查报告
	 * @param taskId 任务编号
	 * @throws Exception
	 */
	public void createSurveyReport(long taskId)throws Exception;

	/**
	 * TODO 获得报告数据集合
	 * @param taskId 任务编号
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> getReportData(long taskId) throws Exception;
}
