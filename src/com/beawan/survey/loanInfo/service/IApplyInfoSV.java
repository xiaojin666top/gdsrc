package com.beawan.survey.loanInfo.service;


import java.util.List;
import java.util.Map;

import com.beawan.survey.loanInfo.bean.ApplyInfo;
import com.beawan.survey.loanInfo.bean.ApplyRate;
import com.beawan.survey.loanInfo.bean.ApplyScheme;
import com.beawan.survey.loanInfo.bean.Rate;
import com.beawan.task.bean.Task;

/**
 * @Description 申请信息服务接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IApplyInfoSV {
	
	ApplyScheme findApplySchemeById(Long id);
	/**
	 * @Description (根据任务号查询本次授信申请情况)
	 * @param taskId 任务号
	 * @return ApplyInfo
	 * @throws Exception
	 */
	public ApplyInfo findApplyInfoByTaskId(Long taskId) throws Exception;
	

	/**
	 * @Description (保存本次授信申请情况)
	 * @param ApplyInfo 本次授信申请情况实体
	 * @throws Exception
	 */
	public void saveApplyInfo(ApplyInfo applyInfo) throws Exception;
	
	
	/**
	 * @Description (根据任务号查询授信申请安排方案列表)
	 * @param taskId 任务号
	 * @return List<ApplyScheme>
	 * @throws Exception
	 */
	public List<ApplyScheme> findApplySchemeListByTaskId(Long taskId) throws Exception;
	
	/**
	 * @Description (保存授信申请安排方案记录)
	 * @param applyScheme 授信申请安实体类
	 * @throws Exception
	 */
	public ApplyScheme saveApplyScheme(ApplyScheme applyScheme) throws Exception;

	/**
	 * @Description (保存授信申请安排方案情况)
	 * @param taskId 任务号
	 * @throws Exception
	 */
	public void saveApplySchemeList(Long taskId, String jsonArray) throws Exception;
	
	/**
	 * @Description (删除授信申请安排方案记录)
	 * @param id 唯一标识
	 * @throws Exception
	 */
	public void deleteApplySchemeById(long id) throws Exception;
	
	/**
	 * @Description (根据客户号查询本次授信申利费率情况列表)
	 * @param taskId 客户号
	 * @return List<ApplyRate>
	 * @throws Exception
	 */
	public List<ApplyRate> findApplyRateListByCustomerNo(String customerNo) throws Exception;
	
	
	/**
	 * @Description (根据任务号查询本次授信申利费率情况列表)
	 * @param taskId 任务号
	 * @return List<ApplyRate>
	 * @throws Exception
	 */
	public List<ApplyRate> queryApplyRateListByTaskId(Long taskId) throws Exception;
	
	/**
	 * @Description (保存本次授信申利费率信息)
	 * @param applyRate 利费率信息实体
	 * @throws Exception
	 */
	public ApplyRate saveApplyRate(ApplyRate applyRate) throws Exception;

	/**
	 * @Description (保存本次授信申利费率情况列表)
	 * @param taskId 任务号
	 * @throws Exception
	 */
	public void saveApplyRateList(Long taskId,String jsonArray) throws Exception;
	
	/**
	 * @Description (删除本次授信申利费率信息)
	 * @param id 唯一标识
	 * @throws Exception
	 */
	public void deleteApplyRateById(long id) throws Exception;
	
	/**
	 * TODO 额度测算并返回结果
	 * @param task 任务信息
	 * @param applyInfo 申请信息
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> getCreditAmtCalResult(Task task,
			ApplyInfo applyInfo) throws Exception;
	
	
	/***
	 * TODO 得到利率计算项目
	 * @param taskId
	 * @return
	 * @throws Exception
	 */
	public Rate selectRateById(Long taskId) throws Exception;
	
	
	/***
	 * TODO 保存计算利率项目信息
	 * @param rateInfo
	 * @throws Exception
	 */
	public void saveRateInfo(String rateInfo) throws Exception; 
	
	/**
	 * TODO 查询额度测算涉及的财务数据
	 * @param taskId 任务号
	 * @return
	 * @throws Exception
	 */
	/*public List<Map<String, Double>> queryAmtCalFianData(String cusId, int finaRepYear,
			int finaRepMonth) throws Exception;*/
	
	/**
	 * TODO 额度测算
	 * @param applyInfo
	 * @param finaData 三年及当期资产负债表和利润表数据
	 * @param finaRepMonth 当期财报月份
	 * @return 各指标数据集合
	 */
	/*public Map<String, Double> calCreditAmt(ApplyInfo applyInfo,
			List<Map<String, Double>> finaData, int finaRepMonth);*/
}
