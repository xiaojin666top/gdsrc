package com.beawan.survey.loanInfo.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.loanInfo.bean.CounterGuaraCom;
/**
 * @Description 一般公司担保信息持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface ICounterGuaraComDAO extends ICommDAO<CounterGuaraCom>{
	/**
	 * @Description (根据任务号查询担保公司反担保企业信息)
	 * @param taskId  任务号
	 *            
	 * @return
	 */
	public List<CounterGuaraCom> queryCounterGuaraComByGuaraId(Long guaraId) throws DataAccessException;
}
