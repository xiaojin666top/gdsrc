package com.beawan.survey.loanInfo.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.loanInfo.bean.GuaraProCom;
/**
 * @Description 担保公司担保信息持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IGuaraProComDAO  extends ICommDAO<GuaraProCom>{

	/**
	 * @Description (根据任务号查询担保公司担保信息)
	 * @param taskId  任务号
	 *            
	 * @return
	 */
	public List<GuaraProCom> queryGuaraProComListByTaskId(Long taskId) throws DataAccessException;
}
