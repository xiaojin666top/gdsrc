package com.beawan.survey.loanInfo.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.loanInfo.bean.MortgageInfo;
/**
 * @Description 抵押物信息持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IMortgageInfoDAO extends ICommDAO<MortgageInfo>{
	/**
	 * @Description (根据任务号查询抵押物信息)
	 * @param taskId  任务号
	 *            
	 * @return
	 */
	public List<MortgageInfo> queryMortgageInfoListByTaskId(Long taskId) throws DataAccessException;
}
