package com.beawan.survey.loanInfo.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.loanInfo.bean.FixedProjCashFow;

/**
 * @Description 固定资产项目建设现金流测算持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IFixedProjCashFlowDAO extends ICommDAO<FixedProjCashFow> {
	
	/**
	 * @Description (根据任务号查询固定资产项目建设现金流测算)
	 * @param taskId  任务号
	 *            
	 * @return
	 */
	public List<FixedProjCashFow> queryFixedProjCashFlowListByTaskId(Long taskId) throws DataAccessException;

}
