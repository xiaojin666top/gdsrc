package com.beawan.survey.loanInfo.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.loanInfo.bean.FixedProjInvestEst;
/**
 * @Description 固定资产项目建设投资估算持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IFixedInvestEstDAO extends ICommDAO<FixedProjInvestEst>{
	
	/**
	 * @Description (根据任务号查询固定资产项目建设投资估算)
	 * @param taskId  任务号
	 *            
	 * @return
	 */
	public List<FixedProjInvestEst> queryFixedProjInvestEstListByTaskId(Long taskId) throws DataAccessException;
}
