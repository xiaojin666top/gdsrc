package com.beawan.survey.loanInfo.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.loanInfo.bean.Rate;
import com.beawan.survey.loanInfo.dao.IRateDAO;
@Repository("rateDao")
public class RateDAOImpl extends CommDAOImpl<Rate> implements IRateDAO{

	@Override
	public Rate selectRateById(Long taskId) throws DataAccessException {
		
		String hql="taskId=:taskId";
		
		Map<String,Object> params=new HashMap<String, Object>();
		params.put("taskId", taskId);
		
		return selectSingle(Rate.class,hql, params);
	}

}
