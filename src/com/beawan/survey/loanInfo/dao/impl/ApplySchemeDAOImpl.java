package com.beawan.survey.loanInfo.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.loanInfo.bean.ApplyScheme;
import com.beawan.survey.loanInfo.dao.IApplySchemeDAO;
@Service("applySchemeDAO")
public class ApplySchemeDAOImpl extends CommDAOImpl<ApplyScheme> implements IApplySchemeDAO{

	@Override
	public List<ApplyScheme> queryApplySchemeListByTaskId(Long taskId) throws DataAccessException {
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		return this.selectByProperty(ApplyScheme.class, params, "creditAmt desc");
	}

}
