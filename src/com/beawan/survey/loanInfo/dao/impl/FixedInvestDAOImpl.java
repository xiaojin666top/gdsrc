package com.beawan.survey.loanInfo.dao.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.loanInfo.bean.FixedProjInvest;
import com.beawan.survey.loanInfo.dao.IFixedInvestDAO;

@Service("fixedInvestDAO")
public class FixedInvestDAOImpl  extends CommDAOImpl<FixedProjInvest> implements IFixedInvestDAO{

	@Override
	public List<FixedProjInvest> queryFixedProjInvestListByTaskId(Long taskId) throws DataAccessException {
		// TODO Auto-generated method stub
		
		String query = "from FixedProjInvest as model where model.taskId ='" + taskId + "'  order by itemCode asc ";
		return getEntityManager().createQuery(query).getResultList();
		
	}
	

}
