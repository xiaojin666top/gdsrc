package com.beawan.survey.loanInfo.dao.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.loanInfo.bean.CounterGuaraCom;
import com.beawan.survey.loanInfo.dao.ICounterGuaraComDAO;
@Service("counterGuaraComDAO")
public class CounterGuaraComDAOImpl extends CommDAOImpl<CounterGuaraCom> implements ICounterGuaraComDAO{

	@Override
	public List<CounterGuaraCom> queryCounterGuaraComByGuaraId(Long guaraId) throws DataAccessException {
		// TODO Auto-generated method stub
		String query = "from CounterGuaraCom as model where model.guaraId ='" + guaraId + "'";
		return getEntityManager().createQuery(query).getResultList();
	}

}
