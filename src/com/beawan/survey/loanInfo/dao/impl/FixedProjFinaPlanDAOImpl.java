package com.beawan.survey.loanInfo.dao.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.loanInfo.bean.FixedProjFinaPlan;
import com.beawan.survey.loanInfo.dao.IFixedProjFinaPlanDAO;

@Service("fixedProjFinaPlanDAO")
public class FixedProjFinaPlanDAOImpl extends CommDAOImpl<FixedProjFinaPlan> implements IFixedProjFinaPlanDAO{

	@Override
	public List<FixedProjFinaPlan> queryFixedProjFinaPlanByTaskId(Long taskId) throws DataAccessException {
		// TODO Auto-generated method stub
		String query = "from FixedProjFinaPlan as model where model.taskId ='" + taskId + "' order by itemCode asc";
		return getEntityManager().createQuery(query).getResultList();
	}

}
