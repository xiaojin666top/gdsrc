package com.beawan.survey.loanInfo.dao.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.beawan.common.dao.ICommDAO;
import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.loanInfo.bean.FixedProjInvestEst;
import com.beawan.survey.loanInfo.dao.IFixedInvestEstDAO;


@Service("fixedInvestEstDAO")
public class FixedInvestEstDAOImpl extends CommDAOImpl<FixedProjInvestEst> implements IFixedInvestEstDAO{

	@Override
	public List<FixedProjInvestEst> queryFixedProjInvestEstListByTaskId(Long taskId) throws DataAccessException {
		// TODO Auto-generated method stub
		String query = "from FixedProjInvestEst as model where model.taskId ='" + taskId + "' order by itemCode asc ";
		return getEntityManager().createQuery(query).getResultList();
	}

}
