package com.beawan.survey.loanInfo.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.loanInfo.bean.ApplyInfo;
import com.beawan.survey.loanInfo.bean.GuaraCom;
import com.beawan.survey.loanInfo.dao.IGuaraComDAO;

@Service("guaraComDAO")
public class GuaraComDAOImpl extends CommDAOImpl<GuaraCom> implements IGuaraComDAO {

	@Override
	public List<GuaraCom> queryGuaraComlistByTaskId(Long taskId) throws DataAccessException{
		// TODO Auto-generated method stub
		String query = "from GuaraCom as model where model.taskId ='" + taskId + "'";
		return getEntityManager().createQuery(query).getResultList();
	}

	@Override
	public GuaraCom queryGuaraComByTaskIdAndNo(Long taskId, String customerNo) throws DataAccessException {
		// TODO Auto-generated method stub
		String hql = "taskId=:taskId And guaraCusNo=:customerNo";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("taskId", taskId);
		params.put("customerNo", customerNo);
		return selectSingle(GuaraCom.class, hql, params);
	}
	
	
}
