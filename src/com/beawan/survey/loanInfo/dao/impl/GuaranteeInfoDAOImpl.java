package com.beawan.survey.loanInfo.dao.impl;

import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.loanInfo.bean.GuaranteeInfo;
import com.beawan.survey.loanInfo.dao.IGuaranteeInfoDAO;



@Service("guaranteeInfoDAO")
public class GuaranteeInfoDAOImpl extends CommDAOImpl<GuaranteeInfo> implements IGuaranteeInfoDAO {

}
