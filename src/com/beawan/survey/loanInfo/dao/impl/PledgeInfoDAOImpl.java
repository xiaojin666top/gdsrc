package com.beawan.survey.loanInfo.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.loanInfo.bean.PledgeInfo;
import com.beawan.survey.loanInfo.dao.IPledgeInfoDAO;
@Service("pledgeInfoDAO")
public class PledgeInfoDAOImpl extends CommDAOImpl<PledgeInfo> implements IPledgeInfoDAO{

	@Override
	public List<PledgeInfo> queryPledgeInfoListByTaskId(Long taskId) throws DataAccessException {
		// TODO Auto-generated method stub
//		String query = "from PledgeInfo as model where model.taskId ='" + taskId + "'";
//		return getEntityManager().createQuery(query).getResultList();
		
		String query = "taskId=:taskId";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("taskId", taskId);
		
		return select(query, params);
	}

}
