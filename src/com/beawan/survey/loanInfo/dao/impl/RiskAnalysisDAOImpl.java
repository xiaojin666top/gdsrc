package com.beawan.survey.loanInfo.dao.impl;

import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.loanInfo.bean.RiskAnalysis;
import com.beawan.survey.loanInfo.dao.IRiskAnalysisDAO;

@Service("riskAnalysisDAO")
public class RiskAnalysisDAOImpl extends CommDAOImpl<RiskAnalysis> implements IRiskAnalysisDAO{

	
}
