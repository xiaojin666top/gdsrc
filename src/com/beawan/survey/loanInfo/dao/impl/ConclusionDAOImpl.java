package com.beawan.survey.loanInfo.dao.impl;

import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.loanInfo.bean.Conclusion;
import com.beawan.survey.loanInfo.dao.IConclusionDAO;

@Service("conclusionDAO")
public class ConclusionDAOImpl extends CommDAOImpl<Conclusion> implements
		IConclusionDAO {

}
