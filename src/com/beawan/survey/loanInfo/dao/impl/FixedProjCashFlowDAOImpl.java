package com.beawan.survey.loanInfo.dao.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.loanInfo.bean.FixedProjCashFow;
import com.beawan.survey.loanInfo.dao.IFixedProjCashFlowDAO;

@Service("fixedProjCashFlowDAO")
public class FixedProjCashFlowDAOImpl extends CommDAOImpl<FixedProjCashFow> implements IFixedProjCashFlowDAO{

	@Override
	public List<FixedProjCashFow> queryFixedProjCashFlowListByTaskId(Long taskId) throws DataAccessException {
		// TODO Auto-generated method stub
		String query = "from FixedProjCashFow as model where model.taskId ='" + taskId + "' order by id asc";
		return getEntityManager().createQuery(query).getResultList();
	}
	
	
	

}
