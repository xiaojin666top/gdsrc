package com.beawan.survey.loanInfo.dao.impl;




import java.util.List;

import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.loanInfo.bean.FixedProjPlan;
import com.beawan.survey.loanInfo.dao.IFixedProjPlanDAO;

@Service("fixedProjPlanDAO")
public class FixedProjPlanDAOImpl extends CommDAOImpl<FixedProjPlan> implements IFixedProjPlanDAO {

	public List<FixedProjPlan> queryFixedProjPlanListByTaskId(Long taskId){
		String query = "from FixedProjPlan as model where model.taskId ='" + taskId + "'  order by id asc";
		return getEntityManager().createQuery(query).getResultList();
	}
}
