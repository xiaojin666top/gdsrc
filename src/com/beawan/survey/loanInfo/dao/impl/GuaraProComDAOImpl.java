package com.beawan.survey.loanInfo.dao.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.loanInfo.bean.GuaraProCom;
import com.beawan.survey.loanInfo.dao.IGuaraProComDAO;

@Service("guaraProComDAO")
public class GuaraProComDAOImpl extends CommDAOImpl<GuaraProCom> implements IGuaraProComDAO{

	@Override
	public List<GuaraProCom> queryGuaraProComListByTaskId(Long taskId) throws DataAccessException {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		String query = "from GuaraProCom as model where model.taskId ='" + taskId + "'";
		return getEntityManager().createQuery(query).getResultList();
	}

}
