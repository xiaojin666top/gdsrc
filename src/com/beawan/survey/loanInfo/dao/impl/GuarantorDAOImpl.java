package com.beawan.survey.loanInfo.dao.impl;

import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.loanInfo.bean.Guarantor;
import com.beawan.survey.loanInfo.dao.IGuarantorDAO;

@Service("guarantorDAO")
public class GuarantorDAOImpl extends CommDAOImpl<Guarantor> implements IGuarantorDAO{

}
