package com.beawan.survey.loanInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.loanInfo.dao.ApplyRateInfoDao;
import com.beawan.survey.loanInfo.bean.ApplyRateInfo;
import org.springframework.stereotype.Repository;
import com.beawan.survey.loanInfo.bean.ApplyRateInfo;

/**
 * @author yzj
 */
@Repository("applyRateInfoDao")
public class ApplyRateInfoDaoImpl extends BaseDaoImpl<ApplyRateInfo> implements ApplyRateInfoDao{

}
