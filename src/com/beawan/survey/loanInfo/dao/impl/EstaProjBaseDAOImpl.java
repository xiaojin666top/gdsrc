package com.beawan.survey.loanInfo.dao.impl;

import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.loanInfo.bean.EstaProjBase;
import com.beawan.survey.loanInfo.dao.IEstaProjBaseDAO;

@Service("estaProjBaseDAO")
public class EstaProjBaseDAOImpl extends CommDAOImpl<EstaProjBase> implements IEstaProjBaseDAO {
	

}
