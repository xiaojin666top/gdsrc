package com.beawan.survey.loanInfo.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.loanInfo.bean.ApplyInfo;
import com.beawan.survey.loanInfo.bean.FixedProjBase;
import com.beawan.survey.loanInfo.dao.IFixedProjBaseDAO;

@Service("fixedProjBaseDAO")
public class FixedProjBaseDAOImpl extends CommDAOImpl<FixedProjBase> implements
		IFixedProjBaseDAO {

	

}
