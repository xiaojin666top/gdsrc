package com.beawan.survey.loanInfo.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.loanInfo.bean.CusLaw;
import com.beawan.survey.loanInfo.dao.ICusLawDAO;

@Service("cusLawDAO")
public class CusLawDAOImpl extends CommDAOImpl<CusLaw> implements ICusLawDAO{

	@Override
	public List<CusLaw> getLawInfoListByNo(String customerNo) throws DataAccessException{
		
		String query="customerNo=:customerNo";
		
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("customerNo", customerNo);
		
		return select(query, params);
	}
	
	
}
