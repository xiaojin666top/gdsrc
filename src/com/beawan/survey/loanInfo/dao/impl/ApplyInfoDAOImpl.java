package com.beawan.survey.loanInfo.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.custInfo.bean.CompBase;
import com.beawan.survey.loanInfo.bean.ApplyInfo;
import com.beawan.survey.loanInfo.dao.IApplyInfoDAO;

@Service("applyInfoDAO")
public class ApplyInfoDAOImpl extends CommDAOImpl<ApplyInfo> implements
		IApplyInfoDAO {

	@Override
	public ApplyInfo queryApplyInfoByTaskId(Long taskId) throws DataAccessException {
		// TODO Auto-generated method stub
		String hql = "taskId=:taskId";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("taskId", taskId);
		return selectSingle(ApplyInfo.class, hql, params);
	}

}
