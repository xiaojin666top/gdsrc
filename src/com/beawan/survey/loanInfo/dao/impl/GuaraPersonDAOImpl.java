package com.beawan.survey.loanInfo.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.loanInfo.bean.GuaraCom;
import com.beawan.survey.loanInfo.bean.GuaraPerson;
import com.beawan.survey.loanInfo.dao.IGuaraPersonDAO;

@Service("guaraPersonDAO")
public class GuaraPersonDAOImpl extends CommDAOImpl<GuaraPerson> implements IGuaraPersonDAO{

	@Override
	public List<GuaraPerson> queryGuaraPersonListByTaskId(Long taskId) throws DataAccessException {
		// TODO Auto-generated method stub
		String query = "from GuaraPerson as model where model.taskId ='" + taskId + "'";
		return getEntityManager().createQuery(query).getResultList();
	}

	@Override
	public GuaraPerson queryGuaraPersonByTaskIdAndNO(Long taskId, String customerNo) throws DataAccessException {
		// TODO Auto-generated method stub
		String hql = "taskId=:taskId And psCusNo=:customerNo";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("taskId", taskId);
		params.put("customerNo", customerNo);
		return selectSingle(GuaraPerson.class, hql, params);
	}

}
