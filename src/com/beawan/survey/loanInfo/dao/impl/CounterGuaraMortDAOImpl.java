package com.beawan.survey.loanInfo.dao.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.loanInfo.bean.CounterGuaraMort;
import com.beawan.survey.loanInfo.dao.ICounterGuaraComDAO;
import com.beawan.survey.loanInfo.dao.ICounterGuaraMortDAO;

@Service("counterGuaraMortDAO")
public class CounterGuaraMortDAOImpl extends CommDAOImpl<CounterGuaraMort> implements ICounterGuaraMortDAO{

	@Override
	public List<CounterGuaraMort> quertCounterGuaraMortListByGuaraId(Long guaraId) throws DataAccessException {
		// TODO Auto-generated method stub
		String query = "from CounterGuaraMort as model where model.guaraId ='" + guaraId + "'";
		return getEntityManager().createQuery(query).getResultList();
	}

}
