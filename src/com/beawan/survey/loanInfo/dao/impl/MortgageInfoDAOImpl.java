package com.beawan.survey.loanInfo.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.loanInfo.bean.MortgageInfo;
import com.beawan.survey.loanInfo.dao.IMortgageInfoDAO;

@Service("mortgageInfoDAO")
public class MortgageInfoDAOImpl extends CommDAOImpl<MortgageInfo> implements IMortgageInfoDAO{

	@Override
	public List<MortgageInfo> queryMortgageInfoListByTaskId(Long taskId) throws DataAccessException{
//		String query = "from MortgageInfo as model where model.taskId = " + taskId;
//		return getEntityManager().createQuery(query).getResultList();
		
		String query = "taskId=:taskId";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("taskId", taskId);
		
		return select(query, params);
	}

}
