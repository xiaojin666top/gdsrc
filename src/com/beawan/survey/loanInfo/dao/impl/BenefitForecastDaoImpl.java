package com.beawan.survey.loanInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.loanInfo.dao.BenefitForecastDao;
import com.beawan.survey.loanInfo.bean.BenefitForecast;
import org.springframework.stereotype.Repository;
import com.beawan.survey.loanInfo.bean.BenefitForecast;

/**
 * @author yzj
 */
@Repository("benefitForecastDao")
public class BenefitForecastDaoImpl extends BaseDaoImpl<BenefitForecast> implements BenefitForecastDao{

}
