package com.beawan.survey.loanInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.loanInfo.dao.BenefitAnalyDao;
import com.beawan.survey.loanInfo.bean.BenefitAnaly;
import org.springframework.stereotype.Repository;
import com.beawan.survey.loanInfo.bean.BenefitAnaly;

/**
 * @author yzj
 */
@Repository("benefitAnalyDao")
public class BenefitAnalyDaoImpl extends BaseDaoImpl<BenefitAnaly> implements BenefitAnalyDao{

}
