package com.beawan.survey.loanInfo.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.loanInfo.bean.FixedProjInvest;

/**
 * @Description 固定资产项目投资资金情况持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IFixedInvestDAO extends ICommDAO<FixedProjInvest>{
	
	/**
	 * @Description (根据任务号查询固定资产项目建设计划)
	 * @param taskId  任务号
	 *            
	 * @return
	 */
	public List<FixedProjInvest> queryFixedProjInvestListByTaskId(Long taskId) throws DataAccessException;

}
