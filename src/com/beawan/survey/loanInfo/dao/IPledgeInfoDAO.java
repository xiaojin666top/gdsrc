package com.beawan.survey.loanInfo.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.loanInfo.bean.PledgeInfo;
/**
 * @Description 质押物信息持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IPledgeInfoDAO extends ICommDAO<PledgeInfo>{
	/**
	 * @Description (根据任务号查询质押物信息)
	 * @param taskId  任务号
	 *            
	 * @return
	 */
	public List<PledgeInfo> queryPledgeInfoListByTaskId(Long taskId) throws DataAccessException;
}
