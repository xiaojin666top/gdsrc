package com.beawan.survey.loanInfo.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.loanInfo.bean.ApplyRate;
/**
 * @Description 申请信息利费率持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IApplyRateDAO extends ICommDAO<ApplyRate>{

	/**
	 * @Description (根据任务号查询本次授信申利费率情况列表)
	 * @param taskId 任务号
	 * @return List<ApplyRate>
	 * @throws Exception
	 */
	public List<ApplyRate> queryApplyRateListByTaskId(Long taskId) throws DataAccessException;
}
