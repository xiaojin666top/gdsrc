package com.beawan.survey.loanInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.loanInfo.bean.BenefitForecast;

/**
 * @author yzj
 */
public interface BenefitForecastDao extends BaseDao<BenefitForecast> {
}
