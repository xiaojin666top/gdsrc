package com.beawan.survey.loanInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.loanInfo.bean.BenefitAnaly;

/**
 * @author yzj
 */
public interface BenefitAnalyDao extends BaseDao<BenefitAnaly> {
}
