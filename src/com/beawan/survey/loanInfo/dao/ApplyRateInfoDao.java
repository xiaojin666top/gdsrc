package com.beawan.survey.loanInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.loanInfo.bean.ApplyRateInfo;

/**
 * @author yzj
 */
public interface ApplyRateInfoDao extends BaseDao<ApplyRateInfo> {
}
