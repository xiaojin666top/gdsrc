package com.beawan.survey.loanInfo.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.loanInfo.bean.ApplyScheme;
/**
 * @Description 申请方案持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IApplySchemeDAO extends ICommDAO<ApplyScheme> {

	/**
	 * @Description (根据任务号查询本次授信安排方案)
	 * @param taskId  任务号
	 *            
	 * @return
	 */
	public List<ApplyScheme> queryApplySchemeListByTaskId(Long taskId) throws DataAccessException;
}
