package com.beawan.survey.loanInfo.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.loanInfo.bean.CounterGuaraMort;

/**
 * @Description 担保公司反担保抵质押物信息持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface ICounterGuaraMortDAO extends ICommDAO<CounterGuaraMort>{
	/**
	 * @Description (根据任务号查询担保公司反担保抵质押物信息)
	 * @param taskId  任务号
	 *            
	 * @return
	 */
	public List<CounterGuaraMort> quertCounterGuaraMortListByGuaraId(Long guaraId) throws DataAccessException;
 }
