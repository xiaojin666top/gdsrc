package com.beawan.survey.loanInfo.dao;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.loanInfo.bean.EstaProjBase;

/**
 * @Description 房地产开发贷款项目信息持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IEstaProjBaseDAO extends ICommDAO<EstaProjBase> {
	

}
