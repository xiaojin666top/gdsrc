package com.beawan.survey.loanInfo.dao;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.loanInfo.bean.ApplyInfo;

/**
 * @Description 申请信息持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IApplyInfoDAO extends ICommDAO<ApplyInfo> {
	
	
	public ApplyInfo queryApplyInfoByTaskId(Long taskId) throws DataAccessException;
}
