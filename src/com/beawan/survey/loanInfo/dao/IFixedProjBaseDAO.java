package com.beawan.survey.loanInfo.dao;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.loanInfo.bean.ApplyInfo;
import com.beawan.survey.loanInfo.bean.FixedProjBase;

/**
 * @Description 固定资产贷款项目信息持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IFixedProjBaseDAO extends ICommDAO<FixedProjBase> {


}
