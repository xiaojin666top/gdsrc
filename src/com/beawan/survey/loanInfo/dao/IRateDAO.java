package com.beawan.survey.loanInfo.dao;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.loanInfo.bean.Rate;

public interface IRateDAO extends ICommDAO<Rate>{

	/***
	 * 根据任务号获取利率定价计算项目
	 * @param taskId
	 * @return
	 * @throws DataAccessException
	 */
	public Rate selectRateById(Long taskId) throws DataAccessException;
	
	
}
