package com.beawan.survey.loanInfo.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.loanInfo.bean.CusLaw;

public interface ICusLawDAO  extends ICommDAO<CusLaw>{
	
	/***
	 * 根据客户号从数据库从查找裁判文书列表
	 * @param customerNo
	 * @return
	 */
	public List<CusLaw> getLawInfoListByNo(String customerNo) throws DataAccessException;
	

}
