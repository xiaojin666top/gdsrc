package com.beawan.survey.loanInfo.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.loanInfo.bean.FixedProjFinaPlan;


/**
 * @Description 固定资产项目建设筹资计划持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IFixedProjFinaPlanDAO extends ICommDAO<FixedProjFinaPlan>{
	
	/**
	 * @Description (根据任务号查询固定资产项目建设筹资计划)
	 * @param taskId  任务号
	 *            
	 * @return
	 */
	public List<FixedProjFinaPlan> queryFixedProjFinaPlanByTaskId(Long taskId) throws DataAccessException;
}
