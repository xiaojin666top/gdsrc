package com.beawan.survey.loanInfo.dao;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.loanInfo.bean.GuaranteeInfo;

/**
 * @Description 担保信息持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IGuaranteeInfoDAO extends ICommDAO<GuaranteeInfo> {

	
	
}
