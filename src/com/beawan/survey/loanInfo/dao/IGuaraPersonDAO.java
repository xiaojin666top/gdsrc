package com.beawan.survey.loanInfo.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.loanInfo.bean.GuaraPerson;

/**
 * @Description 自然人担保信息持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IGuaraPersonDAO extends ICommDAO<GuaraPerson>{
	/**
	 * @Description (根据任务号查询自然人担保信息列表)
	 * @param taskId  任务号
	 *            
	 * @return
	 */
	public List<GuaraPerson> queryGuaraPersonListByTaskId(Long taskId) throws DataAccessException;
	
	
	
	/**
	 * @Description (根据任务号查询自然人担保信息)
	 * @param taskId  任务号
	 *            
	 * @return
	 */
	public GuaraPerson queryGuaraPersonByTaskIdAndNO(Long taskId,String customerNo) throws DataAccessException;
}
