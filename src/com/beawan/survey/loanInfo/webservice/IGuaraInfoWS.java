package com.beawan.survey.loanInfo.webservice;

import java.util.List;


import com.beawan.survey.loanInfo.bean.GuaraCom;
import com.beawan.survey.loanInfo.bean.GuaraPerson;
import com.beawan.survey.loanInfo.bean.GuaraProCom;
import com.beawan.survey.loanInfo.bean.GuaranteeInfo;
import com.beawan.survey.loanInfo.bean.Guarantor;


/**
 * @Description 担保信息webservice服务接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IGuaraInfoWS {
	
	/**
	 * @Description (根据id查询保证人信息)
	 * @param id 保证人ID
	 * @return Guarantor
	 * @throws Exception
	 */
	public String findGuarantorById(Long id);
	
	/**
	 * @Description (根据任务号查询保证人列表)
	 * @param taskId 任务号
	 * @return List<Guarantor>
	 * @throws Exception
	 */
	public String findGuarantorByTaskId(Long taskId);
	
	
	/**
	 * @Description (保存授信担保人信息)
	 * @return Guarantor
	 * @throws Exception
	 */
	public String saveGuarantor(String guarantorinfo,String finaRepYear);
	
	
	/**
	 * @Description (保存授信担保人信息列表)
	 * @param id 保证人ID
	 * @return Guarantor
	 * @throws Exception
	 */
	public String saveGuarantorList(Long taskId,String jsonArray);
	
	/**
	 * @Description (根据任务号查询授信担保信息)
	 * @param taskId 任务号
	 * @return GuaraInfo
	 * @throws Exception
	 */
	public String findGuaraInfoBytaskId(Long taskId);
	
	/**
	 * @Description (保存授信担保信息)
	 * @param taskId 任务号
	 * @return GuaraInfo
	 * @throws Exception
	 */
	public String saveGuaraInfo(String guaraInfo);
	
	
	
	/**
	 * @Description (查询一般公司担保实体)
	 * @param id 保证人ID
	 * @return 
	 * @throws Exception
	 */
	public String findGuaraComById(Long guaraId);
	

	
	/**
	 * @Description (保存一般公司担保信息)
	 * @param id 保证人ID
	 * @return 
	 * @throws Exception
	 */
	public String saveGuaraCom(String guaraCom);
	
	
	
	/**
	 * @Description (查询担保公司担保实体)
	 * @param id 保证人ID
	 * @return 
	 * @throws Exception
	 */
	public String findGuaraProComById(Long guaraId);
	
	/**
	 * @Description (保存担保公司担保信息)
	 * @param id 保证人ID
	 * @return 
	 * @throws Exception
	 */
	public String saveGuaraProCom(String guaraProCom) ;
	
	/**
	 * @Description (查询自然人担保实体)
	 * @param id 保证人ID
	 * @return 
	 * @throws Exception
	 */
	public String findGuaraPersonById(Long guaraId);
	
	/**
	 * @Description (保存自然人担保信息)
	 * @param id 保证人ID
	 * @return 
	 * @throws Exception
	 */
	public String saveGuaraPeson(String guaraPerson) ;
	
	
	/**
	 * @Description (根据任务号担保公司反担保企业信息列表)
	 * @param taskId 任务号
	 * @return List<CounterGuaraCom>
	 * @throws Exception
	 */
	public String findCounterGuaraComListByGuaraId(Long guaraId) ;
	
	/**
	 * @Description (保存担保公司反担保企业信息列表)
	 * @param taskId 任务号
	 * @return 
	 * @throws Exception
	 */
	public String saveCounterGuaraComList(Long guaraId,String jsonArray);
	
	
	/**
	 * @Description (根据任务号担保公司反担保抵质押物信息列表)
	 * @param taskId 任务号
	 * @return List<CounterGuaraMort>
	 * @throws Exception
	 */
	public String findCounterGuaraMortListByGuaraId(Long guaraId) ;
	
	/**
	 * @Description (保存担保公司反担保抵质押物列表)
	 * @param taskId 任务号
	 * @return 
	 * @throws Exception
	 */
	public String saveCounterGuaraMortList(Long guaraId,String jsonArray);
	
	
	/**
	 * @Description (根据任务号查询抵押物信息列表)
	 * @param taskId 任务号
	 * @return List<MortgageInfo>
	 * @throws Exception
	 */
	public String findMortgageInfoListByTaskId(Long taskId);
	
	/**
	 * @Description (保存抵押物信息列表)
	 * @param taskId 任务号
	 * @return 
	 * @throws Exception
	 */
	public String saveMortgageInfoList(Long taskId,String jsonArray);
	
	
	/**
	 * @Description (根据任务号查询质押物信息列表)
	 * @param taskId 任务号
	 * @return List<PledgeInfo>
	 * @throws Exception
	 */
	public String findPledgeInfoListByTaskId(Long taskId);
	
	/**
	 * @Description (保存质押物信息列表)
	 * @param taskId 任务号
	 * @return 
	 * @throws Exception
	 */
	public String savePledgeInfoInfoList(Long taskId,String jsonArray);
}
