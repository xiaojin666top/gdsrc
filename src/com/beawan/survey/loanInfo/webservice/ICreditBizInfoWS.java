package com.beawan.survey.loanInfo.webservice;

import org.springframework.dao.DataAccessException;

public interface ICreditBizInfoWS {
	
	/**
	 * 查看调查报告
	 * @param taskId
	 * @return
	 */
	public String viewSurveyReport(Long taskId);
	
	
	
	/**
	 * 查看主要财务报表
	 * @param taskId
	 * @param customerNo
	 * @param reportType
	 * @return
	 */
	public String mainFinanceReport(Long taskId,String customerNo,String reportType);
	
	
	
	/**
	 * 查看财务情况-系统分析结论
	 * @param taskId
	 * @param customerNo
	 * @param reportType
	 * @return
	 */
	public String conclusionReport(Long taskId,String customerNo);
	
	
	/**
	 * 创建调查报告
	 * @param taskId
	 * @return
	 */
	public String createReport(Long taskId);
	
}
