package com.beawan.survey.loanInfo.webservice.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.beawan.common.util.WSResult;
import com.beawan.survey.loanInfo.bean.FixedProjBase;
import com.beawan.survey.loanInfo.bean.FixedProjCashFow;
import com.beawan.survey.loanInfo.bean.FixedProjFinaPlan;
import com.beawan.survey.loanInfo.bean.FixedProjInvest;
import com.beawan.survey.loanInfo.bean.FixedProjInvestEst;
import com.beawan.survey.loanInfo.bean.FixedProjPlan;
import com.beawan.survey.loanInfo.service.IFixedProjSV;
import com.beawan.survey.loanInfo.webservice.IFixedProjWS;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.platform.util.JacksonUtil;

@Service("fixedProjWS")
public class FixedProjWSImpl implements IFixedProjWS {

	private static final Logger log = Logger.getLogger(IFixedProjWS.class);
	
	@Resource
	protected IFixedProjSV fixedProjSV;
	
	@Override
	public String saveOrUpdateFixedProjBase(String fixedProjBase) {
		WSResult result=new WSResult();
		ObjectMapper mapper=new ObjectMapper();
		FixedProjBase fProjBase=null;
		try {
			fProjBase=mapper.readValue(fixedProjBase, FixedProjBase.class);
			fixedProjSV.saveProjBase(fProjBase);
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("保存固定资产投资项目情况异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String queryFixedProjBaseByTaskIdAndCusNo(Long taskId) {
		WSResult result=new WSResult();
		try {
			FixedProjBase fProjBase=fixedProjSV.queryProjBaseByTaskId(taskId);
			if(fProjBase==null) 
				result.data=null;
			else 
			result.data=JacksonUtil.serialize(fProjBase);
			
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取固定资产投资项目情况异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveFixedProjPlanList(Long taskId, String jsonArray) {
		WSResult result = new WSResult();
		
		try {
			fixedProjSV.saveProjPlanList(taskId, jsonArray);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存固定资产项目建设计划异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String queryFixedProjPlanListByTaskId(Long taskId) {
		WSResult result = new WSResult();
		
		try {
			List<FixedProjPlan> list = fixedProjSV.queryProjPlanByTaskId(taskId);
			if(CollectionUtils.isEmpty(list))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(list);
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取固定资产项目建设计划异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveFixedProjInvestList(Long taskId, String jsonArray) {
		WSResult result = new WSResult();
		
		try {
			fixedProjSV.saveProjInvestList(taskId, jsonArray);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存固定资产项目投资情况异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String queryFixedProjInvestListByTaskId(Long taskId) {
		WSResult result = new WSResult();
		
		try {
			List<FixedProjInvest> list = fixedProjSV.queryProjInvestByTaskId(taskId);
			if(CollectionUtils.isEmpty(list))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(list);
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取固定资产项目投资情况异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveFixedProjInvestEstList(Long taskId, String jsonArray) {
		WSResult result = new WSResult();
		
		try {
			fixedProjSV.saveProjInvestEstList(taskId, jsonArray);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存固定资产项目投资估算异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String queryFixedProjInvestEstListByTaskId(Long taskId) {
		WSResult result = new WSResult();
		
		try {
			List<FixedProjInvestEst> list = fixedProjSV.queryProjInvestEstByTaskId(taskId);
			if(CollectionUtils.isEmpty(list))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(list);
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取固定资产项目投资估算异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveFixedProjFinaPlanList(Long taskId, String jsonArray) {
		WSResult result = new WSResult();
		
		try {
			fixedProjSV.saveProjFinaPlanList(taskId, jsonArray);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存固定资产项目建设筹资计划异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String queryFixedProjFinaPlanListByTaskId(Long taskId) {
		WSResult result = new WSResult();
		
		try {
			List<FixedProjFinaPlan> list = fixedProjSV.queryProjFinaPlanByTaskId(taskId);
			if(CollectionUtils.isEmpty(list))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(list);
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取固定资产项目建设筹资计划异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveFixedProjCashFowList(Long taskId, String jsonArray) {
		WSResult result = new WSResult();
		
		try {
			fixedProjSV.saveProjCashFowList(taskId, jsonArray);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存固定资产项目建设现金流测算异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String queryFixedProjCashFowListByTaskId(Long taskId) {
		WSResult result = new WSResult();
		
		try {
			List<FixedProjCashFow> list = fixedProjSV.queryProjCashFowByTaskId(taskId);
			if(CollectionUtils.isEmpty(list))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(list);
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取固定资产项目建设现金流测算异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	
}
