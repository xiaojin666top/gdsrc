package com.beawan.survey.loanInfo.webservice.impl;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.beawan.common.util.WSResult;
import com.beawan.survey.loanInfo.bean.Conclusion;
import com.beawan.survey.loanInfo.bean.RiskAnalysis;
import com.beawan.survey.loanInfo.service.IConclusionSV;
import com.beawan.survey.loanInfo.webservice.IConclusionWS;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.platform.util.JacksonUtil;

@Service("conclusionWS")
public class ConclusionWSImpl implements IConclusionWS {
	
private static final Logger log = Logger.getLogger(IConclusionWS.class);
	
	@Resource
	protected IConclusionSV conclusionSV;
	@Override
	public String findRiskAnalysisByTaskId(Long taskId) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		try {
			
			RiskAnalysis riskInfo = conclusionSV.findRiskAnalysisByTaskId(taskId);
			if(riskInfo == null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(riskInfo);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取本次信贷风险分析异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveRiskAnalysis(String riskAnalysisInfo) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		
		ObjectMapper mapper = new ObjectMapper();
		RiskAnalysis riskAnalysis = null;
		
		try {
			riskAnalysis = mapper.readValue(riskAnalysisInfo,RiskAnalysis.class);
			conclusionSV.saveRiskAnalysis(riskAnalysis);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存本次信贷风险分析异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String updateAndfindConclusionByTaskId(Long taskId) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		try {
			
			Conclusion conclusionInfo = conclusionSV.updateAndFindConclusionByTaskId(taskId);
			if(conclusionInfo == null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(conclusionInfo);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取综合结论异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveConclusion(String conclusionInfo) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		
		ObjectMapper mapper = new ObjectMapper();
		Conclusion conclusion = null;
		
		try {
			conclusion = mapper.readValue(conclusionInfo,Conclusion.class);
			conclusionSV.saveConclusion(conclusion);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存本次信贷风险分析异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}
	

}
