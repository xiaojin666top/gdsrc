package com.beawan.survey.loanInfo.webservice.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.beawan.survey.custInfo.dto.CompBaseDto;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.beawan.base.entity.TableSubjCode;
import com.beawan.base.service.ISysDicSV;
import com.beawan.base.service.ITableSubjCodeSV;
import com.beawan.common.SysConstants;
import com.beawan.common.util.SystemUtil;
import com.beawan.common.util.WSResult;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.beawan.survey.loanInfo.bean.ApplyInfo;
import com.beawan.survey.loanInfo.bean.ApplyRate;
import com.beawan.survey.loanInfo.bean.ApplyScheme;
import com.beawan.survey.loanInfo.bean.Guarantor;
import com.beawan.survey.loanInfo.bean.MortgageInfo;
import com.beawan.survey.loanInfo.bean.PledgeInfo;
import com.beawan.survey.loanInfo.bean.Rate;
import com.beawan.survey.loanInfo.qbean.NameAndRate;
import com.beawan.survey.loanInfo.service.IApplyInfoSV;
import com.beawan.survey.loanInfo.service.IGuaraInfoSV;
import com.beawan.survey.loanInfo.util.GetRateUtil;
import com.beawan.survey.loanInfo.webservice.IApplyInfoWS;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.platform.util.JacksonUtil;


@Service("applyInfoWS")
public class ApplyInfoWSImpl implements IApplyInfoWS {
	
	private static final Logger log = Logger.getLogger(IApplyInfoWS.class);
	
	@Resource
	protected IApplyInfoSV applyInfoSV;
	@Resource
	protected ITaskSV taskSV;
	@Resource
	private ITableSubjCodeSV tableSubjCodeSV;
	@Resource
	private ICmisInInvokeSV cmisInInvokeSV;
	@Resource
	private ICompBaseSV compBaseSV;
	
	@Resource
	private IGuaraInfoSV guaraInfoSV;
	@Resource
	private ISysDicSV sysDicSV;
	
	@Override
	public String findApplyInfoByTaskId(Long taskId) {	
		
		WSResult result = new WSResult();
		
		try {
			
			ApplyInfo applyInfo = applyInfoSV.findApplyInfoByTaskId(taskId);
			if(applyInfo == null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(applyInfo);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取本次授信申请情况异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveApplyInfo(String applyInfo) {
		
		WSResult result = new WSResult();
		
		ObjectMapper mapper = new ObjectMapper();
		ApplyInfo apply = null;
		
		try {
			apply = mapper.readValue(applyInfo,ApplyInfo.class);
			applyInfoSV.saveApplyInfo(apply);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存本次授信申请情况异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findApplySchemeListByTaskId(Long taskId) {
		
		WSResult result = new WSResult();
		
		try {
			List<ApplyScheme> list = applyInfoSV.findApplySchemeListByTaskId(taskId);
			if(CollectionUtils.isEmpty(list))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(list);
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取本次授信安排方案列表异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveApplySchemeList(Long taskId, String jsonArray) {
		
		WSResult result = new WSResult();
		
		try {
			applyInfoSV.saveApplySchemeList(taskId, jsonArray);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存本次授信安排方案列表异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findApplyRateListByTaskIdAndNO(Long taskId,String customerNo) {
		
		WSResult result = new WSResult();
		
		try {
			List<ApplyRate> list=applyInfoSV.queryApplyRateListByTaskId(taskId);
			if(list==null||list.size()==0) {
			 list = applyInfoSV.findApplyRateListByCustomerNo(customerNo);
			}
			if(CollectionUtils.isEmpty(list))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(list);
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取本次授信利费率情况列表异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveApplyRateList(Long taskId, String jsonArray) {
		
		WSResult result = new WSResult();
		
		try {
			applyInfoSV.saveApplyRateList(taskId, jsonArray);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存本次授信利费率情况列表异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String getRePaymentAnlyHtml(Long taskId) {
		WSResult result = new WSResult();
		Map<String, Object> map=new HashMap<String,Object>();
		try {
			
			Task task = taskSV.getTaskById(taskId);
			map.put("task", task);
			
			ApplyInfo  applyInfo = applyInfoSV.findApplyInfoByTaskId(taskId);
			
			if(applyInfo == null ){
				applyInfo = new ApplyInfo();
				applyInfo.setTaskId(taskId);
			}
			map.put("applyInfo", applyInfo);
			
			List<String> dateListShow = SystemUtil.genFinaDateList(task.getYear(),
					task.getMonth(), false, true);
			map.put("dateList", dateListShow);
			
			//显示的财报科目列表
			List<TableSubjCode> finaSubjects = tableSubjCodeSV.queryByTableSubjCodes("all", "FINA_AMT");
			map.put("finaSubjects", finaSubjects);
			
			//额度测算
			Map<String, Object> calResult = applyInfoSV.getCreditAmtCalResult(task, applyInfo);
			map.put("amtCalFinaData", calResult.get("amtCalFinaData"));
			map.put("amtCalResult", calResult.get("amtCalResult"));
			if(CollectionUtils.isEmpty(map))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(map);
		} catch (Exception e) {
			log.error("授信申请额度及还款能力页面获取异常：", e);
		}
		
		return result.json();
	}

	@Override
	public String getLastCreditInfo(Long taskId) {
		WSResult result=new WSResult();

		try {
			
			Task task=taskSV.getTaskById(taskId);
			
			List<String> infoList = cmisInInvokeSV.getLastCreditInfo(task.getCustomerNo());
			
			if(CollectionUtils.isEmpty(infoList)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(infoList);
			}
			
			
		} catch (Exception e) {
			log.error("查询上期授信情况异常：", e);
			result.status=WSResult.STATUS_ERROR;
		}
	
		return result.json();
	}

	@Override
	public String getRateAnlyHtml(Long taskId) {
		WSResult result = new WSResult();
		Map<String, Object> map=new HashMap<String,Object>();
		try {
			
			Task task = this.taskSV.getTaskById(taskId);
			map.put("task", task);
			
			int loanTerm=task.getLoanTerm();
			map.put("loanTerm", loanTerm);
			
			double basePrice=0;
			if(loanTerm<=1) {
				basePrice=3.614;
			}else if(loanTerm<=5) {
				basePrice=4.391;
			}else {
				basePrice=4.718;
			}
			map.put("basePrice", basePrice);
			
			
			String customerNo=task.getCustomerNo();
			CompBaseDto compBase = compBaseSV.findCompBaseByCustNo(customerNo);
			
			String cutsomerLev=compBase.getCreditLevel();
			map.put("cutsomerLev", cutsomerLev);
			
			double cusRate=GetRateUtil.getRateByCusLev(cutsomerLev);
			map.put("cusRate", cusRate);
			
			List<NameAndRate> allList=new ArrayList<NameAndRate>();
			
			List<Guarantor> glList=guaraInfoSV.findGuarantorByTaskId(taskId);
			
			
			
			if(glList!=null&&glList.size()>0) {
				List<NameAndRate> guaralist=GetRateUtil.getlistFromGuarator(glList);				
				allList.addAll(guaralist);
			}
			List<MortgageInfo> mList=guaraInfoSV.findMortgageInfoByTaskId(taskId);
			if(mList!=null&&mList.size()>0) {
				List<NameAndRate> morlist=GetRateUtil.getlistFromMor(mList);
				allList.addAll(morlist);
			}
			List<PledgeInfo>  pList=guaraInfoSV.findPledgeInfoByTaskId(taskId);
			if(mList!=null&&mList.size()>0) {
				List<NameAndRate> pledlist=GetRateUtil.getlistFromPled(pList);				
				allList.addAll(pledlist);
			}
			
			if(allList==null||allList.size()==0) {
				NameAndRate bean=new NameAndRate();
				bean.setName("信用贷款");
				bean.setRate(1.2);
				List<NameAndRate> honor=new ArrayList<NameAndRate>();
				honor.add(bean);
				map.put("honor", honor);
				map.put("minRate", 1.2);
			}else {
				double min=100;
				for(NameAndRate data:allList) {
					double newData=data.getRate();
					if(newData<min)
						min=newData;					
				}
				map.put("minRate", min);
				map.put("allList", allList);				
			}
			
			ApplyInfo  applyInfo = applyInfoSV.findApplyInfoByTaskId(taskId);
			
			if(applyInfo == null ){
				applyInfo = new ApplyInfo();
				applyInfo.setTaskId(taskId);
			}
			map.put("applyInfo", applyInfo);
			
			//加载字典数据
			String[] optTypes = {
					SysConstants.BsDicRateConstant.STD_RATE_BASE_EDIT,
					SysConstants.BsDicRateConstant.STD_RATE_MODEL_EDIT,
					SysConstants.BsDicRateConstant.STD_RATE_MOR,
					SysConstants.BsDicRateConstant.STD_RATE_POL,
					SysConstants.BsDicRateConstant.STD_RATE_GUR
					};
			map.put("dicData", sysDicSV.queryMapListByTypes(optTypes));
			
			Rate rateInfo=applyInfoSV.selectRateById(taskId);
			if(rateInfo == null ){
				rateInfo = new Rate();
				rateInfo.setTaskId(taskId);
				rateInfo.setWay("0");
				rateInfo.setModel("1");
				rateInfo.setCdRate("0");
			}
			map.put("rateInfo", rateInfo);
			if(CollectionUtils.isEmpty(map))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(map);
		} catch (Exception e) {
			log.error("利费率定价页面获取异常：", e);
		}
		
		return result.json();
	}
	

}
