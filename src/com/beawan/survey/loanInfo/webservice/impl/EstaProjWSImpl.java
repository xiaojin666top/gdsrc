package com.beawan.survey.loanInfo.webservice.impl;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.beawan.common.util.WSResult;
import com.beawan.survey.loanInfo.bean.EstaProjBase;
import com.beawan.survey.loanInfo.service.IEstaProjSV;
import com.beawan.survey.loanInfo.webservice.IEstaProjWS;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.platform.util.JacksonUtil;

@Service("estaProjWS")
public class EstaProjWSImpl implements IEstaProjWS {

	private static final Logger log = Logger.getLogger(IEstaProjWS.class);
	
	@Resource
	protected IEstaProjSV estaProjSV;
	
	@Override
	public String findEstaProjBaseByTaskId(Long taskId) {
		WSResult result = new WSResult();
		try {
			
		EstaProjBase estaProjBase=estaProjSV.findProjBaseByTaskId(taskId);	
			
		if(estaProjBase == null)
			result.data = null;
		else
			result.data = JacksonUtil.serialize(estaProjBase);
		
	} catch (Exception e) {
		
		result.status = WSResult.STATUS_ERROR;
		log.error("查询房地产项目情况异常！",e);
		e.printStackTrace();
	}
		return result.json();
	}

	@Override
	public String saveEstaProjBase(String estaProjBase) {
		WSResult result = new WSResult();
		ObjectMapper mapper = new ObjectMapper();
		EstaProjBase eBase=null;
		try {
			eBase=mapper.readValue(estaProjBase, EstaProjBase.class);
			estaProjSV.saveProjBase(eBase);
			
		} catch (Exception e) {
			// TODO: handle exception
			result.status = WSResult.STATUS_ERROR;
			log.error("保存房地产项目情况异常！",e);
			e.printStackTrace();
		}
		return result.json();
		
	}
	

}
