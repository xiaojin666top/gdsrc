package com.beawan.survey.loanInfo.webservice.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.beawan.analysis.finansis.service.IFinanasisService;
import com.beawan.base.entity.SReportModelRow;
import com.beawan.common.Constants;
import com.beawan.common.util.SystemUtil;
import com.beawan.common.util.WSResult;
import com.beawan.customer.service.ICusFSToolSV;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.beawan.survey.custInfo.service.ICompFinanceSV;
import com.beawan.survey.loanInfo.service.ISurveyReportSV;
import com.beawan.survey.loanInfo.webservice.ICreditBizInfoWS;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.platform.util.JacksonUtil;
import com.platform.util.PropertiesUtil;

@Service("creditBizInfoWS")
public class CreditBizInfoWSImpl implements ICreditBizInfoWS {

	private static final Logger log = Logger.getLogger(ICreditBizInfoWS.class);

	@Resource
	private ISurveyReportSV surveyReportSV;

	@Resource
	private ITaskSV taskSV;

	@Resource
	protected ICompFinanceSV compFinanceSV;
	
	@Resource
	private ICmisInInvokeSV cmisInInvokeSV;
	
	@Resource
	private IFinanasisService finanasisSV;
	
	@Resource
	private ICusFSToolSV cusFSToolSV;
	
	@Override
	public String viewSurveyReport(Long taskId) {
		String result = null;
		InputStream is = null;
		try {
			Task task = taskSV.getTaskById(taskId);
			String para = PropertiesUtil.getProperty(Constants.SYSTEM_PROPS_PATH, "survey.report.path");// 文件缓存地址
			File file = new File(para + task.getSerNo() + ".doc");
			if (!file.exists()) {
				surveyReportSV.createSurveyReport(taskId);
			}
			is = new FileInputStream(file);
			int size = is.available();
			byte[] buffer = new byte[size];
			is.read(buffer);
			is.close();
			result = new String(buffer, "utf-8");
		} catch (Exception e) {
			result = "error";
			e.printStackTrace();
			log.error("查看贷前调查报告异常！", e);
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
					result = "error";
				}
			}
		}
		return result;
	}

	@Override
	public String mainFinanceReport(Long taskId, String customerNo, String reportType) {
			
		WSResult result = new WSResult();
		Map<String, Object> map=new HashMap<String,Object>();
		try {
			
			Map<String, Object> finaRepInfo = compFinanceSV.getFinaRepInfo(taskId, customerNo);
			int finaRepYear = (Integer) finaRepInfo.get("finaRepYear");
			int finaRepMonth = (Integer) finaRepInfo.get("finaRepMonth");
			
			List<String> dateList = SystemUtil.genFinaDateList(finaRepYear,
					finaRepMonth, true, false);
			List<String> dateListShow = SystemUtil.genFinaDateList(finaRepYear,
					finaRepMonth, true, true);
			
			List<SReportModelRow> reportModel = cusFSToolSV.findRepModelByType(customerNo, reportType);
			List<Map<String, Double>> reportData = cusFSToolSV.findRepDataByType(customerNo,
					dateList, reportType);
			
			//若当期为12份年报，则删除上年同期数据
			if(reportData.size() > dateListShow.size())
				reportData.remove(4);
			
			map.put("dateList", dateListShow);
			map.put("reportModel", reportModel);
			map.put("reportData", reportData);
			map.put("reportType", reportType);
			
			result.data = JacksonUtil.serialize(map);
			
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取主要财务报表！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String conclusionReport(Long taskId, String customerNo) {
		WSResult result = new WSResult();
		try {
			
			Map<String, Object> conclusion = finanasisSV.genAnalysisConclusion(taskId,
					customerNo);
			
			if(CollectionUtils.sizeIsEmpty(conclusion))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(conclusion);
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取财务情况系统分析结论异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String createReport(Long taskId) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		try {
		surveyReportSV.createSurveyReport(taskId);
		
	
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("生成调查报告异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

}
