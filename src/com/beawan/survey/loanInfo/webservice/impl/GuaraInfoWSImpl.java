package com.beawan.survey.loanInfo.webservice.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.beawan.common.util.WSResult;
import com.beawan.survey.loanInfo.bean.CounterGuaraCom;
import com.beawan.survey.loanInfo.bean.CounterGuaraMort;
import com.beawan.survey.loanInfo.bean.GuaraCom;
import com.beawan.survey.loanInfo.bean.GuaraPerson;
import com.beawan.survey.loanInfo.bean.GuaraProCom;
import com.beawan.survey.loanInfo.bean.GuaranteeInfo;
import com.beawan.survey.loanInfo.bean.Guarantor;
import com.beawan.survey.loanInfo.bean.MortgageInfo;
import com.beawan.survey.loanInfo.bean.PledgeInfo;
import com.beawan.survey.loanInfo.service.IGuaraInfoSV;
import com.beawan.survey.loanInfo.webservice.IGuaraInfoWS;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;



/**
 * @Description 担保信息webservice服务接口实现
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
@Service("guaraInfoWS")
public class GuaraInfoWSImpl implements IGuaraInfoWS {

	private static final Logger log = Logger.getLogger(IGuaraInfoWS.class);
	
	@Resource
	protected IGuaraInfoSV guaraInfoSV;
	
	
	@Override
	public String findGuaraInfoBytaskId(Long taskId) {
		
		WSResult result = new WSResult();
		try {
			
			GuaranteeInfo guaranteeInfo = guaraInfoSV.findGuaraInfoBytaskId(taskId);
			if(guaranteeInfo == null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(guaranteeInfo);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取担保信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveGuaraInfo(String guaraInfo) {
	
		WSResult result = new WSResult();
		
		ObjectMapper mapper = new ObjectMapper();
		GuaranteeInfo guaranteeInfo = null;
		
		try {
			guaranteeInfo = mapper.readValue(guaraInfo, GuaranteeInfo.class);
			guaraInfoSV.saveGuaraInfo(guaranteeInfo);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存担保信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveGuaraCom(String guaraCom) {
	
		WSResult result = new WSResult();
		
		ObjectMapper mapper = new ObjectMapper();
		GuaraCom guaraComInfo = null;
		
		try {
			guaraComInfo = mapper.readValue(guaraCom,GuaraCom.class);
			guaraInfoSV.saveGuaraCom(guaraComInfo);;
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存一般公司担保信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findGuaraProComById(Long guaraId) {
	
		WSResult result = new WSResult();
		
		try {
			GuaraProCom guaraProCom = guaraInfoSV.findGuaraProComById(guaraId);
			if(guaraProCom==null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(guaraProCom);
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取担保公司担保信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveGuaraProCom(String guaraProCom) {
		
		WSResult result = new WSResult();
		
		ObjectMapper mapper = new ObjectMapper();
		GuaraProCom guaraProComInfo = null;
		
		try {
			guaraProComInfo = mapper.readValue(guaraProCom,GuaraProCom.class);
			guaraInfoSV.saveGuaraProCom(guaraProComInfo);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存担保公司担保信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findGuaraPersonById(Long guaraId) {
		
		WSResult result = new WSResult();
		
		try {
			GuaraPerson guaraPerson= guaraInfoSV.findGuaraPersonById(guaraId);
			if(guaraPerson==null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(guaraPerson);
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取自然人担保信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}


	@Override
	public String saveGuaraPeson(String guaraPerson) {
		
WSResult result = new WSResult();
		
		ObjectMapper mapper = new ObjectMapper();
		GuaraPerson guaraPersonInfo = null;
		
		try {
			guaraPersonInfo = mapper.readValue(guaraPerson,GuaraPerson.class);
			guaraInfoSV.saveGuaraPerson(guaraPersonInfo);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存自然人担保信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findCounterGuaraComListByGuaraId(Long gauraId) {
		
		WSResult result = new WSResult();
		
		try {
			List<CounterGuaraCom> list = guaraInfoSV.findCounterGuaraComByGuaraId(gauraId);
			if(CollectionUtils.isEmpty(list))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(list);
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取担保公司反担保企业列表异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveCounterGuaraComList(Long guaraId, String jsonArray) {
	
		WSResult result = new WSResult();
		
		try {
			guaraInfoSV.saveCounterGuaraComList(guaraId, jsonArray);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存担保公司反担保企业列表异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findCounterGuaraMortListByGuaraId(Long guaraId) {
		
		WSResult result = new WSResult();
		
		try {
			List<CounterGuaraMort> list = guaraInfoSV.findCounterGuaraMortByGuaraId(guaraId);
			if(CollectionUtils.isEmpty(list))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(list);
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取担保公司反担保抵质押物信息列表异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveCounterGuaraMortList(Long guaraId, String jsonArray) {
		
		WSResult result = new WSResult();
		
		try {
			guaraInfoSV.saveCounterGuaraMortList(guaraId, jsonArray);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存担保公司反担保抵质押物信息列表异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findMortgageInfoListByTaskId(Long taskId) {
		
		WSResult result = new WSResult();
		
		try {
			List<MortgageInfo> list = guaraInfoSV.findMortgageInfoByTaskId(taskId);
			if(CollectionUtils.isEmpty(list))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(list);
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取抵押物列表异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveMortgageInfoList(Long taskId, String jsonArray) {
		
		WSResult result = new WSResult();
		
		try {
			guaraInfoSV.saveMortgageInfoList(taskId, jsonArray);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存抵押物列表异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findPledgeInfoListByTaskId(Long taskId) {
		
		WSResult result = new WSResult();
		
		try {
			List<PledgeInfo> list = guaraInfoSV.findPledgeInfoByTaskId(taskId);
			if(CollectionUtils.isEmpty(list))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(list);
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取质押物列表异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String savePledgeInfoInfoList(Long taskId, String jsonArray) {
		
		WSResult result = new WSResult();
		
		try {
			guaraInfoSV.savePledgeInfoList(taskId, jsonArray);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存质押物列表异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findGuaraComById(Long guaraId) {
		
		WSResult result = new WSResult();
		try {
			
			GuaraCom guaraCom = guaraInfoSV.findGuaraComById(guaraId);
			if(guaraCom == null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(guaraCom);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("查询一般公司担保信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findGuarantorById(Long id) {
		
		WSResult result = new WSResult();
		try {
			
			Guarantor guarantor = guaraInfoSV.findGuarantorById(id);
			if(guarantor == null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(guarantor);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("查询担保人信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findGuarantorByTaskId(Long taskId) {
		
		WSResult result = new WSResult();
		try {
			
			List<Guarantor> guarantorList = guaraInfoSV.findGuarantorByTaskId(taskId);
			if(CollectionUtils.isEmpty(guarantorList))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(guarantorList);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("查询担保人信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveGuarantor(String guarantorinfo,String finaRepYear) {
		
		WSResult result = new WSResult();
		
		try {
			
			Guarantor guarantor = guaraInfoSV.saveGuarantor(guarantorinfo);
			
			if(!StringUtil.isEmptyString(finaRepYear)) {				
				GuaraCom guaraCom = guaraInfoSV.findGuaraComById(guarantor.getId());
				if(guaraCom == null){
					guaraCom = new GuaraCom();
					guaraCom.setGuaraId(guarantor.getId());
				}
				guaraCom.setFinaRepYear(finaRepYear);
				guaraInfoSV.saveGuaraCom(guaraCom);
				
			}
			
			
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存担保人信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveGuarantorList(Long taskId, String jsonArray) {
		
		WSResult result = new WSResult();
		
		try {
			guaraInfoSV.saveGuarantorList(taskId, jsonArray);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存担保人列表异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}


	
	
	

}
