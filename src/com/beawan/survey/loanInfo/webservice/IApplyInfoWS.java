package com.beawan.survey.loanInfo.webservice;


/**
 * @Description 申请信息webservice服务接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IApplyInfoWS {
	/**
	 * @Description (根据任务号查询本次授信申请情况)
	 * @param taskId 任务号      
	 * @return 
	 * @
	 */
	public String findApplyInfoByTaskId(Long taskId) ;
	

	/**
	 * @Description (保存本次授信申请情况)
	 * @param ApplyInfo 本次授信申请情况实体
	 * @
	 */
	public String saveApplyInfo(String ApplyInfo) ;
	
	
	
	/**
	 * @Description (根据任务号查询本次授信申请安排方案情况)
	 * @param taskId  任务号
	 * @return 
	 * @
	 */
	public String findApplySchemeListByTaskId(Long taskId) ;
	

	/**
	 * @Description (保存本次授信申请安排方案情况)
	 * @param taskId 任务号
	 * @param jsonArray  
	 */
	public String saveApplySchemeList(Long taskId,String jsonArray) ;
	
	
	/**
	 * @Description (根据任务号和客户号查询本次授信申请利费率情况)
	 * @param taskId  任务号
	 * @param taskId  客户号
	 * @return 
	 * @
	 */
	public String findApplyRateListByTaskIdAndNO(Long taskId,String customerNo) ;
	

	/**
	 * @Description (保存本次授信申请利费率情况)
	 * @param taskId 任务号
	 * @param jsonArray  
	 */
	public String saveApplyRateList(Long taskId,String jsonArray) ;
	
	
	/**
	 * @Description (获取还款能力测算信息页面)
	 * @param taskId 任务号
	 * @param jsonArray  
	 */
	public String getRePaymentAnlyHtml(Long taskId) ;
	
	
	/**
	 * @Description (获取利费率定价测算信息页面)
	 * @param taskId 任务号
	 * @param jsonArray  
	 */
	public String getRateAnlyHtml(Long taskId) ;
	
	
	/**
	 * @Description (查询上期授信情况)
	 * @param taskId 任务号      
	 * @return 
	 * @
	 */
	public String getLastCreditInfo(Long taskId) ;

}
