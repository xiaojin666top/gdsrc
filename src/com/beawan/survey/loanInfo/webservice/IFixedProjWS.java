package com.beawan.survey.loanInfo.webservice;

import java.util.List;

import com.beawan.survey.loanInfo.bean.FixedProjBase;
import com.beawan.survey.loanInfo.bean.FixedProjCashFow;
import com.beawan.survey.loanInfo.bean.FixedProjFinaPlan;
import com.beawan.survey.loanInfo.bean.FixedProjInvest;
import com.beawan.survey.loanInfo.bean.FixedProjInvestEst;
import com.beawan.survey.loanInfo.bean.FixedProjPlan;

/**
 * @Description 固定资产贷款项目信息webservice服务接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IFixedProjWS {
	
	/**
	 * 保存项目基本信息
	 * @param fixedProjBase
	 * @throws Exception
	 */
	public String saveOrUpdateFixedProjBase(String fixedProjBase);
	
	/**
	 * 根据任务号查询项目基本信息
	 * @param taskId 任务号
	 * @return
	 * @throws Exception
	 */
	public String  queryFixedProjBaseByTaskIdAndCusNo(Long taskId);
	
	
	/**
	 * 保存固定资产项目建设计划列表
	 * @param fixedProjBase
	 * @throws Exception
	 */
	public String saveFixedProjPlanList(Long taskId,String  jsonArray);
	
	/**
	 * 根据任务号查询固定资产项目建设计划列表
	 * @param taskId 任务号
	 * @return
	 * @throws Exception
	 */
	public String  queryFixedProjPlanListByTaskId(Long taskId);
	
	
	
	/**
	 * 保存固定资产项目投资情况列表
	 * @param fixedProjBase
	 * @throws Exception
	 */
	public String saveFixedProjInvestList(Long taskId,String  jsonArray);
	
	/**
	 * 根据任务号查询固定资产项目投资情况列表
	 * @param taskId 任务号
	 * @return
	 * @throws Exception
	 */
	public String  queryFixedProjInvestListByTaskId(Long taskId);
	
	
	
	/**
	 * 保存固定资产项目建设投资估算列表
	 * @param fixedProjBase
	 * @throws Exception
	 */
	public String saveFixedProjInvestEstList(Long taskId,String  jsonArray);
	
	/**
	 * 根据任务号查询固定资产项目建设投资估算列表
	 * @param taskId 任务号
	 * @return
	 * @throws Exception
	 */
	public String  queryFixedProjInvestEstListByTaskId(Long taskId);
	
	
	/**
	 * 保存固定资产项目建设筹资计划列表
	 * @param fixedProjBase
	 * @throws Exception
	 */
	public String saveFixedProjFinaPlanList(Long taskId,String  jsonArray);
	
	/**
	 * 根据任务号查询固定资产项目建设筹资计划列表
	 * @param taskId 任务号
	 * @return
	 * @throws Exception
	 */
	public String  queryFixedProjFinaPlanListByTaskId(Long taskId);
	
	
	/**
	 * 保存固定资产项目建设现金流测算列表
	 * @param fixedProjBase
	 * @throws Exception
	 */
	public String saveFixedProjCashFowList(Long taskId,String  jsonArray);
	
	/**
	 * 根据任务号查询固定资产项目建设现金流测算列表
	 * @param taskId 任务号
	 * @return
	 * @throws Exception
	 */
	public String  queryFixedProjCashFowListByTaskId(Long taskId);


}
