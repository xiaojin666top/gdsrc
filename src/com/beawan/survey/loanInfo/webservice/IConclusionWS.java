package com.beawan.survey.loanInfo.webservice;

/**
 * @Description 调查结论webservice服务接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IConclusionWS {
	
	/**
	 * @Description (根据任务号查询信贷风险分析)
	 * @param taskId 任务号      
	 * @return 
	 * @
	 */
	public String findRiskAnalysisByTaskId(Long taskId) ;
	

	/**
	 * @Description (保存信贷风险分析)
	 * @param RiskAnalysis 信贷风险分析实体
	 * @
	 */
	public String saveRiskAnalysis(String riskAnalysisInfo) ;
	
	
	/**
	 * @Description (根据任务号查询本次授信综合结论)
	 * @param taskId 任务号      
	 * @return 
	 * @
	 */
	public String updateAndfindConclusionByTaskId(Long taskId) ;
	

	/**
	 * @Description (保存信贷风险分析)
	 * @param Conclusion 综合结论实体
	 * @
	 */
	public String saveConclusion(String conclusionInfo) ;
	
}
