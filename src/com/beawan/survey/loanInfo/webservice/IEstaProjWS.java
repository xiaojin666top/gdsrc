package com.beawan.survey.loanInfo.webservice;

import com.beawan.survey.loanInfo.bean.EstaProjBase;

/**
 * @Description 房地产开发贷款项目信息webservice服务接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IEstaProjWS {
	
	/**
	 * @Description (根据任务号查询房地产项目情况)
	 * @param taskId 任务号
	 * @return 
	 * @throws Exception
	 */
	public String findEstaProjBaseByTaskId(Long taskId);
	

	/**
	 * @Description (保存房地产项目情况)
	 * @param EstaProjBase 房地产项目情况实体
	 * @throws Exception
	 */
	public String saveEstaProjBase(String estaProjBase);
}
