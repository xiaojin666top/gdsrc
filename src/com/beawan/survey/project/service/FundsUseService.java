package com.beawan.survey.project.service;

import com.beawan.core.BaseService;
import com.beawan.survey.project.entity.FundsUse;

/**
 * @author yzj
 */
public interface FundsUseService extends BaseService<FundsUse> {
}
