package com.beawan.survey.project.service;

import com.beawan.core.BaseService;
import com.beawan.survey.project.entity.ProjectCost;

/**
 * @author yzj
 */
public interface ProjectCostService extends BaseService<ProjectCost> {
}
