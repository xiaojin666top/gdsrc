package com.beawan.survey.project.service;

import com.beawan.core.BaseService;
import com.beawan.survey.project.entity.BusinessManage;

/**
 * @author yzj
 */
public interface BusinessManageService extends BaseService<BusinessManage> {
}
