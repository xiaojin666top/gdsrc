package com.beawan.survey.project.service;

import com.beawan.core.BaseService;
import com.beawan.survey.project.dto.SubProjectDto;
import com.beawan.survey.project.entity.InvestScale;

import java.util.List;

/**
 * @author yzj
 */
public interface InvestScaleService extends BaseService<InvestScale> {

    public List<SubProjectDto> getSubProjectList(String serNo) throws Exception;
}
