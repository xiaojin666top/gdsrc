package com.beawan.survey.project.service;

import com.beawan.core.BaseService;
import com.beawan.survey.project.entity.FundsInput;

/**
 * @author yzj
 */
public interface FundsInputService extends BaseService<FundsInput> {
}
