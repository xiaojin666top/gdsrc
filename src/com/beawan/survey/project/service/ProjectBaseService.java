package com.beawan.survey.project.service;

import com.beawan.core.BaseService;
import com.beawan.survey.project.entity.ProjectBase;

/**
 * @author yzj
 */
public interface ProjectBaseService extends BaseService<ProjectBase> {
}
