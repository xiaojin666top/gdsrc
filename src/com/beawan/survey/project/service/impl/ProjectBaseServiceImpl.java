package com.beawan.survey.project.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.project.dao.ProjectBaseDao;
import com.beawan.survey.project.entity.ProjectBase;
import com.beawan.survey.project.service.ProjectBaseService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.project.entity.ProjectBase;

/**
 * @author yzj
 */
@Service("projectBaseService")
public class ProjectBaseServiceImpl extends BaseServiceImpl<ProjectBase> implements ProjectBaseService{
    /**
    * 注入DAO
    */
    @Resource(name = "projectBaseDao")
    public void setDao(BaseDao<ProjectBase> dao) {
        super.setDao(dao);
    }
    @Resource
    public ProjectBaseDao projectBaseDao;
}
