package com.beawan.survey.project.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.project.dao.BusinessManageDao;
import com.beawan.survey.project.entity.BusinessManage;
import com.beawan.survey.project.service.BusinessManageService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;

/**
 * @author yzj
 */
@Service("businessManageService")
public class BusinessManageServiceImpl extends BaseServiceImpl<BusinessManage> implements BusinessManageService {
    /**
    * 注入DAO
    */
    @Resource(name = "businessManageDao")
    public void setDao(BaseDao<BusinessManage> dao) {
        super.setDao(dao);
    }
    @Resource
    public BusinessManageDao businessManageDao;
}
