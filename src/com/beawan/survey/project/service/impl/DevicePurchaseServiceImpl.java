package com.beawan.survey.project.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.project.dao.DevicePurchaseDao;
import com.beawan.survey.project.entity.DevicePurchase;
import com.beawan.survey.project.service.DevicePurchaseService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.project.entity.DevicePurchase;

/**
 * @author yzj
 */
@Service("devicePurchaseService")
public class DevicePurchaseServiceImpl extends BaseServiceImpl<DevicePurchase> implements DevicePurchaseService{
    /**
    * 注入DAO
    */
    @Resource(name = "devicePurchaseDao")
    public void setDao(BaseDao<DevicePurchase> dao) {
        super.setDao(dao);
    }
    @Resource
    public DevicePurchaseDao devicePurchaseDao;
}
