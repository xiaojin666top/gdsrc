package com.beawan.survey.project.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.project.dao.ProjectCostDao;
import com.beawan.survey.project.entity.ProjectCost;
import com.beawan.survey.project.service.ProjectCostService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.project.entity.ProjectCost;

/**
 * @author yzj
 */
@Service("projectCostService")
public class ProjectCostServiceImpl extends BaseServiceImpl<ProjectCost> implements ProjectCostService{
    /**
    * 注入DAO
    */
    @Resource(name = "projectCostDao")
    public void setDao(BaseDao<ProjectCost> dao) {
        super.setDao(dao);
    }
    @Resource
    public ProjectCostDao projectCostDao;
}
