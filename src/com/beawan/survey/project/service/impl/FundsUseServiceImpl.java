package com.beawan.survey.project.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.project.dao.FundsUseDao;
import com.beawan.survey.project.entity.FundsUse;
import com.beawan.survey.project.service.FundsUseService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.project.entity.FundsUse;

/**
 * @author yzj
 */
@Service("fundsUseService")
public class FundsUseServiceImpl extends BaseServiceImpl<FundsUse> implements FundsUseService{
    /**
    * 注入DAO
    */
    @Resource(name = "fundsUseDao")
    public void setDao(BaseDao<FundsUse> dao) {
        super.setDao(dao);
    }
    @Resource
    public FundsUseDao fundsUseDao;
}
