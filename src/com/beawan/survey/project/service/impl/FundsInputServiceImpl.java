package com.beawan.survey.project.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.project.dao.FundsInputDao;
import com.beawan.survey.project.entity.FundsInput;
import com.beawan.survey.project.service.FundsInputService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.project.entity.FundsInput;

/**
 * @author yzj
 */
@Service("fundsInputService")
public class FundsInputServiceImpl extends BaseServiceImpl<FundsInput> implements FundsInputService{
    /**
    * 注入DAO
    */
    @Resource(name = "fundsInputDao")
    public void setDao(BaseDao<FundsInput> dao) {
        super.setDao(dao);
    }
    @Resource
    public FundsInputDao fundsInputDao;
}
