package com.beawan.survey.project.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.project.dao.InvestScaleDao;
import com.beawan.survey.project.dto.SubProjectDto;
import com.beawan.survey.project.entity.InvestScale;
import com.beawan.survey.project.service.InvestScaleService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import com.beawan.survey.project.entity.InvestScale;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author yzj
 */
@Service("investScaleService")
public class InvestScaleServiceImpl extends BaseServiceImpl<InvestScale> implements InvestScaleService{
    /**
    * 注入DAO
    */
    @Resource(name = "investScaleDao")
    public void setDao(BaseDao<InvestScale> dao) {
        super.setDao(dao);
    }
    @Resource
    public InvestScaleDao investScaleDao;

    @Override
    public List<SubProjectDto> getSubProjectList(String serNo) throws Exception{
        InvestScale investScale = investScaleDao.selectSingleByProperty("serNo", serNo);
        if (investScale == null) return null;

        String subProject = investScale.getSubProjectInfo();
        if(StringUtil.isEmptyString(subProject)) return null;
        
        List<SubProjectDto> list = 
        		JacksonUtil.fromJson(subProject, new TypeReference<List<SubProjectDto>>() {});
        if (CollectionUtils.isEmpty(list)) return null;

        return list;
    }
}
