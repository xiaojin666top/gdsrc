package com.beawan.survey.project.service;

import com.beawan.core.BaseService;
import com.beawan.survey.project.entity.DevicePurchase;

/**
 * @author yzj
 */
public interface DevicePurchaseService extends BaseService<DevicePurchase> {
}
