package com.beawan.survey.project.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.project.entity.ProjectBase;

/**
 * @author yzj
 */
public interface ProjectBaseDao extends BaseDao<ProjectBase> {
}
