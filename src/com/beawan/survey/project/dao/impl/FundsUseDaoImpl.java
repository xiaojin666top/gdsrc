package com.beawan.survey.project.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.project.dao.FundsUseDao;
import com.beawan.survey.project.entity.FundsUse;

/**
 * @author yzj
 */
@Repository("fundsUseDao")
public class FundsUseDaoImpl extends BaseDaoImpl<FundsUse> implements FundsUseDao{

}
