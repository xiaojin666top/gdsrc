package com.beawan.survey.project.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.project.dao.FundsInputDao;
import com.beawan.survey.project.entity.FundsInput;

/**
 * @author yzj
 */
@Repository("fundsInputDao")
public class FundsInputDaoImpl extends BaseDaoImpl<FundsInput> implements FundsInputDao{

}
