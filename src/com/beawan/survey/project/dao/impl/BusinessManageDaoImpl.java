package com.beawan.survey.project.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.project.dao.BusinessManageDao;
import com.beawan.survey.project.entity.BusinessManage;
import org.springframework.stereotype.Repository;

/**
 * @author yzj
 */
@Repository("businessManageDao")
public class BusinessManageDaoImpl extends BaseDaoImpl<BusinessManage> implements BusinessManageDao {

}
