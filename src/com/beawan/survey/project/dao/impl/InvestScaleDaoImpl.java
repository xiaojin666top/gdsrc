package com.beawan.survey.project.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.project.dao.InvestScaleDao;
import com.beawan.survey.project.entity.InvestScale;

/**
 * @author yzj
 */
@Repository("investScaleDao")
public class InvestScaleDaoImpl extends BaseDaoImpl<InvestScale> implements InvestScaleDao{

}
