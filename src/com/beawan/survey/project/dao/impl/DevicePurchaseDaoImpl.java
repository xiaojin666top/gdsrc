package com.beawan.survey.project.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.project.dao.DevicePurchaseDao;
import com.beawan.survey.project.entity.DevicePurchase;

/**
 * @author yzj
 */
@Repository("devicePurchaseDao")
public class DevicePurchaseDaoImpl extends BaseDaoImpl<DevicePurchase> implements DevicePurchaseDao{

}
