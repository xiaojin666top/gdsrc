package com.beawan.survey.project.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.project.dao.ProjectCostDao;
import com.beawan.survey.project.entity.ProjectCost;

/**
 * @author yzj
 */
@Repository("projectCostDao")
public class ProjectCostDaoImpl extends BaseDaoImpl<ProjectCost> implements ProjectCostDao{

}
