package com.beawan.survey.project.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.project.dao.ProjectBaseDao;
import com.beawan.survey.project.entity.ProjectBase;

/**
 * @author yzj
 */
@Repository("projectBaseDao")
public class ProjectBaseDaoImpl extends BaseDaoImpl<ProjectBase> implements ProjectBaseDao{

}
