package com.beawan.survey.project.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.project.entity.InvestScale;

/**
 * @author yzj
 */
public interface InvestScaleDao extends BaseDao<InvestScale> {
}
