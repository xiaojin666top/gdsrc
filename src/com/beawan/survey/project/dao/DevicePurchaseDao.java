package com.beawan.survey.project.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.project.entity.DevicePurchase;

/**
 * @author yzj
 */
public interface DevicePurchaseDao extends BaseDao<DevicePurchase> {
}
