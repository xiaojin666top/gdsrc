package com.beawan.survey.project.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.project.entity.FundsUse;

/**
 * @author yzj
 */
public interface FundsUseDao extends BaseDao<FundsUse> {
}
