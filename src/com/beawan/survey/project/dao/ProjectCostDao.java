package com.beawan.survey.project.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.project.entity.ProjectCost;

/**
 * @author yzj
 */
public interface ProjectCostDao extends BaseDao<ProjectCost> {
}
