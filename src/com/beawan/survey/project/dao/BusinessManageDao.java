package com.beawan.survey.project.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.project.entity.BusinessManage;

/**
 * @author yzj
 */
public interface BusinessManageDao extends BaseDao<BusinessManage> {
}
