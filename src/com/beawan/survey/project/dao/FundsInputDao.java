package com.beawan.survey.project.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.project.entity.FundsInput;

/**
 * @author yzj
 */
public interface FundsInputDao extends BaseDao<FundsInput> {
}
