package com.beawan.survey.project.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 项目资金投入情况（项目类）
 * @author zxh
 * @date 2020/7/30 19:27
 */
@Entity
@Table(name = "CM_FUNDS_INPUT",schema = "GDTCESYS")
public class FundsInput extends BaseEntity {

    @Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="CM_FUNDS_INPUT")
    //@SequenceGenerator(name="CM_FUNDS_INPUT",allocationSize=1,initialValue=1, sequenceName="CM_FUNDS_INPUT_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;
    @Column(name = "CATEGORY")
    private String category; // 项目类别
    @Column(name = "NAME")
    private String name; // 名称
    @Column(name = "AREA_MODEL")
    private String areaModel; // 面积/型号
    @Column(name = "FUNDS")
    private double funds;// 投入金额
    @Column(name = "CAP_TIME")
    private String capTime; // 封顶时间
    @Column(name = "REMARK")
    private String remark; // 备注

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAreaModel() {
        return areaModel;
    }

    public void setAreaModel(String areaModel) {
        this.areaModel = areaModel;
    }

    public double getFunds() {
        return funds;
    }

    public void setFunds(double funds) {
        this.funds = funds;
    }

    public String getCapTime() {
        return capTime;
    }

    public void setCapTime(String capTime) {
        this.capTime = capTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
