package com.beawan.survey.project.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 设备购置情况(项目类)
 * @author zxh
 * @date 2020/7/31 14:37
 */
@Entity
@Table(name = "CM_DEVICE_PURCHASE",schema = "GDTCESYS")
public class DevicePurchase  extends BaseEntity {

    @Id
  //  @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="CM_DEVICE_PURCHASE")
  //  @SequenceGenerator(name="CM_DEVICE_PURCHASE",allocationSize=1,initialValue=1, sequenceName="CM_DEVICE_PURCHASE_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;
    @Column(name = "EQUIP_NAME")
    private String equipmentName;// 设备名称
    @Column(name = "MODEL")
    private String model;// 规格型号
    @Column(name = "NUMBER")
    private int number;// 数量
    @Column(name = "PRICE")
    private double price;// 单价
    @Column(name = "PRICE_SUM")
    private double priceSum;// 总价

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getEquipmentName() {
        return equipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPriceSum() {
        return priceSum;
    }

    public void setPriceSum(double priceSum) {
        this.priceSum = priceSum;
    }
}
