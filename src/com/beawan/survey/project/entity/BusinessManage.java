package com.beawan.survey.project.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 企业经营管理情况（项目类）
 * @author zxh
 * @date 2020/8/1 11:44
 */
@Entity
@Table(name = "CM_BUSINESS_MANAGE",schema = "GDTCESYS")
public class BusinessManage extends BaseEntity {

    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="CM_BUSINESS_MANAGE")
    //@SequenceGenerator(name="CM_BUSINESS_MANAGE",allocationSize=1,initialValue=1, sequenceName="CM_BUSINESS_MANAGE_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;
    @Column(name = "HISTORY")
    private String history; // 申请人历史沿革
    @Column(name = "RUN_INFO")
    private String runInfo; // 经营情况分析
    @Column(name = "MANAGE_INFO")
    private String manageInfo; // 管理情况分析
    @Column(name = "ENVIRONMENT")
    private String environment; // 环保达标情况
    @Column(name = "OVERALL_INFO")
    private String overallInfo; // 总体评价

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public String getRunInfo() {
        return runInfo;
    }

    public void setRunInfo(String runInfo) {
        this.runInfo = runInfo;
    }

    public String getManageInfo() {
        return manageInfo;
    }

    public void setManageInfo(String manageInfo) {
        this.manageInfo = manageInfo;
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public String getOverallInfo() {
        return overallInfo;
    }

    public void setOverallInfo(String overallInfo) {
        this.overallInfo = overallInfo;
    }
}
