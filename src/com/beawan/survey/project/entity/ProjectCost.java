package com.beawan.survey.project.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 工程费用明细(项目类)
 * @author zxh
 * @date 2020/7/31 14:49
 */
@Entity
@Table(name = "CM_PROJECT_COST",schema = "GDTCESYS")
public class ProjectCost  extends BaseEntity {

    @Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_PROJECT_COST")
   // @SequenceGenerator(name = "CM_PROJECT_COST", allocationSize = 1, initialValue = 1, sequenceName = "CM_PROJECT_COST_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;
    @Column(name = "ITEM_NAME")
    private String itemName; // 项目
    @Column(name = "COST_INFO")
    private String costInfo; // 费用明细
    @Column(name = "AMOUNT")
    private double amount; //金额
    @Column(name = "REMARK")
    private String remark; //备注

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getCostInfo() {
        return costInfo;
    }

    public void setCostInfo(String costInfo) {
        this.costInfo = costInfo;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}