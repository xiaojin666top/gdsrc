package com.beawan.survey.project.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 项目投资规模
 * @author zxh
 * @date 2020/7/31 10:39
 */
@Entity
@Table(name = "CM_INVEST_SCALE",schema = "GDTCESYS")
public class InvestScale extends BaseEntity {

    @Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="CM_INVEST_SCALE")
   // @SequenceGenerator(name="CM_INVEST_SCALE",allocationSize=1,initialValue=1, sequenceName="CM_INVEST_SCALE_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;
    @Column(name = "PLAN_AMT")
    private double planAmt; // 计划投资额
    @Column(name = "ACTUAL_AMT")
    private double actualAmt; // 实际投资额
    @Column(name = "FUNDS_SOURCE")
    private String fundsSource; // 资金来源情况
    @Column(name = "SUB_PRO_INFO")
    private String subProjectInfo; // 子项目(名称、投资金额、备注)
    @Column(name = "FUNDS_USE")
    private String fundsUse; // 资金使用情况
    @Column(name = "PROGRESS_INFO")
    private String progressInfo; // 项目进展情况
    @Column(name = "INVERSTMENT_INFO")
    private String investmentInfo;//投资规模说明
    
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public double getPlanAmt() {
        return planAmt;
    }

    public void setPlanAmt(double planAmt) {
        this.planAmt = planAmt;
    }

    public double getActualAmt() {
        return actualAmt;
    }

    public void setActualAmt(double actualAmt) {
        this.actualAmt = actualAmt;
    }

    public String getFundsSource() {
        return fundsSource;
    }

    public void setFundsSource(String fundsSource) {
        this.fundsSource = fundsSource;
    }

    public String getFundsUse() {
        return fundsUse;
    }

    public void setFundsUse(String fundsUse) {
        this.fundsUse = fundsUse;
    }

    public String getProgressInfo() {
        return progressInfo;
    }

    public void setProgressInfo(String progressInfo) {
        this.progressInfo = progressInfo;
    }

    public String getSubProjectInfo() {
        return subProjectInfo;
    }

    public void setSubProjectInfo(String subProjectInfo) {
        this.subProjectInfo = subProjectInfo;
    }
	public String getInvestmentInfo() {
		return investmentInfo;
	}

	public void setInvestmentInfo(String investmentInfo) {
		this.investmentInfo = investmentInfo;
	}
}
