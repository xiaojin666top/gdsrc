package com.beawan.survey.project.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 项目基本信息（项目类模板）
 * @author zxh
 * @date 2020/7/27 16:42
 */
@Entity
@Table(name = "CM_PROJECT_BASE",schema = "GDTCESYS")
public class ProjectBase extends BaseEntity {

    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="CM_PROJECT_BASE")
//@SequenceGenerator(name="CM_PROJECT_BASE",allocationSize=1,initialValue=1, sequenceName="CM_PROJECT_BASE_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;
    @Column(name = "NAME")
    private String name; // 项目名称
    @Column(name = "FUND_SUM")
    private double fundSum; // 总投资
    @Column(name = "IMPLEMENT_UNIT")
    private String implementUnit; // 项目实施单位
    @Column(name = "ADDRESS")
    private String address; // 建设地点
    @Column(name = "SCALE_CONTENT")
    private String scaleContent;// 建设规模及内容
    @Column(name = "APPROVAL")
    private String approval;// 项目批文
    @Column(name = "PROSPECT")
    private String prospect;// 项目前景

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getFundSum() {
        return fundSum;
    }

    public void setFundSum(double fundSum) {
        this.fundSum = fundSum;
    }

    public String getImplementUnit() {
        return implementUnit;
    }

    public void setImplementUnit(String implementUnit) {
        this.implementUnit = implementUnit;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getScaleContent() {
        return scaleContent;
    }

    public void setScaleContent(String scaleContent) {
        this.scaleContent = scaleContent;
    }

    public String getApproval() {
        return approval;
    }

    public void setApproval(String approval) {
        this.approval = approval;
    }

    public String getProspect() {
        return prospect;
    }

    public void setProspect(String prospect) {
        this.prospect = prospect;
    }
}
