package com.beawan.survey.project.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 项目资金使用情况（项目类）
 * @author zxh
 * @date 2020/7/30 19:36
 */
@Entity
@Table(name = "CM_FUNDS_USE",schema = "GDTCESYS")
public class FundsUse extends BaseEntity {

    @Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="CM_FUNDS_USE")
   // @SequenceGenerator(name="CM_FUNDS_USE",allocationSize=1,initialValue=1, sequenceName="CM_FUNDS_USE_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;
    @Column(name = "USE_TO")
    private String useTo; // 用途
    @Column(name = "FUNDS")
    private double funds;// 金额
    @Column(name = "PAY_TIME")
    private String payTime; // 支付时间
    @Column(name = "REMARK")
    private String remark; // 备注

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getUseTo() {
        return useTo;
    }

    public void setUseTo(String useTo) {
        this.useTo = useTo;
    }

    public double getFunds() {
        return funds;
    }

    public void setFunds(double funds) {
        this.funds = funds;
    }

    public String getPayTime() {
        return payTime;
    }

    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
