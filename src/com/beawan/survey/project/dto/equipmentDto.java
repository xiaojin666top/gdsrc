package com.beawan.survey.project.dto;

/**
 * 设备购置情况(项目类)DTO
 * @author zxh
 * @date 2020/7/31 16:44
 */
public class equipmentDto {

    private Integer id;
    private String serNo;
    private String equipmentName;// 设备名称
    private String model;// 规格型号
    private int number;// 数量
    private double price;// 单价
    private double priceSum;// 总价

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getEquipmentName() {
        return equipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPriceSum() {
        return priceSum;
    }

    public void setPriceSum(double priceSum) {
        this.priceSum = priceSum;
    }
}
