package com.beawan.survey.project.dto;

/**
 * 投资子项目信息DTO
 * @author zxh
 * @date 2020/7/31 17:54
 */
public class SubProjectDto {

	private String serNo;
    private String subProject; // 子项目
    private double investSum; // 投资金额
    private String remark; // 备注

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }
    
    public String getSubProject() {
        return subProject;
    }

    public void setSubProject(String subProject) {
        this.subProject = subProject;
    }

    public double getInvestSum() {
        return investSum;
    }

    public void setInvestSum(double investSum) {
        this.investSum = investSum;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
