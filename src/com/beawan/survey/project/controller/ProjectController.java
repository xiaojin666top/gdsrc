package com.beawan.survey.project.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.base.entity.User;
import com.beawan.common.Constants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.survey.project.dto.ProjectCostDto;
import com.beawan.survey.project.dto.SubProjectDto;
import com.beawan.survey.project.dto.equipmentDto;
import com.beawan.survey.project.entity.BusinessManage;
import com.beawan.survey.project.entity.DevicePurchase;
import com.beawan.survey.project.entity.FundsInput;
import com.beawan.survey.project.entity.FundsUse;
import com.beawan.survey.project.entity.InvestScale;
import com.beawan.survey.project.entity.ProjectBase;
import com.beawan.survey.project.entity.ProjectCost;
import com.beawan.survey.project.service.BusinessManageService;
import com.beawan.survey.project.service.DevicePurchaseService;
import com.beawan.survey.project.service.FundsInputService;
import com.beawan.survey.project.service.FundsUseService;
import com.beawan.survey.project.service.InvestScaleService;
import com.beawan.survey.project.service.ProjectBaseService;
import com.beawan.survey.project.service.ProjectCostService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.platform.util.DateUtil;
import com.platform.util.GsonUtil;
import com.platform.util.HttpUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.MapperUtil;
import com.platform.util.StringUtil;

/**
 * 投资项目 控制器(项目类)
 * @author zxh
 * @date 2020/7/27 16:44
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/survey/project/" })
public class ProjectController extends BaseController {

    private static final Logger log = Logger.getLogger(ProjectController.class);

    @Resource
    private ProjectBaseService projectBaseService;
    @Resource
    private FundsUseService fundsUseService;
    @Resource
    private FundsInputService fundsInputService;
    @Resource
    private DevicePurchaseService devicePurchaseService;
    @Resource
    private ProjectCostService projectCostService;
    @Resource
    private InvestScaleService investScaleService;
    @Resource
    private BusinessManageService businessManageService;


    /** 跳转到 项目基本情况 */
    @RequestMapping("projectInfo.do")
    public ModelAndView projectInfo(String serNo){
        ModelAndView mav =  new ModelAndView("views/survey/investProject/projectInfo");
        ProjectBase projectInfo = projectBaseService.selectSingleByProperty("serNo", serNo);
        mav.addObject("serNo", serNo);
        mav.addObject("project", projectInfo);
        return mav;
    }

    /**
     * 保存 项目基本情况
     * @param request
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("saveProjectInfo.do")
    @ResponseBody
    public ResultDto saveProjectInfo(HttpServletRequest request, String serNo){
        ResultDto re = returnFail("保存失败！");
        String projectInfo = request.getParameter("projectInfo");
        try {
            ProjectBase projectBase = GsonUtil.GsonToBean(projectInfo, ProjectBase.class);
            if (projectBase == null){
                re.setMsg("传输数据为空，请重试!");
                return re;
            }

            User user = HttpUtil.getCurrentUser(request);
            ProjectBase oldInfo = projectBaseService.selectSingleByProperty("serNo", serNo);
            if (oldInfo != null)
                projectBase.setId(oldInfo.getId());
            if (projectBase.getId() == null){
                projectBase.setCreater(user.getUserId());
                projectBase.setCreateTime(DateUtil.getNowTimestamp());
            }
            projectBase.setStatus(Constants.NORMAL);
            projectBase.setUpdater(user.getUserId());
            projectBase.setUpdateTime(DateUtil.getNowTimestamp());
            projectBaseService.saveOrUpdate(projectBase);

            re.setCode(Constants.NORMAL);
            re.setMsg("保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 跳转到 资金使用情况
     * @param serNo
     * @return
     */
    @RequestMapping("fundsUseInfo.do")
    public ModelAndView fundsUseInfo(String serNo){
        ModelAndView mav =  new ModelAndView("views/survey/investProject/toFundsInfo");
        mav.addObject("serNo", serNo);
        return mav;
    }

    /**
     * 跳转到 资金使用情况 相应界面
     * @param serNo
     * @param type
     * @return
     */
    @RequestMapping("toFundsInfo.do")
    public ModelAndView toFundsInfo(String serNo, String type){
        ModelAndView mav =  new ModelAndView();
        try {        
            InvestScale investScale = investScaleService.selectSingleByProperty("serNo", serNo);
            if (investScale != null) {
                mav.addObject("fundsSource", investScale.getFundsSource());
                mav.addObject("fundsUse", investScale.getFundsUse());
                mav.addObject("investmentInfo", investScale.getInvestmentInfo());
            }
            mav.addObject("serNo", serNo);
            if (Constants.LB_PAGE_TYPE.FUNDS_INVEST.equals(type)){
                mav.setViewName("views/survey/investProject/fundsInvest");
            }else if (Constants.LB_PAGE_TYPE.FUNDS_USE.equals(type)){
                mav.setViewName("views/survey/investProject/fundsUse");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }

    /**
     * 获取 资金投入 信息列表
     * @param request
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("getFundsInputList.json")
    @ResponseBody
    public ResultDto getFundsInputList(HttpServletRequest request, String serNo){
        ResultDto re = returnFail("获取数据失败！");
        if(StringUtil.isEmptyString(serNo)){
            re.setMsg("任务流水号为空");
            return re;
        }
        try {
            List<FundsInput> list = fundsInputService.selectByProperty("serNo", serNo);
            re = returnSuccess();
            re.setRows(list);
            request.setAttribute("serNo", serNo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 获取 资金使用详情 列表
     * @param request
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("getFundsUseList.json")
    @ResponseBody
    public ResultDto getFundsUseList(HttpServletRequest request, String serNo){
        ResultDto re = returnFail("获取数据失败！");
        if(StringUtil.isEmptyString(serNo)){
            re.setMsg("任务流水号为空");
            return re;
        }
        try {
            List<FundsUse> list = fundsUseService.selectByProperty("serNo", serNo);
            re = returnSuccess();
            re.setRows(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 保存/修改 资金投入 信息
     * @param request
     * @return
     */
    @RequestMapping("saveFundsInput.json")
    @ResponseBody
    public ResultDto saveFundsInput(HttpServletRequest request) {
        ResultDto re = returnFail("保存失败");
        String jsonData = request.getParameter("jsonArray");
        try {
            List<FundsInput> entityList =
                    JacksonUtil.fromJson(jsonData, new TypeReference<List<FundsInput>>() {});
            if (CollectionUtils.isEmpty(entityList))
                return returnFail("请先填写相关信息！");

            User user = HttpUtil.getCurrentUser(request);
            Timestamp nowTimestamp = DateUtil.getNowTimestamp();
            for (FundsInput entity : entityList) {
                if (entity.getId() == null){
                    entity.setCreater(user.getUserId());
                    entity.setCreateTime(nowTimestamp);
                }
                entity.setStatus(Constants.NORMAL);
                entity.setUpdater(user.getUserId());
                entity.setUpdateTime(nowTimestamp);
                fundsInputService.saveOrUpdate(entity);
            }

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 保存/修改 资金使用明细
     * @param jsonArray
     * @return
     */
    @RequestMapping("saveFundsUse.json")
    @ResponseBody
    public ResultDto saveFundsUse(HttpServletRequest request, String jsonArray) {
        ResultDto re = returnFail("保存失败");
        try {
            List<FundsUse> entityList =
                    JacksonUtil.fromJson(jsonArray, new TypeReference<List<FundsUse>>() {});
            if (CollectionUtils.isEmpty(entityList))
                return returnFail("请先填写相关信息！");

            User user = HttpUtil.getCurrentUser(request);
            Timestamp nowTimestamp = DateUtil.getNowTimestamp();
            for (FundsUse entity : entityList) {
                if (entity.getId() == null){
                    entity.setCreater(user.getUserId());
                    entity.setCreateTime(nowTimestamp);
                }
                entity.setStatus(Constants.NORMAL);
                entity.setUpdater(user.getUserId());
                entity.setUpdateTime(nowTimestamp);
                fundsUseService.saveOrUpdate(entity);
            }

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 删除 资金使用明细
     * @param request
     * @param id
     * @return
     */
    @RequestMapping("deleteFundsUse.json")
    @ResponseBody
    public ResultDto deleteFundsUse(HttpServletRequest request, Integer id){
        ResultDto re = returnFail("删除数据异常！");
        try{
            User user = HttpUtil.getCurrentUser(request);
            FundsUse fundsUse = fundsUseService.selectByPrimaryKey(id);
            fundsUse.setStatus(Constants.DELETE);
            fundsUse.setUpdater(user.getUserId());
            fundsUse.setUpdateTime(DateUtil.getNowTimestamp());
            fundsUseService.saveOrUpdate(fundsUse);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("删除数据成功！");
        }catch (Exception e){
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 删除 资金投入信息
     * @param request
     * @param id
     * @return
     */
    @RequestMapping("deleteFundsInput.json")
    @ResponseBody
    public ResultDto deleteFundsInput(HttpServletRequest request, Integer id){
        ResultDto re = returnFail("删除数据异常！");
        try{
            User user = HttpUtil.getCurrentUser(request);
            FundsInput fundsInput = fundsInputService.selectByPrimaryKey(id);
            fundsInput.setStatus(Constants.DELETE);
            fundsInput.setUpdater(user.getUserId());
            fundsInput.setUpdateTime(DateUtil.getNowTimestamp());
            fundsInputService.saveOrUpdate(fundsInput);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("删除数据成功！");
        }catch (Exception e){
            e.printStackTrace();
        }
        return re;
    }

    /** 跳转到 项目投资规模 */
    @RequestMapping("proInvestScale.do")
    public ModelAndView proInvestScale(String serNo){
        ModelAndView mav =  new ModelAndView("views/survey/investProject/investScale");
        InvestScale investScale = investScaleService.selectSingleByProperty("serNo", serNo);
        mav.addObject("serNo", serNo);
        mav.addObject("investScale", investScale);
        return mav;
    }

    /**
     * 保存/修改 投资规模信息
     * @param request
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("saveInvestInfo.json")
    @ResponseBody
    public ResultDto saveInvestInfo(HttpServletRequest request, String serNo){
        ResultDto re = returnFail();
        String investInfo = request.getParameter("investInfo");
        User user = HttpUtil.getCurrentUser(request);
        try {
            InvestScale investScale = JacksonUtil.fromJson(investInfo, InvestScale.class);
            if (investScale == null){
                return returnFail("数据传输异常，请重试！");
            }

            InvestScale oldInfo = investScaleService.selectSingleByProperty("serNo", serNo);
            if (oldInfo != null){
            	oldInfo.setPlanAmt(investScale.getPlanAmt());
            	oldInfo.setActualAmt(investScale.getActualAmt());
            	oldInfo.setSubProjectInfo(investScale.getSubProjectInfo());
            	oldInfo.setInvestmentInfo(investScale.getInvestmentInfo());
            	
            	oldInfo.setStatus(Constants.NORMAL);
            	oldInfo.setUpdater(user.getUserId());
            	oldInfo.setUpdateTime(DateUtil.getNowTimestamp());
            	investScaleService.saveOrUpdate(oldInfo);
            }else{
            	investScale.setCreater(user.getUserId());
            	investScale.setCreateTime(DateUtil.getNowTimestamp());
            	investScale.setUpdater(user.getUserId());
            	investScale.setUpdateTime(DateUtil.getNowTimestamp());
            	investScaleService.saveOrUpdate(investScale);
            }
          
           
            re.setMsg("保存成功！");
            re.setCode(Constants.NORMAL);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 删除 子项目信息
     * @param request
     * @param serNo 任务流水号
     * @param subProject 子项目信息
     * @return
     */
    @RequestMapping("removeInvestInfo.json")
    @ResponseBody
    public ResultDto removeInvestInfo(HttpServletRequest request,String serNo, String subProject){
        ResultDto re = returnFail("删除失败！");
        try {
            InvestScale entity = investScaleService.selectSingleByProperty("serNo", serNo);
            if (entity == null) return returnFail("数据传输异常，请重试！");

            User user = HttpUtil.getCurrentUser(request);
            entity.setUpdater(user.getUserId());
            entity.setUpdateTime(DateUtil.getNowTimestamp());
            entity.setSubProjectInfo(subProject);
            investScaleService.saveOrUpdate(entity);
            
            re.setMsg("保存成功！");
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }
    
    /**
     * 获取 投资子项目 列表
     * @param request
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("getSubProjectList.do")
    @ResponseBody
    public ResultDto getSubProjectList(HttpServletRequest request, String serNo){
        ResultDto re = returnFail("获取数据失败！");
        if(StringUtil.isEmptyString(serNo)){
            re.setMsg("任务流水号为空");
            return re;
        }
        try {
            List<SubProjectDto> list = investScaleService.getSubProjectList(serNo);
            if (list == null || list.size() <= 0){
                list = new ArrayList<>();
            }
            re = returnSuccess();
            re.setRows(list);
            request.setAttribute("serNo", serNo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 跳转到 项目进展情况
     * @param serNo
     * @return
     */
    @RequestMapping("projectProgress.do")
    public ModelAndView projectProgress(String serNo){
        ModelAndView mav =  new ModelAndView("views/survey/investProject/toProgressInfo");
        mav.addObject("serNo", serNo);
        return mav;
    }

    /**
     * 跳转到 项目进展情况 相应界面
     * @param serNo
     * @param type
     * @return
     */
    @RequestMapping("toProgressInfo.do")
    public ModelAndView toProgressInfo(String serNo, String type){
        ModelAndView mav =  new ModelAndView();
        try {
            InvestScale investScale = investScaleService.selectSingleByProperty("serNo", serNo);
            if (investScale != null)
                mav.addObject("progressInfo", investScale.getProgressInfo());
            mav.addObject("serNo", serNo);
            if (Constants.LB_PAGE_TYPE.EQUIPMENT_BUY.equals(type)){
                mav.setViewName("views/survey/investProject/equipmentInfo");
            }else if (Constants.LB_PAGE_TYPE.PROJECT_COST.equals(type)){
                mav.setViewName("views/survey/investProject/projectCost");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }

    /**
     * 获取 工程费用明细 列表
     * @param request
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("getCostList.json")
    @ResponseBody
    public ResultDto getCostList(HttpServletRequest request, String serNo){
        ResultDto re = returnFail("获取数据失败！");
        if(StringUtil.isEmptyString(serNo)){
            re.setMsg("任务流水号为空");
            return re;
        }
        try {
            List<ProjectCost> list = projectCostService.selectByProperty("serNo", serNo);
            re = returnSuccess();
            re.setRows(MapperUtil.trans(list, ProjectCostDto.class));
            request.setAttribute("serNo", serNo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 获取 设备购置情况 列表
     * @param request
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("getEquipmentList.json")
    @ResponseBody
    public ResultDto getEquipmentList(HttpServletRequest request, String serNo){
        ResultDto re = returnFail("获取数据失败！");
        if(StringUtil.isEmptyString(serNo)){
            re.setMsg("任务流水号为空");
            return re;
        }
        try {
            List<DevicePurchase> list = devicePurchaseService.selectByProperty("serNo", serNo);
            re = returnSuccess();
            re.setRows(MapperUtil.trans(list, equipmentDto.class));
            request.setAttribute("serNo", serNo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 保存/修改 工程费用明细
     * @param request
     * @return
     */
    @RequestMapping("saveProjectCast.json")
    @ResponseBody
    public ResultDto saveProjectCast(HttpServletRequest request, String jsonArray) {
        ResultDto re = returnFail("保存失败");
        try {
            List<ProjectCost> entityList = 
                    JacksonUtil.fromJson(jsonArray, new TypeReference<ArrayList<ProjectCost>>() {});
            if (CollectionUtils.isEmpty(entityList))
                return returnFail("请先填写相关信息！");

            User user = HttpUtil.getCurrentUser(request);
            Timestamp now = DateUtil.getNowTimestamp();
            for (ProjectCost projectCost : entityList) {
                if (projectCost.getId() == null){
                    projectCost.setCreater(user.getUserId());
                    projectCost.setCreateTime(now);
                }
                projectCost.setStatus(Constants.NORMAL);
                projectCost.setUpdater(user.getUserId());
                projectCost.setUpdateTime(now);
                projectCostService.saveOrUpdate(projectCost);
            }

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 保存/修改 设备购置情况
     * @param request
     * @return
     */
    @RequestMapping("saveEquipment.json")
    @ResponseBody
    public ResultDto saveEquipment(HttpServletRequest request, String jsonArray) {
        ResultDto re = returnFail("保存失败");
        try {
            List<DevicePurchase> entityList =
                    JacksonUtil.fromJson(jsonArray, new TypeReference<ArrayList<DevicePurchase>>() {});
            if (CollectionUtils.isEmpty(entityList))
                return returnFail("请先填写相关信息！");

            User user = HttpUtil.getCurrentUser(request);
            Timestamp now = DateUtil.getNowTimestamp();
            for (DevicePurchase equipment : entityList) {
                equipment.setPriceSum(equipment.getNumber() * equipment.getPrice());
                if (equipment.getId() == null){
                    equipment.setCreater(user.getUserId());
                    equipment.setCreateTime(now);
                }
                equipment.setStatus(Constants.NORMAL);
                equipment.setUpdater(user.getUserId());
                equipment.setUpdateTime(now);
                devicePurchaseService.saveOrUpdate(equipment);
            }

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

   
    /**
     * 删除 工程费用明细
     * @param request
     * @param id
     * @return
     */
    @RequestMapping("removeProjectCast.json")
    @ResponseBody
    public ResultDto removeProjectCast(HttpServletRequest request, Integer id){
        ResultDto re = returnFail("删除数据异常！");
        try{
            User user = HttpUtil.getCurrentUser(request);
            ProjectCost projectCost = projectCostService.selectByPrimaryKey(id);
            projectCost.setStatus(Constants.DELETE);
            projectCost.setUpdater(user.getUserId());
            projectCost.setUpdateTime(DateUtil.getNowTimestamp());
            projectCostService.saveOrUpdate(projectCost);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("删除数据成功！");
        }catch (Exception e){
            e.printStackTrace();
        }
        return re;
    }


    /**
     * 删除 设备购置情况
     * @param request
     * @param id
     * @return
     */
    @RequestMapping("removeEquipment.json")
    @ResponseBody
    public ResultDto removeEquipment(HttpServletRequest request, Integer id){
        ResultDto re = returnFail("删除数据异常！");
        try{
            User user = HttpUtil.getCurrentUser(request);
            DevicePurchase equipment = devicePurchaseService.selectByPrimaryKey(id);
            equipment.setStatus(Constants.DELETE);
            equipment.setUpdater(user.getUserId());
            equipment.setUpdateTime(DateUtil.getNowTimestamp());
            devicePurchaseService.saveOrUpdate(equipment);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("删除数据成功！");
        }catch (Exception e){
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 保存/修改 投资规模 分析评价信息
     * @param request
     * @return
     */
    @RequestMapping("saveInvestAnalysis.json")
    @ResponseBody
    public ResultDto saveInvestAnalysis(HttpServletRequest request, String serNo) {
        ResultDto re = returnFail("保存失败");
        String fundsSource = request.getParameter("fundsSource");
        String fundsUse = request.getParameter("fundsUse");
        String progressInfo = request.getParameter("progressInfo");
        String investmentInfo = request.getParameter("investmentInfo");
        try {
            User user = HttpUtil.getCurrentUser(request);
            InvestScale entity = investScaleService.selectSingleByProperty("serNo", serNo);
            if (entity == null){
                entity = new InvestScale();
                entity.setSerNo(serNo);
                entity.setCreater(user.getUserId());
                entity.setCreateTime(DateUtil.getNowTimestamp());
            }
            if (fundsSource != null) entity.setFundsSource(fundsSource);
            if (fundsUse != null) entity.setFundsUse(fundsUse);
            if (progressInfo != null) entity.setProgressInfo(progressInfo);
            if (investmentInfo != null) entity.setInvestmentInfo(investmentInfo);

            entity.setUpdater(user.getUserId());
            entity.setUpdateTime(DateUtil.getNowTimestamp());
            investScaleService.saveOrUpdate(entity);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /** 跳转到 企业经营状况(项目类) */
    @RequestMapping("businessInfo.do")
    public ModelAndView businessInfo(String serNo){
        ModelAndView mav =  new ModelAndView("views/survey/investProject/businessInfo");
        BusinessManage businessInfo = businessManageService.selectSingleByProperty("serNo", serNo);
        mav.addObject("businessInfo", businessInfo);
        mav.addObject("serNo", serNo);
        return mav;
    }

    @RequestMapping(value = {"saveBusinessInfo.do"}, method = RequestMethod.POST)
    @ResponseBody
    public ResultDto saveBusinessInfo(HttpServletRequest request, String serNo){
        ResultDto re = returnFail("保存失败！");
        String businessInfo = request.getParameter("businessInfo");
        try {
            BusinessManage businessManage = JacksonUtil.fromJson(businessInfo, BusinessManage.class);
            if (businessManage == null){
                re.setMsg("传输数据为空，请重试！");
                return re;
            }

            BusinessManage oldInfo =
                    businessManageService.selectSingleByProperty("serNo", serNo);
            if (oldInfo != null){
                businessManage.setId(oldInfo.getId());
                businessManage.setSerNo(serNo);
            }
            User user = HttpUtil.getCurrentUser(request);
            if (businessManage.getId() == null){
                businessManage.setCreater(user.getUserId());
                businessManage.setCreateTime(DateUtil.getNowTimestamp());
            }
            businessManage.setUpdater(user.getUserId());
            businessManage.setUpdateTime(DateUtil.getNowTimestamp());
            businessManage.setStatus(Constants.NORMAL);
            businessManageService.saveOrUpdate(businessManage);

            re.setMsg("保存成功！");
            re.setCode(Constants.NORMAL);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return re;
    }


}

