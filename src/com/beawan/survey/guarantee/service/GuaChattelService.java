package com.beawan.survey.guarantee.service;

import com.beawan.core.BaseService;
import com.beawan.survey.guarantee.entity.GuaChattel;

/**
 * @author yzj
 */
public interface GuaChattelService extends BaseService<GuaChattel> {
}
