package com.beawan.survey.guarantee.service;

import com.beawan.core.BaseService;
import com.beawan.survey.guarantee.entity.CompFinanceInfo;

/**
 * @author yzj
 */
public interface CompFinanceInfoService extends BaseService<CompFinanceInfo> {
}
