package com.beawan.survey.guarantee.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.guarantee.dao.CompFinanceInfoDao;
import com.beawan.survey.guarantee.entity.CompFinanceInfo;
import com.beawan.survey.guarantee.service.CompFinanceInfoService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.guarantee.entity.CompFinanceInfo;

/**
 * @author yzj
 */
@Service("compFinanceInfoService")
public class CompFinanceInfoServiceImpl extends BaseServiceImpl<CompFinanceInfo> implements CompFinanceInfoService{
    /**
    * 注入DAO
    */
    @Resource(name = "compFinanceInfoDao")
    public void setDao(BaseDao<CompFinanceInfo> dao) {
        super.setDao(dao);
    }
    @Resource
    public CompFinanceInfoDao compFinanceInfoDao;
}
