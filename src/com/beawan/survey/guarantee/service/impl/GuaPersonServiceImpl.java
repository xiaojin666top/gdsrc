package com.beawan.survey.guarantee.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.guarantee.dao.GuaPersonDao;
import com.beawan.survey.guarantee.entity.GuaPerson;
import com.beawan.survey.guarantee.service.GuaPersonService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.guarantee.entity.GuaPerson;

/**
 * @author yzj
 */
@Service("guaPersonService")
public class GuaPersonServiceImpl extends BaseServiceImpl<GuaPerson> implements GuaPersonService{
    /**
    * 注入DAO
    */
    @Resource(name = "guaPersonDao")
    public void setDao(BaseDao<GuaPerson> dao) {
        super.setDao(dao);
    }
    @Resource
    public GuaPersonDao guaPersonDao;
}
