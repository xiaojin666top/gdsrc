package com.beawan.survey.guarantee.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.guarantee.dao.GuaDepositReceiptDao;
import com.beawan.survey.guarantee.entity.GuaDepositReceipt;
import com.beawan.survey.guarantee.service.GuaDepositReceiptService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.guarantee.entity.GuaDepositReceipt;

/**
 * @author yzj
 */
@Service("guaDepositReceiptService")
public class GuaDepositReceiptServiceImpl extends BaseServiceImpl<GuaDepositReceipt> implements GuaDepositReceiptService{
    /**
    * 注入DAO
    */
    @Resource(name = "guaDepositReceiptDao")
    public void setDao(BaseDao<GuaDepositReceipt> dao) {
        super.setDao(dao);
    }
    @Resource
    public GuaDepositReceiptDao guaDepositReceiptDao;
}
