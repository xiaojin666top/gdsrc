package com.beawan.survey.guarantee.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.dtmp.service.DtmpService;
import com.beawan.qcc.dto.PartnersDto;
import com.beawan.qcc.dto.QccResultFullDatailDto;
import com.beawan.qcc.service.SynchronizationQCCService;
import com.beawan.survey.custInfo.bean.CompBase;
import com.beawan.survey.custInfo.dao.ICompBaseDAO;
import com.beawan.survey.guarantee.dao.GuaCompanyDao;
import com.beawan.survey.guarantee.entity.GuaCompany;
import com.beawan.survey.guarantee.service.GuaCompanyService;
import com.platform.util.DateUtil;
import com.platform.util.StringUtil;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.Resource;

/**
 * @author yzj
 */
@Service("thirdPartyGuaService")
public class GuaCompanyServiceImpl extends BaseServiceImpl<GuaCompany> implements GuaCompanyService {
    /**
    * 注入DAO
    */
    @Resource(name = "thirdPartyGuaDao")
    public void setDao(BaseDao<GuaCompany> dao) {
        super.setDao(dao);
    }
    @Resource
    public GuaCompanyDao guaCompanyDao;
    @Resource
    private ICompBaseDAO compBaseDAO;
    @Resource
    private SynchronizationQCCService synchronizationQCCService;
    @Resource
	private DtmpService dtmpService;
    @Resource
	private ICusBaseSV cusBaseSV;
    
    @Override
    public GuaCompany getBySerNoAndComp(String serNo, String companyName) {
        Map<String, Object> params = new HashMap<>();
        params.put("serNo", serNo);
        params.put("companyName", companyName);
        List<GuaCompany> entityList = guaCompanyDao.selectByProperty(params);
        if (CollectionUtils.isEmpty(entityList))
            return null;
        return entityList.get(0);
    }
    
    @Override
    public Boolean syncGuaCompany(String serNo, String companyName) throws Exception {
        GuaCompany entity = new GuaCompany();
        entity.setSerNo(serNo);
        entity.setCompanyName(companyName);
        entity.setCreater("SYSTEM");
        entity.setCreateTime(DateUtil.getNowTimestamp());
        //1、先查看本地 CompBase 表是否有数据，有则同步到 担保企业信息表 GuaCompany
        CompBase compBase = compBaseDAO.queryByCustomerName(companyName);
        if (!Objects.isNull(compBase)){
            entity.setFoundDate(compBase.getFoundDate());
            entity.setRegCapital(compBase.getRegisterCapital());
            entity.setRealCapital(compBase.getRealCapital());
            entity.setLegalPerson(compBase.getLegalPerson());
            entity.setControlPerson(compBase.getControlPerson());
            entity.setAddress(compBase.getRegisterAddress());// compBase.getRunAddress()
            entity.setMainBusiness(compBase.getMainBusiness());
            entity.setCreditRecord(compBase.getCreditRecord());
            entity.setUpdater("COMP_BASE");
            entity.setUpdateTime(DateUtil.getNowTimestamp());
            guaCompanyDao.saveOrUpdate(entity);
            List<GuaCompany> guaCompanyList = guaCompanyDao.selectByProperty("serNo", serNo);
            if(!CollectionUtils.isEmpty(guaCompanyList)){
	            for(GuaCompany guaCompany : guaCompanyList){
	            	if(companyName.equals(guaCompany.getCompanyName())){
			            CusBase cusBase = cusBaseSV.queryByCusName(entity.getCompanyName());
			            dtmpService.saveCompFinanceInfo(cusBase.getXdCustomerNo(), guaCompany);
		            }
	            }
            }
            return true;
        }

        //2、本地无数据，则同步企查查数据到 担保企业信息表 GuaCompany
        QccResultFullDatailDto qccData = synchronizationQCCService.getQccFullByCustomerName(companyName);
        if (qccData != null){
            entity.setFoundDate(qccData.getStartDate());
            String regCapital = qccData.getRegistCapi();// 注册资本
            if (!StringUtil.isEmptyString(regCapital)) {
                String[] values = StringUtil.spiltMoney(regCapital);
                entity.setRegCapital(Double.parseDouble(values[0]));
                entity.setRealCapital(Double.parseDouble(values[0]));
            }
            String realCapital = qccData.getRecCap();// 实收资本
            if (!StringUtil.isEmptyString(realCapital)) {
                String[] values = StringUtil.spiltMoney(realCapital);
                entity.setRealCapital(Double.parseDouble(values[0]));
            }
            entity.setLegalPerson(qccData.getOperName());
            //出资人及出资信息 实际控制人
            List<PartnersDto> partners = qccData.getPartners();
            if(!CollectionUtils.isEmpty(partners)){
                for(PartnersDto partner : partners){
                    List<String> tagsList = partner.getTagsList();
                    if(tagsList.contains("实际控制人")){
                        entity.setControlPerson(partner.getStockName());
                        break;
                    }
                }
            }
            entity.setAddress(qccData.getAddress());// compBase.getRunAddress()
            entity.setMainBusiness(qccData.getScope());

            entity.setUpdater("QCC");
            entity.setUpdateTime(DateUtil.getNowTimestamp());
            guaCompanyDao.saveOrUpdate(entity);
            return true;
        }
        
        //3、本地/QCC 暂无数据，保存该企业
        //guaCompanyDao.saveOrUpdate(entity);
        return false;
    }
}
