package com.beawan.survey.guarantee.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.guarantee.dao.GuaAccountsDao;
import com.beawan.survey.guarantee.entity.GuaAccounts;
import com.beawan.survey.guarantee.service.GuaAccountsService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.guarantee.entity.GuaAccounts;

/**
 * @author yzj
 */
@Service("guaAccountsService")
public class GuaAccountsServiceImpl extends BaseServiceImpl<GuaAccounts> implements GuaAccountsService{
    /**
    * 注入DAO
    */
    @Resource(name = "guaAccountsDao")
    public void setDao(BaseDao<GuaAccounts> dao) {
        super.setDao(dao);
    }
    @Resource
    public GuaAccountsDao guaAccountsDao;
}
