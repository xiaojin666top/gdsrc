package com.beawan.survey.guarantee.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.guarantee.dao.GuaChattelDao;
import com.beawan.survey.guarantee.entity.GuaChattel;
import com.beawan.survey.guarantee.service.GuaChattelService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.guarantee.entity.GuaChattel;

/**
 * @author yzj
 */
@Service("guaChattelService")
public class GuaChattelServiceImpl extends BaseServiceImpl<GuaChattel> implements GuaChattelService{
    /**
    * 注入DAO
    */
    @Resource(name = "guaChattelDao")
    public void setDao(BaseDao<GuaChattel> dao) {
        super.setDao(dao);
    }
    @Resource
    public GuaChattelDao guaChattelDao;
}
