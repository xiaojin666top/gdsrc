package com.beawan.survey.guarantee.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.guarantee.dao.GuaEstateDao;
import com.beawan.survey.guarantee.entity.GuaEstate;
import com.beawan.survey.guarantee.service.GuaEstateService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.guarantee.entity.GuaEstate;

/**
 * @author yzj
 */
@Service("guaEstateService")
public class GuaEstateServiceImpl extends BaseServiceImpl<GuaEstate> implements GuaEstateService{
    /**
    * 注入DAO
    */
    @Resource(name = "guaEstateDao")
    public void setDao(BaseDao<GuaEstate> dao) {
        super.setDao(dao);
    }
    @Resource
    public GuaEstateDao guaEstateDao;
}
