package com.beawan.survey.guarantee.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.guarantee.dao.GuaAnalysisDao;
import com.beawan.survey.guarantee.entity.GuaAnalysis;
import com.beawan.survey.guarantee.service.GuaAnalysisService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.guarantee.entity.GuaAnalysis;

/**
 * @author yzj
 */
@Service("guaAnalysisService")
public class GuaAnalysisServiceImpl extends BaseServiceImpl<GuaAnalysis> implements GuaAnalysisService{
    /**
    * 注入DAO
    */
    @Resource(name = "guaAnalysisDao")
    public void setDao(BaseDao<GuaAnalysis> dao) {
        super.setDao(dao);
    }
    @Resource
    public GuaAnalysisDao guaAnalysisDao;
}
