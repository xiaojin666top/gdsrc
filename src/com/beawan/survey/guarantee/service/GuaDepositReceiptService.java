package com.beawan.survey.guarantee.service;

import com.beawan.core.BaseService;
import com.beawan.survey.guarantee.entity.GuaDepositReceipt;

/**
 * @author yzj
 */
public interface GuaDepositReceiptService extends BaseService<GuaDepositReceipt> {
}
