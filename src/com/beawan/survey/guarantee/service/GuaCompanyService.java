package com.beawan.survey.guarantee.service;

import com.beawan.core.BaseService;
import com.beawan.qcc.dto.QccResultFullDatailDto;
import com.beawan.survey.guarantee.entity.GuaCompany;

/**
 * @author yzj
 */
public interface GuaCompanyService extends BaseService<GuaCompany> {
	 /**
     * 根据 流水号、企业名 获取担保企业信息
     * @param serNo 流水号
     * @param companyName 企业名
     * @return
     */
	GuaCompany getBySerNoAndComp(String serNo, String companyName);

	/**
     * 同步 本地/QCC 数据到 担保企业信息表
     * @param serNo 流水号
     * @param companyName 流水号
     * @return true/false 数据是否同步成功
     * @throws Exception
     */
    Boolean syncGuaCompany(String serNo, String companyName) throws Exception;
}
