package com.beawan.survey.guarantee.service;

import com.beawan.core.BaseService;
import com.beawan.survey.guarantee.entity.GuaPerson;

/**
 * @author yzj
 */
public interface GuaPersonService extends BaseService<GuaPerson> {
}
