package com.beawan.survey.guarantee.service;

import com.beawan.core.BaseService;
import com.beawan.survey.guarantee.entity.GuaAnalysis;

/**
 * @author yzj
 */
public interface GuaAnalysisService extends BaseService<GuaAnalysis> {
}
