package com.beawan.survey.guarantee.service;

import com.beawan.core.BaseService;
import com.beawan.survey.guarantee.entity.GuaEstate;

/**
 * @author yzj
 */
public interface GuaEstateService extends BaseService<GuaEstate> {
}
