package com.beawan.survey.guarantee.service;

import com.beawan.core.BaseService;
import com.beawan.survey.guarantee.entity.GuaAccounts;

/**
 * @author yzj
 */
public interface GuaAccountsService extends BaseService<GuaAccounts> {
}
