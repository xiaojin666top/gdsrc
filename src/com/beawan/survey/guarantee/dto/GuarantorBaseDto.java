package com.beawan.survey.guarantee.dto;

/**
 * 担保自然人 基本信息DTO
 * @author zxh
 * @date 2020/7/15 11:25
 */
public class GuarantorBaseDto {

    private Integer id;
    private String serNo;
    private String name; // 担保自然人
    private String sex; // 性别
    private double lastYearIncome;// 去年年收入
    private Double guaranteeAmt;// 拟担保金额
    private String guaranteeWay;// 担保方式
    private String maritalStatus; // 婚姻状况
    private String mobilePhone; // 联系电话
    private String certCode; // 证件号码
    private String address;//地址

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public double getLastYearIncome() {
        return lastYearIncome;
    }

    public void setLastYearIncome(double lastYearIncome) {
        this.lastYearIncome = lastYearIncome;
    }

    public Double getGuaranteeAmt() {
        return guaranteeAmt;
    }

    public void setGuaranteeAmt(Double guaranteeAmt) {
        this.guaranteeAmt = guaranteeAmt;
    }

    public String getGuaranteeWay() {
        return guaranteeWay;
    }

    public void setGuaranteeWay(String guaranteeWay) {
        this.guaranteeWay = guaranteeWay;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}
