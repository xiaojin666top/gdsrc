package com.beawan.survey.guarantee.dto;

public class GuaEstateDto {
 	private Integer id;
    private String serNo;// 任务流水号
    private String mortgagor;// 抵押人
    private String mortgagorType;// 类型
    private String location;// 位置
    private String houseType;// 房屋/土地性质
    private double houseArea;// 面积
    private String certificateNo;// 权证号--产权证书号码
    private String certificateDate;// 证书取得日
    private double estimatedValue;// 评估价值/协议价
    private double perSquareValue;// 每平价值
    private String evaluationAgency;// 评估机构
    private String useOrRent;// 自用/出租
    private String isHandleInsurance;// 是否拟办保险
    private double loanAmount;// 贷款额度
    private double discountRate;// 折率

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getMortgagor() {
        return mortgagor;
    }

    public void setMortgagor(String mortgagor) {
        this.mortgagor = mortgagor;
    }

    public String getMortgagorType() {
        return mortgagorType;
    }

    public void setMortgagorType(String mortgagorType) {
        this.mortgagorType = mortgagorType;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getHouseType() {
        return houseType;
    }

    public void setHouseType(String houseType) {
        this.houseType = houseType;
    }

    public String getCertificateNo() {
        return certificateNo;
    }

    public void setCertificateNo(String certificateNo) {
        this.certificateNo = certificateNo;
    }

    public String getCertificateDate() {
        return certificateDate;
    }

    public void setCertificateDate(String certificateDate) {
        this.certificateDate = certificateDate;
    }

    public String getEvaluationAgency() {
        return evaluationAgency;
    }

    public void setEvaluationAgency(String evaluationAgency) {
        this.evaluationAgency = evaluationAgency;
    }

    public String getUseOrRent() {
        return useOrRent;
    }

    public void setUseOrRent(String useOrRent) {
        this.useOrRent = useOrRent;
    }

    public String getIsHandleInsurance() {
        return isHandleInsurance;
    }

    public void setIsHandleInsurance(String isHandleInsurance) {
        this.isHandleInsurance = isHandleInsurance;
    }

    public double getHouseArea() {
        return houseArea;
    }

    public void setHouseArea(double houseArea) {
        this.houseArea = houseArea;
    }

    public double getEstimatedValue() {
        return estimatedValue;
    }

    public void setEstimatedValue(double estimatedValue) {
        this.estimatedValue = estimatedValue;
    }

    public double getPerSquareValue() {
        return perSquareValue;
    }

    public void setPerSquareValue(double perSquareValue) {
        this.perSquareValue = perSquareValue;
    }

    public double getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(double loanAmount) {
        this.loanAmount = loanAmount;
    }

    public double getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(double discountRate) {
        this.discountRate = discountRate;
    }	
}
