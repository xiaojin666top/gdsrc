package com.beawan.survey.guarantee.dto;

/**
 * 担保企业财务信息DTO
 * @author zxh
 * @date 2020/7/14 17:48
 */
public class CompFinanceInfoDto {

    private Integer id;
    private String serNo;// 任务流水号
    private Integer guaCompanyId;// 担保企业
    private String years; // 年份
    private Double assetsSum;// 总资产
    private Double debtSum;// 总负债
    private Double netAssets;// 净资产
    private Double salesRevenue;// 销售收入
    private Double netProfit;// 净利润
    private Double assetDebtRate; // 资产负债率

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public Integer getGuaCompanyId() {
        return guaCompanyId;
    }

    public void setGuaCompanyId(Integer guaCompanyId) {
        this.guaCompanyId = guaCompanyId;
    }

    public String getYears() {
        return years;
    }

    public void setYears(String years) {
        this.years = years;
    }

    public Double getAssetsSum() {
        return assetsSum;
    }

    public void setAssetsSum(Double assetsSum) {
        this.assetsSum = assetsSum;
    }

    public Double getDebtSum() {
        return debtSum;
    }

    public void setDebtSum(Double debtSum) {
        this.debtSum = debtSum;
    }

    public Double getNetAssets() {
        return netAssets;
    }

    public void setNetAssets(Double netAssets) {
        this.netAssets = netAssets;
    }

    public Double getSalesRevenue() {
        return salesRevenue;
    }

    public void setSalesRevenue(Double salesRevenue) {
        this.salesRevenue = salesRevenue;
    }

    public Double getNetProfit() {
        return netProfit;
    }

    public void setNetProfit(Double netProfit) {
        this.netProfit = netProfit;
    }

    public Double getAssetDebtRate() {
        return assetDebtRate;
    }

    public void setAssetDebtRate(Double assetDebtRate) {
        this.assetDebtRate = assetDebtRate;
    }
}
