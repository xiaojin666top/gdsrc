package com.beawan.survey.guarantee.dto;

/**
 * 第三方担保企业 基本信息DTO
 * @author zxh
 * @date 2020/7/11 18:01
 */
public class GuaCompBaseDto {

    private Integer id;
    private String serNo;// 任务流水号
    private String companyName;// 担保企业
    private Double guaranteeAmt;// 拟担保金额
    private String guaranteeWay;// 担保方式
    private String foundDate;// 成立日期
    private Double regCapital; // 注册资本
    private Double realCapital;// 实收资本
    private String legalPerson;// 法定代表人
    private String controlPerson;// 实际控制人

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Double getGuaranteeAmt() {
        return guaranteeAmt;
    }

    public void setGuaranteeAmt(Double guaranteeAmt) {
        this.guaranteeAmt = guaranteeAmt;
    }

    public String getGuaranteeWay() {
        return guaranteeWay;
    }

    public void setGuaranteeWay(String guaranteeWay) {
        this.guaranteeWay = guaranteeWay;
    }

    public String getFoundDate() {
        return foundDate;
    }

    public void setFoundDate(String foundDate) {
        this.foundDate = foundDate;
    }

    public Double getRegCapital() {
        return regCapital;
    }

    public void setRegCapital(Double regCapital) {
        this.regCapital = regCapital;
    }

    public Double getRealCapital() {
        return realCapital;
    }

    public void setRealCapital(Double realCapital) {
        this.realCapital = realCapital;
    }

    public String getLegalPerson() {
        return legalPerson;
    }

    public void setLegalPerson(String legalPerson) {
        this.legalPerson = legalPerson;
    }

    public String getControlPerson() {
        return controlPerson;
    }

    public void setControlPerson(String controlPerson) {
        this.controlPerson = controlPerson;
    }
}
