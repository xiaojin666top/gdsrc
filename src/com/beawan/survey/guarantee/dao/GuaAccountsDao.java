package com.beawan.survey.guarantee.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.guarantee.entity.GuaAccounts;

/**
 * @author yzj
 */
public interface GuaAccountsDao extends BaseDao<GuaAccounts> {
}
