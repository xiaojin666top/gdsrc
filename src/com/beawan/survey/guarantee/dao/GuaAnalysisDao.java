package com.beawan.survey.guarantee.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.guarantee.entity.GuaAnalysis;

/**
 * @author yzj
 */
public interface GuaAnalysisDao extends BaseDao<GuaAnalysis> {
}
