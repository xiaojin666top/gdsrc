package com.beawan.survey.guarantee.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.guarantee.entity.GuaPerson;

/**
 * @author yzj
 */
public interface GuaPersonDao extends BaseDao<GuaPerson> {
}
