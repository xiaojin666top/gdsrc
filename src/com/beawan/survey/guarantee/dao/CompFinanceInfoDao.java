package com.beawan.survey.guarantee.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.guarantee.entity.CompFinanceInfo;

/**
 * @author yzj
 */
public interface CompFinanceInfoDao extends BaseDao<CompFinanceInfo> {
	/**
	 * 根据时间和ID获取担保公司财务状况信息（对公库）
	 * 
	 * @param xdCustNo
	 * @return
	 */
	CompFinanceInfo getCompFinanceInfo(String guaCompanyId, String years);
}
