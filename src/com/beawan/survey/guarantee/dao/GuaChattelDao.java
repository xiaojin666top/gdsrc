package com.beawan.survey.guarantee.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.guarantee.entity.GuaChattel;

/**
 * @author yzj
 */
public interface GuaChattelDao extends BaseDao<GuaChattel> {
}
