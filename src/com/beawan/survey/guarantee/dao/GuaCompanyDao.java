package com.beawan.survey.guarantee.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.guarantee.entity.GuaCompany;

/**
 * @author yzj
 */
public interface GuaCompanyDao extends BaseDao<GuaCompany> {
}
