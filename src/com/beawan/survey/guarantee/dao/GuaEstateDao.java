package com.beawan.survey.guarantee.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.guarantee.entity.GuaEstate;

/**
 * @author yzj
 */
public interface GuaEstateDao extends BaseDao<GuaEstate> {
}
