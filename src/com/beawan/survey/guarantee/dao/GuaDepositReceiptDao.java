package com.beawan.survey.guarantee.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.guarantee.entity.GuaDepositReceipt;

/**
 * @author yzj
 */
public interface GuaDepositReceiptDao extends BaseDao<GuaDepositReceipt> {
}
