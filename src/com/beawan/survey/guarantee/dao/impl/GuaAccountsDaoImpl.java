package com.beawan.survey.guarantee.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.guarantee.dao.GuaAccountsDao;
import com.beawan.survey.guarantee.entity.GuaAccounts;
import org.springframework.stereotype.Repository;
import com.beawan.survey.guarantee.entity.GuaAccounts;

/**
 * @author yzj
 */
@Repository("guaAccountsDao")
public class GuaAccountsDaoImpl extends BaseDaoImpl<GuaAccounts> implements GuaAccountsDao{

}
