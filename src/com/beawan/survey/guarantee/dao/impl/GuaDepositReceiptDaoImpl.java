package com.beawan.survey.guarantee.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.guarantee.dao.GuaDepositReceiptDao;
import com.beawan.survey.guarantee.entity.GuaDepositReceipt;
import org.springframework.stereotype.Repository;
import com.beawan.survey.guarantee.entity.GuaDepositReceipt;

/**
 * @author yzj
 */
@Repository("guaDepositReceiptDao")
public class GuaDepositReceiptDaoImpl extends BaseDaoImpl<GuaDepositReceipt> implements GuaDepositReceiptDao{

}
