package com.beawan.survey.guarantee.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.guarantee.dao.GuaAnalysisDao;
import com.beawan.survey.guarantee.entity.GuaAnalysis;
import org.springframework.stereotype.Repository;
import com.beawan.survey.guarantee.entity.GuaAnalysis;

/**
 * @author yzj
 */
@Repository("guaAnalysisDao")
public class GuaAnalysisDaoImpl extends BaseDaoImpl<GuaAnalysis> implements GuaAnalysisDao{

}
