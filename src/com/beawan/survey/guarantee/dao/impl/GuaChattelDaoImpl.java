package com.beawan.survey.guarantee.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.guarantee.dao.GuaChattelDao;
import com.beawan.survey.guarantee.entity.GuaChattel;
import org.springframework.stereotype.Repository;
import com.beawan.survey.guarantee.entity.GuaChattel;

/**
 * @author yzj
 */
@Repository("guaChattelDao")
public class GuaChattelDaoImpl extends BaseDaoImpl<GuaChattel> implements GuaChattelDao{

}
