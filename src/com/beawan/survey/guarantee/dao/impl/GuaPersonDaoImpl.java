package com.beawan.survey.guarantee.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.guarantee.dao.GuaPersonDao;
import com.beawan.survey.guarantee.entity.GuaPerson;
import org.springframework.stereotype.Repository;
import com.beawan.survey.guarantee.entity.GuaPerson;

/**
 * @author yzj
 */
@Repository("guaPersonDao")
public class GuaPersonDaoImpl extends BaseDaoImpl<GuaPerson> implements GuaPersonDao{

}
