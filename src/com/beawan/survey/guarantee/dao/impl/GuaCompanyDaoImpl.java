package com.beawan.survey.guarantee.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.guarantee.dao.GuaCompanyDao;
import com.beawan.survey.guarantee.entity.GuaCompany;
import org.springframework.stereotype.Repository;

/**
 * @author yzj
 */
@Repository("thirdPartyGuaDao")
public class GuaCompanyDaoImpl extends BaseDaoImpl<GuaCompany> implements GuaCompanyDao {

}
