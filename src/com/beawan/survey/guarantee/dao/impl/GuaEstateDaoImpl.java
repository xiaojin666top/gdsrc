package com.beawan.survey.guarantee.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.guarantee.dao.GuaEstateDao;
import com.beawan.survey.guarantee.entity.GuaEstate;
import org.springframework.stereotype.Repository;
import com.beawan.survey.guarantee.entity.GuaEstate;

/**
 * @author yzj
 */
@Repository("guaEstateDao")
public class GuaEstateDaoImpl extends BaseDaoImpl<GuaEstate> implements GuaEstateDao{

}
