package com.beawan.survey.guarantee.controller;

import com.beawan.base.entity.SysDic;
import com.beawan.base.entity.User;
import com.beawan.base.service.ISysDicSV;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.qcc.service.SynchronizationQCCService;
import com.beawan.survey.custInfo.service.CompCreditService;
import com.beawan.survey.custInfo.service.CompUserCreditService;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.beawan.survey.guarantee.dto.GuaCompBaseDto;
import com.beawan.survey.guarantee.dto.GuaEstateDto;
import com.beawan.survey.guarantee.dto.GuarantorBaseDto;
import com.beawan.survey.guarantee.entity.*;
import com.beawan.survey.guarantee.service.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.platform.util.*;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

/**
 * 企业担保措施 控制器
 * @author zxh
 * @date 2020/7/11 15:41
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/survey/guarantee/" })
public class GuaranteeController extends BaseController {

    private static final Logger log = Logger.getLogger(GuaranteeController.class);

    @Resource
    private GuaCompanyService guaCompanyService;
    @Resource
    private CompFinanceInfoService financeInfoService;
    @Resource
    private GuaPersonService guaPersonService;
    @Resource
    private GuaEstateService guaEstateService;
    @Resource
    private GuaChattelService guaChattelService;
    @Resource
    private GuaAccountsService guaAccountsService;
    @Resource
    private GuaDepositReceiptService guaDepositReceiptService;
    @Resource
    private GuaAnalysisService guaAnalysisService;
    @Resource
    private ISysDicSV sysDicSV;
    @Resource
    private SynchronizationQCCService synchronizationQCCService;
    @Resource
    private ICompBaseSV compBaseSV;
    @Resource
	private CompCreditService compCreditService;
	@Resource
	private CompUserCreditService compUserCreditService;

    /**
     * 跳转到 第三方企业保证担保
     * @param serNo
     * @return
     */
    @RequestMapping("thirdPartyGuarantee.do")
    public ModelAndView thirdPartyGua(String serNo){
        ModelAndView mav =  new ModelAndView("views/survey/guaranteeInfo/thirdPartyGuarantee");
        mav.addObject("serNo", serNo);
        try {
            //加载字典数据
            String[] optTypes = {
                    SysConstants.BsDicConstant.STD_SY_GUAR_TYPE};
            mav.addObject("dicData", sysDicSV.queryMapListByTypes(optTypes));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }

    /**
     * 获取 第三方担保企业基本信息列表
     * @param request
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("getGuaCompanyList.json")
    @ResponseBody
    public ResultDto getCompSupplyList(HttpServletRequest request, String serNo){
        ResultDto re = returnFail("获取担保企业列表失败");
        if(StringUtil.isEmptyString(serNo)){
            re.setMsg("任务流水号为空");
            return re;
        }
        try {
            List<GuaCompany> list = guaCompanyService.selectByProperty("serNo", serNo);
            re = returnSuccess();
            re.setRows(MapperUtil.trans(list, GuaCompBaseDto.class));
            request.setAttribute("serNo", serNo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 新增担保企业、同步企查查数据
     * @param companyName 担保企业全称
     * @return
     */
    @RequestMapping(value = "saveNewCompany.json", method = RequestMethod.POST)
    @ResponseBody
    public ResultDto saveNewCompany(String serNo, String companyName){
        ResultDto re = returnFail("同步企查查数据失败！");
        try {
            GuaCompany entity = guaCompanyService.getBySerNoAndComp(serNo, companyName);
            if (!Objects.isNull(entity))
                return returnFail("该担保企业：" + companyName + " 已在列表中存在！");

            Boolean flag = guaCompanyService.syncGuaCompany(serNo, companyName);
            if (!flag)
                return returnFail("该担保企业：" + companyName +"在企查查中暂无工商数据，请核实企业名称！");

            re.setMsg("同步数据成功！");
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }
    
    
    /**
     * 删除 第三方担保企业 全部信息
     * @param request
     * @param id
     * @return
     */
    @RequestMapping("deleteGuaCompany.json")
    @ResponseBody
    public ResultDto deleteGuaCompany(HttpServletRequest request, Integer id) {
        ResultDto re = returnFail("删除失败");
        if (id == null || id == 0) {
            re.setMsg("传输数据内容异常，请重试");
            return re;
        }
        User user = HttpUtil.getCurrentUser(request);
        try {
            GuaCompany guaCompany = guaCompanyService.findByPrimaryKey(id);
            guaCompany.setStatus(Constants.DELETE);
            guaCompany.setUpdater(user.getUserId());
            guaCompany.setUpdateTime(DateUtil.getNowTimestamp());
            guaCompanyService.saveOrUpdate(guaCompany);
            // 删除财务信息
            List<CompFinanceInfo> financeInfos = financeInfoService.selectByProperty("guaCompanyId", id);
            if (financeInfos != null && financeInfos.size() > 0){
                for (CompFinanceInfo financeInfo : financeInfos) {
                    financeInfo.setStatus(Constants.DELETE);
                    financeInfo.setUpdater(user.getUserId());
                    financeInfo.setUpdateTime(DateUtil.getNowTimestamp());
                    financeInfoService.saveOrUpdate(financeInfo);
                }
            }

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 跳转到 第三方担保企业详情
     * @param serNo
     * @param guaCompanyId
     * @return
     */
    @RequestMapping("guaCompanyInfo.do")
    public ModelAndView guaCompanyInfo(String serNo, Integer guaCompanyId){
        ModelAndView mav =  new ModelAndView("views/survey/guaranteeInfo/toGuaCompany");
        mav.addObject("serNo", serNo);
        mav.addObject("guaCompanyId", guaCompanyId);
        return mav;
    }

    /**
     * 跳转到 担保企业各界面
     * @param serNo 流水号
     * @param pageType 页面type
     * @return
     */
    @RequestMapping("toGuaCompany.do")
    public ModelAndView toGuaCompany(String serNo, String guaCompanyId, String pageType){
    	ModelAndView mav = new ModelAndView();
        try {
            mav.addObject("serNo", serNo);
            mav.addObject("guaCompanyId", guaCompanyId);
            if (Constants.LB_PAGE_TYPE.GUA_COMP_INFO.equals(pageType)){
                mav.setViewName("views/survey/guaranteeInfo/guaCompanyInfo");
                if (!StringUtil.isEmptyString(guaCompanyId)){
                    GuaCompany guaCompany = guaCompanyService.findByPrimaryKey(Integer.parseInt(guaCompanyId));
                    mav.addObject("guaCompany", guaCompany);
                }
            }else if (Constants.LB_PAGE_TYPE.GUA_COMP_FINANCE.equals(pageType)){
                mav.setViewName("views/survey/guaranteeInfo/compFinance");
            }else if (Constants.LB_PAGE_TYPE.GUA_COMP_CREDIT.equals(pageType)){
                mav.setViewName("views/survey/guaranteeInfo/guaCompCredit");
            }
            String[] optTypes = {
            	SysConstants.BsDicConstant.STD_SY_GUAR_TYPE
    		};
            mav.addObject("dicData", sysDicSV.queryMapListByTypes(optTypes));
        } catch (Exception e) {
            e.printStackTrace();
        }     
        return mav;
    }
    
    /**
     * 获取 担保企业财务信息
     * @param guaCompanyId
     * @return
     */
    @RequestMapping("getFinanceInfoList.json")
    @ResponseBody
    public ResultDto getFinanceInfoList(Integer guaCompanyId){
        ResultDto re = returnFail("获取数据异常！");
        try{
            List<CompFinanceInfo> entityList =
                    financeInfoService.selectByProperty("guaCompanyId", guaCompanyId);
            if(!CollectionUtils.isEmpty(entityList)){
            	for(CompFinanceInfo entity : entityList){
            		if(entity.getAssetDebtRate() != null){
            			entity.setAssetDebtRate((double)Math.round((entity.getAssetDebtRate()*100)*100)/100);
            		}
            	}
            }
            re.setRows(entityList);
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("获取数据成功！");
        }catch (Exception e){
            e.printStackTrace();
        }
        return re;
    }
    
    /**
     * 新增/修改 担保企业财务信息
     * @param request
     * @param jsonData 近三年财务数据
     * @return
     */
    @RequestMapping(value = "saveCompFinance.json", method = RequestMethod.POST)
    @ResponseBody
    public ResultDto saveCompFinance(HttpServletRequest request, String jsonData) {
        ResultDto re = returnFail("保存失败");
        try {
            List<CompFinanceInfo> entityList = 
                    JacksonUtil.fromJson(jsonData, new TypeReference<List<CompFinanceInfo>>() {});
            if (CollectionUtils.isEmpty(entityList))
                return returnFail("数据传输异常，请先填写相关信息！");
            double assetDebtRate = 0.00;
            User user = HttpUtil.getCurrentUser(request);
            Timestamp nowTimestamp = DateUtil.getNowTimestamp();
            for (CompFinanceInfo entity : entityList) {
            	if(entity.getAssetsSum() != null && entity.getDebtSum()!=null &&entity.getAssetsSum() != 0d){
            		assetDebtRate = (double)(Math.round(entity.getDebtSum()/entity.getAssetsSum()*100)/100.00);
            		entity.setAssetDebtRate(assetDebtRate);
            	}
                if (entity.getId() == null){
                    entity.setCreater(user.getUserId());
                    entity.setCreateTime(nowTimestamp);
                }
                entity.setUpdater(user.getUserId());
                entity.setUpdateTime(nowTimestamp);
                entity.setStatus(Constants.NORMAL);
                financeInfoService.saveOrUpdate(entity);
            }

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }
   
    /**
     * 删除 担保企业财务信息
     * @param request
     * @param compFinanceId
     * @return
     */
    @RequestMapping("deleteFinanceInfo.json")
    @ResponseBody
    public ResultDto deleteFinanceInfo(HttpServletRequest request, Integer compFinanceId){
        ResultDto re = returnFail("删除数据异常！");
        try{
            User user = HttpUtil.getCurrentUser(request);
            CompFinanceInfo financeInfo = financeInfoService.selectByPrimaryKey(compFinanceId);
            financeInfo.setStatus(Constants.DELETE);
            financeInfo.setUpdater(user.getUserId());
            financeInfo.setUpdateTime(DateUtil.getNowTimestamp());
            financeInfoService.saveOrUpdate(financeInfo);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("删除数据成功！");
        }catch (Exception e){
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 新增/修改 担保企业详细信息
     * @param request
     * @return
     */
    @RequestMapping("saveGuaCompInfo.json")
    @ResponseBody
    public ResultDto saveCompanyInfo(HttpServletRequest request, String guaCompany) {
        ResultDto re = returnFail("保存失败");
        try {
            if (StringUtil.isEmptyString(guaCompany))
                return returnFail("数据传输异常，请重试！");
            
            GuaCompany entity = JacksonUtil.fromJson(guaCompany, GuaCompany.class);
            User user = HttpUtil.getCurrentUser(request);
            Timestamp now = DateUtil.getNowTimestamp();
            if (entity.getId() == null){
            	entity.setCreater(user.getUserId());
            	entity.setCreateTime(now);
            }
            entity.setUpdater(user.getUserId());
            entity.setUpdateTime(now);
            guaCompanyService.saveOrUpdate(entity);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 跳转到  担保自然人 页面
     * @param serNo
     * @return
     */
    @RequestMapping("guarantorInfo.do")
    public ModelAndView guarantorInfo(String serNo){
		ModelAndView mav = new ModelAndView("views/survey/guaranteeInfo/guarantorInfo");
        mav.addObject("serNo", serNo);
        try {
            //加载字典数据
            String[] optTypes = {
                    SysConstants.BsDicConstant.STD_SY_GUAR_TYPE};
            mav.addObject("dicData", sysDicSV.queryMapListByTypes(optTypes));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }

    /**
     * 获取 担保自然人 基本信息列表
     * @param serNo
     * @return
     */
    @RequestMapping("getGuaPersonList.json")
    @ResponseBody
    public ResultDto getGuaPersonList(String serNo){
        ResultDto re = returnFail("获取数据异常！");
        try{
            List<GuaPerson> guaPersonList = guaPersonService.selectByProperty("serNo", serNo);
            re.setRows(MapperUtil.trans(guaPersonList, GuarantorBaseDto.class));
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("获取数据成功！");
        }catch (Exception e){
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 删除 担保自然人
     * @param request
     * @param guaPersonId
     * @return
     */
    @RequestMapping("deleteGuaPerson.json")
    @ResponseBody
    public ResultDto deleteGuaPerson(HttpServletRequest request, Integer guaPersonId) {
        ResultDto re = returnFail("删除失败！");
        try {
            GuaPerson guarantor = guaPersonService.selectByPrimaryKey(guaPersonId);
            User user = HttpUtil.getCurrentUser(request);
            guarantor.setStatus(Constants.DELETE);
            guarantor.setUpdater(user.getUserId());
            guarantor.setUpdateTime(DateUtil.getNowTimestamp());
            guaPersonService.saveOrUpdate(guarantor);
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("删除成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 跳转到 担保自然人详情
     * @param guaPersonId
     * @return
     */
    @RequestMapping("guaPersonInfo.do")
    public ModelAndView getGuaPersonInfo(String serNo, Integer guaPersonId){
        ModelAndView mav =  new ModelAndView("views/survey/guaranteeInfo/guaPersonInfo");
        try {
            //加载字典数据
            String[] optTypes = {
                    SysConstants.BsDicConstant.STD_SY_GUAR_TYPE};
            mav.addObject("dicData", sysDicSV.queryMapListByTypes(optTypes));
        } catch (Exception e) {
            e.printStackTrace();
        }
        mav.addObject("serNo", serNo);
        mav.addObject("guaPersonId", guaPersonId);
        if (guaPersonId != null)
            mav.addObject("guarantor", guaPersonService.selectByPrimaryKey(guaPersonId));
        return mav;
    }

    /**
     * 保存/修改 担保自然人详情
     * @param request
     * @return
     */
    @RequestMapping(value = "saveGuaPerson.json", method = RequestMethod.POST)
    @ResponseBody
    public ResultDto saveGuaPerson(HttpServletRequest request){
        ResultDto re = returnFail("保存失败");
        String jsonData = request.getParameter("guaPerson");
        try {
            GuaPerson guaPerson = JacksonUtil.fromJson(jsonData, GuaPerson.class);
            if (guaPerson == null){
                re.setMsg("传输数据异常，请重试！");
                return re;
            }

            User user = HttpUtil.getCurrentUser(request);
            if (guaPerson.getId() == null){
                guaPerson.setCreater(user.getUserId());
                guaPerson.setCreateTime(DateUtil.getNowTimestamp());
            }
            guaPerson.setUpdater(user.getUserId());
            guaPerson.setUpdateTime(DateUtil.getNowTimestamp());
            guaPerson.setStatus(Constants.NORMAL);
            guaPersonService.saveOrUpdate(guaPerson);

            re.setMsg("保存成功！");
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 跳转到  抵（质）押物担保
     * @param serNo
     * @return
     */
    @RequestMapping("mortgagePledge.do")
    public ModelAndView mortgagePledge(String serNo){
        ModelAndView mav =  new ModelAndView("views/survey/guaranteeInfo/mortgagePledge");
        mav.addObject("serNo", serNo);
        return mav;
    }

    /**
     * 跳转到  抵（质）押物担保 相应界面
     * @param serNo 流水号
     * @param guaType 担保类型
     * @return
     */
    @RequestMapping("toMortgagePledge.do")
    public ModelAndView toMortgagePledge(String serNo, String guaType){
        ModelAndView mav =  new ModelAndView();
        mav.addObject("serNo", serNo);
        try {
            GuaAnalysis guaAnalysis = guaAnalysisService.selectSingleByProperty("serNo", serNo);
            mav.addObject("analysis", guaAnalysis);
            mav.addObject("dicData", sysDicSV.queryMapListByTypes(SysConstants.BsDicConstant.RG_ESTATE_TYPE));
            if (Constants.LB_PAGE_TYPE.REAL_ESTATE.equals(guaType)){
                mav.setViewName("views/survey/guaranteeInfo/estateGua");
            }else if (Constants.LB_PAGE_TYPE.CHATTEL.equals(guaType)){
                mav.setViewName("views/survey/guaranteeInfo/chattelGua");
            }else if (Constants.LB_PAGE_TYPE.ACCOUNTS_RECEIVABLE.equals(guaType)){
                mav.setViewName("views/survey/guaranteeInfo/accountsGua");
            }else if (Constants.LB_PAGE_TYPE.DEPOSIT_RECEIPT.equals(guaType)){
                mav.setViewName("views/survey/guaranteeInfo/receiptGua");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }

    /**
     * 获取 房地产担保信息 列表
     * @param serNo
     * @return
     */
    @RequestMapping("getEstateList.json")
    @ResponseBody
    public ResultDto getEstateList(String serNo){
        ResultDto re = returnFail("获取数据失败！");
        if (StringUtil.isEmptyString(serNo)){
            re.setMsg("传输参数为空，请重试！");
            return re;
        }
        try {
            List<GuaEstate> guaEstateList = guaEstateService.selectByProperty("serNo", serNo);
            List<GuaEstateDto> estateDtoList = MapperUtil.trans(guaEstateList, GuaEstateDto.class);
            //房产类型字典转换
            for (GuaEstateDto guaEstate : estateDtoList) {		
            	SysDic sysDic = this.sysDicSV.findByEnNameAndType(guaEstate.getMortgagorType(),"RG_ESTATE_TYPE");
            	if(sysDic!=null)
            		guaEstate.setMortgagorType(sysDic.getCnName());
			}
            
            re.setRows(estateDtoList);
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("获取数据成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 保存或修改 房地产担保信息
     * @param request
     * @return
     */
    @RequestMapping("saveEstateInfo.do")
    @ResponseBody
    public ResultDto saveEstateInfo(HttpServletRequest request){
        ResultDto re = returnFail("保存失败");
        String estateInfoJson = request.getParameter("estateInfo");
        String serNo = request.getParameter("serNo");
        User user = HttpUtil.getCurrentUser(request);
        try {
            GuaEstate guaEstate = JacksonUtil.fromJson(estateInfoJson, GuaEstate.class);
            if (guaEstate == null) {
                re.setMsg("传输数据内容为空，请重试");
                return re;
            }
            if (StringUtil.isEmptyString(guaEstate.getSerNo()))
                guaEstate.setSerNo(serNo);
            if(guaEstate.getId() == null){
                guaEstate.setCreater(user.getUserId());
                guaEstate.setCreateTime(DateUtil.getNowTimestamp());
            }
            guaEstate.setUpdater(user.getUserId());
            guaEstate.setUpdateTime(DateUtil.getNowTimestamp());
            guaEstate.setStatus(Constants.NORMAL);
            guaEstateService.saveOrUpdate(guaEstate);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 删除 房地产担保信息
     * @param request
     * @param id
     * @return
     */
    @RequestMapping("removeEstateInfo.do")
    @ResponseBody
    public ResultDto removeEstateInfo(HttpServletRequest request, Integer id) {
        ResultDto re = returnFail("删除失败");
        if (id == null || id == 0) {
            re.setMsg("传输数据内容异常，请重试");
            return re;
        }
        User user = HttpUtil.getCurrentUser(request);
        try {
            GuaEstate estateInfo = guaEstateService.findByPrimaryKey(id);
            estateInfo.setStatus(Constants.DELETE);
            estateInfo.setUpdater(user.getUserId());
            estateInfo.setUpdateTime(DateUtil.getNowTimestamp());
            guaEstateService.saveOrUpdate(estateInfo);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 获取 动产担保信息 列表
     * @param serNo
     * @return
     */
    @RequestMapping("getChattelList.json")
    @ResponseBody
    public ResultDto getChattelList(String serNo){
        ResultDto re = returnFail("获取数据失败！");
        if (StringUtil.isEmptyString(serNo)){
            re.setMsg("传输参数为空，请重试！");
            return re;
        }
        try {
            List<GuaChattel> chattelList = guaChattelService.selectByProperty("serNo", serNo);
            re.setRows(chattelList);
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("获取数据成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 保存或修改 动产担保信息
     * @param request
     * @return
     */
    @RequestMapping("saveChattelInfo.do")
    @ResponseBody
    public ResultDto saveChattelInfo(HttpServletRequest request){
        ResultDto re = returnFail("保存失败");
        String chattelInfoJson = request.getParameter("chattelInfo");
        User user = HttpUtil.getCurrentUser(request);
        try {
            GuaChattel guaChattel = JacksonUtil.fromJson(chattelInfoJson, GuaChattel.class);
            if (guaChattel == null) {
                re.setMsg("传输数据内容为空，请重试");
                return re;
            }
            if(guaChattel.getId() == null){
                guaChattel.setCreater(user.getUserId());
                guaChattel.setCreateTime(DateUtil.getNowTimestamp());
            }
            guaChattel.setUpdater(user.getUserId());
            guaChattel.setUpdateTime(DateUtil.getNowTimestamp());
            guaChattel.setStatus(Constants.NORMAL);
            guaChattelService.saveOrUpdate(guaChattel);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 删除 动产担保信息
     * @param request
     * @param id
     * @return
     */
    @RequestMapping("removeChattelInfo.do")
    @ResponseBody
    public ResultDto removeChattelInfo(HttpServletRequest request, Integer id) {
        ResultDto re = returnFail("删除失败");
        if (id == null || id == 0) {
            re.setMsg("传输数据内容异常，请重试");
            return re;
        }
        User user = HttpUtil.getCurrentUser(request);
        try {
            GuaChattel guaChattel = guaChattelService.findByPrimaryKey(id);
            guaChattel.setStatus(Constants.DELETE);
            guaChattel.setUpdater(user.getUserId());
            guaChattel.setUpdateTime(DateUtil.getNowTimestamp());
            guaChattelService.saveOrUpdate(guaChattel);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 获取 应收账款 质押信息 列表
     * @param serNo
     * @return
     */
    @RequestMapping("getAccountsList.json")
    @ResponseBody
    public ResultDto getAccountsList(String serNo){
        ResultDto re = returnFail("获取数据失败！");
        if (StringUtil.isEmptyString(serNo)){
            re.setMsg("传输参数为空，请重试！");
            return re;
        }
        try {
            List<GuaAccounts> accountsList = guaAccountsService.selectByProperty("serNo", serNo);
            re.setRows(accountsList);
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("获取数据成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 保存或修改 应收账款 质押信息
     * @param request
     * @return
     */
    @RequestMapping(value = "saveAccountsInfo.json", method = RequestMethod.POST)
    @ResponseBody
    public ResultDto saveAccountsInfo(HttpServletRequest request){
        ResultDto re = returnFail("保存失败");
        String jsonData = request.getParameter("jsonArray");
        try {
            List<GuaAccounts> entityList = 
            		JacksonUtil.fromJson(jsonData, new TypeReference<List<GuaAccounts>>(){});
            if (CollectionUtils.isEmpty(entityList))
                return returnFail("传输数据内容为空，请重试");

            User user = HttpUtil.getCurrentUser(request);
            Timestamp nowTime = DateUtil.getNowTimestamp();
            for (GuaAccounts entity : entityList) {
                Integer id = entity.getId();
                if(id == null){
                    entity.setCreater(user.getUserId());
                    entity.setCreateTime(nowTime);
                }
                
                entity.setUpdater(user.getUserId());
                entity.setUpdateTime(nowTime);
                entity.setStatus(Constants.NORMAL);
                guaAccountsService.saveOrUpdate(entity);
            }

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
 
    }

    /**
     * 删除 应收账款 质押信息
     * @param request
     * @param id
     * @return
     */
    @RequestMapping("removeAccountsInfo.json")
    @ResponseBody
    public ResultDto removeAccountsInfo(HttpServletRequest request, Integer id) {
        ResultDto re = returnFail("删除失败");
        if (id == null || id == 0) 
            return returnFail("传输数据内容异常，请重试");
        
        User user = HttpUtil.getCurrentUser(request);
        try {
            GuaAccounts guaAccounts = guaAccountsService.findByPrimaryKey(id);
            guaAccounts.setStatus(Constants.DELETE);
            guaAccounts.setUpdater(user.getUserId());
            guaAccounts.setUpdateTime(DateUtil.getNowTimestamp());
            guaAccountsService.saveOrUpdate(guaAccounts);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 获取 存单质押信息 列表
     * @param serNo
     * @return
     */
    @RequestMapping("getDepositReceiptList.json")
    @ResponseBody
    public ResultDto getDepositReceiptList(String serNo){
        ResultDto re = returnFail("获取数据失败！");
        if (StringUtil.isEmptyString(serNo)){
            re.setMsg("传输参数为空，请重试！");
            return re;
        }
        try {
            List<GuaDepositReceipt> list = guaDepositReceiptService.selectByProperty("serNo", serNo);
            re.setRows(list);
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("获取数据成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 保存或修改 存单质押信息
     * @param request
     * @return
     */
    @RequestMapping("saveDepositReceipt.json")
    @ResponseBody
    public ResultDto saveDepositReceipt(HttpServletRequest request){
        ResultDto re = returnFail("保存失败");
        String jsonData = request.getParameter("jsonArray");
        User user = HttpUtil.getCurrentUser(request);
        try {
        	List<GuaDepositReceipt> entityList = 
            		JacksonUtil.fromJson(jsonData, new TypeReference<List<GuaDepositReceipt>>(){});
        	if(CollectionUtils.isEmpty(entityList))
        		return returnFail("传输数据内容为空，请重试");
        	
            for(GuaDepositReceipt entity : entityList){
            	if(entity.getId() == null){
            		entity.setCreater(user.getUserId());
            		entity.setCreateTime(DateUtil.getNowTimestamp());
                }
            	entity.setUpdater(user.getUserId());
            	entity.setUpdateTime(DateUtil.getNowTimestamp());
            	entity.setStatus(Constants.NORMAL);
                guaDepositReceiptService.saveOrUpdate(entity);
            }
            
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 删除 应收账款 质押信息
     * @param request
     * @param id
     * @return
     */
    @RequestMapping("removeDepositReceipt.json")
    @ResponseBody
    public ResultDto removeDepositReceipt(HttpServletRequest request, Integer id) {
        ResultDto re = returnFail("删除失败");
        if (id == null || id == 0) {
            re.setMsg("传输数据内容异常，请重试");
            return re;
        }
        User user = HttpUtil.getCurrentUser(request);
        try {
            GuaDepositReceipt depositReceipt = guaDepositReceiptService.findByPrimaryKey(id);
            depositReceipt.setStatus(Constants.DELETE);
            depositReceipt.setUpdater(user.getUserId());
            depositReceipt.setUpdateTime(DateUtil.getNowTimestamp());
            guaDepositReceiptService.saveOrUpdate(depositReceipt);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 保存或修改 担保分析说明
     * @param request
     * @param serNo
     * @return
     */
    @RequestMapping("saveAnalysisInfo.json")
    @ResponseBody
    public ResultDto saveAnalysisInfo(HttpServletRequest request, String serNo){
        ResultDto re = returnFail("保存失败");
        String housingWayInfo = request.getParameter("housingWayInfo");
		String estateInfo = request.getParameter("estateInfo");
		String chattelAmt = request.getParameter("chattelAmt");
		String superviseInfo = request.getParameter("superviseInfo");
		String chattelInfo = request.getParameter("chattelInfo");
		String accountsAmt = request.getParameter("accountsAmt");
		String accountsInfo = request.getParameter("accountsInfo");
		String depositReceiptAmt = request.getParameter("depositReceiptAmt");
		String depositReceiptInfo = request.getParameter("depositReceiptInfo");
		
        try {
        	User user = HttpUtil.getCurrentUser(request);
            GuaAnalysis entity = guaAnalysisService.selectSingleByProperty("serNo", serNo);
            if (entity == null) {
            	entity = new GuaAnalysis();
            	entity.setSerNo(serNo);
            	entity.setCreater(user.getUserId());
            	entity.setCreateTime(DateUtil.getNowTimestamp());
            }
            
            if(housingWayInfo != null) entity.setHousingWayInfo(housingWayInfo);
            if(estateInfo != null) entity.setEstateInfo(estateInfo);
            if(chattelAmt != null) entity.setChattelAmt(Double.parseDouble(chattelAmt));
            if(superviseInfo != null) entity.setSuperviseInfo(superviseInfo);
            if(chattelInfo != null) entity.setChattelInfo(chattelInfo);
            if(accountsAmt != null) entity.setAccountsAmt(Double.parseDouble(accountsAmt));
            if(accountsInfo != null) entity.setAccountsInfo(accountsInfo);
            if(depositReceiptAmt != null) entity.setDepositReceiptAmt(Double.parseDouble(depositReceiptAmt));
            if(depositReceiptInfo != null) entity.setDepositReceiptInfo(depositReceiptInfo);
            
            entity.setUpdater(user.getUserId());
            entity.setUpdateTime(DateUtil.getNowTimestamp());
            entity.setStatus(Constants.NORMAL);
            guaAnalysisService.saveOrUpdate(entity);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }
}
