package com.beawan.survey.guarantee.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 存单质押担保
 * @author zxh
 * @date 2020/7/16 20:01
 */
@Entity
@Table(name = "CM_GUA_DEPOSIT_RECEIPT", schema = "GDTCESYS")
public class GuaDepositReceipt extends BaseEntity {
    @Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="CM_GUA_DEPOSIT_RECEIPT")
   // @SequenceGenerator(name="CM_GUA_DEPOSIT_RECEIPT",allocationSize=1,initialValue=1, sequenceName="CM_GUA_DEPOSIT_RECEIPT_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;// 任务流水号
    @Column(name = "OWNER")
    private String owner;// 所有权人
    @Column(name = "AMOUNT")
    private double amount;// 金额
    @Column(name = "BANK")
    private String bank;// 存单所在行
    @Column(name = "PLEDGE_RATE")
    private double pledgeRate;// 质押率
    @Column(name = "START_END_DATE")
    private String startEndDate;// 起止日期
    @Column(name = "REMARK")
    private String remark;// 备注

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public double getPledgeRate() {
        return pledgeRate;
    }

    public void setPledgeRate(double pledgeRate) {
        this.pledgeRate = pledgeRate;
    }

    public String getStartEndDate() {
        return startEndDate;
    }

    public void setStartEndDate(String startEndDate) {
        this.startEndDate = startEndDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
