package com.beawan.survey.guarantee.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 动产担保
 * @author zxh
 * @date 2020/7/16 16:06
 */
@Entity
@Table(name = "CM_GUA_CHATTEL", schema = "GDTCESYS")
public class GuaChattel extends BaseEntity {

    @Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="CM_GUA_CHATTEL")
    //@SequenceGenerator(name="CM_GUA_CHATTEL",allocationSize=1,initialValue=1, sequenceName="CM_GUA_CHATTEL_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;// 任务流水号
    @Column(name = "CHATTEL_NAME")
    private String chattelName; // 动产名称
    @Column(name = "SPECIFICATION")
    private String specification;// 规格
    @Column(name = "NUMBER")
    private String number;// 数量
    @Column(name = "OWNERSHIP")
    private String ownership;// 权属/所有权
    @Column(name = "ORIGINAL_VALUE")
    private double originalValue;// 原值
    @Column(name = "NET_VALUE")
    private double netValue;// 净值
    @Column(name = "ESTIMATED_VALUE")
    private double estimatedValue;// 评估值
    @Column(name = "EVALUATION_AGENCY")
    private String evaluationAgency;// 评估机构
    @Column(name = "STORAGE_LOCATION")
    private String storageLocation;// 存放地点
    @Column(name = "IS_HANDLE_INSURANCE")
    private String isHandleInsurance;// 是否拟办保险

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getChattelName() {
        return chattelName;
    }

    public void setChattelName(String chattelName) {
        this.chattelName = chattelName;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getOwnership() {
        return ownership;
    }

    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    public double getOriginalValue() {
        return originalValue;
    }

    public void setOriginalValue(double originalValue) {
        this.originalValue = originalValue;
    }

    public double getNetValue() {
        return netValue;
    }

    public void setNetValue(double netValue) {
        this.netValue = netValue;
    }

    public double getEstimatedValue() {
        return estimatedValue;
    }

    public void setEstimatedValue(double estimatedValue) {
        this.estimatedValue = estimatedValue;
    }

    public String getEvaluationAgency() {
        return evaluationAgency;
    }

    public void setEvaluationAgency(String evaluationAgency) {
        this.evaluationAgency = evaluationAgency;
    }

    public String getStorageLocation() {
        return storageLocation;
    }

    public void setStorageLocation(String storageLocation) {
        this.storageLocation = storageLocation;
    }

    public String getIsHandleInsurance() {
        return isHandleInsurance;
    }

    public void setIsHandleInsurance(String isHandleInsurance) {
        this.isHandleInsurance = isHandleInsurance;
    }
}
