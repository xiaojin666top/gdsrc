package com.beawan.survey.guarantee.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 抵、质押物担保 分析说明
 * @author zxh
 * @date 2020/7/16 20:52
 */
@Entity
@Table(name = "CM_GUA_ANALYSIS", schema = "GDTCESYS")
public class GuaAnalysis extends BaseEntity {

    @Id
  //  @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="CM_GUA_ANALYSIS")
   // @SequenceGenerator(name="CM_GUA_ANALYSIS",allocationSize=1,initialValue=1, sequenceName="CM_GUA_ANALYSIS_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;// 任务流水号
    @Column(name = "HOUSE_WAY_INFO")
    private String housingWayInfo;// 第二住房措施说明
    @Column(name = "ESTATE_INFO")
    private String estateInfo;// 房地产抵押说明
    @Column(name = "CHATTEL_AMT")
    private double chattelAmt;// 动产 拟抵押金额
    @Column(name = "SUPERVISE_INFO")
    private String superviseInfo;// 第三方监管情况
    @Column(name = "CHATTEL_INFO")
    private String chattelInfo;// 动产抵押说明
    @Column(name = "ACCOUNTS_AMT")
    private double accountsAmt;// 应收账款 拟质押金额
    @Column(name = "ACCOUNTS_INFO")
    private String accountsInfo;// 应收账款质押说明
    @Column(name = "DEPOSIT_RECEIPT_AMT")
    private double depositReceiptAmt;// 存单 拟质押金额
    @Column(name = "DEPOSIT_RECEIPT_INFO")
    private String depositReceiptInfo;// 存单质押说明

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getHousingWayInfo() {
        return housingWayInfo;
    }

    public void setHousingWayInfo(String housingWayInfo) {
        this.housingWayInfo = housingWayInfo;
    }

    public String getEstateInfo() {
        return estateInfo;
    }

    public void setEstateInfo(String estateInfo) {
        this.estateInfo = estateInfo;
    }

    public String getSuperviseInfo() {
        return superviseInfo;
    }

    public void setSuperviseInfo(String superviseInfo) {
        this.superviseInfo = superviseInfo;
    }

    public String getChattelInfo() {
        return chattelInfo;
    }

    public void setChattelInfo(String chattelInfo) {
        this.chattelInfo = chattelInfo;
    }

    public String getAccountsInfo() {
        return accountsInfo;
    }

    public void setAccountsInfo(String accountsInfo) {
        this.accountsInfo = accountsInfo;
    }

    public String getDepositReceiptInfo() {
        return depositReceiptInfo;
    }

    public void setDepositReceiptInfo(String depositReceiptInfo) {
        this.depositReceiptInfo = depositReceiptInfo;
    }

    public double getChattelAmt() {
        return chattelAmt;
    }

    public void setChattelAmt(double chattelAmt) {
        this.chattelAmt = chattelAmt;
    }

    public double getAccountsAmt() {
        return accountsAmt;
    }

    public void setAccountsAmt(double accountsAmt) {
        this.accountsAmt = accountsAmt;
    }

    public double getDepositReceiptAmt() {
        return depositReceiptAmt;
    }

    public void setDepositReceiptAmt(double depositReceiptAmt) {
        this.depositReceiptAmt = depositReceiptAmt;
    }
}
