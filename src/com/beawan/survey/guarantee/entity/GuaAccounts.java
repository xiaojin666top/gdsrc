package com.beawan.survey.guarantee.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 应收账款 质押担保
 * @author zxh
 * @date 2020/7/16 18:17
 */
@Entity
@Table(name = "CM_GUA_ACOUNTS", schema = "GDTCESYS")
public class GuaAccounts extends BaseEntity {

    @Id
  //  @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="CM_GUA_CHATTEL")
   // @SequenceGenerator(name="CM_GUA_CHATTEL",allocationSize=1,initialValue=1, sequenceName="CM_GUA_CHATTEL_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;// 任务流水号
    @Column(name = "CUST_NAME")
    private String customerName;// 客户名称
    @Column(name = "AMOUNT")
    private double amount;// 金额
    @Column(name = "ACCOUNTS_AGE")
    private String accountsAge;// 账龄
    @Column(name = "NET_AMT")
    private double netAmount;// 净额
    @Column(name = "REMARK")
    private String remark;// 备注

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getAccountsAge() {
        return accountsAge;
    }

    public void setAccountsAge(String accountsAge) {
        this.accountsAge = accountsAge;
    }

    public double getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(double netAmount) {
        this.netAmount = netAmount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
