package com.beawan.survey.guarantee.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 担保企业财务信息
 * 与 GuaCompany ID、任务流水号serNo 关联
 * @author zxh
 * @date 2020/7/13 10:39
 */
@Entity
@Table(name = "CM_GUA_FINANCE_INFO", schema = "GDTCESYS")
public class CompFinanceInfo extends BaseEntity {

    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="CM_GUA_FINANCE_INFO")
  //  @SequenceGenerator(name="CM_GUA_FINANCE_INFO",allocationSize=1,initialValue=1, sequenceName="CM_GUA_FINANCE_INFO_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;// 任务流水号
    @Column(name = "GUA_COMP_ID")
    private Integer guaCompanyId;// 担保企业
    @Column(name = "YEARS")
    private String years; // 年份
    @Column(name = "ASSETS_SUM")
    private Double assetsSum;// 总资产
    @Column(name = "DEBT_SUM")
    private Double debtSum;// 总负债
    @Column(name = "NET_ASSETS")
    private Double netAssets;// 净资产
    @Column(name = "SALES_REVENUE")
    private Double salesRevenue;// 销售收入
    @Column(name = "NET_PROFIT")
    private Double netProfit;// 净利润
    @Column(name = "ASSETS_DEBT_RATE")
    private Double assetDebtRate; // 资产负债率

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public Integer getGuaCompanyId() {
        return guaCompanyId;
    }

    public void setGuaCompanyId(Integer guaCompanyId) {
        this.guaCompanyId = guaCompanyId;
    }

    public String getYears() {
        return years;
    }

    public void setYears(String years) {
        this.years = years;
    }

    public Double getAssetsSum() {
        return assetsSum;
    }

    public void setAssetsSum(Double assetsSum) {
        this.assetsSum = assetsSum;
    }

    public Double getDebtSum() {
        return debtSum;
    }

    public void setDebtSum(Double debtSum) {
        this.debtSum = debtSum;
    }

    public Double getNetAssets() {
        return netAssets;
    }

    public void setNetAssets(Double netAssets) {
        this.netAssets = netAssets;
    }

    public Double getSalesRevenue() {
        return salesRevenue;
    }

    public void setSalesRevenue(Double salesRevenue) {
        this.salesRevenue = salesRevenue;
    }

    public Double getNetProfit() {
        return netProfit;
    }

    public void setNetProfit(Double netProfit) {
        this.netProfit = netProfit;
    }

    public Double getAssetDebtRate() {
        return assetDebtRate;
    }

    public void setAssetDebtRate(Double assetDebtRate) {
        this.assetDebtRate = assetDebtRate;
    }
}
