package com.beawan.survey.guarantee.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 第三方担保企业信息 与任务流水号相关联
 * @author zxh
 * @date 2020/7/11 15:52
 */
@Entity
@Table(name = "CM_GUA_THIRD_PARTY", schema = "GDTCESYS")
public class GuaCompany extends BaseEntity {

    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="CM_GUA_THIRD_PARTY")
   // @SequenceGenerator(name="CM_GUA_THIRD_PARTY",allocationSize=1,initialValue=1, sequenceName="CM_GUA_THIRD_PARTY_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;// 任务流水号
    @Column(name = "COMPANY_NAME")
    private String companyName;// 担保企业
    @Column(name = "LOAN_CARD_INFO")
    private String loanCardInfo;// 贷款卡年检情况
    @Column(name = "GUA_AMOUNT")
    private Double guaranteeAmt;// 拟担保金额
    @Column(name = "GUA_WAY")
    private String guaranteeWay;// 担保方式
    @Column(name = "FOUND_DATE")
    private String foundDate;// 成立日期
    @Column(name = "DUE_DATE")
    private String dueDate;// 到期日期
    @Column(name = "REG_CAPITAL")
    private Double regCapital; // 注册资本
    @Column(name = "REAL_CAPITAL")
    private Double realCapital;// 实收资本
    @Column(name = "LEGAL_PERSON")
    private String legalPerson;// 法定代表人
    @Column(name = "CONTROL_PERSON")
    private String controlPerson;// 实际控制人
    @Column(name = "CONTROL_PERSON_INFO")
    private String controlPersonInfo;// 实际控制人情况介绍
    @Column(name = "ADDRESS")
    private String address;// 经营或注册地址
    @Column(name = "MAIN_BUSINESS")
    private String mainBusiness;// 主营业务
    @Column(name = "EQUITY_INFO")
    private String equityInfo;// 股权结构
    @Column(name = "RUN_INFO")
    private String runInfo;// 经营状况
    @Column(name = "FINANCE_GUA_INFO")
    private String financeGuaInfo;// 融资及对外担保情况
    @Column(name = "CREDIT_RECORD")
    private String creditRecord; // 征信记录：有无不良记录
    @Column(name = "BAD_EFFECT")
    private String badEffect;// 不良征信记录影响程度
    @Column(name = "RELATION")
    private String relation; // 与借款人关系
    @Column(name = "IS_OFFER_GUA")
    private String isOfferGua; // 是否提供连带责任保证

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getLoanCardInfo() {
        return loanCardInfo;
    }

    public void setLoanCardInfo(String loanCardInfo) {
        this.loanCardInfo = loanCardInfo;
    }

    public Double getGuaranteeAmt() {
        return guaranteeAmt;
    }

    public void setGuaranteeAmt(Double guaranteeAmt) {
        this.guaranteeAmt = guaranteeAmt;
    }

    public String getGuaranteeWay() {
        return guaranteeWay;
    }

    public void setGuaranteeWay(String guaranteeWay) {
        this.guaranteeWay = guaranteeWay;
    }

    public String getFoundDate() {
        return foundDate;
    }

    public void setFoundDate(String foundDate) {
        this.foundDate = foundDate;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public Double getRegCapital() {
        return regCapital;
    }

    public void setRegCapital(Double regCapital) {
        this.regCapital = regCapital;
    }

    public Double getRealCapital() {
        return realCapital;
    }

    public void setRealCapital(Double realCapital) {
        this.realCapital = realCapital;
    }

    public String getLegalPerson() {
        return legalPerson;
    }

    public void setLegalPerson(String legalPerson) {
        this.legalPerson = legalPerson;
    }

    public String getControlPerson() {
        return controlPerson;
    }

    public void setControlPerson(String controlPerson) {
        this.controlPerson = controlPerson;
    }

    public String getControlPersonInfo() {
        return controlPersonInfo;
    }

    public void setControlPersonInfo(String controlPersonInfo) {
        this.controlPersonInfo = controlPersonInfo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMainBusiness() {
        return mainBusiness;
    }

    public void setMainBusiness(String mainBusiness) {
        this.mainBusiness = mainBusiness;
    }

    public String getEquityInfo() {
        return equityInfo;
    }

    public void setEquityInfo(String equityInfo) {
        this.equityInfo = equityInfo;
    }

    public String getRunInfo() {
        return runInfo;
    }

    public void setRunInfo(String runInfo) {
        this.runInfo = runInfo;
    }

    public String getFinanceGuaInfo() {
        return financeGuaInfo;
    }

    public void setFinanceGuaInfo(String financeGuaInfo) {
        this.financeGuaInfo = financeGuaInfo;
    }

    public String getCreditRecord() {
        return creditRecord;
    }

    public void setCreditRecord(String creditRecord) {
        this.creditRecord = creditRecord;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getIsOfferGua() {
        return isOfferGua;
    }

    public void setIsOfferGua(String isOfferGua) {
        this.isOfferGua = isOfferGua;
    }

    public String getBadEffect() {
        return badEffect;
    }

    public void setBadEffect(String badEffect) {
        this.badEffect = badEffect;
    }
}
