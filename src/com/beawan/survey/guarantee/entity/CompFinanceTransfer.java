package com.beawan.survey.guarantee.entity;

public class CompFinanceTransfer {
	
	private Double value;
	private String rowSubject;
	private String date;
	
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public String getRowSubject() {
		return rowSubject;
	}
	public void setRowSubject(String rowSubject) {
		this.rowSubject = rowSubject;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
}
