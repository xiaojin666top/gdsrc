package com.beawan.survey.guarantee.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 担保自然人信息 与任务流水号相关联
 * @author zxh
 * @date 2020/7/14 22:14
 */
@Entity
@Table(name = "CM_GUA_NATURAL_PERSON", schema = "GDTCESYS")
public class GuaPerson extends BaseEntity {
        @Id
        //@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="CM_GUA_NATURAL_PERSON")
      //  @SequenceGenerator(name="CM_GUA_NATURAL_PERSON",allocationSize=1,initialValue=1, sequenceName="CM_GUA_NATURAL_PERSON_SEQ")
        private Integer id;
        @Column(name = "SER_NO")
        private String serNo;// 任务流水号
        @Column(name = "NAME")
        private String name; // 担保人姓名
        @Column(name = "SEX")
        private String sex; // 性别
        @Column(name = "GUA_AMOUNT")
        private double guaranteeAmt;// 拟担保金额
        @Column(name = "GUA_WAY")
        private String guaranteeWay;// 担保方式
        @Column(name = "CERT_CODE")
        private String certCode; // 证件号码
        @Column(name = "MARITAL_STATUS")
        private String maritalStatus; // 婚姻状况
        @Column(name = "ADDRESS")
        private String address; // 户籍地址及现居住地
        @Column(name = "WORK_UNIT_POST")
        private String workUnitPost;// 工作单位及职务
        @Column(name = "MOBILE_PHONE")
        private String mobilePhone; // 联系电话
        @Column(name = "RELATION")
        private String relation;// 担保人与借款人关系
        @Column(name = "LAST_YEAR_INCOME")
        private double lastYearIncome;// 去年年收入
        @Column(name = "GUA_INFO")
        private String guarantorInfo;// 担保人情况说明
        @Column(name = "YEAR_MONTH")
        private String yearMonth;// 调查日期
        @Column(name = "ASSETS_SUM")
        private double assetsSum;// 总资产
        @Column(name = "NET_ASSETS")
        private double netAssets;// 净资产
        @Column(name = "FIXED_ASSETS")
        private double fixedAssets;// 固定资产
        @Column(name = "FLOW_ASSETS")
        private double flowAssets;// 流动资产
        @Column(name = "OTHER_ASSETS")
        private double otherAssets;// 其他资产
        @Column(name = "DEBT_SUM")
        private double debtSum;// 总负债
        @Column(name = "BANK_DEBT")
        private double bankDebt;// 银行负债
        @Column(name = "OTHER_DEBT")
        private double otherDebt;// 其他负债
        @Column(name = "ASSETS_INCOME_INFO")
        private String assetsIncomeInfo;// 担保人有效资产及收入情况
        @Column(name = "OUT_GUA_INFO")
        private String outGuaInfo;// 对外担保情况
        @Column(name = "CREDIT_RECORD")
        private String creditRecord;// 征信记录
        @Column(name = "BAD_EFFECT")
        private String badEffect;// 不良记录影响
        @Column(name = "STOCKHOLDER_INFO")
        private String stockholderInfo;// 自然人股东说明
        @Column(name = "REMARK")
        private String remark;// 分析评价

        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }

        public String getSerNo() {
                return serNo;
        }

        public void setSerNo(String serNo) {
                this.serNo = serNo;
        }

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public String getSex() {
                return sex;
        }

        public void setSex(String sex) {
                this.sex = sex;
        }

        public double getGuaranteeAmt() {
                return guaranteeAmt;
        }

        public void setGuaranteeAmt(double guaranteeAmt) {
                this.guaranteeAmt = guaranteeAmt;
        }

        public String getGuaranteeWay() {
                return guaranteeWay;
        }

        public void setGuaranteeWay(String guaranteeWay) {
                this.guaranteeWay = guaranteeWay;
        }

        public String getCertCode() {
                return certCode;
        }

        public void setCertCode(String certCode) {
                this.certCode = certCode;
        }

        public String getMaritalStatus() {
                return maritalStatus;
        }

        public void setMaritalStatus(String maritalStatus) {
                this.maritalStatus = maritalStatus;
        }

        public String getAddress() {
                return address;
        }

        public void setAddress(String address) {
                this.address = address;
        }

        public String getWorkUnitPost() {
                return workUnitPost;
        }

        public void setWorkUnitPost(String workUnitPost) {
                this.workUnitPost = workUnitPost;
        }

        public String getMobilePhone() {
                return mobilePhone;
        }

        public void setMobilePhone(String mobilePhone) {
                this.mobilePhone = mobilePhone;
        }

        public String getRelation() {
                return relation;
        }

        public void setRelation(String relation) {
                this.relation = relation;
        }

        public double getLastYearIncome() {
                return lastYearIncome;
        }

        public void setLastYearIncome(double lastYearIncome) {
                this.lastYearIncome = lastYearIncome;
        }

        public String getGuarantorInfo() {
                return guarantorInfo;
        }

        public void setGuarantorInfo(String guarantorInfo) {
                this.guarantorInfo = guarantorInfo;
        }

        public String getYearMonth() {
                return yearMonth;
        }

        public void setYearMonth(String yearMonth) {
                this.yearMonth = yearMonth;
        }

        public double getAssetsSum() {
                return assetsSum;
        }

        public void setAssetsSum(double assetsSum) {
                this.assetsSum = assetsSum;
        }

        public double getNetAssets() {
                return netAssets;
        }

        public void setNetAssets(double netAssets) {
                this.netAssets = netAssets;
        }

        public double getFixedAssets() {
                return fixedAssets;
        }

        public void setFixedAssets(double fixedAssets) {
                this.fixedAssets = fixedAssets;
        }

        public double getFlowAssets() {
                return flowAssets;
        }

        public void setFlowAssets(double flowAssets) {
                this.flowAssets = flowAssets;
        }

        public double getOtherAssets() {
                return otherAssets;
        }

        public void setOtherAssets(double otherAssets) {
                this.otherAssets = otherAssets;
        }

        public double getDebtSum() {
                return debtSum;
        }

        public void setDebtSum(double debtSum) {
                this.debtSum = debtSum;
        }

        public double getBankDebt() {
                return bankDebt;
        }

        public void setBankDebt(double bankDebt) {
                this.bankDebt = bankDebt;
        }

        public double getOtherDebt() {
                return otherDebt;
        }

        public void setOtherDebt(double otherDebt) {
                this.otherDebt = otherDebt;
        }

        public String getAssetsIncomeInfo() {
                return assetsIncomeInfo;
        }

        public void setAssetsIncomeInfo(String assetsIncomeInfo) {
                this.assetsIncomeInfo = assetsIncomeInfo;
        }

        public String getOutGuaInfo() {
                return outGuaInfo;
        }

        public void setOutGuaInfo(String outGuaInfo) {
                this.outGuaInfo = outGuaInfo;
        }

        public String getCreditRecord() {
                return creditRecord;
        }

        public void setCreditRecord(String creditRecord) {
                this.creditRecord = creditRecord;
        }

        public String getBadEffect() {
                return badEffect;
        }

        public void setBadEffect(String badEffect) {
                this.badEffect = badEffect;
        }

        public String getStockholderInfo() {
                return stockholderInfo;
        }

        public void setStockholderInfo(String stockholderInfo) {
                this.stockholderInfo = stockholderInfo;
        }

        public String getRemark() {
                return remark;
        }

        public void setRemark(String remark) {
                this.remark = remark;
        }
}
