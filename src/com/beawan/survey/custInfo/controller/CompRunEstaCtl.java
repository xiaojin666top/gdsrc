package com.beawan.survey.custInfo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.base.service.ISysDicSV;
import com.beawan.base.service.ISysTreeDicSV;
import com.beawan.common.SysConstants.ExceptionConstant;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.library.bean.InduSituation;
import com.beawan.library.service.IIndustrySV;
import com.beawan.library.service.IIndustrySituationSV;
import com.beawan.survey.custInfo.bean.CompRunEsta;
import com.beawan.survey.custInfo.bean.CompRunEstaDevedProj;
import com.beawan.survey.custInfo.bean.CompRunEstaDevingProj;
import com.beawan.survey.custInfo.bean.CompRunEstaLand;
import com.beawan.survey.custInfo.service.ICompRunSV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/survey/cust" })
public class CompRunEstaCtl {
	
	@Resource
	protected ITaskSV taskSV;

	@Resource
	private ICompRunSV compRunSV;

	@Resource
	protected IIndustrySV industrySV;

	@Resource
	private ISysDicSV sysDicSV;
	
	@Resource
	private ISysTreeDicSV sysTreeDicSV;

	@Resource
	private IIndustrySituationSV indusSituationSV;
	
	/**
	 * @Description (房地产开发贷款-企业经营状况信息页面跳转)
	 * @param request
	 * @param response
	 * @param taskId 任务ID
	 * @return
	 */
	@RequestMapping("compRunEsta.do")
	public String compRunEsta(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo) {
		
		Task task = this.taskSV.getTaskById(taskId);
		try {
			if (StringUtil.isEmptyString(customerNo)) {
				customerNo = task.getCustomerNo();
			}
			String indusCode=task.getIndustry();
	
			CompRunEsta compRunEsta = compRunSV.queryRunEstaByTIdAndNo(taskId, customerNo);
			
			InduSituation induSituation=indusSituationSV.findInduSituationByName(indusCode);
			
			String situation="";
			
			if(null!=compRunEsta&&null!=compRunEsta.getIndustryAnalysis()&&!"".equals(compRunEsta.getIndustryAnalysis()))
				situation=compRunEsta.getIndustryAnalysis();
			else if(null!=induSituation) {
				situation=induSituation.getSituation();
			}
				
			request.setAttribute("newsituation", situation);
			request.setAttribute("compRunEsta", compRunEsta);
			request.setAttribute("task", task);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/survey/estate/compRunInfo";
	}
	
	/**
	 * @Description (房地产开发贷款-保存企业经营基本信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveCompRunEsta.do")
	@ResponseBody
	public String saveCompRunEsta(HttpServletRequest request, HttpServletResponse response,
			String jsonData) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			CompRunEsta compRunEsta = JacksonUtil.fromJson(jsonData, CompRunEsta.class);
			compRunSV.saveRunEsta(compRunEsta);
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (房地产开发贷款-获取企业项目开发信息列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getRunEstaDevProj.do")
	@ResponseBody
	public String getRunEstaDevProj(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo, String type) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<?> dataGrid = null;
		try {
			if("deved".equals(type))
				dataGrid = compRunSV.queryRunEstaDevedProjByTIdAndNo(taskId, customerNo);
			else if("deving".equals(type))
				dataGrid = compRunSV.queryRunEstaDevingProjByTIdAndNo(taskId, customerNo);
			else if("land".equals(type))
				dataGrid = compRunSV.queryRunEstaLandByTIdAndNo(taskId, customerNo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jRequest.serialize(dataGrid, true);
	}
	
	/**
	 * @Description (房地产开发贷款-保存企业项目开发信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveRunEstaDevProj.do")
	@ResponseBody
	public String saveRunEstaDevProj(HttpServletRequest request, HttpServletResponse response,
			String jsonData, String type) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			
			if("deved".equals(type))
				compRunSV.saveRunEstaDevedProj(JacksonUtil.fromJson(jsonData,
						CompRunEstaDevedProj.class));
			else if("deving".equals(type))
				compRunSV.saveRunEstaDevingProj(JacksonUtil.fromJson(jsonData,
						CompRunEstaDevingProj.class));
			else if("land".equals(type))
				compRunSV.saveRunEstaLand(JacksonUtil.fromJson(jsonData,
						CompRunEstaLand.class));
			
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (房地产开发贷款-删除企业项目开发信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("deleteRunEstaDevProj.do")
	@ResponseBody
	public String deleteRunEstaDevProj(HttpServletRequest request, HttpServletResponse response,
			long id, String type) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			
			if("deved".equals(type)){
				CompRunEstaDevedProj proj = compRunSV.queryRunEstaDevedProjById(id);
				compRunSV.deleteRunEstaDevedProj(proj);
			}else if("deving".equals(type)){
				CompRunEstaDevingProj proj = compRunSV.queryRunEstaDevingProjById(id);
				compRunSV.deleteRunEstaDevingProj(proj);
			}else if("land".equals(type)){
				CompRunEstaLand land = compRunSV.queryRunEstaLandById(id);
				compRunSV.deleteRunEstaLand(land);
			}
			
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
}
