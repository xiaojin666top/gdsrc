package com.beawan.survey.custInfo.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.base.entity.QuickAnaly;
import com.beawan.base.entity.User;
import com.beawan.base.service.ISysDicSV;
import com.beawan.base.service.QuickAnalyService;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.SysConstants.ExceptionConstant;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.Pagination;
import com.beawan.core.ResultDto;
import com.beawan.qcc.dto.QccJudgDtlDto;
import com.beawan.qcc.service.SynchronizationQCCService;
import com.beawan.survey.custInfo.bean.CompFinancing;
import com.beawan.survey.custInfo.bean.CompFinancingBank;
import com.beawan.survey.custInfo.bean.CompFinancingExtGuara;
import com.beawan.survey.custInfo.dto.CompJudgmentDto;
import com.beawan.survey.custInfo.service.CompJudgmentService;
import com.beawan.survey.custInfo.service.ICompFinancingSV;
import com.beawan.survey.loanInfo.service.ICusLawSV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.platform.util.DateUtil;
import com.platform.util.GsonUtil;
import com.platform.util.HttpUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;

/**
 * @ClassName CustFinancingCtl
 * @Description TODO(融资信息controller)
 * @author czc
 * @Date 2017年6月23日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/survey/cust" })
public class CompFinancingCtl extends BaseController {
	
	@Resource
	protected ITaskSV taskSV;
	@Resource
	private ICompFinancingSV compFinancingSV;
	@Resource
	private ISysDicSV sysDicSV;
	@Resource
	private ICusLawSV cusLawSV;
	@Resource
	private CompJudgmentService compJudgmentService;
	@Resource
	private SynchronizationQCCService synchronizationQCCService;
	@Resource
	private ICusBaseSV cusBaseSV;
	@Resource
	private QuickAnalyService quickAnalyService;
	
	/**
	 * 跳转 银行融资信息页面
	 * @param customerNo 客户号
	 * @param serNo 任务流水号
	 * @return
	 */
	@RequestMapping("compFinancingBank.do")
	public ModelAndView compFinancingBank(String serNo, String customerNo) {
		ModelAndView mav = new ModelAndView("views/survey/common/compFinancingBank");
		try {
			Task task = taskSV.getTaskBySerNo(serNo);
			CompFinancing financing = compFinancingSV.findCompFinancingByTIdAndNo(task.getId(), customerNo);
			if(financing == null ){
				financing = new CompFinancing();
				financing.setTaskId(task.getId());
				financing.setCustomerNo(customerNo);
			}
			
			//加载字典数据
			String[] optTypes = {
					SysConstants.BsDicConstant.STD_SY_JB00029,
					SysConstants.BsDicConstant.STD_SY_JB00015,
					SysConstants.BsDicConstant.STD_SY_ODS0400,
					SysConstants.BsDicConstant.STD_SY_GUAR_TYPE,
					SysConstants.BsDicConstant.STD_ZB_FIVE_SORT
			};
			String[] items = new String[]{
					SysConstants.RG_PRE_ITEM.FINANCE_INFO	
			};
			
			Map<String, List<QuickAnaly>> analyMap = quickAnalyService.queryAnalyByItem(items);
			mav.addObject("analyMap", GsonUtil.GsonString(analyMap));
			mav.addObject("taskId", task.getId());
			mav.addObject("customerNo", customerNo);
			mav.addObject("financing", financing);
			mav.addObject("dicData", sysDicSV.queryMapListByTypes(optTypes));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
	
	/**
	 * @Description (获取企业银行融资记录信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getFinancingBank.json")
	@ResponseBody
	public ResultDto getFinancingBank(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo, String flag) {
		ResultDto re = returnFail("获取企业融资数据失败");
		
		try {
			List<CompFinancingBank> dataGrid = compFinancingSV.findCompFinaBankByTIdAndNo(taskId, customerNo, flag);
			
			re.setRows(dataGrid);
			re.setMsg("获取融资数据成功");
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return re;
	}

	/**
	 * 保存/修改 企业银行融资记录
	 * @param request
	 * @return
	 */
	@RequestMapping("saveFinancingBank.json")
	@ResponseBody
	public ResultDto saveFinancingBank(HttpServletRequest request) {
		ResultDto re = returnFail("保存失败");
		String jsonData = request.getParameter("jsonArray");
		try {
			Gson gson = GsonUtil.getGson();
			List<CompFinancingBank> entityList = gson.fromJson(jsonData,
					new TypeToken<ArrayList<CompFinancingBank>>() {}.getType());
			if (entityList == null || entityList.size() <= 0) {
				re.setMsg("传输数据内容为空，请重试");
				return re;
			}

			User user = HttpUtil.getCurrentUser(request);
			Timestamp nowTime = DateUtil.getNowTimestamp();
			for (CompFinancingBank entity : entityList) {
				Long id = entity.getId();
				if(id == null){
					entity.setCreater(user.getUserId());
					entity.setCreateTime(nowTime);
				}
				entity.setUpdater(user.getUserId());
				entity.setUpdateTime(nowTime);
				entity.setStatus(Constants.NORMAL);
				compFinancingSV.saveCompFinaBank(entity);
			}

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (DataAccessException e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除 企业银行融资记录
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("deleteFinancingBank.json")
	@ResponseBody
	public ResultDto deleteFinancingBank(HttpServletRequest request, Long id) {
		ResultDto re = returnFail("删除失败");
		if (id == null || id == 0) {
			re.setMsg("传输数据内容异常，请重试");
			return re;
		}
		User user = HttpUtil.getCurrentUser(request);
		try {
			CompFinancingBank entity = compFinancingSV.findCompFinaBankById(id);
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			entity.setStatus(Constants.DELETE);
			compFinancingSV.saveCompFinaBank(entity);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除成功");
		} catch (DataAccessException e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 合作情况、融资情况说明
	 * @param taskId 任务主键
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping(value = "saveFinancingAnalysis.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto saveFinancingAnalysis(HttpServletRequest request, long taskId, String customerNo){
		ResultDto re = returnFail("保存失败");
		String mbFinancing = request.getParameter("mbFinancing");
		String obFinancing = request.getParameter("obFinancing");
		String externalGua = request.getParameter("externalGua");
		String lawXPResult = request.getParameter("lawXPResult");
		String otherContLiability = request.getParameter("otherContLiability");
		
		try {
			CompFinancing entity = compFinancingSV.findCompFinancingByTIdAndNo(taskId, customerNo);
			User user = HttpUtil.getCurrentUser(request);
			if (entity == null){
				entity = new CompFinancing();
				entity.setTaskId(taskId);
				entity.setCustomerNo(customerNo);
				entity.setCreater(user.getUserId());
				entity.setCreateTime(DateUtil.getNowTimestamp());
			}
			if(mbFinancing != null) entity.setMbFinancing(mbFinancing);
			if(obFinancing != null) entity.setObFinancing(obFinancing);
			if(externalGua != null) entity.setExternalGua(externalGua);
			if(lawXPResult != null) entity.setLawXPResult(lawXPResult);
			if(otherContLiability != null) entity.setExternalGua(otherContLiability);
			
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			compFinancingSV.saveCompFinancing(entity);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}


	/**
	 * @Description (企业对外担保信息页面跳转)
	 * @param request
	 * @param serNo 任务流水号n
	 * @return
	 */
	@RequestMapping("compFinancingExtGuara.do")
	public ModelAndView compFinancingExtGuara(HttpServletRequest request, String serNo, String customerNo) {
		ModelAndView mav = new ModelAndView("views/survey/common/compFinancingExtGuara");
		try {
			Task task = this.taskSV.getTaskBySerNo(serNo);
			System.out.println(task.getId());
			CompFinancing financing = compFinancingSV.findCompFinancingByTIdAndNo(task.getId(), customerNo);
			if(financing == null ){
				financing = new CompFinancing();
				financing.setTaskId(task.getId());
				financing.setCustomerNo(customerNo);
			}
			
			//加载字典数据
			String[] optTypes = {
					SysConstants.BsDicConstant.STD_ZB_FIVE_SORT,
					SysConstants.BsDicConstant.STD_SY_GUAR_TYPE,
					SysConstants.BsDicConstant.STD_COM_EXT_GUARA_REL,
					SysConstants.BsDicConstant.STD_ZB_FIVE_SORT,
			};
			String[] items = new String[]{
					SysConstants.RG_PRE_ITEM.GUARANTEE_INFO	
			};
			
			Map<String, List<QuickAnaly>> analyMap = quickAnalyService.queryAnalyByItem(items);
			mav.addObject("analyMap", GsonUtil.GsonString(analyMap));
			mav.addObject("dicData", sysDicSV.queryMapListByTypes(optTypes));
			mav.addObject("taskId", task.getId());
			mav.addObject("customerNo", customerNo);
			mav.addObject("financing", financing);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}

	/**
	 * 获取 对外担保列表
	 * @param taskId 任务主键
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping(value = "getExternalGuaList.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto getExternalGuaList(long taskId, String customerNo){
		ResultDto re = returnFail("获取对外担保列表失败");
		if(StringUtil.isEmptyString(customerNo)) return returnFail("客户号为空");
		try {
			List<CompFinancingExtGuara> entityList =
					compFinancingSV.findCompFinaExtGuaraByTIdAndNo(taskId, customerNo);
			re.setRows(entityList);
			re.setMsg("获取数据成功！");
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 保存/修改 对外担保记录
	 * @param request
	 * @return
	 */
	@RequestMapping("saveExternalGua.json")
	@ResponseBody
	public ResultDto saveExternalGua(HttpServletRequest request, String jsonArray) {
		ResultDto re = returnFail("保存失败");
		try {
			List<CompFinancingExtGuara> entityList =
					JacksonUtil.fromJson(jsonArray, new TypeReference<List<CompFinancingExtGuara>>() {});
			if (CollectionUtils.isEmpty(entityList))
				return returnFail("传输数据内容为空，请重试！");

			User user = HttpUtil.getCurrentUser(request);
			Timestamp nowTime = DateUtil.getNowTimestamp();
			for (CompFinancingExtGuara entity : entityList) {
				Long id = entity.getId();
				if(id == null){
					entity.setCreater(user.getUserId());
					entity.setCreateTime(nowTime);
				}
				entity.setUpdater(user.getUserId());
				entity.setUpdateTime(nowTime);
				entity.setStatus(Constants.NORMAL);
				compFinancingSV.saveCompFinaExtGuara(entity);
			}

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除 对外担保记录
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("removeExternalGua.json")
	@ResponseBody
	public ResultDto removeExternalGua(HttpServletRequest request, Long id) {
		ResultDto re = returnFail("删除失败");
		if (id == null || id == 0) {
			re.setMsg("传输数据内容异常，请重试");
			return re;
		}
		User user = HttpUtil.getCurrentUser(request);
		try {
			CompFinancingExtGuara entity = compFinancingSV.findCompFinaExtGuaraById(id);
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			entity.setStatus(Constants.DELETE);
			compFinancingSV.saveCompFinaExtGuara(entity);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * @Description (企业或有负债  司法信息页面跳转)
	 * @param request
	 * @param serNo 任务流水号
	 * @return
	 */
	@RequestMapping("compFinancingConLiab.do")
	public String compFinancingConLiab(HttpServletRequest request, String serNo, String customerNo) {
		try {
			Task task = this.taskSV.getTaskBySerNo(serNo);
//			request.setAttribute("task", task);

			request.setAttribute("customerNo", customerNo);
			if(task!=null){
				request.setAttribute("taskId", task.getId());
				CompFinancing financing = compFinancingSV.findCompFinancingByTIdAndNo(task.getId(), customerNo);
				request.setAttribute("financing", financing);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "views/survey/common/compFinancingConLiab";
	}
	
	
	/**
	 * @Description (获取裁判文书列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getLawInfoList.do") 
	@ResponseBody
	public ResultDto getLawInfoList(HttpServletRequest request, HttpServletResponse response,
			Long taskId, String customerNo,
			@RequestParam(value="page", defaultValue="1") int page,
			@RequestParam(value="pageSize", defaultValue="10") int pageSize) {
		
		ResultDto re = returnFail();
		try {

			String applicationTime = DateUtil.getNowY_m_dStr();
			Pagination<CompJudgmentDto> judgmentPager = compJudgmentService.getJudgmentPager(customerNo, "", "", applicationTime, page, pageSize);
			
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("获取裁判文书列表详情成功");
			re.setRows(judgmentPager.getItems());
			re.setTotal(judgmentPager.getRowsCount());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return re;
	}
	
	
	/***
	 * 获取司法分析数据
	 * @param taskId	贷前任务号
	 * @return
	 */
	@RequestMapping("getJudgAnaly.json")
	@ResponseBody
	public ResultDto getJudgAnaly(String customerNo) {
		ResultDto re = returnFail("获取司法预警信息失败");
		if(StringUtil.isEmptyString(customerNo)){
			re.setMsg("客户号为空，请重试");
			return re;
		}
		try {
			CusBase cusBase = cusBaseSV.queryByCusNo(customerNo);
			String customerName = null;
			if (cusBase!=null) {
				if (cusBase.getCustomerName() != null) {
					customerName = cusBase.getCustomerName();
				}
			}
			String applicationTime = DateUtil.getNowY_m_dStr();
			String judgSimAnaly = compJudgmentService.getJudgSimAnaly(applicationTime, customerNo, customerName);
			re.setRows(judgSimAnaly);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("获取司法预警信息成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	
	/**
	 * 跳转到裁判文书详情
	 * @param request
	 * @param serNo
	 * @param customerNo
	 * @return
	 */
	@RequestMapping("judgDtl.do")
	public ModelAndView judgDtl(HttpServletRequest request, String judgId) {
		ModelAndView mav = new ModelAndView("views/survey/common/compJudgDtl");
		
//		judgId = "cc7a7354eb892b9768494a08ab340be9";
		
		try {
			QccJudgDtlDto judgmentDtl = synchronizationQCCService.getJudgmentDtl(judgId);
			mav.addObject("data", judgmentDtl);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
	
	
	
	
	/**
	 * @Description (获取企业普通融资情况)
	 * @param request
	 * @param response
	 * @return
	 */
	@Deprecated
	@RequestMapping("saveFinancing.json")
	@ResponseBody
	public String saveFinancing(HttpServletRequest request, HttpServletResponse response) {
		String jsonString = request.getParameter("jsonString");
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			compFinancingSV.saveCompFinancing(JacksonUtil.fromJson(jsonString, CompFinancing.class));
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	
	/**
	 * @Description (保存企业对外融资与担保信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@Deprecated
	@RequestMapping("saveExterFinanGuara.json")
	@ResponseBody
	public String saveExterFinanGuara(HttpServletRequest request, HttpServletResponse response) {
		String jsonString = request.getParameter("jsonString");
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			compFinancingSV.saveCompFinaExtGuara(JacksonUtil.fromJson(jsonString, CompFinancingExtGuara.class));
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	
	/**
	 * @Description (删除企业对外融资与担保信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@Deprecated
	@RequestMapping("deleteExterFinanGuara.json")
	@ResponseBody
	public String deleteExterFinanGuara(HttpServletRequest request, HttpServletResponse response) {
		String financingIdStr = request.getParameter("id");
		Long financingId = Long.valueOf(financingIdStr);
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			CompFinancingExtGuara curObj = compFinancingSV.findCompFinaExtGuaraById(financingId);
			compFinancingSV.deleteCompFinaExtGuara(curObj);
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * 保存征信系统查询情况
	 * @param request
	 * @param response
	 * @return
	 *//*
	@RequestMapping("saveCredsysQueryInfo.json")
	@ResponseBody
	public Map<String, Object> saveCredsysQueryInfo(HttpServletRequest request, HttpServletResponse response) {
		String jsonString = request.getParameter("jsonString");
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			CompCredSysQueryInfo credsysQueryInfo = JacksonUtil.fromJson(jsonString,CompCredSysQueryInfo.class);
			compFinancingSV.saveOrUpdateCompCredsysQueryInfo(credsysQueryInfo);
			json.put("success", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return json;
	}*/

}
