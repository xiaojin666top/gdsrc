package com.beawan.survey.custInfo.controller;

import com.beawan.analysis.finansis.service.IFinanasisService;
import com.beawan.base.entity.User;
import com.beawan.base.service.ISysDicSV;
import com.beawan.common.BusinessException;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.SysConstants.ExceptionConstant;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.beawan.library.service.IIndustrySV;
import com.beawan.survey.custInfo.bean.*;
import com.beawan.survey.custInfo.dto.CompBaseDto;
import com.beawan.survey.custInfo.dto.CompBaseManagerDto;
import com.beawan.survey.custInfo.dto.CompManagerInfoDto;
import com.beawan.survey.custInfo.entity.CompContribute;
import com.beawan.survey.custInfo.entity.CompCredit;
import com.beawan.survey.custInfo.entity.CompUserCredit;
import com.beawan.survey.custInfo.service.*;
import com.beawan.survey.guarantee.entity.GuaEstate;
import com.beawan.task.service.ITaskSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.platform.util.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 企业征信控制器
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/survey/cust/" })
public class CompCreditCtl extends BaseController {
	
	private static final Logger log = Logger.getLogger(CompCreditCtl.class);

	@Resource
	private CompCreditService compCreditService;
	@Resource
	private CompUserCreditService compUserCreditService;
	@Resource
	private ISysDicSV sysDicSV;



	//跳转到查看企业及个人征信界面
	@RequestMapping("compCredit.do")
	public ModelAndView compCredit(String serialNumber) throws Exception{
		ModelAndView mav =  new ModelAndView("views/survey/common/compCredit");
		//加载字典数据
		String[] optTypes = {
				SysConstants.BsDicConstant.STD_REL_IND_TYPE,
				SysConstants.BsDicConstant.STD_REL_ENT_TYPE
		};
		mav.addObject("dicData", sysDicSV.queryMapListByTypes(optTypes));
		mav.addObject("serialNumber", serialNumber);
		return mav;
	}

	/**
	 * 获取企业征信信息
	 * @param guaCompId 担保企业主键
	 * @param serialNumber 任务流水号
	 * @return
	 */
	@RequestMapping("getCompCredit.json")
	@ResponseBody
	public ResultDto getCompCredit(String serialNumber, String guaCompId){
		ResultDto re = returnFail("获取企业征信报告失败");
		try{
			List<CompCredit> entityList = compCreditService.getBySerNoAndGId(serialNumber, guaCompId);
			re.setRows(entityList);
			re.setMsg("获取企业征信报告成功！");
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		}catch (Exception e){
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 保存/修改 企业征信报告
	 * @param request
	 * @return
	 */
	@RequestMapping("saveCompCredit.json")
	@ResponseBody
	public ResultDto saveCompCredit(HttpServletRequest request, String jsonArray) {
		ResultDto re = returnFail("保存失败");
		try {
			List<CompCredit> entityList = 
					JacksonUtil.fromJson(jsonArray, new TypeReference<List<CompCredit>>() {});
			if (CollectionUtils.isEmpty(entityList)) {
				return returnFail("传输数据内容为空，请重试!");
			}

			User user = HttpUtil.getCurrentUser(request);
			Timestamp nowTime = DateUtil.getNowTimestamp();
			for (CompCredit entity : entityList) {
				Integer id = entity.getId();
				if(id == null){
					entity.setCreater(user.getUserId());
					entity.setCreateTime(nowTime);
				}
				entity.setUpdater(user.getUserId());
				entity.setUpdateTime(nowTime);
				entity.setStatus(Constants.NORMAL);
				compCreditService.saveOrUpdate(entity);
			}

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (DataAccessException e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除企业征信报告
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("removeCompCredit.json")
	@ResponseBody
	public ResultDto removeCompCredit(HttpServletRequest request, Integer id) {
		ResultDto re = returnFail("删除企业征信报告失败");
		if (id == null || id == 0) {
			re.setMsg("传输数据内容异常，请重试");
			return re;
		}
		// 获取到登录的用户编号
		User user = HttpUtil.getCurrentUser(request);
		try {
			CompCredit compCredit = compCreditService.findByPrimaryKey(id);
			compCredit.setStatus(Constants.DELETE);
			compCredit.setUpdater(user.getUserId());
			compCredit.setUpdateTime(DateUtil.getNowTimestamp());
			compCreditService.saveOrUpdate(compCredit);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除企业征信报告成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/******************************************个人征信操作***********************/

	/**
     * 获取个人征信信息
     * @param guaCompId 担保企业主键
     * @param serialNumber 任务流水号
     * @return
     */
    @RequestMapping("getCompUserCredit.json")
    @ResponseBody
    public ResultDto getCompUserCredit(String guaCompId, String serialNumber){
        ResultDto re = returnFail("获取个人征信报告失败");
        try{
        	List<CompUserCredit> entityList = compUserCreditService.getBySerNoAndGId(serialNumber, guaCompId);
            re.setRows(entityList);
            re.setMsg("获取企业征信报告成功！");
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
        }catch (Exception e){
            e.printStackTrace();
        }
        return re;
    }

	/**
	 * 保存/修改 个人征信报告
	 * @param request
	 * @return
	 */
	@RequestMapping("saveUserCredit.json")
	@ResponseBody
	public ResultDto saveUserCredit(HttpServletRequest request, String jsonArray) {
		ResultDto re = returnFail("保存失败");
		try {
			List<CompUserCredit> entityList =
					JacksonUtil.fromJson(jsonArray, new TypeReference<List<CompUserCredit>>() {});
			if (CollectionUtils.isEmpty(entityList)) {
				return returnFail("传输数据内容为空，请重试!");
			}

			User user = HttpUtil.getCurrentUser(request);
			Timestamp nowTime = DateUtil.getNowTimestamp();
			for (CompUserCredit entity : entityList) {
				Integer id = entity.getId();
				if(id == null){
					entity.setCreater(user.getUserId());
					entity.setCreateTime(nowTime);
				}
				entity.setUpdater(user.getUserId());
				entity.setUpdateTime(nowTime);
				entity.setStatus(Constants.NORMAL);
				compUserCreditService.saveOrUpdate(entity);
			}

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除个人征信报告
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("removeCompUserCredit.json")
	@ResponseBody
	public ResultDto removeCompUserCredit(HttpServletRequest request, Integer id) {
		ResultDto re = returnFail("删除个人征信报告失败");
		if (id == null || id == 0) {
			re.setMsg("传输数据内容异常，请重试");
			return re;
		}
		// 获取到登录的用户编号
		User user = HttpUtil.getCurrentUser(request);
		try {
			CompUserCredit userCredit = compUserCreditService.findByPrimaryKey(id);
			userCredit.setStatus(Constants.DELETE);
			userCredit.setUpdater(user.getUserId());
			userCredit.setUpdateTime(DateUtil.getNowTimestamp());
			compUserCreditService.saveOrUpdate(userCredit);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除个人征信报告成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

}
