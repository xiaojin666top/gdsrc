package com.beawan.survey.custInfo.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.library.service.IIndustrySV;
import com.beawan.survey.comm.TreeItemBean;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/survey/cust" })
public class CustInfoCtl {

	@Resource
	protected IIndustrySV industrySV;

	@RequestMapping("getIndustryTree.do")
	@ResponseBody
	public String compCustBase(HttpServletRequest request, HttpServletResponse response) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<TreeItemBean> list = industrySV.findIndustryTree();
		return jRequest.serialize(list, true);
	}

}
