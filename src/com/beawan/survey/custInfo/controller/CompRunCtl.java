package com.beawan.survey.custInfo.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.beawan.base.entity.QuickAnaly;
import com.beawan.base.entity.User;
import com.beawan.common.Constants;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.survey.custInfo.bean.*;
import com.beawan.survey.custInfo.dto.CompBaseDto;
import com.beawan.survey.custInfo.dto.SalesStatDto;
import com.beawan.survey.custInfo.entity.CompEquipment;
import com.beawan.survey.custInfo.entity.CompRawMaterial;
import com.beawan.survey.custInfo.entity.VisitRecord;
import com.beawan.survey.custInfo.service.*;
import com.platform.util.*;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.base.service.ISysDicSV;
import com.beawan.base.service.ISysTreeDicSV;
import com.beawan.base.service.QuickAnalyService;
import com.beawan.common.SysConstants;
import com.beawan.common.SysConstants.ExceptionConstant;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.library.service.IIndustrySV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.fasterxml.jackson.core.type.TypeReference;

import net.sf.json.JSONArray;
import org.springframework.web.servlet.ModelAndView;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/survey/cust" })
public class CompRunCtl extends BaseController {
	
	@Resource
	protected ITaskSV taskSV;
	@Resource
	private ICompRunSV compRunSV;
	@Resource
	protected IIndustrySV industrySV;
	@Resource
	private ICompBaseSV compBaseSV;
	@Resource
	private ISysDicSV sysDicSV;
	@Resource
	private ISysTreeDicSV sysTreeDicSV;
	@Resource
	private CompProductService compProductService;
	@Resource
	private CompPowerService compPowerService;
	@Resource
	private CompProductSalesService compSalesService;
	@Resource
	private compRunAnalysisService compAnalysisService;
	@Resource
	private CompRunSupplyService compSupplyService;
	@Resource
	private CompRawMaterialService compRawMaterialService;
	@Resource
	private CompSalesStatService compSalesStatService;
	@Resource
	private CompEquipmentService compEquipmentService;
	@Resource
	private VisitRecordService visitRecordService;
	@Resource
	private QuickAnalyService quickAnalyService;
	
	/**
	 * @Description (流动资金贷款-企业经营状况-行业分析页面跳转)
	 * @param request
	 * @param serNo 任务流水号
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping("compRunIndustry.do")
	public String compRunIndustry(HttpServletRequest request, String serNo, String customerNo) {
		try {
			Task task = this.taskSV.getTaskBySerNo(serNo);
			if (StringUtil.isEmptyString(customerNo)) {
				customerNo = task.getCustomerNo();
			}

			String industry = sysTreeDicSV.getFullNameByEnName(task.getIndustry(),
					SysConstants.TreeDicConstant.GB_IC_4754_2017);
			CompRun compRun = compRunSV.queryCompRunByTIdAndNo(task.getId(), customerNo);

			String situation = "";
			if(null != compRun && null != compRun.getIndusAnalysis() && !"".equals(compRun.getIndusAnalysis()))
				situation = compRun.getIndusAnalysis();
				
			request.setAttribute("newsituation", situation);
			request.setAttribute("industry", industry);
			request.setAttribute("taskId", task.getId());
			request.setAttribute("customerNo", customerNo);
			request.setAttribute("compRun", compRun);
			
			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.STD_INDUSTRY_POLICY};
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "views/survey/liquidity/common/industry";
	}
	
	/**
	 * @Description (流动资金贷款-保存企业经营状况-行业分析信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveCompIndustry.do")
	@ResponseBody
	public String saveCompIndustry(HttpServletRequest request, HttpServletResponse response) {
		String compRunInfo = request.getParameter("compRun");
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<>();
		try {
			User user = HttpUtil.getCurrentUser();
			CompRun newCompRun = JacksonUtil.fromJson(compRunInfo, CompRun.class);
			CompRun oldCompRun = compRunSV.queryCompRunByTIdAndNo(newCompRun.getTaskId(),
					newCompRun.getCustomerNo());
			if(oldCompRun != null){
				oldCompRun.setIndusComPolicy(newCompRun.getIndusComPolicy());
				oldCompRun.setIndusMyPolicy(newCompRun.getIndusMyPolicy());
//				oldCompRun.setIndusPolicyAnalysis(newCompRun.getIndusPolicyAnalysis());
//				oldCompRun.setIndusBaseInfo(newCompRun.getIndusBaseInfo());
//				oldCompRun.setIndusSwot(newCompRun.getIndusSwot());
				oldCompRun.setIndusAnalysis(newCompRun.getIndusAnalysis());
				oldCompRun.setEnvironment(newCompRun.getEnvironment());
				
				newCompRun.setId(null);
				oldCompRun.setUpdater(user.getUserId());
				oldCompRun.setUpdateTime(DateUtil.getNowTimestamp());
				compRunSV.saveCompRun(oldCompRun);
			}else{
				newCompRun.setCreater(user.getUserId());
				newCompRun.setCreateTime(DateUtil.getNowTimestamp());
				compRunSV.saveCompRun(newCompRun);
			}
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (流动资金贷款-企业经营状况-经营基本情况页面跳转)
	 * @param request
	 * @param response
	 * @param taskId 任务ID
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping("compRunBase.do")
	public String compRunBase(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo) {
		
		try {
			
			Task task = this.taskSV.getTaskById(taskId);
			if (StringUtil.isEmptyString(customerNo)) {
				customerNo = task.getCustomerNo();
			}
			
			CompRun compRun = compRunSV.queryCompRunByTIdAndNo(taskId, customerNo);
			
			request.setAttribute("taskId", taskId);
			request.setAttribute("customerNo", customerNo);
			request.setAttribute("compRun", compRun);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/survey/liquidity/common/compRunBase";
	}
	
	/**
	 * @Description (流动资金贷款-保存企业经营状况-经营基本信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveCompRunBase.do")
	@ResponseBody
	public String saveCompRunBase(HttpServletRequest request, HttpServletResponse response) {
		String compRunBase = request.getParameter("compRunBase");
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			
			CompRun newCompRun = JacksonUtil.fromJson(compRunBase, CompRun.class);
			CompRun oldCompRun = compRunSV.queryCompRunByTIdAndNo(newCompRun.getTaskId(),
					newCompRun.getCustomerNo());
			if(oldCompRun != null){
				oldCompRun.setDevelopment(newCompRun.getDevelopment());
				oldCompRun.setMarketAnalysis(newCompRun.getMarketAnalysis());
				oldCompRun.setProductAnalysis(newCompRun.getProductAnalysis());
				
				newCompRun.setId(null);
				compRunSV.saveCompRun(oldCompRun);
			}else{
				compRunSV.saveCompRun(newCompRun);
			}
			
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}

	/**
	 * @Description (获取企业上游供应商列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getCompRunSupply.do")
	@ResponseBody
	public String getCompRunSupply(HttpServletRequest request, HttpServletResponse response) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<CompRunSupply> dataGrid = null;
		try {
			String taskId = request.getParameter("taskId");
			String customerNo = request.getParameter("customerNo");
			dataGrid = compRunSV.queryRunSupplyByTIdAndNo(Long.valueOf(taskId), customerNo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jRequest.serialize(dataGrid, true);
	}

	/**
	 * @Description (保存企业上游供应商记录)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveCompSupplys.do")
	@ResponseBody
	public String saveCompSupplys(HttpServletRequest request, HttpServletResponse response,
								  long taskId, String customerNo, String compSupplys) {

		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();

		try {
			compRunSV.saveRunSupplys(taskId, customerNo, compSupplys);
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}

		return jRequest.serialize(json, true);
	}

	/**
	 * @Description (删除所有企业上游供应商记录)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("deleteSupplyAll.do")
	@ResponseBody
	public String deleteCustSupply(HttpServletRequest request, HttpServletResponse response,
								   long taskId, String customerNo) {

		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();

		try {
			List<CompRunSupply> list = compRunSV.queryRunSupplyByTIdAndNo(taskId, customerNo);
			if(!CollectionUtils.isEmpty(list)){
				for(CompRunSupply supply : list){
					compRunSV.deleteRunSupply(supply);
				}
			}
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}

		return jRequest.serialize(json, true);
	}

	/**
	 * @Description (保存企业上游供应商分析说明)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveSupplyAnalysis.do")
	@ResponseBody
	public String saveSupplyAnalysis(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo) {
		
		String supplierAnalysis = request.getParameter("supplierAnalysis");
		String rawMaterial = request.getParameter("rawMaterial");
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<>();
		
		try {
			User user = HttpUtil.getCurrentUser();
			CompRun compRun = compRunSV.queryCompRunByTIdAndNo(taskId, customerNo);
			compRun.setSupplierAnalysis(supplierAnalysis);
			compRun.setRawMaterial(rawMaterial);
			compRun.setUpdater(user.getUserId());
			compRun.setUpdateTime(DateUtil.getNowTimestamp());
			compRunSV.saveCompRun(compRun);
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (流动资金贷款-企业经营状况-生产情况页面跳转)
	 * @param request
	 * @param serNo 任务流水号
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping("compRunProduce.do")
	public String compRunProduce(HttpServletRequest request, String serNo, String customerNo) {
		try {
			Task task = this.taskSV.getTaskBySerNo(serNo);
			if (StringUtil.isEmptyString(customerNo)) {
				customerNo = task.getCustomerNo();
			}
			CompRunProduce compRunProduce = compRunSV.queryRunProduceByTIdAndNo(task.getId(), customerNo);
			
			request.setAttribute("taskId", task.getId());
			request.setAttribute("customerNo", customerNo);
			request.setAttribute("runProduce", compRunProduce);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "views/survey/liquidity/common/compRunProduce";
	}
	
	/**
	 * @Description (流动资金贷款-保存企业经营状况-生产情况信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveCompRunProduce.do")
	@ResponseBody
	public String saveCompRunProduce(HttpServletRequest request, HttpServletResponse response) {
		String runProduce = request.getParameter("compRunProduce");
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			
			CompRunProduce compRunProduce = JacksonUtil.fromJson(runProduce, CompRunProduce.class);
			compRunSV.saveRunProduce(compRunProduce);
			
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (流动资金贷款-企业经营状况-销售情况页面跳转)
	 * @param request
	 * @param serNo 任务流水号
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping("compRunSales.do")
	public String compRunSales(HttpServletRequest request, String serNo, String customerNo) {
		try {
			Task task = this.taskSV.getTaskBySerNo(serNo);
			request.setAttribute("task", task);
			if (StringUtil.isEmptyString(customerNo)) {
				customerNo = task.getCustomerNo();
			}
			CompRun compRun = compRunSV.queryCompRunByTIdAndNo(task.getId(), customerNo);
			request.setAttribute("compRun", compRun);
			
			//下游客户列表
			List<CompRunCustomer> runCustomers = compRunSV.queryRunCustomerByTIdAndNo(task.getId(), customerNo);
			request.setAttribute("runCustomers", JSONArray.fromObject(runCustomers).toString());
			
			//产品销售列表
			List<CompRunPrdSale> runPrdSales = compRunSV.queryRunPrdSaleByTIdAndNo(task.getId(), customerNo);
			request.setAttribute("runPrdSales", JSONArray.fromObject(runPrdSales).toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "views/survey/liquidity/common/compRunSales";
	}
	
	/**
	 * @Description (获取企业下游客户列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getCompRunCustomer.do")
	@ResponseBody
	public String getCompRunCustomer(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<CompRunCustomer> dataGrid = null;
		try {
			dataGrid = compRunSV.queryRunCustomerByTIdAndNo(taskId, customerNo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jRequest.serialize(dataGrid, true);
	}
	
	/**
	 * @Description (保存企业下游客户记录)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveRunCustomers.do")
	@ResponseBody
	public String saveRunCustomers(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo, String runCustomers) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			compRunSV.saveRunCustomers(taskId, customerNo, runCustomers);
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (获取企业主要产品销售列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getCompRunPrdSale.do")
	@ResponseBody
	public String getCompRunPrdSale(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<CompRunPrdSale> dataGrid = null;
		try {
			
			dataGrid = compRunSV.queryRunPrdSaleByTIdAndNo(taskId, customerNo);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return jRequest.serialize(dataGrid, true);
	}
	
	/**
	 * @Description (保存企业主要产品销售列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveRunPrdSales.do")
	@ResponseBody
	public String saveRunPrdSales(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo) {
		
		String runPrdSales = request.getParameter("runPrdSales");
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			compRunSV.saveRunPrdSales(taskId, customerNo, runPrdSales);
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (保存企业销售情况分析说明)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveRunSalesAnalysis.do")
	@ResponseBody
	public String saveRunSalesAnalysis(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo) {
		
		String salesModel = request.getParameter("salesModel");
		String customerAnalysis = request.getParameter("customerAnalysis");
		String productSales = request.getParameter("productSales");
		String salesPlan = request.getParameter("salesPlan");
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			CompRun compRun = compRunSV.queryCompRunByTIdAndNo(taskId, customerNo);
			compRun.setSalesModel(salesModel);
			compRun.setDownCustomer(customerAnalysis);
			compRun.setProductSales(productSales);
			compRun.setSalesPlan(salesPlan);
			compRunSV.saveCompRun(compRun);
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (能源耗用及工资统计页面跳转)
	 * @param request
	 * @param taskId 任务ID
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping("compRunPower.do")
	public String compRunPower(HttpServletRequest request, long taskId, String customerNo) {
		try {
			Task task = this.taskSV.getTaskById(taskId);
			request.setAttribute("task", task);
			
			if (StringUtil.isEmptyString(customerNo)) {
				customerNo = task.getCustomerNo();
			}
			
			CompRun compRun = compRunSV.queryCompRunByTIdAndNo(taskId, customerNo);
			//能源耗用及工资统计分析
			request.setAttribute("compRun", compRun);
			
			List<CompRunPower>  runPowers = compRunSV.queryRunPowerByTIdAndNo(taskId, customerNo);
			request.setAttribute("runPowers", JSONArray.fromObject(runPowers).toString());
			
			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.STD_ENERGY_AND_PAY};
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/survey/liquidity/common/compRunPower";
	}
	
	/**
	 * @Description (保存企业主能源耗用及工人工资情况)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveRunPowers.do")
	@ResponseBody
	public String saveRunPowers(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo) {
		
		String runPowers = request.getParameter("runPowers");
		String energyPayAnalysis = request.getParameter("energyPayAnalysis");
		String energyStatMonth = request.getParameter("energyStatMonth");
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			
			CompRun compRun = compRunSV.queryCompRunByTIdAndNo(taskId, customerNo);
			if(compRun == null){
				compRun = new CompRun();
				compRun.setTaskId(taskId);
				compRun.setCustomerNo(customerNo);
			}
			compRun.setEnergyPayAnalysis(energyPayAnalysis);
			compRun.setEnergyStatMonth(energyStatMonth);
			compRunSV.saveCompRun(compRun);
			
			compRunSV.saveRunPowers(taskId, customerNo, runPowers);
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description 企业经营状况信息页面跳转
	 * @param request
	 * @param response
	 * @param taskId 任务ID
	 * @return
	 */
	@RequestMapping("guaraCustRun.do")
	public String guaraCustRun(HttpServletRequest request, HttpServletResponse response, String taskId,
			String customerNo) {
		try {
			CompRun custRun = compRunSV.queryCompRunByTIdAndNo(Long.valueOf(taskId), customerNo);
			if (null == custRun) {
				custRun = new CompRun();
				custRun.setTaskId(Long.valueOf(taskId));
				custRun.setCustomerNo(customerNo);
			}
			request.setAttribute("custRun", custRun);
			request.setAttribute("custRunStr", JacksonUtil.serialize(custRun));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "views/survey/common/guaraCompRunInfo";
	}

	/**
	 * @Description (保存企业经营状况信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveCustRunInfo.do")
	@ResponseBody
	public String saveCustRunInfo(HttpServletRequest request, HttpServletResponse response) {
		String custRunInfo = request.getParameter("custRunInfo");
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			CompRun custRun = JacksonUtil.fromJson(custRunInfo, CompRun.class);
			compRunSV.saveCompRun(custRun);
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}

	/******************************企业经营情况*******************************/

	/** 跳转到 行业状况 风险分析 */
	@RequestMapping("compIndustryRisk.do")
	public ModelAndView compIndustryRisk(String serNo, String customerNo){
		ModelAndView mav =  new ModelAndView("views/survey/liquidity/common/industryRisk");
		try {


			CompBaseDto compBase = compBaseSV.findCompBaseByCustNo(customerNo);
			CompRunAnalysis runAnalysis = compAnalysisService.selectSingleByProperty("serNo", serNo);
			if (runAnalysis == null || StringUtil.isEmptyString(runAnalysis.getIndustryInfo())) {
				runAnalysis = compAnalysisService.syncInduAnalysis(serNo);
			}

			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.STD_INDUSTRY_POLICY};

			mav.addObject("serNo", serNo);
			mav.addObject("customerNo", customerNo);
			if(compBase!=null){
				if (compBase.getIndustryType()!=null){
					mav.addObject("industry", compBase.getIndustryType());
				}
				if (compBase.getIndustryCode()!=null){
					mav.addObject("induCode", compBase.getIndustryCode());
				}
			}
			mav.addObject("dicData", sysDicSV.queryMapListByTypes(optTypes));
			mav.addObject("runAnalysis", runAnalysis);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}

    /**
     * 保存/修改 行业风险分析
     * @param request
     * @return
     */
    @RequestMapping("saveIndustryInfo.do")
    @ResponseBody
    public ResultDto saveCompIndustry(HttpServletRequest request) {
        ResultDto re = returnFail("保存失败");
        String industryRisk = request.getParameter("industryRisk");
        try {
			CompRunAnalysis compRunAnalysis = JacksonUtil.fromJson(industryRisk, CompRunAnalysis.class);
			if (compRunAnalysis == null) {
                re.setMsg("请先填写相关信息！");
                return re;
            }
            User user = HttpUtil.getCurrentUser(request);

			CompRunAnalysis entity = compAnalysisService.selectSingleByProperty("serNo", compRunAnalysis.getSerNo());
			if (entity == null){
				entity = new CompRunAnalysis();
				entity.setSerNo(compRunAnalysis.getSerNo());
				entity.setCreater(user.getUserId());
				entity.setCreateTime(DateUtil.getNowTimestamp());
            }
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			entity.setIndustryInfo(compRunAnalysis.getIndustryInfo());
			entity.setCustomerNo(compRunAnalysis.getCustomerNo());
			compAnalysisService.saveOrUpdate(entity);
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 跳转到 生产情况分析
     * @param serNo
     * @param customerNo
     * @return
     */
    @RequestMapping("compProduceInfo.do")
	public ModelAndView compRunProduct(String serNo, String customerNo){
		ModelAndView mav =  new ModelAndView("views/survey/liquidity/common/compRunProduce");
		try {
			Task task = taskSV.getTaskBySerNo(serNo);
			if (task != null)
				mav.addObject("loanType", task.getBusinessNo());
			mav.addObject("serNo", serNo);
			mav.addObject("customerNo", customerNo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
    
	/** 跳转到 生产环节 产品及能耗详情*/
    /**
	 * 跳转到 生产情况 相应界面
	 * @param serNo
	 * @param customerNo
	 * @return
	 */
	@RequestMapping("toProducePage.do")
	public ModelAndView toProducePage(String serNo, String customerNo, String type){
		ModelAndView mav =  new ModelAndView();
		try {
			CompRunAnalysis entity = compAnalysisService.selectSingleByProperty("serNo", serNo);
			mav.addObject("compRunAnalysis", entity);
			mav.addObject("serNo", serNo);
			mav.addObject("customerNo", customerNo);
			String[] items = new String[]{
					SysConstants.RG_PRE_ITEM.PRODUCT_INFO,
					SysConstants.RG_PRE_ITEM.PRODUCTING_INFO,
					SysConstants.RG_PRE_ITEM.ENVIRON_INFO,
					SysConstants.RG_PRE_ITEM.ZIZHI_INFO,
					SysConstants.RG_PRE_ITEM.SELL_INFO,
					SysConstants.RG_PRE_ITEM.SUPPLY_INFO
			};
			Map<String, List<QuickAnaly>> analyMap = quickAnalyService.queryAnalyByItem(items);
			mav.addObject("analyMap", GsonUtil.GsonString(analyMap));
			
			if (Constants.LB_PAGE_TYPE.PRODUCT.equals(type)){
				mav.setViewName("views/survey/liquidity/common/mainProduct");
			}else if (Constants.LB_PAGE_TYPE.ENERGY.equals(type)){
				mav.setViewName("views/survey/liquidity/common/energyUse");
			}else if (Constants.LB_PAGE_TYPE.EQUIPMENT.equals(type)){
				mav.setViewName("views/survey/liquidity/common/compEquipment");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}

	/**
	 * 获取 生产环节 产品详情列表
	 * @param serNo
	 * @return
	 */
	@RequestMapping("getComProductList.json")
	@ResponseBody
	public ResultDto getCompProductList(HttpServletRequest request, String serNo, String customerNo){
		ResultDto re = returnFail("获取产品详情失败");
		if(StringUtil.isEmptyString(serNo))
			return returnFail("任务流水号为空");
		
		try {
			List<CompRunProduct> productList = compProductService.selectByProperty("serNo", serNo);
			request.setAttribute("serNo", serNo);
			request.setAttribute("customerNo", customerNo);
			re = returnSuccess();
			re.setRows(productList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 保存/修改 生产环节 产品详情
	 * @param request
	 * @return
	 */
	@RequestMapping("saveComProduct.json")
	@ResponseBody
	public ResultDto saveCompProduct(HttpServletRequest request) {
		ResultDto re = returnFail("保存失败");
		String jsonData = request.getParameter("jsonArray");
		try {
			List<CompRunProduct> entityList = 
					JacksonUtil.fromJson(jsonData, new TypeReference<List<CompRunProduct>>() {});
			if (entityList == null || entityList.size() <= 0) {
				re.setMsg("传输数据内容为空，请重试");
				return re;
			}

			User user = HttpUtil.getCurrentUser(request);
			Timestamp nowTime = DateUtil.getNowTimestamp();
			for (CompRunProduct entity : entityList) {
				Integer id = entity.getId();
				if(id == null){
					entity.setCreater(user.getUserId());
					entity.setCreateTime(nowTime);
				}
				entity.setUpdater(user.getUserId());
				entity.setUpdateTime(nowTime);
				entity.setStatus(Constants.NORMAL);
				compProductService.saveOrUpdate(entity);
			}

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除 生产环节 产品详情
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("deleteProduct.json")
	@ResponseBody
	public ResultDto removeCompProduct(HttpServletRequest request, Integer id) {
		ResultDto re = returnFail("删除失败");
		if (id == null || id == 0) {
			re.setMsg("传输数据内容异常，请重试");
			return re;
		}
		User user = HttpUtil.getCurrentUser(request);
		try {
			CompRunProduct product = compProductService.findByPrimaryKey(id);
			product.setStatus(Constants.DELETE);
			product.setUpdater(user.getUserId());
			product.setUpdateTime(DateUtil.getNowTimestamp());
			compProductService.saveOrUpdate(product);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 获取 生产环节 能耗详情列表
	 * @param serNo
	 * @return
	 */
	@RequestMapping("getComPowerList.json")
	@ResponseBody
	public ResultDto getCompPowerList(HttpServletRequest request, String serNo, String customerNo){
		ResultDto re = returnFail("获取产品详情失败");
		if(StringUtil.isEmptyString(serNo)){
			re.setMsg("任务流水号为空");
			return re;
		}
		try {
			List<CompPower> powerInfo = compPowerService.selectByProperty("serNo", serNo);
			request.setAttribute("serNo", serNo);
			request.setAttribute("customerNo", customerNo);
			re = returnSuccess();
			re.setRows(powerInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 保存/修改 生产环节 能耗详情
	 * @param request
	 * @return
	 */
	@RequestMapping("saveComPower.json")
	@ResponseBody
	public ResultDto saveCompPower(HttpServletRequest request) {
		ResultDto re = returnFail("保存失败");
		String jsonData = request.getParameter("jsonArray");
		try {
			List<CompPower> entityList = 
					JacksonUtil.fromJson(jsonData, new TypeReference<List<CompPower>>() {});
			if (entityList == null || entityList.size() <= 0) {
				re.setMsg("传输数据内容为空，请重试");
				return re;
			}

			User user = HttpUtil.getCurrentUser(request);
			Timestamp nowTime = DateUtil.getNowTimestamp();
			for (CompPower entity : entityList) {
				Integer id = entity.getId();
				if(id == null){
					entity.setCreater(user.getUserId());
					entity.setCreateTime(nowTime);
				}
				entity.setUpdater(user.getUserId());
				entity.setUpdateTime(nowTime);
				entity.setStatus(Constants.NORMAL);
				compPowerService.saveOrUpdate(entity);
			}

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除 生产环节 能耗详情
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("deletePower.json")
	@ResponseBody
	public ResultDto removeCompPower(HttpServletRequest request, Integer id) {
		ResultDto re = returnFail("删除失败");
		if (id == null || id == 0) {
			re.setMsg("传输数据内容异常，请重试");
			return re;
		}
		User user = HttpUtil.getCurrentUser(request);
		try {
			CompPower powerInfo = compPowerService.findByPrimaryKey(id);
			powerInfo.setStatus(Constants.DELETE);
			powerInfo.setUpdater(user.getUserId());
			powerInfo.setUpdateTime(DateUtil.getNowTimestamp());
			compPowerService.saveOrUpdate(powerInfo);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/** 跳转到 销售环节 产品销售详情 */
	@RequestMapping("compProductSales.do")
	public ModelAndView compProductSales(String serNo, String customerNo){
		ModelAndView mav =  new ModelAndView("views/survey/liquidity/common/compProductSales");
		try {
			CompRunAnalysis analysisInfo = compAnalysisService.selectSingleByProperty("serNo", serNo);
			if(analysisInfo!=null) {
				mav.addObject("productSalesInfo", analysisInfo.getProductSalesInfo());
			}
			Task task = taskSV.getTaskBySerNo(serNo);
			// 销售统计数据（商贸类）
			if (Constants.LB_SURVEY_TYPE.COMMERCIAL_TRADE.equals(task.getBusinessNo())){
				List<SalesStatDto> salesStatList = compSalesStatService.getSalesStatList(serNo);
				if (salesStatList == null)
					salesStatList = new ArrayList<>();
				mav.addObject("salesStats", JacksonUtil.serialize(salesStatList));
			}
			String[] items = new String[]{
					SysConstants.RG_PRE_ITEM.SELL_INFO
			};
			Map<String, List<QuickAnaly>> analyMap = quickAnalyService.queryAnalyByItem(items);
			mav.addObject("analyMap", GsonUtil.GsonString(analyMap));
			mav.addObject("serNo", serNo);
			mav.addObject("task", task);
			mav.addObject("customerNo", customerNo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}

	/**
	 * 获取 销售环节 主要产品销售详情列表
	 * @param serNo
	 * @return
	 */
	@RequestMapping("getProductSalesList.json")
	@ResponseBody
	public ResultDto getProductSalesList(HttpServletRequest request, String serNo, String customerNo){
		ResultDto re = returnFail("获取产品详情失败");
		if(StringUtil.isEmptyString(serNo)){
			re.setMsg("任务流水号为空");
			return re;
		}
		try {
			List<CompProductSales> salesInfo =
					compSalesService.selectByProperty("serNo", serNo, "PERIOD ASC");
			Collections.sort(salesInfo, new Comparator<CompProductSales>() {
				@Override
				public int compare(CompProductSales arg0, CompProductSales arg1) {
					if (arg1.getYearSales() != null && arg0.getYearSales() != null){
						return (int) (arg1.getYearSales() - arg0.getYearSales());
					}else {
						return 0;
					}
					
				}
			});
			request.setAttribute("serNo", serNo);
			request.setAttribute("customerNo", customerNo);
			re = returnSuccess();
			re.setRows(salesInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 保存/修改 销售环节 产品销售详情
	 * @param request
	 * @return
	 */
	@RequestMapping("saveProductSales.json")
	@ResponseBody
	public ResultDto saveProductSales(HttpServletRequest request) {
		ResultDto re = returnFail("保存失败");
		String jsonData = request.getParameter("jsonArray");
		try {
			List<CompProductSales> entityList = 
					JacksonUtil.fromJson(jsonData, new TypeReference<List<CompProductSales>>() {});
			if (entityList == null || entityList.size() <= 0) {
				re.setMsg("传输数据内容为空，请重试");
				return re;
			}

			User user = HttpUtil.getCurrentUser(request);
			Timestamp nowTime = DateUtil.getNowTimestamp();
			for (CompProductSales entity : entityList) {
				Integer id = entity.getId();
				if(id == null){
					entity.setCreater(user.getUserId());
					entity.setCreateTime(nowTime);
				}
				entity.setUpdater(user.getUserId());
				entity.setUpdateTime(nowTime);
				entity.setStatus(Constants.NORMAL);
				compSalesService.saveOrUpdate(entity);
			}

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除 销售环节 产品销售详情
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("deleteProductSales.json")
	@ResponseBody
	public ResultDto deleteProductSales(HttpServletRequest request, Integer id) {
		ResultDto re = returnFail("删除失败");
		if (id == null || id == 0) {
			re.setMsg("传输数据内容异常，请重试");
			return re;
		}
		User user = HttpUtil.getCurrentUser(request);
		try {
			CompProductSales productSales = compSalesService.findByPrimaryKey(id);
			productSales.setStatus(Constants.DELETE);
			productSales.setUpdater(user.getUserId());
			productSales.setUpdateTime(DateUtil.getNowTimestamp());
			compSalesService.saveOrUpdate(productSales);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 跳转到 采购环节 主要供应商、原材料详情
	 * @param serNo 流水号
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping("compRunSupply.do")
	public ModelAndView compRunSupply(String serNo, String customerNo){
		ModelAndView mav =  new ModelAndView("views/survey/liquidity/common/compRunSupply");
		try {
			Task task = taskSV.getTaskBySerNo(serNo);
			CompRunAnalysis analysisInfo = compAnalysisService.selectSingleByProperty("serNo", serNo);
			// 跳转到 商贸类调查模板
			if (Constants.LB_SURVEY_TYPE.COMMERCIAL_TRADE.equals(task.getBusinessNo()))
				mav.setViewName("views/survey/liquidity/common/compRawMaterial");
			String[] items = new String[]{
					SysConstants.RG_PRE_ITEM.MATERIAL_INFO,
					SysConstants.RG_PRE_ITEM.SUPPLY_INFO
			};
			
			Map<String, List<QuickAnaly>> analyMap = quickAnalyService.queryAnalyByItem(items);
			mav.addObject("analyMap", GsonUtil.GsonString(analyMap));
			mav.addObject("serNo", serNo);
			mav.addObject("customerNo", customerNo);
			if(analysisInfo!=null){
				mav.addObject("supplyInfo", analysisInfo.getSupplyInfo());
				mav.addObject("rawMaterialInfo", analysisInfo.getRawMaterialInfo());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}

	/**
	 * 获取 采购环节 上游供应商列表
	 * @param serNo 流水号
	 * @return
	 */
	@RequestMapping("getSupplyList.json")
	@ResponseBody
	public ResultDto getCompSupplyList(HttpServletRequest request, String serNo, String customerNo){
		ResultDto re = returnFail("获取供应商列表失败");
		if(StringUtil.isEmptyString(serNo)){
			re.setMsg("任务流水号为空");
			return re;
		}
		try {
			List<CompRunSupply> supplyInfo = compSupplyService.selectByProperty("serNo", serNo);
			Collections.sort(supplyInfo, new Comparator<CompRunSupply>() {
				@Override
				public int compare(CompRunSupply arg0, CompRunSupply arg1) {
					if (arg1.getCurrPurchaseAmt() != null && arg0.getCurrPurchaseAmt() != null){
						return (int) (arg1.getCurrPurchaseAmt() - arg0.getCurrPurchaseAmt());
					}else {
						return 0;
					}
					
				}
			});
			request.setAttribute("serNo", serNo);
			request.setAttribute("customerNo", customerNo);
			re = returnSuccess();
			re.setRows(supplyInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 保存/修改 采购环节 上游供应商
	 * @param request
	 * @return
	 */
	@RequestMapping("saveSupply.json")
	@ResponseBody
	public ResultDto saveCompSupply(HttpServletRequest request) {
		ResultDto re = returnFail("保存失败");
		String jsonData = request.getParameter("jsonArray");
		try {
			List<CompRunSupply> entityList = 
					JacksonUtil.fromJson(jsonData, new TypeReference<List<CompRunSupply>>() {});
			if (CollectionUtils.isEmpty(entityList))
					return returnFail("传输数据内容为空，请重试!");

			User user = HttpUtil.getCurrentUser(request);
			Timestamp nowTime = DateUtil.getNowTimestamp();
			for (CompRunSupply entity : entityList) {
				Integer id = entity.getId();
				if(id == null){
					entity.setCreater(user.getUserId());
					entity.setCreateTime(nowTime);
				}
				entity.setUpdater(user.getUserId());
				entity.setUpdateTime(nowTime);
				entity.setStatus(Constants.NORMAL);
				compSupplyService.saveOrUpdate(entity);
			}

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除 采购环节 上游供应商
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("deleteSupply.json")
	@ResponseBody
	public ResultDto deleteCompSupply(HttpServletRequest request, Integer id) {
		ResultDto re = returnFail("删除失败");
		if (id == null || id == 0) {
			re.setMsg("传输数据内容异常，请重试");
			return re;
		}
		User user = HttpUtil.getCurrentUser(request);
		try {
			CompRunSupply supplyInfo = compSupplyService.findByPrimaryKey(id);
			supplyInfo.setStatus(Constants.DELETE);
			supplyInfo.setUpdater(user.getUserId());
			supplyInfo.setUpdateTime(DateUtil.getNowTimestamp());
			compSupplyService.saveOrUpdate(supplyInfo);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	
	/**
	 * 保存商贸类 产品销售情况统计
	 * @param request
	 * @param serNo
	 * @return
	 */
	@RequestMapping("saveSalesStat.json")
	@ResponseBody
	public ResultDto saveSalesStat(HttpServletRequest request, String jsonArray, String serNo) {
		ResultDto re = returnFail();
		User user = HttpUtil.getCurrentUser(request);
		try{
			compSalesStatService.saveStatData(serNo, jsonArray, user.getUserId());
			
			re.setMsg("保存成功");
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 保存/修改 企业经营状况 分析评价信息
	 * @param request
	 * @return
	 */
	@RequestMapping("saveRunAnalysis.json")
	@ResponseBody
	public ResultDto saveRunAnalysis(HttpServletRequest request, String serNo) {
		ResultDto re = returnFail("保存失败");
		String supplyInfo = request.getParameter("supplyInfo");
		String rawMaterial = request.getParameter("rawMaterialInfo");
		String produceInfo = request.getParameter("produceInfo");
		String productInfo = request.getParameter("productInfo");
		String environmentInfo = request.getParameter("environmentInfo");
		String qualifyChange = request.getParameter("qualifyChangeInfo");
		String salesInfo = request.getParameter("productSalesInfo");
		String salesStatData = request.getParameter("salesStatInfo");
		try {

			CompRunAnalysis entity = compAnalysisService.selectSingleByProperty("serNo", serNo);
			if (entity == null) {
				entity = new CompRunAnalysis();
				entity.setSerNo(serNo);
			}
			if (supplyInfo!=null) entity.setSupplyInfo(supplyInfo);
			if (rawMaterial!=null) entity.setRawMaterialInfo(rawMaterial);
			if (produceInfo!=null) entity.setProduceInfo(produceInfo);
			if (productInfo!=null) entity.setProductInfo(productInfo);
			if (environmentInfo!=null) entity.setEnvironmentInfo(environmentInfo);
			if (qualifyChange!=null) entity.setQualifyChangeInfo(qualifyChange);
			if (salesInfo!=null) entity.setProductSalesInfo(salesInfo);

			User user = HttpUtil.getCurrentUser(request);
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			compAnalysisService.saveOrUpdate(entity);
			// 保存 销售统计数据（商贸类）
			if (!StringUtil.isEmptyString(salesStatData))
				compSalesStatService.saveStatData(serNo, salesStatData, user.getUserId());

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 获取 采购环节 主要原材料列表
	 * @param serNo 任务流水号
	 * @return
	 */
	@RequestMapping("getRawMaterialList.do")
	@ResponseBody
	public ResultDto getRawMaterialList(HttpServletRequest request, String serNo){
		ResultDto re = returnFail("获取原材料列表失败");
		if(StringUtil.isEmptyString(serNo)){
			re.setMsg("任务流水号为空");
			return re;
		}
		try {
			List<CompRawMaterial> list =
					compRawMaterialService.selectByProperty("serNo", serNo, "PERIOD ASC");
			Collections.sort(list, new Comparator<CompRawMaterial>() {
				@Override
				public int compare(CompRawMaterial arg0, CompRawMaterial arg1) {					
					return (int) (arg1.getPurchaseAmt() - arg0.getPurchaseAmt());
										
				}
			});
			request.setAttribute("serNo", serNo);
			re = returnSuccess();
			re.setRows(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 新增/修改 主要原材料信息
	 * @param request
	 * @return
	 */
	@RequestMapping("saveRawMaterial.json")
	@ResponseBody
	public ResultDto saveRawMaterial(HttpServletRequest request) {
		ResultDto re = returnFail("保存失败");
		String jsonData = request.getParameter("jsonArray");
		try {
			List<CompRawMaterial> entityList =
					JacksonUtil.fromJson(jsonData, new TypeReference<List<CompRawMaterial>>() {});
			if(CollectionUtils.isEmpty(entityList))
				return returnFail("请先填写相关信息！");

			User user = HttpUtil.getCurrentUser(request);
			Timestamp nowTimestamp = DateUtil.getNowTimestamp();
			for (CompRawMaterial entity : entityList) {
				if (entity.getId() == null){
					entity.setCreater(user.getUserId());
					entity.setCreateTime(nowTimestamp);
				}
				entity.setUpdater(user.getUserId());
				entity.setUpdateTime(nowTimestamp);
				entity.setStatus(Constants.NORMAL);
				compRawMaterialService.saveOrUpdate(entity);
			}

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除 主要原材料
	 * @param request
	 * @return
	 */
	@RequestMapping("deleteRawMaterial.json")
	@ResponseBody
	public ResultDto deleteRawMaterial(HttpServletRequest request, Integer id) {
		ResultDto re = returnFail("删除失败");
		if (id == null || id == 0) {
			re.setMsg("传输数据内容异常，请重试");
			return re;
		}
		User user = HttpUtil.getCurrentUser(request);
		try {
			CompRawMaterial rawMaterial = compRawMaterialService.findByPrimaryKey(id);
			rawMaterial.setStatus(Constants.DELETE);
			rawMaterial.setUpdater(user.getUserId());
			rawMaterial.setUpdateTime(DateUtil.getNowTimestamp());
			compRawMaterialService.saveOrUpdate(rawMaterial);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 获取 生产环节 生产设备详情列表（纺织业）
	 * @param serNo 任务流水号
	 * @return
	 */
	@RequestMapping("getEquipmentList.json")
	@ResponseBody
	public ResultDto getEquipmentList(HttpServletRequest request, String serNo){
		ResultDto re = returnFail("获取设备详情失败");
		if(StringUtil.isEmptyString(serNo)){
			re.setMsg("任务流水号为空");
			return re;
		}
		try {
			List<CompEquipment> list = compEquipmentService.selectByProperty("serNo", serNo);
			request.setAttribute("serNo", serNo);
			re = returnSuccess();
			re.setRows(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除 生产环节 生产设备详情列表（纺织业）
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("deleteEquipment.json")
	@ResponseBody
	public ResultDto deleteEquipment(HttpServletRequest request, Integer id) {
		ResultDto re = returnFail("删除失败");
		if (id == null || id == 0) {
			re.setMsg("传输数据内容异常，请重试");
			return re;
		}
		User user = HttpUtil.getCurrentUser(request);
		try {
			CompEquipment equipment = compEquipmentService.findByPrimaryKey(id);
			equipment.setStatus(Constants.DELETE);
			equipment.setUpdater(user.getUserId());
			equipment.setUpdateTime(DateUtil.getNowTimestamp());
			compEquipmentService.saveOrUpdate(equipment);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 保存/修改 生产环节 生产设备详情列表（纺织业）
	 * @param request
	 * @return
	 */
	@RequestMapping("saveEquipment.json")
	@ResponseBody
	public ResultDto saveEquipment(HttpServletRequest request) {
		ResultDto re = returnFail("保存失败");
		String jsonData = request.getParameter("jsonArray");
		try {
			List<CompEquipment> entityList = 
					JacksonUtil.fromJson(jsonData, new TypeReference<List<CompEquipment>>() {});
			if (entityList == null || entityList.size() <= 0) {
				re.setMsg("传输数据内容为空，请重试");
				return re;
			}

			User user = HttpUtil.getCurrentUser(request);
			Timestamp nowTime = DateUtil.getNowTimestamp();
			for (CompEquipment entity : entityList) {
				Integer id = entity.getId();
				if(id == null){
					entity.setCreater(user.getUserId());
					entity.setCreateTime(nowTime);
				}
				entity.setUpdater(user.getUserId());
				entity.setUpdateTime(nowTime);
				entity.setStatus(Constants.NORMAL);
				compEquipmentService.saveOrUpdate(entity);
			}

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 跳转到 企业上下游情况
	 * @param serNo 流水号
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping(value = "toProductPage.do", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView toProductPage(String serNo, String customerNo){
		ModelAndView mav = new ModelAndView("views/survey/liquidity/common/toProductPage");
		mav.addObject("serNo", serNo);
		mav.addObject("customerNo", customerNo);
		return mav;
	}
	
	
	/**
	 * 跳转到 企业管理情况
	 * @param serNo 流水号
	 * @return
	 */
	@RequestMapping(value = "toManagePage.do", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView toManagePage(String serNo, @RequestParam(value = "type", required = false)String type){
		ModelAndView mav = new ModelAndView("views/survey/liquidity/common/toManagePage");
		mav.addObject("serNo", serNo);
		mav.addObject("type", type);
		return mav;
	}
	
	
	/**
	 * 跳转到 企业管理情况 相应界面
	 * @param serNo 流水号
	 * @return
	 */
	@RequestMapping(value = "managePage.do", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView managePage(String serNo, String type){
		ModelAndView mav = new ModelAndView("views/survey/liquidity/common/visitRecord");
		try{
			mav.addObject("serNo", serNo);
			mav.addObject("type", type);
			VisitRecord entity = visitRecordService.getBySerNoAndType(serNo, type);
			mav.addObject("visitRecord", entity);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
	
	/**
	 * 保存/修改 企业相关人员拜访记录
	 * @param request
	 * @param serNo
	 * @param type
	 * @return
	 */
	@RequestMapping(value = "saveVisitRecord.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto saveVisitRecord(HttpServletRequest request,String serNo, String type){
		ResultDto re = returnFail("保存失败！");
		String visitRecord = request.getParameter("visitRecord");
		try{
			VisitRecord entity = JacksonUtil.fromJson(visitRecord, VisitRecord.class);
			if(entity == null){
				return returnFail("数据传输异常，请重试！");
			}
			
			VisitRecord oldInfo = visitRecordService.getBySerNoAndType(serNo, type);
			if(oldInfo != null){
				entity.setId(oldInfo.getId());
			}
			
			User user = HttpUtil.getCurrentUser(request);
			if(entity.getId() == null){
				entity.setCreater(user.getUserId());
				entity.setCreateTime(DateUtil.getNowTimestamp());
			}
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			visitRecordService.saveOrUpdate(entity);
			
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
}
