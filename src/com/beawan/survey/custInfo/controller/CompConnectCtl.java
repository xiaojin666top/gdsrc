package com.beawan.survey.custInfo.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.base.entity.User;
import com.beawan.base.service.ISysDicSV;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.SysConstants.ExceptionConstant;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.Pagination;
import com.beawan.core.ResultDto;
import com.beawan.dtmp.service.DtmpService;
import com.beawan.qcc.dto.BreakThroughInvesDto;
import com.beawan.qcc.dto.QccInvestThroughDto;
import com.beawan.qcc.service.SynchronizationQCCService;
import com.beawan.survey.custInfo.bean.CompBaseDesc;
import com.beawan.survey.custInfo.bean.CompConnetCm;
import com.beawan.survey.custInfo.bean.CompRunAnalysis;
import com.beawan.survey.custInfo.entity.CompInvestment;
import com.beawan.survey.custInfo.service.CompInvestmentService;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.beawan.survey.custInfo.service.ICompConnectSV;
import com.beawan.survey.custInfo.service.compRunAnalysisService;
import com.beawan.survey.project.dto.SubProjectDto;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.platform.util.DateUtil;
import com.platform.util.GsonUtil;
import com.platform.util.HttpUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;


@Controller
@UserSessionAnnotation
@RequestMapping({ "/survey/cust" })
public class CompConnectCtl extends BaseController {
	
	@Resource
	private ICompBaseSV compBaseSV;
	@Resource
	private ICompConnectSV compConnectSV;
	@Resource
	private ISysDicSV sysDicSV;
	@Resource
	private CompInvestmentService compInvestmentService;
	@Resource
	private SynchronizationQCCService synchronizationQCCService;
	@Resource
	private compRunAnalysisService compAnalysisService;
	@Resource
	private DtmpService dtmpService;
	/**
	 * @Description (编辑企业客户基本概况关联企业跳转)
	 * @param request
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping("compConnectCm.do")
	public ModelAndView compConnectCm(String customerNo) {
		ModelAndView mav = new ModelAndView("views/survey/common/compConnectCm");
		try {
			CompBaseDesc compBaseDesc = compBaseSV.findCompBaseDescByCustNo(customerNo);
			if (null != compBaseDesc)
				mav.addObject("relatedCompanies", compBaseDesc.getRelatedCompanies());
			mav.addObject("customerNo", customerNo);
			// 加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.STD_SY_CL};
			mav.addObject("dicData", sysDicSV.queryMapListByTypes(optTypes));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}

	/**
	 * 获取关联企业信息
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping(value = "getRelatedCompList.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto getRelatedCompList(String customerNo){
		ResultDto re = returnFail("获取关联企业列表失败");
		if(StringUtil.isEmptyString(customerNo)) return returnFail("客户号为空");
		try {
			
			//dtmpService.syncCompConnetCm("CM067202008110001205", "CM20201016026069");
			List<CompConnetCm> entityList = compConnectSV.findCompConnectCmByCustNo(customerNo);

			re.setRows(entityList);
			re.setMsg("获取数据成功！");
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 保存/修改 关联企业信息
	 * @param request
	 * @return
	 */
	@RequestMapping("saveCompConnectCm.json")
	@ResponseBody
	public ResultDto saveCompConnectCm(HttpServletRequest request) {
		ResultDto re = returnFail("保存失败");
		String jsonData = request.getParameter("jsonArray");
		try {
			/*Gson gson = GsonUtil.getGson();
			List<CompConnetCm> entityList = gson.fromJson(jsonData,
					new TypeToken<ArrayList<CompConnetCm>>() {}.getType());*/
			
			List<CompConnetCm> entityList = JacksonUtil.fromJson(jsonData,new TypeReference<List<CompConnetCm>>() {});
			if (entityList == null || entityList.size() <= 0) {
				re.setMsg("传输数据内容为空，请重试");
				return re;
			}

			User user = HttpUtil.getCurrentUser(request);
			Timestamp nowTime = DateUtil.getNowTimestamp();
			for (CompConnetCm equity : entityList) {
				Long id = equity.getId();
				if (id == null) {
					equity.setCreater(user.getUserId());
					equity.setCreateTime(nowTime);
				}
				equity.setUpdater(user.getUserId());
				equity.setUpdateTime(nowTime);
				equity.setStatus(Constants.NORMAL);
				compConnectSV.saveCompConnectCm(equity);
			}

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (DataAccessException e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return re;
	}


	/**
	 * 删除 关联企业信息
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("deleteCompConnectCm.json")
	@ResponseBody
	public ResultDto deleteCompConnectCm(HttpServletRequest request, Long id){
		ResultDto re = returnFail("删除数据异常！");
		try{
			User user = HttpUtil.getCurrentUser();
			CompConnetCm entity = compConnectSV.findById(id);
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			entity.setStatus(Constants.DELETE);
			compConnectSV.saveCompConnectCm(entity);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除数据成功！");
		} catch (DataAccessException e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return re;
	}


	/**
	 * 保存 关联企业情况说明
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveCompConnectCmAnalysis.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto saveCompConnectCmAnalysis(HttpServletRequest request){
		ResultDto re = returnFail("保存失败");
		String customerNo = request.getParameter("customerNo");
		String relatedCompanies = request.getParameter("relatedCompanies");
		try {
			User user = HttpUtil.getCurrentUser(request);
			CompBaseDesc entity = compBaseSV.findCompBaseDescByCustNo(customerNo);
			if(entity == null){
				entity = new CompBaseDesc();
				entity.setCustomerNo(customerNo);
				entity.setCreater(user.getUserId());
				entity.setCreateTime(DateUtil.getNowTimestamp());
			}
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			entity.setRelatedCompanies(relatedCompanies);
			compBaseSV.saveCompBaseDesc(entity);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	/***********************************  企业对外投资信息   ************************************************/
	
	/**
	 * 企业对外投资界面跳转
	 * @param serNo
	 * @param customerNo
	 * @return
	 */
	@RequestMapping("investment.do")
	public ModelAndView investment(String serNo, String customerNo) {
		ModelAndView mav = new ModelAndView("views/survey/common/compInvestment");
		CompRunAnalysis entity = compAnalysisService.selectSingleByProperty("serNo", serNo);
		if (entity != null)
			mav.addObject("relatedCompanies", entity.getRelatedCompanies());
		mav.addObject("customerNo", customerNo);
		mav.addObject("serNo", serNo);
		return mav;
	}
	
	/***
	 * 获取对外投资分页数据
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "getInvestPager.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto getInvestPager(HttpServletRequest request,String customerNo,
			@RequestParam(value="page", defaultValue="1") int page,
			@RequestParam(value="pageSize", defaultValue="10") int pageSize){
		ResultDto re = returnFail("获取对外投资数据失败");
		try{
			Pagination<CompInvestment> pager = compInvestmentService.getInvestPager(customerNo, page, pageSize);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setTotal(pager.getRowsCount());
			re.setRows(pager.getItems());
			re.setMsg("获取数据成功");
		}catch(Exception e){
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 跳转对外投资穿透
	 * @param request
	 * @param customerNo
	 * @return
	 */
	@RequestMapping("investThrough.do")
	public ModelAndView investThrough(HttpServletRequest request, String customerNo) {
		ModelAndView mav = new ModelAndView("views/survey/common/compInvestThrough");
		mav.addObject("customerNo", customerNo);
		return mav;
	}
	
	/***
	 * 获取股权穿透数据结构
	 * @param request
	 * @param customerNo
	 * @param page
	 * @param pageSize
	 * @return
	 */
	@RequestMapping(value = "getInvestThrough.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto getInvestThrough(HttpServletRequest request,String customerNo){
		ResultDto re = returnFail("获取对外投资数据失败");
		try{
			QccInvestThroughDto through = synchronizationQCCService.getInvestmentThrough("", customerNo);
			if(through==null){
				re.setMsg("当前企业不存在对外投资穿透信息");
				return re;
			}
			if(!"Y".equals(through.getFindMatched())){
				re.setMsg("无法穿透出企业对外投资");
				return re;
			}
			List<BreakThroughInvesDto> breakThroughList = through.getBreakThroughList();
			re.setRows(breakThroughList);
			re.setMsg("获取对外投资穿透成功");
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		}catch(Exception e){
			e.printStackTrace();
		}
		return re;
	}
}
