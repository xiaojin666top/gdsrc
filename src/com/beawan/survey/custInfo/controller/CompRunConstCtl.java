package com.beawan.survey.custInfo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.base.service.ISysDicSV;
import com.beawan.base.service.ISysTreeDicSV;
import com.beawan.common.SysConstants;
import com.beawan.common.SysConstants.ExceptionConstant;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.library.bean.InduSituation;
import com.beawan.library.service.IIndustrySV;
import com.beawan.library.service.IIndustrySituationSV;
import com.beawan.survey.custInfo.bean.CompRunConst;
import com.beawan.survey.custInfo.bean.CompRunConstSPDetail;
import com.beawan.survey.custInfo.bean.CompRunConstSPStruc;
import com.beawan.survey.custInfo.service.ICompRunSV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.BeanUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/survey/cust" })
public class CompRunConstCtl {

	@Resource
	protected ITaskSV taskSV;
	@Resource
	private ICompRunSV compRunSV;
	@Resource
	protected IIndustrySV industrySV;
	@Resource
	private ISysDicSV sysDicSV;
	@Resource
	private ISysTreeDicSV sysTreeDicSV;
	@Resource
	private IIndustrySituationSV indusSituationSV;
	
	/**
	 * @Description (流动资金贷款（建筑业）-行业分析页面跳转)
	 * @param request
	 * @param response
	 * @param taskId 任务ID
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping("compRunConstIndustry.do")
	public String compRunConstIndustry(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo) {
		
		try {
			
			Task task = this.taskSV.getTaskById(taskId);
			if (StringUtil.isEmptyString(customerNo)) {
				customerNo = task.getCustomerNo();
			}
			String indusCode=task.getIndustry();
			
			String industry = sysTreeDicSV.getFullNameByEnName(task.getIndustry(), 
					SysConstants.TreeDicConstant.GB_IC_4754_2017);
			
			CompRunConst compRunConst = compRunSV.queryRunConstByTIdAndNo(taskId, customerNo);
			
			InduSituation induSituation=indusSituationSV.findInduSituationByName(indusCode);
			
			String situation="";
			
			if(null!=compRunConst&&null!=compRunConst.getIndusAnalysis()&&!"".equals(compRunConst.getIndusAnalysis()))
				situation=compRunConst.getIndusAnalysis();
			else if(null!=induSituation) {
				situation=induSituation.getSituation();
			}
			
			request.setAttribute("newsituation", situation);
			request.setAttribute("industry", industry);
			request.setAttribute("taskId", taskId);
			request.setAttribute("customerNo", customerNo);
			request.setAttribute("compRunConst", compRunConst);
			
			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.STD_INDUSTRY_POLICY};
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/survey/liquidity/const/industry";
	}
	
	/**
	 * @Description (流动资金贷款（建筑业）-保存企业经营状况-行业分析信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveRunConstIndustry.do")
	@ResponseBody
	public String saveRunConstIndustry(HttpServletRequest request, HttpServletResponse response) {
		
		String compRunInfo = request.getParameter("compRunConst");
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			
			JSONObject jsonObject = JSONObject.fromObject(compRunInfo);
			CompRunConst newCompRun = (CompRunConst) JSONObject.toBean(jsonObject,
					CompRunConst.class);
			CompRunConst oldCompRun = compRunSV.queryRunConstByTIdAndNo(newCompRun.getTaskId(),
					newCompRun.getCustomerNo());
			if(oldCompRun != null){
				oldCompRun = (CompRunConst) BeanUtil.mergeProperties(newCompRun, 
						oldCompRun, jsonObject);
			}else
				oldCompRun = newCompRun;
				
			compRunSV.saveRunConst(oldCompRun);
			
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (流动资金贷款（建筑业）-企业经营状况信息页面跳转)
	 * @param request
	 * @param response
	 * @param taskId 任务ID
	 * @return
	 */
	@RequestMapping("compRunConst.do")
	public String compRunConst(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo) {
		
		try {
			
			Task task = this.taskSV.getTaskById(taskId);
			request.setAttribute("task", task);
			
			if (StringUtil.isEmptyString(customerNo)) {
				customerNo = task.getCustomerNo();
			}
			
			CompRunConst compRunConst = compRunSV.queryRunConstByTIdAndNo(taskId, customerNo);
			request.setAttribute("compRunConst", compRunConst);
			
			//加载字典数据
			String[] optTypes = {
					SysConstants.BsDicConstant.STD_CONST_QUALI_TYPE, 
					SysConstants.BsDicConstant.STD_PROJECT_TAKE_TYPE};
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/survey/liquidity/const/compRunInfo";
	}
	
	/**
	 * @Description (流动资金贷款（建筑业）-获取企业在手已签工程列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getRunConstProj.do")
	@ResponseBody
	public String getRunConstProj(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo, String type) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<?> dataGrid = null;
		try {
			if("detail".equals(type)){
				//在手已签工程明细
				dataGrid = compRunSV.queryRunConstSPDetailByTIdAndNo(taskId, customerNo);
			}else if("struc".equals(type)){
				//在手已签工程结构
				dataGrid = compRunSV.queryRunConstSPStrucByTIdAndNo(taskId, customerNo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jRequest.serialize(dataGrid, true);
	}
	
	/**
	 * @Description (流动资金贷款（建筑业）-保存企业在手已签工程信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveRunConstProj.do")
	@ResponseBody
	public String saveRunConstProj(HttpServletRequest request, HttpServletResponse response,
			String type, String jsonData) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			if("detail".equals(type)){
				//在手已签工程明细
				compRunSV.saveRunConstSPDetail(JacksonUtil.fromJson(jsonData,
						CompRunConstSPDetail.class));
			}else if("struc".equals(type)){
				//在手已签工程结构
				compRunSV.saveRunConstSPStruc(JacksonUtil.fromJson(jsonData,
						CompRunConstSPStruc.class));
			}
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (流动资金贷款（建筑业）-删除企业在手已签工程信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("deleteRunConstProj.do")
	@ResponseBody
	public String deleteRunConstProj(HttpServletRequest request, HttpServletResponse response,
			String type, long id) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			if("detail".equals(type)){
				//在手已签工程明细
				CompRunConstSPDetail temp = compRunSV.queryRunConstSPDetailById(id);
				compRunSV.deleteRunConstSPDetail(temp);
			}else if("struc".equals(type)){
				//在手已签工程结构
				CompRunConstSPStruc temp = compRunSV.queryRunConstSPStrucById(id);
				compRunSV.deleteRunConstSPStruc(temp);
			}
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (流动资金贷款（建筑业）-保存企业经营信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveCompRunConst.do")
	@ResponseBody
	public String saveCompRunConst(HttpServletRequest request, HttpServletResponse response, 
			String jsonData) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			
			JSONObject jsonObject = JSONObject.fromObject(jsonData);
			CompRunConst newCompRun = (CompRunConst) JSONObject.toBean(jsonObject,
					CompRunConst.class);
			CompRunConst oldCompRun = compRunSV.queryRunConstByTIdAndNo(newCompRun.getTaskId(),
					newCompRun.getCustomerNo());
			if(oldCompRun != null){
				oldCompRun = (CompRunConst) BeanUtil.mergeProperties(newCompRun, 
						oldCompRun, jsonObject);
			}else
				oldCompRun = newCompRun;
				
			compRunSV.saveRunConst(oldCompRun);
			
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
}
