package com.beawan.survey.custInfo.controller;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.base.entity.IndusPolicySet;
import com.beawan.base.service.IIndusPolicySetSV;
import com.beawan.base.service.ISysDicSV;
import com.beawan.base.service.ISysTreeDicSV;
import com.beawan.common.SysConstants;
import com.beawan.common.SysConstants.ExceptionConstant;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.library.bean.InduSituation;
import com.beawan.library.bean.IndustryData;
import com.beawan.library.service.IIndustrySV;
import com.beawan.library.service.IIndustrySituationSV;
import com.beawan.survey.custInfo.bean.CompRunCustomer;
import com.beawan.survey.custInfo.bean.CompRunGeneral;
import com.beawan.survey.custInfo.bean.CompRunPower;
import com.beawan.survey.custInfo.bean.CompRunPrdSale;
import com.beawan.survey.custInfo.service.ICompRunSV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.StringUtil;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/survey/cust/general" })
public class CompRunGeneralCtl {
	
	private static final Logger log = Logger.getLogger(CompRunGeneralCtl.class);
	
	@Resource
	protected ITaskSV taskSV;

	@Resource
	private ICompRunSV compRunSV;
	
	@Resource
	private ISysDicSV sysDicSV;
	
	@Resource
	private ISysTreeDicSV sysTreeDicSV;
	
	@Resource
	private IIndusPolicySetSV indusPolicySetSV;

	@Resource
	private IIndustrySV industrySV;
	
	@Resource
	private IIndustrySituationSV indusSituationSV;
	/**
	 * @Description (经营基本情况页面跳转)
	 * @param request
	 * @param response
	 * @param taskId 任务ID
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping("compRunBase.do")
	public String compRunBase(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo) {
		
		try {
			
			Task task = this.taskSV.getTaskById(taskId);
			request.setAttribute("task", task);
			
			if (StringUtil.isEmptyString(customerNo)) {
				customerNo = task.getCustomerNo();
			}
			String indusCode=task.getIndustry();
			
			CompRunGeneral compRun = compRunSV.queryRunGeneralByTIdAndNo(taskId, customerNo);
			
			InduSituation induSituation=indusSituationSV.findInduSituationByName(indusCode);
			
			String situation="";
			
			if(null!=compRun&&null!=compRun.getIndusAnalysis()&&!"".equals(compRun.getIndusAnalysis()))
				situation=compRun.getIndusAnalysis();
			else if(null!=induSituation) {
				situation=induSituation.getSituation();
			}
				
			request.setAttribute("newsituation", situation);
			request.setAttribute("compRun", compRun);
			
		} catch (Exception e) {
			log.error("经营基本情况页面跳转异常 taskId：" + taskId, e);
		}
		
		return "views/survey/general/compRunBase";
	}
	
	/**
	 * @Description (保存企业经营状况-经营基本信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveCompRunBase.do")
	@ResponseBody
	public String saveCompRunBase(HttpServletRequest request, HttpServletResponse response) {
		
		String compRunBase = request.getParameter("compRunBase");
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);
		
		try {
			compRunSV.saveRunGeneral(compRunBase);
			json.put("result", true);
		} catch (Exception e) {
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			log.error("保存企业经营状况-经营基本信息异常：", e);
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (企业经营状况-供应情况页面跳转)
	 * @param request
	 * @param response
	 * @param taskId 任务ID
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping("compRunSupply.do")
	public String compRunSupply(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo) {
		
		try {
			
			Task task = this.taskSV.getTaskById(taskId);
			request.setAttribute("task", task);
			
			if (StringUtil.isEmptyString(customerNo)) {
				customerNo = task.getCustomerNo();
			}
			
			CompRunGeneral compRun = compRunSV.queryRunGeneralByTIdAndNo(taskId, customerNo);
			
			request.setAttribute("compRun", compRun);
			
		} catch (Exception e) {
			log.error("企业经营状况-供应情况页面跳转异常 taskId：" + taskId, e);
		}
		
		return "views/survey/general/compRunSupply";
	}
	
	/**
	 * @Description (企业经营状况-生产情况页面跳转)
	 * @param request
	 * @param response
	 * @param taskId 任务ID
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping("compRunProduce.do")
	public String compRunProduce(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo) {
		
		try {
			
			Task task = this.taskSV.getTaskById(taskId);
			request.setAttribute("task", task);
			
			if (StringUtil.isEmptyString(customerNo)) {
				customerNo = task.getCustomerNo();
			}
			
			CompRunGeneral compRun = compRunSV.queryRunGeneralByTIdAndNo(taskId, customerNo);
			
			request.setAttribute("compRun", compRun);
			
		} catch (Exception e) {
			log.error("企业经营状况-生产情况页面跳转异常 taskId：" + taskId, e);
		}
		
		return "views/survey/general/compRunProduce";
	}
	
	/**
	 * @Description (企业经营状况-销售情况页面跳转)
	 * @param request
	 * @param response
	 * @param taskId 任务ID
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping("compRunSales.do")
	public String compRunSales(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo) {
		
		try {
			
			Task task = this.taskSV.getTaskById(taskId);
			request.setAttribute("task", task);
			
			if (StringUtil.isEmptyString(customerNo)) {
				customerNo = task.getCustomerNo();
			}
			
			CompRunGeneral compRun = compRunSV.queryRunGeneralByTIdAndNo(taskId, customerNo);
			request.setAttribute("compRun", compRun);
			
			//下游客户列表
			List<CompRunCustomer> runCustomers = compRunSV.queryRunCustomerByTIdAndNo(taskId, customerNo);
			request.setAttribute("runCustomers", JSONArray.fromObject(runCustomers).toString());
			
			//产品销售列表
			List<CompRunPrdSale> runPrdSales = compRunSV.queryRunPrdSaleByTIdAndNo(taskId, customerNo);
			request.setAttribute("runPrdSales", JSONArray.fromObject(runPrdSales).toString());
			
		} catch (Exception e) {
			log.error("企业经营状况-销售情况页面跳转异常 taskId：" + taskId, e);
		}
		
		return "views/survey/general/compRunSales";
	}
	
	
	/**
	 * @Description (能源耗用及工资统计页面跳转)
	 * @param request
	 * @param response
	 * @param taskId 任务ID
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping("compRunPower.do")
	public String compRunPower(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo) {
		
		try {
			
			Task task = this.taskSV.getTaskById(taskId);
			request.setAttribute("task", task);
			
			if (StringUtil.isEmptyString(customerNo)) {
				customerNo = task.getCustomerNo();
			}
			
			CompRunGeneral compRun = compRunSV.queryRunGeneralByTIdAndNo(taskId, customerNo);
			//能源耗用及工资统计分析
			request.setAttribute("compRun", compRun);
			
			List<CompRunPower>  runPowers = compRunSV.queryRunPowerByTIdAndNo(taskId, customerNo);
			request.setAttribute("runPowers", JSONArray.fromObject(runPowers).toString());
			
			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.STD_ENERGY_AND_PAY};
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
			
		} catch (Exception e) {
			log.error("能源耗用及工资统计页面跳转异常 taskId：" + taskId, e);
		}
		
		return "views/survey/general/compRunPower";
	}
	
	/**
	 * @Description (保存企业主能源耗用及工人工资情况)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveRunPowers.do")
	@ResponseBody
	public String saveRunPowers(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo) {
		
		String runPowers = request.getParameter("runPowers");
		String energyPayAnalysis = request.getParameter("energyPayAnalysis");
		String energyStatMonth = request.getParameter("energyStatMonth");
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);
		
		try {
			
			CompRunGeneral compRun = compRunSV.queryRunGeneralByTIdAndNo(taskId, customerNo);
			if(compRun == null){
				compRun = new CompRunGeneral();
				compRun.setTaskId(taskId);
				compRun.setCustomerNo(customerNo);
			}
			compRun.setEnergyPayAnalysis(energyPayAnalysis);
			compRun.setEnergyStatMonth(energyStatMonth);
			compRunSV.saveRunGeneral(compRun);
			
			compRunSV.saveRunPowers(taskId, customerNo, runPowers);
			json.put("result", true);
		} catch (Exception e) {
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			log.error("保存企业主能源耗用及工人工资情况异常 taskId：" + taskId, e);
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (企业经营状况-行业分析页面跳转)
	 * @param request
	 * @param taskId 任务ID
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping("compRunIndustry.do")
	public String compRunIndustry(HttpServletRequest request, long taskId, String customerNo) {
		
		try {
			
			Task task = this.taskSV.getTaskById(taskId);
			request.setAttribute("task", task);
			
			if (StringUtil.isEmptyString(customerNo)) {
				customerNo = task.getCustomerNo();
			}
			
			String industry = sysTreeDicSV.getFullNameByEnName(task.getIndustry(), 
					SysConstants.TreeDicConstant.GB_IC_4754_2017);
			request.setAttribute("industry", industry);
			
			CompRunGeneral compRun = compRunSV.queryRunGeneralByTIdAndNo(taskId, customerNo);
			if(compRun == null)
				compRun = new CompRunGeneral();
			if(compRun.getIndusAnalysis()==null||"".equals(compRun.getIndusAnalysis())){
				InduSituation indusAnalysis = indusSituationSV.findInduSituationByName(task.getIndustry());
				if(indusAnalysis!=null)
					compRun.setIndusAnalysis(indusAnalysis.getSituation());
			}
			request.setAttribute("compRun", compRun);
			
			//本行行业政策
			IndusPolicySet ips = indusPolicySetSV.queryIndusPolicy(task.getIndustry(), "10");
			if(ips != null)
				compRun.setIndusMyPolicy(ips.getIndusPolicy());
			//国家行业政策
			ips = indusPolicySetSV.queryIndusPolicy(task.getIndustry(), "20");
			if(ips != null)
				compRun.setIndusComPolicy(ips.getIndusPolicy());
			
			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.STD_INDUSTRY_POLICY};
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
			
		} catch (Exception e) {
			log.error("企业经营状况-行业分析页面跳转异常：", e);
		}
		
		return "views/survey/general/industry";
	}
	
	/**
	 * @Description (跳转具体数据页面)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/getNewhtml.json")
	@ResponseBody
	public Map<String, Object> getNewhtml(HttpServletRequest request,
			HttpServletResponse response, String enName)
			throws UnsupportedEncodingException {
		
		Map<String, Object> model = new HashMap<String, Object>();
		
		IndustryData data = industrySV.getIndustryDataByEnName(enName);

		String url = "";
		if(data == null || data.getUrl() == null || "".equals(data.getUrl())){
			url = "noMess";
		}else{
			url = data.getUrl();
		}
		
		model.put("url", "views/industry/" + url);
		
		return model;	
	}
	
}
