package com.beawan.survey.custInfo.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.analysis.finansis.service.IFinanasisService;
import com.beawan.base.entity.QuickAnaly;
import com.beawan.base.entity.SysDic;
import com.beawan.base.entity.User;
import com.beawan.base.service.ISysDicSV;
import com.beawan.base.service.QuickAnalyService;
import com.beawan.common.BusinessException;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.SysConstants.ExceptionConstant;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.beawan.library.service.IIndustrySV;
import com.beawan.model.entity.AdmitResult;
import com.beawan.model.service.AdmitResultService;
import com.beawan.qcc.dto.BreakThroughDto;
import com.beawan.qcc.dto.QccActualControlDto;
import com.beawan.qcc.dto.QccBeneficiaryInfoDto;
import com.beawan.qcc.service.SynchronizationQCCService;
import com.beawan.survey.custInfo.bean.CompBaseDesc;
import com.beawan.survey.custInfo.bean.CompBaseEquity;
import com.beawan.survey.custInfo.bean.CompBaseItemChange;
import com.beawan.survey.custInfo.bean.CompPenalty;
import com.beawan.survey.custInfo.bean.CompRunAnalysis;
import com.beawan.survey.custInfo.bean.CompRunArea;
import com.beawan.survey.custInfo.bean.CompRunAreaCf;
import com.beawan.survey.custInfo.bean.CompRunAreaTd;
import com.beawan.survey.custInfo.bean.CompShiXinItems;
import com.beawan.survey.custInfo.bean.PersonBase;
import com.beawan.survey.custInfo.dto.CompBaseDto;
import com.beawan.survey.custInfo.dto.CompBaseManagerDto;
import com.beawan.survey.custInfo.dto.CompManagerInfoDto;
import com.beawan.survey.custInfo.dto.EquityEchartInfoDto;
import com.beawan.survey.custInfo.dto.EquityEchartReDto;
import com.beawan.survey.custInfo.entity.CompInnerManage;
import com.beawan.survey.custInfo.entity.CompInvestment;
import com.beawan.survey.custInfo.service.CompInnerManageService;
import com.beawan.survey.custInfo.service.CompInvestmentService;
import com.beawan.survey.custInfo.service.CompPenaltyService;
import com.beawan.survey.custInfo.service.CompShiXinItemsService;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.beawan.survey.custInfo.service.ICompRunSV;
import com.beawan.survey.custInfo.service.compRunAnalysisService;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.platform.util.DateUtil;
import com.platform.util.ExceptionUtil;
import com.platform.util.GsonUtil;
import com.platform.util.HttpUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/survey/cust/" })
public class CompBaseCtl extends BaseController {

	private static final Logger log = Logger.getLogger(CompBaseCtl.class);
	@Resource
	protected ITaskSV taskSV;
	@Resource
	protected ICmisInInvokeSV cmisInInvokeSV;
	@Resource
	protected IFinanasisService finanasisSV;
	@Resource
	protected IIndustrySV industrySV;
	@Resource
	private ICusBaseSV cusBaseSV;
	@Resource
	private ICompBaseSV compBaseSV;
	@Resource
	private ISysDicSV sysDicSV;
	@Resource
	private ICompRunSV  compRunSV;
	@Resource
	private CompInnerManageService manageService;
	@Resource
	private CompPenaltyService compPenaltyService;//行政处罚
	@Resource
	private CompShiXinItemsService compShiXinItemsService;//失信人
	@Resource
	private AdmitResultService admitResultService;
	@Resource
	private SynchronizationQCCService synchronizationQCCService;
	@Resource
	private compRunAnalysisService compAnalysisService;
	@Resource
	private CompInvestmentService compInvestmentService;
	@Resource
	private QuickAnalyService quickAnalyService;
	
	/**
	 * @Description (企业基本信息页面跳转)
	 * @param serNo
	 * @return
	 */
	@RequestMapping("compCustBase.do")
	public ModelAndView compCustBase( String customerNo,String serNo) {
		ModelAndView mav = new ModelAndView("views/survey/common/compBaseInfo");
		try {
			
			CompBaseDto compBase = compBaseSV.findCompBaseByCustNo(customerNo);
			//如果客户基本信息表 无数据 则新增
			if (compBase == null || compBase.getCustomerNo() == null) {
				compBase = new CompBaseDto();
				//还未进入营销任务是进行查看
				CusBase cusBase = cusBaseSV.queryByCusNo(customerNo);
				if(cusBase == null){
					log.error("客户号为：" + customerNo + "信息不存在！");
					ExceptionUtil.throwException("客户号为：" + customerNo + "信息不存在！");
				}
				User user = HttpUtil.getCurrentUser();
				compBase.setCreater(user.getUserId());
				compBase.setCreateTime(DateUtil.getNowTimestamp());
				compBase.setCustomerNo(cusBase.getCustomerNo());
				compBase.setCustomerName(cusBase.getCustomerName());
				//GGG 开始调用企查查接口 获取企业基本信息

				compBaseSV.saveCompBase(compBase);
			}

			//加载字典数据
			String[] optTypes = {
					SysConstants.BsDicConstant.STD_CUS_BIZ_STATUS,
					SysConstants.BsDicConstant.STD_IS_MB_HOLDER,
					SysConstants.BsDicConstant.STD_SY_GB00015,
					SysConstants.BsDicConstant.STD_MB_ACC_TYPE,
					SysConstants.BsDicConstant.STD_SY_COM01,
					SysConstants.BsDicConstant.RG_GRADE_LEVEL,
					SysConstants.BsDicConstant.STD_INDUSTRY_POLICY,
					SysConstants.BsDicConstant.STD_IS_DISCHARGE_PERMIT,
					SysConstants.BsDicConstant.STB_CUS_BIZ_TAXES,
					SysConstants.BsDicConstant.STB_CUS_BIZ_EP
			};
			mav.addObject("compBase", compBase);
			mav.addObject("serNo", serNo);
			mav.addObject("dicData", sysDicSV.queryMapListByTypes(optTypes));
			if(!StringUtil.isEmptyString(serNo)){
				Task task = taskSV.getTaskBySerNo(serNo);
				mav.addObject("loanType", task.getBusinessNo());
			}
		} catch (Exception e) {
			log.error("跳转企业基本信息异常！",e);
		}
		return mav;
	}
	
	/**
	 * @Description (营销阶段的企业基本信息页面跳转)
	 * @param serNo
	 * @return
	 */
	@RequestMapping("compCustBaseLr.do")
	public ModelAndView compCustBaseLr( String customerNo,String serNo) {
		ModelAndView mav = new ModelAndView("views/survey/common/compBaseInfoLr");
		try {
			
			CompBaseDto compBase = compBaseSV.findCompBaseByCustNo(customerNo);
			//如果客户基本信息表 无数据 则新增
			if (compBase == null || compBase.getCustomerNo() == null) {
				compBase = new CompBaseDto();
				//还未进入营销任务是进行查看
				CusBase cusBase = cusBaseSV.queryByCusNo(customerNo);
				if(cusBase == null){
					log.error("客户号为：" + customerNo + "信息不存在！");
					ExceptionUtil.throwException("客户号为：" + customerNo + "信息不存在！");
				}
				User user = HttpUtil.getCurrentUser();
				compBase.setCreater(user.getUserId());
				compBase.setCreateTime(DateUtil.getNowTimestamp());
				compBase.setCustomerNo(cusBase.getCustomerNo());
				compBase.setCustomerName(cusBase.getCustomerName());
				//GGG 开始调用企查查接口 获取企业基本信息

				compBaseSV.saveCompBase(compBase);
			}

			//加载字典数据
			String[] optTypes = {
					SysConstants.BsDicConstant.STD_CUS_BIZ_STATUS,
					SysConstants.BsDicConstant.STD_IS_MB_HOLDER,
					SysConstants.BsDicConstant.STD_SY_GB00015,
					SysConstants.BsDicConstant.STD_MB_ACC_TYPE,
					SysConstants.BsDicConstant.STD_SY_COM01,
					SysConstants.BsDicConstant.RG_GRADE_LEVEL,
					SysConstants.BsDicConstant.STD_INDUSTRY_POLICY,
					SysConstants.BsDicConstant.STD_IS_DISCHARGE_PERMIT,
					SysConstants.BsDicConstant.STB_CUS_BIZ_TAXES,
					SysConstants.BsDicConstant.STB_CUS_BIZ_EP
			};
			mav.addObject("compBase", compBase);
			mav.addObject("serNo", serNo);
			mav.addObject("dicData", sysDicSV.queryMapListByTypes(optTypes));
			if(!StringUtil.isEmptyString(serNo)){
				Task task = taskSV.getTaskBySerNo(serNo);
				mav.addObject("loanType", task.getBusinessNo());
			}
		} catch (Exception e) {
			log.error("跳转企业基本信息异常！",e);
		}
		return mav;
	}
	
	/**
	 * @Description (贷后阶段的企业基本信息页面跳转)
	 * @param serNo
	 * @return
	 */
	@RequestMapping("compCustBaseLf.do")
	public ModelAndView compCustBaseLf( String customerNo,String serNo) {
		ModelAndView mav = new ModelAndView("views/survey/common/compBaseInfoLf");
		try {
			
			CompBaseDto compBase = compBaseSV.findCompBaseByCustNo(customerNo);
			//如果客户基本信息表 无数据 则新增
			if (compBase == null || compBase.getCustomerNo() == null) {
				compBase = new CompBaseDto();
				//还未进入营销任务是进行查看
				CusBase cusBase = cusBaseSV.queryByCusNo(customerNo);
				if(cusBase == null){
					log.error("客户号为：" + customerNo + "信息不存在！");
					ExceptionUtil.throwException("客户号为：" + customerNo + "信息不存在！");
				}
				User user = HttpUtil.getCurrentUser();
				compBase.setCreater(user.getUserId());
				compBase.setCreateTime(DateUtil.getNowTimestamp());
				compBase.setCustomerNo(cusBase.getCustomerNo());
				compBase.setCustomerName(cusBase.getCustomerName());
				//GGG 开始调用企查查接口 获取企业基本信息

				compBaseSV.saveCompBase(compBase);
			}

			//加载字典数据
			String[] optTypes = {
					SysConstants.BsDicConstant.STD_CUS_BIZ_STATUS,
					SysConstants.BsDicConstant.STD_IS_MB_HOLDER,
					SysConstants.BsDicConstant.STD_SY_GB00015,
					SysConstants.BsDicConstant.STD_MB_ACC_TYPE,
					SysConstants.BsDicConstant.STD_SY_COM01,
					SysConstants.BsDicConstant.RG_GRADE_LEVEL,
					SysConstants.BsDicConstant.STD_INDUSTRY_POLICY,
					SysConstants.BsDicConstant.STD_IS_DISCHARGE_PERMIT,
					SysConstants.BsDicConstant.STB_CUS_BIZ_TAXES,
					SysConstants.BsDicConstant.STB_CUS_BIZ_EP
			};
			mav.addObject("compBase", compBase);
			mav.addObject("serNo", serNo);
			mav.addObject("dicData", sysDicSV.queryMapListByTypes(optTypes));
			if(!StringUtil.isEmptyString(serNo)){
				Task task = taskSV.getTaskBySerNo(serNo);
				mav.addObject("loanType", task.getBusinessNo());
			}
		} catch (Exception e) {
			log.error("跳转企业基本信息异常！",e);
		}
		return mav;
	}
	
	/**
	 * 获取企业基本信息---目前用于移动端
	 * @param request
	 * @param customerNo
	 * @return
	 */
	@RequestMapping("compCustBase.json")
	@ResponseBody
	public ResultDto compCustBaseJson(HttpServletRequest request,String customerNo) {
		ResultDto resultDto = returnFail("");
		try {
			//获取客户基本信息
			CompBaseDto compBase = compBaseSV.findCompBaseByCustNo(customerNo);
			//如果客户基本信息表 无数据 则新增-----------这个可以放在sv中
			if (compBase == null || compBase.getCustomerNo() == null) {
				compBase = new CompBaseDto();
				CusBase cusBase = cusBaseSV.queryByCusNo(customerNo);
				if(cusBase == null){
					log.error("客户号为：" + customerNo + "信息不存在！");
					ExceptionUtil.throwException("客户号为：" + customerNo + "信息不存在！");
				}
				User user = HttpUtil.getCurrentUser();
				compBase.setCreater(user.getUserId());
				compBase.setCreateTime(DateUtil.getNowTimestamp());
				compBase.setCustomerNo(cusBase.getCustomerNo());
				compBase.setCustomerName(cusBase.getCustomerName());
				//GGG 开始调用企查查接口 获取企业基本信息
				compBaseSV.saveCompBase(compBase);
			}
			resultDto = returnSuccess();
			resultDto.setRows(compBase);
		}catch (BusinessException e){
			log.error(e.getMessage());
			e.printStackTrace();
			resultDto = returnFail(e.getMessage());
		}catch (Exception e) {
			log.error("获取企业基本信息异常！",e);
		}
		return resultDto;
	}

	/**
	 * @Description (小微企业主贷款-借款人基本信息页面跳转)
	 * @param customerNo
	 * @return
	 */
	@RequestMapping("personBase.do")
	public ModelAndView personBase(String customerNo) {
		ModelAndView mav = new ModelAndView("views/survey/personalLoan/personBaseInfo");
		try {
			PersonBase personBase = compBaseSV.findPersonBaseByCustNo(customerNo);
			if(personBase == null){
				personBase = cmisInInvokeSV.queryPersonBase(customerNo);
			}
			mav.addObject("personBase", JacksonUtil.serialize(personBase));
			//加载字典数据
			String[] optTypes = {
					SysConstants.BsDicConstant.STD_SY_JB00035,
					SysConstants.BsDicConstant.STD_SY_GB00009,
					SysConstants.BsDicConstant.STD_SY_GB00005,
					SysConstants.BsDicConstant.STD_SY_GB00002,
					SysConstants.BsDicConstant.STD_SY_ODS0004,
					SysConstants.BsDicConstant.STD_SY_GB00003,
					SysConstants.BsDicConstant.STD_CUS_BIZ_STATUS};
			mav.addObject("dicData", sysDicSV.queryMapListByTypes(optTypes));
		} catch (Exception e) {
			log.error("借款人基本信息页面跳转异常", e);
		}
		return mav;
	}

	/**
	 * @Description (编辑企业客户基本信息股东信息跳转)
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping("compCustEquity.do")
	public ModelAndView compCustEquity( String customerNo) {
		ModelAndView mav = new ModelAndView("views/survey/common/compBaseEquity");
		try {
		
			CompBaseDesc compBaseDesc = compBaseSV.findCompBaseDescByCustNo(customerNo);
			if (null != compBaseDesc)
				mav.addObject("equityAnalysis", compBaseDesc.getEquityAnalysis());
			mav.addObject("customerNo", customerNo);
			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.STD_SY_COM01};
			mav.addObject("dicData", sysDicSV.queryMapListByTypes(optTypes));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
		
	}

	/**
	 * 跳转到失信人信息
	 * @param request
	 * @param customerNo
	 * @return
	 */
	@RequestMapping("compJudicial.do")
	public ModelAndView compJudicial(HttpServletRequest request, String customerNo) {
		ModelAndView mav = new ModelAndView("views/survey/common/compJudicial");
		mav.addObject("customerNo", customerNo);
		return mav;
	}

	/**
	 * 跳转到行政处罚信息
	 * @param request
	 * @param customerNo
	 * @return
	 */
	@RequestMapping("compSanction.do")
	public ModelAndView compSanction(HttpServletRequest request, String customerNo) {
		ModelAndView mav = new ModelAndView("views/survey/common/compSanction");
		mav.addObject("customerNo", customerNo);
		return mav;
	}

	/**
	 * 获取近若干年的行政处罚
	 * @param request
	 * @param customerNo
	 * @return
	 */
	@RequestMapping("getPaneltyList.json")
	@ResponseBody
	public ResultDto getPaneltyList(HttpServletRequest request, String customerNo) {
		ResultDto  re = returnFail("获取企业行政处罚数据失败");
		try {
			
			Calendar date = Calendar.getInstance();
			date.setTime(new Date());
			date.add(Calendar.YEAR, -1);
			Date OldDate = date.getTime();//去年当期
				
			List<CompPenalty> compPenaltyList = compPenaltyService.selectByProperty("customerNo", customerNo);
			List<CompPenalty> showCompPenalty = new ArrayList<CompPenalty>();
			if(!CollectionUtils.isEmpty(compPenaltyList)){
				for(CompPenalty compPenalty : compPenaltyList){
					if(!StringUtils.isEmpty(compPenalty.getPenaltyDate())){
						SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
						if(OldDate.compareTo(sf.parse(compPenalty.getPenaltyDate())) == -1){
							showCompPenalty.add(compPenalty);
						}
					}
				}
			}
			re = returnSuccess();
			re.setRows(showCompPenalty);
			re.setMsg("获取企业行政处罚数据成功");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 获取近若干年的失信信息
	 * @param customerNo
	 * @return
	 */
	@RequestMapping("getShiXinList.json")
	@ResponseBody
	public ResultDto getShiXinList(String customerNo) {
		ResultDto  re = returnFail("获取失信名单失败");
		try {
			List<CompShiXinItems> compShiXinItems = compShiXinItemsService.selectByProperty("customerNo", customerNo);
			re = returnSuccess();
			re.setRows(compShiXinItems);
			re.setMsg("获取失信名单成功");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 跳转到准入预警信息
	 * @param customerNo
	 * @return
	 */
	@RequestMapping("compAdmitWarn.do")
	public ModelAndView compAdmitWarn(String customerNo) {
		ModelAndView mav = new ModelAndView("views/survey/common/compAdmitWarn");
		mav.addObject("customerNo", customerNo);
		return mav;
	}

	/**
	 * 获取预警信息
	 * @param customerNo
	 * @return
	 */
	@RequestMapping("getAdmitWarnList.json")
	@ResponseBody
	public ResultDto getAdmitWarnList(String customerNo) {
		ResultDto  re = returnFail("获取准入预警信息失败");
		try {
			//获取是否存在准入信息
			Integer maxTimes = admitResultService.getMaxTimes(customerNo);
			Map<String, Object> params = new HashMap<>();
			params.put("times", maxTimes);
			params.put("custNo", customerNo);
			List<AdmitResult> admitResultList = admitResultService.selectByProperty(params);

			re = returnSuccess();
			re.setRows(admitResultList);
			re.setMsg("获取准入预警信息成功");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}


	/**
	 * @Description (编辑企业经营场所信息跳转)
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping("compRunArea.do")
	public ModelAndView compRunArea(String serNo, String customerNo) {
		ModelAndView mav = new ModelAndView("views/survey/common/compRunArea"); 
		try {
			CompRunAnalysis entity = compAnalysisService.selectSingleByProperty("serNo", serNo);
			if (entity != null){
				mav.addObject("businessPlace", entity.getBusinessPlace());    //原没有分土地和厂房的情况
//				mav.addObject("businessPlaceLand", entity.getBusinessPlaceLand());
//			    mav.addObject("businessPlacePlant", entity.getBusinessPlacePlant());
			}
			//获取预设项
			String[] items = new String[]{SysConstants.RG_PRE_ITEM.RUN_AREA_INFO};
			Map<String, List<QuickAnaly>> analyMap = quickAnalyService.queryAnalyByItem(items);
			mav.addObject("analyMap", GsonUtil.GsonString(analyMap));
			Task task = taskSV.getTaskBySerNo(serNo);
			mav.addObject("customerNo", customerNo);
			mav.addObject("serNo", serNo);
			mav.addObject("taskId", task.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
	

	/**
	 * @Description (编辑企业高管情况跳转)
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping("compBaseManager.do")
	public ModelAndView compBaseManager(String customerNo) {
		ModelAndView mav = new ModelAndView("views/survey/common/compBaseManager");
		try {
			CompBaseDesc compBaseDesc = compBaseSV.findCompBaseDescByCustNo(customerNo);
			if (null != compBaseDesc)
				mav.addObject("managersAnalysis", compBaseDesc.getManagersAnalysis());
			mav.addObject("customerNo", customerNo);

			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.STD_SY_GB00005,
					SysConstants.BsDicConstant.STD_SY_GB00002,
					SysConstants.BsDicConstant.STD_SY_ODS0004,
					SysConstants.BsDicConstant.STD_SY_JB00036,
					SysConstants.BsDicConstant.STD_SY_GB00003};
			mav.addObject("dicData", sysDicSV.queryMapListByTypes(optTypes));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}

	/**
	 * @Description (保存企业基本信息)
	 * @param compBase
	 * @return
	 */
	@RequestMapping("saveComBase.do")
	@ResponseBody
	public ResultDto saveCompBase(String compBase, String serNo) {
		ResultDto re = returnFail("保存失败");
		try {
			User user = HttpUtil.getCurrentUser();
			CompBaseDto baseInfo = JacksonUtil.fromJson(compBase, CompBaseDto.class);
			if (baseInfo == null){
				re.setMsg("数据内容为空，请重试");
				return re;
			}
			if (baseInfo.getCustomerNo() == null) {
				baseInfo.setCreater(user.getUserId());
				baseInfo.setCreateTime(DateUtil.getNowTimestamp());
			}

			baseInfo.setUpdater(user.getUserId());
			baseInfo.setUpdateTime(DateUtil.getNowTimestamp());
			baseInfo.setStatus(Constants.NORMAL);
			compBaseSV.saveCompBase(baseInfo);
			
			CompRunAnalysis entity = compAnalysisService.selectSingleByProperty("serNo", serNo);
			if (entity == null) {
				entity = new CompRunAnalysis();
				entity.setSerNo(serNo);
				entity.setCreater(user.getUserId());
				entity.setCreateTime(DateUtil.getNowTimestamp());
			}
			if(!StringUtil.isEmptyString(baseInfo.getEiaGrade())){
				if(baseInfo.getEiaGrade().equals("1021")||baseInfo.getEiaGrade().equals("1022")){
					entity.setEnvironmentInfo("已达标");
				}
			}
			
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			compAnalysisService.saveOrUpdate(entity);
			
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			
			
			re.setMsg("保存成功");
		} catch (DataAccessException e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * @Description (保存企业描述信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveCompBaseDesc.do")
	@ResponseBody
	public String saveCompBaseDesc(HttpServletRequest request, HttpServletResponse response) {

		String compBaseDescInfo = request.getParameter("compBaseDesc");
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			compBaseSV.saveCompBaseDesc(compBaseDescInfo);
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}

		return jRequest.serialize(json, true);
	}

	/**
	 * @Description (保存个人基本信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("savePersonBase.do")
	@ResponseBody
	public String savePersonBase(HttpServletRequest request, HttpServletResponse response,
								 String personBase) {

		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();

		try {
			compBaseSV.savePersonBase(personBase);
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", "保存个人基本信息异常!");
			log.error("保存个人基本信息异常", e);
		}
		return jRequest.serialize(json, true);
	}


	/**
	 * 获取 企业股权结构 列表
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping("getCustEquity.do")
	@ResponseBody
	public ResultDto getCustEquity(String customerNo) {
		ResultDto re = returnFail("获取企业股权情况信息失败");
		List<CompBaseEquity> dataGrid = null;//股东列表
		try {
			dataGrid = compBaseSV.findCompEquityByCustNo(customerNo);
			re = returnSuccess();
			re.setRows(dataGrid);
			re.setMsg("获取企业股权情况信息成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}


	/**
	 * @Description (获取企业股权变更信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getCustEquityChange.do")
	@ResponseBody
	public ResultDto getCustEquityChange(HttpServletRequest request, HttpServletResponse response) {
		List<CompBaseItemChange> changes = null;//股权变更列表
		ResultDto re = returnFail("获取企业股权变更信息失败");
		try {
			String customerNo = request.getParameter("customerNo");
			changes = compBaseSV.findChangeListByCustNo(customerNo);
			re = returnSuccess();
			re.setRows(changes);
			re.setMsg("获取企业股权变更信息成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 保存/修改 企业股权记录
	 * @param request
	 * @return
	 */
	@RequestMapping("saveCompEquity.json")
	@ResponseBody
	public ResultDto saveCompEquity(HttpServletRequest request) {
		ResultDto re = returnFail("保存失败");
		String jsonData = request.getParameter("jsonArray");
		try {
			Gson gson = GsonUtil.getGson(); // new Gson();
			List<CompBaseEquity> entityList = gson.fromJson(jsonData,
					new TypeToken<ArrayList<CompBaseEquity>>() {}.getType());
			if (entityList == null || entityList.size() <= 0) {
				re.setMsg("传输数据内容为空，请重试");
				return re;
			}

			User user = HttpUtil.getCurrentUser(request);
			Timestamp nowTime = DateUtil.getNowTimestamp();
			for (CompBaseEquity equity : entityList) {
				Long id = equity.getId();
				if (id == null) {
					equity.setCreater(user.getUserId());
					equity.setCreateTime(nowTime);
				}
				equity.setUpdater(user.getUserId());
				equity.setUpdateTime(nowTime);
				equity.setStatus(Constants.NORMAL);
				compBaseSV.saveCompEquity(equity);
			}

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (DataAccessException e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 保存/修改 企业股权变更记录
	 * @param request
	 * @return
	 */
	@RequestMapping("saveEquityChange.json")
	@ResponseBody
	public ResultDto saveEquityChange(HttpServletRequest request) {
		ResultDto re = returnFail("保存失败");
		String jsonData = request.getParameter("jsonArray");
		try {
			Gson gson = GsonUtil.getGson();
			List<CompBaseItemChange> entityList = gson.fromJson(jsonData,
					new TypeToken<ArrayList<CompBaseItemChange>>() {}.getType());
			if (entityList == null || entityList.size() <= 0) {
				re.setMsg("传输数据内容为空，请重试");
				return re;
			}

			User user = HttpUtil.getCurrentUser(request);
			Timestamp nowTime = DateUtil.getNowTimestamp();
			for (CompBaseItemChange entity : entityList) {
				Long id = entity.getId();
				if(id == null){
					entity.setCreater(user.getUserId());
					entity.setCreateTime(nowTime);
				}
				entity.setUpdater(user.getUserId());
				entity.setUpdateTime(nowTime);
				entity.setStatus(Constants.NORMAL);
				compBaseSV.saveComItemChange(entity);
			}

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (DataAccessException e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除 企业股权记录
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("deleteEquity.json")
	@ResponseBody
	public ResultDto deleteEquity(HttpServletRequest request, Long id){
		ResultDto re = returnFail("删除数据异常！");
		try{
			User user = HttpUtil.getCurrentUser(request);
			CompBaseEquity entity = compBaseSV.findCompEquityById(id);
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			entity.setStatus(Constants.DELETE);
			compBaseSV.saveCompEquity(entity);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除数据成功！");
		} catch (DataAccessException e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除 企业股权变更记录
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("deleteEquityChange.json")
	@ResponseBody
	public ResultDto deleteEquityChange(HttpServletRequest request, Long id){
		ResultDto re = returnFail("删除数据异常！");
		try{
			User user = HttpUtil.getCurrentUser(request);
			CompBaseItemChange entity = compBaseSV.findCompChangeById(id);
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			entity.setStatus(Constants.DELETE);
			compBaseSV.saveComItemChange(entity);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除数据成功！");
		} catch (DataAccessException e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * @Description (保存企业股权变更分析信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveCompEquityAnalysis.do")
	@ResponseBody
	public String saveCompEquityAnalysis(HttpServletRequest request, HttpServletResponse response) {

		String customerNo = request.getParameter("customerNo");
		String equityAnalysis = request.getParameter("equityAnalysis");

		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<>();
		try {
			CompBaseDesc baseDesc  = compBaseSV.findCompBaseDescByCustNo(customerNo);
			if(baseDesc == null){
				baseDesc = new CompBaseDesc();
				baseDesc.setCustomerNo(customerNo);
			}
			baseDesc.setEquityAnalysis(equityAnalysis);
			compBaseSV.saveCompBaseDesc(baseDesc);
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}


	/**
	 * 获取 经营场所列表
	 * @param taskId 任务主键
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping(value = "getCompRunArea.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto getCompRunArea(Long taskId, String customerNo){
		ResultDto re = returnFail("获取数据失败！");
		try {
			List<CompRunArea> list = compRunSV.queryRunAreaByTIdAndNo(taskId, customerNo);
			re.setMsg("获取数据成功");
			re.setCode(Constants.NORMAL);
			re.setRows(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	
	/**
	 * 获取 经营场所（土地）列表
	 * @param taskId 任务主键
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping(value = "getCompRunAreaLand.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto getCompRunAreaLand(Long taskId, String customerNo){
		ResultDto re = returnFail("获取数据失败！");
		try {
			List<CompRunAreaTd> list = compRunSV.queryRunAreaLandByTIdAndNo(taskId, customerNo);
			re.setMsg("获取数据成功");
			re.setCode(Constants.NORMAL);
			re.setRows(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	
	/**
	 * 获取 经营场所（厂房）列表
	 * @param taskId 任务主键
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping(value = "getCompRunAreaPlant.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto getCompRunAreaPlant(Long taskId, String customerNo){
		ResultDto re = returnFail("获取数据失败！");
		try {
			List<CompRunAreaCf> list = compRunSV.queryRunAreaPlantByTIdAndNo(taskId, customerNo);
			re.setMsg("获取数据成功");
			re.setCode(Constants.NORMAL);
			re.setRows(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 保存/修改 经营场所信息
	 * @param request
	 * @return
	 */
	@RequestMapping("saveCompRunArea.json")
	@ResponseBody
	public ResultDto saveCompRunArea(HttpServletRequest request) {
		ResultDto re = returnFail("保存失败");
		String jsonData = request.getParameter("jsonArray");
		try {
			List<CompRunArea> compRunAreaList =
					JacksonUtil.fromJson(jsonData, new TypeReference<List<CompRunArea>>() {});
			if (CollectionUtils.isEmpty(compRunAreaList)) {
				re.setMsg("传输数据内容为空，请重试");
				return re;
			}
			User user = HttpUtil.getCurrentUser(request);
			Timestamp nowTime = DateUtil.getNowTimestamp();
			for (CompRunArea compRunArea : compRunAreaList) {
				Long id = compRunArea.getId();
				if(id == null){
					compRunArea.setCreater(user.getUserId());
					compRunArea.setCreateTime(nowTime);
				}
				compRunArea.setUpdater(user.getUserId());
				compRunArea.setUpdateTime(nowTime);
				compRunArea.setStatus(Constants.NORMAL);
				compRunSV.saveRunArea(compRunArea);
			}

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 保存/修改 经营场所（土地）信息
	 * @param request
	 * @return
	 */
	@RequestMapping("saveCompRunAreaLand.json")
	@ResponseBody
	public ResultDto saveCompRunAreaLand(HttpServletRequest request) {
		ResultDto re = returnFail("保存失败");
		String jsonData = request.getParameter("jsonArray");
		try {
			List<CompRunAreaTd> compRunAreaList =
					JacksonUtil.fromJson(jsonData, new TypeReference<List<CompRunAreaTd>>() {});
			if (CollectionUtils.isEmpty(compRunAreaList)) {
				re.setMsg("传输数据内容为空，请重试");
				return re;
			}
			User user = HttpUtil.getCurrentUser(request);
			Timestamp nowTime = DateUtil.getNowTimestamp();
			for (CompRunAreaTd compRunArea : compRunAreaList) {
				Long id = compRunArea.getId();
				if(id == null){
					compRunArea.setCreater(user.getUserId());
					compRunArea.setCreateTime(nowTime);
				}
				compRunArea.setUpdater(user.getUserId());
				compRunArea.setUpdateTime(nowTime);
				compRunArea.setStatus(Constants.NORMAL);
				compRunSV.saveRunAreaLand(compRunArea);
			}

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 保存/修改 经营场所(厂房)信息
	 * @param request
	 * @return
	 */
	@RequestMapping("saveCompRunAreaPlant.json")
	@ResponseBody
	public ResultDto saveCompRunAreaPlant(HttpServletRequest request) {
		ResultDto re = returnFail("保存失败");
		String jsonData = request.getParameter("jsonArray");
		try {
			List<CompRunAreaCf> compRunAreaList =
					JacksonUtil.fromJson(jsonData, new TypeReference<List<CompRunAreaCf>>() {});
			if (CollectionUtils.isEmpty(compRunAreaList)) {
				re.setMsg("传输数据内容为空，请重试");
				return re;
			}
			User user = HttpUtil.getCurrentUser(request);
			Timestamp nowTime = DateUtil.getNowTimestamp();
			for (CompRunAreaCf compRunArea : compRunAreaList) {
				Long id = compRunArea.getId();
				if(id == null){
					compRunArea.setCreater(user.getUserId());
					compRunArea.setCreateTime(nowTime);
				}
				compRunArea.setUpdater(user.getUserId());
				compRunArea.setUpdateTime(nowTime);
				compRunArea.setStatus(Constants.NORMAL);
				compRunSV.saveRunAreaPlant(compRunArea);
			}

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}


	/**
	 * 删除 经营场所信息记录
	 * @param request
	 * @param id 主键
	 * @return
	 */
	@RequestMapping(value = "deleteCompRunArea.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto removeCompRunArea(HttpServletRequest request, Long id,String flag) {
		ResultDto re = returnFail("删除失败！");
		if (id == null || id == 0) {
			re.setMsg("传输数据内容异常，请重试");
			return re;
		}
		User user = HttpUtil.getCurrentUser(request);
		try {
//			CompRunArea runArea = compRunSV.queryRunAreaById(id);
//			runArea.setUpdater(user.getUserId());
//			runArea.setUpdateTime(DateUtil.getNowTimestamp());
//			runArea.setStatus(Constants.DELETE);
//			compRunSV.saveRunArea(runArea);
			if (flag.equals("tud")) {
				CompRunAreaTd runArea = compRunSV.queryRunAreaLandById(id);
				runArea.setUpdater(user.getUserId());
				runArea.setUpdateTime(DateUtil.getNowTimestamp());
				runArea.setStatus(Constants.DELETE);
				compRunSV.saveRunAreaLand(runArea);			
			} else {
				CompRunAreaCf runArea = compRunSV.queryRunAreaPlantById(id);
				runArea.setUpdater(user.getUserId());
				runArea.setUpdateTime(DateUtil.getNowTimestamp());
				runArea.setStatus(Constants.DELETE);
				compRunSV.saveRunAreaPlant(runArea);	
			}

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 保存 经营场所说明信息
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveBaseAnalysis.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto saveBaseAnalysis(HttpServletRequest request, String serNo, String customerNo){
		ResultDto re = returnFail("保存失败");
		String businessPlace = request.getParameter("businessPlace");
		String relatedCompanies = request.getParameter("relatedCompanies");
//		String flag = request.getParameter("flag");      //新增：用于区别是土地还是厂房
		try {
			User user = HttpUtil.getCurrentUser(request);
			CompRunAnalysis entity = compAnalysisService.selectSingleByProperty("serNo", serNo);
			entity.setBusinessPlace(businessPlace);        //原：没分土地和厂房的情况
			if(relatedCompanies != null) entity.setRelatedCompanies(relatedCompanies);
	
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			compAnalysisService.saveOrUpdate(entity);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * @Description (法人及高管 获取管理人员信息)
	 * @param customerNo
	 * @return
	 */
	@RequestMapping("getCompManagerList.json")
	@ResponseBody
	public ResultDto getCompManagerList(String customerNo) {
		ResultDto re = returnFail("获取企业高管信息失败");
		List<CompManagerInfoDto> dataGrid = null;
		try {
			dataGrid = compBaseSV.findManagerInfoByCustNo(customerNo);
			//应该由sv层来进行企查查数据的对接
			if(CollectionUtils.isEmpty(dataGrid)){
				//高管信息为空  通过企查查进行查询
				//GGG 伪代码
				//查询出结果对高管信息进行赋值
			}
			re = returnSuccess();
			re.setRows(dataGrid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * @Description (编辑企业客变更信息股东信息跳转)
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping("compChangeEquity.do")
	public ModelAndView compChangeEquity( String customerNo) {
		ModelAndView mav = new ModelAndView("views/survey/common/compChangeInfo");
		try {
		
			CompBaseDesc compBaseDesc = compBaseSV.findCompBaseDescByCustNo(customerNo);
			if (null != compBaseDesc)
				mav.addObject("equityAnalysis", compBaseDesc.getEquityAnalysis());
			mav.addObject("customerNo", customerNo);
			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.STD_SY_COM01};
			mav.addObject("dicData", sysDicSV.queryMapListByTypes(optTypes));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
		
	}
	
	/**
	 * @Description (保存企业管理人员)
	 * @param request
	 * @return
	 */
	@RequestMapping("saveCompManager.json")
	@ResponseBody
	public ResultDto saveCompManager(HttpServletRequest request) {
		ResultDto re = returnFail("保存失败！");
		String managerJson = request.getParameter("jsonArray");
		try {
			Gson gson = new Gson();
			List<CompBaseManagerDto> list = gson.fromJson(managerJson, 
					new TypeToken<List<CompBaseManagerDto>>() {}.getType());
//			CompBaseManagerDto managerDto = JacksonUtil.(managerJson, CompBaseManagerDto.class);
			if(CollectionUtils.isEmpty(list)){
				return re;
			}
			for(CompBaseManagerDto managerDto : list){
				if (managerDto == null) {
					return returnFail("存在必要数据为空，请重试！");
				}
				User user = HttpUtil.getCurrentUser(request);
				compBaseSV.saveCompManager(user.getUserId(), managerDto);
			}
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		} catch (DataAccessException e) {
			re.setMsg(ExceptionConstant.DATA_ACCESS_EXP);
			log.error(ExceptionConstant.DATA_ACCESS_EXP, e);
			e.printStackTrace();
		} catch (Exception e) {
			re.setMsg(ExceptionConstant.UNDEFINED_EXP);
			log.error(ExceptionConstant.UNDEFINED_EXP, e);
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * @Description (删除企业管理人员)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("deleteCompManager.json")
	@ResponseBody
	public ResultDto deleteCompManager(HttpServletRequest request, HttpServletResponse response) {
		Long managerId = Long.valueOf(request.getParameter("id"));
		ResultDto re = returnFail("删除企业管理人员失败！");
		try {
			User user = HttpUtil.getCurrentUser();
			CompBaseManagerDto manager = compBaseSV.findCompManagerById(managerId);
			compBaseSV.deleteCompManager(user.getUserId(), manager);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		} catch (DataAccessException e) {
			re.setMsg(ExceptionConstant.DATA_ACCESS_EXP);
			log.error(ExceptionConstant.DATA_ACCESS_EXP, e);
			e.printStackTrace();
		} catch (Exception e) {
			re.setMsg(ExceptionConstant.UNDEFINED_EXP);
			log.error(ExceptionConstant.UNDEFINED_EXP, e);
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * @Description (保存企业管理层备注说明)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveCompManagersAnalysis.do")
	@ResponseBody
	public ResultDto saveCompManagerAnalysis(HttpServletRequest request, HttpServletResponse response) {
		ResultDto re = returnFail("保存企业管理层备注失败");
		String customerNo = request.getParameter("customerNo");
		String managersAnalysis = request.getParameter("managersAnalysis");
		try {
			CompBaseDesc baseDesc  = compBaseSV.findCompBaseDescByCustNo(customerNo);
			if(baseDesc == null){
				baseDesc = new CompBaseDesc();
				baseDesc.setCustomerNo(customerNo);
			}
			baseDesc.setManagersAnalysis(managersAnalysis);
			compBaseSV.saveCompBaseDesc(baseDesc);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		} catch (DataAccessException e) {
			re.setMsg(ExceptionConstant.DATA_ACCESS_EXP);
			log.error(ExceptionConstant.DATA_ACCESS_EXP, e);
			e.printStackTrace();

		} catch (Exception e) {
			re.setMsg(ExceptionConstant.UNDEFINED_EXP);
			log.error(ExceptionConstant.UNDEFINED_EXP, e);
			e.printStackTrace();
		}

		return re;
	}

	/** 跳转到 企业内部管理 */
	@RequestMapping("compInnerManage.do")
	public ModelAndView getMortgagePledge(String serNo){
		ModelAndView mav =  new ModelAndView("views/survey/common/compInnerManage");
		Task task = taskSV.getTaskBySerNo(serNo);
		CompInnerManage compInnerManage = manageService.selectSingleByProperty("serNo", serNo);
		mav.addObject("serNo", serNo);
		mav.addObject("loanType", task.getBusinessNo());
		mav.addObject("innerManage", compInnerManage);
		
		//获取预设项
		String[] items = new String[]{SysConstants.RG_PRE_ITEM.LEGALER,
				SysConstants.RG_PRE_ITEM.CONTROLLER,
				SysConstants.RG_PRE_ITEM.MANAGE_INFO,
				SysConstants.RG_PRE_ITEM.STAFF_INFO,
				SysConstants.RG_PRE_ITEM.COMBINATION			
		};
		Map<String, List<QuickAnaly>> analyMap = quickAnalyService.queryAnalyByItem(items);
		mav.addObject("analyMap", GsonUtil.GsonString(analyMap));
		return mav;
	}

	/**
	 * 保存 企业内部管理信息
	 * @param request
	 * @param serNo 任务流水号
	 * @return
	 */
	@RequestMapping(value = "saveInnerManageInfo.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto innerManage(HttpServletRequest request, String serNo){
		ResultDto re = returnFail("保存失败");
		String jsonData = request.getParameter("jsonData");
		try {
			CompInnerManage newInfo = JacksonUtil.fromJson(jsonData, CompInnerManage.class);
			if (newInfo == null) {
				re.setMsg("传输数据内容为空，请重试");
				return re;
			}
			CompInnerManage oldInfo = manageService.selectSingleByProperty("serNo", serNo);
			if (oldInfo != null) {
				newInfo.setId(oldInfo.getId());
				newInfo.setSerNo(serNo);
			}

			User user = HttpUtil.getCurrentUser(request);
			if(newInfo.getId() == null){
				newInfo.setCreater(user.getUserId());
				newInfo.setCreateTime(DateUtil.getNowTimestamp());
			}
			newInfo.setUpdater(user.getUserId());
			newInfo.setUpdateTime(DateUtil.getNowTimestamp());
			newInfo.setStatus(Constants.NORMAL);
			manageService.saveOrUpdate(newInfo);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 股权结构图
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("showEchartsEquity.do")
	public ModelAndView showEchartsEquity(HttpServletRequest request, HttpServletResponse response,String customerNo) {
		ModelAndView mav = new ModelAndView("views/survey/common/echartsEquity");
		mav.addObject("customerNo",customerNo);
		return mav;
	}
	
	
	@RequestMapping("echartsEquityInfos.json")
	@ResponseBody
	public ResultDto echartsEquityInfos(String customerNo){
		ResultDto resultDto = returnFail();
		try {
			List<EquityEchartInfoDto> dataList = new ArrayList<>();
			List<EquityEchartReDto> linkList = new ArrayList<>();
			//获取到自己的信息
			CusBase cusBase = cusBaseSV.queryByCusNo(customerNo);
			//自己作为根
			dataList.add(new EquityEchartInfoDto(cusBase.getCustomerName(),0));
			//获取到自己的股东们
			List<CompBaseEquity> selfEquitys = compBaseSV.findCompEquityByCustNo(customerNo);//股东列表
			if(selfEquitys != null && selfEquitys.size()>0){
				for (CompBaseEquity equity:selfEquitys){
					String relationDesc = equity.getRelationDesc();
					if(!StringUtils.isEmpty(relationDesc) && equity.getRelationDesc().contains("董事长")){//区分一下是否为企业
						dataList.add(new EquityEchartInfoDto(equity.getStockName(),1));
					}else{
						dataList.add(new EquityEchartInfoDto(equity.getStockName(),0));
					}
					linkList.add(new EquityEchartReDto(equity.getStockName(),cusBase.getCustomerName(),"参股"));
					//可以从企查查获取股东的股东
					////////

				}
			}
			Map<String,Object> data = new HashMap<>();
			data.put("dataList",dataList);
			data.put("linkList",linkList);
			resultDto = returnSuccess();
			resultDto.setRows(data);
		}catch (Exception e){
			resultDto.setMsg("获取股权结构图数据异常");
			e.printStackTrace();
		}
		return resultDto;
	}
	
	/**
	 * 跳转到实际控制人
	 * @param request
	 * @param response
	 * @param customerNo
	 * @return
	 */
	@RequestMapping("showActualControl.do")
	public ModelAndView showActualControl(HttpServletRequest request, HttpServletResponse response,String customerNo) {
		ModelAndView mav = new ModelAndView("views/survey/common/compActualControl");
		mav.addObject("customerNo",customerNo);
		return mav;
	}
	
	/**
	 * 获取疑似实际控制人
	 * @param customerNo
	 * @return
	 */
	@RequestMapping("getActualControl.json")
	@ResponseBody
	public ResultDto getActualControl(String customerNo){
		ResultDto re = returnFail();
		try {
			QccActualControlDto actualControl = synchronizationQCCService.getActualControl(null, customerNo);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return re;
	}

	
	/***
	 * 受益人穿透界面跳转
	 * @param request
	 * @param response
	 * @param customerNo
	 * @return
	 */
	@RequestMapping("stockList.do")
	public ModelAndView stockList(HttpServletRequest request, HttpServletResponse response,String customerNo) {
		ModelAndView mav = new ModelAndView("views/survey/common/stockList");
		mav.addObject("customerNo",customerNo);
		return mav;
	}
	
	/**
	 * 获取疑似实际控制人
	 * @param customerNo
	 * @return
	 */
	@RequestMapping("getStockList.json")
	@ResponseBody
	public ResultDto getStockList(String customerNo){
		ResultDto re = returnFail();
		try {
			QccBeneficiaryInfoDto stockList = synchronizationQCCService.getStockList(null, customerNo);
			if(stockList==null){
				re.setMsg("当前企业不存在受益人穿透信息");
				re.setCode(ResultDto.RESULT_CODE_SUCCESS);
				return re;
			}
			if(!"Y".equals(stockList.getFindMatched())){
				re.setMsg("无法穿透出企业受益所有人");
				re.setCode(ResultDto.RESULT_CODE_SUCCESS);
				return re;
			}
			List<BreakThroughDto> throughList = stockList.getBreakThroughList();
			re.setRows(throughList);
			re.setMsg("获取企业受益人穿透成功");
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 保存/修改 企业股权记录
	 * @param request
	 * @return
	 */
	@RequestMapping("saveCompInvestment.json")
	@ResponseBody
	public ResultDto saveCompInvestment(HttpServletRequest request) {
		ResultDto re = returnFail("保存失败");
		String jsonData = request.getParameter("jsonArray");
		try {
			Gson gson = GsonUtil.getGson(); // new Gson();
			List<CompInvestment> entityList = gson.fromJson(jsonData,
					new TypeToken<ArrayList<CompInvestment>>() {}.getType());
			if (entityList == null || entityList.size() <= 0) {
				re.setMsg("传输数据内容为空，请重试");
				return re;
			}

			User user = HttpUtil.getCurrentUser(request);
			Timestamp nowTime = DateUtil.getNowTimestamp();
			for (CompInvestment equity : entityList) {
				String KeyNo = equity.getKeyNo();
				if (KeyNo == null) {
					equity.setCreater(user.getUserId());
					equity.setCreateTime(nowTime);
				}
				
				equity.setUpdater(user.getUserId());
				equity.setUpdateTime(nowTime);
				equity.setStatus(Constants.NORMAL);
				compInvestmentService.saveInvestLoan(equity);
			}

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (DataAccessException e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return re;
	}

}
