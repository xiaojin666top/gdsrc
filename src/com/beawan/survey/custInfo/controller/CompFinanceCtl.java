package com.beawan.survey.custInfo.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.analysis.abnormal.bean.ErrData;
import com.beawan.analysis.abnormal.service.IAbnormalService;
import com.beawan.analysis.finansis.bean.FnVerify;
import com.beawan.analysis.finansis.common.FinaRowData;
import com.beawan.analysis.finansis.service.FnVerifyService;
import com.beawan.analysis.finansis.service.IFinanasisService;
import com.beawan.analysis.finansis.service.IFnTableService;
import com.beawan.base.entity.User;
import com.beawan.base.service.ISysDicSV;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.SysConstants.ExceptionConstant;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.common.util.SystemUtil;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.corporateloan.service.LbSurveyReportService;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.bean.FinriskResult;
import com.beawan.customer.dto.WarnItemDto;
import com.beawan.customer.service.FinriskResultService;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.customer.service.ICusFSToolSV;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.beawan.library.service.IIndustrySV;
import com.beawan.survey.custInfo.bean.CompFinance;
import com.beawan.survey.custInfo.bean.CompFinanceTax;
import com.beawan.survey.custInfo.bean.CompFinanceTaxSale;
import com.beawan.survey.custInfo.bean.CompPower;
import com.beawan.survey.custInfo.dto.mainSubjectDto;
import com.beawan.survey.custInfo.entity.FinanceWarn;
import com.beawan.survey.custInfo.service.CompFinanceTaxSaleService;
import com.beawan.survey.custInfo.service.FinanceWarnService;
import com.beawan.survey.custInfo.service.ICompFinanceSV;
import com.beawan.survey.loanInfo.bean.ApplyInfo;
import com.beawan.survey.loanInfo.service.IApplyInfoSV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.platform.util.DateUtil;
import com.platform.util.GsonUtil;
import com.platform.util.HttpUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.MapperUtil;
import com.platform.util.StringUtil;

import net.sf.json.JSONObject;

/**
 * 财务状况类信息
 * @author czc
 * @Date 2017年6月23日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/survey/cust" })
public class CompFinanceCtl extends BaseController {
	
	private static final Logger log = Logger.getLogger(CompFinanceCtl.class);
	@Resource
	private ICusBaseSV cusBaseSV;
	@Resource
	protected ITaskSV taskSV;
	@Resource
	protected ICompFinanceSV compFinanceSV;
	
	@Resource
	private IFnTableService fnTableSV;
	@Resource
	private ISysDicSV sysDicSV;
	@Resource
	private ICmisInInvokeSV cmisInInvokeSV;
	@Resource
	private IIndustrySV industrySV;
	@Resource
	private IFinanasisService finanasisSV;
	@Resource
	private IAbnormalService abnormalSV;
	@Resource
	private ICusFSToolSV cusFSToolSV;
	@Resource
	private FinriskResultService finriskResultService;
	@Resource
	private LbSurveyReportService lbSurveyReportService;
	@Resource
	private LbSurveyReportService surveyReportService;
	@Resource
	private FnVerifyService fnVerifyService;
	@Resource
	private FinanceWarnService financeWarnService;
	@Resource
	private IApplyInfoSV applyInfoSV;
	@Resource
	protected CompFinanceTaxSaleService compFinanceTaxSaleService;
	/**
	 * @Description (保存企业财务状况信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveCompFinance.do")
	@ResponseBody
	public String saveCustFinance(HttpServletRequest request, HttpServletResponse response,
			String jsondata) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			compFinanceSV.saveCompFinance(jsondata);
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}


	/**
	 * 和贷前调查报告中的一样，只获取部分重要的比率指标
	 * @param request
	 * @param response
	 * @param taskId
	 * @param customerNo
	 * @return
	 */
	@RequestMapping("getFinaImportData.do")
	public String getFinaImportData(HttpServletRequest request,long taskId, String customerNo) {
		try {
			Map<String, Object> finaImportData = lbSurveyReportService.getFinaImportData(taskId, customerNo);
			request.setAttribute("taskId", taskId);
			request.setAttribute("customerNo", customerNo);
			request.setAttribute("finaImportData", finaImportData);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "views/survey/common/compFinanceImportData";
	}
	
	/**
	 * @Description (跳转页面请求，跳转到财务报表页面)
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("financeStatements.do")
	public String financeStatements(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo) {
		
		try {
			
			request.setAttribute("taskId", taskId);
			request.setAttribute("customerNo", customerNo);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/survey/common/compFinanceStatements";
	}
	
	/**
	 * @Description (跳转页面请求，跳转到财务报表详情页面)
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("financeReport.do")
	public String financeReport(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo) {
		
		try {
			
			String reportType = request.getParameter("reportType");
			
			Map<String, Object> finaRepInfo = compFinanceSV.getFinaRepInfo(taskId, customerNo);
			int finaRepYear = (Integer) finaRepInfo.get("finaRepYear");
			int finaRepMonth = (Integer) finaRepInfo.get("finaRepMonth");
			
			List<String> dateList = SystemUtil.genFinaDateList(finaRepYear,
					finaRepMonth, true, false);
			List<String> dateListShow = SystemUtil.genFinaDateList(finaRepYear,
					finaRepMonth, true, true);
			
			List<?> reportModel = cusFSToolSV.findRepModelByType(customerNo, reportType);
			List<?> reportData = cusFSToolSV.findRepDataByType(customerNo, dateList, reportType);
			//若当期为12份年报，则删除上年同期数据
			if(reportData.size() > dateListShow.size())
				reportData.remove(4);
			
			request.setAttribute("dateList", dateListShow);
			request.setAttribute("reportModel", reportModel);
			request.setAttribute("reportData", reportData);
			request.setAttribute("modelNo", reportType);
			
		} catch (Exception e) {
			log.error("跳转到财务报表详情页面异常：", e);
		}
		
		return "views/survey/common/compFinanceReport";
	}
	
	/**
	 * 保存 主要财务科目分析
	 * @param taskId
	 * @param jsonData
	 * @return
	 */
	@RequestMapping(value = "saveFinaMainSubAnalysis.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto saveFinaMainSubAnalysis(long taskId, String jsonData) {
		ResultDto re = returnFail("保存失败！");
		try {
			JSONObject json = JSONObject.fromObject(jsonData);
			Iterator<?> it = json.keys();
			String tableItem, analysis;
			while(it.hasNext()){
				tableItem = (String) it.next();
				analysis = json.getString(tableItem);
				ErrData errData = abnormalSV.getItemErrorData(taskId, tableItem);
				if(errData == null){
					if(StringUtil.isEmpty(analysis)) continue;
					errData = new ErrData();
					errData.setTaskId(taskId);
					errData.setName(tableItem);
					errData.setTableItem(tableItem);
					errData.setCategoryFlag(6);
				}
				errData.setExplain(analysis);
				abnormalSV.saveErrorData(errData);
			}
			
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (Exception e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg("保存主要财务科目分析异常！");
			log.error("保存主要财务科目分析异常：", e);
		}
		return re;
	}
	
	/**
	 * @Description (跳转页面请求，跳转到主要财务数据核实说明页面)
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("finaMainSubject.do")
	public String finaMainSubject(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo) {
		
		try {
			
			request.setAttribute("taskId", taskId);
			request.setAttribute("customerNo", customerNo);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/survey/common/compFinanceMainSubject";
	}
	
	/**
	 * @Description (跳转页面请求，跳转到主要资产科目分析说明页面)
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("finaMainAssets.do")
	public String finaMainAssets(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo) {

		List<ErrData> errDataList = new ArrayList<>();
		try {
			
			Task task = taskSV.getTaskById(Long.valueOf(taskId));
			request.setAttribute("task", task);
			
			Map<String, String> errExplainMap = new HashMap<String, String>();

			//由于现在财务异常都是在财报分析系统，所以本地的财务异常的表为空，获取异常的时候，先判断本地异常项表有没有数据
			//若没有的话  则先从财务分析返回的结果表里面获取数据塞到对公异常结果表中
			//若有  则直接使用（说明上一步操作已经进行过）
			errDataList = abnormalSV.getItemErrorDatas(task.getId());
			if(errDataList==null || errDataList.size()==0){
				//获取财务异常项、
				errDataList = finriskResultService.getErr(task.getSerNo());
				if(errDataList!=null && errDataList.size()!=0){
					for(ErrData data : errDataList){
						data.setTaskId(task.getId());
						data.setExplain(data.getErrReason() + data.getReason());
						abnormalSV.saveErrorData(data);
					}
				}
			}

			if(!CollectionUtils.isEmpty(errDataList)){
				for(ErrData errData : errDataList){
					errExplainMap.put(errData.getName(), errData.getExplain());
				}
			}
			request.setAttribute("errExplainMap", errExplainMap);
			
			//加载字典数据
			String[] optTypes = {
					SysConstants.BsDicConstant.STD_INVENTORY_TYPE, 
					SysConstants.BsDicConstant.STD_ACC_AGE_TYPE,
					SysConstants.BsDicConstant.STD_OWNERSHIP_NATURE,
					SysConstants.BsDicConstant.STD_LAND_NATURE,
					SysConstants.BsDicConstant.STD_LAND_USE,
					SysConstants.BsDicConstant.STD_ZX_YES_NO,
                    SysConstants.BsDicConstant.STD_CASH_TYPE
            };
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/survey/common/compFinanceMainAssets";
	}
	
	/**
	 * @Description (跳转页面请求，跳转到主要负债科目分析说明页面)
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("finaMainDebts.do")
	public String finaMainDebts(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo) {

		List<ErrData> errDataList = new ArrayList<>();
		try {
			
			Task task = taskSV.getTaskById(Long.valueOf(taskId));
			request.setAttribute("task", task);
			
			Map<String, String> errExplainMap = new HashMap<String, String>();
			//由于现在财务异常都是在财报分析系统，所以本地的财务异常的表为空，获取异常的时候，先判断本地异常项表有没有数据
			//若没有的话  则先从财务分析返回的结果表里面获取数据塞到对公异常结果表中
			//若有  则直接使用（说明上一步操作已经进行过）
			errDataList = abnormalSV.getItemErrorDatas(task.getId());
			if(errDataList==null || errDataList.size()==0){
				//获取财务异常项、
				errDataList = finriskResultService.getErr(task.getSerNo());
				if(errDataList!=null && errDataList.size()!=0){
					for(ErrData data : errDataList){
						data.setTaskId(task.getId());
						data.setExplain(data.getErrReason() + data.getReason());
						abnormalSV.saveErrorData(data);
					}
				}
			}

			if(!CollectionUtils.isEmpty(errDataList)){
				for(ErrData errData : errDataList){
					errExplainMap.put(errData.getName(), errData.getExplain());
				}
			}
			request.setAttribute("errExplainMap", errExplainMap);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/survey/common/compFinanceMainDebts";
	}

	/**
	 * @Description (跳转页面请求，跳转到财务指标分析页面)
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("finaIndexAnaly.do")
	public String finaIndexAnaly(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		
		try {
			
			Task task = taskSV.getTaskById(Long.valueOf(taskId));
			request.setAttribute("task", task);
			
			List<String> dateListShow = SystemUtil.genFinaDateList(task.getYear(),
					task.getMonth(), false, true);
			request.setAttribute("dateList", JacksonUtil.serialize(dateListShow));
			
		} catch (Exception e) {
			log.error("跳转到财务指标分析页面异常：", e);
		}
		
		return "views/survey/common/compFinanceIndex";
	}
	
	/**
	 * @Description 财务指标分析页面数据查询
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getFinaIndexAnalyData.do")
	@ResponseBody
	public String getFinaIndexAnalyData(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			
			Task task = taskSV.getTaskById(taskId);
			
			List<String> dateList = SystemUtil.genFinaDateList(task.getYear(),
					task.getMonth(), false, false);
			
			//比率指标相关信息
			List<FinaRowData> finaRowData = compFinanceSV.findFinanceIndexs(task.getCustomerNo(),
					dateList, task.getIndustry());
			json.put("finaRowData", finaRowData);
			
			CompFinance compFinance = compFinanceSV.findCompFinanceByTIdAndNo(taskId,
					task.getCustomerNo());
			String ratioAnalysis = "";
			if (compFinance != null)
				 ratioAnalysis = compFinance.getRatioAnalysis();
			
//			if(StringUtil.isEmptyString(ratioAnalysis) 
//					&& SysConstants.CusFinaValidFlag.AVAIL.equals(task.getFinaSyncFlag())){
			if(SysConstants.CusFinaValidFlag.AVAIL.equals(task.getFinaSyncFlag())){
				//获得系统分析结论
				Map<String, Object> conclusion = finanasisSV.genAnalysisConclusion(taskId,
						task.getCustomerNo(), SysConstants.SysFinaAnalyPart.RATIO_INDEX);
				ratioAnalysis = "盈利能力：" + conclusion.get("RatioOne").toString()
						      + "偿债能力：" + conclusion.get("RatioTwo").toString()
						      + "运营能力：" + conclusion.get("RatioThree").toString()
						      + "增长能力：" + conclusion.get("RatioFour").toString();
			}
			json.put("ratioAnalysis", ratioAnalysis);
			
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", "数据加载异常！");
			log.error("财务指标分析页面数据加载异常：", e);
		}
		
		return jRequest.serialize(json, true);
	}

	/**
	 * @Description (跳转页面请求，跳转到现金流量分析页面)
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("finaCashFlowAnaly.do")
	public String finaCashFlowAnaly(HttpServletRequest request, long taskId) {
		try {
			Task task = taskSV.getTaskById(Long.valueOf(taskId));
			request.setAttribute("task", task);
		} catch (Exception e) {
			log.error("跳转到现金流量分析页面异常：", e);
		}
		return "views/survey/common/compFinanceCashFlow";
	}
	
	/**
	 * @Description 现金流量分析页面数据查询
	 * @param taskId
	 * @param customerNo
	 * @return
	 */
	@RequestMapping("getFinaCashFlowAnalyData.do")
	@ResponseBody
	public ResultDto getFinaCashFlowAnalyData(long taskId, String customerNo) {
		ResultDto re = returnFail();
		try {
			Task task = taskSV.getTaskById(taskId);
			Map<String, Object> json = finriskResultService.getLocalCashflowAnalyData(task.getSerNo());
			String cashFlowAnaly;
			//现金流量科目相关信息
			CompFinance compFinance = compFinanceSV.findCompFinanceByTaskId(taskId);
			if(compFinance!=null && !StringUtil.isEmptyString(compFinance.getCashFlowAnaly())){
				cashFlowAnaly = compFinance.getCashFlowAnaly();
			}else{
				if(compFinance==null){
					compFinance = new CompFinance();
					compFinance.setTaskId(taskId);
					compFinance.setCustomerNo(task.getCustomerNo());
				}
				if(json==null){
					re.setMsg("从财务分析系统获取现金流数据失败");
					return re;
				}
				cashFlowAnaly = json.get("third").toString() + "\n"
						+ json.get("four").toString() + "\n"
						+ json.get("five").toString();
				compFinance.setCashFlowAnaly(cashFlowAnaly);
				compFinanceSV.saveCompFinance(compFinance);
			}

			json.put("cashFlowAnaly", cashFlowAnaly);
			re.setRows(json);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);

//			int year,month = 0;
//			year = cashflowAnalyData.get("year");
//			String cashFlowAnaly = cashflowAnalyData.get("third").toString()
//						      + cashflowAnalyData.get("four").toString()
//						      + cashflowAnalyData.get("five").toString();
//			json.put("cashFlowAnaly", cashFlowAnaly);
//				String cashflowListJson = GsonUtil.GsonString(cashflowAnalyData.get("cashflowList"));
//			List<FinaRowDto> cashflowList = gson.fromJson(cashflowListJson
//					, new TypeToken<List<FinaRowDto>>() {}.getType());
//			json.put("cashflowList", cashflowList);



//
//			//企业经营性现金流回笼信息
//			List<CompFinanceOperCF> finaOperCFs = compFinanceSV.findFinanceOperCFByTIdAndNo(taskId, customerNo);
//			json.put("finaOperCFs", finaOperCFs);
//
//			CompFinance compFinance = compFinanceSV.findCompFinanceByTIdAndNo(taskId, customerNo);
//			String cashFlowAnaly = "", operCashFlowAnaly = "";
//			if(compFinance != null) {
//				operCashFlowAnaly = compFinance.getOperCashFlowAnaly();
//				cashFlowAnaly = compFinance.getCashFlowAnaly();
//			}
//			json.put("operCashFlowAnaly", operCashFlowAnaly);
//
//			if(StringUtil.isEmptyString(cashFlowAnaly)
//					&& SysConstants.CusFinaValidFlag.AVAIL.equals(task.getFinaSyncFlag())){
//				//获得系统分析结论
//				Map<String, Object> conclusion = finanasisSV.genAnalysisConclusion(taskId, customerNo,
//						SysConstants.SysFinaAnalyPart.CASH_FLOW);
//				cashFlowAnaly = conclusion.get("first").toString()
//						      + conclusion.get("third").toString()
//						      + conclusion.get("four").toString()
//						      + conclusion.get("five").toString()
//						      + conclusion.get("seven").toString();
//			}
//			json.put("cashFlowAnaly", cashFlowAnaly);
		} catch (Exception e) {
			re.setMsg("现金流量分析数据加载失败");
			log.error("现金流量分析页面数据加载异常：", e);
		}
		return re;
	}
	
	/**
	 * @Description 保存企业经营性现金流回笼记录列表，先删除后插入
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveFinanceOperCFs.do")
	@ResponseBody
	public String saveFinanceOperCFs(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo, String jsonArray) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			compFinanceSV.saveFinanceOperCFs(taskId, customerNo, jsonArray);
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * 跳转到 纳税情况分析页面  
	 * @param taskId 任务id
	 * @param customerNo 客户号
	 * @return
	 */
//	@Deprecated
	@RequestMapping("finaTaxAnaly.do")
	public ModelAndView finaTaxAnaly(long taskId, String customerNo) {
		
		//---->原先增值税   所得税在页面
//		ModelAndView mav = new ModelAndView("views/survey/common/compFinanceTax");
		//跳转到纳税申报表
		ModelAndView mav = new ModelAndView("views/survey/common/compFinanceTaxSale");
		try {
			Task task = taskSV.getTaskById(taskId);
			mav.addObject("task", task);
			mav.addObject("taskId", taskId);
			mav.addObject("customerNo", customerNo);
			CompFinance compFinance = compFinanceSV.findCompFinanceByTIdAndNo(taskId, customerNo);
			if(compFinance != null)
				mav.addObject("taxAnaly", compFinance.getTaxAnaly());
			
//			//缴税信息
			List<CompFinanceTax> financeTaxes = compFinanceSV.findFinanceTaxByTIdAndNo(taskId, customerNo);
//			mav.addObject("financeTaxes", JacksonUtil.serialize(financeTaxes));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
	
	/**
	 * 获取 纳税分析表信息
	 * @param serNo
	 * @return
	 */
	@RequestMapping("getFinanceTaxList.json")
	@ResponseBody
	public ResultDto getFinanceTaxList(HttpServletRequest request, long taskId, String customerNo){
		ResultDto re = returnFail("获取纳税分析详情失败");
		if(StringUtil.isEmptyString(customerNo)){
			re.setMsg("客户号为空");
			return re;
		}
		try {
			
			List<CompFinanceTaxSale> CompFinanceTaxSaleInfo = compFinanceTaxSaleService.selectByProperty("customerNo", customerNo);
			request.setAttribute("taskId", taskId);
			request.setAttribute("customerNo", customerNo);
			re = returnSuccess();
			re.setRows(CompFinanceTaxSaleInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return re;
	}
	
	/**
	 * 保存/修改 纳税销售情况表
	 * @param request
	 * @return
	 */
	@RequestMapping("saveCompFinanceTaxSale.json")
	@ResponseBody
	public ResultDto saveCompFinanceTaxSale(HttpServletRequest request) {
		ResultDto re = returnFail("保存失败");
		String jsonData = request.getParameter("jsonArray");
		try {
			List<CompFinanceTaxSale> entityList = 
					JacksonUtil.fromJson(jsonData, new TypeReference<List<CompFinanceTaxSale>>() {});
			if (entityList == null || entityList.size() <= 0) {
				re.setMsg("传输数据内容为空，请重试");
				return re;
			}
			
			CompFinanceTaxSale compFinanceTaxSale = entityList.get(0);
			String customerNo = compFinanceTaxSale.getCustomerNo();
			
			User user = HttpUtil.getCurrentUser(request);
			Timestamp nowTime = DateUtil.getNowTimestamp();
			for (CompFinanceTaxSale entity : entityList) {
				Long id = entity.getId();				
				if(id == null){
					entity.setCreater(user.getUserId());
					entity.setCreateTime(nowTime);
				}
				entity.setUpdater(user.getUserId());
				entity.setUpdateTime(nowTime);
				entity.setStatus(Constants.NORMAL);
				
				if(entity.getFnValue() == null){
					compFinanceTaxSaleService.getCompFinanceTaxSale(entity, customerNo);
				}
				if(entity.getFnValue()!=null&&entity.getTaxValue()!=null){	
					String deviation = compFinanceTaxSaleService.countDeviation(entity.getFnValue(),entity.getTaxValue());
					entity.setDeviation(deviation);
				}
				compFinanceTaxSaleService.saveOrUpdate(entity);
			}

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除 纳税销售情况表
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("deleteCompFinanceTaxSale.json")
	@ResponseBody
	public ResultDto removeCompFinanceTaxSale(HttpServletRequest request, Long id) {
		ResultDto re = returnFail("删除失败");
		if (id == null || id == 0) {
			re.setMsg("传输数据内容异常，请重试");
			return re;
		}
		User user = HttpUtil.getCurrentUser(request);
		try {
			CompFinanceTaxSale compFinanceTaxSale = compFinanceTaxSaleService.findByPrimaryKey(id);
			compFinanceTaxSale.setStatus(Constants.DELETE);
			compFinanceTaxSale.setUpdater(user.getUserId());
			compFinanceTaxSale.setUpdateTime(DateUtil.getNowTimestamp());
			compFinanceTaxSaleService.saveOrUpdate(compFinanceTaxSale);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 保存、修改 纳税信息列表
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "saveFinaTax.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto saveFinaTax(HttpServletRequest request){
		ResultDto re = returnFail("保存失败");
		String jsonData = request.getParameter("jsonArray");
		try {
			List<CompFinanceTax> entityList =
					JacksonUtil.fromJson(jsonData, new TypeReference<List<CompFinanceTax>>(){});
			if (CollectionUtils.isEmpty(entityList))
				return returnFail("传输数据内容为空，请重试");

			User user = HttpUtil.getCurrentUser(request);
			Timestamp nowTime = DateUtil.getNowTimestamp();
			
			CompFinanceTax entity = entityList.get(0);
			Long taskId = entity.getTaskId();
			String customerNo = entity.getCustomerNo();
			//缴税信息
			List<CompFinanceTax> financeTaxes = compFinanceSV.findFinanceTaxByTIdAndNo(taskId, customerNo);
			if(CollectionUtils.isEmpty(financeTaxes)){
				entity.setCreater(user.getUserId());
				entity.setCreateTime(nowTime);entity.setUpdater(user.getUserId());
				entity.setUpdateTime(nowTime);
				entity.setStatus(Constants.NORMAL);
				compFinanceSV.saveFinanceTax(entity);
			}else{
				CompFinanceTax tax = financeTaxes.get(0);
				tax.setUpdater(user.getUserId());
				tax.setUpdateTime(nowTime);
				tax.setValue1(entity.getValue1());
				tax.setValue2(entity.getValue2());
				tax.setValue3(entity.getValue3());
				tax.setValue4(entity.getValue4());
				tax.setValue5(entity.getValue5());
				compFinanceSV.saveFinanceTax(tax);
			}

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 保存 企业纳税 分析信息
	 * @param taskId 任务id
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping(value = "saveTaxAnalysis.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto saveFinaAnalysis(HttpServletRequest request, long taskId, String customerNo) {
		ResultDto re = returnFail("保存失败！");
		String taxAnalysis = request.getParameter("taxAnalysis");
		try {
			User user = HttpUtil.getCurrentUser(request);
			CompFinance entity = compFinanceSV.findCompFinanceByTIdAndNo(taskId, customerNo);
			if(entity == null){
				entity = new CompFinance();
				entity.setTaskId(taskId);
				entity.setCustomerNo(customerNo);
				entity.setCreater(user.getUserId());
				entity.setCreateTime(DateUtil.getNowTimestamp());
			}
			
			if(taxAnalysis != null) entity.setTaxAnaly(taxAnalysis);
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			compFinanceSV.saveCompFinance(entity);
			
			re.setMsg("保存成功！");
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		} catch (DataAccessException e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			re.setCode(ResultDto.RESULT_CODE_FAIL);
			re.setMsg(ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * @Description (跳转页面请求，跳转到盈利情况分析页面)
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("finaIncomeAnaly.do")
	public String finaIncomeAnaly(HttpServletRequest request, long taskId) {
		try {
			Task task = taskSV.getTaskById(Long.valueOf(taskId));
			request.setAttribute("task", task);
		} catch (Exception e) {
			log.error("跳转到盈利情况分析页面异常：", e);
		}
		return "views/survey/common/compFinanceIncome";
	}
	
	/**
	 * @Description 盈利情况分析页面数据查询
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getFinaIncomeAnalyData.do")
	@ResponseBody
	public String getFinaIncomeAnalyData(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			//利润科目相关信息
			List<FinaRowData> finaRowData = compFinanceSV.findFinanceIncome(taskId, customerNo);
			json.put("finaRowData", finaRowData);
			
			CompFinance compFinance = compFinanceSV.findCompFinanceByTIdAndNo(taskId, customerNo);
			String incomeAnaly="";
			if(compFinance != null)
				incomeAnaly = compFinance.getIncomeAnaly();
			
			Task task = taskSV.getTaskById(taskId);
			
			if(StringUtil.isEmptyString(incomeAnaly)
					&& SysConstants.CusFinaValidFlag.AVAIL.equals(task.getFinaSyncFlag())){
				//获得系统分析结论
				Map<String, Object> conclusion = finanasisSV.genAnalysisConclusion(taskId, customerNo,
						SysConstants.SysFinaAnalyPart.INCOME);
				incomeAnaly = conclusion.get("Incsecond").toString()
							+ conclusion.get("IncsecondThree").toString()
						    + conclusion.get("IncThird").toString()
						    + conclusion.get("IncFour").toString()
						    + conclusion.get("IncFive").toString()
						    + conclusion.get("IncSix").toString();
			}
			json.put("incomeAnaly", incomeAnaly);
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", "数据加载异常！");
			log.error("盈利情况分析页面数据加载异常：", e);
		}
		return jRequest.serialize(json, true);
	}
	
	/**
	 * 跳转到汇率风险分析页面
	 * @param request
	 * @param taskId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("finaExcRateRisk.do")
	public String finaExcRateRisk(HttpServletRequest request,long taskId, String customerNo) {
		try {
			Task task = taskSV.getTaskById(Long.valueOf(taskId));
			request.setAttribute("task", task);
			
			CompFinance compFinance = compFinanceSV.findCompFinanceByTIdAndNo(taskId, customerNo);
			if(compFinance!=null) {
			request.setAttribute("excRateRisk", compFinance.getExcRateRisk());
			}else {
				request.setAttribute("excRateRisk","");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "views/survey/common/compFinanceExcRateRisk";
	}
	
	/**
	 * @Description (跳转页面请求，跳转到财务总体分析结论页面)
	 * @param request
	 * @param taskId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("finaOverallAnaly.do")
	public String finaOverallAnaly(HttpServletRequest request,long taskId, String customerNo) {
		try {
			Task task = taskSV.getTaskById(taskId);
			request.setAttribute("task", task);
			CompFinance compFinance = compFinanceSV.findCompFinanceByTIdAndNo(taskId, customerNo);
			if(compFinance!=null){
				Map<String, Object> localFinAnaly = finriskResultService.getLocalFinAnaly(task.getSerNo());
				if(StringUtil.isEmptyString(compFinance.getProfitAnaly())){
					compFinance.setProfitAnaly(String.valueOf(localFinAnaly.get("profit")));
				}
				if(StringUtil.isEmptyString(compFinance.getDevelopAnaly())){
					compFinance.setDevelopAnaly(String.valueOf(localFinAnaly.get("develop")));
				}
				if(StringUtil.isEmptyString(compFinance.getOperateAnaly())){
					compFinance.setOperateAnaly(String.valueOf(localFinAnaly.get("operate")));
				}
				if(StringUtil.isEmptyString(compFinance.getSolvency())){
					compFinance.setSolvency(String.valueOf(localFinAnaly.get("debt")));
				}
				if(StringUtil.isEmptyString(compFinance.getSystemAnaly())){
					compFinance.setSystemAnaly(String.valueOf(localFinAnaly.get("summay")));
				}
			}
			request.setAttribute("financeAnaly", compFinance);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "views/survey/common/compFinaAnalysis";
	}

	/**
	 * 跳转到流动资金缺口测算界面
	 * @param taskId
	 * @param customerNo
	 * @return
	 */
	@RequestMapping("compFinanceQuota.do")
	public ModelAndView compFinanceQuota(long taskId, String customerNo) {
		ModelAndView mav = new ModelAndView("views/survey/common/compFinanceQuota");
		try {
			mav.addObject("taskId", taskId);
			Task task = taskSV.getTaskById(taskId);
			Map<String, Object> params = new HashMap<>();
			params.put("dgSerialNumber", task.getSerNo());
			params.put("platformType", Constants.Platform.LOCAL_FINRISK);
			List<FinriskResult> finriskResults = finriskResultService.selectByProperty(params);
			//会数组越界
			String quotaInfo="";
			if(!CollectionUtils.isEmpty(finriskResults)){
				quotaInfo = finriskResults.get(0).getQuotaInfo();
			}
			mav.addObject("rows", GsonUtil.GsonToMaps(quotaInfo));
			
			//查询是否存在调整值
			ApplyInfo applyInfo = applyInfoSV.findApplyInfoByTaskId(taskId);
			mav.addObject("adjust", applyInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
	
	/**
	 * 获取  手动调整流动资金测算结果值及其理由
	 * @param taskId
	 * @param adjustVal
	 * @param adjustReson
	 * @return
	 */
	@RequestMapping("getAdjustQuota.json")
	@ResponseBody
	public ResultDto getAdjustQuota(long taskId) {
		ResultDto re = returnFail();
		try{
			ApplyInfo applyInfo = applyInfoSV.findApplyInfoByTaskId(taskId);
			re = returnSuccess();
			re.setRows(applyInfo);
			re.setMsg("获取数据成功");
		}catch(Exception e){
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 保存 手动调整流动资金测算结果值及其理由
	 * @param taskId
	 * @param adjustVal
	 * @param adjustReson
	 * @return
	 */
	@RequestMapping("saveAdjustQuota.json")
	@ResponseBody
	public ResultDto saveAdjustQuota(long taskId, String adjustVal, String adjustReson) {
		ResultDto re = returnFail();
		try{
			ApplyInfo applyInfo = applyInfoSV.findApplyInfoByTaskId(taskId);
			if(applyInfo == null){
				applyInfo = new ApplyInfo();
				applyInfo.setTaskId(taskId);
			}
			applyInfo.setAdjustQuota(Double.parseDouble(adjustVal));
			applyInfo.setAdjustQuotaReason(adjustReson);
			applyInfoSV.saveApplyInfo(applyInfo);
			re = returnSuccess();
			re.setMsg("调整流动资金缺口额度成功");
		}catch(Exception e){
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 根据预计销售增长率计算流动资金缺口测算
	 * @param taskId
	 * @param growth
	 * @return
	 */
	@RequestMapping("getQuotaInfo.json")
	@ResponseBody
	public ResultDto getQuotaInfo(String taskId, String growth){
		ResultDto re = returnFail();
		try{
			Task task = taskSV.getTaskById(Long.parseLong(taskId));
			if(task==null){
				re.setMsg("任务号异常，请重试");
				log.error("任务号异常"+taskId+"，请重试");
				return re;
			}
			FinriskResult finriskResult = finriskResultService.updateQuotaByGrowth(growth, task.getSerNo());
			re.setRows(finriskResult);
		}catch (Exception e){
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 跳转到 重要科目核实
	 * @param taskId 任务ID
	 * @param customerNo 客户号
	 * @return
	 */
	@RequestMapping("mainSubjectVerify.do")
	@ResponseBody
	public ModelAndView mainSubjectVerify(Long taskId, String customerNo){
		ModelAndView mv = new ModelAndView("views/survey/common/mainSubjectVerify");
		mv.addObject("taskId", taskId);
		mv.addObject("customerNo", customerNo);
		return mv;
	}

	/**
	 * 获取 主要科目列表
	 * @param request
	 * @return
	 */
	@RequestMapping("getMainSubject.json")
	@ResponseBody
	public ResultDto getMainSubjectList(HttpServletRequest request, Long taskId, String customerNo) {
		ResultDto re = returnFail("获取数据失败");
		try {
			/**
			 * 查看是否已经保存
			 */
			List<FnVerify> verifyList = fnVerifyService.selectByProperty("taskId", taskId);

			Map<String, Object> finaImportData = surveyReportService.getFinaImportData(taskId, customerNo);
			//各个重要科目的账面值
			Map<String, String> balanceData = surveyReportService.getNowBalanceData(finaImportData);
			//结果集
			List<mainSubjectDto> list = new ArrayList<>();
			if(!CollectionUtils.isEmpty(balanceData)){
				for(Map.Entry<String, String> entry : balanceData.entrySet()){
					String key = entry.getKey();
					String val = entry.getValue();
					mainSubjectDto dto = new mainSubjectDto();
					dto.setSubject(key);
					dto.setReportValue(Double.parseDouble(val));
					dto.setTaskId(taskId);
					dto.setCustomerNo(customerNo);
					//赋值 核查值
					if(!CollectionUtils.isEmpty(verifyList)){
						for(FnVerify verify : verifyList){
							if(key.equals(verify.getSubject())){
								dto.setVerifyValue(verify.getVerifyValue());
								break;
							}
						}

					}
					list.add(dto);
//					dto.setCategory();
				}
			}
//			String jsonData = "[{\"taskId\": 27050,\"customerNo\": \"20200324165810\",\"category\": \"资产项目\",\"subject\": \"货币资金\",\"reportValue\": 5000.0,\"verifyValue\": 8000.0}, {\"taskId\": 27050,\"customerNo\": \"20200324165810\",\"category\": \"资产项目\",\"subject\": \"存货\",\"reportValue\": 5000.0,\"verifyValue\": 8000.0}, {\"taskId\": 27050,\"customerNo\": \"20200324165810\",\"category\": \"资产项目\",\"subject\": \"应收账款\",\"reportValue\": 5000.0,\"verifyValue\": 8000.0}, {\"taskId\": 27050,\"customerNo\": \"20200324165810\",\"category\": \"资产项目\",\"subject\": \"其他应收款\",\"reportValue\": 5000.0,\"verifyValue\": 8000.0}, {\"taskId\": 27050,\"customerNo\": \"20200324165810\",\"category\": \"资产项目\",\"subject\": \"预付账款\",\"reportValue\": 5000.0,\"verifyValue\": 8000.0}, {\"taskId\": 27050,\"customerNo\": \"20200324165810\",\"category\": \"资产项目\",\"subject\": \"主要固定资产\",\"reportValue\": 5000.0,\"verifyValue\": 8000.0}, {\"taskId\": 27050,\"customerNo\": \"20200324165810\",\"category\": \"负债项目\",\"subject\": \"应付账款\",\"reportValue\": 5000.0,\"verifyValue\": 8000.0}, {\"taskId\": 27050,\"customerNo\": \"20200324165810\",\"category\": \"负债项目\",\"subject\": \"其他应付款\",\"reportValue\": 5000.0,\"verifyValue\": 8000.0}, {\"taskId\": 27050,\"customerNo\": \"20200324165810\",\"category\": \"负债项目\",\"subject\": \"预收账款\",\"reportValue\": 5000.0,\"verifyValue\": 8000.0}]";
//			List<mainSubjectDto> list = GsonUtil.GsonToList(jsonData, mainSubjectDto.class);
			re.setRows(list);
			re.setMsg("获取数据成功");
			re.setCode(Constants.NORMAL);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 保存核实项的值
	 * @param taskId
	 * @param jsonArray
	 * @return
	 */
	@RequestMapping("saveMainSubject.json")
	@ResponseBody
	public ResultDto saveMainSubject(Long taskId, String jsonArray) {
		ResultDto re = returnFail();
		try {
			Gson gson =new Gson();

			List<FnVerify> fnVerifies = gson.fromJson(jsonArray,new TypeToken<ArrayList<FnVerify>>() {}.getType());
			fnVerifyService.saveVerifyList(fnVerifies);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功");
//			JacksonUtil.
//			System.out.println(jsonArray);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}


	/**
	 * 跳转到 核实各科目界面
	 * @param taskId 任务ID
	 * @param subjectType 科目类型
	 * @return
	 */
	@RequestMapping("verifySubject.do")
	@ResponseBody
	public ModelAndView verifySubject(Long taskId, Integer subjectType,String customerNo){
		ModelAndView mv = new ModelAndView();
		try {
			mv.addObject("taskId", taskId);
			mv.addObject("customerNo",customerNo);
			Map<String, Object> finaImportData = surveyReportService.getFinaImportData(taskId, customerNo);
			//各个重要科目的账面值
			Map<String, String> balanceData = surveyReportService.getNowBalanceData(finaImportData);
			mv.addObject("balanceData",balanceData);
			switch (subjectType){
				case Constants.Anno.MONETARY_FUND:
					mv.setViewName("views/survey/common/verifyMonetaryFund");
					break;
					
				case Constants.Anno.STOCK:
					mv.setViewName("views/survey/common/verifyStock");
					break;
				case Constants.Anno.ACCOUNT_RECE:
					mv.setViewName("views/survey/common/verifyReceive");
					break;
				case Constants.Anno.OTHER_RECE:
					mv.setViewName("views/survey/common/verifyOtherReceive");
					break;
				case Constants.Anno.PRE_PAY:
					mv.setViewName("views/survey/common/verifyPrepay");
					break;
				case Constants.Anno.FIXED_ASSET:
					mv.setViewName("views/survey/common/verifyFixedAsset");
					break;
				case Constants.Anno.ACCOUNT_PAY:
					mv.setViewName("views/survey/common/verifyAccountPay");
					break;
				case Constants.Anno.OTHER_PAY:
					mv.setViewName("views/survey/common/verifyOtherPay");
					break;
				case Constants.Anno.PRE_RECE:
					mv.setViewName("views/survey/common/verifyPreReceive");
					break;
				default:
					break;
			}

			String[] optTypes = {
					SysConstants.BsDicConstant.STD_INVENTORY_TYPE,
					SysConstants.BsDicConstant.STD_ACC_AGE_TYPE,
					SysConstants.BsDicConstant.STD_OWNERSHIP_NATURE,
					SysConstants.BsDicConstant.STD_LAND_NATURE,
					SysConstants.BsDicConstant.STD_LAND_USE,
					SysConstants.BsDicConstant.STD_ZX_YES_NO,
					SysConstants.BsDicConstant.STD_CASH_TYPE
			};
			mv.addObject("dicData", sysDicSV.queryMapListByTypes(optTypes));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mv;
	}

	/**
	 * 跳转到 财务预警
	 * @param serNo 任务流水号
	 * @return
	 */
	@RequestMapping("abnormalWarn.do")
	@ResponseBody
	public ModelAndView abnormalWarn(String serNo){
		ModelAndView mav = new ModelAndView("views/survey/common/compFinanceWarn");
		mav.addObject("serNo", serNo);
		return mav;
	}

	/**
	 * 获取 财务预警提示项
	 * @param serNo 流水号
	 * @return
	 */
	@RequestMapping(value = "getWarnItemList.json", method = {RequestMethod.POST,RequestMethod.GET})
	@ResponseBody
	public ResultDto getWarnItemList(String serNo){
		ResultDto re = returnFail("获取财务预警提示信息失败！");
		try {
			List<FinanceWarn> result = financeWarnService.selectByProperty("serNo", serNo);
			if (CollectionUtils.isEmpty(result)){
				List<WarnItemDto> warnItemList = finriskResultService.getFinaWarn(serNo);
				if (CollectionUtils.isEmpty(warnItemList))
					return returnFail("暂无财务预警提示信息！");
				for (WarnItemDto item : warnItemList) {
					if (StringUtil.isEmptyString(item.getSerNo()))
						item.setSerNo(serNo);
					financeWarnService.saveOrUpdate(MapperUtil.trans(item, FinanceWarn.class));
				}
				result = financeWarnService.selectByProperty("serNo", serNo);
			}
			re.setMsg("获取数据成功！");
			re.setRows(result);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	@RequestMapping("saveFinanceWarn.json")
	@ResponseBody
	public ResultDto saveFinanceWarn(HttpServletRequest request){
		ResultDto re = returnFail("保存失败");
		String jsonData = request.getParameter("jsonArray");
		try {
			List<FinanceWarn> entityList =
					JacksonUtil.fromJson(jsonData, new TypeReference<List<FinanceWarn>>() {});
			if (CollectionUtils.isEmpty(entityList))
				return returnFail("传输数据内容为空，请重试！");
			User user = HttpUtil.getCurrentUser(request);
			Timestamp nowTime = DateUtil.getNowTimestamp();
			for (FinanceWarn entity : entityList) {
				if (entity.getId() == null){
					entity.setCreater(user.getUserId());
					entity.setCreateTime(nowTime);
				}
				entity.setUpdater(user.getUserId());
				entity.setUpdateTime(nowTime);
				entity.setStatus(Constants.NORMAL);
				financeWarnService.saveOrUpdate(entity);
			}

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return re;
	}

}
