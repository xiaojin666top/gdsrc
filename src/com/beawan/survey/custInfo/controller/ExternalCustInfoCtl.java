package com.beawan.survey.custInfo.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.platform.spider.TYCSimpleSpider;
import com.platform.util.StringUtil;

@Controller
@RequestMapping({ "/survey/cust" })
public class ExternalCustInfoCtl {

	/**
	 * @Description (获取客户外部数据)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("externalinfo.do")
	public String getCustExternalInfo(HttpServletRequest request,
			HttpServletResponse response, String companyId, String companyName) {

		try {
			if(!StringUtil.isEmptyString(companyName)
					&& companyName.equals(new String(companyName.getBytes("ISO-8859-1"), "ISO-8859-1")))
			{
				companyName = new String(companyName.getBytes("ISO-8859-1"),"UTF-8");
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		request.setAttribute("companyId", companyId);
		request.setAttribute("companyName", companyName);
		
		if("杭州海康威视数字技术股份有限公司".equals(companyName))
			return "views/custInfo/externalInfoExample";
		else
			return "views/custInfo/externalInfo";
	}
	
	/**
	 * @Description (获取客户外部数据)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("grabexternalinfo.do")
	@ResponseBody
	public void grabCustExternalInfo(HttpServletRequest request,
			HttpServletResponse response, String companyId, String companyName) {
		
		try {
			
			if(!StringUtil.isEmptyString(companyName)
					&& companyName.equals(new String(companyName.getBytes("ISO-8859-1"), "ISO-8859-1"))){
				companyName = new String(companyName.getBytes("ISO-8859-1"),"UTF-8");
			}
			
			String returnContent;
			TYCSimpleSpider spider = new TYCSimpleSpider();
			int status;
			if(!StringUtil.isEmptyString(companyId))
				status = spider.grabDetailByCompanyId(companyId);
			else
				status = spider.grabDetailByCompanyName(companyName);
			if( status == 1){
				returnContent = spider.getDetailHtmPage();
				request.setAttribute("companyName", spider.getCompanyName());
			}else{ 
				returnContent = "<ht><body><div style='width:100%;height:100%;text-align:center;font-size:36px;padding-top: 20%;'>";
				if(status == 0)
					returnContent += "暂无信息";
				else
					returnContent += "查询出错";
				returnContent += "<div>";
			}
			
			spider.desdroy();
			
			//response.setCharacterEncoding("UTF-8");
			response.setContentType("text/html;charset=GBK");
			response.getOutputStream().write(returnContent.getBytes());
			
			//savePage(returnContent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private void savePage(String pageSource) throws IOException{
		
		String path = "/spidertemp/";
		File tempDir = new File(path);
		if(!tempDir.exists())
			tempDir.mkdirs();
		
		File file = new File(tempDir, new Date().getTime() + ".txt");
		if(!file.exists())
			file.createNewFile();
		
		FileOutputStream fos = new FileOutputStream(file);
		fos.write(pageSource.getBytes());
		fos.flush();
		fos.close();
	}

}
