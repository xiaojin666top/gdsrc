package com.beawan.survey.custInfo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.library.bean.InduSituation;
import com.beawan.library.service.IIndustrySV;
import com.beawan.library.service.IIndustrySituationSV;
import com.beawan.survey.custInfo.bean.CompRunFixed;
import com.beawan.survey.custInfo.bean.CompRunPrdSale;
import com.beawan.survey.custInfo.service.ICompRunSV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.StringUtil;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/survey/cust" })
public class CompRunFixedCtl {
	
	private static final Logger log = Logger.getLogger(CompRunFixedCtl.class);

	@Resource
	protected ITaskSV taskSV;

	@Resource
	private ICompRunSV compRunSV;

	@Resource
	protected IIndustrySV industrySV;
	
	@Resource
	private IIndustrySituationSV indusSituationSV;
	
	/**
	 * @Description (固定资产贷款-企业经营状况信息页面跳转)
	 * @param request
	 * @param response
	 * @param taskId 任务ID
	 * @return
	 */
	@RequestMapping("compRunFixed.do")
	public String compRunFixed(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo) {
		
		Task task = this.taskSV.getTaskById(taskId);
		
		try {
			
			if (StringUtil.isEmptyString(customerNo)) {
				customerNo = task.getCustomerNo();
			}
			String indusCode=task.getIndustry();
			
			CompRunFixed compRunFixed = compRunSV.queryRunFixedByTIdAndNo(taskId, customerNo);
			List<CompRunPrdSale> runPrdSales = compRunSV.queryRunPrdSaleByTIdAndNo(taskId, customerNo);
			
			InduSituation induSituation=indusSituationSV.findInduSituationByName(indusCode);
			
			String situation="";
			
			if(null!=compRunFixed&&null!=compRunFixed.getIndustryAnalysis()&&!"".equals(compRunFixed.getIndustryAnalysis()))
				situation=compRunFixed.getIndustryAnalysis();
			else if(null!=induSituation) {
				situation=induSituation.getSituation();
			}
				
			request.setAttribute("newsituation", situation);
			request.setAttribute("compRunFixed", compRunFixed);
			request.setAttribute("runPrdSales", JSONArray.fromObject(runPrdSales).toString());
			request.setAttribute("task", task);
			
		} catch (Exception e) {
			log.error("固定资产贷款-企业经营状况信息页面跳转异常：", e);
		}
		
		return "views/survey/fixedassets/compRunInfo";
	}
	
	/**
	 * @Description (固定资产贷款-保存企业经营信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveCompRunFixed.do")
	@ResponseBody
	public String saveCompRunFixed(HttpServletRequest request, HttpServletResponse response,
			String jsondata) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			compRunSV.saveRunFixed(jsondata);
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			log.error("固定资产贷款-保存企业经营信息异常：", e);
		}
		
		return jRequest.serialize(json, true);
	}
	
}
