package com.beawan.survey.custInfo.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 内部管理情况
 * @author zxh
 * @date 2020/7/17 14:21
 */
@Entity
@Table(name = "CM_INNER_MANAGE",schema = "GDTCESYS")
public class CompInnerManage extends BaseEntity {

    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="CM_INNER_MANAGE")
   // @SequenceGenerator(name="CM_INNER_MANAGE",allocationSize=1,initialValue=1, sequenceName="CM_INNER_MANAGE_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;
    @Column(name = "LEGAL_PERSON_INFO")
    private String legalPersonInfo;// 法定代表人情况
    @Column(name = "CONTROL_PERSON_INFO")
    private String controlPersonInfo;// 实际代表人情况
    @Column(name = "MANAGER_INFO")
    private String managerInfo;// 管理层情况
    @Column(name = "STAFF_INFO")
    private String staffInfo;// 一般员工情况
    @Column(name = "ORGANIZATION")
    private String organization;// 组织架构
    @Column(name = "MANAGE_INFO")
    private String manageInfo;// 管理情况
    @Column(name = "ANALYSIS_INFO")
    private String analysisInfo;// 分析评价

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getLegalPersonInfo() {
        return legalPersonInfo;
    }

    public void setLegalPersonInfo(String legalPersonInfo) {
        this.legalPersonInfo = legalPersonInfo;
    }

    public String getControlPersonInfo() {
        return controlPersonInfo;
    }

    public void setControlPersonInfo(String controlPersonInfo) {
        this.controlPersonInfo = controlPersonInfo;
    }

    public String getManagerInfo() {
        return managerInfo;
    }

    public void setManagerInfo(String managerInfo) {
        this.managerInfo = managerInfo;
    }

    public String getStaffInfo() {
        return staffInfo;
    }

    public void setStaffInfo(String staffInfo) {
        this.staffInfo = staffInfo;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getManageInfo() {
        return manageInfo;
    }

    public void setManageInfo(String manageInfo) {
        this.manageInfo = manageInfo;
    }

    public String getAnalysisInfo() {
        return analysisInfo;
    }

    public void setAnalysisInfo(String analysisInfo) {
        this.analysisInfo = analysisInfo;
    }
}
