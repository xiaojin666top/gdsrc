package com.beawan.survey.custInfo.entity;

//import com.beawan.base.entity.SReportDataId;
import com.beawan.core.BaseEntity;
import com.google.gson.annotations.SerializedName;

import javax.persistence.*;

/**
 * 对外投资信息表
 * 目前直接根据企查查返回数据报文接口
 */
@SuppressWarnings("serial")
@Entity
@Table(schema = "GDTCESYS", name = "CM_INVESTMENT")
public class CompInvestment extends BaseEntity {

	@Id
	@Column(name = "KEY_NO")
    private String keyNo;//KeyNo
	
    @Column(name = "CUSTOMER_NO")
    private String customerNo;//对公客户号
	
    @Column(name = "CREDIT_CODE")
    private String creditCode;//社会统一信用代码
    @Column(name = "ECON_KIND")
    private String econKind;//企业类型
    @Column(name = "FUNDED_RATIO")
    private String fundedRatio;//出资比列
    @Column(name = "IMAGE_URL")
    private String imageUrl;//公司Logo
    @Column(name = "NAME")
    private String name;//企业名称
    @Column(name = "NO")
    private String no;//注册号
    @Column(name = "OPER_NAME")
    private String operName;//法人名称
    @Column(name = "REGIST_CAPI")
    private String registCapi;//注册资本
    @Column(name = "START_DATE")
    private String startDate;//成立日期
    @Column(name = "STATE")
    private String state;//状态
    @Column(name = "ACTUAL_INVESTMENT_AMOUNT")
    private Double actualInvestmentAmount;//实际投资金额
    @Column(name = "BANK_LOANS")
    private Double bankLoans;//本行贷款
    
    
    
	public String getKeyNo() {
		return keyNo;
	}
	public void setKeyNo(String keyNo) {
		this.keyNo = keyNo;
	}
	
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getCreditCode() {
		return creditCode;
	}
	public void setCreditCode(String creditCode) {
		this.creditCode = creditCode;
	}
	public String getEconKind() {
		return econKind;
	}
	public void setEconKind(String econKind) {
		this.econKind = econKind;
	}
	public String getFundedRatio() {
		return fundedRatio;
	}
	public void setFundedRatio(String fundedRatio) {
		this.fundedRatio = fundedRatio;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getOperName() {
		return operName;
	}
	public void setOperName(String operName) {
		this.operName = operName;
	}
	public String getRegistCapi() {
		return registCapi;
	}
	public void setRegistCapi(String registCapi) {
		this.registCapi = registCapi;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Double getActualInvestmentAmount() {
		return actualInvestmentAmount;
	}
	public void setActualInvestmentAmount(Double actualInvestmentAmount) {
		this.actualInvestmentAmount = actualInvestmentAmount;
	}
	public Double getBankLoans() {
		return bankLoans;
	}
	public void setBankLoans(Double bankLoans) {
		this.bankLoans = bankLoans;
	}
    
    
}
