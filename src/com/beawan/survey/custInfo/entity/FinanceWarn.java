package com.beawan.survey.custInfo.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 企业财务预警项、预警提示信息
 * @author zxh
 * @date 2020/8/27 17:34
 */
@Entity
@Table(name = "CM_FINANCE_WARN",schema = "GDTCESYS")
public class FinanceWarn extends BaseEntity {

    @Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_FINANCE_WARN")
  //  @SequenceGenerator(name = "CM_FINANCE_WARN", allocationSize = 1, initialValue = 1, sequenceName = "CM_FINANCE_WARN_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;
    @Column(name = "ABNORMAL_ITEM")
    private String abnormalItem; // 异常项
    @Column(name = "ERR_REASON")
    private String errReason; // 现象描述
    @Column(name = "REASON")
    private String reason; // 异常原因
    @Column(name = "REASON_LIST")
    private String reasonList; // 异常提示(json): ["reason1","reason2",...]
    @Column(name = "REMARK")
    private String remark;// 异常说明

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getAbnormalItem() {
        return abnormalItem;
    }

    public void setAbnormalItem(String abnormalItem) {
        this.abnormalItem = abnormalItem;
    }

    public String getErrReason() {
        return errReason;
    }

    public void setErrReason(String errReason) {
        this.errReason = errReason;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReasonList() {
        return reasonList;
    }

    public void setReasonList(String reasonList) {
        this.reasonList = reasonList;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}