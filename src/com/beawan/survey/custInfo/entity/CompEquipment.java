package com.beawan.survey.custInfo.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 生产设备详情（纺织业）
 * @author zxh
 * @date 2020/7/30 11:15
 */
@Entity
@Table(name = "CM_EQUIPMENT",schema = "GDTCESYS")
public class CompEquipment extends BaseEntity {

    @Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="CM_EQUIPMENT")
   // @SequenceGenerator(name="CM_EQUIPMENT",allocationSize=1,initialValue=1, sequenceName="CM_EQUIPMENT_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;
    @Column(name = "DEVICE_NAME")
    private String deviceName; //生产设备
    @Column(name = "ADVANCED")
    private String advanced; // 先进性
    @Column(name = "COMPETE")
    private String compete; // 竞争能力
    @Column(name = "USE_LEVEL")
    private String useLevel; // 新旧程度
    @Column(name = "COMPLEXITY")
    private String complexity; // 工艺复杂度
    @Column(name = "PRODUCE_CYCLE")
    private String produceCycle; // 生产周期
    @Column(name = "TECHNOLOGY")
    private String technology; // 自有技术
    @Column(name = "REMARK")
    private String remark; // 备注

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getAdvanced() {
        return advanced;
    }

    public void setAdvanced(String advanced) {
        this.advanced = advanced;
    }

    public String getCompete() {
        return compete;
    }

    public void setCompete(String compete) {
        this.compete = compete;
    }

    public String getUseLevel() {
        return useLevel;
    }

    public void setUseLevel(String useLevel) {
        this.useLevel = useLevel;
    }

    public String getComplexity() {
        return complexity;
    }

    public void setComplexity(String complexity) {
        this.complexity = complexity;
    }

    public String getProduceCycle() {
        return produceCycle;
    }

    public void setProduceCycle(String produceCycle) {
        this.produceCycle = produceCycle;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
