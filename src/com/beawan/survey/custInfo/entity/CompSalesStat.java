package com.beawan.survey.custInfo.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 销售环节 销售统计
 * @author zxh
 * @date 2020/7/29 10:24
 */
@Entity
@Table(name = "CM_SALES_STAT",schema = "GDTCESYS")
public class CompSalesStat extends BaseEntity {

    @Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="CM_SALES_STAT")
   // @SequenceGenerator(name="CM_SALES_STAT", sequenceName="CM_SALES_STAT_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;
    @Column(name = "ITEM_NAME")
    private String itemName; // 类别
    @Column(name = "VALUE1")
    private double value1; // 上一年
    @Column(name = "VALUE2")
    private double value2; // 当年
    @Column(name = "VALUE3")
    private double value3;
    @Column(name = "VALUE4")
    private double value4;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getValue1() {
        return value1;
    }

    public void setValue1(double value1) {
        this.value1 = value1;
    }

    public double getValue2() {
        return value2;
    }

    public void setValue2(double value2) {
        this.value2 = value2;
    }

    public double getValue3() {
        return value3;
    }

    public void setValue3(double value3) {
        this.value3 = value3;
    }

    public double getValue4() {
        return value4;
    }

    public void setValue4(double value4) {
        this.value4 = value4;
    }
}
