package com.beawan.survey.custInfo.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * 个人征信简表
 * 征信数据与任务流水号相关联  和贷款客户主体通过任务间接关联
 */

@Entity
@Table(name = "CM_USER_CREDIT",schema = "GDTCESYS")
public class CompUserCredit extends BaseEntity {

    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="CM_USER_CREDIT_SEQ")
   // @SequenceGenerator(name="CM_USER_CREDIT_SEQ",allocationSize=1,initialValue=1, sequenceName="CM_USER_CREDIT_SEQ")
    private Integer id;

    @Column(name = "SERIAL_NUMBER")
    private String serialNumber;
    @Column(name = "QUERY_NAME")
    private String queryName;//被查询人姓名
    @Column(name = "RELEVANCE_TYPE")
    private String relevanceType;//类型 （字典模式：01为授信企业，02为担保企业）
    @Column(name = "QUERY_DATE")
    private String queryDate;//查询日期
    @Column(name = "NO_SETTLE_SURPLUS")
    private String noSettleSyrplus;//未结清贷款余额
    @Column(name = "OVERDUE_AMOUNT")
    private String overdueAmount;//其中：当前逾期金额
    @Column(name = "DEBTCARD_AMOUNT")
    private String debtcardAmount;//贷记卡授信总额
    @Column(name = "DEBTCARD_SURPLUS")
    private String debtcardSurplus;//贷记卡已用额度
    @Column(name = "DEBTCARD_OVERDUE")
    private String debtcardOverdue;//其中：当前逾期余额
    @Column(name = "OVERDUE_INFO")
    private String overdueInfo;//近24个月贷款和贷记卡逾期情况
    @Column(name = "GUARANTEE_AMOUNT")
    private String guaranteeAmount;//对外担保总额
    @Column(name = "GUARANTEE_BALANCE")
    private String guaranteeBalance;//对外担保余额
    @Column(name = "GUARANTEE_BAD")
    private String guaranteeBad;//其中：不良余额
    @Column(name = "REMARK")
    private String remark;//其他事项说明

    @Column(name = "GUA_COMP_ID")
    private String guaCompId;//担保企业主键

    public String getGuaCompId() {
        return guaCompId;
    }

    public void setGuaCompId(String guaCompId) {
        this.guaCompId = guaCompId;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRelevanceType() {
		return relevanceType;
	}

	public void setRelevanceType(String relevanceType) {
		this.relevanceType = relevanceType;
	}

	public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getQueryName() {
        return queryName;
    }

    public void setQueryName(String queryName) {
        this.queryName = queryName;
    }

    public String getQueryDate() {
        return queryDate;
    }

    public void setQueryDate(String queryDate) {
        this.queryDate = queryDate;
    }

    public String getNoSettleSyrplus() {
        return noSettleSyrplus;
    }

    public void setNoSettleSyrplus(String noSettleSyrplus) {
        this.noSettleSyrplus = noSettleSyrplus;
    }

    public String getOverdueAmount() {
        return overdueAmount;
    }

    public void setOverdueAmount(String overdueAmount) {
        this.overdueAmount = overdueAmount;
    }

    public String getDebtcardAmount() {
        return debtcardAmount;
    }

    public void setDebtcardAmount(String debtcardAmount) {
        this.debtcardAmount = debtcardAmount;
    }

    public String getDebtcardSurplus() {
        return debtcardSurplus;
    }

    public void setDebtcardSurplus(String debtcardSurplus) {
        this.debtcardSurplus = debtcardSurplus;
    }

    public String getDebtcardOverdue() {
        return debtcardOverdue;
    }

    public void setDebtcardOverdue(String debtcardOverdue) {
        this.debtcardOverdue = debtcardOverdue;
    }

    public String getOverdueInfo() {
        return overdueInfo;
    }

    public void setOverdueInfo(String overdueInfo) {
        this.overdueInfo = overdueInfo;
    }

    public String getGuaranteeAmount() {
        return guaranteeAmount;
    }

    public void setGuaranteeAmount(String guaranteeAmount) {
        this.guaranteeAmount = guaranteeAmount;
    }

    public String getGuaranteeBad() {
        return guaranteeBad;
    }

    public void setGuaranteeBad(String guaranteeBad) {
        this.guaranteeBad = guaranteeBad;
    }

    public String getGuaranteeBalance() {
		return guaranteeBalance;
	}

	public void setGuaranteeBalance(String guaranteeBalance) {
		this.guaranteeBalance = guaranteeBalance;
	}

	public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
