package com.beawan.survey.custInfo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;

/**
 * 贷前 企业相关人员拜访记录
 * @author User
 *
 */
@Entity
@Table(name = "CM_VISIT_RECORD",schema = "GDTCESYS")
public class VisitRecord extends BaseEntity {

    @Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_VISIT_RECORD")
    //@SequenceGenerator(name = "CM_VISIT_RECORD", allocationSize = 1, initialValue = 1, sequenceName = "CM_VISIT_RECORD_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;
    @Column(name = "VISIT_PERSON")
    private String visitPerson;//对接人
    @Column(name = "POSITION")
    private String position;// 职位
    @Column(name = "VISIT_TIME")
    private String visitTime;// 拜访时间
    @Column(name = "VISIT_CONTENT")
    private String visitContent;// 拜访内容
    @Column(name = "TYPE")
    private String type;// "M"高管,"C"财务,"Y"员工
    
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSerNo() {
		return serNo;
	}
	public void setSerNo(String serNo) {
		this.serNo = serNo;
	}
	public String getVisitPerson() {
		return visitPerson;
	}
	public void setVisitPerson(String visitPerson) {
		this.visitPerson = visitPerson;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getVisitTime() {
		return visitTime;
	}
	public void setVisitTime(String visitTime) {
		this.visitTime = visitTime;
	}
	public String getVisitContent() {
		return visitContent;
	}
	public void setVisitContent(String visitContent) {
		this.visitContent = visitContent;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}