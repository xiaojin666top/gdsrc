package com.beawan.survey.custInfo.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 采购环节 主要原材料
 * @author zxh
 * @date 2020/7/28 14:59
 */
@Entity
@Table(name = "CM_RAW_MATERIAL",schema = "GDTCESYS")
public class CompRawMaterial extends BaseEntity {

    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="CM_RAW_MATERIAL")
  //  @SequenceGenerator(name="CM_RAW_MATERIAL",allocationSize=1,initialValue=1,sequenceName="CM_RAW_MATERIAL_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;
    @Column(name = "PERIOD")
    private String period;// 时期/年份
    @Column(name = "RAW_MATERIAL")
    private String rawMaterial;// 主要原材料
    @Column(name = "PURCHASE_VOLUME")
    private String purchaseVolume;// 采购量
    @Column(name = "PURCHASE_AMT")
    private double purchaseAmt;// 采购额
    @Column(name = "SUPPLY_NAME")
    private String supplyName;// 主要供应商
    @Column(name = "SETTLE_WAY")
    private String settleWay;// 结算方式
    @Column(name = "SETTLE_CYCLE")
    private String settleCycle;// 结算周期

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getRawMaterial() {
        return rawMaterial;
    }

    public void setRawMaterial(String rawMaterial) {
        this.rawMaterial = rawMaterial;
    }

    public String getPurchaseVolume() {
        return purchaseVolume;
    }

    public void setPurchaseVolume(String purchaseVolume) {
        this.purchaseVolume = purchaseVolume;
    }

    public double getPurchaseAmt() {
        return purchaseAmt;
    }

    public void setPurchaseAmt(double purchaseAmt) {
        this.purchaseAmt = purchaseAmt;
    }

    public String getSupplyName() {
        return supplyName;
    }

    public void setSupplyName(String supplyName) {
        this.supplyName = supplyName;
    }

    public String getSettleWay() {
        return settleWay;
    }

    public void setSettleWay(String settleWay) {
        this.settleWay = settleWay;
    }

    public String getSettleCycle() {
        return settleCycle;
    }

    public void setSettleCycle(String settleCycle) {
        this.settleCycle = settleCycle;
    }
}
