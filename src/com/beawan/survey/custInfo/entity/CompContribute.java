package com.beawan.survey.custInfo.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 综合贡献度分析
 * @author zxh
 * @date 2020/7/18 14:14
 */
@Entity
@Table(name = "CM_CONTRIBUTE",schema = "GDTCESYS")
public class CompContribute extends BaseEntity {

    @Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="CM_CONTRIBUTE")
    //@SequenceGenerator(name="CM_CONTRIBUTE",allocationSize=1,initialValue=1, sequenceName="CM_CONTRIBUTE_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;
    @Column(name = "BUSINESS")
    private String business; // 已办业务
    @Column(name = "APPLY_DATE")
    private String applyDate; // yyyyMMdd 同步日期
    @Column(name = "BANK_BALANCE")
    private String bankBalance="0.00"; // 企业存款余额
    @Column(name = "TIME_DEPOSIT")
    private String timeDeposit="0.00";; // 定期存款余额
    @Column(name = "CURRENT_DEPOSIT")
    private String currentDeposit="0.00";; // 活期存款余额
    @Column(name = "LOAN_BALANCE")
    private String loanBalance="0.00";; // 企业贷款余额
    @Column(name = "DEPOSIT_INFO")
    private String depositInfo=""; // 存款情况
    @Column(name = "INCOME_SUM")
    private String incomeSum; // 综合收益
    @Column(name = "LOAN_RECOVERY")
    private String loanRecovery;//上年度贷款回拢
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }
    
    public String getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(String applyDate) {
		this.applyDate = applyDate;
	}

	public String getBankBalance() {
        return bankBalance;
    }

    public void setBankBalance(String bankBalance) {
        this.bankBalance = bankBalance;
    }

    public String getTimeDeposit() {
        return timeDeposit;
    }

    public void setTimeDeposit(String timeDeposit) {
        this.timeDeposit = timeDeposit;
    }

    public String getCurrentDeposit() {
        return currentDeposit;
    }

    public void setCurrentDeposit(String currentDeposit) {
        this.currentDeposit = currentDeposit;
    }

    public String getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(String loanBalance) {
        this.loanBalance = loanBalance;
    }

    public String getIncomeSum() {
        return incomeSum;
    }

    public void setIncomeSum(String incomeSum) {
        this.incomeSum = incomeSum;
    }
    
    public String getDepositInfo() {
        return depositInfo;
    }

    public void setDepositInfo(String depositInfo) {
        this.depositInfo = depositInfo;
    }

	public String getLoanRecovery() {
		return loanRecovery;
	}

	public void setLoanRecovery(String loanRecovery) {
		this.loanRecovery = loanRecovery;
	}
    
    
}
