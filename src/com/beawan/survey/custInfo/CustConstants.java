package com.beawan.survey.custInfo;

/**
 * 系统常量
 * 
 * 
 */
public class CustConstants {
	
	/**
	 * 客户类型
	 * @author beawan_fengjj
	 *
	 */
	public interface CusType{
		/**
		 * 对公客户
		 */
		public static final String CUS_COMP = "C";
		/**
		 * 个人客户
		 */
		public static final String CUS_PERSON = "P";
		
	}
	
	
	public static final String[] CUST_FINANCE_FUND_SOUR = 
		{	
			"净利润",
			"折扣",
			"增加注册资本",
			"增加银行借款",
			"增加其他借款",
			"现金流流入小计",
			"购置土地",
			"购置房产",
			"购置设备",
			"新增在建工程",
			"对外投资",
			"股东分红",
			"其他",
			"现金净流出小计"
		};
	
	public static final String[] CUST_LONG_ASSETS = 
		{	
			"房屋建筑物",
			"机器设备",
			"运输设备",
			"电子设备"
		};
	
	public static final String[] CUST_FINANCING_ANALY = 
		{	
			"我行",
			"我行",
			"XX银行",
			"XX银行",
			"XX银行",
			"XX银行",
			"XX融资租赁公司",
			"XX小贷公司",
			"债券",
			"民间借款",
		};
	
	public static final String[] CUST_FINANCING_COOP = 
		{	
			"①一般性存款",
			"②保证金帐户存款我行",
			"③定期存款",
			"④外币存款",
			"⑤利息收入",
			"⑥中间业务收入"
		};
	
	public static final String[] CUST_LAND_HOUSE= 
		{	
			"土地",
			"房产"
		};
	
	public static final String[] CUST_GP_DATA_SOUR= 
		{	
			"单体报表",
			"合并报表"
		};
	
	public static final String[] CUST_GP_SUB_FINANCE= 
		{	
				"行业一：",
				"子公司1",
				"子公司2",
				"行业二：",
				"子公司1",
				"子公司2",
				"行业三：",
				"子公司1",
				"其他行业：",
				"子公司1"
		};
	
	public static final String[] GROUP_SUB_RUN= 
		{	
			"集团合计",
			"集团本级"
		};
	
	public static String FUND_NET_PROFIT = "净利润";
	
	public static String FUND_IN_PROJECT = "在建工程";
	
	public static String FUND_GROW_PROJECT = "新增在建工程";
	
	public static final String[] CUST_LOW_FINANCE_RUN = {
			"销售	自制报表",
			"销售 税务报表",
			"利润总额	自制报表",
	};
	
	public static final String[] CUST_FINANCE_FUND_SOUR_PROFIT = 
		{	
			"生产量（注明：单位）",
			"销售量（注明：单位）",
			"主要原材料价格",
			"销售价格",
			"销售收入",
			"其中：开票",
			"不开票",
			"交纳增值税额",
			"其它利润",
			"利润总额",
			" 所得税",
			"净利润+折旧",
		};
	
	public static final String[] PROF_GUARA__SITUA= 
		{	
			"1．担保余额合计",
			"其中：企业法人客户合计",
			"个人客户合计",
			"其他客户合计",
			"2．担保余额中反担保额合计",
			"3．不良担保余额合计",
			"4．担保累计额",
			"5．反担保额累计",
			"6．不良担保累计额"
		};
	
	public static final String[] PROF_GUARA__BALANCE= 
		{	
			"货币资金",
			"短期投资",
			"其他应收款",
			"应收代偿款",
			"存出保证金",
			"长期投资",
			"固定资产",
			""
		};
	
	public static final String[] PROF_GUARA__DEBT= 
		{	
			"短期借款（长期借款）",
			"存入保证金",
			"担保赔偿准备",
			"短期责任准备",
			"其他应付款",
			"",
			""
		};
	
	public static final String[] PROF_GUARA__NET= 
		{	
			"实收资本",
			"一般风险准备",
			"未分配利润",
			""
		};
	
	public static final String[] PROF_GUARA__BUSS_STRU_TYPE={
			"公司客户",
			"个人客户",
			"其他客户"
	};
	
	public static final String[] PROF_GUARA__FIVA_SORT={
			"正常类",
			"关注类",
			"次级类",
			"可疑类",
			"损失类"
	};
	
	public static String FUND_PROFIT_SALE = "销售收入";
	
	public static String FUND_PROFIT_TOTAL_PROFIT = "利润总额";
	
	public static String FUND_PROFIT_TOTAL_TAX = " 所得税";
	
	
	public static final String[] FINA_RECE_DATA_SUBJ= 
		{	
			"资产总计",
			"流动资产合计",
			"货币资金",
			"应收票据",
			"应收账款",
			"预付账款",
			"其他应收款",
			"存货",
			"其他流动资产",
			"非流动资产合计",
			"长期股权投资",
			"投资性房地产",
			"固定资产合计",
			"在建工程",
			"无形资产",
			"负债合计",
			"流动负债合计",
			"短期借款",
			"应付票据",
			"应付账款",
			"预收账款",
			"其他应付款",
			"其他流动负债",
			"非流动负债合计",
			"长期借款",
			"应付债券",
			"长期应付款",
			"所有者权益合计",
			"实收资本",
			"资本公积",
			"未分配利润",
			"主营业务收入",
			"主营业务成本",
			"主营业务税金及附加",
			"主营业务利润",
			"销售费用",
			"管理费用",
			"财务费用",
			"资产减值损失",
			"其他经营收益",
			"公允价值变动净收益",
			"投资净收益",
			"营业利润",
			"营业外收入",
			"营业外支出",
			"利润总额",
			"净利润",
			"经营活动现金流入小计",
			"经营活动产生的现金流量净额",
			"投资活动产生的现金流量净额",
			"筹资活动产生的现金流量净额",
			"现金及现金等价物净增加",
			"资产负债比率",
			"流动比率",
			"速动比率",
			"应收账款周转率",
			"存货周转率",
			"营业利润率",
			"销售净利润率"
		};
	
}
