package com.beawan.survey.custInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.bean.CompRunProduct;

/**
 * @author zxh
 * @date 2020/7/9 17:36
 */
public interface CompProductDao extends BaseDao<CompRunProduct> {
}
