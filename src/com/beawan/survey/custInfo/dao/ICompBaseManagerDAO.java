package com.beawan.survey.custInfo.dao;

import java.util.List;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.custInfo.bean.CompBaseManager;

/**
 * @Description 企业管理人员信息持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface ICompBaseManagerDAO  extends ICommDAO<CompBaseManager> {

	/**
	 * @Description (根据客户号查询企业管理人员信息)
	 * @param customerNo 客户号
	 * @return
	 * @throws  
	 */
	public List<CompBaseManager> queryByCustNo(String customerNo);
}
