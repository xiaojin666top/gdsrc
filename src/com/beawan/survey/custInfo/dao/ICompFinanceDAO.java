package com.beawan.survey.custInfo.dao;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.custInfo.bean.CompFinance;

public interface ICompFinanceDAO extends ICommDAO<CompFinance> {
	
	/**
	 * @Description (根据客户号和任务号查询企业财务状况)
	 * @param custNo
	 *            客户号
	 * @return
	 */
	public CompFinance queryByTaskIdAndCustNo(Long taskId,String custNo) throws DataAccessException;

}
