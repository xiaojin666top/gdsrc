package com.beawan.survey.custInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.entity.CompContribute;

/**
 * @author yzj
 */
public interface CompContributeDao extends BaseDao<CompContribute> {
}
