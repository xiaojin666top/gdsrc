package com.beawan.survey.custInfo.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.custInfo.bean.CompFinancingExtGuara;

/**
 * @Description 企业对外融资与担保信息持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface ICompFinancingExtGuaraDAO  extends ICommDAO<CompFinancingExtGuara> {

	/**
	 * @Description 根据客户号和任务号查找对外融资与担保信息
	 * @param taskId
	 * @param custNo
	 * @return
	 * @throws DataAccessException
	 */
	public List<CompFinancingExtGuara> queryByTaskIdAndCustNo(long taskId,
			String custNo) throws DataAccessException;
}
