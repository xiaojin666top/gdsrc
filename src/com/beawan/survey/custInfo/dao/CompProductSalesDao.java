package com.beawan.survey.custInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.bean.CompProductSales;

import java.util.List;

/**
 * @author yzj
 */
public interface CompProductSalesDao extends BaseDao<CompProductSales> {

    List<CompProductSales> getBySerNoAndYear(String serNo, int year);
}
