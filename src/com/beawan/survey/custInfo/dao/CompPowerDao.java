package com.beawan.survey.custInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.bean.CompPower;

/**
 * @author yzj
 */
public interface CompPowerDao extends BaseDao<CompPower> {
}
