package com.beawan.survey.custInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.entity.CompInnerManage;

/**
 * @author yzj
 */
public interface CompInnerManageDao extends BaseDao<CompInnerManage> {
}
