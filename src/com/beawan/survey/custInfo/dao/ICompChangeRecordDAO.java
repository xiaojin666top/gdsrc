package com.beawan.survey.custInfo.dao;

import java.util.List;

import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.bean.CompChangeRecord;
import com.beawan.survey.custInfo.dto.CompChangeRecordDto;

public interface ICompChangeRecordDAO  extends BaseDao<CompChangeRecord>{

	List<CompChangeRecordDto> groupByItemWhereCustNo(String custNo);
	
	
}
