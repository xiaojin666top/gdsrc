package com.beawan.survey.custInfo.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.custInfo.bean.CompFinancingBank;

/**
 * @Description 企业银行融资信息持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface ICompFinancingBankDAO  extends ICommDAO<CompFinancingBank> {

	/**
	 * @Description 查询本银行融资信息
	 * @param taskId 客户号
	 * @param customerNo 任务号
	 * @return
	 * @throws DataAccessException
	 */
	public List<CompFinancingBank> queryMBankFinance(long taskId, String customerNo) throws DataAccessException;
	
	/**
	 * 获取其他（非我行）的融资数据
	 * @param taskId
	 * @param customerNo
	 * @return
	 */
	public List<CompFinancingBank> queryOtherFinaBank(Long taskId, String customerNo);
}
