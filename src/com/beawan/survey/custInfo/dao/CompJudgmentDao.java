package com.beawan.survey.custInfo.dao;

import java.util.List;

import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.dto.CompJudgStatDto;
import com.beawan.survey.custInfo.dto.CompJudgmentDto;
import com.beawan.survey.custInfo.entity.CompJudgment;

/**
 * @author yzj
 */
public interface CompJudgmentDao extends BaseDao<CompJudgment> {
	
	/**
	 * 获取   截止 贷前调查开始的 近半年 中， 企业作为被告的最近3条数据
	 * @param loanTime		yyyy-MM-dd
	 * @param custNo
	 * @return
	 * @throws Exception
	 */
	public List<CompJudgmentDto> getNewestJudgeList(String loanTime, String startDate, String custNo) throws Exception;

	/**
	 * 分涉诉类型、案由类型统计条数
	 * @param isDefendant	是否至查询被告
	 * @param statCaseType
	 * @param statCaseReason
	 * @return
	 */
	public List<CompJudgStatDto> getJudgStatList(String startDate, String endDate, String custNo,
			boolean isDefendant, boolean statCaseType, boolean statCaseReason) throws Exception;

}
