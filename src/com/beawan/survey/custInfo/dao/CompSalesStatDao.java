package com.beawan.survey.custInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.dto.SalesStatDto;
import com.beawan.survey.custInfo.entity.CompSalesStat;

import java.util.List;

/**
 * @author yzj
 */
public interface CompSalesStatDao extends BaseDao<CompSalesStat> {
}
