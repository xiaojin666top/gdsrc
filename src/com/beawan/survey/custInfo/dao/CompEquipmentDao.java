package com.beawan.survey.custInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.entity.CompEquipment;

/**
 * @author yzj
 */
public interface CompEquipmentDao extends BaseDao<CompEquipment> {
}
