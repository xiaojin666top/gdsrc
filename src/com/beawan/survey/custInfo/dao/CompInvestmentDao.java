package com.beawan.survey.custInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.entity.CompInvestment;

/**
 * @author yzj
 */
public interface CompInvestmentDao extends BaseDao<CompInvestment> {
}
