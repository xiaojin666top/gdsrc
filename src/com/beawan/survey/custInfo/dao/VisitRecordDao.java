package com.beawan.survey.custInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.entity.VisitRecord;

/**
 * @author yzj
 */
public interface VisitRecordDao extends BaseDao<VisitRecord> {
}
