package com.beawan.survey.custInfo.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.custInfo.bean.CompFinanceTax;

/**
 * @Description 企业缴税信息记录持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface ICompFinanceTaxDAO  extends ICommDAO<CompFinanceTax> {

	/**
	 * @Description (根据客户号和任务号查询企业缴税信息记录)
	 * @param taskId
	 * @param custNo
	 * @return
	 * @throws DataAccessException
	 */
	public List<CompFinanceTax> queryByTaskIdAndCustNo(long taskId,
			String custNo) throws DataAccessException;
}
