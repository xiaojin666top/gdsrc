package com.beawan.survey.custInfo.dao;

import java.util.List;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.custInfo.bean.CompBaseEquity;

/**
 * @Description 企业股权结构信息持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface ICompBaseEquityDAO  extends ICommDAO<CompBaseEquity> {

	/**
	 * @Description (根据客户号查询企业股权结构)
	 * @param customerNo 客户号
	 * @return
	 */
	public List<CompBaseEquity> queryByCustNo(String customerNo);
}
