package com.beawan.survey.custInfo.dao;

import java.util.List;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.custInfo.bean.CompBaseEquity;
import com.beawan.survey.custInfo.bean.CompBaseItemChange;

/**
 * @Description 企业变更信息持久化接口
 * @author yzj
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface ICompBaseItemChangeDAO  extends ICommDAO<CompBaseItemChange> {

	/**
	 * @Description (根据客户号查询企业变更记录)
	 * @param customerNo 客户号
	 * @return
	 */
	public List<CompBaseItemChange> queryByCustNo(String customerNo);
}
