package com.beawan.survey.custInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.bean.CompRunSupply;

/**
 * @author yzj
 */
public interface CompRunSupplyDao extends BaseDao<CompRunSupply> {

}
