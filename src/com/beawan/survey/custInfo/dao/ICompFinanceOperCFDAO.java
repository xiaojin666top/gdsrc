package com.beawan.survey.custInfo.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.custInfo.bean.CompFinanceOperCF;

/**
 * @Description 企业经营性现金流回笼记录信息持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface ICompFinanceOperCFDAO  extends ICommDAO<CompFinanceOperCF> {

	/**
	 * @Description (根据客户号和任务号查询企业经营性现金流回笼记录)
	 * @param taskId
	 * @param custNo
	 * @return
	 * @throws DataAccessException
	 */
	public List<CompFinanceOperCF> queryByTaskIdAndCustNo(long taskId,
			String custNo) throws DataAccessException;
}
