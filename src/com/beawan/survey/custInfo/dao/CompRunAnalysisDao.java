package com.beawan.survey.custInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.bean.CompRunAnalysis;

/**
 * @author yzj
 */
public interface CompRunAnalysisDao extends BaseDao<CompRunAnalysis> {
}
