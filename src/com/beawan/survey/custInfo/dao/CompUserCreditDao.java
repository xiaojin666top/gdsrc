package com.beawan.survey.custInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.entity.CompUserCredit;

/**
 * @author yzj
 */
public interface CompUserCreditDao extends BaseDao<CompUserCredit> {
}
