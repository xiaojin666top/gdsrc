package com.beawan.survey.custInfo.dao;

import com.beawan.common.dao.ICommDAO;
import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.bean.CompBase;

public interface ICompBaseDAO extends ICommDAO<CompBase> {
	
	/**
	 * @Description (根据客户号查询企业基本信息)
	 * @param customerNo 客户号
	 * @return
	 */
	public CompBase queryByCustNo(String customerNo) throws Exception;

	/**
	 * 根据 客户名 查询企业基本信息
	 * @param customerName 客户名
	 * @return
	 */
	public CompBase queryByCustomerName(String customerName);
}
