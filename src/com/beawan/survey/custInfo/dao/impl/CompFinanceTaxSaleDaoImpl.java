package com.beawan.survey.custInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.custInfo.dao.CompFinanceTaxSaleDao;
import com.beawan.survey.custInfo.bean.CompFinanceTaxSale;
import org.springframework.stereotype.Repository;
import com.beawan.survey.custInfo.bean.CompFinanceTaxSale;

/**
 * @author yzj
 */
@Repository("compFinanceTaxSaleDao")
public class CompFinanceTaxSaleDaoImpl extends BaseDaoImpl<CompFinanceTaxSale> implements CompFinanceTaxSaleDao{

}
