package com.beawan.survey.custInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.custInfo.bean.CompBaseManager;
import com.beawan.survey.custInfo.dao.ICompManagerDAO;
import org.springframework.stereotype.Repository;

/**
 * @author zxh
 * @date 2020/7/2 20:51
 */
@Repository("compManagerDAO")
public class CompManagerDAOImpl extends BaseDaoImpl<CompBaseManager> implements ICompManagerDAO {
}
