package com.beawan.survey.custInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.custInfo.dao.CompInvestmentDao;
import com.beawan.survey.custInfo.entity.CompInvestment;
import org.springframework.stereotype.Repository;
import com.beawan.survey.custInfo.entity.CompInvestment;

/**
 * @author yzj
 */
@Repository("compInvestmentDao")
public class CompInvestmentDaoImpl extends BaseDaoImpl<CompInvestment> implements CompInvestmentDao{

}
