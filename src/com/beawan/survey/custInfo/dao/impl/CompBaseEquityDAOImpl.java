package com.beawan.survey.custInfo.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.custInfo.bean.CompBaseEquity;
import com.beawan.survey.custInfo.dao.ICompBaseEquityDAO;

@Service("compBaseEquityDAO")
public class CompBaseEquityDAOImpl extends CommDAOImpl<CompBaseEquity>
		implements ICompBaseEquityDAO {

	@Override
	public List<CompBaseEquity> queryByCustNo(String customerNo){
		Map<String, Object> params = new HashMap<>();
		params.put("customer_No", customerNo);
		return this.selectByProperty(CompBaseEquity.class, params);
	}

}
