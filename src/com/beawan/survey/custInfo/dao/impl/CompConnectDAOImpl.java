package com.beawan.survey.custInfo.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.custInfo.bean.CompConnetCm;
import com.beawan.survey.custInfo.dao.ICompConnectDAO;

@Repository("compConnectDAO")
public class CompConnectDAOImpl extends CommDAOImpl<CompConnetCm> implements ICompConnectDAO {

	@Override
	public List<CompConnetCm> queryByCustNo(String customerNo) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("customerNo", customerNo);
		return this.selectByProperty(CompConnetCm.class, params);
	}

}
