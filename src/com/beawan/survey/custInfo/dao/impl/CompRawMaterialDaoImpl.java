package com.beawan.survey.custInfo.dao.impl;

import com.beawan.common.Constants;
import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.custInfo.dao.CompRawMaterialDao;
import com.beawan.survey.custInfo.entity.CompRawMaterial;
import org.springframework.stereotype.Repository;
import com.beawan.survey.custInfo.entity.CompRawMaterial;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yzj
 */
@Repository("compRawMaterialDao")
public class CompRawMaterialDaoImpl extends BaseDaoImpl<CompRawMaterial> implements CompRawMaterialDao{

    @Override
    public List<CompRawMaterial> getBySerNoAndYear(String serNo, int year){
        String query = "SER_NO = :serNo AND PERIOD LIKE :year AND STATUS = :status";
        Map<String, Object> params = new HashMap<>();
        params.put("status", Constants.NORMAL);
        params.put("serNo", serNo);
        params.put("year", "%" + year + "%");
        List<CompRawMaterial> list = this.select(query, params);
        Collections.sort(list, new Comparator<CompRawMaterial>() {
			@Override
			public int compare(CompRawMaterial arg0, CompRawMaterial arg1) {
				
				return (int) arg1.getPurchaseAmt() - (int)arg0.getPurchaseAmt();
			}
		});
        return list;
    }
}
