package com.beawan.survey.custInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.custInfo.dao.CompPenaltyDao;
import com.beawan.survey.custInfo.bean.CompPenalty;
import org.springframework.stereotype.Repository;
import com.beawan.survey.custInfo.bean.CompPenalty;

/**
 * @author yzj
 */
@Repository("compPenaltyDao")
public class CompPenaltyDaoImpl extends BaseDaoImpl<CompPenalty> implements CompPenaltyDao{

}
