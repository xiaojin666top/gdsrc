package com.beawan.survey.custInfo.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.custInfo.bean.CompFinance;
import com.beawan.survey.custInfo.dao.ICompFinanceDAO;

@Repository("compFinanceDAO")
public class CompFinanceDAOImpl extends CommDAOImpl<CompFinance> implements ICompFinanceDAO {

	@Override
	public CompFinance queryByTaskIdAndCustNo(Long taskId, String custNo) throws DataAccessException {
		
		String hql = "taskId=:taskId and customerNo=:customerNo";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("taskId", taskId);
		params.put("customerNo", custNo);
		return selectSingle(hql, params);
	}

}
