package com.beawan.survey.custInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.custInfo.dao.FinanceWarnDao;
import com.beawan.survey.custInfo.entity.FinanceWarn;
import org.springframework.stereotype.Repository;
import com.beawan.survey.custInfo.entity.FinanceWarn;

/**
 * @author yzj
 */
@Repository("financeWarnDao")
public class FinanceWarnDaoImpl extends BaseDaoImpl<FinanceWarn> implements FinanceWarnDao{

}
