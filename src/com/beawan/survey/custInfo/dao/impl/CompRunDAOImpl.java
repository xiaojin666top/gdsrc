package com.beawan.survey.custInfo.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.beawan.common.dao.impl.DaoHandler;
import com.beawan.survey.custInfo.bean.CompRunConst;
import com.beawan.survey.custInfo.bean.CompRunConstSPDetail;
import com.beawan.survey.custInfo.bean.CompRunConstSPStruc;
import com.beawan.survey.custInfo.bean.CompRunCustomer;
import com.beawan.survey.custInfo.bean.CompRunEsta;
import com.beawan.survey.custInfo.bean.CompRunEstaDevedProj;
import com.beawan.survey.custInfo.bean.CompRunEstaDevingProj;
import com.beawan.survey.custInfo.bean.CompRunEstaLand;
import com.beawan.survey.custInfo.bean.CompRunFixed;
import com.beawan.survey.custInfo.bean.CompRunGeneral;
import com.beawan.survey.custInfo.bean.CompRunPower;
import com.beawan.survey.custInfo.bean.CompRun;
import com.beawan.survey.custInfo.bean.CompRunArea;
import com.beawan.survey.custInfo.bean.CompRunAreaCf;
import com.beawan.survey.custInfo.bean.CompRunAreaTd;
import com.beawan.survey.custInfo.bean.CompRunPrdSale;
import com.beawan.survey.custInfo.bean.CompRunProduce;
import com.beawan.survey.custInfo.bean.CompRunSupply;
import com.beawan.survey.custInfo.dao.ICompRunDAO;

@Repository("compRunDAO")
public class CompRunDAOImpl extends DaoHandler implements ICompRunDAO {
	
	@Override
	public <T> T saveEntity(T entity) throws DataAccessException {
		return save(entity);
	}
	
	@Override
	public <T> void deleteEntity(T entity) throws DataAccessException {
		this.delete(entity);
	}
	
	@Override
	public <T> T queryById(Class<T> cls, Object id) throws DataAccessException {
		return selectByPrimaryKey(cls, id);
	}
	
	@Override
	public <T> List<T> query(Class<T> cls, String query, Map<String, Object> args) throws DataAccessException {
		return select(cls, query, args);
	}
	
	@Override
	public CompRun queryCompRunByTIdAndNo(Long taskId, String customerNo) throws DataAccessException {
		HashMap<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		params.put("customerNo", customerNo);
		List<CompRun> list = selectByProperty(CompRun.class, params);
		if (list == null || list.size() <= 0) return null;
		return list.get(0);
	}

	@Override
	public CompRun saveCompRun(CompRun compRun) throws DataAccessException {
		return save(CompRun.class, compRun);
	}
	
	@Override
	public void deleteCompRun(CompRun compRun) throws DataAccessException {
		delete(CompRun.class, compRun);
	}

	@Override
	public List<CompRunPower> queryRunPowerByTIdAndNo(Long taskId, String custNo) throws DataAccessException {
		String query = "taskId = " + taskId + " and customerNo = '" + custNo + "' order by itemCode,month asc";
		return select(CompRunPower.class, query);
	}
	
	@Override
	public CompRunPower saveRunPower(CompRunPower power) throws DataAccessException {
		return save(CompRunPower.class, power);
	}
	
	@Override
	public void deleteRunPower(CompRunPower power) throws DataAccessException {
		delete(CompRunPower.class, power);
	}
	
	@Override
	public List<CompRunArea> queryRunAreaByTIdAndNo(Long taskId, String customerNo) throws DataAccessException {
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		params.put("customerNo", customerNo);
		return selectByProperty(CompRunArea.class, params);
	}
	@Override
	public List<CompRunAreaTd> queryRunAreaLandByTIdAndNo(Long taskId, String customerNo) throws DataAccessException {
		Map<String, Object> params = new HashMap<>();
		params.put("task_Id", taskId);
		params.put("customer_No", customerNo);
		return selectByProperty(CompRunAreaTd.class, params);
	}
	@Override
	public List<CompRunAreaCf> queryRunAreaPlantByTIdAndNo(Long taskId, String customerNo) throws DataAccessException {
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		params.put("customerNo", customerNo);
		return selectByProperty(CompRunAreaCf.class, params);
	}
	
	@Override
	public CompRunArea saveRunArea(CompRunArea runArea) throws DataAccessException {
		return save(CompRunArea.class, runArea);
	}
	
	@Override
	public CompRunAreaTd saveRunAreaLand(CompRunAreaTd runArea) throws DataAccessException {
		return save(CompRunAreaTd.class, runArea);
	}
	
	@Override
	public CompRunAreaCf saveRunAreaPlant(CompRunAreaCf runArea) throws DataAccessException {
		return save(CompRunAreaCf.class, runArea);
	}
	
	@Override
	public void deleteRunArea(CompRunArea runArea) throws DataAccessException {
		delete(CompRunArea.class, runArea);
	}

	@Override
	public List<CompRunSupply> queryRunSupplyByTIdAndNo(Long taskId, String customerNo) throws DataAccessException {
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		params.put("customerNo", customerNo);
		return selectByProperty(CompRunSupply.class, params);
	}

	@Override
	public CompRunSupply saveRunSupply(CompRunSupply supply) throws DataAccessException {
		return save(CompRunSupply.class, supply);
	}
	
	@Override
	public void deleteRunSupply(CompRunSupply supply) throws DataAccessException {
		delete(CompRunSupply.class, supply);
	}

	@Override
	public List<CompRunCustomer> queryRunCustomerByTIdAndNo(Long taskId, String custNo) throws DataAccessException {
		String query = "taskId = " + taskId + " and customerNo = '" + custNo + "'";
		return select(CompRunCustomer.class, query);
	}
	
	@Override
	public CompRunCustomer saveRunCustomer(CompRunCustomer customer) throws DataAccessException {
		return save(CompRunCustomer.class, customer);
	}

	@Override
	public void deleteRunCustomer(CompRunCustomer customer) throws DataAccessException {
		delete(CompRunCustomer.class, customer);
	}

	@Override
	public CompRunProduce queryRunProduceByTIdAndNo(Long taskId, String custNo)
			throws DataAccessException {
		String query = "taskId = " + taskId + " and customerNo = '" + custNo + "'";
		return selectSingle(CompRunProduce.class, query);
	}

	@Override
	public CompRunProduce saveRunProduce(CompRunProduce compRunProduce)
			throws DataAccessException {
		return save(CompRunProduce.class, compRunProduce);
	}

	@Override
	public void deleteRunProduce(CompRunProduce compRunProduce)
			throws DataAccessException {
		delete(CompRunProduce.class, compRunProduce);
	}

	@Override
	public List<CompRunPrdSale> queryRunPrdSaleByTIdAndNo(Long taskId, String custNo)
			throws DataAccessException {
		String query = "taskId = " + taskId + " and customerNo = '" + custNo + "' order by id,year asc";
		return select(CompRunPrdSale.class, query);
	}

	@Override
	public CompRunPrdSale saveRunPrdSale(CompRunPrdSale compRunPrdSale)
			throws DataAccessException {
		return save(CompRunPrdSale.class, compRunPrdSale);
	}

	@Override
	public void deleteRunPrdSale(CompRunPrdSale compRunPrdSale)
			throws DataAccessException {
		delete(CompRunPrdSale.class, compRunPrdSale);
	}

	@Override
	public CompRunConst queryRunConstByTIdAndNo(Long taskId, String custNo)
			throws DataAccessException {
		String query = "taskId = " + taskId + " and customerNo = '" + custNo + "'";
		return selectSingle(CompRunConst.class, query);
	}

	@Override
	public CompRunConst saveRunConst(CompRunConst compRunConst)
			throws DataAccessException {
		return save(CompRunConst.class, compRunConst);
	}

	@Override
	public void deleteRunConst(CompRunConst compRunConst)
			throws DataAccessException {
		delete(CompRunConst.class, compRunConst);
	}

	@Override
	public List<CompRunConstSPDetail> queryRunConstSPDetailByTIdAndNo(Long taskId, String custNo)
			throws DataAccessException {
		String query = "taskId = " + taskId + " and customerNo = '" + custNo + "'";
		return select(CompRunConstSPDetail.class, query);
	}

	@Override
	public CompRunConstSPDetail saveRunConstSPDetail(CompRunConstSPDetail compRunConstSPDetail)
			throws DataAccessException {
		return save(CompRunConstSPDetail.class, compRunConstSPDetail);
	}

	@Override
	public void deleteRunConstSPDetail(CompRunConstSPDetail compRunConstSPDetail)
			throws DataAccessException {
		delete(CompRunConstSPDetail.class, compRunConstSPDetail);
	}

	@Override
	public List<CompRunConstSPStruc> queryRunConstSPStrucByTIdAndNo(Long taskId, String custNo)
			throws DataAccessException {
		String query = "taskId = " + taskId + " and customerNo = '" + custNo + "'";
		return select(CompRunConstSPStruc.class, query);
	}

	@Override
	public CompRunConstSPStruc saveRunConstSPStruc(CompRunConstSPStruc compRunConstSPStruc)
			throws DataAccessException {
		return save(CompRunConstSPStruc.class, compRunConstSPStruc);
	}

	@Override
	public void deleteRunConstSPStruc(CompRunConstSPStruc compRunConstSPStruc)
			throws DataAccessException {
		delete(CompRunConstSPStruc.class, compRunConstSPStruc);
	}

	@Override
	public CompRunFixed queryRunFixedByTIdAndNo(Long taskId, String custNo)
			throws DataAccessException {
		String query = "taskId = " + taskId + " and customerNo = '" + custNo + "'";
		return selectSingle(CompRunFixed.class, query);
	}

	@Override
	public CompRunFixed saveRunFixed(CompRunFixed compRunFixed)
			throws DataAccessException {
		return save(CompRunFixed.class, compRunFixed);
	}

	@Override
	public void deleteRunFixed(CompRunFixed compRunFixed)
			throws DataAccessException {
		delete(CompRunFixed.class, compRunFixed);
	}

	@Override
	public CompRunEsta queryRunEstaByTIdAndNo(Long taskId, String custNo)
			throws DataAccessException {
		String query = "taskId = " + taskId + " and customerNo = '" + custNo + "'";
		return selectSingle(CompRunEsta.class, query);
	}

	@Override
	public CompRunEsta saveRunEsta(CompRunEsta compRunEsta) throws DataAccessException {
		return save(CompRunEsta.class, compRunEsta);
	}

	@Override
	public void deleteRunEsta(CompRunEsta compRunEsta)
			throws DataAccessException {
		delete(CompRunEsta.class, compRunEsta);
	}

	@Override
	public List<CompRunEstaLand> queryRunEstaLandByTIdAndNo(Long taskId, String custNo)
			throws DataAccessException {
		String query = "taskId = " + taskId + " and customerNo = '" + custNo + "'";
		return select(CompRunEstaLand.class, query);
	}

	@Override
	public CompRunEstaLand saveRunEstaLand(CompRunEstaLand compRunEstaLand)
			throws DataAccessException {
		return save(CompRunEstaLand.class, compRunEstaLand);
	}

	@Override
	public void deleteRunEstaLand(CompRunEstaLand compRunEstaLand)
			throws DataAccessException {
		delete(CompRunEstaLand.class, compRunEstaLand);
	}

	@Override
	public List<CompRunEstaDevingProj> queryRunEstaDevingProjByTIdAndNo(Long taskId,
			String custNo) throws DataAccessException {
		String query = "taskId = " + taskId + " and customerNo = '" + custNo + "'";
		return select(CompRunEstaDevingProj.class, query);
	}

	@Override
	public CompRunEstaDevingProj saveRunEstaDevingProj(
			CompRunEstaDevingProj compRunEstaDevingProj)
			throws DataAccessException {
		return save(CompRunEstaDevingProj.class, compRunEstaDevingProj);
	}

	@Override
	public void deleteRunEstaDevingProj(
			CompRunEstaDevingProj compRunEstaDevingProj)
			throws DataAccessException {
		delete(CompRunEstaDevingProj.class, compRunEstaDevingProj);
	}

	@Override
	public List<CompRunEstaDevedProj> queryRunEstaDevedProjByTIdAndNo(Long taskId,
			String custNo) throws DataAccessException {
		String query = "taskId = " + taskId + " and customerNo = '" + custNo + "'";
		return select(CompRunEstaDevedProj.class, query);
	}

	@Override
	public CompRunEstaDevedProj saveRunEstaDevedProj(CompRunEstaDevedProj compRunEstaDevedProj)
			throws DataAccessException {
		return save(CompRunEstaDevedProj.class, compRunEstaDevedProj);
	}

	@Override
	public void deleteRunEstaDevedProj(
			CompRunEstaDevedProj compRunEstaDevedProj)
			throws DataAccessException {
		delete(CompRunEstaDevedProj.class, compRunEstaDevedProj);
	}

	@Override
	public CompRunGeneral queryRunGeneralByTIdAndNo(Long taskId, String custNo)
			throws DataAccessException {
		String query = "taskId = " + taskId + " and customerNo = '" + custNo + "'";
		return selectSingle(CompRunGeneral.class, query);
	}

	@Override
	public CompRunGeneral saveRunGeneral(CompRunGeneral compRunGeneral)
			throws DataAccessException {
		return save(CompRunGeneral.class, compRunGeneral);
	}

	@Override
	public void deleteRunGeneral(CompRunGeneral compRunGeneral)
			throws DataAccessException {
		delete(CompRunGeneral.class, compRunGeneral);
	}
	
}
