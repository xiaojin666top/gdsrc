package com.beawan.survey.custInfo.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.custInfo.bean.CompFinancingExtGuara;
import com.beawan.survey.custInfo.dao.ICompFinancingExtGuaraDAO;

@Repository("compFinancingExtGuaraDAO")
public class CompFinancingExtGuaraDAOImpl extends CommDAOImpl<CompFinancingExtGuara>
		implements ICompFinancingExtGuaraDAO {

	@Override
	public List<CompFinancingExtGuara> queryByTaskIdAndCustNo(long taskId, String custNo)
			throws DataAccessException {

		Map<String, Object> params = new HashMap<>();
		params.put("task_Id", taskId);
		params.put("customer_No", custNo);
		return this.selectByProperty(params);
	}

}
