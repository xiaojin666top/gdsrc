package com.beawan.survey.custInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.custInfo.dao.CompSalesStatDao;
import com.beawan.survey.custInfo.entity.CompSalesStat;
import org.springframework.stereotype.Repository;
import com.beawan.survey.custInfo.entity.CompSalesStat;

/**
 * @author yzj
 */
@Repository("compSalesStatDao")
public class CompSalesStatDaoImpl extends BaseDaoImpl<CompSalesStat> implements CompSalesStatDao{

}
