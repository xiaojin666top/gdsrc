package com.beawan.survey.custInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.custInfo.dao.CompShiXinItemsDao;
import com.beawan.survey.custInfo.bean.CompShiXinItems;
import org.springframework.stereotype.Repository;
import com.beawan.survey.custInfo.bean.CompShiXinItems;

/**
 * @author yzj
 */
@Repository("compShiXinItemsDao")
public class CompShiXinItemsDaoImpl extends BaseDaoImpl<CompShiXinItems> implements CompShiXinItemsDao{

}
