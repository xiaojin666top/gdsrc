package com.beawan.survey.custInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.custInfo.bean.CompRunProduct;
import com.beawan.survey.custInfo.dao.CompProductDao;
import org.springframework.stereotype.Repository;

/**
 * @author zxh
 * @date 2020/7/9 17:38
 */
@Repository("compProductDao")
public class compProductDaoImpl extends BaseDaoImpl<CompRunProduct> implements CompProductDao {
}
