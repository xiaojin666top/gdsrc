package com.beawan.survey.custInfo.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.custInfo.bean.CompFinancing;
import com.beawan.survey.custInfo.dao.ICompFinancingDAO;

@Repository("compFinancingDAO")
public class CompFinancingDAOImpl extends CommDAOImpl<CompFinancing> implements
		ICompFinancingDAO {

	@Override
	public CompFinancing queryByTaskIdAndCustNo(Long taskId, String customerNo)
			throws DataAccessException {
		
		String hql = "taskId=:taskId and customerNo=:customerNo";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("taskId", taskId);
		params.put("customerNo", customerNo);
		
		return selectSingle(hql, params);
	}

}
