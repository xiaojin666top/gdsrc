package com.beawan.survey.custInfo.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.custInfo.bean.CompBaseItemChange;
import com.beawan.survey.custInfo.dao.ICompBaseItemChangeDAO;

@Service("compBaseItemChangeDAO")
public class CompBaseItemChangeDAOImpl extends CommDAOImpl<CompBaseItemChange>
		implements ICompBaseItemChangeDAO {

	@Override
	public List<CompBaseItemChange> queryByCustNo(String customerNo){
		Map<String, Object> params = new HashMap<>();
		params.put("customerNo", customerNo);
		return this.selectByProperty(CompBaseItemChange.class, params);
	}

}
