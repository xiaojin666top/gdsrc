package com.beawan.survey.custInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.custInfo.dao.CompEquipmentDao;
import com.beawan.survey.custInfo.entity.CompEquipment;
import org.springframework.stereotype.Repository;
import com.beawan.survey.custInfo.entity.CompEquipment;

/**
 * @author yzj
 */
@Repository("compEquipmentDao")
public class CompEquipmentDaoImpl extends BaseDaoImpl<CompEquipment> implements CompEquipmentDao{

}
