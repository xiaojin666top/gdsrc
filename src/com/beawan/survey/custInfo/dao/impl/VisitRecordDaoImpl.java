package com.beawan.survey.custInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.custInfo.dao.VisitRecordDao;
import com.beawan.survey.custInfo.entity.VisitRecord;
import org.springframework.stereotype.Repository;
import com.beawan.survey.custInfo.entity.VisitRecord;

/**
 * @author yzj
 */
@Repository("visitRecordDao")
public class VisitRecordDaoImpl extends BaseDaoImpl<VisitRecord> implements VisitRecordDao{

}
