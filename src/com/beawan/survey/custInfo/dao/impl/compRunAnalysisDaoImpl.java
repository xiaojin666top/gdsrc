package com.beawan.survey.custInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.custInfo.dao.CompRunAnalysisDao;
import com.beawan.survey.custInfo.bean.CompRunAnalysis;
import org.springframework.stereotype.Repository;

/**
 * @author yzj
 */
@Repository("compRunAnalysisDao")
public class compRunAnalysisDaoImpl extends BaseDaoImpl<CompRunAnalysis> implements CompRunAnalysisDao {

}
