package com.beawan.survey.custInfo.dao.impl;

import com.beawan.common.Constants;
import com.beawan.core.BaseDaoImpl;
import com.beawan.qcc.dto.CaseRoleDto;
import com.beawan.survey.custInfo.dao.CompJudgmentDao;
import com.beawan.survey.custInfo.dto.CompJudgStatDto;
import com.beawan.survey.custInfo.dto.CompJudgmentDto;
import com.beawan.survey.custInfo.entity.CompJudgment;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.platform.util.DateUtil;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;
import com.beawan.survey.custInfo.entity.CompJudgment;

/**
 * @author yzj
 */
@Repository("compJudgmentDao")
public class CompJudgmentDaoImpl extends BaseDaoImpl<CompJudgment> implements CompJudgmentDao{

	@Override
	public List<CompJudgmentDto> getNewestJudgeList(String loanTime, String startDate, String custNo) throws Exception {
		
		StringBuilder sql = new StringBuilder();
		Map<String, Object> params = new HashMap<>();
		sql.append("select a.ID id,a.CASE_NAME caseName,a.CASE_REASON caseReason,")
			.append(" a.CASE_NO caseNo,a.CASE_ROLE caseRole,a.AMOUNT amount,")
			.append(" (case when a.CASE_TYPE='ms' then '民事'")
			.append(" when a.CASE_TYPE='xs' then '刑事'")
			.append(" when a.CASE_TYPE='xz' then '行政'")
			.append(" when a.CASE_TYPE='zscq' then '知识产权'")
			.append(" when a.CASE_TYPE='pc' then '赔偿'")
			.append(" when a.CASE_TYPE='zx' then '执行' end) caseType,")
			.append(" a.UPDATE_DATE updateDate")
			.append(" from CM_JUDGMENT a where a.CUSTOMER_NO= '"+custNo+"' and a.IS_DEFENDANT="+"true")//
			.append(" and a.UPDATE_DATE >= "+startDate+" and a.UPDATE_DATE <= "+loanTime)
			.append(" order by a.UPDATE_DATE desc ")
			.append(" limit 0,3");
			//.append(" fetch first 3 rows only");
		
		//params.put("custNo", custNo);
		//params.put("defendant", "true");
		//params.put("startDate", startDate);
		//params.put("loanTime", loanTime);
		
		 List<CompJudgmentDto> items = findCustListBySql(CompJudgmentDto.class, sql.toString(), params);
		
		for(CompJudgmentDto dto : items){
			String caseRole = dto.getCaseRole();
			List<CaseRoleDto> roleList = new Gson().fromJson(caseRole,
					new TypeToken<List<CaseRoleDto>>() {}.getType());
			dto.setRoleDto(roleList);
		}
		return items;
	}

	@Override
	public List<CompJudgStatDto> getJudgStatList(String startDate, String endDate, String custNo,
			boolean isDefendant, boolean statCaseType, boolean statCaseReason) throws Exception {
		StringBuilder sql = new StringBuilder();
		Map<String, Object> params = new HashMap<>();
		params.put("startDate", startDate);
		params.put("endDate", endDate);
		params.put("custNo", custNo);
		//直接统计总数  默认
		StringBuilder count = new StringBuilder("count(1) count");
		String group = "";
		if(statCaseType){
			count.append(", (case when a.CASE_TYPE='ms' then '民事'")
				.append(" when a.CASE_TYPE='xs' then '刑事'")
				.append(" when a.CASE_TYPE='xz' then '行政'")
				.append(" when a.CASE_TYPE='zscq' then '知识产权'")
				.append(" when a.CASE_TYPE='pc' then '赔偿'")
				.append(" when a.CASE_TYPE='zx' then '执行' end) caseType ");
			group = "group by CASE_TYPE";
		}else if(statCaseReason){
			count.append(", CASE_REASON caseReason)");
			group = "group by CASE_REASON";
		}
		sql.append("select ")
			.append(count)
			.append(" from CM_JUDGMENT a where a.CUSTOMER_NO=:custNo ")
			.append(" and a.SUBMIT_DATE >=:startDate and a.SUBMIT_DATE <=:endDate ");
		if(isDefendant){
			sql.append(" and a.IS_DEFENDANT=:defendant ");
			params.put("defendant", "true");
		}
		sql.append(group);
		return findCustListBySql(CompJudgStatDto.class, sql, params);
	}
}
