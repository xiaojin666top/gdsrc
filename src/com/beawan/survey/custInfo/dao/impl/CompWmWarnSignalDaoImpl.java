package com.beawan.survey.custInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.custInfo.dao.CompWmWarnSignalDao;
import com.beawan.survey.custInfo.bean.CompWmWarnSignal;
import org.springframework.stereotype.Repository;
import com.beawan.survey.custInfo.bean.CompWmWarnSignal;

/**
 * @author yzj
 */
@Repository("compWmWarnSignalDao")
public class CompWmWarnSignalDaoImpl extends BaseDaoImpl<CompWmWarnSignal> implements CompWmWarnSignalDao{

}
