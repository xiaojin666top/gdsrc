package com.beawan.survey.custInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.custInfo.dao.CompCreditDao;
import com.beawan.survey.custInfo.entity.CompCredit;
import org.springframework.stereotype.Repository;
import com.beawan.survey.custInfo.entity.CompCredit;

/**
 * @author yzj
 */
@Repository("compCreditDao")
public class CompCreditDaoImpl extends BaseDaoImpl<CompCredit> implements CompCreditDao{

}
