package com.beawan.survey.custInfo.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.custInfo.bean.CompBaseEquity;
import com.beawan.survey.custInfo.bean.CompChangeRecord;
import com.beawan.survey.custInfo.dao.ICompChangeRecordDAO;
import com.beawan.survey.custInfo.dto.CompChangeRecordDto;

@Repository("compChangeRecordDAO")
public class CompChangeRecordDAOImpl extends BaseDaoImpl<CompChangeRecord> implements ICompChangeRecordDAO {

	@Override
	public List<CompChangeRecordDto> groupByItemWhereCustNo(String custNo) {
		Map<String,Object> params = new HashMap<>();
		StringBuilder sql = new StringBuilder();
//		  t where record_id=394 and version_code=0  
		sql.append("select change_item changeItem,count(*) numCount from DG_CM_CHANGE_RECORD")
			.append(" where 1=1");
		if (custNo != null || !"".equals(custNo)) {
			sql.append(" and CUSTOMER_NO= :custNo");
			params.put("custNo", custNo);
		}
		sql.append(" group by change_item");
		return findCustListBySql(CompChangeRecordDto.class, sql, params);
	}




}
