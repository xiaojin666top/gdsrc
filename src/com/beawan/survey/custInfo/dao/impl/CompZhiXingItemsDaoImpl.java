package com.beawan.survey.custInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.custInfo.dao.CompZhiXingItemsDao;
import com.beawan.survey.custInfo.bean.CompZhiXingItems;
import org.springframework.stereotype.Repository;
import com.beawan.survey.custInfo.bean.CompZhiXingItems;

/**
 * @author yzj
 */
@Repository("compZhiXingItemsDao")
public class CompZhiXingItemsDaoImpl extends BaseDaoImpl<CompZhiXingItems> implements CompZhiXingItemsDao{

}
