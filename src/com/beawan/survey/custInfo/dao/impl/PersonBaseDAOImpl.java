package com.beawan.survey.custInfo.dao.impl;

import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.custInfo.bean.PersonBase;
import com.beawan.survey.custInfo.dao.IPersonBaseDAO;

@Service("personBaseDAO")
public class PersonBaseDAOImpl extends CommDAOImpl<PersonBase>
		implements IPersonBaseDAO {

	@Override
	public PersonBase queryByCustNo(String customerNo){
		return selectByPrimaryKey(customerNo);
	}

}
