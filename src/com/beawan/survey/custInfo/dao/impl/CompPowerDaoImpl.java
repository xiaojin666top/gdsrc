package com.beawan.survey.custInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.custInfo.dao.CompPowerDao;
import org.springframework.stereotype.Repository;
import com.beawan.survey.custInfo.bean.CompPower;

/**
 * @author yzj
 */
@Repository("compPowerDao")
public class CompPowerDaoImpl extends BaseDaoImpl<CompPower> implements CompPowerDao {

}
