package com.beawan.survey.custInfo.dao.impl;

import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.custInfo.bean.CompBaseDesc;
import com.beawan.survey.custInfo.dao.ICompBaseDescDAO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("compBaseDescDAO")
public class CompBaseDescDAOImpl extends CommDAOImpl<CompBaseDesc>
		implements ICompBaseDescDAO {

	@Override
	public CompBaseDesc queryByCustNo(String customerNo) {
		return this.selectSingleByProperty(CompBaseDesc.class, "customerNo", customerNo);
	}

}
