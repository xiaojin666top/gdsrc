package com.beawan.survey.custInfo.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.beawan.common.Constants;
import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.custInfo.bean.CompFinancingBank;
import com.beawan.survey.custInfo.dao.ICompFinancingBankDAO;

@Repository("compFinancingBankDAO")
public class CompFinancingBankDAOImpl extends CommDAOImpl<CompFinancingBank>
		implements ICompFinancingBankDAO {

	@Override
	public List<CompFinancingBank> queryMBankFinance(long taskId, String customerNo) {
		
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		params.put("customerNo", customerNo);
		params.put("financialOrg", "RGRCB");
		return this.selectByProperty(params);
	}

	@Override
	public List<CompFinancingBank> queryOtherFinaBank(Long taskId, String customerNo) {
		StringBuilder sql = new StringBuilder("select id as id, CREATE_TIME as createTime,")
				.append("BUSINESS_TYPE as businessType, CREDIT_AMT as creditAmt,")
				.append("CREDIT_BAL as creditBal, CREDIT_RATE as creditRate, CREDIT_TERM as creditTerm,")
				.append("CUSTOMER_NO as customerNo, END_DATE as endDate, FINANCIAL_ORG as financialOrg,")
				.append("FIVE_CLASS as fiveClass, GRAUATEE_TYPE as guaranteeType, LOAN_PROJECT as loanProject,")
				.append("PAYMENT_TYPE as PAYMENT18_49_, LINKED_PARTY as LINKED_19_49_, REMARKS as REMARKS20_49_, ")
				.append("REPAYMENT_TYPE as paymentType, START_DATE as startDate, TASK_ID as taskId, ")
				.append("TYPE as type, USE_DESC as useDesc, USED_AMT as usedAmt ")
				.append("from GDTCESYS.CM_FINANCING_BANK compfinanc0_ where FINANCIAL_ORG!=:finOrg ")
				.append(" and TASK_ID=:taskId and CUSTOMER_NO=:customerNo and STATUS=:status");
		Map<String, Object> params = new HashMap<>();
		params.put("finOrg", "RGRCB");
		params.put("taskId", taskId);
		params.put("customerNo", customerNo);
		params.put("status", Constants.NORMAL);
		return findCustListBySql(CompFinancingBank.class, sql, params);
	}

}
