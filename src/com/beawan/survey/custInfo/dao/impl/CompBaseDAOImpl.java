package com.beawan.survey.custInfo.dao.impl;

import org.springframework.stereotype.Repository;
import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.custInfo.bean.CompBase;
import com.beawan.survey.custInfo.dao.ICompBaseDAO;

@Repository("compBaseDAO")
public class CompBaseDAOImpl extends CommDAOImpl<CompBase> implements ICompBaseDAO{
	
	@Override
	public CompBase queryByCustNo(String customerNo) throws Exception {
		return selectSingleByProperty("customerNo", customerNo);
	}

	@Override
	public CompBase queryByCustomerName(String customerName) {
		return this.selectSingleByProperty("customerName", customerName);
	}
}
