package com.beawan.survey.custInfo.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.custInfo.bean.CompFinanceTax;
import com.beawan.survey.custInfo.dao.ICompFinanceTaxDAO;

@Repository("compFinanceTaxDAO")
public class CompFinanceTaxDAOImpl extends CommDAOImpl<CompFinanceTax>
		implements ICompFinanceTaxDAO {

	@Override
	public List<CompFinanceTax> queryByTaskIdAndCustNo(long taskId,
			String custNo) throws DataAccessException {
		
		String query = "taskId=:taskId and customerNo=:customerNo";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("taskId", taskId);
		params.put("customerNo", custNo);
		
		return select( query, params);
	}

}
