package com.beawan.survey.custInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.custInfo.dao.CompRunSupplyDao;
import com.beawan.survey.custInfo.bean.CompRunSupply;
import org.springframework.stereotype.Repository;
import com.beawan.survey.custInfo.bean.CompRunSupply;

/**
 * @author yzj
 */
@Repository("compRunSupplyDao")
public class CompRunSupplyDaoImpl extends BaseDaoImpl<CompRunSupply> implements CompRunSupplyDao{

}
