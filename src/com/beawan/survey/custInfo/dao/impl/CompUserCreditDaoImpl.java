package com.beawan.survey.custInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.custInfo.dao.CompUserCreditDao;
import com.beawan.survey.custInfo.entity.CompUserCredit;
import org.springframework.stereotype.Repository;
import com.beawan.survey.custInfo.entity.CompUserCredit;

/**
 * @author yzj
 */
@Repository("compUserCreditDao")
public class CompUserCreditDaoImpl extends BaseDaoImpl<CompUserCredit> implements CompUserCreditDao{

}
