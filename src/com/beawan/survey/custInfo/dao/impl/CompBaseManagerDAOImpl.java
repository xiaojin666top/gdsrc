package com.beawan.survey.custInfo.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.custInfo.bean.CompBaseManager;
import com.beawan.survey.custInfo.dao.ICompBaseManagerDAO;

@Service("compBaseManagerDAO")
public class CompBaseManagerDAOImpl extends CommDAOImpl<CompBaseManager>
		implements ICompBaseManagerDAO {

	@Override
	public List<CompBaseManager> queryByCustNo(String customerNo){
		Map<String, Object> params = new HashMap<>();
		params.put("cmCusNo", customerNo);
		return this.selectByProperty(CompBaseManager.class, params);
	}

}
