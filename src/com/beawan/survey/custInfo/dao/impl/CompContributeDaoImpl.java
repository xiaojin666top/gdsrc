package com.beawan.survey.custInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.custInfo.dao.CompContributeDao;
import com.beawan.survey.custInfo.entity.CompContribute;
import org.springframework.stereotype.Repository;
import com.beawan.survey.custInfo.entity.CompContribute;

/**
 * @author yzj
 */
@Repository("compContributeDao")
public class CompContributeDaoImpl extends BaseDaoImpl<CompContribute> implements CompContributeDao{

}
