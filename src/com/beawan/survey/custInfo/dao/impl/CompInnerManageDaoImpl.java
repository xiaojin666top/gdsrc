package com.beawan.survey.custInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.custInfo.dao.CompInnerManageDao;
import com.beawan.survey.custInfo.entity.CompInnerManage;
import org.springframework.stereotype.Repository;
import com.beawan.survey.custInfo.entity.CompInnerManage;

/**
 * @author yzj
 */
@Repository("compInnerManageDao")
public class CompInnerManageDaoImpl extends BaseDaoImpl<CompInnerManage> implements CompInnerManageDao{

}
