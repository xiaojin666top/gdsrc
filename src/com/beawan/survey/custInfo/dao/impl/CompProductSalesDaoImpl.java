package com.beawan.survey.custInfo.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.survey.custInfo.dao.CompProductSalesDao;
import com.beawan.survey.custInfo.bean.CompProductSales;
import org.springframework.stereotype.Repository;
import com.beawan.survey.custInfo.bean.CompProductSales;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yzj
 */
@Repository("compProductSalesDao")
public class CompProductSalesDaoImpl extends BaseDaoImpl<CompProductSales> implements CompProductSalesDao{

    @Override
    public List<CompProductSales> getBySerNoAndYear(String serNo, int year) {
        String query = "SER_NO = :serNo AND PERIOD LIKE :year";
        Map<String, Object> params = new HashMap<>();
        params.put("serNo", serNo);
        params.put("year", "%" + year + "%");
        return this.select(query, params);
    }
}
