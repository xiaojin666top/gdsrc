package com.beawan.survey.custInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.bean.CompFinanceTaxSale;

/**
 * @author yzj
 */
public interface CompFinanceTaxSaleDao extends BaseDao<CompFinanceTaxSale> {
}
