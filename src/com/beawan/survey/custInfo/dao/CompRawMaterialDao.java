package com.beawan.survey.custInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.entity.CompRawMaterial;

import java.util.List;

/**
 * @author yzj
 */
public interface CompRawMaterialDao extends BaseDao<CompRawMaterial> {

    List<CompRawMaterial> getBySerNoAndYear(String serNo, int year);
}
