package com.beawan.survey.custInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.entity.CompCredit;

/**
 * @author yzj
 */
public interface CompCreditDao extends BaseDao<CompCredit> {
}
