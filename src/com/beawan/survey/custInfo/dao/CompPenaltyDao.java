package com.beawan.survey.custInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.bean.CompPenalty;

/**
 * @author yzj
 */
public interface CompPenaltyDao extends BaseDao<CompPenalty> {
}
