package com.beawan.survey.custInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.entity.FinanceWarn;

/**
 * @author yzj
 */
public interface FinanceWarnDao extends BaseDao<FinanceWarn> {
}
