package com.beawan.survey.custInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.bean.CompShiXinItems;

/**
 * @author yzj
 */
public interface CompShiXinItemsDao extends BaseDao<CompShiXinItems> {
}
