package com.beawan.survey.custInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.bean.CompWmWarnSignal;

/**
 * @author yzj
 */
public interface CompWmWarnSignalDao extends BaseDao<CompWmWarnSignal> {
}
