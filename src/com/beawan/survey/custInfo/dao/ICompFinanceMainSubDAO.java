package com.beawan.survey.custInfo.dao;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.custInfo.bean.CompFinanceMainSub;

/**
 * @Description 企业主要财务科目分析说明录持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface ICompFinanceMainSubDAO  extends ICommDAO<CompFinanceMainSub> {

	/**
	 * @Description (根据客户号和任务号查询企业主要财务科目分析说明)
	 * @param custNo 客户号
	 * @return
	 */
	public CompFinanceMainSub queryByTaskIdAndCustNo(long taskId,
			String custNo) throws DataAccessException;
}
