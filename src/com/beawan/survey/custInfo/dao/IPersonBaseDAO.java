package com.beawan.survey.custInfo.dao;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.custInfo.bean.PersonBase;

/**
 * @Description 个人基本信息持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface IPersonBaseDAO  extends ICommDAO<PersonBase> {

	/**
	 * @Description (根据客户号查询个人基本信息)
	 * @param customerNo 客户号
	 * @return
	 */
	public PersonBase queryByCustNo(String customerNo);
}
