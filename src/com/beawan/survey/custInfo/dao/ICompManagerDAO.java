package com.beawan.survey.custInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.bean.CompBaseManager;

/**
 * @author zxh
 * @date 2020/7/2 20:48
 */
public interface ICompManagerDAO extends BaseDao<CompBaseManager> {
}
