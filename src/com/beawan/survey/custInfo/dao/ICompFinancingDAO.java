package com.beawan.survey.custInfo.dao;


import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.custInfo.bean.CompFinancing;

/**
 * @Description 企业融资信息持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface ICompFinancingDAO extends ICommDAO<CompFinancing> {
	
	/**
	 * @Description 根据客户号和任务号查找企业融资信息
	 * @param customerNo
	 * @return
	 * @throws DataAccessException
	 */
	public CompFinancing queryByTaskIdAndCustNo(Long taskId,String  customerNo) throws DataAccessException;
	
}
