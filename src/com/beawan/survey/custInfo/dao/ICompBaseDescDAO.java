package com.beawan.survey.custInfo.dao;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.custInfo.bean.CompBaseDesc;

/**
 * @Description 企业相关描述信息持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface ICompBaseDescDAO  extends ICommDAO<CompBaseDesc> {

	/**
	 * @Description (根据客户号查询企业相关描述信息)
	 * @param customerNo 客户号
	 * @return
	 * @throws  
	 */
	public CompBaseDesc queryByCustNo(String customerNo);
}
