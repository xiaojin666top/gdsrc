package com.beawan.survey.custInfo.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.custInfo.bean.CompConnetCm;

public interface ICompConnectDAO extends ICommDAO<CompConnetCm> {

	/**
	 * @Description (通过客户号查询关联企业基本信息)
	 * @param customerNo
	 * @return
	 * @throws Exception
	 */
	public List<CompConnetCm> queryByCustNo(String customerNo);
	
}
