package com.beawan.survey.custInfo.dao;

import com.beawan.core.BaseDao;
import com.beawan.survey.custInfo.bean.CompZhiXingItems;

/**
 * @author yzj
 */
public interface CompZhiXingItemsDao extends BaseDao<CompZhiXingItems> {
}
