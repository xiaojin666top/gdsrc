package com.beawan.survey.custInfo.dao;

import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;

import com.beawan.survey.custInfo.bean.CompRunConst;
import com.beawan.survey.custInfo.bean.CompRunConstSPDetail;
import com.beawan.survey.custInfo.bean.CompRunConstSPStruc;
import com.beawan.survey.custInfo.bean.CompRunCustomer;
import com.beawan.survey.custInfo.bean.CompRunEsta;
import com.beawan.survey.custInfo.bean.CompRunEstaDevedProj;
import com.beawan.survey.custInfo.bean.CompRunEstaDevingProj;
import com.beawan.survey.custInfo.bean.CompRunEstaLand;
import com.beawan.survey.custInfo.bean.CompRunFixed;
import com.beawan.survey.custInfo.bean.CompRunGeneral;
import com.beawan.survey.custInfo.bean.CompRunPower;
import com.beawan.survey.custInfo.bean.CompRun;
import com.beawan.survey.custInfo.bean.CompRunArea;
import com.beawan.survey.custInfo.bean.CompRunAreaCf;
import com.beawan.survey.custInfo.bean.CompRunAreaTd;
import com.beawan.survey.custInfo.bean.CompRunPrdSale;
import com.beawan.survey.custInfo.bean.CompRunProduce;
import com.beawan.survey.custInfo.bean.CompRunSupply;

public interface ICompRunDAO {
	
	public <T> T saveEntity(T entity) throws DataAccessException;
	
	public <T> void deleteEntity(T entity) throws DataAccessException;
	
	public <T> T queryById(Class<T> cls, Object id) throws DataAccessException;
	
	public <T> List<T> query(Class<T> cls, String query, Map<String, Object> args) throws DataAccessException;

	/**
	 * @Description (根据任务号查询客户经营状况)
	 * @param taskId 任务号
	 * @param customerNo 客户号
	 * @return
	 */
	public CompRun queryCompRunByTIdAndNo(Long taskId, String customerNo) throws DataAccessException;

	/**
	 * @Description (保存企业经营状况信息)
	 * @param compRun
	 *            企业经营状况实体
	 */
	public CompRun saveCompRun(CompRun compRun) throws DataAccessException;

	/**
	 * @Description (删除企业经营状况信息)
	 * @param compRun
	 *            企业经营状况实体
	 */
	public void deleteCompRun(CompRun compRun) throws DataAccessException;

	/**
	 * @Description (根据客户号查询企业生产情况)
	 * @param taskId
	 *            任务号
	 * @param custNo
	 *            客户号
	 * @return
	 */
	public CompRunProduce queryRunProduceByTIdAndNo(Long taskId, String custNo)
			throws DataAccessException;

	/**
	 * @Description (保存企业生产情况信息)
	 * @param compRunProduce
	 *            企业生产情况实体
	 */
	public CompRunProduce saveRunProduce(CompRunProduce compRunProduce)
			throws DataAccessException;

	/**
	 * @Description (删除企业生产情况信息)
	 * @param compRunProduce
	 *            企业生产情况信息实体
	 */
	public void deleteRunProduce(CompRunProduce compRunProduce)
			throws DataAccessException;

	/**
	 * @Description (根据任务编号查询水电纳税信息)
	 * @param taskId
	 *            任务编号
	 * @return
	 */
	public List<CompRunPower> queryRunPowerByTIdAndNo(Long taskId, String custNo)
			throws DataAccessException;

	/**
	 * @Description (保存水电纳税信息)
	 * @param power
	 *            水电纳税实体
	 */
	public CompRunPower saveRunPower(CompRunPower power) throws DataAccessException;

	/**
	 * @Description (删除水电纳税记录)
	 * @param power
	 *            水电纳税实体
	 */
	public void deleteRunPower(CompRunPower power) throws DataAccessException;

	/**
	 * @Description (根据任务编号查询经营场所信息)
	 * @param customerNo 客户编号
	 * @param taskId 任务编号
	 * @return
	 */
	public List<CompRunArea> queryRunAreaByTIdAndNo(Long taskId, String customerNo)
			throws DataAccessException;

	/**
	 * @Description (根据任务编号查询经营场所(土地)信息)
	 * @param customerNo 客户编号
	 * @param taskId 任务编号
	 * @return
	 */
	public List<CompRunAreaTd> queryRunAreaLandByTIdAndNo(Long taskId, String customerNo)
			throws DataAccessException;
	
	/**
	 * @Description (根据任务编号查询经营场所(厂房)信息)
	 * @param customerNo 客户编号
	 * @param taskId 任务编号
	 * @return
	 */
	public List<CompRunAreaCf> queryRunAreaPlantByTIdAndNo(Long taskId, String customerNo)
			throws DataAccessException;
	
	/**
	 * @Description (保存企业经营场所信息)
	 * @param runArea
	 *            经营场所实体
	 */
	public CompRunArea saveRunArea(CompRunArea runArea) throws DataAccessException;

	/**
	 * @Description (保存企业经营场所（土地）信息)
	 * @param runArea
	 *            经营场所实体
	 */
	public CompRunAreaTd saveRunAreaLand(CompRunAreaTd runArea) throws DataAccessException;
	
	/**
	 * @Description (保存企业经营场所（厂房）信息)
	 * @param runArea
	 *            经营场所实体
	 */
	public CompRunAreaCf saveRunAreaPlant(CompRunAreaCf runArea) throws DataAccessException;
	
	/**
	 * @Description (删除经营场所记录)
	 * @param runArea
	 *            经营场所实体
	 */
	public void deleteRunArea(CompRunArea runArea) throws DataAccessException;

	/**
	 * @Description (根据任务编号查询上游供应商信息)
	 * @param taskId 任务编号
	 * @return
	 */
	public List<CompRunSupply> queryRunSupplyByTIdAndNo(Long taskId, String customerNo)
			throws DataAccessException;

	/**
	 * @Description (保存上游供应商信息)
	 * @param supply
	 *            上游供应商实体
	 */
	public CompRunSupply saveRunSupply(CompRunSupply supply) throws DataAccessException;

	/**
	 * @Description (删除上游供应商记录)
	 * @param supply
	 *            供应商实体
	 */
	public void deleteRunSupply(CompRunSupply supply)
			throws DataAccessException;

	/**
	 * @Description (根据任务编号查询下游客户信息)
	 * @param taskId
	 *            任务编号
	 * @return
	 */
	public List<CompRunCustomer> queryRunCustomerByTIdAndNo(Long taskId, String custNo)
			throws DataAccessException;

	/**
	 * @Description (保存下游客户信息)
	 * @param customer
	 *            下游客户实体
	 */
	public CompRunCustomer saveRunCustomer(CompRunCustomer customer)
			throws DataAccessException;

	/**
	 * @Description (删除下游客户记录)
	 * @param customer
	 *            下游客户实体
	 */
	public void deleteRunCustomer(CompRunCustomer customer)
			throws DataAccessException;

	/**
	 * @Description (根据任务编号查询产品销售信息)
	 * @param taskId
	 *            任务编号
	 * @return
	 */
	public List<CompRunPrdSale> queryRunPrdSaleByTIdAndNo(Long taskId, String custNo)
			throws DataAccessException;

	/**
	 * @Description (保存产品销售信息)
	 * @param compRunPrdSale
	 *            产品销售信息实体
	 */
	public CompRunPrdSale saveRunPrdSale(CompRunPrdSale compRunPrdSale)
			throws DataAccessException;

	/**
	 * @Description (删除产品销售信息)
	 * @param compRunPrdSale
	 *            产品销售信息实体
	 */
	public void deleteRunPrdSale(CompRunPrdSale compRunPrdSale)
			throws DataAccessException;

	/**
	 * @Description (根据任务号查询客户经营状况，适用于建筑业流动资金贷款)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public CompRunConst queryRunConstByTIdAndNo(Long taskId, String custNo)
			throws DataAccessException;

	/**
	 * @Description (保存企业经营状况信息，适用于建筑业流动资金贷款)
	 * @param compRunConst
	 *            企业经营状况实体
	 */
	public CompRunConst saveRunConst(CompRunConst compRunConst)
			throws DataAccessException;

	/**
	 * @Description (删除企业经营状况信息，适用于建筑业流动资金贷款)
	 * @param compRunConst
	 *            企业经营状况实体
	 */
	public void deleteRunConst(CompRunConst compRunConst)
			throws DataAccessException;

	/**
	 * @Description (根据任务号查询企业在手已签施工工程明细，适用于建筑业流动资金贷款)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public List<CompRunConstSPDetail> queryRunConstSPDetailByTIdAndNo(Long taskId, String custNo)
			throws DataAccessException;

	/**
	 * @Description (保存企业在手已签施工工程明细，适用于建筑业流动资金贷款)
	 * @param compRunConstSPDetail
	 *            企业在手已签施工工程明细实体
	 */
	public CompRunConstSPDetail saveRunConstSPDetail(CompRunConstSPDetail compRunConstSPDetail)
			throws DataAccessException;

	/**
	 * @Description (删除企业在手已签施工工程明细，适用于建筑业流动资金贷款)
	 * @param compRunConstSPDetail
	 *            企业在手已签施工工程明细实体
	 */
	public void deleteRunConstSPDetail(CompRunConstSPDetail compRunConstSPDetail)
			throws DataAccessException;

	/**
	 * @Description (根据任务号查询企业在手已签施工工程结构，适用于建筑业流动资金贷款)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public List<CompRunConstSPStruc> queryRunConstSPStrucByTIdAndNo(Long taskId, String custNo)
			throws DataAccessException;

	/**
	 * @Description (保存企业在手已签施工工程结构，适用于建筑业流动资金贷款)
	 * @param compRunConstSPStruc
	 *            企业在手已签施工工程结构实体
	 */
	public CompRunConstSPStruc saveRunConstSPStruc(CompRunConstSPStruc compRunConstSPStruc)
			throws DataAccessException;

	/**
	 * @Description (删除企业在手已签施工工程结构，适用于建筑业流动资金贷款)
	 * @param compRunConstSPStruc
	 *            企业在手已签施工工程结构实体
	 */
	public void deleteRunConstSPStruc(CompRunConstSPStruc compRunConstSPStruc)
			throws DataAccessException;

	/**
	 * @Description (根据任务号查询客户经营状况，适用于固定资产贷款)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public CompRunFixed queryRunFixedByTIdAndNo(Long taskId, String custNo)
			throws DataAccessException;

	/**
	 * @Description (保存企业经营状况信息，适用于固定资产贷款)
	 * @param compRunFixed
	 *            企业经营状况实体
	 */
	public CompRunFixed saveRunFixed(CompRunFixed compRunFixed)
			throws DataAccessException;

	/**
	 * @Description (删除企业经营状况信息，适用于固定资产贷款)
	 * @param compRunFixed
	 *            企业经营状况实体
	 */
	public void deleteRunFixed(CompRunFixed compRunFixed)
			throws DataAccessException;

	/**
	 * @Description (根据任务号查询客户经营状况，适用于房地产贷款)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public CompRunEsta queryRunEstaByTIdAndNo(Long taskId, String custNo)
			throws DataAccessException;

	/**
	 * @Description (保存企业经营状况信息，适用于房地产贷款)
	 * @param compRunEsta
	 *            企业经营状况实体
	 */
	public CompRunEsta saveRunEsta(CompRunEsta compRunEsta) throws DataAccessException;

	/**
	 * @Description (删除企业经营状况信息，适用于房地产贷款)
	 * @param compRunEsta
	 *            企业经营状况实体
	 */
	public void deleteRunEsta(CompRunEsta compRunEsta)
			throws DataAccessException;

	/**
	 * @Description (根据任务号查询房地产土地储备情况)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public List<CompRunEstaLand> queryRunEstaLandByTIdAndNo(Long taskId, String custNo)
			throws DataAccessException;

	/**
	 * @Description (保存房地产土地储备情况)
	 * @param compRunEstaLand
	 *            房地产土地储备情况实体
	 */
	public CompRunEstaLand saveRunEstaLand(CompRunEstaLand compRunEstaLand)
			throws DataAccessException;

	/**
	 * @Description (删除房地产土地储备情况)
	 * @param compRunEstaLand
	 *            房地产土地储备情况实体
	 */
	public void deleteRunEstaLand(CompRunEstaLand compRunEstaLand)
			throws DataAccessException;

	/**
	 * @Description (根据任务号查询房地产正在开发项目情况)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public List<CompRunEstaDevingProj> queryRunEstaDevingProjByTIdAndNo(Long taskId,
			String custNo) throws DataAccessException;

	/**
	 * @Description (保存房地产正在开发项目情况)
	 * @param compRunEstaDevingProj
	 *            房地产正在开发项目情况实体
	 */
	public CompRunEstaDevingProj saveRunEstaDevingProj(
			CompRunEstaDevingProj compRunEstaDevingProj)
			throws DataAccessException;

	/**
	 * @Description (删除房地产正在开发项目情况)
	 * @param compRunEstaDevingProj
	 *            房地产正在开发项目情况实体
	 */
	public void deleteRunEstaDevingProj(
			CompRunEstaDevingProj compRunEstaDevingProj)
			throws DataAccessException;

	/**
	 * @Description (根据任务号查询房地产已开发项目情况)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public List<CompRunEstaDevedProj> queryRunEstaDevedProjByTIdAndNo(Long taskId, String custNo)
			throws DataAccessException;

	/**
	 * @Description (保存房地产已开发项目情况)
	 * @param compRunEstaDevedProj
	 *            房地产已开发项目情况实体
	 */
	public CompRunEstaDevedProj saveRunEstaDevedProj(CompRunEstaDevedProj compRunEstaDevedProj)
			throws DataAccessException;

	/**
	 * @Description (删除房地产已开发项目情况)
	 * @param compRunEstaDevedProj
	 *            房地产已开发项目情况实体
	 */
	public void deleteRunEstaDevedProj(
			CompRunEstaDevedProj compRunEstaDevingProj)
			throws DataAccessException;
	
	/**
	 * @Description (根据任务号查询客户经营状况，适用于通用模板)
	 * @param taskId 任务号
	 * @param custNo 客户号
	 * @return
	 */
	public CompRunGeneral queryRunGeneralByTIdAndNo(Long taskId, String custNo)
			throws DataAccessException;

	/**
	 * @Description (保存企业经营状况信息，适用于通用模板)
	 * @param compRunGeneral 企业经营状况实体
	 */
	public CompRunGeneral saveRunGeneral(CompRunGeneral compRunGeneral)
			throws DataAccessException;
	
	/**
	 * @Description (删除企业经营状况信息，适用于通用模板)
	 * @param compRunGeneral 企业经营状况实体
	 */
	public void deleteRunGeneral(CompRunGeneral compRunGeneral)
			throws DataAccessException;
	
}
