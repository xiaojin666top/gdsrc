package com.beawan.survey.custInfo.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * CmFinancingExtGuara entity. @author MyEclipse Persistence Tools
 */

@Entity
@Table(name = "CM_FINANCING_EXT_GUARA", schema = "GDTCESYS")
public class CompFinancingExtGuara extends BaseEntity implements java.io.Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_FINANCING_EXT_GUARA")
	//@SequenceGenerator(name = "CM_FINANCING_EXT_GUARA",allocationSize=1,initialValue=1, sequenceName = "CM_FINANCING_EXT_GUARA_SEQ")
	private Long id;
	@Column(name = "CUSTOMER_NO")
	private String customerNo;
	@Column(name = "GUARA_NAME")
	private String guaranteeName; // 被担保企业
	@Column(name = "GUARA_AMT")
	private Double guaranteeAmt; // 担保金额
	@Column(name = "GUARA_BAL")
	private Double guaranteeBal; // 担保余额
	@Column(name = "FINANCIAL_ORG")
	private String financialOrg; // 融资机构
	@Column(name = "RELATION")
	private String relation; // 关联关系
	@Column(name = "ANTI_GUARA_WAY")
	private String antiGuaranteeWay; // 反担保方式
	@Column(name = "GUARA_REASON")
	private String guaranteeReason; // 担保原因
	@Column(name = "BUSINESS_TYPE")
	private String businessType; // 融资品种
	@Column(name = "MATURITY_DATE")
	private String maturityDate; // 到期日
	@Column(name = "FIVE_CLASS")
	private String fiveClass; // 五级分类
	@Column(name = "REMARKS")
	private String remarks; // 备注
	@Column(name = "TASK_ID")
	private Long taskId;

	// Constructors

	/** default constructor */
	public CompFinancingExtGuara() {
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustomerNo() {
		return this.customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getBusinessType() {
		return this.businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public String getMaturityDate() {
		return this.maturityDate;
	}

	public void setMaturityDate(String maturityDate) {
		this.maturityDate = maturityDate;
	}

	public String getFiveClass() {
		return this.fiveClass;
	}

	public void setFiveClass(String fiveClass) {
		this.fiveClass = fiveClass;
	}

	public String getFinancialOrg() {
		return financialOrg;
	}

	public void setFinancialOrg(String financialOrg) {
		this.financialOrg = financialOrg;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getGuaranteeName() {
		return guaranteeName;
	}

	public void setGuaranteeName(String guaranteeName) {
		this.guaranteeName = guaranteeName;
	}

	public String getGuaranteeReason() {
		return guaranteeReason;
	}

	public void setGuaranteeReason(String guaranteeReason) {
		this.guaranteeReason = guaranteeReason;
	}

	public Double getGuaranteeBal() {
		return guaranteeBal;
	}

	public void setGuaranteeBal(Double guaranteeBal) {
		this.guaranteeBal = guaranteeBal;
	}

	public Double getGuaranteeAmt() {
		return guaranteeAmt;
	}

	public void setGuaranteeAmt(Double guaranteeAmt) {
		this.guaranteeAmt = guaranteeAmt;
	}

	public String getAntiGuaranteeWay() {
		return antiGuaranteeWay;
	}

	public void setAntiGuaranteeWay(String antiGuaranteeWay) {
		this.antiGuaranteeWay = antiGuaranteeWay;
	}
}