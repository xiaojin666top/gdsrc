package com.beawan.survey.custInfo.bean;


import com.beawan.core.BaseEntity;
import com.google.gson.annotations.SerializedName;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 行政处罚
 */
@Entity
@Table(schema = "GDTCESYS", name = "COMP_PENALTY")
public class CompPenalty extends BaseEntity {


    @Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="COMP_PENALTY")
	//@SequenceGenerator(name="COMP_PENALTY",allocationSize=1,initialValue=1, sequenceName="COMP_PENALTY_SEQ")
	private Long id;
    @Column(name = "CUSTOMER_NO")
    private String customerNo;

    @Column(name ="CONTENT")
    private String content;//行政处罚内容
    @Column(name ="PENALTY_TYPE")
    private String penaltyType;//违法行为类型
    @Column(name ="REMARK")
    private String remark;//备注
    @Column(name ="OFFICE_NAME")
    private String officeName;//行政处罚决定机关名称
    @Column(name ="PENALTY_DATE")
    private String penaltyDate;//作出行政处罚决定日期
    @Column(name ="PUBLIC_DATE")
    private String publicDate;//作出行政公示日期
    @Column(name ="DOC_NO")
    private String docNo;//行政处罚决定书文号


    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPenaltyType() {
        return penaltyType;
    }

    public void setPenaltyType(String penaltyType) {
        this.penaltyType = penaltyType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getOfficeName() {
        return officeName;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }

    public String getPenaltyDate() {
        return penaltyDate;
    }

    public void setPenaltyDate(String penaltyDate) {
        this.penaltyDate = penaltyDate;
    }

    public String getPublicDate() {
        return publicDate;
    }

    public void setPublicDate(String publicDate) {
        this.publicDate = publicDate;
    }

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }
}
