package com.beawan.survey.custInfo.bean;

import com.beawan.core.BaseEntity;
import com.beawan.scoreCard.AcConstants;

import javax.persistence.*;

/**
 * @ClassName CompBase
 * @Description 企业客户基本信息
 * @author rain
 * @Date 2018年1月3日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
@Entity
@Table(name = "CM_BASE",schema = "GDTCESYS")
public class CompBase extends BaseEntity implements java.io.Serializable {

	@Id
	@Column(name = "CUSTOMER_NO")
	private String customerNo; //客户号
	@Column(name = "CUSTOMER_NAME")
	private String customerName;//客户名称
	@Column(name = "CONTROL_PERSON")
	private String controlPerson;//实际控制人
	@Column(name = "CONTROL_PERSON_CODE")
	private String controlPersonCode;//实际控制人编号
	@Column(name = "CONTROL_WORKING_YEARS")
	private String controlWorkingYears;//实际控制人从业年限
	@Column(name = "CREDIT_LEVEL")
	private String creditLevel;//本行评估即期信用等级
	@Column(name = "CERT_CODE")
	private String certCode;//证件号码
	@Column(name = "CERT_TYPE")
	private String certType;//证件类型
	@Column(name = "FOUND_DATE")
	private String foundDate;//企业成立日期

	@Column(name = "MAJOR_INDU")
	private String majorIndu;
	@Column(name = "INDUSTRY_POLICY")
	private String indusPolicy;// 行业政策
	@Column(name = "INDUSTRY_CODE")
	private String industryCode;//行业类型编号
	@Column(name = "INDUSTRY_TYPE")
	private String industryType;//四级行业类型
	@Column(name = "LEGAL_PERSON")
	private String legalPerson;//法定代表人
	@Column(name = "LEGAL_PERSON_CODE")
	private String legalPersonCode;//法定代表人编号
	@Column(name = "LEGAL_WORKING_YEARS")
	private String legalWorkingYears;//法定代表人从业年限
	@Column(name = "MAIN_BUSINESS")
	private String mainBusiness;//实际主营业务
	@Column(name = "PC_CURRENCY")
	private String pcCurrency;//实收资本币种
	@Column(name = "REAL_CAPITAL")
	private Double realCapital;//实收资本
	@Column(name = "REGISTE_ADDRESS")
	private String registerAddress;//注册地址
	@Column(name = "REGISTE_CAPITAL")
	private Double registerCapital;//注册资本
	@Column(name = "RC_CURRENCY")
	private String rcCurrency;//注册资本币种
	@Column(name = "REGISTE_TIME")
	private String registeTime;//登记时间
	@Column(name = "RUN_ADDRESS")
	private String runAddress;//经营地址
	@Column(name = "RUN_SCOPE")
	private String runScope;//经营范围
	@Column(name = "CUSTOMER_FULL_NAME")
	private String customerFullName;//客户全称
	@Column(name = "STOCK_CUSTOMER")
	private String stockCustomer;//客户类型 01:新增,02:增量,03:存量
	@Column(name = "CERTIFICATE_CREDIT_CODE")
	private String certificateCreditCode;//机构信用证号码
	@Column(name = "CERTIFICATE_ORGAN_CODE")
	private String certificateOrganCode;//机构代码证号码
	@Column(name = "COMPANY_LICENCE_NUM")
	private String companyLicenceNum;//营业执照号码
	@Column(name = "COMPANY_NATURE")
	private String companyNature;//企业性质
	@Column(name = "COMPANY_TYPE")
	private String companyType;//企业类型
	@Column(name = "COMPANY_SCOPE")
	private String companyScope;//企业规模
	@Column(name = "OPEN_BANK")
	private String openBank;//基本开户行
	@Column(name = "MBANK_ACC_TYPE")
	private String mbankAccType;//本行账户性质
	@Column(name = "IS_MBANK_HOLDER")
	private String isMbankHolder;// 是否本行股东
	@Column(name = "STOCK_NUM")
	private Integer stockNum;// 持股数
	@Column(name = "IS_ANTI_PLEDGE")
	private String isAntiPledge;// 股权是否反质押
	@Column(name = "EMPLOYEE_NUM")
	private Integer employeeNum;//员工人数
	@Column(name = "ARTISAN_RATE")
	private String artisanRate;//技术人员占比
	@Column(name = "CUST_STATUS")
	private String custStatus;//客户状态
	@Column(name = "MANAGE_USER")
	private String manageUser;//管户人
	@Column(name = "MANAGE_ORG")
	private String manageOrg;//管理机构
	@Column(name = "CONTROL_RELATION")
	private String controlRelation;//法人与实际控制人关系
	@Column(name = "OUT_CREDIT_LEVEL")
	private String outCreditLevel;//外部评级
	@Column(name = "OUT_CREDIT_SITUA")
	private String outCreditSitua;//外部评级说明
	@Column(name = "FINANCE_BELONG")
	private String financeBelong;//财务报表所属
	@Column(name = "IS_DRAFT")
	private String isDraft;//是否为草稿信息（非信贷客户）
	@Column(name = "CREDIT_RECORD")
	private String creditRecord;// 征信记录
	@Column(name = "REG_NUMBER")
	private String regNumber;// 工商注册号
	@Column(name = "ORG_NUMBER")
	private String orgNumber;// 组织结构代码
	@Column(name = "APPROVED_TIME")
	private String approvedTime;// 核准日期
	@Column(name = "REG_INSTITUTE")
	private String regInstitute;// 登记机关
	@Column(name = "LOAN_HISTORY")
	private String loanHistory;//在我行是否贷款3年以上
	@Column(name = "REVOLE_CREDIT")
	private String revoleCredit = AcConstants.REVOLE_CREDIT.FALSE;//是否循环授信  默认 否
	@Column(name = "TAX_GRADE ")//纳税等级
	private String taxGrade;
	@Column(name = "EIA_GRADE")//环评信用等级
	private String eiaGrade;
	@Column(name = "IS_DISCHARGE_PERMIT")//是否有排污排污许可证
	private String isDischargePermit;
	
	
	/** default constructor */
	public CompBase() {
	}

	/** minimal constructor */
	public CompBase(String customerNo) {
		this.customerNo = customerNo;
	}

	// Property accessors

	public String getCustomerNo() {
		return this.customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getControlPerson() {
		return this.controlPerson;
	}

	public void setControlPerson(String controlPerson) {
		this.controlPerson = controlPerson;
	}

	public String getControlPersonCode() {
		return this.controlPersonCode;
	}

	public void setControlPersonCode(String controlPersonCode) {
		this.controlPersonCode = controlPersonCode;
	}

	public String getCreditLevel() {
		return this.creditLevel;
	}

	public void setCreditLevel(String creditLevel) {
		this.creditLevel = creditLevel;
	}

	public String getCertCode() {
		return this.certCode;
	}

	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}

	public String getCertType() {
		return this.certType;
	}

	public void setCertType(String certType) {
		this.certType = certType;
	}

	public String getFoundDate() {
		return this.foundDate;
	}

	public void setFoundDate(String foundDate) {
		this.foundDate = foundDate;
	}

	public String getIndustryCode() {
		return this.industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public String getMajorIndu() {
		return majorIndu;
	}

	public void setMajorIndu(String majorIndu) {
		this.majorIndu = majorIndu;
	}

	public String getIndustryType() {
		return this.industryType;
	}

	public void setIndustryType(String industryType) {
		this.industryType = industryType;
	}

	public String getLegalPerson() {
		return this.legalPerson;
	}

	public void setLegalPerson(String legalPerson) {
		this.legalPerson = legalPerson;
	}

	public String getLegalPersonCode() {
		return this.legalPersonCode;
	}

	public void setLegalPersonCode(String legalPersonCode) {
		this.legalPersonCode = legalPersonCode;
	}

	public String getMainBusiness() {
		return this.mainBusiness;
	}

	public void setMainBusiness(String mainBusiness) {
		this.mainBusiness = mainBusiness;
	}

	public Double getRealCapital() {
		return this.realCapital;
	}

	public void setRealCapital(Double realCapital) {
		this.realCapital = realCapital;
	}

	public String getRegisterAddress() {
		return registerAddress;
	}

	public void setRegisterAddress(String registerAddress) {
		this.registerAddress = registerAddress;
	}

	public Double getRegisterCapital() {
		return registerCapital;
	}

	public void setRegisterCapital(Double registerCapital) {
		this.registerCapital = registerCapital;
	}

	public String getPcCurrency() {
		return pcCurrency;
	}

	public void setPcCurrency(String pcCurrency) {
		this.pcCurrency = pcCurrency;
	}

	public String getRcCurrency() {
		return rcCurrency;
	}

	public void setRcCurrency(String rcCurrency) {
		this.rcCurrency = rcCurrency;
	}

	public String getRegisteTime() {
		return this.registeTime;
	}

	public void setRegisteTime(String registeTime) {
		this.registeTime = registeTime;
	}

	public String getRunAddress() {
		return this.runAddress;
	}

	public void setRunAddress(String runAddress) {
		this.runAddress = runAddress;
	}

	public String getRunScope() {
		return this.runScope;
	}

	public void setRunScope(String runScope) {
		this.runScope = runScope;
	}

	public String getCustomerFullName() {
		return this.customerFullName;
	}

	public void setCustomerFullName(String customerFullName) {
		this.customerFullName = customerFullName;
	}

	public String getStockCustomer() {
		return this.stockCustomer;
	}

	public void setStockCustomer(String stockCustomer) {
		this.stockCustomer = stockCustomer;
	}

	public String getCertificateCreditCode() {
		return this.certificateCreditCode;
	}

	public void setCertificateCreditCode(String certificateCreditCode) {
		this.certificateCreditCode = certificateCreditCode;
	}

	public String getCertificateOrganCode() {
		return this.certificateOrganCode;
	}

	public void setCertificateOrganCode(String certificateOrganCode) {
		this.certificateOrganCode = certificateOrganCode;
	}

	public String getCompanyLicenceNum() {
		return this.companyLicenceNum;
	}

	public void setCompanyLicenceNum(String companyLicenceNum) {
		this.companyLicenceNum = companyLicenceNum;
	}

	public String getCompanyNature() {
		return this.companyNature;
	}

	public void setCompanyNature(String companyNature) {
		this.companyNature = companyNature;
	}

	public String getCompanyType() {
		return this.companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}
	
	public String getCompanyScope() {
		return companyScope;
	}

	public void setCompanyScope(String companyScope) {
		this.companyScope = companyScope;
	}

	public String getOpenBank() {
		return this.openBank;
	}

	public void setOpenBank(String openBank) {
		this.openBank = openBank;
	}

	public String getMbankAccType() {
		return this.mbankAccType;
	}

	public void setMbankAccType(String mbankAccType) {
		this.mbankAccType = mbankAccType;
	}

	public String getIsMbankHolder() {
		return this.isMbankHolder;
	}

	public void setIsMbankHolder(String isMbankHolder) {
		this.isMbankHolder = isMbankHolder;
	}

	public Integer getStockNum() {
		return stockNum;
	}

	public void setStockNum(Integer stockNum) {
		this.stockNum = stockNum;
	}

	public String getIsAntiPledge() {
		return isAntiPledge;
	}

	public void setIsAntiPledge(String isAntiPledge) {
		this.isAntiPledge = isAntiPledge;
	}

	public Integer getEmployeeNum() {
		return this.employeeNum;
	}

	public void setEmployeeNum(Integer employeeNum) {
		this.employeeNum = employeeNum;
	}

	public String getArtisanRate() {
		return artisanRate;
	}

	public void setArtisanRate(String artisanRate) {
		this.artisanRate = artisanRate;
	}

	public String getCustStatus() {
		return this.custStatus;
	}

	public void setCustStatus(String custStatus) {
		this.custStatus = custStatus;
	}

	public String getManageUser() {
		return this.manageUser;
	}

	public void setManageUser(String manageUser) {
		this.manageUser = manageUser;
	}

	public String getManageOrg() {
		return this.manageOrg;
	}

	public void setManageOrg(String manageOrg) {
		this.manageOrg = manageOrg;
	}

	public String getControlRelation() {
		return this.controlRelation;
	}

	public void setControlRelation(String controlRelation) {
		this.controlRelation = controlRelation;
	}

	public String getOutCreditLevel() {
		return this.outCreditLevel;
	}

	public void setOutCreditLevel(String outCreditLevel) {
		this.outCreditLevel = outCreditLevel;
	}

	public String getOutCreditSitua() {
		return this.outCreditSitua;
	}

	public void setOutCreditSitua(String outCreditSitua) {
		this.outCreditSitua = outCreditSitua;
	}
	
	public String getFinanceBelong() {
		return financeBelong;
	}

	public void setFinanceBelong(String financeBelong) {
		this.financeBelong = financeBelong;
	}

	public String getIsDraft() {
		return this.isDraft;
	}

	public void setIsDraft(String isDraft) {
		this.isDraft = isDraft;
	}

	public String getRegNumber() {
		return regNumber;
	}

	public void setRegNumber(String regNumber) {
		this.regNumber = regNumber;
	}

	public String getOrgNumber() {
		return orgNumber;
	}

	public void setOrgNumber(String orgNumber) {
		this.orgNumber = orgNumber;
	}

	public String getApprovedTime() {
		return approvedTime;
	}

	public void setApprovedTime(String approvedTime) {
		this.approvedTime = approvedTime;
	}

	public String getRegInstitute() {
		return regInstitute;
	}

	public void setRegInstitute(String regInstitute) {
		this.regInstitute = regInstitute;
	}

	public String getControlWorkingYears() {
		return controlWorkingYears;
	}

	public void setControlWorkingYears(String controlWorkingYears) {
		this.controlWorkingYears = controlWorkingYears;
	}

	public String getLegalWorkingYears() {
		return legalWorkingYears;
	}

	public void setLegalWorkingYears(String legalWorkingYears) {
		this.legalWorkingYears = legalWorkingYears;
	}

	public String getCreditRecord() {
		return creditRecord;
	}

	public void setCreditRecord(String creditRecord) {
		this.creditRecord = creditRecord;
	}

	public String getLoanHistory() {
		return loanHistory;
	}

	public void setLoanHistory(String loanHistory) {
		this.loanHistory = loanHistory;
	}

	public String getRevoleCredit() {
		return revoleCredit;
	}

	public void setRevoleCredit(String revoleCredit) {
		this.revoleCredit = revoleCredit;
	}

	public String getIndusPolicy() {
		return indusPolicy;
	}

	public void setIndusPolicy(String indusPolicy) {
		this.indusPolicy = indusPolicy;
	}
	
	public String getTaxGrade() {
		return taxGrade;
	}

	public void setTaxGrade(String taxGrade) {
		this.taxGrade = taxGrade;
	}

	public String getEiaGrade() {
		return eiaGrade;
	}

	public void setEiaGrade(String eiaGrade) {
		this.eiaGrade = eiaGrade;
	}

	public String getIsDischargePermit() {
		return isDischargePermit;
	}

	public void setIsDischargePermit(String isDischargePermit) {
		this.isDischargePermit = isDischargePermit;
	}


}