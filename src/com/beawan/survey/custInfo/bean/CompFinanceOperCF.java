package com.beawan.survey.custInfo.bean;

/**
 * @Description 企业经营性现金流回笼记录
 */

public class CompFinanceOperCF implements java.io.Serializable {

	private Long id;
	private String customerNo;
	private String bank; // 对公结算账户开户行（本行或他行）
	private String month; // 月份
	private Double amount; // 净流入金额
	private Long taskId; //任务编号

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

}