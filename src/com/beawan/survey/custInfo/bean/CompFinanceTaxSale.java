package com.beawan.survey.custInfo.bean;

import com.beawan.core.BaseEntity;
import javax.persistence.*;
/**
 * 纳税销售情况表
 * CompFinanceTax entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CM_FINANCE_TAX_SALE",schema = "GDTCESYS")
public class CompFinanceTaxSale extends BaseEntity {

    @Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_FINANCE_TAX_SALE_SEQ")
   // @SequenceGenerator(name = "CM_FINANCE_TAX_SALE_SEQ", allocationSize = 1, initialValue = 1, sequenceName = "CM_FINANCE_TAX_SALE_SEQ")
    private Long id;
    @Column(name = "TASK_ID")
    private Long taskId;// 贷前任务号
    @Column(name = "CUSTOMER_NO")
    private String customerNo;// 客户号
    @Column(name = "TAX_DATE")
    private String taxDate; // 纳税申报表时间 --》可自定义时间
    @Column(name = "TAX_VALUE")
    private Double taxValue; // 纳税销售额（元
    @Column(name = "FN_VALUE")
    private Double fnValue; // 对应财报销售收入值（元
    @Column(name = "DEVIATION")
    private String deviation;// 偏差率 带百分号  带正负号

    // Constructors

    /** default constructor */
    public CompFinanceTaxSale() {
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}


	public String getTaxDate() {
		return taxDate;
	}

	public void setTaxDate(String taxDate) {
		this.taxDate = taxDate;
	}

	public Double getTaxValue() {
		return taxValue;
	}

	public void setTaxValue(Double taxValue) {
		this.taxValue = taxValue;
	}

	public Double getFnValue() {
		return fnValue;
	}

	public void setFnValue(Double fnValue) {
		this.fnValue = fnValue;
	}

	public String getDeviation() {
		return deviation;
	}

	public void setDeviation(String deviation) {
		this.deviation = deviation;
	}

    




}