package com.beawan.survey.custInfo.bean;

/**
 * CmRunCustomer entity. @author MyEclipse Persistence Tools
 */

public class CompRunCustomer implements java.io.Serializable {

	// Fields

	private Long id;
	private String custName;
	private String saleProduct;//经销主要产品名称
	private Double lastSaleAmt;//上年销售额
	private Double currSaleAmt;//本年当期销售额
	private String settleWay;//结算方式
	private Integer accountPeriod;//账期
	private Double currAmtRate;//占销售收入比重
	private Double currRecAmt;//即期应收账款余额
	private String customerNo;
	private Long taskId;

	// Constructors

	/** default constructor */
	public CompRunCustomer() {
	}


	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustName() {
		return this.custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getSaleProduct() {
		return this.saleProduct;
	}

	public void setSaleProduct(String saleProduct) {
		this.saleProduct = saleProduct;
	}

	public Double getLastSaleAmt() {
		return this.lastSaleAmt;
	}

	public void setLastSaleAmt(Double lastSaleAmt) {
		this.lastSaleAmt = lastSaleAmt;
	}

	public Double getCurrSaleAmt() {
		return this.currSaleAmt;
	}

	public void setCurrSaleAmt(Double currSaleAmt) {
		this.currSaleAmt = currSaleAmt;
	}

	public String getSettleWay() {
		return this.settleWay;
	}

	public void setSettleWay(String settleWay) {
		this.settleWay = settleWay;
	}

	public Integer getAccountPeriod() {
		return this.accountPeriod;
	}

	public void setAccountPeriod(Integer accountPeriod) {
		this.accountPeriod = accountPeriod;
	}

	public Double getCurrAmtRate() {
		return this.currAmtRate;
	}

	public void setCurrAmtRate(Double currAmtRate) {
		this.currAmtRate = currAmtRate;
	}

	public Double getCurrRecAmt() {
		return this.currRecAmt;
	}

	public void setCurrRecAmt(Double currRecAmt) {
		this.currRecAmt = currRecAmt;
	}

	public String getCustomerNo() {
		return customerNo;
	}


	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}


	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

}