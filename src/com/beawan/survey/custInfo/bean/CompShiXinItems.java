package com.beawan.survey.custInfo.bean;



import com.beawan.core.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 失信
 */
@Entity
@Table(schema = "GDTCESYS", name = "COMP_SHIXIN_ITEMS")
public class CompShiXinItems extends BaseEntity {


    @Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="COMP_SHIXIN_ITEMS")
	//@SequenceGenerator(name="COMP_SHIXIN_ITEMS",allocationSize=1,initialValue=1, sequenceName="COMP_SHIXIN_ITEMS_SEQ")
	private Long id;
    @Column(name = "CUSTOMER_NO")
    private String customerNo;
    
    @Column(name = "LIAN_DATE")
    private String liandate;//立案日期
    @Column(name ="EXECUTE_STATUS")
    private String executestatus;//被执行人的履行情况
    @Column(name ="PUBLIC_DATE")
    private String publicdate;//发布时间
    @Column(name ="ANNO")
    private String anno;//立案文书号
    @Column(name ="NAME")
    private String name;//公司名
    @Column(name ="EXECUTE_GOV")
    private String executegov;//执行法院
    @Column(name ="ACTION_REMARK")
    private String actionremark;//行为备注li
    @Column(name ="EXECUTE_NO")
    private String executeno;//执行依据文号
    @Column(name ="ORG_NO")
    private String orgno;//组织机构代码


    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getLiandate() {
        return liandate;
    }

    public void setLiandate(String liandate) {
        this.liandate = liandate;
    }

    public String getExecutestatus() {
        return executestatus;
    }

    public void setExecutestatus(String executestatus) {
        this.executestatus = executestatus;
    }

    public String getPublicdate() {
        return publicdate;
    }

    public void setPublicdate(String publicdate) {
        this.publicdate = publicdate;
    }

    public String getAnno() {
        return anno;
    }

    public void setAnno(String anno) {
        this.anno = anno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExecutegov() {
        return executegov;
    }

    public void setExecutegov(String executegov) {
        this.executegov = executegov;
    }

    public String getActionremark() {
        return actionremark;
    }

    public void setActionremark(String actionremark) {
        this.actionremark = actionremark;
    }

    public String getExecuteno() {
        return executeno;
    }

    public void setExecuteno(String executeno) {
        this.executeno = executeno;
    }

    public String getOrgno() {
        return orgno;
    }

    public void setOrgno(String orgno) {
        this.orgno = orgno;
    }
}
