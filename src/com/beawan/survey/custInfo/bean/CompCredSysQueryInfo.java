package com.beawan.survey.custInfo.bean;

/**
 * CmCredSysQueryInfo entity. @author MyEclipse Persistence Tools
 */

public class CompCredSysQueryInfo implements java.io.Serializable {

	// Fields

	private Long id;
	private String cusNo;
	private Long taskId;
	private String badRecordInfo;
	private String finaBalanceInfo;

	// Constructors

	/** default constructor */
	public CompCredSysQueryInfo() {
	}

	/** minimal constructor */
	public CompCredSysQueryInfo(Long id) {
		this.id = id;
	}

	/** full constructor */
	public CompCredSysQueryInfo(Long id, String cusNo, Long taskId,
			String badRecordInfo, String finaBalanceInfo) {
		this.id = id;
		this.cusNo = cusNo;
		this.taskId = taskId;
		this.badRecordInfo = badRecordInfo;
		this.finaBalanceInfo = finaBalanceInfo;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCusNo() {
		return this.cusNo;
	}

	public void setCusNo(String cusNo) {
		this.cusNo = cusNo;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getBadRecordInfo() {
		return this.badRecordInfo;
	}

	public void setBadRecordInfo(String badRecordInfo) {
		this.badRecordInfo = badRecordInfo;
	}

	public String getFinaBalanceInfo() {
		return this.finaBalanceInfo;
	}

	public void setFinaBalanceInfo(String finaBalanceInfo) {
		this.finaBalanceInfo = finaBalanceInfo;
	}

}