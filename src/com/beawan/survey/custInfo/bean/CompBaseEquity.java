package com.beawan.survey.custInfo.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * CmBaseEquity entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CM_BASE_EQUITY", schema = "GDTCESYS")
public class CompBaseEquity extends BaseEntity implements java.io.Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_BASE_EQUITY")
	//@SequenceGenerator(name = "CM_BASE_EQUITY",allocationSize=1,initialValue=1, sequenceName = "CM_BASE_EQUITY_SEQ")
	private Long id;
	@Column(name = "CUSTOMER_NO")
	private String customerNo;//客户号
	@Column(name = "STOCK_NAME")
	private String stockName;//股东名称
	@Column(name = "CURRENCY")
	private String currency;//币种
	@Column(name = "FUND_AMOUNT")
	private String fundAmount;//出资金额（万元）
	@Column(name = "FUND_RATE")
	private String fundRate;//持股比例
	@Column(name = "FUND_WAY")
	private String fundWay;//出资方式
	@Column(name = "INVT_FACT_AMT")
	private String invtFactAmt;//实际到位资金,实收资本
	@Column(name = "RELATION_DESC")
	private String relationDesc;//与申请人经营管理上及实际控制人的关系
	@Column(name = "REMARK")
	private String remark;//备注

	// Constructors

	/** default constructor */
	public CompBaseEquity() {
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustomerNo() {
		return this.customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getStockName() {
		return this.stockName;
	}

	public void setStockName(String stockName) {
		this.stockName = stockName;
	}
	
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getFundAmount() {
		return this.fundAmount;
	}

	public void setFundAmount(String fundAmount) {
		this.fundAmount = fundAmount;
	}

	public String getFundRate() {
		return this.fundRate;
	}

	public void setFundRate(String fundRate) {
		if(fundRate.contains("%")){
			this.fundRate = fundRate.replace("%", "");
		}else{
			this.fundRate = fundRate;
		}
	}

	public String getFundWay() {
		return this.fundWay;
	}

	public void setFundWay(String fundWay) {
		this.fundWay = fundWay;
	}

	public String getInvtFactAmt() {
		return invtFactAmt;
	}

	public void setInvtFactAmt(String invtFactAmt) {
		this.invtFactAmt = invtFactAmt;
	}

	public String getRelationDesc() {
		return this.relationDesc;
	}

	public void setRelationDesc(String relationDesc) {
		this.relationDesc = relationDesc;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}