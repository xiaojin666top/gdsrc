package com.beawan.survey.custInfo.bean;

// default package

/**
 * CompRunEstaLand entity. @author MyEclipse Persistence Tools
 */

public class CompRunEstaLand implements java.io.Serializable {

	// Fields

	private Long id;
	private String customerNo;//客户号
	private String projName;//项目名称
	private Double landArea;//土地面积
	private String landNature;//性质
	private Double buildPlanArea;//建筑规划面积
	private String planUse;//规划用途
	private String mortStatus;//抵押状态
	private Double payableLandCost;//应付土地费用
	private Double unpaidLandCost;//未付土地费用
	private Double investRate;//投资占比
	private String mortInBank;//抵押行
	private String estimDeveTime;//预计开发时间
	private Long taskId;

	// Constructors

	/** default constructor */
	public CompRunEstaLand() {
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustomerNo() {
		return this.customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getProjName() {
		return this.projName;
	}

	public void setProjName(String projName) {
		this.projName = projName;
	}

	public Double getBuildPlanArea() {
		return this.buildPlanArea;
	}

	public void setBuildPlanArea(Double buildPlanArea) {
		this.buildPlanArea = buildPlanArea;
	}

	public String getPlanUse() {
		return this.planUse;
	}

	public void setPlanUse(String planUse) {
		this.planUse = planUse;
	}

	public String getMortStatus() {
		return this.mortStatus;
	}

	public void setMortStatus(String mortStatus) {
		this.mortStatus = mortStatus;
	}

	public Double getPayableLandCost() {
		return this.payableLandCost;
	}

	public void setPayableLandCost(Double payableLandCost) {
		this.payableLandCost = payableLandCost;
	}

	public Double getUnpaidLandCost() {
		return this.unpaidLandCost;
	}

	public void setUnpaidLandCost(Double unpaidLandCost) {
		this.unpaidLandCost = unpaidLandCost;
	}

	public Double getInvestRate() {
		return this.investRate;
	}

	public void setInvestRate(Double investRate) {
		this.investRate = investRate;
	}

	public String getMortInBank() {
		return this.mortInBank;
	}

	public void setMortInBank(String mortInBank) {
		this.mortInBank = mortInBank;
	}

	public String getEstimDeveTime() {
		return this.estimDeveTime;
	}

	public void setEstimDeveTime(String estimDeveTime) {
		this.estimDeveTime = estimDeveTime;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public Double getLandArea() {
		return landArea;
	}

	public void setLandArea(Double landArea) {
		this.landArea = landArea;
	}

	public String getLandNature() {
		return landNature;
	}

	public void setLandNature(String landNature) {
		this.landNature = landNature;
	}

}