package com.beawan.survey.custInfo.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * CmFinance entity. 各个模块的财务分析内容
 */

@Entity
@Table(name = "CM_FINANCE",schema = "GDTCESYS")
public class CompFinance extends BaseEntity implements java.io.Serializable {

	// Fields

	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="CM_FINANCE_SEQ")
	//@SequenceGenerator(name="CM_FINANCE_SEQ",allocationSize=1,initialValue=1, sequenceName="CM_FINANCE_SEQ")
	private Long id;
	@Column(name = "TASK_ID")
	private Long taskId;
	@Column(name = "CUSTOMER_NO")
	private String customerNo;
	@Column(name = "TAX_ANALY")
	private String taxAnaly; // 纳税分析
	@Column(name = "CASH_FLOW_ANALY")
	private String cashFlowAnaly; // 现金流情况分析
	@Column(name = "PROFIT_ANALY")
	private String profitAnaly;//盈利能力分析
	@Column(name = "DEVELOP_ANALY")
	private String developAnaly;//发展能力分析
	@Column(name = "SOLVENCY")
	private String solvency;//偿债能力分析
	@Column(name = "OPERATE_ANALY")
	private String operateAnaly;//运营能力分析
	@Column(name = "SYSTEM_ANALY")
	private String systemAnaly; // 系统分析结论
	@Column(name = "FINANCE_ANALY")
	private String financeAnaly; // 财务状况总结（客户经理填写）
	@Column(name = "RATIO_ANALYSIS")
	private String ratioAnalysis; // 比率指标分析
	// 暂时没用上，后期删除
	@Column(name = "OPER_CASH_FLOW_ANALY")
	private String operCashFlowAnaly; // 经营性现金流回笼情况说明
	@Column(name = "INCOME_ANALY")
	private String incomeAnaly; // 损益情况说明
	@Column(name = "EXC_RATE_RISK")
	private String excRateRisk; // 汇率风险分析
	


	// Constructors

	/** default constructor */
	public CompFinance() {
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getCustomerNo() {
		return this.customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getRatioAnalysis() {
		return this.ratioAnalysis;
	}

	public void setRatioAnalysis(String ratioAnalysis) {
		this.ratioAnalysis = ratioAnalysis;
	}

	public String getCashFlowAnaly() {
		return this.cashFlowAnaly;
	}

	public void setCashFlowAnaly(String cashFlowAnaly) {
		this.cashFlowAnaly = cashFlowAnaly;
	}

	public String getIncomeAnaly() {
		return this.incomeAnaly;
	}

	public void setIncomeAnaly(String incomeAnaly) {
		this.incomeAnaly = incomeAnaly;
	}

	public String getTaxAnaly() {
		return this.taxAnaly;
	}

	public void setTaxAnaly(String taxAnaly) {
		this.taxAnaly = taxAnaly;
	}

	public String getExcRateRisk() {
		return this.excRateRisk;
	}

	public void setExcRateRisk(String excRateRisk) {
		this.excRateRisk = excRateRisk;
	}

	public String getFinanceAnaly() {
		return this.financeAnaly;
	}

	public void setFinanceAnaly(String financeAnaly) {
		this.financeAnaly = financeAnaly;
	}

	public String getSystemAnaly() {
		return this.systemAnaly;
	}

	public void setSystemAnaly(String systemAnaly) {
		this.systemAnaly = systemAnaly;
	}

	public String getOperCashFlowAnaly() {
		return operCashFlowAnaly;
	}

	public void setOperCashFlowAnaly(String operCashFlowAnaly) {
		this.operCashFlowAnaly = operCashFlowAnaly;
	}

	public String getProfitAnaly() {
		return profitAnaly;
	}

	public void setProfitAnaly(String profitAnaly) {
		this.profitAnaly = profitAnaly;
	}

	public String getDevelopAnaly() {
		return developAnaly;
	}

	public void setDevelopAnaly(String developAnaly) {
		this.developAnaly = developAnaly;
	}

	public String getSolvency() {
		return solvency;
	}

	public void setSolvency(String solvency) {
		this.solvency = solvency;
	}

	public String getOperateAnaly() {
		return operateAnaly;
	}

	public void setOperateAnaly(String operateAnaly) {
		this.operateAnaly = operateAnaly;
	}
}