package com.beawan.survey.custInfo.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 经营场所-土地
 * CmRunArea entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CM_RUN_AREA_LAND",schema = "GDTCESYS")
public class CompRunAreaTd extends BaseEntity implements java.io.Serializable {

	@Id
//	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_RUN_AREA_LAND")
	//@SequenceGenerator(name = "CM_RUN_AREA_LAND",allocationSize=1,initialValue=1, sequenceName = "CM_RUN_AREA_LAND_SEQ")
	private Long id;
	@Column(name = "TASK_ID")
	private Long taskId;
	@Column(name = "CUSTOMER_NO")
	private String customerNo;
	@Column(name = "LAND_AREA")
	private Double landArea;//土地面积
	@Column(name = "LAND_NATURE")
	private String landNature;//土地性质
	@Column(name = "LAND_COST")
	private Double landCost;//土地价值
	@Column(name = "LAND_AGE")
	private String landAge;//取得年限
	@Column(name = "LAND_TYPE")
	private String landType;//土地类型
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getTaskId() {
		return taskId;
	}
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public Double getLandArea() {
		return landArea;
	}
	public void setLandArea(Double landArea) {
		this.landArea = landArea;
	}
	public String getLandNature() {
		return landNature;
	}
	public void setLandNature(String landNature) {
		this.landNature = landNature;
	}
	public Double getLandCost() {
		return landCost;
	}
	public void setLandCost(Double landCost) {
		this.landCost = landCost;
	}
	public String getLandAge() {
		return landAge;
	}
	public void setLandAge(String landAge) {
		this.landAge = landAge;
	}
	public String getLandType() {
		return landType;
	}
	public void setLandType(String landType) {
		this.landType = landType;
	}
	
}