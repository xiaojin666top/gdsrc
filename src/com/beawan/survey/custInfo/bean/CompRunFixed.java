package com.beawan.survey.custInfo.bean;
// default package



/**
 * CompRumFixed entity. @author MyEclipse Persistence Tools
 */

public class CompRunFixed  implements java.io.Serializable {


    // Fields    

	 private Long id;
     private Long taskId;
     private String customerNo;
     private String industryAnalysis; //行业分析
     private String productAnalysis; //产品情况
     private String supplyAnalysis; //供应分析
     private String produceAnalysis; //生产分析
     private String salesAnalysis; //销售基本情况
     private String prodSalesAnalysis; //产品销售分析
     private String enviroment; //环保情况
     private String otherInfo; //其他信息


    // Constructors

    /** default constructor */
    public CompRunFixed() {
    }

   
    // Property accessors

    public Long getTaskId() {
        return this.taskId;
    }
    
    public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getCustomerNo() {
        return this.customerNo;
    }
    
    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getIndustryAnalysis() {
        return this.industryAnalysis;
    }
    
    public void setIndustryAnalysis(String industryAnalysis) {
        this.industryAnalysis = industryAnalysis;
    }

    public String getProductAnalysis() {
        return this.productAnalysis;
    }
    
    public void setProductAnalysis(String productAnalysis) {
        this.productAnalysis = productAnalysis;
    }

    public String getSupplyAnalysis() {
        return this.supplyAnalysis;
    }
    
    public void setSupplyAnalysis(String supplyAnalysis) {
        this.supplyAnalysis = supplyAnalysis;
    }

    public String getProduceAnalysis() {
        return this.produceAnalysis;
    }
    
    public void setProduceAnalysis(String produceAnalysis) {
        this.produceAnalysis = produceAnalysis;
    }

    public String getSalesAnalysis() {
        return this.salesAnalysis;
    }
    
    public void setSalesAnalysis(String salesAnalysis) {
        this.salesAnalysis = salesAnalysis;
    }

    public String getProdSalesAnalysis() {
        return this.prodSalesAnalysis;
    }
    
    public void setProdSalesAnalysis(String prodSalesAnalysis) {
        this.prodSalesAnalysis = prodSalesAnalysis;
    }

    public String getEnviroment() {
        return this.enviroment;
    }
    
    public void setEnviroment(String enviroment) {
        this.enviroment = enviroment;
    }

    public String getOtherInfo() {
        return this.otherInfo;
    }
    
    public void setOtherInfo(String otherInfo) {
        this.otherInfo = otherInfo;
    }
   








}