package com.beawan.survey.custInfo.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;
import com.google.gson.annotations.SerializedName;

/**
 * 执行
 */
@Entity
@Table(schema = "GDTCESYS", name = "COMP_ZHIXING_ITEMS")
public class CompZhiXingItems extends BaseEntity{

	@Id
    @Column(name = "ID")
//	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="COMP_ZHIXING_ITEMS")
//	@SequenceGenerator(name="COMP_ZHIXING_ITEMS",allocationSize=1,initialValue=1, sequenceName="COMP_ZHIXING_ITEMS_SEQ")
    private String id;

    @Column(name = "CUSTOMER_NO")
    private String customerNo;
	@Column(name = "UPDATE_DATE")
    private String updatedate;//数据更新时间
	@Column(name = "PARTY_CARDNUM")
    private String partyCardNum;//身份证号码/组织机构代码
	@Column(name = "ANNO")
    private String anno;//立案号
	@Column(name = "NAME")
    private String name;//名称
	@Column(name = "STATE")
    private String state;//状态
	@Column(name = "SOURCE_ID")
    private String sourceid;//官网系统id
    @Column(name = "EXECUTE_GOV")
    private String executeGov;//执行法院
    @Column(name = "LIAN_DATE")
    private String liandate;//立案时间
    @Column(name = "BIAO_DI")
    private String biaodi;//标地

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(String updatedate) {
        this.updatedate = updatedate;
    }

    public String getPartyCardNum() {
        return partyCardNum;
    }

    public void setPartyCardNum(String partyCardNum) {
        this.partyCardNum = partyCardNum;
    }

    public String getAnno() {
        return anno;
    }

    public void setAnno(String anno) {
        this.anno = anno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getSourceid() {
        return sourceid;
    }

    public void setSourceid(String sourceid) {
        this.sourceid = sourceid;
    }

    public String getExecuteGov() {
        return executeGov;
    }

    public void setExecuteGov(String executeGov) {
        this.executeGov = executeGov;
    }

    public String getLiandate() {
        return liandate;
    }

    public void setLiandate(String liandate) {
        this.liandate = liandate;
    }

    public String getBiaodi() {
        return biaodi;
    }

    public void setBiaodi(String biaodi) {
        this.biaodi = biaodi;
    }
}
