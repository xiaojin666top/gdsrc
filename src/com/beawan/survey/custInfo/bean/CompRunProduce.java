package com.beawan.survey.custInfo.bean;

/**
 * CmRunProduce entity. @author MyEclipse Persistence Tools
 */

public class CompRunProduce implements java.io.Serializable {

	// Fields

	private Long id;
	private Long taskId;
	private String customerNo;
	private String productionScale; //生产规模情况
	private String productionConditions; //主要生产条件情况
	private String productionProcess; //生产工艺状况
	private String productQuality; //产品合格率或质量认证情况
	private String productionPlan; //生产计划分析
	private String productionPower; //生产能力分析
	private String proQualification; //生产资质变化情况

	// Constructors

	/** default constructor */
	public CompRunProduce() {
	}


	// Property accessors

	
	public Long getTaskId() {
		return this.taskId;
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getCustomerNo() {
		return this.customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getProductionScale() {
		return this.productionScale;
	}

	public void setProductionScale(String productionScale) {
		this.productionScale = productionScale;
	}

	public String getProductionConditions() {
		return this.productionConditions;
	}

	public void setProductionConditions(String productionConditions) {
		this.productionConditions = productionConditions;
	}

	public String getProductionProcess() {
		return this.productionProcess;
	}

	public void setProductionProcess(String productionProcess) {
		this.productionProcess = productionProcess;
	}

	public String getProductQuality() {
		return this.productQuality;
	}

	public void setProductQuality(String productQuality) {
		this.productQuality = productQuality;
	}

	public String getProductionPlan() {
		return this.productionPlan;
	}

	public void setProductionPlan(String productionPlan) {
		this.productionPlan = productionPlan;
	}

	public String getProductionPower() {
		return this.productionPower;
	}

	public void setProductionPower(String productionPower) {
		this.productionPower = productionPower;
	}

	public String getProQualification() {
		return this.proQualification;
	}

	public void setProQualification(String proQualification) {
		this.proQualification = proQualification;
	}

}