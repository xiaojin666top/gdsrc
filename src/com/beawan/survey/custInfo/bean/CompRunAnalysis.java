package com.beawan.survey.custInfo.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 企业经营情况说明
 * @author zxh
 * @date 2020/7/10 17:10
 */
@Entity
@Table(name = "CM_RUN_ANALYSIS", schema = "GDTCESYS")
public class CompRunAnalysis extends BaseEntity implements java.io.Serializable {

    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_RUN_ANALYSIS")
   // @SequenceGenerator(name = "CM_RUN_ANALYSIS",allocationSize=1,initialValue=1, sequenceName = "CM_RUN_ANALYSIS_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo;
    @Column(name = "CUSTOMER_NO")
    private String customerNo;
    @Column(name = "BUSINESS_PLACE")
    private String businessPlace;//经营场所情况
    @Column(name = "BUSINESS_PLACE_LAND")
    private String businessPlaceLand;//经营场所情况(土地)
    @Column(name = "BUSINESS_PLACE_PLANT")
    private String businessPlacePlant;//经营场所情况（厂房）
    @Column(name = "RELATED_COMPANIES")
    private String relatedCompanies;//关联企业情况
    @Column(name = "INDUS_INFO")
    private String industryInfo;// 行业基本情况
    @Column(name = "SUPPLIER_INFO")
    private String supplyInfo;// 供应情况说明
    @Column(name = "ROW_MATERIAL_INFO")
    private String rawMaterialInfo;// 原材料情况说明
    @Column(name = "PRODUCE_INFO")
    private String produceInfo;// 生产情况说明
    @Column(name = "PRODUCT_INFO")
    private String productInfo; // 产品情况说明
    @Column(name = "ENVIRONMENT_INFO")
    private String environmentInfo;// 环保达标情况
    @Column(name = "QUALIFY_CHANGE_INFO")
    private String qualifyChangeInfo;// 生产资质变化情况
    @Column(name = "PRODUCT_SALES_INFO")
    private String productSalesInfo; // 销售情况说明

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getBusinessPlace() {
        return businessPlace;
    }

    public void setBusinessPlace(String businessPlace) {
        this.businessPlace = businessPlace;
    }

    public String getRelatedCompanies() {
        return relatedCompanies;
    }

    public void setRelatedCompanies(String relatedCompanies) {
        this.relatedCompanies = relatedCompanies;
    }

    public String getIndustryInfo() {
        return industryInfo;
    }

    public void setIndustryInfo(String industryInfo) {
        this.industryInfo = industryInfo;
    }

    public String getEnvironmentInfo() {
        return environmentInfo;
    }

    public void setEnvironmentInfo(String environmentInfo) {
        this.environmentInfo = environmentInfo;
    }

    public String getSupplyInfo() {
        return supplyInfo;
    }

    public void setSupplyInfo(String supplyInfo) {
        this.supplyInfo = supplyInfo;
    }

    public String getRawMaterialInfo() {
        return rawMaterialInfo;
    }

    public void setRawMaterialInfo(String rawMaterialInfo) {
        this.rawMaterialInfo = rawMaterialInfo;
    }

    public String getProduceInfo() {
        return produceInfo;
    }

    public void setProduceInfo(String produceInfo) {
        this.produceInfo = produceInfo;
    }

    public String getProductSalesInfo() {
        return productSalesInfo;
    }

    public void setProductSalesInfo(String productSalesInfo) {
        this.productSalesInfo = productSalesInfo;
    }

    public String getQualifyChangeInfo() {
        return qualifyChangeInfo;
    }

    public void setQualifyChangeInfo(String qualifyChangeInfo) {
        this.qualifyChangeInfo = qualifyChangeInfo;
    }

    public String getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(String productInfo) {
        this.productInfo = productInfo;
    }

	public String getBusinessPlaceLand() {
		return businessPlaceLand;
	}

	public void setBusinessPlaceLand(String businessPlaceLand) {
		this.businessPlaceLand = businessPlaceLand;
	}

	public String getBusinessPlacePlant() {
		return businessPlacePlant;
	}

	public void setBusinessPlacePlant(String businessPlacePlant) {
		this.businessPlacePlant = businessPlacePlant;
	}
    
    
}
