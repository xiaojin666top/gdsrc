package com.beawan.survey.custInfo.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * CmConnetCm entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CM_CONNET_CM", schema = "GDTCESYS")
public class CompConnetCm extends BaseEntity implements java.io.Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_CONNET_CM")
	//@SequenceGenerator(name = "CM_CONNET_CM",allocationSize=1,initialValue=1, sequenceName = "CM_CONNET_CM_SEQ")
	private Long id;
	@Column(name = "CUSTOMER_NO")
	private String customerNo;//关联客户号
	@Column(name = "COMPANY_NAME")
	private String companyName;//关联企业
	@Column(name = "LEGAL_PERSON")
	private String legalPerson;//法定代表人
	@Column(name = "MAIN_BUNINESS")
	private String mainBusiness;//主营业务
	@Column(name = "RELATIONSHIP")
	private String relationship;//关联关系
	@Column(name = "REGISTE_CAPITAL")
	private Double registerCapital;//注册资本
	@Column(name = "REAL_CAPITAL")
	private Double realCapital;//实收资本
	@Column(name = "FOUND_DATE")
	private String foundDate;//成立日期
	@Column(name = "EQUITY_STRUCTURE")
	private String equityStructure;//股权结构
	@Column(name = "COMPANY_TYPE")
	private String companyType;//企业性质
	@Column(name = "INDUSTRY")
	private String industry;//所属行业
	@Column(name = "BUSINESS_ADRESS")
	private String businessAddress;//经营地址
	@Column(name = "LAST_YEAR_SALE")
	private Double lastYearSale;//上一年销售额
	@Column(name = "LAST_YEAR_NET_PROFIT")
	private Double lastYearNetProfit;//上一年净利润
	@Column(name = "TOTAL_ASSETS")
	private Double totalAssets;//总资产
	@Column(name = "TOTAL_DEBTS")
	private Double totalDebts;//总负债
	@Column(name = "OTHER_INFO")
	private String otherInfo;//其他情况说明
	@Column(name = "LOANS_BANK_INFO")
	private String loansBankInfo;//本行贷款情况
	
	/** default constructor */
	public CompConnetCm() {
	}

	/** minimal constructor */
	public CompConnetCm(Long id) {
		this.id = id;
	}

	/** full constructor */
	public CompConnetCm(Long id, String customerNo, String companyName,
			Double registerCapital, String foundDate, String legalPerson,
			String equityStructure, String companyType, String industry,
			String mainBusiness, String businessAddress, Double lastYearSale,
			Double lastYearNetProfit, Double totalAssets, Double totalDebts,
			String otherInfo, String relationship, Double realCapital,String loansBankInfo) {
		this.id = id;
		this.customerNo = customerNo;
		this.companyName = companyName;
		this.registerCapital = registerCapital;
		this.realCapital = realCapital;
		this.foundDate = foundDate;
		this.legalPerson = legalPerson;
		this.equityStructure = equityStructure;
		this.companyType = companyType;
		this.industry = industry;
		this.mainBusiness = mainBusiness;
		this.businessAddress = businessAddress;
		this.lastYearSale = lastYearSale;
		this.lastYearNetProfit = lastYearNetProfit;
		this.totalAssets = totalAssets;
		this.totalDebts = totalDebts;
		this.otherInfo = otherInfo;
		this.relationship = relationship;
		this.loansBankInfo = loansBankInfo;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustomerNo() {
		return this.customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Double getRegisterCapital() {
		return registerCapital;
	}

	public void setRegisterCapital(Double registerCapital) {
		this.registerCapital = registerCapital;
	}

	public Double getRealCapital() {
		return realCapital;
	}

	public void setRealCapital(Double realCapital) {
		this.realCapital = realCapital;
	}

	public String getFoundDate() {
		return this.foundDate;
	}

	public void setFoundDate(String foundDate) {
		this.foundDate = foundDate;
	}

	public String getLegalPerson() {
		return this.legalPerson;
	}

	public void setLegalPerson(String legalPerson) {
		this.legalPerson = legalPerson;
	}

	public String getEquityStructure() {
		return this.equityStructure;
	}

	public void setEquityStructure(String equityStructure) {
		this.equityStructure = equityStructure;
	}

	public String getCompanyType() {
		return this.companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public String getIndustry() {
		return this.industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getMainBusiness() {
		return mainBusiness;
	}

	public void setMainBusiness(String mainBusiness) {
		this.mainBusiness = mainBusiness;
	}

	public String getBusinessAddress() {
		return businessAddress;
	}

	public void setBusinessAddress(String businessAddress) {
		this.businessAddress = businessAddress;
	}

	public Double getLastYearSale() {
		return this.lastYearSale;
	}

	public void setLastYearSale(Double lastYearSale) {
		this.lastYearSale = lastYearSale;
	}

	public Double getLastYearNetProfit() {
		return this.lastYearNetProfit;
	}

	public void setLastYearNetProfit(Double lastYearNetProfit) {
		this.lastYearNetProfit = lastYearNetProfit;
	}

	public Double getTotalAssets() {
		return this.totalAssets;
	}

	public void setTotalAssets(Double totalAssets) {
		this.totalAssets = totalAssets;
	}

	public Double getTotalDebts() {
		return this.totalDebts;
	}

	public void setTotalDebts(Double totalDebts) {
		this.totalDebts = totalDebts;
	}

	public String getOtherInfo() {
		return this.otherInfo;
	}

	public void setOtherInfo(String otherInfo) {
		this.otherInfo = otherInfo;
	}

	public String getRelationship() {
		return this.relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public String getLoansBankInfo() {
		return loansBankInfo;
	}

	public void setLoansBankInfo(String loansBankInfo) {
		this.loansBankInfo = loansBankInfo;
	}
	
}