package com.beawan.survey.custInfo.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 企业能耗情况
 * @author zxh
 * @date 2020/7/10 11:31
 */
@Entity
@Table(name = "CM_RUN_POWER", schema = "GDTCESYS")
public class CompPower extends BaseEntity implements Serializable {

    @Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_RUN_POWER")
   // @SequenceGenerator(name = "CM_RUN_POWER",allocationSize=1,initialValue=1, sequenceName = "CM_POWER_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo; // 流水号
    @Column(name = "CUSTOMER_NO")
    private String customerNo;// 客户号
    @Column(name = "YEAR")
    private String year;// 年份
    @Column(name = "ELECTRICITY_FEE")
    private Double electricityFee;// 电费
    @Column(name = "OTHER_FEE")
    private Double otherFee;// 水/汽/煤等其他费用
    @Column(name = "STAFF_PAY")
    private Double staffPay;// 员工工资
    @Column(name = "STAFF_NUM")
    private Integer staffNum;// 员工人数

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Double getElectricityFee() {
        return electricityFee;
    }

    public void setElectricityFee(Double electricityFee) {
        this.electricityFee = electricityFee;
    }

    public Double getOtherFee() {
        return otherFee;
    }

    public void setOtherFee(Double otherFee) {
        this.otherFee = otherFee;
    }

    public Double getStaffPay() {
        return staffPay;
    }

    public void setStaffPay(Double staffPay) {
        this.staffPay = staffPay;
    }

    public Integer getStaffNum() {
        return staffNum;
    }

    public void setStaffNum(Integer staffNum) {
        this.staffNum = staffNum;
    }
}
