package com.beawan.survey.custInfo.bean;

/**
 * CmRun entity. @author MyEclipse Persistence Tools
 */

public class CompRunGeneral implements java.io.Serializable {

	// Fields

	private Long id;
	private Long taskId;
	private String customerNo;
	private String development;// 目标和战略
	private String marketAnalysis;// 市场分析
	private String productAnalysis;// 产品情况分析
	private String supplierAnalysis;// 主要供应商分析
	private String rawMaterialAnalysis;// 主要原材料分析
	private String produceAnalysis;// 生产情况分析
	private String salesModel; // 销售策略、模式
	private String customerAnalysis; // 主要下游客户分析
	private String productSales; // 产品销售分析
	private String salesPlan; // 销售计划
	private String energyPayAnalysis; // 能源耗用及工资等情况分析
	private String energyStatMonth; //能源耗用情况及工人工资情况统计截止月份
	private Long indusAnalyCode;// 行业分析编号
	private String indusComPolicy;// 行业国家政策
	private String indusMyPolicy;// 行业我行政策
//	private String indusPolicyAnalysis;// 行业政策分析
//	private String indusBaseInfo;// 行业基本情况分析
//	private String indusSwot;// 行业内地位及优劣势分析
	private String indusAnalysis;//行业分析
	private String enviroment;// 环保情况

	// Constructors

	/** default constructor */
	public CompRunGeneral() {
	}

	/** minimal constructor */
	public CompRunGeneral(Long id) {
		this.id = id;
	}

	// Property accessors

	public Long getTaskId() {
		return this.taskId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getCustomerNo() {
		return this.customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getDevelopment() {
		return this.development;
	}

	public void setDevelopment(String development) {
		this.development = development;
	}

	public String getMarketAnalysis() {
		return this.marketAnalysis;
	}

	public void setMarketAnalysis(String marketAnalysis) {
		this.marketAnalysis = marketAnalysis;
	}

	public String getProductAnalysis() {
		return this.productAnalysis;
	}

	public void setProductAnalysis(String productAnalysis) {
		this.productAnalysis = productAnalysis;
	}

	public String getSupplierAnalysis() {
		return this.supplierAnalysis;
	}

	public void setSupplierAnalysis(String supplierAnalysis) {
		this.supplierAnalysis = supplierAnalysis;
	}

	public String getSalesModel() {
		return this.salesModel;
	}

	public void setSalesModel(String salesModel) {
		this.salesModel = salesModel;
	}

	public String getCustomerAnalysis() {
		return this.customerAnalysis;
	}

	public void setCustomerAnalysis(String customerAnalysis) {
		this.customerAnalysis = customerAnalysis;
	}

	public String getProductSales() {
		return this.productSales;
	}

	public void setProductSales(String productSales) {
		this.productSales = productSales;
	}

	public String getSalesPlan() {
		return this.salesPlan;
	}

	public void setSalesPlan(String salesPlan) {
		this.salesPlan = salesPlan;
	}

	public Long getIndusAnalyCode() {
		return this.indusAnalyCode;
	}

	public void setIndusAnalyCode(Long indusAnalyCode) {
		this.indusAnalyCode = indusAnalyCode;
	}

	public String getIndusComPolicy() {
		return this.indusComPolicy;
	}

	public void setIndusComPolicy(String indusComPolicy) {
		this.indusComPolicy = indusComPolicy;
	}

	public String getIndusMyPolicy() {
		return this.indusMyPolicy;
	}

	public void setIndusMyPolicy(String indusMyPolicy) {
		this.indusMyPolicy = indusMyPolicy;
	}

	

	public String getIndusAnalysis() {
		return indusAnalysis;
	}

	public void setIndusAnalysis(String indusAnalysis) {
		this.indusAnalysis = indusAnalysis;
	}

	public String getEnviroment() {
		return this.enviroment;
	}

	public void setEnviroment(String enviroment) {
		this.enviroment = enviroment;
	}

	public String getProduceAnalysis() {
		return produceAnalysis;
	}

	public void setProduceAnalysis(String produceAnalysis) {
		this.produceAnalysis = produceAnalysis;
	}

	public String getRawMaterialAnalysis() {
		return rawMaterialAnalysis;
	}

	public void setRawMaterialAnalysis(String rawMaterialAnalysis) {
		this.rawMaterialAnalysis = rawMaterialAnalysis;
	}

	public String getEnergyPayAnalysis() {
		return energyPayAnalysis;
	}

	public void setEnergyPayAnalysis(String energyPayAnalysis) {
		this.energyPayAnalysis = energyPayAnalysis;
	}

	public String getEnergyStatMonth() {
		return energyStatMonth;
	}

	public void setEnergyStatMonth(String energyStatMonth) {
		this.energyStatMonth = energyStatMonth;
	}

}