package com.beawan.survey.custInfo.bean;
// default package



/**
 * CompRunEsta entity. @author MyEclipse Persistence Tools
 */

public class CompRunEsta  implements java.io.Serializable {


    // Fields    

	 private Long id;
     private Long taskId;
     private String customerNo;//客户号
     private String industryAnalysis;//行业分析
     private String businessInfo;//企业整体经营情况
     private String estateDevAnaly;//楼盘开发情况分析说明


    // Constructors

    /** default constructor */
    public CompRunEsta() {
    }

   
    // Property accessors

    public Long getTaskId() {
        return this.taskId;
    }
    
    public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getCustomerNo() {
        return this.customerNo;
    }
    
    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getIndustryAnalysis() {
        return this.industryAnalysis;
    }
    
    public void setIndustryAnalysis(String industryAnalysis) {
        this.industryAnalysis = industryAnalysis;
    }

    public String getBusinessInfo() {
        return this.businessInfo;
    }
    
    public void setBusinessInfo(String businessInfo) {
        this.businessInfo = businessInfo;
    }

    public String getEstateDevAnaly() {
        return this.estateDevAnaly;
    }
    
    public void setEstateDevAnaly(String estateDevAnaly) {
        this.estateDevAnaly = estateDevAnaly;
    }
   








}