package com.beawan.survey.custInfo.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 企业融资、对外担保、或有负债情况说明
 */
@Entity
@Table(name = "CM_FINANCING",schema = "GDTCESYS")
public class CompFinancing extends BaseEntity implements java.io.Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="CM_FINANCING")
	//@SequenceGenerator(name="CM_FINANCING",allocationSize=1,initialValue=1, sequenceName="CM_FINANCING_SEQ")
	private Long id;
	@Column(name = "TASK_ID")
	private Long taskId; //业务主键
	@Column(name = "CUSTOMER_NO")
	private String customerNo; //客户号
	@Column(name = "MB_FINANCING")
	private String mbFinancing; //本行合作情况说明
	@Column(name = "OB_FINANCING")
	private String obFinancing; //他行融资情况说明
	@Column(name = "EXT_GUARA")
	private String externalGua; //对外担保情况说明
	@Column(name = "OTHER_CONT_LIABILITY")
	private String otherContLiability; //或有负债情况说明
	@Column(name = "LAWXP_RISK")
	private String lawXPResult; //汇法网查询结果
	@Column(name = "OTHER_INFO")
	private String otherInfo; //其他情况说明
	@Column(name = "BORROWER_CREDIT")
	private String borrowerCredit; //借款人授信情况（建筑业）
	@Column(name = "LINKED_PARTY_CREDIT")
	private String linkedPartyCredit; //关联方授信情况（建筑业）

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustomerNo() {
		return this.customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getMbFinancing() {
		return this.mbFinancing;
	}

	public void setMbFinancing(String mbFinancing) {
		this.mbFinancing = mbFinancing;
	}

	public String getObFinancing() {
		return this.obFinancing;
	}

	public void setObFinancing(String obFinancing) {
		this.obFinancing = obFinancing;
	}

	public String getOtherContLiability() {
		return this.otherContLiability;
	}

	public void setOtherContLiability(String otherContLiability) {
		this.otherContLiability = otherContLiability;
	}

	public String getExternalGua() {
		return externalGua;
	}

	public void setExternalGua(String externalGua) {
		this.externalGua = externalGua;
	}

	public String getLawXPResult() {
		return lawXPResult;
	}

	public void setLawXPResult(String lawXPResult) {
		this.lawXPResult = lawXPResult;
	}

	public String getOtherInfo() {
		return this.otherInfo;
	}

	public void setOtherInfo(String otherInfo) {
		this.otherInfo = otherInfo;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	
	public String getLinkedPartyCredit() {
		return linkedPartyCredit;
	}

	public void setLinkedPartyCredit(String linkedPartyCredit) {
		this.linkedPartyCredit = linkedPartyCredit;
	}
	
	public String getBorrowerCredit() {
		return borrowerCredit;
	}

	public void setBorrowerCredit(String borrowerCredit) {
		this.borrowerCredit = borrowerCredit;
	}
}