package com.beawan.survey.custInfo.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 企业上游供应商情况
 * CmRunSupply entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CM_RUN_SUPPLY", schema = "GDTCESYS")
public class CompRunSupply extends BaseEntity implements java.io.Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_RUN_SUPPLY")
	//@SequenceGenerator(name = "CM_RUN_SUPPLY",allocationSize=1,initialValue=1, sequenceName = "CM_RUN_SUPPLY_SEQ")
	private Integer id;
	@Column(name = "TASK_ID")
	private Long taskId;// 可删
	@Column(name = "SER_NO")
	private String serNo;
	@Column(name = "CUSTOMER_NO")
	private String customerNo;
	@Column(name = "SUPPLY_NAME")
	private String supplyName;//上游供应商
	@Column(name = "STUFF_NAME")
	private String stuffName;//采购主要原材料名称
	@Column(name = "LAST_PURCHASE_AMT")
	private Double lastPurchaseAmt;//上年采购额
	@Column(name = "CURR_PURCHASE_AMT")
	private Double currPurchaseAmt;//本年当期采购额
	@Column(name = "SETTLE_WAY")
	private String settleWay;//结算方式
	@Column(name = "ACCOUNT_PERIOD")
	private String accountPeriod;//账期
	@Column(name = "CURR_AMT_RATE")
	private Double currAmtRate;//占总成本比重
	@Column(name = "CURR_PAY_AMT")
	private Double currPayAmt;//即期应付账款余额

	/** default constructor */
	public CompRunSupply() {
	}

	public String getSerNo() {
		return serNo;
	}

	public void setSerNo(String serNo) {
		this.serNo = serNo;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSupplyName() {
		return this.supplyName;
	}

	public void setSupplyName(String supplyName) {
		this.supplyName = supplyName;
	}

	public String getStuffName() {
		return this.stuffName;
	}

	public void setStuffName(String stuffName) {
		this.stuffName = stuffName;
	}

	public Double getLastPurchaseAmt() {
		return this.lastPurchaseAmt;
	}

	public void setLastPurchaseAmt(Double lastPurchaseAmt) {
		this.lastPurchaseAmt = lastPurchaseAmt;
	}

	public Double getCurrPurchaseAmt() {
		return this.currPurchaseAmt;
	}

	public void setCurrPurchaseAmt(Double currPurchaseAmt) {
		this.currPurchaseAmt = currPurchaseAmt;
	}

	public String getSettleWay() {
		return this.settleWay;
	}

	public void setSettleWay(String settleWay) {
		this.settleWay = settleWay;
	}

	public String getAccountPeriod() {
		return this.accountPeriod;
	}

	public void setAccountPeriod(String accountPeriod) {
		this.accountPeriod = accountPeriod;
	}

	public Double getCurrAmtRate() {
		return this.currAmtRate;
	}

	public void setCurrAmtRate(Double currAmtRate) {
		this.currAmtRate = currAmtRate;
	}

	public Double getCurrPayAmt() {
		return this.currPayAmt;
	}

	public void setCurrPayAmt(Double currPayAmt) {
		this.currPayAmt = currPayAmt;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

}