package com.beawan.survey.custInfo.bean;
// default package



/**
 * CompRunEstaDeveingProj entity. @author MyEclipse Persistence Tools
 */

public class CompRunEstaDevingProj  implements java.io.Serializable {


    // Fields    

     private Long id;
     private String customerNo;//客户号
     private String projName;//项目名称
     private Double totalInveAmt;//总投资额
     private Double fundingGap;//资金缺口
     private String mortStatus;//抵押状态
     private Double buildArea;//建筑面积
     private Double advaSaleAreaRate;//预售面积比例
     private Double inveRate;//投资比例
     private String startDate;//开工日期
     private String endDate;//峻工日期
     private Double ownFunds;//自有资金
     private Double loanFromBank;//银行融资
     private Double expectSaleIncome;//预计还可实现销售
     private Double floorArea;//占地面积
     private Double advaSaleIncome;//已实现预售收入
     private Double advaSaleArea;//已销售面积
     private Long taskId;


    // Constructors

    /** default constructor */
    public CompRunEstaDevingProj() {
    }

    // Property accessors

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerNo() {
        return this.customerNo;
    }
    
    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getProjName() {
        return this.projName;
    }
    
    public void setProjName(String projName) {
        this.projName = projName;
    }

    public Double getTotalInveAmt() {
        return this.totalInveAmt;
    }
    
    public void setTotalInveAmt(Double totalInveAmt) {
        this.totalInveAmt = totalInveAmt;
    }

    public Double getFundingGap() {
        return this.fundingGap;
    }
    
    public void setFundingGap(Double fundingGap) {
        this.fundingGap = fundingGap;
    }

    public String getMortStatus() {
        return this.mortStatus;
    }
    
    public void setMortStatus(String mortStatus) {
        this.mortStatus = mortStatus;
    }

    public Double getBuildArea() {
        return this.buildArea;
    }
    
    public void setBuildArea(Double buildArea) {
        this.buildArea = buildArea;
    }

    public Double getAdvaSaleAreaRate() {
        return this.advaSaleAreaRate;
    }
    
    public void setAdvaSaleAreaRate(Double advaSaleAreaRate) {
        this.advaSaleAreaRate = advaSaleAreaRate;
    }

    public Double getInveRate() {
        return this.inveRate;
    }
    
    public void setInveRate(Double inveRate) {
        this.inveRate = inveRate;
    }

    public String getStartDate() {
        return this.startDate;
    }
    
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return this.endDate;
    }
    
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Double getOwnFunds() {
        return this.ownFunds;
    }
    
    public void setOwnFunds(Double ownFunds) {
        this.ownFunds = ownFunds;
    }

    public Double getLoanFromBank() {
        return this.loanFromBank;
    }
    
    public void setLoanFromBank(Double loanFromBank) {
        this.loanFromBank = loanFromBank;
    }

    public Double getExpectSaleIncome() {
        return this.expectSaleIncome;
    }
    
    public void setExpectSaleIncome(Double expectSaleIncome) {
        this.expectSaleIncome = expectSaleIncome;
    }

    public Double getFloorArea() {
        return this.floorArea;
    }
    
    public void setFloorArea(Double floorArea) {
        this.floorArea = floorArea;
    }

    public Double getAdvaSaleIncome() {
        return this.advaSaleIncome;
    }
    
    public void setAdvaSaleIncome(Double advaSaleIncome) {
        this.advaSaleIncome = advaSaleIncome;
    }

    public Double getAdvaSaleArea() {
        return this.advaSaleArea;
    }
    
    public void setAdvaSaleArea(Double advaSaleArea) {
        this.advaSaleArea = advaSaleArea;
    }

    public Long getTaskId() {
        return this.taskId;
    }
    
    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }
   








}