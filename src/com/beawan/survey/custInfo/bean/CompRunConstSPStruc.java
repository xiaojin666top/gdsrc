package com.beawan.survey.custInfo.bean;
// default package



/**
 * CompRunSignedProjsStruc entity. @author MyEclipse Persistence Tools
 */

public class CompRunConstSPStruc  implements java.io.Serializable {


    // Fields    

     private Long id;
     private String customerNo;
     private String category; //类别
     private Double contractAmt; //合同金额
     private Double projProgress; //进度
     private Double payableAmt; //应结算工程款
     private Double paidoffAmt; //实结算工程款
     private Double unpaidAmt; //应付未付金额
     private Long taskId;


    // Constructors

    /** default constructor */
    public CompRunConstSPStruc() {
    }

    // Property accessors

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerNo() {
        return this.customerNo;
    }
    
    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getCategory() {
        return this.category;
    }
    
    public void setCategory(String category) {
        this.category = category;
    }

    public Double getContractAmt() {
        return this.contractAmt;
    }
    
    public void setContractAmt(Double contractAmt) {
        this.contractAmt = contractAmt;
    }

    public Double getProjProgress() {
        return this.projProgress;
    }
    
    public void setProjProgress(Double projProgress) {
        this.projProgress = projProgress;
    }

    public Double getPayableAmt() {
        return this.payableAmt;
    }
    
    public void setPayableAmt(Double payableAmt) {
        this.payableAmt = payableAmt;
    }

    public Double getPaidoffAmt() {
        return this.paidoffAmt;
    }
    
    public void setPaidoffAmt(Double paidoffAmt) {
        this.paidoffAmt = paidoffAmt;
    }

    public Double getUnpaidAmt() {
        return this.unpaidAmt;
    }
    
    public void setUnpaidAmt(Double unpaidAmt) {
        this.unpaidAmt = unpaidAmt;
    }

    public Long getTaskId() {
        return this.taskId;
    }
    
    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }
   








}