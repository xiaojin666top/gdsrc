package com.beawan.survey.custInfo.bean;

// default package

/**
 * CompRunConst entity. @author MyEclipse Persistence Tools
 */

public class CompRunConst implements java.io.Serializable {

	// Fields

	private Long id;
	private Long taskId;
	private String customerNo;
	 private String development;//目标和战略
     private String marketAnalysis;//市场分析
     private String indusComPolicy;//行业国家政策
     private String indusMyPolicy;//行业我行政策
//     private String indusPolicyAnalysis;//行业政策分析
//     private String indusBaseInfo;//行业基本情况分析
//     private String indusSwot;//行业内地位及优劣势分析
     private String indusAnalysis;//行业分析
     private String enviroment;//环保情况
     private String constQualiType; //施工资质类型
     private String constQualiLevel; //施工资质等级说明
     private String permisConstCategory; //资质证书中允许的施工类别
     private String proConstQualiDesc; //专业施工资质情况分析说明
     private String projectTakeType; //目前主要工程承接方式
     private String businessArea; //经营活动主要区域
     private String constExperience; //施工经历介绍
     private String signedProjsDetail;//在手已签施工工程明细分析说明
     private String signedProjsStruc;//在手已签施工工程结构分析说明
     private String paymentMethod;//主要工程结算方式分析
     private String fundDemandAnalysis;//资金需求分析测算
     private String otherInfo;//其他情况

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getDevelopment() {
		return development;
	}

	public void setDevelopment(String development) {
		this.development = development;
	}

	public String getMarketAnalysis() {
		return marketAnalysis;
	}

	public void setMarketAnalysis(String marketAnalysis) {
		this.marketAnalysis = marketAnalysis;
	}

	public String getIndusComPolicy() {
		return indusComPolicy;
	}

	public void setIndusComPolicy(String indusComPolicy) {
		this.indusComPolicy = indusComPolicy;
	}

	public String getIndusMyPolicy() {
		return indusMyPolicy;
	}

	public void setIndusMyPolicy(String indusMyPolicy) {
		this.indusMyPolicy = indusMyPolicy;
	}

//	public String getIndusPolicyAnalysis() {
//		return indusPolicyAnalysis;
//	}
//
//	public void setIndusPolicyAnalysis(String indusPolicyAnalysis) {
//		this.indusPolicyAnalysis = indusPolicyAnalysis;
//	}
//
//	public String getIndusBaseInfo() {
//		return indusBaseInfo;
//	}
//
//	public void setIndusBaseInfo(String indusBaseInfo) {
//		this.indusBaseInfo = indusBaseInfo;
//	}
//
//	public String getIndusSwot() {
//		return indusSwot;
//	}
//
//	public void setIndusSwot(String indusSwot) {
//		this.indusSwot = indusSwot;
//	}
	
	

	public String getEnviroment() {
		return enviroment;
	}

	public String getIndusAnalysis() {
		return indusAnalysis;
	}

	public void setIndusAnalysis(String indusAnalysis) {
		this.indusAnalysis = indusAnalysis;
	}

	public void setEnviroment(String enviroment) {
		this.enviroment = enviroment;
	}

	public String getConstQualiType() {
		return constQualiType;
	}

	public void setConstQualiType(String constQualiType) {
		this.constQualiType = constQualiType;
	}

	public String getConstQualiLevel() {
		return constQualiLevel;
	}

	public void setConstQualiLevel(String constQualiLevel) {
		this.constQualiLevel = constQualiLevel;
	}

	public String getPermisConstCategory() {
		return permisConstCategory;
	}

	public void setPermisConstCategory(String permisConstCategory) {
		this.permisConstCategory = permisConstCategory;
	}

	public String getProConstQualiDesc() {
		return proConstQualiDesc;
	}

	public void setProConstQualiDesc(String proConstQualiDesc) {
		this.proConstQualiDesc = proConstQualiDesc;
	}

	public String getProjectTakeType() {
		return projectTakeType;
	}

	public void setProjectTakeType(String projectTakeType) {
		this.projectTakeType = projectTakeType;
	}

	public String getBusinessArea() {
		return businessArea;
	}

	public void setBusinessArea(String businessArea) {
		this.businessArea = businessArea;
	}

	public String getConstExperience() {
		return constExperience;
	}

	public void setConstExperience(String constExperience) {
		this.constExperience = constExperience;
	}

	public String getSignedProjsDetail() {
		return signedProjsDetail;
	}

	public void setSignedProjsDetail(String signedProjsDetail) {
		this.signedProjsDetail = signedProjsDetail;
	}

	public String getSignedProjsStruc() {
		return signedProjsStruc;
	}

	public void setSignedProjsStruc(String signedProjsStruc) {
		this.signedProjsStruc = signedProjsStruc;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getFundDemandAnalysis() {
		return fundDemandAnalysis;
	}

	public void setFundDemandAnalysis(String fundDemandAnalysis) {
		this.fundDemandAnalysis = fundDemandAnalysis;
	}

	public String getOtherInfo() {
		return otherInfo;
	}

	public void setOtherInfo(String otherInfo) {
		this.otherInfo = otherInfo;
	}

}