package com.beawan.survey.custInfo.bean;

import com.beawan.core.BaseEntity;
import javax.persistence.*;
/**
 * 纳税记录表
 * CompFinanceTax entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CM_FINANCE_TAX",schema = "GDTCESYS")
public class CompFinanceTax extends BaseEntity implements java.io.Serializable {

    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_FINANCE_TAX")
  //  @SequenceGenerator(name = "CM_FINANCE_TAX", allocationSize = 1, initialValue = 1, sequenceName = "CM_FINANCE_TAX_SEQ")
    private Long id;
    @Column(name = "TASK_ID")
    private Long taskId;// 任务号
    @Column(name = "CUSTOMER_NO")
    private String customerNo;// 客户号
    @Column(name = "ITEM_NAME")
    private String itemName; // 税种名称
    @Column(name = "VALUE1")
    private Double value1; // 第1年数据
    @Column(name = "VALUE2")
    private Double value2; // 第2年数据
    @Column(name = "VALUE3")
    private Double value3;// 第3年数据
    @Column(name = "VALUE4")
    private Double value4;// 当期数据
    @Column(name = "VALUE5")
    private Double value5;// 上年同期数据
    @Column(name = "REMARK")
    private String remark;// 备注


    // Constructors

    /** default constructor */
    public CompFinanceTax() {
    }

    
    // Property accessors

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public Long getTaskId() {
        return this.taskId;
    }
    
    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getCustomerNo() {
        return this.customerNo;
    }
    
    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getItemName() {
        return this.itemName;
    }
    
    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Double getValue1() {
        return this.value1;
    }
    
    public void setValue1(Double value1) {
        this.value1 = value1;
    }

    public Double getValue2() {
        return this.value2;
    }
    
    public void setValue2(Double value2) {
        this.value2 = value2;
    }

    public Double getValue3() {
        return this.value3;
    }
    
    public void setValue3(Double value3) {
        this.value3 = value3;
    }

    public Double getValue4() {
        return this.value4;
    }
    
    public void setValue4(Double value4) {
        this.value4 = value4;
    }

    public Double getValue5() {
        return this.value5;
    }
    
    public void setValue5(Double value5) {
        this.value5 = value5;
    }

    public String getRemark() {
        return this.remark;
    }
    
    public void setRemark(String remark) {
        this.remark = remark;
    }
   








}