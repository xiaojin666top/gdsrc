package com.beawan.survey.custInfo.bean;

/**
 * CmRunPrdSale entity. @author MyEclipse Persistence Tools
 */

public class CompRunPrdSale implements java.io.Serializable {

	// Fields

	private Long id;
	private String productName;//产品名称
	private String year;//年份
	private String salesVolume;//销量
	private String unitPrice;//单价
	private String income;//收入
	private String customerNo;
	private Long taskId;

	// Constructors

	/** default constructor */
	public CompRunPrdSale() {
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductName() {
		return this.productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getYear() {
		return this.year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getSalesVolume() {
		return this.salesVolume;
	}

	public void setSalesVolume(String salesVolume) {
		this.salesVolume = salesVolume;
	}

	public String getUnitPrice() {
		return this.unitPrice;
	}

	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getIncome() {
		return this.income;
	}

	public void setIncome(String income) {
		this.income = income;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

}