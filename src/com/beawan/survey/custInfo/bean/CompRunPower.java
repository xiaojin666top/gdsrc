package com.beawan.survey.custInfo.bean;

/**
 * CmRunPower entity. @author MyEclipse Persistence Tools
 */

public class CompRunPower implements java.io.Serializable {

	// Fields

	private Long id;
	private String itemCode;//项目标识，01：用电量（度）、02：用水量（吨）、03：用气量（立方）、04：工人人数（人）、05：工资总额（万元）
	private String month;//月份
	private Double itemValue;//数值
	private String customerNo;
	private Long taskId;
	
	private String itemName;

	// Constructors

	/** default constructor */
	public CompRunPower() {
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getItemCode() {
		return this.itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getMonth() {
		return this.month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public Double getItemValue() {
		return this.itemValue;
	}

	public void setItemValue(Double itemValue) {
		this.itemValue = itemValue;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

}