package com.beawan.survey.custInfo.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * CmRun entity. @author MyEclipse Persistence Tools
 */

@Entity
@Table(name = "CM_RUN", schema = "GDTCESYS")
public class CompRun extends BaseEntity implements java.io.Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_RUN")
	//@SequenceGenerator(name = "CM_RUN",allocationSize=1,initialValue=1, sequenceName = "CM_RUN_SEQ")
	private Long id;
	@Column(name = "TASK_ID")
	private Long taskId;
	@Column(name = "CUSTOMER_NO")
	private String customerNo;
	@Column(name = "INDUS_COM_POLICY")
	private String indusComPolicy;//行业国家政策
	@Column(name = "INDUS_MY_POLICY")
	private String indusMyPolicy;//行业我行政策
	@Column(name = "INDUS_ANALY")
	private String indusAnalysis;//行业简要分析
	@Column(name = "ENVIRONMENT_ANALY")
	private String environment;//环保达标情况
	@Column(name = "SUPPLIER_ANALY")
	private String supplierAnalysis;//主要供应商分析
	@Column(name = "ROW_MATERIAL_ANALY")
	private String rawMaterial;//主要原材料分析
	@Column(name = "PRODUCT_ANALY")
	private String productAnalysis;//产品情况分析
	@Column(name = "PRODUCT_SALES_ANALY")
	private String productSales; //产品销售分析
	@Column(name = "QUALIFY_CHANGE_ANALY")
	private String qualification;//企业生产资质变化情况
	@Column(name = "SALES_MODEL")
	private String salesModel; //销售策略、模式
	@Column(name = "DOWN_CUSTOMER_ANALY")
	private String downCustomer; //主要下游客户分析
	@Column(name = "DEVELOPMENT")
	private String development;//目标和战略
	@Column(name = "MARKET_ANALY")
	private String marketAnalysis;//市场分析
	@Column(name = "SALES_PLAN")
	private String salesPlan; //销售计划
	@Column(name = "ENERGY_PAY_ANALY")
	private String energyPayAnalysis; //能源耗用及工资等情况分析
	@Column(name = "ENERGY_STAT_MONTH")
	private String energyStatMonth; //能源耗用情况及工人工资情况统计截止月份
	/** default constructor */
	public CompRun() {
	}

	/** minimal constructor */
	public CompRun(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getCustomerNo() {
		return this.customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getProductAnalysis() {
		return this.productAnalysis;
	}

	public void setProductAnalysis(String productAnalysis) {
		this.productAnalysis = productAnalysis;
	}

	public String getSupplierAnalysis() {
		return this.supplierAnalysis;
	}

	public void setSupplierAnalysis(String supplierAnalysis) {
		this.supplierAnalysis = supplierAnalysis;
	}

	public String getProductSales() {
		return this.productSales;
	}

	public void setProductSales(String productSales) {
		this.productSales = productSales;
	}

	public String getIndusComPolicy() {
		return this.indusComPolicy;
	}

	public void setIndusComPolicy(String indusComPolicy) {
		this.indusComPolicy = indusComPolicy;
	}

	public String getIndusMyPolicy() {
		return this.indusMyPolicy;
	}

	public void setIndusMyPolicy(String indusMyPolicy) {
		this.indusMyPolicy = indusMyPolicy;
	}

	public String getIndusAnalysis() {
		return indusAnalysis;
	}

	public void setIndusAnalysis(String indusAnalysis) {
		this.indusAnalysis = indusAnalysis;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public String getRawMaterial() {
		return rawMaterial;
	}

	public void setRawMaterial(String rawMaterial) {
		this.rawMaterial = rawMaterial;
	}

	public String getSalesModel() {
		return salesModel;
	}

	public void setSalesModel(String salesModel) {
		this.salesModel = salesModel;
	}

	public String getDownCustomer() {
		return downCustomer;
	}

	public void setDownCustomer(String downCustomer) {
		this.downCustomer = downCustomer;
	}

	public String getDevelopment() {
		return development;
	}

	public void setDevelopment(String development) {
		this.development = development;
	}

	public String getMarketAnalysis() {
		return marketAnalysis;
	}

	public void setMarketAnalysis(String marketAnalysis) {
		this.marketAnalysis = marketAnalysis;
	}

	public String getSalesPlan() {
		return salesPlan;
	}

	public void setSalesPlan(String salesPlan) {
		this.salesPlan = salesPlan;
	}

	public String getEnergyPayAnalysis() {
		return energyPayAnalysis;
	}

	public void setEnergyPayAnalysis(String energyPayAnalysis) {
		this.energyPayAnalysis = energyPayAnalysis;
	}

	public String getEnergyStatMonth() {
		return energyStatMonth;
	}

	public void setEnergyStatMonth(String energyStatMonth) {
		this.energyStatMonth = energyStatMonth;
	}
}