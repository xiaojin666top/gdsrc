package com.beawan.survey.custInfo.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * CmBaseDesc entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CM_BASE_DESC",schema = "GDTCESYS")
public class CompBaseDesc extends BaseEntity implements java.io.Serializable {

	@Id
	@Column(name = "CUSTOMER_NO")
	private String customerNo;
	@Column(name = "CREDIT_INFO")
	private String creditInfo;//征信情况
	@Column(name = "QUALIFICATION")
	private String qualification;//资质情况
	@Column(name = "ORG_STRUCTURE")
	private String orgStructure;//组织结构及管理模式
	@Column(name = "BUSINESS_INTRO")
	private String businessIntro;//经营情况简介
	@Column(name = "BUSINESS_PLACE")
	private String businessPlace;//经营场所情况分析说明
	@Column(name = "EQUITY_ANALYSIS")
	private String equityAnalysis;//股权情况分析说明（包括变更情况等）
	@Column(name = "MANAGERS_ANALYSIS")
	private String managersAnalysis;//主要管理人员情况分析说明
	@Column(name = "RELATED_COMPANIES")
	private String relatedCompanies;//关联企业分析
	@Column(name = "OTHER_INFO")
	private String otherInfo;//其他情况说明

	// Constructors

	/** default constructor */
	public CompBaseDesc() {
	}

	/** minimal constructor */
	public CompBaseDesc(String customerNo) {
		this.customerNo = customerNo;
	}

	/** full constructor */
	public CompBaseDesc(String customerNo, String creditInfo,
			String qualification, String orgStructure, String businessIntro,
			String businessPlace, String equityAnalysis,
			String managersAnalysis, String relatedCompanies, String otherInfo) {
		this.customerNo = customerNo;
		this.creditInfo = creditInfo;
		this.qualification = qualification;
		this.orgStructure = orgStructure;
		this.businessIntro = businessIntro;
		this.businessPlace = businessPlace;
		this.equityAnalysis = equityAnalysis;
		this.managersAnalysis = managersAnalysis;
		this.relatedCompanies = relatedCompanies;
		this.otherInfo = otherInfo;
	}

	// Property accessors

	public String getCustomerNo() {
		return this.customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getCreditInfo() {
		return this.creditInfo;
	}

	public void setCreditInfo(String creditInfo) {
		this.creditInfo = creditInfo;
	}

	public String getQualification() {
		return this.qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public String getOrgStructure() {
		return this.orgStructure;
	}

	public void setOrgStructure(String orgStructure) {
		this.orgStructure = orgStructure;
	}

	public String getBusinessIntro() {
		return this.businessIntro;
	}

	public void setBusinessIntro(String businessIntro) {
		this.businessIntro = businessIntro;
	}

	public String getBusinessPlace() {
		return this.businessPlace;
	}

	public void setBusinessPlace(String businessPlace) {
		this.businessPlace = businessPlace;
	}

	public String getEquityAnalysis() {
		return this.equityAnalysis;
	}

	public void setEquityAnalysis(String equityAnalysis) {
		this.equityAnalysis = equityAnalysis;
	}

	public String getManagersAnalysis() {
		return this.managersAnalysis;
	}

	public void setManagersAnalysis(String managersAnalysis) {
		this.managersAnalysis = managersAnalysis;
	}

	public String getRelatedCompanies() {
		return this.relatedCompanies;
	}

	public void setRelatedCompanies(String relatedCompanies) {
		this.relatedCompanies = relatedCompanies;
	}

	public String getOtherInfo() {
		return this.otherInfo;
	}

	public void setOtherInfo(String otherInfo) {
		this.otherInfo = otherInfo;
	}

}