package com.beawan.survey.custInfo.bean;
// default package



/**
 * CompRunEstaDevedProj entity. @author MyEclipse Persistence Tools
 */

public class CompRunEstaDevedProj  implements java.io.Serializable {


    // Fields    

     private Long id;
     private String customerNo;
     private String projName;//项目名称
     private Double grossAreaTotal;//总建筑面积
     private Double saleArea;//已销售面积
     private Double saleAreaRate;//已销售比例
     private String projType;//项目类型
     private Double saleIncomeTotal;//已销售总收入
     private Double inveRate;//投资比例
     private String startDate;//开工日期
     private String endDate;//峻工日期
     private Double floorArea;//占地面积
     private Double unSaleArea;//未销售面积
     private Double deveCostTotal;//开发总成本
     private Double unSaleIncome;//未实现销售
     private Long taskId;


    // Constructors

    /** default constructor */
    public CompRunEstaDevedProj() {
    }

    
    /** full constructor */
    public CompRunEstaDevedProj(String customerNo, String projName, Double grossAreaTotal, Double saleArea, Double saleAreaRate, String projType, Double saleIncomeTotal, Double inveRate, String startDate, String endDate, Double floorArea, Double unSaleArea, Double deveCostTotal, Double unSaleIncome, Long taskId) {
        this.customerNo = customerNo;
        this.projName = projName;
        this.grossAreaTotal = grossAreaTotal;
        this.saleArea = saleArea;
        this.saleAreaRate = saleAreaRate;
        this.projType = projType;
        this.saleIncomeTotal = saleIncomeTotal;
        this.inveRate = inveRate;
        this.startDate = startDate;
        this.endDate = endDate;
        this.floorArea = floorArea;
        this.unSaleArea = unSaleArea;
        this.deveCostTotal = deveCostTotal;
        this.unSaleIncome = unSaleIncome;
        this.taskId = taskId;
    }

   
    // Property accessors

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerNo() {
        return this.customerNo;
    }
    
    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getProjName() {
        return this.projName;
    }
    
    public void setProjName(String projName) {
        this.projName = projName;
    }

    public Double getGrossAreaTotal() {
        return this.grossAreaTotal;
    }
    
    public void setGrossAreaTotal(Double grossAreaTotal) {
        this.grossAreaTotal = grossAreaTotal;
    }

    public Double getSaleArea() {
        return this.saleArea;
    }
    
    public void setSaleArea(Double saleArea) {
        this.saleArea = saleArea;
    }

    public Double getSaleAreaRate() {
        return this.saleAreaRate;
    }
    
    public void setSaleAreaRate(Double saleAreaRate) {
        this.saleAreaRate = saleAreaRate;
    }

    public String getProjType() {
        return this.projType;
    }
    
    public void setProjType(String projType) {
        this.projType = projType;
    }

    public Double getSaleIncomeTotal() {
        return this.saleIncomeTotal;
    }
    
    public void setSaleIncomeTotal(Double saleIncomeTotal) {
        this.saleIncomeTotal = saleIncomeTotal;
    }

    public Double getInveRate() {
        return this.inveRate;
    }
    
    public void setInveRate(Double inveRate) {
        this.inveRate = inveRate;
    }

    public String getStartDate() {
        return this.startDate;
    }
    
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return this.endDate;
    }
    
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Double getFloorArea() {
        return this.floorArea;
    }
    
    public void setFloorArea(Double floorArea) {
        this.floorArea = floorArea;
    }

    public Double getUnSaleArea() {
        return this.unSaleArea;
    }
    
    public void setUnSaleArea(Double unSaleArea) {
        this.unSaleArea = unSaleArea;
    }

    public Double getDeveCostTotal() {
        return this.deveCostTotal;
    }
    
    public void setDeveCostTotal(Double deveCostTotal) {
        this.deveCostTotal = deveCostTotal;
    }

    public Double getUnSaleIncome() {
        return this.unSaleIncome;
    }
    
    public void setUnSaleIncome(Double unSaleIncome) {
        this.unSaleIncome = unSaleIncome;
    }

    public Long getTaskId() {
        return this.taskId;
    }
    
    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }
   








}