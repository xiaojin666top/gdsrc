package com.beawan.survey.custInfo.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * CmBaseManager entity. @author MyEclipse Persistence Tools
 */

@Entity
@Table(name = "CM_BASE_MANAGER", schema = "GDTCESYS")
public class CompBaseManager extends BaseEntity implements java.io.Serializable {
	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_BASE_MANAGER")
	//@SequenceGenerator(name = "CM_BASE_MANAGER",allocationSize=1,initialValue=1, sequenceName = "CM_BASE_MANAGER_SEQ")
	private Long id;
	@Column(name = "CM_CUS_NO")
	private String cmCusNo;//企业客户号
	@Column(name = "PS_CUS_NO")
	private String psCusNo;//个人客户号
	@Column(name = "MANAGER_NAME")
	private String managerName;//高管名字
	@Column(name = "MANAGER_TYPE")
	private String managerType;//高管类型/职务
	@Column(name = "OVERALL_MERIT")
	private String overallMerit;//综合评价
	@Column(name = "REMARK")
	private String remark;//备注说明

	/** default constructor */
	public CompBaseManager() {
	}

	public CompBaseManager(String managerName, String managerType) {
		super();
		this.managerName = managerName;
		this.managerType = managerType;
	}

	/** minimal constructor */
	public CompBaseManager(Long id) {
		this.id = id;
	}

	/** full constructor */
	public CompBaseManager(Long id, String cmCusNo, String psCusNo,
			String managerType, String overallMerit, String remark) {
		this.id = id;
		this.cmCusNo = cmCusNo;
		this.psCusNo = psCusNo;
		this.managerType = managerType;
		this.overallMerit = overallMerit;
		this.remark = remark;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCmCusNo() {
		return this.cmCusNo;
	}

	public void setCmCusNo(String cmCusNo) {
		this.cmCusNo = cmCusNo;
	}

	public String getPsCusNo() {
		return this.psCusNo;
	}

	public void setPsCusNo(String psCusNo) {
		this.psCusNo = psCusNo;
	}

	public String getManagerType() {
		return this.managerType;
	}

	public void setManagerType(String managerType) {
		this.managerType = managerType;
	}

	public String getOverallMerit() {
		return this.overallMerit;
	}

	public void setOverallMerit(String overallMerit) {
		this.overallMerit = overallMerit;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

}