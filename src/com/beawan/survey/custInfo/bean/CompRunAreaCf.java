package com.beawan.survey.custInfo.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 经营场所-厂房
 * CmRunArea entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CM_RUN_AREA_PLANT",schema = "GDTCESYS")
public class CompRunAreaCf extends BaseEntity implements java.io.Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_RUN_AREA_PLANT")
	//@SequenceGenerator(name = "CM_RUN_AREA_PLANT",allocationSize=1,initialValue=1, sequenceName = "CM_RUN_AREA_PLANT_SEQ")
	private Long id;
	@Column(name = "TASK_ID")
	private Long taskId;
	@Column(name = "CUSTOMER_NO")
	private String customerNo;
	@Column(name = "PLANT_NAME")
	private String plantName;//房产名称
	@Column(name = "PLANT_AREA")
	private Double plantArea;//房产面积
	@Column(name = "PLANT_NATURE")
	private String plantNature;//房产结构
	@Column(name = "PLANT_COST")
	private Double plantCost;//房产价值
	@Column(name = "USR_CONDITION")
	private String useCondition;//使用情况
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getTaskId() {
		return taskId;
	}
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getPlantName() {
		return plantName;
	}
	public void setPlantName(String plantName) {
		this.plantName = plantName;
	}
	public Double getPlantArea() {
		return plantArea;
	}
	public void setPlantArea(Double plantArea) {
		this.plantArea = plantArea;
	}
	public String getPlantNature() {
		return plantNature;
	}
	public void setPlantNature(String plantNature) {
		this.plantNature = plantNature;
	}
	public Double getPlantCost() {
		return plantCost;
	}
	public void setPlantCost(Double plantCost) {
		this.plantCost = plantCost;
	}
	public String getUseCondition() {
		return useCondition;
	}
	public void setUseCondition(String useCondition) {
		this.useCondition = useCondition;
	}
	
	
}