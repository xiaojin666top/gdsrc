package com.beawan.survey.custInfo.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 贷前 主要产品情况
 * @author zxh
 * @date 2020/7/9 16:42
 */
@Entity
@Table(name = "CM_RUN_PRODUCT", schema = "GDTCESYS")
public class CompRunProduct extends BaseEntity implements Serializable {

    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_RUN_PRODUCT")
  //  @SequenceGenerator(name = "CM_RUN_PRODUCT",allocationSize=1,initialValue=1, sequenceName = "CM_RUN_PRODUCT_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo; // 流水号
    @Column(name = "CUSTOMER_NO")
    private String customerNo;// 客户号
    @Column(name = "PRO_NAME")
    private String productName;// 产品名称
    @Column(name = "PRO_SPECIFY")
    private String specification; // 产品规格
    @Column(name = "DESIGN_CAPACITY")
    private String designCapacity; // 设计产能
    @Column(name = "ACTUAL_OUTPUT")
    private String actualOutput; // 实际产量
    @Column(name = "PRODUCE_CYCLE")
    private String produceCycle; // 生产周期
    @Column(name = "PRODUCE_WAY")
    private String produceWay; // 生产方式
    @Column(name = "MAIN_USE")
    private String mainUse; // 主要用途

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getMainUse() {
        return mainUse;
    }

    public void setMainUse(String mainUse) {
        this.mainUse = mainUse;
    }

    public String getDesignCapacity() {
        return designCapacity;
    }

    public void setDesignCapacity(String designCapacity) {
        this.designCapacity = designCapacity;
    }

    public String getActualOutput() {
        return actualOutput;
    }

    public void setActualOutput(String actualOutput) {
        this.actualOutput = actualOutput;
    }

    public String getProduceCycle() {
        return produceCycle;
    }

    public void setProduceCycle(String produceCycle) {
        this.produceCycle = produceCycle;
    }

    public String getProduceWay() {
        return produceWay;
    }

    public void setProduceWay(String produceWay) {
        this.produceWay = produceWay;
    }
}
