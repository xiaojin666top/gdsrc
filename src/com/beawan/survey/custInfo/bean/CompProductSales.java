package com.beawan.survey.custInfo.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 主要产品销售情况
 * @author zxh
 * @date 2020/7/10 15:27
 */
@Entity
@Table(name = "CM_RUN_PRODUCT_SALES", schema = "GDTCESYS")
public class CompProductSales extends BaseEntity implements Serializable {
    @Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_RUN_PRODUCT_SALES")
    //@SequenceGenerator(name = "CM_RUN_PRODUCT_SALES",allocationSize=1,initialValue=1, sequenceName = "CM_RUN_PRODUCT_SALES_SEQ")
    private Integer id;
    @Column(name = "SER_NO")
    private String serNo; // 流水号
    @Column(name = "CUSTOMER_NO")
    private String customerNo;// 客户号
    @Column(name = "PERIOD")
    private String period;// 时期/年份
    @Column(name = "PRODUCT_NAME")
    private String productName;// 产品名称
    @Column(name = "YEAR_SALES_VOLUME")
    private String yearSalesVolume;// 销售量（吨、套等）
    @Column(name = "YEAR_SALES")
    private Double yearSales;// 销售额（万元）
    @Column(name = "MAIN_CUST")
    private String mainCustomer;// 主要客户
    @Column(name = "SETTLE_WAY")
    private String settleWay;//结算方式
    @Column(name = "SETTLE_CYCLE")
    private String settleCycle;//结算周期
    @Column(name = "PROPORTION")
    private Double proportion;//比例
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getYearSalesVolume() {
        return yearSalesVolume;
    }

    public void setYearSalesVolume(String yearSalesVolume) {
        this.yearSalesVolume = yearSalesVolume;
    }

    public Double getYearSales() {
        return yearSales;
    }

    public void setYearSales(Double yearSales) {
        this.yearSales = yearSales;
    }

    public String getMainCustomer() {
        return mainCustomer;
    }

    public void setMainCustomer(String mainCustomer) {
        this.mainCustomer = mainCustomer;
    }

    public String getSettleWay() {
        return settleWay;
    }

    public void setSettleWay(String settleWay) {
        this.settleWay = settleWay;
    }

    public String getSettleCycle() {
        return settleCycle;
    }

    public void setSettleCycle(String settleCycle) {
        this.settleCycle = settleCycle;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

	public Double getProportion() {
		return proportion;
	}

	public void setProportion(Double proportion) {
		this.proportion = proportion;
	}
    
    
}
