package com.beawan.survey.custInfo.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * PsBase entity. @author MyEclipse Persistence Tools
 */

@Entity
@Table(name = "PS_BASE", schema = "GDTCESYS")
public class PersonBase extends BaseEntity implements java.io.Serializable {

	@Id
	@Column(name = "CUSTOMER_NO")
	private String customerNo; // 客户号
	@Column(name = "NAME")
	private String name; // 姓名
	@Column(name = "SEX")
	private String sex; // 性别
	@Column(name = "CERT_TYPE")
	private String certType; // 证件类型
	@Column(name = "CERT_CODE")
	private String certCode; // 证件号码
	@Column(name = "COUNTRY")
	private String country; // 国籍
	@Column(name = "NATIONALITY")
	private String nationality; // 民族
	@Column(name = "MANAGER_TYPE")
	private String managerType; // 高管类型
	@Column(name = "BIRTH_DATE")
	private String birthday; // 出生日期
	@Column(name = "BIRTH_PLACE")
	private String birthPlace; // 籍贯
	@Column(name = "LIVING_PLACE")
	private String livingPlace; // 居住地
	@Column(name = "DOMIC_PLACE")
	private String domicPlace; // 户籍所在地
	@Column(name = "FAMILY_ADDR")
	private String familyAddr; // 家庭住址
	@Column(name = "MOBILE_PHONE")
	private String mobilePhone; // 手机号码
	@Column(name = "MARITAL_STATUS")
	private String maritalStatus; // 婚姻状况
	@Column(name = "EDUCATION")
	private String education; // 教育程度
	@Column(name = "WORK_UNIT")
	private String workUnit; // 工作单位
	@Column(name = "POSITION")
	private String position; // 职位
	@Column(name = "OCCUPATION")
	private String occupation; // 职业
	@Column(name = "TITLE")
	private String title; // 职称
	@Column(name = "WORK_EXPERIENCE")
	private String workExperience; // 工作经历
	@Column(name = "ANNUAL_INCOME")
	private Double annualIncome; // 年收入
	@Column(name = "SPOUSE_INFO")
	private String spouseInfo; // 配偶信息
	@Column(name = "CHILDREN_INFO")
	private String childrenInfo; // 子女信息
	@Column(name = "CREDIT_INFO")
	private String creditInfo; // 信用记录
	@Column(name = "FAMILY_ASSETS")
	private String familyAssets; // 家庭资产信息
	@Column(name = "FAMILY_DEBTS")
	private String familyDebts; // 家庭负债信息
	@Column(name = "CREDIT_LEVEL")
	private String creditLevel;// 本行评估即期信用等级
	@Column(name = "BIZ_COOPER_INFO")
	private String bizCooperInfo; // 我行业务合作信息
	@Column(name = "REMARK")
	private String remark; // 备注说明
	@Column(name = "STOCK_CUSTOMER")
	private String stockCustomer;//是否存量客户
	@Column(name = "IS_DRAFT")
	private String isDraft; // 是否为草稿信息（非信贷客户）
	@Transient
	private Long type;//1公司 2人
	
	// Constructors

	/** default constructor */
	public PersonBase() {
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getCertType() {
		return certType;
	}

	public void setCertType(String certType) {
		this.certType = certType;
	}

	public String getCertCode() {
		return certCode;
	}

	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getManagerType() {
		return managerType;
	}

	public void setManagerType(String managerType) {
		this.managerType = managerType;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getBirthPlace() {
		return birthPlace;
	}

	public void setBirthPlace(String birthPlace) {
		this.birthPlace = birthPlace;
	}

	public String getLivingPlace() {
		return livingPlace;
	}

	public void setLivingPlace(String livingPlace) {
		this.livingPlace = livingPlace;
	}
	
	public String getDomicPlace() {
		return domicPlace;
	}

	public void setDomicPlace(String domicPlace) {
		this.domicPlace = domicPlace;
	}

	public String getFamilyAddr() {
		return familyAddr;
	}

	public void setFamilyAddr(String familyAddr) {
		this.familyAddr = familyAddr;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getWorkUnit() {
		return workUnit;
	}

	public void setWorkUnit(String workUnit) {
		this.workUnit = workUnit;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWorkExperience() {
		return workExperience;
	}

	public void setWorkExperience(String workExperience) {
		this.workExperience = workExperience;
	}

	public Double getAnnualIncome() {
		return annualIncome;
	}

	public void setAnnualIncome(Double annualIncome) {
		this.annualIncome = annualIncome;
	}

	public String getSpouseInfo() {
		return spouseInfo;
	}

	public void setSpouseInfo(String spouseInfo) {
		this.spouseInfo = spouseInfo;
	}

	public String getChildrenInfo() {
		return childrenInfo;
	}

	public void setChildrenInfo(String childrenInfo) {
		this.childrenInfo = childrenInfo;
	}

	public String getCreditInfo() {
		return creditInfo;
	}

	public void setCreditInfo(String creditInfo) {
		this.creditInfo = creditInfo;
	}

	public String getFamilyAssets() {
		return familyAssets;
	}

	public void setFamilyAssets(String familyAssets) {
		this.familyAssets = familyAssets;
	}

	public String getFamilyDebts() {
		return familyDebts;
	}

	public void setFamilyDebts(String familyDebts) {
		this.familyDebts = familyDebts;
	}

	public String getBizCooperInfo() {
		return bizCooperInfo;
	}

	public void setBizCooperInfo(String bizCooperInfo) {
		this.bizCooperInfo = bizCooperInfo;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getIsDraft() {
		return isDraft;
	}

	public void setIsDraft(String isDraft) {
		this.isDraft = isDraft;
	}
	
	public String getStockCustomer() {
		return stockCustomer;
	}

	public void setStockCustomer(String stockCustomer) {
		this.stockCustomer = stockCustomer;
	}

	public String getCreditLevel() {
		return creditLevel;
	}

	public void setCreditLevel(String creditLevel) {
		this.creditLevel = creditLevel;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}

}