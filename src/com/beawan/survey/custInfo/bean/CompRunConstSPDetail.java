package com.beawan.survey.custInfo.bean;
// default package



/**
 * CompRunSignedProjsDetail entity. @author MyEclipse Persistence Tools
 */

public class CompRunConstSPDetail  implements java.io.Serializable {


    // Fields    

     private Long id;
     private String customerNo;
     private String projName; //工程名称
     private String investor; //投资方
     private String owner; //业主方
     private Double buildArea; //面积
     private Double contractAmt; // 合同金额
     private String startDate; //开工日期
     private String endDate; //峻工日期
     private Double projProgress; //工程进度
     private Double payableAmt; //应结算工程款
     private Double paidoffAmt; //实结算工程款
     private Double unpaidAmt; //应付未付金额
     private Double needInvestAmt; //还需投入资金
     private Long taskId;


    // Constructors

    /** default constructor */
    public CompRunConstSPDetail() {
    }

    // Property accessors

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerNo() {
        return this.customerNo;
    }
    
    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getProjName() {
        return this.projName;
    }
    
    public void setProjName(String projName) {
        this.projName = projName;
    }

    public String getInvestor() {
        return this.investor;
    }
    
    public void setInvestor(String investor) {
        this.investor = investor;
    }

    public String getOwner() {
        return this.owner;
    }
    
    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Double getBuildArea() {
        return this.buildArea;
    }
    
    public void setBuildArea(Double buildArea) {
        this.buildArea = buildArea;
    }

    public Double getContractAmt() {
        return this.contractAmt;
    }
    
    public void setContractAmt(Double contractAmt) {
        this.contractAmt = contractAmt;
    }

    public String getStartDate() {
        return this.startDate;
    }
    
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return this.endDate;
    }
    
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Double getProjProgress() {
        return this.projProgress;
    }
    
    public void setProjProgress(Double projProgress) {
        this.projProgress = projProgress;
    }

    public Double getPayableAmt() {
        return this.payableAmt;
    }
    
    public void setPayableAmt(Double payableAmt) {
        this.payableAmt = payableAmt;
    }

    public Double getPaidoffAmt() {
        return this.paidoffAmt;
    }
    
    public void setPaidoffAmt(Double paidoffAmt) {
        this.paidoffAmt = paidoffAmt;
    }

    public Double getUnpaidAmt() {
        return this.unpaidAmt;
    }
    
    public void setUnpaidAmt(Double unpaidAmt) {
        this.unpaidAmt = unpaidAmt;
    }

    public Double getNeedInvestAmt() {
        return this.needInvestAmt;
    }
    
    public void setNeedInvestAmt(Double needInvestAmt) {
        this.needInvestAmt = needInvestAmt;
    }

    public Long getTaskId() {
        return this.taskId;
    }
    
    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }
   








}