package com.beawan.survey.custInfo.bean;

/**
 * @Description 主要财务科目分析说明实体类（已弃用）
 */

@Deprecated
public class CompFinanceMainSub implements java.io.Serializable {

	// Fields

	private Long id;
	private Long taskId;
	private String customerNo;
	private String inventoryAnalysis;
	private String receiveAnalysis;
	private String otherReceiveAnalysis;
	private String prepayAnalysis;
	private String faBuildingAnalysis;
	private String faEquipmentAnalysis;
	private String faLandAnalysis;
	private String payAnalysis;
	private String otherPayAnalysis;
	private String preReceiveAnalysis; //预收账款分析

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getInventoryAnalysis() {
		return inventoryAnalysis;
	}

	public void setInventoryAnalysis(String inventoryAnalysis) {
		this.inventoryAnalysis = inventoryAnalysis;
	}

	public String getReceiveAnalysis() {
		return receiveAnalysis;
	}

	public void setReceiveAnalysis(String receiveAnalysis) {
		this.receiveAnalysis = receiveAnalysis;
	}

	public String getOtherReceiveAnalysis() {
		return otherReceiveAnalysis;
	}

	public void setOtherReceiveAnalysis(String otherReceiveAnalysis) {
		this.otherReceiveAnalysis = otherReceiveAnalysis;
	}

	public String getPrepayAnalysis() {
		return prepayAnalysis;
	}

	public void setPrepayAnalysis(String prepayAnalysis) {
		this.prepayAnalysis = prepayAnalysis;
	}

	public String getFaBuildingAnalysis() {
		return faBuildingAnalysis;
	}

	public void setFaBuildingAnalysis(String faBuildingAnalysis) {
		this.faBuildingAnalysis = faBuildingAnalysis;
	}

	public String getFaEquipmentAnalysis() {
		return faEquipmentAnalysis;
	}

	public void setFaEquipmentAnalysis(String faEquipmentAnalysis) {
		this.faEquipmentAnalysis = faEquipmentAnalysis;
	}

	public String getFaLandAnalysis() {
		return faLandAnalysis;
	}

	public void setFaLandAnalysis(String faLandAnalysis) {
		this.faLandAnalysis = faLandAnalysis;
	}

	public String getPayAnalysis() {
		return payAnalysis;
	}

	public void setPayAnalysis(String payAnalysis) {
		this.payAnalysis = payAnalysis;
	}

	public String getOtherPayAnalysis() {
		return otherPayAnalysis;
	}

	public void setOtherPayAnalysis(String otherPayAnalysis) {
		this.otherPayAnalysis = otherPayAnalysis;
	}

	public String getPreReceiveAnalysis() {
		return preReceiveAnalysis;
	}

	public void setPreReceiveAnalysis(String preReceiveAnalysis) {
		this.preReceiveAnalysis = preReceiveAnalysis;
	}

}