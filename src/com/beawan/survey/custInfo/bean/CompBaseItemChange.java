package com.beawan.survey.custInfo.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;
import java.util.Date;

/**
 * 企业变更  记录
 * @author yzj
 */
@Entity
@Table(name = "CM_BASE_ITEM_CHANGE", schema = "GDTCESYS")
public class CompBaseItemChange extends BaseEntity {

	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_BASE_ITEM_CHANGE")
	//@SequenceGenerator(name = "CM_BASE_ITEM_CHANGE",allocationSize=1,initialValue=1, sequenceName = "CM_BASE_ITEM_CHANGE_SEQ")
	private Long id;
	@Column(name = "CUSTOMER_NO")
	private String customerNo;
	@Column(name = "CHANGE_DATE")
	private String changeDate;// 变更时间
	@Column(name = "CHANGE_ITEM")
	private String changeItem;// 变更项目
	@Column(name = "CHANGE_BEFORE")
	private String changeBefore;// 变更前
	@Column(name = "CHANGE_AFTER")
	private String changeAfter;// 变更后

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getChangeDate() {
		return changeDate;
	}

	public void setChangeDate(String changeDate) {
		this.changeDate = changeDate;
	}

	public String getChangeItem() {
		return changeItem;
	}
	public void setChangeItem(String changeItem) {
		this.changeItem = changeItem;
	}
	public String getChangeBefore() {
		return changeBefore;
	}
	public void setChangeBefore(String changeBefore) {
		this.changeBefore = changeBefore;
	}
	public String getChangeAfter() {
		return changeAfter;
	}
	public void setChangeAfter(String changeAfter) {
		this.changeAfter = changeAfter;
	}
	
}
