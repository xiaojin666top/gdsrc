package com.beawan.survey.custInfo.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * CmRunArea entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "CM_RUN_AREA",schema = "GDTCESYS")
public class CompRunArea extends BaseEntity implements java.io.Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_RUN_AREA")
	//@SequenceGenerator(name = "CM_RUN_AREA",allocationSize=1,initialValue=1, sequenceName = "CM_RUN_AREA_SEQ")
	private Long id;
	@Column(name = "TASK_ID")
	private Long taskId;
	@Column(name = "CUSTOMER_NO")
	private String customerNo;
	@Column(name = "AREA_ACREAGE")
	private Double areaAcreage;//建筑面积
	@Column(name = "AREA_ADDRESS")
	private String areaAddress;//场所地址
	@Column(name = "AREA_NAME")
	private String areaName;//经营场所名称
	@Column(name = "AREA_RENT")
	private Double areaRent;//年租金
	@Column(name = "AREA_TYPE")
	private String areaType;//房屋类型
	@Column(name = "FREQUENCY_PAYMENT")
	private String frequencyPayment;//缴纳频次
	
	/** default constructor */
	public CompRunArea() {
	}

	/** minimal constructor */
	public CompRunArea(Long id) {
		this.id = id;
	}

	/** full constructor */
	public CompRunArea(Long id, Double areaAcreage, String areaAddress,
			String areaName, Double areaRent, String areaType,
			String customerNo, Long taskId) {
		this.id = id;
		this.areaAcreage = areaAcreage;
		this.areaAddress = areaAddress;
		this.areaName = areaName;
		this.areaRent = areaRent;
		this.areaType = areaType;
		this.customerNo = customerNo;
		this.taskId = taskId;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getAreaAcreage() {
		return this.areaAcreage;
	}

	public void setAreaAcreage(Double areaAcreage) {
		this.areaAcreage = areaAcreage;
	}

	public String getAreaAddress() {
		return this.areaAddress;
	}

	public void setAreaAddress(String areaAddress) {
		this.areaAddress = areaAddress;
	}

	public String getAreaName() {
		return this.areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public Double getAreaRent() {
		return this.areaRent;
	}

	public void setAreaRent(Double areaRent) {
		this.areaRent = areaRent;
	}

	public String getAreaType() {
		return this.areaType;
	}

	public void setAreaType(String areaType) {
		this.areaType = areaType;
	}

	public String getCustomerNo() {
		return this.customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	

	public String getFrequencyPayment() {
		return frequencyPayment;
	}

	public void setFrequencyPayment(String frequencyPayment) {
		this.frequencyPayment = frequencyPayment;
	}

	@Override
	public String toString() {
		return "CompRunArea{" +
				"id=" + id +
				", taskId=" + taskId +
				", customerNo='" + customerNo + '\'' +
				", areaAcreage=" + areaAcreage +
				", areaAddress='" + areaAddress + '\'' +
				", areaName='" + areaName + '\'' +
				", areaRent=" + areaRent +
				", areaType='" + areaType + '\'' +
				'}';
	}
}