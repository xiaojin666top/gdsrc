package com.beawan.survey.custInfo.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;

/**
 * 行内风险预警信号类
 * @author yuzhejia
 *
 */

@Entity
@Table(name = "CM_WM_WARNING_SIGNAL",schema = "GDTCESYS")
public class CompWmWarnSignal extends BaseEntity {
	
	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CM_WM_WARNING_SIGNAL_SEQ")
	//@SequenceGenerator(name = "CM_WM_WARNING_SIGNAL_SEQ", allocationSize = 1, initialValue = 1, sequenceName = "CM_WM_WARNING_SIGNAL_SEQ")
	private Long id;
    @Column(name = "TASK_ID")
    private Long taskId;// 贷前任务号

    @Column(name = "INPUTTIME")
	private String inputTime;//发生时间
    @Column(name = "WARN_LEVEL")
	private String warnLevel;//预警信号等级
    @Column(name = "ALARMINFO_DESC")
	private String alarminfoDesc;//风险预警信号描述
    @Column(name = "FALSE_CAUSE")
	private String falseCause;//造成原由
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getTaskId() {
		return taskId;
	}
	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}
	public String getInputTime() {
		return inputTime;
	}
	public void setInputTime(String inputTime) {
		this.inputTime = inputTime;
	}
	public String getWarnLevel() {
		return warnLevel;
	}
	public void setWarnLevel(String warnLevel) {
		this.warnLevel = warnLevel;
	}
	public String getAlarminfoDesc() {
		return alarminfoDesc;
	}
	public void setAlarminfoDesc(String alarminfoDesc) {
		this.alarminfoDesc = alarminfoDesc;
	}
	public String getFalseCause() {
		return falseCause;
	}
	public void setFalseCause(String falseCause) {
		this.falseCause = falseCause;
	}
	
	

}
