package com.beawan.survey.custInfo.bean;

/**
 * 企业表格那个
 * @author 66666666666666666666
 *
 */
public class CompChangeRecord {
    private Long id;
    private String customerNo;
    private String changeItem; //变更科目
    private String contentBefore;//变更前数据
    private String contentAfter;//变更后数据
    private String changeTime;//变更时间

    private String createTime;//变更时间
    private int status;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getChangeItem() {
		return changeItem;
	}
	public void setChangeItem(String changeItem) {
		this.changeItem = changeItem;
	}
	public String getContentBefore() {
		return contentBefore;
	}
	public void setContentBefore(String contentBefore) {
		this.contentBefore = contentBefore;
	}
	public String getContentAfter() {
		return contentAfter;
	}
	public void setContentAfter(String contentAfter) {
		this.contentAfter = contentAfter;
	}
	public String getChangeTime() {
		return changeTime;
	}
	public void setChangeTime(String changeTime) {
		this.changeTime = changeTime;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
    
    
}
