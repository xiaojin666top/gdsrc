package com.beawan.survey.custInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.custInfo.bean.CompWmWarnSignal;

/**
 * @author yzj
 */
public interface CompWmWarnSignalService extends BaseService<CompWmWarnSignal> {
}
