package com.beawan.survey.custInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.custInfo.entity.CompRawMaterial;

/**
 * @author yzj
 */
public interface CompRawMaterialService extends BaseService<CompRawMaterial> {
}
