package com.beawan.survey.custInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.custInfo.bean.CompFinanceTaxSale;

/**
 * @author yzj
 */
public interface CompFinanceTaxSaleService extends BaseService<CompFinanceTaxSale> {
	/**
	 * 获取对应财报销售收入值
	 * @throws Exception 
	 * */
	void getCompFinanceTaxSale(CompFinanceTaxSale compFinanceTaxSale,String customerNo) throws Exception;
	/**
	 * 获取偏差率
	 *
	 * */
	String countDeviation(Double fnValue, Double taxValue);
}
