package com.beawan.survey.custInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.custInfo.bean.CompRunAnalysis;

/**
 * @author yzj
 */
public interface compRunAnalysisService extends BaseService<CompRunAnalysis> {

    /**
     * 同步客户行业分析数据
     * 1 行业概况  2行业风险
     * @param serNo
     * @throws Exception
     */
    CompRunAnalysis syncInduAnalysis(String serNo) throws Exception;
}
