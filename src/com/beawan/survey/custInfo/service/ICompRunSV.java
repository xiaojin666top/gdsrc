package com.beawan.survey.custInfo.service;

import java.util.List;

import com.beawan.survey.custInfo.bean.CompRunConst;
import com.beawan.survey.custInfo.bean.CompRunConstSPDetail;
import com.beawan.survey.custInfo.bean.CompRunConstSPStruc;
import com.beawan.survey.custInfo.bean.CompRunCustomer;
import com.beawan.survey.custInfo.bean.CompRunEsta;
import com.beawan.survey.custInfo.bean.CompRunEstaDevedProj;
import com.beawan.survey.custInfo.bean.CompRunEstaDevingProj;
import com.beawan.survey.custInfo.bean.CompRunEstaLand;
import com.beawan.survey.custInfo.bean.CompRunFixed;
import com.beawan.survey.custInfo.bean.CompRunGeneral;
import com.beawan.survey.custInfo.bean.CompRunPower;
import com.beawan.survey.custInfo.bean.CompRun;
import com.beawan.survey.custInfo.bean.CompRunArea;
import com.beawan.survey.custInfo.bean.CompRunAreaCf;
import com.beawan.survey.custInfo.bean.CompRunAreaTd;
import com.beawan.survey.custInfo.bean.CompRunPrdSale;
import com.beawan.survey.custInfo.bean.CompRunProduce;
import com.beawan.survey.custInfo.bean.CompRunSupply;

public interface ICompRunSV {

	/**
	 * @Description (根据任务号查询客户经营状况)
	 * @param taskId 任务号
	 * @return
	 */
	public CompRun queryCompRunByTIdAndNo(Long taskId, String customerNo);

	/**
	 * @Description (保存企业经营状况信息)
	 * @param compRun
	 *            企业经营状况实体
	 */
	public void saveCompRun(CompRun compRun) throws Exception;

	/**
	 * @Description (删除企业经营状况信息)
	 * @param compRun
	 *            企业经营状况实体
	 */
	public void deleteCompRun(CompRun compRun) throws Exception;

	/**
	 * @Description (根据客户号查询企业生产情况)
	 * @param taskId
	 *            任务号
	 * @param custNo
	 *            客户号
	 * @return
	 */
	public CompRunProduce queryRunProduceByTIdAndNo(Long taskId, String custNo)
			throws Exception;

	/**
	 * @Description (保存企业生产情况信息)
	 * @param compRunProduce
	 *            企业生产情况实体
	 */
	public void saveRunProduce(CompRunProduce compRunProduce)
			throws Exception;

	/**
	 * @Description (删除企业生产情况信息)
	 * @param compRunProduce
	 *            企业生产情况信息实体
	 */
	public void deleteRunProduce(CompRunProduce compRunProduce)
			throws Exception;

	/**
	 * @Description (根据任务编号查询水电纳税信息)
	 * @param taskId
	 *            任务编号
	 * @return
	 */
	public List<CompRunPower> queryRunPowerByTIdAndNo(Long taskId, String custNo)
			throws Exception;
	
	/**
	 * @Description (根据唯一标识查询水电纳税信息)
	 * @param id
	 *            唯一标识
	 * @return
	 */
	public CompRunPower queryRunPowerById(Long id)
			throws Exception;

	/**
	 * @Description (保存水电纳税信息)
	 * @param power
	 *            水电纳税实体
	 */
	public void saveRunPower(CompRunPower power) throws Exception;
	
	/**
	 * @Description (保存水电纳税信息列表)
	 * @param power
	 *            水电纳税信息列表
	 */
	public void saveRunPowers(Long taskId, String custNo, String jsonArray) throws Exception;

	/**
	 * @Description (删除水电纳税记录)
	 * @param power
	 *            水电纳税实体
	 */
	public void deleteRunPower(CompRunPower power) throws Exception;

	/**
	 * @Description (根据任务编号查询经营场所信息)
	 * @param taskId
	 *            任务编号
	 * @return
	 */
	public List<CompRunArea> queryRunAreaByTIdAndNo(Long taskId, String custNo)
			throws Exception;
	
	/**
	 * @Description (根据任务编号查询经营场所（土地）信息)
	 * @param taskId
	 *            任务编号
	 * @return
	 */
	public List<CompRunAreaTd> queryRunAreaLandByTIdAndNo(Long taskId, String custNo)
			throws Exception;
	
	/**
	 * @Description (根据任务编号查询经营场所（厂房）信息)
	 * @param taskId
	 *            任务编号
	 * @return
	 */
	public List<CompRunAreaCf> queryRunAreaPlantByTIdAndNo(Long taskId, String custNo)
			throws Exception;
	
	/**
	 * @Description (根据唯一标识查询经营场所信息)
	 * @param id 唯一标识
	 * @return
	 */
	public CompRunArea queryRunAreaById(Long id)
			throws Exception;
	
	/**
	 * @Description (根据唯一标识查询经营场所（土地）信息)
	 * @param id 唯一标识
	 * @return
	 */
	public CompRunAreaTd queryRunAreaLandById(Long id)
			throws Exception;
	
	/**
	 * @Description (根据唯一标识查询经营场所（厂房）信息)
	 * @param id 唯一标识
	 * @return
	 */
	public CompRunAreaCf queryRunAreaPlantById(Long id)
			throws Exception;

	/**
	 * @Description (保存企业经营场所信息)
	 * @param runArea
	 *            经营场所实体
	 */
	public void saveRunArea(CompRunArea runArea) throws Exception;
	
	/**
	 * @Description (保存企业经营场所（土地）信息)
	 * @param runArea
	 * 经营场所实体
	 */
	public void saveRunAreaLand(CompRunAreaTd runArea) throws Exception;
	
	/**
	 * @Description (保存企业经营场所（厂房）信息)
	 * @param runArea
	 * 经营场所实体
	 */
	public void saveRunAreaPlant(CompRunAreaCf runArea) throws Exception;
	
	/**
	 * @Description (保存企业经营场所信息列表，先删除后插入)
	 * @param taskId 任务号
	 * @param jsonArray 企业经营场所信息列表
	 * @throws Exception
	 */
	public void saveRunAreas(Long taskId, String custNo, String jsonArray) throws Exception;

	/**
	 * @Description (删除经营场所记录)
	 * @param runArea
	 *            经营场所实体
	 */
	public void deleteRunArea(CompRunArea runArea) throws Exception;

	/**
	 * @Description (根据任务编号查询上游供应商信息)
	 * @param taskId 任务编号
	 * @return
	 */
	public List<CompRunSupply> queryRunSupplyByTIdAndNo(Long taskId, String customerNo);
	
	/**
	 * @Description (根据唯一标识查询上游供应商信息)
	 * @param id 唯一标识
	 * @return
	 */
	public CompRunSupply queryRunSupplyById(Long id)
			throws Exception;

	/**
	 * @Description (保存上游供应商信息)
	 * @param supply
	 *            上游供应商实体
	 */
	public void saveRunSupply(CompRunSupply supply) throws Exception;
	
	/**
	 * @Description (保存上游供应商信息列表)
	 * @param jsonArray
	 *            上游供应商信息列表
	 */
	public void saveRunSupplys(Long taskId, String custNo, String jsonArray) throws Exception;

	/**
	 * @Description (删除上游供应商记录)
	 * @param supply
	 *            供应商实体
	 */
	public void deleteRunSupply(CompRunSupply supply)
			throws Exception;

	/**
	 * @Description (根据任务编号查询下游客户信息)
	 * @param taskId
	 *            任务编号
	 * @return
	 */
	public List<CompRunCustomer> queryRunCustomerByTIdAndNo(Long taskId, String custNo)
			throws Exception;
	
	/**
	 * @Description (根据唯一标识查询下游客户信息)
	 * @param id 唯一标识
	 * @return
	 */
	public CompRunCustomer queryRunCustomerById(Long id)
			throws Exception;

	/**
	 * @Description (保存下游客户信息)
	 * @param customer
	 *            下游客户实体
	 */
	public void saveRunCustomer(CompRunCustomer customer)
			throws Exception;
	
	/**
	 * @Description (保存下游客户信息列表)
	 * @param jsonArray
	 *            下游客户信息列表
	 */
	public void saveRunCustomers(Long taskId, String custNo, String jsonArray)
			throws Exception;

	/**
	 * @Description (删除下游客户记录)
	 * @param customer
	 *            下游客户实体
	 */
	public void deleteRunCustomer(CompRunCustomer customer)
			throws Exception;

	/**
	 * @Description (根据任务编号查询产品销售信息)
	 * @param taskId
	 *            任务编号
	 * @return
	 */
	public List<CompRunPrdSale> queryRunPrdSaleByTIdAndNo(Long taskId, String custNo)
			throws Exception;
	
	/**
	 * @Description (根据唯一标识查询产品销售信息)
	 * @param id
	 *            唯一标识
	 * @return
	 */
	public CompRunPrdSale queryRunPrdSaleById(Long id)
			throws Exception;

	/**
	 * @Description (保存产品销售信息)
	 * @param compRunPrdSale
	 *            产品销售信息实体
	 */
	public void saveRunPrdSale(CompRunPrdSale compRunPrdSale)
			throws Exception;
	
	/**
	 * @Description (保存产品销售信息列表)
	 * @param jsonArray
	 *            产品销售信息列表
	 */
	public void saveRunPrdSales(Long taskId, String custNo, String jsonArray)
			throws Exception;

	/**
	 * @Description (删除产品销售信息)
	 * @param compRunPrdSale
	 *            产品销售信息实体
	 */
	public void deleteRunPrdSale(CompRunPrdSale compRunPrdSale)
			throws Exception;

	/**
	 * @Description (根据任务号查询客户经营状况，适用于建筑业流动资金贷款)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public CompRunConst queryRunConstByTIdAndNo(Long taskId, String custNo)
			throws Exception;

	/**
	 * @Description (保存企业经营状况信息，适用于建筑业流动资金贷款)
	 * @param compRunConst
	 *            企业经营状况实体
	 */
	public void saveRunConst(CompRunConst compRunConst)
			throws Exception;

	/**
	 * @Description (删除企业经营状况信息，适用于建筑业流动资金贷款)
	 * @param compRunConst
	 *            企业经营状况实体
	 */
	public void deleteRunConst(CompRunConst compRunConst)
			throws Exception;

	/**
	 * @Description (根据任务号查询企业在手已签施工工程明细，适用于建筑业流动资金贷款)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public List<CompRunConstSPDetail> queryRunConstSPDetailByTIdAndNo(Long taskId, String custNo)
			throws Exception;
	
	/**
	 * @Description (根据唯一标识查询企业在手已签施工工程明细，适用于建筑业流动资金贷款)
	 * @param id
	 *            唯一标识
	 * @return
	 */
	public CompRunConstSPDetail queryRunConstSPDetailById(Long id)
			throws Exception;

	/**
	 * @Description (保存企业在手已签施工工程明细，适用于建筑业流动资金贷款)
	 * @param compRunConstSPDetail
	 *            企业在手已签施工工程明细实体
	 */
	public void saveRunConstSPDetail(CompRunConstSPDetail compRunConstSPDetail)
			throws Exception;
	
	/**
	 * @Description (保存企业在手已签施工工程明细列表，适用于建筑业流动资金贷款)
	 * @param jsonArray
	 *            企业在手已签施工工程明细列表
	 */
	public void saveRunConstSPDetails(Long taskId, String custNo, String jsonArray)
			throws Exception;

	/**
	 * @Description (删除企业在手已签施工工程明细，适用于建筑业流动资金贷款)
	 * @param compRunConstSPDetail
	 *            企业在手已签施工工程明细实体
	 */
	public void deleteRunConstSPDetail(CompRunConstSPDetail compRunConstSPDetail)
			throws Exception;

	/**
	 * @Description (根据任务号查询企业在手已签施工工程结构，适用于建筑业流动资金贷款)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public List<CompRunConstSPStruc> queryRunConstSPStrucByTIdAndNo(Long taskId, String custNo)
			throws Exception;
	
	/**
	 * @Description (根据主键查询企业在手已签施工工程结构，适用于建筑业流动资金贷款)
	 * @param id
	 *            主键
	 * @return
	 */
	public CompRunConstSPStruc queryRunConstSPStrucById(Long id)
			throws Exception;

	/**
	 * @Description (保存企业在手已签施工工程结构，适用于建筑业流动资金贷款)
	 * @param compRunConstSPStruc
	 *            企业在手已签施工工程结构实体
	 */
	public void saveRunConstSPStruc(CompRunConstSPStruc compRunConstSPStruc)
			throws Exception;
	
	/**
	 * @Description (保存企业在手已签施工工程结构列表，适用于建筑业流动资金贷款)
	 * @param jsonArray
	 *            企业在手已签施工工程结构列表
	 */
	public void saveRunConstSPStrucs(Long taskId, String custNo, String jsonArray) throws Exception;

	/**
	 * @Description (删除企业在手已签施工工程结构，适用于建筑业流动资金贷款)
	 * @param compRunConstSPStruc
	 *            企业在手已签施工工程结构实体
	 */
	public void deleteRunConstSPStruc(CompRunConstSPStruc compRunConstSPStruc)
			throws Exception;

	/**
	 * @Description (根据任务号查询客户经营状况，适用于固定资产贷款)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public CompRunFixed queryRunFixedByTIdAndNo(Long taskId, String custNo)
			throws Exception;

	/**
	 * @Description (保存企业经营状况信息，适用于固定资产贷款)
	 * @param compRunFixed
	 *            企业经营状况实体
	 */
	public CompRunFixed saveRunFixed(CompRunFixed compRunFixed)
			throws Exception;
	
	/**
	 * @Description (保存企业经营状况信息，适用于固定资产贷款)
	 * @param jsondata 企业经营状况信息
	 */
	public CompRunFixed saveRunFixed(String jsondata) throws Exception;

	/**
	 * @Description (删除企业经营状况信息，适用于固定资产贷款)
	 * @param compRunFixed
	 *            企业经营状况实体
	 */
	public void deleteRunFixed(CompRunFixed compRunFixed)
			throws Exception;

	/**
	 * @Description (根据任务号查询客户经营状况，适用于房地产贷款)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public CompRunEsta queryRunEstaByTIdAndNo(Long taskId, String custNo)
			throws Exception;

	/**
	 * @Description (保存企业经营状况信息，适用于房地产贷款)
	 * @param compRunEsta
	 *            企业经营状况实体
	 */
	public void saveRunEsta(CompRunEsta compRunEsta) throws Exception;

	/**
	 * @Description (删除企业经营状况信息，适用于房地产贷款)
	 * @param compRunEsta
	 *            企业经营状况实体
	 */
	public void deleteRunEsta(CompRunEsta compRunEsta)
			throws Exception;

	/**
	 * @Description (根据任务号查询房地产土地储备情况)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public List<CompRunEstaLand> queryRunEstaLandByTIdAndNo(Long taskId, String custNo)
			throws Exception;
	
	/**
	 * @Description (根据主键查询房地产土地储备情况)
	 * @param id
	 *            主键
	 * @return
	 */
	public CompRunEstaLand queryRunEstaLandById(Long id)
			throws Exception;

	/**
	 * @Description (保存房地产土地储备情况)
	 * @param compRunEstaLand
	 *            房地产土地储备情况实体
	 */
	public void saveRunEstaLand(CompRunEstaLand compRunEstaLand)
			throws Exception;
	
	/**
	 * @Description (保存房地产土地储备情况列表)
	 * @param jsonArray
	 *            房地产土地储备情况列表
	 */
	public void saveRunEstaLands(Long taskId, String custNo, String jsonArray) throws Exception;

	/**
	 * @Description (删除房地产土地储备情况)
	 * @param compRunEstaLand
	 *            房地产土地储备情况实体
	 */
	public void deleteRunEstaLand(CompRunEstaLand compRunEstaLand)
			throws Exception;

	/**
	 * @Description (根据任务号查询房地产正在开发项目情况)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public List<CompRunEstaDevingProj> queryRunEstaDevingProjByTIdAndNo(
			Long taskId, String custNo) throws Exception;
	
	/**
	 * @Description (根据主键查询房地产正在开发项目情况)
	 * @param id
	 *            主键
	 * @return
	 */
	public CompRunEstaDevingProj queryRunEstaDevingProjById(Long id)
			throws Exception;

	/**
	 * @Description (保存房地产正在开发项目情况)
	 * @param compRunEstaDevingProj
	 *            房地产正在开发项目情况实体
	 */
	public void saveRunEstaDevingProj(
			CompRunEstaDevingProj compRunEstaDevingProj)
			throws Exception;
	
	/**
	 * @Description (保存房地产正在开发项目情况列表)
	 * @param jsonArray
	 *            房地产正在开发项目情况列表
	 */
	public void saveRunEstaDevingProjs(Long taskId, String custNo, String jsonArray) throws Exception;

	/**
	 * @Description (删除房地产正在开发项目情况)
	 * @param compRunEstaDevingProj
	 *            房地产正在开发项目情况实体
	 */
	public void deleteRunEstaDevingProj(
			CompRunEstaDevingProj compRunEstaDevingProj)
			throws Exception;

	/**
	 * @Description (根据任务号查询房地产已开发项目情况)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public List<CompRunEstaDevedProj> queryRunEstaDevedProjByTIdAndNo(Long taskId, String custNo)
			throws Exception;
	
	/**
	 * @Description (根据主键查询房地产已开发项目情况)
	 * @param id
	 *            主键
	 * @return
	 */
	public CompRunEstaDevedProj queryRunEstaDevedProjById(Long id)
			throws Exception;

	/**
	 * @Description (保存房地产已开发项目情况)
	 * @param compRunEstaDevedProj
	 *            房地产已开发项目情况实体
	 */
	public void saveRunEstaDevedProj(CompRunEstaDevedProj compRunEstaDevedProj)
			throws Exception;
	
	/**
	 * @Description (保存房地产已开发项目情况列表)
	 * @param compRunEstaDevedProj
	 *            房地产已开发项目情况列表
	 */
	public void saveRunEstaDevedProjs(Long taskId, String custNo, String jsonArray) throws Exception;

	/**
	 * @Description (删除房地产已开发项目情况)
	 * @param compRunEstaDevedProj
	 *            房地产已开发项目情况实体
	 */
	public void deleteRunEstaDevedProj(
			CompRunEstaDevedProj compRunEstaDevingProj)
			throws Exception;
	
	/**
	 * @Description (根据任务号查询客户经营状况，适用于通用模板)
	 * @param taskId 任务号
	 * @param custNo 客户号
	 * @return
	 */
	public CompRunGeneral queryRunGeneralByTIdAndNo(Long taskId, String customerNo)
			throws Exception;

	/**
	 * @Description (保存企业经营状况信息，适用于通用模板)
	 * @param compRunGeneral 企业经营状况实体
	 */
	public CompRunGeneral saveRunGeneral(CompRunGeneral compRunGeneral)
			throws Exception;
	
	/**
	 * @Description (保存企业经营状况信息，适用于通用模板)
	 * @param jsondata 企业经营状况信息
	 */
	public CompRunGeneral saveRunGeneral(String jsondata) throws Exception;

	/**
	 * @Description (删除企业经营状况信息，适用于通用模板)
	 * @param compRunGeneral 企业经营状况实体
	 */
	public void deleteRunGeneral(CompRunGeneral compRunGeneral)
			throws Exception;
}
