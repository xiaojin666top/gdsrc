package com.beawan.survey.custInfo.service;

import java.util.List;

import com.beawan.core.BaseService;
import com.beawan.survey.custInfo.dto.CompJudgmentDto;
import com.beawan.survey.custInfo.entity.CompJudgment;
import com.beawan.core.Pagination;

/**
 * @author yzj
 */
public interface CompJudgmentService extends BaseService<CompJudgment> {

	/**
	 * 分页获取裁判文书列表，根据发布日期倒叙		
	 * @param custNo
	 * @param caseType			裁判文书类型
	 * @param caseReason		案由
	 * @param page
	 * @param pageSize
	 * @return
	 */
	Pagination<CompJudgmentDto> getJudgmentPager(String custNo,String caseType, String caseReason, 
			String loanTime,Integer page, Integer pageSize)throws Exception;


	/**
	 * 获取   截止 贷前调查开始的 近半年 中， 企业作为被告的最近3条数据
	 * @param loanTime		yyyy-MM-dd
	 * @param custNo
	 * @return
	 * @throws Exception
	 */
	List<CompJudgmentDto> getNewestJudgeList(String loanTime, String custNo, String custName) throws Exception;
	
	/**
	 * 获取企业司法信息统计分析
	 * @param loanTime
	 * @param custNo
	 * @param custName
	 * @return
	 * @throws Exception
	 */
	String getJudgSimAnaly(String loanTime, String custNo, String custName) throws Exception;
}
