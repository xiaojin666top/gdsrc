package com.beawan.survey.custInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.custInfo.dto.SalesStatDto;
import com.beawan.survey.custInfo.entity.CompSalesStat;

import java.util.List;

/**
 * @author yzj
 */
public interface CompSalesStatService extends BaseService<CompSalesStat> {

    /**
     * 保存 销售环节 销售统计数据(商贸类)
     * @param serNo 任务流水号
     * @param jsonData 统计数据(json格式)
     * @param userNo 客户经理编号
     */
    public void saveStatData(String serNo, String jsonData, String userNo);

    /**
     * 获取 销售统计数据(商贸类)
     * @param serNo 任务流水号
     * @return
     */
    public List<SalesStatDto> getSalesStatList(String serNo);

}
