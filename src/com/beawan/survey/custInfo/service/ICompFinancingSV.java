package com.beawan.survey.custInfo.service;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.survey.custInfo.bean.CompFinancing;
import com.beawan.survey.custInfo.bean.CompFinancingBank;
import com.beawan.survey.custInfo.bean.CompFinancingExtGuara;

public interface ICompFinancingSV {
	
	/**
	 * @Description (保存企业基本融资信息)
	 * @param data
	 *            企业基本融资信息实体
	 */
	public void saveCompFinancing(CompFinancing data) throws Exception;

	/**
	 * @Description (刪除企业基本融资信息)
	 * @param data
	 *            企业基本融资信息实体
	 */
	public void deleteCompFinancing(CompFinancing data) throws Exception;
	
	
	/**
	 * @Description (通过客户号查找企业基本融资信息)
	 * @param customerNo
	 * @return
	 * @throws Exception
	 */
	public CompFinancing findCompFinancingByTIdAndNo(Long taskId, String  customerNo);
	
	
	/**
	 * @Description (通过id查找企业基本融资信息)
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public CompFinancing findCompFinancingById(Long id) throws Exception;
	
	
	/**
	 * @Description (保存企业对外担保信息)
	 * @param data 企业对外担保信息
	 */
	public void saveCompFinaExtGuara(CompFinancingExtGuara data) throws Exception;
	
	/**
	 * @Description (保存企业对外担保信息列表)
	 * @param taskId 任务号
	 * @param customerNo 客户号
	 * @param jsonArray 企业对外担保信息列表
	 * @throws Exception
	 */
	public void saveCompFinaExtGuaras(Long taskId, String customerNo, String jsonArray) throws Exception;

	/**
	 * @Description (刪除企业对外担保信息)
	 * @param data 企业对外担保信息
	 */
	public void deleteCompFinaExtGuara(CompFinancingExtGuara data) throws Exception;
	
	/**
	 * @Description (通过客户号查找对外担保信息)
	 * @param taskId
	 * @return
	 */
	public List<CompFinancingExtGuara>  findCompFinaExtGuaraByTIdAndNo(Long taskId, String customerNo);
	
	/**
	 * @Description (通过id查找企业对外担保信息)
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public CompFinancingExtGuara findCompFinaExtGuaraById(Long id) throws Exception;
	
	/**
	 * @Description (保存企业银行融资信息)
	 * @param data 企业银行融资信息
	 */
	public void saveCompFinaBank(CompFinancingBank data) throws Exception;
	
	/**
	 * @Description (保存企业银行融资信息列表，先删除后插入)
	 * @param taskId 任务号
	 * @param customerNo 客户号
	 * @param jsonArray 企业银行融资信息列表
	 */
	public void saveCompFinaBanks(Long taskId, String customerNo,
			String jsonArray) throws Exception;

	/**
	 * @Description (刪除企业银行融资信息)
	 * @param data 企业银行融资信息
	 */
	public void deleteCompFinaBank(CompFinancingBank data) throws Exception;
	
	/**
	 * @Description (通过客户号查找企业银行融资信息)
	 * @param TaskId
	 * @param customerNo 客户号
	 * @return
	 * @throws Exception
	 */
	public List<CompFinancingBank>  findCompFinaBankByTIdAndNo(Long taskId,
			String customerNo) throws Exception;
	
	/**
	 * @Description (通过客户号查找企业银行融资信息)
	 * @param taskId
	 * @param customerNo 客户号
	 * @param flag 查询标识 M：我行，O：他行
	 * @return
	 * @throws Exception
	 */
	public List<CompFinancingBank>  findCompFinaBankByTIdAndNo(Long taskId,
			String customerNo, String flag);
	
	/**
	 * @Description (通过id查找企业银行融资信息)
	 * @param id
	 * @return
	 */
	public CompFinancingBank findCompFinaBankById(Long id);
	
	/**
	 * @Description (获取客户在我行授信业务情况(CMIS)并保存到本地)
	 * @param customerNo
	 * @return
	 * @throws DataAccessException
	 */
	public List<CompFinancingBank> syncCompFinaBankFromCmis(Long taskId,String  customerNo) throws Exception;
}
