package com.beawan.survey.custInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.custInfo.bean.CompRunSupply;

/**
 * @author yzj
 */
public interface CompRunSupplyService extends BaseService<CompRunSupply> {
}
