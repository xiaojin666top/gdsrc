package com.beawan.survey.custInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.custInfo.bean.CompPower;

/**
 * @author yzj
 */
public interface CompPowerService extends BaseService<CompPower> {
}
