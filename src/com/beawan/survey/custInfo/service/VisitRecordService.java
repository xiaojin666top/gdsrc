package com.beawan.survey.custInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.custInfo.entity.VisitRecord;

/**
 * @author yzj
 */
public interface VisitRecordService extends BaseService<VisitRecord> {
	
	VisitRecord getBySerNoAndType(String serNo, String type);
}
