package com.beawan.survey.custInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.custInfo.dao.CompPowerDao;
import com.beawan.survey.custInfo.service.CompPowerService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.custInfo.bean.CompPower;

/**
 * @author yzj
 */
@Service("compPowerService")
public class CompPowerServiceImpl extends BaseServiceImpl<CompPower> implements CompPowerService {
    /**
    * 注入DAO
    */
    @Resource(name = "compPowerDao")
    public void setDao(BaseDao<CompPower> dao) {
        super.setDao(dao);
    }
    @Resource
    public CompPowerDao compPowerDao;
}
