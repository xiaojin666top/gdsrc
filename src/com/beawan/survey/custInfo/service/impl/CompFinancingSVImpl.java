package com.beawan.survey.custInfo.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.beawan.customer.bean.CusBase;
import com.beawan.customer.dao.impl.CusBaseDAOImpl;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.beawan.base.service.ITableSubjCodeSV;
import com.beawan.common.Constants;
import com.beawan.survey.custInfo.bean.CompFinancing;
import com.beawan.survey.custInfo.bean.CompFinancingBank;
import com.beawan.survey.custInfo.bean.CompFinancingExtGuara;
import com.beawan.survey.custInfo.dao.ICompFinancingBankDAO;
import com.beawan.survey.custInfo.dao.ICompFinancingDAO;
import com.beawan.survey.custInfo.dao.ICompFinancingExtGuaraDAO;
import com.beawan.survey.custInfo.service.ICompFinancingSV;
import com.beawan.task.dao.ITaskDAO;
import com.platform.util.DateUtil;
import com.platform.util.JdbcUtil;

@Service("compFinancingSV")
public class CompFinancingSVImpl implements ICompFinancingSV{
	

	@Resource
	private ICompFinancingDAO compFinancingDAO;
	
	@Resource
	private ICompFinancingExtGuaraDAO compFinancingExtGuaraDAO;
	
	@Resource
	private ICompFinancingBankDAO compFinancingBankDAO;

	@Resource
	private CusBaseDAOImpl cusBaseDAO;
	@Resource
	private ITaskDAO taskDAO;
	
	@Resource
	private ITableSubjCodeSV tableSubjCodeSV;
	
	@Override
	public void saveCompFinancing(CompFinancing data) throws Exception{
		compFinancingDAO.saveOrUpdate(data);
	}
	
	@Override
	public void deleteCompFinancing(CompFinancing data) throws Exception{
		compFinancingDAO.deleteEntity(data);
	}
	
	@Override
	public CompFinancing findCompFinancingByTIdAndNo(Long taskId,String  customerNo){
		return compFinancingDAO.queryByTaskIdAndCustNo(taskId, customerNo);
	}
	
	@Override
	public CompFinancing findCompFinancingById(Long id) throws Exception{
		return compFinancingDAO.selectByPrimaryKey(id);
	}

	@Override
	public void saveCompFinaExtGuara(CompFinancingExtGuara data) throws Exception {
		compFinancingExtGuaraDAO.saveOrUpdate(data);
	}
	
	@Override
	public void saveCompFinaExtGuaras(Long taskId, String customerNo, String jsonArray) throws Exception {
		
		List<CompFinancingExtGuara>  financingCoops = compFinancingExtGuaraDAO.queryByTaskIdAndCustNo(taskId, customerNo);
		for (CompFinancingExtGuara compFinancingCoop : financingCoops) {
			compFinancingExtGuaraDAO.deleteEntity(compFinancingCoop);
		}
		JSONArray array = JSONArray.fromObject(jsonArray);
		if (array != null && array.size() > 0) {
			for (int i = 0; i < array.size(); i++) {
				JSONObject dataStr = JSONObject.fromObject(array.get(i));
				CompFinancingExtGuara data = (CompFinancingExtGuara) JSONObject.toBean(dataStr,CompFinancingExtGuara.class);  
				compFinancingExtGuaraDAO.saveOrUpdate(data);
			}
		}
	}

	@Override
	public void deleteCompFinaExtGuara(CompFinancingExtGuara data) throws Exception {
		compFinancingExtGuaraDAO.deleteEntity(data);
	}

	@Override
	public List<CompFinancingExtGuara> findCompFinaExtGuaraByTIdAndNo(Long taskId,String customerNo){
		return compFinancingExtGuaraDAO.queryByTaskIdAndCustNo(taskId,customerNo);
	}

	@Override
	public CompFinancingExtGuara findCompFinaExtGuaraById(Long id)
			throws Exception {
		return compFinancingExtGuaraDAO.selectByPrimaryKey(id);
	}
	
	
	@Override
	public void saveCompFinaBank(CompFinancingBank data) throws Exception {
		compFinancingBankDAO.saveOrUpdate(data);
	}
	
	@Override
	public void saveCompFinaBanks(Long taskId, String customerNo, String jsonArray) throws Exception {
		
		List<CompFinancingBank>  banks = this.findCompFinaBankByTIdAndNo(taskId, customerNo);
		for (CompFinancingBank bank : banks) {
			this.deleteCompFinaBank(bank);
		}
		
		JSONArray array = JSONArray.fromObject(jsonArray);
		if (array != null && array.size() > 0) {
			for (int i = 0; i < array.size(); i++) {
				JSONObject dataStr = JSONObject.fromObject(array.get(i));
				CompFinancingBank data = (CompFinancingBank) JSONObject.toBean(dataStr,CompFinancingBank.class);  
				this.saveCompFinaBank(data);
			}
		}
	}

	@Override
	public void deleteCompFinaBank(CompFinancingBank data) throws Exception {
		compFinancingBankDAO.deleteEntity(data);
	}
	
	@Override
	public List<CompFinancingBank> findCompFinaBankByTIdAndNo(Long taskId,
			String customerNo) throws Exception {
		
		String query = "taskId=:taskId and customerNo=:customerNo";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("taskId", taskId);
		params.put("customerNo", customerNo);
		
		return compFinancingBankDAO.select(query, params);
	}

	@Override
	public List<CompFinancingBank> findCompFinaBankByTIdAndNo(Long taskId, String customerNo, String flag){
		
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		params.put("customerNo", customerNo);
		if("M".equals(flag)){
			params.put("financialOrg", "RGRCB");
			return compFinancingBankDAO.selectByProperty(params);
		}else{
			return compFinancingBankDAO.queryOtherFinaBank(taskId, customerNo);
		}

	}

	@Override
	public CompFinancingBank findCompFinaBankById(Long id) {
		return compFinancingBankDAO.selectByPrimaryKey(id);
	}

	@Override
	public List<CompFinancingBank> syncCompFinaBankFromCmis(Long taskId, String customerNo) throws Exception {

		CusBase cusBase = cusBaseDAO.selectSingleByProperty("customerNo", customerNo);
		String xdCustNo = cusBase.getXdCustomerNo();
		String sql = "SELECT "
				+"C.CUSTOMERID,T.TYPENAME AS BUSINESSTYPE,C.BUSINESSSUM,"
				+"C.PUTOUTDATE,C.MATURITY,C.BUSINESSRATE,C.BALANCE,C.TERMMONTH,"
				+"C.PAYCYC,C.VOUCHTYPE,C.CLASSIFYRESULT,C.PURPOSE "
				+"FROM CMIS.BUSINESS_CONTRACT C "
				+"LEFT JOIN CMIS.BUSINESS_TYPE T ON T.TYPENO = C.BUSINESSTYPE "
				+"WHERE C.BALANCE > 0 AND C.CUSTOMERID = '" + xdCustNo + "' ORDER BY C.OCCURDATE,C.BUSINESSTYPE";
		
		List<Map<String, Object>> list=JdbcUtil.executeQuery(sql, Constants.DataSource.TCE_DS);
		List<CompFinancingBank> banks=new ArrayList<CompFinancingBank>();
		
		if(list!=null && list.size()>0) {
			for(int i=0;i<list.size();i++) {
				
				CompFinancingBank bank = new CompFinancingBank();
				
				bank.setCustomerNo(customerNo);
				bank.setTaskId(taskId);
				bank.setFinancialOrg("RGRCB");
				bank.setBusinessType(list.get(i).get("businesstype").toString());
				String vouchtype = list.get(i).get("vouchtype").toString();
				if(!StringUtils.isEmpty(vouchtype)){
					if(vouchtype.contains("A")){
						bank.setGuaranteeType("质押");
	            	}else if(vouchtype.contains("B")){
	            		bank.setGuaranteeType("抵押");
	            	}else if(vouchtype.contains("C")){
	            		bank.setGuaranteeType("保证");
	            	}else if(vouchtype.contains("D")){
	            		bank.setGuaranteeType("信用/免担保");
	            	}else if(vouchtype.contains("E")){
	            		bank.setGuaranteeType("组合担保");
	            	}else if(vouchtype.contains("F")){
	            		bank.setGuaranteeType("纸质押");
	            	}else if(vouchtype.contains("G")){
	            		bank.setGuaranteeType("票据贴现");
	            	}else if(vouchtype.contains("H")){
	            		bank.setGuaranteeType("保证金");
	            	}else if(vouchtype.contains("Z")){
	            		bank.setGuaranteeType("其他");
	            	}else{
	            		bank.setGuaranteeType("未说明");
	            	}
				}
				
				bank.setUseDesc(list.get(i).get("purpose").toString());
				String classifyresult = list.get(i).get("classifyresult").toString();
				if(StringUtils.isEmpty(classifyresult)){
					bank.setFiveClass("未分类");
				}else if (classifyresult.startsWith("QL01")){
					bank.setFiveClass("正常");
				}else if (classifyresult.startsWith("QL02")){
					bank.setFiveClass("关注");
				}else if (classifyresult.startsWith("QL03")){
					bank.setFiveClass("次级");
				}else if (classifyresult.startsWith("QL04")){
					bank.setFiveClass("可疑");
				}else if (classifyresult.startsWith("QL05")){
					bank.setFiveClass("损失");
				}
				
				bank.setRepaymentType(list.get(i).get("paycyc").toString());
				
				//授信金额
				Object temp = list.get(i).get("businesssum");
				if(temp != null)
					bank.setCreditAmt(Double.parseDouble(temp.toString())/10000);
				
				//当前余额
				temp = list.get(i).get("balance");
				if(temp != null)
					bank.setCreditBal(Double.parseDouble(temp.toString())/10000);
				
				//利率
				temp = list.get(i).get("businessrate");
				if(temp != null)
					bank.setCreditRate(Double.valueOf(temp.toString()));
				
				//期限
				temp = list.get(i).get("termmonth");
				if(temp != null)
					bank.setCreditTerm(Integer.valueOf(temp.toString()));
				
				String startDate = list.get(i).get("putoutdate").toString();
				String endDate = list.get(i).get("maturity").toString();
				startDate = DateUtil.convert(startDate, Constants.DATE_MASK2, Constants.DATE_MASK);
				endDate = DateUtil.convert(endDate, Constants.DATE_MASK2, Constants.DATE_MASK);
				
				bank.setStartDate(startDate);
				bank.setEndDate(endDate);
				
				this.saveCompFinaBank(bank);
				
				banks.add(bank);
			}
		}
		return banks;
	}

}
