package com.beawan.survey.custInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.dtmp.service.DtmpService;
import com.beawan.survey.custInfo.dao.CompFinanceTaxSaleDao;
import com.beawan.survey.custInfo.bean.CompFinanceTaxSale;
import com.beawan.survey.custInfo.service.CompFinanceTaxSaleService;
import com.platform.util.StringUtil;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.Resource;
import com.beawan.survey.custInfo.bean.CompFinanceTaxSale;

/**
 * @author yzj
 */
@Service("compFinanceTaxSaleService")
public class CompFinanceTaxSaleServiceImpl extends BaseServiceImpl<CompFinanceTaxSale> implements CompFinanceTaxSaleService{
   
	
	@Resource
	private DtmpService dtmpService;
	
	/**
    * 注入DAO
    */
    @Resource(name = "compFinanceTaxSaleDao")
    public void setDao(BaseDao<CompFinanceTaxSale> dao) {
        super.setDao(dao);
    }
    @Resource
    public CompFinanceTaxSaleDao compFinanceTaxSaleDao;
    
    /**
	 * 获取对应财报销售收入值
     * @throws Exception 
	 * */
	@Override
	public void getCompFinanceTaxSale(CompFinanceTaxSale compFinanceTaxSale, String customerNo) throws Exception {
		String TaxDate = compFinanceTaxSale.getTaxDate();
		TaxDate = standardTaxDate(TaxDate);
		
		if(!StringUtil.isEmptyString(TaxDate)){
			Double fnValue = dtmpService.getfnValue(customerNo, TaxDate);			
			compFinanceTaxSale.setFnValue(fnValue);			
		}
	}
	
	private String standardTaxDate(String TaxDate){
		String result="";
		TaxDate = TaxDate.trim();
		for(int i = 0;i<TaxDate.length();i++){
			if(TaxDate.charAt(i) >= 48&&TaxDate.charAt(i) <= 57){
				result += TaxDate.charAt(i);
			}
		}
		if(result.length() == 6){
			StringBuffer str = new StringBuffer();
			str.append(result);
			str.insert(4, "-");
			result = str.toString();
			return result;
		}else if(result.length() == 5){
			StringBuffer str = new StringBuffer();
			str.append(result);
			str.insert(4, "-0");
			result = str.toString();
			return result;	
		}else if(result.length() == 4){		
			result += "-12";
			return result;
		}else{
			return null;
		}
	}

	/**
	 * @param fnValue 对应财报销售收入值
	 * @param taxValue  纳税销售额
	 * 计算偏差率 计算公式: （对应财报销售收入值  - 纳税销售额）/纳税销售额
	 * */
	@Override
	public String countDeviation(Double fnValue, Double taxValue){
		String result = "";
		Double deviation = (double) (Math.round((fnValue - taxValue)/taxValue*100));
		if (deviation > 0){
			result = "+" + deviation.toString() + "%";
		}else{
			result = deviation.toString() + "%";
		}
		return result;
	}
	
	
	

}
