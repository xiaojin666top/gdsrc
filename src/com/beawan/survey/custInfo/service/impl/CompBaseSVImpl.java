package com.beawan.survey.custInfo.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.beawan.survey.custInfo.dao.*;
import com.beawan.survey.custInfo.dto.*;
import com.platform.util.*;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.beawan.base.dto.RandomizingID;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.util.SystemUtil;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.beawan.survey.custInfo.bean.CompBase;
import com.beawan.survey.custInfo.bean.CompBaseDesc;
import com.beawan.survey.custInfo.bean.CompBaseEquity;
import com.beawan.survey.custInfo.bean.CompBaseItemChange;
import com.beawan.survey.custInfo.bean.CompBaseManager;
import com.beawan.survey.custInfo.bean.PersonBase;
import com.beawan.survey.custInfo.dao.impl.CompBaseItemChangeDAOImpl;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.beawan.survey.custInfo.service.ICompConnectSV;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service("compBaseSV")
public class CompBaseSVImpl implements ICompBaseSV {

	@Resource
	private ICompBaseDAO compBaseDAO;
	
	@Resource
	private ICompBaseDescDAO compBaseDescDAO;
	
	@Resource
	private ICompBaseEquityDAO compBaseEquityDAO;
	
	@Resource
	private ICompBaseManagerDAO compBaseManagerDAO;
	
	@Resource
	private IPersonBaseDAO personBaseDAO;
	
	@Resource
	private ICompConnectSV compConnectSV;
	
	@Resource
	private ICmisInInvokeSV cmisInInvokeSV;
	
	@Resource
	private ICusBaseSV cusBaseSV;
	
	@Resource
	private CompBaseItemChangeDAOImpl compBaseItemChangeDAO;
	
	@Resource 
	private ICompChangeRecordDAO compChangeRecordDAO;

	@Resource
	private ICompManagerDAO compManagerDAO;
	

	@Override
	public CompBaseDto findCompBaseByCustNo(String customerNo) throws Exception {

		CompBase compBase = compBaseDAO.queryByCustNo(customerNo);
		CompBaseDto compBaseDto =MapperUtil.trans(compBase, CompBaseDto.class);
//		if(compBaseDto == null)
//			compBaseDto = cmisInInvokeSV.queryCompBaseInfo(custNo);

		if (compBaseDto != null)
			compBaseDto.setCompBaseDesc(this.findCompBaseDescByCustNo(customerNo));
		
		return compBaseDto;
	}

	@Override
	public void saveCompBase(CompBaseDto compBaseDto) throws Exception {
		CompBase compBase = MapperUtil.trans(compBaseDto, CompBase.class);
		if(StringUtil.isEmptyString(compBase.getCustomerNo())) {
			//生成临时客户号
			compBase.setCustomerNo(SystemUtil.genTempCusNo(SysConstants.CusNoType.COMP_CUS));
			//设置为草稿状态
			compBase.setIsDraft(SysConstants.YesOrNo.Yes);
		}
		compBase = compBaseDAO.saveOrUpdate(compBase);
		CompBaseDesc compBaseDesc = compBaseDto.getCompBaseDesc();
		if(compBaseDesc != null){
			compBaseDesc.setCustomerNo(compBase.getCustomerNo());
			compBaseDescDAO.saveOrUpdate(compBaseDesc);
		}
	}

	@Override
	public void deleteCompBase(CompBase compBase) throws Exception {
		List<CompBaseEquity> compCustEquity = compBaseEquityDAO.queryByCustNo(compBase.getCustomerNo());
		if (compCustEquity != null && compCustEquity.size() > 0) {
			for (CompBaseEquity compCustEquity1 : compCustEquity) {
				compBaseEquityDAO.deleteEntity(compCustEquity1);
			}
		}
		List<CompBaseManager> compCustManagers = compBaseManagerDAO.queryByCustNo(compBase.getCustomerNo());
		if (compCustManagers != null && compCustManagers.size() > 0) {
			for (CompBaseManager compCustManager : compCustManagers) {
				compBaseManagerDAO.deleteEntity(compCustManager);
			}
		}
		compBaseDAO.deleteEntity(compBase);
	}
	
	@Override
	public CompBaseDesc findCompBaseDescByCustNo(String custNo) throws Exception {
		CompBaseDesc compBaseDesc = compBaseDescDAO.queryByCustNo(custNo);
		if(compBaseDesc == null) {
			compBaseDesc = new CompBaseDesc();
			compBaseDesc.setCustomerNo(custNo);
		}
		return compBaseDesc;
	}

	@Override
	public void saveCompBaseDesc(CompBaseDesc compBaseDesc) throws Exception {
		compBaseDescDAO.saveOrUpdate(compBaseDesc);
	}
	
	@Override
	public void saveCompBaseDesc(String jsonInfo) throws Exception {
		
		CompBaseDesc temp = JacksonUtil.fromJson(jsonInfo, CompBaseDesc.class);
		CompBaseDesc compBaseDesc = findCompBaseDescByCustNo(temp.getCustomerNo());
		
		if(compBaseDesc == null)
			//新增
			compBaseDesc = temp;
		else{
			//修改
			temp.setCustomerNo(null);
			//该方法仅更新如下字段
			compBaseDesc.setBusinessIntro(temp.getBusinessIntro());
			compBaseDesc.setCreditInfo(temp.getCreditInfo());
			compBaseDesc.setOrgStructure(temp.getOrgStructure());
			compBaseDesc.setQualification(temp.getQualification());
			compBaseDesc.setOtherInfo(temp.getOtherInfo());
		}
		
		compBaseDescDAO.saveOrUpdate(compBaseDesc);
	}

	@Override
	public PersonBase findPersonBaseByCustNo(String custNo) throws Exception {
		PersonBase personBase = personBaseDAO.queryByCustNo(custNo);
		if (personBase == null) {
			personBase = cmisInInvokeSV.queryPersonBase(custNo);
		}
		return personBase;
	}

	@Override
	public PersonBase savePersonBase(PersonBase personBase){
		if(StringUtil.isEmptyString(personBase.getCustomerNo())){
			//生成临时客户号
			RandomizingID random = new RandomizingID("PS", "yyyyMMddHHmmss", 4, false);
			personBase.setCustomerNo(random.genNewId());
			/*personBase.setCustomerNo(SystemUtil.genTempCusNo(SysConstants.CusNoType.PERSON_CUS));
			//设置为草稿状态
			personBase.setIsDraft(SysConstants.YesOrNo.Yes);*/
		}
		return personBaseDAO.saveOrUpdate(personBase);
	}
	
	@Override
	public PersonBase savePersonBase(String jsondata) throws Exception {
		
		if(StringUtil.isEmpty(jsondata))
			return null;
		
		JSONObject json = JSONObject.fromObject(jsondata);
		PersonBase personBase = (PersonBase) JSONObject.toBean(json,
				PersonBase.class);
		
		PersonBase oldPersonBase = personBaseDAO.queryByCustNo(personBase.getCustomerNo());
		
		if(oldPersonBase != null){
			BeanUtil.mergeProperties(personBase, oldPersonBase, json);
		}else
			oldPersonBase = personBase;
		
		return this.savePersonBase(oldPersonBase);
	}

	@Override
	public void deletePersonBase(PersonBase personBase) throws Exception {
		personBaseDAO.deleteEntity(personBase);
	}

	@Override
	public List<CompBaseEquity> findCompEquityByCustNo(String custNo) throws Exception {
		return compBaseEquityDAO.queryByCustNo(custNo);
	}
	
	@Override
	public List<CompBaseEquity> syncCompEquityByCustNo(String custNo) throws Exception {
		
		List<CompBaseEquity> equities;
		
		String sql = "SELECT C.CUSTOMERID AS CUSTOMER_NO, C.CUSTOMERNAME AS STOCK_NAME,"
				   + "C.OUGHTSUM/10000 AS FUND_AMOUNT,C.INVESTMENTSUM/10000 AS INVT_FACT_AMT,"
				   + "C.INVESTMENTPROP AS FUND_RATE,D.CNNAME AS RELATION_DESC,C.CURRENCYTYPE AS CURRENCY "
				   + "FROM CMIS.CUSTOMER_RELATIVE C "
				   + "LEFT JOIN GDTCESYS.BS_DIC D ON D.ENNAME = C.RELATIONSHIP AND OPTTYPE='STD_SY_GB00016' "
				   + "WHERE RELATIONSHIP "
				   + "LIKE '52%' AND CUSTOMERID = '" + custNo + "'";
		
		List<CompBaseEquity> compBaseEquityList = JdbcUtil.executeQuery(sql, Constants.DataSource.TCE_DS, CompBaseEquity.class);
		if(!CollectionUtils.isEmpty(compBaseEquityList)) {
			
			equities = this.findCompEquityByCustNo(custNo);
			if(equities == null)
				equities = new ArrayList<CompBaseEquity>();
			
			for(int i=0; i<compBaseEquityList.size(); i++) {
				CompBaseEquity equity = compBaseEquityList.get(i);
				boolean exist = false;
				for(CompBaseEquity e : equities){
					if(e.getStockName() != null && e.getStockName().equals(equity.getStockName())){
						exist = true;
						break;
					}
				}
				//本地不存在的才保存
				if(!exist){
					compBaseEquityDAO.saveOrUpdate(equity);
					equities.add(equity);
				}
			}
			
		}else
			equities = new ArrayList<CompBaseEquity>();
		
		return equities;
	}
	
	@Override
	public CompBaseEquity findCompEquityById(Long id) throws Exception {
		return compBaseEquityDAO.selectByPrimaryKey(id);
	}

	@Override
	public void saveCompEquity(CompBaseEquity custEquity) throws Exception {
		compBaseEquityDAO.saveOrUpdate(custEquity);
	}
	
	@Override
	public void saveCompEquitys(String custNo, String jsonArray) throws Exception {
		
		//先删除原来的
		List<CompBaseEquity> list = compBaseEquityDAO.queryByCustNo(custNo);
		for (CompBaseEquity data : list) {
			compBaseEquityDAO.deleteEntity(data);
		}
		
		//后插入修改的
		JSONArray array = JSONArray.fromObject(jsonArray);
		if (array != null && array.size() > 0) {
			for (int i = 0; i < array.size(); i++) {
				JSONObject dataStr = JSONObject.fromObject(array.get(i));
				CompBaseEquity data = (CompBaseEquity) JSONObject.toBean(dataStr,CompBaseEquity.class);  
				compBaseEquityDAO.saveOrUpdate(data);
			}
		}
	}

	@Override
	public void deleteCompEquity(CompBaseEquity equity) throws Exception {
		compBaseEquityDAO.deleteEntity(equity);
	}
	
	@Override
	public List<CompBaseManagerDto> findCompManagerByCustNo(String customerNo){
		List<CompBaseManager> baseManagerList = compBaseManagerDAO.queryByCustNo(customerNo);
		List<CompBaseManagerDto> managerList = MapperUtil.trans(baseManagerList, CompBaseManagerDto.class);
		if(!CollectionUtils.isEmpty(managerList)){
			for(CompBaseManagerDto manager : managerList){
				PersonBase personBase = personBaseDAO.queryByCustNo(manager.getPsCusNo());
				manager.setPersonBase(MapperUtil.trans(personBase, PersonBaseDto.class));
			}
		}
		return managerList;
	}

	@Override
	public List<CompManagerInfoDto> findManagerInfoByCustNo(String customerNo){

		StringBuilder sql = new StringBuilder();
		Map<String, Object> params = new HashMap<>();

		sql.append("SELECT cbm.ID id, cbm.CM_CUS_NO customerNo, pb.BIRTH_PLACE birthPlace,")
				.append(" pb.NAME name, cbm.MANAGER_TYPE managerType,")
				.append(" pb.BIRTH_DATE birthday, pb.LIVING_PLACE livingPlace, pb.SEX sex,")
				.append(" pb.MARITAL_STATUS maritalStatus, pb.EDUCATION education, pb.TITLE title")
				.append(" FROM CM_BASE_MANAGER cbm")
				.append(" LEFT JOIN PS_BASE pb ON pb.CUSTOMER_NO = cbm.PS_CUS_NO")
				.append(" WHERE cbm.CM_CUS_NO = "+"'"+customerNo+"'"+" AND cbm.STATUS = "+Constants.NORMAL);
//    params.put("customerNo", customerNo);
//    params.put("status", Constants.NORMAL);
		return compManagerDAO.findCustListBySql(CompManagerInfoDto.class, sql.toString(), params);

      /*String sql = "SELECT "
               + "M.ID, M.CM_CUS_NO, M.MANAGER_TYPE,"
               + "P.NAME, P.SEX, P.BIRTH_DATE,"
               + "P.BIRTH_PLACE, P.LIVING_PLACE,"
               + "P.MARITAL_STATUS, P.EDUCATION, P.TITLE "
               + "FROM GDTCESYS.DG_CM_BASE_MANAGER M "
               + "LEFT JOIN GDTCESYS.DG_PS_BASE P ON P.CUSTOMER_NO = M.PS_CUS_NO "
               + "WHERE M.CM_CUS_NO = '" + customerNo + "'";
      return JdbcUtil.executeQuery(sql, Constants.DataSource.TCE_DS);*/
	}

	@Override
	public List<CompBaseManagerDto> syncCompManagerFromCmis(String customerNo) throws Exception {
		
		List<CompBaseManagerDto> managers;
		
		String sql = "SELECT C.CUSTOMERID AS CM_CUS_NO,C.RELATIVEID AS PS_CUS_NO,"
				   + "C.REMARK AS REMARK,D.CNNAME AS MANAGER_TYPE "
				   + "FROM CMIS.CUSTOMER_RELATIVE C "
				   + "LEFT JOIN GDTCESYS.BS_DIC D ON D.ENNAME = C.RELATIONSHIP AND OPTTYPE='STD_SY_GB00016' "
				   + "WHERE RELATIONSHIP "
				   + "LIKE '01%' AND LENGTH(RELATIONSHIP)=4 AND CUSTOMERID = '" + customerNo + "'";
		
		List<CompBaseManager> compBaseManagerList = JdbcUtil.executeQuery(sql, Constants.DataSource.TCE_DS, CompBaseManager.class);
		if(!CollectionUtils.isEmpty(compBaseManagerList)) {
			
			managers = this.findCompManagerByCustNo(customerNo);
			if(managers == null)
				managers = new ArrayList<CompBaseManagerDto>();
			
			for(int i=0; i<compBaseManagerList.size(); i++) {
				CompBaseManager manager = compBaseManagerList.get(i);
				boolean exist = false;
				for(CompBaseManagerDto m : managers){
					if(m.getPsCusNo().equals(manager.getPsCusNo())){
						exist = true;
						break;
					}
				}
				//本地不存在的才保存
				if(!exist){
					compBaseManagerDAO.saveOrUpdate(manager);
					cmisInInvokeSV.syncCusInfo(manager.getPsCusNo());
					managers.add(MapperUtil.trans(manager, CompBaseManagerDto.class));
				}
			}
			
		}else
			managers = new ArrayList<CompBaseManagerDto>();
		
		if(!CollectionUtils.isEmpty(managers)){

			CompBaseDto compBase = this.findCompBaseByCustNo(customerNo);
			
			if(compBase != null && (StringUtil.isEmptyString(compBase.getLegalPerson()) 
					|| StringUtil.isEmptyString(compBase.getControlPerson()))){
				
				for(CompBaseManagerDto entity : managers){
					
					CusBase cusBase = cusBaseSV.queryByCusNo(entity.getPsCusNo());
					if(cusBase == null)
						continue;
					
					if(StringUtil.isEmptyString(compBase.getLegalPerson()) 
							&& entity.getManagerType().contains("法人代表")){
						
						compBase.setLegalPerson(cusBase.getCustomerName());
						compBase.setLegalPersonCode(cusBase.getCustomerNo());
						
					}else if(StringUtil.isEmptyString(compBase.getControlPerson()) 
							&& entity.getManagerType().contains("实际控制人")){
						
						compBase.setControlPerson(cusBase.getCustomerName());
						compBase.setControlPersonCode(cusBase.getCustomerNo());
					}
				}
				
				this.saveCompBase(compBase);
			}
		}
		
		return managers;
	}
	
	@Override
	public CompBaseManagerDto findCompManagerById(Long id) throws Exception {
		CompBaseManager manager = compBaseManagerDAO.selectByPrimaryKey(id);
		PersonBase personBase = personBaseDAO.queryByCustNo(manager.getPsCusNo());
		CompBaseManagerDto baseManager = MapperUtil.trans(manager, CompBaseManagerDto.class);
		baseManager.setPersonBase(MapperUtil.trans(personBase, PersonBaseDto.class));
		return baseManager;
	}

	@Override
	public void saveCompManager(String userId, CompBaseManagerDto manager) throws Exception {
		Timestamp now = DateUtil.getNowTimestamp();
		CompBaseManager baseManager = MapperUtil.trans(manager, CompBaseManager.class);

		PersonBase personBase = MapperUtil.trans(manager.getPersonBase(), PersonBase.class);
		if (personBase == null) { // 主键 customerNo
			personBase = new PersonBase();
			personBase.setCustomerNo(SystemUtil.genTempCusNo(SysConstants.CusNoType.PERSON_CUS));//生成临时客户号
			personBase.setCreater(userId);
			personBase.setCreateTime(now);
		}
		personBase.setUpdater(userId);
		personBase.setUpdateTime(now);
		personBase.setStatus(Constants.NORMAL);
		//防止customerNo遗漏，若 psCusNo 也为空则 保存时生成客户号
		if (StringUtil.isEmptyString(personBase.getCustomerNo()))
			personBase.setCustomerNo(manager.getPsCusNo());
		personBase = this.savePersonBase(personBase);

		if (baseManager.getId() == null) {
			baseManager.setCreater(userId);
			baseManager.setCreateTime(now);
		}
		baseManager.setUpdater(userId);
		baseManager.setUpdateTime(now);
		baseManager.setStatus(Constants.NORMAL);
		baseManager.setPsCusNo(personBase.getCustomerNo());
		baseManager.setManagerName(personBase.getName());
		compBaseManagerDAO.saveOrUpdate(baseManager);
	}
	
	@Override
	public void saveCompManagers(String custNo, String jsonArray) throws Exception {
		
		//先删除原来的
		List<CompBaseManager> list = compBaseManagerDAO.queryByCustNo(custNo);
		for (CompBaseManager data : list) {
			compBaseManagerDAO.deleteEntity(data);
		}
		
		//后插入修改的
		JSONArray array = JSONArray.fromObject(jsonArray);
		if (array != null && array.size() > 0) {
			for (int i = 0; i < array.size(); i++) {
				JSONObject dataStr = JSONObject.fromObject(array.get(i));
				CompBaseManager data = (CompBaseManager) JSONObject.toBean(dataStr,CompBaseManager.class);  
				compBaseManagerDAO.saveOrUpdate(data);
			}
		}
	}

	@Override
	public void deleteCompManager(String userId, CompBaseManagerDto managerInfo){

		CompBaseManager baseManager = MapperUtil.trans(managerInfo, CompBaseManager.class);
		PersonBase personBase = MapperUtil.trans(managerInfo.getPersonBase(), PersonBase.class);
		Timestamp now = DateUtil.getNowTimestamp();
		personBase.setUpdater(userId);
		personBase.setUpdateTime(now);
		personBase.setStatus(Constants.DELETE);
		this.savePersonBase(personBase);

		baseManager.setUpdater(userId);
		baseManager.setUpdateTime(now);
		baseManager.setStatus(Constants.DELETE); //删除
		compBaseManagerDAO.saveOrUpdate(baseManager);
	}
	
	@Override
	public void syncCustomerInfo(String cusId) throws Exception {
		
		//同步股权结构信息
		this.syncCompEquityByCustNo(cusId);
		
		//同步关联企业信息
		compConnectSV.syncCompConnectCmByCustNo(cusId);
		
		//同步企业高管信息
		this.syncCompManagerFromCmis(cusId);
	}

	@Override
	public void saveComItemChange(CompBaseItemChange change){
		compBaseItemChangeDAO.saveOrUpdate(change);
	}

	@Override
	public List<CompBaseItemChange> findChangeListByCustNo(String customerNo) throws Exception {
		return compBaseItemChangeDAO.queryByCustNo(customerNo);
	}

	@Override
	public void deleteCompEquityChange(CompBaseItemChange change) throws Exception {
		compBaseItemChangeDAO.deleteEntity(change);
	}

	@Override
	public CompBaseItemChange findCompChangeById(Long id) throws Exception {
		// TODO Auto-generated method stub
		return compBaseItemChangeDAO.selectByPrimaryKey(id);
	}
	
	@Override
	public List<CompChangeRecordDto> groupByItemWhereCustNo(String custNo) {
		return compChangeRecordDAO.groupByItemWhereCustNo(custNo);
		
	}



}
