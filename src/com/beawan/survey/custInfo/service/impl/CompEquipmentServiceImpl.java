package com.beawan.survey.custInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.custInfo.dao.CompEquipmentDao;
import com.beawan.survey.custInfo.entity.CompEquipment;
import com.beawan.survey.custInfo.service.CompEquipmentService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.custInfo.entity.CompEquipment;

/**
 * @author yzj
 */
@Service("compEquipmentService")
public class CompEquipmentServiceImpl extends BaseServiceImpl<CompEquipment> implements CompEquipmentService{
    /**
    * 注入DAO
    */
    @Resource(name = "compEquipmentDao")
    public void setDao(BaseDao<CompEquipment> dao) {
        super.setDao(dao);
    }
    @Resource
    public CompEquipmentDao compEquipmentDao;
}
