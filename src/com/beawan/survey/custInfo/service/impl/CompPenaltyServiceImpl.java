package com.beawan.survey.custInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.custInfo.dao.CompPenaltyDao;
import com.beawan.survey.custInfo.bean.CompPenalty;
import com.beawan.survey.custInfo.service.CompPenaltyService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.custInfo.bean.CompPenalty;

/**
 * @author yzj
 */
@Service("compPenaltyService")
public class CompPenaltyServiceImpl extends BaseServiceImpl<CompPenalty> implements CompPenaltyService{
    /**
    * 注入DAO
    */
    @Resource(name = "compPenaltyDao")
    public void setDao(BaseDao<CompPenalty> dao) {
        super.setDao(dao);
    }
    @Resource
    public CompPenaltyDao compPenaltyDao;
}
