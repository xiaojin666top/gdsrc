package com.beawan.survey.custInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.custInfo.dao.CompRawMaterialDao;
import com.beawan.survey.custInfo.entity.CompRawMaterial;
import com.beawan.survey.custInfo.service.CompRawMaterialService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.custInfo.entity.CompRawMaterial;

/**
 * @author yzj
 */
@Service("compRawMaterialService")
public class CompRawMaterialServiceImpl extends BaseServiceImpl<CompRawMaterial> implements CompRawMaterialService{
    /**
    * 注入DAO
    */
    @Resource(name = "compRawMaterialDao")
    public void setDao(BaseDao<CompRawMaterial> dao) {
        super.setDao(dao);
    }
    @Resource
    public CompRawMaterialDao compRawMaterialDao;
}
