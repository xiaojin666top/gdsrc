package com.beawan.survey.custInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.custInfo.dao.FinanceWarnDao;
import com.beawan.survey.custInfo.entity.FinanceWarn;
import com.beawan.survey.custInfo.service.FinanceWarnService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.custInfo.entity.FinanceWarn;

/**
 * @author yzj
 */
@Service("financeWarnService")
public class FinanceWarnServiceImpl extends BaseServiceImpl<FinanceWarn> implements FinanceWarnService{
    /**
    * 注入DAO
    */
    @Resource(name = "financeWarnDao")
    public void setDao(BaseDao<FinanceWarn> dao) {
        super.setDao(dao);
    }
    @Resource
    public FinanceWarnDao financeWarnDao;
}
