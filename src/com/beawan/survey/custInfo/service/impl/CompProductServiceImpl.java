package com.beawan.survey.custInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.custInfo.bean.CompRunProduct;
import com.beawan.survey.custInfo.dao.CompProductDao;
import com.beawan.survey.custInfo.service.CompProductService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author zxh
 * @date 2020/7/9 17:33
 */
@Service("compProductService")
public class CompProductServiceImpl extends BaseServiceImpl<CompRunProduct> implements CompProductService {

    /**
     * 注入DAO
     */
    @Resource(name = "compProductDao")
    public void setDao(BaseDao<CompRunProduct> dao) {
        super.setDao(dao);
    }

    @Resource
    private CompProductDao compProductDao;
}
