package com.beawan.survey.custInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.custInfo.dao.CompInnerManageDao;
import com.beawan.survey.custInfo.entity.CompInnerManage;
import com.beawan.survey.custInfo.service.CompInnerManageService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.custInfo.entity.CompInnerManage;

/**
 * @author yzj
 */
@Service("compInnerManageService")
public class CompInnerManageServiceImpl extends BaseServiceImpl<CompInnerManage> implements CompInnerManageService{
    /**
    * 注入DAO
    */
    @Resource(name = "compInnerManageDao")
    public void setDao(BaseDao<CompInnerManage> dao) {
        super.setDao(dao);
    }
    @Resource
    public CompInnerManageDao compInnerManageDao;
}
