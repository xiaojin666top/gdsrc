package com.beawan.survey.custInfo.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.beawan.common.Constants;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.beawan.survey.custInfo.bean.CompConnetCm;
import com.beawan.survey.custInfo.dao.ICompConnectDAO;
import com.beawan.survey.custInfo.service.ICompConnectSV;
import com.platform.util.JdbcUtil;

@Service("compConnectSV")
public class CompConnectSVImpl implements ICompConnectSV{

	
	@Resource
	private ICompConnectDAO compConnectDAO;
	
	@Resource
	private ICmisInInvokeSV cmisInInvokeSV;
	
	@Override
	public void saveCompConnectCm(CompConnetCm data) throws Exception {
		compConnectDAO.saveOrUpdate(data);
	}

	@Override
	public void deleteCompConnectCm(CompConnetCm data) throws Exception {
		compConnectDAO.deleteEntity(data);
	}
	
	@Override
	public CompConnetCm findById(Long id) throws Exception {
		return compConnectDAO.selectByPrimaryKey(id);
	}

	@Override
	public List<CompConnetCm> findCompConnectCmByCustNo(String customerNo) {
		return compConnectDAO.queryByCustNo(customerNo);
	}


	@Override
	public List<CompConnetCm> syncCompConnectCmByCustNo(String custNo) throws  Exception{
		
		List<CompConnetCm> connetComs;

		String sql = "SELECT C.CUSTOMERID AS CUSTOMER_NO,C.CUSTOMERNAME AS COMPANY_NAME, "
				   + "C.FICTITIOUSPERSON AS LEGAL_PERSON,C.INVESINITIALSUM AS REGISTE_CAPITAL "
				   + "FROM CMIS.CUSTOMER_RELATIVE C WHERE (C.RELATIONSHIP LIKE 'E000%' OR C.RELATIONSHIP LIKE '990%') "
				   + "AND C.CUSTOMERID = '" + custNo + "'";
		
		List<CompConnetCm> compConnetCmList = JdbcUtil.executeQuery(sql, Constants.DataSource.TCE_DS, CompConnetCm.class);
		if(CollectionUtils.isEmpty(compConnetCmList)) return new ArrayList<>();

		connetComs = this.findCompConnectCmByCustNo(custNo);
		if (connetComs == null) return new ArrayList<>();

		for (CompConnetCm conenctCm : compConnetCmList) {
			boolean exist = false;
			for (CompConnetCm c : connetComs) {
				if (c.getCompanyName() != null && c.getCompanyName().equals(conenctCm.getCompanyName())) {
					exist = true;
					break;
				}
			}
			//本地不存在的才保存
			if (!exist) {
				compConnectDAO.saveOrUpdate(conenctCm);
				cmisInInvokeSV.syncCusInfo(conenctCm.getCustomerNo());
				connetComs.add(conenctCm);
			}
		}
		return connetComs;
	}

	@Override
	public void saveCompConnectCms(String custNo, String compInfoJsonArray)
			throws Exception {
		
		//先删除原来的
		List<CompConnetCm> connetCompanies = compConnectDAO.queryByCustNo(custNo);
		for (CompConnetCm connetCompany : connetCompanies) {
			compConnectDAO.deleteEntity(connetCompany);
		}
		
		//后插入修改的
		JSONArray array = JSONArray.fromObject(compInfoJsonArray);
		if (array != null && array.size() > 0) {
			for (int i = 0; i < array.size(); i++) {
				JSONObject dataStr = JSONObject.fromObject(array.get(i));
				CompConnetCm data = (CompConnetCm) JSONObject.toBean(dataStr,CompConnetCm.class);  
				compConnectDAO.saveOrUpdate(data);
			}
		}
		
	}
	
}
