package com.beawan.survey.custInfo.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.beawan.survey.custInfo.bean.CompRunConst;
import com.beawan.survey.custInfo.bean.CompRunConstSPDetail;
import com.beawan.survey.custInfo.bean.CompRunConstSPStruc;
import com.beawan.survey.custInfo.bean.CompRunCustomer;
import com.beawan.survey.custInfo.bean.CompRunEsta;
import com.beawan.survey.custInfo.bean.CompRunEstaDevedProj;
import com.beawan.survey.custInfo.bean.CompRunEstaDevingProj;
import com.beawan.survey.custInfo.bean.CompRunEstaLand;
import com.beawan.survey.custInfo.bean.CompRunFixed;
import com.beawan.survey.custInfo.bean.CompRunGeneral;
import com.beawan.survey.custInfo.bean.CompRunPower;
import com.beawan.survey.custInfo.bean.CompRun;
import com.beawan.survey.custInfo.bean.CompRunArea;
import com.beawan.survey.custInfo.bean.CompRunAreaCf;
import com.beawan.survey.custInfo.bean.CompRunAreaTd;
import com.beawan.survey.custInfo.bean.CompRunPrdSale;
import com.beawan.survey.custInfo.bean.CompRunProduce;
import com.beawan.survey.custInfo.bean.CompRunSupply;
import com.beawan.survey.custInfo.dao.ICompRunDAO;
import com.beawan.survey.custInfo.service.ICompRunSV;
import com.platform.util.BeanUtil;
import com.platform.util.StringUtil;

@Service("compRunSV")
public class CompRunSVImpl implements  ICompRunSV{
	
	@Resource
	private  ICompRunDAO runInfoDAO;
	
	@Override
	public CompRun queryCompRunByTIdAndNo(Long taskId, String customerNo){
		return runInfoDAO.queryCompRunByTIdAndNo(taskId, customerNo);
	}

	@Override
	public void saveCompRun(CompRun runInfo) throws Exception {
		runInfoDAO.saveCompRun(runInfo);
	}
	
	public void deleteCompRun(CompRun runInfo) throws Exception{
		
		if(runInfo != null){
			
			Long taskId = runInfo.getTaskId();
			String custNo = runInfo.getCustomerNo();
			
			List<CompRunCustomer> customerList = runInfoDAO.queryRunCustomerByTIdAndNo(taskId, custNo);
			if (customerList != null && customerList.size() > 0) {
				for (CompRunCustomer compCustCustomer : customerList) {
					runInfoDAO.deleteRunCustomer(compCustCustomer);
				}
			}
			
			List<CompRunArea> compCustRunAreas = runInfoDAO.queryRunAreaByTIdAndNo(taskId, custNo);
			if (compCustRunAreas != null && compCustRunAreas.size() > 0) {
				for (CompRunArea compCustRunArea : compCustRunAreas) {
					runInfoDAO.deleteRunArea(compCustRunArea);
				}
			}
			
			List<CompRunSupply> supplyList = runInfoDAO.queryRunSupplyByTIdAndNo(taskId, custNo);
			if (supplyList != null && supplyList.size() > 0) {
				for (CompRunSupply supply : supplyList) {
					runInfoDAO.deleteRunSupply(supply);
				}
			}
			
			List<CompRunPower> powerList = runInfoDAO.queryRunPowerByTIdAndNo(taskId, custNo);
			if(powerList != null && powerList.size()>0){
				for (CompRunPower compCustPower : powerList) {
					runInfoDAO.deleteRunPower(compCustPower);
				}
			}
			
			CompRunProduce compRunProduce = runInfoDAO.queryRunProduceByTIdAndNo(taskId, custNo);
			if(compRunProduce != null)
				runInfoDAO.deleteRunProduce(compRunProduce);
			
			List<CompRunPrdSale> prdSaleList = runInfoDAO.queryRunPrdSaleByTIdAndNo(taskId, custNo);
			if(prdSaleList != null && prdSaleList.size()>0){
				for (CompRunPrdSale prdSale : prdSaleList) {
					runInfoDAO.deleteRunPrdSale(prdSale);
				}
			}
			
			runInfoDAO.deleteCompRun(runInfo);
		}
	}
	
	@Override
	public CompRunProduce queryRunProduceByTIdAndNo(Long taskId, String custNo) throws Exception {
		CompRunProduce compRunProduce=runInfoDAO.queryRunProduceByTIdAndNo(taskId, custNo);
		return compRunProduce;
	}
	
	@Override
	public void saveRunProduce(CompRunProduce compRunProduce) throws Exception {
		runInfoDAO.saveRunProduce(compRunProduce);
	}
	
	@Override
	public void deleteRunProduce(CompRunProduce compRunProduce) throws Exception {
		runInfoDAO.deleteRunProduce(compRunProduce);
	}

	@Override
	public List<CompRunArea> queryRunAreaByTIdAndNo(Long taskId, String custNo) throws Exception {
		return runInfoDAO.queryRunAreaByTIdAndNo(taskId, custNo);
	}
	
	@Override
	public List<CompRunAreaTd> queryRunAreaLandByTIdAndNo(Long taskId, String custNo) throws Exception {
		return runInfoDAO.queryRunAreaLandByTIdAndNo(taskId, custNo);
	}
	
	@Override
	public List<CompRunAreaCf> queryRunAreaPlantByTIdAndNo(Long taskId, String custNo) throws Exception {
		return runInfoDAO.queryRunAreaPlantByTIdAndNo(taskId, custNo);
	}
	
	@Override
	public CompRunArea queryRunAreaById(Long id)
			throws Exception{
		return runInfoDAO.queryById(CompRunArea.class, id);
	}
	
	@Override
	public CompRunAreaTd queryRunAreaLandById(Long id)
			throws Exception{
		return runInfoDAO.queryById(CompRunAreaTd.class, id);
	}
	
	@Override
	public CompRunAreaCf queryRunAreaPlantById(Long id)
			throws Exception{
		return runInfoDAO.queryById(CompRunAreaCf.class, id);
	}
	
	@Override
	public void saveRunArea(CompRunArea runArea) throws Exception {
		runInfoDAO.saveRunArea(runArea);
	}
	
	@Override
	public void saveRunAreaLand(CompRunAreaTd runArea) throws Exception {
		runInfoDAO.saveRunAreaLand(runArea);
	}
	
	@Override
	public void saveRunAreaPlant(CompRunAreaCf runArea) throws Exception {
		runInfoDAO.saveRunAreaPlant(runArea);
	}
	
	@Override
	public void saveRunAreas(Long taskId, String custNo, String jsonArray) throws Exception {
		
		deleteBeforeSave(taskId, custNo, jsonArray, CompRunArea.class);
	}
	
	@Override
	public void deleteRunArea(CompRunArea runArea) throws Exception {

		runInfoDAO.deleteRunArea(runArea);
	}

	@Override
	public List<CompRunSupply> queryRunSupplyByTIdAndNo(Long taskId, String customerNo) {
		return runInfoDAO.queryRunSupplyByTIdAndNo(taskId, customerNo);
	}
	
	@Override
	public CompRunSupply queryRunSupplyById(Long id){
		return runInfoDAO.queryById(CompRunSupply.class, id);
	}
	
	@Override
	public void saveRunSupply(CompRunSupply supply) throws Exception {
		runInfoDAO.saveRunSupply(supply);
	}
	
	@Override
	public void saveRunSupplys(Long taskId, String custNo, String jsonArray) throws Exception{
		this.deleteBeforeSave(taskId, custNo, jsonArray, CompRunSupply.class);
	}
	
	@Override
	public void deleteRunSupply(CompRunSupply supply) throws Exception {
		runInfoDAO.deleteRunSupply(supply);
	}

	@Override
	public List<CompRunCustomer> queryRunCustomerByTIdAndNo(Long taskId, String custNo) throws Exception {
		return runInfoDAO.queryRunCustomerByTIdAndNo(taskId, custNo);
	}
	
	@Override
	public CompRunCustomer queryRunCustomerById(Long id)
			throws Exception{
		return runInfoDAO.queryById(CompRunCustomer.class, id);
	}
	
	@Override
	public void saveRunCustomer(CompRunCustomer customer) throws Exception {
		runInfoDAO.saveRunCustomer(customer);
	}
	
	@Override
	public void saveRunCustomers(Long taskId, String custNo, String jsonArray)
			throws Exception{
		this.deleteBeforeSave(taskId, custNo, jsonArray, CompRunCustomer.class);
	}
	
	@Override
	public void deleteRunCustomer(CompRunCustomer custCustomer) throws Exception {
		runInfoDAO.deleteRunCustomer(custCustomer);
	}
	
	@Override
	public List<CompRunPower> queryRunPowerByTIdAndNo(Long taskId, String custNo)
			throws Exception{
		return runInfoDAO.queryRunPowerByTIdAndNo(taskId, custNo);
	}
	
	@Override
	public CompRunPower queryRunPowerById(Long id)
			throws Exception{
		return runInfoDAO.queryById(CompRunPower.class, id);
	}

	@Override
	public void saveRunPower(CompRunPower power) throws Exception {
		runInfoDAO.saveRunPower(power);
	}
	
	@Override
	public void saveRunPowers(Long taskId, String custNo, String jsonArray) throws Exception {
		this.deleteBeforeSave(taskId, custNo, jsonArray, CompRunPower.class);
	}

	@Override
	public void deleteRunPower(CompRunPower power) throws Exception {
		runInfoDAO.deleteRunPower(power);
	}
	
	@Override
	public List<CompRunPrdSale> queryRunPrdSaleByTIdAndNo(Long taskId, String custNo)
			throws Exception{
		return runInfoDAO.queryRunPrdSaleByTIdAndNo(taskId, custNo);
	}
	
	@Override
	public CompRunPrdSale queryRunPrdSaleById(Long id)
			throws Exception{
		return runInfoDAO.queryById(CompRunPrdSale.class, id);
	}

	@Override
	public void saveRunPrdSale(CompRunPrdSale compRunPrdSale) throws Exception {
		runInfoDAO.saveRunPrdSale(compRunPrdSale);
	}
	
	@Override
	public void saveRunPrdSales(Long taskId, String custNo, String jsonArray) throws Exception {
		this.deleteBeforeSave(taskId, custNo, jsonArray, CompRunPrdSale.class);
	}

	@Override
	public void deleteRunPrdSale(CompRunPrdSale compRunPrdSale) throws Exception {
		runInfoDAO.deleteRunPrdSale(compRunPrdSale);
	}
	
	@Override
	public CompRunConst queryRunConstByTIdAndNo(Long taskId, String custNo) throws Exception {
		CompRunConst compRun =  runInfoDAO.queryRunConstByTIdAndNo(taskId, custNo);
		
		return compRun;
	}

	@Override
	public void saveRunConst(CompRunConst compRunConst) throws Exception {
		runInfoDAO.saveRunConst(compRunConst);
	}
	
	public void deleteRunConst(CompRunConst runInfo) throws Exception{
		
		if(runInfo != null){
			
			Long taskId = runInfo.getTaskId();
			String custNo = runInfo.getCustomerNo();
			
			deleteByTIdAndNo(taskId, custNo, CompRunCustomer.class);
			deleteByTIdAndNo(taskId, custNo, CompRunArea.class);
			deleteByTIdAndNo(taskId, custNo, CompRunSupply.class);
			deleteByTIdAndNo(taskId, custNo, CompRunPower.class);
			deleteByTIdAndNo(taskId, custNo, CompRunProduce.class);
			deleteByTIdAndNo(taskId, custNo, CompRunPrdSale.class);
			
			deleteByTIdAndNo(taskId, custNo, CompRunConstSPDetail.class);
			deleteByTIdAndNo(taskId, custNo, CompRunConstSPStruc.class);
			
			runInfoDAO.deleteRunConst(runInfo);
		}
	}
	
	@Override
	public List<CompRunConstSPDetail> queryRunConstSPDetailByTIdAndNo(Long taskId, String custNo)
			throws Exception{
		return runInfoDAO.queryRunConstSPDetailByTIdAndNo(taskId, custNo);
	}
	
	@Override
	public CompRunConstSPDetail queryRunConstSPDetailById(Long id)
			throws Exception{
		return runInfoDAO.queryById(CompRunConstSPDetail.class, id);
	}

	@Override
	public void saveRunConstSPDetail(CompRunConstSPDetail compRunConstSPDetail) throws Exception {
		runInfoDAO.saveRunConstSPDetail(compRunConstSPDetail);
	}
	
	@Override
	public void saveRunConstSPDetails(Long taskId, String custNo, String jsonArray) throws Exception {
		this.deleteBeforeSave(taskId, custNo, jsonArray, CompRunConstSPDetail.class);
	}

	@Override
	public void deleteRunConstSPDetail(CompRunConstSPDetail compRunConstSPDetail) throws Exception {
		runInfoDAO.deleteRunConstSPDetail(compRunConstSPDetail);
	}
	
	@Override
	public List<CompRunConstSPStruc> queryRunConstSPStrucByTIdAndNo(Long taskId, String custNo)
			throws Exception{
		return runInfoDAO.queryRunConstSPStrucByTIdAndNo(taskId, custNo);
	}
	
	@Override
	public CompRunConstSPStruc queryRunConstSPStrucById(Long id)
			throws Exception{
		return runInfoDAO.queryById(CompRunConstSPStruc.class, id);
	}

	@Override
	public void saveRunConstSPStruc(CompRunConstSPStruc compRunConstSPStruc) throws Exception {
		runInfoDAO.saveRunConstSPStruc(compRunConstSPStruc);
	}
	
	@Override
	public void saveRunConstSPStrucs(Long taskId, String custNo, String jsonArray) throws Exception {
		this.deleteBeforeSave(taskId, custNo, jsonArray, CompRunConstSPStruc.class);
	}

	@Override
	public void deleteRunConstSPStruc(CompRunConstSPStruc compRunConstSPStruc) throws Exception {
		runInfoDAO.deleteRunConstSPStruc(compRunConstSPStruc);
	}
	
	@Override
	public CompRunFixed queryRunFixedByTIdAndNo(Long taskId, String custNo) throws Exception {
		CompRunFixed compRun =  runInfoDAO.queryRunFixedByTIdAndNo(taskId, custNo);
		
		return compRun;
	}

	@Override
	public CompRunFixed saveRunFixed(CompRunFixed compRunFixed) throws Exception {
		return runInfoDAO.saveRunFixed(compRunFixed);
	}
	
	@Override
	public CompRunFixed saveRunFixed(String jsondata) throws Exception {
		
		if(StringUtil.isEmpty(jsondata))
			return null;
		
		JSONObject json = JSONObject.fromObject(jsondata);
		CompRunFixed compRunFixed = (CompRunFixed) JSONObject.toBean(json,
				CompRunFixed.class);
		
		CompRunFixed oldCompRunFixed = runInfoDAO.queryRunFixedByTIdAndNo(compRunFixed.getTaskId(),
				compRunFixed.getCustomerNo());
		
		if(oldCompRunFixed != null){
			BeanUtil.mergeProperties(compRunFixed, oldCompRunFixed, json);
		}else
			oldCompRunFixed = compRunFixed;
		
		return runInfoDAO.saveRunFixed(compRunFixed);
	}
	
	@Override
	public void deleteRunFixed(CompRunFixed runInfo) throws Exception{
		
		if(runInfo != null){
			
			Long taskId = runInfo.getTaskId();
			String custNo = runInfo.getCustomerNo();
			
			deleteByTIdAndNo(taskId, custNo, CompRunCustomer.class);
			deleteByTIdAndNo(taskId, custNo, CompRunArea.class);
			deleteByTIdAndNo(taskId, custNo, CompRunSupply.class);
			deleteByTIdAndNo(taskId, custNo, CompRunPower.class);
			deleteByTIdAndNo(taskId, custNo, CompRunProduce.class);
			deleteByTIdAndNo(taskId, custNo, CompRunPrdSale.class);
			
			runInfoDAO.deleteRunFixed(runInfo);
		}
	}
	
	@Override
	public CompRunEsta queryRunEstaByTIdAndNo(Long taskId, String custNo) throws Exception {
		CompRunEsta compRun =  runInfoDAO.queryRunEstaByTIdAndNo(taskId, custNo);
		if(compRun == null){
			compRun = new CompRunEsta();
			compRun.setTaskId(taskId);
			compRun.setCustomerNo(custNo);
		}
		return compRun;
	}

	@Override
	public void saveRunEsta(CompRunEsta compRunEsta) throws Exception {
		runInfoDAO.saveRunEsta(compRunEsta);
	}
	
	@Override
	public void deleteRunEsta(CompRunEsta runInfo) throws Exception{
		
		if(runInfo != null){
			
			Long taskId = runInfo.getTaskId();
			String custNo = runInfo.getCustomerNo();
			
			deleteByTIdAndNo(taskId, custNo, CompRunCustomer.class);
			deleteByTIdAndNo(taskId, custNo, CompRunArea.class);
			deleteByTIdAndNo(taskId, custNo, CompRunSupply.class);
			deleteByTIdAndNo(taskId, custNo, CompRunPower.class);
			deleteByTIdAndNo(taskId, custNo, CompRunProduce.class);
			deleteByTIdAndNo(taskId, custNo, CompRunPrdSale.class);
			
			deleteByTIdAndNo(taskId, custNo, CompRunEstaLand.class);
			deleteByTIdAndNo(taskId, custNo, CompRunEstaDevingProj.class);
			deleteByTIdAndNo(taskId, custNo, CompRunEstaDevedProj.class);
			
			runInfoDAO.deleteRunEsta(runInfo);
		}
	}
	
	@Override
	public List<CompRunEstaLand> queryRunEstaLandByTIdAndNo(Long taskId, String custNo)
			throws Exception{
		return runInfoDAO.queryRunEstaLandByTIdAndNo(taskId, custNo);
	}
	
	@Override
	public CompRunEstaLand queryRunEstaLandById(Long id)
			throws Exception{
		return runInfoDAO.queryById(CompRunEstaLand.class, id);
	}

	@Override
	public void saveRunEstaLand(CompRunEstaLand compRunEstaLand) throws Exception {
		runInfoDAO.saveRunEstaLand(compRunEstaLand);
	}
	
	@Override
	public void saveRunEstaLands(Long taskId, String custNo, String jsonArray) throws Exception {
		this.deleteBeforeSave(taskId, custNo, jsonArray, CompRunEstaLand.class);
	}

	@Override
	public void deleteRunEstaLand(CompRunEstaLand compRunEstaLand) throws Exception {
		runInfoDAO.deleteRunEstaLand(compRunEstaLand);
	}
	
	@Override
	public List<CompRunEstaDevingProj> queryRunEstaDevingProjByTIdAndNo(Long taskId, String custNo)
			throws Exception{
		return runInfoDAO.queryRunEstaDevingProjByTIdAndNo(taskId, custNo);
	}
	
	@Override
	public CompRunEstaDevingProj queryRunEstaDevingProjById(Long id)
			throws Exception{
		return runInfoDAO.queryById(CompRunEstaDevingProj.class, id);
	}

	@Override
	public void saveRunEstaDevingProj(CompRunEstaDevingProj compRunEstaDevingProj) throws Exception {
		runInfoDAO.saveRunEstaDevingProj(compRunEstaDevingProj);
	}
	
	@Override
	public void saveRunEstaDevingProjs(Long taskId, String custNo, String jsonArray) throws Exception {
		this.deleteBeforeSave(taskId, custNo, jsonArray, CompRunEstaDevingProj.class);
	}

	@Override
	public void deleteRunEstaDevingProj(CompRunEstaDevingProj compRunEstaDevingProj) throws Exception {
		runInfoDAO.deleteRunEstaDevingProj(compRunEstaDevingProj);
	}
	
	@Override
	public List<CompRunEstaDevedProj> queryRunEstaDevedProjByTIdAndNo(Long taskId, String custNo)
			throws Exception{
		return runInfoDAO.queryRunEstaDevedProjByTIdAndNo(taskId, custNo);
	}
	
	@Override
	public CompRunEstaDevedProj queryRunEstaDevedProjById(Long id)
			throws Exception{
		return runInfoDAO.queryById(CompRunEstaDevedProj.class, id);
	}

	@Override
	public void saveRunEstaDevedProj(CompRunEstaDevedProj compRunEstaDevedProj) throws Exception {
		runInfoDAO.saveRunEstaDevedProj(compRunEstaDevedProj);
	}
	
	@Override
	public void saveRunEstaDevedProjs(Long taskId, String custNo, String jsonArray) throws Exception {
		this.deleteBeforeSave(taskId, custNo, jsonArray, CompRunEstaDevedProj.class);
	}

	@Override
	public void deleteRunEstaDevedProj(CompRunEstaDevedProj compRunEstaDevedProj) throws Exception {
		runInfoDAO.deleteRunEstaDevedProj(compRunEstaDevedProj);
	}
	
	
	@Override
	public CompRunGeneral queryRunGeneralByTIdAndNo(Long taskId, String customerNo) throws Exception {
		return runInfoDAO.queryRunGeneralByTIdAndNo(taskId, customerNo);
	}

	@Override
	public CompRunGeneral saveRunGeneral(CompRunGeneral compRunGeneral) throws Exception {
		return runInfoDAO.saveRunGeneral(compRunGeneral);
	}
	
	@Override
	public CompRunGeneral saveRunGeneral(String jsondata) throws Exception {
		
		if(StringUtil.isEmpty(jsondata))
			return null;
		
		JSONObject json = JSONObject.fromObject(jsondata);
		CompRunGeneral newObj = (CompRunGeneral) JSONObject.toBean(json,
				CompRunGeneral.class);
		
		CompRunGeneral oldObj = runInfoDAO.queryRunGeneralByTIdAndNo(newObj.getTaskId(),
				newObj.getCustomerNo());
		
		if(oldObj != null){
			BeanUtil.mergeProperties(newObj, oldObj, json);
		}else
			oldObj = newObj;
		
		return runInfoDAO.saveRunGeneral(oldObj);
	}
	
	@Override
	public void deleteRunGeneral(CompRunGeneral runInfo) throws Exception{
		
		if(runInfo != null){
			
			Long taskId = runInfo.getTaskId();
			String custNo = runInfo.getCustomerNo();
			
			deleteByTIdAndNo(taskId, custNo, CompRunCustomer.class);
			deleteByTIdAndNo(taskId, custNo, CompRunArea.class);
			deleteByTIdAndNo(taskId, custNo, CompRunSupply.class);
			deleteByTIdAndNo(taskId, custNo, CompRunPower.class);
			deleteByTIdAndNo(taskId, custNo, CompRunPrdSale.class);
			
			runInfoDAO.deleteRunGeneral(runInfo);
		}
	}
	
	
	private void deleteByTIdAndNo(Long taskId, String custNo, Class<?> clazz) throws Exception {
		
		//根据任务号和客户号查询
		String query = "taskId=:taskId and customerNo=:customerNo";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("taskId", taskId);
		params.put("customerNo", custNo);
		
		List<?> list = runInfoDAO.query(clazz, query, params);
		
		//删除
		if(!CollectionUtils.isEmpty(list)){
			for(Object obj : list)
				runInfoDAO.deleteEntity(obj);
		}
		
	}

	private void deleteBeforeSave(Long taskId, String custNo, String jsonArray, Class<?> clazz) throws Exception {
		
		String query = "taskId=:taskId and customerNo=:custNo";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("taskId", taskId);
		params.put("custNo", custNo);
		
		List<?> list = runInfoDAO.query(clazz, query, params);
		//先删除
		if(!CollectionUtils.isEmpty(list)){
			for(Object obj : list)
				runInfoDAO.deleteEntity(obj);
		}
		//后插入
		JSONArray array = JSONArray.fromObject(jsonArray);
		for(int i=0;i<array.size();i++){
			Object obj = JSONObject.toBean(array.getJSONObject(i), clazz);
			runInfoDAO.saveEntity(obj);
		}
	}
	
}
