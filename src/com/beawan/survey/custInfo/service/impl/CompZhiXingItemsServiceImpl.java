package com.beawan.survey.custInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.custInfo.dao.CompZhiXingItemsDao;
import com.beawan.survey.custInfo.bean.CompZhiXingItems;
import com.beawan.survey.custInfo.service.CompZhiXingItemsService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.custInfo.bean.CompZhiXingItems;

/**
 * @author yzj
 */
@Service("compZhiXingItemsService")
public class CompZhiXingItemsServiceImpl extends BaseServiceImpl<CompZhiXingItems> implements CompZhiXingItemsService{
    /**
    * 注入DAO
    */
    @Resource(name = "compZhiXingItemsDao")
    public void setDao(BaseDao<CompZhiXingItems> dao) {
        super.setDao(dao);
    }
    @Resource
    public CompZhiXingItemsDao compZhiXingItemsDao;
}
