package com.beawan.survey.custInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.custInfo.dao.CompWmWarnSignalDao;
import com.beawan.survey.custInfo.bean.CompWmWarnSignal;
import com.beawan.survey.custInfo.service.CompWmWarnSignalService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.custInfo.bean.CompWmWarnSignal;

/**
 * @author yzj
 */
@Service("compWmWarnSignalService")
public class CompWmWarnSignalServiceImpl extends BaseServiceImpl<CompWmWarnSignal> implements CompWmWarnSignalService{
    /**
    * 注入DAO
    */
    @Resource(name = "compWmWarnSignalDao")
    public void setDao(BaseDao<CompWmWarnSignal> dao) {
        super.setDao(dao);
    }
    @Resource
    public CompWmWarnSignalDao compWmWarnSignalDao;
}
