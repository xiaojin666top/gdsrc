package com.beawan.survey.custInfo.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Service;

import com.beawan.analysis.finansis.common.FinaRowData;
import com.beawan.base.entity.SysDic;
import com.beawan.base.service.ISysDicSV;
import com.beawan.base.service.ITableSubjCodeSV;
import com.beawan.common.SysConstants;
import com.beawan.common.util.SystemUtil;
import com.beawan.customer.service.ICusFSToolSV;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.beawan.library.bean.IndustryQRadio;
import com.beawan.library.service.IIndustrySV;
import com.beawan.survey.custInfo.bean.CompBase;
import com.beawan.survey.custInfo.bean.CompFinance;
import com.beawan.survey.custInfo.bean.CompFinanceMainSub;
import com.beawan.survey.custInfo.bean.CompFinanceOperCF;
import com.beawan.survey.custInfo.bean.CompFinanceTax;
import com.beawan.survey.custInfo.dao.ICompBaseDAO;
import com.beawan.survey.custInfo.dao.ICompFinanceDAO;
import com.beawan.survey.custInfo.dao.ICompFinanceMainSubDAO;
import com.beawan.survey.custInfo.dao.ICompFinanceOperCFDAO;
import com.beawan.survey.custInfo.dao.ICompFinanceTaxDAO;
import com.beawan.survey.custInfo.service.ICompFinanceSV;
import com.beawan.survey.loanInfo.bean.GuaraCom;
import com.beawan.survey.loanInfo.service.IGuaraInfoSV;
import com.beawan.task.bean.Task;
import com.beawan.task.dao.ITaskDAO;
import com.platform.util.BeanUtil;
import com.platform.util.StringUtil;

@Service("compFinanceSV")
public class CompFinanceSVImpl implements ICompFinanceSV {

	@Resource
	private ICompFinanceDAO compFinanceDAO;
	
	@Resource
	private ICompFinanceOperCFDAO compFinanceOperCFDAO;
	
	@Resource
	private ICompFinanceTaxDAO compFinanceTaxDAO;
	
	@Resource
	private ICompFinanceMainSubDAO compFinanceMainSubDAO;

	@Resource
	private ITaskDAO taskDAO;
	
	@Resource
	private ICompBaseDAO compBaseDAO;

	@Resource
	private ITableSubjCodeSV tableSubjCodeSV;
	
	@Resource
	private ICmisInInvokeSV cmisInInvokeSV;
	
	@Resource
	private IIndustrySV industrySV;
	
	@Resource
	private ISysDicSV sysDicSV;
	
	@Resource
	private IGuaraInfoSV guaraInfoSV;
	
	@Resource
	private ICusFSToolSV cusFSToolSV;
	
	@Override
	public CompFinance findCompFinanceById(Long id) throws Exception {
		return compFinanceDAO.selectByPrimaryKey(id);
	}

	@Override
	public CompFinance findCompFinanceByTaskId(Long taskId) throws Exception {
		return compFinanceDAO.selectSingleByProperty("taskId", taskId);
	}

	@Override
	public CompFinance findCompFinanceByTIdAndNo(Long taskId, String custNo) throws Exception {
		return compFinanceDAO.queryByTaskIdAndCustNo(taskId, custNo);
	}

	@Override
	public CompFinance saveCompFinance(CompFinance data) throws Exception {
		return compFinanceDAO.saveOrUpdate(data);
	}
	
	@Override
	public void saveCompFinance(String jsondata) throws Exception {
		
		if(StringUtil.isEmpty(jsondata))
			return;
		
		JSONObject json = JSONObject.fromObject(jsondata);
		CompFinance finance = (CompFinance) JSONObject.toBean(json,
				CompFinance.class);
		CompFinance oldFinance = compFinanceDAO.queryByTaskIdAndCustNo(finance.getTaskId(),
				finance.getCustomerNo());
		
		if(oldFinance != null){
			BeanUtil.mergeProperties(finance, oldFinance, json);
		}else
			oldFinance = finance;
		
		compFinanceDAO.saveOrUpdate(oldFinance);
	}

	@Override
	public void deleteCompFinance(CompFinance data) throws Exception {
		compFinanceDAO.deleteEntity(data);
	}
	
	@Override
	public CompFinanceMainSub findFinaMainSubById(Long id) throws Exception {
		return compFinanceMainSubDAO.selectByPrimaryKey(id);
	}
	
	@Override
	public CompFinanceMainSub findFinaMainSubByTIdAndNo(Long taskId,
			String custNo) throws Exception {
		CompFinanceMainSub finaMainSub = compFinanceMainSubDAO.queryByTaskIdAndCustNo(taskId, custNo);
		if(finaMainSub == null){
			finaMainSub = new CompFinanceMainSub();
			finaMainSub.setTaskId(taskId);
			finaMainSub.setCustomerNo(custNo);
		}
		return finaMainSub;
	}

	@Override
	public void saveFinaMainSub(CompFinanceMainSub data) throws Exception {
		compFinanceMainSubDAO.saveOrUpdate(data);
	}
	
	@Override
	public void saveFinaMainSub(String jsondata) throws Exception {
		
		if(StringUtil.isEmpty(jsondata))
			return;
		
		JSONObject json = JSONObject.fromObject(jsondata);
		CompFinanceMainSub finaMainSub = (CompFinanceMainSub) JSONObject.toBean(json,
				CompFinanceMainSub.class);
		CompFinanceMainSub oldFinaMainSub = compFinanceMainSubDAO.queryByTaskIdAndCustNo(finaMainSub.getTaskId(),
				finaMainSub.getCustomerNo());
		
		if(oldFinaMainSub != null){
			BeanUtil.mergeProperties(finaMainSub, oldFinaMainSub, json);
		}else
			oldFinaMainSub = finaMainSub;
		
		compFinanceMainSubDAO.saveOrUpdate(oldFinaMainSub);
	}

	@Override
	public void deleteFinaMainSub(CompFinanceMainSub data) throws Exception {
		compFinanceMainSubDAO.deleteEntity(data);
	}
	
	@Override
	public List<CompFinanceOperCF>  findFinanceOperCFByTIdAndNo(Long taskId,
			String customerNo) throws Exception {
		return compFinanceOperCFDAO.queryByTaskIdAndCustNo(taskId, customerNo);
	}
	
	@Override
	public CompFinanceOperCF saveFinanceOperCF(CompFinanceOperCF data) throws Exception {
		return compFinanceOperCFDAO.saveOrUpdate(data);
	}
	
	@Override
	public void saveFinanceOperCFs(Long taskId, String customerNo,
			String jsonArray) throws Exception {
		
		List<CompFinanceOperCF>  financeOperCFs = compFinanceOperCFDAO.queryByTaskIdAndCustNo(taskId, customerNo);
		for (CompFinanceOperCF financeOperCF : financeOperCFs) {
			compFinanceOperCFDAO.deleteEntity(financeOperCF);
		}
		
		JSONArray array = JSONArray.fromObject(jsonArray);
		if (array != null && array.size() > 0) {
			for (int i = 0; i < array.size(); i++) {
				JSONObject dataStr = JSONObject.fromObject(array.get(i));
				CompFinanceOperCF data = (CompFinanceOperCF) JSONObject.toBean(dataStr,CompFinanceOperCF.class);  
				data.setTaskId(taskId);
				data.setCustomerNo(customerNo);
				compFinanceOperCFDAO.saveOrUpdate(data);
			}
		}
	}
	
	@Override
	public void deleteFinanceOperCF(CompFinanceOperCF data) throws Exception {
		compFinanceOperCFDAO.deleteEntity(data);
	}
	
	@Override
	public List<CompFinanceTax>  findFinanceTaxByTIdAndNo(Long taskId,
			String customerNo) throws Exception {
		return compFinanceTaxDAO.queryByTaskIdAndCustNo(taskId, customerNo);
	}
	
	@Override
	public CompFinanceTax saveFinanceTax(CompFinanceTax data) throws Exception {
		return compFinanceTaxDAO.saveOrUpdate(data);
	}
	
	@Override
	public void saveFinanceTaxes(Long taskId, String customerNo,
			String jsonArray) throws Exception {
		
		List<CompFinanceTax>  financeTaxs = compFinanceTaxDAO.queryByTaskIdAndCustNo(taskId,
				customerNo);
		for (CompFinanceTax financeTax : financeTaxs) {
			compFinanceTaxDAO.deleteEntity(financeTax);
		}
		
		JSONArray array = JSONArray.fromObject(jsonArray);
		if (array != null && array.size() > 0) {
			for (int i = 0; i < array.size(); i++) {
				JSONObject dataStr = JSONObject.fromObject(array.get(i));
				CompFinanceTax data = (CompFinanceTax) JSONObject.toBean(dataStr,CompFinanceTax.class);  
				data.setTaskId(taskId);
				data.setCustomerNo(customerNo);
				compFinanceTaxDAO.saveOrUpdate(data);
			}
		}
		
	}
	
	@Override
	public void deleteFinanceTax(CompFinanceTax data) throws Exception {
		compFinanceTaxDAO.deleteEntity(data);
	}
	
	@Override
	public List<FinaRowData> findFinanceIndexs(long taskId) throws Exception {
		
		Task task = taskDAO.getTaskById(taskId);
		
		List<String> dateList = SystemUtil.genFinaDateList(task.getYear(),
				task.getMonth(), true, false);
		
		return findFinanceIndexs(task.getCustomerNo(), dateList, task.getIndustry());
	}
	
	@Override
	public List<FinaRowData> findFinanceIndexs(String customerNo, List<String> dateList,
			String industry) throws Exception {
		
		List<Map<String, Double>> result = cusFSToolSV.findRepDataByType(customerNo,
				dateList, SysConstants.CmisFianRepType.RATIO_INDEX);
		
		return findFinanceIndexs(customerNo, industry, result);
	}
	
	@Override
	public List<FinaRowData> findFinanceIndexs(String customerNo, String industry,
			List<Map<String, Double>> cmisRatioIndexData) throws Exception {
		
		List<SysDic> indexList = sysDicSV.findByType("STD_FINA_MAIN_INDEX");
		
		Map<String, IndustryQRadio> radioMap = industrySV.getQuotaByEnName(industry);
		
		List<FinaRowData> list = new ArrayList<FinaRowData>();
		
		for(SysDic dic : indexList){
			
			FinaRowData row = new FinaRowData();
			row.setItemName(dic.getCnName());
			
			String subject = dic.getEnName();
			
			if(!StringUtil.isEmpty(subject)){
				
				row.setFirstYear(cmisRatioIndexData.get(0).get(subject));
				row.setSecondYear(cmisRatioIndexData.get(1).get(subject));
				row.setThirdYear(cmisRatioIndexData.get(2).get(subject));
				row.setFourthYear(cmisRatioIndexData.get(3).get(subject));
				//根据指标名称获得行业平均值
				IndustryQRadio iq = radioMap.get(dic.getCnName());
				//流动比率或速动比率
				if(subject.equals("600") || subject.equals("602")){
					row.setItemName(dic.getCnName().replace("％", "倍"));
					if(iq != null && iq.getAverageValue() != null)
						row.setIndusAvg(iq.getAverageValue()/100);
				}else{
					if(iq != null)
						row.setIndusAvg(iq.getAverageValue());
				}
			}
			
			list.add(row);
		}
		
		return list;
	}
	
	@Override
	public List<FinaRowData> findFinanceCashFlow(Long taskId, String cusNo) throws Exception {
		
		Task task = taskDAO.getTaskById(taskId);
		
		List<String> dateList = SystemUtil.genFinaDateList(task.getYear(),
				task.getMonth(), true, false);
		
		List<Map<String, Double>> result = cusFSToolSV.findRepDataByType(cusNo,
				dateList, SysConstants.CmisFianRepType.CASH_FLOW);
		
		return findFinanceCashFlow(result);
	}
	
	@Override
	public List<FinaRowData> findFinanceCashFlow(List<Map<String, Double>> cmisCashFlowData) throws Exception {
		
		List<SysDic> indexList = sysDicSV.findByType("STD_FINA_CASH_FLOW");
		
		List<FinaRowData> list = new ArrayList<FinaRowData>();
		for(SysDic dic : indexList){
			FinaRowData row = new FinaRowData();
			row.setItemName(dic.getCnName());
			
			if(dic.getEnName() != null){
				row.setFirstYear(cmisCashFlowData.get(0).get(dic.getEnName()));
				row.setSecondYear(cmisCashFlowData.get(1).get(dic.getEnName()));
				row.setThirdYear(cmisCashFlowData.get(2).get(dic.getEnName()));
				row.setFourthYear(cmisCashFlowData.get(3).get(dic.getEnName()));
			}
			
			list.add(row);
		}
		
		return list;
	}
	
	
	
	@Override
	public List<FinaRowData> findFinanceIncome(Long taskId, String cusNo) throws Exception {
		
		Task task = taskDAO.getTaskById(taskId);
		
		List<String> dateList = SystemUtil.genFinaDateList(task.getYear(),
				task.getMonth(), true, false);
		
		List<Map<String, Double>> result = cusFSToolSV.findRepDataByType(cusNo,
				dateList, SysConstants.CmisFianRepType.INCOME);
		
		return findFinanceIncome(result);
	}
	
	@Override
	public List<FinaRowData> findFinanceIncome(List<Map<String, Double>> cmisIncomeData) throws Exception {
		
		List<SysDic> indexList = sysDicSV.findByType("STD_FINA_INCOME");
		
		List<FinaRowData> rowDataList = new ArrayList<FinaRowData>();
		for(int i=0;i<indexList.size();i++){
			
			SysDic dic = indexList.get(i);
			
			FinaRowData row = new FinaRowData();
			row.setItemName(dic.getCnName());
			
			if(!StringUtil.isEmptyString(dic.getEnName())){
				row.setFirstYear(cmisIncomeData.get(0).get(dic.getEnName()));
				row.setSecondYear(cmisIncomeData.get(1).get(dic.getEnName()));
				row.setThirdYear(cmisIncomeData.get(2).get(dic.getEnName()));
				row.setFourthYear(cmisIncomeData.get(3).get(dic.getEnName()));
			}else{
				if(rowDataList.get(0).getFirstYear() != null
						&& rowDataList.get(1).getFirstYear() != null)
					row.setFirstYear(rowDataList.get(1).getFirstYear()/rowDataList.get(0).getFirstYear()*100);
				if(rowDataList.get(0).getSecondYear() != null
						&& rowDataList.get(1).getSecondYear() != null)
					row.setSecondYear(rowDataList.get(1).getSecondYear()/rowDataList.get(0).getSecondYear()*100);
				if(rowDataList.get(0).getThirdYear() != null
						&& rowDataList.get(1).getThirdYear() != null)
					row.setThirdYear(rowDataList.get(1).getThirdYear()/rowDataList.get(0).getThirdYear()*100);
				if(rowDataList.get(0).getFourthYear() != null
						&& rowDataList.get(1).getFourthYear() != null)
					row.setFourthYear(rowDataList.get(1).getFourthYear()/rowDataList.get(0).getFourthYear()*100);
			}
			
			rowDataList.add(row);
		}
		
		return rowDataList;
	}
	
	@Override
	public Map<String,Object> getFinaRepInfo(long taskId, String customerNo) throws Exception{
		
		int finaRepYear = 0, finaRepMonth = 0;
		String industry = null, cusFlag = null;
		Task task = this.taskDAO.getTaskById(taskId);
		//借款人
		if(task != null && task.getCustomerNo().equals(customerNo)){
			finaRepYear = task.getYear();
			finaRepMonth = task.getMonth();
			industry = task.getIndustry();
			cusFlag = SysConstants.CusFlag.BORROWER;
		}else{
			GuaraCom guaraCom = guaraInfoSV.findGuaraCom(taskId, customerNo);
			//一般企业保证人
			if(guaraCom != null){
				String[] dateArr = guaraCom.getFinaRepYear().split("-"); 
				finaRepYear = Integer.parseInt(dateArr[0]);
				finaRepMonth = Integer.parseInt(dateArr[1]);
				
				CompBase compBase = compBaseDAO.queryByCustNo(customerNo);
				if(null!=compBase) {
				industry = compBase.getIndustryCode();
				}
				cusFlag = SysConstants.CusFlag.GUARA_COM;
			}
		}
		
		Map<String,Object> finaRepInfo = new HashMap<String,Object>();
		finaRepInfo.put("finaRepYear", finaRepYear);
		finaRepInfo.put("finaRepMonth", finaRepMonth);
		finaRepInfo.put("industry", industry);
		finaRepInfo.put("cusFlag", cusFlag);
		
		return finaRepInfo;
	}
	
}
