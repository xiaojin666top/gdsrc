package com.beawan.survey.custInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.custInfo.dao.CompShiXinItemsDao;
import com.beawan.survey.custInfo.bean.CompShiXinItems;
import com.beawan.survey.custInfo.service.CompShiXinItemsService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.custInfo.bean.CompShiXinItems;

/**
 * @author yzj
 */
@Service("compShiXinItemsService")
public class CompShiXinItemsServiceImpl extends BaseServiceImpl<CompShiXinItems> implements CompShiXinItemsService{
    /**
    * 注入DAO
    */
    @Resource(name = "compShiXinItemsDao")
    public void setDao(BaseDao<CompShiXinItems> dao) {
        super.setDao(dao);
    }
    @Resource
    public CompShiXinItemsDao compShiXinItemsDao;
}
