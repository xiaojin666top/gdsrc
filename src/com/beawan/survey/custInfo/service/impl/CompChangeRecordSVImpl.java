package com.beawan.survey.custInfo.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.custInfo.bean.CompChangeRecord;
import com.beawan.survey.custInfo.service.ICompChangeRecordSV;


@Service("compChangeRecordSV")
public class CompChangeRecordSVImpl extends BaseServiceImpl<CompChangeRecord> implements ICompChangeRecordSV{
	/**
	    * 注入DAO
	    */
	    @Resource(name = "compChangeRecordDAO")
	    public void setDao(BaseDao<CompChangeRecord> dao) {
	        super.setDao(dao);
	    }
	
}
