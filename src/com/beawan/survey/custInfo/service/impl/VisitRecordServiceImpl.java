package com.beawan.survey.custInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.custInfo.dao.VisitRecordDao;
import com.beawan.survey.custInfo.entity.VisitRecord;
import com.beawan.survey.custInfo.service.VisitRecordService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import com.beawan.survey.custInfo.entity.VisitRecord;

/**
 * @author yzj
 */
@Service("visitRecordService")
public class VisitRecordServiceImpl extends BaseServiceImpl<VisitRecord> implements VisitRecordService{
    /**
    * 注入DAO
    */
    @Resource(name = "visitRecordDao")
    public void setDao(BaseDao<VisitRecord> dao) {
        super.setDao(dao);
    }
    @Resource
    public VisitRecordDao visitRecordDao;
	
    @Override
	public VisitRecord getBySerNoAndType(String serNo, String type) {
    	String query = " serNo =:serNo AND type =:type";
    	Map<String, Object> params = new HashMap<>();
    	params.put("serNo", serNo);
    	params.put("type", type);
		return visitRecordDao.selectSingle(query, params);
	}

}
