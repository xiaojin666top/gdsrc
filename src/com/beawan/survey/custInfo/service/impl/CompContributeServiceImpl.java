package com.beawan.survey.custInfo.service.impl;

import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.dao.impl.CusBaseDAOImpl;
import com.beawan.survey.custInfo.dao.CompContributeDao;
import com.beawan.survey.custInfo.entity.CompContribute;
import com.beawan.survey.custInfo.service.CompContributeService;
import com.beawan.task.service.ITaskSV;
import com.platform.util.DateUtil;
import com.platform.util.JdbcUtil;
import com.platform.util.StringUtil;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

/**
 * @author yzj
 */
@Service("compContributeService")
public class CompContributeServiceImpl extends BaseServiceImpl<CompContribute> implements CompContributeService{
    /**
    * 注入DAO
    */
    @Resource(name = "compContributeDao")
    public void setDao(BaseDao<CompContribute> dao) {
        super.setDao(dao);
    }
    @Resource
    public CompContributeDao compContributeDao;
    @Resource
    private CusBaseDAOImpl cusBaseDAO;
    @Resource
	private ITaskSV taskSV;
    
    
    @Override
    public CompContribute syncContributeFromCMIS(String serNo, String customerNo) throws Exception{
        
    	CompContribute result = new CompContribute();
        result.setSerNo(serNo);
        result.setApplyDate(DateUtil.getNowYmdStr());
        result.setCreater("CMIS");
        result.setUpdater("CMIS");
        result.setCreateTime(DateUtil.getNowTimestamp());
        result.setUpdateTime(DateUtil.getNowTimestamp());
        
    	CusBase cusBase = cusBaseDAO.selectSingleByProperty("customerNo", customerNo);
        if (cusBase == null || StringUtil.isEmptyString(cusBase.getMfCustomerNo())){
        	result.setDepositInfo("该企业在我行暂无存款信息！");
        	return compContributeDao.saveOrUpdate(result);
        }
        
        String mfCustomerNo = cusBase.getMfCustomerNo();
        // 获取企业存款、贷款余额
        String sql = "SELECT A.mfCustomerNo, B.customerName, A.bankBalance bankBalance, B.loanBalance loanBalance FROM" +
                " (SELECT CUST_NO mfCustomerNo,SUM(YE) bankBalance FROM" +
                " (SELECT T.TD_CUST_NO CUST_NO,SUM(T.TD_ACTU_AMT) YE FROM CBOD.TDACNACN T" +
                " WHERE SUBSTR(T.TD_PDP_CODE,6,1)='1' AND T.TD_ACCT_STS='01' GROUP BY T.TD_CUST_NO" +
                " UNION ALL" +
                " SELECT S.SA_CUST_NO CUST_NO,SUM(SA.SA_ACCT_BAL) YE FROM CBOD.SAACNACN S" +
                " LEFT JOIN CBOD.SAACNAMT SA ON S.SA_ACCT_NO = SA.FK_SAACN_KEY" +
                " WHERE SUBSTR(S.SA_PDP_CODE,6,1)='1' AND SA.SA_DDP_ACCT_STS='01' GROUP BY S.SA_CUST_NO)" +
                " GROUP BY CUST_NO) A LEFT JOIN" +
                " (SELECT L.LN_CUST_NO lnCustomerNo, L.LN_CUST_NAME customerName, SUM(L.LN_LN_BAL) loanBalance" +
                " FROM CBOD.LNLNSLNS L WHERE L.LN_CUST_TYP='2' AND L.ln_ln_bal>0 AND L.LN_APCL_FLG<>'Y'" +
                " AND L.LN_BELONG_INSTN_COD<>'320622900' AND L.ln_busn_typ NOT IN ('680')" +
                " GROUP BY L.LN_CUST_NO, L.LN_CUST_NAME) B ON B.lnCustomerNo = A.mfCustomerNo" +
                " WHERE A.mfCustomerNo= '"+mfCustomerNo+"'";
        List<Map<String, Object>> entityList = JdbcUtil.executeQuery(sql, Constants.DataSource.TCE_DS);
        if (!CollectionUtils.isEmpty(entityList)){
            Map<String, Object> resultMap = entityList.get(0);
            Object bankBalance = resultMap.get("bankbalance");
            if (bankBalance != null)
                result.setBankBalance(bankBalance.toString());
            Object loanBalance = resultMap.get("loanbalance");
            if (loanBalance != null)
                result.setLoanBalance(loanBalance.toString());
        }

        // 定期余额,日均
        sql = "SELECT T.TD_CUST_NO CUST_NO,SUM(T.TD_ACTU_AMT) timeDeposit FROM CBOD.TDACNACN T" +
            " WHERE SUBSTR(T.TD_PDP_CODE,6,1)='1' AND T.TD_ACCT_STS='01' AND T.TD_CUST_NO='" + mfCustomerNo + "'" +
            " GROUP BY T.TD_CUST_NO";
        entityList = JdbcUtil.executeQuery(sql, Constants.DataSource.TCE_DS);
        if (!CollectionUtils.isEmpty(entityList)){
            Map<String, Object> resultMap = entityList.get(0);
            Object timeDeposit = resultMap.get("timedeposit");
            if (timeDeposit != null)
                result.setTimeDeposit(timeDeposit.toString());
        }

        // 活期余额,日均
        sql = "SELECT S.SA_CUST_NO CUST_NO,SUM(SA.SA_ACCT_BAL) currentDeposit FROM CBOD.SAACNACN S" +
            " LEFT JOIN CBOD.SAACNAMT SA ON S.SA_ACCT_NO = SA.FK_SAACN_KEY" +
            " WHERE SUBSTR(S.SA_PDP_CODE,6,1)='1' AND SA.SA_DDP_ACCT_STS='01' AND S.SA_CUST_NO='" + mfCustomerNo + "'" +
            " GROUP BY S.SA_CUST_NO";
        entityList = JdbcUtil.executeQuery(sql, Constants.DataSource.TCE_DS);
        if (!CollectionUtils.isEmpty(entityList)){
            Map<String, Object> resultMap = entityList.get(0);
            Object timeDeposit = resultMap.get("currentdeposit");
            if (timeDeposit != null)
                result.setTimeDeposit(timeDeposit.toString());
        }
        //存款情况
        String syncDate = result.getApplyDate();// yyyyMMdd
        String customerName = cusBase.getCustomerName();
        String bankBalance = result.getBankBalance();
        String loanBalance = result.getLoanBalance();
        String deposit = "截止" + syncDate.substring(0,4)+ "年" + syncDate.substring(4,6) +"月" + syncDate.substring(6,8) + "日，";
        if (Double.parseDouble(bankBalance)<=0 && Double.parseDouble(loanBalance) <=0){
            deposit += customerName + "在我行暂无存款信息！";
            result.setDepositInfo(deposit);
            return compContributeDao.saveOrUpdate(result);
        }
        if (Double.parseDouble(bankBalance) > 10000.00d){
            bankBalance = String.format("%.2f", Double.parseDouble(bankBalance)/10000.00d) + "万";
        }
        if (Double.parseDouble(loanBalance) > 10000.00d){
            loanBalance = String.format("%.2f", Double.parseDouble(loanBalance)/10000.00d) + "万";
        }
        String timeDeposit = result.getTimeDeposit();
        if (Double.parseDouble(timeDeposit) > 10000.00d){
            timeDeposit = String.format("%.2f", Double.parseDouble(timeDeposit)/10000.00d) + "万";
        }
        String currentDeposit = result.getCurrentDeposit();
        if (Double.parseDouble(currentDeposit) > 10000.00d){
            currentDeposit = String.format("%.2f", Double.parseDouble(currentDeposit)/10000.00d) + "万";
        }
        deposit += customerName + "在我行存款余额为："+ bankBalance +"元（其中定期存款为：" + timeDeposit + "元，活期存款为："
                + currentDeposit + "元），在我行贷款余额为：" + loanBalance + "元。";
        result.setDepositInfo(deposit);
        return compContributeDao.saveOrUpdate(result);
    }
}
