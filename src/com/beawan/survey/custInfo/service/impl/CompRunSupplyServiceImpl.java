package com.beawan.survey.custInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.custInfo.dao.CompRunSupplyDao;
import com.beawan.survey.custInfo.bean.CompRunSupply;
import com.beawan.survey.custInfo.service.CompRunSupplyService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.custInfo.bean.CompRunSupply;

/**
 * @author yzj
 */
@Service("compRunSupplyService")
public class CompRunSupplyServiceImpl extends BaseServiceImpl<CompRunSupply> implements CompRunSupplyService{
    /**
    * 注入DAO
    */
    @Resource(name = "compRunSupplyDao")
    public void setDao(BaseDao<CompRunSupply> dao) {
        super.setDao(dao);
    }
    @Resource
    public CompRunSupplyDao compRunSupplyDao;
}
