package com.beawan.survey.custInfo.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.core.Pagination;
import com.beawan.survey.custInfo.dao.CompInvestmentDao;
import com.beawan.survey.custInfo.entity.CompInvestment;
import com.beawan.survey.custInfo.service.CompInvestmentService;

/**
 * @author yzj
 */
@Service("compInvestmentService")
public class CompInvestmentServiceImpl extends BaseServiceImpl<CompInvestment> implements CompInvestmentService{
    /**
    * 注入DAO
    */
    @Resource(name = "compInvestmentDao")
    public void setDao(BaseDao<CompInvestment> dao) {
        super.setDao(dao);
    }
    @Resource
    public CompInvestmentDao compInvestmentDao;
    
    
	@Override
	public Pagination<CompInvestment> getInvestPager(String custNo, Integer page,
			Integer pageSize) {
		StringBuilder sql = new StringBuilder();
		Map<String, Object> params = new HashMap<String, Object>();
		sql.append("SELECT KEY_NO keyNo,NAME name,REGIST_CAPI registCapi,FUNDED_RATIO fundedRatio,")
			.append(" ECON_KIND econKind,OPER_NAME operName,START_DATE startDate,STATE state,")
			.append(" ACTUAL_INVESTMENT_AMOUNT actualInvestmentAmount,BANK_LOANS bankLoans")
			.append(" FROM CM_INVESTMENT")
			.append(" WHERE CUSTOMER_NO=:custNo");
		params.put("custNo", custNo);
		return compInvestmentDao.findCustSqlPagination(CompInvestment.class, sql.toString(), params, page, pageSize);
	}
	
	@Override
	public void saveInvestLoan(CompInvestment compInvestment){
		CompInvestment entity = compInvestmentDao.findByPrimaryKey(compInvestment.getKeyNo());
		entity.setBankLoans(compInvestment.getBankLoans());
		entity.setActualInvestmentAmount(compInvestment.getActualInvestmentAmount());
		entity.setUpdater(compInvestment.getUpdater());
		entity.setUpdateTime(compInvestment.getUpdateTime());
		entity.setStatus(compInvestment.getStatus());
		compInvestmentDao.saveOrUpdate(entity);
	}
	
}
