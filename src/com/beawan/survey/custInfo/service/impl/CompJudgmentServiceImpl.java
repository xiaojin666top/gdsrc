package com.beawan.survey.custInfo.service.impl;

import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.core.Pagination;
import com.beawan.qcc.dto.CaseRoleDto;
import com.beawan.qcc.dto.QccJudgmentDto;
import com.beawan.survey.custInfo.dao.CompJudgmentDao;
import com.beawan.survey.custInfo.dto.CompJudgStatDto;
import com.beawan.survey.custInfo.dto.CompJudgmentDto;
import com.beawan.survey.custInfo.entity.CompJudgment;
import com.beawan.survey.custInfo.service.CompJudgmentService;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import com.platform.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import com.beawan.survey.custInfo.entity.CompJudgment;

/**
 * @author yzj
 */
@Service("compJudgmentService")
public class CompJudgmentServiceImpl extends BaseServiceImpl<CompJudgment> implements CompJudgmentService{
    /**
    * 注入DAO
    */
    @Resource(name = "compJudgmentDao")
    public void setDao(BaseDao<CompJudgment> dao) {
        super.setDao(dao);
    }
    @Resource
    public CompJudgmentDao compJudgmentDao;
    
	@Override
	public Pagination<CompJudgmentDto> getJudgmentPager(String custNo, String caseType, String caseReason, 
			String loanTime, Integer page, Integer pageSize) throws Exception {

		Date date = DateUtil.parseDate(loanTime);
		String startDate = DateUtil.format(DateUtil.addMonth(date, -12), Constants.DATE_MASK);
		StringBuilder sql = new StringBuilder();
		Map<String, Object> params = new HashMap<>();
		sql.append("select a.ID id,a.CASE_NAME caseName,a.CASE_REASON caseReason,")
			.append(" a.CASE_NO caseNo,a.CASE_ROLE caseRole,a.AMOUNT amount,")
			.append(" (case when a.CASE_TYPE='ms' then '民事'")
			.append(" when a.CASE_TYPE='xs' then '刑事'")
			.append(" when a.CASE_TYPE='xz' then '行政'")
			.append(" when a.CASE_TYPE='zscq' then '知识产权'")
			.append(" when a.CASE_TYPE='pc' then '赔偿'")
			.append(" when a.CASE_TYPE='zx' then '执行' end) caseType,")
			.append(" a.UPDATE_DATE updateDate,a.SUBMIT_DATE submitDate")
			.append(" from CM_JUDGMENT a where a.CUSTOMER_NO='"+custNo+"' ");
		params.put("custNo", custNo);
		if(!StringUtils.isEmpty(caseType)){
			sql.append(" and a.CASE_TYPE='"+caseType+"' ");
			params.put("caseType", caseType);
		}
		if(!StringUtils.isEmpty(caseReason)){
			sql.append(" and a.CASE_REASON='"+caseReason+"' ");
			params.put("caseReason", caseReason);
		}
		if(!StringUtils.isEmpty(startDate)){
			sql.append(" and a.SUBMIT_DATE>='"+startDate+"' ");
			params.put("startDate", startDate);
		}
		if(!StringUtils.isEmpty(loanTime)){
			sql.append(" and a.SUBMIT_DATE<='"+loanTime+"' ");
			params.put("loanTime", loanTime);
		}
		String count = "select count(*) FROM (" + sql.toString() + ") tb3";
		sql.append(" order by a.UPDATE_DATE desc,a.IS_DEFENDANT desc");
		params.clear();
		Pagination<CompJudgmentDto> pager = compJudgmentDao
				.findCustSqlPagination(CompJudgmentDto.class, sql.toString(), count, params, page, pageSize);
		List<CompJudgmentDto> items = pager.getItems();
		if(CollectionUtils.isEmpty(items))
			return pager;
		for(CompJudgmentDto dto : items){
			String caseRole = dto.getCaseRole();
			List<CaseRoleDto> roleList = new Gson().fromJson(caseRole,
					new TypeToken<List<CaseRoleDto>>() {}.getType());
			dto.setRoleDto(roleList);
		}
		return pager;
	}
	
	
	public List<CompJudgmentDto> getNewestJudgeList(String loanTime, String custNo, String custName) throws Exception {
		//获取近1年  授信客户作为被告  最新的3条数据
		Date date = DateUtil.parseDate(loanTime);
		String startDate = DateUtil.format(DateUtil.addMonth(date, -12), Constants.DATE_MASK);
		List<CompJudgmentDto> newestJudgeList = compJudgmentDao.getNewestJudgeList(loanTime, startDate, custNo);
		this.formatCaseRole(newestJudgeList);
		return newestJudgeList;
	}
	
	private void formatCaseRole(List<CompJudgmentDto> judgList){
		if(CollectionUtils.isEmpty(judgList))
			return;
		for(CompJudgmentDto dto : judgList){
			List<CaseRoleDto> roleDto = dto.getRoleDto();
			if(CollectionUtils.isEmpty(roleDto))
				continue;
			List<String> prosecutorList = new ArrayList<>();//原告
			List<String> defendantList = new ArrayList<>();
			for(CaseRoleDto role : roleDto){
				if(role.getRole().contains("原告")){
					prosecutorList.add(role.getPerson());
				}else if(role.getRole().contains("被告")){
					defendantList.add(role.getPerson());
				}
			}
			dto.setProsecutorList(prosecutorList);
			dto.setDefendantList(defendantList);
		}
	}
	
	@Override
	public String getJudgSimAnaly(String loanTime, String custNo, String custName) throws Exception {
		Date date = DateUtil.parseDate(loanTime);
		String startDate = DateUtil.format(DateUtil.addMonth(date, -12), Constants.DATE_MASK);
		
		//裁判文书的分析口径
		StringBuilder judgAlaly = new StringBuilder(startDate + "至" + loanTime + "期间");
//				2017年7月至2020年7月，xx企业共发生涉诉纠纷案件Xxx起(涉及原告及被告)，其中xx企业作为被告共xx起，包括民事案件xx起，刑事案件xx起。
		//获取总的涉诉纠纷案件数
		List<CompJudgStatDto> sumList = compJudgmentDao.getJudgStatList(startDate, loanTime, custNo, false, false, false);
		CompJudgStatDto sumDto = sumList.get(0);
		Long sum = sumDto.getCount();
		if(sum==0){
			judgAlaly.append("," + custName + "未发生涉诉纠纷案件，司法信息方面表现良好");
			return judgAlaly.toString();
		}
		judgAlaly.append("," + custName + "共发生涉诉纠纷案件"+ sum +"起(涉及原告及被告)");
		//获取作为被告的数量
		List<CompJudgStatDto> defendantList = compJudgmentDao.getJudgStatList(startDate, loanTime, custNo, true, false, false);
		CompJudgStatDto defendantDto = defendantList.get(0);
		Long defendantCount = defendantDto.getCount();
		if(defendantCount==0){
			judgAlaly.append(",其中无作为被告案件。");
			return judgAlaly.toString();
		}
		judgAlaly.append("," + "其中"+custName+"作为被告方共" + defendantCount + "起,包括");
		//获取按事件类型统计数量
		List<CompJudgStatDto> caseTypeList = compJudgmentDao.getJudgStatList(startDate, loanTime, custNo, true, true, false);
		for(CompJudgStatDto dto : caseTypeList){
			judgAlaly.append(dto.getCaseType())
				.append("案件")
				.append(dto.getCount() + "起,");
		}
		
		//判断司法总体情况
		if(defendantCount <=5){
			judgAlaly.append("企业司法信息状况正常。");
		}else if(defendantCount <= 10){
			judgAlaly.append("企业涉诉纠纷较多，司法情况一般。");
		}else{
			judgAlaly.append("企业涉诉纠纷情况较为复杂，需要对企业实际经营情况进一步核实。");
		}
		
		return judgAlaly.toString();
	}


}
