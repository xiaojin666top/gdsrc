package com.beawan.survey.custInfo.service.impl;

import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.custInfo.dao.CompSalesStatDao;
import com.beawan.survey.custInfo.dto.SalesStatDto;
import com.beawan.survey.custInfo.entity.CompSalesStat;
import com.beawan.survey.custInfo.service.CompSalesStatService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.platform.util.DateUtil;
import com.platform.util.MapperUtil;
import com.platform.util.StringUtil;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.custInfo.entity.CompSalesStat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yzj
 */
@Service("compSalesStatService")
public class CompSalesStatServiceImpl extends BaseServiceImpl<CompSalesStat> implements CompSalesStatService{
    /**
    * 注入DAO
    */
    @Resource(name = "compSalesStatDao")
    public void setDao(BaseDao<CompSalesStat> dao) {
        super.setDao(dao);
    }
    @Resource
    public CompSalesStatDao compSalesStatDao;

    @Override
    public void saveStatData(String serNo, String jsonData, String userNo) {
        Gson gson = new Gson();
        List<CompSalesStat> salesStatList = gson.fromJson(jsonData,
                new TypeToken<ArrayList<CompSalesStat>>(){}.getType());
        if (salesStatList == null || salesStatList.size() <= 0) return;

        for (CompSalesStat salesStat : salesStatList) {
            if (StringUtil.isEmptyString(salesStat.getSerNo()))
                salesStat.setSerNo(serNo);
            if (salesStat.getId() == null){
                salesStat.setCreater(userNo);
                salesStat.setCreateTime(DateUtil.getNowTimestamp());
            }
            salesStat.setUpdater(userNo);
            salesStat.setUpdateTime(DateUtil.getNowTimestamp());
            salesStat.setStatus(Constants.NORMAL);
            compSalesStatDao.saveOrUpdate(salesStat);
        }
    }

    @Override
    public List<SalesStatDto> getSalesStatList(String serNo) {
        Map<String, Object> params = new HashMap<>();
        params.put("serNo", serNo);
        List<CompSalesStat> list = compSalesStatDao.selectByProperty(params);
        if (list == null || list.size() <= 0) return null;
        return MapperUtil.trans(list, SalesStatDto.class);
    }
}
