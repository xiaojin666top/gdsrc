package com.beawan.survey.custInfo.service.impl;

import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.custInfo.dao.CompCreditDao;
import com.beawan.survey.custInfo.entity.CompCredit;
import com.beawan.survey.custInfo.service.CompCreditService;
import com.platform.util.StringUtil;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import com.beawan.survey.custInfo.entity.CompCredit;

/**
 * @author yzj
 */
@Service("compCreditService")
public class CompCreditServiceImpl extends BaseServiceImpl<CompCredit> implements CompCreditService{
    /**
    * 注入DAO
    */
    @Resource(name = "compCreditDao")
    public void setDao(BaseDao<CompCredit> dao) {
        super.setDao(dao);
    }
    @Resource
    public CompCreditDao compCreditDao;
    
    @Override
    public List<CompCredit> getBySerNoAndGId(String serialNumber, String guaCompId) {
        List<CompCredit> result = new ArrayList<>();;
        Map<String, Object> params = new HashMap<>();
        params.put("serialNumber", serialNumber);
        if (StringUtil.isEmptyString(guaCompId)){
            String query = "serialNumber = :serialNumber and status=:status and guaCompId is null";
            params.put("status", Constants.NORMAL);
            result = compCreditDao.select(query, params);
        }else{
            params.put("guaCompId", guaCompId);
            result = compCreditDao.selectByProperty(params);
        }
        return result;
    }
}
