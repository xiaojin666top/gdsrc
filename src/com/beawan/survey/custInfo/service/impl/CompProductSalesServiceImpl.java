package com.beawan.survey.custInfo.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.custInfo.dao.CompProductSalesDao;
import com.beawan.survey.custInfo.bean.CompProductSales;
import com.beawan.survey.custInfo.service.CompProductSalesService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.survey.custInfo.bean.CompProductSales;

/**
 * @author yzj
 */
@Service("compProductSalesService")
public class CompProductSalesServiceImpl extends BaseServiceImpl<CompProductSales> implements CompProductSalesService{
    /**
    * 注入DAO
    */
    @Resource(name = "compProductSalesDao")
    public void setDao(BaseDao<CompProductSales> dao) {
        super.setDao(dao);
    }
    @Resource
    public CompProductSalesDao compProductSalesDao;
}
