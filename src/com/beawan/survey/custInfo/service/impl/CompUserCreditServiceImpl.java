package com.beawan.survey.custInfo.service.impl;

import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.survey.custInfo.dao.CompUserCreditDao;
import com.beawan.survey.custInfo.entity.CompUserCredit;
import com.beawan.survey.custInfo.service.CompUserCreditService;
import com.platform.util.StringUtil;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import com.beawan.survey.custInfo.entity.CompUserCredit;

/**
 * @author yzj
 */
@Service("compUserCreditService")
public class CompUserCreditServiceImpl extends BaseServiceImpl<CompUserCredit> implements CompUserCreditService{
    /**
    * 注入DAO
    */
    @Resource(name = "compUserCreditDao")
    public void setDao(BaseDao<CompUserCredit> dao) {
        super.setDao(dao);
    }
    @Resource
    public CompUserCreditDao compUserCreditDao;
    
    @Override
    public List<CompUserCredit> getBySerNoAndGId(String serialNumber, String guaCompId) {
        List<CompUserCredit> result = new ArrayList<>();
        Map<String, Object> params = new HashMap<>();
        params.put("serialNumber", serialNumber);
        if (StringUtil.isEmptyString(guaCompId)){
            String query = "serialNumber = :serialNumber and status=:status and guaCompId is null";
            params.put("status", Constants.NORMAL);
            result = compUserCreditDao.select(query, params);
        }else{
            params.put("guaCompId", guaCompId);
            result = compUserCreditDao.selectByProperty(params);
        }
        return result;
    }
}
