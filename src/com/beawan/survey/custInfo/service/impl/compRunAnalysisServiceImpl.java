package com.beawan.survey.custInfo.service.impl;

import com.beawan.base.entity.InduInfo;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.dao.ICusBaseDAO;
import com.beawan.customer.dto.InduMapDto;
import com.beawan.customer.service.FinriskResultService;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.survey.custInfo.dao.CompRunAnalysisDao;
import com.beawan.survey.custInfo.bean.CompBase;
import com.beawan.survey.custInfo.bean.CompRunAnalysis;
import com.beawan.survey.custInfo.dao.ICompBaseDAO;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.beawan.survey.custInfo.service.compRunAnalysisService;
import com.beawan.survey.loanInfo.bean.RiskAnalysis;
import com.beawan.survey.loanInfo.dao.IRiskAnalysisDAO;
import com.beawan.task.bean.Task;
import com.beawan.task.dao.ITaskDAO;
import com.beawan.task.service.ITaskSV;
import com.platform.util.DateUtil;
import com.platform.util.StringUtil;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;

/**
 * @author yzj
 */
@Service("compRunAnalysisService")
public class compRunAnalysisServiceImpl extends BaseServiceImpl<CompRunAnalysis> implements compRunAnalysisService{
    /**
    * 注入DAO
    */
    @Resource(name = "compRunAnalysisDao")
    public void setDao(BaseDao<CompRunAnalysis> dao) {
        super.setDao(dao);
    }
    @Resource
    public CompRunAnalysisDao compRunAnalysisDao;
    @Resource
    private ITaskDAO taskDAO;
    @Resource
    private FinriskResultService finriskResultService;
	@Resource
	private IRiskAnalysisDAO riskDao;
	@Resource
	private ICompBaseDAO compBaseDAO;

    @Override
    public CompRunAnalysis syncInduAnalysis(String serNo) throws Exception {
        CompRunAnalysis entity = compRunAnalysisDao.selectSingleByProperty("serNo", serNo);
        Task task = taskDAO.getTaskBySerNo(serNo);
        String industry = task.getIndustry();
        String customerNo = task.getCustomerNo();
        CompBase compBase = compBaseDAO.findByPrimaryKey(customerNo);
        if(compBase!=null){
        	String industryCode = compBase.getIndustryCode();
        	if(!industry.equals(industryCode)){
        		industry = industryCode;
        		task.setIndustry(industryCode);
                taskDAO.saveOrUpdate(task);
        	}
        }
        
        InduInfo induInfo = finriskResultService.getInduAnalysis(industry);

        if(induInfo==null) {
            return entity;
        }
        if(entity==null){
            entity = new CompRunAnalysis();
            entity.setSerNo(serNo);
            entity.setCustomerNo(task.getCustomerNo());
            entity.setCreateTime(DateUtil.getNowTimestamp());
        }
        //开始保存行业分析--》行业概况
        String surveys = induInfo.getInduSurvey();
        if(!StringUtil.isEmptyString(surveys)) {
            if(surveys.length()>1500){
                surveys = surveys.substring(1500);
            }
            entity.setIndustryInfo(surveys);
            entity = compRunAnalysisDao.saveOrUpdate(entity);
        }
        //开始保存行业风险
        String induRisk = induInfo.getInduRisk();
        if(!StringUtil.isEmptyString(induRisk)){
	        RiskAnalysis riskAnalysis = riskDao.selectSingleByProperty("serNo", serNo);
			if(riskAnalysis == null ){
				riskAnalysis = new RiskAnalysis();
				riskAnalysis.setSerNo(serNo);
			}
			riskAnalysis.setIndustryRisk(induRisk);
        }
        return entity;

    }
}
