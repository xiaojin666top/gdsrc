package com.beawan.survey.custInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.custInfo.bean.CompPenalty;

/**
 * @author yzj
 */
public interface CompPenaltyService extends BaseService<CompPenalty> {
}
