package com.beawan.survey.custInfo.service;

import java.util.List;

import com.beawan.core.BaseService;
import com.beawan.survey.custInfo.entity.CompCredit;

/**
 * @author yzj
 */
public interface CompCreditService extends BaseService<CompCredit> {
	
	 /**
     * 根据 流水号、担保企业ID 获取企业征信
     * @param serialNumber 流水号
     * @param guaCompId 担保企业ID
     * @return
     */
	List<CompCredit> getBySerNoAndGId(String serialNumber, String guaCompId);
}
