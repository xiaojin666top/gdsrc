package com.beawan.survey.custInfo.service;

import java.util.List;

import com.beawan.survey.custInfo.bean.CompConnetCm;

public interface ICompConnectSV {
	
	/**
	 * @Description (保存关联企业信息)
	 * @param data
	 *            企业关联企业信息
	 */
	public void saveCompConnectCm(CompConnetCm data) throws Exception;
	
	/**
	 * @Description (保存关联企业信息列表)
	 * @param custNo 客户号
	 * @param compInfoJsonArray 关联企业信息json串
	 * @throws Exception
	 */
	public void saveCompConnectCms(String custNo, String compInfoJsonArray) throws Exception;

	/**
	 * @Description (刪除企业关联企业信息)
	 * @param data
	 *            企业关联企业信息
	 */
	public void deleteCompConnectCm(CompConnetCm data) throws Exception;
	
	
	/**
	 * @Description (通过客户号查找关联企业信息)
	 * @param customerNo
	 * @return
	 * @throws Exception
	 */
	public List<CompConnetCm> findCompConnectCmByCustNo(String customerNo);
	
	/**
	 * @Description (同步关联企业信息)
	 * @param custNo
	 * @return
	 * @throws Exception
	 */
	public List<CompConnetCm> syncCompConnectCmByCustNo(String custNo) throws Exception;

	/**
	 * @Description (通过id查找关联企业信息)
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public CompConnetCm findById(Long id) throws Exception;

}
