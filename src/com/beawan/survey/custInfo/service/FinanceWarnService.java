package com.beawan.survey.custInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.custInfo.entity.FinanceWarn;

/**
 * @author yzj
 */
public interface FinanceWarnService extends BaseService<FinanceWarn> {
}
