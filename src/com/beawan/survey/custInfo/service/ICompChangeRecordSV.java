package com.beawan.survey.custInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.custInfo.bean.CompChangeRecord;

/**
 * 贷款产品
 * @author beawan_fengjj
 *
 */
public interface ICompChangeRecordSV extends BaseService<CompChangeRecord>{
	
}
