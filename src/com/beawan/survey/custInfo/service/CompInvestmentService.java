package com.beawan.survey.custInfo.service;

import com.beawan.core.BaseService;
import com.beawan.core.Pagination;
import com.beawan.survey.custInfo.dto.CompJudgmentDto;
import com.beawan.survey.custInfo.entity.CompInvestment;

/**
 * @author yzj
 */
public interface CompInvestmentService extends BaseService<CompInvestment> {
	
	/***
	 * 分页获取企业对外投资信息
	 * @param custNo
	 * @param page
	 * @param pageSize
	 * @return
	 */
	Pagination<CompInvestment> getInvestPager(String custNo, Integer page, Integer pageSize);
	
	
	/***
	 * 更新可修改的实际投资金额及本行贷款
	 * @param compInvestment
	 * @return
	 */
	void saveInvestLoan(CompInvestment compInvestment);
}
