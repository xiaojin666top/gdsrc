package com.beawan.survey.custInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.custInfo.entity.CompEquipment;

/**
 * @author yzj
 */
public interface CompEquipmentService extends BaseService<CompEquipment> {
}
