package com.beawan.survey.custInfo.service;

import java.util.List;

import com.beawan.survey.custInfo.bean.CompBase;
import com.beawan.survey.custInfo.bean.CompBaseDesc;
import com.beawan.survey.custInfo.bean.CompBaseEquity;
import com.beawan.survey.custInfo.bean.CompBaseItemChange;
import com.beawan.survey.custInfo.bean.CompBaseManager;
import com.beawan.survey.custInfo.bean.PersonBase;
import com.beawan.survey.custInfo.dto.CompBaseDto;
import com.beawan.survey.custInfo.dto.CompBaseManagerDto;
import com.beawan.survey.custInfo.dto.CompChangeRecordDto;
import com.beawan.survey.custInfo.dto.CompManagerInfoDto;

public interface ICompBaseSV {
	
	/**
	 * @Description (根据客户号查询企业基本信息)
	 * @param customerNo 客户号
	 * @return CompCustBase
	 * @throws Exception
	 */
	public CompBaseDto findCompBaseByCustNo(String customerNo) throws Exception;
	

	/**
	 * @Description (保存企业基本信息)
	 * @param custBaseDto 企业基本信息实体
	 * @throws Exception
	 */
	public void saveCompBase(CompBaseDto custBaseDto) throws Exception;
	
	/**
	 * @Description (删除企业基本信息,同时删除股东信息)
	 * @param compBase 企业基本信息实体
	 * @throws Exception
	 */
	public void deleteCompBase(CompBase compBase) throws Exception;
	
	/**
	 * @Description (根据客户号查询企业描述信息)
	 * @param custNo 客户号
	 * @return CompCustBaseDesc
	 * @throws Exception
	 */
	public CompBaseDesc findCompBaseDescByCustNo(String custNo) throws Exception;
	
	/**
	 * @Description (根据客户号查询企业变更信息分组)
	 * @param custNo
	 * @return
	 */
	public List<CompChangeRecordDto> groupByItemWhereCustNo(String custNo);
	
	/**
	 * @Description (保存企业描述信息)
	 * @param compBaseDesc 企业描述信息实体
	 * @throws Exception
	 */
	public void saveCompBaseDesc(CompBaseDesc compBaseDesc) throws Exception;
	
	/**
	 * @Description (保存企业描述信息，仅更新主要信息字段)
	 * @param jsonInfo 企业描述信息json串
	 * @throws Exception
	 */
	public void saveCompBaseDesc(String jsonInfo) throws Exception;
	
	/**
	 * @Description (根据客户号查询个人信息)
	 * @param custNo 客户号
	 * @return PersonBase
	 * @throws Exception
	 */
	public PersonBase findPersonBaseByCustNo(String custNo) throws Exception;
	

	/**
	 * @Description (保存个人信息)
	 * @param personBase 个人信息实体
	 * @throws Exception
	 */
	public PersonBase savePersonBase(PersonBase personBase) throws Exception;
	
	/**
	 * @Description (保存个人信息)
	 * @param jsondata 个人信息json串
	 * @return
	 * @throws Exception
	 */
	public PersonBase savePersonBase(String jsondata) throws Exception;
	
	/**
	 * @Description (删除个人信息)
	 * @param custBase 个人信息实体
	 * @throws Exception
	 */
	public void deletePersonBase(PersonBase personBase) throws Exception;
	
	
	/**
	 * @Description (根据客户号查询企业股权信息)
	 * @param custNo 客户号
	 * @return DataGrid<CompCustEquity>
	 * @throws Exception
	 */
	public List<CompBaseEquity> findCompEquityByCustNo(String custNo) throws Exception;
	
	/**
	 * @Description (同步股权信息)
	 * @param custNo
	 * @return
	 * @throws Exception
	 */
	public List<CompBaseEquity> syncCompEquityByCustNo(String custNo) throws Exception ;
	
	/**
	 * @Description (根据唯一标识查询企业股权信息)
	 * @param id 唯一标识
	 * @return
	 * @throws Exception
	 */
	public CompBaseEquity findCompEquityById(Long id) throws Exception;

	public CompBaseItemChange findCompChangeById(Long id) throws Exception;
	
	/**
	 * @Description (保存企业股权信息)
	 * @param custEquity 股权信息实体
	 * @throws Exception
	 */
	public void saveCompEquity(CompBaseEquity custEquity) throws Exception;
	
	/**
	 * @Description (保存企业变更信息)
	 * @param change 客户号
	 * @throws Exception
	 */
	public void saveComItemChange(CompBaseItemChange change);
	
	/**
	 * 查询企业变更历史记录
	 * @param customerNo
	 * @return
	 * @throws Exception
	 */
	public List<CompBaseItemChange> findChangeListByCustNo(String customerNo) throws Exception;
	
	/**
	 * @Description (保存企业股权信息列表，先删除后插入)
	 * @param custNo 客户号
	 * @param jsonArray 股权信息实体列表
	 * @throws Exception
	 */
	public void saveCompEquitys(String custNo, String jsonArray) throws Exception;
	
	/**
	 * @Description (删除企业股权记录)
	 * @param equity 股权实体
	 * @throws Exception
	 */
	public void deleteCompEquity(CompBaseEquity equity) throws Exception;

	/**
	 * @Description (删除企业股权变更记录)
	 * @param change 股权变更实体
	 * @throws Exception
	 */
	public void deleteCompEquityChange(CompBaseItemChange change) throws Exception;
	
	/**
	 * @Description (通过客户号查询管理人员信息)
	 * @param customerNo 客户号
	 * @return
	 * @throws Exception
	 */
	public List<CompBaseManagerDto>  findCompManagerByCustNo(String customerNo);
	
	/**
	 * @Description (通过客户号查询管理人员信息， SQL查询，返回map集合)
	 * @param customerNo 客户号
	 * @return
	 */
	public List<CompManagerInfoDto> findManagerInfoByCustNo(String customerNo);
	
	/**
	 * 
	 * @param customerNo
	 * @return 到信贷系统查询
	 * @throws Exception
	 */
	public List<CompBaseManagerDto> syncCompManagerFromCmis(String customerNo) throws Exception;
	
	/**
	 * @Description (保存管理人员信息)
	 * @param managerInfo 管理人员信息实体
	 * @throws Exception
	 */
	public void saveCompManager(String userId, CompBaseManagerDto managerInfo) throws Exception;
	
	/**
	 * @Description (保存管理人员信息列表)
	 * @throws Exception
	 */
	public void saveCompManagers(String custNo, String jsonArray) throws Exception;
	
	/**
	 * @Description (删除管理人员信息)
	 * @param managerInfo
	 * @throws Exception
	 */
	public void deleteCompManager(String userId, CompBaseManagerDto managerInfo) throws Exception;

	/**
	 * @Description (根据唯一标识查询企业管理人员信息)
	 * @param id CM_BASE_MANAGER_ID 唯一标识
	 * @return
	 * @throws Exception
	 */
	public CompBaseManagerDto findCompManagerById(Long id) throws Exception;

	/**
	 * TODO 从信贷同步信息，包括股权结构信息，关联企业信息，高管信息
	 * @param cusId
	 * @throws Exception
	 */
	public void syncCustomerInfo(String cusId) throws Exception;
	
}
