package com.beawan.survey.custInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.custInfo.bean.CompShiXinItems;

/**
 * @author yzj
 */
public interface CompShiXinItemsService extends BaseService<CompShiXinItems> {
}
