package com.beawan.survey.custInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.custInfo.bean.CompZhiXingItems;

/**
 * @author yzj
 */
public interface CompZhiXingItemsService extends BaseService<CompZhiXingItems> {
}
