package com.beawan.survey.custInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.custInfo.entity.CompContribute;

/**
 * @author yzj
 */
public interface CompContributeService extends BaseService<CompContribute> {
	
	/***
     * 从信贷系统CMIS同步本行贡献数据到本地
     * @param serNo 流水号
     * @param customerNo 客户号
     * @return
     */
    public CompContribute syncContributeFromCMIS(String serNo, String customerNo) throws Exception;
}
