package com.beawan.survey.custInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.custInfo.bean.CompProductSales;

/**
 * @author yzj
 */
public interface CompProductSalesService extends BaseService<CompProductSales> {
}
