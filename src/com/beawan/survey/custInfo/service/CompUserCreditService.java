package com.beawan.survey.custInfo.service;

import java.util.List;

import com.beawan.core.BaseService;
import com.beawan.survey.custInfo.entity.CompUserCredit;

/**
 * @author yzj
 */
public interface CompUserCreditService extends BaseService<CompUserCredit> {
	
	/**
     * 根据 流水号、担保企业ID 获取个人征信
     * @param serialNumber 流水号
     * @param guaCompId 担保企业ID
     * @return
     */
    List<CompUserCredit> getBySerNoAndGId(String serialNumber, String guaCompId);
}
