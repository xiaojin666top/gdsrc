package com.beawan.survey.custInfo.service;

import java.util.List;
import java.util.Map;

import com.beawan.analysis.finansis.common.FinaRowData;
import com.beawan.survey.custInfo.bean.CompFinance;
import com.beawan.survey.custInfo.bean.CompFinanceMainSub;
import com.beawan.survey.custInfo.bean.CompFinanceOperCF;
import com.beawan.survey.custInfo.bean.CompFinanceTax;

public interface ICompFinanceSV {
	
	/**
	 * @Description (根据客户号和任务号查询企业财务状况分析说明)
	 * @param taskId 任务号
	 * @param custNo 客户号
	 * @return
	 * @throws Exception
	 */
	public CompFinance findCompFinanceByTIdAndNo(Long taskId, String custNo) throws Exception;
	
	/**
	 * @Description (根据ID查询企业财务状况分析说明)
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public CompFinance findCompFinanceById(Long id) throws Exception;

	/**
	 * @Description (根据ID查询企业财务状况分析说明)
	 * @param taskId 贷前任务号
	 * @return
	 * @throws Exception
	 */
	public CompFinance findCompFinanceByTaskId(Long taskId) throws Exception;

	/**
	 * @Description (保存企业财务状况分析说明)
	 * @param data 企业财务状况分析说明信息实体
	 */
	public CompFinance saveCompFinance(CompFinance data) throws Exception;
	
	/**
	 * @Description (保存企业财务状况分析说明)
	 * @param jsondata 企业财务状况分析说明信息json串
	 */
	public void saveCompFinance(String jsondata) throws Exception;

	/**
	 * @Description (刪除企业财务状况分析说明)
	 * @param data 企业财务状况分析说明信息实体
	 */
	public void deleteCompFinance(CompFinance data) throws Exception;
	
	/**
	 * @Description (根据客户号和任务号查询企业主要财务科目分析说明)
	 * @param taskId 任务号
	 * @param custNo 客户号
	 * @return
	 * @throws Exception
	 */
	public CompFinanceMainSub findFinaMainSubByTIdAndNo(Long taskId, 
			String custNo) throws Exception;
	
	/**
	 * @Description (根据ID查询企业主要财务科目分析说明)
	 * @param taskId 任务号
	 * @return
	 * @throws Exception
	 */
	public CompFinanceMainSub findFinaMainSubById(Long taskId) throws Exception;

	/**
	 * @Description (保存企业主要财务科目分析说明)
	 * @param data 企业主要财务科目分析说明信息实体
	 */
	public void saveFinaMainSub(CompFinanceMainSub data) throws Exception;
	
	/**
	 * @Description (保存企业主要财务科目分析说明)
	 * @param jsondata 企业主要财务科目分析说明信息json串
	 */
	public void saveFinaMainSub(String jsondata) throws Exception;

	/**
	 * @Description (刪除企业主要财务科目分析说明)
	 * @param data 企业主要财务科目分析说明信息实体
	 */
	public void deleteFinaMainSub(CompFinanceMainSub data) throws Exception;
	
	/**
	 * @Description (保存企业经营性现金流回笼记录)
	 * @param data
	 *            企业经营性现金流回笼记录实体
	 */
	public CompFinanceOperCF saveFinanceOperCF(CompFinanceOperCF data) throws Exception;
	
	/**
	 * @Description (保存企业经营性现金流回笼记录列表，先删除后插入)
	 * @param taskId 任务号
	 * @param customerNo 客户号
	 * @param jsonArray 企业经营性现金流回笼记录列表
	 */
	public void saveFinanceOperCFs(Long taskId, String customerNo,
			String jsonArray) throws Exception;

	/**
	 * @Description (刪除企业经营性现金流回笼记录)
	 * @param data
	 *            企业经营性现金流回笼记录实体
	 */
	public void deleteFinanceOperCF(CompFinanceOperCF data) throws Exception;
	
	/**
	 * @Description (根据客户号和任务号查询企业经营性现金流回笼记录)
	 * @param taskId
	 * @param custNo
	 * @return
	 * @throws Exception
	 */
	public List<CompFinanceOperCF>  findFinanceOperCFByTIdAndNo(Long taskId,
			String customerNo) throws Exception;
	
	/**
	 * @Description (保存企业缴税信息记录)
	 * @param data
	 *            企业缴税信息记录实体
	 */
	public CompFinanceTax saveFinanceTax(CompFinanceTax data) throws Exception;
	
	/**
	 * @Description (保存企业缴税信息记录列表，先删除后插入)
	 * @param taskId 任务号
	 * @param customerNo 客户号
	 * @param jsonArray 企业缴税信息记录列表
	 */
	public void saveFinanceTaxes(Long taskId, String customerNo,
			String jsonArray) throws Exception;

	/**
	 * @Description (刪除企业缴税信息记录)
	 * @param data
	 *            企业缴税信息记录实体
	 */
	public void deleteFinanceTax(CompFinanceTax data) throws Exception;
	
	/**
	 * @Description (根据客户号和任务号查询企业缴税信息记录)
	 * @param taskId
	 * @param custNo
	 * @return
	 * @throws Exception
	 */
	public List<CompFinanceTax>  findFinanceTaxByTIdAndNo(Long taskId,
			String customerNo) throws Exception;
	
	/**
	 * @Description 比率指标分析-查询财务比率指标
	 * @param taskId
	 * @param cusNo
	 * @return
	 * @throws Exception
	 */
	public List<FinaRowData> findFinanceIndexs(long taskId) throws Exception;
	
	/**
	 * @Description 比率指标分析-查询财务比率指标
	 * @param customerNo 客户号
	 * @param dateList 报表日期
	 * @param industry 行业标识
	 * @return
	 * @throws Exception
	 */
	public List<FinaRowData> findFinanceIndexs(String customerNo, List<String> dateList,
			String industry) throws Exception;
	
	/**
	 * @Description 比率指标分析-查询财务比率指标（信贷原始数据）
	 * @param customerNo 客户号
	 * @param industry 行业标识
	 * @param cmisRatioIndexData 比率指标原始数据（信贷原始数据）
	 * @return
	 * @throws Exception
	 */
	public List<FinaRowData> findFinanceIndexs(String customerNo, String industry,
			List<Map<String, Double>> cmisRatioIndexData) throws Exception;
	
	/**
	 * @Description 现金流量分析-查询财务报表现金流量数据
	 * @param taskId
	 * @param cusNo
	 * @return
	 * @throws Exception
	 */
	public List<FinaRowData> findFinanceCashFlow(Long taskId, String cusNo) throws Exception;
	
	/**
	 * @Description 现金流量分析-查询财务报表现金流量数据（信贷原始数据）
	 * @param cmisCashFlowData 现金流量表原始数据（信贷原始数据）
	 * @return
	 * @throws Exception
	 */
	public List<FinaRowData> findFinanceCashFlow(List<Map<String, Double>> cmisCashFlowData) throws Exception;
	

	
	/**
	 * @Description 盈利情况分析-查询财务报表利润数据（信贷原始数据）
	 * @param taskId
	 * @param cusNo
	 * @return
	 * @throws Exception
	 */
	public List<FinaRowData> findFinanceIncome(Long taskId, String cusNo) throws Exception;
	
	/**
	 * @Description 盈利情况分析-查询财务报表利润数据
	 * @param cmisIncomeData 利润表原始数据（信贷原始数据）
	 * @return
	 * @throws Exception
	 */
	public List<FinaRowData> findFinanceIncome(List<Map<String, Double>> cmisIncomeData) throws Exception;
	
	/**
	 * @Description 获得财报年月及客户所属行业标识
	 * @param taskId 任务号
	 * @param customerNo 客户号
	 * @param cusFlag 客户标识
	 * @return
	 * @throws Exception
	 */
	public Map<String,Object> getFinaRepInfo(long taskId, String customerNo) throws Exception;
}
