package com.beawan.survey.custInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.custInfo.bean.CompRunProduct;

/**
 * 企业主要产品
 * @author zxh
 * @date 2020/7/9 17:31
 */
public interface CompProductService extends BaseService<CompRunProduct> {
}
