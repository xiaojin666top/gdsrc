package com.beawan.survey.custInfo.service;

import com.beawan.core.BaseService;
import com.beawan.survey.custInfo.entity.CompInnerManage;

/**
 * @author yzj
 */
public interface CompInnerManageService extends BaseService<CompInnerManage> {
}
