package com.beawan.survey.custInfo.qbean;

/**
 * 财务指标查询类
 * @author beawan_fengjj
 *
 */
public class FianceIndexData {

	
	private String subjName;//项目
	private Double befoLastYearAmt;//前年数值
	private Double lastYearAmt;//去年数值
	private Double currAmt;//当前数值
	
	public String getSubjName() {
		return subjName;
	}
	public void setSubjName(String subjName) {
		this.subjName = subjName;
	}
	public Double getBefoLastYearAmt() {
		return befoLastYearAmt;
	}
	public void setBefoLastYearAmt(Double befoLastYearAmt) {
		this.befoLastYearAmt = befoLastYearAmt;
	}
	public Double getLastYearAmt() {
		return lastYearAmt;
	}
	public void setLastYearAmt(Double lastYearAmt) {
		this.lastYearAmt = lastYearAmt;
	}
	public Double getCurrAmt() {
		return currAmt;
	}
	public void setCurrAmt(Double currAmt) {
		this.currAmt = currAmt;
	}
	
}
