package com.beawan.survey.custInfo.dto;

import com.beawan.survey.custInfo.bean.PersonBase;

/**
 * 企业高管基本信息DTO
 *
 * @author zxh
 * @date 2020/7/2 19:59
 */
public class CompManagerInfoDto {

    private Integer id;
    private String customerNo; //企业客户号
    private String name;//高管姓名
    private String sex;//性别
    private String birthday;//出生日期 生日
    private String managerType;// 高管类别/职务
    private String title;//职称
    private String education;//学历
    private String maritalStatus;//婚姻状况
    private String birthPlace;//籍贯
    private String livingPlace;//出生地

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getManagerType() {
        return managerType;
    }

    public void setManagerType(String managerType) {
        this.managerType = managerType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getLivingPlace() {
        return livingPlace;
    }

    public void setLivingPlace(String livingPlace) {
        this.livingPlace = livingPlace;
    }

}
