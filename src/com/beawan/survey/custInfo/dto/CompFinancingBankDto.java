package com.beawan.survey.custInfo.dto;


public class CompFinancingBankDto {
	private Long id;
	private String customerNo;
	private String financialOrg; // 金融机构(RGRCB为本行)
	private String businessType; // 授信品种
	private Double creditAmt; // 授信额度
	private Double creditBal; // 授信余额
	private Integer creditTerm; // 授信期限(月)
	private Double creditRate; // 授信利率
	private String guaranteeType; // 担保方式
	private String repaymentType; // 还款方式
	private String useDesc; // 贷款用途
	private String paymentType; // 支付方式
	private String startDate; // 发放日
	private String endDate; // 到期日
	private Double usedAmt; // 用信额度
	private String fiveClass; // 五级分类
	private String remarks; // 备注
	private Long taskId;//任务ID
	private String loanProject; // 申贷项目（建筑业）
	private String relatedParty; // 关联方（建筑业）
	private String type; // 01:借款人 ,02:关联方（建筑业）

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustomerNo() {
		return this.customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getFinancialOrg() {
		return financialOrg;
	}

	public void setFinancialOrg(String financialOrg) {
		this.financialOrg = financialOrg;
	}

	public String getBusinessType() {
		return this.businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public Double getCreditAmt() {
		return this.creditAmt;
	}

	public void setCreditAmt(Double creditAmt) {
		this.creditAmt = creditAmt;
	}

	public Double getCreditBal() {
		return this.creditBal;
	}

	public void setCreditBal(Double creditBal) {
		this.creditBal = creditBal;
	}

	public Integer getCreditTerm() {
		return this.creditTerm;
	}

	public void setCreditTerm(Integer creditTerm) {
		this.creditTerm = creditTerm;
	}

	public Double getCreditRate() {
		return this.creditRate;
	}

	public void setCreditRate(Double creditRate) {
		this.creditRate = creditRate;
	}

	public String getGuaranteeType() {
		return guaranteeType;
	}

	public void setGuaranteeType(String guaranteeType) {
		this.guaranteeType = guaranteeType;
	}

	public String getRepaymentType() {
		return repaymentType;
	}

	public void setRepaymentType(String repaymentType) {
		this.repaymentType = repaymentType;
	}

	public String getUseDesc() {
		return this.useDesc;
	}

	public void setUseDesc(String useDesc) {
		this.useDesc = useDesc;
	}

	public String getPaymentType() {
		return this.paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Double getUsedAmt() {
		return this.usedAmt;
	}

	public void setUsedAmt(Double usedAmt) {
		this.usedAmt = usedAmt;
	}

	public String getFiveClass() {
		return this.fiveClass;
	}

	public void setFiveClass(String fiveClass) {
		this.fiveClass = fiveClass;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getLoanProject() {
		return loanProject;
	}

	public void setLoanProject(String loanProject) {
		this.loanProject = loanProject;
	}

	public String getRelatedParty() {
		return relatedParty;
	}

	public void setRelatedParty(String relatedParty) {
		this.relatedParty = relatedParty;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
