package com.beawan.survey.custInfo.dto;

//import com.beawan.base.entity.SReportDataId;
import com.beawan.core.BaseEntity;
import com.beawan.qcc.dto.CaseRoleDto;

import java.util.List;

import javax.persistence.*;

/**
 * 裁判文书 司法信息表
 */
public class CompJudgmentDto extends BaseEntity {

    private String id;

    private String customerNo;//客户号
    private String court;//执行法院
    private String caseName;//裁判文书名字
    private String caseNo;//裁判文书编号
    private String caseType;//裁判文书类型
    private String submitDate;//发布时间
    private String updateDate;//审判时间
    private String isProsecutor;//是否原告（供参考）
    private String isDefendant;//是否被告（供参考）
    private String courtYear;//开庭时间年份
    private String caseRole;//涉案人员角色
    
    private List<CaseRoleDto> roleDto;//当事人角色
    
    private String courtLevel;//法院级别，最高法院 5 、高级法院 4 、中级法院 3 、基层法院 2 、其他 1
    private String caseReason;//案由
    private String caseReasonType;//案由类型
    private String courtMonth;//开庭时间月份
    private String amount;//案件金额
    
    private List<String> prosecutorList;//原告列表
    private List<String> defendantList;//被告列表

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    

    public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getCourt() {
        return court;
    }

    public void setCourt(String court) {
        this.court = court;
    }

    public String getCaseName() {
        return caseName;
    }

    public void setCaseName(String caseName) {
        this.caseName = caseName;
    }

    public String getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    public String getCaseType() {
        return caseType;
    }

    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    public String getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(String submitDate) {
        this.submitDate = submitDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getIsProsecutor() {
        return isProsecutor;
    }

    public void setIsProsecutor(String isProsecutor) {
        this.isProsecutor = isProsecutor;
    }

    public String getIsDefendant() {
        return isDefendant;
    }

    public void setIsDefendant(String isDefendant) {
        this.isDefendant = isDefendant;
    }

    public String getCourtYear() {
        return courtYear;
    }

    public void setCourtYear(String courtYear) {
        this.courtYear = courtYear;
    }

    public String getCaseRole() {
        return caseRole;
    }

    public void setCaseRole(String caseRole) {
        this.caseRole = caseRole;
    }
    

    public List<CaseRoleDto> getRoleDto() {
		return roleDto;
	}

	public void setRoleDto(List<CaseRoleDto> roleDto) {
		this.roleDto = roleDto;
	}

	public String getCourtLevel() {
        return courtLevel;
    }

    public void setCourtLevel(String courtLevel) {
        this.courtLevel = courtLevel;
    }

    public String getCaseReason() {
        return caseReason;
    }

    public void setCaseReason(String caseReason) {
        this.caseReason = caseReason;
    }

    public String getCaseReasonType() {
        return caseReasonType;
    }

    public void setCaseReasonType(String caseReasonType) {
        this.caseReasonType = caseReasonType;
    }

    public String getCourtMonth() {
        return courtMonth;
    }

    public void setCourtMonth(String courtMonth) {
        this.courtMonth = courtMonth;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

	public List<String> getProsecutorList() {
		return prosecutorList;
	}

	public void setProsecutorList(List<String> prosecutorList) {
		this.prosecutorList = prosecutorList;
	}

	public List<String> getDefendantList() {
		return defendantList;
	}

	public void setDefendantList(List<String> defendantList) {
		this.defendantList = defendantList;
	}
    
    
}
