package com.beawan.survey.custInfo.dto;

import javax.persistence.Column;

import com.beawan.core.BaseDto;
import com.beawan.scoreCard.AcConstants;
import com.beawan.survey.custInfo.bean.CompBaseDesc;

/**
 * 企业客户基本信息DTO
 * @author zxh
 * @date 2020/7/1 19:18
 */
public class CompBaseDto extends BaseDto {

    private String customerNo; //客户号
    private String customerName;//企业名称
    private String controlPerson;//实际控制人
    private String controlPersonCode;//实际控制人编号
    private String controlWorkingYears;//实际控制人从业年限
    private String creditLevel;//本行信用等级
    private String certCode;//证件号码
    private String certType;//证件类型
    private String foundDate;//企业成立日期
    private String majorIndu;//行业大类
    private String indusPolicy;// 行业政策
    private String industryCode;//行业类型编号
    private String industryType;//行业类型
    private String legalPerson;//法定代表人
    private String legalPersonCode;//法定代表人编号
    private String legalWorkingYears;//法定代表人从业年限
    private String mainBusiness;//实际主营业务
    private Double realCapital;//实收资本
    private String pcCurrency;//实收资本币种
    private String registerAddress;//注册地址
    private Double registerCapital;//注册资本
    private String rcCurrency;//注册资本币种
    private String runAddress;//经营地址
    private String runScope;//经营范围
    private String stockCustomer;//客户类别
    private String companyNature;//企业性质
    private String openBank;//基本开户行
    private String mbankAccType;//本行账户性质
    private String isMbankHolder;//是否本行股东
    private Integer stockNum; // 持股数
    private String isAntiPledge;// 股权是否反质押
    private Integer employeeNum;//员工人数/参保人数
    private String manageUser;//管户人
    private String manageOrg;//管理机构
    private String financeBelong;//财务报表所属
    private String regNumber;//工商注册号
    private String orgNumber;//组织结构代码
    private String approvedTime;//核准日期
    private String regInstitute;//登记机关
    private String isDraft;//是否为草稿信息（非信贷客户）
    private String creditRecord;//征信记录
    private String loanHistory;//在我行是否贷款3年以上
    private String revoleCredit;//是否循环授信  默认 否
	private CompBaseDesc compBaseDesc;//企业基本信息描述
    private String taxGrade;//纳税等级
	private String eiaGrade;//环评信用等级
	private String isDischargePermit;//是否有排污许可证

    public String getTaxGrade() {
		return taxGrade;
	}

	public void setTaxGrade(String taxGrade) {
		this.taxGrade = taxGrade;
	}

	public String getEiaGrade() {
		return eiaGrade;
	}

	public void setEiaGrade(String eiaGrade) {
		this.eiaGrade = eiaGrade;
	}

	public String getIsDischargePermit() {
		return isDischargePermit;
	}

	public void setIsDischargePermit(String isDischargePermit) {
		this.isDischargePermit = isDischargePermit;
	}

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getControlPerson() {
        return controlPerson;
    }

    public void setControlPerson(String controlPerson) {
        this.controlPerson = controlPerson;
    }

    public String getControlPersonCode() {
        return controlPersonCode;
    }

    public void setControlPersonCode(String controlPersonCode) {
        this.controlPersonCode = controlPersonCode;
    }

    public String getCreditLevel() {
        return creditLevel;
    }

    public void setCreditLevel(String creditLevel) {
        this.creditLevel = creditLevel;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getFoundDate() {
        return foundDate;
    }

    public void setFoundDate(String foundDate) {
        this.foundDate = foundDate;
    }

    public String getMajorIndu() {
        return majorIndu;
    }

    public void setMajorIndu(String majorIndu) {
        this.majorIndu = majorIndu;
    }

    public String getIndustryCode() {
        return industryCode;
    }

    public void setIndustryCode(String industryCode) {
        this.industryCode = industryCode;
    }

    public String getIndustryType() {
        return industryType;
    }

    public void setIndustryType(String industryType) {
        this.industryType = industryType;
    }

    public String getLegalPerson() {
        return legalPerson;
    }

    public void setLegalPerson(String legalPerson) {
        this.legalPerson = legalPerson;
    }

    public String getLegalPersonCode() {
        return legalPersonCode;
    }

    public void setLegalPersonCode(String legalPersonCode) {
        this.legalPersonCode = legalPersonCode;
    }

    public String getMainBusiness() {
        return mainBusiness;
    }

    public void setMainBusiness(String mainBusiness) {
        this.mainBusiness = mainBusiness;
    }

    public String getPcCurrency() {
        return pcCurrency;
    }

    public void setPcCurrency(String pcCurrency) {
        this.pcCurrency = pcCurrency;
    }

    public Double getRealCapital() {
        return realCapital;
    }

    public void setRealCapital(Double realCapital) {
        this.realCapital = realCapital;
    }

    public String getRegisterAddress() {
        return registerAddress;
    }

    public void setRegisterAddress(String registerAddress) {
        this.registerAddress = registerAddress;
    }

    public Double getRegisterCapital() {
        return registerCapital;
    }

    public void setRegisterCapital(Double registerCapital) {
        this.registerCapital = registerCapital;
    }

    public String getRcCurrency() {
        return rcCurrency;
    }

    public void setRcCurrency(String rcCurrency) {
        this.rcCurrency = rcCurrency;
    }

    public String getRunAddress() {
        return runAddress;
    }

    public void setRunAddress(String runAddress) {
        this.runAddress = runAddress;
    }

    public String getRunScope() {
        return runScope;
    }

    public void setRunScope(String runScope) {
        this.runScope = runScope;
    }

    public String getStockCustomer() {
        return stockCustomer;
    }

    public void setStockCustomer(String stockCustomer) {
        this.stockCustomer = stockCustomer;
    }

    public String getCompanyNature() {
        return companyNature;
    }

    public void setCompanyNature(String companyNature) {
        this.companyNature = companyNature;
    }

    public String getOpenBank() {
        return openBank;
    }

    public void setOpenBank(String openBank) {
        this.openBank = openBank;
    }

    public String getMbankAccType() {
        return mbankAccType;
    }

    public void setMbankAccType(String mbankAccType) {
        this.mbankAccType = mbankAccType;
    }

    public String getIsMbankHolder() {
        return isMbankHolder;
    }

    public void setIsMbankHolder(String isMbankHolder) {
        this.isMbankHolder = isMbankHolder;
    }

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public String getIsAntiPledge() {
        return isAntiPledge;
    }

    public void setIsAntiPledge(String isAntiPledge) {
        this.isAntiPledge = isAntiPledge;
    }

    public Integer getEmployeeNum() {
        return employeeNum;
    }

    public void setEmployeeNum(Integer employeeNum) {
        this.employeeNum = employeeNum;
    }

    public String getManageUser() {
        return manageUser;
    }

    public void setManageUser(String manageUser) {
        this.manageUser = manageUser;
    }

    public String getManageOrg() {
        return manageOrg;
    }

    public void setManageOrg(String manageOrg) {
        this.manageOrg = manageOrg;
    }

    public String getFinanceBelong() {
        return financeBelong;
    }

    public void setFinanceBelong(String financeBelong) {
        this.financeBelong = financeBelong;
    }

    public String getIsDraft() {
        return isDraft;
    }

    public void setIsDraft(String isDraft) {
        this.isDraft = isDraft;
    }

    public String getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public String getOrgNumber() {
        return orgNumber;
    }

    public void setOrgNumber(String orgNumber) {
        this.orgNumber = orgNumber;
    }

    public String getApprovedTime() {
        return approvedTime;
    }

    public void setApprovedTime(String approvedTime) {
        this.approvedTime = approvedTime;
    }

    public String getRegInstitute() {
        return regInstitute;
    }

    public void setRegInstitute(String regInstitute) {
        this.regInstitute = regInstitute;
    }

    public CompBaseDesc getCompBaseDesc() {
        return compBaseDesc;
    }

    public void setCompBaseDesc(CompBaseDesc compBaseDesc) {
        this.compBaseDesc = compBaseDesc;
    }

    public String getControlWorkingYears() {
        return controlWorkingYears;
    }

    public void setControlWorkingYears(String controlWorkingYears) {
        this.controlWorkingYears = controlWorkingYears;
    }

    public String getLegalWorkingYears() {
        return legalWorkingYears;
    }

    public void setLegalWorkingYears(String legalWorkingYears) {
        this.legalWorkingYears = legalWorkingYears;
    }

    public String getCreditRecord() {
        return creditRecord;
    }

    public void setCreditRecord(String creditRecord) {
        this.creditRecord = creditRecord;
    }

    public String getLoanHistory() {
        return loanHistory;
    }

    public void setLoanHistory(String loanHistory) {
        this.loanHistory = loanHistory;
    }

    public String getRevoleCredit() {
        return revoleCredit;
    }

    public void setRevoleCredit(String revoleCredit) {
        this.revoleCredit = revoleCredit;
    }

	public String getIndusPolicy() {
		return indusPolicy;
	}

	public void setIndusPolicy(String indusPolicy) {
		this.indusPolicy = indusPolicy;
	}
}
