package com.beawan.survey.custInfo.dto;

public class CompUserCreditDto {
	
	private Integer id;
    private String serialNumber;
    private String queryName;//被查询人姓名
    private String relevanceType;//类型 （字典模式：01为授信企业，02为担保企业）
    private String queryDate;//查询日期
    private String noSettleSyrplus;//未结清贷款余额
    private String overdueAmount;//其中：当前逾期金额
    private String debtcardAmount;//贷记卡授信总额
    private String debtcardSurplus;//贷记卡已用额度
    private String debtcardOverdue;//其中：当前逾期余额
    private String overdueInfo;//近24个月贷款和贷记卡逾期情况
    private String guaranteeAmount;//对外担保总额
    private String guaranteeBalance;//对外担保余额
    private String guaranteeBad;//其中：不良余额
    private String remark;//其他事项说明
    private String guaCompId;//担保企业主键

    public String getGuaCompId() {
        return guaCompId;
    }

    public void setGuaCompId(String guaCompId) {
        this.guaCompId = guaCompId;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRelevanceType() {
		return relevanceType;
	}

	public void setRelevanceType(String relevanceType) {
		this.relevanceType = relevanceType;
	}

	public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getQueryName() {
        return queryName;
    }

    public void setQueryName(String queryName) {
        this.queryName = queryName;
    }

    public String getQueryDate() {
        return queryDate;
    }

    public void setQueryDate(String queryDate) {
        this.queryDate = queryDate;
    }

    public String getNoSettleSyrplus() {
        return noSettleSyrplus;
    }

    public void setNoSettleSyrplus(String noSettleSyrplus) {
        this.noSettleSyrplus = noSettleSyrplus;
    }

    public String getOverdueAmount() {
        return overdueAmount;
    }

    public void setOverdueAmount(String overdueAmount) {
        this.overdueAmount = overdueAmount;
    }

    public String getDebtcardAmount() {
        return debtcardAmount;
    }

    public void setDebtcardAmount(String debtcardAmount) {
        this.debtcardAmount = debtcardAmount;
    }

    public String getDebtcardSurplus() {
        return debtcardSurplus;
    }

    public void setDebtcardSurplus(String debtcardSurplus) {
        this.debtcardSurplus = debtcardSurplus;
    }

    public String getDebtcardOverdue() {
        return debtcardOverdue;
    }

    public void setDebtcardOverdue(String debtcardOverdue) {
        this.debtcardOverdue = debtcardOverdue;
    }

    public String getOverdueInfo() {
        return overdueInfo;
    }

    public void setOverdueInfo(String overdueInfo) {
        this.overdueInfo = overdueInfo;
    }

    public String getGuaranteeAmount() {
        return guaranteeAmount;
    }

    public void setGuaranteeAmount(String guaranteeAmount) {
        this.guaranteeAmount = guaranteeAmount;
    }

    public String getGuaranteeBad() {
        return guaranteeBad;
    }

    public void setGuaranteeBad(String guaranteeBad) {
        this.guaranteeBad = guaranteeBad;
    }

    public String getGuaranteeBalance() {
		return guaranteeBalance;
	}

	public void setGuaranteeBalance(String guaranteeBalance) {
		this.guaranteeBalance = guaranteeBalance;
	}

	public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
