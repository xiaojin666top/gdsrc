package com.beawan.survey.custInfo.dto;

/**
 * 企业 变更记录 信息表
 * @author
 *
 */
public class CompChangeRecordDto {
    private String customerNo;
    private String changeItem; //变更科目
    
    private Integer numCount;

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getChangeItem() {
		return changeItem;
	}

	public void setChangeItem(String changeItem) {
		this.changeItem = changeItem;
	}

	public Integer getNumCount() {
		return numCount;
	}

	public void setNumCount(Integer numCount) {
		this.numCount = numCount;
	}

    
}
