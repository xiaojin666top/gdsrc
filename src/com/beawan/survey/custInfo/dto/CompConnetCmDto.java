package com.beawan.survey.custInfo.dto;

public class CompConnetCmDto {

	private Long id;
	private String customerNo;//关联客户号
	private String companyName;//关联企业
	private String legalPerson;//法定代表人
	private String mainBusiness;//主营业务
	private String relationship;//关联关系
	private Double registerCapital;//注册资本
	private Double realCapital;//实收资本
	private String foundDate;//成立日期
	private String equityStructure;//股权结构
	private String companyType;//企业性质
	private String industry;//所属行业
	private String businessAddress;//经营地址
	private Double lastYearSale;//上一年销售额
	private Double lastYearNetProfit;//上一年净利润
	private Double totalAssets;//总资产
	private Double totalDebts;//总负债
	private String otherInfo;//其他情况说明
	private String loansBankInfo;//本行贷款情况
	
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustomerNo() {
		return this.customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Double getRegisterCapital() {
		return registerCapital;
	}

	public void setRegisterCapital(Double registerCapital) {
		this.registerCapital = registerCapital;
	}

	public Double getRealCapital() {
		return realCapital;
	}

	public void setRealCapital(Double realCapital) {
		this.realCapital = realCapital;
	}

	public String getFoundDate() {
		return this.foundDate;
	}

	public void setFoundDate(String foundDate) {
		this.foundDate = foundDate;
	}

	public String getLegalPerson() {
		return this.legalPerson;
	}

	public void setLegalPerson(String legalPerson) {
		this.legalPerson = legalPerson;
	}

	public String getEquityStructure() {
		return this.equityStructure;
	}

	public void setEquityStructure(String equityStructure) {
		this.equityStructure = equityStructure;
	}

	public String getCompanyType() {
		return this.companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public String getIndustry() {
		return this.industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getMainBusiness() {
		return mainBusiness;
	}

	public void setMainBusiness(String mainBusiness) {
		this.mainBusiness = mainBusiness;
	}

	public String getBusinessAddress() {
		return businessAddress;
	}

	public void setBusinessAddress(String businessAddress) {
		this.businessAddress = businessAddress;
	}

	public Double getLastYearSale() {
		return this.lastYearSale;
	}

	public void setLastYearSale(Double lastYearSale) {
		this.lastYearSale = lastYearSale;
	}

	public Double getLastYearNetProfit() {
		return this.lastYearNetProfit;
	}

	public void setLastYearNetProfit(Double lastYearNetProfit) {
		this.lastYearNetProfit = lastYearNetProfit;
	}

	public Double getTotalAssets() {
		return this.totalAssets;
	}

	public void setTotalAssets(Double totalAssets) {
		this.totalAssets = totalAssets;
	}

	public Double getTotalDebts() {
		return this.totalDebts;
	}

	public void setTotalDebts(Double totalDebts) {
		this.totalDebts = totalDebts;
	}

	public String getOtherInfo() {
		return this.otherInfo;
	}

	public void setOtherInfo(String otherInfo) {
		this.otherInfo = otherInfo;
	}

	public String getRelationship() {
		return this.relationship;
	}

	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}

	public String getLoansBankInfo() {
		return loansBankInfo;
	}

	public void setLoansBankInfo(String loansBankInfo) {
		this.loansBankInfo = loansBankInfo;
	}
}
