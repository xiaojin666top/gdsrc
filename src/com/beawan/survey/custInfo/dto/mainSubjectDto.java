package com.beawan.survey.custInfo.dto;

/**
 * 主要科目核实DTO
 * @author zxh
 * @date 2020/8/13 15:26
 */
public class mainSubjectDto {

    private Long taskId;
    private String customerNo;
    private String category; // 科目类别
    private String subject; // 主要科目
    private Double reportValue; // 财报值
    private Double verifyValue; // 核实值


    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Double getReportValue() {
        return reportValue;
    }

    public void setReportValue(Double reportValue) {
        this.reportValue = reportValue;
    }

    public Double getVerifyValue() {
        return verifyValue;
    }

    public void setVerifyValue(Double verifyValue) {
        this.verifyValue = verifyValue;
    }
}
