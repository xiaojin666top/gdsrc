package com.beawan.survey.custInfo.dto;


public class CompCreditDto {
	
	    private Integer id;
	    private String serialNumber;
	    private String compName;//企业名称
	    private String relevanceType;//类型 （字典模式：01为借款企业，02为担保企业）
	    private String queryDate;//查询日期
	    private String noSettleInterest;//未结清欠息余额
	    private String noSettleBankNum;//未结清业务授信银行家数
	    private String noSettleAmount;//未结清贷款总额
	    private String warnSurplus;//其中：关注类授信余额
	    private String badSurplus;//其中：不良类授信余额
	    private String settltBadInfo;//近24个月已结清不良信息
	    private String guaranteeAmount;//对外担保总额
	    private String guaranteeBalance;//对外担保余额
	    private String guaranteeBad;//其中：不良余额
	    private String remark;//其他事项说明

	    private String guaCompId;//担保企业主键

	    public String getGuaCompId() {
	        return guaCompId;
	    }

	    public void setGuaCompId(String guaCompId) {
	        this.guaCompId = guaCompId;
	    }
	    
	    public Integer getId() {
	        return id;
	    }

	    public void setId(Integer id) {
	        this.id = id;
	    }

	    public String getRelevanceType() {
			return relevanceType;
		}

		public void setRelevanceType(String relevanceType) {
			this.relevanceType = relevanceType;
		}

		public String getSerialNumber() {
	        return serialNumber;
	    }

	    public void setSerialNumber(String serialNumber) {
	        this.serialNumber = serialNumber;
	    }

	    public String getCompName() {
	        return compName;
	    }

	    public void setCompName(String compName) {
	        this.compName = compName;
	    }

	    public String getQueryDate() {
	        return queryDate;
	    }

	    public void setQueryDate(String queryDate) {
	        this.queryDate = queryDate;
	    }

	    public String getNoSettleInterest() {
	        return noSettleInterest;
	    }

	    public void setNoSettleInterest(String noSettleInterest) {
	        this.noSettleInterest = noSettleInterest;
	    }

	    public String getNoSettleBankNum() {
	        return noSettleBankNum;
	    }

	    public void setNoSettleBankNum(String noSettleBankNum) {
	        this.noSettleBankNum = noSettleBankNum;
	    }

	    public String getNoSettleAmount() {
	        return noSettleAmount;
	    }

	    public void setNoSettleAmount(String noSettleAmount) {
	        this.noSettleAmount = noSettleAmount;
	    }

	    public String getWarnSurplus() {
	        return warnSurplus;
	    }

	    public void setWarnSurplus(String warnSurplus) {
	        this.warnSurplus = warnSurplus;
	    }

	    public String getBadSurplus() {
	        return badSurplus;
	    }

	    public void setBadSurplus(String badSurplus) {
	        this.badSurplus = badSurplus;
	    }

	    public String getSettltBadInfo() {
	        return settltBadInfo;
	    }

	    public void setSettltBadInfo(String settltBadInfo) {
	        this.settltBadInfo = settltBadInfo;
	    }

	    public String getGuaranteeAmount() {
	        return guaranteeAmount;
	    }

	    public void setGuaranteeAmount(String guaranteeAmount) {
	        this.guaranteeAmount = guaranteeAmount;
	    }

	    public String getGuaranteeBad() {
	        return guaranteeBad;
	    }

	    public void setGuaranteeBad(String guaranteeBad) {
	        this.guaranteeBad = guaranteeBad;
	    }

	    public String getRemark() {
	        return remark;
	    }

	    public void setRemark(String remark) {
	        this.remark = remark;
	    }

		public String getGuaranteeBalance() {
			return guaranteeBalance;
		}

		public void setGuaranteeBalance(String guaranteeBalance) {
			this.guaranteeBalance = guaranteeBalance;
		}
}
