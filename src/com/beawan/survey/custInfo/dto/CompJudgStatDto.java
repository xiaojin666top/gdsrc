package com.beawan.survey.custInfo.dto;


/**
 * 分涉诉类型、案由类型统计条数
 * @author User
 *
 */
public class CompJudgStatDto {

	private String caseType;//裁判文书类型
	private String caseReason;//案由
	private Long count;
	
	
	public String getCaseType() {
		return caseType;
	}
	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}
	public String getCaseReason() {
		return caseReason;
	}
	public void setCaseReason(String caseReason) {
		this.caseReason = caseReason;
	}
	public Long getCount() {
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}
	
	
}
