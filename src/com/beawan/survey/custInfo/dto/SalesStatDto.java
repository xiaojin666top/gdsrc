package com.beawan.survey.custInfo.dto;

import javax.persistence.Column;

/**
 * 销售环节 销售统计DTO
 * @author zxh
 * @date 2020/7/29 12:59
 */
public class SalesStatDto {

    private Integer id;
    private String serNo;
    private String itemName;
    private double value1;
    private double value2;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getValue1() {
        return value1;
    }

    public void setValue1(double value1) {
        this.value1 = value1;
    }

    public double getValue2() {
        return value2;
    }

    public void setValue2(double value2) {
        this.value2 = value2;
    }
}
