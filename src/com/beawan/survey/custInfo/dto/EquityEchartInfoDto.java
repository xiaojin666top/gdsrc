package com.beawan.survey.custInfo.dto;

/**
 * @Author: xyh
 * @Date: 17/08/2020
 * @Description: 股权结构图数据（节点数据）
 */
public class EquityEchartInfoDto {
    private String name;
    private Integer category;//类型----0是企业，1自然人

    public EquityEchartInfoDto(String name, Integer category) {
        this.name = name;
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }
}
