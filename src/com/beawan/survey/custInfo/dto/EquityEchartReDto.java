package com.beawan.survey.custInfo.dto;

/**
 * @Author: xyh
 * @Date: 17/08/2020
 * @Description: 股权结构图数据（关联关系）
 */
public class EquityEchartReDto {
    private String source;//父节点---出箭头的节点
    private String target;//子节点---箭头指向的节点
    private String name;//连接线类型

    /**
     * 构造函数
     * @param source 出箭头的节点
     * @param target 被指向的节点
     * @param name 连接线的类型
     */
    public EquityEchartReDto(String source, String target, String name) {
        this.source = source;
        this.target = target;
        this.name = name;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
