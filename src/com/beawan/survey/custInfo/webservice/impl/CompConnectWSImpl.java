package com.beawan.survey.custInfo.webservice.impl;

import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.beawan.common.util.WSResult;
import com.beawan.survey.custInfo.bean.CompConnetCm;
import com.beawan.survey.custInfo.service.ICompConnectSV;
import com.beawan.survey.custInfo.webservice.ICompConnectWS;
import com.platform.util.JacksonUtil;

@Service("compConnectWS")
public class CompConnectWSImpl implements ICompConnectWS {

	private static final Logger log = Logger.getLogger(ICompConnectWS.class);

	@Resource
	protected ICompConnectSV compConnectSV;

	@Override
	public String saveCompConnectCm(String jsonInfo) {

		WSResult result = new WSResult();
		
		try {

			CompConnetCm data = (CompConnetCm) JSONObject.toBean(
					JSONObject.fromObject(jsonInfo), CompConnetCm.class);
			compConnectSV.saveCompConnectCm(data);

		} catch (Exception e) {

			result.status = WSResult.STATUS_ERROR;
			log.error("保存关联企业信息异常！", e);
			e.printStackTrace();
		}

		return result.json();
	}
	
	@Override
	public String saveCompConnectCmList(String custNo, String jsonArray) {

		WSResult result = new WSResult();
		
		try {

			compConnectSV.saveCompConnectCms(custNo, jsonArray);

		} catch (Exception e) {

			result.status = WSResult.STATUS_ERROR;
			log.error("保存关联企业信息异常！", e);
			e.printStackTrace();
		}

		return result.json();
	}

	@Override
	public String findCompConnectCmByCustNo(String custNo) {

		WSResult result = new WSResult();
		try {
			List<CompConnetCm> data = compConnectSV
					.findCompConnectCmByCustNo(custNo);
			if (data == null || data.size() == 0)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(data);
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("查询关联企业信息异常！", e);
			e.printStackTrace();
		}

		return result.json();
	}

	@Override
	public String deleteCompConnectCm(Long id) {

		WSResult result = new WSResult();
		
		try {

			CompConnetCm cm = compConnectSV.findById(id);
			if (cm != null)
				compConnectSV.deleteCompConnectCm(cm);

		} catch (Exception e) {

			result.status = WSResult.STATUS_ERROR;
			log.error("删除关联企业信息异常！", e);
			e.printStackTrace();
		}

		return result.json();
	}

}
