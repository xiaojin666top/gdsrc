package com.beawan.survey.custInfo.webservice.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.beawan.common.util.WSResult;
import com.beawan.survey.custInfo.bean.CompFinancing;
import com.beawan.survey.custInfo.bean.CompFinancingBank;
import com.beawan.survey.custInfo.bean.CompFinancingExtGuara;
import com.beawan.survey.custInfo.service.ICompFinancingSV;
import com.beawan.survey.custInfo.webservice.ICompFinancingWS;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.platform.util.JacksonUtil;

import net.sf.json.JSONObject;

@Service("compFinancingWS")
public class CompFinancingWSImpl implements ICompFinancingWS{
	
	private static final Logger log = Logger.getLogger(ICompFinancingWS.class);
	
	@Resource
	protected  ICompFinancingSV financingSV;
	
	public String findCompFinancingByTIdAndNo(Long taskId,String customerNo) {
		
		WSResult result = new WSResult();
		
		try {
			CompFinancing data = financingSV.findCompFinancingByTIdAndNo(taskId, customerNo);
			if(data == null)
				result.data = null;
			else 
				result.data = JacksonUtil.serialize(data);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取融资信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}
	
	@Override
	public String saveCompFinancing(String financingInfo) {
		
		WSResult result = new WSResult();
		ObjectMapper mapper = new ObjectMapper();
		CompFinancing financing = null;
		
		try {
			
			financing = mapper.readValue(financingInfo,CompFinancing.class);
			financingSV.saveCompFinancing(financing);
			
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存融资信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}


	@Override
	public String saveCompFinaExtGuara(String jsonInfo) {
		
		WSResult result = new WSResult();
		
		try {
			
			CompFinancingExtGuara data = (CompFinancingExtGuara) JSONObject.toBean(
					JSONObject.fromObject(jsonInfo), CompFinancingExtGuara.class);
			financingSV.saveCompFinaExtGuara(data);
			
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存对外融资与担保信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}
	
	@Override
	public String saveCompFinaExtGuaraList(Long taskId,String customerNo, String jsonArray) {
		
		WSResult result = new WSResult();
		
		try {
			
			financingSV.saveCompFinaExtGuaras(taskId, customerNo, jsonArray);
			
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存对外融资与担保信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findCompFinaExtGuaraByTIdAndNo(Long taskId, String customerNo) {
		
		WSResult result = new WSResult();
		
		try {
			
			List<CompFinancingExtGuara> data = financingSV.findCompFinaExtGuaraByTIdAndNo(taskId, customerNo);
			if(data == null || data.size()==0 )
				result.data = null;
			else
				result.data = JacksonUtil.serialize(data);
			
		} catch (Exception e) {
			
			result.data = WSResult.STATUS_ERROR;
			log.error("获取对外融资与担保信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}
	
	@Override
	public String deleteCompFinaExtGuara(Long id) {
		
		WSResult result = new WSResult();
		
		try {
			
			CompFinancingExtGuara data = financingSV.findCompFinaExtGuaraById(id);
			if(data != null)
				financingSV.deleteCompFinaExtGuara(data);
			
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("删除对外融资与担保信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveCompFinaBank(String jsonInfo) {
		
		WSResult result = new WSResult();
		
		try {
			
			CompFinancingBank data = (CompFinancingBank) JSONObject.toBean(
					JSONObject.fromObject(jsonInfo), CompFinancingBank.class);
			financingSV.saveCompFinaBank(data);
			
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存企业银行融资信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveCompFinaBankList(Long taskId, String customerNo,
			String jsonArray) {
		
		WSResult result = new WSResult();
		
		try {
			
			financingSV.saveCompFinaBanks(taskId, customerNo, jsonArray);
			
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存企业银行融资信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String deleteCompFinaBank(Long id) {
		WSResult result = new WSResult();
		try {
			CompFinancingBank data = financingSV.findCompFinaBankById(id);
			if(data != null)
				financingSV.deleteCompFinaBank(data);
		}catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("删除企业银行融资信息异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String findCompFinaBankByTIdAndNo(Long taskId, String customerNo) {
		
		WSResult result = new WSResult();
		
		try {
			List<CompFinancingBank> data = financingSV.findCompFinaBankByTIdAndNo(taskId, customerNo);
			if(CollectionUtils.isEmpty(data))
				data = financingSV.syncCompFinaBankFromCmis(taskId, customerNo);
			if(data == null || data.size()==0 )
				result.data = null;
			else
				result.data = JacksonUtil.serialize(data);
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取企业银行融资信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

}
