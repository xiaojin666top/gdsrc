package com.beawan.survey.custInfo.webservice.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.beawan.analysis.finansis.common.FinaRowData;
import com.beawan.analysis.finansis.service.IFinanasisService;
import com.beawan.common.SysConstants;
import com.beawan.common.util.WSResult;
import com.beawan.customer.service.ICusFSToolSV;
import com.beawan.survey.custInfo.bean.CompFinance;
import com.beawan.survey.custInfo.bean.CompFinanceOperCF;
import com.beawan.survey.custInfo.bean.CompFinanceTax;
import com.beawan.survey.custInfo.service.ICompFinanceSV;
import com.beawan.survey.custInfo.webservice.ICompFinanceWS;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;

@Service("compFinanceWS")
public class CompFinanceWSImpl implements ICompFinanceWS {

	private static final Logger log = Logger.getLogger(ICompFinanceWS.class);

	@Resource
	private ICompFinanceSV financeSV;
	
	@Resource
	private ITaskSV taskSV;
	
	@Resource
	private IFinanasisService finanasisSV;
	
	@Resource
	private ICusFSToolSV cusFSToolSV;

	@Override
	public String findCompFinanceByTIdAndNo(Long taskId, String custNo) {
		WSResult result = new WSResult();
		try {
			Task task = taskSV.getTaskById(taskId);
			
			CompFinance compFinance = financeSV.findCompFinanceByTIdAndNo(taskId, custNo);

			if (compFinance == null) {
				compFinance=new CompFinance();
				compFinance.setTaskId(taskId);
				compFinance.setCustomerNo(custNo);
			}
			String ratioAnalysis = compFinance.getRatioAnalysis();
			if(StringUtil.isEmptyString(ratioAnalysis)&&
					SysConstants.CusFinaValidFlag.AVAIL.equals(task.getFinaSyncFlag())){
				//获得系统分析结论(比率指标分析)
				Map<String, Object> conclusion = finanasisSV.genAnalysisConclusion(taskId, custNo,
						SysConstants.SysFinaAnalyPart.RATIO_INDEX);
				ratioAnalysis = "盈利能力：" + conclusion.get("RatioOne").toString()
						      + "偿债能力：" + conclusion.get("RatioTwo").toString()
						      + "运营能力：" + conclusion.get("RatioThree").toString()
						      + "增长能力：" + conclusion.get("RatioFour").toString();
			}	
			String cashFlowAnaly = compFinance.getCashFlowAnaly();
			if(StringUtil.isEmptyString(cashFlowAnaly)&&
					SysConstants.CusFinaValidFlag.AVAIL.equals(task.getFinaSyncFlag())){
				//获得系统分析结论(现金流量分析)
				Map<String, Object> conclusion = finanasisSV.genAnalysisConclusion(taskId, custNo,
						SysConstants.SysFinaAnalyPart.CASH_FLOW);
				cashFlowAnaly = conclusion.get("first").toString()
						      + conclusion.get("third").toString()
						      + conclusion.get("four").toString()
						      + conclusion.get("five").toString()
						      + conclusion.get("seven").toString();
			}
			String incomeAnaly = compFinance.getIncomeAnaly();
			if(StringUtil.isEmptyString(incomeAnaly)&&
					SysConstants.CusFinaValidFlag.AVAIL.equals(task.getFinaSyncFlag())){
				//获得系统分析结论(盈利情况分析)
				Map<String, Object> conclusion = finanasisSV.genAnalysisConclusion(taskId, custNo,
						SysConstants.SysFinaAnalyPart.INCOME);
				incomeAnaly = conclusion.get("Incsecond").toString()
							+ conclusion.get("IncsecondThree").toString()
						    + conclusion.get("IncThird").toString()
						    + conclusion.get("IncFour").toString()
						    + conclusion.get("IncFive").toString()
						    + conclusion.get("IncSix").toString();
			}
			compFinance.setCashFlowAnaly(cashFlowAnaly);
			compFinance.setIncomeAnaly(incomeAnaly);
			compFinance.setRatioAnalysis(ratioAnalysis);
			
		result.data = JacksonUtil.serialize(compFinance);
				
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取财务状况信息异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveCompFinance(String financeInfo) {
		WSResult result = new WSResult();
		ObjectMapper mapper = new ObjectMapper();
		CompFinance data = null;
		try {
			data = mapper.readValue(financeInfo, CompFinance.class);
			financeSV.saveCompFinance(data);
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("保存财务状况信息异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String findFinanceIndexs(Long taskId,String cusNo) {
		WSResult result=new WSResult();
		try {
			Task task = taskSV.getTaskById(taskId);
			
			List<Map<String, Double>> cmisRatioIndexData = cusFSToolSV.findRepDataByType(cusNo,
					task.getYear(), task.getMonth(), SysConstants.CmisFianRepType.RATIO_INDEX);
			List<FinaRowData> list = financeSV.findFinanceIndexs(cusNo,
					task.getIndustry(), cmisRatioIndexData);
			if(CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取比率指标异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String findFinanceCashFlow(Long taskId, String cusNo) {
		WSResult result=new WSResult();
		try {
			List<FinaRowData> finaRowData = financeSV.findFinanceCashFlow(taskId, cusNo);
			if(CollectionUtils.isEmpty(finaRowData)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(finaRowData);
			}
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取现金流指标异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}

	
	@Override
	public String findOperCashFlow(Long taskId, String cusNo) {
		WSResult result=new WSResult();
		try {
			List<CompFinanceOperCF> finaOperCFs = financeSV.findFinanceOperCFByTIdAndNo(taskId, cusNo);
			if(CollectionUtils.isEmpty(finaOperCFs)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(finaOperCFs);
			}
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取企业现金流回笼情况异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}
	
	
	@Override
	public String findFinanceTax(Long taskId, String cusNo) {
		WSResult result=new WSResult();
		try {
			List<CompFinanceTax> list=financeSV.findFinanceTaxByTIdAndNo(taskId, cusNo);
			if(CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取税收指标异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String findFinanceIncome(Long taskId, String cusNo) {
		WSResult result=new WSResult();
		try {
			List<FinaRowData> list=financeSV.findFinanceIncome(taskId, cusNo);
			if(CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取盈利情况异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveFinanceTaxs(Long taskId, String customerNo, String jsonArray) {
		WSResult result=new WSResult();
		try {
			financeSV.saveFinanceTaxes(taskId, customerNo, jsonArray);
			
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("保存纳税数据信息异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveOperCashFlow(Long taskId, String customerNo, String jsonArray) {
		WSResult result=new WSResult();
		try {
			financeSV.saveFinanceOperCFs(taskId, customerNo, jsonArray);
			
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("保存现金回笼情况信息异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	

}
