package com.beawan.survey.custInfo.webservice.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.beawan.base.service.ITableSubjCodeSV;
import com.beawan.common.util.WSResult;
import com.beawan.survey.custInfo.bean.CompRun;
import com.beawan.survey.custInfo.bean.CompRunArea;
import com.beawan.survey.custInfo.bean.CompRunConst;
import com.beawan.survey.custInfo.bean.CompRunConstSPDetail;
import com.beawan.survey.custInfo.bean.CompRunConstSPStruc;
import com.beawan.survey.custInfo.bean.CompRunCustomer;
import com.beawan.survey.custInfo.bean.CompRunEsta;
import com.beawan.survey.custInfo.bean.CompRunEstaDevedProj;
import com.beawan.survey.custInfo.bean.CompRunEstaDevingProj;
import com.beawan.survey.custInfo.bean.CompRunEstaLand;
import com.beawan.survey.custInfo.bean.CompRunFixed;
import com.beawan.survey.custInfo.bean.CompRunGeneral;
import com.beawan.survey.custInfo.bean.CompRunPower;
import com.beawan.survey.custInfo.bean.CompRunPrdSale;
import com.beawan.survey.custInfo.bean.CompRunProduce;
import com.beawan.survey.custInfo.bean.CompRunSupply;
import com.beawan.survey.custInfo.service.ICompRunSV;
import com.beawan.survey.custInfo.webservice.ICompRunWS;
import com.beawan.task.dao.ITaskDAO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.platform.util.JacksonUtil;

@Service("compRunWS")
public class CompRunWSImpl implements ICompRunWS {

	private static final Logger log = Logger.getLogger(ICompRunWS.class);
	@Resource
	private ICompRunSV  compRunSV;
	@Resource
	private ITaskDAO taskDAO;
	@Resource
	private ITableSubjCodeSV tableSubjCodeSV;

	@Override
	public String findCompRunByTIdAndNo(Long taskId, String customerNo) {
		
		WSResult result = new WSResult();
		
		try {
			
			CompRun custRun = compRunSV.queryCompRunByTIdAndNo(taskId, customerNo);
			
			if (custRun == null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(custRun);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			result.message = "获取经营信息异常！";
			
			log.error(result.message, e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveCompRun(String runInfo) {
		
		WSResult result = new WSResult();
		
		ObjectMapper mapper = new ObjectMapper();
		CompRun compRun = null;
		
		try {
			
			compRun = mapper.readValue(runInfo, CompRun.class);
			compRunSV.saveCompRun(compRun);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存经营信息异常！", e);
			e.printStackTrace();
		}
		
		return result.json();
	}
	
	@Override
	public String saveRunAreas(Long taskId, String custNo, String jsonArray) {
		// TODO Auto-generated method stub
			WSResult result = new WSResult();
		
		try {
			
			compRunSV.saveRunAreas(taskId, custNo, jsonArray);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存经营场所信息列表异常！", e);
			e.printStackTrace();
		}
		
		return result.json();
		
	}

	@Override
	public String queryRunAreaByTIdAndNo(Long taskId, String custNo) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		
		try {
			
			List<CompRunArea> areas=compRunSV.queryRunAreaByTIdAndNo(taskId, custNo);
			if(CollectionUtils.isEmpty(areas))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(areas);
			
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取经营场所信息列表异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deleteRunArea(CompRunArea runArea) {
		// TODO Auto-generated method stub
				WSResult result = new WSResult();
		
		try {
			
			compRunSV.deleteRunArea(runArea);;
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("删除经营场所信息列表异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String findCompRunSupplyByTIdAndNo(Long taskId,String custNo) {
			
		WSResult result = new WSResult();
	
		try {
			List<CompRunSupply> areas=compRunSV.queryRunSupplyByTIdAndNo(taskId, custNo);
			if(CollectionUtils.isEmpty(areas))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(areas);
			
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取供应商信息列表异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveCompRunSupply(Long taskId, String custNo, String jsonArray){
		// TODO Auto-generated method stub
					WSResult result = new WSResult();
				
				try {
					
					compRunSV.saveRunSupplys(taskId, custNo, jsonArray);
					
				} catch (Exception e) {
					
					result.status = WSResult.STATUS_ERROR;
					log.error("保存供应商信息列表异常！", e);
					e.printStackTrace();
				}
				
				return result.json();
	}

	@Override
	public String findCompProduceByTIdAndNo(Long taskId, String customerNo) {
		// TODO Auto-generated method stub
			WSResult result = new WSResult();
		
		try {
			
			CompRunProduce custRun = compRunSV.queryRunProduceByTIdAndNo(taskId, customerNo);
			
			if (custRun == null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(custRun);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			result.message = "获取经营生产信息异常！";
			
			log.error(result.message, e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveCompProduce(String runInfo) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		
		ObjectMapper mapper = new ObjectMapper();
		CompRunProduce compRunProduce = null;
		
		try {
			compRunProduce = mapper.readValue(runInfo, CompRunProduce.class);
			compRunSV.saveRunProduce(compRunProduce);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存经营生产信息异常！", e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findCompRunCustomerByTIdAndNo(Long taskId, String customerNo) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		
		try {
			
			List<CompRunCustomer> areas=compRunSV.queryRunCustomerByTIdAndNo(taskId, customerNo);
			if(CollectionUtils.isEmpty(areas))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(areas);
			
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取下游客户信息列表异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveCompRunCustomer(Long taskId, String custNo, String jsonArray) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
	
	try {
		
		compRunSV.saveRunCustomers(taskId, custNo, jsonArray);
		
	} catch (Exception e) {
		
		result.status = WSResult.STATUS_ERROR;
		log.error("保存下游客户信息列表异常！", e);
		e.printStackTrace();
	}
	
	return result.json();
	}

	@Override
	public String findCompRunPowerByTIdAndNo(Long taskId, String customerNo) {
		// TODO Auto-generated method stub
			WSResult result = new WSResult();
		
		try {
			
			List<CompRunPower> areas=compRunSV.queryRunPowerByTIdAndNo(taskId, customerNo);
			if(CollectionUtils.isEmpty(areas))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(areas);
			
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取能源耗用量及人工情况异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveCompRunPower(Long taskId, String custNo, String jsonArray) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		
		try {
			
			compRunSV.saveRunPowers(taskId, custNo, jsonArray);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存能源耗用量及人工情况异常！", e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findCompRunPrdSaleByTIdAndNo(Long taskId, String customerNo) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		
		try {
			
			List<CompRunPrdSale> areas=compRunSV.queryRunPrdSaleByTIdAndNo(taskId, customerNo);
			if(CollectionUtils.isEmpty(areas))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(areas);
			
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取主要产品销售情况异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveCompRunPrdSale(Long taskId, String custNo, String jsonArray) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		
		try {
			
			compRunSV.saveRunPrdSales(taskId, custNo, jsonArray);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存主要产品销售情况异常！", e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String queryRunConstByTIdAndNo(Long taskId, String custNo) {
		// TODO Auto-generated method stub		
		WSResult result = new WSResult();
		
		try {
			CompRunConst custRunConst = compRunSV.queryRunConstByTIdAndNo(taskId, custNo);
			if (custRunConst == null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(custRunConst);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			result.message = "获取经营信息异常！";
			log.error(result.message, e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveRunConst(String compRunConst) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		ObjectMapper mapper = new ObjectMapper();
		CompRunConst cRunConst = null;
		try {
			
			cRunConst = mapper.readValue(compRunConst, CompRunConst.class);
			compRunSV.saveRunConst(cRunConst);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存经营信息异常！", e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String deleteRunConst(String compRunConst) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		ObjectMapper mapper = new ObjectMapper();
		CompRunConst cRunConst = null;
		try {
			
			cRunConst = mapper.readValue(compRunConst, CompRunConst.class);
			compRunSV.deleteRunConst(cRunConst);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("删除经营信息异常！", e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String queryRunConstSPDetailByTIdAndNo(Long taskId, String custNo) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		
		try {
			
			List<CompRunConstSPDetail> lists=compRunSV.queryRunConstSPDetailByTIdAndNo(taskId, custNo);
			if(CollectionUtils.isEmpty(lists))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(lists);
			
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取在手已签施工工程明细列表异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String queryRunConstSPDetailById(Long id) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		
		try {
			CompRunConstSPDetail cDetail = compRunSV.queryRunConstSPDetailById(id);
			if (cDetail == null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(cDetail);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			result.message = "获取在手已签施工工程明细异常！";
			log.error(result.message, e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveRunConstSPDetail(String compRunConstSPDetail) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		ObjectMapper mapper = new ObjectMapper();
		CompRunConstSPDetail cDetail = null;
		try {
			
			cDetail = mapper.readValue(compRunConstSPDetail, CompRunConstSPDetail.class);
			compRunSV.saveRunConstSPDetail(cDetail);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存在手已签施工工程明细异常！", e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveRunConstSPDetails(Long taskId, String custNo, String jsonArray) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		try {
			
			compRunSV.saveRunConstSPDetails(taskId, custNo, jsonArray);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存在手已签施工工程明细列表异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deleteRunConstSPDetail(String compRunConstSPDetail) {
		// TODO Auto-generated method stub
				WSResult result = new WSResult();
				ObjectMapper mapper = new ObjectMapper();
				CompRunConstSPDetail cDetail = null;
				try {
					
					cDetail = mapper.readValue(compRunConstSPDetail, CompRunConstSPDetail.class);
					compRunSV.deleteRunConstSPDetail(cDetail);
				} catch (Exception e) {
					
					result.status = WSResult.STATUS_ERROR;
					log.error("删除在手已签施工工程明细异常！", e);
					e.printStackTrace();
				}
				
				return result.json();
	}

	@Override
	public String queryRunConstSPStrucByTIdAndNo(Long taskId, String custNo) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		
		try {
			
			List<CompRunConstSPStruc> lists=compRunSV.queryRunConstSPStrucByTIdAndNo(taskId, custNo);
			if(CollectionUtils.isEmpty(lists))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(lists);
			
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取在手已签施工工程结构列表异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String queryRunConstSPStrucById(Long id) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		
		try {
			CompRunConstSPStruc cStruc = compRunSV.queryRunConstSPStrucById(id);
			if (cStruc == null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(cStruc);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			result.message = "获取在手已签施工工程结构异常！";
			log.error(result.message, e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveRunConstSPStruc(String compRunConstSPStruc) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		ObjectMapper mapper = new ObjectMapper();
		CompRunConstSPStruc cStruc = null;
		try {
			
			cStruc = mapper.readValue(compRunConstSPStruc, CompRunConstSPStruc.class);
			compRunSV.saveRunConstSPStruc(cStruc);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存在手已签施工工程结构异常！", e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveRunConstSPStrucs(Long taskId, String custNo, String jsonArray) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		try {
			
			compRunSV.saveRunConstSPStrucs(taskId, custNo, jsonArray);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存在手已签施工工程结构列表异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deleteRunConstSPStruc(String compRunConstSPStruc) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		ObjectMapper mapper = new ObjectMapper();
		CompRunConstSPStruc cStruc = null;
		try {
			
			cStruc = mapper.readValue(compRunConstSPStruc, CompRunConstSPStruc.class);
			compRunSV.deleteRunConstSPStruc(cStruc);
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("删除在手已签施工工程结构异常！", e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String queryRunFixedByTIdAndNo(Long taskId, String custNo) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		try {
			CompRunFixed cFixed = compRunSV.queryRunFixedByTIdAndNo(taskId, custNo);
			if (cFixed == null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(cFixed);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			result.message = "获取经营情况异常！";
			log.error(result.message, e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveRunFixed(String compRunFixed) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		ObjectMapper mapper = new ObjectMapper();
		CompRunFixed cFixed = null;
		try {
			
			cFixed = mapper.readValue(compRunFixed, CompRunFixed.class);
			compRunSV.saveRunFixed(cFixed);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存经营信息异常！", e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String deleteRunFixed(String compRunFixed) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		ObjectMapper mapper = new ObjectMapper();
		CompRunFixed cFixed = null;
		try {
			
			cFixed = mapper.readValue(compRunFixed, CompRunFixed.class);
			compRunSV.deleteRunFixed(cFixed);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("删除经营信息异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String queryRunEstaByTIdAndNo(Long taskId, String custNo) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		try {
			CompRunEsta cEsta = compRunSV.queryRunEstaByTIdAndNo(taskId, custNo);
			if (cEsta == null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(cEsta);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			result.message = "获取经营情况异常！";
			log.error(result.message, e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveRunEsta(String compRunEsta) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		ObjectMapper mapper = new ObjectMapper();
		CompRunEsta cEsta = null;
		try {
			
			cEsta = mapper.readValue(compRunEsta, CompRunEsta.class);
			compRunSV.saveRunEsta(cEsta);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存经营信息异常！", e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String deleteRunEsta(String compRunEsta) {
		// TODO Auto-generated method stub
				WSResult result = new WSResult();
				ObjectMapper mapper = new ObjectMapper();
				CompRunEsta cEsta = null;
				try {
					
					cEsta = mapper.readValue(compRunEsta, CompRunEsta.class);
					compRunSV.deleteRunEsta(cEsta);
					
				} catch (Exception e) {
					
					result.status = WSResult.STATUS_ERROR;
					log.error("删除经营信息异常！", e);
					e.printStackTrace();
				}
				
				return result.json();
	}

	@Override
	public String queryRunEstaLandByTIdAndNo(Long taskId, String custNo) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		
		try {
			
			List<CompRunEstaLand> lists=compRunSV.queryRunEstaLandByTIdAndNo(taskId, custNo);
			if(CollectionUtils.isEmpty(lists))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(lists);
			
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取房地产土地储备信息列表异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String queryRunEstaLandById(Long id) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		
		try {
			
			CompRunEstaLand cLand=compRunSV.queryRunEstaLandById(id);
			if (cLand == null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(cLand);
			
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取房地产土地储备信息异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveRunEstaLand(String compRunEstaLand) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		ObjectMapper mapper = new ObjectMapper();
		CompRunEstaLand cEstaLand = null;
		try {
			
			cEstaLand = mapper.readValue(compRunEstaLand, CompRunEstaLand.class);
			compRunSV.saveRunEstaLand(cEstaLand);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存房地产土地储备信息异常！", e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveRunEstaLands(Long taskId, String custNo, String jsonArray) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		try {
			
			compRunSV.saveRunEstaLands(taskId, custNo, jsonArray);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存房地产土地储备信息列表异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deleteRunEstaLand(String compRunEstaLand) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		ObjectMapper mapper = new ObjectMapper();
		CompRunEstaLand cEstaLand = null;
		try {
			
			cEstaLand = mapper.readValue(compRunEstaLand, CompRunEstaLand.class);
			compRunSV.deleteRunEstaLand(cEstaLand);
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("删除房地产土地储备信息异常！", e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String queryRunEstaDevingProjByTIdAndNo(Long taskId, String custNo) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		
		try {
			
			List<CompRunEstaDevingProj> lists=compRunSV.queryRunEstaDevingProjByTIdAndNo(taskId, custNo);
			if(CollectionUtils.isEmpty(lists))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(lists);
			
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取房地产正在开发项目情况列表异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String queryRunEstaDevingProjById(Long id) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		
		try {
			CompRunEstaDevingProj cEstaDevingProj = compRunSV.queryRunEstaDevingProjById(id);
			if (cEstaDevingProj == null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(cEstaDevingProj);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			result.message = "获取房地产正在开发项目情况异常！";
			log.error(result.message, e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveRunEstaDevingProj(String compRunEstaDevingProj) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		ObjectMapper mapper = new ObjectMapper();
		CompRunEstaDevingProj cDevingProj = null;
		try {			
			cDevingProj = mapper.readValue(compRunEstaDevingProj, CompRunEstaDevingProj.class);
			compRunSV.saveRunEstaDevingProj(cDevingProj);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存房地产正在开发项目异常!", e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveRunEstaDevingProjs(Long taskId, String custNo, String jsonArray) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		try {
			
			compRunSV.saveRunEstaDevingProjs(taskId, custNo, jsonArray);
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("保存房地产正在开发项目列表异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deleteRunEstaDevingProj(String compRunEstaDevingProj) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		ObjectMapper mapper = new ObjectMapper();
		CompRunEstaDevingProj cDetail = null;
		try {
			
			cDetail = mapper.readValue(compRunEstaDevingProj, CompRunEstaDevingProj.class);
			compRunSV.deleteRunEstaDevingProj(cDetail);
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("删除房地产正在开发项目情况异常！", e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String queryRunEstaDevedProjByTIdAndNo(Long taskId, String custNo) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		
		try {
			
			List<CompRunEstaDevedProj> lists=compRunSV.queryRunEstaDevedProjByTIdAndNo(taskId, custNo);
			if(CollectionUtils.isEmpty(lists))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(lists);
			
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取房地产已开发项目情况列表异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String queryRunEstaDevedProjById(Long id) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		
		try {
			CompRunEstaDevedProj cDevedProj = compRunSV.queryRunEstaDevedProjById(id);
			if (cDevedProj == null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(cDevedProj);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			result.message = "获取房地产已开发项目情况异常！";
			log.error(result.message, e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveRunEstaDevedProj(String compRunEstaDevedProj) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		ObjectMapper mapper = new ObjectMapper();
		CompRunEstaDevedProj cEstaDevedProj = null;
		try {
			
			cEstaDevedProj = mapper.readValue(compRunEstaDevedProj, CompRunEstaDevedProj.class);
			compRunSV.saveRunEstaDevedProj(cEstaDevedProj);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存房地产已开发项目情况异常！", e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveRunEstaDevedProjs(Long taskId, String custNo, String jsonArray) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		try {
			
			compRunSV.saveRunEstaDevedProjs(taskId, custNo, jsonArray);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存房地产已开发项目情况列表异常！", e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deleteRunEstaDevedProj(String compRunEstaDevedProj) {
		// TODO Auto-generated method stub
		WSResult result = new WSResult();
		ObjectMapper mapper = new ObjectMapper();
		CompRunEstaDevedProj cEstaDevedProj = null;
		try {
			
			cEstaDevedProj = mapper.readValue(compRunEstaDevedProj, CompRunEstaDevedProj.class);
			compRunSV.deleteRunEstaDevedProj(cEstaDevedProj);
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("删除房地产已开发项目情况异常！", e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findCompGeneralRunByTIdAndNo(Long taskId, String customerNo) {
WSResult result = new WSResult();
		
		try {
			
			CompRunGeneral custRun = compRunSV.queryRunGeneralByTIdAndNo(taskId, customerNo);
			
			if (custRun == null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(custRun);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			result.message = "获取经营信息异常！";
			
			log.error(result.message, e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveCompGeneralRun(String runInfo) {
		WSResult result = new WSResult();
		
		ObjectMapper mapper = new ObjectMapper();
		CompRunGeneral compRun = null;
		
		try {
			
			compRun = mapper.readValue(runInfo, CompRunGeneral.class);
			compRunSV.saveRunGeneral(compRun);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存经营信息异常！", e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	
	
	
	
}
