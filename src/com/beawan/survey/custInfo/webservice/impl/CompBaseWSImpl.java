package com.beawan.survey.custInfo.webservice.impl;

import java.util.List;

import javax.annotation.Resource;

import com.beawan.survey.custInfo.dto.CompBaseDto;
import com.beawan.survey.custInfo.dto.CompBaseManagerDto;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.beawan.base.service.ISysTreeDicSV;
import com.beawan.common.SysConstants;
import com.beawan.common.util.WSResult;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.beawan.library.bean.TreeData;
import com.beawan.survey.custInfo.bean.CompBase;
import com.beawan.survey.custInfo.bean.CompBaseEquity;
import com.beawan.survey.custInfo.bean.CompBaseManager;
import com.beawan.survey.custInfo.bean.PersonBase;
import com.beawan.survey.custInfo.dao.ICompBaseDAO;
import com.beawan.survey.custInfo.dao.ICompFinancingDAO;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.beawan.survey.custInfo.webservice.ICompBaseWS;
import com.beawan.task.dao.ITaskDAO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;

import net.sf.json.JSONObject;

@Service("custBaseWS")
public class CompBaseWSImpl implements ICompBaseWS{

	private static final Logger log = Logger.getLogger(ICompBaseWS.class);
	
	@Resource
	protected ICompBaseDAO compBaseDAO;
	
	@Resource
	protected ICompFinancingDAO  financingDao;
	
	@Resource
	private ITaskDAO taskDAO;
	
	@Resource
	protected ICompBaseSV compBaseSV;
	
	@Resource
	protected ISysTreeDicSV sysTreeDicSV;
	
	@Resource
	protected ICmisInInvokeSV cmisInInvokeSV;

	@Override
	public String findCompBaseByCustNo(String customerNo) {

		WSResult result = new WSResult();
		
		try {

			CompBaseDto custBase = compBaseSV.findCompBaseByCustNo(customerNo);
			if(custBase == null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(custBase);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取基本信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveCompBase(String custBaseInfo) {
		
		WSResult result = new WSResult();
		
		ObjectMapper mapper = new ObjectMapper();
		CompBaseDto custBase = null;
		
		try {
			custBase = mapper.readValue(custBaseInfo, CompBaseDto.class);
			compBaseSV.saveCompBase(custBase);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存基本信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findCompEquityByCustNo(String custNo) {
		
		WSResult result = new WSResult();
		
		try {
			List<CompBaseEquity> equities = compBaseSV.findCompEquityByCustNo(custNo);
			if(CollectionUtils.isEmpty(equities))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(equities);
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取股权信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}
	
	@Override
	public String saveCompEquity(String jsonInfo) {
		
		WSResult result = new WSResult();
		
		try {
			
			CompBaseEquity equity = (CompBaseEquity) JSONObject.toBean(
					JSONObject.fromObject(jsonInfo), CompBaseEquity.class);
			
			compBaseSV.saveCompEquity(equity);
				
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存股权结构信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveCompEquityList(String custNo , String jsonArray) {
		
		WSResult result = new WSResult();
		
		try {
			compBaseSV.saveCompEquitys(custNo, jsonArray);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存股权结构信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}
	
	@Override
	public String deleteCompEquityById(Long id) {
		
		WSResult result = new WSResult();
		
		try {
			CompBaseEquity equity = compBaseSV.findCompEquityById(id);
			compBaseSV.deleteCompEquity(equity);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存股权结构信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findCompManagerByCustNo(String customerNo) {
		
		WSResult result = new WSResult();
		
		try {
			List<CompBaseManagerDto> managerList = compBaseSV.findCompManagerByCustNo(customerNo);
			if(CollectionUtils.isEmpty(managerList))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(managerList);
		} catch (Exception e) {

			result.status = WSResult.STATUS_ERROR;
			log.error("获取管理人员信息异常！", e);
			e.printStackTrace();
		}
		
		return result.json();
	}
	
	@Override
	public String saveCompManagerList(String custNo, String jsonArray) {
		
		WSResult result = new WSResult();
		
		try {
			compBaseSV.saveCompManagers(custNo, jsonArray);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存管理人员信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String saveCompManager(String userId,String jsonInfo) {
		WSResult result = new WSResult();
		try {

			CompBaseManagerDto manager = (CompBaseManagerDto) JSONObject.toBean(
					JSONObject.fromObject(jsonInfo), CompBaseManagerDto.class);
			compBaseSV.saveCompManager(userId, manager);
			
		}catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("保存管理人员信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}
	
	@Override
	public String deleteCompManagerById(Long id) {
		
		WSResult result = new WSResult();
		
		try {
			
			CompBaseManagerDto manager = compBaseSV.findCompManagerById(id);
			//if(manager != null)
				//compBaseSV.deleteCompManager(manager);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("删除管理人员信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String findPersonBaseByCustNo(String custNo)  {
		
		WSResult result = new WSResult();
		
		try {
			PersonBase personBase = compBaseSV.findPersonBaseByCustNo(custNo);
			if(personBase==null) {
				personBase=new PersonBase();
			}
			result.data = JacksonUtil.serialize(personBase);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取个人基本信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String savePersonBase(String personBase)  {
		
		WSResult result = new WSResult();		
		try {
			PersonBase custBase = JacksonUtil.fromJson(personBase, PersonBase.class);
			compBaseSV.savePersonBase(custBase);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("保存基本信息异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String getCusIndustry(String customerNo) {

		WSResult result = new WSResult();
		
		try {
			
			String industry = null;
			//先查本地数据
			CompBaseDto compBase = compBaseSV.findCompBaseByCustNo(customerNo);
			if(compBase != null && !StringUtil.isEmptyString(compBase.getIndustryCode()))
				industry = compBase.getIndustryCode();
			else{
				//再查信贷系统数据
				compBase = cmisInInvokeSV.queryCompBaseInfo(customerNo);
				if(compBase != null)
					industry = compBase.getIndustryCode();
			}
			
			TreeData treeData = sysTreeDicSV.findByEnNameAndType(industry,
					SysConstants.TreeDicConstant.GB_IC_4754_2017);
				
			if(treeData == null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(treeData);
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取客户行业异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}
	
}
