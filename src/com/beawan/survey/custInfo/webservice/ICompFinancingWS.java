package com.beawan.survey.custInfo.webservice;


public interface ICompFinancingWS {
	
	/**
	 * @Description (保存企业融资、对外担保、或有负债情况)
	 * @param data 企业基本融资信息实体
	 */
	public String saveCompFinancing(String financingInfo) ;

	/**
	 * @Description (通过客户号任务号查找企业融资、对外担保、或有负债情况)
	 * @param TaskId
	 * @return
	 * @
	 */
	public String findCompFinancingByTIdAndNo(Long taskId, String customerNo) ;
	
	
	/**
	 * @Description 保存企业对外担保信息，保存单条记录
	 * @param jsonInfo 企业对外担保信息json串
	 * @return
	 */
	public String saveCompFinaExtGuara(String jsonInfo) ;
	
	/**
	 * @Description 保存企业对外担保信息列表，先删除原来的，再插入新的
	 * @param taskId 任务编号
	 * @param customerNo 客户号
	 * @param jsonArray 企业对外担保信息列表
	 * @return
	 */
	public String saveCompFinaExtGuaraList(Long taskId, String customerNo, String jsonArray) ;
	
	/**
	 * @Description 删除保存企业对外担保信息
	 * @param id 唯一标识
	 * @return
	 */
	public String deleteCompFinaExtGuara(Long id) ;

	/**
	 * @Description (通过客户号查找对外融资与担保信息)
	 * @param TaskId
	 * @return
	 * @
	 */
	public String findCompFinaExtGuaraByTIdAndNo(Long taskId, String customerNo) ;
	
	
	/**
	 * @Description 保存企业银行融资信息，保存单条记录
	 * @param jsonInfo 企业银行融资信息json串
	 * @return
	 */
	public String saveCompFinaBank(String jsonInfo) ;
	
	/**
	 * @Description 保存企业银行融资信息列表，先删除原来的，再插入新的
	 * @param taskId 任务编号
	 * @param customerNo 客户号
	 * @param jsonArray 企业银行融资信息列表
	 * @return
	 */
	public String saveCompFinaBankList(Long taskId, String customerNo, String jsonArray) ;
	
	/**
	 * @Description 删除企业银行融资信息
	 * @param id 唯一标识
	 * @return
	 */
	public String deleteCompFinaBank(Long id) ;

	/**
	 * @Description (通过客户号查找企业银行融资信息)
	 * @param taskId 任务编号
	 * @param customerNo 客户号
	 * @return
	 */
	public String findCompFinaBankByTIdAndNo(Long taskId, String customerNo) ;
	
}
