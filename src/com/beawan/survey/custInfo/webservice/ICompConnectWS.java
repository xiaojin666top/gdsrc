package com.beawan.survey.custInfo.webservice;


public interface ICompConnectWS {
	
	/**
	 * @Description (保存关联企业信息)
	 * @param data 企业关联企业信息
	 */
	public String saveCompConnectCm(String jsonInfo) ;
	
	/**
	 * @Description (保存关联企业信息列表)
	 * @param data 企业关联企业信息列表
	 */
	public String saveCompConnectCmList(String custNo, String jsonArray) ;
	
	
	/**
	 * @Description (通过客户号查找关联企业信息)
	 * @param custNo
	 * @return
	 */
	public String findCompConnectCmByCustNo(String custNo) ;


	/**
	 * @Description (根据id删除关联企业信息)
	 * @param id
	 * @return
	 */
	public String deleteCompConnectCm(Long id);

}
