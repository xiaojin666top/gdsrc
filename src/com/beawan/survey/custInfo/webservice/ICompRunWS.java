package com.beawan.survey.custInfo.webservice;

import java.util.List;

import com.beawan.survey.custInfo.bean.CompRunArea;
import com.beawan.survey.custInfo.bean.CompRunConst;
import com.beawan.survey.custInfo.bean.CompRunConstSPDetail;
import com.beawan.survey.custInfo.bean.CompRunConstSPStruc;
import com.beawan.survey.custInfo.bean.CompRunEsta;
import com.beawan.survey.custInfo.bean.CompRunEstaDevedProj;
import com.beawan.survey.custInfo.bean.CompRunEstaDevingProj;
import com.beawan.survey.custInfo.bean.CompRunEstaLand;
import com.beawan.survey.custInfo.bean.CompRunFixed;
import com.beawan.survey.custInfo.bean.CompRunSupply;

public interface ICompRunWS {
	
	/**
	 * @Description (通过客户号、任务号查询经营信息)
	 * @param taskId
	 * @param customerNo
	 * @return
	 */
	public String  findCompRunByTIdAndNo(Long taskId, String customerNo);

	/**
	 * @Description (保存企业经营状况)
	 * @param runInfo
	 *            经营状况实体
	 */
	public String saveCompRun(String runInfo) ;

	/**
	 * @Description (根据任务编号查询经营场所信息)
	 * @param taskId
	 *            任务编号
	 * @return
	 */
	public String queryRunAreaByTIdAndNo(Long taskId, String custNo);
			
	/**
	 * @Description (保存经营场所信息列表)
	 * @param taskId 任务编号
	 * @param custNo 客户号
	 * @param jsonArray 信息列表串
	 * @return
	 */
	public String saveRunAreas(Long taskId, String custNo, String jsonArray);

	
	/**
	 * @Description (删除经营场所记录)
	 * @param runArea
	 *            经营场所实体
	 */
	public String deleteRunArea(CompRunArea runArea);
	
	/**
	 * @Description (查询供应商列表)
	 * @param custNo
	 * @return
	 * @throws Exception
	 */
	public String findCompRunSupplyByTIdAndNo(Long taskId,String customerNo);

	/**
	 * @Description (保存供应商列表)
	 * @param data
	 * @throws Exception
	 */
	public String saveCompRunSupply(Long taskId, String custNo, String jsonArray);
	
	/**
	 * @Description (通过客户号、任务号查询生产信息)
	 * @param taskId
	 * @param customerNo
	 * @return
	 */
	public String  findCompProduceByTIdAndNo(Long taskId, String customerNo);

	/**
	 * @Description (保存生产信息)
	 * @param runInfo
	 *            经营生产状况实体
	 */
	public String saveCompProduce(String runInfo) ;
	
	
	/**
	 * @Description (查询下游客户列表)
	 * @param custNo
	 * @return
	 * @throws Exception
	 */
	public String findCompRunCustomerByTIdAndNo(Long taskId,String customerNo);

	/**
	 * @Description (保存下游客户列表)
	 * @param data
	 * @throws Exception
	 */
	public String saveCompRunCustomer(Long taskId, String custNo, String jsonArray);
	
	
	/**
	 * @Description (查询能源耗用量及人工情况	)
	 * @param custNo
	 * @return
	 * @throws Exception
	 */
	public String findCompRunPowerByTIdAndNo(Long taskId,String customerNo);

	/**
	 * @Description (保存能源耗用量及人工情况)
	 * @param data
	 * @throws Exception
	 */
	public String saveCompRunPower(Long taskId, String custNo, String jsonArray);
	
	
	/**
	 * @Description (查询主要产品销售情况)
	 * @param custNo
	 * @return
	 * @throws Exception
	 */
	public String findCompRunPrdSaleByTIdAndNo(Long taskId,String customerNo);

	/**
	 * @Description (保存主要产品销售情况)
	 * @param data
	 * @throws Exception
	 */
	public String saveCompRunPrdSale(Long taskId, String custNo, String jsonArray);
	
	
	/**
	 * @Description (根据任务号查询客户经营状况，适用于建筑业流动资金贷款)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public String queryRunConstByTIdAndNo(Long taskId, String custNo);

	/**
	 * @Description (保存企业经营状况信息，适用于建筑业流动资金贷款)
	 * @param compRunConst
	 *            企业经营状况实体
	 */
	public String saveRunConst(String compRunConst);

	/**
	 * @Description (删除企业经营状况信息，适用于建筑业流动资金贷款)
	 * @param compRunConst
	 *            企业经营状况实体
	 */
	public String deleteRunConst(String compRunConst);

	/**
	 * @Description (根据任务号查询企业在手已签施工工程明细，适用于建筑业流动资金贷款)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public String queryRunConstSPDetailByTIdAndNo(Long taskId, String custNo);
	
	/**
	 * @Description (根据唯一标识查询企业在手已签施工工程明细，适用于建筑业流动资金贷款)
	 * @param id
	 *            唯一标识
	 * @return
	 */
	public String queryRunConstSPDetailById(Long id);

	/**
	 * @Description (保存企业在手已签施工工程明细，适用于建筑业流动资金贷款)
	 * @param compRunConstSPDetail
	 *            企业在手已签施工工程明细实体
	 */
	public String saveRunConstSPDetail(String compRunConstSPDetail);
	
	/**
	 * @Description (保存企业在手已签施工工程明细列表，适用于建筑业流动资金贷款)
	 * @param jsonArray
	 *            企业在手已签施工工程明细列表
	 */
	public String saveRunConstSPDetails(Long taskId, String custNo, String jsonArray);

	/**
	 * @Description (删除企业在手已签施工工程明细，适用于建筑业流动资金贷款)
	 * @param compRunConstSPDetail
	 *            企业在手已签施工工程明细实体
	 */
	public String deleteRunConstSPDetail(String compRunConstSPDetail);

	/**
	 * @Description (根据任务号查询企业在手已签施工工程结构，适用于建筑业流动资金贷款)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public String queryRunConstSPStrucByTIdAndNo(Long taskId, String custNo);
	
	/**
	 * @Description (根据主键查询企业在手已签施工工程结构，适用于建筑业流动资金贷款)
	 * @param id
	 *            主键
	 * @return
	 */
	public String queryRunConstSPStrucById(Long id);

	/**
	 * @Description (保存企业在手已签施工工程结构，适用于建筑业流动资金贷款)
	 * @param compRunConstSPStruc
	 *            企业在手已签施工工程结构实体
	 */
	public String saveRunConstSPStruc(String compRunConstSPStruc);
	
	/**
	 * @Description (保存企业在手已签施工工程结构列表，适用于建筑业流动资金贷款)
	 * @param jsonArray
	 *            企业在手已签施工工程结构列表
	 */
	public String saveRunConstSPStrucs(Long taskId, String custNo, String jsonArray);

	/**
	 * @Description (删除企业在手已签施工工程结构，适用于建筑业流动资金贷款)
	 * @param compRunConstSPStruc
	 *            企业在手已签施工工程结构实体
	 */
	public String deleteRunConstSPStruc(String compRunConstSPStruc);

	/**
	 * @Description (根据任务号查询客户经营状况，适用于固定资产贷款)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public String queryRunFixedByTIdAndNo(Long taskId, String custNo);

	/**
	 * @Description (保存企业经营状况信息，适用于固定资产贷款)
	 * @param compRunFixed
	 *            企业经营状况实体
	 */
	public String saveRunFixed(String compRunFixed);

	/**
	 * @Description (删除企业经营状况信息，适用于固定资产贷款)
	 * @param compRunFixed
	 *            企业经营状况实体
	 */
	public String deleteRunFixed(String compRunFixed);

	/**
	 * @Description (根据任务号查询客户经营状况，适用于房地产贷款)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public String queryRunEstaByTIdAndNo(Long taskId, String custNo);

	/**
	 * @Description (保存企业经营状况信息，适用于房地产贷款)
	 * @param compRunEsta
	 *            企业经营状况实体
	 */
	public String saveRunEsta(String compRunEsta);

	/**
	 * @Description (删除企业经营状况信息，适用于房地产贷款)
	 * @param compRunEsta
	 *            企业经营状况实体
	 */
	public String deleteRunEsta(String compRunEsta);

	/**
	 * @Description (根据任务号查询房地产土地储备情况)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public String queryRunEstaLandByTIdAndNo(Long taskId, String custNo);
	
	/**
	 * @Description (根据主键查询房地产土地储备情况)
	 * @param id
	 *            主键
	 * @return
	 */
	public String queryRunEstaLandById(Long id);

	/**
	 * @Description (保存房地产土地储备情况)
	 * @param compRunEstaLand
	 *            房地产土地储备情况实体
	 */
	public String saveRunEstaLand(String compRunEstaLand);
	
	/**
	 * @Description (保存房地产土地储备情况列表)
	 * @param jsonArray
	 *            房地产土地储备情况列表
	 */
	public String saveRunEstaLands(Long taskId, String custNo, String jsonArray);

	/**
	 * @Description (删除房地产土地储备情况)
	 * @param compRunEstaLand
	 *            房地产土地储备情况实体
	 */
	public String deleteRunEstaLand(String compRunEstaLand);

	/**
	 * @Description (根据任务号查询房地产正在开发项目情况)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public String queryRunEstaDevingProjByTIdAndNo(
			Long taskId, String custNo);
	
	/**
	 * @Description (根据主键查询房地产正在开发项目情况)
	 * @param id
	 *            主键
	 * @return
	 */
	public String queryRunEstaDevingProjById(Long id);

	/**
	 * @Description (保存房地产正在开发项目情况)
	 * @param compRunEstaDevingProj
	 *            房地产正在开发项目情况实体
	 */
	public String saveRunEstaDevingProj(
			String compRunEstaDevingProj);
	
	/**
	 * @Description (保存房地产正在开发项目情况列表)
	 * @param jsonArray
	 *            房地产正在开发项目情况列表
	 */
	public String saveRunEstaDevingProjs(Long taskId, String custNo, String jsonArray) ;

	/**
	 * @Description (删除房地产正在开发项目情况)
	 * @param compRunEstaDevingProj
	 *            房地产正在开发项目情况实体
	 */
	public String deleteRunEstaDevingProj(
			String compRunEstaDevingProj);

	/**
	 * @Description (根据任务号查询房地产已开发项目情况)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public String queryRunEstaDevedProjByTIdAndNo(Long taskId, String custNo);
	
	/**
	 * @Description (根据主键查询房地产已开发项目情况)
	 * @param id
	 *            主键
	 * @return
	 */
	public String queryRunEstaDevedProjById(Long id);

	/**
	 * @Description (保存房地产已开发项目情况)
	 * @param compRunEstaDevedProj
	 *            房地产已开发项目情况实体
	 */
	public String saveRunEstaDevedProj(String compRunEstaDevedProj);
			
	
	/**
	 * @Description (保存房地产已开发项目情况列表)
	 * @param compRunEstaDevedProj
	 *            房地产已开发项目情况列表
	 */
	public String saveRunEstaDevedProjs(Long taskId, String custNo, String jsonArray);

	/**
	 * @Description (删除房地产已开发项目情况)
	 * @param compRunEstaDevedProj
	 *            房地产已开发项目情况实体
	 */
	public String deleteRunEstaDevedProj(
			String compRunEstaDevingProj);
	
	
	
	/**
	 * @Description (通过客户号、任务号查询经营信息(通用类型))
	 * @param taskId
	 * @param customerNo
	 * @return
	 */
	public String  findCompGeneralRunByTIdAndNo(Long taskId, String customerNo);

	/**
	 * @Description (保存企业经营状况(通用类型))
	 * @param runInfo
	 *            经营状况实体
	 */
	public String saveCompGeneralRun(String runInfo) ;
}
