package com.beawan.survey.custInfo.webservice;


public interface ICompBaseWS {
	
	/**
	 * @Description (根据客户号查询企业基本信息)
	 * @param custNo
	 *            客户号
	 * @return CompCustBase
	 * @
	 */
	public String findCompBaseByCustNo(String custNo) ;
	

	/**
	 * @Description (保存企业基本信息)
	 * @param custBase
	 *            企业基本信息实体
	 * @
	 */
	public String saveCompBase(String custBaseInfo) ;
	
	/**
	 * @Description (根据客户号查询企业股权信息)
	 * @param custNo
	 *            客户号
	 * @return DataGrid<CompCustEquity>
	 * @
	 */
	public String findCompEquityByCustNo(String custNo) ;

	/**
	 * @Description (保存企业股权信息)
	 * @param custEquity
	 *            股权信息实体
	 * @
	 */
	public String saveCompEquity(String jsonInfo) ;
	
	/**
	 * @Description (保存企业股权信息列表，先删除后插入)
	 * @param customerNo 客户号
	 * @param jsonArray 企业股权信息列表
	 * @return
	 */
	public String saveCompEquityList(String customerNo , String jsonArray) ;
	
	/**
	 * @Description 删除企业股权信息列表
	 * @param id 唯一标识
	 * @return
	 */
	public String deleteCompEquityById(Long id) ;
	
	/**
	 * @Description (通过客户号查询管理人员信息)
	 * @param customerNo
	 * @return
	 * @
	 */
	public String  findCompManagerByCustNo(String customerNo);
	
	/**
	 * @Description (保存管理人员信息)
	 * @param jsonInfo
	 */
	public String saveCompManager(String userId, String jsonInfo) ;
	
	/**
	 * @Description (保存管理人员信息列表，先删除后插入)
	 * @param custNo 客户号
	 * @param jsonArray 管理人员信息列表
	 * @return
	 */
	public String saveCompManagerList(String custNo, String jsonArray) ;
	
	/**
	 * @Description 删除企业管理人员信息
	 * @param id 唯一标识
	 * @return
	 */
	public String deleteCompManagerById(Long id) ;
	
	/**
	 * @Description (根据客户号查询个人信息)
	 * @param custNo
	 *            客户号
	 * @return PersonBase
	 * @throws Exception
	 */
	public String findPersonBaseByCustNo(String custNo) ;
	
	/**
	 * @Description (保存个人信息)
	 * @param personBase 个人信息实体
	 * @throws Exception
	 */
	public String savePersonBase(String personBase) ;
	
	
	/**
	 * TODO 根据客户号获得客户所在行业标识
	 * @param customerNo
	 * @return
	 */
	public String getCusIndustry(String customerNo);
	
}
