package com.beawan.survey.custInfo.webservice;


public interface ICompFinanceWS {
	
	/**
	 * @Description (根据客户号和任务号查询企业财务状况分析说明)
	 * @param taskId 任务号
	 * @param custNo 客户号
	 * @return
	 */
	public String findCompFinanceByTIdAndNo(Long taskId, String custNo);

	/**
	 * @Description (保存企业财务状况分析说明)
	 * @param data 企业财务状况分析说明信息实体
	 */
	public String saveCompFinance(String financeInfo);

	/**
	 * @Description 比率指标分析-查询财务比率指标
	 * @param taskId
	 * @param cusNo
	 * @return
	 * @throws Exception
	 */
	public String findFinanceIndexs(Long taskId, String cusNo);
		
	/**
	 * @Description 现金流量分析-查询财务报表现金流量数据
	 * @param taskId
	 * @param cusNo
	 * @return
	 * @throws Exception
	 */
	public String findFinanceCashFlow(Long taskId, String cusNo);
	
	
	/**
	 * @Description 企业性现金流回笼信息-查询财务报表现金流量数据
	 * @param taskId
	 * @param cusNo
	 * @return
	 * @throws Exception
	 */
	public String findOperCashFlow(Long taskId, String cusNo);
	
	
	/**
	 * @Description 缴税情况分析-查询财务报表纳税数据
	 * @param taskId
	 * @param cusNo
	 * @return
	 * @throws Exception
	 */
	public String findFinanceTax(Long taskId, String cusNo);
	
	
	/**
	 * @Description 保存纳税数据
	 * @param taskId
	 * @param cusNo
	 * @return
	 * @throws Exception
	 */
	public String saveFinanceTaxs(Long taskId, String customerNo,
			String jsonArray);
	
	/**
	 * @Description 保存现金回笼情况信息
	 * @param cusNo
	 * @return
	 * @throws Exception
	 */
	public String saveOperCashFlow(Long taskId, String customerNo,
			String jsonArray);
	
	/**
	 * @Description 盈利情况分析-查询财务报表纳税数据
	 * @param taskId
	 * @param cusNo
	 * @return
	 * @throws Exception
	 */
	public String findFinanceIncome(Long taskId, String cusNo);

}
