package com.beawan.analysis.finansis.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 应收账款的统计功能，在如皋未使用该类
 */
@Entity
@Table(name = "FN_RECEIVE_STAT",schema = "GDTCESYS")
public class FnReceiveStat extends BaseEntity implements java.io.Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="FN_RECEIVE_STAT_SEQ")
	//@SequenceGenerator(name="FN_RECEIVE_STAT_SEQ",allocationSize=1,initialValue=1, sequenceName="FN_RECEIVE_STAT_SEQ")
	private Long id;
	@Column(name = "TASK_ID")
	private Long taskId;// 任务号
	@Column(name = "REMAIN_AMT")
	private Double remainAmt;// 金额
	@Column(name = "RATE")
	private Double rate;// 占比
	@Column(name = "ACC_AGE")
	private String accAge;// 账龄范围标识

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public Double getRemainAmt() {
		return remainAmt;
	}

	public void setRemainAmt(Double remainAmt) {
		this.remainAmt = remainAmt;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public String getAccAge() {
		return accAge;
	}

	public void setAccAge(String accAge) {
		this.accAge = accAge;
	}

}