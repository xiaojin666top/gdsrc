package com.beawan.analysis.finansis.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * FnFixedAssetsBuilding entity. @author MyEclipse Persistence Tools
 * 固定资产-房屋
 */

@Entity
@Table(name = "FN_FIXED_ASSETS_BUILDING",schema = "GDTCESYS")
public class FnFixedAssetsBuilding extends BaseEntity implements java.io.Serializable {

	// Fields

	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="FN_FIXED_ASSETS_BUILDING_SEQ")
	//@SequenceGenerator(name="FN_FIXED_ASSETS_BUILDING_SEQ",allocationSize=1,initialValue=1, sequenceName="FN_FIXED_ASSETS_BUILDING_SEQ")
	private Long id;
	@Column(name = "TASK_ID")
	private Long taskId;//
	@Column(name = "NAME")
	private String name;//房产位置名称
	@Column(name = "TYPE")
	private String type;//房产类型
	@Column(name = "WARRANT")
	private String warrant;//权属证明
	@Column(name = "OWNERSHIP_NATURE")
	private String ownershipNature;//权属性质
	@Column(name = "AREA")
	private Double area;//面积
	@Column(name = "USE")
	private String use;//用途
	@Column(name = "ORIGINAL_VALUE")
	private Double originalValue;//资产原值
	@Column(name = "NET_VALUE")
	private Double netValue;//资产净值
	@Column(name = "IS_MORTGAGE")
	private String isMortgage;//是否被抵押
	@Column(name = "MORTGAGEE")
	private String mortgagee;//抵押权人
	@Column(name = "IS_IDLE")
	private String isIdle;//是否被闲置
	@Column(name = "REMARKS")
	private String remarks;//备注
	@Column(name = "RATE")
	private Double rate;
	// Constructors

	/** default constructor */
	public FnFixedAssetsBuilding() {
	}

	/** minimal constructor */
	public FnFixedAssetsBuilding(Long id) {
		this.id = id;
	}

	/** full constructor */
	public FnFixedAssetsBuilding(Long id, Long taskId, String name,
			String type, String warrant, String ownershipNature, Double area,
			String use, Double originalValue, Double netValue,
			String isMortgage, String mortgagee, String isIdle, String remarks, Double rate) {
		this.id = id;
		this.taskId = taskId;
		this.name = name;
		this.type = type;
		this.warrant = warrant;
		this.ownershipNature = ownershipNature;
		this.area = area;
		this.use = use;
		this.originalValue = originalValue;
		this.netValue = netValue;
		this.isMortgage = isMortgage;
		this.mortgagee = mortgagee;
		this.isIdle = isIdle;
		this.remarks = remarks;
		this.rate = rate;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getWarrant() {
		return this.warrant;
	}

	public void setWarrant(String warrant) {
		this.warrant = warrant;
	}

	public String getOwnershipNature() {
		return this.ownershipNature;
	}

	public void setOwnershipNature(String ownershipNature) {
		this.ownershipNature = ownershipNature;
	}

	public Double getArea() {
		return this.area;
	}

	public void setArea(Double area) {
		this.area = area;
	}

	public String getUse() {
		return this.use;
	}

	public void setUse(String use) {
		this.use = use;
	}

	public Double getOriginalValue() {
		return this.originalValue;
	}

	public void setOriginalValue(Double originalValue) {
		this.originalValue = originalValue;
	}

	public Double getNetValue() {
		return this.netValue;
	}

	public void setNetValue(Double netValue) {
		this.netValue = netValue;
	}

	public String getIsMortgage() {
		return this.isMortgage;
	}

	public void setIsMortgage(String isMortgage) {
		this.isMortgage = isMortgage;
	}

	public String getMortgagee() {
		return this.mortgagee;
	}

	public void setMortgagee(String mortgagee) {
		this.mortgagee = mortgagee;
	}

	public String getIsIdle() {
		return this.isIdle;
	}

	public void setIsIdle(String isIdle) {
		this.isIdle = isIdle;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	public Double getRate() {
		return this.rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

}