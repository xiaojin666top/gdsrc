package com.beawan.analysis.finansis.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * FnFixedAssetsEquipment entity. @author MyEclipse Persistence Tools
 * 固定资产-设备
 */
@Entity
@Table(name = "FN_FIXED_ASSETS_EQUIPMENT",schema = "GDTCESYS")
public class FnFixedAssetsEquipment extends BaseEntity implements java.io.Serializable {

	// Fields
	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="FN_FIXED_ASSETS_EQUIPMENT_SEQ")
//	@SequenceGenerator(name="FN_FIXED_ASSETS_EQUIPMENT_SEQ",allocationSize=1,initialValue=1, sequenceName="FN_FIXED_ASSETS_EQUIPMENT_SEQ")
	private Long id;
	@Column(name = "TASK_ID")
	private Long taskId;//任务号
	@Column(name = "NAME")
	private String name;//设备名称
	@Column(name = "NUMBER_N")
	private Integer number;//数量
	@Column(name = "ORIGINAL_VALUE")
	private Double originalValue;//资产原值
	@Column(name = "NET_VALUE")
	private Double netValue;//资产净值
	@Column(name = "ORIGIN_PLACE")
	private String originPlace;//产地
	@Column(name = "PURCHASE_DATE")
	private String purchaseDate;//购入时间
	@Column(name = "IS_MORTGAGE")
	private String isMortgage;//是否被抵押
	@Column(name = "MORTGAGEE")
	private String mortgagee;//抵押权人
	@Column(name = "IS_IDLE")
	private String isIdle;//是否被闲置
	@Column(name = "REMARKS")
	private String remarks;//备注
	@Column(name = "RATE")
	private Double rate;//比率

	// Constructors

	/** default constructor */
	public FnFixedAssetsEquipment() {
	}

	/** minimal constructor */
	public FnFixedAssetsEquipment(Long id) {
		this.id = id;
	}

	/** full constructor */
	public FnFixedAssetsEquipment(Long id, Long taskId, String name,
			Integer number, Double originalValue, Double netValue,
			String originPlace, String purchaseDate, String isMortgage,
			String mortgagee, String isIdle, String remarks,Double rate) {
		this.id = id;
		this.taskId = taskId;
		this.name = name;
		this.number = number;
		this.originalValue = originalValue;
		this.netValue = netValue;
		this.originPlace = originPlace;
		this.purchaseDate = purchaseDate;
		this.isMortgage = isMortgage;
		this.mortgagee = mortgagee;
		this.isIdle = isIdle;
		this.remarks = remarks;
		this.rate = rate;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNumber() {
		return this.number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Double getOriginalValue() {
		return this.originalValue;
	}

	public void setOriginalValue(Double originalValue) {
		this.originalValue = originalValue;
	}

	public Double getNetValue() {
		return this.netValue;
	}

	public void setNetValue(Double netValue) {
		this.netValue = netValue;
	}

	public String getOriginPlace() {
		return this.originPlace;
	}

	public void setOriginPlace(String originPlace) {
		this.originPlace = originPlace;
	}

	public String getPurchaseDate() {
		return this.purchaseDate;
	}

	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public String getIsMortgage() {
		return this.isMortgage;
	}

	public void setIsMortgage(String isMortgage) {
		this.isMortgage = isMortgage;
	}

	public String getMortgagee() {
		return this.mortgagee;
	}

	public void setMortgagee(String mortgagee) {
		this.mortgagee = mortgagee;
	}

	public String getIsIdle() {
		return this.isIdle;
	}

	public void setIsIdle(String isIdle) {
		this.isIdle = isIdle;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	public Double getRate() {
		return this.rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

}