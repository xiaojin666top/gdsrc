package com.beawan.analysis.finansis.bean;

/**
 * FnConstruc entity. @author MyEclipse Persistence Tools
 */

public class FnConstruc implements java.io.Serializable {

	// Fields

	private Long id;
	private Long taskId;//任务号
	private String projName;//项目名称
	private Double projInve;//工程总投资
	private Double amountInve;//已投资金
	private Double nonInve;//尚投资金
	private String projLoan;//配套项目贷款情况
	private String inveTime;//预计投资时间
	private String completSche;//完工进度估算
	private String isDeliver;//是否已交付使用

	// Constructors

	/** default constructor */
	public FnConstruc() {
	}

	/** minimal constructor */
	public FnConstruc(Long id) {
		this.id = id;
	}

	/** full constructor */
	public FnConstruc(Long id, Long taskId, String projName, Double projInve,
			Double amountInve, Double nonInve, String projLoan,
			String inveTime, String completSche, String isDeliver) {
		this.id = id;
		this.taskId = taskId;
		this.projName = projName;
		this.projInve = projInve;
		this.amountInve = amountInve;
		this.nonInve = nonInve;
		this.projLoan = projLoan;
		this.inveTime = inveTime;
		this.completSche = completSche;
		this.isDeliver = isDeliver;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getProjName() {
		return this.projName;
	}

	public void setProjName(String projName) {
		this.projName = projName;
	}

	public Double getProjInve() {
		return this.projInve;
	}

	public void setProjInve(Double projInve) {
		this.projInve = projInve;
	}

	public Double getAmountInve() {
		return this.amountInve;
	}

	public void setAmountInve(Double amountInve) {
		this.amountInve = amountInve;
	}

	public Double getNonInve() {
		return this.nonInve;
	}

	public void setNonInve(Double nonInve) {
		this.nonInve = nonInve;
	}

	public String getProjLoan() {
		return this.projLoan;
	}

	public void setProjLoan(String projLoan) {
		this.projLoan = projLoan;
	}

	public String getInveTime() {
		return this.inveTime;
	}

	public void setInveTime(String inveTime) {
		this.inveTime = inveTime;
	}

	public String getCompletSche() {
		return this.completSche;
	}

	public void setCompletSche(String completSche) {
		this.completSche = completSche;
	}

	public String getIsDeliver() {
		return this.isDeliver;
	}

	public void setIsDeliver(String isDeliver) {
		this.isDeliver = isDeliver;
	}

}