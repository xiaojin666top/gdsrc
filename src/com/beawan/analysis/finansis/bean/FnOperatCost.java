package com.beawan.analysis.finansis.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * FnOperatCost entity. 营业费用明细
 */

@Entity
@Table(name = "FN_OPERAT_COST",schema = "GDTCESYS")
public class FnOperatCost extends BaseEntity implements java.io.Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="FN_OPERAT_COST_SEQ")
	//@SequenceGenerator(name="FN_OPERAT_COST_SEQ",allocationSize=1,initialValue=1, sequenceName="FN_OPERAT_COST_SEQ")
	private Long id;
	@Column(name = "TASK_ID")
	private Long taskId;//任务号
	@Column(name = "ITEM_NAME")
	private String itemName;//项目名称
	@Column(name = "COST_VALUE")
	private Double costValue;//费用金额

	// Constructors

	/** default constructor */
	public FnOperatCost() {
	}

	/** minimal constructor */
	public FnOperatCost(Long id) {
		this.id = id;
	}

	/** full constructor */
	public FnOperatCost(Long id, Long taskId, String itemName, Double costValue) {
		this.id = id;
		this.taskId = taskId;
		this.itemName = itemName;
		this.costValue = costValue;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getItemName() {
		return this.itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Double getCostValue() {
		return this.costValue;
	}

	public void setCostValue(Double costValue) {
		this.costValue = costValue;
	}

}