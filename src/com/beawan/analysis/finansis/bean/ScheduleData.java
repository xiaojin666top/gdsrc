package com.beawan.analysis.finansis.bean;

import java.io.Serializable;


/**
 * @ClassName ScheduleData
 * @Description (明细表单行数据)
 * @author czc
 * @Date 2017年5月16日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public class ScheduleData implements Serializable {

	private static final long serialVersionUID = 8998419987782552311L;
	
	
	private Long id;
	
	/** @Field @name : (名称) */
	private String name;
	
	/** @Field @itemType : (明细表类别，区分不同的明细表) */
	private String reportType;
	
	/** @Field @detailData : (明细表表格数据) */
	private String detailData;
	
	/** @Field @firstTdRow : (该行第一个单元格所占行数) */
	private String firstTdRow;
	
	/** @Field taskId : (任务号) */
	private Long taskId;

	/** @Field @index : (位置信息) */
	private int index;
	
	/** @Field @errDataTrId : (明细表的核查信息) */
	private Long errDataTrId;
	
	/** @Field @remark : (备注) */
	private String remark;

	public ScheduleData(String reportType, String detailData, Long taskId) {
		super();
		this.reportType = reportType;
		this.detailData = detailData;
		this.taskId = taskId;
	}


	@Override
	public boolean equals(Object obj) {
		ScheduleData report = null;
		if(null == obj) {
			return false;
		}
		if(obj instanceof ScheduleData) {
			report = (ScheduleData) obj;
		}else {
			return false;
		}
		return this.name.equals(report.getName());
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}




	public String getReportType() {
		return reportType;
	}


	public void setReportType(String reportType) {
		this.reportType = reportType;
	}


	public String getDetailData() {
		return detailData;
	}


	public void setDetailData(String detailData) {
		this.detailData = detailData;
	}


	public String getFirstTdRow() {
		return firstTdRow;
	}


	public void setFirstTdRow(String firstTdRow) {
		this.firstTdRow = firstTdRow;
	}

	public Long getTaskId() {
		return taskId;
	}


	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}


	public int getIndex() {
		return index;
	}


	public void setIndex(int index) {
		this.index = index;
	}

	public Long getErrDataTrId() {
		return errDataTrId;
	}


	public void setErrDataTrId(Long errDataTrId) {
		this.errDataTrId = errDataTrId;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
}
