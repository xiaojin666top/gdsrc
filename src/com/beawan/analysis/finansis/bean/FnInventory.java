package com.beawan.analysis.finansis.bean;


import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 存货明细
 */
@Entity
@Table(name = "FN_INVENTORY",schema = "GDTCESYS")
public class FnInventory  extends BaseEntity implements java.io.Serializable{

	// Fields
	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="FN_INVENTORY_SEQ")
	//@SequenceGenerator(name="FN_INVENTORY_SEQ",allocationSize=1,initialValue=1, sequenceName="FN_INVENTORY_SEQ")
	private Long id;
	@Column(name = "TASK_ID")
	private Long taskId;
	@Column(name = "NAME")
	private String name;//存货名称
	@Column(name = "TYPE")
	private String type;//存货类型
	@Column(name = "NUMBER_N")
	private String number;//存货数量
	@Column(name = "BOOK_VALUE")
	private Double bookValue;//账面价值
	@Column(name = "EVALUAT_VALUE")
	private Double evaluatValue;//评估价值
	@Column(name = "PURCHASE_DATE")
	private String purchaseDate;//购入时间
	@Column(name = "RATE")
	private Double rate;//占比
	@Column(name = "MEMO")
	private String memo;//存货名称

	// Constructors

	/** default constructor */
	public FnInventory() {
	}

	/** minimal constructor */
	public FnInventory(Long id) {
		this.id = id;
	}

	/** full constructor */
	public FnInventory(Long id, Long taskId, String name, String type,
			String number, Double bookValue, Double evaluatValue,
			String purchaseDate) {
		this.id = id;
		this.taskId = taskId;
		this.name = name;
		this.type = type;
		this.number = number;
		this.bookValue = bookValue;
		this.evaluatValue = evaluatValue;
		this.purchaseDate = purchaseDate;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNumber() {
		return this.number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Double getBookValue() {
		return this.bookValue;
	}

	public void setBookValue(Double bookValue) {
		this.bookValue = bookValue;
	}

	public Double getEvaluatValue() {
		return this.evaluatValue;
	}

	public void setEvaluatValue(Double evaluatValue) {
		this.evaluatValue = evaluatValue;
	}

	public String getPurchaseDate() {
		return this.purchaseDate;
	}

	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}
}