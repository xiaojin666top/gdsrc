package com.beawan.analysis.finansis.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * FnCash entity. 货币资金
 */
@Entity
@Table(name = "FN_CASH",schema = "GDTCESYS")
public class FnCash extends BaseEntity implements java.io.Serializable {

	// Fields
	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="FN_CASH_SEQ")
	//@SequenceGenerator(name="FN_CASH_SEQ",allocationSize=1,initialValue=1, sequenceName="FN_CASH_SEQ")
	private Long id;
	@Column(name = "TASK_ID")
	private Long taskId;//任务号
	@Column(name = "CASH_TYPE")
	private String cashType;//资金类型，现金/存款/保证金
	@Column(name = "BANK_NAME")
	private String bankName;//银行名称
	@Column(name = "AMOUNT")
	private Double amount;//金额
	@Column(name = "RATE")
	private Double rate;//比率
	

	// Constructors

	/** default constructor */
	public FnCash() {
	}

	/** minimal constructor */
	public FnCash(Long id) {
		this.id = id;
	}

	/** full constructor */
	public FnCash(Long id, Long taskId, String cashType, String bankName,
			Double amount, Double rate) {
		this.id = id;
		this.taskId = taskId;
		this.cashType = cashType;
		this.bankName = bankName;
		this.amount = amount;
		this.rate = rate;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getCashType() {
		return this.cashType;
	}

	public void setCashType(String cashType) {
		this.cashType = cashType;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	public Double getRate() {
		return this.rate;
	}

	public void setRate(Double rate) {
		this.rate= rate;
	}

}