package com.beawan.analysis.finansis.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;


/**
 * 重要科目核实项
 */

@Entity
@Table(schema = "GDTCESYS", name = "FN_VERIFY")
public class FnVerify extends BaseEntity {

    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FN_VERIFY")
  //  @SequenceGenerator(name = "FN_VERIFY",allocationSize=1,initialValue=1, sequenceName = "FN_VERIFY_SEQ")
    private Integer id;
    @Column(name = "TASK_ID")
    private Long taskId;    //贷前任务号
    @Column(name = "SUBJECT")
    private String subject;//科目名称
    @Column(name = "VERIFY_VAL")
    private Double verifyValue;//核实值

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Double getVerifyValue() {
        return verifyValue;
    }

    public void setVerifyValue(Double verifyValue) {
        this.verifyValue = verifyValue;
    }
}
