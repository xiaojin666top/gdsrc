package com.beawan.analysis.finansis.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * FnOtherPay entity. 其他应付账款
 */

@Entity
@Table(name = "FN_OTHER_PAY",schema = "GDTCESYS")
public class FnOtherPay extends BaseEntity implements java.io.Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="FN_OTHER_PAY_SEQ")
	//@SequenceGenerator(name="FN_OTHER_PAY_SEQ",allocationSize=1,initialValue=1, sequenceName="FN_OTHER_PAY_SEQ")
	private Long id;
	@Column(name = "TASK_ID")
	private Long taskId;//任务号
	@Column(name = "SUPPLIER_NAME")
	private String supplierName;//供应商名称
	@Column(name = "REMAIN_AMT")
	private Double remainAmt;//金额
	@Column(name = "RATE")
	private Double rate;//占比
	@Column(name = "ACCOUNT_AGE")
	private Integer accountAge;//账龄（月）
	@Column(name = "REMARKS")
	private String remarks;//欠款原因
	@Column(name = "ACC_TYPE")
	private String accType;//账款类型，关联往来/对外往来

	// Constructors

	/** default constructor */
	public FnOtherPay() {
	}

	/** minimal constructor */
	public FnOtherPay(Long id) {
		this.id = id;
	}

	/** full constructor */
	public FnOtherPay(Long id, Long taskId, String supplierName,
			Double remainAmt, Double rate, Integer accountAge, String remarks,
			String accType) {
		this.id = id;
		this.taskId = taskId;
		this.supplierName = supplierName;
		this.remainAmt = remainAmt;
		this.rate = rate;
		this.accountAge = accountAge;
		this.remarks = remarks;
		this.accType = accType;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getSupplierName() {
		return this.supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public Double getRemainAmt() {
		return this.remainAmt;
	}

	public void setRemainAmt(Double remainAmt) {
		this.remainAmt = remainAmt;
	}

	public Double getRate() {
		return this.rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Integer getAccountAge() {
		return this.accountAge;
	}

	public void setAccountAge(Integer accountAge) {
		this.accountAge = accountAge;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getAccType() {
		return this.accType;
	}

	public void setAccType(String accType) {
		this.accType = accType;
	}

}