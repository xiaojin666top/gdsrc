package com.beawan.analysis.finansis.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * FnFinancCost entity. 财务费用明细
 */

@Entity
@Table(name = "FN_FINANC_COST",schema = "GDTCESYS")
public class FnFinancCost extends BaseEntity implements java.io.Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="FN_FINANC_COST_SEQ")
	//@SequenceGenerator(name="FN_FINANC_COST_SEQ",allocationSize=1,initialValue=1, sequenceName="FN_FINANC_COST_SEQ")
	private Long id;
	@Column(name = "TASK_ID")
	private Long taskId;//任务号
	@Column(name = "ITEM_NAME")
	private String itemName;//项目名称
	@Column(name = "COST_VALUE")
	private Double costValue;//费用

	// Constructors

	/** default constructor */
	public FnFinancCost() {
	}

	/** minimal constructor */
	public FnFinancCost(Long id) {
		this.id = id;
	}

	/** full constructor */
	public FnFinancCost(Long id, Long taskId, String itemName, Double costValue) {
		this.id = id;
		this.taskId = taskId;
		this.itemName = itemName;
		this.costValue = costValue;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getItemName() {
		return this.itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Double getCostValue() {
		return this.costValue;
	}

	public void setCostValue(Double costValue) {
		this.costValue = costValue;
	}

}