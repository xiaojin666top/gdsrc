package com.beawan.analysis.finansis.bean;

import java.io.Serializable;

/**
 * @ClassName ReportDataTR
 * @Description 三大报表行数据，已废弃
 * @author czc
 * @Date 2017年5月16日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public class ReportDataTR implements Serializable {

	private static final long serialVersionUID = 8720387048815207617L;
	
	private Long id;

	/** @Field @name : (表格每行科目名字) */
	private String name;
	
	
	/** @Field @numValue : (第一年数据) */
	private Double firstYearValue;
	
	/** @Field @numValue : (第二年数据) */
	private Double secondYearValue;
	
	/** @Field @numValue : (第三年数据) */
	private Double thirdYearValue;
	
	/** @Field @numValue : (第四年数据 当期数据) */
	private Double fourthYearValue;
	
	/** @Field @numValue : (第四年数据 当期数据) */
	private Double lastYearTermValue;
	
	/** @Field @index : (位置信息) */
	private Integer index;
	
	/** @Field @reportDataId : (相关联的表的id) */
	private Long reportDataId;
	
	/** @Field @boolAdjust : (判断是否修改了报表值，若修改了，将值设置为 “true”) */
	private String boolAdjust;   
	
	/** @Field @rankOrder : (位置，处于表格中第几行) */
	private String rankOrder;
	
	/** @Field @remark : () */
	private String remark;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Double getFirstYearValue() {
		return firstYearValue;
	}

	public void setFirstYearValue(Double firstYearValue) {
		this.firstYearValue = firstYearValue;
	}

	public Double getSecondYearValue() {
		return secondYearValue;
	}

	public Double getLastYearTermValue() {
		return lastYearTermValue;
	}

	public void setLastYearTermValue(Double lastYearTermValue) {
		this.lastYearTermValue = lastYearTermValue;
	}

	public void setSecondYearValue(Double secondYearValue) {
		this.secondYearValue = secondYearValue;
	}

	public Double getThirdYearValue() {
		return thirdYearValue;
	}

	public void setThirdYearValue(Double thirdYearValue) {
		this.thirdYearValue = thirdYearValue;
	}

	public Double getFourthYearValue() {
		return fourthYearValue;
	}

	public void setFourthYearValue(Double fourthYearValue) {
		this.fourthYearValue = fourthYearValue;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public Long getReportDataId() {
		return reportDataId;
	}

	public void setReportDataId(Long reportDataId) {
		this.reportDataId = reportDataId;
	}

	public String getBoolAdjust() {
		return boolAdjust;
	}

	public void setBoolAdjust(String boolAdjust) {
		this.boolAdjust = boolAdjust;
	}

	public String getRankOrder() {
		return rankOrder;
	}

	public void setRankOrder(String rankOrder) {
		this.rankOrder = rankOrder;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}


}
