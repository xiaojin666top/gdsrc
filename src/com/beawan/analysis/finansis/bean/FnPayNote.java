package com.beawan.analysis.finansis.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * FnPayNote entity. 应付票据
 */
@Entity
@Table(name = "FN_PAY_NOTE",schema = "GDTCESYS")
public class FnPayNote extends BaseEntity implements java.io.Serializable {

	// Fields
	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="FN_PAY_NOTE_SEQ")
	//@SequenceGenerator(name="FN_PAY_NOTE_SEQ",allocationSize=1,initialValue=1, sequenceName="FN_PAY_NOTE_SEQ")
	private Long id;
	@Column(name = "TASK_ID")
	private Long taskId;//任务号
	@Column(name = "CREDITOR_NAME")
	private String creditorName;//名称
	@Column(name = "AMOUNT")
	private Double amount;//金额
	@Column(name = "TERM")
	private String term;//期限
	@Column(name = "DUE_DATE")
	private String dueDate;//到期日
	@Column(name = "REMARKS")
	private String remarks;//备注

	// Constructors

	/** default constructor */
	public FnPayNote() {
	}

	/** minimal constructor */
	public FnPayNote(Long id) {
		this.id = id;
	}

	/** full constructor */
	public FnPayNote(Long id, Long taskId, String creditorName, Double amount,
			String term, String dueDate, String remarks) {
		this.id = id;
		this.taskId = taskId;
		this.creditorName = creditorName;
		this.amount = amount;
		this.term = term;
		this.dueDate = dueDate;
		this.remarks = remarks;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getCreditorName() {
		return this.creditorName;
	}

	public void setCreditorName(String creditorName) {
		this.creditorName = creditorName;
	}

	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getTerm() {
		return this.term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getDueDate() {
		return this.dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}