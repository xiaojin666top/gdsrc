package com.beawan.analysis.finansis.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * FnOtherReceive entity. 其他应收账款
 */

@Entity
@Table(name = "FN_OTHER_RECEIVE",schema = "GDTCESYS")
public class FnOtherReceive extends BaseEntity implements java.io.Serializable {

	// Fields
	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="FN_OTHER_RECEIVE_SEQ")
	//@SequenceGenerator(name="FN_OTHER_RECEIVE_SEQ",allocationSize=1,initialValue=1, sequenceName="FN_OTHER_RECEIVE_SEQ")
	private Long id;
	@Column(name = "TASK_ID")
	private Long taskId;
	@Column(name = "CUS_NAME")
	private String cusName;//客户名称
	@Column(name = "REMAIN_AMT")
	private Double remainAmt;//金额
	@Column(name = "RATE")
	private Double rate;//占比
	@Column(name = "ACCOUNT_AGE")
	private Integer accountAge;//账龄（月）
	@Column(name = "REMARKS")
	private String remarks;//欠款原因
	@Column(name = "ACC_TYPE")
	private String accType;//账款类型，关联往来/对外往来

	// Constructors

	/** default constructor */
	public FnOtherReceive() {
	}

	/** minimal constructor */
	public FnOtherReceive(Long id) {
		this.id = id;
	}

	/** full constructor */
	public FnOtherReceive(Long id, Long taskId, String cusName,
			Double remainAmt, Double rate, Integer accountAge, String remarks,
			String accType) {
		this.id = id;
		this.taskId = taskId;
		this.cusName = cusName;
		this.remainAmt = remainAmt;
		this.rate = rate;
		this.accountAge = accountAge;
		this.remarks = remarks;
		this.accType = accType;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getCusName() {
		return this.cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public Double getRemainAmt() {
		return this.remainAmt;
	}

	public void setRemainAmt(Double remainAmt) {
		this.remainAmt = remainAmt;
	}

	public Double getRate() {
		return this.rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Integer getAccountAge() {
		return this.accountAge;
	}

	public void setAccountAge(Integer accountAge) {
		this.accountAge = accountAge;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getAccType() {
		return this.accType;
	}

	public void setAccType(String accType) {
		this.accType = accType;
	}

}