package com.beawan.analysis.finansis.bean;

import java.io.Serializable;

/**
 * @ClassName ReportData
 * @Description 存储资产负债表、利润表、现金流量表，已废弃
 * @author czc
 * @Date 2017年5月16日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public class ReportData implements Serializable{

	private static final long serialVersionUID = -1225346724816843494L;
	
	private Long id;
	
	/** @Field @name : (行业名称) */
	private String name;
	
	/** @Field @remark : (备注) */
	private String remark;
	
	/** @Field taskId : (任务号) */
	private Long taskId;
	
	/** @Field @customerNo : (客户号) */
	private String customerNo;
	
	/** @Field @reportType : (表类型，资产负债表是  balance 利润表  income 现金流量表 cashFlow 修改后 的资产负债表 newBlance  newIncome newCashFlow) */
	private String reportType;
	
	/** @Field @reportHtmlUrl : (模板表url) */
	private String reportHtmlUrl;

	public ReportData() {
		super();
	}
	
	public ReportData(Long taskId , String customerNo , String reportType) {
		this.taskId = taskId;
		this.customerNo = customerNo;
		this.reportType = reportType;
	}

	public ReportData(String name) {
		this.name = name;
	}
	
	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	

	public Long getTaskId() {
		return taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public String getReportHtmlUrl() {
		return reportHtmlUrl;
	}

	public void setReportHtmlUrl(String reportHtmlUrl) {
		this.reportHtmlUrl = reportHtmlUrl;
	}

	@Override
	public boolean equals(Object obj) {
		ReportData report = null;
		if(null == obj) {
			return false;
		}
		if(obj instanceof ReportData) {         
			report = (ReportData) obj;
		}else {
			return false;
		}
		return this.name.equals(report.getName());
	}
}
