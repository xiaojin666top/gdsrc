package com.beawan.analysis.finansis.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * FnReceNote entity. 应收票据
 */

@Entity
@Table(name = "FN_RECE_NOTE",schema = "GDTCESYS")
public class FnReceNote extends BaseEntity implements java.io.Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="FN_RECE_NOTE_SEQ")
	//@SequenceGenerator(name="FN_RECE_NOTE_SEQ",allocationSize=1,initialValue=1, sequenceName="FN_RECE_NOTE_SEQ")
	private Long id;
	@Column(name = "TASK_ID")
	private Long taskId;
	@Column(name = "OBLIGOR_NAME")
	private String obligorName;//债务人名称
	@Column(name = "AMOUNT")
	private Double amount;//金额
	@Column(name = "TERM")
	private String term;//期限
	@Column(name = "DUE_DATE")
	private String dueDate;//到期日
	@Column(name = "REMARKS")
	private String remarks;//备注

	// Constructors

	/** default constructor */
	public FnReceNote() {
	}

	/** minimal constructor */
	public FnReceNote(Long id) {
		this.id = id;
	}

	/** full constructor */
	public FnReceNote(Long id, Long taskId, String obligorName, Double amount,
			String term, String dueDate, String remarks) {
		this.id = id;
		this.taskId = taskId;
		this.obligorName = obligorName;
		this.amount = amount;
		this.term = term;
		this.dueDate = dueDate;
		this.remarks = remarks;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getObligorName() {
		return this.obligorName;
	}

	public void setObligorName(String obligorName) {
		this.obligorName = obligorName;
	}

	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getTerm() {
		return this.term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getDueDate() {
		return this.dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}