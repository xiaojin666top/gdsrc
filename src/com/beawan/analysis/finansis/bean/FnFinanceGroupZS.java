package com.beawan.analysis.finansis.bean;

import java.util.List;

import com.beawan.survey.custInfo.bean.CompRunCustomer;
import com.beawan.survey.custInfo.bean.CompRunSupply;

public class FnFinanceGroupZS {
	
	private String stockValue; //存货
	
	private String stockOri;  //存货原材料
	
	private String stockIndo;  //存货在产品
	
	private String stockPro;  //存货产成品
	
	private String otherRece;  //其他应收款
	
	private List<FnOtherReceive>  otherReceives ;  //其他应收款
	
	private String otherPay;  //其他应付款
	
	private List<FnOtherPay>  otherPays ;  //其他应付款
	
	private String receive;
	
	private List<FnReceive> receives;
	
	private List<CompRunSupply> supplies;
	
	private List<CompRunCustomer> customers ;
	
	private String prePay;
	
	private List<FnPrepay>  prepays;
	
	
	private String first;
	
	private List<String> tableOne;
	
	private List<String> tableTwo;
	
	private String third;
	
	private String four;
	
	private String five;
	
	private List<String> six;
	
	private String seven;
	
	private List<List<String>> Incfirst;
	
	private String incsecond;
	
	private String incsecondThree;
	
	private String incThird;
	
	private String incFour;
	
	private String incFive;
	
	private String incSix;
	
	private   List<List<String>> assetOne;
	
	private   List<List<String>> assetTwo;
	
	private   String assetThree;
	
	private   String ratioOne;
	
	private   String ratioTwo;
	
	private   String ratioThree;
	
	private   String ratioFour;
	
	private   String conclusionOne;
	private   List<String> conclusionTwo;
	private   String conclusionThree;
	private   String conclusionFour;
	
	private   String conclusionInFourFive;
	private   String conclusionFive;
	
	

	public String getStockValue() {
		return stockValue;
	}

	public String getStockOri() {
		return stockOri;
	}

	public String getStockIndo() {
		return stockIndo;
	}

	public String getStockPro() {
		return stockPro;
	}

	public String getOtherRece() {
		return otherRece;
	}

	public List<FnOtherReceive> getOtherReceives() {
		return otherReceives;
	}


	public String getOtherPay() {
		return otherPay;
	}

	public List<FnOtherPay> getOtherPays() {
		return otherPays;
	}

	public String getReceive() {
		return receive;
	}

	public List<FnReceive> getReceives() {
		return receives;
	}

	public List<CompRunSupply> getSupplies() {
		return supplies;
	}

	public List<CompRunCustomer> getCustomers() {
		return customers;
	}

	public String getPrePay() {
		return prePay;
	}

	public List<FnPrepay> getPrepays() {
		return prepays;
	}

	public void setStockValue(String stockValue) {
		this.stockValue = stockValue;
	}

	public void setStockOri(String stockOri) {
		this.stockOri = stockOri;
	}

	public void setStockIndo(String stockIndo) {
		this.stockIndo = stockIndo;
	}

	public void setStockPro(String stockPro) {
		this.stockPro = stockPro;
	}

	public void setOtherRece(String otherRece) {
		this.otherRece = otherRece;
	}

	public void setOtherReceives(List<FnOtherReceive> otherReceives) {
		this.otherReceives = otherReceives;
	}

	public void setOtherPay(String otherPay) {
		this.otherPay = otherPay;
	}

	public void setOtherPays(List<FnOtherPay> otherPays) {
		this.otherPays = otherPays;
	}

	public void setReceive(String receive) {
		this.receive = receive;
	}

	public void setReceives(List<FnReceive> receives) {
		this.receives = receives;
	}

	public void setSupplies(List<CompRunSupply> supplies) {
		this.supplies = supplies;
	}

	public void setCustomers(List<CompRunCustomer> customers) {
		this.customers = customers;
	}

	public void setPrePay(String prePay) {
		this.prePay = prePay;
	}

	public void setPrepays(List<FnPrepay> prepays) {
		this.prepays = prepays;
	}

	public String getFirst() {
		return first;
	}

	public List<String> getTableOne() {
		return tableOne;
	}

	public List<String> getTableTwo() {
		return tableTwo;
	}

	public String getThird() {
		return third;
	}

	public String getFour() {
		return four;
	}

	public String getFive() {
		return five;
	}

	public List<String> getSix() {
		return six;
	}

	public String getSeven() {
		return seven;
	}

	public List<List<String>> getIncfirst() {
		return Incfirst;
	}

	public String getIncsecond() {
		return incsecond;
	}

	public String getIncsecondThree() {
		return incsecondThree;
	}

	public String getIncThird() {
		return incThird;
	}

	public String getIncFour() {
		return incFour;
	}

	public String getIncFive() {
		return incFive;
	}

	public String getIncSix() {
		return incSix;
	}

	public List<List<String>> getAssetOne() {
		return assetOne;
	}

	public List<List<String>> getAssetTwo() {
		return assetTwo;
	}

	public String getAssetThree() {
		return assetThree;
	}

	public String getRatioOne() {
		return ratioOne;
	}

	public String getRatioTwo() {
		return ratioTwo;
	}

	public String getRatioThree() {
		return ratioThree;
	}

	public String getRatioFour() {
		return ratioFour;
	}

	public String getConclusionOne() {
		return conclusionOne;
	}

	public List<String> getConclusionTwo() {
		return conclusionTwo;
	}

	public String getConclusionThree() {
		return conclusionThree;
	}

	public String getConclusionFour() {
		return conclusionFour;
	}

	public String getConclusionInFourFive() {
		return conclusionInFourFive;
	}

	public String getConclusionFive() {
		return conclusionFive;
	}

	public void setFirst(String first) {
		this.first = first;
	}

	public void setTableOne(List<String> tableOne) {
		this.tableOne = tableOne;
	}

	public void setTableTwo(List<String> tableTwo) {
		this.tableTwo = tableTwo;
	}

	public void setThird(String third) {
		this.third = third;
	}

	public void setFour(String four) {
		this.four = four;
	}

	public void setFive(String five) {
		this.five = five;
	}

	public void setSix(List<String> six) {
		this.six = six;
	}

	public void setSeven(String seven) {
		this.seven = seven;
	}

	public void setIncfirst(List<List<String>> incfirst) {
		Incfirst = incfirst;
	}

	public void setIncsecond(String incsecond) {
		this.incsecond = incsecond;
	}

	public void setIncsecondThree(String incsecondThree) {
		this.incsecondThree = incsecondThree;
	}

	public void setIncThird(String incThird) {
		this.incThird = incThird;
	}

	public void setIncFour(String incFour) {
		this.incFour = incFour;
	}

	public void setIncFive(String incFive) {
		this.incFive = incFive;
	}

	public void setIncSix(String incSix) {
		this.incSix = incSix;
	}

	public void setAssetOne(List<List<String>> assetOne) {
		this.assetOne = assetOne;
	}

	public void setAssetTwo(List<List<String>> assetTwo) {
		this.assetTwo = assetTwo;
	}

	public void setAssetThree(String assetThree) {
		this.assetThree = assetThree;
	}

	public void setRatioOne(String ratioOne) {
		this.ratioOne = ratioOne;
	}

	public void setRatioTwo(String ratioTwo) {
		this.ratioTwo = ratioTwo;
	}

	public void setRatioThree(String ratioThree) {
		this.ratioThree = ratioThree;
	}

	public void setRatioFour(String ratioFour) {
		this.ratioFour = ratioFour;
	}

	public void setConclusionOne(String conclusionOne) {
		this.conclusionOne = conclusionOne;
	}

	public void setConclusionTwo(List<String> conclusionTwo) {
		this.conclusionTwo = conclusionTwo;
	}

	public void setConclusionThree(String conclusionThree) {
		this.conclusionThree = conclusionThree;
	}

	public void setConclusionFour(String conclusionFour) {
		this.conclusionFour = conclusionFour;
	}

	public void setConclusionInFourFive(String conclusionInFourFive) {
		this.conclusionInFourFive = conclusionInFourFive;
	}

	public void setConclusionFive(String conclusionFive) {
		this.conclusionFive = conclusionFive;
	}

	
	
	
}
