package com.beawan.analysis.finansis.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * FnReceive entity. 应收账款
 */
@Entity
@Table(name = "FN_RECEIVE",schema = "GDTCESYS")
public class FnReceive extends BaseEntity implements java.io.Serializable {

	// Fields
	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="FN_RECEIVE_SEQ")
//	@SequenceGenerator(name="FN_RECEIVE_SEQ",allocationSize=1,initialValue=1, sequenceName="FN_RECEIVE_SEQ")
	private Long id;
	@Column(name = "TASK_ID")
	private Long taskId;//任务号
	@Column(name = "CUS_NAME")
	private String cusName;//客户名称
	@Column(name = "REMAIN_AMT")
	private Double remainAmt;//金额
	@Column(name = "RATE")
	private Double rate;//占比
	@Column(name = "ACCOUNT_AGE")
	private Integer accountAge;//账龄(月)
	@Column(name = "REMARKS")
	private String remarks;//欠款原因

	// Constructors

	/** default constructor */
	public FnReceive() {
	}

	/** minimal constructor */
	public FnReceive(Long id) {
		this.id = id;
	}

	/** full constructor */
	public FnReceive(Long id, Long taskId, String cusName, Double remainAmt,
			Double rate, Integer accountAge, String remarks) {
		this.id = id;
		this.taskId = taskId;
		this.cusName = cusName;
		this.remainAmt = remainAmt;
		this.rate = rate;
		this.accountAge = accountAge;
		this.remarks = remarks;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getCusName() {
		return this.cusName;
	}

	public void setCusName(String cusName) {
		this.cusName = cusName;
	}

	public Double getRemainAmt() {
		return this.remainAmt;
	}

	public void setRemainAmt(Double remainAmt) {
		this.remainAmt = remainAmt;
	}

	public Double getRate() {
		return this.rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Integer getAccountAge() {
		return this.accountAge;
	}

	public void setAccountAge(Integer accountAge) {
		this.accountAge = accountAge;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}