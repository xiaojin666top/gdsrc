package com.beawan.analysis.finansis.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * FnPay entity. 应付账款
 */
@Entity
@Table(name = "FN_PAY",schema = "GDTCESYS")
public class FnPay extends BaseEntity implements java.io.Serializable {

	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="FN_PAY_SEQ")
	//@SequenceGenerator(name="FN_PAY_SEQ",allocationSize=1,initialValue=1, sequenceName="FN_PAY_SEQ")
	private Long id;
	@Column(name = "TASK_ID")
	private Long taskId;//任务号
	@Column(name = "SUPPLIER_NAME")
	private String supplierName;//供应商名称
	@Column(name = "REMAIN_AMT")
	private Double remainAmt;//金额
	@Column(name = "RATE")
	private Double rate;//占比
	@Column(name = "ACCOUNT_AGE")
	private Integer accountAge;//账龄（月）
	@Column(name = "REMARKS")
	private String remarks;//欠款原因

	// Constructors

	/** default constructor */
	public FnPay() {
	}

	/** minimal constructor */
	public FnPay(Long id) {
		this.id = id;
	}

	/** full constructor */
	public FnPay(Long id, Long taskId, String supplierName, Double remainAmt,
			Double rate, Integer accountAge, String remarks) {
		this.id = id;
		this.taskId = taskId;
		this.supplierName = supplierName;
		this.remainAmt = remainAmt;
		this.rate = rate;
		this.accountAge = accountAge;
		this.remarks = remarks;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

	public String getSupplierName() {
		return this.supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public Double getRemainAmt() {
		return this.remainAmt;
	}

	public void setRemainAmt(Double remainAmt) {
		this.remainAmt = remainAmt;
	}

	public Double getRate() {
		return this.rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Integer getAccountAge() {
		return this.accountAge;
	}

	public void setAccountAge(Integer accountAge) {
		this.accountAge = accountAge;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}