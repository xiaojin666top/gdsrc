package com.beawan.analysis.finansis.common;

/**
 * @Description 财务通用行数据类
 * @author rain
 *
 */
public class FinaRowData implements java.io.Serializable {

	private String itemCode; // 项目代码
	private String itemName; // 项目名称
	private Double firstYear; // 第一年值
	private Double secondYear; // 第二年值
	private Double thirdYear; // 第三年值
	private Double fourthYear; // 第四年值
	private Double indusAvg; // 行业平均值
	private String remarks; // 备注

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Double getFirstYear() {
		return firstYear;
	}

	public void setFirstYear(Double firstYear) {
		this.firstYear = firstYear;
	}

	public Double getSecondYear() {
		return secondYear;
	}

	public void setSecondYear(Double secondYear) {
		this.secondYear = secondYear;
	}

	public Double getThirdYear() {
		return thirdYear;
	}

	public void setThirdYear(Double thirdYear) {
		this.thirdYear = thirdYear;
	}

	public Double getFourthYear() {
		return fourthYear;
	}

	public void setFourthYear(Double fourthYear) {
		this.fourthYear = fourthYear;
	}

	public Double getIndusAvg() {
		return indusAvg;
	}

	public void setIndusAvg(Double indusAvg) {
		this.indusAvg = indusAvg;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
