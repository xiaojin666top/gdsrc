package com.beawan.analysis.finansis.dao;

import com.beawan.core.BaseDao;
import com.beawan.analysis.finansis.bean.FnVerify;

/**
 * @author yzj
 */
public interface FnVerifyDao extends BaseDao<FnVerify> {
}
