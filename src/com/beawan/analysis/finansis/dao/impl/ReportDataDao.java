package com.beawan.analysis.finansis.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.analysis.finansis.bean.ReportData;
import com.beawan.analysis.finansis.dao.IReportDataDao;
import com.beawan.common.dao.impl.DaoHandler;

@Repository("reportDataDao")
public class ReportDataDao extends DaoHandler implements IReportDataDao {

	@Override
	public ReportData getReportDataById(Long id) {
		return this.selectByPrimaryKey(ReportData.class, id);
	}

	@Override
	public ReportData getReportDataByTaskIdCustNoAndType(Long taskId, 
			String customerNo, String reportType) {
		String query = "from ReportData as model where model.taskId=" + taskId
					 + " and model.customerNo='" + customerNo
					 + "' and model.reportType='" + reportType + "'";
		return this.selectSingle(ReportData.class, query);
	}

	@Override
	public ReportData getReportDataByName(String name) {
		String query = "from ReportData as model where name ='" + name +"'";
		return this.selectSingle(ReportData.class, query);
	}

	@Override
	public ReportData saveReportData(ReportData data) {
		return save(ReportData.class, data);
	}

	@Override
	public void daleteReportData(ReportData data) {
		delete(ReportData.class, data);
	}

}
