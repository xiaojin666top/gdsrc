package com.beawan.analysis.finansis.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.beawan.analysis.finansis.bean.ReportDataTR;
import com.beawan.analysis.finansis.dao.IReportDataTRDao;
import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.common.dao.impl.DaoHandler;
import com.beawan.survey.loanInfo.bean.MortgageInfo;

@Deprecated
@Repository("reportDataTRDao")
public class ReportDataTRDao extends CommDAOImpl<ReportDataTR> implements IReportDataTRDao {

	@Override
	public ReportDataTR getReportDataTrById(Long id) {
		// TODO Auto-generated method stub
		String query = "from ReportDataTR as model where id ='" + id +"'";
		List<ReportDataTR> list = getEntityManager().createQuery(query).getResultList();
		if(list.size() > 0){
			return list.get(0);
		}else{
			return null;
		}
	}

	@Override
	public List<ReportDataTR> getReportDataTrsByReportDataId(Long reportDataId) {
		// TODO Auto-generated method stub
//		String query = "from ReportDataTR as model where reportDataId ='" + reportDataId +"' order by index";
//		List<ReportDataTR> list = getEntityManager().createQuery(query).getResultList();
		
		String query = "reportDataId=:reportDataId order by index";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("reportDataId", reportDataId);
		
		List<ReportDataTR> list = select(query, params);
		
		if(list.size() > 0){
			return list;
		}else{
			return null;
		}
	}

	@Override
	public void saveReportDataTr(ReportDataTR data) {
		// TODO Auto-generated method stub
		save(ReportDataTR.class, data);
	}

	@Override
	public void deleteReportDataTr(ReportDataTR data) {
		// TODO Auto-generated method stub
		delete(ReportDataTR.class, data);
	}


}
