package com.beawan.analysis.finansis.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.beawan.common.Constants;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.beawan.analysis.finansis.bean.FnCash;
import com.beawan.analysis.finansis.bean.FnConstruc;
import com.beawan.analysis.finansis.bean.FnFinancCost;
import com.beawan.analysis.finansis.bean.FnFixedAssetsBuilding;
import com.beawan.analysis.finansis.bean.FnFixedAssetsEquipment;
import com.beawan.analysis.finansis.bean.FnFixedAssetsLand;
import com.beawan.analysis.finansis.bean.FnLongTermPay;
import com.beawan.analysis.finansis.bean.FnManageCost;
import com.beawan.analysis.finansis.bean.FnOperatCost;
import com.beawan.analysis.finansis.bean.FnOtherPay;
import com.beawan.analysis.finansis.bean.FnOtherReceive;
import com.beawan.analysis.finansis.bean.FnPay;
import com.beawan.analysis.finansis.bean.FnPayNote;
import com.beawan.analysis.finansis.bean.FnPreReceive;
import com.beawan.analysis.finansis.bean.FnPrepay;
import com.beawan.analysis.finansis.bean.FnReceNote;
import com.beawan.analysis.finansis.bean.FnReceive;
import com.beawan.analysis.finansis.bean.FnInventory;
import com.beawan.analysis.finansis.bean.FnReceiveStat;
import com.beawan.analysis.finansis.bean.FnTableAnaly;
import com.beawan.analysis.finansis.dao.IFnTableDao;
import com.beawan.common.dao.impl.DaoHandler;

@Repository("fnTableDao")
public class FnTableDao extends DaoHandler implements IFnTableDao{
	
	@Override
	public FnTableAnaly queryTableAnalyByTIdAndNo(Long taskId,String customerNo) throws DataAccessException{
		String hql = "taskId=:taskId and customerNo=:customerNo";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("taskId", taskId);
		params.put("customerNo", customerNo);
		return selectSingle(FnTableAnaly.class, hql, params);
	}

	@Override
	public void saveTableAnaly(FnTableAnaly data) throws DataAccessException{
		save(FnTableAnaly.class, data);
	}
	
	@Override
	public void deleteTableAnaly(FnTableAnaly data) throws DataAccessException{
		delete(FnTableAnaly.class, data);
	}

	@Override
	public List<FnLongTermPay> queryLongTermPayByTId(Long taskId) throws DataAccessException {
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		return this.selectByProperty(FnLongTermPay.class, params);
	}

	@Override
	public void saveLongTermPay(FnLongTermPay data) throws DataAccessException {
		save(FnLongTermPay.class, data);
	}

	@Override
	public void deleteLongTermPay(FnLongTermPay data) throws DataAccessException {
		delete(FnLongTermPay.class, data);
	}

	@Override
	public List<FnOtherPay> queryOtherPayByTId(Long taskId)throws DataAccessException {
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		return this.selectByProperty(FnOtherPay.class, params);
	}

	@Override
	public void saveOtherPay(FnOtherPay data) throws DataAccessException {
		save(FnOtherPay.class, data);
	}

	@Override
	public void deleteOtherPay(FnOtherPay data) throws DataAccessException {
		delete(FnOtherPay.class, data);
	}

	@Override
	public List<FnOtherReceive> queryOtherReceiveByTId(Long taskId) throws DataAccessException {
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		return this.selectByProperty(FnOtherReceive.class, params);
	}


	@Override
	public void saveOtherReceive(FnOtherReceive data) throws DataAccessException {
		save(FnOtherReceive.class, data);
	}

	@Override
	public void deleteOtherReceive(FnOtherReceive data) throws DataAccessException {
		delete(FnOtherReceive.class, data);
	}

	@Override
	public List<FnPay> queryPayByTId(Long taskId) throws DataAccessException {
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		return this.selectByProperty(FnPay.class, params, "id ASC");
	}

	@Override
	public void savePay(FnPay data) throws DataAccessException {
		save(FnPay.class, data);
	}

	@Override
	public void deletePay(FnPay data) throws DataAccessException {
		delete(FnPay.class, data);
	}

	@Override
	public List<FnPrepay> queryPrepayByTId(Long taskId) throws DataAccessException {
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		return this.selectByProperty(FnPrepay.class, params, "id ASC");
	}

	@Override
	public void savePrepay(FnPrepay data) throws DataAccessException {
		save(FnPrepay.class, data);
	}

	@Override
	public void deletePrepay(FnPrepay data) throws DataAccessException {
		delete(FnPrepay.class, data);
	}

	@Override
	public List<FnPreReceive> queryPreReceiveByTId(Long taskId) throws DataAccessException {
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		return this.selectByProperty(FnPreReceive.class, params, "id ASC");
	}

	@Override
	public void savePreReceive(FnPreReceive data) throws DataAccessException {
		save(FnPreReceive.class, data);
	}

	@Override
	public void deletePreReceive(FnPreReceive data) throws DataAccessException {
		delete(FnPreReceive.class, data);
	}

	@Override
	public List<FnReceive> queryReceiveByTId(Long taskId) throws DataAccessException {
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		return this.selectByProperty(FnReceive.class, params, "id ASC");
	}

	@Override
	public void saveReceive(FnReceive data) throws DataAccessException {
		save(FnReceive.class, data);
	}

	@Override
	public void deleteReceive(FnReceive data) throws DataAccessException {
		delete(FnReceive.class, data);
	}
	
	@Override
	public List<FnReceiveStat> queryReceiveStatByTId(Long taskId) throws DataAccessException {
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		return this.selectByProperty(FnReceiveStat.class, params, "id ASC");
	}

	@Override
	public void saveReceiveStat(FnReceiveStat data) throws DataAccessException {
		save(FnReceiveStat.class, data);
	}

	@Override
	public void deleteReceiveStat(FnReceiveStat data) throws DataAccessException {
		delete(FnReceiveStat.class, data);
	}

	@Override
	public FnCash queryFnCashById(Long id) {
		return this.selectByPrimaryKey(FnCash.class, id);
	}

	@Override
	public FnInventory queryFnInventoryById(Long id) {
		return this.selectByPrimaryKey(FnInventory.class, id);
	}

	@Override
	public FnReceive queryFnReceiveById(Long id) {
		return this.selectByPrimaryKey(FnReceive.class, id);
	}

	@Override
	public FnOtherReceive queryFnOtherReceiveById(Long id) {
		return this.selectByPrimaryKey(FnOtherReceive.class, id);
	}

	@Override
	public FnPrepay queryFnPrepayById(Long id) {
		return this.selectByPrimaryKey(FnPrepay.class, id);
	}

	@Override
	public FnFixedAssetsLand queryFnLandById(Long id) {
		return this.selectByPrimaryKey(FnFixedAssetsLand.class, id);
	}

	@Override
	public FnFixedAssetsBuilding queryFnBuildingById(Long id) {
		return this.selectByPrimaryKey(FnFixedAssetsBuilding.class, id);
	}

	@Override
	public FnFixedAssetsEquipment queryFnEquipmentById(Long id) {
		return this.selectByPrimaryKey(FnFixedAssetsEquipment.class, id);
	}

	@Override
	public FnPreReceive queryFnPreReceiveById(Long id) {
		return this.selectByPrimaryKey(FnPreReceive.class, id);
	}

	@Override
	public FnPay queryFnPayById(Long id) {
		return this.selectByPrimaryKey(FnPay.class, id);
	}

	@Override
	public FnOtherPay queryFnOtherPayById(Long id) {
		return this.selectByPrimaryKey(FnOtherPay.class, id);
	}

	@Override
	public List<FnCash> queryFnCashByTId(Long taskId) throws DataAccessException {
		// TODO Auto-generated method stub
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		return this.selectByProperty(FnCash.class, params);
	}

	@Override
	public void saveFnCash(FnCash data) throws DataAccessException {
		// TODO Auto-generated method stub
		save(FnCash.class, data);
	}

	@Override
	public void deleteFnCash(FnCash data) throws DataAccessException {
		// TODO Auto-generated method stub
		delete(FnCash.class, data);
	}

	@Override
	public List<FnConstruc> queryFnConstrucByTId(Long taskId) throws DataAccessException {
		// TODO Auto-generated method stub
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		return this.selectByProperty(FnConstruc.class, params);
	}

	@Override
	public void saveFnConstruc(FnConstruc data) throws DataAccessException {
		// TODO Auto-generated method stub
		save(FnConstruc.class, data);
	}

	@Override
	public void deleteFnConstruc(FnConstruc data) throws DataAccessException {
		// TODO Auto-generated method stub
		delete(FnConstruc.class, data);
	}

	@Override
	public List<FnFinancCost> queryFinancCostByTId(Long taskId) throws DataAccessException {
		// TODO Auto-generated method stub
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		return this.selectByProperty(FnFinancCost.class, params);
	}

	@Override
	public void saveFinancCost(FnFinancCost data) throws DataAccessException {
		// TODO Auto-generated method stub
		save(FnFinancCost.class, data);
	}

	@Override
	public void deleteFinancCost(FnFinancCost data) throws DataAccessException {
		// TODO Auto-generated method stub
		delete(FnFinancCost.class, data);
	}

	@Override
	public List<FnManageCost> queryManageCostByTId(Long taskId) throws DataAccessException {
		// TODO Auto-generated method stub
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		return this.selectByProperty(FnManageCost.class, params);
	}

	@Override
	public void saveManageCost(FnManageCost data) throws DataAccessException {
		// TODO Auto-generated method stub
		save(FnManageCost.class, data);
	}

	@Override
	public void deleteManageCost(FnManageCost data) throws DataAccessException {
		// TODO Auto-generated method stub
		delete(FnManageCost.class, data);
	}

	@Override
	public List<FnOperatCost> queryOperatCostByTId(Long taskId) throws DataAccessException {
		// TODO Auto-generated method stub
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		return this.selectByProperty(FnOperatCost.class, params);
	}

	@Override
	public void saveOperatCost(FnOperatCost data) throws DataAccessException {
		// TODO Auto-generated method stub
		save(FnOperatCost.class, data);
	}

	@Override
	public void deleteOperatCost(FnOperatCost data) throws DataAccessException {
		// TODO Auto-generated method stub
		delete(FnOperatCost.class, data);
	}

	@Override
	public List<FnPayNote> queryFnPayNoteByTId(Long taskId) throws DataAccessException {
		// TODO Auto-generated method stub
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		return this.selectByProperty(FnPayNote.class, params);
	}

	@Override
	public void saveFnPayNote(FnPayNote data) throws DataAccessException {
		// TODO Auto-generated method stub
		save(FnPayNote.class, data);
	}

	@Override
	public void deleteFnPayNote(FnPayNote data) throws DataAccessException {
		// TODO Auto-generated method stub
		delete(FnPayNote.class, data);
	}

	@Override
	public List<FnReceNote> queryFnReceNoteByTId(Long taskId) throws DataAccessException {
		// TODO Auto-generated method stub
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		return this.selectByProperty(FnReceNote.class, params);
	}

	@Override
	public void saveFnReceNote(FnReceNote data) throws DataAccessException {
		// TODO Auto-generated method stub
		save(FnReceNote.class, data);
	}

	@Override
	public void deleteFnReceNote(FnReceNote data) throws DataAccessException {
		// TODO Auto-generated method stub
		delete(FnReceNote.class, data);
	}

	@Override
	public List<FnInventory> queryFnInventoryByTId(Long taskId) throws DataAccessException {
		// TODO Auto-generated method stub
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		return this.selectByProperty(FnInventory.class, params);
	}

	@Override
	public void saveFnInventory(FnInventory data) throws DataAccessException {
		// TODO Auto-generated method stub
		save(FnInventory.class, data);
	}

	@Override
	public void deleteFnInventory(FnInventory data) throws DataAccessException {
		// TODO Auto-generated method stub
		delete(FnInventory.class, data);
	}

	@Override
	public List<FnFixedAssetsLand> queryFnFixedAssetsLandByTId(Long taskId) throws DataAccessException {
		// TODO Auto-generated method stub
		Map<String, Object> params = new HashMap<>();
		params.put("task_Id", taskId);
		return this.selectByProperty(FnFixedAssetsLand.class, params);
	}

	@Override
	public void saveFnFixedAssetsLand(FnFixedAssetsLand data) throws DataAccessException {
		// TODO Auto-generated method stub
		save(FnFixedAssetsLand.class,data);
	}

	@Override
	public void deleteFnFixedAssetsLand(FnFixedAssetsLand data) throws DataAccessException {
		// TODO Auto-generated method stub
		delete(FnFixedAssetsLand.class,data);
	}

	@Override
	public List<FnFixedAssetsBuilding> queryFnFixedAssetsBuildingByTId(Long taskId) throws DataAccessException {
		// TODO Auto-generated method stub
		Map<String, Object> params = new HashMap<>();
		params.put("task_Id", taskId);
		return this.selectByProperty(FnFixedAssetsBuilding.class, params);
	}

	@Override
	public void saveFnFixedAssetsBuilding(FnFixedAssetsBuilding data) throws DataAccessException {
		// TODO Auto-generated method stub
		save(FnFixedAssetsBuilding.class,data);
	}

	@Override
	public void deleteFnFixedAssetsBuilding(FnFixedAssetsBuilding data) throws DataAccessException {
		// TODO Auto-generated method stub
		delete(FnFixedAssetsBuilding.class,data);
	}

	@Override
	public List<FnFixedAssetsEquipment> queryFnFixedAssetsEquipmentByTId(Long taskId) throws DataAccessException {
		// TODO Auto-generated method stub
		Map<String, Object> params = new HashMap<>();
		params.put("taskId", taskId);
		return this.selectByProperty(FnFixedAssetsEquipment.class, params);
	}

	@Override
	public void saveFnFixedAssetsEquipment(FnFixedAssetsEquipment data) throws DataAccessException {
		// TODO Auto-generated method stub
		save(FnFixedAssetsEquipment.class,data);
	}

	@Override
	public void deleteFnFnFixedAssetsEquipment(FnFixedAssetsEquipment data) throws DataAccessException {
		// TODO Auto-generated method stub
		delete(FnFixedAssetsEquipment.class,data);
	}

}
