package com.beawan.analysis.finansis.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.analysis.finansis.dao.FnVerifyDao;
import com.beawan.analysis.finansis.bean.FnVerify;
import org.springframework.stereotype.Repository;
import com.beawan.analysis.finansis.bean.FnVerify;

/**
 * @author yzj
 */
@Repository("fnVerifyDao")
public class FnVerifyDaoImpl extends BaseDaoImpl<FnVerify> implements FnVerifyDao{

}
