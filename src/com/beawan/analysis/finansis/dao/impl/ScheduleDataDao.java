package com.beawan.analysis.finansis.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.beawan.analysis.finansis.bean.ScheduleData;
import com.beawan.analysis.finansis.dao.IScheduleDataDao;
import com.beawan.common.dao.impl.DaoHandler;
import com.beawan.common.dao.impl.SerializeDao;

@Deprecated
@Repository("scheduleDataDao")
public class ScheduleDataDao extends DaoHandler implements IScheduleDataDao {

	@Override
	public List<ScheduleData> getScheByTaskIdAndRepType(Long taskId, String repType) {
		// TODO Auto-generated method stub
		String query = "from ScheduleData as model where model.taskId ='" + taskId +"' and model.reportType ='" + repType + "' order by index";
		List<ScheduleData> list = getEntityManager().createQuery(query).getResultList();
		if(list.size() > 0){
			return list;
		}else{
			return null;
		}
	}

	@Override
	public void saveScheduleData(ScheduleData data) {
		// TODO Auto-generated method stub
		save(ScheduleData.class, data);
	}

	@Override
	public void deleteScheduleData(ScheduleData data) {
		// TODO Auto-generated method stub
		delete(ScheduleData.class, data);
	}
	
	@Override
	public List<ScheduleData> getAllScheduleByTaskId(Long taskId){
		String query = "from ScheduleData as model where taskId ='" + taskId +"'";
		List<ScheduleData> list = getEntityManager().createQuery(query).getResultList();
		if(list.size() > 0){
			return list;
		}else{
			return null;
		}
	}

}
