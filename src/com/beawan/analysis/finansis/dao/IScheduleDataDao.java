package com.beawan.analysis.finansis.dao;

import java.util.List;

import com.beawan.analysis.finansis.bean.ScheduleData;

public interface IScheduleDataDao {
	
	/**
	 * @Description (通过客户号和表格类型查找明细表)
	 * @param customerNo 客户号
	 * @param repType	表格类型
	 * @return
	 */
	List<ScheduleData> getScheByTaskIdAndRepType(Long taskId, String repType);

	/**
	 * @Description (保存明细表)
	 * @param data
	 */
	public void saveScheduleData(ScheduleData data);
	
	
	/**
	 * @Description (删除明细表)
	 * @param data
	 */
	public void deleteScheduleData(ScheduleData data);
	
	/**
	 * @Description (找出一个客户的所有明细表)
	 * @param data
	 */
	public List<ScheduleData> getAllScheduleByTaskId(Long taskId);
	

}
