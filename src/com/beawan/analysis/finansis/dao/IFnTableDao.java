package com.beawan.analysis.finansis.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.analysis.finansis.bean.FnCash;
import com.beawan.analysis.finansis.bean.FnConstruc;
import com.beawan.analysis.finansis.bean.FnFinancCost;
import com.beawan.analysis.finansis.bean.FnFixedAssetsBuilding;
import com.beawan.analysis.finansis.bean.FnFixedAssetsEquipment;
import com.beawan.analysis.finansis.bean.FnFixedAssetsLand;
import com.beawan.analysis.finansis.bean.FnLongTermPay;
import com.beawan.analysis.finansis.bean.FnManageCost;
import com.beawan.analysis.finansis.bean.FnOperatCost;
import com.beawan.analysis.finansis.bean.FnOtherPay;
import com.beawan.analysis.finansis.bean.FnOtherReceive;
import com.beawan.analysis.finansis.bean.FnPay;
import com.beawan.analysis.finansis.bean.FnPayNote;
import com.beawan.analysis.finansis.bean.FnPreReceive;
import com.beawan.analysis.finansis.bean.FnPrepay;
import com.beawan.analysis.finansis.bean.FnReceNote;
import com.beawan.analysis.finansis.bean.FnReceive;
import com.beawan.analysis.finansis.bean.FnInventory;
import com.beawan.analysis.finansis.bean.FnReceiveStat;
import com.beawan.analysis.finansis.bean.FnTableAnaly;

public interface IFnTableDao {
	
	/**
	 * @Description (根据客户号和任务号查询明细表分析)
	 * @param custNo
	 *            客户号
	 * @return
	 */
	public FnTableAnaly queryTableAnalyByTIdAndNo(Long taskId,String custNo) throws DataAccessException;

	/**
	 * @Description (保存明细表分析)
	 * @param data
	 */
	public void saveTableAnaly(FnTableAnaly data) throws DataAccessException;
	
	/**
	 * @Description (删除明细表分析)
	 * @param data
	 *            
	 */
	public void deleteTableAnaly(FnTableAnaly data) throws DataAccessException;
	
	/**
	 * @Description (根据任务号查询长期应付款)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public List<FnLongTermPay> queryLongTermPayByTId(Long taskId) throws DataAccessException;

	/**
	 * @Description (保存长期应付款)
	 * @param data
	 */
	public void saveLongTermPay(FnLongTermPay data) throws DataAccessException;
	
	/**
	 * @Description (删除长期应付款)
	 * @param data
	 *            
	 */
	public void deleteLongTermPay(FnLongTermPay data) throws DataAccessException;
	
	
	/**
	 * @Description (根据任务号查询其他应付款)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public List<FnOtherPay> queryOtherPayByTId(Long taskId) throws DataAccessException;

	/**
	 * @Description (保存其他应付款)
	 * @param data
	 *            现金流量简要的结构分析)
	 */
	public void saveOtherPay(FnOtherPay data) throws DataAccessException;
	
	/**
	 * @Description (删除其他应付款)
	 * @param data
	 *            现金流量简要的结构分析)
	 */
	public void deleteOtherPay(FnOtherPay data) throws DataAccessException;
	
	
	
	
	/**
	 * @Description (根据任务号查询其他应收款)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public List<FnOtherReceive> queryOtherReceiveByTId(Long taskId) throws DataAccessException;
	
	/**
	 * @Description (保存其他应收款)
	 * @param data
	 */
	public void saveOtherReceive(FnOtherReceive data) throws DataAccessException;
	
	/**
	 * @Description (删除企业其他应收款)
	 * @param data
	 */
	public void deleteOtherReceive(FnOtherReceive data) throws DataAccessException;
	
	
	
	/**
	 * @Description (根据任务号查询应付账款)
	 * @param taskId任务号
	 * @return
	 */
	public List<FnPay> queryPayByTId(Long taskId) throws DataAccessException;

	/**
	 * @Description (保存企业应付账款)
	 * @param data
	 *            现金流量简要的结构分析)
	 */
	public void savePay(FnPay data) throws DataAccessException;
	
	/**
	 * @Description (删除企业应付账款)
	 * @param data
	 *            现金流量简要的结构分析)
	 */
	public void deletePay(FnPay data) throws DataAccessException;
	
	
	
	/**
	 * @Description (根据任务号查询预付账款)
	 * @param taskId 任务号
	 * @return
	 */
	public List<FnPrepay> queryPrepayByTId(Long taskId) throws DataAccessException;

	/**
	 * @Description (保存预付账款)
	 * @param data
	 */
	public void savePrepay(FnPrepay data) throws DataAccessException;
	
	/**
	 * @Description (删除企业预付账款)
	 * @param data
	 */
	public void deletePrepay(FnPrepay data) throws DataAccessException;
	
	
	
	/**
	 * @Description (根据任务号查询预收账款)
	 * @param taskId任务号
	 * @return
	 */
	public List<FnPreReceive> queryPreReceiveByTId(Long taskId) throws DataAccessException;

	/**
	 * @Description (保存企业预收账款
	 *            现金流量简要的结构分析)
	 */
	public void savePreReceive(FnPreReceive data) throws DataAccessException;
	
	/**
	 * @Description (删除企业预收账款)
	 * @param data
	 */
	public void deletePreReceive(FnPreReceive data) throws DataAccessException;
	
	
	
	/**
	 * @Description (根据任务号查询应收账款)
	 * @param custNo
	 *            客户号
	 * @return
	 */
	public List<FnReceive> queryReceiveByTId(Long taskId) throws DataAccessException;

	/**
	 * @Description (保存企业应收账款)
	 * @param data
	 */
	public void saveReceive(FnReceive data) throws DataAccessException;
	
	/**
	 * @Description (删除企业应收账款)
	 * @param data
	 */
	public void deleteReceive(FnReceive data) throws DataAccessException;
	
	/**
	 * @Description (根据任务号查询应收账款统计信息)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public List<FnReceiveStat> queryReceiveStatByTId(Long taskId) throws DataAccessException;

	/**
	 * @Description (保存企业应收账款统计信息)
	 * @param data
	 */
	public void saveReceiveStat(FnReceiveStat data) throws DataAccessException;
	
	/**
	 * @Description (删除企业应收账款统计信息)
	 * @param data
	 */
	public void deleteReceiveStat(FnReceiveStat data) throws DataAccessException;

	/**
	 * 根据主键查询 货币资金
	 * @param id
	 * @return
	 */
	FnCash queryFnCashById(Long id);

	/**
	 * 根据主键查询 存货
	 * @param id
	 * @return
	 */
	FnInventory queryFnInventoryById(Long id);

	/**
	 * 根据主键查询 应收账款
	 * @param id
	 * @return
	 */
	FnReceive queryFnReceiveById(Long id);

	/**
	 * 根据主键查询 其他应收账款
	 * @param id
	 * @return
	 */
	FnOtherReceive queryFnOtherReceiveById(Long id);

	/**
	 * 根据主键查询 预付账款
	 * @param id
	 * @return
	 */
	FnPrepay queryFnPrepayById(Long id);

	/**
	 * 根据主键查询 固定资产-土地
	 * @param id
	 * @return
	 */
	FnFixedAssetsLand queryFnLandById(Long id);

	/**
	 * 根据主键查询 固定资产-房屋
	 * @param id
	 * @return
	 */
	FnFixedAssetsBuilding queryFnBuildingById(Long id);

	/**
	 * 根据主键查询 固定资产-设备
	 * @param id
	 * @return
	 */
	FnFixedAssetsEquipment queryFnEquipmentById(Long id);

	/**
	 * 根据主键查询 固定资产-设备
	 * @param id
	 * @return
	 */
	FnPreReceive queryFnPreReceiveById(Long id);

	/**
	 * 根据主键查询 应付账款
	 * @param id
	 * @return
	 */
	FnPay queryFnPayById(Long id);

	/**
	 * 根据主键查询 其他应付账款
	 * @param id
	 * @return
	 */
	FnOtherPay queryFnOtherPayById(Long id);

	/**
	 * @Description (根据任务号查询货币资金)
	 * @param taskId 任务号
	 * @return
	 */
	public List<FnCash> queryFnCashByTId(Long taskId) throws DataAccessException;

	/**
	 * @Description (保存企业货币资金)
	 * @param data
	 */
	public void saveFnCash(FnCash data) throws DataAccessException;
	
	/**
	 * @Description (删除企业货币资金)
	 * @param data
	 */
	public void deleteFnCash(FnCash data) throws DataAccessException;
	
	/**
	 * @Description (根据任务号查询在建工程)
	 * @param taskId任务号
	 * @return
	 */
	public List<FnConstruc> queryFnConstrucByTId(Long taskId) throws DataAccessException;

	/**
	 * @Description (保存企业在建工程)
	 * @param data
	 */
	public void saveFnConstruc(FnConstruc data) throws DataAccessException;
	
	/**
	 * @Description (删除企业在建工程)
	 * @param data
	 */
	public void deleteFnConstruc(FnConstruc data) throws DataAccessException;
	
	/**
	 * @Description (根据任务号查询财务费用明细)
	 * @param taskId任务号
	 * @return
	 */
	public List<FnFinancCost> queryFinancCostByTId(Long taskId) throws DataAccessException;

	/**
	 * @Description (保存财务费用明细
	 * @param data
	 */
	public void saveFinancCost(FnFinancCost data) throws DataAccessException;
	
	/**
	 * @Description (删除财务费用明细)
	 * @param data
	 */
	public void deleteFinancCost(FnFinancCost data) throws DataAccessException;
	
	/**
	 * @Description (根据任务号查询管理费用明细)
	 * @param taskId任务号
	 * @return
	 */
	public List<FnManageCost> queryManageCostByTId(Long taskId) throws DataAccessException;

	/**
	 * @Description (保存管理明细)
	 * @param data
	 */
	public void saveManageCost(FnManageCost data) throws DataAccessException;
	
	/**
	 * @Description (删除管理明细)
	 * @param data
	 */
	public void deleteManageCost(FnManageCost data) throws DataAccessException;
	
	/**
	 * @Description (根据任务号查询营业费用明细)
	 * @param taskId 任务号
	 * @return
	 */
	public List<FnOperatCost> queryOperatCostByTId(Long taskId) throws DataAccessException;

	/**
	 * @Description (保存营业明细)
	 * @param data
	 */
	public void saveOperatCost(FnOperatCost data) throws DataAccessException;
	
	/**
	 * @Description (删除营业明细)
	 * @param data
	 */
	public void deleteOperatCost(FnOperatCost data) throws DataAccessException;
	
	/**
	 * @Description (根据任务号查询应付票据明细）
	 * @param taskId 任务号
	 * @return
	 */
	public List<FnPayNote> queryFnPayNoteByTId(Long taskId) throws DataAccessException;

	/**
	 * @Description (保存应付票据明细）
	 * @param data
	 */
	public void saveFnPayNote(FnPayNote data) throws DataAccessException;
	
	/**
	 * @Description (删除应付票据明细）
	 * @param data
	 */
	public void deleteFnPayNote(FnPayNote data) throws DataAccessException;
	
	/**
	 * @Description (根据任务号查询应收票据明细）
	 * @param taskId 任务号
	 * @return
	 */
	public List<FnReceNote> queryFnReceNoteByTId(Long taskId) throws DataAccessException;

	/**
	 * @Description (保存应收票据明细）
	 * @param data
	 */
	public void saveFnReceNote(FnReceNote data) throws DataAccessException;
	
	/**
	 * @Description (删除应收票据明细）
	 * @param data
	 */
	public void deleteFnReceNote(FnReceNote data) throws DataAccessException;
	

	/**
	 * @Description (根据任务号查询存货明细）
	 * @param taskId 任务号
	 * @return
	 */
	public List<FnInventory> queryFnInventoryByTId(Long taskId) throws DataAccessException;

	/**
	 * @Description (保存存货明细）
	 * @param data
	 */
	public void saveFnInventory(FnInventory data) throws DataAccessException;
	
	/**
	 * @Description (删除存货明细）
	 * @param data
	 */
	public void deleteFnInventory(FnInventory data) throws DataAccessException;
	
	
	/**
	 * @Description (根据任务号查询固定资产-土地）
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public List<FnFixedAssetsLand> queryFnFixedAssetsLandByTId(Long taskId) throws DataAccessException;

	/**
	 * @Description (保存固定资产-土地）
	 * @param data
	 */
	public void saveFnFixedAssetsLand(FnFixedAssetsLand data) throws DataAccessException;
	
	/**
	 * @Description (删除固定资产-土地）
	 * @param data
	 */
	public void deleteFnFixedAssetsLand(FnFixedAssetsLand data) throws DataAccessException;
	
	
	/**
	 * @Description (根据任务号查询固定资产-房屋）
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public List<FnFixedAssetsBuilding> queryFnFixedAssetsBuildingByTId(Long taskId) throws DataAccessException;

	/**
	 * @Description (保存固定资产-房屋）
	 * @param data
	 */
	public void saveFnFixedAssetsBuilding(FnFixedAssetsBuilding data) throws DataAccessException;
	
	/**
	 * @Description (删除固定资产-房屋）
	 * @param data
	 */
	public void deleteFnFixedAssetsBuilding(FnFixedAssetsBuilding data) throws DataAccessException;

	
	/**
	 * @Description (根据任务号查询固定资产-设备）
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public List<FnFixedAssetsEquipment> queryFnFixedAssetsEquipmentByTId(Long taskId) throws DataAccessException;

	/**
	 * @Description (保存固定资产-设备）
	 * @param data
	 */
	public void saveFnFixedAssetsEquipment(FnFixedAssetsEquipment data) throws DataAccessException;
	
	/**
	 * @Description (删除固定资产-设备）
	 * @param data
	 */
	public void deleteFnFnFixedAssetsEquipment(FnFixedAssetsEquipment data) throws DataAccessException;
}
