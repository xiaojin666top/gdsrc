package com.beawan.analysis.finansis.dao;

import com.beawan.analysis.finansis.bean.ReportData;

public interface IReportDataDao {
	
	/**
	 * @Description (通过id查询三大报表表格数据)
	 * @param id
	 * @return
	 */
	public ReportData getReportDataById(Long id);
	
	/**
	 * @Description (通过客户号和表格类型查询三大报表表格数据，其中类型分别为  balance，newBlance income newIncome，cashFlow newCashFlow)
	 * @param id
	 * @return
	 */
	public ReportData getReportDataByTaskIdCustNoAndType(Long taskId, String customerNo,String reportType);
	
	/**
	 * @Description (通过name查询三大报表表格数据)
	 * @param name
	 * @return
	 */
	public ReportData getReportDataByName(String name);
	
	
	/**
	 * @Description (保存表格数据)
	 * @param data
	 */
	public ReportData saveReportData(ReportData data);
	
	/**
	 * @Description (删除表格数据)
	 * @param data
	 */
	public void daleteReportData(ReportData data);


}
