package com.beawan.analysis.finansis.dao;

import java.util.List;

import com.beawan.analysis.finansis.bean.ReportDataTR;
import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.loanInfo.bean.PledgeInfo;

public interface IReportDataTRDao extends ICommDAO<ReportDataTR>{
	
	/**
	 * @Description (通过id查找reportDataId)
	 * @param id
	 * @return
	 */
	public ReportDataTR getReportDataTrById(Long id);

	/**
	 * @Description (通过关联表格表格id查找表格所有的行)
	 * @return
	 */
	public List<ReportDataTR> getReportDataTrsByReportDataId(Long reportDataId);
	
	/**
	 * @Description (TODO这里用一句话描述这个方法的作用)
	 * @param data
	 */
	public void saveReportDataTr(ReportDataTR data);
	
	/**
	 * @Description (TODO这里用一句话描述这个方法的作用)
	 * @param data
	 */
	public void deleteReportDataTr(ReportDataTR data);

}
