package com.beawan.analysis.finansis.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.beawan.Interface.SaveReportDataTr;
import com.beawan.analysis.abnormal.bean.ErrData;
import com.beawan.analysis.abnormal.service.IAbnormalService;
import com.beawan.analysis.abnormal.uitls.GetErrNameReason;
import com.beawan.analysis.finansis.bean.ReportData;
import com.beawan.analysis.finansis.bean.ReportDataTR;
import com.beawan.analysis.finansis.bean.ScheduleData;
import com.beawan.analysis.finansis.service.IFinanasisService;
import com.beawan.analysis.finansis.utils.MakeCashFlowUtil;
import com.beawan.analysis.finansis.utils.ReadSheel;
import com.beawan.analysis.finansis.utils.ReadThreeReportSheel;
import com.beawan.base.entity.User;
import com.beawan.base.service.ITableSubjCodeSV;
import com.beawan.base.service.IUserSV;
import com.beawan.common.SysConstants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.library.bean.IndustryQRadio;
import com.beawan.library.bean.ReportItemTemp;
import com.beawan.library.bean.ReportTemplet;
import com.beawan.library.bean.TreeData;
import com.beawan.library.service.IIndustrySV;
import com.beawan.library.service.ITempletSV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.beawan.web.impl.UserSession;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.platform.util.HttpUtil;
import com.platform.util.StringUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/" })
public class ReportDataCtl {

	private static final Logger log = Logger.getLogger(ReportDataCtl.class);

	@Resource
	private IUserSV userSV;
	@Resource
	private ITaskSV taskSV;
	@Resource
	private IFinanasisService finanasisSV;
	@Resource
	private ITempletSV templetService;
	@Resource
	private IIndustrySV industrySV;
	@Resource
	private IAbnormalService abnormalService;
	@Resource
	private ITableSubjCodeSV tableSubjCodeSV;

	private Map<String, Object> model = new HashMap<String, Object>();

	public Map<String, Object> getModel() {
		return model;
	}

	public void setModel(Map<String, Object> model) {
		this.model = model;
	}

	/**
	 * @Description (资产负债表、利润表电子版导入)
	 * @param files
	 * @param request
	 * @param response
	 * @param repId
	 *            表id 用于区分是资产负债表还是利润表
	 * @param taskId
	 *            任务id
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("loadFile.json")
	@ResponseBody
	public Map<String, Object> loadFile(@RequestParam("files") CommonsMultipartFile files, HttpServletRequest request,
			HttpServletResponse response, String repId, String taskId, String alertFalg)
			throws IOException {
		ReportTemplet report = templetService.getReportTempletById(Long.valueOf(repId));
		InputStream inStream = files.getInputStream(); // 读入原文件
		if(alertFalg==null || alertFalg.equals("")){
			alertFalg="false";
		}
		if (report.getName().equals("资产负债表")) {
			List<ReportItemTemp> reportItemTemps = templetService.getAllReportItemByType("1");
			Map<String, Object> map = ReadThreeReportSheel.getSheelReportData(0, 1, 101, inStream, 1, 1, "balance",
					reportItemTemps,alertFalg);
			@SuppressWarnings("unchecked")
			List<ReportDataTR> reportDataTRs = (List<ReportDataTR>) map.get("trs");
			request.getSession().setAttribute("balanceTRs", reportDataTRs);
			@SuppressWarnings("unchecked")
			List<String> strings = (List<String>) map.get("notNames");
			model.put("url", report.getUrl());
			model.put("notNames", strings);
		}
		if (report.getName().equals("利润表")) {
			List<ReportItemTemp> reportItemTemps = templetService.getAllReportItemByType("2");
			Map<String, Object> map = ReadThreeReportSheel.getSheelReportData(1, 1, 50, inStream, 1, 1, "income",
					reportItemTemps,alertFalg);
			@SuppressWarnings("unchecked")
			List<ReportDataTR> reportDataTRs = (List<ReportDataTR>) map.get("trs");
			request.getSession().setAttribute("incomeTRs", reportDataTRs);
			@SuppressWarnings("unchecked")
			List<String> strings = (List<String>) map.get("notNames");
			model.put("url", report.getUrl());
			model.put("notNames", strings);
		}
		model.put("modelId", repId);
		model.put("id", taskId);
		return model;
	}

	@RequestMapping("updateTableItem.json")
	@ResponseBody
	public Map<String, Object> updateTableItem(HttpServletRequest request, HttpServletResponse response) {
		String trdata = HttpUtil.getAsString(request, "trdata");
		try {
			JSONArray array = JSONArray.fromObject(trdata);
			if (array != null && array.size() > 0) {
				for (int i = 0; i < array.size(); i++) {
					JSONObject trStr = JSONObject.fromObject(array.get(i));
					ReportItemTemp tr = (ReportItemTemp) JSONObject.toBean(trStr, ReportItemTemp.class);
					String name = tr.getName();
					if(tr.getNeedChangeName()==null || tr.getNeedChangeName().equals("")){
						continue;
					}
					ReportItemTemp target = templetService.getAllReportItemByName(name);
					if(target!=null){
						if(target.getNeedChangeName()==null){
							target.setNeedChangeName(tr.getNeedChangeName());
						}else{
							String tarString = target.getNeedChangeName()+tr.getNeedChangeName()+",";
							target.setNeedChangeName(tarString);
						}
						templetService.saveReportItem(target);
					}
				}
			}
			model.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			log.error(" 跳转房地产合法、合规性分析信息异常！", e);
		}
		return model;
	}

	/**
	 * @Description 跳转房地产合法、合规性分析信息
	 * @param request
	 * @param response
	 * @param abvenName
	 * @return
	 */
	@RequestMapping("uploadFileView.do")
	public String projAnalyInfo(HttpServletRequest request, HttpServletResponse response) {
		String pageUrl = "";
		try {
			String id = HttpUtil.getAsString(request, "id");
			Task task = taskSV.getTaskById(Long.valueOf(id));
			String modelId = HttpUtil.getAsString(request, "modelId");
			pageUrl = HttpUtil.getAsString(request, "pageUrl");
			String customerNo = HttpUtil.getAsString(request, "customerNo");
			String type = HttpUtil.getAsString(request, "type");
			String alertFalg = HttpUtil.getAsString(request, "alertFalg");
			String notNames = HttpUtil.getAsString(request, "notNames");
			String[] notN = notNames.split(",");
			/*String reportDataTrs = HttpUtil.getAsString(request, "reportDataTrs");
			List<ReportDataTR> trs = new ArrayList<ReportDataTR>();
			JSONArray array = JSONArray.fromObject(reportDataTrs);
			if (array != null && array.size() > 0) {
				for (int i = 0; i < array.size(); i++) {
					JSONObject dataJson = JSONObject.fromObject(array.get(i));
					ReportDataTR data = (ReportDataTR) JSONObject.toBean(dataJson, ReportDataTR.class);
					trs.add(data);
				}
			}*/
			
			ReportTemplet report = templetService.getReportTempletById(Long.valueOf(modelId));
			if(report.getName().equals("资产负债表")){
				List<ReportDataTR> reportDataTRs = (List<ReportDataTR>) request.getSession().getAttribute("balanceTRs");
				request.setAttribute("reportDataTrs", reportDataTRs);
			}else if(report.getName().equals("利润表")){
				List<ReportDataTR> reportDataTRs = (List<ReportDataTR>) request.getSession().getAttribute("incomeTRs");
				request.setAttribute("reportDataTrs", reportDataTRs);
			}
			
			request.setAttribute("year", task.getYear());
			request.setAttribute("month", task.getMonth());
			request.setAttribute("notN", notN);
			request.setAttribute("id", id);
			request.setAttribute("customerNo", customerNo);
			request.setAttribute("type", type);
			request.setAttribute("alertFalg", alertFalg);
			request.setAttribute("modelId", modelId);
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(" 跳转房地产合法、合规性分析信息异常！", e);
		}
		return pageUrl;
	}

	/**
	 * @Description (报表信息编辑页面)
	 * @param request
	 * @param response
	 * @param id
	 * @param state
	 * @return
	 */
	@RequestMapping("editTaskView.do")
	public String editTaskView(HttpServletRequest request, HttpServletResponse response, String taskId) {
		request.setAttribute("taskId", taskId);
		List<ReportTemplet> reportid = this.templetService.getAllReportTemplets();
		Task task = taskSV.getTaskById(Long.valueOf(taskId));
		for (ReportTemplet rep : reportid) {
			if ("资产负债表".equals(rep.getName())) {
				ReportData balance = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(),
						task.getCustomerNo(), SysConstants.ReportType.BALANCE);
				if (balance != null) {
					request.setAttribute("balanceDataId", String.valueOf(balance.getId()));
				} else {
					request.setAttribute("balanceDataId", "");
				}
				request.setAttribute("balanceTId", rep.getId());
			} else if ("利润表".equals(rep.getName())) {
				ReportData income = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(),
						task.getCustomerNo(), SysConstants.ReportType.INCOME);
				if (income != null) {
					request.setAttribute("incomeDataId", String.valueOf(income.getId()));
				} else {
					request.setAttribute("incomeDataId", "");
				}
				request.setAttribute("incomeTId", rep.getId());
			}
		}
		return "views/Application/checkForm";
	}

	/**
	 * @Description (报表信息编辑页面)
	 * @param request
	 * @param response
	 * @param id
	 * @param state
	 * @return
	 */
	@RequestMapping("editGpTaskView.do")
	public String editGpTaskView(HttpServletRequest request, HttpServletResponse response, String taskId,
			String customerNo, String type) {
		request.setAttribute("taskId", taskId);
		request.setAttribute("customerNo", customerNo);
		request.setAttribute("type", type);
		List<ReportTemplet> reportid = this.templetService.getAllReportTemplets();
		Task task = taskSV.getTaskById(Long.valueOf(taskId));

		for (ReportTemplet rep : reportid) {
			if ("资产负债表".equals(rep.getName())) {
				String repType = "";
				if (type.equals("0")) {
					repType = SysConstants.ReportType.BALANCE;
				} else if(type.equals("1")) {
					repType = SysConstants.ReportType.PARENT_BALANCE;
				} else if(type.equals("2")) {
					repType = SysConstants.ReportType.TAX_BALANCE;
				}
				ReportData balance = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(), customerNo,
						repType);
				if (balance != null) {
					request.setAttribute("balanceDataId", String.valueOf(balance.getId()));
				} else {
					request.setAttribute("balanceDataId", "");
				}
				request.setAttribute("balanceTId", rep.getId());
			} else if ("利润表".equals(rep.getName())) {
				String repType = "";
				if (type.equals("0")) {
					repType = SysConstants.ReportType.INCOME;
				} else if(type.equals("1")) {
					repType = SysConstants.ReportType.PARENT_INCOME;
				}else if(type.equals("2")) {
					repType = SysConstants.ReportType.TAX_INCOME;
				}
				ReportData income = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(), customerNo,
						repType);
				if (income != null) {
					request.setAttribute("incomeDataId", String.valueOf(income.getId()));
				} else {
					request.setAttribute("incomeDataId", "");
				}
				request.setAttribute("incomeTId", rep.getId());
			}
		}
		return "views/Application/checkForm";
	}

	/**
	 * @Description (保存报表,生成异常)
	 * @param request
	 * @param response
	 * @param taskId
	 * @return
	 */
	@RequestMapping("saveForm.do")
	public String saveForm(HttpServletRequest request, HttpServletResponse response, String taskId, String customerNo,
			String type) {
		try {
			Task task = taskSV.getTaskById(Long.valueOf(taskId));
			if (StringUtil.isEmptyString(customerNo)) {
				customerNo = task.getCustomerNo();
			}
			if (StringUtil.isEmptyString(type)) {
				type = "0";
			}
			String balanceStr = SysConstants.ReportType.BALANCE;
			String newBalanceStr = SysConstants.ReportType.NEW_BALANCE;
			String incomeStr = SysConstants.ReportType.INCOME;
			String newIncomeStr = SysConstants.ReportType.NEW_INCOME;
			String cashFlowStr = SysConstants.ReportType.CASH_FLOW;
			String newCashFlowStr = SysConstants.ReportType.NEW_CASH_FLOW;
			if (type.equals("1")) {
				balanceStr = SysConstants.ReportType.PARENT_BALANCE;
				newBalanceStr = SysConstants.ReportType.NEW_PARENT_BALANCE;
				incomeStr = SysConstants.ReportType.PARENT_INCOME;
				newIncomeStr = SysConstants.ReportType.NEW_PARENT_INCOME;
				cashFlowStr = SysConstants.ReportType.PARENT_CASH_FLOW;
				newCashFlowStr = SysConstants.ReportType.NEW_PARENT_CASH_FLOW;
			}else if(type.equals("2")){
				balanceStr = SysConstants.ReportType.TAX_BALANCE;
				newBalanceStr = SysConstants.ReportType.NEW_TAX_BALANCE;
				incomeStr = SysConstants.ReportType.TAX_INCOME;
				newIncomeStr = SysConstants.ReportType.NEW_TAX_INCOME;
				cashFlowStr = SysConstants.ReportType.TAX_CASH_FLOW;
				newCashFlowStr = SysConstants.ReportType.NEW_TAX_CASH_FLOW;
			}
			String reportId = request.getParameter("reportId");
			ReportTemplet tmp = this.templetService.getReportTempletById(Long.valueOf(reportId));
			if (tmp == null) {
				return "views/Application/task3";
			}
			if ("资产负债表".equals(tmp.getName())) {
				if (!StringUtil.isEmptyString(type)) {
					ReportData balance = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(), customerNo,
							balanceStr);
					if (balance != null) {
						finanasisSV.deleteReportData(balance);
						ReportData newBalance = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(),
								customerNo, newBalanceStr);
						if (newBalance != null) {
							finanasisSV.deleteReportData(newBalance);
						}
						ReportData cashFlow = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(),
								customerNo, cashFlowStr);
						if (cashFlow != null) {
							finanasisSV.deleteReportData(cashFlow);
							ReportData newCashFlow = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(),
									customerNo, newCashFlowStr);
							if (newCashFlow != null) {
								finanasisSV.deleteReportData(newCashFlow);
							}
						}
					}
					request.getSession().removeAttribute("balanceTRs");
				}
			}
			if ("利润表".equals(tmp.getName())) {
				if (!StringUtil.isEmptyString(type)) {
					ReportData income = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(), customerNo,
							incomeStr);
					if (income != null) {
						finanasisSV.deleteReportData(income);
						ReportData newIncome = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(),
								customerNo, newIncomeStr);
						if (newIncome != null) {
							finanasisSV.deleteReportData(newIncome);
						}
						ReportData cashFlow = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(),
								customerNo, cashFlowStr);
						if (cashFlow != null) {
							finanasisSV.deleteReportData(cashFlow);
							ReportData newCashFlow = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(),
									customerNo, newCashFlowStr);
							if (newCashFlow != null) {
								finanasisSV.deleteReportData(newCashFlow);
							}
						}
					}
				}
				request.getSession().removeAttribute("incomeTRs");
			}
			String reportTr = request.getParameter("reportTr");
			if (reportTr != null) {
				ReportData reportData = new ReportData(tmp.getName());
				ReportData newReportData = new ReportData(tmp.getName());
				reportData.setTaskId(task.getId());
				reportData.setCustomerNo(customerNo);
				newReportData.setTaskId(task.getId());
				newReportData.setCustomerNo(customerNo);
				if ("资产负债表".equals(tmp.getName())) {
					reportData.setReportHtmlUrl("views/libraryManage/showTables/balance");
					newReportData.setReportHtmlUrl("views/libraryManage/showTables/balance");
					if (!StringUtil.isEmptyString(type)) {
						reportData.setReportType(balanceStr);
						newReportData.setReportType(newBalanceStr);
					}
				} else if ("利润表".equals(tmp.getName())) {
					reportData.setReportHtmlUrl("views/libraryManage/showTables/income");
					newReportData.setReportHtmlUrl("views/libraryManage/showTables/income");
					if (!StringUtil.isEmptyString(type)) {
						reportData.setReportType(incomeStr);
						newReportData.setReportType(newIncomeStr);
					}
				}
				reportData = finanasisSV.saveReportData(reportData);
				newReportData = finanasisSV.saveReportData(newReportData);

				List<ReportDataTR> reportDataTRs = new ArrayList<ReportDataTR>();
				JSONArray array = JSONArray.fromObject(reportTr);
				if (array != null && array.size() > 0) {
					for (int i = 0; i < array.size(); i++) {
						JSONObject trStr = JSONObject.fromObject(array.get(i));
						ReportDataTR tr = (ReportDataTR) JSONObject.toBean(trStr, ReportDataTR.class);
						reportDataTRs.add(tr);
					}
				}

				int index = 0;
				for (ReportDataTR reportDataTR : reportDataTRs) {
					reportDataTR.setIndex(index);
					reportDataTR.setReportDataId(reportData.getId());

					ReportDataTR newTr = new ReportDataTR();
					newTr.setIndex(index);
					newTr.setName(reportDataTR.getName());
					newTr.setFirstYearValue(reportDataTR.getFirstYearValue());
					newTr.setSecondYearValue(reportDataTR.getSecondYearValue());
					newTr.setThirdYearValue(reportDataTR.getThirdYearValue());
					newTr.setFourthYearValue(reportDataTR.getFourthYearValue());
					newTr.setLastYearTermValue(reportDataTR.getLastYearTermValue());
					newTr.setRankOrder(reportDataTR.getRankOrder());
					newTr.setReportDataId(newReportData.getId());

					finanasisSV.saveReportDataTR(reportDataTR);
					finanasisSV.saveReportDataTR(newTr);
					index++;
				}
			}
			SaveReportDataTr<ReportDataTR> saveReport = (SaveReportDataTr<ReportDataTR>) finanasisSV;
			ReportData balance = null;
			ReportData income = null;
			ReportData newBalance = null;
			ReportData newIncome = null;
			ReportData cashFlow = null;
			if (!StringUtil.isEmptyString(type)) {
				balance = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(), customerNo, balanceStr);
				income = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(), customerNo, incomeStr);
				newBalance = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(), customerNo,
						newBalanceStr);
				newIncome = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(), customerNo, newIncomeStr);
				if (balance != null && income != null) {
					ReportData cashFlowData = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(),
							customerNo, cashFlowStr);
					if (cashFlowData == null) {
						cashFlowData = new ReportData(Long.valueOf(taskId), customerNo, cashFlowStr);
						cashFlowData.setName("现金流量表");
						cashFlowData = finanasisSV.saveReportData(cashFlowData);
					}
					MakeCashFlowUtil.makeCashFlow(Long.valueOf(taskId), customerNo, balanceStr, incomeStr,
							cashFlowData.getId(), saveReport, finanasisSV);
				}
				if (newBalance != null && newIncome != null) {
					ReportData newCashFlowData = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(),
							customerNo, newCashFlowStr);
					if (newCashFlowData == null) {
						newCashFlowData = new ReportData(Long.valueOf(taskId), customerNo, newCashFlowStr);
						newCashFlowData.setName("现金流量表");
						newCashFlowData = finanasisSV.saveReportData(newCashFlowData);
					}
					MakeCashFlowUtil.makeCashFlow(Long.valueOf(taskId), customerNo, newBalanceStr, newIncomeStr,
							newCashFlowData.getId(), saveReport, finanasisSV);
				}
				cashFlow = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(), customerNo, cashFlowStr);
			}
			String industry = task.getIndustry();
			
			/*GpBorrower borrower  = groupService.findGpBorrowerByTIdAndNo(Long.valueOf(taskId), customerNo);
			if(borrower !=null && !StringUtil.isEmptyString(borrower.getTreeDataStd())){
				taskTreedataStd = borrower.getTreeDataStd();
			}*/
			TreeData industryData = industrySV.getTreeDataByEnname(industry, 
					SysConstants.TreeDicConstant.GB_IC_4754_2017);
			Map<String, IndustryQRadio> quotaMap = this.industrySV.getQuotaByIndustry(industryData);
			if (balance != null && income != null && cashFlow != null && !type.equals("2")) {
				/*if (task.getBusinessNo().equals(SysConstants.LoanTypeConstant.LOAN_CONNECT_CM)
						&& task.getCustomerNo().equals(customerNo) && !type.equals("1")) {

				} else {*/
					List<ErrData> errDatas = abnormalService.getErrorDataByTIdAndNo(Long.valueOf(task.getId()),
							customerNo);
					if (!CollectionUtils.isEmpty(errDatas)) {
						for (ErrData errData : errDatas) {
							abnormalService.deleteErrorData(errData);
						}
					}
					GetErrNameReason.getErrList(task, finanasisSV, templetService,
							abnormalService, tableSubjCodeSV, quotaMap);
					GetErrNameReason.getCheckErrList(task, finanasisSV, templetService,
							abnormalService, quotaMap);
				//}
			}
			taskSV.saveOrUpdate(task);
		} catch (Exception e) {
			e.printStackTrace();
		}
		request.setAttribute("taskId", taskId);
		request.setAttribute("type", type);
		request.setAttribute("customerNo", customerNo);
		return "views/Application/checkForm";
	}

	@RequestMapping("checkForm.do")
	public String checkForm(HttpServletRequest request, HttpServletResponse response, String repId, String id,
			String modelId, String customerNo, String type) {
		String viewUrl = "";
		Task task = taskSV.getTaskById(Long.valueOf(id));
		if (StringUtil.isEmptyString(customerNo)) {
			customerNo = task.getCustomerNo();
		}
		Long modelIdLo = Long.valueOf(modelId);
		List<ReportTemplet> reports = this.templetService.getAllReportTemplets();
		for (ReportTemplet rep : reports) {
			if (modelIdLo.equals(rep.getId()) && "资产负债表".equals(rep.getName())) {
				viewUrl = rep.getUrl();
				String typrStr = "";
				if (!StringUtil.isEmptyString(type)) {
					if (type.equals("1")) {
						typrStr = SysConstants.ReportType.PARENT_BALANCE;
					} else if(type.equals("2")){
						typrStr = SysConstants.ReportType.TAX_BALANCE;
					}else{
						typrStr = SysConstants.ReportType.BALANCE;
					}
				} else {
					typrStr = SysConstants.ReportType.BALANCE;
				}
				ReportData balance = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(), customerNo,
						typrStr);
				if (balance != null) {
					if (repId.equals(String.valueOf(balance.getId()))) {
						balance.setReportHtmlUrl("views/editTableTop/balance");
						List<ReportDataTR> trs = finanasisSV.getReportDataTrsByReportDataId(balance.getId());
						request.setAttribute("reportDataTrs", trs);
					}
				}
				break;
			} else if (modelIdLo.equals(rep.getId()) && "利润表".equals(rep.getName())) {
				viewUrl = rep.getUrl();
				String typrStr = "";
				if (!StringUtil.isEmptyString(type)) {
					if (type.equals("1")) {
						typrStr = SysConstants.ReportType.PARENT_INCOME;
					} else if(type.equals("2")){
						typrStr = SysConstants.ReportType.TAX_INCOME;
					}else {
						typrStr = SysConstants.ReportType.INCOME;
					}
				} else {
					typrStr = SysConstants.ReportType.INCOME;
				}
				ReportData income = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(), customerNo,
						typrStr);
				if (income != null) {
					if (repId.equals(String.valueOf(income.getId()))) {
						income.setReportHtmlUrl("views/editTableTop/income");
						List<ReportDataTR> trs = finanasisSV.getReportDataTrsByReportDataId(income.getId());
						request.setAttribute("reportDataTrs", trs);
					}
				}
				break;
			}
		}
		int year = task.getYear();
		int month = task.getMonth();
		request.setAttribute("year", year);
		request.setAttribute("month", month);
		request.setAttribute("customerNo", customerNo);
		request.setAttribute("type", type);
		request.setAttribute("id", id);
		request.setAttribute("modelId", modelId);
		return viewUrl;
	}

	@RequestMapping("importCheckForm.do")
	public String importCheckForm(HttpServletRequest request, HttpServletResponse response, String repId, String id) {
		String viewUrl = "";
		List<ReportTemplet> reports = this.templetService.getAllReportTemplets();
		for (ReportTemplet rep : reports) {
			if (rep.getId().equals(repId)) {
				viewUrl = rep.getUrl();
				String path = request.getServletContext().getRealPath("/file").replace("\\", "/") + "/2016.4.xls";
				File srcFile = new File(path);
				String repType = rep.getReportType();
				ReadSheel readSheel = new ReadSheel();
				String result = "";
				if ("receival".equals(repType)) {
					result = readSheel.getResult(readSheel.betterXls(srcFile, 1, "应收账款", 1, 9));// 应收账款
				} else if ("noteReceivable".equals(repType)) {
					result = readSheel.getResult(readSheel.betterXls(srcFile, 1, "应收票据", 1, 9));// 应收票据
				} else if ("prepay".equals(repType)) {
					result = readSheel.getResult(readSheel.betterXls(srcFile, 1, "预付账款", 1, 9));// 预付账款
				} else if ("otherReceiver".equals(repType)) {
					result = readSheel.getResult(readSheel.betterXls(srcFile, 1, "其他应收款", 1, 9));// 其他应收款
				} else if ("fixAsset".equals(repType)) {
					result = readSheel.getResult(readSheel.betterXls(srcFile, 1, "固定资产", 1, 9));// 固定资产
				} else if ("constructionProject".equals(repType)) {
					result = readSheel.getResult(readSheel.betterXls(srcFile, 1, "在建工程", 1, 9));// 在建工程
				} else if ("intangibleAssets".equals(repType)) {
					result = readSheel.getResult(readSheel.betterXls(srcFile, 1, "无形资产", 1, 9)); // 无形资产
				} else if ("notesPayable".equals(repType)) {
					result = readSheel.getResult(readSheel.betterXls(srcFile, 1, "应付票据", 1, 10)); // 应付票据
				} else if ("pay".equals(repType)) {
					result = readSheel.getResult(readSheel.betterXls(srcFile, 1, "应付账款", 1, 10)); // 应付账款
				} else if ("shortDebt".equals(repType)) {
					result = readSheel.getResult(readSheel.betterXls(srcFile, 1, "短期借款", 1, 10)); // 短期借款
				} else if ("otherPay".equals(repType)) {
					result = readSheel.getResult(readSheel.betterXls(srcFile, 1, "其他应付款", 1, 10)); // 其他应付款
				} else if ("longDebt".equals(repType)) {
					result = readSheel.getResult(readSheel.betterXls(srcFile, 1, "长期借款", 1, 10)); // 长期借款
				} else if ("OperatingCost".equals(repType)) {
					result = readSheel.getResult(readSheel.betterXls(srcFile, 1, "销售费用", 1, 8)); // 销售费用
				} else if ("Cost_OfManager".equals(repType)) {
					result = readSheel.getResult(readSheel.betterXls(srcFile, 1, "管理费用", 1, 8)); // 管理费用
				} else if ("FinanceCost".equals(repType)) {
					result = readSheel.getResult(readSheel.betterXls(srcFile, 1, "财务费用", 1, 8)); // 财务费用
				} else if ("MainCostOfSale".equals(repType)) {
					result = readSheel.getResult(readSheel.betterXls(srcFile, 1, "主营业务成本", 1, 8)); // 主营业务成本
				} else if ("stock".equals(repType)) {
					result += readSheel.solveStr(readSheel.xls2String(srcFile, 0, "1403", 1, 9)) + "#"; // 存货
					result += readSheel.solveStr(readSheel.xls2String(srcFile, 0, "1405", 1, 9)) + "#";
					result += readSheel.solveStr(readSheel.xls2String(srcFile, 0, "1411", 1, 9));
					List<String> precents = new ArrayList<String>();
					List<String> precentnum = new ArrayList<String>();
					double all = 0.0;
					String[] resultStr = result.split("#");
					for (int i = 0; i < (resultStr.length / 2); i++) {
						precentnum.add(resultStr[2 * i + 1]);
						all += Double.valueOf(resultStr[2 * i + 1]);
					}
					for (int i = 0; i < precentnum.size(); i++) {
						precents.add(String.format("%.2f", Double.valueOf(precentnum.get(i)) / all * 100));
					}
					request.setAttribute("precents", precents);
				}
				List<String> names = new ArrayList<String>();
				List<String> values = new ArrayList<String>();
				String[] resultStr = result.split("#");
				for (int i = 0; i < (resultStr.length / 2); i++) {
					names.add(resultStr[2 * i]);
					values.add(resultStr[2 * i + 1]);
				}
				request.setAttribute("names", names);
				request.setAttribute("values", values);
				request.setAttribute("report", rep);
			}
		}
		request.setAttribute("id", id);
		request.setAttribute("modelId", repId);
		return viewUrl;
	}

	@RequestMapping("saveCheckForm.do")
	@ResponseBody
	public String saveCheckForm(HttpServletRequest request, HttpServletResponse response) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		String dataJSON = request.getParameter("detailData");
		String id = request.getParameter("taskId");
		String repId = request.getParameter("repId");
		List<ReportTemplet> reports = this.templetService.getAllReportTemplets();
		List<ScheduleData> oldDetailList = null;
		for (ReportTemplet rep : reports) {
			if (rep.getId().equals(repId)) {
				oldDetailList = finanasisSV.getScheByTaskIdAndRepType(Long.valueOf(id), rep.getReportType());
			}
		}
		ObjectMapper mapper = new ObjectMapper();
		List<ScheduleData> detailData = new ArrayList<ScheduleData>();
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			detailData = mapper.readValue(dataJSON,
					TypeFactory.defaultInstance().constructCollectionType(ArrayList.class, ScheduleData.class));
			finanasisSV.saveScheduleData(detailData, oldDetailList);
			json.put("result", true);
		} catch (IOException e) {
			json.put("result", false);
			json.put("msg", "系统异常");
			e.printStackTrace();
		}

		return jRequest.serialize(json, true);
	}

	/**
	 * @Description (表格输入)
	 * @param request
	 * @param response
	 * @param parent
	 * @param id
	 * @return
	 */
	@RequestMapping("editTaskMenu.do")
	@ResponseBody
	public String editTaskMenu(HttpServletRequest request, HttpServletResponse response, String id) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("type", "failed");
		List<ReportTemplet> reportid = this.templetService.getAllReportTemplets();
		Task task = taskSV.getTaskById(Long.valueOf(id));
		for (ReportTemplet rep : reportid) {
			if ("资产负债表".equals(rep.getName())) {
				ReportData balance = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(),
						task.getCustomerNo(), SysConstants.ReportType.BALANCE);
				if (balance != null) {
					rep.setUrl(String.valueOf(balance.getId()));
				} else {
					rep.setUrl("");
				}
			} else if ("利润表".equals(rep.getName())) {
				ReportData income = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(),
						task.getCustomerNo(), SysConstants.ReportType.INCOME);
				if (income != null) {
					rep.setUrl(String.valueOf(income.getId()));
				} else {
					rep.setUrl("");
				}
			} else {
				rep.setUrl("");
			}
		}
		if (reportid != null) {
			json.put("checks", reportid);
		}
		return jRequest.serialize(json, true);
	}

	/**
	 * @Description (得到所有表格模板)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("queryAllReport.do")
	@ResponseBody
	public String queryAllReport(HttpServletRequest request, HttpServletResponse response) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			json.put("reportList", this.templetService.getAllReportTemplets());
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", "系统异常");
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}

	/**
	 * @Description (报表及明细表编辑)
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("editTaskForm.do")
	public String editTaskForm(HttpServletRequest request, HttpServletResponse response, String id) throws Exception {
		Task task = this.taskSV.getTaskById(Long.valueOf(id));
		String customerNo = task.getCustomerNo();
		ReportData balance = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(), task.getCustomerNo(),
				SysConstants.ReportType.BALANCE);
		ReportData income = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(), task.getCustomerNo(),
				SysConstants.ReportType.INCOME);
		if (balance != null && income != null) { // 第二次编辑
			List<ReportData> reports = new ArrayList<ReportData>();
			List<List<ReportDataTR>> reportsData = new ArrayList<List<ReportDataTR>>();

			balance.setReportHtmlUrl("views/editTableTop/balance");
			List<ReportDataTR> balanceTrs = finanasisSV.getReportDataTrsByReportDataId(balance.getId());
			reportsData.add(balanceTrs);
			reports.add(balance);

			income.setReportHtmlUrl("views/editTableTop/income");
			List<ReportDataTR> incomeTrs = finanasisSV.getReportDataTrsByReportDataId(income.getId());
			reportsData.add(incomeTrs);
			reports.add(income);

			List<ReportTemplet> reportid = this.templetService.getAllReportTemplets();
			List<ReportTemplet> reportidAct = new ArrayList<ReportTemplet>(); // 第二次及以后编辑
			for (ReportData reportData : reports) {
				for (ReportTemplet report : reportid) {
					if (reportData.getName().equals(report.getName())) {
						reportidAct.add(report);
					}
				}
			}
			// List<Report> reportid =
			// this.finanasisSV.getReportByTreeDataId(task.getTreedata().getStdId());
			int year = task.getYear();
			int month = task.getMonth();
			request.setAttribute("year", year);
			request.setAttribute("month", month);
			request.setAttribute("id", task.getId());
			request.setAttribute("havereports", reports);
			request.setAttribute("reportids", reportidAct);
			request.setAttribute("reportsData", reportsData);
		} else {
			task.setState(0);
			UserSession us = UserSession.getUserSession(request); // 第一次编辑
			User manager = userSV.queryById(us.getLoggedUserId());
			task.setUserNo(manager.getUserId());
			this.taskSV.saveTaskInfo(task, "1");
			us.setTaskId(String.valueOf(task.getId()));
			List<ReportTemplet> reports = this.templetService.getAllReportTemplets();
			// List<Report> reports =
			// this.finanasisSV.getReportByTreeDataId(task.getTreedata().getStdId());
			request.setAttribute("id", task.getId());
			request.setAttribute("reports", reports);
			int year = task.getYear();
			int month = task.getMonth();
			request.setAttribute("year", year);
			request.setAttribute("month", month);
		}
		return "views/task/form";
	}

}
