package com.beawan.analysis.finansis.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.beawan.analysis.finansis.utils.AssetProjData;
import com.beawan.analysis.finansis.utils.EastProjData;
import com.beawan.common.annotation.UserSessionAnnotation;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/proj" })
public class ProjCtl {
	
	/**
	 * @Description (房地产分析 项目总投资分析页面跳转)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("estateTotal.do")
	public String estateTotal(HttpServletRequest request, HttpServletResponse response) {
		try {
			EastProjData data = new EastProjData();

			List<String[]> totalCost = data.projTotalInve();
			request.setAttribute("totalCost", totalCost);

			List<String[]> landCost = data.landCost();
			request.setAttribute("landCost", landCost);

			List<String[]> frontCost = data.frontCost();
			request.setAttribute("frontCost", frontCost);

			List<String[]> infrastructureCost = data.infrastructureCost();
			request.setAttribute("infrastructureCost", infrastructureCost);

			List<String[]> installCost = data.installCost();
			request.setAttribute("installCost", installCost);

			List<String[]> publicCost = data.publicCost();
			request.setAttribute("publicCost", publicCost);

			List<String[]> taxCost = data.taxCost();
			request.setAttribute("taxCost", taxCost);

			List<String[]> otherCost = data.otherCost();
			request.setAttribute("otherCost", otherCost);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "views/Analysis/finansis/estate/totalCost";
	}

	/**
	 * @Description (房地产分析 项目总投资分析页面跳转)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("inveFinanc.do")
	public String inveFinanc(HttpServletRequest request, HttpServletResponse response) {
		try {
			EastProjData data = new EastProjData();

			List<String[]> inveFinanc = data.inveFinanc();
			request.setAttribute("inveFinanc", inveFinanc);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "views/Analysis/finansis/estate/inveFinanc";
	}

	/**
	 * @Description (开发产品成本归集页面跳转)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("developCost.do")
	public String developCost(HttpServletRequest request, HttpServletResponse response) {
		try {
			EastProjData data = new EastProjData();

			List<String[]> developCost = data.developCost();
			request.setAttribute("developCost", developCost);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "views/Analysis/finansis/estate/developCost";
	}

	/**
	 * @Description (开发产品成本分摊页面跳转)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("assignCost.do")
	public String assignCost(HttpServletRequest request, HttpServletResponse response) {
		try {
			EastProjData data = new EastProjData();

			List<String[]> assignCost = data.assignCost();
			request.setAttribute("assignCost", assignCost);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "views/Analysis/finansis/estate/assignCost";
	}

	/**
	 * @Description (开发产品成本分摊页面跳转)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("incomeTax.do")
	public String incomeTax(HttpServletRequest request, HttpServletResponse response) {
		try {
			EastProjData data = new EastProjData();

			List<String[]> incomeTax = data.incomeTax();
			request.setAttribute("incomeTax", incomeTax);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "views/Analysis/finansis/estate/incomeTax";
	}

	/**
	 * @Description (开发产品成本分摊页面跳转)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("profit.do")
	public String profit(HttpServletRequest request, HttpServletResponse response) {
		try {
			EastProjData data = new EastProjData();

			List<String[]> profit = data.profit();
			request.setAttribute("profit", profit);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "views/Analysis/finansis/estate/profit";
	}

	/**
	 * @Description (项目现金流页面跳转)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("estateCashFlow.do")
	public String estateCashFlow(HttpServletRequest request, HttpServletResponse response) {
		try {
			EastProjData data = new EastProjData();

			List<String[]> cashFlow = data.cashFlow();
			request.setAttribute("cashFlow", cashFlow);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "views/Analysis/finansis/estate/estateCashFlow";
	}

	/**
	 * @Description (项目项目贷款偿还期跳转)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("payProjPlan.do")
	public String payProjPlan(HttpServletRequest request, HttpServletResponse response) {
		try {
			EastProjData data = new EastProjData();

			List<String[]> payProjPlan = data.payProjPlan();
			request.setAttribute("payProjPlan", payProjPlan);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "views/Analysis/finansis/estate/payProjPlan";
	}

	/**
	 * @Description (项目项目贷款偿还期跳转)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("payGerenalPlan.do")
	public String payGerenalPlan(HttpServletRequest request, HttpServletResponse response) {
		try {
			EastProjData data = new EastProjData();

			List<String[]> payGerenalPlan = data.payGerenalPlan();
			request.setAttribute("payGerenalPlan", payGerenalPlan);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "views/Analysis/finansis/estate/payGerenalPlan";
	}

	/**
	 * @Description (项目项目贷款偿还期跳转)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("sensibility.do")
	public String sensibility(HttpServletRequest request, HttpServletResponse response) {
		try {
			EastProjData data = new EastProjData();

			List<String[]> sensibility = data.sensibility();
			request.setAttribute("sensibility", sensibility);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "views/Analysis/finansis/estate/sensibility";
	}
	
	
	/**
	 * @Description (固定资产投资估算跳转)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("assetTotal.do")
	public String assetTotal(HttpServletRequest request, HttpServletResponse response) {
		try {
			AssetProjData data = new AssetProjData();

			List<String[]> projTotalInve = data.projTotalInve();
			request.setAttribute("projTotalInve",projTotalInve);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "views/Analysis/finansis/asset/projTotalInve";
	}
	
	/**
	 * @Description (固定资产投资资产分类跳转)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("assetCateInve.do")
	public String assetCateInve(HttpServletRequest request, HttpServletResponse response) {
		try {
			AssetProjData data = new AssetProjData();

			List<String[]> assetCateInve = data.assetCateInve();
			request.setAttribute("assetCateInve",assetCateInve);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "views/Analysis/finansis/asset/assetCateInve";
	}
	
	
	/**
	 * @Description (固定资产投资计划与资金筹措跳转)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("assetInvePlan.do")
	public String assetInvePlan(HttpServletRequest request, HttpServletResponse response) {
		try {
			AssetProjData data = new AssetProjData();

			List<String[]> assetInvePlan = data.invePlan();
			request.setAttribute("assetInvePlan",assetInvePlan);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "views/Analysis/finansis/asset/assetInvePlan";
	}
	
	/**
	 * @Description (固定资产总成本费用估算跳转)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("aasetCostReckon.do")
	public String aasetCostReckon(HttpServletRequest request, HttpServletResponse response) {
		try {
			AssetProjData data = new AssetProjData();

			List<String[]> aasetCostReckon = data.costReckon();
			request.setAttribute("aasetCostReckon",aasetCostReckon);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "views/Analysis/finansis/asset/aasetCostReckon";
	}
	
	/**
	 * @Description (固定资产总成本费用估算跳转)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("assetIncomeProfit.do")
	public String assetIncomeProfit(HttpServletRequest request, HttpServletResponse response) {
		try {
			AssetProjData data = new AssetProjData();

			List<String[]> assetIncomeProfit = data.incomeProfit();
			request.setAttribute("assetIncomeProfit",assetIncomeProfit);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "views/Analysis/finansis/asset/assetIncomeProfit";
	}
	
	/**
	 * @Description (固定资产贷款偿还期计算跳转)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("assetProjPay.do")
	public String assetProjPay(HttpServletRequest request, HttpServletResponse response) {
		try {
			AssetProjData data = new AssetProjData();

			List<String[]> assetProjPay = data.projPay();
			request.setAttribute("assetProjPay",assetProjPay);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "views/Analysis/finansis/asset/assetProjPay";
	}

	/**
	 * @Description (固定资产贷款偿还期计算跳转)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("assetCashFlow.do")
	public String assetCashFlow(HttpServletRequest request, HttpServletResponse response) {
		try {
			AssetProjData data = new AssetProjData();

			List<String[]> assetCashFlow = data.projCashFlow();
			request.setAttribute("assetCashFlow",assetCashFlow);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "views/Analysis/finansis/asset/assetCashFlow";
	}
	
	
}
