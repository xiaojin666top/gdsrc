package com.beawan.analysis.finansis.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.beawan.survey.custInfo.dto.CompBaseDto;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.Interface.GetErrTemp;
import com.beawan.analysis.abnormal.bean.ErrData;
import com.beawan.analysis.abnormal.service.IAbnormalService;
import com.beawan.analysis.balance.balanceAssetTrendData;
import com.beawan.analysis.balance.balanceDebtTrendData;
import com.beawan.analysis.balance.balanceOwnerRatio;
import com.beawan.analysis.balance.balanceOwnerStruct;
import com.beawan.analysis.finansis.bean.ReportData;
import com.beawan.analysis.finansis.bean.ReportDataTR;
import com.beawan.analysis.finansis.service.IFinanasisService;
import com.beawan.analysis.finansis.utils.ControllerJSONUtil;
import com.beawan.analysis.finansis.utils.SingleAnaly;
import com.beawan.analysis.finansis.utils.ThreeRpoetFormAnalysisUtil;
import com.beawan.analysis.finansis.utils.CodeUtil;
import com.beawan.analysis.income.IncomeStruct;
import com.beawan.analysis.report.CommonResult;
import com.beawan.analysis.report.RatioCommon;
import com.beawan.base.service.ISysDicSV;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.library.bean.ErrTemplet;
import com.beawan.library.bean.IndustryAnalysis;
import com.beawan.library.bean.IndustryQRadio;
import com.beawan.library.bean.TreeData;
import com.beawan.library.service.IIndustrySV;
import com.beawan.library.service.ITempletSV;
import com.beawan.survey.custInfo.bean.CompBase;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.beawan.survey.custInfo.service.ICompFinanceSV;
import com.beawan.survey.loanInfo.bean.Guarantor;
import com.beawan.survey.loanInfo.service.IGuaraInfoSV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.beawan.utils.LiquidityloanCalcu;
import com.beawan.utils.MapUtil;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.beawan.web.impl.UserSession;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.platform.util.HttpUtil;
import com.platform.util.PropertiesUtil;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/finansis" })
public class FinansisCtl {

	private static final Logger log = Logger.getLogger(FinansisCtl.class);

	@Resource
	private ITaskSV taskSV;
	@Resource
	private ITempletSV templetSV;
	@Resource
	private IIndustrySV industrySV;
	@Resource
	private ICompBaseSV compBaseSV;
	@Resource
	private ISysDicSV sysDicSV;
	@Resource
	private ICompFinanceSV compFinanceSV;
	@Resource
	private IGuaraInfoSV guaraInfoSV;
	
	@Resource
	private IFinanasisService finanasisSV;
	@Resource
	private IAbnormalService abnormalService;

	/**
	 * @Description 任务列表选择进去后 左边菜单栏 跳转
	 * @param request
	 * @param response
	 * @param index
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("analy.do")
	public String analy(HttpServletRequest request, HttpServletResponse response, int index) {
		
		try {
			
			long taskId = HttpUtil.getAsLong(request, "taskId");
			String customerNo = HttpUtil.getAsString(request, "customerNo");
			String cusFlag = HttpUtil.getAsString(request, "cusFlag");
			
			request.setAttribute("taskId", taskId);
			request.setAttribute("customerNo", customerNo);
			request.setAttribute("cusFlag", cusFlag);
			
			Map<String,Object> finaRepInfo = compFinanceSV.getFinaRepInfo(taskId, customerNo);
			
			int finaRepYear = (Integer) finaRepInfo.get("finaRepYear");
			int finaRepMonth = (Integer) finaRepInfo.get("finaRepMonth");
			String industry = (String) finaRepInfo.get("industry");
			
			@SuppressWarnings("unchecked")
			GetErrTemp<ErrTemplet> getErrTemplet = (GetErrTemp<ErrTemplet>) templetSV;

			List<Map<String, Double>> balanceData = finanasisSV.queryBalanceSheet(customerNo,
					finaRepYear, finaRepMonth);
			List<Map<String, Double>> incomeData = finanasisSV.queryIncomeSheet(customerNo,
					finaRepYear, finaRepMonth);
			List<Map<String, Double>> cashFlowData = finanasisSV.queryCashFlowSheet(customerNo,
					finaRepYear, finaRepMonth);

			HashMap<String, Double> balance1 = (HashMap<String, Double>) balanceData.get(0);
			HashMap<String, Double> balance2 = (HashMap<String, Double>) balanceData.get(1);
			HashMap<String, Double> balance3 = (HashMap<String, Double>) balanceData.get(2);
			HashMap<String, Double> balance4 = (HashMap<String, Double>) balanceData.get(3);
			HashMap<String, Double> balance5 = (HashMap<String, Double>) balanceData.get(4);

			HashMap<String, Double> income1 = (HashMap<String, Double>) incomeData.get(0);
			HashMap<String, Double> income2 = (HashMap<String, Double>) incomeData.get(1);
			HashMap<String, Double> income3 = (HashMap<String, Double>) incomeData.get(2);
			HashMap<String, Double> income4 = (HashMap<String, Double>) incomeData.get(3);
			HashMap<String, Double> income5 = (HashMap<String, Double>) incomeData.get(4);
			
			HashMap<String, Double> cashFlow3 = (HashMap<String, Double>) cashFlowData.get(2);
			HashMap<String, Double> cashFlow4 = (HashMap<String, Double>) cashFlowData.get(3);

			TreeData taskTreeData = industrySV.getTreeDataByEnname(industry,
					SysConstants.TreeDicConstant.GB_IC_4754_2017);
			
			Map<String, IndustryQRadio> quotaMap = industrySV.getQuotaByIndustry(taskTreeData);
			IndustryAnalysis taskIndestry = industrySV.getIndustryAnalysisByTaskTreeData(taskTreeData);

			if (0 == index) {
				return "views/Analysis/finansis/cashFlow/cashFlowPandectTwo"; // 平板端8层现金流分析
			} else if (3 == index) {
				// 比率分析
				return "views/Analysis/finansis/ratio";
			} else if (88 == index) {
				// 比率分析
				return "views/Analysis/finansis/balance";
			}  else if (6 == index) {
				List<ErrData> errlists = abnormalService.getErrorDataByTaskId(taskId);
				List<ErrData> errMust = new ArrayList<ErrData>();
				List<ErrData> errPro = new ArrayList<ErrData>();
				List<ErrData> ratioUpDown = new ArrayList<ErrData>();
				List<ErrData> errAbnors = new ArrayList<ErrData>();
				List<ErrData> commonAbnors = new ArrayList<ErrData>();
				if(errlists != null){
					for (ErrData errdata : errlists) {
						if (errdata.getCategoryFlag() == 1) {
							errMust.add(errdata);
						} else if (errdata.getCategoryFlag() == 2 || errdata.getCategoryFlag() == 3) {
							errPro.add(errdata);
						} else if (errdata.getCategoryFlag() == 4) {
							errAbnors.add(errdata);
						} else if (errdata.getCategoryFlag() == 5) {
							ratioUpDown.add(errdata);
						} else if (errdata.getCategoryFlag() == 6) {
							commonAbnors.add(errdata);
						}
					}
				}
				
				request.setAttribute("errMusts", errMust); // 必查项
				request.setAttribute("errPros", errPro); // 占比过高
				request.setAttribute("errAbnors", errAbnors); // 异常
				request.setAttribute("ratioUpDown", ratioUpDown); // 比率异常
				request.setAttribute("commonAbnors", commonAbnors); // 比率异常

				return "views/Analysis/abnormal/errNew";
				
			} else if (10 == index) {//资产总览
				
				request.setAttribute("year", finaRepYear);
				request.setAttribute("trendStr", SingleAnaly.balanceAssetTrend(finaRepYear,
						balanceAssetTrendData.getBalanceAssetTrendData(balance1, balance2, balance3, balance4, 0.1)));
				request.setAttribute("structStr", CommonResult
						.getAssetProporHighData(finaRepYear, balance3, balance4, income3, income4, taskIndestry)
						.get(0));
				request.setAttribute("ratioStr", CommonResult.getRatioThreeData(finaRepYear, finaRepMonth,
						balance3, balance4, income3, income4, quotaMap));
				
				return "views/Analysis/finansis/balance/balanceAssets";
				
			} else if (11 == index) {//负债总览
				
				request.setAttribute("year", finaRepYear);
				request.setAttribute("trendStr", SingleAnaly.balanceDebtTrend(finaRepYear,
						balanceDebtTrendData.getBalanceDebtTrendData(balance1, balance2, balance3, balance4, 0.1)));
				request.setAttribute("structStr", CommonResult
						.getAssetProporHighData(finaRepYear, balance3, balance4, income3, income4, taskIndestry)
						.get(1));
				request.setAttribute("ratioStr",
						CommonResult.getRatioTwoData(finaRepYear, balance3, balance4, income3, income4, quotaMap));
				
				return "views/Analysis/finansis/balance/balanceDebt";
				
			} else if (12 == index) {//所有者权益总览
				
				request.setAttribute("year", finaRepYear);
				IndustryQRadio tatio1 = quotaMap.get(SysConstants.FinanceQuota.ZZCSY_RATE);
				double avg1 = tatio1.getAverageValue();
				IndustryQRadio tatio2 = quotaMap.get(SysConstants.FinanceQuota.ZBJL_RATE);
				double avg2 = tatio2.getAverageValue();
				request.setAttribute("AssetThree", CommonResult.getNetAssetData(finaRepYear, balance3, balance4));
				request.setAttribute("structStr", SingleAnaly.balanceNetAssetStruct(finaRepYear, balanceOwnerStruct
						.getBalanceOwnerStructTableData(balance1, balance2, balance3, balance4, 0.1)));
				request.setAttribute("ratioStr",
						SingleAnaly.balanceNetAssetRatio(finaRepYear,
								balanceOwnerRatio.getBalanceOwnerRatioData(finaRepMonth, balance1, balance2,
										balance3, balance4, income2, income3, income4, avg1, avg2),
								avg1));
				return "views/Analysis/finansis/balance/balanceOwner";
				
			} else if (21 == index) {//利润总览
				
				request.setAttribute("year", finaRepYear);
				request.setAttribute("Incsecond", CommonResult.getIncomeAnalysis(finaRepYear, finaRepMonth,
						balance2, balance3, balance4, balance5, income2, income3, income4, income5));
				request.setAttribute("IncsecondThree",
						CommonResult.getIncomeInTwoThreeData(finaRepYear, income3, income4, quotaMap));
				request.setAttribute("IncThird", CommonResult.getIncomethreeData(getErrTemplet, finaRepYear,
						finaRepMonth, income3, income4));
				request.setAttribute("IncFour", CommonResult.getIncomeFourData(getErrTemplet, finaRepYear,
						finaRepMonth, income3, income4));
				request.setAttribute("IncFive", CommonResult.getIncomeFiveData(getErrTemplet, finaRepYear,
						finaRepMonth, balance3, income3));
				request.setAttribute("IncSix",
						CommonResult.getIncomeSixData(getErrTemplet, finaRepYear, finaRepMonth, income4));
				request.setAttribute("struct",
						SingleAnaly.incomeStruct(IncomeStruct.getIncomeStructData(income1, income2, income3, income4)));
				request.setAttribute("ratioStr", CommonResult.getRatioOneData(finaRepYear, finaRepMonth, balance3,
						balance4, income3, income4, quotaMap));
				return "views/Analysis/finansis/income/incomePandect";
				
			} else if (31 == index) {//现金流量总览
				
				request.setAttribute("year", finaRepYear);
				request.setAttribute("commonStr", CommonResult.getCashFlowThirdStringData(finaRepYear, balance3,
						balance4, income4, cashFlow3, cashFlow4));
				return "views/Analysis/finansis/cashFlow/cashFlowPandect";
				
			} else if (32 == index) {//流动资金贷款预测算

				List<List<String>> calcu = LiquidityloanCalcu.getLiquidityLoanCalcu(finaRepMonth, balance3, balance4,
						income2, income3, income4);
				request.setAttribute("year", finaRepYear);
				request.setAttribute("month", finaRepMonth);
				request.setAttribute("resultLists", calcu);
				return "views/Analysis/finansis/cashFlow/liquidityloanCalcu";
			} 
			
			request.setAttribute("message", "请求页面不存在");
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error("跳转分析页面异常！", e);
		}
		
		return "views/Application/tip";
	}


	/**
	 * @Description (检查报表正确完整性)
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("balance.json")
	@ResponseBody
	public String balanceJSON(HttpServletRequest request, HttpServletResponse response) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		
		try {
			
			long taskId = HttpUtil.getAsLong(request, "taskId");
			String customerNo = HttpUtil.getAsString(request, "customerNo");
			
			Map<String,Object> finaRepInfo = compFinanceSV.getFinaRepInfo(taskId, customerNo);
			
			int finaRepYear = (Integer) finaRepInfo.get("finaRepYear");
			int finaRepMonth = (Integer) finaRepInfo.get("finaRepMonth");

			List<Map<String, Double>> balanceData = finanasisSV.queryBalanceSheet(customerNo,
					finaRepYear, finaRepMonth);
			List<Map<String, Double>> incomeData = finanasisSV.queryIncomeSheet(customerNo,
					finaRepYear, finaRepMonth);
			List<Map<String, Double>> cashFlowData = finanasisSV.queryCashFlowSheet(customerNo,
					finaRepYear, finaRepMonth);

			HashMap<String, Double> balance1 = (HashMap<String, Double>) balanceData.get(0);
			HashMap<String, Double> balance2 = (HashMap<String, Double>) balanceData.get(1);
			HashMap<String, Double> balance3 = (HashMap<String, Double>) balanceData.get(2);
			HashMap<String, Double> balance4 = (HashMap<String, Double>) balanceData.get(3);

			HashMap<String, Double> income1 = (HashMap<String, Double>) incomeData.get(0);
			HashMap<String, Double> income2 = (HashMap<String, Double>) incomeData.get(1);
			HashMap<String, Double> income3 = (HashMap<String, Double>) incomeData.get(2);
			HashMap<String, Double> income4 = (HashMap<String, Double>) incomeData.get(3);

			HashMap<String, Double> cashFlow1 = (HashMap<String, Double>) cashFlowData.get(0);
			HashMap<String, Double> cashFlow2 = (HashMap<String, Double>) cashFlowData.get(1);
			HashMap<String, Double> cashFlow3 = (HashMap<String, Double>) cashFlowData.get(2);
			HashMap<String, Double> cashFlow4 = (HashMap<String, Double>) cashFlowData.get(3);

			hashMap.put("json1", ControllerJSONUtil.getBalanceJSON(balance1, income1, cashFlow1));
			hashMap.put("json2", ControllerJSONUtil.getBalanceJSON(balance2, income2, cashFlow2));
			hashMap.put("json3", ControllerJSONUtil.getBalanceJSON(balance3, income3, cashFlow3));
			hashMap.put("json4", ControllerJSONUtil.getBalanceJSON(balance4, income4, cashFlow4));

			hashMap.put("year", finaRepYear);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("检查报表正确完整性异常！", e);
		}

		return jRequest.serialize(hashMap);
	}

	/**
	 * @Description (资产负债表分析中的资产分析)
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("balanceAssets.json")
	@ResponseBody
	public String balanceAssetsJSON(HttpServletRequest request, HttpServletResponse response) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> hashMap = new HashMap<String, Object>();
		
		try {
			
			long taskId = HttpUtil.getAsLong(request, "taskId");
			String customerNo = HttpUtil.getAsString(request, "customerNo");
			
			Map<String,Object> finaRepInfo = compFinanceSV.getFinaRepInfo(taskId, customerNo);
			
			int finaRepYear = (Integer) finaRepInfo.get("finaRepYear");
			int finaRepMonth = (Integer) finaRepInfo.get("finaRepMonth");
			String industry = (String) finaRepInfo.get("industry");

			List<Map<String, Double>> balanceData = finanasisSV.queryBalanceSheet(customerNo,
					finaRepYear, finaRepMonth);
			List<Map<String, Double>> incomeData = finanasisSV.queryIncomeSheet(customerNo,
					finaRepYear, finaRepMonth);

			HashMap<String, Double> balance1 = (HashMap<String, Double>) balanceData.get(0);
			HashMap<String, Double> balance2 = (HashMap<String, Double>) balanceData.get(1);
			HashMap<String, Double> balance3 = (HashMap<String, Double>) balanceData.get(2);
			HashMap<String, Double> balance4 = (HashMap<String, Double>) balanceData.get(3);

			HashMap<String, Double> income2 = (HashMap<String, Double>) incomeData.get(1);
			HashMap<String, Double> income3 = (HashMap<String, Double>) incomeData.get(2);
			HashMap<String, Double> income4 = (HashMap<String, Double>) incomeData.get(3);

			TreeData taskTreeData = industrySV.getTreeDataByEnname(industry,
					SysConstants.TreeDicConstant.GB_IC_4754_2017);

			Map<String, IndustryQRadio> quotaMap = this.industrySV.getQuotaByIndustry(taskTreeData);

			IndustryQRadio tatio1 = quotaMap.get(SysConstants.FinanceQuota.CHZZ_COUNT);
			double avg1 = tatio1.getAverageValue();
			IndustryQRadio tatio2 = quotaMap.get(SysConstants.FinanceQuota.LDZCZZ_RATE);
			double avg2 = tatio2.getAverageValue();
			IndustryQRadio tatio3 = quotaMap.get(SysConstants.FinanceQuota.ZZZZZ_RATE);
			double avg3 = tatio3.getAverageValue();
			IndustryQRadio tatio4 = quotaMap.get(SysConstants.FinanceQuota.YSZKZZ_RATE);
			double avg4 = tatio4.getAverageValue();

			hashMap.put("json1",
					ThreeRpoetFormAnalysisUtil.getBalanceAssetTrendData(balance1, balance2, balance3, balance4, 0.05));
			hashMap.put("json2", ThreeRpoetFormAnalysisUtil.getBalanceAssetRatioData(finaRepMonth, balance1,
					balance2, balance3, balance4, income2, income3, income4, avg1, avg2, avg3, avg4));
			hashMap.put("json3",
					ThreeRpoetFormAnalysisUtil.getBalanceAssetStructData(balance1, balance2, balance3, balance4, 0.05));
			hashMap.put("json4", ThreeRpoetFormAnalysisUtil.getBalanceAssetStructTableData(balance1, balance2, balance3,
					balance4, 0.05));
			hashMap.put("year", finaRepYear);

			hashMap.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("资产负债表分析中的资产分析异常！", e);
		}

		return jRequest.serialize(hashMap);
	}

	/**
	 * @Description (资产负债表分析中的 负债分析)
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("balanceDebt.json")
	@ResponseBody
	public String balanceDebtJSON(HttpServletRequest request, HttpServletResponse response) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		
		try {
			
			long taskId = HttpUtil.getAsLong(request, "taskId");
			String customerNo = HttpUtil.getAsString(request, "customerNo");
			
			Map<String,Object> finaRepInfo = compFinanceSV.getFinaRepInfo(taskId, customerNo);
			
			int finaRepYear = (Integer) finaRepInfo.get("finaRepYear");
			int finaRepMonth = (Integer) finaRepInfo.get("finaRepMonth");
			String industry = (String) finaRepInfo.get("industry");

			List<Map<String, Double>> balanceData = finanasisSV.queryBalanceSheet(customerNo,
					finaRepYear, finaRepMonth);
			List<Map<String, Double>> incomeData = finanasisSV.queryIncomeSheet(customerNo, 
					finaRepYear, finaRepMonth);
			List<Map<String, Double>> cashFlowData = finanasisSV.queryCashFlowSheet(customerNo,
					finaRepYear, finaRepMonth);

			HashMap<String, Double> balance1 = (HashMap<String, Double>) balanceData.get(0);
			HashMap<String, Double> balance2 = (HashMap<String, Double>) balanceData.get(1);
			HashMap<String, Double> balance3 = (HashMap<String, Double>) balanceData.get(2);
			HashMap<String, Double> balance4 = (HashMap<String, Double>) balanceData.get(3);

			HashMap<String, Double> income2 = (HashMap<String, Double>) incomeData.get(1);
			HashMap<String, Double> income3 = (HashMap<String, Double>) incomeData.get(2);
			HashMap<String, Double> income4 = (HashMap<String, Double>) incomeData.get(3);

			HashMap<String, Double> cashFlow2 = (HashMap<String, Double>) cashFlowData.get(1);
			HashMap<String, Double> cashFlow3 = (HashMap<String, Double>) cashFlowData.get(2);
			HashMap<String, Double> cashFlow4 = (HashMap<String, Double>) cashFlowData.get(3);

			TreeData taskTreeData = industrySV.getTreeDataByEnname(industry,
					SysConstants.TreeDicConstant.GB_IC_4754_2017);

			Map<String, IndustryQRadio> quotaMap = this.industrySV.getQuotaByIndustry(taskTreeData);

			IndustryQRadio tatio1 = quotaMap.get(SysConstants.FinanceQuota.ZCFZ_RATE);
			double avg1 = tatio1.getAverageValue();
			IndustryQRadio tatio2 = quotaMap.get(SysConstants.FinanceQuota.YHLX_COUNT);
			double avg2 = tatio2.getAverageValue();
			IndustryQRadio tatio3 = quotaMap.get(SysConstants.FinanceQuota.SD_RATE);
			double avg3 = tatio3.getAverageValue();
			IndustryQRadio tatio4 = quotaMap.get(SysConstants.FinanceQuota.XJLDFZ_RATE);
			double avg4 = tatio4.getAverageValue();

			hashMap.put("json1",
					ThreeRpoetFormAnalysisUtil.getBalanceDebtTrendData(balance1, balance2, balance3, balance4, 0.05));
			hashMap.put("json2", ThreeRpoetFormAnalysisUtil.getBalanceDebtRatioData(balance1, balance2, balance3,
					balance4, income2, income3, income4, cashFlow2, cashFlow3, cashFlow4, avg1, avg2, avg3, avg4));
			hashMap.put("json3",
					ThreeRpoetFormAnalysisUtil.getBalanceDebtStructData(balance1, balance2, balance3, balance4, 0.05));
			hashMap.put("json4", ThreeRpoetFormAnalysisUtil.getBalanceDebtStructTableData(balance1, balance2, balance3,
					balance4, 0.05));
			hashMap.put("year", finaRepYear);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("资产负债表分析中的负债分析异常！", e);
		}

		return jRequest.serialize(hashMap);
	}

	@RequestMapping("balanceOwner.json")
	@ResponseBody
	public String balanceOwnerJSON(HttpServletRequest request, HttpServletResponse response) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		
		try {
			
			long taskId = HttpUtil.getAsLong(request, "taskId");
			String customerNo = HttpUtil.getAsString(request, "customerNo");
			
			Map<String,Object> finaRepInfo = compFinanceSV.getFinaRepInfo(taskId, customerNo);
			
			int finaRepYear = (Integer) finaRepInfo.get("finaRepYear");
			int finaRepMonth = (Integer) finaRepInfo.get("finaRepMonth");
			String industry = (String) finaRepInfo.get("industry");

			List<Map<String, Double>> balanceData = finanasisSV.queryBalanceSheet(customerNo,
					finaRepYear, finaRepMonth);
			List<Map<String, Double>> incomeData = finanasisSV.queryIncomeSheet(customerNo,
					finaRepYear, finaRepMonth);

			HashMap<String, Double> balance1 = (HashMap<String, Double>) balanceData.get(0);
			HashMap<String, Double> balance2 = (HashMap<String, Double>) balanceData.get(1);
			HashMap<String, Double> balance3 = (HashMap<String, Double>) balanceData.get(2);
			HashMap<String, Double> balance4 = (HashMap<String, Double>) balanceData.get(3);

			HashMap<String, Double> income2 = (HashMap<String, Double>) incomeData.get(1);
			HashMap<String, Double> income3 = (HashMap<String, Double>) incomeData.get(2);
			HashMap<String, Double> income4 = (HashMap<String, Double>) incomeData.get(3);

			TreeData taskTreeData = industrySV.getTreeDataByEnname(industry,
					SysConstants.TreeDicConstant.GB_IC_4754_2017);

			Map<String, IndustryQRadio> quotaMap = this.industrySV.getQuotaByIndustry(taskTreeData);

			IndustryQRadio tatio1 = quotaMap.get(SysConstants.FinanceQuota.ZZCSY_RATE);
			double avg1 = tatio1.getAverageValue();
			IndustryQRadio tatio2 = quotaMap.get(SysConstants.FinanceQuota.ZBJL_RATE);
			double avg2 = tatio2.getAverageValue();

			hashMap.put("json1",
					ThreeRpoetFormAnalysisUtil.getBalanceOwnerTrendData(balance1, balance2, balance3, balance4, 0.05));
			hashMap.put("json2", ThreeRpoetFormAnalysisUtil.getBalanceOwnerRatioData(finaRepMonth, balance1,
					balance2, balance3, balance4, income2, income3, income4, avg1, avg2));
			hashMap.put("json3",
					ThreeRpoetFormAnalysisUtil.getBalanceOwnerStructData(balance1, balance2, balance3, balance4, 0.05));
			hashMap.put("json4", ThreeRpoetFormAnalysisUtil.getBalanceOwnerStructTableData(balance1, balance2, balance3,
					balance4, 0.05));
			hashMap.put("year", finaRepYear);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("资产负债表分析中的所有者权益异常", e);
		}
		return jRequest.serialize(hashMap);
	}

	/**
	 * @Description (利润表分析)
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("incomeSummary.json")
	@ResponseBody
	public String incomeSummaryJSON(HttpServletRequest request, HttpServletResponse response) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		
		try {
			
			long taskId = HttpUtil.getAsLong(request, "taskId");
			String customerNo = HttpUtil.getAsString(request, "customerNo");
			
			Map<String,Object> finaRepInfo = compFinanceSV.getFinaRepInfo(taskId, customerNo);
			
			int finaRepYear = (Integer) finaRepInfo.get("finaRepYear");
			int finaRepMonth = (Integer) finaRepInfo.get("finaRepMonth");
			String industry = (String) finaRepInfo.get("industry");

			List<Map<String, Double>> balanceData = finanasisSV.queryBalanceSheet(customerNo,
					finaRepYear, finaRepMonth);
			List<Map<String, Double>> incomeData = finanasisSV.queryIncomeSheet(customerNo,
					finaRepYear, finaRepMonth);

			HashMap<String, Double> balance1 = (HashMap<String, Double>) balanceData.get(0);
			HashMap<String, Double> balance2 = (HashMap<String, Double>) balanceData.get(1);
			HashMap<String, Double> balance3 = (HashMap<String, Double>) balanceData.get(2);
			HashMap<String, Double> balance4 = (HashMap<String, Double>) balanceData.get(3);

			HashMap<String, Double> income1 = (HashMap<String, Double>) incomeData.get(0);
			HashMap<String, Double> income2 = (HashMap<String, Double>) incomeData.get(1);
			HashMap<String, Double> income3 = (HashMap<String, Double>) incomeData.get(2);
			HashMap<String, Double> income4 = (HashMap<String, Double>) incomeData.get(3);

			TreeData taskTreeData = industrySV.getTreeDataByEnname(industry,
					SysConstants.TreeDicConstant.GB_IC_4754_2017);
			Map<String, IndustryQRadio> quotaMap = this.industrySV.getQuotaByIndustry(taskTreeData);
			IndustryQRadio tatio1 = quotaMap.get(SysConstants.FinanceQuota.ZYYWLR_RATE);
			double mip = tatio1.getAverageValue();
			IndustryQRadio tatio2 = quotaMap.get(SysConstants.FinanceQuota.ZZCBC_RATE);
			double rot = tatio2.getAverageValue();
			IndustryQRadio tatio3 = quotaMap.get(SysConstants.FinanceQuota.CBFYLR_RATE);
			double rop = tatio3.getAverageValue();
			IndustryQRadio tatio4 = quotaMap.get(SysConstants.FinanceQuota.XSZZ_RATE);
			double igr = tatio4.getAverageValue();
			IndustryQRadio tatio5 = quotaMap.get(SysConstants.FinanceQuota.XSLRZZ_RATE);
			double pgr = tatio5.getAverageValue();

			hashMap.put("name", ThreeRpoetFormAnalysisUtil.getTrendIncomeItemNameData());

			hashMap.put("json1", ThreeRpoetFormAnalysisUtil.getTrendIncomeDataOne(income1, income2, income3,income4));
			hashMap.put("json2", ThreeRpoetFormAnalysisUtil.getTrendIncoemJSONTwo(income1, income2, income3,income4));
			hashMap.put("json3", ThreeRpoetFormAnalysisUtil.getTrendIncoemJSONThree(income1, income2, income3,income4));
			hashMap.put("json4", ThreeRpoetFormAnalysisUtil.getTrendIncoemJSONFour(income1, income2, income3,income4));

			hashMap.put("json11", ThreeRpoetFormAnalysisUtil.getIncomeRatioData(finaRepMonth, balance1, balance2,
					balance3, balance4, income1, income2, income3, income4, mip, rot, rop, igr, pgr));
			hashMap.put("json12", ThreeRpoetFormAnalysisUtil.getIncomeStructData(income1, income2, income3, income4));
			hashMap.put("json13",
					ThreeRpoetFormAnalysisUtil.getIncomeStructTableData(income1, income2, income3, income4));
			hashMap.put("year", finaRepYear);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("利润表分析异常！", e);
		}

		return jRequest.serialize(hashMap);
	}

	/**
	 * @Description (现金流量表分析（加上层次分析）)
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("cashFlowTrend.json")
	@ResponseBody
	public String cashFlowTrendJSON(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		
		try {
			
			long taskId = HttpUtil.getAsLong(request, "taskId");
			String customerNo = HttpUtil.getAsString(request, "customerNo");
			
			Map<String,Object> finaRepInfo = compFinanceSV.getFinaRepInfo(taskId, customerNo);
			
			int finaRepYear = (Integer) finaRepInfo.get("finaRepYear");
			int finaRepMonth = (Integer) finaRepInfo.get("finaRepMonth");
			String industry = (String) finaRepInfo.get("industry");

			List<Map<String, Double>> balanceData = finanasisSV.queryBalanceSheet(customerNo,
					finaRepYear, finaRepMonth);
			List<Map<String, Double>> incomeData = finanasisSV.queryIncomeSheet(customerNo,
					finaRepYear, finaRepMonth);
			List<Map<String, Double>> cashFlowData = finanasisSV.queryCashFlowSheet(customerNo,
					finaRepYear, finaRepMonth);

			HashMap<String, Double> balance1 = (HashMap<String, Double>) balanceData.get(0);
			HashMap<String, Double> balance2 = (HashMap<String, Double>) balanceData.get(1);
			HashMap<String, Double> balance3 = (HashMap<String, Double>) balanceData.get(2);
			HashMap<String, Double> balance4 = (HashMap<String, Double>) balanceData.get(3);

			HashMap<String, Double> income1 = (HashMap<String, Double>) incomeData.get(0);
			HashMap<String, Double> income2 = (HashMap<String, Double>) incomeData.get(1);
			HashMap<String, Double> income3 = (HashMap<String, Double>) incomeData.get(2);
			HashMap<String, Double> income4 = (HashMap<String, Double>) incomeData.get(3);

			HashMap<String, Double> cashFlow1 = (HashMap<String, Double>) cashFlowData.get(0);
			HashMap<String, Double> cashFlow2 = (HashMap<String, Double>) cashFlowData.get(1);
			HashMap<String, Double> cashFlow3 = (HashMap<String, Double>) cashFlowData.get(2);
			HashMap<String, Double> cashFlow4 = (HashMap<String, Double>) cashFlowData.get(3);

			TreeData taskTreeData = industrySV.getTreeDataByEnname(industry,
					SysConstants.TreeDicConstant.GB_IC_4754_2017);
			Map<String, IndustryQRadio> quotaMap = this.industrySV.getQuotaByIndustry(taskTreeData);

			double cfc = 0.0;
			IndustryQRadio tatio1 = quotaMap.get(SysConstants.FinanceQuota.XJLDFZ_RATE);
			if(tatio1 != null)
				cfc = tatio1.getAverageValue();

			hashMap.put("json1",
					ThreeRpoetFormAnalysisUtil.getCashFlowTrendDataOne(cashFlow1, cashFlow2, cashFlow3, cashFlow4));
			hashMap.put("json2", ThreeRpoetFormAnalysisUtil.getCashFlowTrendDataTwo(cashFlow1, cashFlow2, cashFlow3,
					cashFlow4, income1, income2, income3, income4));
			hashMap.put("json3",
					ThreeRpoetFormAnalysisUtil.getCashFlowTrendDataThree(cashFlow1, cashFlow2, cashFlow3, cashFlow4));
			hashMap.put("json4",
					ThreeRpoetFormAnalysisUtil.getCashFlowTrendDataFour(cashFlow1, cashFlow2, cashFlow3, cashFlow4));
			hashMap.put("json5",
					ThreeRpoetFormAnalysisUtil.getCashFlowTrendDataFive(cashFlow1, cashFlow2, cashFlow3, cashFlow4));
			hashMap.put("json6",
					ThreeRpoetFormAnalysisUtil.getCashFlowTrendDataSix(cashFlow1, cashFlow2, cashFlow3, cashFlow4));
			hashMap.put("json11", ThreeRpoetFormAnalysisUtil.getCashFlowRatioData(balance1, balance2, balance3,
					balance4, cashFlow2, cashFlow3, cashFlow4, 100, cfc, 9.50));

			hashMap.put("ehght1",
					ThreeRpoetFormAnalysisUtil.getEightCashFlowOne(balance1, balance2, balance3, income2, income3));
			hashMap.put("ehght2",
					ThreeRpoetFormAnalysisUtil.getEightCashFlowTwo(balance1, balance2, balance3, income2, income3));
			hashMap.put("ehght3",
					ThreeRpoetFormAnalysisUtil.getEightCashFlowThree(balance1, balance2, balance3, income2, income3));
			hashMap.put("ehght4",
					ThreeRpoetFormAnalysisUtil.getEightCashFlowFour(balance1, balance2, balance3, income2, income3));
			hashMap.put("ehght5",
					ThreeRpoetFormAnalysisUtil.getEightCashFlowFive(balance1, balance2, balance3, income2, income3));
			hashMap.put("ehght6",
					ThreeRpoetFormAnalysisUtil.getEightCashFlowSix(balance1, balance2, balance3, income2, income3));
			hashMap.put("ehght7",
					ThreeRpoetFormAnalysisUtil.getEightCashFlowSeven(balance1, balance2, balance3, income2, income3));
			hashMap.put("ehght8",
					ThreeRpoetFormAnalysisUtil.getEightCashFlowEight(balance1, balance2, balance3, income2, income3));

			hashMap.put("ehghtCommon1", ThreeRpoetFormAnalysisUtil.getEightCashFlowCommonOne(finaRepYear, balance1,
					balance2, balance3, income2, income3));
			hashMap.put("ehghtCommon2", ThreeRpoetFormAnalysisUtil.getEightCashFlowCommonTwo(finaRepYear, balance1,
					balance2, balance3, income2, income3));
			hashMap.put("ehghtCommon3", ThreeRpoetFormAnalysisUtil.getEightCashFlowCommonThree(finaRepYear, balance1,
					balance2, balance3, income2, income3));
			hashMap.put("ehghtCommon4", ThreeRpoetFormAnalysisUtil.getEightCashFlowCommonFour(finaRepYear, balance1,
					balance2, balance3, income2, income3));
			hashMap.put("ehghtCommon5", ThreeRpoetFormAnalysisUtil.getEightCashFlowCommonFive(finaRepYear, balance1,
					balance2, balance3, income2, income3));
			hashMap.put("ehghtCommon6", ThreeRpoetFormAnalysisUtil.getEightCashFlowCommonSix(finaRepYear, balance1,
					balance2, balance3, income2, income3));
			hashMap.put("ehghtCommon7", ThreeRpoetFormAnalysisUtil.getEightCashFlowCommonSeven(finaRepYear, balance1,
					balance2, balance3, income2, income3));
			hashMap.put("ehghtCommon8", ThreeRpoetFormAnalysisUtil.getEightCashFlowCommonEight(finaRepYear, balance1,
					balance2, balance3, income2, income3));
			hashMap.put("year", finaRepYear);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("现金流量表分析（加上层次分析）异常！", e);
		}

		return jRequest.serialize(hashMap);
	}

	/**
	 * @Description (现金流层次分析，之前主要为了在安卓端显示)
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("cashFlowPandectTwo.json")
	@ResponseBody
	public String cashFlowPandectTwoJSON(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		
		try {
			
			long taskId = HttpUtil.getAsLong(request, "taskId");
			String customerNo = HttpUtil.getAsString(request, "customerNo");
			
			Map<String,Object> finaRepInfo = compFinanceSV.getFinaRepInfo(taskId, customerNo);
			
			int finaRepYear = (Integer) finaRepInfo.get("finaRepYear");
	
			ReportData balanceData = finanasisSV.getReportDataByTaskIdCustNoAndType(taskId, customerNo,
					"balance");
			ReportData incomeData = finanasisSV.getReportDataByTaskIdCustNoAndType(taskId, customerNo,
					"income");
	
			List<ReportDataTR> balanceTrs = finanasisSV.getReportDataTrsByReportDataId(balanceData.getId());
			List<ReportDataTR> incomeTrs = finanasisSV.getReportDataTrsByReportDataId(incomeData.getId());
	
			HashMap<String, Double> balance1 = MapUtil.getValueList(balanceTrs).get(0);
			HashMap<String, Double> balance2 = MapUtil.getValueList(balanceTrs).get(1);
			HashMap<String, Double> balance3 = MapUtil.getValueList(balanceTrs).get(2);
	
			HashMap<String, Double> income2 = MapUtil.getValueList(incomeTrs).get(1);
			HashMap<String, Double> income3 = MapUtil.getValueList(incomeTrs).get(2);
	
			hashMap.put("ehght1",
					ThreeRpoetFormAnalysisUtil.getEightCashFlowOne(balance1, balance2, balance3, income2, income3));
			hashMap.put("ehght2",
					ThreeRpoetFormAnalysisUtil.getEightCashFlowTwo(balance1, balance2, balance3, income2, income3));
			hashMap.put("ehght3",
					ThreeRpoetFormAnalysisUtil.getEightCashFlowThree(balance1, balance2, balance3, income2, income3));
			hashMap.put("ehght4",
					ThreeRpoetFormAnalysisUtil.getEightCashFlowFour(balance1, balance2, balance3, income2, income3));
			hashMap.put("ehght5",
					ThreeRpoetFormAnalysisUtil.getEightCashFlowFive(balance1, balance2, balance3, income2, income3));
			hashMap.put("ehght6",
					ThreeRpoetFormAnalysisUtil.getEightCashFlowSix(balance1, balance2, balance3, income2, income3));
			hashMap.put("ehght7",
					ThreeRpoetFormAnalysisUtil.getEightCashFlowSeven(balance1, balance2, balance3, income2, income3));
			hashMap.put("ehght8",
					ThreeRpoetFormAnalysisUtil.getEightCashFlowEight(balance1, balance2, balance3, income2, income3));
	
			hashMap.put("ehghtCommon1", ThreeRpoetFormAnalysisUtil.getEightCashFlowCommonOne(finaRepYear, balance1,
					balance2, balance3, income2, income3));
			hashMap.put("ehghtCommon2", ThreeRpoetFormAnalysisUtil.getEightCashFlowCommonTwo(finaRepYear, balance1,
					balance2, balance3, income2, income3));
			hashMap.put("ehghtCommon3", ThreeRpoetFormAnalysisUtil.getEightCashFlowCommonThree(finaRepYear, balance1,
					balance2, balance3, income2, income3));
			hashMap.put("ehghtCommon4", ThreeRpoetFormAnalysisUtil.getEightCashFlowCommonFour(finaRepYear, balance1,
					balance2, balance3, income2, income3));
			hashMap.put("ehghtCommon5", ThreeRpoetFormAnalysisUtil.getEightCashFlowCommonFive(finaRepYear, balance1,
					balance2, balance3, income2, income3));
			hashMap.put("ehghtCommon6", ThreeRpoetFormAnalysisUtil.getEightCashFlowCommonSix(finaRepYear, balance1,
					balance2, balance3, income2, income3));
			hashMap.put("ehghtCommon7", ThreeRpoetFormAnalysisUtil.getEightCashFlowCommonSeven(finaRepYear, balance1,
					balance2, balance3, income2, income3));
			hashMap.put("ehghtCommon8", ThreeRpoetFormAnalysisUtil.getEightCashFlowCommonEight(finaRepYear, balance1,
					balance2, balance3, income2, income3));
			
			hashMap.put("year", finaRepYear);
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error("现金流层次分析异常！", e);
		}
		
		return jRequest.serialize(hashMap);
	}

	/**
	 * @Description (比率分析)
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("ratio.json")
	@ResponseBody
	public String ratioJSON(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		
		try {
			
			long taskId = HttpUtil.getAsLong(request, "taskId");
			String customerNo = HttpUtil.getAsString(request, "customerNo");
			
			Map<String,Object> finaRepInfo = compFinanceSV.getFinaRepInfo(taskId, customerNo);
			
			int finaRepYear = (Integer) finaRepInfo.get("finaRepYear");
			int finaRepMonth = (Integer) finaRepInfo.get("finaRepMonth");
			String industry = (String) finaRepInfo.get("industry");

			TreeData taskTreeData = industrySV.getTreeDataByEnname(industry,
					SysConstants.TreeDicConstant.GB_IC_4754_2017);
			Map<String, IndustryQRadio> quotaMap = this.industrySV.getQuotaByIndustry(taskTreeData);

			List<Map<String, Double>> balanceData = finanasisSV.queryBalanceSheet(customerNo,
					finaRepYear, finaRepMonth);
			List<Map<String, Double>> incomeData = finanasisSV.queryIncomeSheet(customerNo,
					finaRepYear, finaRepMonth);
			List<Map<String, Double>> cashFlowData = finanasisSV.queryCashFlowSheet(customerNo,
					finaRepYear, finaRepMonth);

			HashMap<String, Double> balance1 = (HashMap<String, Double>) balanceData.get(0);
			HashMap<String, Double> balance2 = (HashMap<String, Double>) balanceData.get(1);
			HashMap<String, Double> balance3 = (HashMap<String, Double>) balanceData.get(2);
			HashMap<String, Double> balance4 = (HashMap<String, Double>) balanceData.get(3);

			HashMap<String, Double> income1 = (HashMap<String, Double>) incomeData.get(0);
			HashMap<String, Double> income2 = (HashMap<String, Double>) incomeData.get(1);
			HashMap<String, Double> income3 = (HashMap<String, Double>) incomeData.get(2);
			HashMap<String, Double> income4 = (HashMap<String, Double>) incomeData.get(3);

			HashMap<String, Double> cashFlow1 = (HashMap<String, Double>) cashFlowData.get(0);
			HashMap<String, Double> cashFlow2 = (HashMap<String, Double>) cashFlowData.get(1);
			HashMap<String, Double> cashFlow3 = (HashMap<String, Double>) cashFlowData.get(2);
			HashMap<String, Double> cashFlow4 = (HashMap<String, Double>) cashFlowData.get(3);

			Map<String, Object> ratioMap = ControllerJSONUtil.getRatioJSON(finaRepMonth, balance3, balance4, income3, income4,
					cashFlow3, cashFlow4);
			hashMap.put("json1", ControllerJSONUtil.getRatioJSON(finaRepMonth, balance3, balance4, income3, income4,
					cashFlow3, cashFlow4));

			hashMap.put("json2", ControllerJSONUtil.getIndustryCompareJSON(finaRepMonth, balance1, balance2,
					balance3, balance4, income1, income2, income3, income4, quotaMap));

			int month = finaRepMonth;
			// 比率趋势分析 盈利能力分析
			hashMap.put("json11", ControllerJSONUtil.getTrendRatioRoeJSON(month, balance1, balance2, balance3, balance4,
					income1, income2, income3, income4));
			hashMap.put("json12", ControllerJSONUtil.getTrendRatioRoaJSON(month, balance1, balance2, balance3, balance4,
					income1, income2, income3, income4));
			hashMap.put("json13", ControllerJSONUtil.getTrendRatioGMJSON(balance1, balance2, balance3, balance4,
					income1, income2, income3, income4));
			hashMap.put("json14", ControllerJSONUtil.getTrendRatioOpfJSON(month, balance1, balance2, balance3, balance4,
					income1, income2, income3, income4));
			hashMap.put("json15", ControllerJSONUtil.getTrendRatioCneJSON(balance1, balance2, balance3, balance4,
					income1, income2, income3, income4));
			hashMap.put("json16", ControllerJSONUtil.getTrendRatioRoasJSON(month, balance1, balance2, balance3,
					balance4, income1, income2, income3, income4));

			// 比率趋势分析 运营能力分析
			hashMap.put("json21", ControllerJSONUtil.getTrendRatioTrJSON(month, balance1, balance2, balance3, balance4,
					income1, income2, income3, income4));
			hashMap.put("json22", ControllerJSONUtil.getTrendRatioRRJSON(month, balance1, balance2, balance3, balance4,
					income1, income2, income3, income4));
			hashMap.put("json23", ControllerJSONUtil.getTrendRatioIRJSON(month, balance1, balance2, balance3, balance4,
					income1, income2, income3, income4));
			hashMap.put("json24", ControllerJSONUtil.getTrendRatioCAJSON(month, balance1, balance2, balance3, balance4,
					income1, income2, income3, income4));

			// 比率趋势分析 偿债能力分析
			hashMap.put("json30", ControllerJSONUtil.getTrendRatioDARJSON(balance1, balance2, balance3, balance4,
					income1, income2, income3, income4));
			hashMap.put("json31", ControllerJSONUtil.getTrendRatioICRJSON(balance1, balance2, balance3, balance4,
					income1, income2, income3, income4));
			hashMap.put("json32", ControllerJSONUtil.getTrendRatioQRJSON(balance1, balance2, balance3, balance4,
					income1, income2, income3, income4));
			hashMap.put("json33", ControllerJSONUtil.getTrendRatioCRJSON(balance1, balance2, balance3, balance4,
					income1, income2, income3, income4));
			hashMap.put("json34", ControllerJSONUtil.getTrendRatioCFJSON(balance1, balance2, balance3, balance4,
					cashFlow1, cashFlow2, cashFlow3, cashFlow4));

			// 比率趋势分析 成长能力分析
			hashMap.put("json41", ControllerJSONUtil.getTrendRatioSGRJSON(month,balance1, balance2, balance3, balance4,
					income1, income2, income3, income4));
			hashMap.put("json42", ControllerJSONUtil.getTrendRatioTAGJSON(balance1, balance2, balance3, balance4,
					income1, income2, income3, income4));

			hashMap.put("common", RatioCommon.getRatioCommonJSON(4, balance1, balance2, balance3, balance4, income1,
					income2, income3, income4, quotaMap));
			hashMap.put("year", finaRepYear);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("比率分析异常！", e);
		}

		return jRequest.serialize(hashMap);
	}
	
	/**
	 * 一般公司保证人分析页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("analyGuaraComp.do")
	public String analyGuaraComp(HttpServletRequest request, HttpServletResponse response) {
		
		try {
			
			String taskId = HttpUtil.getAsString(request, "taskId");
			String customerNo = HttpUtil.getAsString(request, "customerNo");
			
			CompBaseDto compBase = compBaseSV.findCompBaseByCustNo(customerNo);
			request.setAttribute("cusFlag", SysConstants.CusFlag.GUARA_COM);
			request.setAttribute("taskId", taskId);
			request.setAttribute("customerNo", customerNo);
			request.setAttribute("customerName", compBase.getCustomerName());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/Application/analysis_index";
	}
	
	/**
	 * 一般企业保证人列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("compGuaraInfo.do")
	public String compGuaraInfo(HttpServletRequest request, HttpServletResponse response) {
		
		try {
			
			Long taskId = HttpUtil.getAsLong(request, "taskId");
			List<Guarantor> guarantors = guaraInfoSV.findGuarantorByTaskId(taskId,
					SysConstants.GuarantorType.COMP_GUARA);
			
			List<CompBaseDto> comps = null;
			if(guarantors != null){
				comps = new ArrayList<>();
				for(Guarantor guarantor : guarantors){
					comps.add(compBaseSV.findCompBaseByCustNo(guarantor.getCusNo()));
				}
			}
			
			request.setAttribute("guarantors", comps);
			request.setAttribute("taskId", taskId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/Analysis/finansis/guaraInfo";
	}
	
	/**
	 * 跳转分析结论页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("conclusionInfo.do")
	public String conclusionInfo(HttpServletRequest request, HttpServletResponse response,
			long taskId, String customerNo) {
		
		try {
			
			Map<String, Object> conclusion = finanasisSV.genAnalysisConclusion(taskId,
					customerNo);
			Iterator<String> it = conclusion.keySet().iterator();
			while(it.hasNext()){
				String key = it.next();
				request.setAttribute(key, conclusion.get(key));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "views/Analysis/finansis/conclusion";
	}
	
	/**
	 * @Description (流动资金贷款预测算编辑后上传到服务器)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("downloadExcel.do")
	@ResponseBody
	public String downloadExcel(HttpServletRequest request, HttpServletResponse response) {
		String dataJSON = request.getParameter("listOne");
		Gson gson = new Gson();
		List<List<String>> lists = gson.fromJson(dataJSON, new TypeToken<List<List<String>>>() {
		}.getType());
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		String taskID = UserSession.getUserSession(request).getTaskId();
		Task task = taskSV.getTaskById(Long.valueOf(taskID));
		
		String name = task.getCustomerName();
		String para = PropertiesUtil.getProperty(Constants.SYSTEM_PROPS_PATH, "survey.report.path");// 文件缓存地址
		String dir = para + "/downloadreport";
		File file = new File(dir);
		if (!file.exists()) {
			file.mkdirs();
		}
		String namefinal = CodeUtil.parseGBK(name + "流动资金贷款预测算");
		para += "/downloadreport/" + namefinal + ".xls";
		try {
			WritableWorkbook book = Workbook.createWorkbook(new File(para));
			WritableSheet sheet = book.createSheet(namefinal, 0);
			for (int i = 0; i < lists.size(); i++) {
				for (int j = 0; j < lists.get(i).size(); j++) {
					sheet.addCell(new Label(j, i, lists.get(i).get(j)));
				}
			}
			book.write();
			// 关闭文件
			book.close();
		} catch (IOException e) {
			e.printStackTrace();
			log.error("流动资金贷款预测算编辑后上传到服务器异常！", e);
		} catch (RowsExceededException e) {
			e.printStackTrace();
			log.error("流动资金贷款预测算编辑后上传到服务器异常！", e);
		} catch (WriteException e) {
			e.printStackTrace();
			log.error("流动资金贷款预测算编辑后上传到服务器异常！", e);
		}
		json.put("result", true);
		return jRequest.serialize(json, true);
	}

	/**
	 * @Description (流动资金贷款预测算从服务器上下载)
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("getExcel.do")
	public ResponseEntity<byte[]> getExcel(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		
		String para = PropertiesUtil.getProperty(Constants.SYSTEM_PROPS_PATH, "survey.report.path");// 文件缓存地址
		String taskId = UserSession.getUserSession(request).getTaskId();
		Task task = taskSV.getTaskById(Long.valueOf(taskId));
		String name = task.getCustomerName();
		String namefinal = CodeUtil.parseGBK(name + "流动资金贷款预测算");
		para += "/downloadreport/" + namefinal + ".xls";
		File file = new File(para);
		HttpHeaders headers = new HttpHeaders();
		String fileName = new String(namefinal + ".xls");
		headers.setContentDispositionFormData("attachment", fileName);
		headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		
		return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file),
				headers, HttpStatus.CREATED);
	}

}
