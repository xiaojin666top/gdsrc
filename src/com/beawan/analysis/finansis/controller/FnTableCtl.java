package com.beawan.analysis.finansis.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.beawan.analysis.finansis.bean.*;
import com.beawan.base.entity.User;
import com.beawan.common.Constants;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.platform.util.DateUtil;
import com.platform.util.HttpUtil;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.analysis.finansis.service.IFnTableService;
import com.beawan.base.entity.SysDic;
import com.beawan.base.service.ISysDicSV;
import com.beawan.common.SysConstants;
import com.beawan.common.SysConstants.ExceptionConstant;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.task.service.ITaskSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/fnTable" })
public class FnTableCtl extends BaseController {
	
	private static final Logger log = Logger.getLogger(FnTableCtl.class);
	
	@Resource
	private ITaskSV taskSV;
	
	@Resource
	private IFnTableService fnTableService ;
	
	@Resource
	private ISysDicSV sysDicSV;
	
	
    private Map<String , Object> model = new HashMap<String, Object>();
	
	public Map<String, Object> getModel() {
		return model;
	}

	public void setModel(Map<String, Object> model) {
		this.model = model;
	}

	/**
	 * @Description (获取存货信息列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getCash.json")
	@ResponseBody
	public String getCash(HttpServletRequest request, HttpServletResponse response,
								long taskId) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<FnCash> dataGrid = null;
		try {
			dataGrid = fnTableService.findFnCashByTId(taskId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jRequest.serialize(dataGrid, true);
	}

	/**
	 * @Description (保存货币资金信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveCash.json")
	@ResponseBody
	public String saveCash(HttpServletRequest request, HttpServletResponse response,
								 long taskId, String jsonArray) {

		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();

		try {
			fnTableService.saveFnCash(taskId, jsonArray);
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			log.error("保存货币资金信息异常", e);
			e.printStackTrace();
		}

		return jRequest.serialize(json, true);
	}

	/**
	 * @Description (获取存货信息列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getInventorys.json")
	@ResponseBody
	public String getInventorys(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<FnInventory> dataGrid = null;
		try {
			dataGrid = fnTableService.findFnInventoryByTId(taskId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jRequest.serialize(dataGrid, true);
	}
	
	/**
	 * @Description (保存存货信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveInventorys.json")
	@ResponseBody
	public String saveInventorys(HttpServletRequest request, HttpServletResponse response,
			long taskId, String jsonArray) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			fnTableService.saveFnInventory(taskId, jsonArray);
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			log.error("保存存货信息异常", e);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (获取应收账款信息列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getReceives.json")
	@ResponseBody
	public String getReceives(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<FnReceive> dataGrid = null;
		try {
			dataGrid = fnTableService.findReceiveByTId(taskId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jRequest.serialize(dataGrid, true);
	}
	
	/**
	 * @Description (保存应收账款信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveReceives.json")
	@ResponseBody
	public String saveReceives(HttpServletRequest request, HttpServletResponse response,
			long taskId, String jsonArray) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			fnTableService.saveReceive(taskId, jsonArray);
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			log.error("保存应收账款信息异常", e);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (获取应收账款统计信息列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getReceiveStats.do")
	@ResponseBody
	public String getReceiveStats(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<FnReceiveStat> dataGrid = null;
		try {
			dataGrid = fnTableService.findReceiveStatByTId(taskId);
			if(CollectionUtils.isEmpty(dataGrid)){
				List<SysDic> dics = sysDicSV.findByType(SysConstants.BsDicConstant.STD_ACC_AGE_TYPE);
				dataGrid = new ArrayList<FnReceiveStat>();
				for(SysDic dic : dics){
					FnReceiveStat rs = new FnReceiveStat();
					rs.setTaskId(taskId);
					rs.setAccAge(dic.getEnName());
					dataGrid.add(rs);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jRequest.serialize(dataGrid, true);
	}
	
	/**
	 * @Description (保存应收账款统计信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveReceiveStats.do")
	@ResponseBody
	public String saveReceiveStats(HttpServletRequest request, HttpServletResponse response,
			long taskId, String jsonArray) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			fnTableService.saveReceiveStat(taskId, jsonArray);
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			log.error("保存应收账款统计信息异常", e);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (获取其他应收账款信息列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getOtherReceives.json")
	@ResponseBody
	public String getOtherReceives(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<FnOtherReceive> dataGrid = null;
		try {
			dataGrid = fnTableService.findOtherReceiveByTId(taskId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jRequest.serialize(dataGrid, true);
	}
	
	/**
	 * @Description (保存其他应收账款信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveOtherReceives.json")
	@ResponseBody
	public String saveOtherReceives(HttpServletRequest request, HttpServletResponse response,
			long taskId, String jsonArray) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			fnTableService.saveOtherReceive(taskId, jsonArray);
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			log.error("保存其他应收账款信息异常", e);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (获取主要固定资产 土地 信息列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getFaLands.json")
	@ResponseBody
	public String getFaLands(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<FnFixedAssetsLand> dataGrid = null;
		try {
			dataGrid = fnTableService.queryFnFixedAssetsLandByTId(taskId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jRequest.serialize(dataGrid, true);
	}
	
	/**
	 * @Description (保存主要固定资产（土地）信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveFaLands.json")
	@ResponseBody
	public String saveFaLands(HttpServletRequest request, HttpServletResponse response,
			long taskId, String jsonArray) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			fnTableService.saveFnFixedAssetsLands(taskId, jsonArray);
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			log.error("保存主要固定资产（土地）信息异常", e);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (获取主要固定资产（房产）信息列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getFaBuildings.json")
	@ResponseBody
	public String getFaBuildings(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<FnFixedAssetsBuilding> dataGrid = null;
		try {
			dataGrid = fnTableService.queryFnFixedAssetsBuildingByTId(taskId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jRequest.serialize(dataGrid, true);
	}
	
	/**
	 * @Description (保存主要固定资产（房产）信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveFaBuildings.json")
	@ResponseBody
	public String saveFaBuildings(HttpServletRequest request, HttpServletResponse response,
			long taskId, String jsonArray) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			fnTableService.saveFnFixedAssetsBuildings(taskId, jsonArray);
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			log.error("保存主要固定资产（房产）信息异常", e);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (获取主要固定资产（设备）信息列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getFaEquipments.json")
	@ResponseBody
	public String getFaEquipments(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<FnFixedAssetsEquipment> dataGrid = null;
		try {
			dataGrid = fnTableService.queryFnFixedAssetsEquipmentByTId(taskId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jRequest.serialize(dataGrid, true);
	}
	
	/**
	 * @Description (保存主要固定资产（设备）信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveFaEquipments.json")
	@ResponseBody
	public String saveFaEquipments(HttpServletRequest request, HttpServletResponse response,
			long taskId, String jsonArray) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			fnTableService.saveFnFixedAssetsEquipments(taskId, jsonArray);
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			log.error("保存主要固定资产（设备）信息异常", e);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (获取主要预付账款信息列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getPrepays.json")
	@ResponseBody
	public String getPrepays(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<FnPrepay> dataGrid = null;
		try {
			dataGrid = fnTableService.findPrepayByTId(taskId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jRequest.serialize(dataGrid, true);
	}
	
	/**
	 * @Description (保存主要预付账款信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("savePrepays.json")
	@ResponseBody
	public String savePrepays(HttpServletRequest request, HttpServletResponse response,
			long taskId, String jsonArray) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			fnTableService.savePrepay(taskId, jsonArray);
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			log.error("保存主要预付账款信息异常", e);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (获取应付账款信息列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getPays.json")
	@ResponseBody
	public String getPays(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<FnPay> dataGrid = null;
		try {
			dataGrid = fnTableService.findPayByTId(taskId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jRequest.serialize(dataGrid, true);
	}
	
	/**
	 * @Description (保存应付账款信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("savePays.json")
	@ResponseBody
	public String savePays(HttpServletRequest request, HttpServletResponse response,
			long taskId, String jsonArray) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			fnTableService.savePay(taskId, jsonArray);
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			log.error("保存应付账款信息异常", e);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (获取其他应付账款信息列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getOtherPays.json")
	@ResponseBody
	public String getOtherPays(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<FnOtherPay> dataGrid = null;
		try {
			dataGrid = fnTableService.findOtherPayByTId(taskId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jRequest.serialize(dataGrid, true);
	}
	
	/**
	 * @Description (保存其他应付账款信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveOtherPays.json")
	@ResponseBody
	public String saveOtherPays(HttpServletRequest request, HttpServletResponse response,
			long taskId, String jsonArray) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			fnTableService.saveOtherPay(taskId, jsonArray);
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			log.error("保存其他应付账款信息异常", e);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (获取预收账款信息列表)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getPreReceives.json")
	@ResponseBody
	public String getPreReceives(HttpServletRequest request, HttpServletResponse response,
			long taskId) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<FnPreReceive> dataGrid = null;
		try {
			dataGrid = fnTableService.findPreReceiveByTId(taskId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jRequest.serialize(dataGrid, true);
	}
	
	/**
	 * @Description (保存预收账款信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("savePreReceives.json")
	@ResponseBody
	public String savePreReceives(HttpServletRequest request, HttpServletResponse response,
			long taskId, String jsonArray) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			fnTableService.savePreReceive(taskId, jsonArray);
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			log.error("保存预收账款信息异常", e);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}

	/**
	 * 删除 货币资金信息
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("deleteCash.json")
	@ResponseBody
	public ResultDto deleteCash(HttpServletRequest request, Long id) {
		ResultDto re = returnFail("删除失败");
		if (id == null || id == 0) {
			re.setMsg("传输数据内容异常，请重试");
			return re;
		}
		User user = HttpUtil.getCurrentUser(request);
		try {
			FnCash entity = fnTableService.getFnCashById(id);
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			entity.setStatus(Constants.DELETE);
			fnTableService.saveFnCash(entity);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除 存货信息
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("deleteInventory.json")
	@ResponseBody
	public ResultDto deleteInventory(HttpServletRequest request, Long id) {
		ResultDto re = returnFail("删除失败");
		if (id == null || id == 0) {
			re.setMsg("传输数据内容异常，请重试");
			return re;
		}
		User user = HttpUtil.getCurrentUser(request);
		try {
			FnInventory entity = fnTableService.getFnInventoryById(id);
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			entity.setStatus(Constants.DELETE);
			fnTableService.saveFnInventory(entity);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除 应收账款信息
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("deleteReceive.json")
	@ResponseBody
	public ResultDto deleteReceive(HttpServletRequest request, Long id) {
		ResultDto re = returnFail("删除失败");
		if (id == null || id == 0) {
			re.setMsg("传输数据内容异常，请重试");
			return re;
		}
		User user = HttpUtil.getCurrentUser(request);
		try {
			FnReceive entity = fnTableService.getFnReceiveById(id);
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			entity.setStatus(Constants.DELETE);
			fnTableService.saveFnReceive(entity);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除 其他应收账款 信息
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("deleteOtherReceive.json")
	@ResponseBody
	public ResultDto deleteOtherReceive(HttpServletRequest request, Long id) {
		ResultDto re = returnFail("删除失败");
		if (id == null || id == 0) {
			re.setMsg("传输数据内容异常，请重试");
			return re;
		}
		User user = HttpUtil.getCurrentUser(request);
		try {
			FnOtherReceive entity = fnTableService.getFnOtherReceiveById(id);
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			entity.setStatus(Constants.DELETE);
			fnTableService.saveFnOtherReceive(entity);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除 预付账款 信息
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("deletePrepay.json")
	@ResponseBody
	public ResultDto deletePrepay (HttpServletRequest request, Long id) {
		ResultDto re = returnFail("删除失败");
		if (id == null || id == 0) {
			re.setMsg("传输数据内容异常，请重试");
			return re;
		}
		User user = HttpUtil.getCurrentUser(request);
		try {
			FnPrepay entity = fnTableService.getFnPrepayById(id);
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			entity.setStatus(Constants.DELETE);
			fnTableService.saveFnPrepay(entity);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除 固定资产-土地 信息
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("deleteFaLand.json")
	@ResponseBody
	public ResultDto deleteFaLand (HttpServletRequest request, Long id) {
		ResultDto re = returnFail("删除失败");
		if (id == null || id == 0) {
			re.setMsg("传输数据内容异常，请重试");
			return re;
		}
		User user = HttpUtil.getCurrentUser(request);
		try {
			FnFixedAssetsLand entity = fnTableService.getFnLandById(id);
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			entity.setStatus(Constants.DELETE);
			fnTableService.saveFnLand(entity);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除 固定资产-房屋 信息
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("deleteFaBuilding.json")
	@ResponseBody
	public ResultDto deleteFaBuilding (HttpServletRequest request, Long id) {
		ResultDto re = returnFail("删除失败");
		if (id == null || id == 0) {
			re.setMsg("传输数据内容异常，请重试");
			return re;
		}
		User user = HttpUtil.getCurrentUser(request);
		try {
			FnFixedAssetsBuilding entity = fnTableService.getFnBuildingById(id);
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			entity.setStatus(Constants.DELETE);
			fnTableService.saveFnBuilding(entity);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除 固定资产-设备 信息
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("deleteFaEquipment.json")
	@ResponseBody
	public ResultDto deleteFaEquipment (HttpServletRequest request, Long id) {
		ResultDto re = returnFail("删除失败");
		if (id == null || id == 0) {
			re.setMsg("传输数据内容异常，请重试");
			return re;
		}
		User user = HttpUtil.getCurrentUser(request);
		try {
			FnFixedAssetsEquipment entity = fnTableService.getFnEquipmentById(id);
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			entity.setStatus(Constants.DELETE);
			fnTableService.saveFnEquipment(entity);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除 应付账款
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("deletePays.json")
	@ResponseBody
	public ResultDto deletePays (HttpServletRequest request, Long id) {
		ResultDto re = returnFail("删除失败");
		if (id == null || id == 0) {
			re.setMsg("传输数据内容异常，请重试");
			return re;
		}
		User user = HttpUtil.getCurrentUser(request);
		try {
			FnPay entity = fnTableService.getFnPayById(id);
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			entity.setStatus(Constants.DELETE);
			fnTableService.saveFnPay(entity);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除 其他应付账款
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("deleteOtherPays.json")
	@ResponseBody
	public ResultDto deleteOtherPays (HttpServletRequest request, Long id) {
		ResultDto re = returnFail("删除失败");
		if (id == null || id == 0) {
			re.setMsg("传输数据内容异常，请重试");
			return re;
		}
		User user = HttpUtil.getCurrentUser(request);
		try {
			FnOtherPay entity = fnTableService.getFnOtherPayById(id);
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			entity.setStatus(Constants.DELETE);
			fnTableService.saveFnOtherPay(entity);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除 预收账款
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("deletePreReceives.json")
	@ResponseBody
	public ResultDto deletePreReceives (HttpServletRequest request, Long id) {
		ResultDto re = returnFail("删除失败");
		if (id == null || id == 0) {
			re.setMsg("传输数据内容异常，请重试");
			return re;
		}
		User user = HttpUtil.getCurrentUser(request);
		try {
			FnPreReceive entity = fnTableService.getFnPreReceiveById(id);
			entity.setUpdater(user.getUserId());
			entity.setUpdateTime(DateUtil.getNowTimestamp());
			entity.setStatus(Constants.DELETE);
			fnTableService.saveFnPreReceive(entity);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

}
