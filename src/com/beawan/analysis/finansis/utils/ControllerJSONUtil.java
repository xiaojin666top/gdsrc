package com.beawan.analysis.finansis.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.beawan.analysis.otherAnalysis.IndustryCompareUtil;
import com.beawan.common.SysConstants;
import com.beawan.library.bean.IndustryQRadio;
import com.beawan.ratioBility.developmentCapability.Total_Assets_Growth_Rate;
import com.beawan.ratioBility.longDebtPayingCapability.Debt_to_Assets_Ratio;
import com.beawan.ratioBility.longDebtPayingCapability.Interest_Coverage_Ratio;
import com.beawan.ratioBility.operationCapability.Inventory_Turnover;
import com.beawan.ratioBility.operationCapability.Receivables_Turnover_Ratio;
import com.beawan.ratioBility.operationCapability.Total_Assets_Turnover;
import com.beawan.ratioBility.profitablity.Main_Income_Profit_Ratio;
import com.beawan.ratioBility.profitablity.Rate_of_Return_on_Common;
import com.beawan.ratioBility.profitablity.Ratio_of_Profits_to_Cost_and_Expense;
import com.beawan.ratioBility.profitablity.Return_on_Total_Asset_Ratio;
import com.beawan.ratioBility.shortDebtPayingCapability.Quick_Ratio;
import com.beawan.utils.BalanceCheckJSON;
import com.beawan.utils.RatioJSON;
import com.beawan.utils.RatioTrendJSON;

public class ControllerJSONUtil {

	/*
	 * 获取比率分析中行业比较的json数据
	 * 
	 * @param balance1 balance2最近两个完整年度的资产负债表数据 按时间从早到晚排序
	 * 
	 * @param income 最近一个完整年度的利润表数据 industry 行业
	 * 
	 * @return json数据
	 */
	public static Map<String, Object> getIndustryCompareJSON(int month, HashMap<String, Double> balance1,
			HashMap<String, Double> balance2, HashMap<String, Double> balance3, HashMap<String, Double> balance4,
			HashMap<String, Double> income1, HashMap<String, Double> income2, HashMap<String, Double> income3,
			HashMap<String, Double> income4, Map<String, IndustryQRadio> quotaMap) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		List<Object> data = new ArrayList<Object>();

		
		List<String> ratio_data1 = getIndustryRatioData(balance1, balance2, income2);
		List<String> ratio_data2 = getIndustryRatioData(balance2, balance3, income3);
		List<String> ratio_data3 = getIndustryRatioTheYearData(month, balance3, balance4, income4);

		List<Double> avgNumber = new ArrayList<Double>();
		List<String> compare_data1 = new ArrayList<String>();
		List<String> compare_data2 = new ArrayList<String>();
		List<String> compare_data3 = new ArrayList<String>();
		
		if(quotaMap != null && quotaMap.size() != 0){
			avgNumber = IndustryCompareUtil.getCompareAvg(quotaMap);
			compare_data1 = IndustryCompareUtil.getCompareResult(balance1, balance2, income2, quotaMap);
			compare_data2 = IndustryCompareUtil.getCompareResult(balance2, balance3, income3, quotaMap);
			compare_data3 = IndustryCompareUtil.getCompareTheYearResult(month, balance3, balance4, income4,
					quotaMap);
		}

		data.add(avgNumber);
		data.add(ratio_data1);
		data.add(compare_data1);
		data.add(ratio_data2);
		data.add(compare_data2);
		data.add(ratio_data3);
		data.add(compare_data3);

		jsonMap.put("data", data);
		
		return jsonMap;
	}

	/*
	 * 获取比率分析中行业比较的json数据
	 * 
	 * @param balance1 balance2最近两个完整年度的资产负债表数据 按时间从早到晚排序
	 * 
	 * @param income 最近一个完整年度的利润表数据 industry 行业
	 * 
	 * @return json数据
	 */
	public static List<String> getIndustryRatioData(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> income) {

		List<String> ratio_data = new ArrayList<String>();
		// 净资产收益率
		double roe = Rate_of_Return_on_Common.cal(income, balance1, balance2);
		ratio_data.add(String.format("%.2f%%", roe * 100));
		// 总资产报酬率
		double roa = Return_on_Total_Asset_Ratio.cal(balance1, balance2, income);
		ratio_data.add(String.format("%.2f%%", roa * 100));

		// 成本费用利润率
		double zyyact = Ratio_of_Profits_to_Cost_and_Expense.cal(income);
		ratio_data.add(String.format("%.2f%%", zyyact * 100));
		
		//主营业务利润率
		double zyywact = Main_Income_Profit_Ratio.cal(income);
		ratio_data.add(String.format("%.2f%%", zyywact * 100));
		
		// 资产负债率
		double debtAssetRatio = Debt_to_Assets_Ratio.cal(balance2);
		ratio_data.add(String.format("%.2f%%", debtAssetRatio * 100));
		// // 流动比率
		// double currentRatio = Current_Ratio.cal(balance2);
		// ratio_data.add(String.format("%.2f%%", currentRatio * 100));
		// 速动比率
		double quickRatio = Quick_Ratio.cal(balance2);
		ratio_data.add(String.format("%.2f%%", quickRatio * 100));
		// 利息保障倍数
		double interestCoverageRatio = Interest_Coverage_Ratio.cal(income);
		ratio_data.add(String.format("%.2f", interestCoverageRatio));
		// // 销售利润率
		// double grossMargin = Gross_Margin.cal(income);
		// ratio_data.add(String.format("%.2f%%", grossMargin * 100));

		// 总资产周转次数
		double totalRation = Total_Assets_Turnover.cal(income, balance1, balance2);
		ratio_data.add(String.format("%.2f", totalRation));
		// 存货周转率
		double itnvr = Inventory_Turnover.cal(balance1, balance2, income);
		ratio_data.add(String.format("%.2f", itnvr));

		// 应收账款周转率（次）
		Double recevialRation1 = Receivables_Turnover_Ratio.cal(balance1, balance2, income);
		ratio_data.add(String.format("%.2f", recevialRation1));
		// 总资产增长率
		double totalAssets = Total_Assets_Growth_Rate.cal(balance1, balance2);
		ratio_data.add(String.format("%.2f%%", totalAssets * 100));

		return ratio_data;
	}

	/*
	 * 得到比率变化过快的异常，判断 标准为相比上一年增幅或者减幅超过20%并且具体变化超过2%
	 */
	public static Map<String, List<String>> getMainRatioBigChangeList(int month, HashMap<String, Double> newbalance2,
			HashMap<String, Double> newbalance3, HashMap<String, Double> newbalance4,
			HashMap<String, Double> newincome3, HashMap<String, Double> newincome4) {

		Map<String, List<String>> hashMap = new HashMap<String, List<String>>();
		List<String> lastRatioData = getIndustryRatioData(newbalance2, newbalance3, newincome3);
		List<String> theRatioData = getIndustryRatioTheYearData(month, newbalance3, newbalance4, newincome4);
		List<String> ratioName = getRatioCompareName();

		List<String> resultName = new ArrayList<String>();
		List<String> resultValue = new ArrayList<String>();
		List<String> resultChangeValue = new ArrayList<String>();
		List<String> resultChangeRatio = new ArrayList<String>();

		for (int i = 0; i < ratioName.size(); i++) {
			String name = ratioName.get(i);
			String lastRatio = lastRatioData.get(i);
			String theRatio = theRatioData.get(i);
			if (lastRatio.contains("%")) {
				lastRatio = lastRatio.substring(0, lastRatio.lastIndexOf("%"));
			}
			if (theRatio.contains("%")) {
				theRatio = theRatio.substring(0, theRatio.lastIndexOf("%"));
			}
			double lastDouble = Double.valueOf(lastRatio);
			double theDouble = Double.valueOf(theRatio);
			double changeValue = theDouble - lastDouble;
			double changeRatio = 0.0;
			if (lastDouble != 0.0) {
				changeRatio = (theDouble - lastDouble) / lastDouble;
			}
			if (changeRatio > 0.2 && changeValue > 2) {
				resultName.add(name);
				resultValue.add(theRatio);
				resultChangeValue.add(String.format("%.2f", changeValue));
				resultChangeRatio.add(String.format("%.2f%%", changeRatio * 100));
			}
		}
		hashMap.put("name", resultName);
		hashMap.put("value", resultValue);
		hashMap.put("changeValue", resultChangeValue);
		hashMap.put("changeRatio", resultChangeRatio);
		return hashMap;
	}

	/**
	 * 得到比率行业比较的比较名字
	 * 
	 * @return
	 */
	public static List<String> getRatioCompareName() {
		List<String> result = new ArrayList<String>();
		result.add("净资产收益率");
		result.add("总资产报酬率");
		result.add("成本费用利润率");
		result.add("主营业务利润率");
		result.add("资产负债率");
		result.add("速动比率");
		result.add("利息保障倍数");
		result.add("总资产周转次数");
		result.add("存货周转率");
		result.add("应收账款周转率");
		result.add("总资产增长率");
		return result;
	}

	/**
	 * 得到比率行业比较的比率缩写
	 * 
	 * @return
	 */
	public static List<String> getRatioCompareConstants() {
		List<String> result = new ArrayList<String>();
		result.add(SysConstants.FinanceQuota.ZZCSY_RATE);
		result.add(SysConstants.FinanceQuota.ZZCBC_RATE);
		result.add(SysConstants.FinanceQuota.CBFYLR_RATE);
		result.add(SysConstants.FinanceQuota.ZYYWLR_RATE);
		result.add(SysConstants.FinanceQuota.ZCFZ_RATE);
		result.add(SysConstants.FinanceQuota.SD_RATE);
		result.add(SysConstants.FinanceQuota.YHLX_COUNT);
		result.add(SysConstants.FinanceQuota.ZZZZZ_RATE);
		result.add(SysConstants.FinanceQuota.CHZZ_COUNT);
		result.add(SysConstants.FinanceQuota.YSZKZZ_RATE);
		result.add(SysConstants.FinanceQuota.ZZCZZ_RATE);
		return result;
	}

	/*
	 * 获取当期比率分析中行业比较的json数据
	 * 
	 * @param balance1 balance2最近两个完整年度的资产负债表数据 按时间从早到晚排序
	 * 
	 * @param income 最近一个完整年度的利润表数据 industry 行业
	 * 
	 * @return json数据
	 */
	public static List<String> getIndustryRatioTheYearData(int month, HashMap<String, Double> balance1,
			HashMap<String, Double> balance2, HashMap<String, Double> income) {

		List<String> ratio_data = new ArrayList<String>();
		// 净资产收益率
		double roe = Rate_of_Return_on_Common.cal(income, balance1, balance2) * (12.0 / month);
		ratio_data.add(String.format("%.2f%%", roe * 100));
		// 总资产报酬率
		double roa = Return_on_Total_Asset_Ratio.cal(balance1, balance2, income) * (12.0 / month);
		ratio_data.add(String.format("%.2f%%", roa * 100));

		// 成本费用利润率
		double zyyact = Ratio_of_Profits_to_Cost_and_Expense.cal(income);
		ratio_data.add(String.format("%.2f%%", zyyact * 100));
		

		//主营业务利润率
		double zyywact = Main_Income_Profit_Ratio.cal(income);
		ratio_data.add(String.format("%.2f%%", zyywact * 100));
		
		// 资产负债率
		double debtAssetRatio = Debt_to_Assets_Ratio.cal(balance2);
		ratio_data.add(String.format("%.2f%%", debtAssetRatio * 100));
		// // 流动比率
		// double currentRatio = Current_Ratio.cal(balance2);
		// ratio_data.add(String.format("%.2f%%", currentRatio * 100));
		// 速动比率
		double quickRatio = Quick_Ratio.cal(balance2);
		ratio_data.add(String.format("%.2f%%", quickRatio * 100));
		// 利息保障倍数
		double interestCoverageRatio = Interest_Coverage_Ratio.cal(income);
		ratio_data.add(String.format("%.2f", interestCoverageRatio));
		// // 销售利润率
		// double grossMargin = Gross_Margin.cal(income);
		// ratio_data.add(String.format("%.2f%%", grossMargin * 100));

		// 总资产周转次数
		double totalRation = Total_Assets_Turnover.cal(income, balance1, balance2) * (12.0 / month);
		ratio_data.add(String.format("%.2f", totalRation));
		// 存货周转率
		double itnvr = Inventory_Turnover.cal(balance1, balance2, income) * (12.0 / month);
		ratio_data.add(String.format("%.2f", itnvr));

		// 应收账款周转率（次）
		Double recevialRation1 = Receivables_Turnover_Ratio.cal(balance1, balance2, income) * (12.0 / month);
		ratio_data.add(String.format("%.2f", recevialRation1));
		// 总资产增长率
		double totalAssets = Total_Assets_Growth_Rate.cal(balance1, balance2);
		ratio_data.add(String.format("%.2f%%", totalAssets * 100));

		return ratio_data;
	}

	/*
	 * 获取比率趋势分析中“净资产收益率（%）”JSON数据
	 * 
	 * @return JSON数据
	 */
	public static Map<String, Object> getTrendRatioRoeJSON(int month, HashMap<String, Double> balance1,
			HashMap<String, Double> balance2, HashMap<String, Double> balance3, HashMap<String, Double> balance4,
			HashMap<String, Double> income1, HashMap<String, Double> income2, HashMap<String, Double> income3,
			HashMap<String, Double> income4) {

		Map<String, Object> jsonMap = new HashMap<String, Object>();
		List<String> title = new ArrayList<String>() {
			{
				add("净资产收益率（%）");
			}
		};
		jsonMap.put("title", title);
		jsonMap.put("data", RatioTrendJSON.getTrendRatioRoeData(month, balance1, balance2, balance3, balance4, income1,
				income2, income3, income4));
		return jsonMap;
	}

	/*
	 * 获取比率趋势分析中“总资产报酬率（%）”JSON数据
	 * 
	 * @return JSON数据
	 */
	public static Map<String, Object> getTrendRatioRoaJSON(int month, HashMap<String, Double> balance1,
			HashMap<String, Double> balance2, HashMap<String, Double> balance3, HashMap<String, Double> balance4,
			HashMap<String, Double> income1, HashMap<String, Double> income2, HashMap<String, Double> income3,
			HashMap<String, Double> income4) {

		Map<String, Object> jsonMap = new HashMap<String, Object>();
		List<String> title = new ArrayList<String>() {
			{
				add("总资产报酬率（%）");
			}
		};
		jsonMap.put("title", title);
		jsonMap.put("data", RatioTrendJSON.getTrendRatioRoaData(month, balance1, balance2, balance3, balance4, income1,
				income2, income3, income4));
		return jsonMap;
	}

	/* 比率趋势分析 */

	/*
	 * 获取比率趋势分析中“主营业务利润率（%）”data数据
	 * 
	 * @return data数据
	 */
	public static Map<String, Object> getTrendRatioOpfJSON(int month, HashMap<String, Double> balance1,
			HashMap<String, Double> balance2, HashMap<String, Double> balance3, HashMap<String, Double> balance4,
			HashMap<String, Double> income1, HashMap<String, Double> income2, HashMap<String, Double> income3,
			HashMap<String, Double> income4) {

		// 营业利润率
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		List<String> title = new ArrayList<String>() {
			{
				add("营业利润率（%）");
			}
		};
		jsonMap.put("title", title);
		jsonMap.put("data", RatioTrendJSON.getTrendRatioOpfData(month, balance1, balance2, balance3, balance4, income1,
				income2, income3, income4));
		return jsonMap;
	}

	/* 比率趋势分析 */
	/*
	 * 获取比率趋势分析中“成本费用利润率（%）”json数据
	 * 
	 * @return json数据
	 */
	public static Map<String, Object> getTrendRatioCneJSON(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, HashMap<String, Double> income1,
			HashMap<String, Double> income2, HashMap<String, Double> income3, HashMap<String, Double> income4) {

		// 成本费用利润率
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		List title = new ArrayList<String>() {
			{
				add("成本费用利润率（%）");
			}
		};
		jsonMap.put("title", title);
		jsonMap.put("data", RatioTrendJSON.getTrendRatioCneData(balance1, balance2, balance3, balance4, income1,
				income2, income3, income4));
		return jsonMap;
	}

	/* 比率趋势分析 */
	/*
	 * 获取比率趋势分析中“毛利率（%）”json数据
	 * 
	 * @return json数据
	 */
	public static Map<String, Object> getTrendRatioGMJSON(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, HashMap<String, Double> income1,
			HashMap<String, Double> income2, HashMap<String, Double> income3, HashMap<String, Double> income4) {

		// 毛利率
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		List title = new ArrayList<String>() {
			{
				add("毛利率");
			}
		};
		jsonMap.put("title", title);
		jsonMap.put("data", RatioTrendJSON.getTrendRatioGMData(balance1, balance2, balance3, balance4, income1, income2,
				income3, income4));
		return jsonMap;
	}

	/* 比率趋势分析 */
	/*
	 * 获取比率趋势分析中“资产收益率（%）”json数据
	 * 
	 * @return json数据
	 */
	public static Map<String, Object> getTrendRatioRoasJSON(int month, HashMap<String, Double> balance1,
			HashMap<String, Double> balance2, HashMap<String, Double> balance3, HashMap<String, Double> balance4,
			HashMap<String, Double> income1, HashMap<String, Double> income2, HashMap<String, Double> income3,
			HashMap<String, Double> income4) {

		// 营业利润率
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		List<String> title = new ArrayList<String>() {
			{
				add("资产收益率（%）");
			}
		};
		jsonMap.put("title", title);
		jsonMap.put("data", RatioTrendJSON.getTrendRatioRoasData(month, balance1, balance2, balance3, balance4, income1,
				income2, income3, income4));
		return jsonMap;
	}

	/* 比率趋势分析 */
	/*
	 * 获取比率趋势分析中“总资产周转率（次）”json数据
	 * 
	 * @return json数据
	 */
	public static Map<String, Object> getTrendRatioTrJSON(int month, HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, HashMap<String, Double> income1,
			HashMap<String, Double> income2, HashMap<String, Double> income3, HashMap<String, Double> income4) {

		// 营业利润率
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		List<String> title = new ArrayList<String>() {
			{
				add("总资产周转率（次）");
			}
		};
		jsonMap.put("title", title);
		jsonMap.put("data", RatioTrendJSON.getTrendRatioTrData(month, balance1, balance2, balance3, balance4, income1,
				income2, income3, income4));
		return jsonMap;
	}

	/* 比率趋势分析 */
	/*
	 * 获取比率趋势分析中“应收账款周转率（次）”json数据
	 * 
	 * @return json数据
	 */
	public static Map<String, Object> getTrendRatioRRJSON(int month, HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, HashMap<String, Double> income1,
			HashMap<String, Double> income2, HashMap<String, Double> income3, HashMap<String, Double> income4) {
		// 应收账款周转率（次）
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		List<String> title = new ArrayList<String>() {
			{
				add("应收账款周转率（次）");
			}
		};
		jsonMap.put("title", title);
		jsonMap.put("data", RatioTrendJSON.getTrendRatioRRData(month, balance1, balance2, balance3, balance4, income1,
				income2, income3, income4));
		return jsonMap;
	}

	/* 比率趋势分析 */
	/*
	 * 获取比率趋势分析中“存货周转率（次）”json数据
	 * 
	 * @return json数据
	 */
	public static Map<String, Object> getTrendRatioIRJSON(int month, HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, HashMap<String, Double> income1,
			HashMap<String, Double> income2, HashMap<String, Double> income3, HashMap<String, Double> income4) {
		// 存货周转率（次）
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		List<String> title = new ArrayList<String>() {
			{
				add("存货周转率（次）");
			}
		};
		jsonMap.put("title", title);
		jsonMap.put("data", RatioTrendJSON.getTrendRatioIRData(month, balance1, balance2, balance3, balance4, income1,
				income2, income3, income4));
		return jsonMap;
	}

	/* 比率趋势分析 */
	/*
	 * 获取比率趋势分析中“流动资产周转率”json数据
	 * 
	 * @return json数据
	 */
	public static Map<String, Object> getTrendRatioCAJSON(int month, HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, HashMap<String, Double> income1,
			HashMap<String, Double> income2, HashMap<String, Double> income3, HashMap<String, Double> income4) {
		// 流动资产周转率
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		List<String> title = new ArrayList<String>() {
			{
				add("流动资产周转率(次)");
			}
		};
		jsonMap.put("title", title);
		jsonMap.put("data", RatioTrendJSON.getTrendRatioCAData(month, balance1, balance2, balance3, balance4, income1,
				income2, income3, income4));
		return jsonMap;
	}

	/* 比率趋势分析 */
	/*
	 * 获取比率趋势分析中“资产负债率”json数据
	 * 
	 * @return json数据
	 */
	public static Map<String, Object> getTrendRatioDARJSON(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, HashMap<String, Double> income1,
			HashMap<String, Double> income2, HashMap<String, Double> income3, HashMap<String, Double> income4) {
		// 资产负债率
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		List<String> title = new ArrayList<String>() {
			{
				add("资产负债率（%）");
			}
		};
		jsonMap.put("title", title);
		jsonMap.put("data", RatioTrendJSON.getTrendRatioDARData(balance1, balance2, balance3, balance4, income1,
				income2, income3, income4));
		return jsonMap;
	}

	/* 比率趋势分析 */
	/*
	 * 获取比率趋势分析中“利息保障倍数”json数据
	 * 
	 * @return json数据
	 */
	public static Map<String, Object> getTrendRatioICRJSON(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, HashMap<String, Double> income1,
			HashMap<String, Double> income2, HashMap<String, Double> income3, HashMap<String, Double> income4) {
		// 利息保障倍数
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		List<String> title = new ArrayList<String>() {
			{
				add("利息保障倍数");
			}
		};
		jsonMap.put("title", title);
		jsonMap.put("data", RatioTrendJSON.getTrendRatioICRData(balance1, balance2, balance3, balance4, income1,
				income2, income3, income4));
		return jsonMap;
	}

	/* 比率趋势分析 */
	/*
	 * 获取比率趋势分析中“速动比率（%）”json数据
	 * 
	 * @return json数据
	 */
	public static Map<String, Object> getTrendRatioQRJSON(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, HashMap<String, Double> income1,
			HashMap<String, Double> income2, HashMap<String, Double> income3, HashMap<String, Double> income4) {
		// 速动比率（%）
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		List<String> title = new ArrayList<String>() {
			{
				add("速动比率（%）");
			}
		};
		jsonMap.put("title", title);
		jsonMap.put("data", RatioTrendJSON.getTrendRatioQRData(balance1, balance2, balance3, balance4, income1, income2,
				income3, income4));
		return jsonMap;
	}

	/* 比率趋势分析 */
	/*
	 * 获取比率趋势分析中“流动比率（%）”json数据
	 * 
	 * @return json数据
	 */
	public static Map<String, Object> getTrendRatioCRJSON(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, HashMap<String, Double> income1,
			HashMap<String, Double> income2, HashMap<String, Double> income3, HashMap<String, Double> income4) {
		// 流动比率（%）
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		List<String> title = new ArrayList<String>() {
			{
				add("流动比率（%）");
			}
		};
		jsonMap.put("title", title);
		jsonMap.put("data", RatioTrendJSON.getTrendRatioCRData(balance1, balance2, balance3, balance4, income1, income2,
				income3, income4));
		return jsonMap;
	}

	/* 比率趋势分析 */
	/*
	 * 获取比率趋势分析中“现金流动负债比率（%）”json数据
	 * 
	 * @return json数据
	 */
	public static Map<String, Object> getTrendRatioCFJSON(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, HashMap<String, Double> cashFlow1,
			HashMap<String, Double> cashFlow2, HashMap<String, Double> cashFlow3, HashMap<String, Double> cashFlow4) {
		// 现金流动负债比率（%）
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		List<String> title = new ArrayList<String>() {
			{
				add("现金流动负债比率（%）");
			}
		};
		jsonMap.put("title", title);
		jsonMap.put("data", RatioTrendJSON.getTrendRatioCFData(balance1, balance2, balance3, balance4, cashFlow1,
				cashFlow2, cashFlow3, cashFlow4));
		return jsonMap;
	}

	/* 比率趋势分析 */
	/*
	 * 获取比率趋势分析中“销售（营业）增长率比率（%）”json数据
	 * 
	 * @return json数据
	 */
	public static Map<String, Object> getTrendRatioSGRJSON(int month,HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, HashMap<String, Double> income1,
			HashMap<String, Double> income2, HashMap<String, Double> income3, HashMap<String, Double> income4) {
		// 销售（营业）增长率比率（%）
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		List<String> title = new ArrayList<String>() {
			{
				add("销售（营业）增长率比率（%）");
			}
		};
		jsonMap.put("title", title);
		jsonMap.put("data", RatioTrendJSON.getTrendRatioSGRData(month,balance1, balance2, balance3, balance4, income1,
				income2, income3, income4));
		return jsonMap;
	}

	/* 比率趋势分析 */
	/*
	 * 获取比率趋势分析中“总资产增长率（%）”json数据
	 * 
	 * @return json数据
	 */
	public static Map<String, Object> getTrendRatioTAGJSON(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, HashMap<String, Double> income1,
			HashMap<String, Double> income2, HashMap<String, Double> income3, HashMap<String, Double> income4) {
		// 总资产增长率
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		List<String> title = new ArrayList<String>() {
			{
				add("总资产增长率（%）");
			}
		};
		jsonMap.put("title", title);
		jsonMap.put("data", RatioTrendJSON.getTrendRatioTAGData(balance1, balance2, balance3, balance4, income1,
				income2, income3, income4));
		return jsonMap;
	}
	/* 比率趋势分析 END */

	/*
	 * 获取平衡性分析模块中的json数据
	 * 
	 * @param balance最近一个完整年度的资产负债表数据 income最近一个完整年度的利润表数据
	 * cashFlow最近一个完整年度的现金流量表数据
	 * 
	 * @return json数据
	 */
	public static Map<String, Object> getBalanceJSON(HashMap<String, Double> balance, HashMap<String, Double> income,
			HashMap<String, Double> cashFlow) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();

		jsonMap.put("data", BalanceCheckJSON.getBalanceData(balance, income, cashFlow));
		return jsonMap;
	}

	/*
	 * 获取比率分析模块中的json数据
	 * 
	 * @param balance1 balance2最近两个完整年度的资产负债表数据 balance1时间在前
	 * 
	 * @param income1 income2 最近两个完整年度的利润表数据 income1时间在前
	 * 
	 * @param cashFlow1 cahsFlow2 最近两个完整年度的现金流量表数据 cashFlow1时间在前
	 * 
	 * @return json数据
	 */
	public static Map<String, Object> getRatioJSON(Integer month , HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> income1, HashMap<String, Double> income2, HashMap<String, Double> cashFlow1,
			HashMap<String, Double> cashFlow2) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();
		jsonMap.put("data", RatioJSON.getRatioData(month ,balance1, balance2, income1, income2, cashFlow1, cashFlow2));
		return jsonMap;
	}

	/**
	 * @Description (用于处理产品分析中当年趋势分析中的数字 去掉中文)
	 * @param str
	 * @param mutl
	 *            要乘的倍数
	 * @return
	 */
	public static String cutStringNum(String str, double mutl) {
		String reg = "[\u4e00-\u9fa5]";
		Pattern p = Pattern.compile(reg);
		Matcher m = p.matcher(str);
		String num = m.replaceAll("").trim();
		String result = String.format("%.2f", Double.valueOf(num) * mutl);
		return result;
	}

}
