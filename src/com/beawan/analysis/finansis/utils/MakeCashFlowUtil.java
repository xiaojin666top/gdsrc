package com.beawan.analysis.finansis.utils;

import java.util.HashMap;
import java.util.List;

import com.beawan.Interface.SaveReportDataTr;
import com.beawan.analysis.finansis.bean.ReportData;
import com.beawan.analysis.finansis.bean.ReportDataTR;
import com.beawan.analysis.finansis.service.IFinanasisService;
import com.beawan.task.bean.Task;
import com.beawan.utils.GetCashFlow;
import com.beawan.utils.MapUtil;
import com.beawan.utils.TableStructList;

public class MakeCashFlowUtil {

	/**
	 * @Description (TODO这里用一句话描述这个方法的作用)
	 * @param taskId   任务号
	 * @param customerNo  客户号
	 * @param balanceType	资产负债表类型
	 * @param incomeType	利润表类型
	 * @param cashFlowId		现金流量表id
	 * @param saveReport	
	 * @param finanasisSV
	 */
	public static void makeCashFlow(Long taskId,String customerNo,String balanceType,String incomeType, Long cashFlowId, SaveReportDataTr<ReportDataTR> saveReport,
			IFinanasisService finanasisSV) {
		ReportData balanceData = finanasisSV.getReportDataByTaskIdCustNoAndType(taskId, customerNo,
				balanceType);
		ReportData incomeData = finanasisSV.getReportDataByTaskIdCustNoAndType(taskId, customerNo,
				incomeType);
		List<ReportDataTR> balanceTrs = finanasisSV.getReportDataTrsByReportDataId(balanceData.getId());
		List<ReportDataTR> incomeTrs = finanasisSV.getReportDataTrsByReportDataId(incomeData.getId());
		HashMap<String, Double> balance1 = MapUtil.getValueList(balanceTrs).get(0);
		HashMap<String, Double> balance2 = MapUtil.getValueList(balanceTrs).get(1);
		HashMap<String, Double> balance3 = MapUtil.getValueList(balanceTrs).get(2);
		HashMap<String, Double> balance4 = MapUtil.getValueList(balanceTrs).get(3);
		HashMap<String, Double> income1 = MapUtil.getValueList(incomeTrs).get(0);
		HashMap<String, Double> income2 = MapUtil.getValueList(incomeTrs).get(1);
		HashMap<String, Double> income3 = MapUtil.getValueList(incomeTrs).get(2);
		HashMap<String, Double> income4 = MapUtil.getValueList(incomeTrs).get(3);
		GetCashFlow.getCashFlow(cashFlowId, balance1, balance2, balance3, balance4, income1, income2, income3, income4,
				saveReport);
	}

	/**
	 * 修改报表项目中的某一年分的数据
	 * 
	 * @param data
	 *            所需修改的报表
	 * @param needChangeYear
	 *            三年报表加当期，所需修改的年份 0为第一年， 1为第二年，2为第三年，3为当期
	 * @param tableItemName
	 *            所需修改的项目名称，如"应收账款"、"存货"
	 * @param itemValue
	 *            修改值
	 * @param state
	 *            方法之中又调用了 此方法 状态依据
	 */
	public static void adjustTableValue(SaveReportDataTr<ReportDataTR> saveReport, List<ReportDataTR> newTrs,
			int needChangeYear, String tableItemName, double itemValue, int state) {

		List<String> BalanceCurrentAsset = TableStructList.getBalanceCurrentAssetList(); // 流动资产子项
		List<String> BalanceLongTerminvestments = TableStructList.getBalanceLongTerminvestmentsList(); // 长期投资子项
		List<String> BalanceFixAssets = TableStructList.getBalanceFixAssetsList(); // 固定资产子项
		List<String> BalanceIntangibleAsset = TableStructList.getBalanceIntangibleAssetList(); // 无形资产子项
		List<String> BalanceCurrentDebt = TableStructList.getBalanceCurrentDebtList(); // 流动负债子项
		List<String> BalanceLongDebt = TableStructList.getBalanceLongDebtList(); // 长期负债子项
		List<String> BalanceOwner = TableStructList.getBalanceOwnerList(); // 所有者权益子项

		for (ReportDataTR tr : newTrs) {
			double firstYear = 0.0;
			double secondYear = 0.0;
			double thirdYear = 0.0;
			double fourthYear  =0.0;
			if (tr.getFirstYearValue() != null) {
				 firstYear = tr.getFirstYearValue();
			}
			if (tr.getSecondYearValue() != null) {
				 secondYear = tr.getSecondYearValue();
			}
			if (tr.getThirdYearValue() != null) {
				 thirdYear = tr.getThirdYearValue();
			}
			if (tr.getFourthYearValue() != null) {
				 fourthYear = tr.getFourthYearValue();
			}

			String name = tr.getName();

			if (firstYear != 0.0 && secondYear != 0.0 && thirdYear != 0.0 && fourthYear != 0.0) {
				if (tableItemName.equals("固定资产")) {
					tableItemName = "固定资产合计";
				}
				if (tableItemName.equals(name)) {
					double oldValue = 0.0;
					if (needChangeYear == 0) {
						oldValue = firstYear;
					} else if (needChangeYear == 1) {
						oldValue = secondYear;
					} else if (needChangeYear == 2) {
						oldValue = thirdYear;
					} else if (needChangeYear == 3) {
						oldValue = fourthYear;
					}
					if (state == 1) {
						itemValue = oldValue + itemValue;
					}
					double changeValue = itemValue - oldValue;
					if (needChangeYear == 0) {
						tr.setFirstYearValue(itemValue);
					} else if (needChangeYear == 1) {
						tr.setSecondYearValue(itemValue);
					} else if (needChangeYear == 2) {
						tr.setThirdYearValue(itemValue);
					} else if (needChangeYear == 3) {
						tr.setFourthYearValue(itemValue);
					}
					saveReport.onSaveReportDataTR(tr);
					if (state != 1 && BalanceCurrentAsset.contains(tableItemName)) {
						adjustTableValue(saveReport, newTrs, needChangeYear, "流动资产合计", changeValue, 1);
						adjustTableValue(saveReport, newTrs, needChangeYear, "资产总计", changeValue, 1);
					}
					if (state != 1 && BalanceLongTerminvestments.contains(tableItemName)) {
						adjustTableValue(saveReport, newTrs, needChangeYear, "长期投资合计", changeValue, 1);
						adjustTableValue(saveReport, newTrs, needChangeYear, "资产总计", changeValue, 1);
					}
					if (state != 1 && BalanceFixAssets.contains(tableItemName)) {
						adjustTableValue(saveReport, newTrs, needChangeYear, "固定资产净值", changeValue, 1);
						adjustTableValue(saveReport, newTrs, needChangeYear, "固定资产净额", changeValue, 1);
						adjustTableValue(saveReport, newTrs, needChangeYear, "资产总计", changeValue, 1);
					}
					if (state != 1 && BalanceIntangibleAsset.contains(tableItemName)) {
						adjustTableValue(saveReport, newTrs, needChangeYear, "无形及其他资产合计", changeValue, 1);
						adjustTableValue(saveReport, newTrs, needChangeYear, "资产总计", changeValue, 1);
					}
					if (state != 1 && BalanceCurrentDebt.contains(tableItemName)) {
						adjustTableValue(saveReport, newTrs, needChangeYear, "流动负债合计", changeValue, 1);
						adjustTableValue(saveReport, newTrs, needChangeYear, "负债合计", changeValue, 1);
						adjustTableValue(saveReport, newTrs, needChangeYear, "资产总计", changeValue, 1);
					}
					if (state != 1 && BalanceLongDebt.contains(tableItemName)) {
						adjustTableValue(saveReport, newTrs, needChangeYear, "长期负债合计", changeValue, 1);
						adjustTableValue(saveReport, newTrs, needChangeYear, "负债合计", changeValue, 1);
						adjustTableValue(saveReport, newTrs, needChangeYear, "资产总计", changeValue, 1);
					}
					if (state != 1 && BalanceOwner.contains(tableItemName)) {
						adjustTableValue(saveReport, newTrs, needChangeYear, "所有者权益合计", changeValue, 1);
						adjustTableValue(saveReport, newTrs, needChangeYear, "资产总计", changeValue, 1);
					}
					if (state != 1 && tableItemName.equals("主营业务收入")) {
						adjustTableValue(saveReport, newTrs, needChangeYear, "主营业务利润", changeValue, 1);
						adjustTableValue(saveReport, newTrs, needChangeYear, "营业利润", changeValue, 1);
						adjustTableValue(saveReport, newTrs, needChangeYear, "利润总额", changeValue, 1);
						adjustTableValue(saveReport, newTrs, needChangeYear, "净利润", changeValue, 1);
					}
					if (state != 1 && (tableItemName.equals("营业费用") || tableItemName.equals("管理费用")
							|| tableItemName.equals("财务费用"))) {
						adjustTableValue(saveReport, newTrs, needChangeYear, "营业利润", -changeValue, 1);
						adjustTableValue(saveReport, newTrs, needChangeYear, "利润总额", -changeValue, 1);
						adjustTableValue(saveReport, newTrs, needChangeYear, "净利润", -changeValue, 1);
					}
					if (state != 1 && tableItemName.equals("所得税")) {
						adjustTableValue(saveReport, newTrs, needChangeYear, "净利润", -changeValue, 1);
					}
					break;
				}
			}
		}
	}

}
