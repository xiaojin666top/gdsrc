package com.beawan.analysis.finansis.utils;

import java.util.ArrayList;
import java.util.List;

public class SingleAnaly {
	
	/**
	 * @Description (资产趋势  结论)
	 * @param trends
	 * @return
	 */
	public static  List<String>  balanceAssetTrend(int year,List<List<String>> trends){
		List<String> result = new ArrayList<String>();
		if(trends.size() < 3)
			return result;
		List<String> items = trends.get(0);
		List<String> asset = trends.get(1);
		List<String> netAsset = trends.get(2);
		double assetRatio = 0.0;
		if(Double.valueOf(asset.get(3))!=0.0){
			assetRatio = (Double.valueOf(asset.get(3))-Double.valueOf(asset.get(2)))/Double.valueOf(asset.get(3));
		}
		double netAssetRatio = 0.0;
		if(Double.valueOf(netAsset.get(3))!=0.0){
			netAssetRatio = (Double.valueOf(netAsset.get(3))-Double.valueOf(netAsset.get(2)))/Double.valueOf(netAsset.get(3));
		}
		String assets = (year)+"年，资产"+asset.get(3)+"万元，相比上年"+asset.get(2)+"万元"+upOrDownRatio(assetRatio)+"；"+
														"权益"+netAsset.get(3)+"万元，相比上年"+netAsset.get(2)+"万元"+upOrDownRatio(netAssetRatio)+"";
		if(assetRatio>0 && netAssetRatio >0 && Math.abs(assetRatio-netAssetRatio)<0.2){
			assets += "，资产与权益基本保持同步增长，企业正在持续发展中";
		}
		assets += "。";
		result.add(assets);
		for(int i=3;i<trends.size();i++){
			double tempRatio =0.0;
			if(Double.valueOf(trends.get(i).get(3))!=0.0){
				tempRatio = (Double.valueOf(trends.get(i).get(3))-Double.valueOf(trends.get(i).get(2)))/Double.valueOf(trends.get(i).get(2));
			}
			String temp  =items.get(i-1) +trends.get(i).get(3)+"万元，相比上年"+trends.get(i).get(2)+"万元"+upOrDownRatio(tempRatio)+"。" ;
			result.add(temp);
		}
		return result;
	}
	
	/**
	 * @Description (负债趋势  结论)
	 * @param trends
	 * @return
	 */
	public static  List<String>  balanceDebtTrend(int year,List<List<String>> trends){
		List<String> result = new ArrayList<String>();
		List<String> items = trends.get(0);
		for(int i=1;i<trends.size();i++){
			double tempRatio =0.0;
			if(Double.valueOf(trends.get(i).get(3))!=0.0){
				tempRatio = (Double.valueOf(trends.get(i).get(3))-Double.valueOf(trends.get(i).get(2)))/Double.valueOf(trends.get(i).get(2));
			}
			String temp  =items.get(i-1) +trends.get(i).get(3)+"万元，相比上年"+trends.get(i).get(2)+"万元"+upOrDownRatio(tempRatio)+"。" ;
			result.add(temp);
		}
		return result;
	}
	
	/**
	 * @Description (所有者权益结构)
	 * @param year
	 * @param tableData
	 * @return
	 */
	public static  List<String>  balanceNetAssetStruct(int year,List<List<String>> tableData){
		List<String> result = new ArrayList<String>();
		for (int i=1;i<tableData.size();i++) {
			List<String> tempList = tableData.get(i);
			String tempStr = i+"."+tempList.get(0)+tempList.get(7)+"万元，占总权益"+tempList.get(8)+"。";
			result.add(tempStr);
		}
		return result;
	}
	
	/**
	 * @Description (所有者权益比率)
	 * @param year
	 * @param tableData
	 * @return
	 */
	public static  List<String>  balanceNetAssetRatio(int year,List<List<String>> ratioData,double avg1){
		List<String> result = new ArrayList<String>();
		List<String> theYear  = ratioData.get(2);  
		String tempStr ="";
		if(Double.valueOf(theYear.get(0))>avg1){
			 tempStr = "净资产收益率"+theYear.get(0)+"%，高于行业平均值，企业的资金使用效率较高，投资收益较高。";
		}else{
			 tempStr = "净资产收益率"+theYear.get(0)+"%，低于行业平均值，企业的资金使用效率较低，投资收益较低。";
		}
		result.add(tempStr);
		return result;
	}
	
	public  static String  incomeStruct(List<List<String>> incomeData){
		String result ="";
		List<String> items = incomeData.get(0);
		List<String> struct = incomeData.get(4);
		double total = 0.0;
		for(int i=0;i<items.size();i++){
			total+=Double.valueOf(struct.get(i));
		}
		double mainRatio = Double.valueOf(struct.get(0))/total;
		if(total!=0.0){
			mainRatio = Double.valueOf(struct.get(0))/total;
		}
		if(mainRatio == 1.0){
			result = "企业收入均来自于主营业务收入，无其他收入来源。";
			return result;
		}else{
			 if(mainRatio>0.9)
				 result = "企业主营业务突出，占总收入"+String.format("%.2f%%", mainRatio*100)+"，";
			 else
				 result = "企业主营业务并不突出，只占总收入"+String.format("%.2f%%", mainRatio*100)+"，";
			for(int i=1;i<items.size();i++){
				double ratio = Double.valueOf(struct.get(i))/total;
				result +=items.get(i)+ "占总收入"+String.format("%.2f%%", ratio*100)+"，";
			}
			result = result.substring(0,result.length()-1);
			result+="。";
			return result;
		}
	}
	
	
	public static String upOrDownRatio(double temp) {
		String result = "";
		if (temp > 0) {
			result = "增幅" + String.format("%.2f%%", temp * 100);
		}
		if (temp < 0) {
			result = "下降" + String.format("%.2f%%", -temp * 100);
		}
		return result;
	}
}
