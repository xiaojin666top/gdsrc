package com.beawan.analysis.finansis.utils;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.beawan.analysis.finansis.bean.FnPay;
import com.beawan.analysis.finansis.bean.FnPreReceive;
import com.beawan.analysis.finansis.bean.FnPrepay;
import com.beawan.analysis.finansis.bean.FnReceive;
import com.beawan.survey.custInfo.bean.CompBaseEquity;
import com.beawan.survey.custInfo.bean.CompRunEstaDevingProj;
import com.beawan.survey.custInfo.bean.CompRunEstaLand;
import com.beawan.survey.custInfo.bean.CompRunEstaDevedProj;

public class InfoSortCaluTotal {
	/**
	 * 这里的问题是因为double加String
	 * @param equities
	 * @return
	 */
	public List<CompBaseEquity> sortBaseEquity(List<CompBaseEquity> equities) {
		if (!CollectionUtils.isEmpty(equities)) {
			double fundAmountAll = 0.0;
			double fundRateAll = 0.0;
//			double invtFactAmtAll = 0.0;
			for (CompBaseEquity compBaseEquity : equities) {
				if (compBaseEquity.getFundAmount() != null) {
					fundAmountAll += Double.valueOf(compBaseEquity.getFundAmount());
				}
				if (compBaseEquity.getFundRate() != null) {
					fundRateAll += Double.valueOf(compBaseEquity.getFundRate());
				}
//				if (compBaseEquity.getInvtFactAmt() != null) {
//					invtFactAmtAll += compBaseEquity.getInvtFactAmt();
//				}
			}
			CompBaseEquity all = new CompBaseEquity();
			all.setStockName("合计");
			all.setFundAmount(fundAmountAll+"");
			all.setFundRate(fundRateAll+"");
//			all.setInvtFactAmt(invtFactAmtAll);
			equities.add(all);
		}
		return equities;
	}

	public List<FnPay> sortPays(List<FnPay> pays) {
		if (!CollectionUtils.isEmpty(pays)) {
			double valueAll = 0.0;
			double rateAll = 0.0;
			for (FnPay fnReceive : pays) {
				if (fnReceive.getRemainAmt() != null) {
					valueAll += fnReceive.getRemainAmt();
				}
				if (fnReceive.getRate() != null) {
					rateAll += fnReceive.getRate();
				}
			}
			FnPay allRe = new FnPay();
			allRe.setSupplierName("合计");
			allRe.setRemainAmt(valueAll);
			allRe.setRate(rateAll);
			pays.add(allRe);
		}
		return pays;
	}

	public List<FnPreReceive> sortPreReceives(List<FnPreReceive> preReceives) {
		if (!CollectionUtils.isEmpty(preReceives)) {
			double valueAll = 0.0;
			double rateAll = 0.0;
			for (FnPreReceive fnReceive : preReceives) {
				if (fnReceive.getPaidAmt() != null) {
					valueAll += fnReceive.getPaidAmt();
				}
				if (fnReceive.getRate() != null) {
					rateAll += fnReceive.getRate();
				}
			}
			FnPreReceive allRe = new FnPreReceive();
			allRe.setCusName("合计");
			allRe.setPaidAmt(valueAll);
			allRe.setRate(rateAll);
			preReceives.add(allRe);
		}
		return preReceives;
	}

	public List<FnPrepay> sortPrepays(List<FnPrepay> prepays) {
		if (!CollectionUtils.isEmpty(prepays)) {
			double valueAll = 0.0;
			double rateAll = 0.0;
			for (FnPrepay fnReceive : prepays) {
				if (fnReceive.getPaidAmt() != null) {
					valueAll += fnReceive.getPaidAmt();
				}
				if (fnReceive.getRate() != null) {
					rateAll += fnReceive.getRate();
				}
			}
			FnPrepay allRe = new FnPrepay();
			allRe.setSupplierName("合计");
			allRe.setPaidAmt(valueAll);
			allRe.setRate(rateAll);
			prepays.add(allRe);
		}
		return prepays;
	}

	public List<FnReceive> sortReceive(List<FnReceive> receives) {
		if (!CollectionUtils.isEmpty(receives)) {
			double valueAll = 0.0;
			double rateAll = 0.0;
			for (FnReceive fnReceive : receives) {
				if (fnReceive.getRemainAmt() != null) {
					valueAll += fnReceive.getRemainAmt();
				}
				if (fnReceive.getRate() != null) {
					rateAll += fnReceive.getRate();
				}
			}
			FnReceive allRe = new FnReceive();
			allRe.setCusName("合计");
			allRe.setRemainAmt(valueAll);
			allRe.setRate(rateAll);
			receives.add(allRe);
		}
		return receives;
	}

	public List<CompRunEstaDevingProj> sortRunProjDevings(List<CompRunEstaDevingProj> devedings) {
		if (!CollectionUtils.isEmpty(devedings)) {
			double inveRateAll = 0.0;
			double floorAreaAll = 0.0;
			double buildAreaAll = 0.0;
			double totalInveAmtAll = 0.0;
			double ownFundsAll = 0.0;
			double loanFromBankAll = 0.0;
			double advaSaleIncomeAll = 0.0;
			double expectSaleIncomeAll = 0.0;
			for (CompRunEstaDevingProj devings : devedings) {
				if (devings.getInveRate() != null) {
					inveRateAll += devings.getInveRate();
				}
				if (devings.getFloorArea() != null) {
					floorAreaAll += devings.getFloorArea();
				}
				if (devings.getBuildArea() != null) {
					buildAreaAll += devings.getBuildArea();
				}
				if (devings.getTotalInveAmt() != null) {
					totalInveAmtAll += devings.getTotalInveAmt();
				}
				if (devings.getOwnFunds() != null) {
					ownFundsAll += devings.getOwnFunds();
				}
				if (devings.getLoanFromBank() != null) {
					loanFromBankAll += devings.getLoanFromBank();
				}
				if (devings.getAdvaSaleIncome() != null) {
					advaSaleIncomeAll += devings.getAdvaSaleIncome();
				}
				if (devings.getExpectSaleIncome() != null) {
					expectSaleIncomeAll += devings.getExpectSaleIncome();
				}
			}
			CompRunEstaDevingProj allRe = new CompRunEstaDevingProj();
			allRe.setProjName("合计");
			allRe.setInveRate(inveRateAll);
			allRe.setFloorArea(floorAreaAll);
			allRe.setBuildArea(buildAreaAll);
			allRe.setTotalInveAmt(totalInveAmtAll);
			allRe.setOwnFunds(ownFundsAll);
			allRe.setLoanFromBank(loanFromBankAll);
			allRe.setAdvaSaleIncome(advaSaleIncomeAll);
			allRe.setExpectSaleIncome(expectSaleIncomeAll);
			devedings.add(allRe);
		}
		return devedings;
	}

	public List<CompRunEstaDevedProj> sortRunProjDevieds(List<CompRunEstaDevedProj> deveds) {
		if (!CollectionUtils.isEmpty(deveds)) {
			double inveRateAll = 0.0;
			double floorAreaAll = 0.0;
			double buildAreaAll = 0.0;
			double totalInveAmtAll = 0.0;
			double ownFundsAll = 0.0;
			double loanFromBankAll = 0.0;
			double advaSaleIncomeAll = 0.0;
			double expectSaleIncomeAll = 0.0;
			for (CompRunEstaDevedProj devings : deveds) {
				if (devings.getInveRate() != null) {
					inveRateAll += devings.getInveRate();
				}
				if (devings.getFloorArea() != null) {
					floorAreaAll += devings.getFloorArea();
				}
				if (devings.getGrossAreaTotal() != null) {
					buildAreaAll += devings.getGrossAreaTotal();
				}
				if (devings.getSaleArea() != null) {
					totalInveAmtAll += devings.getSaleArea();
				}
				if (devings.getUnSaleArea() != null) {
					ownFundsAll += devings.getUnSaleArea();
				}
				if (devings.getDeveCostTotal() != null) {
					loanFromBankAll += devings.getDeveCostTotal();
				}
				if (devings.getSaleIncomeTotal() != null) {
					advaSaleIncomeAll += devings.getSaleIncomeTotal();
				}
				if (devings.getUnSaleIncome() != null) {
					expectSaleIncomeAll += devings.getUnSaleIncome();
				}
			}
			CompRunEstaDevedProj allRe = new CompRunEstaDevedProj();
			allRe.setProjName("合计");
			allRe.setInveRate(inveRateAll);
			allRe.setFloorArea(floorAreaAll);
			allRe.setGrossAreaTotal(buildAreaAll);
			allRe.setSaleArea(totalInveAmtAll);
			allRe.setUnSaleArea(ownFundsAll);
			allRe.setDeveCostTotal(loanFromBankAll);
			allRe.setSaleIncomeTotal(advaSaleIncomeAll);
			allRe.setUnSaleIncome(expectSaleIncomeAll);
			deveds.add(allRe);
		}
		return deveds;
	}
	
	
	public List<CompRunEstaLand> sortEatateLand(List<CompRunEstaLand> lands) {
		if (!CollectionUtils.isEmpty(lands)) {
			double inveRateAll = 0.0;
			double landAreaAll = 0.0;
			double buildAreaAll = 0.0;
			double payableAmtAll = 0.0;
			double unPaidAmtAll = 0.0;
			for (CompRunEstaLand devings : lands) {
				if (devings.getInvestRate() != null) {
					inveRateAll += devings.getInvestRate();
				}
				if (devings.getLandArea() != null) {
					landAreaAll += devings.getLandArea();
				}
				if (devings.getBuildPlanArea() != null) {
					buildAreaAll += devings.getBuildPlanArea();
				}
				if (devings.getPayableLandCost() != null) {
					payableAmtAll += devings.getPayableLandCost();
				}
				if (devings.getUnpaidLandCost() != null) {
					unPaidAmtAll += devings.getUnpaidLandCost();
				}
			}
			CompRunEstaLand allRe = new CompRunEstaLand();
			allRe.setProjName("合计");
			allRe.setInvestRate(inveRateAll);
			allRe.setLandArea(landAreaAll);
			allRe.setBuildPlanArea(buildAreaAll);
			allRe.setPayableLandCost(buildAreaAll);
			allRe.setUnpaidLandCost(unPaidAmtAll);
			lands.add(allRe);
		}
		return lands;
	}
	
}
