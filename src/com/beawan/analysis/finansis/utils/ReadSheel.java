package com.beawan.analysis.finansis.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

public class ReadSheel implements Runnable {

	private String[] StData;

	long start = 0;
	private String FileName2;

	public ReadSheel() {

	}

	public ReadSheel(String[] x, String File2, long asd) {
		StData = x;
		start = asd;
		FileName2 = File2;
	}

	public void run() {

		File cuFile = new File(FileName2);
		for (int i = 0; i < StData.length; i++) {
			if (StData[i].equals("t")) {
				break;
			}
			// System.out.println(x[i]);
			String t2 = xls2String(cuFile, 0, StData[i], 0, 3);
			try {
				String[] re = t2.split("!!");
				re[1].isEmpty();
				// throw new Exception();

			}
			// System.out.println(t2);
			catch (Exception e) {
				// re[0]=StData[i];
				System.out.println("请注意\"" + StData[i] + "\"这项");
			}
			System.out.println(t2);
		}
		System.out.println("耗时：" + (System.currentTimeMillis() - start) + "毫秒");
	}

	public String[] readText(String filePath, String a) {

		String[] data = new String[300];
		for (int i = 0; i < 300; i++) {
			data[i] = "t";
		}
		int k = 0;
		String cut = String.valueOf(a);
		try {
			String encoding = "utf-8";
			File file = new File(filePath);
			if (file.isFile() && file.exists()) {
				InputStreamReader read = new InputStreamReader(new FileInputStream(file), encoding);
				BufferedReader bufferedReader = new BufferedReader(read);
				String lineTxt = null;
				while ((lineTxt = bufferedReader.readLine()) != null) {

					String[] midd = lineTxt.split(cut);
					int size = midd.length;
					for (int i = 0; i < size; i++) {
						if (isChinese(midd[i])) {
							data[k] = midd[i];

							k++;
						}
					}
				}
				read.close();
			} else {
				System.out.println("找不到指定的文件");
			}
		} catch (Exception e) {
			System.out.println("读取文件内容出错");
			e.printStackTrace();
		}

		return data;
	}

	private boolean isChinese(char c) {
		Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
		if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
				|| ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
				|| ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
				|| ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B
				|| ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
				|| ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS
				|| ub == Character.UnicodeBlock.GENERAL_PUNCTUATION) {
			return true;
		}
		return false;
	}

	public boolean isChinese(String strName) {
		char[] ch = strName.toCharArray();
		for (int i = 0; i < ch.length; i++) {
			char c = ch[i];
			if (isChinese(c)) {
				return true;
			}
		}
		return false;
	}

	public String xls2String(File file, int cu, String curr, int a, int b)// 返回某一特征编号的所有数据，参数表：目标文件，特征编号，数据1所在单元格，数据2所在单元格
	{
		String result = "";
		try {
			FileInputStream fis = new FileInputStream(file);
			StringBuilder name = new StringBuilder();
			StringBuilder amount = new StringBuilder();
			jxl.Workbook rwb = Workbook.getWorkbook(fis);
			Sheet[] sheet = rwb.getSheets();
			for (int i = 0; i < sheet.length; i++) {
				Sheet rs = rwb.getSheet(i);
				for (int j = 0; j < rs.getRows(); j++) {
					Cell[] cells = rs.getRow(j);
					if (cells[cu].getContents().trim().equals(curr)) {
						// 可在此处添加一些参数获取更多的单元格数据
						String temp1 = new String(cells[a].getContents());
						String temp2 = new String();
						String temp3 = new String();
						String temp4 = new String();
						name.append(temp1 + "#"); // a变量在此用到
						try {
							temp2 = cells[b].getContents();
							temp3 = cells[b - 1].getContents();
							temp4 = cells[b - 2].getContents();
							if (temp2.equals("")) {
								temp2 = "0";
							}
							if (temp3.equals("")) {
								temp3 = "0";
							}
							if (temp4.equals("")) {
								temp4 = "0";
							}
							temp2 = String
									.valueOf(Double.valueOf(temp2) + Double.valueOf(temp3) - Double.valueOf(temp4));
						} catch (Exception e) {
							amount.append("0#");
							continue;
						}
						if (temp2.equals("")) {
							amount.append(0 + "#");
						} else {
							amount.append(temp2 + "#");
						}
					}
				}
			}
			fis.close();
			result = name.toString() + "!!" + amount.toString();
			// System.out.println(name.toString().trim()+"!!"+amount.toString().trim());

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	// 1 "" 1 9
	public String betterXls(File file, int cu, String curr, int a, int b) {
		String data = new String();
		try {
			FileInputStream fis = new FileInputStream(file);
			StringBuilder name = new StringBuilder();
			StringBuilder amount = new StringBuilder();
			jxl.Workbook rwb = Workbook.getWorkbook(fis);
			Sheet[] sheet = rwb.getSheets();
			for (int i = 0; i < sheet.length; i++) {
				boolean find = false;
				Sheet rs = rwb.getSheet(i);
				String num = new String();
				for (int j = 0; j < rs.getRows(); j++) {
					String te = new String();
					Cell[] cells = rs.getRow(j);
					te = cells[cu].getContents().trim();
					if (te.equals(curr)) {
						find = true;
						// System.out.println(cells[0].getContents().trim());
						num = cells[0].getContents().trim();
						// num=mid[0];
						break;
					}
				}
				boolean ha = true;
				for (int j = 0; j < rs.getRows(); j++) {
					// int w=0;

					if (j != rs.getRows() - 1) {
						// boolean ha=true;
						Cell[] yoo1 = rs.getRow(j);
						Cell[] yoo2 = rs.getRow(j + 1);
						String oo1 = new String(yoo1[0].getContents().trim());
						String oo2 = new String(yoo2[0].getContents().trim());
						try {
							Cell[] yoo3 = rs.getRow(j + 2);
							String oo3 = new String(yoo3[0].getContents().trim());
							String[] x1 = oo2.split("\\.");
							String[] x2 = oo3.split("\\.");
							if (oo1.split("\\.").length == 1 && oo2.split("\\.").length == 2
									&& (!x1[0].equals(x2[0]))) {
								ha = true;
								continue;
							}
						} catch (Exception e) {

						}
						if (!(oo1.equals(oo2))) {
							ha = true;
							if (oo1.split("\\.").length == 2 && oo2.split("\\.").length == 3) {
								ha = true;
								continue;
							}

						}

						if (oo1.equals(oo2) && ha && oo2.split("\\.").length != 1) {
							ha = false;
							continue;
						}

					}
					Cell[] cells = rs.getRow(j);
					String mid[] = new String[2];
					String tnum = new String();
					// String key=new String(".");
					try {
						// System.out.println(cells[0].getContents().trim());
						mid = cells[0].getContents().trim().split("\\.");
						tnum = mid[0];

					} catch (Exception e) {
						// e.printStackTrace();
						tnum = cells[0].getContents().trim();
					}

					if (tnum.equals(num)) {
						String temp1 = new String(cells[a].getContents());
						String temp2 = new String();
						String temp3 = new String();
						String temp4 = new String();
						name.append(temp1 + "#");
						try {
							if (b == 9) {
								temp2 = cells[b].getContents();
								temp3 = cells[b - 1].getContents();
								temp4 = cells[b - 2].getContents();
								if (temp2.equals("")) {
									temp2 = "0";
								}
								if (temp3.equals("")) {
									temp3 = "0";
								}
								if (temp4.equals("")) {
									temp4 = "0";
								}
								temp2 = String
										.valueOf(Double.valueOf(temp2) + Double.valueOf(temp3) - Double.valueOf(temp4));
							} else if (b == 10) {
								temp2 = cells[b].getContents();
								temp3 = cells[b - 1].getContents();
								temp4 = cells[b - 2].getContents();
								if (temp2.equals("")) {
									temp2 = "0";
								}
								if (temp3.equals("")) {
									temp3 = "0";
								}
								if (temp4.equals("")) {
									temp4 = "0";
								}
								temp2 = String
										.valueOf(Double.valueOf(temp2) + Double.valueOf(temp4) - Double.valueOf(temp3));
							} else if (b == 8) {
								temp2 = cells[b].getContents();
							}
						} catch (Exception e) {
							amount.append("0#");
							continue;
						}
						if (temp2.equals("")) {
							amount.append("0#");
							continue;
						}
						amount.append(temp2 + "#");
						// w++;
					}

				}
				if (find) {
					break;
				}
			}
			fis.close();
			data = name.toString() + "!!" + amount.toString();
			// System.out.println(name.toString().trim()+"!!"+amount.toString().trim());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return data;
	}

	public String twoYearJud(String ye1, String ye2)// 该函数需要与xls2String,getResult两个函数配合使用，用于处理前两个函数返回的长字符串
	{
		String warn = new String();
		boolean ju = true;
		String yeData1[] = ye1.split("#");
		int yeSize1 = (yeData1.length) / 2;
		String yeData2[] = ye2.split("#");
		int yeSize2 = (yeData2.length) / 2;
		String[] yeName1 = new String[yeSize1];
		String[] yeName2 = new String[yeSize2];
		String[] yeAmount1 = new String[yeSize1];
		String[] yeAmount2 = new String[yeSize2];
		for (int i = 0; i < yeSize1; i++) {
			yeName1[i] = yeData1[2 * i];
			yeAmount1[i] = yeData1[2 * i + 1];
		}
		for (int i = 0; i < yeSize2; i++) {
			yeName2[i] = yeData2[2 * i];
			yeAmount2[i] = yeData2[2 * i + 1];
		}
		for (int i = 0; i < yeSize1; i++) {
			if (yeName1[i].equals("其它") || yeName1[i].equals("总计")) {
				continue;
			}
			for (int j = 0; j < yeSize2; j++) {
				if (yeName1[i].equals(yeName2[j])) {
					double per = Math.abs(1 - (Double.valueOf(yeAmount1[i]) / Double.valueOf(yeAmount2[j])));// 上下浮动，取绝对值
					if (per <= 0.05)// 此处用于判定是否浮动变化小于5%
					{
						ju = false;
						warn = warn + yeName1[i] + "#";
					}
				}
			}
		}
		if (ju) {
			warn = "safe";
		}
		return warn;
	}

	public String solveStr(String result)// 用于处理单单一行excel的数据，将xls2String返回的字符串处理为“第一个单元格＃第二个单元格”
	{
		String data = "";
		String[] mid = result.split("!!");
		String[] mid2;
		mid2 = mid[0].split("#");
		data += mid2[0] + "#";
		mid2 = mid[1].split("#");
		data += formatFloatNumber(Double.valueOf(mid2[0]) / 10000);
		return data;
	}

	public double getAllNoData(String result)// 得到某一大项的总数
	{
		String[] mid = result.split("!!");
		String[] mid2;
		mid2 = mid[1].split("#");
		double data = Double.valueOf(mid2[0]) / 10000;
		return data;
	}

	public String getResult(String result)// 处理xls2String返回的字符串，并按照“单元格1＃单元格2＃单元格1＃单元格2”的格式排列好所有数据
	{
		String data = new String();
		int i = 0, size = 0;
		double x = 0.0;
		String[] mid1 = result.split("!!");
		String[] name = mid1[0].split("#");
		String[] amount = mid1[1].split("#");
		if (name.length == 1) {
			data = name[0] + "#" + formatFloatNumber(Double.valueOf(amount[0]) / 10000) + "#" + "总计" + "#"
					+ formatFloatNumber(Double.valueOf(amount[0]) / 10000) + "#";
			return data;
		}
		i = amount.length;
		double amo[] = new double[amount.length];
		double amo2[] = new double[amount.length];
		for (int j = 0; j < i; j++) {
			amo[j] = Double.valueOf(amount[j]);
			amo2[j] = Double.valueOf(amount[j]);
			// System.out.print(name[j]);
			// System.out.println(amo2[j]);

		}
		// amo2=amo;
		Double[] midamount = Sor(amo);
		double sum = 0.0;
		size = midamount.length;
		for (int j = 0; j < size; j++) {
			sum += midamount[j];
			// System.out.println(midamount[j]);
		}
		for (int j = 0; j < size; j++) {
			for (int k = 0; k < i; k++) {
				if (amo2[k] == midamount[j]) {
					data = data + name[k] + "#";// +midamount[j]+"#";
				}
			}
			for (int k = 0; k < i; k++) {
				// x=midamount[j]/10000;
				if (Math.floor(Double.valueOf(amount[k])) == Math
						.floor(Double.valueOf(formatFloatNumber(midamount[j])))) {
					data = data + formatFloatNumber(Double.valueOf(amount[k]) / 10000) + "#";
				}
			}
		}
		double els = Double.valueOf(amount[0]) - sum;
		x = els / 10000;
		if (x >= 0.001) {
			// System.out.println(x);
			data = data + "其它" + "#" + formatFloatNumber(x) + "#";
		}
		data = data + "总计" + "#" + formatFloatNumber(Double.valueOf(amount[0]) / 10000) + "#";
		return data;
	}

	public Double[] Sor(double data[])// 排列目标单元格列中的数据，取出一个大于总量80%的数组
	{
		// double []max;
		int i = data.length;
		int size = 0;
		double midAmount = 0.0;
		for (int j = 1; j < i; j++) {
			for (int k = 1; k < i; k++) {
				// System.out.println(data[k]);
				if (data[j] > data[k]) {
					double midd = 0.0;
					midd = data[j];
					data[j] = data[k];
					data[k] = midd;
				}
			}
		}
		while (midAmount / data[0] <= 0.8 || size < 3) {

			size++;

			try {
				midAmount += data[size];
			} catch (Exception e) {
				size--;
				break;
			}
			if (data[size] == 0) {
				size--;
				break;
			}
			if (size == 3) {
				break;
			}
		}
		Double[] max = new Double[size];
		for (int j = 0; j < size; j++) {
			max[j] = data[j + 1];
			// System.out.println(max[j]);
		}
		return max;
	}

	public String formatFloatNumber(double value) // 格式化输出double，防止出现科学计数法
	{
		if (value != 0.00) {
			java.text.DecimalFormat df = new java.text.DecimalFormat("##0.00");
			return df.format(value);
		} else {
			return "0.00";
		}

	}

}
