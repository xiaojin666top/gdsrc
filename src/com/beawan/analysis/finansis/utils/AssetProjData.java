package com.beawan.analysis.finansis.utils;

import java.util.ArrayList;
import java.util.List;

public class AssetProjData {
	
	 /**
		 * @Description (固定资产投资估算表)
		 * @return
		 */
		public List<String[]> projTotalInve(){
			 List<String[]> result = new ArrayList<String[]>();
			 
			 String[] one= {"一","静态投资","12000","4000","3000","1000","20000",""};
			 String[] oneone= {"1","工程费用","10800","3000","2500","800","17100",""};
			 String[] onetwo= {"","主要生产项目","9120","2550","1880","660","14210",""};
			 String[] onethree= {"","辅助生产项目","500","230","250","20","1000",""};
			 String[] onefour= {"","公用工程","1180","220","370","120","1890",""};
			 String[] onefive= {"2","其它费用","700","640","330","120","1500",""};
			 String[] onesix= {"3","基本预备费","500","360","170","80","1400",""};
			 String[] oneseven= {"","不可预见费","400","230","115","45","980",""};
			 String[] oneeight= {"","价差费","100","130","55","35","420",""};
			 String[] onenine= {"二","动态投资","2000","1400","1200","200","4800",""};
			 String[] oneten= {"1","建设期利息","800","520","430","135","1885",""};
			 String[] oneeleven= {"2","汇率变动部分","300","260","200","25","785",""};
			 String[] onetwe= {"3","固定资产投资方向调节税","300","200","180","15","695",""};
			 String[] two= {"4","国家新批准的税费","400","280","250","10","940",""};
			 String[] three= {"5","涨价预备金","200","140","140","15","445",""};
			 String[] four= {"三","固定资产投资","14000","5400","4200","1200","24800",""};
			 
			 result.add(one);
			 result.add(oneone);
			 result.add(onetwo);
			 result.add(onethree);
			 result.add(onefour);
			 result.add(onefive);
			 result.add(onesix);
			 result.add(oneseven);
			 result.add(oneeight);
			 result.add(onenine);
			 result.add(oneten);
			 result.add(oneeleven);
			 result.add(onetwe);
			 result.add(two);
			 result.add(three);
			 result.add(four);
			 return result;
		 }
		
		 /**
			 * @Description (固定资产投资估算表)
			 * @return
			 */
			public List<String[]> assetCateInve(){
				 List<String[]> result = new ArrayList<String[]>();
				 
				 String[] one= {"一","固定资产","20050"};
				 String[] oneone= {"1","工程费用","17100"};
				 String[] onetwo= {"","主要生产项目","14500"};
				 String[] onethree= {"","辅助生产项目","2000"};
				 String[] onefour= {"","公用工程","600"};
				 String[] onefive= {"2","其它形成固定资产费用","1500"};
				 String[] onesix= {"3","基本预备费","600"};
				 String[] oneseven= {"","不可预见费","400"};
				 String[] oneeight= {"","价差费","200"};
				 String[] onenine= {"4","建设期利息","450"};
				 String[] oneten= {"5","汇率变动部分","100"};
				 String[] oneeleven= {"6","固定资产投资方向调节税","100"};
				 String[] onetwe= {"7","国家新批准的税费","100"};
				 String[] two= {"8","涨价预备金","100"};
				 String[] three= {"二","无形资产","4000"};
				 String[] four= {"1","土地使用权","2300"};
				 String[] fourone= {"2","专利权","500"};
				 String[] fourtwo= {"3","非专利技术","400"};
				 String[] fourthree= {"4","商标权","600"};
				 String[] fourfour= {"5","其它无形资产","200"};
				 String[] fourfive= {"三","长期待摊费用","2500"};
				 String[] fiveone= {"1","开办费","500"};
				 String[] fivetwo= {"","咨询费","50"};
				 String[] fivethree= {"","人员培训费(生产职工)","150"};
				 String[] fivefour= {"","筹建人员工资","300"};
				 String[] fivefive= {"","其它开办费","0"};
				 String[] fivesix= {"2","样品样机购置费","1000"};
				 String[] fiveseven= {"3","农业开荒费","0"};
				 String[] fiveeight= {"4","非常损失","400"};
				 String[] fivenine= {"5","其它长期待摊费用","600"};
				 String[] fiveten= {"四","固定资产投资","26550"};
				 
				 result.add(one);
				 result.add(oneone);
				 result.add(onetwo);
				 result.add(onethree);
				 result.add(onefour);
				 result.add(onefive);
				 result.add(onesix);
				 result.add(oneseven);
				 result.add(oneeight);
				 result.add(onenine);
				 result.add(oneten);
				 result.add(oneeleven);
				 result.add(onetwe);
				 result.add(two);
				 result.add(three);
				 result.add(four);
				 result.add(fourone);
				 result.add(fourtwo);
				 result.add(fourthree);
				 result.add(fourfour);
				 result.add(fourfive);
				 
				 result.add(fiveone);
				 result.add(fivetwo);
				 result.add(fivethree);
				 result.add(fivefour);
				 result.add(fivefive);
				 
				 result.add(fivesix);
				 result.add(fiveseven);
				 result.add(fiveeight);
				 result.add(fivenine);
				 result.add(fiveten);
				 return result;
			 }
		
			
			
			 /**
			 * @Description (固定资产投资估算表)
			 * @return
			 */
			public List<String[]> invePlan(){
				 List<String[]> result = new ArrayList<String[]>();
				 
				 String[] one= {"一","项目总投资","23000","9200","9200","2300","2300"};
				 String[] oneone= {"1","固定资产投资","20000","8000","8000","2000","2000"};
				 String[] onetwo= {"1.1","静态投资","18000","7200","7200","1800","1800"};
				 String[] onethree= {"1.2","动态投资","2000","800","800","200","200"};
				 String[] onefour= {"","建设期利息","700","280","280","70","70"};
				 String[] onefive= {"","汇率变动部分","250","100","100","25","25"};
				 String[] onesix= {"","投资方向调节税","100","40","40","10","10"};
				 String[] oneseven= {"","国家新批准的税费","300","120","120","30","30"};
				 String[] oneeight= {"","涨价准备金","700","280","280","70","70"};
				 String[] onenine= {"2","流动资金","3000","1200","1200","300","300"};
				 String[] oneten= {"","其中:铺底流动资金","1500","600","600","150","150"};
				 String[] oneeleven= {"二","资金筹措","25000","10000","10000","2500","2500"};
				 String[] onetwe= {"1","项目资本金来源","20000","8000","8000","2000","2000"};
				 String[] two= {"1.1","企业自有资金","16000","6400","6400","1600","1600"};
				 String[] three= {"1.1.1","企业现有货币资金","10000","4000","4000","1000","1000"};
				 String[] four= {"1.1.2","企业未来经营收益","4000","1200","1200","400","400"};
				 String[] fourone= {"1.1.3","企业资产变现","2000","800","800","200","200"};
				 String[] fourtwo= {"1.2","股东追加投资","0","0","0","0","0"};
				 String[] fourthree= {"2","项目长期负债","3000","400","400","300","300"};
				 String[] fourfour= {"2.1","长期借款","3000","400","400","300","300"};
				 String[] fourfive= {"2.2","长期债券","","","","",""};
				 String[] fiveone= {"2.3","融资租赁","","","","",""};
				 String[] fivetwo= {"2.4","补偿贸易","","","","",""};
				 String[] fivethree= {"3","项目短期负债","2000","800","800","200","200"};
				 String[] fivefour= {"3.1","流动资金借款","2000","800","800","200","200"};
				 String[] fivefive= {"3.2","其它短期借款","","","","",""};
				 String[] fivesix= {"3.3","其它资金","","","","",""};
				 
				 result.add(one);
				 result.add(oneone);
				 result.add(onetwo);
				 result.add(onethree);
				 result.add(onefour);
				 result.add(onefive);
				 result.add(onesix);
				 result.add(oneseven);
				 result.add(oneeight);
				 result.add(onenine);
				 result.add(oneten);
				 result.add(oneeleven);
				 result.add(onetwe);
				 result.add(two);
				 result.add(three);
				 result.add(four);
				 result.add(fourone);
				 result.add(fourtwo);
				 result.add(fourthree);
				 result.add(fourfour);
				 result.add(fourfive);
				 
				 result.add(fiveone);
				 result.add(fivetwo);
				 result.add(fivethree);
				 result.add(fivefour);
				 result.add(fivefive);
				 result.add(fivesix);
				 return result;
			 }
			
			
			 /**
			 * @Description (总成本费用估算表)
			 * @return
			 */
			public List<String[]> costReckon(){
				 List<String[]> result = new ArrayList<String[]>();
				 
				 String[] one= {"一","产品制造成本","12000","5600","5600","400","400"};
				 String[] oneone= {"1","直接材料","8000","4000","4000","",""};
				 String[] onetwo= {"1.1","原材料","6000","3000","3000","",""};
				 String[] onethree= {"1.2","辅助材料","500","250","250","",""};
				 String[] onefour= {"1.3","备品备件","","","","",""};
				 String[] onefive= {"1.4","外购半成品","1000","500","500","",""};
				 String[] onesix= {"1.5","外购燃料动力","","","","",""};
				 String[] oneseven= {"1.6","包装物","500","250","250","",""};
				 String[] oneeight= {"1.7","其它直接材料","","","","",""};
				 String[] onenine= {"2","直接工资及福利费","1000","300","300","200","200"};
				 String[] oneten= {"3","其它直接支出","1000","300","300","200","200"};
				 String[] oneeleven= {"4","制造费用","2000","1000","1000","",""};
				 String[] onetwe= {"4.1","折旧","700","350","350","",""};
				 String[] two= {"4.2","维简费","400","200","200","",""};
				 String[] three= {"4.3","租赁费","","","","",""};
				 String[] four= {"4.4","修理费","900","450","450","",""};
				 String[] fourone= {"4.5","其它制造费用","","","","",""};
				 String[] fourtwo= {"二","管理费用","1300","300","300","350","350"};
				 String[] fourthree= {"1.1","无形资产摊销","200","50","50","50","50"};
				 String[] fourfour= {"1.2","长期待摊费用摊销","","","","",""};
				 String[] fourfive= {"1.3","开办费摊销","300","100","100","50","50"};
				 String[] fiveone= {"1.4","其它管理费用","800","150","150","350","350"};
				 String[] fivetwo= {"三","财务费用","1300","400","400","250","250"};
				 String[] fivethree= {"1.1","短期负债利息净支出","1300","400","400","250","250"};
				 String[] fivefour= {"1.2","长期负债利息净支出","","","","",""};
				 String[] fivefive= {"1.3","其它财务费用","","","","",""};
				 String[] fivesix= {"四","销售费用","700","100","100","350","150"};
				 String[] fiveseven= {"五","经营成本","5000","500","500","2000","2000"};
				 String[] fiveeight= {"六","固定成本","4000","1000","1000","1000","1000"};
				 String[] fivenine= {"七","可变成本","2000","300","300","700","700"};
				 
				 result.add(one);
				 result.add(oneone);
				 result.add(onetwo);
				 result.add(onethree);
				 result.add(onefour);
				 result.add(onefive);
				 result.add(onesix);
				 result.add(oneseven);
				 result.add(oneeight);
				 result.add(onenine);
				 result.add(oneten);
				 result.add(oneeleven);
				 result.add(onetwe);
				 result.add(two);
				 result.add(three);
				 result.add(four);
				 result.add(fourone);
				 result.add(fourtwo);
				 result.add(fourthree);
				 result.add(fourfour);
				 result.add(fourfive);
				 
				 result.add(fiveone);
				 result.add(fivetwo);
				 result.add(fivethree);
				 result.add(fivefour);
				 result.add(fivefive);
				 result.add(fivesix);
				 
				 result.add(fiveseven);
				 result.add(fiveeight);
				 result.add(fivenine);
				 return result;
			 }
			
			
			 /**
			 * @Description (损益和利润分配表)
			 * @return
			 */
			public List<String[]> incomeProfit(){
				 List<String[]> result = new ArrayList<String[]>();
				 
				 String[] one= {"1","产吕销售(营业)收入","40000","0.0","0.0","30000","10000"};
				 String[] oneone= {"2","销售税金及附加","1600","0.0","0.0","1200","400"};
				 String[] onetwo= {"3","增值税","4000","0.0","0.0","2000","2000"};
				 String[] onethree= {"4","总成本费用","25000","10000","10000","2500","2500"};
				 String[] onefour= {"5","销售利润","9400","-10000","-10000","24300","5100"};
				 String[] onefive= {"6","其它业务利润","3000","1500","1500","",""};
				 String[] onesix= {"7","对外投资收益","0.0","0.0","0.0","0.0","0.0"};
				 String[] oneseven= {"8","营业外净收入","0.0","0.0","0.0","0.0","0.0"};
				 String[] oneeight= {"9","利润总额","12400","-8500","-8500","24300","5100"};
				 String[] onenine= {"10","弥补以前年度亏损","0.0","0.0","0.0","0.0","0.0"};
				 String[] oneten= {"11","应纳税所得额","12400","-8500","-8500","24300","5100"};
				 String[] oneeleven= {"12","所得税","2000","0.0","0.0","1500","500"};
				 String[] onetwe= {"13","税后利润","10400","-8500","-8500","22800","4600"};
				 String[] two= {"14","提取法定盈余公积金","1000","0.0","0.0","600","400"};
				 String[] three= {"15","提取公益金","","","","",""};
				 String[] four= {"16","提取任意盈余公积金","","","","",""};
				 String[] fourone= {"17","可供分配利润","9400","-8500","-8500","22200","4200"};
				 String[] fourtwo= {"18","应付利润","4000","-4500","-4500","11000","2000"};
				 String[] fourthree= {"19","未分配利润","5400","-4000","-4000","11200","2200"};
				 String[] fourfour= {"20","累计未分配利润","5400","-4000","-4000","11200","2200"};
				 String[] fourfive= {"21","可还款利润","5400","-4000","-4000","11200","2200"};
				 
				 result.add(one);
				 result.add(oneone);
				 result.add(onetwo);
				 result.add(onethree);
				 result.add(onefour);
				 result.add(onefive);
				 result.add(onesix);
				 result.add(oneseven);
				 result.add(oneeight);
				 result.add(onenine);
				 result.add(oneten);
				 result.add(oneeleven);
				 result.add(onetwe);
				 result.add(two);
				 result.add(three);
				 result.add(four);
				 result.add(fourone);
				 result.add(fourtwo);
				 result.add(fourthree);
				 result.add(fourfour);
				 result.add(fourfive);
				 
				 return result;
			 }
			
			
			 /**
			 * @Description (损益和利润分配表)
			 * @return
			 */
			public List<String[]> projPay(){
				List<String[]> result = new ArrayList<String[]>();
				 
				 String[] one= {"一","借款偿还","0","0","6000","2000"};
				 String[] oneone= {"1","年初借款累计","0","3000","6000","2000"};
				 String[] onetwo= {"2","年内借款支用","3000","3000","2000","2000"};
				 String[] onethree= {"3","年内借款应计利息","250","500","700","120"};
				 String[] onefour= {"3.1","计入投资","0","0","0","0"};
				 String[] onefive= {"3.2","计入财务费用","250","500","700","120"}; 
				 String[] onesix= {"4","本年还本","0","0","6000","4000"};
				 String[] oneseven= {"5","本年付息","250","500","700","120"};
				 String[] oneeight= {"6","年末借款累计","3000","6000","2000","0"};
				 String[] onenine= {"二","还款资金来源","0","0","6000","4000"};
				 String[] oneten= {"1","可还款利润","0","0","6000","4000"};
				 String[] oneeleven= {"2","可还款折旧或维简费","0","0","0","0"};
				 String[] onetwe= {"3","摊销费","0","0","0","0"};
				 String[] two= {"4","其它资金","0","0","0","0"};
				
				 result.add(one);
				 result.add(oneone);
				 result.add(onetwo);
				 result.add(onethree);
				 result.add(onefour);
				 result.add(onefive);
				 result.add(onesix);
				 result.add(oneseven);
				 result.add(oneeight);
				 result.add(onenine);
				 result.add(oneten);
				 result.add(oneeleven);
				 result.add(onetwe);
				 result.add(two);
				 
				 return result;
			}
			
			 /**
			 * @Description (项目现金流量表)
			 * @return
			 */
			public List<String[]> projCashFlow(){
				List<String[]> result = new ArrayList<String[]>();
				 
				 String[] one= {"一","现金流入","5000","4600","28000","9300"};
				 String[] oneone= {"1","产品销售(营业)收入","0.0","0.0","25800","7700"};
				 String[] onetwo= {"2","回收固定资产余值","0.0","0.0","",""};
				 String[] onethree= {"3","回收流动资金","4500","3600","700","500"};
				 String[] onefour= {"4","其它现金流入","500","1000","1500","1100"};
				 String[] onefive= {"二","现金流出","15000","16000","5300","4800"}; 
				 String[] onesix= {"1","静态投资","9600","9600","1200","1200"};
				 String[] oneseven= {"2","投资方向调节税","400","400","300","300"};
				 String[] oneeight= {"3","国家新批准的税费","100","100","0","0"};
				 String[] onenine= {"4","涨价预备金","","","",""};
				 String[] oneten= {"5","流动资金","2000","2000","1500","1500"};
				 String[] oneeleven= {"6","经营成本","2400","2400","1000","1000"};
				 String[] onetwe= {"7","销售税金及附加","200","200","600","600"};
				 String[] two= {"8","增值税","300","300","",""};
				 String[] three= {"9","所得税","","","500","200"};
				 String[] four= {"10","其它现金流出","","1000","200",""};
				 String[] five= {"三","净现金流量","-10000","-14000","24700","4500"};
				 String[] six= {"四","累计净现金流量","10000","-24000","3700","8200"};
				 
				 result.add(one);
				 result.add(oneone);
				 result.add(onetwo);
				 result.add(onethree);
				 result.add(onefour);
				 result.add(onefive);
				 result.add(onesix);
				 result.add(oneseven);
				 result.add(oneeight);
				 result.add(onenine);
				 result.add(oneten);
				 result.add(oneeleven);
				 result.add(onetwe);
				 result.add(two);
				 
				 result.add(three);
				 result.add(four);
				 result.add(five);
				 result.add(six);
				 
				 return result;
			}
}
