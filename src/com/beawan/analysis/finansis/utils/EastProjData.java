package com.beawan.analysis.finansis.utils;

import java.util.ArrayList;
import java.util.List;

public class EastProjData {
	
	 /**
	 * @Description (项目总投资估算表)
	 * @return
	 */
	public List<String[]> projTotalInve(){
		 List<String[]> result = new ArrayList<String[]>();
		 
		 String[] one= {"1","开发建设投资","170000",""};
		 String[] oneone= {"1.1","土地费用","74332",""};
		 String[] onetwo= {"1.2","前期工程费用","432","工程总造价0.2%"};
		 String[] onethree= {"1.3","基础设施建设费","12567",""};
		 String[] onefour= {"1.4","建筑安装工程费","25534",""};
		 String[] onefive= {"1.5","公共配套设施建设费","15428",""};
		 String[] onesix= {"1.6","开发间接费用","821",""};
		 String[] oneseven= {"1.7","管理费用","13421",""};
		 String[] oneeight= {"1.8","财务费用","11254",""};
		 String[] onenine= {"1.9","销售费用","3329",""};
		 String[] oneten= {"1.10","开发期税费","129",""};
		 String[] oneeleven= {"1.11","其他费用","2169",""};
		 String[] onetwe= {"1.12","不可预见费","1000",""};
		 
		 String[] two= {"2","经营资金","50000",""};
		 
		 String[] three= {"3","项目总投资","220000",""};
		 
		 result.add(one);
		 result.add(oneone);
		 result.add(onetwo);
		 result.add(onethree);
		 result.add(onefour);
		 result.add(onefive);
		 result.add(onesix);
		 result.add(oneseven);
		 result.add(oneeight);
		 result.add(onenine);
		 result.add(oneten);
		 result.add(oneeleven);
		 result.add(onetwe);
		 result.add(two);
		 result.add(three);
		 return result;
	 }
	
	/**
	 * @Description (１－１土地费用估算表)
	 * @return
	 */
	public List<String[]> landCost(){
		
		 List<String[]> result = new ArrayList<String[]>();
		
		 String[] one= {"1","土地出让金","4353",""};
		 String[] two= {"2","征地费","8009",""};
		 String[] three= {"3","拆迁安置补偿费","12079",""};
		 String[] four= {"4","土地转让费","14387",""};
		 String[] five= {"5","土地租用费","43321",""};
		 String[] six= {"6","土地投资折价","4321",""};
		 String[] seven= {"7","其他","0",""};
		 String[] eight= {"8","合计","74332",""};
		 
		 result.add(one);
		 result.add(two);
		 result.add(three);
		 result.add(four);
		 result.add(five);
		 result.add(six);
		 result.add(seven);
		 result.add(eight);
		 
		 return result;
	}
	
	/**
	 * @Description (１－2前期费用估算表)
	 * @return
	 */
	public List<String[]> frontCost(){
		
		 List<String[]> result = new ArrayList<String[]>();
		
		 String[] one= {"1","规划、设计、可研费","54",""};
		 String[] two= {"2","水文、地质勘察费","65",""};
		 String[] three= {"3","道路费","108",""};
		 String[] four= {"4","供水费","40",""};
		 String[] five= {"5","供电费","60",""};
		 String[] six= {"6","土地平整费","108",""};
		 String[] seven= {"7","其他","12",""};
		 String[] eight= {"8","合计","432",""};
		 
		 result.add(one);
		 result.add(two);
		 result.add(three);
		 result.add(four);
		 result.add(five);
		 result.add(six);
		 result.add(seven);
		 result.add(eight);
		 
		 return result;
	}
	
	/**
	 * @Description (１－3基础设施建设费估算表)
	 * @return
	 */
	public List<String[]> infrastructureCost(){
		
		 List<String[]> result = new ArrayList<String[]>();
		
		 String[] one= {"1","供电工程","903","200","1103"};
		 String[] two= {"2","供水工程","659","123","782"};
		 String[] three= {"3","供气工程","1003","243","1246"};
		 String[] four= {"4","供暖工程","564","120","684"};
		 String[] five= {"5","排污工程","700","140","840"};
		 String[] six= {"6","小区道路工程","2076","489","2565"};
		 String[] seven= {"7","路灯工程","424","103","527"};
		 String[] eight= {"8","小区绿化工程","903","240","1143"};
		 String[] nine= {"9","环卫设施","400","89","489"};
		 String[] ten= {"10","其他","243","122","365"};
		 String[] eve= {"11","合计","10078","2489","12567"};
		 
		 result.add(one);
		 result.add(two);
		 result.add(three);
		 result.add(four);
		 result.add(five);
		 result.add(six);
		 result.add(seven);
		 result.add(eight);
		 result.add(nine);
		 result.add(ten);
		 result.add(eve);
		 
		 return result;
	}

	
	/**
	 * @Description (１－4 建筑安装工程费用估算表)
	 * @return
	 */
	public List<String[]> installCost(){
		
		 List<String[]> result = new ArrayList<String[]>();
		
		 String[] one= {"绿园1期","57383","0","2427","0.05","3196","0","757","6381"};
		 String[] two= {"帝豪春苑","98992","0","4187","0.05","5513","0","1306","11007"};
		 String[] three= {"古丽苑2期","73248","0","3098","0.05","4079","0","966","8145"};
		 String[] four= {"合计","229623","","9713","","12790","","3031","25534"};
		 
		 result.add(one);
		 result.add(two);
		 result.add(three);
		 result.add(four);
		 
		 return result;
	}
	
	
	/**
	 * @Description (１－5公共配套设施建设费)
	 * @return
	 */
	public List<String[]> publicCost(){
		
		 List<String[]> result = new ArrayList<String[]>();
		
		 String[] one= {"1","居委会","1125",""};
		 String[] two= {"2","派出所","1567",""};
		 String[] three= {"3","托儿所","784",""};
		 String[] four= {"4","幼儿园","2008",""};
		 String[] five= {"5","公共厕所","190",""};
		 String[] six= {"6","停车场","4007",""};
		 String[] seven= {"7","其他","2164",""};
		 String[] eight= {"8","合计","15428",""};
		 
		 result.add(one);
		 result.add(two);
		 result.add(three);
		 result.add(four);
		 result.add(five);
		 result.add(six);
		 result.add(seven);
		 result.add(eight);
		 
		 return result;
	}
	
	/**
	 * @Description (１－10开发期税费估算表)
	 * @return
	 */
	public List<String[]> taxCost(){
		
		 List<String[]> result = new ArrayList<String[]>();
		
		 String[] one= {"1","土地使用税","403",""};
		 String[] two= {"2","市政支管线分摊费","50",""};
		 String[] three= {"3","供电贴费","64",""};
		 String[] four= {"4","用电权费","30",""};
		 String[] five= {"5","分散建设市政公用设施建设费","40",""};
		 String[] six= {"6","绿化建设费","150",""};
		 String[] seven= {"7","电话初装费","80",""};
		 String[] eight= {"8","合计","821",""};
		 
		 result.add(one);
		 result.add(two);
		 result.add(three);
		 result.add(four);
		 result.add(five);
		 result.add(six);
		 result.add(seven);
		 result.add(eight);
		 
		 return result;
	}
	
	/**
	 * @Description (１－11其他费用估算表)
	 * @return
	 */
	public List<String[]> otherCost(){
		
		 List<String[]> result = new ArrayList<String[]>();
		
		 String[] one= {"1","临时用地","1050",""};
		 String[] two= {"2","临建费","0",""};
		 String[] three= {"3","施工图预算或标底编制费","108","0.1%"};
		 String[] four= {"4","工程合同预算或标底审查费","103","合同预算造价的0.05%"};
		 String[] five= {"5","招标管理费","89","中标价格的0.06%"};
		 String[] six= {"6","总承包管理费","58",""};
		 String[] seven= {"7","合同公证费","20","合同价款的0.02%"};
		 String[] eight= {"8","施工执照费","30","0.1%"};
		 String[] nine= {"9","工程质量监督费","100","预算造价的0.25%"};
		 String[] ten= {"10","工程监理费","129","工程概算的0.6%"};
		 String[] eleven= {"11","竣工图编制费","140","设计费的7%"};
		 String[] twe= {"12","工程保险费","200","工程概算加成的0.2%"};
		 String[] threety= {"13","合计","2169",""};
		 
		 result.add(one);
		 result.add(two);
		 result.add(three);
		 result.add(four);
		 result.add(five);
		 result.add(six);
		 result.add(seven);
		 result.add(eight);
		 result.add(nine);
		 result.add(ten);
		 result.add(eleven);
		 result.add(twe);
		 result.add(threety);
		 return result;
	}
	
	
	/**
	 * @Description (2投资计划与资金筹措表)
	 * @return
	 */
	public List<String[]> inveFinanc(){
		
		 List<String[]> result = new ArrayList<String[]>();
		
		 String[] one= {"1","项目总投资","220000","60000","90000","70000"};
		 String[] two= {"1.1","开发建设投资","170000","47000","70000","55000"};
		 String[] three= {"1.2","经营资金","50000","13000","20000","15000"};
		 String[] four= {"2","资金筹措","220000","60000","90000","70000"};
		 String[] five= {"2.1","资本金","58000","14500","23500","19000"};
		 String[] six= {"2.2","预售收入","34000","4000","16000","14000"};
		 String[] seven= {"2.3","预租收入","22000","10000","7000","5000"};
		 String[] eight= {"2.4","其他收入","6000","1500","3500","2000"};
		 String[] nine= {"2.5","借贷资金","100000","30000","40000","30000"};
		 
		 result.add(one);
		 result.add(two);
		 result.add(three);
		 result.add(four);
		 result.add(five);
		 result.add(six);
		 result.add(seven);
		 result.add(eight);
		 result.add(nine);
		 return result;
	}
	
	
	/**
	 * @Description (开发产品成本归集)
	 * @return
	 */
	public List<String[]> developCost(){
		
		 List<String[]> result = new ArrayList<String[]>();
		
		 String[] one= {"1","土地费用","58986","","10743","","","","","","","","","","69729"};
		 String[] two= {"2","前期工程费用","378","","54","","","","","","","","","","432"};
		 String[] three= {"3","基础设施建设费","11307","","1260","","","","","","","","","","12567"};
		 String[] four= {"4","建筑安装工程费","23619","","1914","","","","","","","","","","25534"};
		 String[] five= {"5","公共配套设施建设费","13984","","1443","","","","","","","","","","15428"};
		 String[] six= {"6","开发间接费","673","","147","","","","","","","","","","821"};
		 String[] seven= {"7","管理费用","11308","","2112","","","","","","","","","","13421"};
		 String[] eight= {"8","财务费用","10538","","716","","","","","","","","","","11254"};
		 String[] nine= {"9","销售费用","3087","","242","","","","","","","","","","3329"};
		 String[] ten= {"10","开发期税费","119","","10","","","","","","","","","","129"};
		 String[] eleven= {"11","其他费用","1956","","213","","","","","","","","","","2169"};
		 String[] twe= {"12","不可预见费","906","","93","","","","","","","","","","1000"};
		 String[] threety= {"13","合计","136867","","33132","","","","","","","","","","170000"};
		 
		 result.add(one);
		 result.add(two);
		 result.add(three);
		 result.add(four);
		 result.add(five);
		 result.add(six);
		 result.add(seven);
		 result.add(eight);
		 result.add(nine);
		 result.add(ten);
		 result.add(eleven);
		 result.add(twe);
		 result.add(threety);
		 return result;
	}
	
	/**
	 * @Description 开发产品成本分摊估算)
	 * @return
	 */
	public List<String[]> assignCost(){
		
		 List<String[]> result = new ArrayList<String[]>();
			
		 String[] one= {"一","出售产品","	136867","0","36954","0","56115","0","43797"};
		 String[] two= {"1","住宅","136867","0","36954","0","56115","0","43797"};
		 String[] three= {"2","写字楼","","","","","","",""};
		 String[] four= {"3","商铺","","","","","","",""};
		 String[] five= {"4","其他","","","","","","",""};
		 String[] six= {"二","出租产品","","","","","","",""};
		 String[] seven= {"1","住宅","","","","","","",""};
		 String[] eight= {"2","写字楼","","","","","","",""};
		 String[] nine= {"3","商铺","","","","","","",""};
		 String[] ten= {"4","其他","","","","","","",""};
		 String[] eleven= {"三","自营产品","33132","0","8945","0","13584","0","10602"};
		 String[] twe= {"1","住宅","33132","0","8945","0","13584","0","10602"};
		 String[] threety= {"2","写字楼","","","","","","",""};
		 String[] fourty= {"3","商铺","","","","","","",""};
		 String[] fifty= {"4","其他","","","","","","",""};
		 String[] sixty= {"","产品分摊成本合计","170000","0","45900","0","69700","0","54400"};
		 
		 result.add(one);
		 result.add(two);
		 result.add(three);
		 result.add(four);
		 result.add(five);
		 result.add(six);
		 result.add(seven);
		 result.add(eight);
		 result.add(nine);
		 result.add(ten);
		 result.add(eleven);
		 result.add(twe);
		 result.add(threety);
		 result.add(fourty);
		 result.add(fifty);
		 result.add(sixty);
		 return result;
		
	}
	
	/**
	 * @Description (项目经营收入与经营税金及附加估算)
	 * @return
	 */
	public List<String[]>  incomeTax(){
		 List<String[]> result = new ArrayList<String[]>();
			
		 String[] one= {"一","经营收入","280000","80000","110000","90000"};
		 String[] two= {"1","销售收入","230000","68000","90000","72000"};
		 String[] three= {"1.1","住宅","230000","68000","90000","72000"};
		 String[] four= {"1.2","写字楼","","","",""};
		 String[] five= {"1.3","商铺","","","",""};
		 String[] six= {"1.4","其他","","","",""};
		 String[] seven= {"2","租金收入","","","",""};
		 String[] eight= {"2.1","住宅","","","",""};
		 String[] nine= {"2.2","写字楼","","","",""};
		 String[] ten= {"2.3","商铺","","","",""};
		 String[] ele= {"2.4","其他","","","",""};
		 String[] twe= {"3","自营收入","50000","12000","20000","18000"};
		 String[] thrty= {"3.1","住宅","50000","12000","20000","18000"};
		 String[] fourty= {"3.2","写字楼","","","",""};
		 String[] fifty= {"3.3","商铺","","","",""};
		 String[] sixty= {"3.4","其他","","","",""};
		 
		 String[] seventy= {"二","经营税金及附加","5832","1498","2530","1803"};
		 String[] eightty= {"1","营业税","4390","1283.69","1598","1508"};
		 String[] ninety= {"2","城市建设维护税","1442","358","664","419"};
		 String[] tenty= {"3","教育费附加","","","",""};
		 
		 result.add(one);
		 result.add(two);
		 result.add(three);
		 result.add(four);
		 result.add(five);
		 result.add(six);
		 result.add(seven);
		 result.add(eight);
		 result.add(nine);
		 
		 result.add(ten);
		 result.add(ele);
		 result.add(twe);
		 result.add(thrty);
		 result.add(fourty);
		 result.add(fifty);
		 result.add(sixty);
		 
		 result.add(seventy);
		 result.add(eightty);
		 result.add(ninety);
		 result.add(tenty);
		 
		 return result;
	}
	
	/**
	 * @Description (项目损益)
	 * @return
	 */
	public List<String[]> profit(){
		 List<String[]> result = new ArrayList<String[]>();
			
		 String[] one= {"1","经营收入","280000","80000","110000","90000"};
		 String[] two= {"1.1","销售收入","230000","68000","90000","72000"};
		 String[] three= {"1.2","出租收入","","","",""};
		 String[] four= {"1.3","自营收入","50000","12000","20000","18000"};
		 String[] five= {"2","经营成本","179600","49260","74019","56319"};
		 String[] six= {"2.1","产品分摊成本","170000","45900","69700","54400"};
		 String[] seven= {"2.2","运营费用","6329","2267","2790","1271"};
		 String[] eight= {"2.3","修理费用","3270","1092","1529","647"};
		 String[] nine= {"3","经营税金及附加","5832","1498","2530","1803"};
		 String[] ten= {"4","土地增值税","5630","1295","2388","1947"};
		 String[] ele= {"5","利润总额","88936","27945","31061","29929"};
		 String[] twe= {"6","税前弥补亏损","","","",""};
		 String[] thrty= {"7","所得税","21344","6706","7454","7183"};
		 String[] fourty= {"8","税后利润","67591","21238","23606","22746"};
		 String[] fifty= {"8.1","税后弥补亏损","","","",""};
		 String[] sixty= {"8.2","盈余公积金","17718","5358","922","11438"};
		 String[] seventy= {"8.3","公益金","","","",""};
		 String[] eightty= {"8.4","应付利润","","","",""};
		 String[] ninety= {"8.5","未分配利润","49873","15880","22684","11308"};
		 
		 result.add(one);
		 result.add(two);
		 result.add(three);
		 result.add(four);
		 result.add(five);
		 result.add(six);
		 result.add(seven);
		 result.add(eight);
		 result.add(nine);
		 
		 result.add(ten);
		 result.add(ele);
		 result.add(twe);
		 result.add(thrty);
		 result.add(fourty);
		 result.add(fifty);
		 result.add(sixty);
		 
		 result.add(seventy);
		 result.add(eightty);
		 result.add(ninety);
		 
		 return result;
	}
	
	/**
	 * @Description (项目现金流)
	 * @return
	 */
	public List<String[]> cashFlow(){
		 List<String[]> result = new ArrayList<String[]>();
			
		 String[] one= {"1","现金流入","193737","56687","76129","60920"};
		 String[] two= {"1.1","销售收入","156723","41896","63981","50846"};
		 String[] three= {"1.2","出租收入","","","",""};
		 String[] four= {"1.3","自营收入","37013","14791","12148","10073"};
		 String[] five= {"1.4","其他收入","","","",""};
		 String[] six= {"1.5","回收固定资产余值","","","",""};
		 String[] seven= {"2","现金流出","177843","51726","69342","56773"};
		 String[] eight= {"2.1","开发建设投资","126349","42672","53987","29689"};
		 String[] nine= {"2.2","经营资金","25763","4376","10832","10553"};
		 String[] ten= {"2.3","运营费用","5879","1689","2087","2102"};
		 String[] ele= {"2.4","修理费用","2000","677","677","677"};
		 String[] twe= {"2.5","经营税金及附加","5089","1583","2500","1005"};
		 String[] thrty= {"2.6","土地增值税","5229","1789","2400","1039"};
		 String[] fourty= {"2.7","所得税","7532","","",""};
		 String[] fifty= {"3","净现金流量","15893","4960","6786","4146"};
		 String[] sixty= {"4","折现净现金流量","15893","4960","6786","4146"};
		 
		 result.add(one);
		 result.add(two);
		 result.add(three);
		 result.add(four);
		 result.add(five);
		 result.add(six);
		 result.add(seven);
		 result.add(eight);
		 result.add(nine);
		 
		 result.add(ten);
		 result.add(ele);
		 result.add(twe);
		 result.add(thrty);
		 result.add(fourty);
		 result.add(fifty);
		 result.add(sixty);
		 
		 return result;
	}
	
	/**
	 * @Description (项目贷款偿还期计算)
	 * @return
	 */
	public List<String[]> payProjPlan(){
		 List<String[]> result = new ArrayList<String[]>();
		 
		 String[] one= {"一","借款偿还","20000","20000","20000","10000","10000","10000"};
		 String[] two= {"1","年初借款累计","35000","35000","35000","20000","20000","20000"};
		 String[] three= {"2","本年借款增加","-20000","-20000","-20000","-10000","-10000","-10000"};
		 String[] four= {"3","本年应计利息","146","146","146","88","88","88"};
		 String[] five= {"4","本年还本","15000","15000","15000","10000","10000","10000"};
		 String[] six= {"5","本年付息","146","146","146","88","88","88"};
		 String[] seven= {"6","年末借款累计","15000","15000","15000","10000","10000","10000"};
		 String[] eight= {"二","还款资金来源","20000","20000","20000","10000","10000","10000"};
		 String[] nine= {"1","利润","13443","16323","14239","7663","8609","7890"};
		 String[] ten= {"2","折旧费","","","","","",""};
		 String[] ele= {"3","摊销费","","","","","",""};
		 String[] twe= {"4","其它还款资金","6556","3676","5760","2336","1390","2109"};
		 
		 result.add(one);
		 result.add(two);
		 result.add(three);
		 result.add(four);
		 result.add(five);
		 result.add(six);
		 result.add(seven);
		 result.add(eight);
		 result.add(nine);
		 result.add(ten);
		 result.add(ele);
		 result.add(twe);
		 
		 return result;
	}
	
	
	/**
	 * @Description (综合贷款偿还期计算)
	 * @return
	 */
	public List<String[]> payGerenalPlan(){
		 List<String[]> result = new ArrayList<String[]>();
		 
		 String[] one= {"一","借款偿还","25000","20000","20000","10000","10000","10000","95000"};
		 String[] two= {"1","年初企业全部借款累计","35000","35000","35000","20000","20000","20000","165000"};
		 String[] three= {"2","本年借款增加","-25000","-20000","-20000","-10000","-10000","-10000","-95000"};
		 String[] four= {"3","本年应计利息","146","146","146","88","88","88","704"};
		 String[] five= {"4","本年还本","15000","15000","15000","10000","10000","10000","75000"};
		 String[] six= {"5","本年付息","146","146","146","88","88","88","704"};
		 String[] seven= {"6","年末企业全部借款累计","10000","15000","15000","10000","10000","10000","70000"};
		 String[] eight= {"二","还款资金来源","25000","20000","20000","10000","10000","10000","95000"};
		 String[] nine= {"1","利润","18443","16323","14239","7663","8609","7890","73168"};
		 String[] ten= {"2","折旧费","","","","","","",""};
		 String[] ele= {"3","摊销费","","","","","","",""};
		 String[] twe= {"4","其它还款资金","6556","3676","5760","2336","1390","2109","21831"};
		 
		 result.add(one);
		 result.add(two);
		 result.add(three);
		 result.add(four);
		 result.add(five);
		 result.add(six);
		 result.add(seven);
		 result.add(eight);
		 result.add(nine);
		 result.add(ten);
		 result.add(ele);
		 result.add(twe);
		 
		 return result;
	}
	
	/**
	 * @Description (项目敏感性分析)
	 * @return
	 */
	public List<String[]> sensibility(){
		 List<String[]> result = new ArrayList<String[]>();
		 
		 String[] one= {"1","基本方案","+0%","8%","30.72%","14716.34","8.0%","3年"};
		 String[] two= {"2","变动方案1","+5%","8%","27.36%","14716.34","7.4%","3年"};
		 String[] three= {"3","变动方案2","+0%","7%","30.72%","15832.98","8.3%","3年"};
		 
		 result.add(one);
		 result.add(two);
		 result.add(three);
		 return result;
	}
	
	
}
