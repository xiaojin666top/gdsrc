package com.beawan.analysis.finansis.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


import org.apache.poi.ss.usermodel.Workbook;

import com.beawan.analysis.finansis.bean.ReportDataTR;
import com.beawan.library.bean.ReportItemTemp;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

public class ReadThreeReportSheel {

	/**
	 * 
	 * @param sheelnum
	 *            ecxel 表中的第几张表
	 * @param start
	 *            开始行距离第一行的距离
	 * @param end
	 *            结束行距离第一行的距离
	 * @param inStream
	 *            输入流
	 * @param nameOffset
	 *            名字偏移量
	 * @param numberOffset
	 *            数字偏移量
	 * @return 表数据
	 * @throws IOException
	 */
	public static Map<String, Object> getSheelReportData(int sheelnum, int start, int end, InputStream inStream,
			int nameOffset, int numberOffset,String reportType,List<ReportItemTemp> reportItemTemps,String alertFlag) throws IOException {
		Map<String, Object> result =  new HashMap<String, Object>();
		
		List<ReportDataTR> reportDataTRs = new ArrayList<ReportDataTR>();
		List<String> nameList = new ArrayList<String>();
		Workbook wb = new HSSFWorkbook(inStream);
		Sheet sheet = wb.getSheetAt(sheelnum);// 第一个工作表
		Map<String, String> ks = adapterForKS(reportItemTemps);
		Set<String> ketList = ks.keySet();
		
		List<String> adapterName = adapterName();
		
		int firstRowIndex = sheet.getFirstRowNum();// 第一行
		int index = 0;
		for (int rIndex = firstRowIndex + start; rIndex < firstRowIndex + end; rIndex++) {
			Row row = sheet.getRow(rIndex);
			if (row != null) {
				int firstCellIndex = row.getFirstCellNum();	// 第一列
				int lastCellIndex = row.getLastCellNum();	// 最后一列
				int one = nameOffset + numberOffset;		//第二年数据
				int two = nameOffset + 2 * numberOffset;	//第三年数据
				int three = nameOffset + 3 * numberOffset;	//第四年数据
				int lastTerm = nameOffset + 4 * numberOffset;	//第四年数据
				ReportDataTR tr = new ReportDataTR();
				boolean emptyFlag = false;
				for (int cIndex = firstCellIndex; cIndex < lastCellIndex; cIndex++) {
					if (cIndex == firstCellIndex) {
						Cell cell = row.getCell(cIndex);
						String value = "";
						if (cell != null) {
							value = cell.toString();
							if (value.trim().isEmpty()) {
								emptyFlag = true;
								break;
							}
							value = value.replaceAll("（[^）]*）", "");
							value = value.replaceAll(" \\([^)]*\\)", "");
							value = value.replaceAll("　", "");
							value = value.replaceAll("  ", "");
							value = value.replace(" ", "");
							value = value.replaceAll("-", "");
							value = value.replaceAll("其它", "其他");
							value = value.replaceAll("减：", "");
							value = value.replaceAll("加：", "");
							value = value.replaceAll("帐款", "账款");
							value = value.replaceAll("其中:", "");
							value = value.replaceAll("其中：", "");
							value = value.replaceAll("：", "");
							value = value.replaceAll(":", "");
							value = value.replaceAll("一、", "");
							value = value.replaceAll("二、", "");
							value = value.replaceAll("三、", "");
							value = value.replaceAll("四、", "");
							value = value.replaceAll("五、", "");
							value = value.replaceAll("六、", "");
							value = value.replaceAll("七、", "");
							value = value.replaceAll("八、", "");
							value = value.replaceAll("九、", "");
							value = value.replaceAll("十、", "");
							value = value.replaceAll(" +","");
							if (ketList.contains(value)) {
								value = ks.get(value);
							}
							if(value.equals("固定资产")){
								value = "固定资产合计";
							}
							tr.setName(value);
							nameList.add(value);
						} else {
							emptyFlag = true;
							break;
							/*
							 * if(cell.toString().equals("")){ continue; }else{
							 * tr.setName(""); }
							 */
						}
					}else{
						double value = readCell(row, cIndex);
						if (cIndex == nameOffset) {
							tr.setFirstYearValue(value);
						} else if (cIndex == one) {
							tr.setSecondYearValue(value);
						} else if (cIndex == two) {
							tr.setThirdYearValue(value);
						} else if (cIndex == three) {
							tr.setFourthYearValue(value);
						}else if (cIndex == lastTerm) {
							tr.setLastYearTermValue(value);
						}
					}
				}
				if (emptyFlag == true) {
					continue;
				}
				tr.setIndex(index);
				reportDataTRs.add(tr);
				index++;
			}
		}
		inStream.close();
		
		if(reportType.equals("balance")){
			for(String adpterN : adapterName){
				if(!nameList.contains(adpterN)){
					if(adpterN.equals("固定资产净值")||adpterN.equals("固定资产净额")||adpterN.equals("固定资产原价")){
						int assetIndex  = nameList.indexOf("固定资产合计");
						if(assetIndex >0){
							ReportDataTR asset = reportDataTRs.get(assetIndex);
							ReportDataTR tr = new ReportDataTR();
							tr.setName(adpterN);
							tr.setFirstYearValue(asset.getFirstYearValue());
							tr.setSecondYearValue(asset.getSecondYearValue());
							tr.setThirdYearValue(asset.getThirdYearValue());
							tr.setFourthYearValue(asset.getFourthYearValue());
							if(asset.getLastYearTermValue()!=null){
								tr.setLastYearTermValue(asset.getLastYearTermValue());
							}else{
								tr.setLastYearTermValue(0.0);
							}
							reportDataTRs.add(tr);
							nameList.add(adpterN);
						}
					}else if(adpterN.equals("长期投资合计")){
						int longIndex  = nameList.indexOf("长期股权投资");
						if(longIndex>0){
							ReportDataTR longTr = reportDataTRs.get(longIndex);
							int debtIndex  = nameList.indexOf("长期债券投资");
							if(debtIndex>0){
								ReportDataTR debtTr = reportDataTRs.get(longIndex);
								ReportDataTR tr = new ReportDataTR();
								tr.setName(adpterN);
								tr.setFirstYearValue(longTr.getFirstYearValue()+debtTr.getFirstYearValue());
								tr.setSecondYearValue(longTr.getSecondYearValue()+debtTr.getSecondYearValue());
								tr.setThirdYearValue(longTr.getThirdYearValue()+debtTr.getThirdYearValue());
								tr.setFourthYearValue(longTr.getFourthYearValue()+debtTr.getFourthYearValue());
								if(longTr.getLastYearTermValue()!=null && debtTr.getLastYearTermValue()!=null){
									tr.setLastYearTermValue(longTr.getLastYearTermValue()+debtTr.getLastYearTermValue());
								}else if(longTr.getLastYearTermValue()!=null){
									tr.setLastYearTermValue(longTr.getLastYearTermValue());
								}else if(debtTr.getLastYearTermValue()!=null){
									tr.setLastYearTermValue(debtTr.getLastYearTermValue());
								}else{
									tr.setLastYearTermValue(0.0);
								}
								reportDataTRs.add(tr);
							}else{
								ReportDataTR tr = new ReportDataTR();
								tr.setName(adpterN);
								tr.setFirstYearValue(longTr.getFirstYearValue());
								tr.setSecondYearValue(longTr.getSecondYearValue());
								tr.setThirdYearValue(longTr.getThirdYearValue());
								tr.setFourthYearValue(longTr.getFourthYearValue());
								if(longTr.getLastYearTermValue()!=null){
									tr.setLastYearTermValue(longTr.getLastYearTermValue());
								}else{
									tr.setLastYearTermValue(0.0);
								}
								reportDataTRs.add(tr);
							}
							nameList.add(adpterN);
						}
					}else{
						ReportDataTR tr = new ReportDataTR();
						tr.setName(adpterN);
						tr.setFirstYearValue(0.0);
						tr.setSecondYearValue(0.0);
						tr.setThirdYearValue(0.0);
						tr.setFourthYearValue(0.0);
						tr.setLastYearTermValue(0.0);
						reportDataTRs.add(tr);
						nameList.add(adpterN);
					}
				}
			}
		}
		
			
		List<String> notNames = new ArrayList<String>();
		if(reportType.equals("income")){
			notNames =notIncomeName();
		}else if(reportType.equals("balance")){
			notNames = notBalanceName();
		}
		if(alertFlag.equals("false")){
			List<String> list = new ArrayList<String>();
			for (String noName : notNames) {
				if(!nameList.contains(noName)){
					list.add(noName);
				}
			}
			result.put("notNames", list);
		}else{
			for (String noName : notNames) {
				if(!nameList.contains(noName)){
					ReportDataTR tr = new ReportDataTR();
					tr.setName(noName);
					tr.setFirstYearValue(0.0);
					tr.setSecondYearValue(0.0);
					tr.setThirdYearValue(0.0);
					tr.setFourthYearValue(0.0);
					reportDataTRs.add(tr);
				}
			}
		}
		result.put("trs", reportDataTRs);
		return result;
	}

	// 读取 单元格
	public static double readCell(Row row, int cIndex) {
		Cell cell = row.getCell(cIndex);
		double value = 0.0;
		if (cell != null) {
			switch (cell.getCellType()) {
			case HSSFCell.CELL_TYPE_FORMULA:
				try {
					value = cell.getNumericCellValue();
				} catch (IllegalStateException e) {
					String empty = String.valueOf(cell.getRichStringCellValue());
					if (empty == null || empty.trim().isEmpty()) {
						value = 0.0;
					} else {
						value = Double.valueOf(String.valueOf(cell.getRichStringCellValue()));
					}
				}
				break;
			case HSSFCell.CELL_TYPE_NUMERIC:
				value = cell.getNumericCellValue();
				break;
			case HSSFCell.CELL_TYPE_STRING:
				if (!cell.toString().equals("")) {
					value = Double.valueOf(cell.toString());
				} else {
					value = 0.0;
				}
				break;
			}
		} else {
			value = 0.0;
		}
		return value;
	}

	public static Map<String, String> adapterForKS(List<ReportItemTemp> reportItemTemps) {
		Map<String, String> map = new HashMap<String, String>();
		for (ReportItemTemp itemTemp : reportItemTemps) {
			if(itemTemp.getNeedChangeName()!=null && !itemTemp.getNeedChangeName().equals("")){
				String[] changes = itemTemp.getNeedChangeName().split(",");
				for (String string : changes) {
					map.put(string, itemTemp.getName());
				}
			}
		}
		//map.put("流动资产", "流动资产合计");
		//map.put("长期投资", "长期投资合计");
		//map.put("固定资产", "固定资产合计");
		//map.put("流动负债", "流动负债合计");
		//map.put("应交管理费", "应付股利");
		//map.put("长期负债", "长期负债合计");
		//map.put("所有者权益", "所有者权益合计");
		//map.put("负债及所有者权益合计", "负债及所有者权益总计");
		//map.put("递延所得税资产", "递延资产");
		return map;
	}
	
	
	public static List<String> adapterName (){
		List<String>  names = new ArrayList<String>();
		names.add("应收补贴款");
		names.add("待摊费用");
		names.add("一年内到期的长期债券投资");
		names.add("待处理流动资产净损失");
		names.add("长期投资合计");
		names.add("固定资产原价");
		names.add("累计折旧");
		names.add("固定资产净值");
		names.add("待处理固定资产净损失");
		names.add("固定资产净额");
		names.add("递延资产");
		names.add("应付福利费");
		names.add("应付股利");
		names.add("其他应交款");
		names.add("预提费用");
		names.add("一年内到期的长期负债");
		return names;
	}

	
	public static List<String> notBalanceName() {
		List<String>  names = new ArrayList<String>();
		names.add("货币资金");
		names.add("短期投资");
		names.add("应收票据");
		names.add("应收账款");
		names.add("其他应收款");
		names.add("预付账款");
		names.add("应收补贴款");
		names.add("存货");
		names.add("待摊费用");
		names.add("一年内到期的长期债券投资");
		names.add("其他流动资产");
		names.add("待处理流动资产净损失");
		names.add("流动资产合计");
		names.add("长期投资合计");
		
		names.add("固定资产原价");
		names.add("累计折旧");
		names.add("固定资产净值");
		names.add("待处理固定资产净损失");
		names.add("固定资产净额");
		names.add("工程物资");
		
		names.add("在建工程");
		names.add("固定资产清理");
		names.add("固定资产合计");
		names.add("无形资产");
		names.add("其他长期资产");
		names.add("无形及其他资产合计");
		names.add("递延资产");
		names.add("资产总计");
		names.add("短期借款");
		names.add("应付票据");
		names.add("应付账款");
		names.add("预收账款");
		names.add("应付工资");
		names.add("应付福利费");
		names.add("应付股利");
		names.add("应交税金");
		names.add("其他应交款");
		names.add("其他应付款");
		names.add("预提费用");
		names.add("一年内到期的长期负债");
		names.add("其他流动负债");
		names.add("流动负债合计");
		
		names.add("长期借款");
		names.add("应付债券");
		names.add("长期应付款");
		names.add("其他长期负债");
		names.add("长期负债合计");
		
		names.add("负债合计");
		names.add("实收资本");
		names.add("资本公积");
		names.add("盈余公积");
		names.add("未分配利润");
		names.add("所有者权益合计");
		names.add("负债及所有者权益总计");
		
		return names;
	}
	
	
	public static List<String> notIncomeName() {
		List<String>  names = new ArrayList<String>();
		names.add("主营业务收入");
		names.add("主营业务成本");
		names.add("主营业务税金及附加");
		names.add("主营业务利润");
		names.add("营业费用");
		names.add("管理费用");
		names.add("财务费用");
		names.add("投资收益");
		names.add("其他业务利润");
		names.add("营业利润");		
		names.add("营业外收入");
		
		names.add("补贴收入");
		names.add("营业外支出");
		names.add("利润总额");
		names.add("所得税");		
		names.add("净利润");
		return names;
	}
}
