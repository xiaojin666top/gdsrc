package com.beawan.analysis.finansis.utils;

import java.io.UnsupportedEncodingException;

public class CodeUtil {
	
	/**
	 * @Description (将名字转换格式)
	 * @param sIn
	 * @return
	 */
	public static String parseGBK(String sIn) {
		if (sIn == null || sIn.equals(""))
			return sIn;
		try {
			return new String(sIn.getBytes("GBK"), "ISO-8859-1");
		} catch (UnsupportedEncodingException usex) {
			return sIn;
		}
	}

}
