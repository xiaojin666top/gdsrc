package com.beawan.analysis.finansis.utils;

import java.util.HashMap;
import java.util.Map;

import com.beawan.analysis.balance.balanceAssetStructData;
import com.beawan.analysis.balance.balanceAssetTrendData;
import com.beawan.analysis.balance.balanceAssetsRatioData;
import com.beawan.analysis.balance.balanceDebtRatioData;
import com.beawan.analysis.balance.balanceDebtStructData;
import com.beawan.analysis.balance.balanceDebtTrendData;
import com.beawan.analysis.balance.balanceOwnerRatio;
import com.beawan.analysis.balance.balanceOwnerStruct;
import com.beawan.analysis.balance.balanceOwnerTrendData;
import com.beawan.analysis.cashFlow.cashFlowRatioData;
import com.beawan.analysis.cashFlow.cashFlowTrendData;
import com.beawan.analysis.income.IncomeRatio;
import com.beawan.analysis.income.IncomeStruct;
import com.beawan.analysis.income.IncomeTrendJSON;

public class ThreeRpoetFormAnalysisUtil {


	 /**
	 * 
	 * @param balance1 第一年资产负债数据
	 * 
	 * @param balance2 第二年资产负债数据
	 * 
	 * @param balance3 第三年资产负债数据
	 * 
	 * @param addRatio 所占资产临界百分比值，大于则加入到队列中
	 * 
	 * @return 拼装好的资产负债中资产的趋势JSON
	 */

	public static Map<String, Object> getBalanceAssetTrendData(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, double addRatio) {

		Map<String, Object> jsonMap = new HashMap<String, Object>();

		jsonMap.put("data",
				balanceAssetTrendData.getBalanceAssetTrendData(balance1, balance2, balance3, balance4, addRatio));

		return jsonMap;

	}

	/**
	 * 存货周转率 Inventory_Turnover 流动资产周转率 Current_Assets_Turnover 总资产周转率(次)
	 * Total_Assets_Turnover 应收账款周转率（次）Receivables_Turnover_Ratio 总资产增长率
	 * Total_Assets_Growth_Rate
	 * 
	 * @author czc
	 *
	 */

	public static Map<String, Object> getBalanceAssetRatioData(Integer month, HashMap<String, Double> balance1,
			HashMap<String, Double> balance2, HashMap<String, Double> balance3, HashMap<String, Double> balance4,
			HashMap<String, Double> income2, HashMap<String, Double> income3, HashMap<String, Double> income4,
			double ch, double ldzc, double zzczzc, double yszk) {

		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", balanceAssetsRatioData.getBalanceAssetRatioData(month, balance1, balance2, balance3,
				balance4, income2, income3, income4, ch, ldzc, zzczzc, yszk));

		return jsonMap;

	}

	/**
	 * 得到资产总览的结构分析的 饼状图数据 List 0 项目名称 1 第一年数据 2 第二年数据 3第三年数据
	 */
	public static Map<String, Object> getBalanceAssetStructData(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, double addRatio) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data",
				balanceAssetStructData.getBalanceAssetStructData(balance1, balance2, balance3, balance4, addRatio));

		return jsonMap;
	}

	/**
	 * 得到资产总览的结构分析的 表格数据
	 */
	public static Map<String, Object> getBalanceAssetStructTableData(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, double addRatio) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", balanceAssetStructData.getBalanceAssetStructTableData(balance1, balance2, balance3,
				balance4, addRatio));

		return jsonMap;
	}

	/**
	 * 
	 * @param balance1
	 *            第一年资产负债数据
	 * @param balance2
	 *            第二年资产负债数据
	 * @param balance3
	 *            第三年资产负债数据
	 * @param addRatio
	 *            所占资产临界百分比值，大于则加入到队列中
	 * @return 拼装好的资产负债中负债的趋势JSON
	 */

	public static Map<String, Object> getBalanceDebtTrendData(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, double addRatio) {

		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data",
				balanceDebtTrendData.getBalanceDebtTrendData(balance1, balance2, balance3, balance4, addRatio));

		return jsonMap;

	}

	/**
	 * 资产负债率 Debt_to_Assets_Ratio 利息保障倍数 Interest_Coverage_Ratio 速动比率Quick_Ratio
	 * 现金流动负债比率Cash_Flows_Coverage_Ratio
	 * 
	 * @author czc
	 *
	 */

	public static Map<String, Object> getBalanceDebtRatioData(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, HashMap<String, Double> income2,
			HashMap<String, Double> income3, HashMap<String, Double> income4, HashMap<String, Double> cashFlow2,
			HashMap<String, Double> cashFlow3, HashMap<String, Double> cashFlow4, double dar, double icr, double qr,
			double cfcr) {

		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", balanceDebtRatioData.getBalanceDebtRatioData(balance1, balance2, balance3, balance4,
				income2, income3, income4, cashFlow2, cashFlow3, cashFlow4, dar, icr, qr, cfcr));

		return jsonMap;

	}

	/**
	 * 得到资产总览的结构分析的 饼状图数据 List 0 项目名称 1 第一年数据 2 第二年数据 3第三年数据
	 */
	public static Map<String, Object> getBalanceDebtStructData(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, double addRatio) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data",
				balanceDebtStructData.getBalanceDebtStructData(balance1, balance2, balance3, balance4, addRatio));

		return jsonMap;
	}

	/**
	 * 得到资产总览的结构分析的 表格数据
	 */
	public static Map<String, Object> getBalanceDebtStructTableData(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, double addRatio) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data",
				balanceDebtStructData.getBalanceDebtStructTableData(balance1, balance2, balance3, balance4, addRatio));

		return jsonMap;
	}

	/**
	 * 
	 * @param balance1
	 *            第一年资产负债数据
	 * @param balance2
	 *            第二年资产负债数据
	 * @param balance3
	 *            第三年资产负债数据
	 * @param addRatio
	 *            所占所有者权益临界百分比值，大于则加入到队列中
	 * @return 拼装好的资产负债中所有者权益的趋势JSON
	 */

	public static Map<String, Object> getBalanceOwnerTrendData(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, double addRatio) {

		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data",
				balanceOwnerTrendData.getBalanceOwnerTrendData(balance1, balance2, balance3, balance4, addRatio));

		return jsonMap;

	}

	/**
	 * 净资产收益率（ROE） Rate_of_Return_on_Common 资本积累率 Owners_Equity_Growth_Rate
	 * 
	 * @author czc
	 *
	 */

	public static Map<String, Object> getBalanceOwnerRatioData(int month, HashMap<String, Double> balance1,
			HashMap<String, Double> balance2, HashMap<String, Double> balance3, HashMap<String, Double> balance4,
			HashMap<String, Double> income2, HashMap<String, Double> income3, HashMap<String, Double> income4,
			double dar, double icr) {

		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", balanceOwnerRatio.getBalanceOwnerRatioData(month, balance1, balance2, balance3, balance4,
				income2, income3, income4, dar, icr));

		return jsonMap;

	}

	/**
	 * 得到资产总览的结构分析的 饼状图数据 List 0 项目名称 1 第一年数据 2 第二年数据 3第三年数据
	 */
	public static Map<String, Object> getBalanceOwnerStructData(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, double addRatio) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data",
				balanceOwnerStruct.getBalanceOwnerStructData(balance1, balance2, balance3, balance4, addRatio));

		return jsonMap;
	}

	/**
	 * 得到资产总览的结构分析的 表格数据
	 */
	public static Map<String, Object> getBalanceOwnerStructTableData(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, double addRatio) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data",
				balanceOwnerStruct.getBalanceOwnerStructTableData(balance1, balance2, balance3, balance4, addRatio));

		return jsonMap;
	}



	// 得到利润表分析中趋势分析选择项名字的数据

	public static Map<String, Object> getTrendIncomeItemNameData() {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("name", IncomeTrendJSON.getTrendSelectItemName());

		return jsonMap;
	}

	// 得到利润表中的第一项 主营业务收入、主营业务成本、利润的数据

	public static Map<String, Object> getTrendIncomeDataOne(HashMap<String, Double> income1,
			HashMap<String, Double> income2, HashMap<String, Double> income3, HashMap<String, Double> income4) {

		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", IncomeTrendJSON.getTrendIncoemDataOne(income1, income2, income3, income4));

		return jsonMap;
	}

	// 得到利润表的第二项 营业费用、 管理费用、财务费用的数据

	public static Map<String, Object> getTrendIncoemJSONTwo(HashMap<String, Double> income1,
			HashMap<String, Double> income2, HashMap<String, Double> income3, HashMap<String, Double> income4) {

		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", IncomeTrendJSON.getTrendIncoemDataTwo(income1, income2, income3, income4));

		return jsonMap;
	}

	// 得到利润表中的第三项 营业利润的数据

	public static Map<String, Object> getTrendIncoemJSONThree(HashMap<String, Double> income1,
			HashMap<String, Double> income2, HashMap<String, Double> income3, HashMap<String, Double> income4) {

		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", IncomeTrendJSON.getTrendIncoemDataThree(income1, income2, income3, income4));

		return jsonMap;
	}

	// 得到利润表中的第四项 利润总额、所得税、净利润的数据

	public static Map<String, Object> getTrendIncoemJSONFour(HashMap<String, Double> income1,
			HashMap<String, Double> income2, HashMap<String, Double> income3, HashMap<String, Double> income4) {

		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", IncomeTrendJSON.getTrendIncoemDataFour(income1, income2, income3, income4));

		return jsonMap;
	}

	/**
	 * 主营业务利润率 Main_Income_Profit_Ratio ZYYWLR_RATE 总资产报酬率
	 * Return_on_Total_Asset_Ratio ZZCBC_RATE 成本费用利润率
	 * Ratio_of_Profits_to_Cost_and_Expense CBFYLR_RATE 营业收入增长率
	 * Income_Growth_Rate XSZZ_RATE 利润增长率 Profit_Growth_Rate XSLRZZ_RATE
	 * 
	 * @author czc
	 *
	 */
	public static Map<String, Object> getIncomeRatioData(int month, HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, HashMap<String, Double> income1,
			HashMap<String, Double> income2, HashMap<String, Double> income3, HashMap<String, Double> income4,
			double mip, double rot, double rop, double igr, double pgr) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", IncomeRatio.getIncomeRatioData(month, balance1, balance2, balance3, balance4, income1,
				income2, income3, income4, mip, rot, rop, igr, pgr));

		return jsonMap;
	}

	/**
	 * 
	 * @param income1
	 * @param income2
	 * @param income3
	 * @param income4
	 * @return 利润表饼状图结构分析 List 0 项目名称 1 第一年数据 2 第二年数据 3第三年数据 4第四年数据
	 */
	public static Map<String, Object> getIncomeStructData(HashMap<String, Double> income1, HashMap<String, Double> income2,
			HashMap<String, Double> income3, HashMap<String, Double> income4) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", IncomeStruct.getIncomeStructData(income1, income2, income3, income4));

		return jsonMap;
	}

	/**
	 * 
	 * @param income1
	 * @param income2
	 * @param income3
	 * @param income4
	 * @return 得到利润概述分析的表格数据
	 */
	public static Map<String, Object> getIncomeStructTableData(HashMap<String, Double> income1, HashMap<String, Double> income2,
			HashMap<String, Double> income3, HashMap<String, Double> income4) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", IncomeStruct.getIncomeStructTableData(income1, income2, income3, income4));

		return jsonMap;
	}





	/**
	 * 得到现金流量总览的趋势分析的 第一个 现金及现金等价物净增加额 数据
	 */
	public static Map<String, Object> getCashFlowTrendDataOne(HashMap<String, Double> cashFlow1,
			HashMap<String, Double> cashFlow2, HashMap<String, Double> cashFlow3, HashMap<String, Double> cashFlow4) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();

		jsonMap.put("data", cashFlowTrendData.getCashFlowTrendDataOne(cashFlow1, cashFlow2, cashFlow3, cashFlow4));

		return jsonMap;
	}

	/**
	 * 得到现金流量总览的 趋势分析的 第2个 经营活动 数据
	 */
	public static Map<String, Object> getCashFlowTrendDataTwo(HashMap<String, Double> cashFlow1,
			HashMap<String, Double> cashFlow2, HashMap<String, Double> cashFlow3, HashMap<String, Double> cashFlow4,
			HashMap<String, Double> income1, HashMap<String, Double> income2, HashMap<String, Double> income3,
			HashMap<String, Double> income4) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();

		jsonMap.put("data", cashFlowTrendData.getCashFlowTrendDataTwo(cashFlow1, cashFlow2, cashFlow3, cashFlow4,
				income1, income2, income3, income4));

		return jsonMap;
	}

	/**
	 * 得到现金流量总览的 趋势分析的 第3个 销售商品、提供劳务收到的现金 购买商品、接受劳务支付的现金 数据
	 */
	public static Map<String, Object> getCashFlowTrendDataThree(HashMap<String, Double> cashFlow1,
			HashMap<String, Double> cashFlow2, HashMap<String, Double> cashFlow3, HashMap<String, Double> cashFlow4) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();

		jsonMap.put("data", cashFlowTrendData.getCashFlowTrendDataThree(cashFlow1, cashFlow2, cashFlow3, cashFlow4));

		return jsonMap;
	}

	/**
	 * 得到现金流量总览的 趋势分析的 第4个 投资活动产生的现金流量净额 筹资活动产生的现金流量净额 数据
	 */
	public static Map<String, Object> getCashFlowTrendDataFour(HashMap<String, Double> cashFlow1,
			HashMap<String, Double> cashFlow2, HashMap<String, Double> cashFlow3, HashMap<String, Double> cashFlow4) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();

		jsonMap.put("data", cashFlowTrendData.getCashFlowTrendDataFour(cashFlow1, cashFlow2, cashFlow3, cashFlow4));

		return jsonMap;
	}





	/**
	 * 得到现金流量总览的 趋势分析的 第5个 投资活动产生的现金流量净额 处置固定、无形和其他长期资产所收回的现金净额 数据
	 */
	public static Map<String, Object> getCashFlowTrendDataFive(HashMap<String, Double> cashFlow1,
			HashMap<String, Double> cashFlow2, HashMap<String, Double> cashFlow3, HashMap<String, Double> cashFlow4) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();

		jsonMap.put("data", cashFlowTrendData.getCashFlowTrendDataFive(cashFlow1, cashFlow2, cashFlow3, cashFlow4));

		return jsonMap;
	}

	/**
	 * 得到现金流量总览的 趋势分析的 第6个 吸收投资所收到的现金 借款所收到的现金 偿还债务所支付的现金 分配股利、利润或偿付利息所支付的现金
	 */
	public static Map<String, Object> getCashFlowTrendDataSix(HashMap<String, Double> cashFlow1,
			HashMap<String, Double> cashFlow2, HashMap<String, Double> cashFlow3, HashMap<String, Double> cashFlow4) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();

		jsonMap.put("data", cashFlowTrendData.getCashFlowTrendDataSix(cashFlow1, cashFlow2, cashFlow3, cashFlow4));

		return jsonMap;
	}

	/**
	 * 现金到期债务比 Cash_Maturity_Dabt_Ratio 平均值为1 现金流动负债比 Cash_Flows_Coverage_Ratio
	 * XJLDFZ_RATE 现金债务总额比 CashDebtCoverageRatio
	 * 
	 * @return
	 */
	public static Map<String, Object> getCashFlowRatioData(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> balance4, HashMap<String, Double> cashFlow2,
			HashMap<String, Double> cashFlow3, HashMap<String, Double> cashFlow4, double cmd, double cfc, double cdc) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", cashFlowRatioData.getCashFlowRatioData(balance1, balance2, balance3, balance4, cashFlow2,
				cashFlow3, cashFlow4, cmd, cfc, cdc));

		return jsonMap;
	}

	/**
	 * 现金流分层分析第一层表格数据
	 * 
	 * @return
	 */
	public static Map<String, Object> getEightCashFlowOne(HashMap<String, Double> balance1,HashMap<String, Double> balance2, HashMap<String, Double> balance3,
			HashMap<String, Double> income2, HashMap<String, Double> income3) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", cashFlowTrendData.getEightCashFlowOne(balance1,balance2, balance3, income2, income3));

		return jsonMap;
	}
	
	/**
	 * 现金流分层分析第一层分析数据
	 * 
	 * @return
	 */
	public static Map<String, Object> getEightCashFlowCommonOne(int year,HashMap<String, Double> balance1,HashMap<String, Double> balance2, HashMap<String, Double> balance3,
			HashMap<String, Double> income2, HashMap<String, Double> income3) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", cashFlowTrendData.getEightCashFlowOneCommon(year,balance1,balance2, balance3, income2, income3));

		return jsonMap;
	}
	/**
	 * 现金流分层分析第二层分析数据
	 * 
	 * @return
	 */
	public static Map<String, Object> getEightCashFlowCommonTwo(int year,HashMap<String, Double> balance1,HashMap<String, Double> balance2, HashMap<String, Double> balance3,
			HashMap<String, Double> income2, HashMap<String, Double> income3) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", cashFlowTrendData.getEightCashFlowTwoCommon(year,balance1,balance2, balance3, income2, income3));

		return jsonMap;
	}
	
	/**
	 * 现金流分层分析第三层分析数据
	 * 
	 * @return
	 */
	public static Map<String, Object> getEightCashFlowCommonThree(int year,HashMap<String, Double> balance1,HashMap<String, Double> balance2, HashMap<String, Double> balance3,
			HashMap<String, Double> income2, HashMap<String, Double> income3) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", cashFlowTrendData.getEightCashFlowThreeCommon(year,balance1,balance2, balance3, income2, income3));

		return jsonMap;
	}
	
	/**
	 * 现金流分层分析第四层分析数据
	 * 
	 * @return
	 */
	public static Map<String, Object> getEightCashFlowCommonFour(int year,HashMap<String, Double> balance1,HashMap<String, Double> balance2, HashMap<String, Double> balance3,
			HashMap<String, Double> income2, HashMap<String, Double> income3) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", cashFlowTrendData.getEightCashFlowFourCommon(year,balance1,balance2, balance3, income2, income3));

		return jsonMap;
	}
	
	
	/**
	 * 现金流分层分析第五 层分析数据
	 * 
	 * @return
	 */
	public static Map<String, Object> getEightCashFlowCommonFive(int year,HashMap<String, Double> balance1,HashMap<String, Double> balance2, HashMap<String, Double> balance3,
			HashMap<String, Double> income2, HashMap<String, Double> income3) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", cashFlowTrendData.getEightCashFlowFiveCommon(year,balance1,balance2, balance3, income2, income3));

		return jsonMap;
	}
	
	/**
	 * 现金流分层分析第六层分析数据
	 * 
	 * @return
	 */
	public static Map<String, Object> getEightCashFlowCommonSix(int year,HashMap<String, Double> balance1,HashMap<String, Double> balance2, HashMap<String, Double> balance3,
			HashMap<String, Double> income2, HashMap<String, Double> income3) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", cashFlowTrendData.getEightCashFlowSixCommon(year,balance1,balance2, balance3, income2, income3));

		return jsonMap;
	}
	/**
	 * 现金流分层分析第七层分析数据
	 * 
	 * @return
	 */
	public static Map<String, Object> getEightCashFlowCommonSeven(int year,HashMap<String, Double> balance1,HashMap<String, Double> balance2, HashMap<String, Double> balance3,
			HashMap<String, Double> income2, HashMap<String, Double> income3) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", cashFlowTrendData.getEightCashFlowSevenCommon(year,balance1,balance2, balance3, income2, income3));

		return jsonMap;
	}
	/**
	 * 现金流分层分析第八层分析数据
	 * 
	 * @return
	 */
	public static Map<String, Object> getEightCashFlowCommonEight(int year,HashMap<String, Double> balance1,HashMap<String, Double> balance2, HashMap<String, Double> balance3,
			HashMap<String, Double> income2, HashMap<String, Double> income3) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", cashFlowTrendData.getEightCashFlowEightCommon(year,balance1,balance2, balance3, income2, income3));

		return jsonMap;
	}
	/**
	 * 现金流分层分析第二层表格数据
	 * 
	 * @return
	 */
	public static Map<String, Object> getEightCashFlowTwo(HashMap<String, Double> balance1,HashMap<String, Double> balance2, HashMap<String, Double> balance3,
			HashMap<String, Double> income2, HashMap<String, Double> income3) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", cashFlowTrendData.getEightCashFlowTwo(balance1,balance2, balance3, income2, income3));

		return jsonMap;
	}

	/**
	 * 现金流分层分析第三层表格数据
	 * 
	 * @return
	 */
	public static Map<String, Object> getEightCashFlowThree(HashMap<String, Double> balance1,HashMap<String, Double> balance2, HashMap<String, Double> balance3,
			HashMap<String, Double> income2, HashMap<String, Double> income3) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", cashFlowTrendData.getEightCashFlowThree(balance1,balance2, balance3, income2, income3));

		return jsonMap;
	}

	/**
	 * 现金流分层分析第四层表格数据
	 * 
	 * @return
	 */
	public static Map<String, Object> getEightCashFlowFour(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> income2, HashMap<String, Double> income3) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", cashFlowTrendData.getEightCashFlowFour(balance1, balance2, balance3, income2, income3));

		return jsonMap;
	}
	
	/**
	 * 现金流分层分析第五层表格数据
	 * 
	 * @return
	 */
	public static Map<String, Object> getEightCashFlowFive(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> income2, HashMap<String, Double> income3) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", cashFlowTrendData.getEightCashFlowFive(balance1, balance2, balance3, income2, income3));

		return jsonMap;
	}
	
	/**
	 * 现金流分层分析第六层表格数据
	 * 
	 * @return
	 */
	public static Map<String, Object> getEightCashFlowSix(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> income2, HashMap<String, Double> income3) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", cashFlowTrendData.getEightCashFlowSix(balance1, balance2, balance3, income2, income3));

		return jsonMap;
	}
	
	/**
	 * 现金流分层分析第七层表格数据
	 * 
	 * @return
	 */
	public static Map<String, Object> getEightCashFlowSeven(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> income2, HashMap<String, Double> income3) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", cashFlowTrendData.getEightCashFlowSeven(balance1, balance2, balance3, income2, income3));

		return jsonMap;
	}
	
	/**
	 * 现金流分层分析第八层表格数据
	 * 
	 * @return
	 */
	public static Map<String, Object> getEightCashFlowEight(HashMap<String, Double> balance1, HashMap<String, Double> balance2,
			HashMap<String, Double> balance3, HashMap<String, Double> income2, HashMap<String, Double> income3) {
		Map<String, Object> jsonMap = new HashMap<String, Object>();;

		jsonMap.put("data", cashFlowTrendData.getEightCashFlowEight(balance1, balance2, balance3, income2, income3));

		return jsonMap;
	}


}
