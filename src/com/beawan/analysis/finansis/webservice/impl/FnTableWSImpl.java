package com.beawan.analysis.finansis.webservice.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.beawan.analysis.finansis.bean.FnCash;
import com.beawan.analysis.finansis.bean.FnConstruc;
import com.beawan.analysis.finansis.bean.FnFinancCost;
import com.beawan.analysis.finansis.bean.FnFixedAssetsBuilding;
import com.beawan.analysis.finansis.bean.FnFixedAssetsEquipment;
import com.beawan.analysis.finansis.bean.FnFixedAssetsLand;
import com.beawan.analysis.finansis.bean.FnLongTermPay;
import com.beawan.analysis.finansis.bean.FnManageCost;
import com.beawan.analysis.finansis.bean.FnOperatCost;
import com.beawan.analysis.finansis.bean.FnOtherPay;
import com.beawan.analysis.finansis.bean.FnOtherReceive;
import com.beawan.analysis.finansis.bean.FnPay;
import com.beawan.analysis.finansis.bean.FnPayNote;
import com.beawan.analysis.finansis.bean.FnPreReceive;
import com.beawan.analysis.finansis.bean.FnPrepay;
import com.beawan.analysis.finansis.bean.FnReceNote;
import com.beawan.analysis.finansis.bean.FnReceive;
import com.beawan.analysis.finansis.bean.FnInventory;
import com.beawan.analysis.finansis.bean.FnTableAnaly;
import com.beawan.analysis.finansis.dao.IFnTableDao;
import com.beawan.analysis.finansis.service.IFnTableService;
import com.beawan.analysis.finansis.webservice.IFnTableWS;
import com.beawan.common.util.WSResult;
import com.platform.util.JacksonUtil;

@Service("fnTableWS")
public class FnTableWSImpl implements IFnTableWS {

	private static final Logger log = Logger.getLogger(IFnTableWS.class);

	private Map<String, Object> model = new HashMap<String, Object>();

	public Map<String, Object> getModel() {
		return model;
	}

	public void setModel(Map<String, Object> model) {
		this.model = model;
	}

	@Resource
	private IFnTableDao fnTableDao;
	
	@Resource
	protected IFnTableService fnTableSV;

	@Override
	public String findTableAnalyByTIdAndNo(Long taskId, String custNo)  {
		// TODO Auto-generated method stub
		String result = null;
		try {
			FnTableAnaly data = fnTableDao.queryTableAnalyByTIdAndNo(taskId, custNo);
			if(data == null ){
				return result;
			}
			result = JacksonUtil.serialize(data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = "error";
			log.error("获取财务明细分析异常！",e);
		}
		return result;
	}

	@Override
	public String saveTableAnaly( String info)  {
		// TODO Auto-generated method stub
		String result = null;
		try {
			FnTableAnaly bean = JacksonUtil.fromJson(info, FnTableAnaly.class);
			fnTableDao.saveTableAnaly(bean);
			result = "success";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			result = "error";
			log.error("保存财务明细分析异常！", e);
			e.printStackTrace();
		}
		return result;
	}

	

	@Override
	public String queryFnFixedAssetsLandByTId(Long taskId) {
		WSResult result=new WSResult();
		try {
			List<FnFixedAssetsLand> list=fnTableSV.queryFnFixedAssetsLandByTId(taskId);
			if (CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
		}catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("获取固定资产-土地异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveFnFixedAssetsLands(Long taskId, String jsonArray) {
		WSResult result=new WSResult();
		try {
			
			fnTableSV.saveFnFixedAssetsLands(taskId, jsonArray);
			
		}catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("保存固定资产-土地异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String queryFnFixedAssetsBuildingByTId(Long taskId) {
		WSResult result=new WSResult();
		try {
			List<FnFixedAssetsBuilding> list=fnTableSV.queryFnFixedAssetsBuildingByTId(taskId);
			if (CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
			
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("查找固定资产-房产异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveFnFixedAssetsBuildings(Long taskId, String jsonArray) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			
			fnTableSV.saveFnFixedAssetsBuildings(taskId, jsonArray);
			
		}catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("保存固定资产-房产异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String queryFnFixedAssetsEquipmentByTId(Long taskId) {
		WSResult result=new WSResult();
		try {
			List<FnFixedAssetsEquipment> list=fnTableSV.queryFnFixedAssetsEquipmentByTId(taskId);
			if (CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
			
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("查找固定资产-设备异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveFnFixedAssetsEquipments(Long taskId, String jsonArray) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			
			fnTableSV.saveFnFixedAssetsEquipments(taskId, jsonArray);
			
		}catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("保存固定资产-房产异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deleteTableAnaly(FnTableAnaly data) {
		WSResult result=new WSResult();
		try {
			fnTableSV.deleteTableAnaly(data);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("保存删除表格明细异常！",e);
			e.printStackTrace();
		
		}
		return result.json();
	}

	@Override
	public String findLongTermPayByTId(Long taskId) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			List<FnLongTermPay> list=fnTableSV.findLongTermPayByTId(taskId);
			if (CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("查找长期应付款异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveLongTermPay(Long taskId, String jsonArray) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			fnTableSV.saveLongTermPay(taskId, jsonArray);
			
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("保存长期应付款异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deleteLongTermPay(FnLongTermPay data) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			fnTableSV.deleteLongTermPay(data);
			
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("删除长期应付款异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String findOtherPayByTId(Long taskId) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			List<FnOtherPay> list=fnTableSV.findOtherPayByTId(taskId);
			if (CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("查找其他应付款异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveOtherPay(Long taskId, String jsonArray) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			fnTableSV.saveOtherPay(taskId, jsonArray);
			
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("查找其他应付款异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deleteOtherPay(FnOtherPay data) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			fnTableSV.deleteOtherPay(data);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("删除其他应付款异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String findOtherReceiveByTId(Long taskId) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			List<FnOtherReceive> list=fnTableSV.findOtherReceiveByTId(taskId);
			if (CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("查找其他应收款异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveOtherReceive(Long taskId, String jsonArray) {
		WSResult result=new WSResult();
		try {
			fnTableSV.saveOtherReceive(taskId, jsonArray);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("保存其他应收款异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deleteOtherReceive(FnOtherReceive data) {
		WSResult result=new WSResult();
		try {
			fnTableSV.deleteOtherReceive(data);
			
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("删除其他应收款异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String findPayByTId(Long taskId) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			List<FnPay> list=fnTableSV.findPayByTId(taskId);
			if (CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("查找应付账款异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String savePay(Long taskId, String jsonArray) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			fnTableSV.savePay(taskId, jsonArray);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("保存应付账款异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deletePay(FnPay data) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			fnTableSV.deletePay(data);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("删除应付账款异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String findPrepayByTId(Long taskId) {
		WSResult result=new WSResult();
		try {
			List<FnPrepay> list=fnTableSV.findPrepayByTId(taskId);
			if (CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("查找预付账款异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String savePrepay(Long taskId, String jsonArray) {
		WSResult result=new WSResult();
		try {
			fnTableSV.savePrepay(taskId, jsonArray);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("保存预付账款异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deletePrepay(FnPrepay data) {
		WSResult result=new WSResult();
		try {
			fnTableSV.deletePrepay(data);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("删除预付账款异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String findPreReceiveByTId(Long taskId) {
		WSResult result=new WSResult();
		try {
			List<FnPreReceive> list=fnTableSV.findPreReceiveByTId(taskId);
			if (CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("查找预收账款异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String savePreReceive(Long taskId, String jsonArray) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			fnTableSV.savePreReceive(taskId, jsonArray);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("保存预收账款异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deletePreReceive(FnPreReceive data) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			fnTableSV.deletePreReceive(data);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("删除预收账款异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String findReceiveByTId(Long taskId) {
		WSResult result=new WSResult();
		try {
			List<FnReceive> list=fnTableSV.findReceiveByTId(taskId);
			if (CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("查找应收账款异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveReceive(Long taskId, String jsonArray) {
		WSResult result=new WSResult();
		try {
			fnTableSV.saveReceive(taskId, jsonArray);
			
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("查找应收账款异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deleteReceive(FnReceive data) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			fnTableSV.deleteReceive(data);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("删除应收账款异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String findFnCashByTId(Long taskId) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			List<FnCash> list=fnTableSV.findFnCashByTId(taskId);
			if (CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("查找企业货币资金异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveFnCash(Long taskId, String jsonArray) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			fnTableSV.saveFnCash(taskId, jsonArray);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("保存企业货币资金异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deleteFnCash(FnCash data) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			fnTableSV.deleteFnCash(data);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("删除企业货币资金异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String findFnConstrucByTId(Long taskId) {
		WSResult result=new WSResult();
		try {
			List<FnConstruc> list=fnTableSV.findFnConstrucByTId(taskId);
			if (CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("查找在建工程异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveFnConstruc(Long taskId, String jsonArray) {
		WSResult result=new WSResult();
		try {
			fnTableSV.saveFnConstruc(taskId, jsonArray);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("保存在建工程异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deleteFnConstruc(FnConstruc data) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			fnTableSV.deleteFnConstruc(data);
			
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("删除在建工程异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String findFinancCostByTId(Long taskId) {
		WSResult result=new WSResult();
		try {
			List<FnFinancCost> list=fnTableSV.findFinancCostByTId(taskId);
			if (CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("查询财务费用明细异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveFinancCost(Long taskId, String jsonArray) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			fnTableSV.saveFinancCost(taskId, jsonArray);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("保存财务费用明细异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deleteFinancCost(FnFinancCost data) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			fnTableSV.deleteFinancCost(data);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("删除财务费用明细异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String findManageCostByTId(Long taskId) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			List<FnManageCost> list=fnTableSV.findManageCostByTId(taskId);
			if (CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("查询管理费用明细异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveManageCost(Long taskId, String jsonArray) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			fnTableSV.saveManageCost(taskId, jsonArray);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("保存管理费用明细异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deleteManageCost(FnManageCost data) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			fnTableSV.deleteManageCost(data);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("删除管理费用明细异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String findOperatCostByTId(Long taskId) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			List<FnOperatCost> list=fnTableSV.findOperatCostByTId(taskId);
			if (CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("查询营业费用明细异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveOperatCost(Long taskId, String jsonArray) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			fnTableSV.saveOperatCost(taskId, jsonArray);
			
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("保存营业费用明细异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deleteOperatCost(FnOperatCost data) {
		WSResult result=new WSResult();
		try {
			fnTableSV.deleteOperatCost(data);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("删除营业费用明细异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String findFnPayNoteByTId(Long taskId) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			List<FnPayNote> list=fnTableSV.findFnPayNoteByTId(taskId);
			if (CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("查询应付票据明细异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveFnPayNote(Long taskId, String jsonArray) {
		WSResult result=new WSResult();
		try {
			fnTableSV.saveFnPayNote(taskId, jsonArray);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("保存应付票据明细异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deleteFnPayNote(FnPayNote data) {
		WSResult result=new WSResult();
		try {
			fnTableSV.deleteFnPayNote(data);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("删除应付票据明细异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String findFnReceNoteByTId(Long taskId) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			List<FnReceNote> list=fnTableSV.findFnReceNoteByTId(taskId);
			if (CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("查询应收票据明细异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveFnReceNote(Long taskId, String jsonArray) {
		WSResult result=new WSResult();
		try {
			fnTableSV.saveFnReceNote(taskId, jsonArray);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("保存应收票据明细异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deleteFnReceNote(FnReceNote data) {
		WSResult result=new WSResult();
		try {
			fnTableSV.deleteFnReceNote(data);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("删除应收票据明细异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String findFnInventoryByTId(Long taskId) {
		WSResult result=new WSResult();
		try {
			List<FnInventory> list=fnTableSV.findFnInventoryByTId(taskId);
			if (CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("查询存货明细异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveFnInventory(Long taskId, String jsonArray) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			fnTableSV.saveFnInventory(taskId, jsonArray);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("保存存货明细异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deleteFnInventory(FnInventory data) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			fnTableSV.deleteFnInventory(data);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("删除存货明细异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deleteFnFixedAssetsLand(FnFixedAssetsLand data) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			fnTableSV.deleteFnFixedAssetsLand(data);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("删除固定资产-土地明细异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deleteFnFixedAssetsBuilding(FnFixedAssetsBuilding data) {
		WSResult result=new WSResult();
		try {
			fnTableSV.deleteFnFixedAssetsBuilding(data);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("删除固定资产-房产明细异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String deleteFnFnFixedAssetsEquipment(FnFixedAssetsEquipment data) {
		WSResult result=new WSResult();
		try {
			fnTableSV.deleteFnFnFixedAssetsEquipment(data);
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("删除固定资产-设备明细异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	

	


}
