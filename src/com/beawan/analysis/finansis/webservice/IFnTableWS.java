package com.beawan.analysis.finansis.webservice;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.analysis.finansis.bean.FnCash;
import com.beawan.analysis.finansis.bean.FnConstruc;
import com.beawan.analysis.finansis.bean.FnFinancCost;
import com.beawan.analysis.finansis.bean.FnFixedAssetsBuilding;
import com.beawan.analysis.finansis.bean.FnFixedAssetsEquipment;
import com.beawan.analysis.finansis.bean.FnFixedAssetsLand;
import com.beawan.analysis.finansis.bean.FnLongTermPay;
import com.beawan.analysis.finansis.bean.FnManageCost;
import com.beawan.analysis.finansis.bean.FnOperatCost;
import com.beawan.analysis.finansis.bean.FnOtherPay;
import com.beawan.analysis.finansis.bean.FnOtherReceive;
import com.beawan.analysis.finansis.bean.FnPay;
import com.beawan.analysis.finansis.bean.FnPayNote;
import com.beawan.analysis.finansis.bean.FnPreReceive;
import com.beawan.analysis.finansis.bean.FnPrepay;
import com.beawan.analysis.finansis.bean.FnReceNote;
import com.beawan.analysis.finansis.bean.FnReceive;
import com.beawan.analysis.finansis.bean.FnInventory;
import com.beawan.analysis.finansis.bean.FnTableAnaly;

public interface IFnTableWS {
	

	/**
	 * @Description (根据客户号和任务号查询明细表分析)
	 * @param custNo
	 *            客户号
	 * @return
	 */
	public String findTableAnalyByTIdAndNo(Long taskId,String custNo) ;

	/**
	 * @Description (保存明细表分析)
	 * @param data
	 */
	public String saveTableAnaly(String info);
	
	/**
	 * @Description (删除明细表分析)
	 * @param data
	 *            
	 */
	public String deleteTableAnaly(FnTableAnaly data);
	/**
	 * @Description (根据任务号查询长期应付款)
	 * @param taskId 任务编号
	 * @return
	 */
	public String findLongTermPayByTId(Long taskId);
		

	/**
	 * @Description (保存长期应付款)
	 * @param data
	 */
	public String saveLongTermPay(Long taskId, String jsonArray);
	
	/**
	 * @Description (删除长期应付款)
	 * @param data
	 *            
	 */
	public String deleteLongTermPay(FnLongTermPay data);
	
	
	/**
	 * @Description (根据任务号查询其他应付款)
	 * @param taskId 任务号
	 * @return
	 */
	public String findOtherPayByTId(Long taskId);

	/**
	 * @Description (保存其他应付款)
	 * @param data
	 *            现金流量简要的结构分析)
	 */
	public String saveOtherPay(Long taskId, String jsonArray);
	
	/**
	 * @Description (删除其他应付款)
	 * @param data
	 *            现金流量简要的结构分析)
	 */
	public String deleteOtherPay(FnOtherPay data);
	
	
	
	
	/**
	 * @Description (根据任务号查询其他应收款)
	 * @param taskId 任务号
	 * @return
	 */
	public String findOtherReceiveByTId(Long taskId);
	
	/**
	 * @Description (保存其他应收款)
	 * @param data
	 */
	public String saveOtherReceive(Long taskId, String jsonArray);
	
	/**
	 * @Description (删除企业其他应收款)
	 * @param data
	 */
	public String deleteOtherReceive(FnOtherReceive data);
	
	
	
	/**
	 * @Description (根据任务号查询应付账款)
	 * @param taskId 任务号
	 * @return
	 */
	public String findPayByTId(Long taskId);

	/**
	 * @Description (保存企业应付账款)
	 * @param data
	 *            现金流量简要的结构分析)
	 */
	public String savePay(Long taskId,String jsonArray);
	
	/**
	 * @Description (删除企业应付账款)
	 * @param data
	 *            现金流量简要的结构分析)
	 */
	public String deletePay(FnPay data);
	
	
	
	/**
	 * @Description (根据任务号查询预付账款)
	 * @param taskId 任务号
	 * @return
	 */
	public String findPrepayByTId(Long taskId);

	/**
	 * @Description (保存预付账款)
	 * @param data
	 */
	public String savePrepay(Long taskId,String jsonArray);
	
	/**
	 * @Description (删除企业预付账款)
	 * @param data
	 */
	public String deletePrepay(FnPrepay data);
	
	
	
	/**
	 * @Description (根据任务号查询预收账款)
	 * @param taskId 任务号
	 * @return
	 */
	public String findPreReceiveByTId(Long taskId);

	/**
	 * @Description (保存企业预收账款
	 *            现金流量简要的结构分析)
	 */
	public String savePreReceive(Long taskId,String jsonArray);
	
	/**
	 * @Description (删除企业预收账款)
	 * @param data
	 */
	public String deletePreReceive(FnPreReceive data);
	
	
	
	/**
	 * @Description (根据任务号查询应收账款)
	 * @param taskId 任务号
	 * @return
	 */
	public String findReceiveByTId(Long taskId);

	/**
	 * @Description (保存企业应收账款)
	 * @param data
	 */
	public String saveReceive(Long taskId,String jsonArray);
	
	/**
	 * @Description (删除企业应收账款)
	 * @param data
	 */
	public String deleteReceive(FnReceive data);
	
	
	
	/**
	 * @Description (根据任务号查询货币资金)
	 * @param taskId 客户号
	 * @return
	 */
	public String findFnCashByTId(Long taskId);

	/**
	 * @Description (保存企业货币资金)
	 * @param data
	 */
	public String saveFnCash(Long taskId,String jsonArray);
	
	/**
	 * @Description (删除企业货币资金)
	 * @param data
	 */
	public String deleteFnCash(FnCash data);
	
	/**
	 * @Description (根据任务号查询在建工程)
	 * @param taskId任务号
	 * @return
	 */
	public String findFnConstrucByTId(Long taskId);

	/**
	 * @Description (保存企业在建工程)
	 * @param data
	 */
	public String saveFnConstruc(Long taskId,String jsonArray);
	
	/**
	 * @Description (删除企业在建工程)
	 * @param data
	 */
	public String deleteFnConstruc(FnConstruc data);
	
	/**
	 * @Description (根据任务号查询财务费用明细)
	 * @param taskId 任务号
	 * @return
	 */
	public String findFinancCostByTId(Long taskId);

	/**
	 * @Description (保存财务费用明细
	 * @param data
	 */
	public String saveFinancCost(Long taskId,String jsonArray);
	
	/**
	 * @Description (删除财务费用明细)
	 * @param data
	 */
	public String deleteFinancCost(FnFinancCost data);
	
	/**
	 * @Description (根据任务号查询管理费用明细)
	 * @param taskId 任务号
	 * @return
	 */
	public String findManageCostByTId(Long taskId);

	/**
	 * @Description (保存管理明细)
	 * @param data
	 */
	public String saveManageCost(Long taskId,String jsonArray);
	
	/**
	 * @Description (删除管理明细)
	 * @param data
	 */
	public String deleteManageCost(FnManageCost data);
	
	/**
	 * @Description (根据任务号查询营业费用明细)
	 * @param taskId任务号
	 * @return
	 */
	public String findOperatCostByTId(Long taskId);

	/**
	 * @Description (保存营业明细)
	 * @param data
	 */
	public String saveOperatCost(Long taskId,String jsonArray);
	
	/**
	 * @Description (删除营业明细)
	 * @param data
	 */
	public String deleteOperatCost(FnOperatCost data);
	
	/**
	 * @Description (根据任务号查询应付票据明细）
	 * @param taskId任务号
	 * @return
	 */
	public String findFnPayNoteByTId(Long taskId);

	/**
	 * @Description (保存应付票据明细）
	 * @param data
	 */
	public String saveFnPayNote(Long taskId,String jsonArray);
	
	/**
	 * @Description (删除应付票据明细）
	 * @param data
	 */
	public String deleteFnPayNote(FnPayNote data);
	
	/**
	 * @Description (根据任务号查询应收票据明细）
	 * @param taskId 任务号
	 * @return
	 */
	public String findFnReceNoteByTId(Long taskId);

	/**
	 * @Description (保存应收票据明细）
	 * @param data
	 */
	public String saveFnReceNote(Long taskId,String jsonArray);
	
	/**
	 * @Description (删除应收票据明细）
	 * @param data
	 */
	public String deleteFnReceNote(FnReceNote data);
	

	/**
	 * @Description (根据任务号查询存货明细）
	 * @param taskId 任务号
	 * @return
	 */
	public String findFnInventoryByTId(Long taskId);

	/**
	 * @Description (保存存货明细）
	 * @param data
	 */
	public String saveFnInventory(Long taskId,String jsonArray);
	
	/**
	 * @Description (删除存货明细）
	 * @param data
	 */
	public String deleteFnInventory(FnInventory data);
	
	
	
	/**
	 * @Description (根据任务号查询固定资产-土地）
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public String queryFnFixedAssetsLandByTId(Long taskId);

	/**
	 * @Description (保存固定资产-土地）
	 * @param data
	 */
	public String saveFnFixedAssetsLands(Long taskId,String jsonArray);
	
	/**
	 * @Description (删除固定资产-土地）
	 * @param data
	 */
	public String deleteFnFixedAssetsLand(FnFixedAssetsLand data);
	
	
	/**
	 * @Description (根据任务号查询固定资产-房屋）
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public String queryFnFixedAssetsBuildingByTId(Long taskId);

	/**
	 * @Description (保存固定资产-房屋）
	 * @param data
	 */
	public String saveFnFixedAssetsBuildings(Long taskId,String jsonArray);
	
	/**
	 * @Description (删除固定资产-房屋）
	 * @param data
	 */
	public String deleteFnFixedAssetsBuilding(FnFixedAssetsBuilding data);

	
	/**
	 * @Description (根据任务号查询固定资产-设备）
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public String queryFnFixedAssetsEquipmentByTId(Long taskId);

	/**
	 * @Description (保存固定资产-设备）
	 * @param data
	 */
	public String saveFnFixedAssetsEquipments(Long taskId,String jsonArray);
	
	/**
	 * @Description (删除固定资产-设备）
	 * @param data
	 */
	public String deleteFnFnFixedAssetsEquipment(FnFixedAssetsEquipment data);

	
}
