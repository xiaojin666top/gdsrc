package com.beawan.analysis.finansis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.beawan.analysis.finansis.bean.ReportData;
import com.beawan.analysis.finansis.bean.ReportDataTR;
import com.beawan.analysis.finansis.bean.ScheduleData;
import com.beawan.task.bean.Task;

public interface IFinanasisService {

	/* ReportData */
	/**
	 * @Description (通过id找表格数据)
	 * @param id
	 * @return
	 */
	public ReportData getReportDataById(Long id);

	/**
	 * @Description (保存表格数据)
	 * @param ReportData
	 * @return
	 */
	public ReportData saveReportData(ReportData ReportData);

	/**
	 * @Description (删除表格数据，同时删除相关联的reportdatatr)
	 * @param ReportData
	 */
	public void deleteReportData(ReportData ReportData);
	
	public  void saveThreeReportData(long taskId, String customerNo, int finaRepYear,
			int finaRepMonth)throws Exception;
	
	public  List<HashMap<String, Double>>  getTableMapList(Long taskId,String customerNo,String reportType);
	
	
	/**
	 * @Description (通过客户号和表格类型查询三大报表表格数据，其中类型分别为  balance，newBlance income newIncome，cashFlow newCashFlow)
	 * @param custNo
	 * @param reportType
	 * @return
	 */
	public ReportData getReportDataByTaskIdCustNoAndType(Long taskId,String customerNo ,String reportType);

	
	/* ReportData end */

	/* ReportDataTR */
	/**
	 * @Description (通过id查找表格行)
	 * @param id
	 * @return
	 */
	public ReportDataTR getReportDataTR(Long id);

	/**
	 * @Description (保存表格行)
	 * @param ReportDataTR
	 * @return
	 */
	public void saveReportDataTR(ReportDataTR ReportDataTR);

	/**
	 * @Description (删除表格行)
	 * @param ReportDataTR
	 */
	public void deleteReportDataTR(ReportDataTR ReportDataTR);

	/**
	 * @Description (找出表格的所有行，并按照位置升序排列)
	 * @return
	 */
	public List<ReportDataTR> getReportDataTrsByReportDataId(Long reportDataId);

	/* ReportDataTR end */


	
	/* ScheduleData */
	/**
	 * @Description (通过任务id和明细表类型保存行业明细表)
	 * @param taskId
	 * @param repType
	 * @return
	 */
	public List<ScheduleData> getScheByTaskIdAndRepType(Long taskId, String repType);
	
	/**
	 * @Description (保存明细表数据)
	 * @param detailData
	 * @param oldDetailList
	 */
	public void saveScheduleData(List<ScheduleData> detailData,List<ScheduleData> oldDetailList);
	
	
	/**
	 * @Description (删除明细表数据)
	 * @param data
	 */
	public void deleteScheduleData(ScheduleData data);
	
	
	/* ScheduleData end */
	
	/**
	 * TODO 财报数据转为对应系统分析科目
	 * @param reportType 财报类型
	 * @param reportData 多期财报数据集合
	 * @return
	 */
	public List<Map<String, Double>> convertSubjcet(String modelClass, String reportType,
			List<Map<String, Double>> reportData) throws Exception ;
	
	/**
	 * TODO 财报数据转为对应系统分析科目
	 * @param reportType 财报类型
	 * @param reportData 一期财报数据集合
	 * @return
	 */
	public Map<String, Double> convertSubjcet(String modelClass, String reportType,
			Map<String, Double> reportData) throws Exception ;
	
	/**
	 * TODO 查询（前三年加当期加去年同期）资产负债表数据，并进行科目转换
	 * @param cusId 客户号
	 * @param year 财报年份
	 * @param month 财报月份
	 * @return 返回转换后的数据集合列表
	 * @throws Exception
	 */
	public List<Map<String, Double>> queryBalanceSheet(String cusId, int year, int month)throws Exception;
	
	/**
	 * TODO 查询 （前三年加当期加去年同期）损益表数据，并进行科目转换
	 * @param cusId 客户号
	 * @param year 财报年份
	 * @param month 财报月份
	 * @return 返回转换后的数据集合列表
	 * @throws Exception
	 */
	public List<Map<String, Double>> queryIncomeSheet(String cusId, int year, int month)throws Exception;
	
	/**
	 * TODO 查询 （前三年加当期加去年同期）现金流量表数据，并进行科目转换
	 * @param cusId 客户号
	 * @param year 财报年份
	 * @param month 财报月份
	 * @return 返回转换后的数据集合列表
	 * @throws Exception
	 */
	public List<Map<String, Double>> queryCashFlowSheet(String cusId, int year, int month)throws Exception;
	
	/**
	 * TODO 查询 （前三年加当期加去年同期）通用财务指标值，并进行科目转换
	 * @param cusId 客户号
	 * @param year 财报年份
	 * @param month 财报月份
	 * @return 返回转换后的数据集合列表
	 * @throws Exception
	 */
	public List<Map<String, Double>> queryFinanIndex(String cusId, int year, int month)throws Exception;
	
	
	/**
	 * @Description 获得财务分析结论数据集合
	 * @param taskId 任务号
	 * @param customerNo 客户号
	 * @return
	 */
	public Map<String,Object> genAnalysisConclusion(long taskId,
			String customerNo) throws Exception ;
	
	/**
	 * @Description 获得财务分析结论数据集合
	 * @param taskId 任务号
	 * @param customerNo 客户号
	 * @param whichPart 返回那一部分分析结果
	 * @return
	 */
	public Map<String,Object> genAnalysisConclusion(long taskId,
			String customerNo, String whichPart) throws Exception ;
	
	/**
	 * @Description 获得财务分析结论数据集合
	 * @param taskId 任务号
	 * @param customerNo 客户号
	 * @param finaRepYear 客户财报年份
	 * @param finaRepMonth 客户财报月份
	 * @param industry 客户行业标识
	 * @param cusFlag 客户分类标识
	 * @param balanceData 科目转换后的资产负债表数据
	 * @param incomeData 科目转换后的利润表原始数据
	 * @param cashFlowData 科目转换后的现金流量表原始数据
	 * @param needConvert 是否需要进行科目转换
	 * @param whichPart 返回那一部分分析结果
	 * @return
	 */
	public Map<String, Object> genAnalysisConclusion(long taskId, String customerNo,
			int finaRepYear, int finaRepMonth, String industry, String cusFlag,
			List<Map<String, Double>> balanceData,
			List<Map<String, Double>> incomeData,
			List<Map<String, Double>> cashFlowData,
			boolean needConvert, String whichPart) throws Exception;
	
	/**
	 * TODO 将客户财务数据按系统分析所需的结构保存，并分析生成异常项
	 * @param task
	 * @param validFlag 财务报表完整性验证，1：验证，其他：不验证
	 * @throws Exception
	 */
	public void saveFinaDataAndAnalyze(Task task, String validFlag) throws Exception;
}
