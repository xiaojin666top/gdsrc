package com.beawan.analysis.finansis.service;

import com.beawan.core.BaseService;
import com.beawan.analysis.finansis.bean.FnVerify;

import java.util.List;

/**
 * @author yzj
 */
public interface FnVerifyService extends BaseService<FnVerify> {

    /**
     * 批量保存列表
     */
    void saveVerifyList(List<FnVerify> list);
}
