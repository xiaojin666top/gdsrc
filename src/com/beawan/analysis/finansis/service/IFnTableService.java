package com.beawan.analysis.finansis.service;

import java.util.List;

import com.beawan.analysis.finansis.bean.FnCash;
import com.beawan.analysis.finansis.bean.FnConstruc;
import com.beawan.analysis.finansis.bean.FnFinancCost;
import com.beawan.analysis.finansis.bean.FnFixedAssetsBuilding;
import com.beawan.analysis.finansis.bean.FnFixedAssetsEquipment;
import com.beawan.analysis.finansis.bean.FnFixedAssetsLand;
import com.beawan.analysis.finansis.bean.FnLongTermPay;
import com.beawan.analysis.finansis.bean.FnManageCost;
import com.beawan.analysis.finansis.bean.FnOperatCost;
import com.beawan.analysis.finansis.bean.FnOtherPay;
import com.beawan.analysis.finansis.bean.FnOtherReceive;
import com.beawan.analysis.finansis.bean.FnPay;
import com.beawan.analysis.finansis.bean.FnPayNote;
import com.beawan.analysis.finansis.bean.FnPreReceive;
import com.beawan.analysis.finansis.bean.FnPrepay;
import com.beawan.analysis.finansis.bean.FnReceNote;
import com.beawan.analysis.finansis.bean.FnReceive;
import com.beawan.analysis.finansis.bean.FnInventory;
import com.beawan.analysis.finansis.bean.FnReceiveStat;
import com.beawan.analysis.finansis.bean.FnTableAnaly;

public interface IFnTableService {
	
	/**
	 * @Description (根据客户号和任务号查询明细表分析)
	 * @param custNo
	 *            客户号
	 * @return
	 */
	public FnTableAnaly findTableAnalyByTIdAndNo(Long taskId,String custNo) throws Exception;

	/**
	 * @Description (保存明细表分析)
	 * @param data
	 */
	public void saveTableAnaly(FnTableAnaly data) throws Exception;
	
	/**
	 * @Description (删除明细表分析)
	 * @param data
	 *            
	 */
	public void deleteTableAnaly(FnTableAnaly data) throws Exception;
	/**
	 * @Description (根据任务号查询长期应付款)
	 * @param taskId 任务编号
	 * @return
	 */
	public List<FnLongTermPay> findLongTermPayByTId(Long taskId) throws Exception;
		

	/**
	 * @Description (保存长期应付款)
	 * @param data
	 */
	public void saveLongTermPay(Long taskId, String jsonArray) throws Exception;
	
	/**
	 * @Description (删除长期应付款)
	 * @param data
	 *            
	 */
	public void deleteLongTermPay(FnLongTermPay data) throws Exception;
	
	
	/**
	 * @Description (根据任务号查询其他应付款)
	 * @param taskId 任务号
	 * @return
	 */
	public List<FnOtherPay> findOtherPayByTId(Long taskId) throws Exception;

	/**
	 * @Description (保存其他应付款)
	 * @param data
	 *            现金流量简要的结构分析)
	 */
	public void saveOtherPay(Long taskId, String jsonArray) throws Exception;
	
	/**
	 * @Description (删除其他应付款)
	 * @param data
	 *            现金流量简要的结构分析)
	 */
	public void deleteOtherPay(FnOtherPay data) throws Exception;
	
	
	
	
	/**
	 * @Description (根据任务号查询其他应收款)
	 * @param taskId 任务号
	 * @return
	 */
	public List<FnOtherReceive> findOtherReceiveByTId(Long taskId) throws Exception;
	
	/**
	 * @Description (保存其他应收款)
	 * @param data
	 */
	public void saveOtherReceive(Long taskId, String jsonArray) throws Exception;
	
	/**
	 * @Description (删除企业其他应收款)
	 * @param data
	 */
	public void deleteOtherReceive(FnOtherReceive data) throws Exception;
	
	
	
	/**
	 * @Description (根据任务号查询应付账款)
	 * @param taskId 任务号
	 * @return
	 */
	public List<FnPay> findPayByTId(Long taskId) throws Exception;

	/**
	 * @Description (保存企业应付账款)
	 * @param data
	 *            现金流量简要的结构分析)
	 */
	public void savePay(Long taskId,String jsonArray) throws Exception;
	
	/**
	 * @Description (删除企业应付账款)
	 * @param data
	 *            现金流量简要的结构分析)
	 */
	public void deletePay(FnPay data) throws Exception;
	
	
	
	/**
	 * @Description (根据任务号查询预付账款)
	 * @param taskId 任务号
	 * @return
	 */
	public List<FnPrepay> findPrepayByTId(Long taskId) throws Exception;

	/**
	 * @Description (保存预付账款)
	 * @param data
	 */
	public void savePrepay(Long taskId,String jsonArray) throws Exception;
	
	/**
	 * @Description (删除企业预付账款)
	 * @param data
	 */
	public void deletePrepay(FnPrepay data) throws Exception;
	
	
	
	/**
	 * @Description (根据任务号查询预收账款)
	 * @param taskId 任务号
	 * @return
	 */
	public List<FnPreReceive> findPreReceiveByTId(Long taskId) throws Exception;

	/**
	 * @Description (保存企业预收账款
	 *            现金流量简要的结构分析)
	 */
	public void savePreReceive(Long taskId,String jsonArray) throws Exception;
	
	/**
	 * @Description (删除企业预收账款)
	 * @param data
	 */
	public void deletePreReceive(FnPreReceive data) throws Exception;
	
	
	
	/**
	 * @Description (根据任务号查询应收账款)
	 * @param taskId 任务号
	 * @return
	 */
	public List<FnReceive> findReceiveByTId(Long taskId) throws Exception;

	/**
	 * @Description (保存企业应收账款)
	 * @param data
	 */
	public void saveReceive(Long taskId,String jsonArray) throws Exception;
	
	/**
	 * @Description (删除企业应收账款)
	 * @param data
	 */
	public void deleteReceive(FnReceive data) throws Exception;
	
	/**
	 * @Description (根据任务号查询应收账款统计信息)
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public List<FnReceiveStat> findReceiveStatByTId(Long taskId) throws Exception;

	/**
	 * @Description (保存企业应收账款统计信息)
	 * @param data
	 */
	public void saveReceiveStat(Long taskId,String jsonArray) throws Exception;
	
	/**
	 * @Description (删除企业应收账款统计信息)
	 * @param data
	 */
	public void deleteReceiveStat(FnReceiveStat data) throws Exception;

	/**
	 * 根据主键查询 货币资金
	 * @param id
	 * @return
	 */
	FnCash getFnCashById(Long id);

	/**
	 * 保存或更新 货币资金
	 * @param entity
	 * @return
	 */
	void saveFnCash(FnCash entity);

	/**
	 * 根据主键查询 存货
	 * @param id
	 * @return
	 */
	FnInventory getFnInventoryById(Long id);

	/**
	 * 保存或更新 存货
	 * @param entity
	 * @return
	 */
	void saveFnInventory(FnInventory entity);

	/**
	 * 根据主键查询 应收账款
	 * @param id
	 * @return
	 */
	FnReceive getFnReceiveById(Long id);

	/**
	 * 保存或更新 应收账款
	 * @param entity
	 * @return
	 */
	void saveFnReceive(FnReceive entity);

	/**
	 * 根据主键查询 其他应收账款
	 * @param id
	 * @return
	 */
	FnOtherReceive getFnOtherReceiveById(Long id);

	/**
	 * 保存或更新 其他应收账款
	 * @param entity
	 * @return
	 */
	void saveFnOtherReceive(FnOtherReceive entity);

	/**
	 * 根据主键查询 固定资产-土地
	 * @param id
	 * @return
	 */
	FnFixedAssetsLand getFnLandById(Long id);

	/**
	 * 保存或更新 固定资产-土地
	 * @param entity
	 * @return
	 */
	void saveFnLand(FnFixedAssetsLand entity);

	/**
	 * 根据主键查询 固定资产-房屋
	 * @param id
	 * @return
	 */
	FnFixedAssetsBuilding getFnBuildingById(Long id);

	/**
	 * 保存或更新 固定资产-房屋
	 * @param entity
	 * @return
	 */
	void saveFnBuilding(FnFixedAssetsBuilding entity);

	/**
	 * 根据主键查询 固定资产-设备
	 * @param id
	 * @return
	 */
	FnFixedAssetsEquipment getFnEquipmentById(Long id);

	/**
	 * 保存或更新 固定资产-设备
	 * @param entity
	 * @return
	 */
	void saveFnEquipment(FnFixedAssetsEquipment entity);

	/**
	 * 根据主键查询 预付账款
	 * @param id
	 * @return
	 */
	FnPrepay getFnPrepayById(Long id);

	/**
	 * 保存或更新 预付账款
	 * @param entity
	 * @return
	 */
	void saveFnPrepay(FnPrepay entity);

	/**
	 * 根据主键查询 应付账款
	 * @param id
	 * @return
	 */
	FnPay getFnPayById(Long id);

	/**
	 * 保存或更新 应付账款
	 * @param entity
	 * @return
	 */
	void saveFnPay(FnPay entity);

	/**
	 * 根据主键查询 其他应付账款
	 * @param id
	 * @return
	 */
	FnOtherPay getFnOtherPayById(Long id);

	/**
	 * 保存或更新 其他应付账款
	 * @param entity
	 * @return
	 */
	void saveFnOtherPay(FnOtherPay entity);

	/**
	 * 根据主键查询 预收账款
	 * @param id
	 * @return
	 */
	FnPreReceive getFnPreReceiveById(Long id);

	/**
	 * 保存或更新 预收账款
	 * @param entity
	 * @return
	 */
	void saveFnPreReceive(FnPreReceive entity);

	/**
	 * @Description (根据任务号查询货币资金)
	 * @param taskId 客户号
	 * @return
	 */
	public List<FnCash> findFnCashByTId(Long taskId) throws Exception;

	/**
	 * @Description (保存企业货币资金)
	 * @param data
	 */
	public void saveFnCash(Long taskId,String jsonArray) throws Exception;
	
	/**
	 * @Description (删除企业货币资金)
	 * @param data
	 */
	public void deleteFnCash(FnCash data) throws Exception;
	
	/**
	 * @Description (根据任务号查询在建工程)
	 * @param taskId任务号
	 * @return
	 */
	public List<FnConstruc> findFnConstrucByTId(Long taskId) throws Exception;

	/**
	 * @Description (保存企业在建工程)
	 * @param data
	 */
	public void saveFnConstruc(Long taskId,String jsonArray) throws Exception;
	
	/**
	 * @Description (删除企业在建工程)
	 * @param data
	 */
	public void deleteFnConstruc(FnConstruc data) throws Exception;
	
	/**
	 * @Description (根据任务号查询财务费用明细)
	 * @param taskId 任务号
	 * @return
	 */
	public List<FnFinancCost> findFinancCostByTId(Long taskId) throws Exception;

	/**
	 * @Description (保存财务费用明细
	 * @param data
	 */
	public void saveFinancCost(Long taskId,String jsonArray) throws Exception;
	
	/**
	 * @Description (删除财务费用明细)
	 * @param data
	 */
	public void deleteFinancCost(FnFinancCost data) throws Exception;
	
	/**
	 * @Description (根据任务号查询管理费用明细)
	 * @param taskId 任务号
	 * @return
	 */
	public List<FnManageCost> findManageCostByTId(Long taskId) throws Exception;

	/**
	 * @Description (保存管理明细)
	 * @param data
	 */
	public void saveManageCost(Long taskId,String jsonArray) throws Exception;
	
	/**
	 * @Description (删除管理明细)
	 * @param data
	 */
	public void deleteManageCost(FnManageCost data) throws Exception;
	
	/**
	 * @Description (根据任务号查询营业费用明细)
	 * @param taskId任务号
	 * @return
	 */
	public List<FnOperatCost> findOperatCostByTId(Long taskId) throws Exception;

	/**
	 * @Description (保存营业明细)
	 * @param data
	 */
	public void saveOperatCost(Long taskId,String jsonArray) throws Exception;
	
	/**
	 * @Description (删除营业明细)
	 * @param data
	 */
	public void deleteOperatCost(FnOperatCost data) throws Exception;
	
	/**
	 * @Description (根据任务号查询应付票据明细）
	 * @param taskId任务号
	 * @return
	 */
	public List<FnPayNote> findFnPayNoteByTId(Long taskId) throws Exception;

	/**
	 * @Description (保存应付票据明细）
	 * @param data
	 */
	public void saveFnPayNote(Long taskId,String jsonArray) throws Exception;
	
	/**
	 * @Description (删除应付票据明细）
	 * @param data
	 */
	public void deleteFnPayNote(FnPayNote data) throws Exception;
	
	/**
	 * @Description (根据任务号查询应收票据明细）
	 * @param taskId 任务号
	 * @return
	 */
	public List<FnReceNote> findFnReceNoteByTId(Long taskId) throws Exception;

	/**
	 * @Description (保存应收票据明细）
	 * @param data
	 */
	public void saveFnReceNote(Long taskId,String jsonArray) throws Exception;
	
	/**
	 * @Description (删除应收票据明细）
	 * @param data
	 */
	public void deleteFnReceNote(FnReceNote data) throws Exception;
	

	/**
	 * @Description (根据任务号查询存货明细）
	 * @param taskId 任务号
	 * @return
	 */
	public List<FnInventory> findFnInventoryByTId(Long taskId) throws Exception;

	/**
	 * @Description (保存存货明细）
	 * @param data
	 */
	public void saveFnInventory(Long taskId,String jsonArray) throws Exception;
	
	/**
	 * @Description (删除存货明细）
	 * @param data
	 */
	public void deleteFnInventory(FnInventory data) throws Exception;
	
	
	
	/**
	 * @Description (根据任务号查询固定资产-土地）
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public List<FnFixedAssetsLand> queryFnFixedAssetsLandByTId(Long taskId) throws Exception;

	/**
	 * @Description (保存固定资产-土地）
	 * @param data
	 */
	public void saveFnFixedAssetsLands(Long taskId,String jsonArray) throws Exception;
	
	/**
	 * @Description (删除固定资产-土地）
	 * @param data
	 */
	public void deleteFnFixedAssetsLand(FnFixedAssetsLand data) throws Exception;
	
	
	/**
	 * @Description (根据任务号查询固定资产-房屋）
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public List<FnFixedAssetsBuilding> queryFnFixedAssetsBuildingByTId(Long taskId) throws Exception;

	/**
	 * @Description (保存固定资产-房屋）
	 * @param data
	 */
	public void saveFnFixedAssetsBuildings(Long taskId,String jsonArray) throws Exception;
	
	/**
	 * @Description (删除固定资产-房屋）
	 * @param data
	 */
	public void deleteFnFixedAssetsBuilding(FnFixedAssetsBuilding data) throws Exception;

	
	/**
	 * @Description (根据任务号查询固定资产-设备）
	 * @param taskId
	 *            任务号
	 * @return
	 */
	public List<FnFixedAssetsEquipment> queryFnFixedAssetsEquipmentByTId(Long taskId) throws Exception;

	/**
	 * @Description (保存固定资产-设备）
	 * @param data
	 */
	public void saveFnFixedAssetsEquipments(Long taskId,String jsonArray) throws Exception;
	
	/**
	 * @Description (删除固定资产-设备）
	 * @param data
	 */
	public void deleteFnFnFixedAssetsEquipment(FnFixedAssetsEquipment data) throws Exception;
	
	
	/**
	 * @Description (删除所有信息）
	 * @param taskId
	 */
	public void deleteAll(Long taskId,String customerNo) throws Exception;
}
