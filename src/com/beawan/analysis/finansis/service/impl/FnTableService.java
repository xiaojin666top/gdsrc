package com.beawan.analysis.finansis.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.platform.util.GsonUtil;
import com.platform.util.TimestampMorpher;
import net.sf.json.util.JSONUtils;
import org.springframework.stereotype.Service;

import com.beawan.analysis.finansis.bean.FnCash;
import com.beawan.analysis.finansis.bean.FnConstruc;
import com.beawan.analysis.finansis.bean.FnFinancCost;
import com.beawan.analysis.finansis.bean.FnFixedAssetsBuilding;
import com.beawan.analysis.finansis.bean.FnFixedAssetsEquipment;
import com.beawan.analysis.finansis.bean.FnFixedAssetsLand;
import com.beawan.analysis.finansis.bean.FnLongTermPay;
import com.beawan.analysis.finansis.bean.FnManageCost;
import com.beawan.analysis.finansis.bean.FnOperatCost;
import com.beawan.analysis.finansis.bean.FnOtherPay;
import com.beawan.analysis.finansis.bean.FnOtherReceive;
import com.beawan.analysis.finansis.bean.FnPay;
import com.beawan.analysis.finansis.bean.FnPayNote;
import com.beawan.analysis.finansis.bean.FnPreReceive;
import com.beawan.analysis.finansis.bean.FnPrepay;
import com.beawan.analysis.finansis.bean.FnReceNote;
import com.beawan.analysis.finansis.bean.FnReceive;
import com.beawan.analysis.finansis.bean.FnInventory;
import com.beawan.analysis.finansis.bean.FnReceiveStat;
import com.beawan.analysis.finansis.bean.FnTableAnaly;
import com.beawan.analysis.finansis.dao.IFnTableDao;
import com.beawan.analysis.finansis.service.IFnTableService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service("fnTableSV")
public class FnTableService implements IFnTableService{
	
	
	@Resource
	protected IFnTableDao fnTableDao;
	
	public FnTableAnaly findTableAnalyByTIdAndNo(Long taskId,String custNo) throws Exception{
		FnTableAnaly tableAnaly  =  fnTableDao.queryTableAnalyByTIdAndNo(taskId, custNo);
		return tableAnaly;
	}

	@Override
	public void saveTableAnaly(FnTableAnaly data) throws Exception{
		fnTableDao.saveTableAnaly(data);
	}
	
	@Override
	public void deleteTableAnaly(FnTableAnaly data) throws Exception{
		fnTableDao.deleteTableAnaly(data);
	}

	@Override
	public List<FnLongTermPay> findLongTermPayByTId(Long taskId) throws Exception {
		// TODO Auto-generated method stub
		return fnTableDao.queryLongTermPayByTId(taskId);
	}

	@Override
	public void saveLongTermPay(Long taskId,String jsonArray) throws Exception {
		// TODO Auto-generated method stub
		List<FnLongTermPay> list=fnTableDao.queryLongTermPayByTId(taskId);
		for(FnLongTermPay data:list) {
			fnTableDao.deleteLongTermPay(data);
		}
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				//将时间字符串适配timestamp类型
				String[] formats={"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd"};
				JSONUtils.getMorpherRegistry().registerMorpher(new TimestampMorpher(formats));
				JSONObject datastr=JSONObject.fromObject(array.get(i));
				FnLongTermPay data=(FnLongTermPay)JSONObject.toBean(datastr, FnLongTermPay.class);
				fnTableDao.saveLongTermPay(data);
			}
		}
	}

	@Override
	public void deleteLongTermPay(FnLongTermPay data) throws Exception {
		// TODO Auto-generated method stub
		fnTableDao.deleteLongTermPay(data);
	}



	@Override
	public List<FnOtherPay> findOtherPayByTId(Long taskId) throws Exception {
		// TODO Auto-generated method stub
		return fnTableDao.queryOtherPayByTId(taskId);
	}

	@Override
	public void saveOtherPay(Long taskId, String jsonArray) throws Exception {
		// TODO Auto-generated method stub
		/*List<FnOtherPay> list=fnTableDao.queryOtherPayByTId(taskId);
		for(FnOtherPay data:list) {
			fnTableDao.deleteOtherPay(data);
		}
		*/
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				//将时间字符串适配timestamp类型
				String[] formats={"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd"};
				JSONUtils.getMorpherRegistry().registerMorpher(new TimestampMorpher(formats));
				JSONObject datastr=JSONObject.fromObject(array.get(i));
				FnOtherPay data=(FnOtherPay)JSONObject.toBean(datastr, FnOtherPay.class);
				fnTableDao.saveOtherPay(data);
			}
		}
	}

	@Override
	public void deleteOtherPay(FnOtherPay data) throws Exception {
		// TODO Auto-generated method stub
		fnTableDao.deleteOtherPay(data);
	}

	@Override
	public List<FnOtherReceive> findOtherReceiveByTId(Long taskId) throws Exception {
		// TODO Auto-generated method stub
		return fnTableDao.queryOtherReceiveByTId(taskId);
	}

	@Override
	public void saveOtherReceive(Long taskId,String jsonArray) throws Exception {
		// TODO Auto-generated method stub
		/*List<FnOtherReceive> list=fnTableDao.queryOtherReceiveByTId(taskId);
		for(FnOtherReceive data:list) {
			fnTableDao.deleteOtherReceive(data);
		}*/
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				//将时间字符串适配timestamp类型
				String[] formats={"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd"};
				JSONUtils.getMorpherRegistry().registerMorpher(new TimestampMorpher(formats));
				JSONObject datastr=JSONObject.fromObject(array.get(i));
				FnOtherReceive data=(FnOtherReceive)JSONObject.toBean(datastr, FnOtherReceive.class);
				data.setTaskId(taskId);
				fnTableDao.saveOtherReceive(data);
			}
		}
	}

	@Override
	public void deleteOtherReceive(FnOtherReceive data) throws Exception {
		// TODO Auto-generated method stub
		fnTableDao.deleteOtherReceive(data);
	}

	@Override
	public List<FnPay> findPayByTId(Long taskId) throws Exception {
		// TODO Auto-generated method stub
		return fnTableDao.queryPayByTId(taskId);
	}

	@Override
	public void savePay(Long taskId,String jsonArray) throws Exception {
		// TODO Auto-generated method stub
		/*List<FnPay> list=fnTableDao.queryPayByTId(taskId);
		for(FnPay data:list) {
			fnTableDao.deletePay(data);
		}*/
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				//将时间字符串适配timestamp类型
				String[] formats={"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd"};
				JSONUtils.getMorpherRegistry().registerMorpher(new TimestampMorpher(formats));
				JSONObject datastr=JSONObject.fromObject(array.get(i));
				FnPay data=(FnPay)JSONObject.toBean(datastr, FnPay.class);
				fnTableDao.savePay(data);
			}
		}
	}

	@Override
	public void deletePay(FnPay data) throws Exception {
		// TODO Auto-generated method stub
		fnTableDao.deletePay(data);
	}

	@Override
	public List<FnPrepay> findPrepayByTId(Long taskId) throws Exception {
		// TODO Auto-generated method stub
		return fnTableDao.queryPrepayByTId(taskId);
	}

	@Override
	public void savePrepay(Long taskId,String jsonArray) throws Exception {
		// TODO Auto-generated method stub
		/*List<FnPrepay> list=fnTableDao.queryPrepayByTId(taskId);
		for(FnPrepay data:list) {
			fnTableDao.deletePrepay(data);
		}*/
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				//将时间字符串适配timestamp类型
				String[] formats={"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd"};
				JSONUtils.getMorpherRegistry().registerMorpher(new TimestampMorpher(formats));
				JSONObject datastr=JSONObject.fromObject(array.get(i));
				FnPrepay data=(FnPrepay)JSONObject.toBean(datastr, FnPrepay.class);
				data.setTaskId(taskId);
				fnTableDao.savePrepay(data);
			}
		}
	}

	@Override
	public void deletePrepay(FnPrepay data) throws Exception {
		fnTableDao.deletePrepay(data);
	}

	@Override
	public List<FnPreReceive> findPreReceiveByTId(Long taskId) throws Exception {
		return fnTableDao.queryPreReceiveByTId(taskId);
	}

	@Override
	public void savePreReceive(Long taskId,String jsonArray) throws Exception {
		List<FnPreReceive> list=fnTableDao.queryPreReceiveByTId(taskId);
		for(FnPreReceive data:list) {
			fnTableDao.deletePreReceive(data);
		}
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				//将时间字符串适配timestamp类型
				String[] formats={"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd"};
				JSONUtils.getMorpherRegistry().registerMorpher(new TimestampMorpher(formats));
				JSONObject datastr=JSONObject.fromObject(array.get(i));
				FnPreReceive data=(FnPreReceive)JSONObject.toBean(datastr, FnPreReceive.class);
				fnTableDao.savePreReceive(data);
			}
		}
	}

	@Override
	public void deletePreReceive(FnPreReceive data) throws Exception {
		fnTableDao.deletePreReceive(data);
	}

	@Override
	public List<FnReceive> findReceiveByTId(Long taskId) throws Exception {
		
		return fnTableDao.queryReceiveByTId(taskId);
	}

	@Override
	public void saveReceive(Long taskId,String jsonArray) throws Exception {
		/*List<FnReceive> list=fnTableDao.queryReceiveByTId(taskId);
		for(FnReceive data:list) {
			fnTableDao.deleteReceive(data);
		}*/
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				//将时间字符串适配timestamp类型
				String[] formats={"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd"};
				JSONUtils.getMorpherRegistry().registerMorpher(new TimestampMorpher(formats));
				JSONObject datastr=JSONObject.fromObject(array.get(i));
				FnReceive data=(FnReceive)JSONObject.toBean(datastr, FnReceive.class);
				data.setTaskId(taskId);
				fnTableDao.saveReceive(data);
			}
		}
		
	}

	@Override
	public void deleteReceive(FnReceive data) throws Exception {
		// TODO Auto-generated method stub
		fnTableDao.deleteReceive(data);
	}
	
	
	@Override
	public List<FnReceiveStat> findReceiveStatByTId(Long taskId) throws Exception {
		return fnTableDao.queryReceiveStatByTId(taskId);
	}

	@Override
	public void saveReceiveStat(Long taskId, String jsonArray) throws Exception {
		
		List<FnReceiveStat> list = fnTableDao.queryReceiveStatByTId(taskId);
		for(FnReceiveStat data:list) {
			fnTableDao.deleteReceiveStat(data);
		}
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				//将时间字符串适配timestamp类型
				String[] formats={"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd"};
				JSONUtils.getMorpherRegistry().registerMorpher(new TimestampMorpher(formats));
				JSONObject datastr=JSONObject.fromObject(array.get(i));
				FnReceiveStat data=(FnReceiveStat)JSONObject.toBean(datastr, FnReceiveStat.class);
				fnTableDao.saveReceiveStat(data);
			}
		}
		
	}

	@Override
	public void deleteReceiveStat(FnReceiveStat data) throws Exception {
		fnTableDao.deleteReceiveStat(data);
	}

	@Override
	public FnCash getFnCashById(Long id) {
		return fnTableDao.queryFnCashById(id);
	}

	@Override
	public void saveFnCash(FnCash entity) {
		fnTableDao.saveFnCash(entity);
	}

	@Override
	public FnInventory getFnInventoryById(Long id) {
		return fnTableDao.queryFnInventoryById(id);
	}

	@Override
	public void saveFnInventory(FnInventory entity) {
		fnTableDao.saveFnInventory(entity);
	}

	@Override
	public FnReceive getFnReceiveById(Long id) {
		return fnTableDao.queryFnReceiveById(id);
	}

	@Override
	public void saveFnReceive(FnReceive entity) {
		fnTableDao.saveReceive(entity);
	}

	@Override
	public FnOtherReceive getFnOtherReceiveById(Long id) {
		return fnTableDao.queryFnOtherReceiveById(id);
	}

	@Override
	public void saveFnOtherReceive(FnOtherReceive entity) {
		fnTableDao.saveOtherReceive(entity);
	}

	@Override
	public FnPrepay getFnPrepayById(Long id) {
		return fnTableDao.queryFnPrepayById(id);
	}

	@Override
	public void saveFnPrepay(FnPrepay entity) {
		fnTableDao.savePrepay(entity);
	}

	@Override
	public FnPay getFnPayById(Long id) {
		return fnTableDao.queryFnPayById(id);
	}

	@Override
	public void saveFnPay(FnPay entity) {
		fnTableDao.savePay(entity);
	}

	@Override
	public FnOtherPay getFnOtherPayById(Long id) {
		return fnTableDao.queryFnOtherPayById(id);
	}

	@Override
	public void saveFnOtherPay(FnOtherPay entity) {
		fnTableDao.saveOtherPay(entity);
	}

	@Override
	public FnPreReceive getFnPreReceiveById(Long id) {
		return fnTableDao.queryFnPreReceiveById(id);
	}

	@Override
	public void saveFnPreReceive(FnPreReceive entity) {
		fnTableDao.savePreReceive(entity);
	}

	@Override
	public FnFixedAssetsLand getFnLandById(Long id) {
		return fnTableDao.queryFnLandById(id);
	}

	@Override
	public void saveFnLand(FnFixedAssetsLand entity) {
		fnTableDao.saveFnFixedAssetsLand(entity);
	}

	@Override
	public FnFixedAssetsBuilding getFnBuildingById(Long id) {
		return fnTableDao.queryFnBuildingById(id);
	}

	@Override
	public void saveFnBuilding(FnFixedAssetsBuilding entity) {
		fnTableDao.saveFnFixedAssetsBuilding(entity);
	}

	@Override
	public FnFixedAssetsEquipment getFnEquipmentById(Long id) {
		return fnTableDao.queryFnEquipmentById(id);
	}

	@Override
	public void saveFnEquipment(FnFixedAssetsEquipment entity) {
		fnTableDao.saveFnFixedAssetsEquipment(entity);
	}

	@Override
	public List<FnCash> findFnCashByTId(Long taskId) throws Exception {
		return fnTableDao.queryFnCashByTId(taskId);
	}

	@Override
	public void saveFnCash(Long taskId,String jsonArray) throws Exception {
		/*List<FnCash> list=fnTableDao.queryFnCashByTId(taskId);
		for(FnCash data:list) {
			fnTableDao.deleteFnCash(data);
		}*/
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				//将时间字符串适配timestamp类型
				String[] formats={"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd"};
				JSONUtils.getMorpherRegistry().registerMorpher(new TimestampMorpher(formats));
				JSONObject datastr=JSONObject.fromObject(array.get(i));
				FnCash data=(FnCash)JSONObject.toBean(datastr, FnCash.class);
				fnTableDao.saveFnCash(data);
			}
		}
	}

	@Override
	public void deleteFnCash(FnCash data) throws Exception {
		// TODO Auto-generated method stub
		fnTableDao.deleteFnCash(data);
	}

	@Override
	public List<FnConstruc> findFnConstrucByTId(Long taskId) throws Exception {
	
		return fnTableDao.queryFnConstrucByTId(taskId);
	}

	@Override
	public void saveFnConstruc(Long taskId,String jsonArray) throws Exception {
		List<FnConstruc> list=fnTableDao.queryFnConstrucByTId(taskId);
		for(FnConstruc data:list) {
			fnTableDao.deleteFnConstruc(data);
		}
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				//将时间字符串适配timestamp类型
				String[] formats={"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd"};
				JSONUtils.getMorpherRegistry().registerMorpher(new TimestampMorpher(formats));
				JSONObject datastr=JSONObject.fromObject(array.get(i));
				FnConstruc data=(FnConstruc)JSONObject.toBean(datastr, FnConstruc.class);
				fnTableDao.saveFnConstruc(data);
			}
		}
	}

	@Override
	public void deleteFnConstruc(FnConstruc data) throws Exception {
		// TODO Auto-generated method stub
		fnTableDao.deleteFnConstruc(data);
	}

	@Override
	public List<FnFinancCost> findFinancCostByTId(Long taskId) throws Exception {
	
		return fnTableDao.queryFinancCostByTId(taskId);
	}

	@Override
	public void saveFinancCost(Long taskId,String jsonArray) throws Exception {
	
		List<FnFinancCost> list=fnTableDao.queryFinancCostByTId(taskId);
		for(FnFinancCost data:list) {
			fnTableDao.deleteFinancCost(data);
		}
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				//将时间字符串适配timestamp类型
				String[] formats={"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd"};
				JSONUtils.getMorpherRegistry().registerMorpher(new TimestampMorpher(formats));
				JSONObject datastr=JSONObject.fromObject(array.get(i));
				FnFinancCost data=(FnFinancCost)JSONObject.toBean(datastr, FnFinancCost.class);
				fnTableDao.saveFinancCost(data);
			}
		}
	}

	@Override
	public void deleteFinancCost(FnFinancCost data) throws Exception {
		
		fnTableDao.deleteFinancCost(data);
	}

	@Override
	public List<FnManageCost> findManageCostByTId(Long taskId) throws Exception {
		
		return fnTableDao.queryManageCostByTId(taskId);
	}

	@Override
	public void saveManageCost(Long taskId,String jsonArray) throws Exception {
	
		List<FnManageCost> list=fnTableDao.queryManageCostByTId(taskId);
		for(FnManageCost data:list) {
			fnTableDao.deleteManageCost(data);
		}
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				//将时间字符串适配timestamp类型
				String[] formats={"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd"};
				JSONUtils.getMorpherRegistry().registerMorpher(new TimestampMorpher(formats));
				JSONObject datastr=JSONObject.fromObject(array.get(i));
				FnFinancCost data=(FnFinancCost)JSONObject.toBean(datastr, FnFinancCost.class);
				fnTableDao.saveFinancCost(data);
			}
		}
	}

	@Override
	public void deleteManageCost(FnManageCost data) throws Exception {

		fnTableDao.deleteManageCost(data);
	}

	@Override
	public List<FnOperatCost> findOperatCostByTId(Long taskId) throws Exception {
	
		return fnTableDao.queryOperatCostByTId(taskId);
	}

	@Override
	public void saveOperatCost(Long taskId,String  jsonArray) throws Exception {
		
		List<FnOperatCost> list=fnTableDao.queryOperatCostByTId(taskId);
		for(FnOperatCost data:list) {
			fnTableDao.deleteOperatCost(data);
		}
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				//将时间字符串适配timestamp类型
				String[] formats={"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd"};
				JSONUtils.getMorpherRegistry().registerMorpher(new TimestampMorpher(formats));
				JSONObject datastr=JSONObject.fromObject(array.get(i));
				FnOperatCost data=(FnOperatCost)JSONObject.toBean(datastr, FnOperatCost.class);
				fnTableDao.saveOperatCost(data);
			}
		}
	}

	@Override
	public void deleteOperatCost(FnOperatCost data) throws Exception {
	
		fnTableDao.deleteOperatCost(data);
	}

	@Override
	public List<FnPayNote> findFnPayNoteByTId(Long taskId) throws Exception {
	
		return fnTableDao.queryFnPayNoteByTId(taskId);
	}

	@Override
	public void saveFnPayNote(Long taskId,String jsonArray) throws Exception {
	
		List<FnPayNote> list=fnTableDao.queryFnPayNoteByTId(taskId);
		for(FnPayNote data:list) {
			fnTableDao.deleteFnPayNote(data);
		}
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				//将时间字符串适配timestamp类型
				String[] formats={"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd"};
				JSONUtils.getMorpherRegistry().registerMorpher(new TimestampMorpher(formats));
				JSONObject datastr=JSONObject.fromObject(array.get(i));
				FnPayNote data=(FnPayNote)JSONObject.toBean(datastr, FnPayNote.class);
				fnTableDao.saveFnPayNote(data);
			}
		}
	}

	@Override
	public void deleteFnPayNote(FnPayNote data) throws Exception {

		fnTableDao.deleteFnPayNote(data);
	}

	@Override
	public List<FnReceNote> findFnReceNoteByTId(Long taskId) throws Exception {
	
		return fnTableDao.queryFnReceNoteByTId(taskId);
	}

	@Override
	public void saveFnReceNote(Long taskId,String  jsonArray) throws Exception {

		List<FnReceNote> list=fnTableDao.queryFnReceNoteByTId(taskId);
		for(FnReceNote data:list) {
			fnTableDao.deleteFnReceNote(data);
		}
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				//将时间字符串适配timestamp类型
				String[] formats={"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd"};
				JSONUtils.getMorpherRegistry().registerMorpher(new TimestampMorpher(formats));
				JSONObject datastr=JSONObject.fromObject(array.get(i));
				FnReceNote data=(FnReceNote)JSONObject.toBean(datastr, FnReceNote.class);
				fnTableDao.saveFnReceNote(data);
			}
		}
	}

	@Override
	public void deleteFnReceNote(FnReceNote data) throws Exception {

		fnTableDao.deleteFnReceNote(data);
	}

	@Override
	public List<FnInventory> findFnInventoryByTId(Long taskId) throws Exception {
	
		return fnTableDao.queryFnInventoryByTId(taskId);
	}

	@Override
	public void saveFnInventory(Long taskId,String jsonArray) throws Exception {
		/*List<FnInventory> list=fnTableDao.queryFnInventoryByTId(taskId);
		for(FnInventory data:list) {
			fnTableDao.deleteFnInventory(data);
		}*/

		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				//将时间字符串适配timestamp类型
				String[] formats={"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd"};
				JSONUtils.getMorpherRegistry().registerMorpher(new TimestampMorpher(formats));
				JSONObject datastr=JSONObject.fromObject(array.get(i));
				FnInventory data=(FnInventory)JSONObject.toBean(datastr, FnInventory.class);
				data.setTaskId(taskId);
				fnTableDao.saveFnInventory(data);
			}
		}
	}

	@Override
	public void deleteFnInventory(FnInventory data) throws Exception {

		fnTableDao.deleteFnInventory(data);
	}

	@Override
	public List<FnFixedAssetsLand> queryFnFixedAssetsLandByTId(Long taskId) throws Exception {

		return fnTableDao.queryFnFixedAssetsLandByTId(taskId);
	}

	

	@Override
	public void deleteFnFixedAssetsLand(FnFixedAssetsLand data) throws Exception {
		
		fnTableDao.deleteFnFixedAssetsLand(data);
	}

	@Override
	public List<FnFixedAssetsBuilding> queryFnFixedAssetsBuildingByTId(Long taskId) throws Exception {
		
		return fnTableDao.queryFnFixedAssetsBuildingByTId(taskId);
	}

	

	@Override
	public void deleteFnFixedAssetsBuilding(FnFixedAssetsBuilding data) throws Exception {
		
		fnTableDao.deleteFnFixedAssetsBuilding(data);
	}

	@Override
	public List<FnFixedAssetsEquipment> queryFnFixedAssetsEquipmentByTId(Long taskId) throws Exception {

		return fnTableDao.queryFnFixedAssetsEquipmentByTId(taskId);
	}

	

	@Override
	public void deleteFnFnFixedAssetsEquipment(FnFixedAssetsEquipment data) throws Exception {
		
		fnTableDao.deleteFnFnFixedAssetsEquipment(data);
	}

	@Override
	public void saveFnFixedAssetsLands(Long taskId, String jsonArray) throws Exception {
		
		/*List<FnFixedAssetsLand> list=fnTableDao.queryFnFixedAssetsLandByTId(taskId);
		for(FnFixedAssetsLand data:list) {
			fnTableDao.deleteFnFixedAssetsLand(data);
		}*/
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				//将时间字符串适配timestamp类型
				String[] formats={"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd"};
				JSONUtils.getMorpherRegistry().registerMorpher(new TimestampMorpher(formats));
				JSONObject datastr=JSONObject.fromObject(array.get(i));
				FnFixedAssetsLand land=(FnFixedAssetsLand)JSONObject.toBean(datastr, FnFixedAssetsLand.class);
				land.setTaskId(taskId);
				fnTableDao.saveFnFixedAssetsLand(land);
			}
		}
		
	}

	@Override
	public void saveFnFixedAssetsBuildings(Long taskId, String jsonArray) throws Exception {
		/*List<FnFixedAssetsBuilding> list=fnTableDao.queryFnFixedAssetsBuildingByTId(taskId);
		if(list!=null && list.size()!=0) {
			for (FnFixedAssetsBuilding data : list) {
				fnTableDao.deleteFnFixedAssetsBuilding(data);
			}
		}*/
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				//将时间字符串适配timestamp类型
				String[] formats={"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd"};
				JSONUtils.getMorpherRegistry().registerMorpher(new TimestampMorpher(formats));
				JSONObject datastr=JSONObject.fromObject(array.get(i));
				FnFixedAssetsBuilding building=(FnFixedAssetsBuilding)JSONObject.toBean(datastr, FnFixedAssetsBuilding.class);
				building.setTaskId(taskId);
				fnTableDao.saveFnFixedAssetsBuilding(building);
			}
		}
		
	}

	@Override
	public void saveFnFixedAssetsEquipments(Long taskId, String jsonArray) throws Exception {
		/*
		List<FnFixedAssetsEquipment> list=fnTableDao.queryFnFixedAssetsEquipmentByTId(taskId);
		for(FnFixedAssetsEquipment data:list) {
			fnTableDao.deleteFnFnFixedAssetsEquipment(data);
		}*/
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				//将时间字符串适配timestamp类型
				String[] formats={"yyyy-MM-dd HH:mm:ss","yyyy-MM-dd"};
				JSONUtils.getMorpherRegistry().registerMorpher(new TimestampMorpher(formats));
				JSONObject datastr=JSONObject.fromObject(array.get(i));
				FnFixedAssetsEquipment equipment=(FnFixedAssetsEquipment)JSONObject.toBean(datastr, FnFixedAssetsEquipment.class);
				equipment.setTaskId(taskId);
				fnTableDao.saveFnFixedAssetsEquipment(equipment);
			}
		}
	}

	@Override
	public void deleteAll(Long taskId, String customerNo) throws Exception{
		// TODO Auto-generated method stub
//		FnTableAnaly fnTableAnaly=this.findTableAnalyByTIdAndNo(taskId, customerNo);
//		if(fnTableAnaly!=null) {
//			fnTableDao.deleteTableAnaly(fnTableAnaly);
//		}
		List<FnCash> fnCashs=this.findFnCashByTId(taskId);
		if(fnCashs!=null) {
		for(FnCash fnCash:fnCashs) {
			fnTableDao.deleteFnCash(fnCash);
		}
		}
		
		List<FnConstruc> fnConstrucs=this.findFnConstrucByTId(taskId);
		if(fnConstrucs!=null) {
		for(FnConstruc fnConstruc:fnConstrucs) {
			fnTableDao.deleteFnConstruc(fnConstruc);
		}
		}
		
		List<FnFinancCost> fnFinancCosts=this.findFinancCostByTId(taskId);
		if(fnFinancCosts!=null) {
		for(FnFinancCost fnFinancCost:fnFinancCosts) {
			fnTableDao.deleteFinancCost(fnFinancCost);
		}
		}
		
		List<FnFixedAssetsBuilding> buildings=this.queryFnFixedAssetsBuildingByTId(taskId);
		if(buildings!=null) {
		for(FnFixedAssetsBuilding building:buildings) {
			fnTableDao.deleteFnFixedAssetsBuilding(building);
		}
		}
		
		List<FnFixedAssetsEquipment> equipments=this.queryFnFixedAssetsEquipmentByTId(taskId);
		if(equipments!=null) {
		for(FnFixedAssetsEquipment equipment:equipments) {
			fnTableDao.deleteFnFnFixedAssetsEquipment(equipment);
		}
		}
		
		List<FnFixedAssetsLand> lands=this.queryFnFixedAssetsLandByTId(taskId);
		if(lands!=null) {
		for(FnFixedAssetsLand land:lands) {
			fnTableDao.deleteFnFixedAssetsLand(land);
		}
		}
		
		List<FnInventory> fnInventories=this.findFnInventoryByTId(taskId);
		if(fnInventories!=null) {
		for(FnInventory fnInventory:fnInventories) {
			fnTableDao.deleteFnInventory(fnInventory);
		}
		}
		
		List<FnLongTermPay> longTermPays=this.findLongTermPayByTId(taskId);
		if(longTermPays!=null) {
		for(FnLongTermPay longTermPay:longTermPays) {
			fnTableDao.deleteLongTermPay(longTermPay);
		}
		}
		
		List<FnManageCost> manageCosts=this.findManageCostByTId(taskId);
		if(manageCosts!=null) {
		for(FnManageCost fnManageCost:manageCosts) {
			fnTableDao.deleteManageCost(fnManageCost);
		}
		}
		
		List<FnOperatCost> operatCosts=this.findOperatCostByTId(taskId);
		if(operatCosts!=null) {
		for(FnOperatCost operatCost:operatCosts) {
			fnTableDao.deleteOperatCost(operatCost);
		}
		}
		List<FnOtherPay> otherPays=this.findOtherPayByTId(taskId);
		if(otherPays!=null) {
		for(FnOtherPay otherPay:otherPays) {
			fnTableDao.deleteOtherPay(otherPay);
		}
		}
		
		List<FnOtherReceive> otherReceives=this.findOtherReceiveByTId(taskId);
		if(otherReceives!=null) {
		for(FnOtherReceive otherReceive:otherReceives) {
			fnTableDao.deleteOtherReceive(otherReceive);
		}
		}
		List<FnPay> fnPays=this.findPayByTId(taskId);
		if(fnPays!=null) {
		for(FnPay pay:fnPays) {
			fnTableDao.deletePay(pay);
		}
		}
		
		List<FnPayNote> fnPayNotes=this.findFnPayNoteByTId(taskId);
		if(fnPayNotes!=null) {
		for(FnPayNote fnPayNote:fnPayNotes ) {
			fnTableDao.deleteFnPayNote(fnPayNote);
		}
		}
		
		List<FnPrepay> prepays=this.findPrepayByTId(taskId);
		if(prepays!=null) {
		for(FnPrepay prepay:prepays) {
			fnTableDao.deletePrepay(prepay);
		}
		}
		
		List<FnPreReceive> preReceives=this.findPreReceiveByTId(taskId);
		if(preReceives!=null) {
		for(FnPreReceive preReceive:preReceives) {
			fnTableDao.deletePreReceive(preReceive);
		}
		}
		
		List<FnReceive> receives=this.findReceiveByTId(taskId);
		if(receives!=null) {
		for(FnReceive fnReceive:receives) {
			fnTableDao.deleteReceive(fnReceive);
		}
		}
		
		List<FnReceiveStat> fnReceiveStats=this.findReceiveStatByTId(taskId);
		if(fnReceiveStats!=null) {
		for(FnReceiveStat fnReceiveStat:fnReceiveStats) {
			fnTableDao.deleteReceiveStat(fnReceiveStat);
		}
		}
		
		List<FnReceNote> receNotes=this.findFnReceNoteByTId(taskId);
		if(receNotes!=null) {
		for(FnReceNote receNote:receNotes) {
			fnTableDao.deleteFnReceNote(receNote);
		}
		}
		
		
	}

}
