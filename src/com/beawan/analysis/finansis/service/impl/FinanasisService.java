package com.beawan.analysis.finansis.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.Interface.GetErrExplainData;
import com.beawan.Interface.GetErrTemp;
import com.beawan.Interface.SaveReportData;
import com.beawan.Interface.SaveReportDataTr;
import com.beawan.analysis.abnormal.bean.ErrData;
import com.beawan.analysis.abnormal.bean.ErrDataTr;
import com.beawan.analysis.abnormal.bean.ErrExplainData;
import com.beawan.analysis.abnormal.dao.IErrDataTrDao;
import com.beawan.analysis.abnormal.service.IAbnormalService;
import com.beawan.analysis.abnormal.uitls.GetErrNameReason;
import com.beawan.analysis.finansis.bean.ReportData;
import com.beawan.analysis.finansis.bean.ReportDataTR;
import com.beawan.analysis.finansis.bean.ScheduleData;
import com.beawan.analysis.finansis.dao.IReportDataDao;
import com.beawan.analysis.finansis.dao.IReportDataTRDao;
import com.beawan.analysis.finansis.dao.IScheduleDataDao;
import com.beawan.analysis.finansis.service.IFinanasisService;
import com.beawan.analysis.finansis.service.IFnTableService;
import com.beawan.analysis.report.CommonResult;
import com.beawan.analysis.report.FinalCommomResult;
import com.beawan.base.service.ISysDicSV;
import com.beawan.base.service.ITableSubjCodeSV;
import com.beawan.common.BusinessException;
import com.beawan.common.SysConstants;
import com.beawan.common.util.SystemUtil;
import com.beawan.customer.service.ICusFSToolSV;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.beawan.exterinvoke.util.FncStatMapUtil;
import com.beawan.library.bean.ErrTemplet;
import com.beawan.library.bean.IndustryAnalysis;
import com.beawan.library.bean.IndustryQRadio;
import com.beawan.library.bean.TreeData;
import com.beawan.library.service.IIndustrySV;
import com.beawan.library.service.ITempletSV;
import com.beawan.survey.custInfo.bean.CompFinance;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.beawan.survey.custInfo.service.ICompFinanceSV;
import com.beawan.survey.custInfo.service.ICompFinancingSV;
import com.beawan.survey.loanInfo.bean.ApplyInfo;
import com.beawan.survey.loanInfo.service.IApplyInfoSV;
import com.beawan.task.bean.Task;
import com.beawan.task.dao.ITaskDAO;
import com.beawan.utils.MapUtil;
import com.platform.util.ExceptionUtil;
import com.platform.util.StringUtil;

import net.sf.json.JSONObject;

@Service("finanasisSV")
public class FinanasisService implements IFinanasisService, SaveReportDataTr<ReportDataTR>, SaveReportData<ReportData> {

	@Resource
	protected IReportDataDao reportDataDao;
	@Resource
	protected IReportDataTRDao reportDataTRDao;
	@Resource
	protected IScheduleDataDao scheduleDataDao;
	@Resource
	protected ITaskDAO taskDAO;
	@Resource
	protected IErrDataTrDao errDataTrDao;
	@Resource
	private ITempletSV templetSV;
	@Resource
	private IIndustrySV industrySV;
	@Resource
	private ICompBaseSV compBaseSV;
	@Resource
	private IAbnormalService abnormalSV;
	@Resource
	private ISysDicSV sysDicSV;
	@Resource
	private ICompFinanceSV compFinanceSV;
	@Resource
	private ICompFinancingSV compFinancingSV;
	@Resource
	private IFnTableService fnTableSV;
	@Resource
	private ICmisInInvokeSV cmisInInvokeSV;
	@Resource
	private IApplyInfoSV applyInfoSV;
	@Resource
	protected ITableSubjCodeSV tableSubjCodeSV;

	@Resource
	private ICusFSToolSV cusFSToolSV;
	@Resource
	private IFinanasisService finanasisSV;

	/* ReportData */

	@Override
	public ReportData getReportDataById(Long id) {
		return this.reportDataDao.getReportDataById(id);
	}

	@Override
	public ReportData saveReportData(ReportData ReportData) {
		return this.reportDataDao.saveReportData(ReportData);
	}

	@Override
	public ReportData onSaveReportData(ReportData ReportData) {
		return this.reportDataDao.saveReportData(ReportData);
	}

	@Override
	public List<HashMap<String, Double>> getTableMapList(Long taskId, String customerNo, String reportType) {

		List<HashMap<String, Double>> result = new ArrayList<HashMap<String, Double>>();
		ReportData balanceData = getReportDataByTaskIdCustNoAndType(taskId, customerNo, reportType);
		if (balanceData != null) {
			List<ReportDataTR> trs = getReportDataTrsByReportDataId(balanceData.getId());
			HashMap<String, Double> balance1 = MapUtil.getValueList(trs).get(0);

			HashMap<String, Double> balance2 = MapUtil.getValueList(trs).get(1);
			HashMap<String, Double> balance3 = MapUtil.getValueList(trs).get(2);
			HashMap<String, Double> balance4 = MapUtil.getValueList(trs).get(3);

			result.add(balance1);
			result.add(balance2);
			result.add(balance3);
			result.add(balance4);
		}

		return result;
	}

	@Override
	public void deleteReportData(ReportData ReportData) {
		List<ReportDataTR> trs = reportDataTRDao.getReportDataTrsByReportDataId(ReportData.getId());
		if (trs != null) {
			for (ReportDataTR reportDataTR : trs) {
				reportDataTRDao.deleteReportDataTr(reportDataTR);
			}
		}
		this.reportDataDao.daleteReportData(ReportData);
	}

	@Override
	public ReportData getReportDataByTaskIdCustNoAndType(Long taskId, String customerNo, String reportType) {
		return this.reportDataDao.getReportDataByTaskIdCustNoAndType(taskId, customerNo, reportType);
	}

	@Override
	public void saveThreeReportData(long taskId, String customerNo, int finaRepYear, int finaRepMonth)
			throws Exception {

		ReportData oldBalance = reportDataDao.getReportDataByTaskIdCustNoAndType(taskId, customerNo, "newBalance");

		if (oldBalance != null) {
			List<ReportDataTR> trs = reportDataTRDao.getReportDataTrsByReportDataId(oldBalance.getId());
			for (ReportDataTR reportDataTR : trs) {
				this.reportDataTRDao.deleteReportDataTr(reportDataTR);
			}
			this.reportDataDao.daleteReportData(oldBalance);
		}

		ReportData oldincome = reportDataDao.getReportDataByTaskIdCustNoAndType(taskId, customerNo, "newIncome");
		if (oldincome != null) {
			List<ReportDataTR> trs = reportDataTRDao.getReportDataTrsByReportDataId(oldincome.getId());
			for (ReportDataTR reportDataTR : trs) {
				this.reportDataTRDao.deleteReportDataTr(reportDataTR);
			}
			this.reportDataDao.daleteReportData(oldincome);
		}

		ReportData oldcashFlow = reportDataDao.getReportDataByTaskIdCustNoAndType(taskId, customerNo, "newCashFlow");
		if (oldincome != null) {
			List<ReportDataTR> trs = reportDataTRDao.getReportDataTrsByReportDataId(oldcashFlow.getId());
			for (ReportDataTR reportDataTR : trs) {
				this.reportDataTRDao.deleteReportDataTr(reportDataTR);
			}
			this.reportDataDao.daleteReportData(oldcashFlow);
		}

		List<String> balanceTop = MapUtil.getBalancetop();
		List<String> incomeTop = MapUtil.getIncometop();
		List<String> cashflowTop = MapUtil.getCashFlowtop();

		List<Map<String, Double>> balanceData = this.queryBalanceSheet(customerNo, finaRepYear, finaRepMonth);
		List<Map<String, Double>> incomeData = this.queryIncomeSheet(customerNo, finaRepYear, finaRepMonth);
		List<Map<String, Double>> cashFLowData = this.queryCashFlowSheet(customerNo, finaRepYear, finaRepMonth);

		ReportData balance = new ReportData(taskId, customerNo, "newBalance");
		ReportData income = new ReportData(taskId, customerNo, "newIncome");
		ReportData cashFlow = new ReportData(taskId, customerNo, "newCashFlow");

		balance = reportDataDao.saveReportData(balance);
		income = reportDataDao.saveReportData(income);
		cashFlow = reportDataDao.saveReportData(cashFlow);

		for (int i = 0; i < balanceTop.size(); i++) {
			String itemName = balanceTop.get(i);
			if (balanceData.get(3).containsKey(itemName)) {
				ReportDataTR tr = new ReportDataTR();
				tr.setName(itemName);
				tr.setReportDataId(balance.getId());
				tr.setFirstYearValue(balanceData.get(0).get(itemName));
				tr.setSecondYearValue(balanceData.get(1).get(itemName));
				tr.setThirdYearValue(balanceData.get(2).get(itemName));
				tr.setFourthYearValue(balanceData.get(3).get(itemName));
				tr.setLastYearTermValue(balanceData.get(4).get(itemName));
				tr.setIndex(i);
				this.reportDataTRDao.saveReportDataTr(tr);
			}
		}
		for (int i = 0; i < incomeTop.size(); i++) {
			String itemName = incomeTop.get(i);
			if (incomeData.get(3).containsKey(itemName)) {
				ReportDataTR tr = new ReportDataTR();
				tr.setName(itemName);
				tr.setReportDataId(income.getId());
				tr.setFirstYearValue(incomeData.get(0).get(itemName));
				tr.setSecondYearValue(incomeData.get(1).get(itemName));
				tr.setThirdYearValue(incomeData.get(2).get(itemName));
				tr.setFourthYearValue(incomeData.get(3).get(itemName));
				tr.setLastYearTermValue(incomeData.get(4).get(itemName));
				tr.setIndex(i);
				this.reportDataTRDao.saveReportDataTr(tr);
			}
		}
		for (int i = 0; i < cashflowTop.size(); i++) {
			String itemName = cashflowTop.get(i);
			if (cashFLowData.get(3).containsKey(itemName)) {
				ReportDataTR tr = new ReportDataTR();
				tr.setName(itemName);
				tr.setReportDataId(cashFlow.getId());
				tr.setFirstYearValue(cashFLowData.get(0).get(itemName));
				tr.setSecondYearValue(cashFLowData.get(1).get(itemName));
				tr.setThirdYearValue(cashFLowData.get(2).get(itemName));
				tr.setFourthYearValue(cashFLowData.get(3).get(itemName));
				tr.setLastYearTermValue(cashFLowData.get(4).get(itemName));
				tr.setIndex(i);
				this.reportDataTRDao.saveReportDataTr(tr);
			}
		}
	}

	/* ReportData end */

	/* ReportDataTR */
	@Override
	public ReportDataTR getReportDataTR(Long id) {
		return this.reportDataTRDao.getReportDataTrById(id);
	}

	@Override
	public void saveReportDataTR(ReportDataTR ReportDataTR) {
		this.reportDataTRDao.saveReportDataTr(ReportDataTR);
	}

	@Override
	public void onSaveReportDataTR(ReportDataTR ReportDataTR) {
		this.reportDataTRDao.saveReportDataTr(ReportDataTR);
	}

	@Override
	public void deleteReportDataTR(ReportDataTR ReportDataTR) {
		this.reportDataTRDao.deleteReportDataTr(ReportDataTR);
	}

	@Override
	public List<ReportDataTR> getReportDataTrsByReportDataId(Long reportDataId) {
		return this.reportDataTRDao.getReportDataTrsByReportDataId(reportDataId);
	}
	/* ReportDataTR end */

	/* ScheduleData */

	@Override
	public List<ScheduleData> getScheByTaskIdAndRepType(Long taskId, String repType) {
		return this.scheduleDataDao.getScheByTaskIdAndRepType(taskId, repType);
	}

	@Override
	public void saveScheduleData(List<ScheduleData> detailData, List<ScheduleData> oldDetailList) {
		if (oldDetailList != null) {
			for (ScheduleData oldData : oldDetailList) {
				this.scheduleDataDao.deleteScheduleData(oldData);
			}
			int i = 0;
			for (ScheduleData data : detailData) {
				data.setIndex(i);
				this.scheduleDataDao.saveScheduleData(data);
				i++;
			}
		} else {
			for (ScheduleData data : detailData) {
				this.scheduleDataDao.saveScheduleData(data);
			}
		}
	}

	public void deleteScheduleData(ScheduleData data) {
		ErrDataTr errDataTr = errDataTrDao.getErrDataTrById(data.getErrDataTrId());
		this.errDataTrDao.deleteErrDataTr(errDataTr);
		this.scheduleDataDao.deleteScheduleData(data);
	}

	/* ScheduleData end */

	/**
	 *从信贷查询（前三年加当期加去年同期）财报数据，并进行科目转换
	 * @param cusId 客户号
	 * @param year 财报年份
	 * @param month 财报月份
	 * @param reportType 财报类型
	 * @return 返回转换后的数据集合列表
	 * @throws Exception
	 */
	private List<Map<String, Double>> getReportData(String cusId, int year, int month, String reportType)
			throws Exception {

		List<Map<String, Double>> list = new ArrayList<Map<String, Double>>();

		List<String> dateList = SystemUtil.genFinaDateList(year, month, true, false);

		String modelClass = cusFSToolSV.getFinaRepModelClass(cusId);
		List<Map<String, Double>> reportDataList = cusFSToolSV.findRepDataByType(cusId, dateList, reportType);

		for (int i = 0; i < reportDataList.size(); i++) {
			// 取得一期报表数据
			Map<String, Double> reportData = reportDataList.get(i);
			list.add(this.convertSubjcet(modelClass, reportType, reportData));
		}

		// 如果只有4期数据（当期为12月时），则默认为第三年数据为上年同期
		if (reportDataList.size() == 4) {
			list.add(list.get(2));
		}
		return list;
	}

	@Override
	public List<Map<String, Double>> queryBalanceSheet(String cusId, int year, int month) throws Exception {
		return this.getReportData(cusId, year, month, SysConstants.CmisFianRepType.BALANCE);
	}

	@Override
	public List<Map<String, Double>> queryIncomeSheet(String cusId, int year, int month) throws Exception {
		return this.getReportData(cusId, year, month, SysConstants.CmisFianRepType.INCOME);
	}

	@Override
	public List<Map<String, Double>> queryCashFlowSheet(String cusId, int year, int month) throws Exception {

		List<Map<String, Double>> result = this.getReportData(cusId, year, month,
				SysConstants.CmisFianRepType.CASH_FLOW);

		for (Map<String, Double> dataMap : result) {
			double businCashTotal = dataMap.get("经营活动产生的现金流量净额");
			double invesCashTotal = dataMap.get("投资活动产生的现金流量净额");
			double raiseCashTotal = dataMap.get("筹资活动产生的现金流量净额");
			dataMap.put("1、经营活动现金净流量", businCashTotal);
			dataMap.put("2、投资活动现金净流量", invesCashTotal);
			dataMap.put("3、筹资活动现金净流量", raiseCashTotal);
			dataMap.put("合   计", businCashTotal + invesCashTotal + raiseCashTotal);
		}

		return result;
	}

	@Override
	public List<Map<String, Double>> queryFinanIndex(String cusId, int year, int month) throws Exception {
		return this.getReportData(cusId, year, month, SysConstants.CmisFianRepType.RATIO_INDEX);
	}

	@Override
	public List<Map<String, Double>> convertSubjcet(String modelClass, String reportType,
			List<Map<String, Double>> reportData) throws Exception {

		List<Map<String, Double>> resultList = null;

		if (reportData != null) {
			resultList = new ArrayList<Map<String, Double>>();
			for (int i = 0; i < reportData.size(); i++) {
				resultList.add(this.convertSubjcet(modelClass, reportType, reportData.get(i)));
			}
		}

		return resultList;
	}

	@Override
	public Map<String, Double> convertSubjcet(String modelClass, String reportType, Map<String, Double> reportData)
			throws Exception {

		Map<String, String> bsNumAndNameMap = FncStatMapUtil.getModelSubjectMap(modelClass, reportType);

		Map<String, Double> sheetMap = new HashMap<String, Double>();

		Iterator<String> it = bsNumAndNameMap.keySet().iterator();

		if (reportData != null && reportData.size() != 0) {

			while (it.hasNext()) {
				String subjectId = it.next();
				String localSubName = bsNumAndNameMap.get(subjectId);
				// 找不到对应科目
				if (StringUtil.isEmpty(localSubName))
					continue;

				Double subjectValue = reportData.get(subjectId);
				if (subjectValue == null)
					subjectValue = 0.0;

				sheetMap.put(localSubName, subjectValue);
			}

		} else {
			while (it.hasNext()) {
				sheetMap.put(bsNumAndNameMap.get(it.next()), 0.0);
			}
		}

		return sheetMap;
	}

	@Override
	public Map<String, Object> genAnalysisConclusion(long taskId, String customerNo) throws Exception {
		return genAnalysisConclusion(taskId, customerNo, SysConstants.SysFinaAnalyPart.ALL);
	}

	@Override
	public Map<String, Object> genAnalysisConclusion(long taskId, String customerNo, String whichPart)
			throws Exception {

		Map<String, Object> finaRepInfo = compFinanceSV.getFinaRepInfo(taskId, customerNo);

		int finaRepYear = (Integer) finaRepInfo.get("finaRepYear");
		int finaRepMonth = (Integer) finaRepInfo.get("finaRepMonth");
		String industry = (String) finaRepInfo.get("industry");
		String cusFlag = (String) finaRepInfo.get("cusFlag");
		List<Map<String, Double>> balanceData = this.queryBalanceSheet(customerNo, finaRepYear, finaRepMonth);
		List<Map<String, Double>> incomeData = this.queryIncomeSheet(customerNo, finaRepYear, finaRepMonth);
		List<Map<String, Double>> cashFlowData = this.queryCashFlowSheet(customerNo, finaRepYear, finaRepMonth);

		return this.genAnalysisConclusion(taskId, customerNo, finaRepYear, finaRepMonth, industry, cusFlag, balanceData,
				incomeData, cashFlowData, false, whichPart);
	}

	@Override
	public Map<String, Object> genAnalysisConclusion(long taskId, String customerNo, int finaRepYear, int finaRepMonth,
			String industry, String cusFlag, List<Map<String, Double>> balanceData,
			List<Map<String, Double>> incomeData, List<Map<String, Double>> cashFlowData, boolean needConvert,
			String whichPart) throws Exception {

		if (needConvert) {
			String modelClass = cusFSToolSV.getFinaRepModelClass(customerNo);
			// 系统分析需要将原始财报数据转为本地分析所需科目
			balanceData = this.convertSubjcet(modelClass, SysConstants.CmisFianRepType.BALANCE, balanceData);
			incomeData = this.convertSubjcet(modelClass, SysConstants.CmisFianRepType.INCOME, incomeData);
			cashFlowData = this.convertSubjcet(modelClass, SysConstants.CmisFianRepType.CASH_FLOW, cashFlowData);
		}

		Map<String, Object> dataMap = new HashMap<String, Object>();

		dataMap.put("year", finaRepYear + "");
		dataMap.put("firstYear", (finaRepYear - 3) + "");
		dataMap.put("secondYear", (finaRepYear - 2) + "");
		dataMap.put("thirdYear", (finaRepYear - 1) + "");
		dataMap.put("month", finaRepMonth + "");

		TreeData taskTreeData = industrySV.getTreeDataByEnname(industry, SysConstants.TreeDicConstant.GB_IC_4754_2017);
		Map<String, IndustryQRadio> quotaMap = this.industrySV.getQuotaByIndustry(taskTreeData);

		IndustryAnalysis taskIndestry = industrySV.getIndustryAnalysisByTaskTreeData(taskTreeData);

		Map<String, Double> oldbalance4 = (HashMap<String, Double>) balanceData.get(3);
		Map<String, Double> oldincome4 = (HashMap<String, Double>) incomeData.get(3);

		Map<String, Double> balance1 = null;
		Map<String, Double> balance2 = null;
		Map<String, Double> balance3 = null;
		Map<String, Double> balance4 = null;
		Map<String, Double> balance5 = null;
		Map<String, Double> income1 = null;
		Map<String, Double> income2 = null;
		Map<String, Double> income3 = null;
		Map<String, Double> income4 = null;
		Map<String, Double> income5 = null;
		Map<String, Double> cashFlow3 = null;
		Map<String, Double> cashFlow4 = null;

		try {
			// 借款人
			if (SysConstants.CusFlag.BORROWER.equals(cusFlag)) {

				ReportData newBalanceData = reportDataDao.getReportDataByTaskIdCustNoAndType(taskId, customerNo,
						SysConstants.ReportType.NEW_BALANCE);
				if (newBalanceData == null)
					ExceptionUtil.throwException(null, "NO_LOCAL_DATA");

				ReportData newIncomeData = reportDataDao.getReportDataByTaskIdCustNoAndType(taskId, customerNo,
						SysConstants.ReportType.NEW_INCOME);
				if (newIncomeData == null)
					ExceptionUtil.throwException(null, "NO_LOCAL_DATA");

				ReportData newCashFlowData = reportDataDao.getReportDataByTaskIdCustNoAndType(taskId, customerNo,
						SysConstants.ReportType.NEW_CASH_FLOW);
				if (newCashFlowData == null)
					ExceptionUtil.throwException(null, "NO_LOCAL_DATA");

				List<ReportDataTR> newBalanceTrs = reportDataTRDao
						.getReportDataTrsByReportDataId(newBalanceData.getId());
				List<ReportDataTR> newIncomeTrs = reportDataTRDao.getReportDataTrsByReportDataId(newIncomeData.getId());
				List<ReportDataTR> newCashFlowTrs = reportDataTRDao
						.getReportDataTrsByReportDataId(newCashFlowData.getId());

				balance1 = MapUtil.getValueList(newBalanceTrs).get(0);
				balance2 = MapUtil.getValueList(newBalanceTrs).get(1);
				balance3 = MapUtil.getValueList(newBalanceTrs).get(2);
				balance4 = MapUtil.getValueList(newBalanceTrs).get(3);
				balance5 = MapUtil.getValueList(newBalanceTrs).get(4);
				income1 = MapUtil.getValueList(newIncomeTrs).get(0);
				income2 = MapUtil.getValueList(newIncomeTrs).get(1);
				income3 = MapUtil.getValueList(newIncomeTrs).get(2);
				income4 = MapUtil.getValueList(newIncomeTrs).get(3);
				income5 = MapUtil.getValueList(newIncomeTrs).get(4);
				cashFlow3 = MapUtil.getValueList(newCashFlowTrs).get(2);
				cashFlow4 = MapUtil.getValueList(newCashFlowTrs).get(3);

			} else if (SysConstants.CusFlag.GUARA_COM.equals(cusFlag)) {

				ExceptionUtil.throwException(null, "NO_LOCAL_DATA");
			}

		} catch (BusinessException be) {

			if ("NO_LOCAL_DATA".equals(be.getFlag())) {
				balance1 = balanceData.get(0);
				balance2 = balanceData.get(1);
				balance3 = balanceData.get(2);
				balance4 = balanceData.get(3);
				balance5 = balanceData.get(4);
				income1 = incomeData.get(0);
				income2 = incomeData.get(1);
				income3 = incomeData.get(2);
				income4 = incomeData.get(3);
				income5 = incomeData.get(4);
				cashFlow3 = cashFlowData.get(2);
				cashFlow4 = cashFlowData.get(3);
			}
		}

		@SuppressWarnings("unchecked")
		GetErrTemp<ErrTemplet> getErrTemplet = (GetErrTemp<ErrTemplet>) templetSV;
		@SuppressWarnings("unchecked")
		GetErrExplainData<ErrExplainData> errExplainData = (GetErrExplainData<ErrExplainData>) abnormalSV;

		List<ErrData> taskErrData = abnormalSV.getErrorDataByTIdAndNo(taskId, customerNo);
		for (ErrData data : taskErrData) {
			String tableItem = data.getTableItem();
			if ("存货".equals(tableItem)) {
				dataMap.put("inventoryExplain", data.getExplain());
			} else if ("应收账款".equals(tableItem)) {
				dataMap.put("fnReceiveExplain", data.getExplain());
			} else if ("其他应收款".equals(tableItem)) {
				dataMap.put("fnOtherReceiveExplain", data.getExplain());
			} else if ("预付账款".equals(tableItem)) {
				dataMap.put("prePayExplain", data.getExplain());
			} else if ("固定资产".equals(tableItem)) {
				dataMap.put("fixedExplain", data.getExplain());
			} else if ("应付账款".equals(tableItem)) {
				dataMap.put("payExplain", data.getExplain());
			} else if ("其他应付款".equals(tableItem)) {
				dataMap.put("otherPayExplain", data.getExplain());
			} else if ("预收账款".equals(tableItem)) {
				dataMap.put("preReceiveExplain", data.getExplain());
			}
		}

		if (SysConstants.SysFinaAnalyPart.ALL.equals(whichPart)
				|| SysConstants.SysFinaAnalyPart.INCOME.equals(whichPart)) {
			/* 系统利润分析 */
			dataMap.put("Incfirst",
					FinalCommomResult.getFinalIncomeOneStringData(income1, income2, income3, income4, income5));
			dataMap.put("Incsecond", FinalCommomResult.getFinalIncomeAnalysis(getErrTemplet, finaRepYear, finaRepMonth,
					balance2, balance3, balance4, balance5, oldincome4, income2, income3, income4, income5, quotaMap));
			dataMap.put("IncsecondThree",
					FinalCommomResult.getFinalIncomeInTwoThreeData(finaRepYear, income3, income4, quotaMap));
			dataMap.put("IncThird", FinalCommomResult.getFinalIncomethreeData(finaRepYear, finaRepMonth, income2,
					income3, income4, income5, taskErrData, getErrTemplet, errExplainData));
			dataMap.put("IncFour", FinalCommomResult.getFinalIncomeFourData(finaRepYear, finaRepMonth, income2, income3,
					income4, income5, taskErrData, getErrTemplet, errExplainData));
			dataMap.put("IncFive", FinalCommomResult.getFinalIncomeFiveData(finaRepYear, finaRepMonth, balance3,
					income3, taskErrData, getErrTemplet, errExplainData));
			dataMap.put("IncSix", FinalCommomResult.getFinalIncomeSixData(finaRepYear, finaRepMonth, income4,
					taskErrData, getErrTemplet, errExplainData));
		}

		if (SysConstants.SysFinaAnalyPart.ALL.equals(whichPart)
				|| SysConstants.SysFinaAnalyPart.ASSETS.equals(whichPart)) {
			/* 系统资产质量分析 */
			dataMap.put("AssetOne", FinalCommomResult.getFinalAssetTableData(balance1, balance2, balance3, balance4,
					balance5, income1, income2, income3, income4, income5));
			dataMap.put("AssetTwo",
					FinalCommomResult.getFinalAssetProporHighData(finaRepYear, oldbalance4, balance3, balance4,
							balance5, income3, income4, income5, taskErrData, taskIndestry, getErrTemplet,
							errExplainData));
			dataMap.put("AssetThree", FinalCommomResult.getFinalNetAssetData(finaRepYear, balance3, balance4));
		}

		if (SysConstants.SysFinaAnalyPart.ALL.equals(whichPart)
				|| SysConstants.SysFinaAnalyPart.CASH_FLOW.equals(whichPart)) {

			ApplyInfo applyInfo = applyInfoSV.findApplyInfoByTaskId(taskId);
			double incomeGrowthRate = 0.0;
			double otherChannelCapital = 0.0;
			if (applyInfo != null) {
				incomeGrowthRate = applyInfo.getIncomeGrowthRate();
				otherChannelCapital = applyInfo.getOtherChannelCapital();
			}

			/* 系统现金流分析 */
			dataMap.put("first", FinalCommomResult.getFinalCashFlowFirstData(finaRepYear, income4, cashFlow4));
			dataMap.put("tableOne",
					FinalCommomResult.getFinalCashFlowSecondTableData(balance3, balance4, income4, cashFlow4).get(0));
			dataMap.put("tableTwo",
					FinalCommomResult.getFinalCashFlowSecondTableData(balance3, balance4, income4, cashFlow4).get(1));
			dataMap.put("third", FinalCommomResult.getFinalCashFlowThirdStringData(finaRepYear, balance3, balance4,
					income4, cashFlow3, cashFlow4));
			dataMap.put("four", FinalCommomResult.getFinalCashFlowFourStringData(finaRepYear, balance3, balance4,
					income4, cashFlow4));
			dataMap.put("five",
					FinalCommomResult.getFinalCashFlowFiveStringData(finaRepYear, balance4, cashFlow4, quotaMap));

			if (finaRepMonth == 12)
				dataMap.put("six", FinalCommomResult.getCashFlowSixTableData(balance3, balance4, balance4, income3,
						income4, income4, incomeGrowthRate, otherChannelCapital));
			else
				dataMap.put("six", FinalCommomResult.getCashFlowSixTableData(balance2, balance3, balance4, income2,
						income3, income4, incomeGrowthRate, otherChannelCapital));

			dataMap.put("seven",
					FinalCommomResult.getFinalCashFlowSevenStringData(balance3, balance4, income3, income4, cashFlow4));
		}

		if (SysConstants.SysFinaAnalyPart.ALL.equals(whichPart)
				|| SysConstants.SysFinaAnalyPart.RATIO_INDEX.equals(whichPart)) {
			/* 系统比率分析 */
			dataMap.put("RatioOne", FinalCommomResult.getFinalRatioOneData(finaRepYear, finaRepMonth,
					balanceData.get(2), balanceData.get(3), income3, income4, quotaMap, taskErrData));
			dataMap.put("RatioTwo", FinalCommomResult.getFinalRatioTwoData(finaRepYear, balanceData.get(2),
					balanceData.get(3), income3, balanceData.get(3), quotaMap, taskErrData));
			dataMap.put("RatioThree", FinalCommomResult.getFinalRatioThreeData(finaRepYear, finaRepMonth,
					balanceData.get(2), balanceData.get(3), income3, income4, quotaMap, taskErrData));
			dataMap.put("RatioFour", FinalCommomResult.getFinalRatioFourData(finaRepYear, finaRepMonth,
					balanceData.get(2), balanceData.get(3), income3, income4, quotaMap, taskErrData));
		}

		if (SysConstants.SysFinaAnalyPart.ALL.equals(whichPart)
				|| SysConstants.SysFinaAnalyPart.CONCLUSION.equals(whichPart)) {
			/* 系统分析结论 */
			dataMap.put("ConclusionOne", FinalCommomResult.getFinalConclusion(finaRepYear, finaRepMonth, balance2,
					balance3, balance4, balance5, income2, income3, income4, income5, cashFlow4));
			dataMap.put("ConclusionTwo", FinalCommomResult.getFinalConClusionTwo(finaRepYear, finaRepMonth, balance2,
					balance3, balance4, income3, income4, taskTreeData));
			dataMap.put("ConclusionThree",
					FinalCommomResult.getFinalConClusionThree(finaRepYear, balance2, balance3, balance4));
			dataMap.put("ConclusionFour",
					FinalCommomResult.getFinalConClusionFour(finaRepYear, finaRepMonth, balance3, balance4, quotaMap));
			dataMap.put("ConclusionInFourFive",
					CommonResult.getConClusionInFourFive(finaRepYear, income3, income4, cashFlow3, cashFlow4));
			dataMap.put("ConclusionFive", FinalCommomResult.getFinalConClusionFive(finaRepYear, balance3, balance4,
					income4, cashFlow3, cashFlow4));
		}

		return dataMap;
	}

	@Override
	public void saveFinaDataAndAnalyze(Task task, String validFlag) throws Exception {

		// 如果已经保存并分析了的，或者是完全不能满足分析要求的则不再继续
		if (!SysConstants.CusFinaValidFlag.TEMP_UNAVAIL.equals(task.getFinaSyncFlag()))
			return;

		if ("1".equals(validFlag))
			// 检查财报信息是否满足系统分析要求
			cusFSToolSV.checkCusInfoRule(task.getCustomerNo(), task.getYear(), task.getMonth());

		// 同步
		this.saveThreeReportData(task.getId(), task.getCustomerNo(), task.getYear(), task.getMonth());

		TreeData taskTreeData = industrySV.getTreeDataByEnname(task.getIndustry(),
				SysConstants.TreeDicConstant.GB_IC_4754_2017);
		Map<String, IndustryQRadio> quotaMap = this.industrySV.getQuotaByIndustry(taskTreeData);

		// 生成异常项
//		GetErrNameReason.getErrList(task, this, templetSV, abnormalSV, tableSubjCodeSV, quotaMap);
//		GetErrNameReason.getCheckErrList(task, this, templetSV, abnormalSV, quotaMap);

		task.setFinaSyncFlag(SysConstants.CusFinaValidFlag.AVAIL);

		taskDAO.saveTask(task);

		// 更新比率指标数据
		if (SysConstants.CusFinaValidFlag.AVAIL.equals(task.getFinaSyncFlag())) {
			// 获得系统分析结论
			Map<String, Object> conclusion = finanasisSV.genAnalysisConclusion(task.getId(), task.getCustomerNo(),
					SysConstants.SysFinaAnalyPart.RATIO_INDEX);
			String ratioAnalysis = "盈利能力：" + conclusion.get("RatioOne").toString() + "偿债能力："
					+ conclusion.get("RatioTwo").toString() + "运营能力：" + conclusion.get("RatioThree").toString()
					+ "增长能力：" + conclusion.get("RatioFour").toString();

			CompFinance finance = new CompFinance();
			finance.setTaskId(task.getId());
			finance.setCustomerNo(task.getCustomerNo());
			Map<String, String> map = new HashMap<String, String>();
			map.put("taskId", task.getId().toString());
			map.put("customerNo", task.getCustomerNo().toString());
			map.put("ratioAnalysis", ratioAnalysis);
			String jsondata = JSONObject.fromObject(map).toString();
			compFinanceSV.saveCompFinance(jsondata);
		}

	}

}
