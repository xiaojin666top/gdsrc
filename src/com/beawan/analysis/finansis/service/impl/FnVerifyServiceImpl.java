package com.beawan.analysis.finansis.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.analysis.finansis.dao.FnVerifyDao;
import com.beawan.analysis.finansis.bean.FnVerify;
import com.beawan.analysis.finansis.service.FnVerifyService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.analysis.finansis.bean.FnVerify;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yzj
 */
@Service("fnVerifyService")
public class FnVerifyServiceImpl extends BaseServiceImpl<FnVerify> implements FnVerifyService{
    /**
    * 注入DAO
    */
    @Resource(name = "fnVerifyDao")
    public void setDao(BaseDao<FnVerify> dao) {
        super.setDao(dao);
    }
    @Resource
    public FnVerifyDao fnVerifyDao;

    @Override
    public void saveVerifyList(List<FnVerify> list) {
        List<FnVerify> resultList = new ArrayList<>();
        Map<String, Object> params = new HashMap<>();
        for(FnVerify verify : list){
            params.put("taskId", verify.getTaskId());
            params.put("subject", verify.getSubject());
            List<FnVerify> fnVerifies = fnVerifyDao.selectByProperty(params);
            if(!CollectionUtils.isEmpty(fnVerifies)){
                FnVerify entity = fnVerifies.get(0);
                entity.setVerifyValue(verify.getVerifyValue());
                resultList.add(entity);
            }else{
                resultList.add(verify);
            }
        }
//        fnVerifyDao.s
        fnVerifyDao.batchSaveUpdata(resultList);
    }
}
