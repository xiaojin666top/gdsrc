package com.beawan.analysis.finansis.dto;

public class FinaRatioDto {


    private String name;
    private String firstYearValue;
    private String secondYearValue;
    private String thirdYearValue;  //当期
    private String ratioType;//在存放比率指标的时候使用   代表四力


    public FinaRatioDto() {
    }

    public FinaRatioDto(String name, String thirdYearValue) {
        this.name = name;
        this.thirdYearValue = thirdYearValue;
    }

    public FinaRatioDto(String name, String firstYearValue, String secondYearValue, String thirdYearValue, String ratioType) {
        this.name = name;
        this.firstYearValue = firstYearValue;
        this.secondYearValue = secondYearValue;
        this.thirdYearValue = thirdYearValue;
        this.ratioType = ratioType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstYearValue() {
        return firstYearValue;
    }

    public void setFirstYearValue(String firstYearValue) {
        this.firstYearValue = firstYearValue;
    }

    public String getSecondYearValue() {
        return secondYearValue;
    }

    public void setSecondYearValue(String secondYearValue) {
        this.secondYearValue = secondYearValue;
    }

    public String getThirdYearValue() {
        return thirdYearValue;
    }

    public void setThirdYearValue(String thirdYearValue) {
        this.thirdYearValue = thirdYearValue;
    }

    public String getRatioType() {
        return ratioType;
    }

    public void setRatioType(String ratioType) {
        this.ratioType = ratioType;
    }
}
