package com.beawan.analysis.finansis.dto;

/**
 * 财务分析通用行数据 传输对象
 */
public class FinaRowDto {

    private String name;
    private Double firstYearValue;
    private Double secondYearValue;
    private Double thirdYearValue;
    private Double fourthYearValue;
    private Double lastYearTermValue;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getFirstYearValue() {
        return firstYearValue;
    }

    public void setFirstYearValue(Double firstYearValue) {
        this.firstYearValue = firstYearValue;
    }

    public Double getSecondYearValue() {
        return secondYearValue;
    }

    public void setSecondYearValue(Double secondYearValue) {
        this.secondYearValue = secondYearValue;
    }

    public Double getThirdYearValue() {
        return thirdYearValue;
    }

    public void setThirdYearValue(Double thirdYearValue) {
        this.thirdYearValue = thirdYearValue;
    }

    public Double getFourthYearValue() {
        return fourthYearValue;
    }

    public void setFourthYearValue(Double fourthYearValue) {
        this.fourthYearValue = fourthYearValue;
    }

    public Double getLastYearTermValue() {
        return lastYearTermValue;
    }

    public void setLastYearTermValue(Double lastYearTermValue) {
        this.lastYearTermValue = lastYearTermValue;
    }
}
