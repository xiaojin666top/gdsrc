package com.beawan.analysis.salesIncome.service;
/**
 * @ClassName ISaleIncomeSV
 * @Description (收入流水分析逻辑层)
 * @author czc
 * @Date 2018年1月27日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */

import java.util.List;

import com.beawan.analysis.salesIncome.bean.SaleBankStatement;
import com.beawan.analysis.salesIncome.bean.SalePower;
import com.beawan.analysis.salesIncome.bean.SalePowerAnalysis;
import com.beawan.analysis.salesIncome.bean.SaleRevenue;
import com.beawan.analysis.salesIncome.bean.SaleSalary;

public interface ISaleIncomeSV {
	
	/**
	 * @Description (通过taskId查找银行流水)
	 * @param taskId 任务号
	 * @return List<SaleBankStatement>
	 */
	public List<SaleBankStatement> querySaleBankStates(Long taskId) throws Exception;
	
	
	
	
	/**
	 * @Description (通过taskId查找水电流水)
	 * @param taskId 任务号
	 * @return List<SalePower>
	 */
	public List<SalePower> querySalePowers(Long taskId) throws Exception;
	
	
	/**
	 * @Description (通过taskId查找水电流水分析)
	 * @param taskId 任务号
	 * @return List<SalePowerAnalysis>
	 */
	public SalePowerAnalysis querySalePowerAnalysis(Long taskId) throws Exception;
	
	
	/**
	 * @Description (通过taskId查找税收流水)
	 * @param taskId 任务号
	 * @return List<SaleRevenue>
	 */
	public List<SaleRevenue> querySaleRevenues(Long taskId) throws Exception;
	
	
	
	/**
	 * @Description (通过taskId查找工资流水)
	 * @param taskId 任务号
	 * @return List<SaleSalary>
	 */
	public List<SaleSalary> querySaleSalarys(Long taskId) throws Exception;
	
	
	
	
	/**
	 * @Description (保存银行流水)
	 * @param taskId 任务号
	 * @return 
	 */
	public void  saveSaleBankStatments(Long taskId,String jsonArray) throws Exception;
	
	
	/**
	 * @Description (保存水电流水)
	 * @param taskId 任务号
	 * @return 
	 */
	public void  savePowers(Long taskId,String jsonArray) throws Exception;
	
	/**
	 * @Description (保存水电流水分析)
	 * @param taskId 任务号
	 * @return 
	 */
	public void  saveSalePowerAnalysis(SalePowerAnalysis data) throws Exception;
	
	/**
	 * @Description (保存税收流水)
	 * @param taskId 任务号
	 * @return 
	 */
	public void  saveSaleRevenues(Long taskId,String jsonArray) throws Exception;
	
	/**
	 * @Description (保存工资流水)
	 * @param taskId 任务号
	 * @return 
	 */
	public void  saveSaleSalarys(Long taskId,String jsonArray) throws Exception;
	
	
	/**
	 * @Description (删除所有信息)
	 * @param taskId 任务号
	 * @return 
	 */
	public void  deleteAll(Long taskId) throws Exception;
}
