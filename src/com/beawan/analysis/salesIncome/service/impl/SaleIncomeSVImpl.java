package com.beawan.analysis.salesIncome.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.procedure.internal.Util.ResultClassesResolutionContext;
import org.springframework.stereotype.Service;

import com.beawan.analysis.salesIncome.bean.SaleBankStatement;
import com.beawan.analysis.salesIncome.bean.SalePower;
import com.beawan.analysis.salesIncome.bean.SalePowerAnalysis;
import com.beawan.analysis.salesIncome.bean.SaleRevenue;
import com.beawan.analysis.salesIncome.bean.SaleSalary;
import com.beawan.analysis.salesIncome.dao.ISaleBankStateDAO;
import com.beawan.analysis.salesIncome.dao.ISalePowerAnlyDAO;
import com.beawan.analysis.salesIncome.dao.ISalePowerDAO;
import com.beawan.analysis.salesIncome.dao.ISaleRevenueDAO;
import com.beawan.analysis.salesIncome.dao.ISaleSalaryDAO;
import com.beawan.analysis.salesIncome.service.ISaleIncomeSV;

import com.google.gson.JsonArray;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Service("saleIncomeSV")
public class SaleIncomeSVImpl implements ISaleIncomeSV{
 
	@Resource
	private ISaleBankStateDAO saleBankStateDAO;
	
	@Resource
	private ISalePowerDAO powerDAO;
	
	@Resource
	private ISalePowerAnlyDAO powerAnlyDAO;
	
	@Resource
	private ISaleRevenueDAO  revenueDAO;
	
	@Resource
	private ISaleSalaryDAO salaryDAO;

	
	@Override
	public List<SaleBankStatement> querySaleBankStates(Long taskId) throws Exception{
		
		return saleBankStateDAO.queryBankStateByTaskId(taskId);
	}

	@Override
	public List<SalePower> querySalePowers(Long taskId) throws Exception{
		
		return powerDAO.querySalePowersByTaskId(taskId);
	}

	@Override
	public SalePowerAnalysis querySalePowerAnalysis(Long taskId) throws Exception{
		SalePowerAnalysis salePowerAnalysis=null;
		List<SalePowerAnalysis> list=powerAnlyDAO.queryPowerAnalyByTaskId(taskId);
		if(list!=null&&list.size()>0) {
			salePowerAnalysis=list.get(0);
		}
		
		return salePowerAnalysis;
	}

	@Override
	public List<SaleRevenue> querySaleRevenues(Long taskId) throws Exception {
	
		return revenueDAO.querySaleRevenuesByTaskId(taskId);
	}

	@Override
	public List<SaleSalary> querySaleSalarys(Long taskId) throws Exception {
		
		return salaryDAO.querySaleSalarysByTaskId(taskId);
	}

	@Override
	public void saveSaleBankStatments(Long taskId, String jsonArray) throws Exception{
		
		List<SaleBankStatement> list=saleBankStateDAO.queryBankStateByTaskId(taskId);
		for(SaleBankStatement data:list) {
			saleBankStateDAO.deleteEntity(data);
		}
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				JSONObject datastr=JSONObject.fromObject(array.get(i));
				SaleBankStatement data=(SaleBankStatement)JSONObject.toBean(datastr, SaleBankStatement.class);
				saleBankStateDAO.saveOrUpdate(data);
				
			}
		}
		
	}

	@Override
	public void savePowers(Long taskId, String jsonArray) throws Exception {
		
		List<SalePower> list=powerDAO.querySalePowersByTaskId(taskId);
		for(SalePower data:list) {
			powerDAO.deleteEntity(data);
		}
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				JSONObject datastr=JSONObject.fromObject(array.get(i));
				SalePower data=(SalePower)JSONObject.toBean(datastr, SalePower.class);
				powerDAO.saveOrUpdate(data);
			}
		}
		
	}

	@Override
	public void saveSalePowerAnalysis(SalePowerAnalysis data) throws Exception {
		
				powerAnlyDAO.saveOrUpdate(data);
		
		
	}

	@Override
	public void saveSaleRevenues(Long taskId, String jsonArray) throws Exception {
		
		List<SaleRevenue> list=revenueDAO.querySaleRevenuesByTaskId(taskId);
		for(SaleRevenue data:list) {
			revenueDAO.deleteEntity(data);
		}
		
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				JSONObject datastr=JSONObject.fromObject(array.get(i));
				SaleRevenue data=(SaleRevenue)JSONObject.toBean(datastr, SaleRevenue.class);
				revenueDAO.saveOrUpdate(data);
			}
		}
		
	}

	@Override
	public void saveSaleSalarys(Long taskId, String jsonArray) throws Exception {

	List<SaleSalary> list=salaryDAO.querySaleSalarysByTaskId(taskId);
		for(SaleSalary data:list) {
			salaryDAO.deleteEntity(data);
		}
		JSONArray array=JSONArray.fromObject(jsonArray);
		if(array!=null&&array.size()>0) {
			for(int i=0;i<array.size();i++) {
				JSONObject datastr=JSONObject.fromObject(array.get(i));
				SaleSalary data=(SaleSalary)JSONObject.toBean(datastr, SaleSalary.class);
				salaryDAO.saveOrUpdate(data);
			
			}
		}
		
	}

	@Override
	public void deleteAll(Long taskId) throws Exception {
		
		List<SaleBankStatement> banks=this.querySaleBankStates(taskId);
		if(banks!=null) {
		for(SaleBankStatement bank:banks) {
			saleBankStateDAO.deleteEntity(bank);
		}
		}
		List<SalePower> powers=this.querySalePowers(taskId);
		if(powers!=null) {
		for(SalePower power:powers) {
			powerDAO.deleteEntity(power);
		}
		}
		
		SalePowerAnalysis analysis=this.querySalePowerAnalysis(taskId);
		if(analysis!=null) {
		powerAnlyDAO.deleteEntity(analysis);
		}
		
		List<SaleRevenue> revenues=this.querySaleRevenues(taskId);
		if(revenues!=null) {
		for(SaleRevenue revenue:revenues) {
			revenueDAO.deleteEntity(revenue);
		}
		}
		
		List<SaleSalary> salaries=this.querySaleSalarys(taskId);
		if(salaries!=null) {
		for(SaleSalary salary:salaries) {
			salaryDAO.deleteEntity(salary);
		}
		}
		
	}
	
	
}
