package com.beawan.analysis.salesIncome.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.analysis.salesIncome.bean.SaleBankStatement;
import com.beawan.analysis.salesIncome.dao.ISaleBankStateDAO;
import com.beawan.common.SysConstants.ExceptionConstant;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;

@Controller
@RequestMapping({ "/" })
public class saleIncomeCtl {
	
	@Resource
	private ISaleBankStateDAO saleBankStateDAO;
	
	/**
	 * @Description (保存企业描述信息)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("testSaveSales.do")
	@ResponseBody
	public String testSaveSales(HttpServletRequest request, HttpServletResponse response) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			SaleBankStatement state = new SaleBankStatement();
			state.setBank("MB");
			state.setAmount(123456.00);
			saleBankStateDAO.saveOrUpdate(state);
			json.put("result", true);
		} catch (DataAccessException e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.DATA_ACCESS_EXP);
			e.printStackTrace();
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", ExceptionConstant.UNDEFINED_EXP);
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
}
