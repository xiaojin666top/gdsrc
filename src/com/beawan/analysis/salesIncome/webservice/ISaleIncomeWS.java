package com.beawan.analysis.salesIncome.webservice;

import java.util.List;

import com.beawan.analysis.salesIncome.bean.SaleBankStatement;
import com.beawan.analysis.salesIncome.bean.SalePower;
import com.beawan.analysis.salesIncome.bean.SalePowerAnalysis;
import com.beawan.analysis.salesIncome.bean.SaleRevenue;
import com.beawan.analysis.salesIncome.bean.SaleSalary;

/**
 * @Description 销售收入流水信息webservice服务接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface ISaleIncomeWS {

	/**
	 * @Description (通过taskId查找银行流水)
	 * @param taskId 任务号
	 * @return 
	 */
	public String querySaleBankStates(Long taskId) ;
	
	
	
	
	/**
	 * @Description (通过taskId查找水电流水)
	 * @param taskId 任务号
	 * @return 
	 */
	public String querySalePowers(Long taskId) ;
	
	
	/**
	 * @Description (通过taskId查找水电流水分析)
	 * @param taskId 任务号
	 * @return 
	 */
	public String querySalePowerAnalysis(Long taskId);
	
	
	/**
	 * @Description (通过taskId查找税收流水)
	 * @param taskId 任务号
	 * @return 
	 */
	public String querySaleRevenues(Long taskId);
	
	
	
	/**
	 * @Description (通过taskId查找工资流水)
	 * @param taskId 任务号
	 * @return
	 */
	public String querySaleSalarys(Long taskId) ;
	
	
	
	
	/**
	 * @Description (保存银行流水)
	 * @param taskId 任务号
	 * @return 
	 */
	public String  saveSaleBankStatments(Long taskId,String jsonArray);
	
	
	/**
	 * @Description (保存水电流水)
	 * @param taskId 任务号
	 * @return 
	 */
	public String  savePowers(Long taskId,String jsonArray);
	
	/**
	 * @Description (保存水电流水分析)
	 * @param taskId 任务号
	 * @return 
	 */
	public String  saveSalePowerAnalysis(String data);
	
	/**
	 * @Description (保存税收流水)
	 * @param taskId 任务号
	 * @return 
	 */
	public String  saveSaleRevenues(Long taskId,String jsonArray);
	
	/**
	 * @Description (保存工资流水)
	 * @param taskId 任务号
	 * @return 
	 */
	public String  saveSaleSalarys(Long taskId,String jsonArray);
}
