package com.beawan.analysis.salesIncome.webservice.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.beawan.analysis.salesIncome.bean.SaleBankStatement;
import com.beawan.analysis.salesIncome.bean.SalePower;
import com.beawan.analysis.salesIncome.bean.SalePowerAnalysis;
import com.beawan.analysis.salesIncome.bean.SaleRevenue;
import com.beawan.analysis.salesIncome.bean.SaleSalary;
import com.beawan.analysis.salesIncome.service.ISaleIncomeSV;
import com.beawan.analysis.salesIncome.webservice.ISaleIncomeWS;
import com.beawan.common.util.WSResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.platform.util.JacksonUtil;

import net.sf.json.JSONArray;

@Service("saleIncomeWS")
public class SaleIncomeWSImpl implements ISaleIncomeWS{
	private static final Logger log = Logger.getLogger(ISaleIncomeWS.class);
	@Resource
	private ISaleIncomeSV saleIncomeSV;

	@Override
	public String querySaleBankStates(Long taskId) {
		WSResult result=new WSResult();
		try {
			List<SaleBankStatement> list=saleIncomeSV.querySaleBankStates(taskId);
			if(CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("查找银行流水异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String querySalePowers(Long taskId) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			List<SalePower> list=saleIncomeSV.querySalePowers(taskId);
			if(CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("查找水电流水异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String querySalePowerAnalysis(Long taskId) {
		// TODO Auto-generated method stub
		WSResult result=new WSResult();
		try {
			SalePowerAnalysis data=saleIncomeSV.querySalePowerAnalysis(taskId);
			if(data==null) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(data);
			}
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("查找水电流水分析异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String querySaleRevenues(Long taskId) {
		WSResult result=new WSResult();
		try {
			List<SaleRevenue> list=saleIncomeSV.querySaleRevenues(taskId);
			if(CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("查找税收流水异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String querySaleSalarys(Long taskId) {
		WSResult result=new WSResult();
		try {
			List<SaleSalary> list=saleIncomeSV.querySaleSalarys(taskId);
			if(CollectionUtils.isEmpty(list)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(list);
			}
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("查找税收流水异常！",e);
			e.printStackTrace();
		}
		return result.json();
	}

	@Override
	public String saveSaleBankStatments(Long taskId, String jsonArray) {
		WSResult result=new WSResult();
		try {
			saleIncomeSV.saveSaleBankStatments(taskId, jsonArray);
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("保存银行流水信息异常！",e);
			e.printStackTrace();
		}
		return  result.json();
	}

	@Override
	public String savePowers(Long taskId, String jsonArray) {
		WSResult result=new WSResult();
		try {
			saleIncomeSV.savePowers(taskId, jsonArray);
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("保存水电流水信息异常！",e);
			e.printStackTrace();
		}
		return  result.json();
	}

	@Override
	public String saveSalePowerAnalysis(String data) {
		WSResult result=new WSResult();
		ObjectMapper mapper = new ObjectMapper();
		SalePowerAnalysis sAnalysis=null;
		try {
			sAnalysis=mapper.readValue(data, SalePowerAnalysis.class);
			saleIncomeSV.saveSalePowerAnalysis(sAnalysis);
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("保存水电流水分析信息异常！",e);
			e.printStackTrace();
		}
		return  result.json();
	}

	@Override
	public String saveSaleRevenues(Long taskId, String jsonArray) {
		WSResult result=new WSResult();
		try {
			saleIncomeSV.saveSaleRevenues(taskId, jsonArray);
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("保存税收流水信息异常！",e);
			e.printStackTrace();
		}
		return  result.json();
	}

	@Override
	public String saveSaleSalarys(Long taskId, String jsonArray) {
		WSResult result=new WSResult();
		try {
			saleIncomeSV.saveSaleSalarys(taskId, jsonArray);
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("保存工资流水信息异常！",e);
			e.printStackTrace();
		}
		return  result.json();
	}

}
