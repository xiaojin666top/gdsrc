package com.beawan.analysis.salesIncome.bean;

/**
 * SalePower entity. @author MyEclipse Persistence Tools
 */

public class SalePower implements java.io.Serializable {

	// Fields

	private Long id;
	private String itemCode;//项目标识：01：用电量（度）、02：用水量（吨）
	private String month;//月份
	private Double itemValue;//数值
	private Long taskId;//任务号

	// Constructors

	/** default constructor */
	public SalePower() {
	}

	/** minimal constructor */
	public SalePower(Long id) {
		this.id = id;
	}

	/** full constructor */
	public SalePower(Long id, String itemCode, String month, Double itemValue,
			Long taskId) {
		this.id = id;
		this.itemCode = itemCode;
		this.month = month;
		this.itemValue = itemValue;
		this.taskId = taskId;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getItemCode() {
		return this.itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getMonth() {
		return this.month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public Double getItemValue() {
		return this.itemValue;
	}

	public void setItemValue(Double itemValue) {
		this.itemValue = itemValue;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

}