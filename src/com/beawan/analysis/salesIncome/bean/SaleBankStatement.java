package com.beawan.analysis.salesIncome.bean;

/**
 * SaleBankStatement entity. @author MyEclipse Persistence Tools
 */

public class SaleBankStatement implements java.io.Serializable {

	// Fields

	private Long id;
	private String month;//月份
	private String bank;//开户行，MB：本行
	private Double amount;//净现金流金额
	private Long taskId;//任务号

	// Constructors

	/** default constructor */
	public SaleBankStatement() {
	}

	/** minimal constructor */
	public SaleBankStatement(Long id) {
		this.id = id;
	}

	/** full constructor */
	public SaleBankStatement(Long id, String month, String bank, Double amount,
			Long taskId) {
		this.id = id;
		this.month = month;
		this.bank = bank;
		this.amount = amount;
		this.taskId = taskId;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMonth() {
		return this.month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getBank() {
		return this.bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

}