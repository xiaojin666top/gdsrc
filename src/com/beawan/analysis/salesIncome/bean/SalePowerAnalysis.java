package com.beawan.analysis.salesIncome.bean;

/**
 * SalePowerAnalysis entity. @author MyEclipse Persistence Tools
 */

public class SalePowerAnalysis implements java.io.Serializable {

	// Fields

	private Long id;
	private Double lastyearPowerEqui;//上年每吨用电折合产值
	private Double lastyearPowerUse;//上年度用电量
	private Double lastyearValue;//上年度产值
	private Double lastyearWaterEqui;//上年每吨用水折合产值
	private Double lastyearWaterUse;//上年度用水量
	private Double thisyearPowerEqui;//本年每吨用电折合产值
	private Double thisyearPowerUse;//本年度用电量
	private Double thisyearValue;//本年度产值
	private Double thisyearWaterEqui;//本年每吨用水折合产值
	private Double thisyearWaterUse;//本年度用水量
	private Long taskId;//任务号

	// Constructors

	/** default constructor */
	public SalePowerAnalysis() {
	}

	/** minimal constructor */
	public SalePowerAnalysis(Long id) {
		this.id = id;
	}

	/** full constructor */
	public SalePowerAnalysis(Long id, Double lastyearPowerEqui,
			Double lastyearPowerUse, Double lastyearValue,
			Double lastyearWaterEqui, Double lastyearWaterUse,
			Double thisyearPowerEqui, Double thisyearPowerUse,
			Double thisyearValue, Double thisyearWaterEqui,
			Double thisyearWaterUse, Long taskId) {
		this.id = id;
		this.lastyearPowerEqui = lastyearPowerEqui;
		this.lastyearPowerUse = lastyearPowerUse;
		this.lastyearValue = lastyearValue;
		this.lastyearWaterEqui = lastyearWaterEqui;
		this.lastyearWaterUse = lastyearWaterUse;
		this.thisyearPowerEqui = thisyearPowerEqui;
		this.thisyearPowerUse = thisyearPowerUse;
		this.thisyearValue = thisyearValue;
		this.thisyearWaterEqui = thisyearWaterEqui;
		this.thisyearWaterUse = thisyearWaterUse;
		this.taskId = taskId;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getLastyearPowerEqui() {
		return this.lastyearPowerEqui;
	}

	public void setLastyearPowerEqui(Double lastyearPowerEqui) {
		this.lastyearPowerEqui = lastyearPowerEqui;
	}

	public Double getLastyearPowerUse() {
		return this.lastyearPowerUse;
	}

	public void setLastyearPowerUse(Double lastyearPowerUse) {
		this.lastyearPowerUse = lastyearPowerUse;
	}

	public Double getLastyearValue() {
		return this.lastyearValue;
	}

	public void setLastyearValue(Double lastyearValue) {
		this.lastyearValue = lastyearValue;
	}

	public Double getLastyearWaterEqui() {
		return this.lastyearWaterEqui;
	}

	public void setLastyearWaterEqui(Double lastyearWaterEqui) {
		this.lastyearWaterEqui = lastyearWaterEqui;
	}

	public Double getLastyearWaterUse() {
		return this.lastyearWaterUse;
	}

	public void setLastyearWaterUse(Double lastyearWaterUse) {
		this.lastyearWaterUse = lastyearWaterUse;
	}

	public Double getThisyearPowerEqui() {
		return this.thisyearPowerEqui;
	}

	public void setThisyearPowerEqui(Double thisyearPowerEqui) {
		this.thisyearPowerEqui = thisyearPowerEqui;
	}

	public Double getThisyearPowerUse() {
		return this.thisyearPowerUse;
	}

	public void setThisyearPowerUse(Double thisyearPowerUse) {
		this.thisyearPowerUse = thisyearPowerUse;
	}

	public Double getThisyearValue() {
		return this.thisyearValue;
	}

	public void setThisyearValue(Double thisyearValue) {
		this.thisyearValue = thisyearValue;
	}

	public Double getThisyearWaterEqui() {
		return this.thisyearWaterEqui;
	}

	public void setThisyearWaterEqui(Double thisyearWaterEqui) {
		this.thisyearWaterEqui = thisyearWaterEqui;
	}

	public Double getThisyearWaterUse() {
		return this.thisyearWaterUse;
	}

	public void setThisyearWaterUse(Double thisyearWaterUse) {
		this.thisyearWaterUse = thisyearWaterUse;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

}