package com.beawan.analysis.salesIncome.bean;
public class SaleSalary implements java.io.Serializable {

	// Fields

	private Long id;
	private String month;//月份
	private Integer staffNum;//员工总数
	private Double grossPay;//工资总额
	private String payoffDate;//发放日期
	private String isArrear;//是否拖欠
	private Long taskId;//任务号

	// Constructors

	/** default constructor */
	public SaleSalary() {
	}

	/** minimal constructor */
	public SaleSalary(Long id) {
		this.id = id;
	}

	/** full constructor */
	public SaleSalary(Long id, String month, Integer staffNum, Double grossPay,
			String payoffDate, String isArrear, Long taskId) {
		this.id = id;
		this.month = month;
		this.staffNum = staffNum;
		this.grossPay = grossPay;
		this.payoffDate = payoffDate;
		this.isArrear = isArrear;
		this.taskId = taskId;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMonth() {
		return this.month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public Integer getStaffNum() {
		return this.staffNum;
	}

	public void setStaffNum(Integer staffNum) {
		this.staffNum = staffNum;
	}

	public Double getGrossPay() {
		return this.grossPay;
	}

	public void setGrossPay(Double grossPay) {
		this.grossPay = grossPay;
	}

	public String getPayoffDate() {
		return this.payoffDate;
	}

	public void setPayoffDate(String payoffDate) {
		this.payoffDate = payoffDate;
	}

	public String getIsArrear() {
		return this.isArrear;
	}

	public void setIsArrear(String isArrear) {
		this.isArrear = isArrear;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

}