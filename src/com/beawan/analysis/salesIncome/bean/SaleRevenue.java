package com.beawan.analysis.salesIncome.bean;

/**
 * SaleRevenue entity. @author MyEclipse Persistence Tools
 */

public class SaleRevenue implements java.io.Serializable {

	// Fields

	private Long id;
	private String month;//月份
	private String taxType;//税种，1：增值税或营业税，2：所得税
	private Double taxableAmt;//计税金额或销售收入
	private Double taxRate;//税率或单位税额
	private Double paidTaxAmt;//实缴税额
	private String remarks;//备注
	private Long taskId;//任务号

	// Constructors

	/** default constructor */
	public SaleRevenue() {
	}

	/** minimal constructor */
	public SaleRevenue(Long id) {
		this.id = id;
	}

	/** full constructor */
	public SaleRevenue(Long id, String month, String taxType,
			Double taxableAmt, Double taxRate, Double paidTaxAmt,
			String remarks, Long taskId) {
		this.id = id;
		this.month = month;
		this.taxType = taxType;
		this.taxableAmt = taxableAmt;
		this.taxRate = taxRate;
		this.paidTaxAmt = paidTaxAmt;
		this.remarks = remarks;
		this.taskId = taskId;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMonth() {
		return this.month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getTaxType() {
		return this.taxType;
	}

	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}

	public Double getTaxableAmt() {
		return this.taxableAmt;
	}

	public void setTaxableAmt(Double taxableAmt) {
		this.taxableAmt = taxableAmt;
	}

	public Double getTaxRate() {
		return this.taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public Double getPaidTaxAmt() {
		return this.paidTaxAmt;
	}

	public void setPaidTaxAmt(Double paidTaxAmt) {
		this.paidTaxAmt = paidTaxAmt;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getTaskId() {
		return this.taskId;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}

}