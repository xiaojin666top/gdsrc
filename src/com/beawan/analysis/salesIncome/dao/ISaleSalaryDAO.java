package com.beawan.analysis.salesIncome.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.analysis.salesIncome.bean.SaleSalary;
import com.beawan.common.dao.ICommDAO;
import com.beawan.survey.loanInfo.bean.ApplyInfo;

/**
 * @Description 工资流水持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface ISaleSalaryDAO extends ICommDAO<SaleSalary>{
	/**
	 * @Description (根据任务号查询工资信息)
	 * @param taskId  任务号
	 *            
	 * @return
	 */
	public List<SaleSalary> querySaleSalarysByTaskId(Long taskId) throws DataAccessException;
	
	

}
