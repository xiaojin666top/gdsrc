package com.beawan.analysis.salesIncome.dao.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.beawan.analysis.salesIncome.bean.SalePower;
import com.beawan.analysis.salesIncome.dao.ISalePowerDAO;
import com.beawan.common.dao.impl.CommDAOImpl;

@Service("salePowerDAO")
public class SalePowerDAOImpl extends CommDAOImpl<SalePower> implements ISalePowerDAO {

	@Override
	public List<SalePower> querySalePowersByTaskId(Long taskId) throws DataAccessException {
		String query = "from SalePower as model where model.taskId ='" + taskId + "'";
		return getEntityManager().createQuery(query).getResultList();
	}

}
