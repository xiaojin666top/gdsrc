package com.beawan.analysis.salesIncome.dao.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.beawan.analysis.salesIncome.bean.SaleBankStatement;
import com.beawan.analysis.salesIncome.dao.ISaleBankStateDAO;
import com.beawan.common.dao.impl.CommDAOImpl;

@Service("saleBankStateDAO")
public class SaleBankStateDAOImpl extends CommDAOImpl<SaleBankStatement> implements ISaleBankStateDAO{

	@Override
	public List<SaleBankStatement> queryBankStateByTaskId(Long taskId) throws DataAccessException {
		// TODO Auto-generated method stub
		String query = "from SaleBankStatement as model where model.taskId ='" + taskId + "'";
		return getEntityManager().createQuery(query).getResultList();
	}

}
