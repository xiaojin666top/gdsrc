package com.beawan.analysis.salesIncome.dao.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.beawan.analysis.salesIncome.bean.SalePowerAnalysis;
import com.beawan.analysis.salesIncome.dao.ISalePowerAnlyDAO;
import com.beawan.common.dao.impl.CommDAOImpl;

@Service("salePowerAnlyDAO")
public class SalePowerAnlyDAOImpl extends CommDAOImpl<SalePowerAnalysis> implements ISalePowerAnlyDAO{

	@Override
	public List<SalePowerAnalysis> queryPowerAnalyByTaskId(Long taskId) throws DataAccessException {
		String query = "from SalePowerAnalysis as model where model.taskId ='" + taskId + "' order by id desc";
		return getEntityManager().createQuery(query).getResultList();
	}

}
