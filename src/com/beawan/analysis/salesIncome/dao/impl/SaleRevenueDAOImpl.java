package com.beawan.analysis.salesIncome.dao.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.beawan.analysis.salesIncome.bean.SaleRevenue;
import com.beawan.analysis.salesIncome.dao.ISaleRevenueDAO;
import com.beawan.common.dao.impl.CommDAOImpl;

@Service("saleRevenueDAO")
public class SaleRevenueDAOImpl extends CommDAOImpl<SaleRevenue> implements ISaleRevenueDAO{

	@Override
	public List<SaleRevenue> querySaleRevenuesByTaskId(Long taskId) throws DataAccessException {
		// TODO Auto-generated method stub
		String query = "from SaleRevenue as model where model.taskId ='" + taskId + "'";
		return getEntityManager().createQuery(query).getResultList();
	}

}
