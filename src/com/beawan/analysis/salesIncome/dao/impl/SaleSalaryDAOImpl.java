package com.beawan.analysis.salesIncome.dao.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.beawan.analysis.salesIncome.bean.SaleSalary;
import com.beawan.analysis.salesIncome.dao.ISaleSalaryDAO;
import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.survey.loanInfo.bean.ApplyInfo;

@Repository("saleSalaryDAO")
public class SaleSalaryDAOImpl extends CommDAOImpl<SaleSalary> implements ISaleSalaryDAO{

	@Override
	public List<SaleSalary> querySaleSalarysByTaskId(Long taskId) throws DataAccessException {
		String query = "from SaleSalary as model where model.taskId ='" + taskId + "'";
		return getEntityManager().createQuery(query).getResultList();
	}
	


}
