package com.beawan.analysis.salesIncome.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.analysis.salesIncome.bean.SaleRevenue;
import com.beawan.common.dao.ICommDAO;
/**
 * @Description 税收流水持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface ISaleRevenueDAO extends ICommDAO<SaleRevenue>{
	/**
	 * @Description (根据任务号查询税收信息)
	 * @param taskId  任务号
	 *            
	 * @return
	 */
	public List<SaleRevenue> querySaleRevenuesByTaskId(Long taskId) throws DataAccessException;
}
