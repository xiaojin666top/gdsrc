package com.beawan.analysis.salesIncome.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.analysis.salesIncome.bean.SalePowerAnalysis;
import com.beawan.common.dao.ICommDAO;
/**
 * @Description 水电流水分析持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface ISalePowerAnlyDAO extends ICommDAO<SalePowerAnalysis>{
	/**
	 * @Description (根据任务号查询水电情况分析)
	 * @param taskId  任务号
	 *            
	 * @return
	 */
	public List<SalePowerAnalysis> queryPowerAnalyByTaskId(Long taskId) throws DataAccessException;
}
