package com.beawan.analysis.salesIncome.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.analysis.salesIncome.bean.SaleBankStatement;
import com.beawan.common.dao.ICommDAO;


/**
 * @Description 银行流水持久化接口
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface ISaleBankStateDAO extends ICommDAO<SaleBankStatement> {
	
	/**
	 * @Description (根据任务号查询银行流水)
	 * @param taskId  任务号
	 *            
	 * @return
	 */
	public List<SaleBankStatement> queryBankStateByTaskId(Long taskId) throws DataAccessException;
}
