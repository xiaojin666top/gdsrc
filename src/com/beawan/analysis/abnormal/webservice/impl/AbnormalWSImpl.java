package com.beawan.analysis.abnormal.webservice.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.beawan.analysis.abnormal.bean.ErrData;
import com.beawan.analysis.abnormal.bean.ErrDataTr;
import com.beawan.analysis.abnormal.bean.ErrExplainData;
import com.beawan.analysis.abnormal.dao.IErrDataDao;
import com.beawan.analysis.abnormal.dao.IErrDataTrDao;
import com.beawan.analysis.abnormal.dao.IErrExplainDataDao;
import com.beawan.analysis.abnormal.webservice.IAbnormalWS;
import com.beawan.analysis.finansis.bean.FnLongTermPay;
import com.beawan.analysis.finansis.bean.FnTableAnaly;
import com.beawan.analysis.finansis.dao.IFnTableDao;
import com.platform.util.JacksonUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
@Service("abnormalWS")
public class AbnormalWSImpl implements IAbnormalWS{
	
	private static final Logger log = Logger.getLogger(IAbnormalWS.class);

	private Map<String, Object> model = new HashMap<String, Object>();

	public Map<String, Object> getModel() {
		return model;
	}

	public void setModel(Map<String, Object> model) {
		this.model = model;
	}

	@Resource
	private IErrDataDao errDataDao;
	
	@Resource
	private IErrDataTrDao errdataTrDao;
	
	@Resource
	private  IErrExplainDataDao  explainDao;
	
	@Override
	public String getErrorDataById(Long id) {
		// TODO Auto-generated method stub
		String result = null;
		try {
			ErrData data = errDataDao.findErrdataById(id);
			if(data == null ){
				return result;
			}
			result = JacksonUtil.serialize(data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = "error";
			log.error("通过id获取异常失败！",e);
		}
		return result;
	}

	@Override
	public String saveErrorData(String errorData) {
		// TODO Auto-generated method stub
		String result = null;
		try {
			ErrData bean = JacksonUtil.fromJson(errorData, ErrData.class);
			errDataDao.savaErrData(bean);
			result = "success";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			result = "error";
			log.error("保存异常失败！", e);
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public String getErrorDataByTaskId(Long taskId) {
		// TODO Auto-generated method stub
		String result = null;
		try {
			List<ErrData> data = errDataDao.findErrdataByTaskId(taskId);
			if(data == null ){
				return result;
			}
			result = JacksonUtil.serialize(data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = "error";
			log.error("获取异常列表失败！",e);
		}
		return result;
	}

	@Override
	public String getErrDataTrById(Long id) {
		// TODO Auto-generated method stub
		String result = null;
		try {
			ErrDataTr data = errdataTrDao.getErrDataTrById(id);
			if(data == null ){
				return result;
			}
			result = JacksonUtil.serialize(data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = "error";
			log.error("通过id获取异常明细失败！",e);
		}
		return result;
	}

	@Override
	public String saveErrDataTr(String data)  {
		// TODO Auto-generated method stub
		String result = null;
		try {
			ErrDataTr bean = JacksonUtil.fromJson(data, ErrDataTr.class);
			errdataTrDao.saveErrDataTr(bean);
			result = "success";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			result = "error";
			log.error("保存异常明细失败！", e);
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public String findErrdataTrByErrDataId(Long errDataId) {
		// TODO Auto-generated method stub
		String result = null;
		try {
			List<ErrDataTr> data = errdataTrDao.findErrdataTrByErrDataId(errDataId);
			if(data == null ){
				return result;
			}
			result = JacksonUtil.serialize(data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = "error";
			log.error("通过异常id保存异常明细列表失败！",e);
		}
		return result;
	}

	@Override
	public String saveErrExplainDatas(Long errdataId,String ErrExplainDatas) {
		// TODO Auto-generated method stub
		String result = null;
		try {
			List<ErrExplainData>  lists = explainDao.getErrExplainDataByErrDataId(errdataId);
			if (lists!=null) {
			for (ErrExplainData errExplainData : lists) {
				explainDao.deleteErrExplainData(errExplainData);
			}
			}
			JSONArray array = JSONArray.fromObject(ErrExplainDatas);
			if (array != null && array.size() > 0) {
				for (int i = 0; i < array.size(); i++) {
					JSONObject dataStr = JSONObject.fromObject(array.get(i));
					ErrExplainData data= (ErrExplainData) JSONObject.toBean(dataStr,ErrExplainData.class);  
					explainDao.saveErrExplainData(data);
				}
			}
			result = "success";
		}catch (Exception e) {
			// TODO Auto-generated catch block
			result = "error";
			log.error("保存异常解释说明失败！",e);
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public String getErrExplainDataByName(String name) {
		// TODO Auto-generated method stub
		String result = null;
		try {
			ErrExplainData data = explainDao.getErrExplainDataByName(name);
			if(data == null ){
				return result;
			}
			result = JacksonUtil.serialize(data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = "error";
			log.error("通过名字获取异常解释说明失败！",e);
		}
		return result;
	}

	@Override
	public String getErrExplainDataById(Long id) {
		// TODO Auto-generated method stub
		String result = null;
		try {
			ErrExplainData data = explainDao.getErrExplainDataById(id);
			if(data == null ){
				return result;
			}
			result = JacksonUtil.serialize(data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = "error";
			log.error("通过id获取异常解释说明失败！",e);
		}
		return result;
	}

	@Override
	public String saveErrExplainData(String data)  {
		// TODO Auto-generated method stub
		String result = null;
		try {
			ErrExplainData bean = JacksonUtil.fromJson(data, ErrExplainData.class);
			explainDao.saveErrExplainData(bean);
			result = "success";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			result = "error";
			log.error("保存异常解释说明失败！", e);
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public String getErrExplainDataByErrDataId(Long errDataId) {
		// TODO Auto-generated method stub
		String result = null;
		try {
			List<ErrExplainData> data = explainDao.getErrExplainDataByErrDataId(errDataId);
			if(data == null ){
				return result;
			}
			result = JacksonUtil.serialize(data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = "error";
			log.error("通过异常解释说明列表失败！",e);
		}
		return result;
	}
	
	

}
