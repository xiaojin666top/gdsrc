package com.beawan.analysis.abnormal.webservice;

import java.util.List;

import com.beawan.analysis.abnormal.bean.ErrData;
import com.beawan.analysis.abnormal.bean.ErrDataTr;
import com.beawan.analysis.abnormal.bean.ErrExplainData;

public interface IAbnormalWS {
	
	/**
	 * @Description (通过Id查找异常数据)
	 * @param id
	 * @return
	 */
	public String getErrorDataById(Long id);

	/**
	 * @Description (保存异常)
	 * @param errorData
	 */
	public String saveErrorData(String errorData);
	
	/**
	 * @Description (通过客户号查找一个客户的所有异常)
	 * @param custNo
	 * @return
	 */
	public String getErrorDataByTaskId(Long taskId);
	
	
	/* ErrorData end */
	
	/* ErrDataTr */
	
	/**
	 * @Description (通过id查询ErrDataTr)
	 * @param name
	 * @return
	 */
	public String getErrDataTrById(Long id);
	
	
	/**
	 * @Description (保存ErrDataTr)
	 * @param data
	 * @return
	 */
	public String saveErrDataTr(String data) ;
	
	
	/**
	 * @Description (通过 errDataId 查找 与errdata关联的 tr )
	 * @param errDataId
	 * @return
	 */
	public String findErrdataTrByErrDataId(Long errDataId);
	
	
	/* saveErrDataTr end */

	/* Rejection end */

	/* ErrExplainData */
	
	/**
	 * @Description (循环保存ErrExplainDatas)
	 * @param ErrExplainDatas
	 */
	public String saveErrExplainDatas(Long errdataId,String ErrExplainDatas);
	
	/**
	 * @Description (通过名字查询ErrExplainData)
	 * @param name
	 * @return
	 */
	public String getErrExplainDataByName(String name);
	
	/**
	 * @Description (通过id查询ErrExplainData)
	 * @param id
	 * @return
	 */
	public String getErrExplainDataById(Long id);
	
	
	/**
	 * @Description (保存ErrExplainData)
	 * @param data
	 * @return
	 */
	public String saveErrExplainData(String data) ;
	
	
	/**
	 * @Description (得到与errdata关联的ErrExplainData)
	 * @param data
	 * @return
	 */
	public String getErrExplainDataByErrDataId(Long errDataId);

}
