package com.beawan.analysis.abnormal.bean;

import java.io.Serializable;

/**
 * @ClassName ErrCase
 * @Description TODO(异常项中系统提供的解释说明以及pad端选择的解释说明)
 * @author czc
 * @Date 2017年5月15日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public class ErrExplainData implements Serializable{

	private static final long serialVersionUID = -1985012812955667458L;

	private Long id;

	/** @Field @isRequired : TODO(名字) */
	private String name;

	/** @Field @isRequired : TODO(结论是否需要客户意见 0 不需要 1需要 2不要名字（净资产收益率）) */
	private Integer isRequired = 0;

	/** @Field @errKnoId : TODO(与errdata相关联的字段，存放异常的id) */
	private Long errDataId;

	/** @Field @custRejection : TODO(拒绝原因) */
	private String custRejection;

	/** @Field @seat : TODO(位置字段) */
	private Long seat;

	/** @Field @seat : TODO(备注) */
	private String remark;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public ErrExplainData() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	public Integer getIsRequired() {
		return isRequired;
	}

	public void setIsRequired(Integer isRequired) {
		this.isRequired = isRequired;
	}

	public String getCustRejection() {
		return custRejection;
	}

	public void setCustRejection(String custRejection) {
		this.custRejection = custRejection;
	}


	public Long getSeat() {
		return seat;
	}

	public void setSeat(Long seat) {
		this.seat = seat;
	}

	public Long getErrDataId() {
		return errDataId;
	}

	public void setErrDataId(Long errDataId) {
		this.errDataId = errDataId;
	}

}
