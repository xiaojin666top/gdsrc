package com.beawan.analysis.abnormal.bean;

import java.io.Serializable;

/**
 * @ClassName ErrData
 * @Description (异常项内容)
 * @author czc
 * @Date 2017年5月15日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public class ErrData implements Serializable {111111
	
	
	/** @Field @serialVersionUID : (这里用一句话描述这个字段的作用) */
	private static final long serialVersionUID = -3764977249682716382L;
	
	private Long id;
	
	
	/** @Field taskId : (任务号) */
	private Long taskId;
	
	private String customerNo;
	
	
	/** @Field @name : (异常名，与errKno里面的name相同) */
	private String name;
	
	/** @Field @errKnoId : (与模板errkno相关联的字段，存放模板异常的id) */
	private Long errTempletId;

	/** @Field @precent : (占比过高或者变化幅度过大百分比) */
	private String precent;

	/** @Field @categoryFlag : (区分占比异常还是 普通异常 必查项1  占比高2  变化幅度大3  异常4  行业对标5 常规调查6   调整过后的经营异常11，对标12) */
	private Integer categoryFlag;

	/** @Field @tableData : (异常表格数据) */
	private String tableData = null;

	/** @Field @errReason : (异常原因描述) */
	private String errReason;
	
	/** @Field @incOrBalan : (该异常是属于资产负债科目，还是属于损益表科目，资产负债表1，损益表2) */
	private Integer incOrBalan;

	/** @Field @explain : (解释说明) */
	private String explain;
	
	/** @Field @total : (所对应的报表数据总额，经营分析异常为null) */
	private String total;
	
	/** @Field @reason : (异常核查之后的原因) */
	private String reason;
	
	/** @Field @commonValue : (备用字段，可以存应收账款、存货计提政策、跌价损失) */
	private String commonValue;
	
	/** @Field @errState : (经营分析异常要区分，如存货是增加还是减少，增加1，减少2) */
	private Integer errState;
	
	/** @Field @needCheck : (是否需要核查，需要核查为1，通过之前的核查，无需核查为2) */
	private Integer needCheck = 1;

	/** @Field @adjustValue : (核查之后的调整值) */
	private String adjustValue;
	
	/** @Field @tableItem : (报表中的科目名称) */
	private String tableItem;
	
	/** @Field @beforeErrCase : (PAD端传来的数据) */
	private String beforeErrCase;
	
	/** @Field @beforeErrCase : (备注) */
	private String remark;

	public ErrData() {
	}

	public ErrData(String name, String errReason, String reason) {
		this.name = name;
		this.errReason = errReason;
		this.reason = reason;
	}

	public String getRemark() {
		return remark;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public Long getTaskId() {
		return taskId;
	}


	public String getTableItem() {
		return tableItem;
	}

	public void setTableItem(String tableItem) {
		this.tableItem = tableItem;
	}

	public void setTaskId(Long taskId) {
		this.taskId = taskId;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	public Long getErrTempletId() {
		return errTempletId;
	}

	public void setErrTempletId(Long errTempletId) {
		this.errTempletId = errTempletId;
	}

	public String getPrecent() {
		return precent;
	}


	public void setPrecent(String precent) {
		this.precent = precent;
	}


	public Integer getCategoryFlag() {
		return categoryFlag;
	}


	public void setCategoryFlag(Integer categoryFlag) {
		this.categoryFlag = categoryFlag;
	}


	public String getTableData() {
		return tableData;
	}


	public void setTableData(String tableData) {
		this.tableData = tableData;
	}


	public String getErrReason() {
		return errReason;
	}


	public void setErrReason(String errReason) {
		this.errReason = errReason;
	}


	public Integer getIncOrBalan() {
		return incOrBalan;
	}


	public void setIncOrBalan(Integer incOrBalan) {
		this.incOrBalan = incOrBalan;
	}


	public String getExplain() {
		return explain;
	}


	public void setExplain(String explain) {
		this.explain = explain;
	}


	public String getTotal() {
		return total;
	}


	public void setTotal(String total) {
		this.total = total;
	}


	public String getReason() {
		return reason;
	}


	public void setReason(String reason) {
		this.reason = reason;
	}


	public String getCommonValue() {
		return commonValue;
	}


	public void setCommonValue(String commonValue) {
		this.commonValue = commonValue;
	}


	public Integer getErrState() {
		return errState;
	}


	public void setErrState(Integer errState) {
		this.errState = errState;
	}


	public Integer getNeedCheck() {
		return needCheck;
	}


	public void setNeedCheck(Integer needCheck) {
		this.needCheck = needCheck;
	}


	public String getAdjustValue() {
		return adjustValue;
	}


	public void setAdjustValue(String adjustValue) {
		this.adjustValue = adjustValue;
	}


	public String getBeforeErrCase() {
		return beforeErrCase;
	}


	public void setBeforeErrCase(String beforeErrCase) {
		this.beforeErrCase = beforeErrCase;
	}
	


}
