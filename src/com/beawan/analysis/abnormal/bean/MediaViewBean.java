package com.beawan.analysis.abnormal.bean;

import java.io.Serializable;


/**
 * @ClassName MediaViewBean
 * @Description TODO(影像信息)
 * @author czc
 * @Date 2017年5月15日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public class MediaViewBean  implements Serializable {

	private static final long serialVersionUID = -4262137745821492012L;
	
	private Long id;
	
	/** @Field @name : TODO(名称) */
	private String name;
	
	/** @Field @errKnoId : TODO(与errdata相关联的字段，存放异常的id) */
	private Long errDataId;

	/** @Field @mediaName : TODO(图片名称) */
	private String mediaName;
	
	/** @Field @mediaType : TODO(影像类型（图片P、音频V、视频A) */
	private String mediaType;
	
	/** @Field @mediaPath : TODO(安卓路径) */
	private String mediaPath;  
	
	/** @Field @mediaDesc : TODO(图片描述) */
	private String mediaDesc;
	
	/** @Field @mediaPath : TODO(拍摄时间) */
	private String createTime;
	
	/** @Field @mediaPath : TODO(拍摄位置) */
	private String shottingLocation; 
	
	/** @Field @mediaPath : TODO(安卓路径) */
	private Double longitude;

	/** @Field @mediaPath : TODO(拍摄纬度) */
	private Double latitude;
	
	/** @Field @mediaPath : TODO(类别名称) */
	private String mediaGroup;
	
	/** @Field @mediaPath : TODO(分类名  1.2.3.4 ) */
	private String groupPath;  
	
	/** @Field @mediaPath : TODO(存储路径) */
	private String uploadPath; 
	
	/** @Field @beforeErrCase : TODO(备注) */
	private String remark;

	public MediaViewBean() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MediaViewBean(String mediaName, String mediaPath) {
		super();
		this.mediaName = mediaName;
		this.mediaPath = mediaPath;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public Long getErrDataId() {
		return errDataId;
	}

	public void setErrDataId(Long errDataId) {
		this.errDataId = errDataId;
	}

	public String getMediaName() {
		return mediaName;
	}

	public void setMediaName(String mediaName) {
		this.mediaName = mediaName;
	}

	public String getMediaType() {
		return mediaType;
	}

	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

	public String getMediaPath() {
		return mediaPath;
	}

	public void setMediaPath(String mediaPath) {
		this.mediaPath = mediaPath;
	}

	public String getMediaDesc() {
		return mediaDesc;
	}

	public void setMediaDesc(String mediaDesc) {
		this.mediaDesc = mediaDesc;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getShottingLocation() {
		return shottingLocation;
	}

	public void setShottingLocation(String shottingLocation) {
		this.shottingLocation = shottingLocation;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public String getMediaGroup() {
		return mediaGroup;
	}

	public void setMediaGroup(String mediaGroup) {
		this.mediaGroup = mediaGroup;
	}

	public String getGroupPath() {
		return groupPath;
	}

	public void setGroupPath(String groupPath) {
		this.groupPath = groupPath;
	}

	public String getUploadPath() {
		return uploadPath;
	}

	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}



}
