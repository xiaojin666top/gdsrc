package com.beawan.analysis.abnormal.bean;

import java.io.Serializable;


/**
 * @ClassName ErrDataTr
 * @Description TODO(异常项明细核查内容) 已弃用
 * @author czc
 * @Date 2017年5月15日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
@Deprecated
public class ErrDataTr implements Serializable {

	private static final long serialVersionUID = 8538910562587596469L;
	
	
	private Long id;
	
	/** @Field @errKnoId : (与errdata相关联的字段，存放异常的id) */
	private Long errDataId;
	
	/** @Field @name : (名字) */
	private String name;

	/** @Field @tableData : (表格数据) */
	private String tableData = null;

	/** @Field @urls : (图片的urls) */
	private String urls = null;
	
	/** @Field @customerNo : (备注) */
	private String remark;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getErrDataId() {
		return errDataId;
	}

	public void setErrDataId(Long errDataId) {
		this.errDataId = errDataId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getTableData() {
		return tableData;
	}

	public void setTableData(String tableData) {
		this.tableData = tableData;
	}

	public String getUrls() {
		return urls;
	}

	public void setUrls(String urls) {
		this.urls = urls;
	}
}
