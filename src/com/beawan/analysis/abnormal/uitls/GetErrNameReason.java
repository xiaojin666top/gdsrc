package com.beawan.analysis.abnormal.uitls;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.beawan.Interface.GetErrTemp;
import com.beawan.analysis.abnormal.bean.ErrData;
import com.beawan.analysis.abnormal.service.IAbnormalService;
import com.beawan.analysis.finansis.bean.ReportData;
import com.beawan.analysis.finansis.bean.ReportDataTR;
import com.beawan.analysis.finansis.service.IFinanasisService;
import com.beawan.analysis.finansis.utils.ControllerJSONUtil;
import com.beawan.analysis.otherAnalysis.IndustryCompareUtil;
import com.beawan.base.entity.TableSubjCode;
import com.beawan.base.service.ITableSubjCodeSV;
import com.beawan.check.AbnormalCheck;
import com.beawan.common.SysConstants;
import com.beawan.library.bean.ErrTemplet;
import com.beawan.library.bean.IndustryQRadio;
import com.beawan.library.bean.ReportTemplet;
import com.beawan.library.service.ITempletSV;
import com.beawan.task.bean.Task;
import com.beawan.utils.MapUtil;

public class GetErrNameReason {

	/**
	 * @Description (对得到的任务分析异常)
	 * @param task
	 * @param customerNo
	 * @param type
	 * @param finanasisSV
	 * @param templetService
	 * @param abnormalService
	 * @param tableSubjCodeSV
	 * @param quotaMap 行业比率数据
	 * @throws Exception
	 */
	public static void getErrList(Task task, IFinanasisService finanasisSV, ITempletSV templetService,
			IAbnormalService abnormalService, ITableSubjCodeSV tableSubjCodeSV, Map<String, IndustryQRadio> quotaMap)
			throws Exception {

		String balanceStr = SysConstants.ReportType.NEW_BALANCE;
		String incomeStr = SysConstants.ReportType.NEW_INCOME;
		
		String customerNo = task.getCustomerNo();
		
		ReportData newBalanceData = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(),
				customerNo, balanceStr);
		ReportData newIncomeData = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(),
				customerNo, incomeStr);

		List<ReportDataTR> newBalanceTrs = finanasisSV.getReportDataTrsByReportDataId(newBalanceData.getId());
		List<ReportDataTR> newIncomeTrs = finanasisSV.getReportDataTrsByReportDataId(newIncomeData.getId());

		HashMap<String, Double> balance3 = MapUtil.getValueList(newBalanceTrs).get(2);
		HashMap<String, Double> balance4 = MapUtil.getValueList(newBalanceTrs).get(3);
		HashMap<String, Double> balance5 = MapUtil.getValueList(newBalanceTrs).get(4);
		HashMap<String, Double> income2 = MapUtil.getValueList(newIncomeTrs).get(1);
		HashMap<String, Double> income3 = MapUtil.getValueList(newIncomeTrs).get(2);
		HashMap<String, Double> income4 = MapUtil.getValueList(newIncomeTrs).get(3);
		HashMap<String, Double> income5 = MapUtil.getValueList(newIncomeTrs).get(4);
		GetErrTemp<ErrTemplet> getErrTemplet = (GetErrTemp<ErrTemplet>) templetService;
		List<List<String>> lists1 = AbnormalCheck.getErrItemAndReason(task.getYear(), task.getMonth(), getErrTemplet,
				balance3, balance4,balance5,income2, income3, income4,income5);

		// ListMarge.getMargeErrItem(list3);
		List<String> errRunLists = lists1.get(2); // 经营异常
		List<String> errProHighLists = lists1.get(5); // 占比过高
		List<String> errChangeBigLists = lists1.get(8); // 变化过快
		List<String> errMustLists = lists1.get(11); // 必查项

		List<ErrTemplet> ErrTempletList = templetService.getAllErrTemplet();
		List<ErrTemplet> errMustResults = new ArrayList<ErrTemplet>(); // 必查项（销售收入）
		List<ErrTemplet> errProHighResults = new ArrayList<ErrTemplet>(); // 占比过高
		List<ErrTemplet> errChangeBigResults = new ArrayList<ErrTemplet>(); // 变化过快
		List<ErrTemplet> errRunResults = new ArrayList<ErrTemplet>(); // 经营异常

		for (int i = 0; i < errMustLists.size(); i++) {
			for (ErrTemplet err : ErrTempletList) {
				if (err.getName().equals(errMustLists.get(i))) {
					errMustResults.add(err);
					break;
				}
			}
		}

		for (int i = 0; i < errProHighLists.size(); i++) {
			for (ErrTemplet err : ErrTempletList) {
				if (err.getName().equals(errProHighLists.get(i))) {
					errProHighResults.add(err);
					break;
				}
			}
		}

		for (int i = 0; i < errChangeBigLists.size(); i++) {
			for (ErrTemplet err : ErrTempletList) {
				if (err.getName().equals(errChangeBigLists.get(i))) {
					errChangeBigResults.add(err);
					break;
				}
			}
		}

		for (int i = 0; i < errRunLists.size(); i++) {
			for (ErrTemplet err : ErrTempletList) {
				if (err.getRemark() != null && !err.getRemark().equals("")
						&& err.getRemark().equals(errRunLists.get(i))) {
					errRunResults.add(err);
					break;
				}
			}
		}

		List<ErrData> results2 = new ArrayList<ErrData>();

		int q = 0; // 必查项
		for (ErrTemplet mustErrTemplet : errMustResults) {
			ErrData errMust = new ErrData();
			errMust.setErrReason(lists1.get(10).get(q));
			errMust.setErrTempletId(mustErrTemplet.getId());
			if (mustErrTemplet.getName().equals("销售收入")) {
				errMust.setTotal(String.format("%.2f", income3.get("主营业务收入")) + "万元");
				errMust.setIncOrBalan(2);
			}
			errMust.setCategoryFlag(1);
			errMust.setTaskId(task.getId());
			errMust.setTableItem("主营业务收入");
			errMust.setName(mustErrTemplet.getName());
			errMust.setCustomerNo(customerNo);
			errMust = abnormalService.saveErrorData(errMust);
			results2.add(errMust);
			q++;
		}

		int j = 0; // 占比过高
		for (ErrTemplet proHighKno : errProHighResults) {
			ErrData errPro = new ErrData();
			errPro.setErrReason(lists1.get(4).get(j));
			errPro.setErrTempletId(proHighKno.getId());
			double precent = 0.0;
			if (proHighKno.getRemark().equals("固定资产")) {
				precent = balance4.get("固定资产合计") / balance4.get("资产总计");
				errPro.setTotal(String.format("%.2f", balance4.get("固定资产合计")) + "万元");
			} else {
				errPro.setTotal(String.format("%.2f", balance4.get(proHighKno.getRemark())) + "万元");
				precent = balance4.get(proHighKno.getRemark()) / balance4.get("资产总计");
			}
			errPro.setPrecent(String.format("%.2f%%", precent * 100));
			errPro.setIncOrBalan(1);
			errPro.setCategoryFlag(2);
			errPro.setCustomerNo(customerNo);
			errPro.setName(proHighKno.getRemark());
			errPro.setTableItem(proHighKno.getRemark());
			errPro.setTaskId(task.getId());
			errPro = abnormalService.saveErrorData(errPro);
			results2.add(errPro);
			j++;
		}

		int p = 0; // 变化幅度过大
		for (ErrTemplet changeBigKno : errChangeBigResults) {
			ErrData errChangeBig = new ErrData();
			errChangeBig.setErrReason(lists1.get(7).get(p));
			errChangeBig.setErrTempletId(changeBigKno.getId());
			double precent = 0.0;
			if (changeBigKno.getRemark().equals("固定资产")) {
				precent = (balance4.get("固定资产合计") - balance3.get("固定资产合计")) / balance3.get("固定资产合计");
				errChangeBig.setTotal(String.format("%.2f", balance3.get("固定资产合计")) + "万元");
			} else {
				precent = (balance4.get(changeBigKno.getRemark()) - balance3.get(changeBigKno.getRemark()))
						/ balance3.get(changeBigKno.getRemark());
				errChangeBig.setTotal(String.format("%.2f", balance3.get(changeBigKno.getRemark())) + "万元");
			}
			errChangeBig.setPrecent(String.format("%.2f%%", precent * 100));
			errChangeBig.setIncOrBalan(1);
			errChangeBig.setCustomerNo(customerNo);
			errChangeBig.setName(changeBigKno.getRemark());
			errChangeBig.setTableItem(changeBigKno.getRemark());
			errChangeBig.setCategoryFlag(3);
			errChangeBig.setTaskId(task.getId());
			errChangeBig = abnormalService.saveErrorData(errChangeBig);
			results2.add(errChangeBig);
			p++;
		}

		int i = 0; // 异常项
		for (ErrTemplet errRunKno : errRunResults) {
			ErrData err = new ErrData();
			err.setErrReason(lists1.get(1).get(i));
			err.setErrTempletId(errRunKno.getId());
			ReportTemplet reportT = null;
			String reportItem = "";
			if (errRunKno.getReportTempId() != null) {
				reportT = templetService.getReportTempletById(errRunKno.getReportTempId());
				reportItem = reportT.getItem();
			}
			if (reportItem.equals("所得税")) {
				i++;
				continue;
			}
			if (errRunKno.getRemark().equals("财务费用")
					|| errRunKno.getRemark().equals("营业费用") || errRunKno.getRemark().equals("主营业务成本")
					|| errRunKno.getRemark().equals("管理费用") || errRunKno.getRemark().equals("所得税")) {
				// err.setTotal(String.format("%.2f",
				// income3.get(errRunKno.getRemark())) + "万元");
				err.setIncOrBalan(2);
			} else {
				if (errRunKno.getName().equals(errRunKno.getRemark() + "与收入变动不成比例")) {
					if ((balance4.get(reportT.getItem()) - balance3.get(reportT.getItem())) > 0) {
						err.setErrState(2);
					} else {
						err.setErrState(1);
					}
				}
				// err.setTotal(String.format("%.2f",
				// balance3.get(errRunKno.getRemark())) + "万元");
				err.setIncOrBalan(1);
			}
			err.setTableItem(reportT.getItem());
			err.setCategoryFlag(4);
			err.setTaskId(task.getId());
			err.setCustomerNo(customerNo);
			err.setName(errRunKno.getName());
			err = abnormalService.saveErrorData(err);
			results2.add(err);
			i++;
		}

		// 行业对标
		int year = task.getYear();
		List<String> ratio_name = ControllerJSONUtil.getRatioCompareName();
		List<String> constants_name = ControllerJSONUtil.getRatioCompareConstants();
		List<String> compare_data2 = IndustryCompareUtil.getERRCompareResult(task.getMonth(), balance3, balance4,
				income4, quotaMap);
		
		
		List<String> ratio_data2 = ControllerJSONUtil.getIndustryRatioTheYearData(task.getMonth(), balance3, balance4,
				income4);
		for (int m = 0; m < ratio_name.size(); m++) {
			for (ErrTemplet errRatioKno : ErrTempletList) {
				String str1 = ratio_name.get(m); // 比率名字
				String str2 = compare_data2.get(m); // 比较结果
				IndustryQRadio roe = quotaMap.get(constants_name.get(m));
				double ratio = roe.getAverageValue();
				double fineRatio = roe.getExcellentValue();
				ErrData errdata = null;
				if (errRatioKno.getRemark() != null && errRatioKno.getName().equals("资产负债率")) { // 资产负债率反着来，越高越差
					if (str2.equals("极优秀") && errRatioKno.getRemark().equals(str1 + "在行业内过低")) {
						errdata = new ErrData();
						errdata.setErrTempletId(errRatioKno.getId());
						errdata.setErrReason((year) + "年，" + str1 + "为" + ratio_data2.get(m) + "，行业平均值为"
								+ String.format("%.2f", ratio) + "，行业优秀值为"+ String.format("%.2f", fineRatio)  +"，企业该指标过于优秀。");
					} else if ((str2.equals("差") || str2.equals("极差")) && errRatioKno.getRemark() != null
							&& errRatioKno.getRemark().equals(str1 + "在行业内过高")) {
						errdata = new ErrData();
						errdata.setErrTempletId(errRatioKno.getId());
						errdata.setErrReason((year) + "年，" + str1 + "为" + ratio_data2.get(m) + "，行业平均值为"
								+ String.format("%.2f", ratio) + "，企业该指标在行业内处于较低水平。");
					}
				}else if(str2.equals("极优秀") && errRatioKno.getRemark() != null && errRatioKno.getRemark().equals(str1 +"在行业内过高") && str1.equals("主营业务利润率") ){
					errdata = new ErrData();
					errdata.setErrTempletId(errRatioKno.getId());
					errdata.setErrReason((year) + "年，" + str1 + "为" + ratio_data2.get(i) + "，远高于行业平均值"
							+ String.format("%.2f", ratio) + "，请说明该企业核心竞争力。");
				}else {
					if (str2.equals("极优秀") && errRatioKno.getRemark() != null
							&& errRatioKno.getRemark().equals(str1 + "在行业内过高")) {
						errdata = new ErrData();
						errdata.setErrTempletId(errRatioKno.getId());
						errdata.setErrReason((year) + "年，" + str1 + "为" + ratio_data2.get(m) + "，行业平均值为"
								+ String.format("%.2f", ratio) +"，行业优秀值为"+ String.format("%.2f", fineRatio)  + "，企业该指标过于优秀。");
					} else if ((str2.equals("差") || str2.equals("极差")) && errRatioKno.getRemark() != null
							&& errRatioKno.getRemark().equals(str1 + "在行业内过低")) {
						errdata = new ErrData();
						errdata.setErrTempletId(errRatioKno.getId());
						errdata.setErrReason((year) + "年，" + str1 + "为" + ratio_data2.get(m) + "，行业平均值为"
								+ String.format("%.2f", ratio) + "，企业该指标在行业内处于较低水平。");
					}
				}
				if (errdata != null) {
					errdata.setIncOrBalan(1);
					errdata.setName(errRatioKno.getRemark());
					errdata.setCategoryFlag(5);
					errdata.setCustomerNo(customerNo);
					errdata.setPrecent(ratio_data2.get(m));
					errdata.setTaskId(task.getId());
					errdata = abnormalService.saveErrorData(errdata);
					results2.add(errdata);
					break;
				}
			}
		}

		/*
		 * 合并异常项
		 */
		for (int m = 0; m < results2.size() - 1; m++) {
			for (int n = m + 1; n < results2.size(); n++) {
				ErrData errdataOne = results2.get(m); // 第一个
				ErrData errdataTwo = results2.get(n); // 需要合并的
				String itemOne = errdataOne.getTableItem();
				String itemTwo = errdataTwo.getTableItem();
				if (itemOne != null && itemTwo != null && itemOne.equals(itemTwo)) {
					String str = errdataOne.getErrReason();
					str = str.replaceAll("。", "，");
					String[] strList = str.split("，");
					String addStr = errdataTwo.getErrReason();
					addStr = addStr.replaceAll("。", "，");
					String[] addStrList = addStr.split("，");
					if (errdataOne.getCategoryFlag() < errdataTwo.getCategoryFlag()) {
						errdataOne.setCategoryFlag(errdataTwo.getCategoryFlag());
					}
					for (int s1 = 0; s1 < strList.length; s1++) {
						String strTemp1 = strList[s1];
						for (int s2 = 0; s2 < addStrList.length; s2++) {
							String strTemp2 = addStrList[s2];
							if (strTemp1.equals(strTemp2)) {
								addStrList[s2] = "";
							}
						}
					}
					String reason = "";
					for (int s3 = 0; s3 < strList.length; s3++) {
						if (!strList[s3].equals("")) {
							reason += strList[s3] + "，";
						}
					}
					for (int s3 = 0; s3 < addStrList.length; s3++) {
						if (!addStrList[s3].equals("")) {
							reason += addStrList[s3] + "，";
						}
					}
					reason = reason.substring(0, reason.length() - 1);
					reason += "。";
					errdataOne.setErrReason(reason);
					errdataOne = abnormalService.saveErrorData(errdataOne);
					results2.remove(n);
					abnormalService.deleteErrorData(errdataTwo);
					n--;
				}
			}
		}

		/*
		 * 常规核查项目
		 */
		List<TableSubjCode> subjNameList = tableSubjCodeSV.queryByTableSubjCodes(task.getBusinessNo(), "ERR");
		List<String> haveItem = new ArrayList<String>();
		for (int m = 0; m < results2.size() - 1; m++) {
			ErrData errdataOne = results2.get(m); // 第一个
			if (errdataOne.getErrTempletId() != null) {
				ErrTemplet errOneKno = templetService.getErrTempletById(errdataOne.getErrTempletId());
				ReportTemplet oneReport = null;
				if (errOneKno.getReportTempId() != null) {
					oneReport = templetService.getReportTempletById(errOneKno.getReportTempId());
					haveItem.add(oneReport.getItem());
				}
			}
		}

		for (TableSubjCode tableSubjCode : subjNameList) {
			String tableItem = tableSubjCode.getSubjName();
			if (!haveItem.contains(tableItem)) {
				ErrData errdata = new ErrData();
				for (ErrTemplet errT : ErrTempletList) {
					ReportTemplet report = null;
					if (errT.getReportTempId() != null) {
						report = templetService.getReportTempletById(errT.getReportTempId());
						if (report.getItem().equals(tableItem)) {
							errdata.setErrTempletId(errT.getId());
							break;
						}
					}
				}
				
				errdata.setName(tableItem);
				errdata.setIncOrBalan(1);
				errdata.setCategoryFlag(6);
				errdata.setTableItem(tableItem);
				errdata.setTaskId(task.getId());
				
				if("固定资产".equals(tableItem))
					tableItem = "固定资产合计";
				errdata.setTotal(String.format("%.2f", balance4.get(tableItem)) + "万元");
				errdata.setPrecent(String.format("%.2f%%", balance4.get(tableItem) / balance4.get("资产总计") * 100));
				
				errdata = abnormalService.saveErrorData(errdata);
			}
		}
		
	}

	/**
	 * @Description (对进行核查完后的异常项进行重新分析)
	 * @param task
	 * @param finanasisSV
	 * @param templetService
	 * @param abnormalService
	 * @param quotaMap
	 * @throws Exception
	 */
	public static void getCheckErrList(Task task, IFinanasisService finanasisSV, ITempletSV templetService,
			IAbnormalService abnormalService, Map<String, IndustryQRadio> quotaMap) throws Exception {
		GetErrTemp<ErrTemplet> getErrTemplet = (GetErrTemp<ErrTemplet>) templetService;

		String balanceStr = SysConstants.ReportType.NEW_BALANCE;
		String incomeStr = SysConstants.ReportType.NEW_INCOME;
		
		String customerNo = task.getCustomerNo();
		
		ReportData newBalanceData = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(),
				customerNo, balanceStr);
		ReportData newIncomeData = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(),
				customerNo, incomeStr);

		List<ReportDataTR> newBalanceTrs = finanasisSV.getReportDataTrsByReportDataId(newBalanceData.getId());
		List<ReportDataTR> newIncomeTrs = finanasisSV.getReportDataTrsByReportDataId(newIncomeData.getId());

		HashMap<String, Double> balance3 = MapUtil.getValueList(newBalanceTrs).get(2);
		HashMap<String, Double> balance4 = MapUtil.getValueList(newBalanceTrs).get(3);
		HashMap<String, Double> balance5 = MapUtil.getValueList(newBalanceTrs).get(4);
		HashMap<String, Double> income2 = MapUtil.getValueList(newIncomeTrs).get(1);
		HashMap<String, Double> income3 = MapUtil.getValueList(newIncomeTrs).get(2);
		HashMap<String, Double> income4 = MapUtil.getValueList(newIncomeTrs).get(3);
		HashMap<String, Double> income5 = MapUtil.getValueList(newIncomeTrs).get(4);
		
		List<ErrTemplet> ErrTempletList = templetService.getAllErrTemplet();
		List<ErrTemplet> ErrTempletResults = new ArrayList<ErrTemplet>();

		List<List<String>> lists1 = AbnormalCheck.getCheckErrItem(task.getYear(), task.getMonth(), getErrTemplet,
				balance3, balance4,balance5,income2, income3, income4,income5);

		List<String> errRunLists = lists1.get(2); // 异常

		for (int i = 0; i < errRunLists.size(); i++) {
			for (ErrTemplet err : ErrTempletList) {
				if (err.getRemark() != null && !err.getRemark().equals("")
						&& err.getRemark().equals(errRunLists.get(i))) {
					ErrTempletResults.add(err);
					break;
				}
			}
		}

		List<ErrData> errs = abnormalService.getErrorDataByTIdAndNo(Long.valueOf(task.getId()), customerNo);

		for (int i = 0; i < errs.size(); i++) {
			ErrData tempErr = errs.get(i);
			if (tempErr.getCategoryFlag() == 11 || tempErr.getCategoryFlag() == 12) {
				abnormalService.deleteErrorData(tempErr);
			}
		}

		int i = 0; // 异常项
		for (ErrTemplet runErrTemplet : ErrTempletResults) {
			ErrData err = new ErrData();
			err.setErrReason(lists1.get(1).get(i));
			err.setErrTempletId(runErrTemplet.getId());
			if (runErrTemplet.getRemark().equals("财务费用") || runErrTemplet.getRemark().equals("所得税")
					|| runErrTemplet.getRemark().equals("营业费用") || runErrTemplet.getRemark().equals("主营业务成本")
					|| runErrTemplet.getRemark().equals("管理费用") || runErrTemplet.getRemark().equals("所得税")) {
				err.setTotal(String.format("%.2f", income3.get(runErrTemplet.getRemark())) + "万元");
				err.setIncOrBalan(2);
			} else {
				if (runErrTemplet.getName().equals(runErrTemplet.getRemark() + "与收入变动不成比例")) {
					if ((balance4.get(runErrTemplet.getRemark()) - balance3.get(runErrTemplet.getRemark())) > 0) {
						err.setErrState(2);
					} else {
						err.setErrState(1);
					}
				}
				err.setTotal(String.format("%.2f", balance4.get(runErrTemplet.getRemark())) + "万元");
				err.setIncOrBalan(1);
			}
			err.setName(runErrTemplet.getName());
			err.setCategoryFlag(11);
			err.setCustomerNo(customerNo);
			err.setTaskId(task.getId());
			abnormalService.saveErrorData(err);
			i++;
		}

		setErrRatio(task,customerNo, null, ErrTempletList, abnormalService, finanasisSV, quotaMap);

	}

	/**
	 * @Description (对更新后的报表进行 比率趋势 比率行业对比 重新分析)
	 * @param task
	 *            任务
	 * @param ErrTempletList
	 *            异常模板
	 * @param knowledgeService
	 * @param quotaMap
	 *            行业比率值
	 */
	public static void setErrRatio(Task task,String customerNo,String type, List<ErrTemplet> ErrTempletList, IAbnormalService abnormalService,
			IFinanasisService finanasisSV, Map<String, IndustryQRadio> quotaMap) {
		// 比率高或者低
		int year = task.getYear();
		String balanceStr = SysConstants.ReportType.NEW_BALANCE;
		String incomeStr = SysConstants.ReportType.NEW_INCOME;
		/*if(type.equals("1")){
			 balanceStr =SysConstants.ReportType.NEW_PARENT_BALANCE;
			 incomeStr = SysConstants.ReportType.NEW_PARENT_INCOME;
		}*/
		ReportData newBalance = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(), customerNo,
				balanceStr);
		List<ReportDataTR> balanceTr = finanasisSV.getReportDataTrsByReportDataId(newBalance.getId());
		ReportData newIncome = finanasisSV.getReportDataByTaskIdCustNoAndType(task.getId(), customerNo,
				incomeStr);
		List<ReportDataTR> incomeTr = finanasisSV.getReportDataTrsByReportDataId(newIncome.getId());

		HashMap<String, Double> newbalance2 = MapUtil.getValueList(balanceTr).get(1);
		HashMap<String, Double> newbalance3 = MapUtil.getValueList(balanceTr).get(2);
		HashMap<String, Double> newbalance4 = MapUtil.getValueList(balanceTr).get(3);
		HashMap<String, Double> newincome3 = MapUtil.getValueList(incomeTr).get(2);
		HashMap<String, Double> newincome4 = MapUtil.getValueList(incomeTr).get(3);

		// 找出与上年相比变化幅度大的比率
		Map<String, List<String>> map = ControllerJSONUtil.getMainRatioBigChangeList(task.getMonth(), newbalance2,
				newbalance3, newbalance4, newincome3, newincome4);
		List<String> nameList = map.get("name");
		List<String> valueList = map.get("value");
		List<String> changeValueList = map.get("changeValue");
		List<String> changeRatioList = map.get("changeRatio");
		for (int i = 0; i < nameList.size(); i++) {
			for (ErrTemplet err : ErrTempletList) {
				String name = nameList.get(i);
				String value = valueList.get(i);
				String changeValue = changeValueList.get(i);
				String changeRatio = changeRatioList.get(i);
				if (changeRatio.contains("%")) {
					changeRatio = changeRatio.substring(0, changeRatio.lastIndexOf("%"));
				}
				if (err.getRemark() != null && err.getRemark().equals(name + "相比上年变化幅度较大")) {
					ErrData errdata = new ErrData();
					errdata.setErrTempletId(err.getId());
					errdata.setIncOrBalan(1);
					errdata.setCategoryFlag(12);
					errdata.setPrecent(value);
					String reason = (year) + "年，" + name + "为" + value + "，相比上年";
					if (Double.valueOf(changeValue) > 0) {
						reason += "增加" + changeValue;
					} else if (Double.valueOf(changeValue) < 0) {
						reason += "减少" + (-Double.valueOf(changeValue));
					}
					if (Double.valueOf(changeRatio) > 0) {
						reason += "，增幅" + changeRatio + "%";
					} else if (Double.valueOf(changeRatio) < 0) {
						reason += "，降幅" + (-Double.valueOf(changeRatio)) + "%";
					}
					reason += ("。");

					errdata.setErrReason(reason);
					errdata.setCustomerNo(customerNo);
					errdata.setTaskId(task.getId());
					abnormalService.saveErrorData(errdata);
					break;
				}
			}
		}

		// 找出与行业相比过高或者过低的比率
		List<String> ratio_name = ControllerJSONUtil.getRatioCompareName();
		List<String> constants_name = ControllerJSONUtil.getRatioCompareConstants();
		List<String> compare_data2 = IndustryCompareUtil.getERRCompareResult(task.getMonth(), newbalance3, newbalance4,
				newincome4, quotaMap);
		List<String> ratio_data2 = ControllerJSONUtil.getIndustryRatioTheYearData(task.getMonth(), newbalance3,
				newbalance4, newincome4);
		for (int i = 0; i < ratio_name.size(); i++) {
			for (ErrTemplet err : ErrTempletList) {
				String str1 = ratio_name.get(i); // 比率名字
				String str2 = compare_data2.get(i); // 比较结果
				IndustryQRadio roe = quotaMap.get(constants_name.get(i));
				double ratio = roe.getAverageValue();
				ErrData errdata = null;
				if (err.getRemark() != null && err.getName().equals("资产负债率")) { // 资产负债率反着来，越高越差
					if (str2.equals("极优秀") && err.getRemark().equals(str1 + "在行业内过低")) {
						errdata = new ErrData();
						errdata.setErrTempletId(err.getId());
						errdata.setErrReason((year) + "年，" + str1 + "为" + ratio_data2.get(i) + "，行业平均值为"
								+ String.format("%.2f", ratio) + "，企业该指标过于优秀。");
					} else if ((str2.equals("差") || str2.equals("极差")) && err.getRemark() != null
							&& err.getRemark().equals(str1 + "在行业内过高")) {
						errdata = new ErrData();
						errdata.setErrTempletId(err.getId());
						errdata.setErrReason((year) + "年，" + str1 + "为" + ratio_data2.get(i) + "，行业平均值为"
								+ String.format("%.2f", ratio) + "，企业该指标在行业内处于较低水平。");
					}
				} else if(str2.equals("极优秀") && err.getRemark() != null && err.getRemark().equals(str1 +"在行业内过高") && str1.equals("主营业务利润率") ){
					errdata = new ErrData();
					errdata.setErrTempletId(err.getId());
					errdata.setErrReason((year) + "年，" + str1 + "为" + ratio_data2.get(i) + "，远高于行业平均值"
							+ String.format("%.2f", ratio) + "，请说明该企业核心竞争力。");
				}else {
					if (str2.equals("极优秀") && err.getRemark() != null && err.getRemark().equals(str1 + "在行业内过高")) {
						errdata = new ErrData();
						errdata.setErrTempletId(err.getId());
						errdata.setErrReason((year) + "年，" + str1 + "为" + ratio_data2.get(i) + "，行业平均值为"
								+ String.format("%.2f", ratio) + "，企业该指标过于优秀。");
					} else if ((str2.equals("差") || str2.equals("极差")) && err.getRemark() != null
							&& err.getRemark().equals(str1 + "在行业内过低")) {
						errdata = new ErrData();
						errdata.setErrTempletId(err.getId());
						errdata.setErrReason((year) + "年，" + str1 + "为" + ratio_data2.get(i) + "，行业平均值为"
								+ String.format("%.2f", ratio) + "，企业该指标在行业内处于较低水平。");
					}
				}
				if (errdata != null) {
					errdata.setName(err.getRemark());
					errdata.setCustomerNo(customerNo);
					errdata.setIncOrBalan(1);
					errdata.setCategoryFlag(12);
					errdata.setPrecent(ratio_data2.get(i));
					errdata.setTaskId(task.getId());
					abnormalService.saveErrorData(errdata);
					break;
				}
			}
		}
	}
}
