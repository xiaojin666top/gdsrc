package com.beawan.analysis.abnormal.uitls;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

public class GetZipReadPhoto {
	
	public static void getZip(HttpServletRequest request,MultipartFile multipartFile,String photopath){
		try {
			InputStream is = multipartFile.getInputStream();
			byte[] b = new byte[is.available()];
			String path = request.getServletContext().getRealPath("/").replace("\\", "/");        //复制zip
			FileOutputStream fos = new FileOutputStream(
					new File(path + "/uploadphoto/" + multipartFile.getOriginalFilename()));
			int len = 0;
			while ((len = is.read(b)) != -1) {
				fos.write(b, 0, len);
			}
			is.close();
			fos.close();

			ZipInputStream Zin = new ZipInputStream(
					new FileInputStream(path + "/uploadphoto/" + multipartFile.getOriginalFilename())); // 输入源zip路径
			BufferedInputStream Bin = new BufferedInputStream(Zin);
			String Parent = photopath; // 将图片复制到文件夹中
			File dir = new File(Parent);
			if (!dir.exists() && !dir.isDirectory()) {
				dir.mkdir();
			}
			File Fout = null;
			ZipEntry entry;
			while ((entry = Zin.getNextEntry()) != null && !entry.isDirectory()) {
				Fout = new File(Parent, entry.getName());
				if (!Fout.exists()) {
					(new File(Fout.getParent())).mkdirs();
				}
				FileOutputStream out = new FileOutputStream(Fout);
				BufferedOutputStream Bout = new BufferedOutputStream(out);
				int n;
				while ((n = Bin.read()) != -1) {
					Bout.write(n);
				}
				Bout.close();
				out.close();
			}
			Bin.close();
			Zin.close();

			File zipfile = new File(path + "/uploadphoto/" + multipartFile.getOriginalFilename());    // 删除zip
			if (zipfile.isFile() && zipfile.exists()) {
				zipfile.delete();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
