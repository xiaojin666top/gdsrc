package com.beawan.analysis.abnormal.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.Interface.GetErrExplainData;
import com.beawan.analysis.abnormal.bean.ErrData;
import com.beawan.analysis.abnormal.bean.ErrDataTr;
import com.beawan.analysis.abnormal.bean.ErrExplainData;
import com.beawan.analysis.abnormal.bean.MediaViewBean;
import com.beawan.analysis.abnormal.dao.IErrDataDao;
import com.beawan.analysis.abnormal.dao.IErrDataTrDao;
import com.beawan.analysis.abnormal.dao.IErrExplainDataDao;
import com.beawan.analysis.abnormal.dao.IMediaViewBeanDao;
import com.beawan.analysis.abnormal.dao.impl.ErrDataDaoimpl;
import com.beawan.analysis.abnormal.service.IAbnormalService;

@Service("abnormalSV")
public class AbnormalService implements IAbnormalService, GetErrExplainData<ErrExplainData>{

	/* ErrorData */
	@Resource
	protected IErrDataDao errDataDao;

	@Override
	public ErrData getErrorDataById(long id) {
		return this.errDataDao.findErrdataById(id);
	}
	
	@Override
	public ErrData getItemErrorData(long taskId, String tableItem) {
		
		String query = "from ErrData where taskId = " + taskId
				     + " and tableItem = '" + tableItem + "'"
				     + " and categoryFlag not in (11,12)";
		
		return ((ErrDataDaoimpl)errDataDao).selectSingle(ErrData.class, query);
	}
	
	@Override
	public List<ErrData> getItemErrorDatas(long taskId) {
		
		String query = "from ErrData where taskId = " + taskId
			     + " and categoryFlag not in (11,12)";
		
		return ((ErrDataDaoimpl)errDataDao).select(ErrData.class, query);
	}

	@Override
	public ErrData saveErrorData(ErrData errorData) {
		return this.errDataDao.savaErrData(errorData);
	}

	@Override
	public void deleteErrorData(ErrData errorData) {
		this.errDataDao.deleteErrData(errorData);
	}

	@Override
	public List<ErrData> getErrorDataByTaskId(Long taskId) {
		return this.errDataDao.findErrdataByTaskId(taskId);
	}
	
	@Override
	public List<ErrData> getErrorDataByTIdAndNo(Long taskId,String customerNo) {
		return this.errDataDao.getErrorDataByTIdAndNo(taskId, customerNo);
	}

	/* ErrorData end */

	/* ErrorDataTr */

	@Resource
	protected IErrDataTrDao errDataTrDao;

	@Override
	public ErrDataTr getErrDataTrById(Long id) {
		return this.errDataTrDao.getErrDataTrById(id);
	}

	@Override
	public void saveErrDataTr(ErrDataTr data) throws Exception {
		this.errDataTrDao.saveErrDataTr(data);
	}

	@Override
	public void deleteErrDataTr(ErrDataTr data) throws Exception {
		this.errDataTrDao.deleteErrDataTr(data);
	}

	@Override
	public List<ErrDataTr> findErrdataTrByErrDataId(Long errDataId) {
		return this.errDataTrDao.findErrdataTrByErrDataId(errDataId);
	}

	/* ErrorDataTr end */

	

	/* ErrExplainData */
	@Resource
	protected IErrExplainDataDao ErrExplainDataDao;

	@Override
	public ErrExplainData getErrExplainDataByName(String name) {
		return this.ErrExplainDataDao.getErrExplainDataByName(name);
	}

	@Override
	public ErrExplainData getErrExplainDataById(Long id) {
		return this.ErrExplainDataDao.getErrExplainDataById(id);
	}

	@Override
	public void saveErrExplainData(ErrExplainData data) throws Exception {
		this.ErrExplainDataDao.saveErrExplainData(data);
	}

	@Override
	public void saveErrExplainDatas(List<ErrExplainData> ErrExplainDatas) {
		if (ErrExplainDatas != null) {
			for (ErrExplainData ErrExplainData : ErrExplainDatas) {
				this.ErrExplainDataDao.saveErrExplainData(ErrExplainData);
			}
		}
	}
	
	@Override
	public void deleteErrExplainData(ErrExplainData data) throws Exception {
		this.ErrExplainDataDao.deleteErrExplainData(data);
	}


	@Override
	public List<ErrExplainData> getErrExplainDataByErrDataId(Long errDataId) {
		return this.ErrExplainDataDao.getErrExplainDataByErrDataId(errDataId);
	}
	
	@Override
	public  List<ErrExplainData> onGetErrExplainDataByErrDataId(Long errDataId){
		return this.ErrExplainDataDao.getErrExplainDataByErrDataId(errDataId);
	}

	/* ErrExplainData end */

	/* mediaviewbean */
	@Resource
	protected IMediaViewBeanDao mediaViewBeanDao;

	@Override
	public MediaViewBean findMediaViewBeanById(Long id) {
		return this.mediaViewBeanDao.findMediaViewBeanById(id);
	}

	@Override
	public List<MediaViewBean> findMediaViewByErrDataId(Long errDataId) {
		return this.mediaViewBeanDao.findMediaViewByErrDataId(errDataId);
	}

	@Override
	public void saveMediaViewBean(MediaViewBean data) throws Exception {
		this.mediaViewBeanDao.saveMediaViewBean(data);
	}

	@Override
	public void deleteMediaViewBean(MediaViewBean data) throws Exception {
		this.mediaViewBeanDao.deleteMediaViewBean(data);
	}



	/* mediaviewbean */
	
	@Override
	public void deleteAll(Long errDataId) throws Exception {
		
		List<ErrDataTr> errDataTrs=this.findErrdataTrByErrDataId(errDataId);
		if (errDataTrs!=null) {
			
		for(ErrDataTr dataTr:errDataTrs) {
			this.deleteErrDataTr(dataTr);
		}
		}
		List<ErrExplainData> errExplainDatas=this.getErrExplainDataByErrDataId(errDataId);
		if(errExplainDatas!=null) {
		for(ErrExplainData errExplainData:errExplainDatas) {
			this.deleteErrExplainData(errExplainData);
		}
		}
	}

}
