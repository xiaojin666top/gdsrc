package com.beawan.analysis.abnormal.service;

import java.util.List;

import com.beawan.analysis.abnormal.bean.ErrData;
import com.beawan.analysis.abnormal.bean.ErrDataTr;
import com.beawan.analysis.abnormal.bean.ErrExplainData;
import com.beawan.analysis.abnormal.bean.MediaViewBean;


public interface IAbnormalService {
	
	

	/* ErrorData */
	

	/**
	 * @Description (通过Id查找异常数据)
	 * @param id
	 * @return
	 */
	public ErrData getErrorDataById(long id);
	
	/**
	 * TODO 获得科目异常记录（核查前异常）
	 * @param taskId 任务号
	 * @param tableItem 科目名称
	 * @return
	 */
	public ErrData getItemErrorData(long taskId, String tableItem);
	
	/**
	 * TODO 获得科目异常记录（核查前异常）
	 * @param taskId 任务号
	 * @return
	 */
	public List<ErrData> getItemErrorDatas(long taskId);

	/**
	 * @Description (保存异常)
	 * @param errorData
	 */
	public ErrData saveErrorData(ErrData errorData);
	
	/**
	 * @Description (删除异常)
	 * @param errorData
	 */
	public void deleteErrorData(ErrData errorData);
	
	/**
	 * @Description (通过客户号查找一个客户的所有异常)
	 * @param custNo
	 * @return
	 */
	public List<ErrData> getErrorDataByTaskId(Long taskId);
	
	public List<ErrData> getErrorDataByTIdAndNo(Long taskId,String customerNo);
	
	
	/* ErrorData end */
	
	/* ErrDataTr */
	
	/**
	 * @Description (通过id查询ErrDataTr)
	 * @param name
	 * @return
	 */
	public ErrDataTr getErrDataTrById(Long id);
	
	
	/**
	 * @Description (保存ErrDataTr)
	 * @param data
	 * @return
	 */
	public void saveErrDataTr(ErrDataTr data) throws Exception;
	
	/**
	 * @Description (删除ErrDataTr)
	 * @param datas
	 * @return
	 */
	public void deleteErrDataTr(ErrDataTr data) throws Exception;
	
	
	/**
	 * @Description (通过 errDataId 查找 与errdata关联的 tr )
	 * @param errDataId
	 * @return
	 */
	public List<ErrDataTr> findErrdataTrByErrDataId(Long errDataId);
	
	
	/* saveErrDataTr end */

	


	/* Rejection end */

	/* ErrExplainData */
	
	/**
	 * @Description (循环保存ErrExplainDatas)
	 * @param ErrExplainDatas
	 */
	public void saveErrExplainDatas(List<ErrExplainData> ErrExplainDatas);
	
	/**
	 * @Description (通过名字查询ErrExplainData)
	 * @param name
	 * @return
	 */
	public ErrExplainData getErrExplainDataByName(String name);
	
	/**
	 * @Description (通过id查询ErrExplainData)
	 * @param id
	 * @return
	 */
	public ErrExplainData getErrExplainDataById(Long id);
	
	
	/**
	 * @Description (保存ErrExplainData)
	 * @param data
	 * @return
	 */
	public void saveErrExplainData(ErrExplainData data) throws Exception;
	
	
	/**
	 * @Description (删除ErrExplainData)
	 * @param data
	 * @return
	 */
	public void deleteErrExplainData(ErrExplainData data) throws Exception;
	
	
	/**
	 * @Description (得到与errdata关联的ErrExplainData)
	 * @param data
	 * @return
	 */
	public List<ErrExplainData> getErrExplainDataByErrDataId(Long errDataId);
	
	/* ErrExplainData end */
	

	
	/*mediaviewbean*/
	
	/**
	 * @Description (通过id找影像信息)
	 * @param id
	 * @return
	 */
	public  MediaViewBean findMediaViewBeanById(Long id);
	
	/**
	 * @Description (通过相关联的异常id找相关影像信息)
	 * @param id
	 * @return
	 */
	public  List<MediaViewBean> findMediaViewByErrDataId(Long errDataId);
	
	/**
	 * @Description (保存MediaViewBean)
	 * @param data
	 * @return
	 */
	public void saveMediaViewBean(MediaViewBean data) throws Exception;
	
	/**
	 * @Description (删除MediaViewBean)
	 * @param data
	 * @return
	 */
	public void deleteMediaViewBean(MediaViewBean data) throws Exception;
	 
	/*mediaviewbean*/
	
	/**
	 * @Description (删除ErrDataTr与ErrExplainData)
	 * @param data
	 * @return
	 */
	public void deleteAll(Long errDataId) throws Exception;
	
}
