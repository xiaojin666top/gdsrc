package com.beawan.analysis.abnormal.dao;

import org.springframework.dao.DataAccessException;

import com.beawan.analysis.abnormal.bean.ErrData;

import java.util.List;

public interface IErrDataDao  {
	
	/**
	 * @Description (保存异常项)
	 * @param data
	 * @throws DataAccessException
	 */
	public ErrData savaErrData(ErrData data)throws DataAccessException;
	
	/**
	 * @Description (删除异常项)
	 * @param data
	 * @throws DataAccessException
	 */
	public void deleteErrData(ErrData data)throws DataAccessException;
	
	/**
	 * @Description (通过客户号找出该客户的所有异常)
	 * @param customerNo 客户号
	 * @return
	 */
	public List<ErrData> findErrdataByTaskId(long taskId) ;
	
	
	/**
	 * @Description (通过id查找异常)
	 * @param customerNo 客户号
	 * @return
	 */
	public ErrData findErrdataById(Long id);
	
	public List<ErrData> getErrorDataByTIdAndNo(Long taskId, String customerNo);
	
	
}
