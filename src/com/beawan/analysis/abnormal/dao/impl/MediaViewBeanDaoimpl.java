package com.beawan.analysis.abnormal.dao.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.beawan.analysis.abnormal.bean.MediaViewBean;
import com.beawan.analysis.abnormal.dao.IMediaViewBeanDao;
import com.beawan.common.dao.impl.DaoHandler;

@Repository("mediaviewbean")
public class MediaViewBeanDaoimpl extends DaoHandler implements IMediaViewBeanDao{

	@Override
	public MediaViewBean findMediaViewBeanById(Long id) {
		// TODO Auto-generated method stub
		String query = "from MediaViewBean as model where model.id ='" + id +"'";
		List<MediaViewBean> list = getEntityManager().createQuery(query).getResultList();
		if(list.size() > 0){
			return list.get(0);
		}else{
			return null;
		}
	}

	@Override
	public List<MediaViewBean> findMediaViewByErrDataId(Long errDataId) {
		// TODO Auto-generated method stub
		String query = "from MediaViewBean as model where model.errDataId ='" + errDataId +"'";
		List<MediaViewBean> list = getEntityManager().createQuery(query).getResultList();
		if(list.size() > 0){
			return list;
		}else{
			return null;
		}
	}

	@Override
	public void saveMediaViewBean(MediaViewBean data) throws DataAccessException {
		// TODO Auto-generated method stub
		save(MediaViewBean.class, data);
	}

	@Override
	public void deleteMediaViewBean(MediaViewBean data) throws DataAccessException {
		// TODO Auto-generated method stub
		delete(MediaViewBean.class, data);
	}
	
	

}
