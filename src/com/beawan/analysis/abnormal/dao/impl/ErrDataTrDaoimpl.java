package com.beawan.analysis.abnormal.dao.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.beawan.analysis.abnormal.bean.ErrDataTr;
import com.beawan.analysis.abnormal.dao.IErrDataTrDao;
import com.beawan.common.dao.impl.DaoHandler;

@Repository("errDataTrDao")
public class ErrDataTrDaoimpl extends DaoHandler implements IErrDataTrDao{

	@Override
	public ErrDataTr getErrDataTrById(Long id) {
		// TODO Auto-generated method stub
		String query = "from ErrDataTr as model where model.id ='" + id +"'";
		List<ErrDataTr> list = getEntityManager().createQuery(query).getResultList();
		if(list.size() > 0){
			return list.get(0);
		}else{
			return null;
		}
	}

	@Override
	public void saveErrDataTr(ErrDataTr data) throws DataAccessException {
		// TODO Auto-generated method stub
		save(ErrDataTr.class, data);
	}

	@Override
	public void deleteErrDataTr(ErrDataTr data) throws DataAccessException {
		// TODO Auto-generated method stub
		delete(ErrDataTr.class, data);
	}

	@Override
	public List<ErrDataTr> findErrdataTrByErrDataId(Long errDataId) {
		// TODO Auto-generated method stub
		String query = "from ErrDataTr as model where model.errDataId ='" + errDataId +"'";
		List<ErrDataTr> list = getEntityManager().createQuery(query).getResultList();
		if(list.size() > 0){
			return list;
		}else{
			return null;
		}
	}

}
