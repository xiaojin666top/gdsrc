package com.beawan.analysis.abnormal.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.beawan.analysis.abnormal.bean.ErrData;
import com.beawan.analysis.abnormal.dao.IErrDataDao;
import com.beawan.common.dao.impl.DaoHandler;

@Repository("errDataDao")
public class ErrDataDaoimpl extends DaoHandler implements IErrDataDao {

	@Override
	public ErrData savaErrData(ErrData data) throws DataAccessException {
		// TODO Auto-generated method stub
		return save(ErrData.class, data);
	}

	@Override
	public void deleteErrData(ErrData data) throws DataAccessException {
		delete(ErrData.class, data);
	}
	
	@Override
	public ErrData findErrdataById(Long id){
		String query = "from ErrData as model where model.id ='" + id +"'";
		List<ErrData> list = getEntityManager().createQuery(query).getResultList();
		if(list.size() > 0){
			return list.get(0);
		}else{
			return null;
		}
	}

	@Override
	public List<ErrData> findErrdataByTaskId(long taskId) {
		String query = "from ErrData as model where model.taskId ='" + taskId +"'";
		List<ErrData> list = getEntityManager().createQuery(query).getResultList();
		if(list.size() > 0){
			return list;
		}else{
			return null;
		}
	}
	
	public List<ErrData> getErrorDataByTIdAndNo(Long taskId,String customerNo){
		String hql = "taskId=:taskId and customerNo=:customerNo";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("taskId", taskId);
		params.put("customerNo", customerNo);
		return select(ErrData.class, hql, params);
	}

}
