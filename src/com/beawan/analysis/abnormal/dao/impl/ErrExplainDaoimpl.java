package com.beawan.analysis.abnormal.dao.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.beawan.analysis.abnormal.bean.ErrExplainData;
import com.beawan.analysis.abnormal.dao.IErrExplainDataDao;
import com.beawan.common.dao.impl.DaoHandler;
import com.beawan.common.dao.impl.SerializeDao;

@Repository("ErrExplainDao")
public class ErrExplainDaoimpl extends DaoHandler implements IErrExplainDataDao{

	
	@Override
	public ErrExplainData getErrExplainDataByName(String name){
		String query = "from ErrExplainData as model where model.name ='" + name +"'";
		List<ErrExplainData> list = getEntityManager().createQuery(query).getResultList();
		if(list.size() > 0){
			return list.get(0);
		}else{
			return null;
		}
	}

	@Override
	public ErrExplainData getErrExplainDataById(Long id) {
		// TODO Auto-generated method stub
		String query = "from ErrExplainData as model where model.id ='" + id +"'";
		List<ErrExplainData> list = getEntityManager().createQuery(query).getResultList();
		if(list.size() > 0){
			return list.get(0);
		}else{
			return null;
		}
	}

	@Override
	public void saveErrExplainData(ErrExplainData data) throws DataAccessException {
		// TODO Auto-generated method stub
		save(ErrExplainData.class, data);
	}

	@Override
	public void deleteErrExplainData(ErrExplainData data) throws DataAccessException {
		// TODO Auto-generated method stub
		delete(ErrExplainData.class, data);
	}


	@Override
	public List<ErrExplainData> getErrExplainDataByErrDataId(Long errDataId){
		// TODO Auto-generated method stub
		String query = "from ErrExplainData as model where model.errDataId ='" + errDataId +"'";
		List<ErrExplainData> list = getEntityManager().createQuery(query).getResultList();
		if(list.size() > 0){
			return list;
		}else{
			return null;
		}
	}

	
	

}
