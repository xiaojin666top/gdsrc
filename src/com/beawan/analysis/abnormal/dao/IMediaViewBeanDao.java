package com.beawan.analysis.abnormal.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.analysis.abnormal.bean.MediaViewBean;

public interface IMediaViewBeanDao{
	
	/**
	 * @Description (通过id找影像信息)
	 * @param id
	 * @return
	 */
	public  MediaViewBean findMediaViewBeanById(Long id);
	
	/**
	 * @Description (通过相关联的异常id找相关影像信息)
	 * @param id
	 * @return
	 */
	public  List<MediaViewBean> findMediaViewByErrDataId(Long errDataId);
	
	/**
	 * @Description (保存MediaViewBean)
	 * @param data
	 * @return
	 */
	public void saveMediaViewBean(MediaViewBean data) throws DataAccessException;
	
	/**
	 * @Description (删除MediaViewBean)
	 * @param data
	 * @return
	 */
	public void deleteMediaViewBean(MediaViewBean data) throws DataAccessException;

}
