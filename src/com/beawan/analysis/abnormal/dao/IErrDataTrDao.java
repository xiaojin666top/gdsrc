package com.beawan.analysis.abnormal.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.analysis.abnormal.bean.ErrDataTr;


public interface IErrDataTrDao{
	
	/**
	 * @Description (通过id查询ErrDataTr)
	 * @param name
	 * @return
	 */
	public ErrDataTr getErrDataTrById(Long id);
	
	
	/**
	 * @Description (保存ErrDataTr)
	 * @param data
	 * @return
	 */
	public void saveErrDataTr(ErrDataTr data) throws DataAccessException;
	
	/**
	 * @Description (删除ErrDataTr)
	 * @param datas
	 * @return
	 */
	public void deleteErrDataTr(ErrDataTr data) throws DataAccessException;
	
	
	/**
	 * @Description (通过 errDataId 查找 与errdata关联的 tr )
	 * @param errDataId
	 * @return
	 */
	public List<ErrDataTr> findErrdataTrByErrDataId(Long errDataId);
	
}
