package com.beawan.analysis.abnormal.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.analysis.abnormal.bean.ErrExplainData;

public interface IErrExplainDataDao{

	/**
	 * @Description (通过名字查询ErrExplain)
	 * @param name
	 * @return
	 */
	public ErrExplainData getErrExplainDataByName(String name);
	
	/**
	 * @Description (通过id查询ErrExplain)
	 * @param id
	 * @return
	 */
	public ErrExplainData getErrExplainDataById(Long id);
	
	
	/**
	 * @Description (保存ErrExplain)
	 * @param data
	 * @return
	 */
	public void saveErrExplainData(ErrExplainData data) throws DataAccessException;
	
	
	/**
	 * @Description (删除ErrExplain)
	 * @param data
	 * @return
	 */
	public void deleteErrExplainData(ErrExplainData data) throws DataAccessException;
	
	
	/**
	 * @Description (得到与errdata关联的ErrExplain)
	 * @param data
	 * @return
	 */
	public List<ErrExplainData> getErrExplainDataByErrDataId(Long errDataId);
	
}
