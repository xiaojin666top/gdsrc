package com.beawan.analysis.abnormal.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.beawan.analysis.abnormal.bean.ErrData;
import com.beawan.analysis.abnormal.bean.ErrDataTr;
import com.beawan.analysis.abnormal.bean.ErrExplainData;
import com.beawan.analysis.abnormal.bean.MediaViewBean;
import com.beawan.analysis.abnormal.service.IAbnormalService;
import com.beawan.analysis.finansis.bean.ScheduleData;
import com.beawan.analysis.finansis.service.IFinanasisService;
import com.beawan.base.service.ISysDicSV;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.library.bean.ErrExplainTemplet;
import com.beawan.library.bean.ErrTemplet;
import com.beawan.library.bean.ReportTemplet;
import com.beawan.library.service.IIndustrySV;
import com.beawan.library.service.ITempletSV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.beawan.web.impl.UserSession;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/" })
public class AbnormalCtl {

	@Resource
	private ITaskSV taskSV;
	@Resource
	private IFinanasisService finanasisSV;
	@Resource
	private ITempletSV templetService;
	@Resource
	private IIndustrySV industrySV;
	@Resource
	private IAbnormalService abnormalService;
	@Resource
	private ISysDicSV sysDicSV;


	/**
	 * @Description (加载客户经理核查过程数据，在管理员界面回显)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("geterrData.do")
	public String GeterrData(HttpServletRequest request, HttpServletResponse response) {
		String taskId = UserSession.getUserSession(request).getTaskId();
		Task task = this.taskSV.getTaskById(Long.valueOf(taskId));
		String customerNo = task.getCustomerNo();
		List<ErrData> taskErrData = abnormalService.getErrorDataByTIdAndNo(Long.valueOf(taskId), customerNo);

		String reportId = request.getParameter("reportId");
		String errId = request.getParameter("errId");

		ErrData err = this.abnormalService.getErrorDataById(Long.valueOf(errId));

		ErrTemplet ErrTemplet = templetService.getErrTempletById(err.getErrTempletId());

		if (err.getExplain() != null) {
			request.setAttribute("explain", err.getExplain());
		}
		List<ReportTemplet> reports = this.templetService.getAllReportTemplets();
		String viewUrl = "";
		for (ReportTemplet rep : reports) {
			if (rep.getId().equals(reportId)) {
				viewUrl = ErrTemplet.getUrl();
				if ("主营业务收入".equals(rep.getName())) {
//					SaleIncome saleIncome = saleIncomeService.getSaleIncomeByTaskId(task.getId());
//					if (saleIncome != null) {
//						// 流水
//						List<Stream> streams = saleIncomeService.getStreamListBySaleId(saleIncome.getId());
//						if (streams != null && streams.size() > 0) {
//							// 流水
//							Stream stream1 = streams.get(0);
//							List<BankDetail> streamBank1 = saleIncomeService.getBankDetailByStreamId(stream1.getId());
//							request.setAttribute("stream1", stream1);
//							request.setAttribute("streamBank1", streamBank1);
//
//							Stream stream2 = streams.get(1);
//							List<BankDetail> streamBank2 = saleIncomeService.getBankDetailByStreamId(stream2.getId());
//							request.setAttribute("stream2", stream2);
//							request.setAttribute("streamBank2", streamBank2);
//
//							Stream stream3 = streams.get(2);
//							List<BankDetail> streamBank3 = saleIncomeService.getBankDetailByStreamId(stream3.getId());
//							request.setAttribute("stream3", stream3);
//							request.setAttribute("streamBank3", streamBank3);
//						}
//						// 税收
//						Revenue revenue = saleIncomeService.getRevenueBySaleId(saleIncome.getId());
//						if (revenue != null) {
//							List<BankDetail> addedTax = saleIncomeService.getBankDetailByRevenueId(revenue.getId(), 1); // 增值税
//							List<BankDetail> incomeTax = saleIncomeService.getBankDetailByRevenueId(revenue.getId(), 2); // 所得税
//							request.setAttribute("addedTaxnew", addedTax);
//							request.setAttribute("incomeTaxnew", incomeTax);
//						}
//						Hydropower hydropower = saleIncomeService.getHydropowerBySaleId(saleIncome.getId());
//						if (hydropower != null) {
//							List<BankDetail> water = saleIncomeService.getBankDetailByHydropowerId(hydropower.getId(),
//									1); // 水
//							List<BankDetail> pwer = saleIncomeService.getBankDetailByHydropowerId(hydropower.getId(),
//									2); // 电
//							request.setAttribute("waternew", water);
//							request.setAttribute("powernew", pwer);
//							request.setAttribute("hydropower", hydropower);
//						}
//						Salary salary = saleIncomeService.getSalaryBySaleId(saleIncome.getId());
//						if (salary != null) {
//							List<BankDetail> salarylist = saleIncomeService.getBankDetailBySalaryId(salary.getId()); // 工资
//							request.setAttribute("salarylist", salarylist);
//						}
//						List<MediaViewBean> views = abnormalService.findMediaViewByErrDataId(Long.valueOf(errId));
//						if (views.size() > 0) {
//							request.setAttribute("photoView", views);
//						}
//					}
				} else {
					List<ScheduleData> detailList = finanasisSV.getScheByTaskIdAndRepType(task.getId(),
							rep.getReportType());
					List<HashMap<String, String>> listMap = new ArrayList<HashMap<String, String>>();
					for (int j = 0; j < detailList.size(); j++) {
						ScheduleData scheduleData = detailList.get(j);
						ErrDataTr errDataTr = abnormalService.getErrDataTrById(scheduleData.getErrDataTrId());
						String data = null;
						ObjectMapper mapper = new ObjectMapper();
						if (errDataTr != null) {
							data = errDataTr.getTableData();
							try {
								HashMap<String, String> detailData = mapper.readValue(data, HashMap.class);
								listMap.add(detailData);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else {
							listMap.add(new HashMap<String, String>());
						}
					}
					List<ErrExplainTemplet> ErrTempletCases = templetService
							.getErrExplainTempletByErrTempletId(ErrTemplet.getId());
					if (ErrTempletCases != null && ErrTempletCases.size() > 0) {
						request.setAttribute("errCase", ErrTempletCases);
					}
					List<ErrExplainData> errDataCases = abnormalService.getErrExplainDataByErrDataId(err.getId());
					if (errDataCases.size() > 0) {
						request.setAttribute("errData", errDataCases);
					}
					List<MediaViewBean> views = abnormalService.findMediaViewByErrDataId(err.getId());
					if (views != null && views.size() > 0) {
						request.setAttribute("photoView", views);
					}
					boolean flag = false;

					for (ErrData errData : taskErrData) {
						ErrTemplet ErrTempletTemp = templetService.getErrTempletById(errData.getErrTempletId());
						if (err.getCategoryFlag() == 4
								&& (errData.getCategoryFlag() == 2 || errData.getCategoryFlag() == 3)
								&& ErrTemplet.getName().contains(ErrTempletTemp.getRemark())) {
							if (errData.getAdjustValue() != null && !errData.getAdjustValue().equals("")) {
								flag = true;
								break;
							}
						}
					}
					if (err.getCommonValue() != null && !err.getCommonValue().equals("")) {
						ObjectMapper mapper = new ObjectMapper();
						String data = err.getCommonValue();
						try {
							HashMap<String, String> commonData = mapper.readValue(data, HashMap.class);
							request.setAttribute("commonvalue", commonData);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					request.setAttribute("flag", flag);
					request.setAttribute("listMap", listMap);
					request.setAttribute("detailList", detailList);
					break;
				}
			}
			if (viewUrl.equals("")) {
				viewUrl = "views/Analysis/abnormal/commonCheck/";
			}
		}
		return viewUrl;
	}

	/**
	 * @Description (加载客户经理所采集的照片)
	 * @param request
	 * @param response
	 * @param url
	 */
	@RequestMapping("loadPhoto.do")
	public void loadPhoto(HttpServletRequest request, HttpServletResponse response, String url) {
		FileInputStream fis = null;
		OutputStream os = null;
		try {
			fis = new FileInputStream(url);
			os = response.getOutputStream();
			int count = 0;
			byte[] buffer = new byte[1024 * 8];
			while ((count = fis.read(buffer)) != -1) {
				os.write(buffer, 0, count);
				os.flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			fis.close();
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
