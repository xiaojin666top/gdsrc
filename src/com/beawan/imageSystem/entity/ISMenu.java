package com.beawan.imageSystem.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;

/**
 * @Author: xyh
 * @Date: 21/07/2020
 * @Description: 影像系统--目录类
 */
@Entity
@Table(name = "IS_MENU",schema = "GDTCESYS")
public class ISMenu extends BaseEntity {
    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="IS_MENU_SEQ")
   // @SequenceGenerator(name="IS_MENU_SEQ",allocationSize=1,initialValue=1, sequenceName="IS_MENU_SEQ")
    private Integer id;
    @Column(name = "PARENT_ID")
    private Integer parentId;//父级id
    @Column(name = "IS_MENU_NAME")
    private String isMenuName;//目录名称
    @Column(name = "IS_MENU_SORT")
    private Integer isMenuSort;//排序
    @Column(name = "IS_MENU_DESC")
    private String isMenuDesc;//描述
    @Column(name = "BOOKMARK")
    private String bookmark;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getIsMenuName() {
        return isMenuName;
    }

    public void setIsMenuName(String isMenuName) {
        this.isMenuName = isMenuName;
    }

    public Integer getIsMenuSort() {
        return isMenuSort;
    }

    public void setIsMenuSort(Integer isMenuSort) {
        this.isMenuSort = isMenuSort;
    }

    public String getIsMenuDesc() {
        return isMenuDesc;
    }

    public void setIsMenuDesc(String isMenuDesc) {
        this.isMenuDesc = isMenuDesc;
    }

    public String getBookmark() {
        return bookmark;
    }

    public void setBookmark(String bookmark) {
        this.bookmark = bookmark;
    }
}
