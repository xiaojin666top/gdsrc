package com.beawan.imageSystem.entity;


import com.beawan.core.BaseEntity;

import javax.persistence.*;


/**
 * 影像系统-影像资料表
 */
@Entity
@Table(name = "IS_IMAGE_INFO",schema = "GDTCESYS")
public class ISImageInfo extends BaseEntity {

    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="IS_IMAGE_INFO_SEQ")
  //  @SequenceGenerator(name="IS_IMAGE_INFO_SEQ",allocationSize=1,initialValue=1, sequenceName="IS_IMAGE_INFO_SEQ")
    private Integer id;

    @Column(name = "SERIAL_NUMBER")
    private String serialNumber;   //流水号
    @Column(name = "USER_NO")
    private String userNo;     //客户的编号
    @Column(name = "TYPE")
    private String type;        //类型：100营销      200贷前    300审批   400贷后
    @Column(name = "ORGI_NAME")
    private String orgiName;   //原始名
    @Column(name = "FORMAT_NAME")
    private String formatName;  //格式化后名称
    @Column(name = "FILE_TYPE")
    private String fileType;   //文件后缀类型
    @Column(name = "SIZE")
    private Long size;       //文件大小 KB
    @Column(name = "FILE_PATH")
    private String filePath;   //文件保存地址
    @Column(name = "MAPPING_PATH")
    private String mappingPath;//查看文件映射路径
    @Column(name = "DIRECTORY")
    private String directory;  //父级标签   基本信息/营业执照

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOrgiName() {
        return orgiName;
    }

    public void setOrgiName(String orgiName) {
        this.orgiName = orgiName;
    }

    public String getFormatName() {
        return formatName;
    }

    public void setFormatName(String formatName) {
        this.formatName = formatName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getMappingPath() {
        return mappingPath;
    }

    public void setMappingPath(String mappingPath) {
        this.mappingPath = mappingPath;
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }
}
