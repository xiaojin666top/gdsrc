package com.beawan.imageSystem.dao;
import com.beawan.core.BaseDao;
import com.beawan.imageSystem.entity.ISMenu;

/**
 * @Author: xyh
 * @Date: 21/07/2020
 * @Description:
 */
public interface ISMenuDao extends BaseDao<ISMenu> {
}
