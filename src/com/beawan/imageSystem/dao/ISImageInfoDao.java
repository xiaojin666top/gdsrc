package com.beawan.imageSystem.dao;

import com.beawan.core.BaseDao;
import com.beawan.imageSystem.entity.ISImageInfo;

/**
 * @author yzj
 */
public interface ISImageInfoDao extends BaseDao<ISImageInfo> {
}
