package com.beawan.imageSystem.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.imageSystem.dao.ISImageInfoDao;
import com.beawan.imageSystem.entity.ISImageInfo;
import org.springframework.stereotype.Repository;

/**
 * @author yzj
 */
@Repository("iSImageInfoDao")
public class ISImageInfoDaoImpl extends BaseDaoImpl<ISImageInfo> implements ISImageInfoDao {

}
