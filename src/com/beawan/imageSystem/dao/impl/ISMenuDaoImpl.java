package com.beawan.imageSystem.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.imageSystem.dao.ISMenuDao;
import com.beawan.imageSystem.entity.ISMenu;
import org.springframework.stereotype.Repository;

/**
 * @Author: xyh
 * @Date: 21/07/2020
 * @Description:
 */
@Repository("iSMenuDao")
public class ISMenuDaoImpl extends BaseDaoImpl<ISMenu> implements ISMenuDao {
}
