package com.beawan.imageSystem.service.impl;

import com.beawan.base.dto.RandomizingID;
import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.imageSystem.dao.ISImageInfoDao;
import com.beawan.imageSystem.entity.ISImageInfo;
import com.beawan.imageSystem.service.ISImageInfoService;
import com.beawan.common.config.AppConfig;
import com.platform.util.DateUtil;
import com.platform.util.StringUtil;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yzj
 */
@Service("iSImageInfoService")
public class ISImageInfoServiceImpl extends BaseServiceImpl<ISImageInfo> implements ISImageInfoService {
    /**
    * 注入DAO
    */
    @Resource(name = "iSImageInfoDao")
    public void setDao(BaseDao<ISImageInfo> dao) {
        super.setDao(dao);
    }
    @Resource
    public ISImageInfoDao iSImageInfoDao;

    @Override
    public void submitImage(MultipartFile[] dataFiles, String serialNumber, String customerNo, String uploadType, String dirPath, String userId) throws Exception {
        ISImageInfo imageInfo = new ISImageInfo();
        //获取图片编号
        RandomizingID random = new RandomizingID(uploadType, "yyyyMMddHHmmss", 4, false);
        //图片保存的路径--本地
        String paraDir = AppConfig.Cfg.IMAGE_PATH;
        //固定映射前缀
        String dir_mapping = AppConfig.Cfg.IMAGE_MAPPING;
        //映射地址
        String mapping = "";//根据不同的上传类型，映射地址不同
        if(Constants.LoanImageType.RETAIL.equals(uploadType)){
            //如果是营销上传图片
            mapping = AppConfig.Cfg.IMAGE_RETAIL_MAPPING;
            imageInfo.setDirectory("营销影像信息");
            dirPath = "retail";
        }else if(Constants.LoanImageType.SURVEY.equals(uploadType)){
            mapping = AppConfig.Cfg.IMAGE_SURVEY_MAPPING;
            imageInfo.setDirectory("贷前影像信息");
        }else if(Constants.LoanImageType.APPROVAL.equals(uploadType)){
            imageInfo.setDirectory("审查审批影像信息");
        }else if(Constants.LoanImageType.AFTER.equals(uploadType)){
            imageInfo.setDirectory("贷后影像信息");
        }

        if(dataFiles != null && dataFiles.length >0){
            for(int i= 0 ;i <dataFiles.length;i++){
                MultipartFile dataFile = dataFiles[i];
                //上传的文件名字
                String fileName = dataFile.getOriginalFilename().toString();
                //获取上传的文件的大小
                Long size = Long.parseLong(String.valueOf(dataFile.getSize())) / 1024;
                BigDecimal b = new BigDecimal(size);
                // 2表示2位 ROUND_HALF_UP表明四舍五入，
                size = b.setScale(2, BigDecimal.ROUND_HALF_UP).longValue();
                //获取上传文件的后缀
                int point = fileName.lastIndexOf(".");
                //后缀带.
                String suffix = fileName.substring(point);
                //新建一个本地的文件-----本地地址/前缀地址/业务流水号/文件名
                File target = new File(paraDir + File.separator + mapping + File.separator + serialNumber + File.separator + dirPath, random.genNewId() + suffix);
                if (!target.exists()) {
                    target.mkdirs();
                }
                //类似于copy
                dataFile.transferTo(target);
                //保存图像信息
                imageInfo.setSerialNumber(serialNumber);
                imageInfo.setUserNo(customerNo);//客户编号
                imageInfo.setType(uploadType);//图片类型
                imageInfo.setOrgiName(fileName);
                imageInfo.setFormatName(target.getName());
                imageInfo.setFileType(suffix);
                imageInfo.setFilePath(target.getAbsolutePath());
                imageInfo.setMappingPath("/"+ dir_mapping + "/" + mapping + "/" + serialNumber + "/" + dirPath + "/" + target.getName());
                imageInfo.setSize(size);
                imageInfo.setCreater(userId);
                imageInfo.setUpdater(userId);
                imageInfo.setUpdateTime(DateUtil.getNowTimestamp());
                iSImageInfoDao.saveOrUpdate(imageInfo);
            }
        }
    }

    @Override
    public List<ISImageInfo> queryByCondition(String serialNumber, String customerNo, String uploadType, String dirPath) throws Exception {
        StringBuilder sql = new StringBuilder("select id,MAPPING_PATH as mappingPath from IS_IMAGE_INFO ");
        sql.append(" where 1=1 and status=:status");
        Map<String,Object> params = new HashMap<>();
        params.put("status",Constants.NORMAL);
        if(!StringUtil.isEmptyString(serialNumber)){
            sql.append(" and SERIAL_NUMBER=:serialNumber");
            params.put("serialNumber",serialNumber);
        }
        if(!StringUtil.isEmptyString(customerNo)){
            sql.append(" and USER_NO=:customerNo");
            params.put("customerNo",customerNo);
        }
        if(!StringUtil.isEmptyString(uploadType)){
            sql.append(" and type=:uploadType");
            params.put("uploadType",uploadType);
        }
        if(!StringUtil.isEmptyString(dirPath)){
            sql.append(" and MAPPING_PATH like:dirPath");
            params.put("dirPath","%/"+dirPath+"/%");
        }
        List<ISImageInfo> imageInfos = iSImageInfoDao.findCustListBySql(ISImageInfo.class,sql
        .toString(),params);
        return imageInfos;
    }
}