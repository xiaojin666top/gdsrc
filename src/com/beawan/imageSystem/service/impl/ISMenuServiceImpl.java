package com.beawan.imageSystem.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.imageSystem.dao.ISMenuDao;
import com.beawan.imageSystem.dto.ISMenuDto;
import com.beawan.imageSystem.entity.ISImageInfo;
import com.beawan.imageSystem.entity.ISMenu;
import com.beawan.imageSystem.service.ISImageInfoService;
import com.beawan.imageSystem.service.ISMenuService;
import com.platform.util.DateUtil;
import com.platform.util.MapperUtil;

/**
 * @Author: xyh
 * @Date: 21/07/2020
 * @Description:
 */
@Service("iSMenuService")
public class ISMenuServiceImpl extends BaseServiceImpl<ISMenu> implements ISMenuService {

    /**
     * 注入DAO
     */
    @Resource(name = "iSMenuDao")
    public void setDao(BaseDao<ISMenu> dao) {
        super.setDao(dao);
    }
    @Resource
    public ISMenuDao iSMenuDao;
    
    @Resource
    private ISImageInfoService iSImageInfoService;


    @Override
    public List<ISMenuDto> getISMenuList(Integer parentId, String serNo, String custNo) throws Exception {
        //获取这个id下面的子菜单
        List<ISMenu> childMenus = iSMenuDao.selectByProperty("parentId",parentId,"isMenuSort");
        //获取子菜单下的子菜单
        if(CollectionUtils.isEmpty(childMenus)){
            return null;
        }
        List<ISMenuDto> realRet= MapperUtil.trans(childMenus, ISMenuDto.class);
        
        
        for(ISMenuDto isMenuDto : realRet){
            List<ISImageInfo> imageList = iSImageInfoService.queryByCondition(serNo,custNo,Constants.LoanImageType.SURVEY,isMenuDto.getId()+"");
            if(CollectionUtils.isEmpty(imageList)){
            	isMenuDto.setCount(0);
        	}else{
        		isMenuDto.setCount(imageList.size());
        	}
            List<ISMenu> dtoChild = iSMenuDao.selectByProperty("parentId",isMenuDto.getId(),"isMenuSort");
            if(CollectionUtils.isEmpty(dtoChild)){
                continue;
            }
            List<ISMenuDto> trans = MapperUtil.trans(dtoChild, ISMenuDto.class);
            isMenuDto.setChildMenus(trans);
        }
        return realRet;
    }

    @Override
    public ISMenu addIsMenu(ISMenu isMenu,String userId) throws Exception {
        if(isMenu.getId() == null || isMenu.getId() == 0){
            isMenu.setCreater(userId);
            isMenu.setUpdater(userId);
            isMenu.setUpdateTime(DateUtil.getNowTimestamp());
            //表示新增
            return iSMenuDao.saveOrUpdate(isMenu);
        }
        ISMenu old = iSMenuDao.selectByPrimaryKey(isMenu.getId());
        old.setIsMenuName(isMenu.getIsMenuName());
        old.setIsMenuSort(isMenu.getIsMenuSort());
        old.setIsMenuDesc(isMenu.getIsMenuDesc());
        old.setBookmark(isMenu.getBookmark());
        old.setUpdater(userId);
        old.setUpdateTime(DateUtil.getNowTimestamp());
        return iSMenuDao.saveOrUpdate(old);
    }

    @Override
    public void deleteIsMenu(Integer id,String userId) throws Exception {
        //根据id获取目录
        ISMenu nowMenu = iSMenuDao.findByPrimaryKey(id);
        nowMenu.setUpdater(userId);
        nowMenu.setUpdateTime(DateUtil.getNowTimestamp());
        nowMenu.setStatus(Constants.DELETE);
        iSMenuDao.saveOrUpdate(nowMenu);
        //删除子集----树形显示，非必要
        //List<ISMenu> childMenus= iSMenuDao.selectByProperty("parentId",id,"isMenuSort");
    }


}
