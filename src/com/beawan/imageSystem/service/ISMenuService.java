package com.beawan.imageSystem.service;

import com.beawan.core.BaseService;
import com.beawan.imageSystem.dto.ISMenuDto;
import com.beawan.imageSystem.entity.ISMenu;

import java.util.List;

/**
 * @Author: xyh
 * @Date: 21/07/2020
 * @Description:
 */
public interface ISMenuService extends BaseService<ISMenu> {
    /**
     * 根据id获取id的子节点和子节点的子节点
     * @param parentId
     * @return
     * @throws Exception
     */
    public List<ISMenuDto> getISMenuList(Integer parentId, String serNo, String custNo)throws Exception;

    /**
     * 新增影像目录
     * @param isMenu
     * @return
     * @throws Exception
     */
    public ISMenu addIsMenu(ISMenu isMenu,String userId)throws Exception;

    /**
     * 删除影像目录
     * @param id
     * @throws Exception
     */
    public void deleteIsMenu(Integer id,String userId)throws Exception;


}
