package com.beawan.imageSystem.service;

import com.beawan.core.BaseService;
import com.beawan.imageSystem.entity.ISImageInfo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author yzj
 */
public interface ISImageInfoService extends BaseService<ISImageInfo> {
    /**
     * 上传文件
     * @param dataFile 文件
     * @param serialNumber 业务流水号
     * @param customerNo 客户编号
     * @param uploadType 上传类型 ---营销，贷前，贷后
     * @param dirPath 目录地址
     * @param userId 上传的客户经理编号
     * @throws Exception
     */
    void submitImage(MultipartFile[] dataFile,String serialNumber,String customerNo,String uploadType,String dirPath,String userId)throws Exception;

    /**
     * 根据条件查询影像信息
     * @param serialNumber  业务流水号
     * @param customerNo 客户号
     * @param uploadType 上传照片的阶段
     * @param dirPath 路径。。。传入当前级，会在前后加上/   这样路径是唯一的，就不需要考虑父级
     * @return
     * @throws Exception
     */
    List<ISImageInfo> queryByCondition(String serialNumber,String customerNo,String uploadType,String dirPath)throws Exception;
}
