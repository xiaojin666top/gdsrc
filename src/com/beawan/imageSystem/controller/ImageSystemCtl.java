package com.beawan.imageSystem.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.base.entity.User;
import com.beawan.common.Constants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.imageSystem.dto.ISMenuDto;
import com.beawan.imageSystem.entity.ISImageInfo;
import com.beawan.imageSystem.entity.ISMenu;
import com.beawan.imageSystem.service.ISImageInfoService;
import com.beawan.imageSystem.service.ISMenuService;
import com.platform.util.DateUtil;
import com.platform.util.HttpUtil;
import com.platform.util.StringUtil;

/**
 * @Author: xyh
 * @Date: 21/07/2020
 * @Description: 影像目录控制器
 */
@Controller
@UserSessionAnnotation
@RequestMapping({"/image/system/"})
public class ImageSystemCtl extends BaseController {
    private static final Logger log = Logger.getLogger(ImageSystemCtl.class);
    @Resource
    private ISMenuService iSMenuService;
    @Resource
    private ISImageInfoService iSImageInfoService;

    //=======================目录管理=======================
    /**
     * 跳转到影像目录列表界面
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("iSMenuList.do")
    public ModelAndView iSMenuList(HttpServletRequest request, HttpServletResponse response){
        ModelAndView mav = new ModelAndView("views/imageSystem/ismenu/ismenulist");
        return mav;
    }

    /**
     * 获取影像目录列表
     * @param request
     * @param response
     * @param id
     * @return
     */
    @RequestMapping("getISMenuChildList.json")
    @ResponseBody
    public ResultDto getISMenuChildList(HttpServletRequest request,HttpServletResponse response,
    		@RequestParam(value="id",defaultValue = "0") int id,
    		String serNo, String custNo){
    	ResultDto resultDto = returnFail("获取影响目录失败");
        try{
            List<ISMenuDto> childList = iSMenuService.getISMenuList(id, serNo, custNo);
            resultDto = returnSuccess();
            resultDto.setRows(childList);
        }catch (Exception e){
            resultDto = returnFail("获取影响目录失败");
            e.printStackTrace();
        }
        return resultDto;
    }

    /**
     * 保存影像目录---用于修改或新增影像信息
     * @param request
     * @param response
     * @param isMenu
     * @return
     */
    @RequestMapping("saveISMenu.json")
    @ResponseBody
    public ResultDto saveISMenu(HttpServletRequest request, HttpServletResponse response, ISMenu isMenu){
        ResultDto resultDto = returnFail("新增目录失败");
        if(isMenu == null){
            return  resultDto;
        }
        try{
            User user = HttpUtil.getCurrentUser(request);
            iSMenuService.addIsMenu(isMenu,user.getUserId());
            resultDto = returnSuccess();
        }catch (Exception e){
            resultDto = returnFail("新增目录失败");
            e.printStackTrace();
        }
        return resultDto;
    }

    /**
     * 删除影像目录
     * @param request
     * @param response
     * @param id
     * @return
     */
    @RequestMapping("deleteISMenu.json")
    @ResponseBody
    public ResultDto deleteISMenu(HttpServletRequest request, HttpServletResponse response, int id){
        ResultDto resultDto = returnFail("删除目录失败");
        User user = HttpUtil.getCurrentUser(request);
        try{
            iSMenuService.deleteIsMenu(id,user.getUserId());
            resultDto = returnSuccess();
        }catch (Exception e){
            resultDto = returnFail("删除目录失败");
            e.printStackTrace();
        }
        return resultDto;
    }

    //=================================影像管理=========================
    /**
     * 获取审查影像列表
     * @param request
     * @param serialNumber
     * @return
     */
    @RequestMapping("getComplianceISTree.json")
    @ResponseBody
    public ResultDto getComplianceISTree(HttpServletRequest request,HttpServletResponse response,
    		@RequestParam(value="id",defaultValue = "0") int id,String serialNumber,
    		String custNo){
        ResultDto resultDto = returnFail("获取审查影像列表异常");
        try{
            List<ISMenuDto> childList = iSMenuService.getISMenuList(id, serialNumber, custNo);
//            for(ISMenuDto isMenuDto : childList){
//            	List<ISImageInfo> imageInfoList = iSImageInfoService.queryByCondition(serNo,custNo,uploadType,dirPath);
//            }
            resultDto = returnSuccess();
            resultDto.setRows(childList);
        }catch (Exception e){
            resultDto = returnFail("获取审查影像列表异常");
            e.printStackTrace();
        }
        return  resultDto;
    }

    /**
     * 上传文件
     * @param request
     * @param response
     * @param dataFile
     * @return
     */
    @RequestMapping("uploadImage.json")
    @ResponseBody
    public ResultDto uploadImage(HttpServletRequest request, HttpServletResponse response,
                                 String serNo,String custNo,String uploadType,String dirPath,
                                 @RequestParam(value = "file", required = false) MultipartFile[] dataFile) {
        ResultDto re = returnFail("");
        // 专门给ie和火狐下使用 uploadify插件
        request.setAttribute(HandlerMapping.PRODUCIBLE_MEDIA_TYPES_ATTRIBUTE, null);
        User user = HttpUtil.getCurrentUser(request);
        try{
            iSImageInfoService.submitImage(dataFile,serNo,custNo,uploadType,dirPath,user.getUserId());
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("上传成功");
        }catch (Exception e){
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 根据流水号或客户号查询营销营销资料
     * @param request
     * @param response
     * @param serNo
     * @param custNo
     * @return
     */
    @RequestMapping("getImages.json")
    @ResponseBody
    public ResultDto getImages(HttpServletRequest request, HttpServletResponse response,
                               String serNo,String custNo,String uploadType,String dirPath) {
        ResultDto re = returnFail("");
        if(StringUtil.isEmptyString(serNo) && StringUtil.isEmptyString(custNo)){
            re.setMsg("传输参数异常，请重试");
            return re;
        }
        try {
            List<ISImageInfo> imageInfoList = iSImageInfoService.queryByCondition(serNo,custNo,uploadType,dirPath);
           
            re.setRows(imageInfoList);
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("获取营销影像资料成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 根据ID移除营销影像资料
     * @param request
     * @param id
     * @return
     */
    @RequestMapping("removeImages.json")
    @ResponseBody
    public ResultDto removeImages(HttpServletRequest request, String id ) {
        ResultDto re = returnFail("");
        if(StringUtil.isEmptyString(id)){
            re.setMsg("参数内容异常，请重试");
            return re;
        }
        User user = HttpUtil.getCurrentUser(request);
        try {
            ISImageInfo imageInfo = iSImageInfoService.findByPrimaryKey(Integer.parseInt(id));
            if(imageInfo==null){
                re.setMsg("删除影像资料异常，请重试");
                log.error("查询营销营销数据为空，查询id为：" + id);
                return re;
            }
            imageInfo.setUpdater(user.getUserId());
            imageInfo.setUpdateTime(DateUtil.getNowTimestamp());
            imageInfo.setStatus(Constants.DELETE);
            iSImageInfoService.saveOrUpdate(imageInfo);
            
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setRows(imageInfo);
            re.setMsg("获取营销影像资料成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }
    /**
     * 根据mappingPath移除营销影像资料
     * @param request
     * @param mappingPath
     * @return
     */
    @RequestMapping("removeImagesByMappingPath.json")
    @ResponseBody
    public ResultDto removeImagesByMappingPath(HttpServletRequest request, String mappingPath ) {
        ResultDto re = returnFail("");
        if(StringUtil.isEmptyString(mappingPath)){
            re.setMsg("参数内容异常，请重试");
            return re;
        }
        User user = HttpUtil.getCurrentUser(request);
        try {
            List<ISImageInfo> imageInfos = iSImageInfoService.selectByProperty("mappingPath",mappingPath);
            if(CollectionUtils.isEmpty(imageInfos)){
                re.setMsg("删除影像资料异常，请重试");
                log.error("查询营销营销数据为空，查询mappingPath为：" + mappingPath);
                return re;
            }
            ISImageInfo delImage = imageInfos.get(0);
            delImage.setUpdater(user.getUserId());
            delImage.setUpdateTime(DateUtil.getNowTimestamp());
            delImage.setStatus(Constants.DELETE);
            iSImageInfoService.saveOrUpdate(delImage);
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("获取营销影像资料成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

}
