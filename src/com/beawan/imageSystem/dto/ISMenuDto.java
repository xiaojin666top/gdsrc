package com.beawan.imageSystem.dto;

import java.util.List;

/**
 * @Author: xyh
 * @Date: 21/07/2020
 * @Description:
 */
public class ISMenuDto {
    private Integer id;
    private Integer parentId;//父级id
    private String isMenuName;//目录名称
    private Integer isMenuSort;//排序
    private String isMenuDesc;//描述
    private String bookmark;//书签
    private List<ISMenuDto> childMenus;//子目录集合
    private Integer count;//映像照片数量
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getIsMenuName() {
        return isMenuName;
    }

    public void setIsMenuName(String isMenuName) {
        this.isMenuName = isMenuName;
    }

    public Integer getIsMenuSort() {
        return isMenuSort;
    }

    public void setIsMenuSort(Integer isMenuSort) {
        this.isMenuSort = isMenuSort;
    }

    public String getIsMenuDesc() {
        return isMenuDesc;
    }

    public void setIsMenuDesc(String isMenuDesc) {
        this.isMenuDesc = isMenuDesc;
    }

    public List<ISMenuDto> getChildMenus() {
        return childMenus;
    }

    public String getBookmark() {
        return bookmark;
    }

    public void setBookmark(String bookmark) {
        this.bookmark = bookmark;
    }

    public void setChildMenus(List<ISMenuDto> childMenus) {
        this.childMenus = childMenus;
    }

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}
    
}
