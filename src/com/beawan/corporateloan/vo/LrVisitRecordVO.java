package com.beawan.corporateloan.vo;

import com.beawan.core.BaseEntity;
import java.sql.Timestamp;

/**
 * 营销记录表
 */

public class LrVisitRecordVO extends BaseEntity {
    private Integer id;
	private String serialNumber;//流水号
    private Integer visitWay;//拜访方式 0微信  1电话  2实地
    private String visitPersonName;//拜访客户名字-对接人
    private String visitSituation;//拜访信息
    private String visitTime;//拜访时间
    private String visitAddr;//拜访地址
    private Double longGitude;//经度
    private Double latitude;//纬度
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public Integer getVisitWay() {
		return visitWay;
	}
	public void setVisitWay(Integer visitWay) {
		this.visitWay = visitWay;
	}
	public String getVisitPersonName() {
		return visitPersonName;
	}
	public void setVisitPersonName(String visitPersonName) {
		this.visitPersonName = visitPersonName;
	}
	public String getVisitSituation() {
		return visitSituation;
	}
	public void setVisitSituation(String visitSituation) {
		this.visitSituation = visitSituation;
	}
	public String getVisitTime() {
		return visitTime;
	}

	public void setVisitTime(String visitTime) {
		this.visitTime = visitTime;
	}
	public String getVisitAddr() {
		return visitAddr;
	}
	public void setVisitAddr(String visitAddr) {
		this.visitAddr = visitAddr;
	}
	public Double getLongGitude() {
		return longGitude;
	}
	public void setLongGitude(Double longGitude) {
		this.longGitude = longGitude;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
    
}
