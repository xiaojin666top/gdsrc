package com.beawan.corporateloan.service;

import com.beawan.core.BaseService;
import com.beawan.corporateloan.entity.LcDoc;

/**
 * @author yzj
 */
public interface LcDocService extends BaseService<LcDoc> {

    /**
     * 保存更新审查文档
     * @param docId
     * @param docInfo
     * @param noHtml
     */
    String saveDoc(Integer docId, String docInfo, String noHtml);
}
