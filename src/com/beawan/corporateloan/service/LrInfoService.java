package com.beawan.corporateloan.service;

import com.beawan.core.BaseService;
import com.beawan.corporateloan.entity.LrInfo;

/**
 * @author yzj
 */
public interface LrInfoService extends BaseService<LrInfo> {
	/**
	 * 创建新的营销任务
	 * @param customerNo
	 * @param userNo
	 * @return
	 * @throws Exception
	 */
	LrInfo createNewLrTask(String customerNo,String userNo)throws Exception;
	/**
	 * 同步LrInfo信息的保存，并将Lrinfo中有关客户的信息同步到LrCmClass中
	 * @throws Exception
	 */
	void synchroLrInfo(LrInfo lrInfo,String userNo)throws Exception;
	/**
	 * 营销结束
	 * @param serialNumber
	 * @param userNo
	 * @throws Exception
	 */
	void finishRetailTask(String serialNumber,String userNo)throws Exception;
	/**
	 * 跳转到营销进件节点
	 * @param serialNumber
	 * @param userNo
	 * @throws Exception
	 */
	void submitRetailTask(String serialNumber,String userNo)throws Exception;

	/**
	 * 提交营销进件中的任务--》提交到贷前调查岗（待分配状态）
	 * @param serialNumber
	 * @param userNo
	 * @throws Exception
	 */
	void submitToSurvey(String serialNumber, String userNo) throws  Exception;

	/**
	 * 提交营销进件中的任务--》提交到贷前调查岗（待分配状态） -->提交到贷前待处理任务
	 * 因公司部业务调整，合并以上两个流程，进行统一事物管理
	 * @param serialNumber
	 * @param userNo
	 * @throws Exception
	 */
	void submitRetailToSurvey(String serialNumber, String userNo, String userName) throws  Exception;
}
