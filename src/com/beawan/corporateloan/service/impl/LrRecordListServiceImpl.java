package com.beawan.corporateloan.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.core.Pagination;
import com.beawan.corporateloan.dao.LrRecordListDao;
import com.beawan.corporateloan.dto.LrResultDto;
import com.beawan.corporateloan.entity.LrRecordList;
import com.beawan.corporateloan.service.LrRecordListService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.corporateloan.entity.LrRecordList;

import java.util.HashMap;
import java.util.Map;

/**
 * @author yzj
 */
@Service("lrRecordListService")
public class LrRecordListServiceImpl extends BaseServiceImpl<LrRecordList> implements LrRecordListService{
    /**
    * 注入DAO
    */
    @Resource(name = "lrRecordListDao")
    public void setDao(BaseDao<LrRecordList> dao) {
        super.setDao(dao);
    }
    @Resource
    public LrRecordListDao lrRecordListDao;

    @Override
    public Pagination<LrResultDto> queryLrResultList(String userNo, String resultType, int page, int rows) {
        Map<String, Object> params = new HashMap<>();
        StringBuilder sql = new StringBuilder("select ser_no as serNo,c.user_name username,b.customer_name,a.customer_class customerClass,")
                .append("a_way retailWay,a_TIME retailTime,a_product as retailProduct")
                .append(" from lr_record a")
                .append(" inner join CUS_BASE b on a.customer_no=b.customer_no")
                .append(" inner join BS_USER_INFO c on a.user_no=c.user_id")
                .append(" where 1=1");
        if(userNo!=null && !"".equals(userNo)){
            sql.append(" and a.user_no=:userNo");
            params.put("userNo", userNo);
        }
        if(resultType!=null && !"".equals(resultType)){
            sql.append(" and a_result=:resultType");
            params.put("resultType", resultType);
        }
        String count = "select count(1) from (" + sql + ")";
        Pagination<LrResultDto> pager = lrRecordListDao.findCustSqlPagination(LrResultDto.class, sql.toString(), count, params, page, rows);
        return pager;
    }
}
