package com.beawan.corporateloan.service.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.beawan.analysis.abnormal.service.IAbnormalService;
import com.beawan.analysis.finansis.service.IFnTableService;
import com.beawan.base.dao.ISysDicDAO;
import com.beawan.base.entity.SysDic;
import com.beawan.base.entity.User;
import com.beawan.base.service.IUserSV;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.core.Pagination;
import com.beawan.corporateloan.dao.LcReviewContentDao;
import com.beawan.corporateloan.dao.LcRiskDbDao;
import com.beawan.corporateloan.dao.LeExamineContentDao;
import com.beawan.corporateloan.dao.LeExaminePointDao;
import com.beawan.corporateloan.dao.LeTaskDao;
import com.beawan.corporateloan.dao.LmProcessDao;
import com.beawan.corporateloan.dao.LrInfoDao;
import com.beawan.corporateloan.dto.LcTaskDto;
import com.beawan.corporateloan.dto.LeTaskDto;
import com.beawan.corporateloan.dto.LmTaskInfoDto;
import com.beawan.corporateloan.entity.LcTask;
import com.beawan.corporateloan.entity.LeExamineContent;
import com.beawan.corporateloan.entity.LeExaminePoint;
import com.beawan.corporateloan.entity.LeTask;
import com.beawan.corporateloan.entity.LmProcess;
import com.beawan.corporateloan.entity.LrInfo;
import com.beawan.corporateloan.service.LcTaskService;
import com.beawan.corporateloan.service.LmProcessService;
import com.beawan.customer.bean.CusFSRecord;
import com.beawan.customer.dao.ICusBaseDAO;
import com.beawan.customer.service.FinriskResultService;
import com.beawan.customer.service.ICusFSRecordSV;
import com.beawan.customer.service.ICusFSToolSV;
import com.beawan.rate.service.RcResultService;
import com.beawan.survey.custInfo.dao.CompContributeDao;
import com.beawan.survey.custInfo.dao.CompInnerManageDao;
import com.beawan.survey.custInfo.dao.CompPowerDao;
import com.beawan.survey.custInfo.dao.CompProductDao;
import com.beawan.survey.custInfo.dao.CompProductSalesDao;
import com.beawan.survey.custInfo.dao.CompRunAnalysisDao;
import com.beawan.survey.custInfo.dao.CompRunSupplyDao;
import com.beawan.survey.custInfo.dao.ICompConnectDAO;
import com.beawan.survey.custInfo.dao.ICompFinancingBankDAO;
import com.beawan.survey.custInfo.dao.ICompFinancingExtGuaraDAO;
import com.beawan.survey.custInfo.dao.ICompManagerDAO;
import com.beawan.survey.custInfo.dao.ICompRunDAO;
import com.beawan.survey.custInfo.dto.CompBaseDto;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.beawan.survey.custInfo.service.ICompFinanceSV;
import com.beawan.survey.guarantee.dao.CompFinanceInfoDao;
import com.beawan.survey.guarantee.dao.GuaAccountsDao;
import com.beawan.survey.guarantee.dao.GuaAnalysisDao;
import com.beawan.survey.guarantee.dao.GuaChattelDao;
import com.beawan.survey.guarantee.dao.GuaCompanyDao;
import com.beawan.survey.guarantee.dao.GuaDepositReceiptDao;
import com.beawan.survey.guarantee.dao.GuaEstateDao;
import com.beawan.survey.guarantee.dao.GuaPersonDao;
import com.beawan.survey.loanInfo.dao.IRiskAnalysisDAO;
import com.beawan.survey.loanInfo.service.IApplyInfoSV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.platform.util.DateUtil;
import com.platform.util.ExceptionUtil;
import com.platform.util.StringUtil;

/**
 *
 * @ClassName: LmProcessServiceImpl
 * @author xyh
 * @date 16 Jun 2020
 *
 */
@Service("lmProcessService")
public class LmProcessServiceImpl extends BaseServiceImpl<LmProcess> implements LmProcessService {

	@Resource(name = "lmProcessDao")
	public void setDao(BaseDao<LmProcess> dao) {
		super.setDao(dao);
	}

	@Resource
	private LmProcessDao lmProcessDao;
	@Resource
	private ICompBaseSV compBaseSV;
	@Resource
	private ISysDicDAO sysDicDAO;
	@Resource
	private ICompRunDAO compRunDAO;
	@Resource
	private ICompManagerDAO compManagerDAO;
	@Resource
	private ICompConnectDAO compConnectDAO;
	@Resource
	private ICompFinancingBankDAO compFinancingBankDAO;
	@Resource
	private ICompFinancingExtGuaraDAO compFinancingExtGuaraDAO;
	@Resource
	public CompInnerManageDao compInnerManageDao;
	@Resource
	private CompRunAnalysisDao compRunAnalysisDao;
	@Resource
	private CompRunSupplyDao compRunSupplyDao;
	@Resource
	private CompProductDao compProductDao;
	@Resource
	private CompPowerDao compPowerDao;
	@Resource
	private CompProductSalesDao compProductSalesDao;
	@Resource
	private GuaCompanyDao guaCompanyDao;
	@Resource
	private CompFinanceInfoDao compFinanceInfoDao;
	@Resource
	private GuaPersonDao guaPersonDao;
	@Resource
	private GuaEstateDao guaEstateDao;
	@Resource
	private GuaChattelDao guaChattelDao;
	@Resource
	private GuaAccountsDao guaAccountsDao;
	@Resource
	private GuaDepositReceiptDao guaDepositReceiptDao;
	@Resource
	private GuaAnalysisDao guaAnalysisDao;
	@Resource
	private CompContributeDao compContributeDao;
	@Resource
	private IRiskAnalysisDAO riskAnalysisDAO;
	@Resource
	private ITaskSV taskSV;
	@Resource
	private LrInfoDao lrInfoDao;
	@Resource
	private ICusFSRecordSV cusFSRecordSV;

	@Resource
	protected ICompFinanceSV compFinanceSV;
	@Resource
	private ICusFSToolSV cusFSToolSV;
	@Resource
	private FinriskResultService finriskResultService;
	@Resource
	private IApplyInfoSV applyInfoSV;
	@Resource
	private IFnTableService fnTableService;
	@Resource
	private IAbnormalService abnormalSV;
	@Resource
	private IUserSV userSV;
	@Resource
	private LcTaskService lcTaskService;
	@Resource
	private LeTaskDao leTaskDao;
	@Resource
	private LeExaminePointDao leExaminePointDao;
	@Resource
	private LeExamineContentDao leExamineContentDao;
	@Resource
	private RcResultService resultService;
	@Resource
	private LcRiskDbDao lcRiskDbDao;
	@Resource
	private LcReviewContentDao lcReviewContentDao;
	@Resource
	private ICusBaseDAO cusBaseDAO;


	@Override
	public Pagination<LmTaskInfoDto> queryLmProcessPaging(String userNo,String stageType,int stage, int page, int pageSize) throws Exception {
		return queryLmProcessPaging(null,null, userNo, stageType, stage, page, pageSize);
	}

	@Override
	public Pagination<LmTaskInfoDto> queryLmProcessPaging(Map<String, String> query,String orgNo, String userNo, String stageType, int stage, int page, int pageSize) throws Exception {
		StringBuilder sql = new StringBuilder();
		Map<String, Object> params = new HashMap<>();
		//	select lmp.CUSTOMER_NO as customerNo, lmp.SERIAL_NUMBER as serialNumber,
		//lmp.STAGE as stage ,cb.CUSTOMER_NAME as customerName ,lmp.position_user as userNo,
		//lmp.position_user_name as userName,C.USER_NAME as lrUserName,lmp.UPDATE_TIME as updateTime,
		//lri.UPDATE_TIME as lrUpdateTime,lri.CUSTOMER_CLASS as lrCustomerClass
		//from LM_PROCESS lmp
		//left join CUS_BASE cb on lmp.CUSTOMER_NO = cb.CUSTOMER_NO
		//left join LR_INFO lri on lmp.SERIAL_NUMBER = lri.SERIAL_NUMBER
		//left join BS_USER_INFO C ON lri.user_no=c.user_id
		//where 1=1;
		sql.append("select lmp.CUSTOMER_NO as customerNo, lmp.SERIAL_NUMBER as serialNumber,");
		sql.append(" lmp.STAGE as stage ,cb.CUSTOMER_NAME as customerName ,lmp.position_user as userNo,");
		sql.append(" lmp.position_user_name as userName,C.USER_NAME as lrUserName,lmp.UPDATE_TIME as updateTime, ");
		sql.append(" lri.UPDATE_TIME as lrUpdateTime,lri.CUSTOMER_CLASS as lrCustomerClass ");
		sql.append(" from LM_PROCESS lmp  ");
		sql.append(" left join CUS_BASE cb on lmp.CUSTOMER_NO = cb.CUSTOMER_NO");
		sql.append(" left join LR_INFO lri on lmp.SERIAL_NUMBER = lri.SERIAL_NUMBER");
		sql.append(" left join BS_USER_INFO C ON lri.user_no=c.user_id");
		sql.append(" where 1=1");
		//stageType=
		if(!StringUtil.isEmptyString(stageType)){
			//鍙互鏄瓑浜庯紝澶т簬锛屽皬浜庯紝澶т簬绛変簬锛屽皬浜庣瓑浜?
			sql.append(" and lmp.STAGE ").append(stageType).append(stage);
			//params.put("stage", stage);
		}
		//褰撳墠鑺傜偣鎿嶄綔浜轰负鐧诲綍鐢ㄦ埛
		if(!StringUtil.isEmptyString(userNo)){
			sql.append(" and lmp.position_user = "+userNo);
			//params.put("userNo", userNo);
		}
		if(!StringUtil.isEmptyString(orgNo)){
			sql.append(" and C.ACC_ORG_NO "+orgNo);
			//params.put("orgNo",orgNo);
		}
		//鍒ゆ柇鏄惁瀛樺湪杩囨护鍙傛暟
		if(query!=null && query.size()!=0){
			String customerName = query.get("customerName");
			String customerNo = query.get("customerNo");
			if(!StringUtil.isEmptyString(customerName)){
				//if (!StringUtil.isEmptyString(custName)) {
				//			sql.append(" and b.customer_name like '%"+custName+"%'");
				//		//	params.put("custName", "%" + custName + "%");
				//		}
				sql.append(" and cb.CUSTOMER_NAME like '%"+customerName+"%'");
				//params.put("customerName", "%"+customerName+"%");

			}
			if(!StringUtil.isEmptyString(customerNo)){
				sql.append(" and cb.CUSTOMER_NO ="+"'"+customerNo+"' ");
				//params.put("customerNo", customerNo);
			}
		}
		String count = "select count(*) from (" + sql + ") tbl";
		sql.append(" order by lmp.UPDATE_TIME DESC");
		Pagination<LmTaskInfoDto> pager = lmProcessDao.findCustSqlPagination(LmTaskInfoDto.class, sql.toString(), count, params, page, pageSize);
		return pager;
	}

	@Override
	public Pagination<LmTaskInfoDto> queryUpRetailPaging(String userNo, String customerNo, String orgNo, String customerName, int page, int pageSize) throws Exception {
		StringBuilder sql = new StringBuilder();
		Map<String, Object> params = new HashMap<>();
		sql.append("select lmp.CUSTOMER_NO as customerNo, lmp.SERIAL_NUMBER as serialNumber,")
				.append(" lmp.STAGE as stage ,cb.CUSTOMER_NAME as customerName ,lmp.position_user as userNo,")
				.append(" lmp.position_user_name as userName,lmp.UPDATE_TIME as updateTime")
				.append(" from LM_PROCESS lmp")
				.append(" left join LR_INFO lri on lmp.SERIAL_NUMBER = lri.SERIAL_NUMBER")
				.append(" left join CUS_BASE cb on lmp.CUSTOMER_NO = cb.CUSTOMER_NO")
				.append(" left join BS_USER_INFO C ON lri.user_no=c.user_id ")
				.append(" where 1=1 ");
		sql.append(" and lmp.STAGE > "+Constants.LoanProcessType.RETAIL_IN_ING);
		//params.put("stage", Constants.LoanProcessType.RETAIL_IN_ING);
		//营销客户经理
		if(!StringUtil.isEmptyString(userNo)){
			sql.append(" and  lri.user_no  = "+userNo);
			//params.put("userNo", userNo);
		}else{
			sql.append(" and  lri.user_no is not null");
		}
		if(!StringUtil.isEmptyString(orgNo)){
			sql.append(" and C.ACC_ORG_NO "+orgNo);
			//params.put("orgNo",orgNo);
		}
		if(!StringUtils.isEmpty(customerNo)){
			sql.append(" and lmp.CUSTOMER_NO = customerNo");
			//params.put("customerNo", );
		}
		if(!StringUtils.isEmpty(customerNo)){
			sql.append(" and cb.CUSTOMER_NAME like customerName");
			params.put("customerName", "%" + customerName + "%");
		}
		String count = "select count(*) from (" + sql + ") tbl";

		sql.append(" order by lri.UPDATE_TIME DESC");
		Pagination<LmTaskInfoDto> pager = lmProcessDao.findCustSqlPagination(LmTaskInfoDto.class, sql.toString(), count, params, page, pageSize);
		return pager;
	}

	@Override
	public Pagination<LmTaskInfoDto> queryDownRetailPaging(String userNo, int page, int pageSize) throws Exception {
		StringBuilder sql = new StringBuilder();
		Map<String, Object> params = new HashMap<>();
		sql.append("select lmp.CUSTOMER_NO as customerNo, lmp.SERIAL_NUMBER as serialNumber,")
				.append(" lmp.STAGE as stage ,cb.CUSTOMER_NAME as customerName ,lmp.position_user as userNo,")
				.append(" lmp.position_user_name as userName,lmp.UPDATE_TIME as updateTime")
				.append(" from LM_PROCESS lmp")
				.append(" left join LR_INFO lri on lmp.SERIAL_NUMBER = lri.SERIAL_NUMBER")
				.append(" left join CUS_BASE cb on lmp.CUSTOMER_NO = cb.CUSTOMER_NO")
				.append(" where 1=1 ");
		sql.append(" and lmp.STAGE = "+Constants.LoanProcessType.RETAIl_STOP);
		//params.put("stage", Constants.LoanProcessType.RETAIl_STOP);
		//营销客户经理
		if(!StringUtil.isEmptyString(userNo)){
			sql.append(" and  lri.user_no  = "+userNo);
			//params.put("userNo", userNo);
		}else{
			sql.append(" and  lri.user_no is not null");
		}
		String count = "select count(*) from (" + sql + ") tbl";
		Pagination<LmTaskInfoDto> pager = lmProcessDao.findCustSqlPagination(LmTaskInfoDto.class, sql.toString(), count, params, page, pageSize);
		return pager;
	}

	public StringBuilder getQueryLcTaskCommSql(){
		StringBuilder sql = new StringBuilder();
		sql.append("select a.serial_number serialNumber, b.customer_name customerName,")
				.append(" b.lb_user_name lbUserName,b.business_type businessType,")
				.append(" b.loan_number loanNumber,b.loan_term loanTerm,b.id lcTaskId,")
				.append(" b.submission_time submissionTime,b.loan_rate loanRate")
				.append(" from LM_PROCESS a")
				.append(" left join LC_TASK b on a.serial_number=b.serial_number")
				.append(" left join BS_USER_INFO C ON b.user_no=c.user_id");
		return sql;
	}

	@Override
	public Pagination<LcTaskDto> queryLcTaskPaging(int stage,String userNo,String orgNo, String custName, String userName, String startTime, String endTime, int page, int pageSize) throws Exception {
		StringBuilder sql = new StringBuilder();
		Map<String, Object> params = new HashMap<>();
		params.put("stage", stage);
		params.put("status", Constants.NORMAL);
		sql = this.getQueryLcTaskCommSql();
		sql.append(" where a.stage=:stage and a.status=:status");
		if(!StringUtil.isEmptyString(userNo)){
			sql.append(" and b.USER_NO = :userNo");
			params.put("userNo", userNo);
		}
		if(!StringUtil.isEmptyString(orgNo)){
			sql.append(" and C.ACC_ORG_NO=:orgNo ");
			params.put("orgNo",orgNo);
		}
		if(!StringUtil.isEmptyString(custName)){
			sql.append(" and b.customer_name like :custName");
			params.put("custName", "%" + custName + "%");
		}
		if(!StringUtil.isEmptyString(userName)){
			sql.append(" and b.lb_user_name like :userName");
			params.put("userName", "%" + userName + "%");
		}
		if(!StringUtil.isEmptyString(startTime)){
			sql.append(" and b.submission_time >= :startTime");
			params.put("startTime", startTime);
		}
		if(!StringUtil.isEmptyString(endTime)){
			sql.append(" and b.submission_time <= :endTime");
			params.put("endTime", endTime);
		}
		String count =  "select count(1) from (" + sql.toString() + ")";
		sql.append(" order by b.submission_time desc");
		Pagination<LcTaskDto> pager = lmProcessDao.findCustSqlPagination(LcTaskDto.class, sql.toString(), count, params, page, pageSize);
		return pager;
	}

	@Override
	public Pagination<LcTaskDto> queryLcTaskFinishPaging(int stage,String userNo,String orgNo, String custName, String userName, String startTime, String endTime, int page, int pageSize) throws Exception {
		StringBuilder sql = new StringBuilder();
		Map<String, Object> params = new HashMap<>();
		params.put("stage", stage);
		params.put("status", Constants.NORMAL);
		sql = this.getQueryLcTaskCommSql();
		sql.append(" where a.stage>:stage and a.status=:status");
		if(!StringUtil.isEmptyString(userNo)){
			sql.append(" and b.USER_NO = :userNo");
			params.put("userNo", userNo);
		}
		if(!StringUtil.isEmptyString(orgNo)){
			sql.append(" and C.ACC_ORG_NO=:orgNo ");
			params.put("orgNo",orgNo);
		}
		if(!StringUtil.isEmptyString(custName)){
			sql.append(" and b.customer_name like :custName");
			params.put("custName", "%" + custName + "%");
		}
		if(!StringUtil.isEmptyString(userName)){
			sql.append(" and b.lb_user_name like :userName");
			params.put("userName", "%" + userName + "%");
		}
		if(!StringUtil.isEmptyString(startTime)){
			sql.append(" and b.submission_time >= :startTime");
			params.put("startTime", startTime);
		}
		if(!StringUtil.isEmptyString(endTime)){
			sql.append(" and b.submission_time <= :endTime");
			params.put("endTime", endTime);
		}
		String count =  "select count(1) from (" + sql.toString() + ")";
		sql.append(" order by b.submission_time desc");
		Pagination<LcTaskDto> pager = lmProcessDao.findCustSqlPagination(LcTaskDto.class, sql.toString(), count, params, page, pageSize);
		return pager;
	}

	@Override
	public void updateAppointLbUser(String serialNumber, String userNo, String userName, String updater) throws Exception{


		//设置主流程到待处理调查状态的节点
		LmProcess process = lmProcessDao.selectSingleByProperty("serialNumber", serialNumber);
		if(process==null){
			ExceptionUtil.throwException("当前任务流水号异常");
		}
		process.setStage(Constants.LoanProcessType.SURVEY_RECEIVE);
		process.setPositionUser(userNo);
		process.setPositionUserName(userName);
		process.setUpdater(updater);
		process.setUpdateTime(DateUtil.getNowTimestamp());
		lmProcessDao.saveOrUpdate(process);

		//创建贷前调查的任务
		//直接引用营销的结论
		LrInfo lrInfo = lrInfoDao.selectSingleByProperty("serialNumber", serialNumber);
		if(lrInfo==null){
			ExceptionUtil.throwException("营销进件信息为空，查询业务流水号为：" + serialNumber);
			return ;
		}
		CompBaseDto compBase = compBaseSV.findCompBaseByCustNo(lrInfo.getCustomerNo());

		Task task = new Task();
		task.setSerNo(serialNumber);
		task.setApplicationTime(DateUtil.getServerTime());
		task.setLoanNumber(lrInfo.getRetailMoney());
		task.setLoanRate(lrInfo.getRetailRate().toString());
		task.setCustomerNo(lrInfo.getCustomerNo());
		task.setCustomerName(compBase.getCustomerName());
		task.setAssureMeans(lrInfo.getRetailGuarante());
		task.setLoanTerm(lrInfo.getRetailTerm());
		//调查报告模板类型
//		task.setBusinessNo(businessNo);
//		task.setBusinessType(lrInfo.getRetailProduct());
		task.setProductId(Integer.parseInt(lrInfo.getRetailProduct()));
		SysDic dic = sysDicDAO.queryByEnNameAndOptType(lrInfo.getRetailProduct(), SysConstants.BsDicConstant.STD_PRO_RG);
		task.setLoanUse(lrInfo.getRetailUse());
		task.setProductName(dic.getCnName());
		task.setUserNo(userNo);
		task.setUserName(userName);
//		task.setReplyWay(lrInfo.g);
		task.setIndustry(compBase.getIndustryCode());
		task.setFinaSyncFlag(SysConstants.CusFinaValidFlag.AVAIL);

		//获取当前客户财报列表   默认分析最新一期的
		List<CusFSRecord> records = cusFSRecordSV.queryByCusId(lrInfo.getCustomerNo());
		if(records==null || records.size()==0){
			ExceptionUtil.throwException("当前客户财报数据为空，请补充完财报信息");
		}
		Collections.sort(records, new Comparator<CusFSRecord>() {
			@Override
			public int compare(CusFSRecord o1, CusFSRecord o2) {
				Long date1 = 0l;
				Long date2 = 0l;
				try {
					Date d1 = DateUtil.parse(o1.getReportDate(), "yyyy-MM");
					if(d1!=null){
						date1 = d1.getTime();
					}
					Date d2 = DateUtil.parse(o2.getReportDate(), "yyyy-MM");
					if(d1!=null){
						date2 = d2.getTime();
					}
				}catch (Exception e){

				}
				return (int)(date1 - date2);
			}
		});
		//获取最新一期进行分析
		CusFSRecord record = records.get(0);
		String reportDate = record.getReportDate();
		int year = Integer.parseInt(reportDate.substring(0, 4));
		int month = Integer.parseInt(reportDate.substring(6));
		task.setYear(year);
		task.setMonth(month);
		taskSV.saveOrUpdate(task);


		//开始财务分析   这一步放到第一次贷前调查按钮上
//		finriskResultService.syncFinriskResult(serialNumber, lrInfo.getCustomerNo(), compBase.getIndustryCode());

	}

	@Override
	public void saveLbTask(String serialNumber, String customerNo, String userNo, String userName, String updater) throws Exception {
		//设置主流程到待处理调查状态的节点
		LmProcess process = lmProcessDao.selectSingleByProperty("serialNumber", serialNumber);
		if(process==null){
			//主流程为空  代表这笔业务是从贷前发起  则新建主流程任务
			process = new LmProcess();
			process.setSerialNumber(serialNumber);
			process.setCustomerNo(customerNo);
//			ExceptionUtil.throwException("当前任务流水号异常");
		}
		process.setStage(Constants.LoanProcessType.SURVEY_ING);
		process.setPositionUser(userNo);
		process.setPositionUserName(userName);
		process.setUpdater(updater);
		process.setUpdateTime(DateUtil.getNowTimestamp());
		lmProcessDao.saveOrUpdate(process);
	}

	@Override
	public Pagination<Task> queryLbTaskPager(String userNo, String orgNo, String custName, String stage, int page, int pageSize) {
		StringBuilder sql = new StringBuilder();
		Map<String, Object> params = new HashMap<>();
		sql.append("SELECT A.SERIAL_NUMBER serNo,B.USER_NAME userName,B.CUSTOMER_NO customerNo,")
				.append(" B.CUSTOMER_NAME customerName,B.LOAN_NUMBER loanNumber,B.PRODUCT_NAME productName,")
				.append(" B.LOAN_TERM loanTerm,B.BUSINESS_TYPE businessType,b.id id,b.LE_USER_NO leUserNo,")
				.append(" b.APPLICATION_TIME applicationTime,b.ASSURE_MEANS assureMeans,a.stage state")
				.append(" FROM LM_PROCESS A")
				.append(" JOIN LB_TASK B ON A.SERIAL_NUMBER=B.SER_NO")
				.append(" WHERE B.status=0");
		if(!StringUtil.isEmptyString(userNo)){
			//sql.append(" and b.USER_NO=:userNo");
			sql.append(" and b.USER_NO="+userNo);
			params.put("userNo", userNo);
		}
		if(!StringUtil.isEmptyString(orgNo)){
			//sql.append(" and b.INSTITUTION_NO=:orgNo");
			sql.append(" and b.INSTITUTION_NO="+orgNo);
			params.put("orgNo", orgNo);
		}
		if(!StringUtil.isEmptyString(stage)){
			if(Constants.LoanProcessType.SURVEY_ING==Integer.parseInt(stage)){
				//sql.append(" and (A.STAGE="+stage +"or A.STAGE=:repluseLc or A.STAGE=:repluseLe)");
				sql.append(" and (A.STAGE="+stage +" or A.STAGE="+Constants.LoanProcessType.SURVEY_REPLUSE_LC+" or A.STAGE="+Constants.LoanProcessType.SURVEY_REPLUSE_LE+")");
				//params.put("repluseLc", Constants.LoanProcessType.SURVEY_REPLUSE_LC);
				//params.put("repluseLe", Constants.LoanProcessType.SURVEY_REPLUSE_LE);
				//params.put("stage", stage);
			}else if(Constants.LoanProcessType.SURVEY_SUBMIT==Integer.parseInt(stage)){
				//sql.append(" and A.STAGE>:surveySubmit");
				sql.append(" and A.STAGE>300");
				params.put("surveySubmit", "300");
			}else{
				//sql.append(" and A.STAGE=:stage");
				sql.append(" and A.STAGE="+stage);
				//params.put("stage", stage);
			}
			//sql.append(")");
		}
		if(!StringUtil.isEmptyString(custName)){
			sql.append(" and b.CUSTOMER_NAME like '%"+custName+"%'");
			//params.put("custName", "%" + custName + "%");
		}
		String count  = "select count(*) from (" + sql + ") tb1";
		sql.append(" order by APPLICATION_TIME desc");

		return lmProcessDao.findCustSqlPagination(Task.class, sql.toString(), count, params, page, pageSize);
	}
	@Override
	public void updateLbTask(String serNo, String userNo)throws Exception {
		//设置主流程提交到到合规审查任务的节点
		LmProcess process = lmProcessDao.selectSingleByProperty("serialNumber", serNo);
		if(process==null){
			ExceptionUtil.throwException("当前任务流水号异常");
		}
		process.setStage(Constants.LoanProcessType.COMPLIANCE_PENDING);
		//目前行里面合规审查部门做审查工作的只有一个人
		//范存建  编号--》HG3001
		//GGG 根据信贷系统给我们的用户编号  给范存建指定角色。目前这个用户我自己手动新增的
		String userId = "F060222";
		User user = userSV.queryById(userNo);
		String userName = user.getUserName();
		process.setPositionUser(userNo);
		process.setPositionUserName(userName);
		process.setUpdater(userNo);
		process.setUpdateTime(DateUtil.getNowTimestamp());
		lmProcessDao.saveOrUpdate(process);

		Task lbTask = taskSV.getTaskBySerNo(serNo);
		//测算利率
		Double dgRate = resultService.syncRcResult(lbTask.getSerNo());
		LcTask lcTask = lcTaskService.selectSingleByProperty("serialNumber", serNo);
		//判断当前任务是否打回的
		//为空表示第一次提交，新建，并设置count为1
		//不为空表示被打回过，直接+1
		if(lcTask==null) {
			lcTask = new LcTask();
			lcTask.setLcCount(1);
			//获取审查 的行业分类
			String compInduInLc = this.getCompInduInLc(lbTask.getCustomerNo());
			lcTask.setIndustry(compInduInLc);
		}else{
			lcTask.setLcCount(lcTask.getLcCount() + 1);
		}

		lcTask.setLeUserNo(lbTask.getLeUserNo());
		lcTask.setSerialNumber(lbTask.getSerNo());
		lcTask.setUserNo(userId);
		lcTask.setCustomerNo(lbTask.getCustomerNo());
		lcTask.setCustomerName(lbTask.getCustomerName());
		lcTask.setDgRate(dgRate);
		lcTask.setLbUserNo(lbTask.getUserNo());
		lcTask.setLbUserName(lbTask.getUserName());

		if(null!=lbTask.getProductId())
			lcTask.setBusinessType(Integer.parseInt(lbTask.getProductId()+""));
		lcTask.setLoanNumber(lbTask.getLoanNumber());
		lcTask.setLoanTerm(lbTask.getLoanTerm());
		lcTask.setDgGrade(lbTask.getDgGrade());

		//GGG   客户在我行的存量金额
		lcTask.setStockNumber(0.0d);
		lcTask.setLoanUse(lbTask.getLoanUse());
		lcTask.setAssureMeans(lbTask.getAssureMeans());
		String loanRate = lbTask.getLoanRate();
		if(!StringUtil.isEmptyString(loanRate))
			lcTask.setLoanRate(Float.parseFloat(loanRate));
		lcTask.setSubmissionTime(DateUtil.getNowTimestamp());
		//GGG  最近一笔授信到期日
		lcTask.setCreditExpireDate(DateUtil.getLastMonthTimestamp(5));
		lcTask = lcTaskService.saveOrUpdate(lcTask);
	}

	@Override
	public void repluseCompliance(String serNo, String userNo)throws Exception {
		LmProcess process = lmProcessDao.selectSingleByProperty("serialNumber", serNo);
		LcTask lcTask = lcTaskService.queryBySerialNumber(serNo);
		if(process==null){
			ExceptionUtil.throwException("当前任务流水号异常");
		}
		process.setStage(Constants.LoanProcessType.SURVEY_REPLUSE_LC);
		process.setPositionUser(lcTask.getLbUserNo());
		process.setPositionUserName(lcTask.getLbUserName());
		process.setUpdater(userNo);
		process.setUpdateTime(DateUtil.getNowTimestamp());
		lmProcessDao.saveOrUpdate(process);
	}

	@Override
	public Pagination<LeTaskDto> queryLeTaskPaging(int stage,String userNo,String orgNo, int page, int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();
		Map<String, Object> params = new HashMap<>();
		sql.append("select a.serial_number serialNumber, b.customer_name customerName,")
				.append(" b.lb_user_name lbUserName,b.business_type businessType,")
				.append(" b.loan_number loanNumber,b.loan_term loanTerm,b.id leTaskId,")
				.append(" b.submission_time submissionTime,b.loan_rate loanRate")
				.append(" from LM_PROCESS a")
				.append("  join LE_TASK b on a.serial_number=b.serial_number")
				.append("  join BS_USER_INFO C ON b.user_no="+stage)
				.append(" where a.stage="+stage+" and a.status= "+Constants.NORMAL);
//		params.put("stage", stage);
//		params.put("status", Constants.NORMAL);
		if(!StringUtil.isEmptyString(userNo)){
			sql.append(" and b.user_no= "+userNo);
			//params.put("userNo",userNo);
		}
		if(!StringUtil.isEmptyString(orgNo)){
			sql.append(" and C.ACC_ORG_NO= "+orgNo);
			//params.put("orgNo",orgNo);
		}
		String count =  "select count(*) from (" + sql.toString() + ") tbl";
		Pagination<LeTaskDto> pager = lmProcessDao.findCustSqlPagination(LeTaskDto.class, sql.toString(), count, params, page, pageSize);
		return pager;
	}

	@Override
	public Pagination<LeTaskDto> queryLePassTaskPaging(String customerName, String userNo,String orgNo, int page, int pageSize) throws Exception {
		StringBuffer sql = new StringBuffer();
		Map<String, Object> params = new HashMap<>();
		sql.append("select a.serial_number serialNumber, b.customer_name customerName,")
				.append(" b.lb_user_name lbUserName,b.business_type businessType,")
				.append(" b.loan_number loanNumber,b.loan_term loanTerm,b.id leTaskId,")
				.append(" b.submission_time submissionTime,b.loan_rate loanRate")
				.append(" from LM_PROCESS a")
				.append(" left join LE_TASK b on a.serial_number=b.serial_number")
				.append(" left join BS_USER_INFO C ON b.user_no=c.user_id")
				.append(" where a.stage>="+Constants.LoanProcessType.EXAMINE_PASS+" and a.status= "+Constants.NORMAL);
//		params.put("stage", Constants.LoanProcessType.EXAMINE_PASS);
		//params.put("status", Constants.NORMAL);
		if(!StringUtil.isEmptyString(userNo)){
			sql.append(" and b.user_no= "+userNo);
			//params.put("userNo",userNo);
		}
		if(!StringUtil.isEmptyString(orgNo)){
			sql.append(" and C.ACC_ORG_NO= "+orgNo);
			//params.put("orgNo",orgNo);
		}
		if(!StringUtil.isEmptyString(customerName)){
			sql.append(" and b.CUSTOMER_NAME like'%"+customerName+"%'");
//			if (!StringUtil.isEmptyString(custName)) {
//				sql.append(" and b.customer_name like '%"+custName+"%'");
//				//	params.put("custName", "%" + custName + "%");
//			}
			//params.put("customerName","%"+customerName+"%");
		}
		String count =  "select count(*) from (" + sql.toString() + ") tb1";
		Pagination<LeTaskDto> pager = lmProcessDao.findCustSqlPagination(LeTaskDto.class, sql.toString(), count, params, page, pageSize);
		return pager;
	}




	@Override
	public void updateLeTask(String serialNumber,String distributionUserNo, String userNo) throws Exception {
		//获取主流程任务
		LmProcess process = lmProcessDao.selectSingleByProperty("serialNumber", serialNumber);
		//获取信贷审查任务信息
		LeTask leTask = leTaskDao.selectSingleByProperty("serialNumber",serialNumber);
		if(process==null || leTask == null){
			ExceptionUtil.throwException("当前任务流水号异常");
		}
		process.setStage(Constants.LoanProcessType.EXAMINE_PENDING);
		User user = userSV.queryById(distributionUserNo);
		process.setPositionUser(user.getUserId());
		process.setPositionUserName(user.getUserName());
		process.setUpdater(userNo);
		process.setUpdateTime(DateUtil.getNowTimestamp());
		lmProcessDao.saveOrUpdate(process);
		// 信贷审查任务设置当前审查经理
		leTask.setUserNo(distributionUserNo);
		leTask.setUpdater(userNo);
		leTask.setUpdateTime(DateUtil.getNowTimestamp());
		leTask = leTaskDao.saveOrUpdate(leTask);
		//初始化 审查细项信息
		initLeContent(leTask, distributionUserNo);
	}

	@Override
	public void repluseExamine(String serNo, String userNo) throws Exception {
		LmProcess process = lmProcessDao.selectSingleByProperty("serialNumber", serNo);
		LeTask leTask = leTaskDao.selectSingleByProperty("serialNumber",serNo);
		if(process==null || leTask == null){
			ExceptionUtil.throwException("当前任务流水号异常");
		}
		process.setStage(Constants.LoanProcessType.SURVEY_REPLUSE_LE);
		process.setPositionUser(leTask.getLbUserNo());
		process.setPositionUserName(leTask.getLbUserName());
		process.setUpdater(userNo);
		process.setUpdateTime(DateUtil.getNowTimestamp());
		lmProcessDao.saveOrUpdate(process);
	}

	@Override
	public void submitToExamine(String serialNumber, String userNo) throws Exception {
		//获取贷款主表信息
		LmProcess process = lmProcessDao.selectSingleByProperty("serialNumber", serialNumber);
		LcTask lcTask = lcTaskService.queryBySerialNumber(serialNumber);
		if(process==null || lcTask == null){
			ExceptionUtil.throwException("当前任务流水号异常");
		}
		lcTask.setUpdateTime(DateUtil.getNowTimestamp());
		lcTaskService.saveOrUpdate(lcTask);

		process.setPositionUser(userNo);
//		process.setStage(Constants.LoanProcessType.EXAMINE_DISTRIBUTION);
		process.setStage(Constants.LoanProcessType.EXAMINE_PENDING);

		process.setUpdater(userNo);
		process.setUpdateTime(DateUtil.getNowTimestamp());
		lmProcessDao.saveOrUpdate(process);

		//获取信贷审查任务
		LeTask leTask = leTaskDao.selectSingleByProperty("serialNumber", serialNumber);
		if(leTask == null) {
			//创建信贷审查任务
			leTask = new LeTask();
			leTask.setSerialNumber(lcTask.getSerialNumber());
			//leTask.setUserNo(userNo);//这里是贷前审查的用户，之后需要更改
			leTask.setUserNo(lcTask.getLeUserNo());
//			leTask.setUserNo("F063041");//张燕的用户号
			leTask.setCustomerNo(lcTask.getCustomerNo());
			leTask.setCustomerName(lcTask.getCustomerName());
			leTask.setLbUserNo(lcTask.getLbUserNo());
			leTask.setLbUserName(lcTask.getLbUserName());
			leTask.setBusinessType(lcTask.getBusinessType());
			leTask.setLoanNumber(lcTask.getLoanNumber());
			leTask.setLoanTerm(lcTask.getLoanTerm());
			leTask.setLoanUse(lcTask.getLoanUse());
			leTask.setAssureMeans(lcTask.getAssureMeans());
			leTask.setLoanRate(lcTask.getLoanRate());
			leTask.setSubmissionTime(DateUtil.getNowTimestamp());
			leTask.setReviewCount(1);
			leTask.setCreater(userNo);
		}else{//如果存在
			leTask.setSerialNumber(lcTask.getSerialNumber());
			leTask.setUserNo(lcTask.getLeUserNo());
			leTask.setCustomerNo(lcTask.getCustomerNo());
			leTask.setCustomerName(lcTask.getCustomerName());
			leTask.setLbUserNo(lcTask.getLbUserNo());
			leTask.setLbUserName(lcTask.getLbUserName());
			leTask.setBusinessType(lcTask.getBusinessType());
			leTask.setLoanNumber(lcTask.getLoanNumber());
			leTask.setLoanTerm(lcTask.getLoanTerm());
			leTask.setLoanUse(lcTask.getLoanUse());
			leTask.setAssureMeans(lcTask.getAssureMeans());
			leTask.setLoanRate(lcTask.getLoanRate());
			leTask.setSubmissionTime(DateUtil.getNowTimestamp());
		}

		leTask.setDgRate(lcTask.getDgRate());
		leTask.setDgGrade(lcTask.getDgGrade());
		leTask.setUpdater(userNo);
		leTask.setUpdateTime(DateUtil.getNowTimestamp());
		leTask = leTaskDao.saveOrUpdate(leTask);
	}

	@Override
	public String getCompInduInLc(String custNo) throws Exception {
		CompBaseDto compBase = compBaseSV.findCompBaseByCustNo(custNo);
		//获取客户大类行业
		String majorIndu = compBase.getMajorIndu();
		if(StringUtil.isEmptyString(majorIndu)){
			//默认使用工业制造业
			return Constants.LC_RISK_INDU.MANUF;
		}
		//对2017国标行业进行整体分类
		List<String> induStr = Arrays.asList(new String[]{"B", "C", "D", "I"});
		List<String> busiStr = Arrays.asList(new String[]{"F", "H", "R", "R", "J"});
		List<String> estateStr = Arrays.asList(new String[]{"E", "K"});
		List<String> pubStr = Arrays.asList(new String[]{"A","L", "O", "T","G", "M", "N", "P", "Q", "S"});

		if(induStr.contains(majorIndu)){				//制造业工业
			return Constants.LC_RISK_INDU.MANUF;
		}else if(busiStr.contains(majorIndu)){			//商业
			return Constants.LC_RISK_INDU.BUSINESS;
		}else if(estateStr.contains(majorIndu)){		//房地产
			return Constants.LC_RISK_INDU.BUILD;
		}else if(pubStr.contains(majorIndu)){			//其他
			return Constants.LC_RISK_INDU.OTHER;
		}
		return Constants.LC_RISK_INDU.MANUF;
	}

	/**
	 *@Description 初始化审查细项信息
	 *@param leTask 审查任务
	 *@param userNo 审查人编号
	 * @author xyh
	 */
	private void initLeContent(LeTask leTask,String userNo){
		if(leTask.getReviewCount() == 1){
			//查询所有审查要点项
			List<LeExaminePoint> pointList = leExaminePointDao.getAll();
			for(LeExaminePoint point : pointList){
				LeExamineContent entity = new LeExamineContent();
				entity.setLeTaskId(leTask.getId());
				entity.setExaminePointId(point.getId());
				entity.setExamineType(point.getExamineType());
				entity.setExamineName(point.getExamineName());
				entity.setExamineCount(1);//直接认定是1
				entity.setIsPass(0);//默认通过
				entity.setCreater(userNo);
				entity.setUpdater(userNo);
				entity.setUpdateTime(DateUtil.getNowTimestamp());
				leExamineContentDao.saveOrUpdate(entity);
			}
		}else{
			//获取所有上一次的审查细项
			Map<String,Object> params = new HashMap<>();
			params.put("leTaskId",leTask.getId());
			params.put("examineCount",leTask.getReviewCount() - 1);
			List<LeExamineContent> contentList = leExamineContentDao.selectByProperty(params," examineType asc ");
			//同步：如果上次是不通过的，那么设置处理状态为未处理，其他不管
			for(LeExamineContent content : contentList){
				LeExamineContent entity = new LeExamineContent();
				entity.setLeTaskId(content.getLeTaskId());
				entity.setExaminePointId(content.getExaminePointId());
				entity.setExamineType(content.getExamineType());
				entity.setExamineName(content.getExamineName());
				entity.setExamineCount(leTask.getReviewCount());
				entity.setContent(content.getContent());
				entity.setIsPass(content.getIsPass());
				if(Constants.LE_REVIEW_CONTENT_STATE.IS_UN_PASS == content.getIsPass()){
					//如果上次是不通过打回的
					entity.setState(Constants.LE_REVIEW_CONTENT_STATE.NOT_SOLVE);
				}
				entity.setCreater(userNo);
				entity.setUpdater(userNo);
				entity.setUpdateTime(DateUtil.getNowTimestamp());
				leExamineContentDao.saveOrUpdate(entity);
			}
		}
	}
}
