package com.beawan.corporateloan.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.corporateloan.dao.LcDocDao;
import com.beawan.corporateloan.entity.LcDoc;
import com.beawan.corporateloan.service.LcDocService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.corporateloan.entity.LcDoc;

/**
 * @author yzj
 */
@Service("lcDocService")
public class LcDocServiceImpl extends BaseServiceImpl<LcDoc> implements LcDocService{
    /**
    * 注入DAO
    */
    @Resource(name = "lcDocDao")
    public void setDao(BaseDao<LcDoc> dao) {
        super.setDao(dao);
    }
    @Resource
    public LcDocDao lcDocDao;

    @Override
    public String saveDoc(Integer docId, String docInfo, String noHtml) {

        LcDoc doc = lcDocDao.findByPrimaryKey(docId);
        doc.setDocInfo(docInfo);
        doc.setNoHtmlDoc(noHtml);

        lcDocDao.saveOrUpdate(doc);
        return doc.getType();
    }
}
