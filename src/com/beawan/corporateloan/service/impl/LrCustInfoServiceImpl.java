package com.beawan.corporateloan.service.impl;

import java.util.*;

import javax.annotation.Resource;

import com.beawan.corporateloan.dao.LrRecordListDao;
import com.beawan.corporateloan.dto.CustNumDto;
import com.beawan.corporateloan.dto.LrAreaStatDto;
import com.beawan.corporateloan.dto.LrCoverageDto;
import com.beawan.corporateloan.dto.LrCustClassDto;
import com.beawan.corporateloan.entity.LrRecordList;
import com.beawan.model.service.AdmitResultService;
import com.beawan.model.service.AdmitService;
import com.beawan.qcc.service.SynchronizationQCCService;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.platform.util.DateUtil;
import com.platform.util.ExceptionUtil;
import com.platform.util.RgnshUtils;
import com.platform.util.StringUtil;
import org.springframework.stereotype.Service;

import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.core.Pagination;
import com.beawan.corporateloan.dao.LrCustInfoDao;
import com.beawan.corporateloan.dto.LrCustDto;
import com.beawan.corporateloan.dto.LrCustSubClassDto;
import com.beawan.corporateloan.entity.LrCustInfo;
import com.beawan.corporateloan.service.LrCustInfoService;

/**
 * @author yzj
 */
@Service("lrCustInfoService")
public class LrCustInfoServiceImpl extends BaseServiceImpl<LrCustInfo> implements LrCustInfoService {
	/**
	 * 注入DAO
	 */
	@Resource(name = "lrCustInfoDao")
	public void setDao(BaseDao<LrCustInfo> dao) {
		super.setDao(dao);
	}

	@Resource
	public LrCustInfoDao lrCustInfoDao;
	@Resource
	public LrRecordListDao lrRecordListDao;
	@Resource
	public SynchronizationQCCService qccService;
	@Resource
	private AdmitResultService admitResultService;
	@Resource
	private AdmitService admitSV;
	@Resource
	protected ICompBaseSV compBaseSV;

	@Override
	public Pagination<LrCustDto> queryPubpoolPaging(String custNo, String custName,
													int page, int pageSize) {
		StringBuilder sql = new StringBuilder();
		Map<String, Object> params = new HashMap<>();
		sql.append("select b.customer_no as customerNo,b.customer_name as customerName,")
				.append(" b.customer_type as customerType,")
				.append(" a.cust_addr as custAddr,a.create_time as createTime,")
				.append(" a.CUSTOMER_CLASS as customerClass ")
				.append(" from LR_CUST_INFO a")
				.append(" inner join CUS_BASE b")
				.append(" on a.customer_no=b.customer_no")
				.append(" where a.status=0");
		sql.append(" and a.CUSTOMER_LR_TYPE = "+Constants.LrCustType.PUB_POOL_CUST);
		//params.put("customerLrType", Constants.LrCustType.PUB_POOL_CUST);
		// if(!StringUtil.isEmptyString(userName)){
		//            sql.append(" and c.user_name like '%"+userName+"%'");
		//          //  params.put("userName", "%"+ userName + "%");
		//        }
		if (!StringUtil.isEmptyString(custNo)) {
			sql.append(" and a.customer_no like '%"+custNo+"%'");
			//	params.put("custNo", "%" + custNo + "%");
		}
		if (!StringUtil.isEmptyString(custName)) {
			sql.append(" and b.customer_name like '%"+custName+"%'");
			//params.put("custName", "%" + custName + "%");
		}
		String count = "select count(*) from (" + sql + ") tbl";
		sql.append(" order by a.CREATE_TIME DESC");
		Pagination<LrCustDto> pager = lrCustInfoDao.findCustSqlPagination(LrCustDto.class, sql.toString(), count,
				params, page, pageSize);
		return pager;
	}

	@Override
	public Pagination<LrCustDto> queryBlacklistPaging(String custNo, String custName, int page, int pageSize) {
		StringBuilder sql = new StringBuilder();
		Map<String, Object> params = new HashMap<>();
		sql.append("select b.customer_no as customerNo,b.customer_name as customerName,")
				.append(" b.customer_type as customerType,")
				.append(" a.CUST_ADDR as custAddr,a.create_time as createTime")
				.append(" from LR_CUST_INFO a")
				.append(" inner join CUS_BASE b")
				.append(" on a.customer_no=b.customer_no")
				.append(" where a.status=0");
		sql.append(" and a.CUSTOMER_LR_TYPE = "+Constants.LrCustType.BLACK_CUST);
		//params.put("customerLrType", Constants.LrCustType.BLACK_CUST);
		if (!StringUtil.isEmptyString(custNo)) {
			//if (!StringUtil.isEmptyString(custName)) {
			//			sql.append(" and b.customer_name like '%"+custName+"%'");
			//		//	params.put("custName", "%" + custName + "%");
			//		}
			sql.append(" and a.customer_no like '%"+custNo+"'");
			//params.put("custNo", "%" + custNo + "%");
		}
		if (!StringUtil.isEmptyString(custName)) {
			sql.append(" and b.customer_name like '%"+custName+"'");
			//	params.put("custName", "%" + custName + "%");
		}
		String count = "select count(*) from (" + sql + ")tbl";
		sql.append(" order by a.CREATE_TIME DESC");
		Pagination<LrCustDto> pager = lrCustInfoDao.findCustSqlPagination(LrCustDto.class, sql.toString(), count,
				params, page, pageSize);
		return pager;
	}

	@Override
	public Pagination<LrCustDto> queryRetailIngCustPaging(String userNo,String orgNo, String userName,
														  String custName,String customerClss, int page, int pageSize) {
		StringBuilder sql = new StringBuilder();
		Map<String, Object> params = new HashMap<>();
		sql.append("select b.customer_no as customerNo,b.customer_name as customerName,")
				.append(" b.customer_type as customerType,a.cust_addr as custAddr,")
				.append(" a.expire_time expireDate,a.expire_status expireStatus,a.create_time as createTime,")
				.append(" a.CUSTOMER_CLASS as customerClass ")
				.append(" from LR_CUST_INFO a")
				.append(" inner join CUS_BASE b on a.customer_no=b.customer_no")
				.append(" left join BS_USER_info c on c.user_id = a.HOLD_USER_NO")
				.append(" where a.status=0");
		sql.append(" and a.CUSTOMER_LR_TYPE = "+Constants.LrCustType.RETAIL_CUST);
		//sql.append("and customerLrType"+Constants.LrCustType.RETAIL_CUST);
		//params.put("customerLrType", Constants.LrCustType.RETAIL_CUST);
		if(!StringUtil.isEmptyString(userNo)){
			sql.append(" and a.HOLD_USER_NO="+userNo);
			//  params.put("userNo", userNo);
		}
		if(!StringUtil.isEmptyString(orgNo)){
			sql.append(" and C.ACC_ORG_NO= "+orgNo);
			//params.put("orgNo",orgNo);
		}
		//sql.append(" and b.CUSTOMER_NAME like '%"+custName+"%'");
		if(!StringUtil.isEmptyString(userName)){
			sql.append(" and c.user_name like '%"+userName+"%'");
			//  params.put("userName", "%"+ userName + "%");
		}
		if (!StringUtil.isEmptyString(custName)) {
			sql.append(" and b.customer_name like '%"+custName+"%'");
			//	params.put("custName", "%" + custName + "%");
		}
		if(!StringUtil.isEmptyString(customerClss)){
			sql.append(" and a.CUSTOMER_CLASS = "+customerClss);
			//params.put("customerClss", customerClss);
		}
		String count = "select count(*) from (" + sql + ") tbl"  ;
		sql.append(" order by a.EXPIRE_STATUS desc,a.EXPIRE_TIME");
		Pagination<LrCustDto> pager = lrCustInfoDao.findCustSqlPagination(LrCustDto.class, sql.toString(), count,
				params, page, pageSize);
		return pager;
	}

	@Override
	public LrCustInfo updateCustMoveRetailIng(String holdUserNo, String customerNo, String dealUserNo, Integer retailDay) throws Exception {
		//设置客户营销经理
		//获取到这个客户
		LrCustInfo lrCustInfo = lrCustInfoDao.selectSingleByProperty("customerNo",customerNo);
		if(lrCustInfo == null){
			ExceptionUtil.throwException("客户不存在");
		}
		if(!Constants.LrCustType.PUB_POOL_CUST.equals(lrCustInfo.getCustomerLrType()) && holdUserNo.equals(dealUserNo)){
			ExceptionUtil.throwException("客户不是公共池客户，不可领取");
		}
		//领取客户--设置为营销客户，设置管户客户经理，设置过期时间
		lrCustInfo.setCustomerLrType(Constants.LrCustType.RETAIL_CUST);
		//先定为90天，可以配置
		if(RgnshUtils.isEmptyInteger(retailDay)){
			lrCustInfo.setExpireTime(DateUtil.getAddDayTimestamp(90));
		}else{
			lrCustInfo.setExpireTime(DateUtil.getAddDayTimestamp(retailDay));
		}
		lrCustInfo.setExpireStatus(Constants.RetailReal.NOT_SUCC);
		lrCustInfo.setHoldUserNo(holdUserNo);
		lrCustInfo.setUpdater(dealUserNo);
		lrCustInfo.setUpdateTime(DateUtil.getNowTimestamp());
		lrCustInfoDao.saveOrUpdate(lrCustInfo);

		//=========================添加营销记录表=============
		LrRecordList lrRecordList = new LrRecordList();
		lrRecordList.setCustomerNo(customerNo);
		lrRecordList.setCreater(dealUserNo);
		lrRecordList.setUpdater(dealUserNo);
		lrRecordList.setUpdateTime(DateUtil.getNowTimestamp());
		lrRecordList.setUserNo(holdUserNo);
		if(dealUserNo.equals(holdUserNo)){//自己领取营销客户
			lrRecordList.setDealType(Constants.LrRecordType.MARKET_CUST_DISTRBUTION_S);
			lrRecordList.setRecordRemark(dealUserNo + "将客户领取到自己名下，成功");
		}else {//管理员分配营销客户
			lrRecordList.setDealType(Constants.LrRecordType.MARKET_CUST_DISTRBUTION_P);
			lrRecordList.setRecordRemark(dealUserNo + "将客户分配给"+ holdUserNo +"名下，成功");
		}
		lrRecordListDao.saveOrUpdate(lrRecordList);
		return lrCustInfo;
	}

	@Override
	public List<LrCustClassDto> queryCustClass(String dataRange, String startTime, String endTime) {
		StringBuilder sql = new StringBuilder();
		Map<String,Object> params = new HashMap<>();
//		select  A.CUSTOMER_CLASS,COUNT(1) num  from GDTCESYS.LR_CUST_INFO A
//		WHERE A.CUSTOMER_LR_TYPE !='0' AND A.UPDATE_TIME >= '2020-07-01 00:00:00' AND  A.UPDATE_TIME <= '2020-08-01 00:00:00'
//		GROUP BY A.CUSTOMER_CLASS
		sql.append("select A.CUSTOMER_CLASS customerClassCode,COUNT(1) num  from GDTCESYS.LR_CUST_INFO A")
				.append(" WHERE 1=1");
		if(!StringUtil.isEmptyString(dataRange) && "2".equals(dataRange)){
			sql.append(" AND A.CUSTOMER_LR_TYPE !='0'");
		}
		if(!StringUtil.isEmptyString(startTime)){
			sql.append(" AND A.UPDATE_TIME >= :startTime");
			params.put("startTime", startTime);
		}
		if(!StringUtil.isEmptyString(endTime)){
			sql.append(" AND A.UPDATE_TIME <= :endTime");
			params.put("endTime", endTime);
		}
		sql.append(" GROUP BY A.CUSTOMER_CLASS");
		List<LrCustClassDto> custClassDtos = lrCustInfoDao.findCustListBySql(LrCustClassDto.class, sql, params);
		return custClassDtos;
	}

	@Override
	public Pagination<LrCoverageDto> queryCoverage(String userName, String startTime, String endTime, int page, int pageSize) throws  Exception {
		Pagination<LrCoverageDto> pager = lrCustInfoDao
				.getCustCoverage(userName, startTime, endTime, page, pageSize);
		return pager;

		//		//获取营销客户经理总的管户客户数
//		Pagination<CoverageDto> holdCustPager = lrCustInfoDao.getHoldCustNumPaging(page, pageSize);
//		List<CoverageDto> holdCustList = holdCustPager.getItems();
//		//获取营销经理在startTime和endTime之间拜访过的客户数量
//		Pagination<CoverageDto> visitCustPager = lrCustInfoDao.getVisitCustNumPaging(startTime, endTime, page, pageSize);
//		List<CoverageDto> visitCustList = visitCustPager.getItems();
//
//		if(holdCustPager.getRowsCount()!=visitCustPager.getRowsCount()){
//			ExceptionUtil.throwException("分析营销客户覆盖率数据异常，请重试");
//		}
//		if(holdCustList==null || visitCustList==null){
//			ExceptionUtil.throwException("分析营销客户覆盖率数据为空，请重试");
//		}
//		//合并数据   数据放在hold里面
//		for(int i = 0; i < holdCustList.size(); i++){
//			CoverageDto hold = holdCustList.get(i);
//			for(CoverageDto visit : visitCustList){
//				if(hold.getUserId().equals(visit.getUserId())){
//					hold.setVisitNumber(visit.getVisitNumber());
//					String coverAge = "";
//					if(hold.getCustNum()==0){
//						coverAge = "0.00";
//					}else{
//						Double value = new BigDecimal(visit.getVisitNumber() * 100.0d / hold.getCustNum())
//								.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
//						coverAge = value.toString();
//					}
//					int rank = (page - 1) * pageSize + i + 1;
//					hold.setRank(rank);
//					hold.setCoverageRate(coverAge);
//				}
//			}
//		}
//
//		return holdCustPager;
	}

	@Override
	public Pagination<LrCoverageDto> queryConversion(String userName, String startTime, String endTime, int page, int pageSize) throws Exception {
		return lrCustInfoDao.getCustConversion(userName, startTime, endTime, page, pageSize);
	}

	@Override
	public Pagination<LrAreaStatDto> queryAreaStat(String userNo, String startTime, String endTime, int page, int pageSize) {
		return lrCustInfoDao.getAreaStatData(userNo, startTime, endTime, page, pageSize);
	}

	@Override
	public Pagination<LrCustSubClassDto> querySubCustClass(int page, int pageSize,String orgName, String startTime, String endTime) {
		StringBuilder sql = new StringBuilder();
		Map<String,Object> params = new HashMap<>();
		sql.append("SELECT A.ORGANNO,A.ORGANSHORTFORM,")
				.append(" SUM(CASE WHEN CUSTOMER_CLASS = '0' THEN NUMS ELSE 0 END) AS SUM1,")//无效客户
				.append(" SUM(CASE WHEN CUSTOMER_CLASS = '1' THEN NUMS ELSE 0 END) AS SUM2,")//潜在客户
				.append(" SUM(CASE WHEN CUSTOMER_CLASS = '2' THEN NUMS ELSE 0 END) AS SUM3,")//机会客户
				.append(" SUM(CASE WHEN CUSTOMER_CLASS = '3' THEN NUMS ELSE 0 END) AS SUM4,")//意向客户
				.append(" SUM(CASE WHEN CUSTOMER_CLASS = '4' THEN NUMS ELSE 0 END) AS SUM5 ")//成功客户
				.append(" FROM BS_ORG A ")
				.append(" LEFT JOIN BS_USER_INFO B ON A.ORGANNO = B.ACC_ORG_NO ")
				.append(" LEFT JOIN (")
				.append(" SELECT HOLD_USER_NO,CUSTOMER_CLASS,COUNT(1) NUMS FROM LR_CUST_INFO")
				.append(" WHERE CUSTOMER_LR_TYPE = '1'");//只统计营销中客户
		if(!StringUtil.isEmptyString(startTime)){
			sql.append(" AND A.UPDATE_TIME >= :startTime");
			params.put("startTime", startTime);
		}
		if(!StringUtil.isEmptyString(endTime)){
			sql.append(" AND A.UPDATE_TIME <= :endTime");
			params.put("endTime", endTime);
		}
		sql.append(" GROUP BY CUSTOMER_CLASS,HOLD_USER_NO")
				.append(" ) C ON B.USER_ID = C.HOLD_USER_NO")
				.append(" WHERE")
				.append(" A.ORGANSHORTFORM IS NOT NULL")

				.append(" AND A.ORGANLEVEL > 3");//等级为3是总行
		if(!StringUtil.isEmptyString(orgName)){
			sql.append(" AND A.ORGANSHORTFORM LIKE:ORGANSHORTFORM");
			params.put("ORGANSHORTFORM", "%"+orgName+"%");
		}
		sql.append(" GROUP BY A.ORGANNO,A.ORGANSHORTFORM");
		String count = "select count(1) from (" + sql + ")";
		Pagination<LrCustSubClassDto> lrCustSubClassDtos = lrCustInfoDao.findCustSqlPagination(LrCustSubClassDto.class, sql.toString(),count, params, page, pageSize);
		return lrCustSubClassDtos;
	}

	@Override
	public List<LrCustSubClassDto> queryManageInSubCustClass(String organNo, String startTime, String endTime) {
		StringBuilder sql = new StringBuilder();
		Map<String,Object> params = new HashMap<>();
		sql.append("SELECT A.ORGANNO,A.ORGANSHORTFORM,B.USER_ID as userId,B.USER_NAME as userName,")
				.append(" SUM(CASE WHEN CUSTOMER_CLASS = '0' THEN NUMS ELSE 0 END) AS SUM1,")//无效客户
				.append(" SUM(CASE WHEN CUSTOMER_CLASS = '1' THEN NUMS ELSE 0 END) AS SUM2,")//潜在客户
				.append(" SUM(CASE WHEN CUSTOMER_CLASS = '2' THEN NUMS ELSE 0 END) AS SUM3,")//机会客户
				.append(" SUM(CASE WHEN CUSTOMER_CLASS = '3' THEN NUMS ELSE 0 END) AS SUM4,")//意向客户
				.append(" SUM(CASE WHEN CUSTOMER_CLASS = '4' THEN NUMS ELSE 0 END) AS SUM5 ")//成功客户
				.append(" FROM BS_ORG A ")
				.append(" LEFT JOIN BS_USER_INFO B ON A.ORGANNO = B.ACC_ORG_NO ")
				.append(" LEFT JOIN (")
				.append(" SELECT HOLD_USER_NO,CUSTOMER_CLASS,COUNT(1) NUMS FROM LR_CUST_INFO")
				.append(" WHERE CUSTOMER_LR_TYPE = '1'");//只统计营销中客户
		if(!StringUtil.isEmptyString(startTime)){
			sql.append(" AND A.UPDATE_TIME >= :startTime");
			params.put("startTime", startTime);
		}
		if(!StringUtil.isEmptyString(endTime)){
			sql.append(" AND A.UPDATE_TIME <= :endTime");
			params.put("endTime", endTime);
		}
		sql.append(" GROUP BY CUSTOMER_CLASS,HOLD_USER_NO")
				.append(" ) C ON B.USER_ID = C.HOLD_USER_NO")
				.append(" WHERE")
				.append(" A.ORGANSHORTFORM IS NOT NULL")
				.append(" AND A.ORGANLEVEL > 3");//等级为3是总行

		if(!StringUtil.isEmptyString(organNo)){
			//会出现有的支行没有客户经理的情况，所以设置一个不可以为null
			//直接join也可以解决这个问题
			sql.append(" AND A.ORGANNO =:ORGANNO AND B.USER_ID IS NOT NULL");
			params.put("ORGANNO", organNo);
		}
		sql.append(" GROUP BY A.ORGANNO,A.ORGANSHORTFORM,B.USER_ID,B.USER_NAME");
		List<LrCustSubClassDto> lrCustSubClassDtos = lrCustInfoDao.findCustListBySql(LrCustSubClassDto.class, sql.toString(), params);
		return lrCustSubClassDtos;
	}

	@Override
	public Pagination<LrCustDto> queryDistributionPaging(String custNo, String custName, int page, int pageSize) {
		StringBuilder sql = new StringBuilder();
		Map<String, Object> params = new HashMap<>();
		sql.append("select b.customer_no as customerNo,b.customer_name as customerName,")
				.append(" b.customer_type as customerType,")
				.append(" a.cust_addr as custAddr,a.create_time as createTime,")
				.append(" a.CUSTOMER_CLASS as customerClass, ")
				.append("  c.user_name as retailUser ")
				.append(" from LR_CUST_INFO a")
				.append(" inner join CUS_BASE b on a.customer_no=b.customer_no")
				.append(" inner join BS_USER_info c on c.user_id = a.HOLD_USER_NO")
				.append(" where a.status=0");
		sql.append(" and a.CUSTOMER_LR_TYPE in("+ Constants.LrCustType.PUB_POOL_CUST+Constants.LrCustType.RETAIL_CUST+")");
		//查询的是公共池和营销中的客户
//		List<String>customerLrTypes = new ArrayList<String>();
//		customerLrTypes.add(Constants.LrCustType.PUB_POOL_CUST);
//		customerLrTypes.add(Constants.LrCustType.RETAIL_CUST);
//		params.put("customerLrType", customerLrTypes);
//		if (!StringUtil.isEmptyString(custName)) {
//			sql.append(" and b.customer_name like '%"+custName+"%'");
//			//	params.put("custName", "%" + custName + "%");
//		}
		if (!StringUtil.isEmptyString(custNo)) {
			sql.append(" and a.customer_no like '%"+custNo+"%'");
			//params.put("custNo", "%" + custNo + "%");
		}
		if (!StringUtil.isEmptyString(custName)) {
			sql.append(" and b.customer_name like '%"+custName+"%'");
			//params.put("custName", "%" + custName + "%");
		}
		String count = "select count(*) from (" + sql + ") tbl";
		sql.append(" order by a.CREATE_TIME DESC");
		Pagination<LrCustDto> pager = lrCustInfoDao.findCustSqlPagination(LrCustDto.class, sql.toString(), count,
				params, page, pageSize);
		return pager;
	}

	@Override
	public CustNumDto statCustNumByType(String userNo, String orgNo) {
		CustNumDto result = new CustNumDto();
		//统计各个客户分类下的客户数量
		Map<String, Object> params = new HashMap<>();
		//params.put("status", Constants.NORMAL);

		String userSelect = "";
		if(!StringUtil.isEmptyString(userNo)){
			userSelect =" and holdUserNo=userNo ";
			params.put("userNo", userNo);
		}

		//
		long allNum = lrCustInfoDao.selectCount("status="+Constants.NORMAL + userSelect);
		result.setAllNum(allNum);

		//params.put("clazz", Constants.CustClassify.INVALID);
		//long invalidNum = lrCustInfoDao.selectCount("status=status and customerClass=clazz " + userSelect, params);
		long invalidNum = lrCustInfoDao.selectCount("status="+Constants.NORMAL +" and "+"customerClass="+Constants.CustClassify.INVALID+ userSelect);
		result.setInvalidNum(invalidNum);

		//params.put("clazz", Constants.CustClassify.LEADS);
		//long leadNum = lrCustInfoDao.selectCount("status=status and customerClass=clazz " + userSelect, params);
		long leadNum = lrCustInfoDao.selectCount("status="+Constants.NORMAL +" and "+"customerClass="+Constants.CustClassify.LEADS+ userSelect);
		result.setLeadNum(leadNum);

		//params.put("clazz", Constants.CustClassify.OPPORTUNITY);
		//long opporNum = lrCustInfoDao.selectCount("status=status and customerClass=clazz " + userSelect, params);
		long opporNum = lrCustInfoDao.selectCount("status="+Constants.NORMAL +" and "+"customerClass="+Constants.CustClassify.OPPORTUNITY+ userSelect);
		result.setOpporNum(opporNum);

		//params.put("clazz", Constants.CustClassify.INTENDED);
		//long intendenNum = lrCustInfoDao.selectCount("status=status and customerClass=clazz" + userSelect, params);
		long intendenNum = lrCustInfoDao.selectCount("status="+Constants.NORMAL +" and "+"customerClass="+Constants.CustClassify.INTENDED+ userSelect);
		result.setIntendenNum(intendenNum);

		//params.put("clazz", Constants.CustClassify.SUCCESS);
		//long succNum = lrCustInfoDao.selectCount("status=status and customerClass=clazz " + userSelect, params);
		long succNum = lrCustInfoDao.selectCount("status="+Constants.NORMAL +" and "+"customerClass="+Constants.CustClassify.SUCCESS+ userSelect);
		result.setSuccNum(succNum);

		return result;
	}


}
