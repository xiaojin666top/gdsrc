package com.beawan.corporateloan.service.impl;

import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.core.Pagination;
import com.beawan.corporateloan.dao.LcTaskDao;
import com.beawan.corporateloan.dto.*;
import com.beawan.corporateloan.entity.LcTask;
import com.beawan.corporateloan.service.LcReviewPointService;
import com.beawan.corporateloan.service.LcTaskService;
import com.platform.util.DateUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.time.Year;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author yzj
 */
@Service("lcTaskService")
public class LcTaskServiceImpl extends BaseServiceImpl<LcTask> implements LcTaskService {
    /**
     * 注入DAO
     */
    @Resource(name = "lcTaskDao")
    public void setDao(BaseDao<LcTask> dao) {
        super.setDao(dao);
    }

    @Resource
    public LcTaskDao lcTaskDao;

    @Resource
    public LcReviewPointService lcReviewPointService;

    @Override
    public Pagination<LcTaskStatDto> queryTaskStatPaging(String userNo, String startTime, String endTime, int page, int pageSize) {
        StringBuilder sql = new StringBuilder();
        Map<String, Object> params = new HashMap<>();
        sql.append("SELECT lt.ID lcTaskId, lt.CUSTOMER_NAME customerName, lt.BUSINESS_TYPE businessType, lt.LOAN_TERM loanTerm,")
                .append(" lt.LOAN_NUMBER loanNumber, lt.STOCK_NUMBER stockNumber, lt.LOAN_USE loanUse,")
                .append(" lt.ASSURE_MEANS assureMeans, lt.LOAN_RATE loanRate, lt.SUBMISSION_TIME submissionTime,")
                .append(" lt.CREDIT_EXPIRE_DATE creditExpireDate, lt.GUARANTOR guarantor, lt.LB_USER_NAME lbUserName")
                .append(" FROM LC_TASK lt")
                .append(" LEFT JOIN LM_PROCESS lp ON lp.SERIAL_NUMBER = lt.SERIAL_NUMBER")
                .append(" WHERE lp.STAGE > :stage AND lt.STATUS = :status");
        params.put("stage", Constants.LoanProcessType.COMPLIANCE_PENDING);
        params.put("status", Constants.NORMAL);
        

        if (userNo != null && !"".equals(userNo)) {
            sql.append(" AND lt.USER_NO = :userNo");
            params.put("userNo", userNo);
        }
        
        if (startTime != null && !"".equals(startTime)) {
            sql.append(" AND lt.UPDATE_TIME >= :startTime");
            params.put("startTime", startTime);
        }
        if (endTime != null && !"".equals(endTime)) {
            sql.append(" AND lt.UPDATE_TIME <= :endTime");
            params.put("endTime", endTime);
        }
        String count = "select count(1) from (" + sql + ")";
        return lcTaskDao.findCustSqlPagination(LcTaskStatDto.class, sql.toString(), count, params, page, pageSize);
    }

    @Override
    public Pagination<LcTaskRiskDto> queryTaskRiskPaging(Integer id, String userNo, int page, int pageSize) {
        Map<String, Object> params = new HashMap<>();
        StringBuilder sql = new StringBuilder("select b.id reviewContentId,a.id taskId,c.id riskId,")
                .append(" c.risk_info riskInfo,d.id reviewId,d.REVIEW_NAME reviewName,")
                .append(" b.RISK_DESCR riskDescr")
                .append(" from LC_TASK A")
                .append(" JOIN LC_REVIEW_CONTENT B ON A.ID=B.LC_TASK_ID")
                .append(" JOIN LC_RISK_DB C ON B.RISK_ID=C.ID")
                .append(" JOIN LC_REVIEW_POINT D ON C.POINT_ID=D.ID")
                .append(" where B.status='0' and A.id=:id and a.USER_NO=:userNo");
        params.put("id", id);
        params.put("userNo", userNo);
        String count = "select count(1) from (" + sql + ")";
        Pagination<LcTaskRiskDto> pager =
                lcTaskDao.findCustSqlPagination(LcTaskRiskDto.class, sql.toString(), count, params, page, pageSize);
        return pager;
    }

    @Override
    public List<LcTaskRiskDto> queryTaskRiskList(Integer taskId) {
        Map<String, Object> params = new HashMap<>();
        StringBuilder sql = new StringBuilder("select b.id reviewContentId,a.id taskId,c.id riskId,")
                .append(" c.risk_info riskInfo,d.id reviewId,d.REVIEW_NAME reviewName,")
                .append(" b.RISK_DESCR riskDescr,B.state state,B.IS_PASS isPass,B.RECTIFY rectify")
                .append(" from LC_TASK A")
                .append(" JOIN LC_REVIEW_CONTENT B ON A.ID=B.LC_TASK_ID")
                .append(" JOIN LC_RISK_DB C ON B.RISK_ID=C.ID")
                .append(" JOIN LC_REVIEW_POINT D ON C.POINT_ID=D.ID")
                .append(" where B.status=:status and A.id=:taskId")
                .append(" and B.lc_count in (select max(lc_count) from LC_REVIEW_CONTENT where LC_TASK_ID=:taskId)")
                .append(" and b.RISK_DESCR is not null")
                //.append(" and B.IS_PASS=:isPass")
                .append(" order by B.RECTIFY desc");
        System.out.println(sql.toString());
        System.out.println(taskId);
        params.put("taskId", taskId);
        //params.put("isPass", Constants.LE_REVIEW_CONTENT_STATE.IS_UN_PASS);
        params.put("status", Constants.NORMAL);
        List<LcTaskRiskDto> riskList = lcTaskDao.findCustListBySql(LcTaskRiskDto.class, sql.toString(), params);
        //排序
//        Collections.sort(riskList, new Comparator<LcTaskRiskDto>(){
//        	@Override
//        	public int compare(LcTaskRiskDto lc1, LcTaskRiskDto lc2){
//        		return lc2.getIsPass().compareTo(lc1.getIsPass());
//        	}
//        });
       // System.out.println(riskList.toString());
      
        return riskList;
    }

    @Override
    public List<LcTaskRiskDto> queryTaskRiskList(String userNo, String startTime, String endTime) {
        Map<String, Object> params = new HashMap<>();
        StringBuilder sql = new StringBuilder("select D.REVIEW_NAME reviewName,C.RISK_INFO riskInfo,")
                .append(" A.CUSTOMER_NAME customerName,A.business_type businessType,")
                .append(" A.LOAN_NUMBER loanNumber,B.RISK_DESCR riskDescr")
                .append(" from LM_PROCESS m")
                .append(" join   LC_TASK A on m.SERIAL_NUMBER=A.SERIAL_NUMBER")
                .append(" JOIN LC_REVIEW_CONTENT B ON A.ID=B.LC_TASK_ID")
                .append(" JOIN LC_RISK_DB C ON B.RISK_ID=C.ID")
                .append(" JOIN LC_REVIEW_POINT D ON C.POINT_ID=D.ID")
                .append(" where B.status=:status and m.stage>:stage")
                .append(" and a.USER_NO=:userNo and B.IS_PASS=:isPass");

        if(startTime!=null && !"".equals(startTime)){
            sql.append(" and a.UPDATE_TIME >= :startTime");
            params.put("startTime", startTime);
        }
        if(endTime!=null && !"".equals(endTime)){
            sql.append(" and a.UPDATE_TIME <= :endTime");
            params.put("endTime", endTime);
        }
        params.put("status", Constants.NORMAL);
        params.put("stage", Constants.LoanProcessType.COMPLIANCE_PENDING);
        params.put("userNo", userNo);
        params.put("isPass", Constants.LE_REVIEW_CONTENT_STATE.IS_UN_PASS);
        List<LcTaskRiskDto> riskList = lcTaskDao.findCustListBySql(LcTaskRiskDto.class, sql.toString(), params);
        return riskList;
    }

    @Override
    public List<LcTaskRiskDto> queryQuestFrequency(String userNo, String startTime, String endTime) {
        Map<String, Object> params = new HashMap<>();
        StringBuilder sql = new StringBuilder("select d.REVIEW_NAME reviewName,c.risk_info riskInfo,")
                .append(" count(1) groupNum,sum(CASE  WHEN b.STATE='0' THEN 1 ELSE 0 end)  groupFixNum")
                .append(" from LM_PROCESS m")
                .append(" join LC_TASK A on m.SERIAL_NUMBER=A.SERIAL_NUMBER")
                .append(" JOIN LC_REVIEW_CONTENT B ON A.ID=B.LC_TASK_ID")
                .append(" JOIN LC_RISK_DB C ON B.RISK_ID=C.ID")
                .append(" JOIN LC_REVIEW_POINT D ON C.POINT_ID=D.ID")
                .append(" where B.status=:status and m.stage>:stage")
                .append(" and a.USER_NO=:userNo and B.IS_PASS=:isPass");
        params.put("status", Constants.NORMAL);
        params.put("stage", Constants.LoanProcessType.COMPLIANCE_PENDING);
        params.put("userNo", userNo);
        params.put("isPass", Constants.LE_REVIEW_CONTENT_STATE.IS_UN_PASS);
        if(startTime!=null && !"".equals(startTime)){
            sql.append(" and a.UPDATE_TIME >= :startTime");
            params.put("startTime", startTime);
        }
        if(endTime!=null && !"".equals(endTime)){
            sql.append(" and a.UPDATE_TIME <= :endTime");
            params.put("endTime", endTime);
        }
        sql.append(" group by c.risk_info, d.REVIEW_NAME");
        List<LcTaskRiskDto> riskList = lcTaskDao.findCustListBySql(LcTaskRiskDto.class, sql.toString(), params);
        return riskList;
    }

    @Override
    public Pagination<LcRiskStatDto> queryRiskStatPaging(String userNo, String startTime, String endTime, String rectify, int page, int pageSize) {
        StringBuilder sql = new StringBuilder();
        Map<String, Object> params = new HashMap<>();
        sql.append("SELECT lt.ID lcTaskId, lt.CUSTOMER_NAME customerName, lt.BUSINESS_TYPE businessType,")
                .append(" lt.LOAN_TERM loanTerm, lt.LOAN_NUMBER loanNumber, lt.LOAN_USE loanUse,")
                .append(" lt.GUARANTOR guarantor, lt.LB_USER_NAME lbUserName,")
                .append(" lrd.RISK_INFO riskInfo, lrc.RISK_DESCR riskDescr")
                .append(" FROM LC_TASK lt")
                .append(" LEFT JOIN LM_PROCESS lp ON lp.SERIAL_NUMBER = lt.SERIAL_NUMBER")
                .append(" LEFT JOIN LC_REVIEW_CONTENT lrc ON lrc.LC_TASK_ID = lt.ID")
                .append(" LEFT JOIN LC_RISK_DB lrd ON lrd.ID = lrc.RISK_ID")
                .append(" WHERE lp.STAGE > :stage AND lt.STATUS = :status")
        		.append(" AND lrc.IS_PASS = :isPass");
        params.put("stage", Constants.LoanProcessType.COMPLIANCE_PENDING);
        params.put("status", Constants.NORMAL);
        params.put("isPass", Constants.LE_REVIEW_CONTENT_STATE.IS_UN_PASS);
        

        if (userNo != null && !"".equals(userNo)) {
            sql.append(" AND lt.USER_NO = :userNo");
            params.put("userNo", userNo);
        }
        if (startTime != null && !"".equals(startTime)) {
            sql.append(" AND lt.UPDATE_TIME >=:startTime");
            params.put("startTime", startTime);
        }
        if (endTime != null && !"".equals(endTime)) {
            sql.append(" AND lt.UPDATE_TIME <=:endTime");
            params.put("endTime", endTime);
        }
        if (rectify != null && !"".equals(rectify)) {
            sql.append(" AND lrc.RECTIFY =:rectify");
            params.put("rectify", rectify);
        }

        String count = "select count(1) from (" + sql.toString() + ")";
        return lcTaskDao.findCustSqlPagination(LcRiskStatDto.class, sql.toString(), count, params, page, pageSize);
    }

    @Override
    public List<LcTaskStatDto> queryTaskStatList(String userNo, String startTime, String endTime) {
        StringBuilder sql = new StringBuilder();
        Map<String, Object> params = new HashMap<>();
        sql.append("SELECT lt.ID lcTaskId, lt.CUSTOMER_NAME customerName, lt.BUSINESS_TYPE businessType, lt.LOAN_TERM loanTerm,")
                .append(" lt.LOAN_NUMBER loanNumber, lt.STOCK_NUMBER stockNumber, lt.LOAN_USE loanUse,")
                .append(" lt.ASSURE_MEANS assureMeans, lt.LOAN_RATE loanRate, lt.SUBMISSION_TIME submissionTime,")
                .append(" lt.CREDIT_EXPIRE_DATE creditExpireDate, lt.GUARANTOR guarantor, lt.LB_USER_NAME lbUserName")
                .append(" FROM LC_TASK lt")
                .append(" LEFT JOIN LM_PROCESS lp ON lp.SERIAL_NUMBER = lt.SERIAL_NUMBER")
                .append(" WHERE lp.STAGE > :stage AND lt.STATUS = :status");
        params.put("stage", Constants.LoanProcessType.COMPLIANCE_PENDING);
        params.put("status", Constants.NORMAL);
        if (userNo != null && !"".equals(userNo)) {
            sql.append(" AND lt.USER_NO = :userNo");
            params.put("userNo", userNo);
        }
        if (startTime != null && !"".equals(startTime)) {
            sql.append(" AND lt.UPDATE_TIME >=:startTime");
            params.put("startTime", startTime);
        }
        if (endTime != null && !"".equals(endTime)) {
            sql.append(" AND lt.UPDATE_TIME <=:endTime");
            params.put("endTime", endTime);
        }
        
        return lcTaskDao.findCustListBySql(LcTaskStatDto.class, sql.toString(), params);
    }

    @Override
    public List<LcRiskStatDto> queryRiskStatList(String userNo, String rectify,String startTime, String endTime) {
        StringBuilder sql = new StringBuilder();
        Map<String, Object> params = new HashMap<>();
        sql.append("SELECT lt.ID lcTaskId, lt.CUSTOMER_NAME customerName, lt.BUSINESS_TYPE businessType,")
	        .append(" lt.LOAN_TERM loanTerm, lt.LOAN_NUMBER loanNumber, lt.LOAN_USE loanUse,")
	        .append(" lt.GUARANTOR guarantor, lt.LB_USER_NAME lbUserName,")
	        .append(" lrd.RISK_INFO riskInfo, lrc.RISK_DESCR riskDescr,lrc.RECTIFY rectify")
	        .append(" FROM LC_TASK lt")
	        .append(" LEFT JOIN LM_PROCESS lp ON lp.SERIAL_NUMBER = lt.SERIAL_NUMBER")
	        .append(" LEFT JOIN LC_REVIEW_CONTENT lrc ON lrc.LC_TASK_ID = lt.ID")
	        .append(" LEFT JOIN LC_RISK_DB lrd ON lrd.ID = lrc.RISK_ID")
	        .append(" WHERE lp.STAGE > :stage AND lt.STATUS = :status")
			.append(" AND lrc.IS_PASS = :isPass");
		params.put("stage", Constants.LoanProcessType.COMPLIANCE_PENDING);
		params.put("status", Constants.NORMAL);
		params.put("isPass", Constants.LE_REVIEW_CONTENT_STATE.IS_UN_PASS);
//        if (userNo != null && !"".equals(userNo)) {
//            sql.append(" AND lt.USER_NO = :userNo");
//            params.put("userNo", userNo);
//        }
        if (startTime != null && !"".equals(startTime)) {
            sql.append(" AND lt.UPDATE_TIME >=:startTime");
            params.put("startTime", startTime);
        }
        if (endTime != null && !"".equals(endTime)) {
            sql.append(" AND lt.UPDATE_TIME <=:endTime");
            params.put("endTime", endTime);
        }
        if (rectify != null && !"".equals(rectify)) {
            sql.append(" AND lrc.RECTIFY =:rectify");
            params.put("rectify", rectify);
        }
        return lcTaskDao.findCustListBySql(LcRiskStatDto.class, sql.toString(), params);
    }

    @Override
    public List<LcManagerRiskStatDto> queryManagerRiskStatList(String userNo, String startTime, String endTime) {
        Map<String, Object> params = new HashMap<>();
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT lbUserNo, lbUserName, count(1) loanNUM, sum(loanAvg) loanSum, sum(riskNum) riskSum")
                .append(" FROM (SELECT lbUserNo, lbUserName, avg(loanNumber) loanAvg, count(riskDesc) riskNum")
                .append(" FROM (SELECT lt.ID lcTaskId, lt.LB_USER_NO lbUserNo, lt.LB_USER_NAME lbUserName,")
                .append(" lt.LOAN_NUMBER loanNumber, lrc.RISK_DESCR riskDesc")
                .append(" FROM LC_TASK lt")
                .append(" LEFT JOIN LM_PROCESS lp ON lp.SERIAL_NUMBER = lt.SERIAL_NUMBER")
                .append(" LEFT JOIN LC_REVIEW_CONTENT lrc ON lrc.LC_TASK_ID = lt.ID")
                .append(" WHERE lp.STAGE > :stage AND lt.USER_NO = :userNo AND lt.STATUS = :status and lrc.IS_PASS=:isPass");
        params.put("stage", Constants.LoanProcessType.COMPLIANCE_PENDING);
        params.put("status", Constants.NORMAL);
        params.put("isPass", Constants.LE_REVIEW_CONTENT_STATE.IS_UN_PASS);
        if (userNo != null && !"".equals(userNo)) {
            sql.append(" AND lt.USER_NO = :userNo");
            params.put("userNo", userNo);
        }
        if (startTime != null && !"".equals(startTime)) {
            sql.append(" AND lt.UPDATE_TIME >=:startTime");
            params.put("startTime", startTime);
        }
        if (endTime != null && !"".equals(endTime)) {
            sql.append(" AND lt.UPDATE_TIME <=:endTime");
            params.put("endTime", endTime);
        }
        
        sql.append(") GROUP BY lcTaskId, lbUserNo, lbUserName) GROUP BY lbUserNo, lbUserName, riskNum");
        List<LcManagerRiskStatDto> list = lcTaskDao.findCustListBySql(LcManagerRiskStatDto.class, sql.toString(), params);
        for (LcManagerRiskStatDto item : list) {
            String colorCate = "";
            Integer riskSum = item.getRiskSum();
            if (riskSum <= 5) {
                colorCate = "Green";
            } else if (riskSum <= 10) {
                colorCate = "Blue";
            } else if (riskSum <= 15) {
                colorCate = "Yellow";
            } else {
                colorCate = "Red";
            }
            item.setColorCate(colorCate);
        }
        return list;
    }

    @Override
    public Pagination<LcManagerRiskStatDto> queryManagerRiskStatPaging(String userNo, String startTime, String endTime, int page, int pageSize) {
        Map<String, Object> params = new HashMap<>();
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT lbUserNo, lbUserName, count(1) loanNUM, sum(loanAvg) loanSum, sum(riskNum) riskSum FROM")
                .append(" (SELECT lbUserNo, lbUserName, avg(loanNumber) loanAvg, count(riskDesc) riskNum FROM")
                .append(" (SELECT lt.ID lcTaskId, lt.LB_USER_NO lbUserNo, lt.LB_USER_NAME lbUserName,")
                .append(" lt.LOAN_NUMBER loanNumber, lrc.RISK_DESCR riskDesc")
                .append(" FROM LC_TASK lt")
                .append(" LEFT JOIN LM_PROCESS lp ON lp.SERIAL_NUMBER = lt.SERIAL_NUMBER")
                .append(" LEFT JOIN LC_REVIEW_CONTENT lrc ON lrc.LC_TASK_ID = lt.ID")
                .append(" WHERE lp.STAGE > :stage  AND lt.STATUS = :status and lrc.IS_PASS=:isPass");
        params.put("stage", Constants.LoanProcessType.COMPLIANCE_PENDING);
        params.put("status", Constants.NORMAL);
        params.put("isPass", Constants.LE_REVIEW_CONTENT_STATE.IS_UN_PASS);
        
        if (userNo != null && !"".equals(userNo)) {
            sql.append(" AND lt.USER_NO = :userNo");
            params.put("userNo", userNo);
        }
        if (startTime != null && !"".equals(startTime)) {
            sql.append(" AND lt.UPDATE_TIME >=:startTime");
            params.put("startTime", startTime);
        }
        if (endTime != null && !"".equals(endTime)) {
            sql.append(" AND lt.UPDATE_TIME <=:endTime");
            params.put("endTime", endTime);
        }
        sql.append(") GROUP BY lcTaskId, lbUserNo, lbUserName) GROUP BY lbUserNo, lbUserName, riskNum");
        String count = "select count(1) from (" + sql.toString() + ")";
        Pagination<LcManagerRiskStatDto> pager =
                lcTaskDao.findCustSqlPagination(LcManagerRiskStatDto.class, sql.toString(), count, params, page, pageSize);
        List<LcManagerRiskStatDto> items = pager.getItems();
        for (LcManagerRiskStatDto item : items) {
            String colorCate = "";
            Integer riskSum = item.getRiskSum();
            //GGG到时候改成可配置化
            if (riskSum <= 5) {
                colorCate = "Green";
            } else if (riskSum <= 10) {
                colorCate = "Blue";
            } else if (riskSum <= 15) {
                colorCate = "Yellow";
            } else {
                colorCate = "Red";
            }
            item.setColorCate(colorCate);
        }
        return pager;
    }

    @Override
    public LcMonthStatDto queryMonthStat(String userNo, String startTime, String endTime) {
        LcMonthStatDto result = null;
        Map<String, Object> params = new HashMap<>();
        StringBuilder sql = new StringBuilder();
        sql.append("select count(SERIAL_NUMBER) loanCount,sum(LOAN_NUMBER) loanMoney,")
                .append(" sum(questNum) questNum,sum(fixQuestNum) fixQuestNum from (")
                .append(" select A.user_no,A.SERIAL_NUMBER,A.LOAN_NUMBER,count(1) as questNum,")
                .append(" sum(CASE  WHEN b.STATE='0' THEN 1 ELSE 0 end)  fixQuestNum")
                .append(" from LM_PROCESS m")
                .append(" join   LC_TASK A on m.SERIAL_NUMBER=A.SERIAL_NUMBER")
                .append(" JOIN LC_REVIEW_CONTENT B ON A.ID=B.LC_TASK_ID")
                .append(" JOIN LC_RISK_DB C ON B.RISK_ID=C.ID")
                .append(" JOIN LC_REVIEW_POINT D ON C.POINT_ID=D.ID")
                .append(" where B.status=:status and m.stage>:stage")
                .append(" and a.USER_NO=:userNo and B.IS_PASS=:isPass")
                .append(" and a.UPDATE_TIME >=:startTime AND a.UPDATE_TIME <=:endTime")
                .append(" group by A.user_no,A.SERIAL_NUMBER,A.LOAN_NUMBER")
                .append(" ) group by user_no");
        params.put("stage", Constants.LoanProcessType.COMPLIANCE_PENDING);
        params.put("status", Constants.NORMAL);
        params.put("userNo", userNo);
        params.put("startTime", startTime);
        params.put("endTime", endTime);
        params.put("isPass", Constants.LE_REVIEW_CONTENT_STATE.IS_UN_PASS);
        List<LcMonthStatDto> dtos = lcTaskDao.findCustListBySql(LcMonthStatDto.class, sql.toString(), params);
        if(dtos!=null && dtos.size()!=0)
            result = dtos.get(0);
        return result;
    }

    @Override
    public LcReportDto queryLcReportData(String userNo, String startTime, String endTime) {
        LcReportDto reportDto = new LcReportDto();
        //若起止日期都为空  默认为上个月的时间段
        if((startTime==null || "".equals(startTime)) && (endTime==null || "".equals(endTime))){
            startTime = DateUtil.getLastFirstData(1);
            endTime = DateUtil.getLastEndData(1);
        }
        reportDto.setStartTime(startTime);
        reportDto.setEndTime(endTime);
        String month = endTime.substring(5,7);
        String year = endTime.substring(0,4);

        reportDto.setMonth(month);
        reportDto.setYear(year);

        //获取  二、与上月对比：
        LcMonthStatDto monthStat = this.queryMonthStat(userNo, startTime, endTime);
        LcMonthStatDto lastMonthStat = this.queryMonthStat(userNo,
                DateUtil.getLastFirstData(2),
                DateUtil.getLastEndData(2));
        reportDto.setMonthStatDto(monthStat);
        reportDto.setLastMonthStatDto(lastMonthStat);

        //获取  三、今年以来涉及的风险点、频次及即时整改情况：
        String yearFirst = DateUtil.getCurrYearFirst();
        //获取今年问题出现频次统计
        List<LcTaskRiskDto> yearRiskList = this.queryQuestFrequency(userNo, yearFirst, endTime);
        //获取本月问题出现频次统计
        List<LcTaskRiskDto> monthRiskDtos = this.queryQuestFrequency(userNo, startTime, endTime);
        if(yearRiskList!=null && yearRiskList.size()!=0){
            for(LcTaskRiskDto yearDto : yearRiskList){
                yearDto.setGroupYearNum(yearDto.getGroupNum());
                yearDto.setGroupYearFixNum(yearDto.getGroupFixNum());
                if(monthRiskDtos!=null && monthRiskDtos.size()!=0){
                    month:for(LcTaskRiskDto monthDto : monthRiskDtos){
                        if(yearDto.getRiskInfo().equals(monthDto.getRiskInfo())) {
                            yearDto.setGroupMonthNum(monthDto.getGroupNum());
                            yearDto.setGroupMonthFixNum(monthDto.getGroupFixNum());
                            break month;
                        }
                    }
                }

            }
        }
        reportDto.setRiskDtoList(yearRiskList);

        //获取  四、触及流程环节及业务领域的风险点：
        if(monthRiskDtos!=null && monthRiskDtos.size()!=0) {
            Map<String, List<LcTaskRiskDto>> taskRiskMap =
                    monthRiskDtos.stream().collect(Collectors.groupingBy(LcTaskRiskDto::getReviewName));
            reportDto.setTaskRiskMap(taskRiskMap);
        }

        //获取 五、存在的主要问题：
        Map<String, Map<String, List<LcTaskRiskDto>>> questMap = new HashMap<>();
        List<LcTaskRiskDto> riskList = this.queryTaskRiskList(userNo, startTime, endTime);
        if(riskList!=null && riskList.size()!=0) {
            Map<String, List<LcTaskRiskDto>> pointMap = riskList.stream().collect(Collectors.groupingBy(LcTaskRiskDto::getReviewName));
            for(Map.Entry<String, List<LcTaskRiskDto>> entry : pointMap.entrySet()){
                String key = entry.getKey();
                List<LcTaskRiskDto> pointList = entry.getValue();
                Map<String, List<LcTaskRiskDto>> riskMap = pointList.stream().collect(Collectors.groupingBy(LcTaskRiskDto::getRiskInfo));
                questMap.put(key, riskMap);
            }
        }
        reportDto.setQuestMap(questMap);

        return reportDto;
    }

    @Override
    public LcTask queryBySerialNumber(String serialNumber) throws Exception {
        return lcTaskDao.selectSingleByProperty("serialNumber",serialNumber);
    }

	@Override
	public String ChineseIndustry(String industry) {
		// TODO Auto-generated method stub
		String result = "";
		if(industry.equals("OTHER")){
			result = "其他行业";
		}else if(industry.equals("MANUF")){
			result = "制造业";
		}else if(industry.equals("BUILD")){
			result = "建筑业";
		}else if(industry.equals("BUSINESS")){
			result = "贸易业";
		}
		
		return result;
	}


}
