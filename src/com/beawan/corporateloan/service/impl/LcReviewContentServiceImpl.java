package com.beawan.corporateloan.service.impl;

import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.corporateloan.dao.LcReviewContentDao;
import com.beawan.corporateloan.dao.LcReviewPointDao;
import com.beawan.corporateloan.dao.LcRiskDbDao;
import com.beawan.corporateloan.dao.LcTaskDao;
import com.beawan.corporateloan.entity.*;
import com.beawan.corporateloan.service.LcReviewContentService;
import com.platform.util.ExceptionUtil;
import com.platform.util.GsonUtil;
import org.apache.bcel.classfile.Constant;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.corporateloan.entity.LcReviewContent;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yzj
 */
@Service("lcReviewContentService")
public class LcReviewContentServiceImpl extends BaseServiceImpl<LcReviewContent> implements LcReviewContentService{

    private static final Logger log = Logger.getLogger(LcReviewContentServiceImpl.class);

    /**
    * 注入DAO
    */
    @Resource(name = "lcReviewContentDao")
    public void setDao(BaseDao<LcReviewContent> dao) {
        super.setDao(dao);
    }
    @Resource
    public LcReviewContentDao lcReviewContentDao;
    @Resource
    public LcTaskDao lcTaskDao;
    @Resource
    private LcRiskDbDao lcRiskDbDao;
    @Resource
    private LcReviewPointDao lcReviewPointDao;

    @Override
    public void saveOrUpdateContent(Integer taskId, String descrData) {

        List<Map<String, Object>> maps = GsonUtil.GsonToListMaps(descrData);
        List<LcReviewContent> result = new ArrayList<>();

        LcTask lcTask = lcTaskDao.findByPrimaryKey(taskId);
        //获取本次审查结果项
        Map<String, Object> params = new HashMap<>();
        params.put("state", Constants.NORMAL);
        params.put("lcTaskId", taskId);
        List<LcReviewContent> lcReviewContents = lcReviewContentDao.selectByProperty(params);
        
        for(LcReviewContent content : lcReviewContents) {
            for (Map<String, Object> map : maps) {
                int id = Integer.parseInt(map.get("id") + "");
                if(content.getId().equals(id)) {
                    //是否处理标识为空
                    if (content.getState() == null) {//如果为空，传过来是什么就是什么
                        content.setIsPass(Integer.parseInt(map.get("isPass").toString()));
                    } else {
                        //如果不为空：
                        //传过来是通过，就是已处理
                        //传过来是不通过，就是未处理
                        if ((Constants.LE_REVIEW_CONTENT_STATE.IS_PASS + "").equals(map.get("isPass").toString())) {
                            content.setIsPass(Constants.LE_REVIEW_CONTENT_STATE.IS_PASS);
                            //content.setState(Constants.LE_REVIEW_CONTENT_STATE.SOLVE);
                        } else if ((Constants.LE_REVIEW_CONTENT_STATE.IS_UN_PASS + "").equals(map.get("isPass").toString())) {
                            content.setIsPass(Constants.LE_REVIEW_CONTENT_STATE.IS_UN_PASS);
                            //content.setState(Constants.LE_REVIEW_CONTENT_STATE.NOT_SOLVE);
                        }
                    }

                    content.setRiskDescr(map.get("riskDescr") + "");
                    content.setRectify(Constants.LC_REVIEW_CONTENT_STATE.NOT_SOLVE);
                    result.add(content);
                }
            }
        }
        if(result==null){
            log.error("审查内容为空！请排查前台传输参数");
            return;
        }
        lcReviewContentDao.batchSaveUpdata(result);
    }
    
  
    @Override
    public void updateContent(Integer taskId, String indu)throws Exception {
    	//获取到所属行业的审查内容
        List<LcReviewPoint> pointList = lcReviewPointDao.select("indu=?1 or indu='COMM'", indu);
        //获取审查任务信息
        LcTask task = lcTaskDao.findByPrimaryKey(taskId);
        //如果查询不出来，或者审查count为空，表示数据有问题
        if(task == null ||task.getLcCount()==null){
            ExceptionUtil.throwException("数据异常，初始化数据失败");
        }
        //获取到审查信息列表
        //lcCount==1表示第一次提交
        Map<String, Object> params = new HashMap<>();
        System.out.println("task.getLcCount() = " + task.getLcCount());
        params.put("state", Constants.NORMAL);
        params.put("lcTaskId", taskId);
        List<LcReviewContent> histList = lcReviewContentDao.selectByProperty(params);
        
        //用来存  更新的审查项列表
        List<LcReviewContent> list = new ArrayList<>();
      
        
        ////第一次
//        if(task.getLcCount()==1){
	    //若存在历史数据 且 这次选择的行业与上次不同，则删除历史数据  并进行初始化
	    if(!CollectionUtils.isEmpty(histList) && !task.getIndustry().equals(indu)){
	    	Integer LcCount = histList.get(0).getLcCount() + 1;
	        for(LcReviewContent hist : histList){
	            hist.setState(Constants.DELETE);           
	        }
	        System.out.println("LcCount = " + LcCount);
	        lcReviewContentDao.batchSaveUpdata(histList);
	
	        task.setIndustry(indu);
	        for (LcReviewPoint point : pointList) {
	            List<LcRiskDb> riskList = lcRiskDbDao.selectByProperty("pointId", point.getId());
	            for (LcRiskDb risk : riskList) {
	                LcReviewContent content = new LcReviewContent();
	                content.setLcCount(LcCount);
	                content.setLcTaskId(taskId);
	                content.setRiskId(risk.getId());
	                //默认都是通过状态
	                content.setIsPass(0);
	                content.setState(Constants.NORMAL);
	                //content.setRiskDescr("发生行业转换，但未开始审查，默认通过状态。");
	                content.setRectify(Constants.LC_REVIEW_CONTENT_STATE.NOT_SOLVE);
	                list.add(content);
	            }
	        }
	    }else if(!CollectionUtils.isEmpty(histList) && task.getIndustry().equals(indu)){
	        //保持原样
	        return;
	    }else if(CollectionUtils.isEmpty(histList)) {
	        //无历史数据  则初始化
	        task.setIndustry(indu);
	        for (LcReviewPoint point : pointList) {
	            List<LcRiskDb> riskList = lcRiskDbDao.selectByProperty("pointId", point.getId());
	            for (LcRiskDb risk : riskList) {
	                LcReviewContent content = new LcReviewContent();
	                content.setLcCount(1);
	                content.setLcTaskId(taskId);
	                content.setRiskId(risk.getId());
	                //默认都是通过状态
	                content.setIsPass(0);
	                content.setState(Constants.NORMAL);
	                list.add(content);
	            }
	        }
	    }
//        }else{
//        	//不是第一次提交，而且这一次提交已经存在了审查信息
//        	//直接不变
//        	if(!CollectionUtils.isEmpty(histList)){
//        		return;
//        	}
//            //打回后重新提交的
//            //获取上一次提交结果
//            //params.put("lcCount", task.getLcCount() -1);
//        	params.put("state", Constants.NORMAL);
//            histList = lcReviewContentDao.selectByProperty(params);
//            for(LcReviewContent hist : histList){
//                LcReviewContent content = new LcReviewContent();
//                content.setLcTaskId(taskId);
//                content.setLcCount(hist.getLcCount() + 1);
//                content.setRiskId(hist.getRiskId());
//                //默认都是通过状态
//                content.setIsPass(hist.getIsPass());
//                //同步上一次的未通过的数据
//                content.setRiskDescr(hist.getRiskDescr());
////                if(hist.getIsPass().equals("1")){
////                    content.setState(1);
////                }
//                content.setState(Constants.NORMAL);
//                list.add(content);
//            }
//        }
        lcTaskDao.saveOrUpdate(task);
        //保存结果
        lcReviewContentDao.batchSaveUpdata(list);
    }
}
