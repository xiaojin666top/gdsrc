package com.beawan.corporateloan.service.impl;

import javax.annotation.Resource;

import com.platform.util.StringUtil;
import org.springframework.stereotype.Service;

import com.beawan.base.entity.User;
import com.beawan.base.dao.IUserDAO;
import com.beawan.common.Constants;
import com.beawan.common.util.SystemUtil;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.corporateloan.dao.LmProcessDao;
import com.beawan.corporateloan.dao.LrCustInfoDao;
import com.beawan.corporateloan.dao.LrInfoDao;
import com.beawan.corporateloan.dao.LrRecordListDao;
import com.beawan.corporateloan.entity.LmProcess;
import com.beawan.corporateloan.entity.LrCustInfo;
import com.beawan.corporateloan.entity.LrInfo;
import com.beawan.corporateloan.entity.LrRecordList;
import com.beawan.corporateloan.service.LmProcessService;
import com.beawan.corporateloan.service.LrInfoService;
import com.beawan.customer.bean.CusBase;
import com.platform.util.DateUtil;
import com.platform.util.ExceptionUtil;

/**
 * @author yzj
 */
@Service("lrInfoService")
public class LrInfoServiceImpl extends BaseServiceImpl<LrInfo> implements LrInfoService {
	/**
	 * 注入DAO
	 */
	@Resource(name = "lrInfoDao")
	public void setDao(BaseDao<LrInfo> dao) {
		super.setDao(dao);
	}
	@Resource
	private LmProcessService lmProcessService;

	@Resource
	public LrInfoDao lrInfoDao;

	@Resource
	public LrCustInfoDao lrCustInfoDao;
	@Resource
	public LmProcessDao lmProcessDao;
	@Resource
	public LrRecordListDao lrRecordListDao;
	@Resource
	public IUserDAO userDAO;

	@Override
	public LrInfo createNewLrTask(String customerNo, String userNo) throws Exception {
		User retailUser = userDAO.getUserById(userNo);
		if (retailUser == null) {
			ExceptionUtil.throwException("查询用户信息异常，用户编号为：" + userNo);
		}

		// =========新建主表任务=========
		// 1.获取业务流水号
		String serialNumber = SystemUtil.generateSerialNo();
		// 2.新建任务
		LmProcess lmProcess = new LmProcess();
		lmProcess.setSerialNumber(serialNumber);
		lmProcess.setCustomerNo(customerNo);
		// 设置营销中状态
		lmProcess.setStage(Constants.LoanProcessType.RETAIL_ING);
		// 添加营销用户编号--创建人是操作人
		lmProcess.setPositionUser(userNo);
		lmProcess.setPositionUserName(retailUser.getUserName());
		lmProcess.setCreater(userNo);
		lmProcess.setUpdater(userNo);
		lmProcess.setUpdateTime(DateUtil.getNowTimestamp());
		lmProcessDao.saveOrUpdate(lmProcess);

		// =========添加主表记录============

		// =========添加营销任务============
		LrInfo lrInfo = new LrInfo();
		lrInfo.setSerialNumber(serialNumber);
		// 查询出客户信息
		LrCustInfo lrCmClass = lrCustInfoDao.selectSingleByProperty("customerNo", customerNo);
		lrInfo.setCustomerNo(customerNo);
		lrInfo.setUserNo(userNo);
		// 设置客户分类信息
		if (lrCmClass != null) {
			lrInfo.setCustomerClass(lrCmClass.getCustomerClass());
		}
		lrInfo.setCreater(userNo);
		lrInfo.setUpdater(userNo);
		lrInfo.setUpdateTime(DateUtil.getNowTimestamp());
		lrInfoDao.saveOrUpdate(lrInfo);

		// ========添加营销记录============
		LrRecordList lrRecordList = new LrRecordList();
		lrRecordList.setSerialNumber(serialNumber);
		lrRecordList.setCustomerNo(customerNo);
		lrRecordList.setDealType(Constants.LrRecordType.CREATE_MARKET_SUCCESS);
		lrRecordList.setUserNo(userNo);
		lrRecordList.setRecordRemark("新建营销任务");
		lrRecordList.setCreater(userNo);
		lrRecordList.setUpdater(userNo);
		lrRecordList.setUpdateTime(DateUtil.getNowTimestamp());
		lrRecordListDao.saveOrUpdate(lrRecordList);
		return lrInfo;
	}
	
	@Override
	public void synchroLrInfo(LrInfo lrInfo,String userNo) throws Exception {
		// 根据id获取到 lrInfo
		LrInfo saveLrInfo = lrInfoDao.findByPrimaryKey(lrInfo.getId());
		if (saveLrInfo == null) {
			ExceptionUtil.throwException("未查询到营销信息");
		}
		// 查询客户信息表
		LrCustInfo lrCustInfo = lrCustInfoDao.selectSingleByProperty("customerNo", lrInfo.getCustomerNo());
		if (lrCustInfo == null) {
			ExceptionUtil.throwException("未查询到客户信息");
		}
		// 将前台传来的LrInfo保存
		if(lrInfo.getCustomerClass() != null){
			saveLrInfo.setCustomerClass(lrInfo.getCustomerClass());
		}

		saveLrInfo.setRetailProduct(lrInfo.getRetailProduct());
		saveLrInfo.setRetailTerm(lrInfo.getRetailTerm());
		saveLrInfo.setRetailMoney(lrInfo.getRetailMoney());
		saveLrInfo.setRetailRate(lrInfo.getRetailRate());
		saveLrInfo.setRetailUse(lrInfo.getRetailUse());
		saveLrInfo.setRetailGuarante(lrInfo.getRetailGuarante());
		saveLrInfo.setUpdater(userNo);
		saveLrInfo.setUpdateTime(DateUtil.getNowTimestamp());
		lrInfoDao.saveOrUpdate(saveLrInfo);
		
		//将客户信息同步保存
		lrCustInfo.setCustomerClass(lrInfo.getCustomerClass());
		lrCustInfo.setUpdater(userNo);
		lrCustInfo.setUpdateTime(DateUtil.getNowTimestamp());
		lrCustInfoDao.saveOrUpdate(lrCustInfo);
	}


	@Override
	public void finishRetailTask(String serialNumber, String userNo) throws Exception {
		// =========更新主表任务=========
		LmProcess lmProcess = lmProcessDao.selectSingleByProperty("serialNumber", serialNumber);
		if (lmProcess == null) {
			// 在主表中没有查询到当前serialNumber对应的主表信息
			ExceptionUtil.throwException("未查询到匹配的任务，任务编号为：" + serialNumber);
		}
		lmProcess.setStage(Constants.LoanProcessType.RETAIl_STOP);
		lmProcess.setPositionUser("");
		lmProcess.setPositionUserName("");
		lmProcess.setUpdater(userNo);
		lmProcess.setUpdateTime(DateUtil.getNowTimestamp());
		lmProcessDao.saveOrUpdate(lmProcess);

		// =========添加一条主表记录=========

		// =========更新营销任务=========
		LrInfo lrInfo = lrInfoDao.selectSingleByProperty("serialNumber", serialNumber);
		// 营销结束算失败
		lrInfo.setRetailStatus(Constants.RetailResult.RETAIL_FAIL);
		lrInfo.setUpdater(userNo);
		lrInfo.setUpdateTime(DateUtil.getNowTimestamp());
		lrInfoDao.saveOrUpdate(lrInfo);

		// =========添加一条营销记录=========
		LrRecordList lrRecordList = new LrRecordList();
		lrRecordList.setSerialNumber(serialNumber);
		lrRecordList.setCustomerNo(lmProcess.getCustomerNo());
		// 营销未进件
		lrRecordList.setDealType(Constants.LrRecordType.MARKET_FINISH);
		lrRecordList.setUserNo(userNo);
		lrRecordList.setRecordRemark("营销失败：营销未进件");
		lrRecordList.setCreater(userNo);
		lrRecordList.setUpdater(userNo);
		lrRecordList.setUpdateTime(DateUtil.getNowTimestamp());
		lrRecordListDao.saveOrUpdate(lrRecordList);

	}

	@Override
	public void submitRetailTask(String serialNumber, String userNo) throws Exception {
		// =========更新主表任务=========
		LmProcess lmProcess = lmProcessDao.selectSingleByProperty("serialNumber", serialNumber);
		if (lmProcess == null) {
			// 在主表中没有查询到当前serialNumber对应的主表信息
			ExceptionUtil.throwException("未查询到匹配的任务，任务编号为：" + serialNumber);
		}
		lmProcess.setStage(Constants.LoanProcessType.RETAIL_IN_ING);
		lmProcess.setUpdater(userNo);
		lmProcess.setUpdateTime(DateUtil.getNowTimestamp());
		lmProcessDao.saveOrUpdate(lmProcess);

		// =========添加一条主表记录=========

		// =========更新营销任务=========
		LrInfo lrInfo = lrInfoDao.selectSingleByProperty("serialNumber", serialNumber);
		// 营销结束算失败
		lrInfo.setRetailStatus(Constants.RetailResult.RETAIL_SUCCESS);
		lrInfo.setUpdater(userNo);
		lrInfo.setUpdateTime(DateUtil.getNowTimestamp());
		lrInfoDao.saveOrUpdate(lrInfo);

		// =========添加一条营销记录=========
		
		LrRecordList lrRecordList = new LrRecordList();
		lrRecordList.setSerialNumber(serialNumber);
		lrRecordList.setCustomerNo(lmProcess.getCustomerNo());
		// 营销未进件
		lrRecordList.setDealType(Constants.LrRecordType.MARKET_IN);
		lrRecordList.setUserNo(userNo);
		lrRecordList.setRecordRemark("营销成功：进入营销进件");
		lrRecordList.setCreater(userNo);
		lrRecordList.setUpdater(userNo);
		lrRecordList.setUpdateTime(DateUtil.getNowTimestamp());
		lrRecordListDao.saveOrUpdate(lrRecordList);

	}

	@Override
	public void submitToSurvey(String serialNumber, String userNo) throws Exception {
		// =========更新主表任务=========
		LmProcess lmProcess = lmProcessDao.selectSingleByProperty("serialNumber", serialNumber);
		if (lmProcess == null) {
			// 在主表中没有查询到当前serialNumber对应的主表信息
			ExceptionUtil.throwException("未查询到匹配的任务，任务编号为：" + serialNumber);
		}
		lmProcess.setStage(Constants.LoanProcessType.SURVEY_PENDING);
		lmProcess.setUpdater(userNo);
		lmProcess.setUpdateTime(DateUtil.getNowTimestamp());
		lmProcessDao.saveOrUpdate(lmProcess);

		// =========添加一条主表记录=========

		// =========添加一条营销记录=========
		LrRecordList lrRecordList = new LrRecordList();
		lrRecordList.setSerialNumber(serialNumber);
		lrRecordList.setCustomerNo(lmProcess.getCustomerNo());
		// 营销进件完成-提交到贷前调查
		lrRecordList.setDealType(Constants.LrRecordType.MARKET_IN_SUCCESS);
		lrRecordList.setUserNo(userNo);
		lrRecordList.setRecordRemark("进件完成：进入贷前调查");
		lrRecordList.setCreater(userNo);
		lrRecordList.setUpdater(userNo);
		lrRecordList.setUpdateTime(DateUtil.getNowTimestamp());
		lrRecordListDao.saveOrUpdate(lrRecordList);
	}


	public void submitRetailToSurvey(String serialNumber, String userNo, String userName) throws  Exception {

		//提交到待分配
		this.submitToSurvey(serialNumber, userNo);
		//提交到贷前待处理
		//创建贷前  待处理任务 -->跳过原先需要贷前管理岗分配客户经理的流程
		lmProcessService.updateAppointLbUser(serialNumber, userNo, userName, userNo);
	}
}
