package com.beawan.corporateloan.service.impl;

import com.beawan.analysis.abnormal.bean.ErrData;
import com.beawan.analysis.abnormal.service.IAbnormalService;
import com.beawan.analysis.finansis.bean.FnCash;
import com.beawan.analysis.finansis.bean.FnVerify;
import com.beawan.analysis.finansis.dao.FnVerifyDao;
import com.beawan.analysis.finansis.dao.IFnTableDao;
import com.beawan.analysis.finansis.dto.FinaRatioDto;
import com.beawan.base.dao.ISysDicDAO;
import com.beawan.base.entity.SReportModelRow;
import com.beawan.base.entity.SysDic;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.util.SystemUtil;
import com.beawan.corporateloan.controller.FinanceController;
import com.beawan.corporateloan.dto.*;
import com.beawan.corporateloan.service.LbSurveyReportService;
import com.beawan.customer.bean.FinriskResult;
import com.beawan.customer.service.FinriskResultService;
import com.beawan.customer.service.ICusFSToolSV;
import com.beawan.loanAfter.dto.LfConstants;
import com.beawan.survey.building.dao.*;
import com.beawan.survey.building.entity.*;
import com.beawan.survey.custInfo.bean.*;
import com.beawan.survey.custInfo.dao.*;
import com.beawan.survey.custInfo.dto.CompBaseDto;
import com.beawan.survey.custInfo.dto.CompConnetCmDto;
import com.beawan.survey.custInfo.dto.CompCreditDto;
import com.beawan.survey.custInfo.dto.CompFinancingBankDto;
import com.beawan.survey.custInfo.dto.CompJudgmentDto;
import com.beawan.survey.custInfo.dto.CompUserCreditDto;
import com.beawan.survey.custInfo.entity.*;
import com.beawan.survey.custInfo.service.CompJudgmentService;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.beawan.survey.custInfo.service.ICompConnectSV;
import com.beawan.survey.custInfo.service.ICompFinanceSV;
import com.beawan.survey.custInfo.service.ICompFinancingSV;
import com.beawan.survey.custInfo.service.compRunAnalysisService;
import com.beawan.survey.guarantee.dao.*;
import com.beawan.survey.guarantee.dto.GuaEstateDto;
import com.beawan.survey.guarantee.entity.*;
import com.beawan.survey.loanInfo.bean.ApplyInfo;
import com.beawan.survey.loanInfo.bean.ApplyRateInfo;
import com.beawan.survey.loanInfo.bean.ApplyScheme;
import com.beawan.survey.loanInfo.bean.BenefitAnaly;
import com.beawan.survey.loanInfo.bean.BenefitForecast;
import com.beawan.survey.loanInfo.bean.Conclusion;
import com.beawan.survey.loanInfo.bean.RiskAnalysis;
import com.beawan.survey.loanInfo.dao.IApplyInfoDAO;
import com.beawan.survey.loanInfo.dao.IApplySchemeDAO;
import com.beawan.survey.loanInfo.dao.IRiskAnalysisDAO;
import com.beawan.survey.loanInfo.dto.ApplySchemeDto;
import com.beawan.survey.loanInfo.service.ApplyRateInfoService;
import com.beawan.survey.loanInfo.service.BenefitAnalyService;
import com.beawan.survey.loanInfo.service.BenefitForecastService;
import com.beawan.survey.loanInfo.service.IConclusionSV;
import com.beawan.survey.project.dao.*;
import com.beawan.survey.project.dto.SubProjectDto;
import com.beawan.survey.project.entity.*;
import com.beawan.task.bean.Task;
import com.beawan.task.dao.ITaskDAO;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.platform.util.DateUtil;
import com.platform.util.GsonUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.MapperUtil;
import com.platform.util.StringUtil;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zxh
 * @date 2020/8/6 10:20
 */
@Service("surveyReportService")
public class LbSurveyReportServiceImpl implements LbSurveyReportService {
	private static final Logger log = Logger.getLogger(LbSurveyReportServiceImpl.class);

    @Resource
    private ITaskDAO taskDAO;
    @Resource
    private ISysDicDAO sysDicDAO;
    @Resource
    private ICompRunDAO compRunDAO;
    @Resource
    private ICompManagerDAO compManagerDAO;
    @Resource
    public CompInvestmentDao compInvestmentDao;
    @Resource
    private ICompFinancingBankDAO compFinancingBankDAO;
    @Resource
    private ICompFinancingExtGuaraDAO compFinancingExtGuaraDAO;
    @Resource
    private CompInnerManageDao compInnerManageDao;
    @Resource
    private CompRunAnalysisDao compRunAnalysisDao;
    @Resource
    private CompRunSupplyDao compRunSupplyDao;
    @Resource
    private CompProductDao compProductDao;
    @Resource
    private CompPowerDao compPowerDao;
    @Resource
    private CompProductSalesDao compProductSalesDao;
    @Resource
    private GuaCompanyDao guaCompanyDao;
    @Resource
    private CompFinanceInfoDao compFinanceInfoDao;
    @Resource
    private GuaPersonDao guaPersonDao;
    @Resource
    private GuaEstateDao guaEstateDao;
    @Resource
    private GuaChattelDao guaChattelDao;
    @Resource
    private GuaAccountsDao guaAccountsDao;
    @Resource
    private GuaDepositReceiptDao guaDepositReceiptDao;
    @Resource
    private GuaAnalysisDao guaAnalysisDao;
    @Resource
    private CompContributeDao compContributeDao;
    @Resource
    private IRiskAnalysisDAO riskAnalysisDAO;
    @Resource
    private IFnTableDao fnTableDao;
    @Resource
    private ICompFinanceTaxDAO compFinanceTaxDAO;
    @Resource
    private IApplyInfoDAO applyInfoDAO;
    @Resource
    private IApplySchemeDAO applySchemaDAO;
    @Resource
    private ICompFinanceDAO compFinanceDAO;
    @Resource
    private CompEquipmentDao compEquipmentDao;
    @Resource
    private CompRawMaterialDao compRawMaterialDao;
    @Resource
    private CompSalesStatDao compSalesStatDao;
    @Resource
    private ProjectBaseDao projectBaseDao;
    @Resource
    private InvestScaleDao investScaleDao;
    @Resource
    private FundsInputDao fundsInputDao;
    @Resource
    private FundsUseDao fundsUseDao;
    @Resource
    private DevicePurchaseDao devicePurchaseDao;
    @Resource
    private ProjectCostDao projectCostDao;
    @Resource
    private BusinessManageDao businessManageDao;
    @Resource
    private MultiBusinessDao multiBusinessDao;
    @Resource
    private InvestProjectDao investProjectDao;
    @Resource
    private ManageInfoDao manageInfoDao;
    @Resource
    private BusinessInfoDao businessInfoDao;
    @Resource
    private FinishProjectDao finishProjectDao;
    @Resource
    private StartProjectDao startProjectDao;
    @Resource
    private PlanProjectDao planProjectDao;
    @Resource
    private LoanProjectDao loanProjectDao;
    @Resource
    private CreditInfoDao creditInfoDao;
    @Resource
    private MajorItemDao majorItemDao;
    @Resource
    private CompCreditDao compCreditDao;
    @Resource
    private CompUserCreditDao compUserCreditDao;
    @Resource
	private ICompFinancingSV compFinancingSV;
    @Resource
    private FinriskResultService finriskResultService;
    @Resource
    private IAbnormalService abnormalSV;
    @Resource
    private IConclusionSV conclusionSV;
    @Resource
    private ICompBaseSV compBaseSV;
    @Resource
    private ICompFinanceSV compFinanceSV;
    @Resource
    private ICusFSToolSV cusFSToolSV;
    @Resource
    public FnVerifyDao fnVerifyDao;
	@Resource
	private CompJudgmentService compJudgmentService;
    @Resource
	private compRunAnalysisService compAnalysisService;
    @Resource
    private BuildingAnalysisDao buildingAnalysisDao;
    @Resource
	private ICompConnectSV compConnectSV;
    @Resource
	private ApplyRateInfoService applyRateInfoService;
	@Resource
	private BenefitForecastService benefitForecastService;
	@Resource
	private BenefitAnalyService benefitAnalyService;

	//6种模板，不同组装页面
    @Override
    public Map<String, Object> getReportData(String serNo) throws Exception {
        Task task = taskDAO.getTaskBySerNo(serNo);//根据贷前任务表的流水号查询任务当前信息
        if(task==null) return null;
        String loanType = task.getBusinessNo();

        Map<String, Object> dataMap = new HashMap<>();;
        String templateName = "", viewName = "", examineName = "";

        //loanType = Constants.LB_SURVEY_TYPE.BANK;//测试新的模板(官渡银行)

        if (Constants.LB_SURVEY_TYPE.COMMON.equals(loanType)//00 通用贷款类型
                || Constants.LB_SURVEY_TYPE.MANUFACTURE.equals(loanType)//04 流动资金贷款（制造业）
                || Constants.LB_SURVEY_TYPE.FOOD.equals(loanType)){//06 流动资金贷款（农副食品加工业）
            viewName = "views/survey/report/commonSurveyReport";//视图名字
            examineName = "views/survey/report/view/commonSurveyReport";//贷款名字
            templateName = "loan_statistics/lb/commonSurveyReport.htm";
            dataMap = this.getGeneralData(task);
        }else if(Constants.LB_SURVEY_TYPE.TEXTILE.equals(loanType)){/** 03 流动资金贷款（纺织业） **/
            viewName = "views/survey/report/textileSurveyReport";
            examineName = "views/survey/report/view/textileSurveyReport";
            templateName = "loan_statistics/lb/textileSurveyReport.htm";
            dataMap = this.getTextileData(task);
        }else if(Constants.LB_SURVEY_TYPE.COMMERCIAL_TRADE.equals(loanType)){/** 01 流动资金贷款（商贸类）**/
            viewName = "views/survey/report/businessSurveyReport";
            examineName = "views/survey/report/view/businessSurveyReport";
            templateName = "loan_statistics/lb/businessSurveyReport.htm";
            dataMap = this.getCommercialTradeData(task);
        }else if(Constants.LB_SURVEY_TYPE.PROJECT.equals(loanType)){/** 02 流动资金贷款（项目类） **/
            viewName = "views/survey/report/projectSurveyReport";
            examineName = "views/survey/report/view/projectSurveyReport";
            templateName = "loan_statistics/lb/projectSurveyReport.htm";
            dataMap = this.getProjectData(task);
        }else if(Constants.LB_SURVEY_TYPE.BUILDING.equals(loanType)){/** 05 流动资金贷款（建筑业） **/
            viewName = "views/survey/report/buildingSurveyReport";
            examineName = "views/survey/report/view/buildingSurveyReport";
            templateName = "loan_statistics/lb/buildingSurveyReport.htm";
            dataMap = this.getBuildingData(task);
        } else if(Constants.LB_SURVEY_TYPE.BANK.equals(loanType)){/** 07 流动资金贷款 (银行) **/
            viewName = "views/survey/report/bankSurveyReport";
            examineName = "views/survey/report/view/bankSurveyReport";
            templateName = "loan_statistics/lb/bankSurveyReport.htm";//这3个只用到了这个
            dataMap = this.getBankData(task);
        }

        dataMap.put("viewName", viewName);
        dataMap.put("examineName", examineName);
        dataMap.put("templateName", templateName);
        return dataMap;
    }

    /**RiskAnalysis
     * 获取 纺织业 调查模板数据
     * @param task 任务对象
     * @return
     */
    private Map<String, Object> getTextileData(Task task) throws Exception {
        Map<String, Object> dataMap = this.getGeneralData(task);
        // 生产环节 生产设备
        List<CompEquipment> equipmentList = compEquipmentDao.selectByProperty("serNo", task.getSerNo());
        dataMap.put("equipmentList", equipmentList);
        return dataMap;
    }

    /**
     * 获取（通用、制造业、农副食品加工业）调查模板数据
     * @param task 任务对象
     * @return
     */
    private Map<String, Object> getGeneralData(Task task) throws Exception {
        Map<String, Object> dataMap = this.getCommonData(task);
        Long taskId = task.getId();
        String serNo = task.getSerNo();
        String customerNo = task.getCustomerNo();
        
        // 经营场所情况
//        List<CompRunArea> runAreaList = compRunDAO.queryRunAreaByTIdAndNo(taskId, customerNo);
//        dataMap.put("runAreas", runAreaList);
        List<CompRunAreaTd> runAreaLandList = compRunDAO.queryRunAreaLandByTIdAndNo(taskId, customerNo);
        List<CompRunAreaCf> runAreaPlantList = compRunDAO.queryRunAreaPlantByTIdAndNo(taskId, customerNo);
        dataMap.put("runAreasLand", runAreaLandList);
        dataMap.put("runAreasPlant", runAreaPlantList);
        // 内部管理情况
        CompInnerManage innerManage = compInnerManageDao.selectSingleByProperty("serNo", serNo);
        dataMap.put("manageInfo", innerManage);
        
        // 与金融机构合作关系
        dataMap.put("MFinancingList", this.getFinancingList(taskId, customerNo, "M"));
        // 企业融资情况
        dataMap.put("financingInfos", this.getFinancingList(taskId, customerNo, "O"));
        
//        lawXPResult

        // 行业状况 分析评价
        dataMap.put("compRunInfo", this.getRunInfo(serNo));
        // 采购环节 主要供应商
        List<CompRunSupply> supplyList = compRunSupplyDao.selectByProperty("serNo", serNo);
        Collections.sort(supplyList, new Comparator<CompRunSupply>() {
			@Override
			public int compare(CompRunSupply arg0, CompRunSupply arg1) {
				if (arg1.getCurrPurchaseAmt() != null && arg0.getCurrPurchaseAmt() != null){
					return (int) (arg1.getCurrPurchaseAmt() - arg0.getCurrPurchaseAmt());
				}else {
					return 0;
				}
				
			}
		});
        dataMap.put("supplies", supplyList);
        // 生产环节 主要产品
        List<CompRunProduct> productList = compProductDao.selectByProperty("serNo", serNo);
        dataMap.put("products", productList);
        // 生产环节 能耗详情
        dataMap.put("powers", this.getPowerInfo(serNo));
        // 销售环节 产品销售详情
        List<CompProductSales> salesList = compProductSalesDao.selectByProperty("serNo", serNo);
        Collections.sort(salesList, new Comparator<CompProductSales>() {
			@Override
			public int compare(CompProductSales arg0, CompProductSales arg1) {
				if (arg1.getYearSales() != null && arg0.getYearSales() != null){
					return (int) (arg1.getYearSales() - arg0.getYearSales());
				}else {
					return 0;
				}
				
			}
		});
        dataMap.put("productSales", salesList);
        // 综合贡献度分析
        CompContribute contribute = compContributeDao.selectSingleByProperty("serNo", serNo);
        dataMap.put("contribute", contribute);
        return dataMap;
    }

    
    private void forGuarTyp(List<CompFinancingBank> banks) {
    	if(CollectionUtils.isEmpty(banks))
    		return;
    	for(CompFinancingBank bank : banks){
        	try{
        		

				String guaTypes = bank.getGuaranteeType();
                if(!StringUtil.isEmptyString(guaTypes)){
                    StringBuilder guaType = new StringBuilder();
                    String[] typeList = guaTypes.split(",");
                    for (String type : typeList) {
                        SysDic guaTypeDic = sysDicDAO.queryByEnNameAndOptType(
                                type, SysConstants.BsDicConstant.STD_SY_GUAR_TYPE);
                        guaType.append(guaTypeDic.getCnName()).append("，");
                    }
                    bank.setGuaranteeType(guaType.substring(0, guaType.length()-1));
                }	

        		bank.setFiveClass(LfConstants.getLfFiveNotoFiveName(bank.getFiveClass()));
        		
        	}catch(Exception e){
        		log.error("格式化企业融资关系中的担保类型时，出现异常");
        	}
        }
    }
    
    private void forGuarFiveTyp(List<LbExternalGuaDto> banks) {
    	if(CollectionUtils.isEmpty(banks))
    		return;
    	for(LbExternalGuaDto bank : banks){
        	try{
        		bank.setFiveClass(LfConstants.getLfFiveNotoFiveName(bank.getFiveClass()));
        	}catch(Exception e){
        		log.error("格式化担保五级分类时，出现异常");
        	}
        }
    }
    /**
     * 获取 商贸类 调查模板数据
     * @param task 任务对象
     * @return
     */
    private Map<String, Object> getCommercialTradeData(Task task) throws Exception {
        Map<String, Object> dataMap = this.getCommonData(task);//成功
        Long taskId = task.getId();
        String serNo = task.getSerNo();
        String customerNo = task.getCustomerNo();
    
        // 经营场所情况
//      List<CompRunArea> runAreaList = compRunDAO.queryRunAreaByTIdAndNo(taskId, customerNo);
//      dataMap.put("runAreas", runAreaList);
      List<CompRunAreaTd> runAreaLandList = compRunDAO.queryRunAreaLandByTIdAndNo(taskId, customerNo);
      List<CompRunAreaCf> runAreaPlantList = compRunDAO.queryRunAreaPlantByTIdAndNo(taskId, customerNo);
      dataMap.put("runAreasLand", runAreaLandList);
      dataMap.put("runAreasPlant", runAreaPlantList);
        // 内部管理情况
        CompInnerManage innerManage = compInnerManageDao.selectSingleByProperty("serNo", serNo);
        dataMap.put("manageInfo", innerManage);
        // 与金融机构合作关系
        dataMap.put("MFinancingList", this.getFinancingList(taskId, customerNo, "M"));
        // 企业融资情况
        dataMap.put("financingInfos", this.getFinancingList(taskId, customerNo, "O"));
        
        // 行业状况 分析评价
        dataMap.put("compRunInfo", this.getRunInfo(serNo));
        // 采购环节 主要原材料(商贸类)
        LbRawMaterialDto rawMaterial = this.getRawMaterial(serNo);
        dataMap.put("rawMaterial", rawMaterial);
        // 销售环节 产品销售详情
        LbProductSalesDto salesData = this.getSalesData(serNo);
        dataMap.put("productSales", salesData);
        // 销售环节 销售收入统计
        List<CompSalesStat> salesStatList = compSalesStatDao.selectByProperty("serNo", serNo);
        dataMap.put("salesStat", salesStatList);
        return dataMap;
    }

    /**
     * 获取 建筑业 调查模板数据
     * @param task 任务对象
     * @return
     */
    private Map<String, Object> getBuildingData(Task task) throws Exception {
        Map<String, Object> dataMap = this.getCommonData(task);
        Long taskId = task.getId();
        String serNo = task.getSerNo();
        String customerNo = task.getCustomerNo();
        
        // 多元业务
        List<MultiBusiness> businessList = multiBusinessDao.selectByProperty("serNo", serNo);
        dataMap.put("multiBusiness", businessList);
        // 多元化投资项目
        List<InvestProject> projectList = investProjectDao.selectByProperty("serNo", serNo);
        dataMap.put("investProject", projectList);
        // 内部管理情况
        ManageInfo manageInfo = manageInfoDao.selectSingleByProperty("serNo", serNo);
        dataMap.put("manageInfo", manageInfo);
        // 经营情况
        BusinessInfo businessInfo = businessInfoDao.selectSingleByProperty("serNo", serNo);
        dataMap.put("businessInfo", businessInfo);

        // 近三年完工项目情况
        List<FinishProject> finishProjectList = finishProjectDao.selectByProperty("serNo", serNo);
        dataMap.put("finishProject", finishProjectList);
        // 在建项目情况
        List<StartProject> startProjectList = startProjectDao.selectByProperty("serNo", serNo);
        dataMap.put("startProject", startProjectList);
        // 已中标未开工项目情况
        List<PlanProject> planProjectList = planProjectDao.selectByProperty("serNo", serNo);
        dataMap.put("planProject", planProjectList);
        // 近三年申贷项目情况
        List<LoanProject> loanProjectList = loanProjectDao.selectByProperty("serNo", serNo);
        dataMap.put("loanProject", loanProjectList);
        // 项目情况说明
        BuildingAnalysis buildingAnalysis = buildingAnalysisDao.selectSingleByProperty("serNo", serNo);	
        dataMap.put("buildingAnalysis", buildingAnalysis);
        // 借款人授信情况
        List<CompFinancingBank> borrowerCreditList =
                this.getCreditInfo(taskId, customerNo, Constants.CM_CREDIT.BORROWER);
        dataMap.put("borrower", borrowerCreditList);
        // 关联方授信情况
        List<CompFinancingBank> otherCreditList =
                this.getCreditInfo(taskId, customerNo, Constants.CM_CREDIT.RELATED_PARTY);
        dataMap.put("linkedParty", otherCreditList);
        // 企业重大事项
        MajorItem majorItem = majorItemDao.selectSingleByProperty("serNo", serNo);
        dataMap.put("majorItem", majorItem);

        // 综合贡献度分析
        CompContribute contribute = compContributeDao.selectSingleByProperty("serNo", serNo);
        dataMap.put("contribute", contribute);
        return dataMap;
    }

    /**
     * 获取 项目类 调查模板数据
     * @param task 任务对象
     * @return
     */
    private Map<String, Object> getProjectData(Task task) throws Exception {
        Map<String, Object> dataMap = this.getCommonData(task);
        Long taskId = task.getId();
        String serNo = task.getSerNo();
        String customerNo = task.getCustomerNo();

        // 经营场所情况
//      List<CompRunArea> runAreaList = compRunDAO.queryRunAreaByTIdAndNo(taskId, customerNo);
//      dataMap.put("runAreas", runAreaList);
      List<CompRunAreaTd> runAreaLandList = compRunDAO.queryRunAreaLandByTIdAndNo(taskId, customerNo);
      List<CompRunAreaCf> runAreaPlantList = compRunDAO.queryRunAreaPlantByTIdAndNo(taskId, customerNo);
      dataMap.put("runAreasLand", runAreaLandList);
      dataMap.put("runAreasPlant", runAreaPlantList);

        // 项目概况 介绍
        ProjectBase projectBase = projectBaseDao.selectSingleByProperty("serNo", serNo);
        dataMap.put("projectInfo", projectBase);
        // 项目投资规模
        LbInvestScaleDto investScale = this.getInvestScale(serNo);
        dataMap.put("investScale", investScale);
        // 资金投入情况
        List<FundsInput> fundsInput = fundsInputDao.selectByProperty("serNo", serNo);
        dataMap.put("fundsInputList", fundsInput);
        // 资金使用明细
        List<FundsUse> fundsUse = fundsUseDao.selectByProperty("serNo", serNo);
        dataMap.put("fundsUseList", fundsUse);
        // 设备购置情况(设备购置费估算)
        List<DevicePurchase> deviceList = devicePurchaseDao.selectByProperty("serNo", serNo);
        dataMap.put("deviceList", deviceList);
        // 工程费用明细(工程费用指标)
        List<ProjectCost> costList = projectCostDao.selectByProperty("serNo", serNo);
        dataMap.put("projectCostList", costList);

        // 与金融机构合作关系
        dataMap.put("MFinancingList", this.getFinancingList(taskId, customerNo, "M"));
        // 企业融资情况
        dataMap.put("financingInfos", this.getFinancingList(taskId, customerNo, "O"));
        // 涉诉纠纷情况

        // 行业状况 分析评价
        LbCompRunInfoDto runInfo = this.getRunInfo(serNo);
        dataMap.put("compRunInfo", runInfo);
        // 经营管理情况
        BusinessManage manageInfo = businessManageDao.selectSingleByProperty("serNo", serNo);
        dataMap.put("businessManage", manageInfo);
        
        BenefitForecast benefitForecast = benefitForecastService.selectSingleByProperty("taskId", taskId);
        BenefitAnaly benefitAnaly = benefitAnalyService.selectSingleByProperty("taskId", taskId);
        dataMap.put("benefitAnaly", benefitAnaly);
        if (benefitForecast != null && !StringUtil.isEmpty(benefitForecast.getTableCols())){
        	List<String> tableCols = GsonUtil.GsonToList(benefitForecast.getTableCols(), String.class);
            List<Map<String, Object>> tableData = GsonUtil.GsonToListMaps(benefitForecast.getTableData());
            dataMap.put("benefitForecast", benefitForecast);
            dataMap.put("tableCols", tableCols);
            dataMap.put("tableData", tableData);
        }
       
//    	private BenefitForecastService benefitForecastService;
//    	private BenefitAnalyService benefitAnalyService;
        return dataMap;
    }

    /**
     * 获取 官渡银行 调查模板数据
     * @param task 任务对象
     * @return
     */
    private Map<String, Object> getBankData(Task task) throws Exception {
        Map<String, Object> dataMap = this.getCommonData(task);
        Long taskId = task.getId();
        String serNo = task.getSerNo();
        String customerNo = task.getCustomerNo();

        // 多元业务
        List<MultiBusiness> businessList = multiBusinessDao.selectByProperty("serNo", serNo);
        dataMap.put("multiBusiness", businessList);
        // 多元化投资项目
        List<InvestProject> projectList = investProjectDao.selectByProperty("serNo", serNo);
        dataMap.put("investProject", projectList);
        // 内部管理情况
        ManageInfo manageInfo = manageInfoDao.selectSingleByProperty("serNo", serNo);
        dataMap.put("manageInfo", manageInfo);
        // 经营情况
        BusinessInfo businessInfo = businessInfoDao.selectSingleByProperty("serNo", serNo);
        dataMap.put("businessInfo", businessInfo);

        // 近三年完工项目情况
        List<FinishProject> finishProjectList = finishProjectDao.selectByProperty("serNo", serNo);
        dataMap.put("finishProject", finishProjectList);
        // 在建项目情况
        List<StartProject> startProjectList = startProjectDao.selectByProperty("serNo", serNo);
        dataMap.put("startProject", startProjectList);
        // 已中标未开工项目情况
        List<PlanProject> planProjectList = planProjectDao.selectByProperty("serNo", serNo);
        dataMap.put("planProject", planProjectList);
        // 近三年申贷项目情况
        List<LoanProject> loanProjectList = loanProjectDao.selectByProperty("serNo", serNo);
        dataMap.put("loanProject", loanProjectList);
        // 项目情况说明
        BuildingAnalysis buildingAnalysis = buildingAnalysisDao.selectSingleByProperty("serNo", serNo);
        dataMap.put("buildingAnalysis", buildingAnalysis);
        // 借款人授信情况
        List<CompFinancingBank> borrowerCreditList =
                this.getCreditInfo(taskId, customerNo, Constants.CM_CREDIT.BORROWER);
        dataMap.put("borrower", borrowerCreditList);
        // 关联方授信情况
        List<CompFinancingBank> otherCreditList =
                this.getCreditInfo(taskId, customerNo, Constants.CM_CREDIT.RELATED_PARTY);
        dataMap.put("linkedParty", otherCreditList);
        // 企业重大事项
        MajorItem majorItem = majorItemDao.selectSingleByProperty("serNo", serNo);
        dataMap.put("majorItem", majorItem);

        // 综合贡献度分析
        CompContribute contribute = compContributeDao.selectSingleByProperty("serNo", serNo);
        dataMap.put("contribute", contribute);
        return dataMap;
    }

    /**
     * 获取 公共部分信息（所有模板）
     * @param task 任务对象
     * @return
     */
    private Map<String, Object> getCommonData(Task task) throws Exception {
        Map<String, Object> dataMap = new HashMap<>();
        Long taskId = task.getId();
        String serNo = task.getSerNo();
        String customerNo = task.getCustomerNo();
        //customerNo = "20201023162649";

        String applicationTime = task.getApplicationTime().substring(0, 9);
        String customerName = task.getCustomerName();

        //客户信息说明
        CompBaseDesc compBaseDesc = compBaseSV.findCompBaseDescByCustNo(customerNo);
        dataMap.put("compBaseDesc", compBaseDesc);
        // 企业概况 企业基本情况
        LbCompBaseDto compBase = this.getCompBaseInfo(customerNo);//
        dataMap.put("compBase", compBase);
        //股权信息
        List<CompBaseEquity> equities = compBaseSV.findCompEquityByCustNo(customerNo);
//       List<CompBaseEquity> quity = new ArrayList();
//       MyCompartor mc = new MyCompartor();
        Collections.sort(equities, new Comparator<CompBaseEquity>() {

            @Override
            public int compare(CompBaseEquity arg0, CompBaseEquity arg1) {
                return (int) (Double.valueOf((arg1.getFundAmount()))-(Double.valueOf(arg0.getFundAmount())));
            }
        });
        dataMap.put("equities", equities);
        // 法定代表人/实际控制人 (建筑业没有这块内容)
        List<String> personList = new LinkedList<>();

/*       if (compBase.getLegalPerson()!=null){
           String legalPerson = compBase.getLegalPerson();///////!!!!!
       }
       if (compBase.getControlPerson()!=null){
           String controlPerson = compBase.getControlPerson();
       }*/
        if (compBase!=null) {
            String legalPerson = compBase.getLegalPerson();
            if (!StringUtil.isBlank(compBase.getLegalPerson()))
                personList.add(compBase.getLegalPerson());
            if (!StringUtil.isBlank(compBase.getControlPerson()) && !compBase.getControlPerson().equals(compBase.getLegalPerson()))
                personList.add(compBase.getControlPerson());
        }

//       dataMap.put("managerInfos", this.getManagerInfo(customerNo, personList));   //不显示高管信息
        // 前五大对外投资企业
        dataMap.put("linkedComps", this.getLinkedComp(customerNo));
        // 资信状况--企业征信
        List<CompCredit> compCreditList = this.getCompCredit(serNo, null);
        List<CompCreditDto> compCreditDtoList = MapperUtil.trans(compCreditList, CompCreditDto.class);
        if(!CollectionUtils.isEmpty(compCreditDtoList)){
            for(CompCreditDto compCredit : compCreditDtoList){
                if(!StringUtil.isEmptyString(compCredit.getRelevanceType())){
                    SysDic dicBusiType = sysDicDAO.queryByEnNameAndOptType(
                            compCredit.getRelevanceType(), SysConstants.BsDicConstant.STD_REL_ENT_TYPE);
                    if(dicBusiType!=null){
                        compCredit.setRelevanceType(dicBusiType.getCnName());
                    }
                }
            }
        }
        dataMap.put("compCredit", compCreditDtoList);

        // 个人征信
        List<CompUserCredit> userCreditList = this.getUserCredit(serNo, null);
        List<CompUserCreditDto> userCreditDtoList = MapperUtil.trans(userCreditList, CompUserCreditDto.class);
        if(!CollectionUtils.isEmpty(userCreditDtoList)){
            for(CompUserCreditDto userCredit : userCreditDtoList){
                if(!StringUtil.isEmptyString(userCredit.getRelevanceType())){
                    SysDic dicBusiType = sysDicDAO.queryByEnNameAndOptType(
                            userCredit.getRelevanceType(), SysConstants.BsDicConstant.STD_REL_IND_TYPE);
                    if(dicBusiType!=null){
                        userCredit.setRelevanceType(dicBusiType.getCnName());
                    }
                }
            }
        }
        dataMap.put("userCredit",userCreditDtoList);
        //集团客户或关联企业信息11
        List<CompConnetCm> compConnetCmList = compConnectSV.findCompConnectCmByCustNo(customerNo);
        List<CompConnetCmDto> compConnetCmDtoList = MapperUtil.trans(compConnetCmList, CompConnetCmDto.class);
        if(!CollectionUtils.isEmpty(compConnetCmDtoList)){
            for(CompConnetCmDto entity : compConnetCmDtoList){
                if(!StringUtil.isEmptyString(entity.getRelationship())){
                    SysDic dicBusiType = sysDicDAO.queryByEnNameAndOptType(
                            entity.getRelationship(), SysConstants.BsDicConstant.STD_SY_CL);
                    if(dicBusiType!=null){
                        entity.setRelationship(dicBusiType.getCnName());
                    }
                }
            }
        }
        dataMap.put("CompConnetCmList", compConnetCmDtoList);
        // 对外担保情况
        List<LbExternalGuaDto> externalGua = this.getExternalGua(taskId, customerNo);
        this.forGuarFiveTyp(externalGua);
        dataMap.put("guaranteeInfos", externalGua);
        // 涉诉纠纷情况11
        List<CompJudgmentDto> newestJudgeList = compJudgmentService.getNewestJudgeList(applicationTime, customerNo, customerName);
        String judgSimAnaly = compJudgmentService.getJudgSimAnaly(applicationTime, customerNo, customerName);
        dataMap.put("newestJudgeList", newestJudgeList);
        //对企查查司法信息统计分析----》这里注意区分  客户经理自己匀汇法网查询数据 手动填写在信息
        dataMap.put("judgSimAnaly", judgSimAnaly);
        // 融资负债情况分析 --->包含汇法网信息
        CompFinancing financeEntity = compFinancingSV.findCompFinancingByTIdAndNo(taskId, customerNo);
        dataMap.put("financeInfo", financeEntity);
        //获取申请利率信息
        ApplyRateInfo rate = applyRateInfoService.selectSingleByProperty("taskId", taskId);
        dataMap.put("rate", rate);
        // 财务状况 基本财务分析
        dataMap.put("compFinance", this.getFinanceAnaly(taskId, customerNo, serNo));
        // 财务分析主要指标（简表） 拿不到数据，要根据cmis查询
        //Map<String, Object> finaImportData = this.getFinaImportData(taskId, customerNo);
        Map<String, Object> finaImportData = new HashMap<>();
        dataMap.put("finaImportData", finaImportData);

        //四力分析所需的财务比率指标
        List<FinaRatioDto> ratioList = finriskResultService.getRatio(serNo);
        if(!CollectionUtils.isEmpty(ratioList)){
            Map<String, List<FinaRatioDto>> ratioMap = ratioList.stream().collect(Collectors.groupingBy(FinaRatioDto::getRatioType));
            dataMap.put("ratioMap", ratioMap);
        }

        //现金流数据
        dataMap.put("cashflowData", finriskResultService.getLocalCashflowAnalyData(serNo));
        int year = task.getYear();
        int month = task.getMonth();
        String[] years = new String[3];
        years[2] = year + "年" + (month<10?"0"+month:""+month)+"月";
        for(int i=1; i >=0; i--){
            --year;
            years[i] = year + "年";
        }
        dataMap.put("years", years);

        //重要财务科目获取 （财务附注及核实）
        //各个重要科目的账面值
        Map<String, String> balanceData = this.getNowBalanceData(finaImportData);
        //各个重要指标说明信息
      /*  List<ErrData> errList = abnormalSV.getItemErrorDatas(task.getId());

        Map<String, String> errExplainMap = new HashMap<>();
        if(!CollectionUtils.isEmpty(errList)){
            for(ErrData errData : errList){
                errExplainMap.put(errData.getName(), errData.getExplain());
            }
        }
        dataMap.put("errExplainMap", errExplainMap);*/
        Map<String, String> errExplainMap = new HashMap<>();
        errExplainMap.put("errName","没找到errData表");
        dataMap.put("errExplainMap", errExplainMap);

        //货币资金
        List<FnCash> cashList = fnTableDao.queryFnCashByTId(taskId);
        if(cashList!=null && cashList.size()!=0){
            Double sum = 0d;
            for(FnCash cash : cashList){
                //字典项 1=现金 2=存款 3保证金
                if("3".equals(cash.getCashType())){
                    sum += cash.getAmount();
                }
            }
            balanceData.put("保证金", String.valueOf(sum));
        }
        dataMap.put("cashList", cashList);
        dataMap.put("nowBanlanceData", balanceData);

        //存货
        dataMap.put("inventoryList", fnTableDao.queryFnInventoryByTId(taskId));
        //应收账款
        dataMap.put("receiveList", fnTableDao.queryReceiveByTId(taskId));
        //其他应收款
        dataMap.put("otherReceiveList", fnTableDao.queryOtherReceiveByTId(taskId));
        //固定资产分三块：土地
        dataMap.put("landList", fnTableDao.queryFnFixedAssetsLandByTId(taskId));
        //房屋
        dataMap.put("buildingList", fnTableDao.queryFnFixedAssetsBuildingByTId(taskId));
        //设备
        dataMap.put("equipmentList", fnTableDao.queryFnFixedAssetsEquipmentByTId(taskId));
        //应付账款
        dataMap.put("payList", fnTableDao.queryPayByTId(taskId));
        //其他应付款
        dataMap.put("otherPayList", fnTableDao.queryOtherPayByTId(taskId));

        //获取纳税信息：缴税信息
        int taxYear = task.getYear();
        int taxMonth = task.getMonth();
        String[] taxDateList = new String[4];
        taxDateList[3] = taxYear + "年" + (taxMonth<10?"0":""+taxMonth)+"月";
        for(int i=2; i >=0; i--){
            --taxYear;
            taxDateList[i] = taxYear + "年";
        }
        List<CompFinanceTax> financeTaxes = compFinanceTaxDAO.queryByTaskIdAndCustNo(taskId, customerNo);
        if(financeTaxes!=null && financeTaxes.size()!=0){
            CompFinanceTax financeTax = financeTaxes.get(0);
            dataMap.put("financeTax", financeTax);
            dataMap.put("taxDateList", taxDateList);
        }

        // 担保措施 担保方式统计
        LinkedList<LbGuaWayStat> guaWAyList = new LinkedList<>();
        // 第三方企业保证担保、财务状况
        List<LbGuaCompany> companyList = this.getGuaCompanyInfo(serNo);
        if (!CollectionUtils.isEmpty(companyList)){
            double guaAmount = 0d;
            String guaInfo = "";
            //担保企业   征信极其法人征信
            List<CompCredit> guarCompCreditList = new ArrayList<>();
            List<CompUserCredit> guarUserCreditList = new ArrayList<>();
            for(LbGuaCompany comp : companyList){

                guaInfo += "、" + comp.getCompanyName();
                List<CompCredit> compCredit = this.getCompCredit(serNo, comp.getId()+"");
                if(!CollectionUtils.isEmpty(compCredit)){
                    guarCompCreditList.addAll(compCredit);
                }
                List<CompUserCredit> userCredit = this.getUserCredit(serNo, comp.getId()+"");
                if(!CollectionUtils.isEmpty(userCredit)){
                    guarUserCreditList.addAll(userCredit);
                }

                Double guaranteeAmt = comp.getGuaranteeAmt();
                if(guaranteeAmt==null){
                    continue;
                }
                guaAmount += guaranteeAmt;
            }
            guaInfo = guaInfo.substring(1);
            guaInfo += "保证担保";
            guaWAyList.add(new LbGuaWayStat(guaAmount, guaInfo));
            // 资信状况-- 担保企业征信
            dataMap.put("guarCompCreditList", guarCompCreditList);
            // 担保企业  个人征信
            dataMap.put("guarUserCreditList", guarUserCreditList);
        }
        dataMap.put("guaCompanies", companyList);

        // 自然人保证担保
        List<GuaPerson> guaPersonList = guaPersonDao.selectByProperty("serNo", serNo);
        if (!CollectionUtils.isEmpty(guaPersonList)){
            guaWAyList.add(new LbGuaWayStat(
                    guaPersonList.stream().mapToDouble(GuaPerson::getGuaranteeAmt).sum(),
                    "自然人担保保证"));
        }
        dataMap.put("guaPerson", guaPersonList);

        // 房地产抵押
        List<GuaEstate> estateList = guaEstateDao.selectByProperty("serNo", serNo);
        List<GuaEstateDto> estateDtoList = MapperUtil.trans(estateList, GuaEstateDto.class);
        double loanAmtSum = 0;
        if (!CollectionUtils.isEmpty(estateDtoList)){
            for(GuaEstateDto estate : estateDtoList){
                String mortgagorType = estate.getMortgagorType();
                //本身存的就是中文字典名称
                if(!StringUtils.isEmpty(mortgagorType)){
                    SysDic mortType = sysDicDAO.queryByCnNameAndOptType(mortgagorType, SysConstants.BsDicConstant.RG_ESTATE_TYPE);
                    if(mortType==null){
                        log.error("抵押的房地产类型："+mortgagorType + "在字典中不存在，请检查---异常任务号为："+taskId);
                    }else
                        estate.setMortgagorType(mortType.getCnName());
                }

//        		Double loanAmount = estate.getLoanAmount();
//        		if(loanAmount==null){
//        			continue;
//        		}
//        		loanAmtSum += loanAmount;
            }
            loanAmtSum = estateList.stream().mapToDouble(GuaEstate::getLoanAmount).sum();
            guaWAyList.add(new LbGuaWayStat(loanAmtSum, "房地产抵押"));
        }
        dataMap.put("guaEstates", estateDtoList);

        // 动产抵押
        dataMap.put("guaChattels", guaChattelDao.selectByProperty("serNo", serNo));
        // 应收账款质押
        dataMap.put("guaAccounts", guaAccountsDao.selectByProperty("serNo", serNo));
        // 存单质押
        dataMap.put("guaDepositReceipts", guaDepositReceiptDao.selectByProperty("serNo", serNo));
        // 抵、质押物担保 分析说明
        GuaAnalysis guaAnalysis = guaAnalysisDao.selectSingleByProperty("serNo", serNo);
        if (guaAnalysis != null && guaAnalysis.getChattelAmt() > 0){
            guaWAyList.add(new LbGuaWayStat(guaAnalysis.getChattelAmt(), "动产抵押"));
        }
        if (guaAnalysis != null && guaAnalysis.getAccountsAmt() > 0){
            guaWAyList.add(new LbGuaWayStat(guaAnalysis.getAccountsAmt(), "应收账款质押"));
        }
        if (guaAnalysis != null && guaAnalysis.getDepositReceiptAmt() > 0){
            guaWAyList.add(new LbGuaWayStat(guaAnalysis.getDepositReceiptAmt(), "存单质押"));
        }
        dataMap.put("guaAnalysis", guaAnalysis);
        // 担保方式统计
        dataMap.put("guaWayStatList", guaWAyList);

        // 融资额度与用途 获取流动资金测算表数据
        Map<String, Object> params = new HashMap<>();
        params.put("dgSerialNumber", serNo);
        params.put("platformType", Constants.Platform.LOCAL_FINRISK);
        List<FinriskResult> finriskResults = finriskResultService.selectByProperty(params);
        if (finriskResults != null && finriskResults.size()>0){
            String quotaInfo = finriskResults.get(0).getQuotaInfo();
            Map<String, Object> quotaMap = GsonUtil.GsonToMaps(quotaInfo);
            dataMap.put("quotaMap", quotaMap);
        }
        // 授信理由及用途
        ApplyInfo applyInfo = applyInfoDAO.queryApplyInfoByTaskId(taskId);
        dataMap.put("applyInfo", applyInfo);
        // 授信申请列表
        List<ApplySchemeDto> forApplySchema = this.getApplySchema(taskId);
        String hasAdd = "0";
        Map<String, Double> integration = new HashMap<String, Double>();
        if(!CollectionUtils.isEmpty(forApplySchema)){
            for(ApplySchemeDto applySchema : forApplySchema){
                if(!StringUtils.isEmpty(applySchema.getAddGrauatee()) && "0".equals(hasAdd)){
                    hasAdd = "1";
                }
                if(!StringUtils.isEmpty(applySchema.getBusinessType()) && applySchema.getCreditAmt() != null){
                    if(integration.containsKey(applySchema.getBusinessType())){
                        Double Sum = integration.get(applySchema.getBusinessType()) + applySchema.getCreditAmt();
                        integration.put(applySchema.getBusinessType(), Sum);
                    }else{
                        integration.put(applySchema.getBusinessType(), applySchema.getCreditAmt());
                    }
                }
            }
        }
        dataMap.put("integration", integration);

        dataMap.put("hasAdd", hasAdd);
        dataMap.put("applySchemeList", forApplySchema);




        // 风险分析及防范
        RiskAnalysis riskAnalysis = riskAnalysisDAO.selectSingleByProperty("serNo", serNo);
        dataMap.put("riskAnalysis", riskAnalysis);
        // 调查结论及意见
        Conclusion conclusion = conclusionSV.updateAndFindConclusionByTaskId(taskId);
        dataMap.put("surveyResult", conclusion);

        Double creditAmtSum = 0.00;
        Integer creditTermMax = 0;

        if(!CollectionUtils.isEmpty(forApplySchema)){
            for(ApplySchemeDto applySchema : forApplySchema){
                if (applySchema.getCreditAmt() != null){
                    creditAmtSum = creditAmtSum + applySchema.getCreditAmt();
                }
                if(applySchema.getCreditTerm() != null && creditTermMax < applySchema.getCreditTerm()){
                    creditTermMax = applySchema.getCreditTerm();
                }
            }
        }

        task.setLoanNumber(creditAmtSum);
        task.setLoanTerm(creditTermMax);
        taskDAO.saveOrUpdate(task);
        //授信基本信息
        if (task.getInstitutionName()!=null) {
            task.setInstitutionName(task.getInstitutionName().replace("江苏如皋农村商业银行", ""));
        }
        dataMap.put("task", task);
        return dataMap;
    }

    /**
     * 获取 授信申请列表
     * @param taskId
     * @return
     */
    private List<ApplySchemeDto> getApplySchema(long taskId){
    	List<ApplyScheme> schemesList = applySchemaDAO.queryApplySchemeListByTaskId(taskId);
    	if(CollectionUtils.isEmpty(schemesList)) return null;
    	List<ApplySchemeDto> entityList = MapperUtil.trans(schemesList, ApplySchemeDto.class);
    	try{
	    	for(ApplySchemeDto entity : entityList){

	    		String businessType = entity.getBusinessType();
	    		if(!StringUtil.isEmptyString(businessType)){
					SysDic dicBusiType = sysDicDAO.queryByEnNameAndOptType(
							businessType, SysConstants.BsDicConstant.RG_PRODUCT_LIST);
					if(dicBusiType!=null){
						entity.setBusinessType(dicBusiType.getCnName());
					}
				}
//	    		if(!StringUtil.isEmptyString(entity.getCreditAmtType())){
//					SysDic dicAmtType = sysDicDAO.queryByEnNameAndOptType(
//	    					entity.getCreditAmtType(), SysConstants.BsDicConstant.STD_ZB_LIMIT_TYPE);
//	    			entity.setCreditAmtType(dicAmtType.getCnName());
//				}

	    		String guaTypes = entity.getGrauateeWay();
                if(!StringUtil.isEmptyString(guaTypes)){
                    StringBuilder guaType = new StringBuilder();
                    String[] typeList = guaTypes.split(",");
                    for (String type : typeList) {
                        SysDic guaTypeDic = sysDicDAO.queryByEnNameAndOptType(
                                type, SysConstants.BsDicConstant.STD_SY_GUAR_TYPE);
                        guaType.append(guaTypeDic.getCnName()).append("，");
                    }
                    entity.setGrauateeWay(guaType.substring(0, guaType.length()-1));
                }
               
                if(!StringUtil.isEmptyString(entity.getAddGrauatee())){
					SysDic guaTypeDic = sysDicDAO.queryByEnNameAndOptType(
							entity.getAddGrauatee(), SysConstants.BsDicConstant.STD_SY_GUAR_TYPE);
					entity.setAddGrauatee(guaTypeDic.getCnName());
				}
                
				if(!StringUtil.isEmptyString(entity.getPaymentType())){
					SysDic dicPayType = sysDicDAO.queryByEnNameAndOptType(
							entity.getPaymentType(), SysConstants.BsDicConstant.STD_IQP_PAY_TYPE);
					entity.setPaymentType(dicPayType.getCnName());
				}
	    	}
    	}catch(Exception e){
			e.printStackTrace();
		}
    	return entityList;
    }
    /**
     * 根据客户号获取企业基本信息
     * @param customerNo 客户号
     * @return
     * @throws Exception
     */
    public LbCompBaseDto getCompBaseInfo(String customerNo) throws Exception {
        CompBaseDto baseInfo = compBaseSV.findCompBaseByCustNo(customerNo);
        if (baseInfo == null) return null;

        LbCompBaseDto compBaseDto = MapperUtil.trans(baseInfo, LbCompBaseDto.class);
        String registerCapital = baseInfo.getRegisterCapital() + "元";
        if(baseInfo.getRegisterCapital()>=100000){
        	registerCapital = (int)(baseInfo.getRegisterCapital() / 10000) + "万元";
        }
        compBaseDto.setRegisterCapital(registerCapital);
        String realCapital = baseInfo.getRealCapital() + "元";
        if(baseInfo.getRealCapital()>=100000){
        	realCapital = (int)(baseInfo.getRealCapital() / 10000) + "万元";
        }
        compBaseDto.setRealCapital(realCapital);

        SysDic accountType = sysDicDAO.queryByEnNameAndOptType(baseInfo.getMbankAccType(),
                SysConstants.BsDicConstant.STD_MB_ACC_TYPE);
        if(accountType!=null){
        	compBaseDto.setBankAccType(accountType.getCnName());
        }
        SysDic policyType = sysDicDAO.queryByEnNameAndOptType(baseInfo.getIndusPolicy(),
                SysConstants.BsDicConstant.STD_INDUSTRY_POLICY);
        if(policyType!=null){
            compBaseDto.setIndusPolicy(policyType.getCnName());
        }

        //  获取股权结构信息
        List<CompBaseEquity> equities = compBaseSV.findCompEquityByCustNo(customerNo);
        if(equities == null || equities.size() <= 0) return compBaseDto;

        List<String> equityInfo = new LinkedList<>();
        for (CompBaseEquity equity : equities) {
            String rate = equity.getFundRate();
            rate = rate.contains("%") ? rate : rate + "%";
            if (!StringUtil.isEmpty(equity.getInvtFactAmt())){
                equityInfo.add(equity.getStockName() + "出资" + equity.getFundAmount() + "万元，实际缴纳出资"+equity.getInvtFactAmt()+"万元，占比" + rate + "；");
            } else {
            	equityInfo.add(equity.getStockName() + "出资" + equity.getFundAmount() + "万元，占比" + rate + "；");
            }
        }
        //报告企业成立时间
        if(!StringUtils.isEmpty(compBaseDto.getFoundDate())){
        	if(compBaseDto.getFoundDate().length() > 10){
        		compBaseDto.setFoundDate(compBaseDto.getFoundDate().substring(0, 10));
        	}
        }
        
        compBaseDto.setEquity(equityInfo);
        return compBaseDto;
    }

    /**
     * 根据姓名获取高管信息
     * @param nameList 高管姓名列表
     * @return
     * @throws Exception
     */
    public List<LbManagerInfoDto> getManagerInfo(String customerNo, List<String> nameList) throws Exception {
        StringBuilder sql = new StringBuilder();
        Map<String, Object> params = new HashMap<>();
        sql.append("SELECT pb.NAME name, pb.BIRTH_DATE birthday, pb.BIRTH_PLACE birthPlace,")
                .append(" cbm.MANAGER_TYPE managerPost,pb.MARITAL_STATUS maritalStatus,")
                .append(" pb.EDUCATION education, pb.LIVING_PLACE livingPlace,")
                .append(" pb.WORK_EXPERIENCE workExperience, pb.CREDIT_INFO creditInfo,")
                .append(" cbm.OVERALL_MERIT overallMerit, cbm.REMARK remark")
                .append(" FROM CM_BASE_MANAGER cbm")
                .append(" LEFT JOIN PS_BASE pb ON pb.CUSTOMER_NO = cbm.PS_CUS_NO")
                .append(" WHERE cbm.STATUS = :status AND cbm.CM_CUS_NO = :customerNo")
                .append(" AND (cbm.MANAGER_NAME IN (:managerNames) OR pb.NAME IN (:managerNames))");
        params.put("status", Constants.NORMAL);
        params.put("customerNo", customerNo);
        params.put("managerNames", nameList);
        List<LbManagerInfoDto> list = compManagerDAO.findCustListBySql(LbManagerInfoDto.class, sql.toString(), params);
        if(list == null || list.size() <= 0) return null;

        for (LbManagerInfoDto entity : list) {
            if (!StringUtil.isEmptyString(entity.getBirthday())) {
                entity.setAge(String.valueOf(DateUtil.getAgeByBirthday(entity.getBirthday())));
            }else{
                entity.setAge("--");
            }

            SysDic eduType = sysDicDAO.queryByEnNameAndOptType(entity.getEducation(),
                    SysConstants.BsDicConstant.STD_SY_GB00003);
            if(eduType != null) entity.setEducation(eduType.getCnName());

            SysDic mariType = sysDicDAO.queryByEnNameAndOptType(entity.getMaritalStatus(),
                    SysConstants.BsDicConstant.STD_SY_GB00002);
            if(mariType != null) entity.setMaritalStatus(mariType.getCnName());
        }
        return list;
    }

    /**
     * 按 投资比例 获取 前五大对外投资信息
     * @param customerNo 客户号
     * @return
     * @throws Exception
     */
    public List<CompInvestment> getLinkedComp(String customerNo){
        //  取前五大 对外投资企业
        List<CompInvestment> list =
                compInvestmentDao.selectByProperty("customerNo", customerNo, "fundedRatio DESC");///!!!!!!!!!!!
        if (CollectionUtils.isEmpty(list)) return null;
        if (list.size() > 5) return list.subList(0, 5);
        return list;
    }

    /**
     * 根据流水号获取企业经营情况
     * @param serNo 业务流水号
     * @return
     * @throws Exception
     */
    public LbCompRunInfoDto getRunInfo(String serNo) throws Exception {
        StringBuilder sql = new StringBuilder();
        Map<String, Object> params = new HashMap<>();
        sql.append("SELECT cb.INDUSTRY_TYPE industry, cra.INDUS_INFO industryInfo,")
                .append(" cra.SUPPLIER_INFO supplierInfo, cra.ROW_MATERIAL_INFO rawMaterialInfo,")
                .append(" cra.PRODUCE_INFO produceInfo, cra.PRODUCT_SALES_INFO productSalesInfo,")
                .append(" cra.PRODUCT_INFO productInfo, ")
                .append(" cra.ENVIRONMENT_INFO environmentInfo, cra.QUALIFY_CHANGE_INFO qualifyChangeInfo,")
                .append(" cra.BUSINESS_PLACE businessPlace,cra.BUSINESS_PLACE_LAND businessPlaceLand,cra.BUSINESS_PLACE_PLANT businessPlacePlant,")
                .append(" cra.RELATED_COMPANIES relatedCompanies")
                .append(" FROM CM_RUN_ANALYSIS cra")
                .append(" LEFT JOIN CM_BASE cb ON cb.CUSTOMER_NO = cra.CUSTOMER_NO")
                .append(" WHERE cra.SER_NO = :serNo AND cb.STATUS = :status");
        params.put("serNo", serNo);
        params.put("status", Constants.NORMAL);
        List<LbCompRunInfoDto> resultList = 
        		compRunAnalysisDao.findCustListBySql(LbCompRunInfoDto.class, sql.toString(), params);
        if (CollectionUtils.isEmpty(resultList)) return null;
        
        return resultList.get(0);
    }

    /**
     * 根据流水号获取企业能耗详情
     * @param serNo 业务流水号
     * @return
     */
    public LbPowersDto getPowerInfo(String serNo) {
        StringBuilder sql = new StringBuilder();
        Map<String, Object> params = new HashMap<>();
        sql.append("SELECT crp.YEAR year , crp.ELECTRICITY_FEE electricityFee ,")
                .append(" crp.OTHER_FEE otherFee , crp.STAFF_PAY staffPay , crp.STAFF_NUM staffNum")
                .append(" FROM CM_RUN_POWER crp")
                .append(" WHERE crp.SER_NO = '"+serNo+"'")
                .append(" ORDER BY crp.YEAR DESC");
        params.put("serNo", serNo);
        params.clear();
        List<LbPowerDto> list = compPowerDao.findCustListBySql(LbPowerDto.class, sql.toString(), params);
        if (list == null || list.size() <= 0) return null;

        List<String> years = list.stream().map(LbPowerDto::getYear).collect(Collectors.toList());
        List<Double> electricityFee = list.stream().map(LbPowerDto::getElectricityFee).collect(Collectors.toList());
        List<Double> otherFee = list.stream().map(LbPowerDto::getOtherFee).collect(Collectors.toList());
        List<Double> staffPay = list.stream().map(LbPowerDto::getStaffPay).collect(Collectors.toList());
        List<Integer> staffNum = list.stream().map(LbPowerDto::getStaffNum).collect(Collectors.toList());
        int size = list.size();
        if(size < 4){
            String year = list.get(size - 1).getYear().substring(0, 4);
            for (int i = 0; i < 4 - size; i++) {
                years.add((Integer.parseInt(year) - i - 1) + "年");
            }
        }

        LbPowersDto powers = new LbPowersDto();
        powers.setYears(years);
        powers.setElectricityFee(electricityFee);
        powers.setOtherFee(otherFee);
        powers.setStaffPay(staffPay);
        powers.setStaffNum(staffNum);
        return powers;
    }

    /**
     * 获取 担保企业信息、财务状况
     * @param serNo 业务流水号
     * @return
     */
    public List<LbGuaCompany> getGuaCompanyInfo(String serNo){
        List<GuaCompany> companies = guaCompanyDao.selectByProperty("serNo", serNo);
        if (companies == null || companies.size() <= 0) return null;
        List<LbGuaCompany> guaCompanies = MapperUtil.trans(companies, LbGuaCompany.class);
        for (LbGuaCompany guaCompany : guaCompanies) {
            Map<String, Object> params = new HashMap<>();
            params.put("guaCompanyId", guaCompany.getId());
            List<CompFinanceInfo> financeInfos =
                    compFinanceInfoDao.selectByProperty(params, "YEARS ASC");
            guaCompany.setFinanceInfos(financeInfos);
        }
        return guaCompanies;
    }

    /**
     * 获取调查报告中 资产负债简表
     * @return
     */
    public Map<String, Object> getFinaImportData(Long taskId, String customerNo) throws Exception{
        Map<String, Object> finaImport = new HashMap<>();
        List<Map<String, Double>> balanceList = new ArrayList<>();
        List<Map<String, Double>> incomeList = new ArrayList<>();
//  正常
        Map<String, Object> finaRepInfo = compFinanceSV.getFinaRepInfo(taskId, customerNo);
        int finaRepYear = (Integer) finaRepInfo.get("finaRepYear");
        int finaRepMonth = (Integer) finaRepInfo.get("finaRepMonth");
        List<String> dateList = SystemUtil.genFinaDateList(finaRepYear,
                finaRepMonth, true, false);
        if(dateList.size()>4){
        	dateList.remove(4);
        }
    	dateList.remove(0);
        List<String> dateListShow = SystemUtil.genFinaDateList(finaRepYear,
                finaRepMonth, true, true);
        if(dateListShow.size()>4){
        	dateListShow.remove(4);
        }
    	dateListShow.remove(0);
        List<?> balanceModel = cusFSToolSV.findRepModelByType(customerNo, "balance", Constants.FINA_MODEL_TYPE.IMPORT_ROWS);

        List<?> balanceData = cusFSToolSV.findRepDataByType(customerNo, dateList, "balance");
        List<?> incomeModel = cusFSToolSV.findRepModelByType(customerNo, "income", Constants.FINA_MODEL_TYPE.IMPORT_ROWS);
        List<?> incomeData = cusFSToolSV.findRepDataByType(customerNo, dateList, "income");
//        //若当期为12份年报，则删除上年同期数据
//        if(balanceData.size() > dateListShow.size())
//            balanceData.remove(4);
//        //若当期为12份年报，则删除上年同期数据
//        if(incomeData.size() > dateListShow.size())
//            incomeData.remove(4);

        SReportModelRow model = new SReportModelRow();
        if(balanceData!=null && balanceData.size()!=0){
            Iterator<?> it = balanceData.iterator();
            while(it.hasNext()){

                Map<String, Double> balanceMap = new TreeMap<>();
                HashMap<String, Double> hashMap = (HashMap<String, Double>)it.next();
                if(hashMap==null || hashMap.size()==0){
                    balanceList.add(balanceMap);
                    continue;
                }
                for(Map.Entry<String, Double> entry : hashMap.entrySet()){
                    String key = entry.getKey();
                    model.setRowSubject(key);
                    if(balanceModel.contains(model)){
                    	Double value = entry.getValue();
                        balanceMap.put(key, value);
                    }
                }
                balanceList.add(balanceMap);
            }
        }
        if(incomeData!=null && incomeData.size()!=0){
            Iterator<?> it = incomeData.iterator();
            while(it.hasNext()){
                Map<String, Double> incomeMap = new TreeMap<>();
                HashMap<String, Double> hashMap = (HashMap<String, Double>)it.next();
                if(hashMap==null || hashMap.size()==0){
                    incomeList.add(incomeMap);
                    continue;
                }
                for(Map.Entry<String, Double> entry : hashMap.entrySet()){
                    String key = entry.getKey();
                    model.setRowSubject(key);
                    if(incomeModel.contains(model)){
                    	Double value = entry.getValue();
                        incomeMap.put(key, value);
                    }
                }
                incomeList.add(incomeMap);
            }
        }
        finaImport.put("dateListShow", dateListShow);
        finaImport.put("balanceModel", balanceModel);
        finaImport.put("balanceMap", balanceList);
        finaImport.put("incomeModel", incomeModel);
        finaImport.put("incomeMap", incomeList);
        return finaImport;
    }

    /**
     * 获取财务分析 纳税分析、现金流分析、总体分析结论
     * @param taskId 任务主键
     * @param customerNo 客户号
     * @param serNo 任务流水号
     * @return
     * @throws Exception
     */
    public CompFinance getFinanceAnaly(Long taskId, String customerNo, String serNo)throws Exception{
        CompFinance compFinance = compFinanceDAO.queryByTaskIdAndCustNo(taskId, customerNo);
        if(compFinance!=null){
            Map<String, Object> localFinAnaly = finriskResultService.getLocalFinAnaly(serNo);
            if(StringUtil.isEmptyString(compFinance.getProfitAnaly())){
                compFinance.setProfitAnaly(String.valueOf(localFinAnaly.get("profit")));
            }
            if(StringUtil.isEmptyString(compFinance.getDevelopAnaly())){
                compFinance.setDevelopAnaly(String.valueOf(localFinAnaly.get("develop")));
            }
            if(StringUtil.isEmptyString(compFinance.getOperateAnaly())){
                compFinance.setOperateAnaly(String.valueOf(localFinAnaly.get("operate")));
            }
            if(StringUtil.isEmptyString(compFinance.getSolvency())){
                compFinance.setSolvency(String.valueOf(localFinAnaly.get("debt")));
            }
            if(StringUtil.isEmptyString(compFinance.getSystemAnaly())){
                compFinance.setSystemAnaly(String.valueOf(localFinAnaly.get("summay")));
            }
        }
        return compFinance;
    }

    /**
     * 各个重要科目的账面值
     * 并且按照占总资产的15%过滤
     * @param finaImportData
     * @return
     */
    public Map<String, String> getNowBalanceData(Map<String, Object> finaImportData){
        Map<String, String> data = new HashMap<>();
        List<Map<String, Double>> balanceMap = (List<Map<String, Double>>)finaImportData.get("balanceMap");
        if(CollectionUtils.isEmpty(balanceMap)) return data;

        //获取当期的资产表的数据
        TreeMap<String, Double> balanceData  = (TreeMap<String, Double>)balanceMap.get(balanceMap.size() - 1);
        if(CollectionUtils.isEmpty(balanceMap)) return data;
        
        String[] rowNos = {
                Constants.FINA_OBJECT_ROW_NO.TOTAL_ASSET, Constants.FINA_OBJECT_ROW_NO.CASH,
                Constants.FINA_OBJECT_ROW_NO.RECEIVE, Constants.FINA_OBJECT_ROW_NO.OTHER_RECEIVE,
                Constants.FINA_OBJECT_ROW_NO.INVENTORY, Constants.FINA_OBJECT_ROW_NO.FIX_NEW,
                Constants.FINA_OBJECT_ROW_NO.PAY, Constants.FINA_OBJECT_ROW_NO.OTHER_PAY,
                Constants.FINA_OBJECT_ROW_NO.PRE_PAY, Constants.FINA_OBJECT_ROW_NO.PRE_RECE,
                Constants.FINA_OBJECT_ROW_NO.FIX_OLD
            };
        String[] objects = {"总资产", "货币资金", "应收账款", "其他应收款", "存货", "固定资产", "应付账款",
                "其他应付款", "预付账款", "预收账款"};
        double totalAsset = 0d;
        DecimalFormat df = new DecimalFormat("#.00");
        for (int i = 0; i < objects.length; i++) {
            Double value = balanceData.get(rowNos[i]);
            //FIX_NEW固定资产:若 新准则 为空，则使用 旧准则
            if (i == 5 && value == null) value = balanceData.get(rowNos[10]);
            if (value == null) value = 0d;
            if (i == 0) { // 总资产
                totalAsset = value;
//                data.put(objects[i], df.format(BigDecimal.valueOf(value)));
                continue;
            }
            // 判断 value 是否占 totalAsset 的15%以上
//            if (this.overPercent(null, value, totalAsset)){
                data.put(objects[i], df.format(BigDecimal.valueOf(value)));
//            }
        }
        return data;
        
        /*DecimalFormat df = new DecimalFormat("#.00");
        //货币资金
        String cash = df.format(new BigDecimal(balanceData.get(Constants.FINA_OBJECT_ROW_NO.CASH)));
        //应收账款
        String receive = df.format(new BigDecimal(balanceData.get(Constants.FINA_OBJECT_ROW_NO.RECEIVE)));
        //其他应收款
        String otherRceive = df.format(new BigDecimal(balanceData.get(Constants.FINA_OBJECT_ROW_NO.OTHER_RECEIVE)));
        //存货
        String inventory = df.format(new BigDecimal(balanceData.get(Constants.FINA_OBJECT_ROW_NO.INVENTORY)));
        //新准则的固定资产
        Double fixNew = balanceData.get(Constants.FINA_OBJECT_ROW_NO.FIX_NEW);
        String fix;
//        若无
        if(fixNew==null){
            fix = df.format(new BigDecimal(balanceData.get(Constants.FINA_OBJECT_ROW_NO.FIX_OLD)));
        }else{
            fix = df.format(new BigDecimal(fixNew));
        }
        //应付账款
        Double payValue = balanceData.get(Constants.FINA_OBJECT_ROW_NO.PAY);
        String pay = df.format(BigDecimal.valueOf(payValue));
        //其他应付款
        String otherPay = df.format(new BigDecimal(balanceData.get(Constants.FINA_OBJECT_ROW_NO.OTHER_PAY)));
        //预付账款
        String perPay = df.format(new BigDecimal(balanceData.get(Constants.FINA_OBJECT_ROW_NO.PRE_PAY)));
        //预收账款
        String perRece = df.format(new BigDecimal(balanceData.get(Constants.FINA_OBJECT_ROW_NO.PRE_RECE)));

        //总资产
        String totalAsset = df.format(new BigDecimal(balanceData.get(Constants.FINA_OBJECT_ROW_NO.TOTAL_ASSET)));

        if(this.overPrcent(null, cash, totalAsset)) {
            data.put("货币资金", cash);
        }
        if(this.overPrcent(null, receive, totalAsset)) {
            data.put("应收账款", receive);
        }
        if(this.overPrcent(null, otherRceive, totalAsset)) {
            data.put("其他应收款", otherRceive);
        }
        if(this.overPrcent(null, inventory, totalAsset)) {
            data.put("存货", inventory);
        }
        if(this.overPrcent(null, fix, totalAsset)) {
            data.put("固定资产", fix);
        }
        if(this.overPrcent(null, pay, totalAsset)) {
            data.put("应付账款", pay);
        }
        if(this.overPrcent(null, otherPay, totalAsset)) {
            data.put("其他应付款", otherPay);
        }
        if(this.overPrcent(null, perPay, totalAsset)) {
            data.put("预付账款", perPay);
        }
        if(this.overPrcent(null, perRece, totalAsset)) {
            data.put("预收账款", perRece);
        }
//        data.put("总资产", totalAsset);
        return data;*/
    }

    /**
     *  判断 val 是否占 total 的15%以上
     * @param percent 默认15%
     * @return
     */
    public boolean overPercent(Double percent, Double val, Double total){
        if (total <= 0d) return false;
        if (percent == null) percent = 15d;
        return (val * 100) / total >= percent;
    }
    
    /**
     *  判断val是否占total的15%以上
     * @param prcent    默认15%
     * @return
     *//*
    public boolean overPrcent(Double prcent, String val, String total){
        if(StringUtil.isEmptyString(val)){
            return false;
        }
        if(StringUtil.isEmptyString(total)){
            return false;
        }
        if(prcent==null){
            prcent = 15d;
        }
        Double tar = Double.parseDouble(val);
        Double all = Double.parseDouble(total);
        if((tar * 100) / all >=prcent){
            return true;
        }
        return false;
    }*/

    /**
     * 获取 今年、去年 原材料采购情况
     * @param serNo 任务流水号
     * @return
     */
    public LbRawMaterialDto getRawMaterial(String serNo){
        LbRawMaterialDto result = new LbRawMaterialDto();
        int thisYear = DateUtil.getYear(new Date());
        List<CompRawMaterial> thisYearList = compRawMaterialDao.getBySerNoAndYear(serNo, thisYear);
        List<CompRawMaterial> lastYearList = compRawMaterialDao.getBySerNoAndYear(serNo, thisYear - 1);
        result.setThisYear(thisYearList);
        result.setLastYear(lastYearList);
        return result;
    }

    /**
     * 获取 今年、去年 产品销售情况
     * @param serNo 任务流水号
     * @return
     */
    public LbProductSalesDto getSalesData(String serNo){
        LbProductSalesDto result = new LbProductSalesDto();
        int thisYear = DateUtil.getYear(new Date());
        List<CompProductSales> thisYearList = compProductSalesDao.getBySerNoAndYear(serNo, thisYear);
        List<CompProductSales> lastYearList = compProductSalesDao.getBySerNoAndYear(serNo, thisYear-1);
        Collections.sort(lastYearList, new Comparator<CompProductSales>() {
			@Override
			public int compare(CompProductSales arg0, CompProductSales arg1) {
				if (arg1.getYearSales() != null && arg0.getYearSales() != null){
					return (int) (arg1.getYearSales() - arg0.getYearSales());
				}else {
					return 0;
				}
				
			}
		});
        
        result.setThisYear(thisYearList);
        result.setLastYear(lastYearList);
        return result;
    }

    /**
     * 获取 项目投资规模信息
     * @param serNo 任务流水号
     * @return
     */
    public LbInvestScaleDto getInvestScale(String serNo) throws Exception {
        InvestScale investScale = investScaleDao.selectSingleByProperty("serNo", serNo);
        if (investScale == null) return null;
        LbInvestScaleDto result = MapperUtil.trans(investScale, LbInvestScaleDto.class);

        String subProjectInfo = investScale.getSubProjectInfo();
        if (StringUtil.isEmptyString(subProjectInfo)) return result;
        List<SubProjectDto> entityList =
                JacksonUtil.fromJson(subProjectInfo, new TypeReference<List<SubProjectDto>>() {});
        if (CollectionUtils.isEmpty(entityList)) return result;
        result.setSubProjectList(entityList);
        return result;
    }

    /**
     * 获取 借款人/关联方授信情况
     * @param taskId 任务主键
     * @param customerNo 客户号
     * @param type "01：借款人 02：关联方"
     * @return
     * @throws Exception 
     */
    public List<CompFinancingBank> getCreditInfo(Long taskId, String customerNo, String type) throws Exception{
    	 Map<String, Object> params = new HashMap<>();
         params.put("taskId", taskId);
         params.put("customerNo", customerNo);
         params.put("type", type);
         List<CompFinancingBank> resultList = creditInfoDao.selectByProperty(params);
         for (CompFinancingBank entity : resultList) {
             SysDic guaType = sysDicDAO.queryByEnNameAndOptType(entity.getGuaranteeType(),
                     SysConstants.BsDicConstant.STD_SY_GUAR_TYPE);
             if (guaType != null){
                 entity.setGuaranteeType(guaType.getCnName());
             }
         }
         return resultList;
    }
    
    /**
     * 获取 贷款企业征信（不超过3条）
     * @param serNo 任务流水号
     * @param guaCompId 担保企业id
     * @return
     */
    public List<CompCredit> getCompCredit(String serNo, String guaCompId){
        Map<String, Object> params = new HashMap<>();
        //params.put("serialNumber", serNo);
        String query = "serial_Number = '"+serNo+"' and STATUS =0 ";
        if(StringUtils.isEmpty(guaCompId)){
        	query += " and gua_Comp_Id is null";
        }else{
        	query += " and guaCompId = "+guaCompId;
        	//params.put("guaCompId", guaCompId);
        }
        
        List<CompCredit> resultList = compCreditDao.select(query, params);
        if(CollectionUtils.isEmpty(resultList)) return null;

        if (resultList.size() > 3) return resultList.subList(0, 3);
        return resultList;
    }

    /**
     * 获取 贷款个人征信（不超过3条）
     * @param serNo 任务流水号
     * @param guaCompId 担保企业id
     * @return
     */
    public List<CompUserCredit> getUserCredit(String serNo, String guaCompId){
        Map<String, Object> params = new HashMap<>();
        //params.put("serialNumber", serNo);
        String query = "serial_Number = '"+serNo+ "' and STATUS = 0";
        if(StringUtils.isEmpty(guaCompId)){
        	query += " and gua_Comp_Id is null";
        }else{
        	query += " and gua_Comp_Id = "+guaCompId;
        	//params.put("guaCompId", guaCompId);
        }
        List<CompUserCredit> resultList = compUserCreditDao.select(query, params);//

        if(CollectionUtils.isEmpty(resultList)) return null;

        if (resultList.size() > 3) return resultList.subList(0, 3);
        return resultList;
    }
    
    /**
     * 获取 对外担保信息
     * @param taskId 任务主键
     * @param customerNo 客户号
     * @return
     * @throws Exception
     */
    public List<LbExternalGuaDto> getExternalGua(Long taskId, String customerNo) throws Exception {
        List<CompFinancingExtGuara> extGuaList =
                compFinancingExtGuaraDAO.queryByTaskIdAndCustNo(taskId, customerNo);
        if (CollectionUtils.isEmpty(extGuaList)) return null;

        List<LbExternalGuaDto> resultList = new ArrayList<>();
        for (CompFinancingExtGuara entity : extGuaList) {
        	LbExternalGuaDto result = MapperUtil.trans(entity, LbExternalGuaDto.class);
        	//获取五级分类名称
        	
            SysDic relationType = sysDicDAO.queryByEnNameAndOptType(entity.getRelation(),
                    SysConstants.BsDicConstant.STD_COM_EXT_GUARA_REL);
            if (relationType != null){
                result.setRelation(relationType.getCnName());
            }

            //取值不对，取到的是B，enname是数字表示
          /*  SysDic guaType = sysDicDAO.queryByEnNameAndOptType(entity.getAntiGuaranteeWay(),
                    SysConstants.BsDicConstant.STD_SY_GUAR_TYPE);
            if (guaType != null){
                result.setAntiGuaranteeWay(guaType.getCnName());
            }*/
            resultList.add(result);
        }
        return resultList;
    }
    
    /**
     * 获取 企业融资信息
     * @param taskId 任务ID
     * @param customerNo 客户号
     * @param type "M" 我行
     * 如何区别我行和其他融资途径，没有直接的一个字段标志，M O只是service层业务逻辑判断用的，
     * 区分的核心是融资机构名称 financialOrg 本行的融资机构名称是RGRCB
     * @return
     * @throws Exception
     */
    public List<CompFinancingBankDto> getFinancingList(Long taskId, String customerNo, String type) throws Exception {
        Map<String, Object> params = new HashMap<>();
        params.put("taskId", taskId);
        params.put("customerNo", customerNo);
        List<CompFinancingBank> resultList = null;
        if("M".equals(type)){// 本行合作情况
			params.put("financialOrg", "RGRCB");
	        resultList = compFinancingBankDAO.selectByProperty(params);
		}else {
			resultList = compFinancingBankDAO.queryOtherFinaBank(taskId, customerNo);
		}
        
        if (CollectionUtils.isEmpty(resultList)) return null;
        List<CompFinancingBankDto> CompFinancingBankDtoList = MapperUtil.trans(resultList, CompFinancingBankDto.class);
        if(type.equals("O")){
        	for (CompFinancingBankDto entity : CompFinancingBankDtoList) {
	        	String guaTypes = entity.getGuaranteeType();
	            if(!StringUtil.isEmptyString(guaTypes)){
	                StringBuilder guaType = new StringBuilder();
	                String[] typeList = guaTypes.split(",");
	                for (String types : typeList) {
	                    SysDic guaTypeDic = sysDicDAO.queryByEnNameAndOptType(
	                            types, SysConstants.BsDicConstant.STD_SY_GUAR_TYPE);
	                    guaType.append(guaTypeDic.getCnName()).append("，");
	                }
	                entity.setGuaranteeType(guaType.substring(0, guaType.length()-1));
	            }	
	            entity.setFiveClass(LfConstants.getLfFiveNotoFiveName(entity.getFiveClass()));
	        }
        }
        return CompFinancingBankDtoList;
    }
    
   
}

class MyCompartor implements Comparator{
	@Override
	public int compare(Object o1,Object o2){
		Double a = Double.parseDouble((String)o1);
		Double b = Double.parseDouble((String)o2);
		return a.compareTo(b);
	}
}
