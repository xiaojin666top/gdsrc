package com.beawan.corporateloan.service.impl;

import com.beawan.base.entity.User;
import com.beawan.base.service.IUserSV;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.core.Pagination;
import com.beawan.corporateloan.dao.LcTaskDao;
import com.beawan.corporateloan.dao.LeExamineContentDao;
import com.beawan.corporateloan.dao.LeExaminePointDao;
import com.beawan.corporateloan.dao.LeTaskDao;
import com.beawan.corporateloan.entity.LeExamineContent;
import com.beawan.corporateloan.entity.LeExaminePoint;
import com.beawan.corporateloan.entity.LeTask;
import com.beawan.corporateloan.service.LeExamineContentService;
import com.beawan.corporateloan.service.LeExaminePointService;
import com.beawan.corporateloan.service.LeTaskService;
import com.beawan.library.bean.TreeData;
import com.beawan.library.service.IIndustrySV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.platform.util.DateUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: xyh
 * @Date: 03/08/2020
 * @Description:
 */
@Service("leTaskService")
public class LeTaskServiceImpl extends BaseServiceImpl<LeTask> implements LeTaskService {
    @Resource(name = "leTaskDao")
    public void setDao(BaseDao<LeTask> dao){
        super.setDao(dao);
    }
    @Resource
    public LeTaskDao leTaskDao;
    @Resource
    public LeExamineContentDao leExamineContentDao;
    @Resource
	private ITaskSV taskSV;
    @Resource
	private IIndustrySV industrySV;
    @Resource
    private LeExamineContentService leExamineConentService;
    @Resource
    private IUserSV userSV;

    
    @Override
    public LeTask queryBySerialNumber(String serialNumber) throws Exception {
        return leTaskDao.selectSingleByProperty("serialNumber",serialNumber);
    }

    @Override
    public Map<String, Object> getExamineReportData(String serialNumber) throws Exception {
        //获取审查表信息
        Map<String,Object> data = new HashMap<>();
        LeTask leTask = this.queryBySerialNumber(serialNumber);
        data.put("leTask",leTask);
        Task lbTask = taskSV.getTaskBySerNo(serialNumber);
        data.put("lbTask", lbTask);
        String institution = lbTask.getInstitutionName();
        if(institution.contains("江苏如皋农村商业银行")){
        	institution = institution.substring(10,institution.length());
        	data.put("institution", institution);
        }
        String industryCode = lbTask.getIndustry();
        TreeData treeData = industrySV.getTreeDataByEnname(industryCode,
				SysConstants.TreeDicConstant.GB_IC_4754_2017);
        String industryCodeAll[] = treeData.getLocate().split(",");
        String industryCodeTop = industryCodeAll[1];
        TreeData treeData1 = industrySV.getTreeDataByEnname(industryCodeTop,
				SysConstants.TreeDicConstant.GB_IC_4754_2017);
        data.put("industryName", treeData1.getCnName());
        List<LeExamineContent> contents = leExamineConentService.queryByUnPass(leTask.getId(),leTask.getReviewCount(),1);
        data.put("LeExamineContent", contents);
        User user = userSV.queryById(leTask.getUserNo());
        data.put("user", user);
        //获取最后一次的审查细项记录，且处理标记不为空
        //获取到两种审查结果细项
        //1.所有处理标识不为空的情况
        //2.处理标识为空，但是未通过的
        StringBuilder sql = new StringBuilder(" from LeExamineContent ");
        sql.append("where 1=1 ")
                .append(" and leTaskId =:taskId ")
                .append(" and examineCount =:examineCount ")
                .append(" and (STATE is not null or (STATE is null and isPass =:isPass ))")
                .append(" and status=:status ");
        Map<String, Object> params = new HashMap<>();
        params.put("taskId",leTask.getId());
        params.put("examineCount",leTask.getReviewCount());
        params.put("isPass",Constants.LE_REVIEW_CONTENT_STATE.IS_UN_PASS);
        params.put("status", Constants.NORMAL);
        //单表不分页
        List<LeExamineContent> leExamineContents = leExamineContentDao.select(sql.toString(),params);
        data.put("leContentsResult",leExamineContents);
        return data;
    }
}
