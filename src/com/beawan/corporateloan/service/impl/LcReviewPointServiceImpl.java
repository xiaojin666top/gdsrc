package com.beawan.corporateloan.service.impl;

import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.corporateloan.dao.LcReviewPointDao;
import com.beawan.corporateloan.dao.LcRiskDbDao;
import com.beawan.corporateloan.dto.LcPointRiskDto;
import com.beawan.corporateloan.entity.LcReviewPoint;
import com.beawan.corporateloan.entity.LcRiskDb;
import com.beawan.corporateloan.service.LcReviewPointService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.corporateloan.entity.LcReviewPoint;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yzj
 */
@Service("lcReviewPointService")
public class LcReviewPointServiceImpl extends BaseServiceImpl<LcReviewPoint> implements LcReviewPointService{
    /**
    * 注入DAO
    */
    @Resource(name = "lcReviewPointDao")
    public void setDao(BaseDao<LcReviewPoint> dao) {
        super.setDao(dao);
    }
    @Resource
    public LcReviewPointDao lcReviewPointDao;
    @Resource
    public LcRiskDbDao lcRiskDbDao;

    @Override
    public List<LcPointRiskDto> getPointRiskList(String indu) {
        List<LcPointRiskDto> result = new ArrayList<>();
        //审查点列表
        List<LcReviewPoint> pointlist = lcReviewPointDao
                .select("status=" + Constants.NORMAL + " and (indu='" + indu + "' or indu='COMM')");
        //获取所有的问题点
        List<LcRiskDb> riskDbList = lcRiskDbDao.select("status=" + Constants.NORMAL);
        if(pointlist==null || pointlist.size()==0)
            return null;
        if(riskDbList==null || riskDbList.size()==0){
            return null;
        }
        for (LcReviewPoint point : pointlist){
            LcPointRiskDto dto = new LcPointRiskDto();
            dto.setReviewName(point.getReviewName());
            List<LcRiskDb> riskList = new ArrayList<>();
            for(LcRiskDb risk : riskDbList){
                if(point.getId().equals(risk.getPointId())) {
                    riskList.add(risk);
                }
            }
            dto.setRiskList(riskList);
            result.add(dto);
        }
        return result;
    }
}
