package com.beawan.corporateloan.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.corporateloan.dao.LrVisitRecordDao;
import com.beawan.corporateloan.entity.LrVisitRecord;
import com.beawan.corporateloan.service.LrVisitRecordService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.corporateloan.entity.LrVisitRecord;

/**
 * @author yzj
 */
@Service("lrVisitRecordService")
public class LrVisitRecordServiceImpl extends BaseServiceImpl<LrVisitRecord> implements LrVisitRecordService{
    /**
    * 注入DAO
    */
    @Resource(name = "lrVisitRecordDao")
    public void setDao(BaseDao<LrVisitRecord> dao) {
        super.setDao(dao);
    }
    @Resource
    public LrVisitRecordDao lrVisitRecordDao;
}
