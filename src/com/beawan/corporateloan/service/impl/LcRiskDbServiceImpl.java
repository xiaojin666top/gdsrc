package com.beawan.corporateloan.service.impl;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.corporateloan.dao.LcRiskDbDao;
import com.beawan.corporateloan.entity.LcRiskDb;
import com.beawan.corporateloan.service.LcRiskDbService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.beawan.corporateloan.entity.LcRiskDb;

/**
 * @author yzj
 */
@Service("lcRiskDbService")
public class LcRiskDbServiceImpl extends BaseServiceImpl<LcRiskDb> implements LcRiskDbService{
    /**
    * 注入DAO
    */
    @Resource(name = "lcRiskDbDao")
    public void setDao(BaseDao<LcRiskDb> dao) {
        super.setDao(dao);
    }
    @Resource
    public LcRiskDbDao lcRiskDbDao;
}
