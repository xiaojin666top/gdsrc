package com.beawan.corporateloan.service.impl;

import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.core.Pagination;
import com.beawan.corporateloan.dao.LeExaminePointDao;
import com.beawan.corporateloan.entity.LeExaminePoint;
import com.beawan.corporateloan.service.LeExaminePointService;
import com.gargoylesoftware.htmlunit.Page;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: xyh
 * @Date: 03/08/2020
 * @Description:
 */
@Service("leExaminePointService")
public class LeExaminePointServiceImpl extends BaseServiceImpl<LeExaminePoint> implements LeExaminePointService {
    @Resource(name = "leExaminePointDao")
    public void setDao(BaseDao<LeExaminePoint> dao){
        super.setDao(dao);
    }
    @Resource
    public LeExaminePointDao leExaminePointDao;


    @Override
    public Pagination<LeExaminePoint> queryPageList(int page, int rows) throws Exception {
        Map<String, Object> params = new HashMap<>();
        StringBuilder sql = new StringBuilder("select id,EXAMINE_TYPE as examineType,EXAMINE_NAME as examineName,EXAMINE_MEMO as examineMemo,flag,NEED_MINUTE as needMinute ");
        sql.append(" from LE_EXAMINE_POINT ");
        sql.append(" where 1=1 and status="+Constants.NORMAL);
        sql.append(" order by EXAMINE_TYPE asc ");
        // params.put("status", Constants.NORMAL);
        String count = "select count(*) from (" + sql + ") tbl";
        Pagination<LeExaminePoint> pager = leExaminePointDao.findCustSqlPagination(LeExaminePoint.class,sql.toString(),count,params,page,rows);
        return pager;
    }

    /**
     *获取所有可用的审查要点（状态正常）	
     *
     */
    @Override
    public List<LeExaminePoint> getAllLeExaminPoint(){
        List<LeExaminePoint> LeExaminPointList = leExaminePointDao.getAllLeExaminPoint();
        return LeExaminPointList;
    }


}
