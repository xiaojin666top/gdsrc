package com.beawan.corporateloan.service.impl;

import com.beawan.common.Constants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.corporateloan.dao.LeExamineContentDao;
import com.beawan.corporateloan.dao.LeTaskDao;
import com.beawan.corporateloan.dto.LeExamineContentDto;
import com.beawan.corporateloan.entity.LeExamineContent;
import com.beawan.corporateloan.entity.LeTask;
import com.beawan.corporateloan.service.LeExamineContentService;
import com.beawan.corporateloan.service.LeTaskService;
import com.platform.util.GsonUtil;
import com.platform.util.StringUtil;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: xyh
 * @Date: 03/08/2020
 * @Description:
 */
@Service("leExamineContentService")
public class LeExamineContentServiceImpl extends BaseServiceImpl<LeExamineContent> implements LeExamineContentService {
    @Resource(name = "leExamineContentDao")
    public void setDao(BaseDao<LeExamineContent> dao){
        super.setDao(dao);
    }
    @Resource
    public LeExamineContentDao leExamineContentDao;
    @Resource
    public LeTaskDao leTaskDao;

    @Override
    public List<LeExamineContent> queryByCondition(Integer leTaskId, Integer reviewCount) throws Exception {
        Map<String,Object> params = new HashMap<>();
        params.put("leTaskId",leTaskId);
        params.put("examineCount",reviewCount);
        List<LeExamineContent> leExamineContents = leExamineContentDao.selectByProperty(params," examineType asc ");
        return leExamineContents;
    }

    @Override
    public List<LeExamineContent> queryExamineContResult(Integer leTaskId) throws Exception {
        //获取到本笔贷款的审查任务表信息
        LeTask leTask = leTaskDao.findByPrimaryKey(leTaskId);
        //获取到两种审查结果细项
        //1.所有处理标识不为空的情况
        //2.处理标识为空，但是未通过的
        StringBuilder sql = new StringBuilder(" from LeExamineContent ");
        sql.append("where 1=1 ")
                .append(" and leTaskId =:taskId ")
                .append(" and examineCount =:examineCount ")
                .append(" and (STATE is not null or (STATE is null and isPass =:isPass ))")
                .append(" and status=:status and CONTENT IS NOT NULL");
        Map<String, Object> params = new HashMap<>();
        params.put("taskId",leTask.getId());
        params.put("examineCount",leTask.getReviewCount());
        params.put("isPass",Constants.LE_REVIEW_CONTENT_STATE.IS_UN_PASS);
        params.put("status", Constants.NORMAL);
        //单表不分页
        List<LeExamineContent> leExamineContents = leExamineContentDao.select(sql.toString(),params);
        return leExamineContents;
    }

    @Override
    public void updateLeExamineContent(Integer taskId, String descrData) throws Exception {
        //解析出审查结果
        List<Map<String, Object>> maps = GsonUtil.GsonToListMaps(descrData);
        /**
         * 更新审查记录规则一式两份
         *  1.第一次提交为都是通过，但是否处理标记都为空
         *     如果处理标记是空的，只需要把通过的变成不通过就可以
         *  2.被打回后的提交，会去同步一次，上一次的所有审查细项，
         *    在同步的时候就需要去吧上次所有未通过的打回标记上未处理标识
         *  数据例子：
         *    第一次：(无论第一次这么保存，state永远为null)
         *     name:审查细项1    isPass:0    state :null
         *     name:审查细项2    isPass:1    state :null
         *    打回后提交：（这里无论怎么保存，审查细项1永远为null，审查细项2永不为null）
         *    name:审查细项1    isPass:0    state :null
         *    name:审查细项2    isPass:1    state :1
         */
        //获取到审查任务记录表
        LeTask leTask = leTaskDao.findByPrimaryKey(taskId);
        //获取本次审查的所有记录
        List<LeExamineContent> contents = queryByCondition(leTask.getId(),leTask.getReviewCount());

        //下面双重循环是否可以都用id去排序一下，这样就可以只有一重循环
        for(LeExamineContent content : contents){
            for(Map<String, Object> map : maps){
                if(map.get("id").toString().equals(content.getId()+"")){ //如果id相同
                    //是否处理标识为空
                    if(content.getState() == null){//如果为空，传过来是什么就是什么
                        content.setIsPass(Integer.parseInt(map.get("isPass").toString()));
                    }else{
                        //如果不为空：
                        //传过来是通过，就是已处理
                        //传过来是不通过，就是未处理
                        if((Constants.LE_REVIEW_CONTENT_STATE.IS_PASS + "").equals(map.get("isPass").toString())){
                            content.setIsPass(Constants.LE_REVIEW_CONTENT_STATE.IS_PASS );
                            content.setState(Constants.LE_REVIEW_CONTENT_STATE.SOLVE );
                        }else if((Constants.LE_REVIEW_CONTENT_STATE.IS_UN_PASS + "").equals(map.get("isPass").toString())){
                            content.setIsPass(Constants.LE_REVIEW_CONTENT_STATE.IS_UN_PASS );
                            content.setState(Constants.LE_REVIEW_CONTENT_STATE.NOT_SOLVE);
                        }
                    }
                   
                    content.setContent(map.get("content").toString() + "");
                    	
                    leExamineContentDao.saveOrUpdate(content);
                    break;
                }
            }
        }
    }
    
    @Override
    public List<LeExamineContent> queryByUnPass(Integer leTaskId, Integer reviewCount, Integer state) throws Exception {
        Map<String,Object> params = new HashMap<>();
        params.put("leTaskId",leTaskId);
        params.put("examineCount",reviewCount);
        params.put("state", state);
        List<LeExamineContent> leExamineContents = leExamineContentDao.selectByProperty(params," examineType asc ");
        return leExamineContents;
    }
    
}
