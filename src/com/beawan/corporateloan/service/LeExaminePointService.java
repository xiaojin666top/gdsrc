package com.beawan.corporateloan.service;

import com.beawan.core.BaseService;
import com.beawan.core.Pagination;
import com.beawan.corporateloan.entity.LeExaminePoint;

import java.util.List;

/**
 * @Author: xyh
 * @Date: 03/08/2020
 * @Description:
 */
public interface LeExaminePointService extends BaseService<LeExaminePoint> {
    //分页
    Pagination<LeExaminePoint> queryPageList(int page, int rows)throws Exception;
    /**
     *获取所有可用的审查要点（状态正常）	
     *
     */
	List<LeExaminePoint> getAllLeExaminPoint();

}
