package com.beawan.corporateloan.service;

import com.beawan.core.BaseService;
import com.beawan.corporateloan.entity.LcRiskDb;

/**
 * @author yzj
 */
public interface LcRiskDbService extends BaseService<LcRiskDb> {
}
