package com.beawan.corporateloan.service;

import com.beawan.core.BaseService;
import com.beawan.corporateloan.entity.LeTask;

import java.util.Map;

/**
 * @Author: xyh
 * @Date: 03/08/2020
 * @Description:
 */
public interface LeTaskService extends BaseService<LeTask> {
    /**
     * 根据流水号，获取审查任务
     * @param serialNumber
     * @return
     * @throws Exception
     */
    LeTask queryBySerialNumber(String serialNumber)throws Exception;

    /**
     * 下载报告
     * @param serialNumber
     * @return
     * @throws Exception
     */
    Map<String, Object> getExamineReportData(String serialNumber)throws Exception;

}
