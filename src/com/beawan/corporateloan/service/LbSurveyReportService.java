package com.beawan.corporateloan.service;

import java.util.Map;

/**
 * 调查报告相关方法接口
 * @author zxh
 * @date 2020/8/6 10:13
 */
public interface LbSurveyReportService {

    /**
     * 获取 贷前调查报告数据集合
     * @param serNo 任务流水号
     * @return
     * @throws Exception
     */
    Map<String, Object> getReportData(String serNo) throws Exception;

    /**
     * 获取调查报告中 资产负债简表
     * @return
     */
    public Map<String, Object> getFinaImportData(Long taskId, String customerNo) throws Exception;


    /**
     * 各个重要科目的账面值
     * 并且按照占总资产的15%过滤
     * @param finaImportData
     * @return
     */
    public Map<String, String> getNowBalanceData(Map<String, Object> finaImportData);


}
