package com.beawan.corporateloan.service;

import com.beawan.core.BaseService;
import com.beawan.corporateloan.dto.LeExamineContentDto;
import com.beawan.corporateloan.entity.LeExamineContent;

import java.util.List;

/**
 * @Author: xyh
 * @Date: 03/08/2020
 * @Description:
 */
public interface LeExamineContentService extends BaseService<LeExamineContent> {
    /**
     * 根据查询条件获取审查细项
     * @param leTaskId  任务编号
     * @param reviewCount 提交到审查的次数
     * @return
     * @throws Exception
     */
    List<LeExamineContent> queryByCondition(Integer leTaskId, Integer reviewCount)throws Exception;

    /**
     * 获取到当前任务的审查结论
     * @param leTaskId
     * @return
     * @throws Exception
     */
    List<LeExamineContent> queryExamineContResult(Integer leTaskId)throws Exception;

    /**
     * 更新审查记录规则一式两份
     *  1.第一次提交为都是通过，但是否处理标记都为空
     *    如果处理标记是空的，只需要把通过的变成不通过就可以
     *  2.被打回后的提交，会去同步一次，上一次的所有审查细项，
     *    在同步的时候就需要去吧上次所有未通过的打回标记上未处理标识
     *  数据例子：
     *    第一次：(无论第一次这么保存，state永远为null)
     *    name:审查细项1    isPass:0    state :null
     *    name:审查细项2    isPass:1    state :null
     *    打回后提交：（这里无论怎么保存，审查细项1永远为null，审查细项2永不为null）
     *    name:审查细项1    isPass:0    state :null
     *    name:审查细项2    isPass:1    state :1
     * @param taskId   审查任务id
     * @param descrData 审查细项
     * @throws Exception
     */
    void updateLeExamineContent(Integer taskId,String descrData)throws Exception;
    
    /**
     * 根据查询条件获取审查细项
     * @param leTaskId  任务编号
     * @param reviewCount 提交到审查的次数
     * @param state 是否通过状态
     * @return
     * @throws Exception
     */
    List<LeExamineContent> queryByUnPass(Integer leTaskId, Integer reviewCount, Integer state)throws Exception;
}
