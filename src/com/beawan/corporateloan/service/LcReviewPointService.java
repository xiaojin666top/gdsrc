package com.beawan.corporateloan.service;

import com.beawan.core.BaseService;
import com.beawan.corporateloan.dto.LcPointRiskDto;
import com.beawan.corporateloan.entity.LcReviewPoint;

import java.util.List;

/**
 * @author yzj
 */
public interface LcReviewPointService extends BaseService<LcReviewPoint> {
    /**
     * 根据审查内容  获取审查内容下的问题点列表
     * @Param indu  所在行业
     * @return
     */
    List<LcPointRiskDto> getPointRiskList(String indu);
}
