package com.beawan.corporateloan.service;

import com.beawan.core.BaseService;
import com.beawan.corporateloan.dto.*;
import com.beawan.core.Pagination;
import com.beawan.corporateloan.entity.LcTask;

import java.util.List;
import java.util.Map;


/**
 * @author yzj
 */
public interface LcTaskService extends BaseService<LcTask> {

    /** 获取合规审查 汇总统计 分页 */
    Pagination<LcTaskStatDto> queryTaskStatPaging(String userNo, String startTime, String endTime, int page, int pageSize);

    /**
     * 获取当前任务存在问题分页列表
     * @param id  任务id
     * @param page
     * @param pageSize
     * @return
     */
    Pagination<LcTaskRiskDto> queryTaskRiskPaging(Integer id, String userNo, int page, int pageSize);

    /** 获取合规审查 问题统计 分页 */
    Pagination<LcRiskStatDto> queryRiskStatPaging(String userNo, String startTime, String endTime, String rectify, int page, int pageSize);

    /** 获取合规审查 汇总统计列表 */
    List<LcTaskStatDto> queryTaskStatList(String userNo, String startTime, String endTime);

    /**
     * 根据任务号获取当前问题列表
     * @param taskId
     * @return
     */
    List<LcTaskRiskDto> queryTaskRiskList(Integer taskId);

    /**
     * 根据统计 时间范围内的问题列表
     * @param userNo
     * @return
     */
    List<LcTaskRiskDto> queryTaskRiskList(String userNo, String startTime, String endTime);

    /**
     * 统计在时间期限内容的 问题出现频次
     * @param userNo  合规经理编号
     * @param startTime  开始时间
     * @param endTime   结束时间
     * @return
     */
    List<LcTaskRiskDto> queryQuestFrequency(String userNo, String startTime, String endTime);

    /** 获取合规审查 问题统计列表 */
    List<LcRiskStatDto> queryRiskStatList(String userNo, String rectify, String startTime, String endTime);

    /** 获取合规审查 客户经理问题统计列表 */
    List<LcManagerRiskStatDto> queryManagerRiskStatList(String userNo, String startTime, String endTime);

    /** 获取合规审查 客户经理问题汇总 分页 */
    Pagination<LcManagerRiskStatDto> queryManagerRiskStatPaging(String userNo, String startTime, String endTime, int page, int pageSize);

    /** 获取合规审查 第二部分 -》笔数统计 ***/
    LcMonthStatDto queryMonthStat(String userNo, String startTime, String endTime);

    /**
     * 获取合规审查报告所需要的数据内容
     * @param userNo
     * @param startTime
     * @param endTime
     * @return
     */
    LcReportDto queryLcReportData(String userNo, String startTime, String endTime);

   /**
     *
     * @param serialNumber
     * @return
     * @throws Exception
     */
    LcTask queryBySerialNumber(String serialNumber)throws Exception;
   
    /**
     * 获取合规审查中的数据库中英文行业分类转成中文（四个行业写死了）
     * @param industry
     * 
     * @return
     */
    String ChineseIndustry(String industry);

	
}
