package com.beawan.corporateloan.service;

import com.beawan.core.BaseService;
import com.beawan.core.Pagination;
import com.beawan.corporateloan.dto.LrResultDto;
import com.beawan.corporateloan.entity.LrRecordList;

/**
 * @author yzj
 */
public interface LrRecordListService extends BaseService<LrRecordList> {
    Pagination<LrResultDto> queryLrResultList(String userNo, String resultType, int page, int rows);
}
