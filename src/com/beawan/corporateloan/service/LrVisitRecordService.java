package com.beawan.corporateloan.service;

import com.beawan.core.BaseService;
import com.beawan.corporateloan.entity.LrVisitRecord;

/**
 * @author yzj
 */
public interface LrVisitRecordService extends BaseService<LrVisitRecord> {
	
}
