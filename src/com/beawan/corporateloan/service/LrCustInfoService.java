package com.beawan.corporateloan.service;

import com.beawan.core.BaseService;
import com.beawan.core.Pagination;
import com.beawan.corporateloan.dto.CustNumDto;
import com.beawan.corporateloan.dto.LrAreaStatDto;
import com.beawan.corporateloan.dto.LrCoverageDto;
import com.beawan.corporateloan.dto.LrCustClassDto;
import com.beawan.corporateloan.dto.LrCustDto;
import com.beawan.corporateloan.dto.LrCustSubClassDto;
import com.beawan.corporateloan.entity.LrCustInfo;

import java.util.List;

/**
 * @author yzj
 */
public interface LrCustInfoService extends BaseService<LrCustInfo> {
	/**
	 * 查询公共池客户分页
	 * @param custNo
	 * @param custName
	 * @param page
	 * @param pageSize
	 * @return
	 */
	Pagination<LrCustDto> queryPubpoolPaging(String custNo, String custName, int page, int pageSize);
	/**
	 * 
	 *@Description 分配客户列表
	 *@param custNo
	 *@param custName
	 *@param page
	 *@param pageSize
	 *@return
	 * @author xyh
	 */
	Pagination<LrCustDto> queryDistributionPaging(String custNo, String custName, int page, int pageSize);
	/**
	 * 查询给名单客户
	 * @param custNo
	 * @param custName
	 * @param page
	 * @param pageSize
	 * @return
	 */
	Pagination<LrCustDto> queryBlacklistPaging(String custNo, String custName, int page, int pageSize);
	/**
	 * 查询营销中客户
	 * @param userNo
	 * @param userName
	 * @param custNo
	 * @param custName
	 * @param page
	 * @param pageSize
	 * @return
	 */
	Pagination<LrCustDto> queryRetailIngCustPaging(String userNo,String orgNo,String userName, String custName,String customerClss, int page, int pageSize);

	/**
	 * 将公共池客户转移给客户经理
	 * @param holdUserNo  营销客户经理编号
	 * @param customerNo 客户编号
	 * @param dealUserNo 操作人编号
	 * @param retailDay 指定营销时间，如果为null设置为默认时间
	 * @return
	 * @throws Exception
	 */
	LrCustInfo updateCustMoveRetailIng(String holdUserNo,String customerNo,String dealUserNo,Integer retailDay)throws Exception;

	/**
	 * 获取客户分层分类统计
	 * @param dataRange	数据范围 1全量 2不包括公共池客户
	 * @param startTime	客户分类变化时间
	 * @param endTime
	 * @return
	 */
	List<LrCustClassDto> queryCustClass(String dataRange, String startTime, String endTime);

	/**
	 * 分页获取客户覆盖率情况
	 * @param startTime	开始拜访时间
	 * @param endTime	结束拜访时间
	 * @param page
	 * @param pageSize
	 * @return
	 */
    Pagination<LrCoverageDto> queryCoverage(String userName, String startTime, String endTime, int page, int pageSize)throws  Exception;

	/**
	 * 分页获取客户 营销转化率情况
	 * @param userName
	 * @param startTime
	 * @param endTime
	 * @param page
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	Pagination<LrCoverageDto> queryConversion(String userName, String startTime, String endTime, int page, int pageSize)throws  Exception;


	/**
	 * 分页获取 分区域客户营销情况
	 * @param userName
	 * @param startTime
	 * @param endTime
	 * @param page
	 * @param pageSize
	 * @return
	 */
    Pagination<LrAreaStatDto> queryAreaStat(String userName, String startTime, String endTime, int page, int pageSize);
    /**
     * 按支行进行客户分层分类
     *@Description
     *@param organNo 如果不为空表示进行，表示进行客户经理的分层分类
     *@param startTime
     *@param endTime
     *@return
     * @author xyh
     */
    Pagination<LrCustSubClassDto> querySubCustClass(int page, int pageSize,String orgName, String startTime, String endTime);
    /**
     * 
     *@Description 支行内客户经理的客户分层分类情况
     *@param organNo
     *@param startTime
     *@param endTime
     *@return
     * @author xyh
     */
    List<LrCustSubClassDto> queryManageInSubCustClass(String organNo, String startTime, String endTime);
    
    /**
     * 统计各个客户分类下的客户数量
     * @param userNo
     * @return
     */
    CustNumDto statCustNumByType(String userNo, String orgNo);
}
