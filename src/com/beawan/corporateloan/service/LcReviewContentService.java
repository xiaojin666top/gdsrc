package com.beawan.corporateloan.service;

import com.beawan.core.BaseService;
import com.beawan.corporateloan.entity.LcReviewContent;

import java.util.List;
import java.util.Map;

/**
 * @author yzj
 */
public interface LcReviewContentService extends BaseService<LcReviewContent> {
    /**
     * 保存 合规审查详细内容
     * @param taskId
     * @param descrData
     */
    void saveOrUpdateContent(Integer taskId, String descrData);

    /**
     * 初始化 合规审查要点
     * @param taskId
     * @param indu
     */
    void updateContent(Integer taskId, String indu)throws Exception;
}
