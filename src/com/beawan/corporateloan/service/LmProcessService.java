package com.beawan.corporateloan.service;

import com.beawan.core.BaseService;
import com.beawan.core.Pagination;
import com.beawan.corporateloan.dto.*;
import com.beawan.corporateloan.entity.LmProcess;
import com.beawan.task.bean.Task;

import java.util.Map;

/**
 * 
 * @ClassName: LmProcessService
 * @author xyh
 * @date 16 Jun 2020
 *
 */
public interface LmProcessService extends BaseService<LmProcess>{

	/**
	 * 查询流程信息的分页
	 * @param userNo 节点操作员编号，like
	 * @param stageType 查询类型 等于、大于、小于
	 * @param stage 所处节点
	 * @param page
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	Pagination<LmTaskInfoDto> queryLmProcessPaging(String userNo,String stageType,int stage,int page, int pageSize)throws Exception;

	/**
	 * 重载方法
	 * @param params	过滤条件，暂定key为customerName customerNo
	 * @param userNo	操作员编号
	 * @param stageType
	 * @param stage
	 * @param page
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	Pagination<LmTaskInfoDto> queryLmProcessPaging(Map<String, String> params,String orgNo, String userNo, String stageType, int stage, int page, int pageSize)throws Exception;


	/**
	 * 获取营销提交后的任务列表
	 * @param userNo  营销人编号
	 * @param page
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	Pagination<LmTaskInfoDto> queryUpRetailPaging(String userNo, String customerNo, String orgNo, String customerName,
			int page, int pageSize) throws Exception;
	/**
	 * 获取直接结束的营销任务列表
	 * @param userNo  营销人编号
	 * @param page
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	Pagination<LmTaskInfoDto> queryDownRetailPaging(String userNo,int page,int pageSize)throws Exception;

	/**
	 * 查看合规审查 任务列表 分页
	 * @param stage   状态 301待处理
	 * @param page
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	Pagination<LcTaskDto> queryLcTaskPaging(int stage,String userNo,String orgNo, String custName, String userName, String startTime, String endTime, int page, int pageSize)throws Exception;

	/**
	 * 查看合规审查 已提交任务列表 分页
	 * @param stage   状态 大于301都是已提交状态
	 * @param page
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	Pagination<LcTaskDto> queryLcTaskFinishPaging(int stage,String userNo,String orgNo, String custName, String userName, String startTime, String endTime, int page, int pageSize)throws Exception;

	/**
	 * 营销进件提交任务指定客户经理
	 * @param serialNumber
	 * @param userNo	调查客户经理编号
	 * @param updater	操作人编号
	 *
	 */
	void updateAppointLbUser(String serialNumber, String userNo, String userName, String updater) throws Exception;

	/**
	 * 新增授信开始贷前调查
	 * @param serialNumber
	 * @param userNo
	 * @param userName
	 * @param updater
	 * @throws Exception
	 */
	void saveLbTask(String serialNumber, String customerNo, String userNo, String userName, String updater) throws Exception;

	/**
	 * 分页获取贷前 任务列表
	 * @param custName	客户名称模糊查询
	 * @param stage		状态 待确认状态202   调查中任务203
	 * @param page
	 * @param pageSize
	 * @return
	 */
	Pagination<Task> queryLbTaskPager(String userNo, String orgNo, String custName, String stage, int page, int pageSize);

	/**
	 * 获取客户在合规审查中的  自动行业分类
	 * 合规审查的行业是用来对应 审查的风险项
	 * @param custNo
	 * @return
	 */
	String getCompInduInLc(String custNo) throws Exception;

	/**
	 * 提交贷前调查报告至合规审查
	 * @param serNo
	 */
	void updateLbTask(String serNo, String userNo)throws Exception;

	/**
	 * 打回合规审查任务
	 * @param serNo 	合规审查任务流水号
	 * @param userNo	操作人号
	 */
    void repluseCompliance(String serNo, String userNo)throws Exception;

	/**
	 * 获取 待审查任务列表
	 * @param stage
	 * @param page
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	Pagination<LeTaskDto> queryLeTaskPaging(int stage,String userNo,String orgNo, int page, int pageSize)throws Exception;

	/**
	 * 获取 审查通过分页信息
	 * @param userNo
	 * @param page
	 * @param pageSize
	 * @return
	 * @throws Exception
	 */
	Pagination<LeTaskDto> queryLePassTaskPaging(String customerName,String userNo,String orgNo,int page,int pageSize)throws Exception;
	
	/**
	 * 
	 *@Description 信贷审查分配
	 *@param serialNumber
	 *@param distributionUserNo  被分配到任务的审查人
	 *@param userNo 操作人
	 *@throws Exception
	 * @author xyh
	 */
	void updateLeTask(String serialNumber,String distributionUserNo,String userNo)throws Exception;
	
	/**
	 * 信贷审查打回---整个流程的控制是否可以被统一
	 * @param serNo
	 * @param userNo
	 * @throws Exception
	 */
	void repluseExamine(String serNo, String userNo)throws Exception;
	/**
	 * 提交到贷款审查
	 * 如果流水号查不到审查任务：
	 *     查询出所有符合条件的审查细项，插入到审查细项记录表
	 * 如果查的到：
	 *     查询出上一次的审查细项记录，复制一份，并为其中没有通过的细项记录，设置上未处理
	 *
	 * @param serialNumber
	 * @param userNo
	 * @throws Exception
	 */
	void submitToExamine(String serialNumber,String userNo)throws Exception;

	

}
