package com.beawan.corporateloan.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.base.dto.RandomizingID;
import com.beawan.base.entity.User;
import com.beawan.base.service.ISysDicSV;
import com.beawan.base.service.IUserSV;
import com.beawan.common.BusinessException;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.Pagination;
import com.beawan.core.ResultDto;
import com.beawan.corporateloan.dto.CustNumDto;
import com.beawan.corporateloan.dto.LmTaskInfoDto;
import com.beawan.corporateloan.dto.LrAreaStatDto;
import com.beawan.corporateloan.dto.LrCoverageDto;
import com.beawan.corporateloan.dto.LrCustClassDto;
import com.beawan.corporateloan.dto.LrCustDto;
import com.beawan.corporateloan.dto.LrCustSubClassDto;
import com.beawan.corporateloan.entity.LrCustInfo;
import com.beawan.corporateloan.entity.LrInfo;
import com.beawan.corporateloan.entity.LrRecordList;
import com.beawan.corporateloan.entity.LrVisitRecord;
import com.beawan.corporateloan.service.LmProcessService;
import com.beawan.corporateloan.service.LrCustInfoService;
import com.beawan.corporateloan.service.LrInfoService;
import com.beawan.corporateloan.service.LrRecordListService;
import com.beawan.corporateloan.service.LrVisitRecordService;
import com.beawan.corporateloan.vo.LrVisitRecordVO;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.model.entity.AdmitResult;
import com.beawan.model.service.AdmitResultService;
import com.beawan.qcc.service.SynchronizationQCCService;
import com.platform.util.DateUtil;
import com.platform.util.HttpUtil;
import com.platform.util.RgnshUtils;
import com.platform.util.StringUtil;

/**
 * @Author: xyh
 * @Date: 22/06/2020
 * @Description:  <generator class="native"></generator>控制器
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/retail/" })
public class LoanRetailController extends BaseController {
	private static final Logger log = Logger.getLogger(LoanRetailController.class);
	@Resource
	private ISysDicSV sysDicSV;
	@Resource
	private ICusBaseSV cusBaseSV;
	@Resource
	private IUserSV userSV;
	@Resource
	private LrRecordListService lrRecordListService;
	@Resource
	private LmProcessService lmProcessService;
	@Resource
	private LrInfoService lrInfoService;
	@Resource
	private LrVisitRecordService lrVisitRecordService;
	@Resource
	private LrCustInfoService lrCustInfoService;
	@Resource
	private AdmitResultService admitResultService;
	@Resource
	private SynchronizationQCCService synchronizationQCCService;


	/**
	 * 跳转到我的营销客户界面
	 * @param request
	 * @return
	 */
	@RequestMapping("myLrCust.do")
	public ModelAndView productMge(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView("views/retail/lrcust/myLrCust");
		try {
			User user = HttpUtil.getCurrentUser(request);
			String userDA = user.getDataAccess();
			String userNo = null;
			String orgNo = null;
			if(SysConstants.UserDataAccess.PERSONAL.equals(userDA)) {
				userNo = user.getUserId();
			}else if(SysConstants.UserDataAccess.ORG.equals(userDA)) {
				orgNo = user.getAccOrgNo();
			}
			CustNumDto result = lrCustInfoService.statCustNumByType(userNo, orgNo);
			mav.addObject("statData", result);
			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.RG_STREET_TOWNSHIP};
			mav.addObject("dicData",sysDicSV.queryMapListByTypes(optTypes));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}
	/**
	 * @Author: xyh
	 * @Date: 12/08/2020
	 * @Description:  跳转到营销客户的界面
	 *               custNo：客户编号
	 *               stage：是否可以编辑
	 */
	@RequestMapping("retailCustIndex.do")
	public ModelAndView retailCustIndex(HttpServletRequest request, String custNo
			, String stage){
		ModelAndView mav = new ModelAndView("views/retail/retail_cust_index");
		User user = HttpUtil.getCurrentUser(request);
		try{
			//查询客户编号
			CusBase cusBase = cusBaseSV.queryByCusNo(custNo);
			mav.addObject("custNo", cusBase.getCustomerNo());
			mav.addObject("custName", cusBase.getCustomerName());

			mav.addObject("stage", stage);
			mav.addObject("user", user);


			//获取是否存在准入信息
			Integer maxTimes = admitResultService.getMaxTimes(custNo);
			Map<String, Object> params = new HashMap<>();
			params.put("times", maxTimes);
			params.put("custNo", custNo);
			List<AdmitResult> admitResultList = admitResultService.selectByProperty(params);
			mav.addObject("admitList", admitResultList);
		}catch (Exception e){
			e.printStackTrace();
		}
		return mav;
	}


	/**
	 * 跳转到开始营销 继续营销  营销进件 通过状态stage控制页面左侧菜单栏内容首页界面
	 * @param serNo 任务流水号
	 * @param custNo
	 * @return
	 */
	@RequestMapping("retailTask.do")
	public ModelAndView retailTask(HttpServletRequest request, String serNo, String custNo
			, String custName, String stage, String id, String func){
		ModelAndView mav = new ModelAndView("views/retail/retail_index");
		User user = HttpUtil.getCurrentUser(request);
		try {
			mav.addObject("id", id);
			mav.addObject("serNo", serNo);
			mav.addObject("custNo", custNo);
			mav.addObject("stage", stage);
			mav.addObject("custName", URLDecoder.decode(custName, "UTF-8"));
			mav.addObject("user", user);
			mav.addObject("func", func);

			//获取是否存在准入信息
			Integer maxTimes = admitResultService.getMaxTimes(custNo);
			Map<String, Object> params = new HashMap<>();
			params.put("times", maxTimes);
			params.put("custNo", custNo);
			List<AdmitResult> admitResultList = admitResultService.selectByProperty(params);
			mav.addObject("admitList", admitResultList);
		}catch (Exception e){
			e.printStackTrace();
		}
		return mav;
	}

	/**
	 * 营销经理手动新增客户
	 * 新增客户是不存在LR_CUST_INFO表中，若存在直接返回失败，提示营销经理  该客户已经存在
	 * 若新增，则根据新增的操作人，自动转入到操作人的管户客户名单下，且营销期限为1个月
	 * @return
	 */
	@RequestMapping("saveNewCustomer.json")
	@ResponseBody
	public ResultDto saveNewCustomer(HttpServletRequest request, String customerName, String custAddr){
		ResultDto re = returnFail();
		if(StringUtil.isEmptyString(customerName) || StringUtil.isEmptyString(custAddr)){
			return returnFail("请将信息补充完整再提交！");
		}

		String userId = HttpUtil.getCurrentUser(request).getUserId();
		try{
			CusBase cusBase = cusBaseSV.queryByCusName(customerName);
			if(cusBase != null){ // 当前客户名在系统中已存在且为全客户名单，里面有一些是存量客户名单，不在营销客户名单中
				String customerNo = cusBase.getCustomerNo();
				LrCustInfo entity = lrCustInfoService.selectSingleByProperty("customerNo", customerNo);
				if(entity != null){
					String customerType = entity.getCustomerLrType();
					if(Constants.LrCustType.BLACK_CUST.equals(customerType)){
						return returnFail("该客户：" + customerName + " 为黑名单客户，保存失败！");
					}else if(Constants.LrCustType.RETAIL_CUST.equals(customerType)){
						return returnFail("该客户：" + customerName + " 已有营销经理管户，保存失败！");
					}else{
						//当前新增的客户是公共池客户，执行同领取客户的操作
						log.info(customerName + "是公共池客户,继续执行");
						//将客户转移到营销中状态，记录到营销记录表中
						lrCustInfoService.updateCustMoveRetailIng(userId, customerNo, userId, null);

						synchronizationQCCService.syncQccAllData(customerNo);
						cusBase.setQccSyncTime(DateUtil.getNowY_m_dStr());
						cusBaseSV.save(cusBase);
						//准入分析
						admitResultService.saveAdmitCheck(customerNo);
					}
				}else{
					//将客户转移到营销中状态，记录到营销记录表中
					LrCustInfo lrCust = createLrCustInfo(customerNo, userId, Constants.LrCustType.RETAIL_CUST);
					lrCust.setCustomerClass(Constants.CustClassify.LEADS);
					lrCust.setCustAddr(custAddr);
					lrCust.setHoldUserNo(userId);
					lrCust.setExpireStatus(Constants.RetailReal.NOT_SUCC);
					lrCust.setExpireTime(DateUtil.getAddDayTimestamp(30));
					lrCustInfoService.saveOrUpdate(lrCust);
				}
			}else{ // 全新客户，保存到CusBase表中
				CusBase entity = new CusBase();
				entity.setCustomerName(customerName);
				//GGG插入企查查的数据接口
				entity.setCustomerNo(StringUtil.genRandomNo());
				entity.setUpdateDate(DateUtil.getNowYmdStr());
				entity.setCreater(userId);
				entity.setUpdater(userId);
				entity.setUpdateTime(DateUtil.getNowTimestamp());
				cusBaseSV.save(entity);

				//将客户转移到营销中状态，记录到营销记录表中
				LrCustInfo lrCust = createLrCustInfo(entity.getCustomerNo(), userId ,Constants.LrCustType.RETAIL_CUST);
				lrCust.setCustomerClass(Constants.CustClassify.LEADS);
				lrCust.setCustAddr(custAddr);
				lrCust.setHoldUserNo(userId);
				lrCust.setExpireStatus(Constants.RetailReal.NOT_SUCC);
				lrCust.setExpireTime(DateUtil.getAddDayTimestamp(30));
				lrCustInfoService.saveOrUpdate(lrCust);

				synchronizationQCCService.syncQccAllData(lrCust.getCustomerNo());
				entity.setQccSyncTime(DateUtil.getNowY_m_dStr());
				cusBaseSV.save(entity);

				//准入分析
				admitResultService.saveAdmitCheck(lrCust.getCustomerNo());



			}

			re = returnSuccess();
			re.setMsg("保存营销客户成功");
		}catch (Exception e){
			e.printStackTrace();
			return returnFail("该客户：" + customerName + " 数据未查到，请检查客户信息！");
		}
		return re;
	}


	/**
	 * 管理员上传如皋本地规模以上企业名单 xls
	 *
	 * @param request
	 * @param response
	 * @param dataFile
	 * @return
	 */
	@RequestMapping("uploadExcel.json")
	@ResponseBody
	public ResultDto uploadExcel(HttpServletRequest request, HttpServletResponse response,
								 @RequestParam(value = "file", required = false) MultipartFile dataFile) {
		ResultDto re = returnFail("上传失败，请重试！");
		// 专门给ie和火狐下使用 uploadify插件
		request.setAttribute(HandlerMapping.PRODUCIBLE_MEDIA_TYPES_ATTRIBUTE, null);
		RandomizingID random = new RandomizingID("ID", "yyyyMMddHHmmss", 4, false);

		User sessionUser = HttpUtil.getCurrentUser(request);

		try {
			String fileName = dataFile.getOriginalFilename().toString();
			// 流水号或者时间
			// xxx
			File target = new File("D:\\RG_PRO\\file\\", random.genNewId() + ".xls");
			if (!target.exists()) {
				target.mkdirs();
			}
			dataFile.transferTo(target);
			// 分析excel
			InputStream excelInputStream = new FileInputStream(target);

			Workbook wb = null;
			if (fileName.endsWith(".xls")) {
				wb = new HSSFWorkbook(excelInputStream);
			} else if (fileName.endsWith(".xlsx")) {
				wb = new XSSFWorkbook(excelInputStream);
			}
			Sheet sheet = wb.getSheetAt(0);
			Row firstRow = sheet.getRow(0);

			int totalRows = sheet.getPhysicalNumberOfRows();
			int rowCells = firstRow.getPhysicalNumberOfCells();

			// 客户名称 列
			int custCol = -1;
			// 客户所在街道 列
			int addrCol = -1;
			// 客户经理 列
			int userCol = -1;
			for (int i = 0; i < rowCells; i++) {
				if ("客户名称".equals(firstRow.getCell(i).getStringCellValue())) {
					custCol = i;
				} else if ("街道乡镇".equals(firstRow.getCell(i).getStringCellValue())) {
					addrCol = i;
				} else if ("客户经理".equals(firstRow.getCell(i).getStringCellValue())) {
					userCol = i;
				}
			}
			if (custCol == -1) {
				throw new Exception("未找到客户名称列");
			}
			if (userCol == -1) {
				throw new Exception("未找到客户经理列");
			}
			String date = DateUtil.format(new Date(), Constants.DATE_MASK);
			for (int i = 1; i < totalRows; i++) {
				String custName = sheet.getRow(i).getCell(custCol).getStringCellValue();
				String custAddr = sheet.getRow(i).getCell(addrCol).getStringCellValue();
				String userName = sheet.getRow(i).getCell(userCol).getStringCellValue();

				System.out.println(custName + "--" + custAddr + "--" + userName);

				CusBase entity = cusBaseSV.queryByCusName(custName);
				// 新客户
				if (entity == null) {
					//客户信息保存到CusBase表中
					entity = new CusBase();
					entity.setCustomerName(custName);
					entity.setCustomerNo(StringUtil.genRandomNo());
					entity.setUpdateDate(date);
					cusBaseSV.save(entity);
				} else {
					// 存量客户
					entity.setUpdateDate(date);
				}
				//如果营销经理为空--表示客户为公共池客户
				if (userName == null || "".equals(userName)) {
					LrCustInfo pubPoolCust = createLrCustInfo(entity.getCustomerNo(),sessionUser.getUserId(),Constants.LrCustType.PUB_POOL_CUST);
					pubPoolCust.setCustAddr(custAddr);
					lrCustInfoService.saveOrUpdate(pubPoolCust);
					continue;
				}
				List<User> userList = userSV.getUserByFullName(userName);
				if (userList == null || userList.size() == 0) {
					LrCustInfo pubPoolCust = createLrCustInfo(entity.getCustomerNo(),sessionUser.getUserId(),Constants.LrCustType.PUB_POOL_CUST);
					pubPoolCust.setCustAddr(custAddr);
					lrCustInfoService.saveOrUpdate(pubPoolCust);
					log.info("客户经理名称： " + userName + "在系统中未找到该客户经理，请核实!该客户进入公共池");
				} else if (userList.size() > 1) {
					log.info("客户经理名称： " + userName + "在系统中存在重名，请确认!");
					continue;
				} else {
					//可以查询到管理经理
					User user = userList.get(0);
					LrCustInfo custInfo = lrCustInfoService.selectSingleByProperty("customerNo", entity.getCustomerNo());
					if(custInfo==null){
						custInfo = createLrCustInfo(entity.getCustomerNo(),sessionUser.getUserId(),Constants.LrCustType.RETAIL_CUST);
					}else{
						custInfo.setCustomerNo(entity.getCustomerNo());
						custInfo.setCustomerLrType(Constants.LrCustType.RETAIL_CUST);
						custInfo.setCreater(sessionUser.getUserId());
						custInfo.setUpdater(sessionUser.getUserId());
						custInfo.setUpdateTime(DateUtil.getNowTimestamp());
						custInfo.setCustomerClass(Constants.CustClassify.LEADS);
					}
					custInfo.setHoldUserNo(user.getUserId());
					custInfo.setExpireStatus(Constants.RetailReal.NOT_SUCC);
					custInfo.setCustAddr(custAddr);
					lrCustInfoService.saveOrUpdate(custInfo);
				}
				//
				// 去信贷系统查询 客户经理名称为 userName的 然后将客户经理的id插入Crm表中
				synchronizationQCCService.syncQccAllData(entity.getCustomerNo());
				entity.setQccSyncTime(DateUtil.getNowY_m_dStr());
				cusBaseSV.save(entity);
				//准入分析
				admitResultService.saveAdmitCheck(entity.getCustomerNo());


				re.setCode(ResultDto.RESULT_CODE_SUCCESS);
				re.setMsg("数据导入成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 消除重复代码，创建一个空的客户
	 * @param customerNo 客户编号
	 * @param userNo 用户编号-创建客户经理
	 * @param lrCustType 客户类型
	 * @return
	 */
	private LrCustInfo createLrCustInfo(String customerNo,String userNo,String lrCustType){
		LrCustInfo lrCustInfo = new LrCustInfo();
		lrCustInfo.setCustomerNo(customerNo);
		lrCustInfo.setCustomerLrType(lrCustType);
		lrCustInfo.setCreater(userNo);
		lrCustInfo.setUpdater(userNo);
		lrCustInfo.setUpdateTime(DateUtil.getNowTimestamp());
		lrCustInfo.setCustomerClass(Constants.CustClassify.LEADS);
		return lrCustInfo;
	}






	/**
	 * 跳转到营销公共池列表
	 *
	 * @return
	 */
	@RequestMapping("pubpool.do")
	public ModelAndView pubpool() {
		ModelAndView mav = new ModelAndView("views/retail/lrcust/pubpool");
		//获取当前登录用户
		User curUser = HttpUtil.getCurrentUser();
		//如果是<generator class="native"></generator>员或者是超级管理原，返回管理员角色为1
		//在界面上会显示分配客户按钮
		if(curUser!= null && !StringUtil.isEmpty(curUser.getRoleNo())&&
				(curUser.getRoleNo().contains(Constants.ROLE.LR_MANAGER)||
						curUser.getRoleNo().contains(Constants.ROLE.MAX_MANAGER))){
			mav.addObject("manager", 1);
		}else{
			mav.addObject("manager", 0);
		}
		return mav;
	}

	/**
	 * 查询公共池客户列表
	 * @param customerNo
	 * @param customerName
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping("getPubPoolCustList.json")
	@ResponseBody
	public ResultDto getPubPoolCustList(String customerNo, String customerName,
										int page, int rows) {
		ResultDto re = returnFail("查询公共池客户列表失败");
		try {
			Pagination<LrCustDto> pager = lrCustInfoService.queryPubpoolPaging(customerNo, customerName,
					page, rows);
			re = returnSuccess();
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());
			re.setMsg("获取公共池列表成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}


	/**
	 * 跳转到营销可分配客户列表
	 *
	 * @return
	 */
	@RequestMapping("goDistribution.do")
	public ModelAndView goDistribution() {
		ModelAndView mav = new ModelAndView("views/retail/lrcust/distribution");
		//获取当前登录用户
		User curUser = HttpUtil.getCurrentUser();
		//如果是<generator class="native"></generator>员或者是超级管理原，返回管理员角色为1
		//在界面上会显示分配客户按钮
		if(curUser!= null && !StringUtil.isEmpty(curUser.getRoleNo())&&
				(curUser.getRoleNo().contains(Constants.ROLE.LR_MANAGER) ||
						curUser.getRoleNo().contains(Constants.ROLE.MAX_MANAGER))){
			mav.addObject("manager", 1);
		}else{
			//二进制中0表示假
			mav.addObject("manager", 0);
		}
		return mav;
	}

	/**
	 * 查询可分配客户列表
	 * @param customerNo
	 * @param customerName
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping("getDistributionCustList.json")
	@ResponseBody
	public ResultDto getDistributionCustList(String customerNo, String customerName,
											 int page, int rows) {
		ResultDto re = returnFail("查询可分配客户列表失败");
		try {
			Pagination<LrCustDto> pager = lrCustInfoService.queryDistributionPaging(customerNo, customerName,
					page, rows);
			re = returnSuccess();
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());
			re.setMsg("获取可分配客户列表成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}



	/**
	 * 跳转到营销黑名单列表
	 *
	 * @return
	 */
	@RequestMapping("blacklist.do")
	public ModelAndView blacklist() {
		ModelAndView mav = new ModelAndView("views/retail/lrcust/blacklist");
		return mav;
	}

	/**
	 * 查询黑名单客户列表
	 * @param customerNo
	 * @param customerName
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping("getblacklist.json")
	@ResponseBody
	public ResultDto getblacklist(String customerNo, String customerName, int page,
								  int rows) {
		ResultDto re = returnFail("查询黑名单列表失败");
		try {
			Pagination<LrCustDto> pager = lrCustInfoService.queryBlacklistPaging(customerNo,customerName,page,rows);
			re = returnSuccess();
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());
			re.setMsg("获取黑名单列表成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 查询营销客户列表
	 * @param request
	 * @param customerNo
	 * @param customerName
	 * @param userNo
	 * @param userName
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping("getRetailCustList.json")
	@ResponseBody
	public ResultDto getRetailCustList(HttpServletRequest request,
									   String customerName, String customerClass, String userName, int page, int rows) {
		ResultDto re = returnFail("查询营销客户列表失败");
		// 获取到登录的用户编号
		User user = HttpUtil.getCurrentUser(request);
		String userNo = null;
		String orgNo = null;
		try {
			String userDA = user.getDataAccess();
			if(SysConstants.UserDataAccess.PERSONAL.equals(userDA)) {
				userNo = user.getUserId();
			}else if(SysConstants.UserDataAccess.ORG.equals(userDA)) {
				orgNo = user.getAccOrgNo();
			}
			Pagination<LrCustDto> pager = lrCustInfoService.queryRetailIngCustPaging(userNo,orgNo, userName, customerName,customerClass,
					page, rows);
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());
			re.setMsg("获取营销客户列表成功");
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 将客户移动到公共池 （管理员权限）
	 *
	 * @param custNo
	 * @return
	 */
	@RequestMapping("movePubPool.json")
	@ResponseBody
	public ResultDto movePubPool(String custNo) {
		ResultDto re = new ResultDto();

		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 跳转到
	 * 分配具体营销经理界面
	 * @return
	 */
	@RequestMapping("selectUser.do")
	public ModelAndView selectUser(String custNo) {
		ModelAndView mav =  new ModelAndView("views/retail/lrcust/query_user");
		mav.addObject("custNo", custNo);
		return mav;
	}

	/**
	 * 管理员分配客户给客户经理去营销
	 * @param custNo 客户编号
	 * @param userNo 用户编号
	 * @param retailDay 营销天数，如果不指定天数，就设置为默认的配置天数
	 * @return
	 */
	@RequestMapping("moveRetailIng.json")
	@ResponseBody
	public ResultDto moveRetailIng(HttpServletRequest request,String custNo,String userNo,Integer retailDay) {
		ResultDto re = new ResultDto();
		if(StringUtil.isEmptyString(custNo) || StringUtil.isEmptyString(userNo)){
			re.setMsg("分配信息不完整");
			log.error("分配信息不完整");
			return re;
		}
		// 获取到登录的用户编号
		User user = HttpUtil.getCurrentUser(request);
		try{
			//将客户转移到营销中状态，记录到营销记录表中
			lrCustInfoService.updateCustMoveRetailIng(userNo,custNo,user.getUserId(),retailDay);
			re = returnSuccess();
		}catch (BusinessException e){
			re.setMsg(e.getMessage());
			log.error(e.getMessage());
		}catch (Exception e){
			re.setMsg("系统异常");
			log.error("系统异常");
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 从公共池领取客户
	 * @param request
	 * @param customerNo
	 * @return
	 */
	@RequestMapping("getRetailToSelf.json")
	@ResponseBody
	public ResultDto getRetailToSelf(HttpServletRequest request,String customerNo){
		ResultDto re = returnFail("获取客户失败");
		if(StringUtil.isEmptyString(customerNo)){
			re.setMsg("客户编号为空");
			log.error("客户编号为空");
			return re;
		}
		// 获取到登录的用户编号
		User user = HttpUtil.getCurrentUser(request);
		try{
			//将客户转移到营销中状态，记录到营销记录表中
			lrCustInfoService.updateCustMoveRetailIng(user.getUserId(),customerNo,user.getUserId(),null);
			re = returnSuccess();
		}catch (BusinessException e){
			re.setMsg(e.getMessage());
			log.error(e.getMessage());
		}catch (Exception e){
			re.setMsg("系统异常");
			log.error("系统异常");
			e.printStackTrace();
		}
		return re;
	}


	/************************************* 营销任务 *********************/
	/**
	 * 开始营销第一步 后台创建营销任务
	 *
	 * @param custNo
	 * @return
	 */
	@RequestMapping(value = "startRetail.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto startRetail(HttpServletRequest request, String custNo) {
		ResultDto re = returnFail("创建任务失败");
		// 获取到登录的用户编号
		User user = HttpUtil.getCurrentUser(request);
		try {
			// 新建营销信息表--并对主表的当前操作人设置为当前用户
			LrInfo lrInfo = lrInfoService.createNewLrTask(custNo, user.getUserId());
			re = returnSuccess();
			re.setRows(lrInfo);
			re.setMsg("创建任务成功");
		} catch (BusinessException e) {
			re = returnFail(e.getMessage());
			log.error(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 跳转到营销中任务列表界面
	 * @author: xyh
	 * @date:   2020年6月20日 上午10:55:50
	 * @return
	 */
	@RequestMapping("retailIngTask.do")
	public ModelAndView retailIngTask() {
		ModelAndView mav = new ModelAndView("views/retail/lrtask/retailIngTaskList");
		return mav;
	}

	/**
	 * 客户拜访记录
	 * @return
	 */
	@RequestMapping("custRetailVisitList.do")
	public ModelAndView custRetailVisitList(String customerNo,int stage) {
		ModelAndView mav = new ModelAndView("views/retail/lrcust/custRetailVisitList");
		mav.addObject("customerNo",customerNo);
		mav.addObject("stage",stage);
		return mav;
	}

	@RequestMapping(value = "getCustRetailVisitPaging.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto getCustRetailVisitPaging(HttpServletRequest request,String customerNo, int stage, int page, int rows) {
		ResultDto re = returnFail();
		User user = HttpUtil.getCurrentUser(request);
		try{
			Map<String,String> paramMap = new HashMap<>();
			paramMap.put("customerNo",customerNo);
			Pagination<LmTaskInfoDto> pager =
					lmProcessService.queryLmProcessPaging(paramMap,null, user.getUserId(), ">=",
							stage, page, rows);
			re = returnSuccess();
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());
		}catch (Exception e){
			e.printStackTrace();
		}
		return re;
	}




	/**
	 * 营销中任务列表
	 * @author: xyh
	 * @date:   2020年6月20日 上午11:21:30
	 * @return
	 */
	@RequestMapping("retailTaskIn.do")
	public ModelAndView retailTaskIn() {
		ModelAndView mav = new ModelAndView("views/retail/lrtask/retailTaskInList");
		return mav;
	}

	/**
	 * 已提交任务----在营销之后的任务
	 * @author: xyh
	 * @date:   2020年6月20日 上午11:21:37
	 * @return
	 */
	@RequestMapping("retailTaskUp.do")
	public ModelAndView retailTaskUp() {
		ModelAndView mav = new ModelAndView("views/retail/lrtask/retailTaskUpList");
		return mav;
	}

	/**
	 * 已结束任务
	 * @author: xyh
	 * @date:   2020年6月20日 上午11:21:37
	 * @return
	 */
	@RequestMapping("retailTaskDown.do")
	public ModelAndView retailTaskDown() {
		ModelAndView mav = new ModelAndView("views/retail/lrtask/retailTaskDownList");
		return mav;
	}



	/**
	 * 获取营销中节点的分页
	 *
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(value = "getRetailIngPaging.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto getRetailIngPaging(HttpServletRequest request, int stage, int page, int rows,
										String customerNo,String customerName) {
		ResultDto re = returnFail();
		// 获取到登录的用户编号
		User curUser = HttpUtil.getCurrentUser(request);
		String userNo = null;
		String orgNo = null;
		try {
			Map<String, String> query = new HashMap<>();
			if(!StringUtils.isEmpty(customerNo)){
				query.put("customerNo", customerNo);
			}
			if(!StringUtils.isEmpty(customerName)){
				query.put("customerName", customerName);
			}
			String userDA = curUser.getDataAccess();
			if(SysConstants.UserDataAccess.PERSONAL.equals(userDA)) {
				userNo = curUser.getUserId();
			}else if(SysConstants.UserDataAccess.ORG.equals(userDA)) {
				orgNo = curUser.getAccOrgNo();
			}

			Pagination<LmTaskInfoDto> pager =
					lmProcessService.queryLmProcessPaging(query,orgNo, userNo, "=", stage, page, rows);
			re = returnSuccess();
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	/**
	 * 获取营销提交后的列表
	 * @param request
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(value = "getUpRetailPaging.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto getUpRetailPaging(HttpServletRequest request ,String customerNo, String customerName,
									   int page, int rows){
		ResultDto re = returnFail();
		// 获取到登录的用户编号
		User user = HttpUtil.getCurrentUser(request);
		String userNo = null;
		String orgNo = null;
		try {
			String userDA = user.getDataAccess();
			if(SysConstants.UserDataAccess.PERSONAL.equals(userDA)) {
				userNo = user.getUserId();
			}else if(SysConstants.UserDataAccess.ORG.equals(userDA)) {
				orgNo = user.getAccOrgNo();
			}


			Pagination<LmTaskInfoDto> pager = lmProcessService.queryUpRetailPaging(userNo,orgNo,
					customerNo, customerName, page,rows);
			re = returnSuccess();
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}


	/**
	 * 获取直接结束的营销任务列表
	 * @param request
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping(value = "getDownRetailPaging.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto getDownRetailPaging(HttpServletRequest request ,int page, int rows){
		ResultDto re = returnFail();
		// 获取到登录的用户编号
		User user = HttpUtil.getCurrentUser(request);
		try {
			Pagination<LmTaskInfoDto> pager = lmProcessService.queryDownRetailPaging(user.getUserId(), page,rows);
			re = returnSuccess();
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 跳转到拜访记录界面
	 * @param serNo	业务流水号
	 * @return
	 */
	@RequestMapping("visitInfo.do")
	public ModelAndView visitInfo(String serNo, String customerNo, String stage){
		ModelAndView mav = new ModelAndView("views/retail/lrtask/visitInfo");
		if(StringUtil.isEmptyString(stage)){
			stage = Constants.EDIT;
		}
		mav.addObject("stage", stage);
		mav.addObject("serNo", serNo);
		mav.addObject("customerNo", customerNo);
		return mav;
	}

	/**
	 * 跳转到 修改对接信息内容页
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("editVisitInfo.do")
	public ModelAndView editVisitInfo(HttpServletRequest request, HttpServletResponse response,
									  Integer id) {
		ModelAndView mav = new ModelAndView("views/retail/lrtask/editVisitInfo");
		try {
			String[] optTypes = {SysConstants.BsDicConstant.RG_VISIT_TYPE};
			String dicData = sysDicSV.queryMapListByTypes(optTypes);
			mav.addObject("dicData", dicData);
			LrVisitRecord lrVisitRecord = lrVisitRecordService.findByPrimaryKey(id);
			if(lrVisitRecord == null){
				lrVisitRecord = new LrVisitRecord();
			}
			mav.addObject("lrVisitRecord",lrVisitRecord);
		}catch (Exception e){
			log.error("跳转添加对接信息页面异常", e);
			e.printStackTrace();
		}
		return mav;
	}
	@RequestMapping("addVisitInfo.do")
	public ModelAndView addVisitInfo(HttpServletRequest request, HttpServletResponse response,String serialNumber) {
		ModelAndView mav = new ModelAndView("views/retail/lrtask/addVisitInfo");
		try {
			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.RG_VISIT_TYPE};
			String dicData = sysDicSV.queryMapListByTypes(optTypes);
			mav.addObject("dicData", dicData);
			mav.addObject("serialNumber",serialNumber);
		}catch (Exception e){
			log.error("跳转添加对接信息页面异常", e);
			e.printStackTrace();
		}
		return mav;
	}


	/**
	 * 查询营销任务中的拜访信息，授信信息
	 *
	 * @param serialNumber
	 * @return
	 */
	@RequestMapping(value = "getRetailInfo.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto getRetailInfo(HttpServletRequest request, String serialNumber) {
		ResultDto re = returnFail("获取拜访记录,授信信息失败");
		if (StringUtil.isEmptyString(serialNumber)) {
			log.error("获取拜访记录,授信信息失败");
			return re;
		}
		try {
			//营销信息
			LrInfo lrInfo = lrInfoService.selectSingleByProperty("serialNumber", serialNumber);
			re = returnSuccess();
			re.setRows(lrInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	@RequestMapping(value = "saveRetailInfo.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto saveRetailInfo(HttpServletRequest request, String serialNumber,Integer business) {
		ResultDto re = returnFail();
		if (StringUtil.isEmptyString(serialNumber) || business == null) {
			log.error("数据不完整保存失败");
			return re;
		}
		User currUser = HttpUtil.getCurrentUser();
		try {
			//营销信息
			LrInfo lrInfo = lrInfoService.selectSingleByProperty("serialNumber", serialNumber);
			LrCustInfo lrCustInfo = lrCustInfoService.selectSingleByProperty("customerNo",lrInfo.getCustomerNo());
			//更新营销信息和客户信息
			lrInfo.setCustomerClass(business);
			lrInfo.setUpdateTime(DateUtil.getNowTimestamp());
			lrInfo.setUpdater(currUser.getUserId());
			lrInfoService.saveOrUpdate(lrInfo);
			lrCustInfo.setCustomerClass(business);
			lrCustInfo.setUpdater(currUser.getUserId());
			lrCustInfo.setUpdateTime(DateUtil.getNowTimestamp());
			lrCustInfoService.saveOrUpdate(lrCustInfo);
			re = returnSuccess();
			re.setRows(lrInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 查询营销任务中的拜访信息，授信信息
	 * 这里被客户信息界面有调用，需要修改
	 * @param serialNumber
	 * @return
	 */
	@RequestMapping(value = "getRetailVisitInfo.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto getRetailVisitInfo(HttpServletRequest request, String serialNumber) {
		ResultDto re = returnFail("获取拜访记录失败");
		if (StringUtil.isEmptyString(serialNumber)) {
			log.error("获取拜访记录失败");
			return re;
		}
		try {
			// 拜访信息
			List<LrVisitRecord> lrVisitRecords = lrVisitRecordService.selectByProperty("serialNumber", serialNumber);
			re = returnSuccess();
			re.setRows(lrVisitRecords);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 查询营销任务中的拜访信息，授信信息
	 *
	 * @param serialNumber
	 * @return
	 */
	@RequestMapping(value = "getRetailInfoList.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto getRetailInfoList(HttpServletRequest request, String serialNumber) {
		ResultDto re = returnFail("获取拜访记录,授信信息失败");
		if (StringUtil.isEmptyString(serialNumber)) {
			log.error("获取拜访记录,授信信息失败");
			return re;
		}
		try {
			// 拜访信息
			List<LrVisitRecord> lrVisitRecords = lrVisitRecordService.selectByProperty("serialNumber", serialNumber);
			//营销信息
			LrInfo lrInfo = lrInfoService.selectSingleByProperty("serialNumber", serialNumber);

			Map<String, Object> retailInfos = new HashMap<>();
			retailInfos.put("lrVisitRecords", lrVisitRecords);
			retailInfos.put("lrInfo", lrInfo);
			re = returnSuccess();
			re.setRows(retailInfos);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 保存拜访记录
	 * @param request
	 * @param lrVisitRecordVO  用于接收html和android上的数据
	 * @return
	 */
	@RequestMapping(value = "saveLrVisitRecord.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto saveLrVisitRecord(HttpServletRequest request, LrVisitRecordVO lrVisitRecordVO) {
		ResultDto re = returnFail("保存拜访记录失败");
		if (lrVisitRecordVO == null) {
			re.setMsg("拜访记录为空，保存失败");
			log.error("拜访记录为空，保存失败");
			return re;
		}
		// 获取到登录的用户编号
		User user = HttpUtil.getCurrentUser(request);
		LrVisitRecord saveLrVisitRecord;
		try {
			if (RgnshUtils.isEmptyInteger(lrVisitRecordVO.getId())) {
				// 如果id为空或者为0，表示这个是新增
				saveLrVisitRecord = new LrVisitRecord();
				saveLrVisitRecord.setCreater(user.getUserId());
			} else {
				// 更新操作
				saveLrVisitRecord = lrVisitRecordService.findByPrimaryKey(lrVisitRecordVO.getId());
			}
			saveLrVisitRecord.setSerialNumber(lrVisitRecordVO.getSerialNumber());
			saveLrVisitRecord.setVisitWay(lrVisitRecordVO.getVisitWay());
			saveLrVisitRecord.setVisitPersonName(URLDecoder.decode(URLDecoder.decode(lrVisitRecordVO.getVisitPersonName(),"UTF-8"),"UTF-8"));
			saveLrVisitRecord.setVisitSituation(URLDecoder.decode(URLDecoder.decode(lrVisitRecordVO.getVisitSituation(),"UTF-8"),"UTF-8"));
			saveLrVisitRecord.setVisitTime(DateUtil.formarYMD(lrVisitRecordVO.getVisitTime()));
			saveLrVisitRecord.setVisitAddr(lrVisitRecordVO.getVisitAddr());
			saveLrVisitRecord.setLongGitude(lrVisitRecordVO.getLongGitude());
			saveLrVisitRecord.setLatitude(lrVisitRecordVO.getLatitude());
			// 设置更新时间更新人
			saveLrVisitRecord.setUpdater(user.getUserId());
			saveLrVisitRecord.setUpdateTime(DateUtil.getNowTimestamp());

			lrVisitRecordService.saveOrUpdate(saveLrVisitRecord);
			re = returnSuccess();
			re.setMsg("保存拜访信息成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除拜访记录
	 *
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "deleteLrVisitRecord.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto deleteLrVisitRecord(HttpServletRequest request, Integer id) {
		ResultDto re = returnFail("删除拜访记录失败");
		if (RgnshUtils.isEmptyInteger(id)) {
			re.setMsg("删除标识不存在，删除失败");
			log.error("删除标识不存在，删除失败");
			return re;
		}
		User user = HttpUtil.getCurrentUser(request);
		try {
			LrVisitRecord lrVisitRecord = lrVisitRecordService.findByPrimaryKey(id);
			if (lrVisitRecord == null) {
				re.setMsg("删除标识不存在，删除失败");
				log.error("删除标识不存在，删除失败");
				return re;
			}
			lrVisitRecord.setUpdater(user.getUserId());
			lrVisitRecord.setUpdateTime(DateUtil.getNowTimestamp());
			lrVisitRecord.setStatus(Constants.DELETE);
			lrVisitRecordService.saveOrUpdate(lrVisitRecord);
			re = returnSuccess();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 跳转到营销影像信息
	 * 营销营销是根据任务走
	 * @return
	 */
	@RequestMapping("retailImage.do")
	public ModelAndView retailImage(String stage){
		ModelAndView mav = new ModelAndView("views/retail/lrtask/retailImage");
		if(StringUtil.isEmptyString(stage)){
			stage = Constants.EDIT;
		}
		mav.addObject("stage", stage);
		return mav;
	}



	/**
	 * 跳转到营销结论界面
	 * @return
	 */
	@RequestMapping("retailResult.do")
	public ModelAndView retailResult(String serNo, String stage, String func){
		ModelAndView mav = new ModelAndView("views/retail/lrtask/retailResult");
		try{
			if(StringUtil.isEmptyString(stage)){
				stage = Constants.EDIT;
			}
			mav.addObject("stage", stage);
			mav.addObject("func", func);
			//营销信息
			LrInfo lrInfo = lrInfoService.selectSingleByProperty("serialNumber", serNo);
			mav.addObject("data", lrInfo);
			String[] optTypes = {SysConstants.BsDicConstant.STD_PRO_RG};
			String dicData = sysDicSV.queryMapListByTypes(optTypes);
			mav.addObject("dicData", dicData);
		}catch (Exception e){
			e.printStackTrace();
		}
		return mav;
	}

	/**
	 * 暂存营销任务
	 * @param request
	 * @param lrInfo
	 * @return
	 */
	@RequestMapping(value = "tempSaveRetailTask.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto tempSaveRetailTask(HttpServletRequest request, LrInfo lrInfo) {
		ResultDto re = returnFail("暂存营销任务失败");
		if (lrInfo == null || StringUtil.isEmptyString(lrInfo.getSerialNumber())) {
			re.setMsg("暂存营销任务失败");
			log.error("暂存营销任务失败,业务流水号为" + lrInfo.getSerialNumber());
			return re;
		}
		// 获取当前登录用户
		User user = HttpUtil.getCurrentUser(request);
		try {
			lrInfoService.synchroLrInfo(lrInfo, user.getUserId());
			re = returnSuccess();
		} catch (BusinessException e) {
			re = returnFail(e.getMessage());
			log.error(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return re;
	}

	/**
	 * 营销结束
	 * @param request
	 * @param lrInfo 前台传过来的营销信息
	 * @return
	 */
	@RequestMapping(value = "stopRetailTask.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto stopRetailTask(HttpServletRequest request, LrInfo lrInfo) {

		ResultDto re = returnFail("营销结束");
		if (lrInfo == null || StringUtil.isEmptyString(lrInfo.getSerialNumber())) {
			re.setMsg("完成本次营销失败");
			log.error("完成本次营销失败,业务流水号为" + lrInfo.getSerialNumber());
			return re;
		}
		// 获取当前登录用户
		User user = HttpUtil.getCurrentUser(request);
		try {
			// 同步前端的客户分类信息
			lrInfoService.synchroLrInfo(lrInfo, user.getUserId());
			// 结束营销任务
			lrInfoService.finishRetailTask(lrInfo.getSerialNumber(), user.getUserId());
			re = returnSuccess();
		} catch (BusinessException e) {
			re = returnFail(e.getMessage());
			log.error(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 跳转到营销进件状态
	 * @param request
	 * @param serialNumber 业务流水号
	 * @param business 客户分类
	 * @return
	 */
	@RequestMapping(value = "retailTaskIn.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto retailTaskIn(HttpServletRequest request, LrInfo postLrInfo,Integer business) {
		ResultDto re = returnFail();
		if (StringUtil.isEmptyString(postLrInfo.getSerialNumber())) {
			log.error("数据不完整保存失败");
			re.setMsg("数据不完整保存失败");
			return re;
		}

		User currUser = HttpUtil.getCurrentUser();
		try {
			LrRecordList lrRecord = lrRecordListService.selectSingleByProperty("serialNumber", postLrInfo.getSerialNumber());
			if(lrRecord != null){
				re = returnFail("当前任务已进件，请勿重复提交！");
				return re;
			}
//			lrInfoService.synchroLrInfo(postLrInfo, currUser.getUserId());
			business = Constants.CustClassify.SUCCESS;//默认设置为成功客户吧
			//营销信息
			LrInfo lrInfo = lrInfoService.selectSingleByProperty("serialNumber", postLrInfo.getSerialNumber());
			LrCustInfo lrCustInfo = lrCustInfoService.selectSingleByProperty("customerNo",lrInfo.getCustomerNo());
			//更新营销信息和客户信息
			lrInfo.setCustomerClass(business);
			lrInfo.setUpdateTime(DateUtil.getNowTimestamp());
			lrInfo.setUpdater(currUser.getUserId());
			lrInfoService.saveOrUpdate(lrInfo);
			lrCustInfo.setCustomerClass(business);
			lrCustInfo.setUpdater(currUser.getUserId());
			lrCustInfo.setUpdateTime(DateUtil.getNowTimestamp());
			lrCustInfoService.saveOrUpdate(lrCustInfo);

			// 结束营销任务
			lrInfoService.submitRetailTask(lrInfo.getSerialNumber(), currUser.getUserId());
			re = returnSuccess();
			re.setRows(lrInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	/**
	 * 提交营销进件中的任务
	 * @author: yuzhejia
	 * @param serialNumber
	 * @return
	 */
	@RequestMapping("submitRetail.json")
	@ResponseBody
	public ResultDto submitRetail(HttpServletRequest request, LrInfo lrInfo){
		ResultDto re = returnFail("提交营销任务失败");
		if (lrInfo == null || StringUtil.isEmptyString(lrInfo.getSerialNumber())) {
			re.setMsg("完成本次营销失败");
			log.error("完成本次营销失败,业务流水号为" + lrInfo.getSerialNumber());
			return re;
		}
		User user = HttpUtil.getCurrentUser(request);
		try {
			/**
			 * 伪代码GGG
			 * 这里判断营销进件后的数据有没有录入 --》 进行各种判断
			 * 没有录入信息直接 提示那部分数据缺失
			 */

			String userNo = user.getUserId();

			//创建贷前  待处理任务 -->跳过原先需要贷前管理岗分配客户经理的流程
			String userName = userSV.queryNameByNos(userNo);

			//判断并修改当前营销客户的管护经理
			lrInfoService.synchroLrInfo(lrInfo, user.getUserId());
			CusBase cusBase = cusBaseSV.queryByCusNo(lrInfo.getCustomerNo());
			String managerUserId = cusBase.getManagerUserId();
			if(!StringUtil.isEmptyString(managerUserId) && !managerUserId.equals(userNo)){
				re.setMsg("当前客户在信贷系统中的管护客户经理编号为："+managerUserId+",请先在信贷系统中改变客户管护权");
				return re;
			}
			cusBase.setManagerUserId(userNo);
			cusBaseSV.save(cusBase);


			lrInfoService.submitRetailToSurvey(lrInfo.getSerialNumber(), userNo, userName);
			re = returnSuccess();

		} catch (BusinessException e) {
			re.setMsg(e.getMessage());
			log.error(e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return re;
	}



	/*********************************营销统计*****************************/
	//查询出客户被营销的记录
	//营销记录表----根据客户经理编号，时间    统计总量，成功量     计算转换率

	/**
	 * 跳转到客户分层分类界面
	 * @return
	 */
	@RequestMapping("custLayered.do")
	public ModelAndView custLayered(){
		ModelAndView mav = new ModelAndView("views/retail/stat/statModel");
		return mav;
	}

	/***
	 * 获取客户分层分类数据
	 * @param dataRange	数据范围 1全量客户  2不包括公共池客户
	 * @param yyyyMM		客户分类变化时间范围
	 * @return
	 */
	@RequestMapping("queryCustLayered.json")
	@ResponseBody
	public ResultDto queryCustLayered(String dataRange, String yyyyMM){
		ResultDto re = returnFail("获取客户分层分类数据失败");
		String startTime = null;
		String endTime = null;
		try {
			if(!StringUtil.isEmptyString(yyyyMM) && yyyyMM.length()>5){
				//String year = yyyyMM.substring(0, 4);
				String month = yyyyMM.substring(5);
				startTime = DateUtil.getFirstDayOfMonth(DateUtil.getYear(new Date()), Integer.parseInt(month));
				endTime = DateUtil.getLastDayOfMonth(DateUtil.getYear(new Date()), Integer.parseInt(month));
			}
			List<LrCustClassDto> data = new ArrayList<>(5);
			List<LrCustClassDto> lrCustClassDtos = lrCustInfoService.queryCustClass(dataRange, startTime, endTime);

			int sum = 0;
			//补充到5条数据，为空的数据默认补零
			if(lrCustClassDtos==null){
//				0无效客户  1潜在客户 2机会客户 3意向客户 4成功客户
				LrCustClassDto dto1 = new LrCustClassDto("无效客户", 0l, 100, "0.00");
				LrCustClassDto dto2 = new LrCustClassDto("潜在客户", 0l, 80, "0.00");
				LrCustClassDto dto3 = new LrCustClassDto("机会客户", 0l, 60, "0.00");
				LrCustClassDto dto4 = new LrCustClassDto("意向客户", 0l, 40, "0.00");
				LrCustClassDto dto5 = new LrCustClassDto("成功客户", 0l, 20, "0.00");
				data.add(dto1);
				data.add(dto2);
				data.add(dto3);
				data.add(dto4);
				data.add(dto5);
			}else{
				for(int i = 0; i < 5; i++){
					boolean flag = false;
					LrCustClassDto clazz = null;
					for(int j =0; j < lrCustClassDtos.size(); j ++){
						LrCustClassDto dto = lrCustClassDtos.get(j);
						if(dto.getCustomerClassCode()!=null && dto.getCustomerClassCode().equals(i)){
							flag = true;
							clazz = dto;
							sum += dto.getNum();
							break;
						}
					}
					String className = null;
					Integer value = null;
					/*Integer clazzCode = null;
					if(!flag){
						clazzCode = i;
					}else{
						clazzCode = clazz.getCustomerClassCode();
					}*/
					switch (i){
						case 0: className = "无效客户";
							value = 100;break;
						case 1: className = "潜在客户";
							value = 80;break;
						case 2: className = "机会客户";
							value = 60;break;
						case 3: className = "意向客户";
							value = 40;break;
						case 4: className = "成功客户";
							value = 20;break;
						default: break;
					}
					if(!flag){
						LrCustClassDto dto = new LrCustClassDto(className, 0l, value);
						data.add(dto);
					}else{
						clazz.setCustomerClassName(className);
						clazz.setValue(value);
						data.add(clazz);
					}
				}
			}
			if(sum!=0) {
				for (int i = 0; i < data.size(); i++) {
					Long num = data.get(i).getNum();
					Double percent = new BigDecimal(num * 100 / sum).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
					data.get(i).setPercent(percent.toString());
				}
			}
			re = returnSuccess();
			re.setRows(data);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return re;
	}

	@RequestMapping("custSubLayered.do")
	public ModelAndView custSubLayered(){
		ModelAndView mav = new ModelAndView("views/retail/stat/custSubLayered");
		return mav;
	}

	@RequestMapping("querySubCustLayered.json")
	@ResponseBody
	public ResultDto querySubCustLayered(int page, int pageSize,String yyyyMM,String orgName){
		ResultDto re = returnFail("获取客户分层分类数据失败");
		String startTime = null;
		String endTime = null;
		try {
			if(!StringUtil.isEmptyString(yyyyMM) && yyyyMM.length()>5){
				String month = yyyyMM.substring(5);
				startTime = DateUtil.getFirstDayOfMonth(DateUtil.getYear(new Date()), Integer.parseInt(month));
				endTime = DateUtil.getLastDayOfMonth(DateUtil.getYear(new Date()), Integer.parseInt(month));
			}
			Pagination<LrCustSubClassDto> pager = lrCustInfoService.querySubCustClass(page,pageSize,orgName, startTime, endTime);
			re = returnSuccess();
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return re;
	}

	@RequestMapping("custSubLayeredDetail.do")
	public ModelAndView custSubLayeredDetail(String organno, String yearMonth){
		String startTime = null;
		String endTime = null;
		ModelAndView mav = new ModelAndView("views/retail/stat/custSubLayeredDetail");
		try {
			if(!StringUtil.isEmptyString(yearMonth) && yearMonth.length()>5){
				String month = yearMonth.substring(5);
				startTime = DateUtil.getFirstDayOfMonth(DateUtil.getYear(new Date()), Integer.parseInt(month));
				endTime = DateUtil.getLastDayOfMonth(DateUtil.getYear(new Date()), Integer.parseInt(month));
			}
			List<LrCustSubClassDto> lrCustSubClassDtos = lrCustInfoService.queryManageInSubCustClass(organno, startTime, endTime);
			mav.addObject("lrCustSubClassDtos", lrCustSubClassDtos);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mav;
	}

	/**
	 * 跳转到客户覆盖率界面
	 * @return
	 */
	@RequestMapping("custCoverage.do")
	public ModelAndView custCoverage(){
		ModelAndView mav = new ModelAndView("views/retail/stat/statCoverage");
		return mav;
	}

	/**
	 * 获取营销客户覆盖率
	 * @param page
	 * @param pageSize
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	@RequestMapping("getCustConverage.json")
	@ResponseBody
	public ResultDto getCustConverage(String userName, int page, int pageSize,String startTime, String endTime){
		ResultDto re = returnFail("获取营销客户覆盖率数据失败");
		try {
			Pagination<LrCoverageDto> pager = lrCustInfoService
					.queryCoverage(userName, startTime, endTime, page, pageSize);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());
			re.setMsg("获取营销客户覆盖率数据成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 跳转到客户转化率界面
	 * @return
	 */
	@RequestMapping("custConversion.do")
	public ModelAndView custConversion(){
		ModelAndView mav = new ModelAndView("views/retail/stat/statConversion");
		return mav;
	}

	/**
	 * 获取客户转化率数据
	 * @param page
	 * @param pageSize
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	@RequestMapping("getCustConversion.json")
	@ResponseBody
	public ResultDto getCustConversion(String userName, int page, int pageSize,String startTime, String endTime){
		ResultDto re = returnFail("获取营销客户转化率数据失败");
		try {
			Pagination<LrCoverageDto> pager = lrCustInfoService
					.queryConversion(userName, startTime, endTime, page, pageSize);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());
			re.setMsg("获取营销客户转化率数据成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 跳转到区域营销情况界面
	 * @return
	 */
	@RequestMapping("areaStat.do")
	public ModelAndView areaStat(){
		return new ModelAndView("views/retail/stat/areaStat");
	}


	/**
	 * 获取 区域营销情况
	 * @param page
	 * @param pageSize
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	@RequestMapping("getAreaStat.json")
	@ResponseBody
	public ResultDto getAreaStatList(HttpServletRequest request, int page, int pageSize,
									 String startTime, String endTime) {
		ResultDto re = returnFail("获取区域营销情况数据失败");
		User user = HttpUtil.getCurrentUser(request);
		try {
			//角色编号
			String roleNo = user.getRoleNo();
			String userNo = null;
			//当前用户是营销客户经理时，则查询范围在自己管户的客户下
			if(Constants.ROLE.LR_USER.equals(roleNo)){
				userNo = user.getUserId();
			}
			Pagination<LrAreaStatDto> pager =
					lrCustInfoService.queryAreaStat(userNo, startTime, endTime, page, pageSize);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());
			re.setMsg("获取区域营销数据成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 根据客户编号，获取客户营销记录
	 * @author: xyh
	 * @date:   2020年6月20日 下午2:58:33
	 * @param customerNO
	 * @return
	 */
	public ResultDto findCustLrRecord(String customerNO){
		ResultDto re = returnFail("查询客户营销记录");
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}

		return re;
	}
}
