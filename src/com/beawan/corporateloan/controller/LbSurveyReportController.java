package com.beawan.corporateloan.controller;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.analysis.finansis.utils.CodeUtil;
import com.beawan.base.entity.User;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.common.config.AppConfig;
import com.beawan.corporateloan.entity.LeTask;
import com.beawan.corporateloan.service.LbSurveyReportService;
import com.beawan.corporateloan.service.LeTaskService;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.platform.util.DateUtil;
import com.platform.util.FreeMarkerWordUtil;
import com.platform.util.HttpUtil;
import com.platform.util.StringUtil;

/**
 * 贷前调查报告 Controller
 * @author zxh
 * @date 2020/8/6 10:07
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/survey/report/" })
public class LbSurveyReportController {
    private static final Logger log = Logger.getLogger(LbSurveyReportController.class);

    @Resource
    private LbSurveyReportService surveyReportService;
	@Resource
	private ITaskSV taskSV;
	@Resource 
	private LeTaskService leTaskService;

    /**
     * 预览 贷前调查报告
     * @param serNo 任务流水号
     * @param lce 是否在审查审批阶段查看标志  --》界面大小不同
     * @return
     */
    @RequestMapping("viewLbReport.do")
    public ModelAndView viewLbReport(String serNo, @RequestParam(value = "lce", required = false) String lce){
        ModelAndView mav = new ModelAndView();
        try {
            Map<String, Object> reportData = surveyReportService.getReportData(serNo);
            mav.addObject("lce", lce);
            mav.setViewName((String)reportData.get("viewName"));
            mav.addObject("data", reportData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }

    /**
     * 在审查审批阶段
     * 预览 贷前调查报告
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("viewLbReportInLCE.do")
    public ModelAndView viewLbReportInLCE(String serNo){
        ModelAndView mav = new ModelAndView();
        try {
            Map<String, Object> reportData = surveyReportService.getReportData(serNo);
            mav.setViewName((String)reportData.get("examineName"));
            mav.addObject("data", reportData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }

    /**
     * 下载 贷前调查报告
     * @param request
     * @param serNo 任务流水号
     * @return
     */
    @RequestMapping("downloadLbReport.json")
    public ResponseEntity<byte[]> downloadLcReport(HttpServletRequest request, String serNo) {
        ResponseEntity<byte[]> result = null;
        //模板数据
        Map<String, Object> dataMap = new HashMap<>();
        User user = HttpUtil.getCurrentUser(request);
        if(StringUtil.isEmptyString(serNo)){
            log.error("参数信息为空，请排查原因");
            return result;
        }
        try {
        	Task lbTask = taskSV.getTaskBySerNo(serNo);//贷前任务表LB_TASK，业务流水号
        	String customerName = lbTask.getCustomerName();//公司名
            Map<String, Object> reportData = surveyReportService.getReportData(serNo);//这个map里面放了3个页面名字
            dataMap.put("data", reportData);
            String templateName = (String)reportData.get("templateName");
            //String fileName = customerName + "贷前调查报告.doc";
            String fileName = customerName.substring(0,1)+"公司贷前调查报告111.doc";
            //利用时间生成文件不重复路径
            String destPath = AppConfig.Cfg.REPORT_PATH
                    + DateUtil.formatDate(new Date(), "yyyy-MM" );
            File file = new File(destPath , fileName);
            if(!file.getParentFile().exists())
                file.getParentFile().mkdirs();
            //填充的数据，文件模板，文件路径，文件名字
            FreeMarkerWordUtil.createWord(dataMap, templateName, destPath, file.getName());
            HttpHeaders headers = new HttpHeaders();
            headers.setContentDispositionFormData("attachment", CodeUtil.parseGBK(fileName));
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            result = new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file), headers, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            log.error("下载贷前调查报告文件异常：", e);
        }
        return result;
    }
    
    /**
     * 下载 贷前调查报告
     * @param request
     * @param leTaskId 审查id
     * @return
     */
    @RequestMapping("downloadLbReportfromle.json")
    public ResponseEntity<byte[]> downloadLcReportfromle(HttpServletRequest request, Integer leTaskId) {
    	LeTask task = leTaskService.findByPrimaryKey(leTaskId);
        ResponseEntity<byte[]> result = null;
        //模板数据
        Map<String, Object> dataMap = new HashMap<>();
        User user = HttpUtil.getCurrentUser(request);
        if(StringUtil.isEmptyString(task.getSerialNumber())){
            log.error("参数信息为空，请排查原因");
            return result;
        }
        try {
        	Task lbTask = taskSV.getTaskBySerNo(task.getSerialNumber());
        	String customerName = lbTask.getCustomerName();
            Map<String, Object> reportData = surveyReportService.getReportData(task.getSerialNumber());
            dataMap.put("data", reportData);
            String templateName = (String)reportData.get("templateName");
            String fileName = customerName + "贷前调查报告.doc";
            String destPath = AppConfig.Cfg.REPORT_PATH
                    + DateUtil.formatDate(new Date(), "yyyy-MM" );
            File file = new File(destPath , fileName);
            if(!file.getParentFile().exists())
                file.getParentFile().mkdirs();
            FreeMarkerWordUtil.createWord(dataMap, templateName, destPath, file.getName());
            HttpHeaders headers = new HttpHeaders();
            headers.setContentDispositionFormData("attachment", CodeUtil.parseGBK(fileName));
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            result = new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file), headers, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            log.error("下载贷前调查报告文件异常：", e);
        }
        return result;
    }

}
