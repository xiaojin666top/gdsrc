package com.beawan.corporateloan.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.common.config.AppConfig;
import com.beawan.core.BaseController;
import com.beawan.corporateloan.dto.FnUrlDto;
import com.beawan.customer.bean.FinriskResult;
import com.beawan.customer.service.FinriskResultService;


/**
 * 财务分析控制器
 *
 * @author  yuzhejia
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/fn/" })
public class FinanceController extends BaseController {
	private static final Logger log = Logger.getLogger(FinanceController.class);

	@Resource
	private FinriskResultService finriskResultService;


	/**
	 * 跳转到当前贷前任务的财务分析界面
	 * 财务分析的内容全部来自财务分析系统
	 * @param request
	 * @param serialNumber	业务流水号
	 * @return
	 */
	@RequestMapping("financesysIndex.do")
	public ModelAndView pendingTaskList(HttpServletRequest request, String serialNumber) {

		ModelAndView mav = new ModelAndView("views/finance/finance_index");
		FinriskResult finriskResult = finriskResultService.getLocalFinrisk(serialNumber);
		if(finriskResult==null) {
			log.error("该信贷业务不存在本地财务分析结果！暂无额度推荐");
		}
		String resultId = finriskResult.getResultTaskId();
		mav.addObject("resultId", resultId);

		List<FnUrlDto> urls = new ArrayList<>();
		urls.add(new FnUrlDto("异常预警" , AppConfig.Cfg.ROOT_FINRISK + AppConfig.Cfg.V_CLOUD + AppConfig.Cfg.ERR_SUMMARY));
		urls.add(new FnUrlDto("资产分析",AppConfig.Cfg.ROOT_FINRISK + AppConfig.Cfg.V_CLOUD +  AppConfig.Cfg.BALANCE_ASSETS));
		urls.add(new FnUrlDto("负债分析", AppConfig.Cfg.ROOT_FINRISK + AppConfig.Cfg.V_CLOUD + AppConfig.Cfg.BALANCE_DEBT));
		urls.add(new FnUrlDto("所有者权益分析", AppConfig.Cfg.ROOT_FINRISK + AppConfig.Cfg.V_CLOUD + AppConfig.Cfg.BALANCE_OWNER));
		urls.add(new FnUrlDto("利润分析", AppConfig.Cfg.ROOT_FINRISK + AppConfig.Cfg.V_CLOUD + AppConfig.Cfg.INCOME_PANDECT));
		urls.add(new FnUrlDto("现金流分析", AppConfig.Cfg.ROOT_FINRISK + AppConfig.Cfg.V_CLOUD + AppConfig.Cfg.CASH_FLOW_PANDECT));
		urls.add(new FnUrlDto("比率分析", AppConfig.Cfg.ROOT_FINRISK + AppConfig.Cfg.V_CLOUD + AppConfig.Cfg.RATIO));
		mav.addObject("urls", urls);

		return mav;
	}



}
