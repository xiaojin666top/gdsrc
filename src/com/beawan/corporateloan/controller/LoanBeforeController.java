package com.beawan.corporateloan.controller;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.base.dto.UserDto;
import com.beawan.base.entity.User;
import com.beawan.base.service.IUserSV;
import com.beawan.common.Constants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.Pagination;
import com.beawan.core.ResultDto;
import com.beawan.corporateloan.dto.LmTaskInfoDto;
import com.beawan.corporateloan.service.LmProcessService;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.platform.util.HttpUtil;
import com.platform.util.StringUtil;


/**
 * 贷前调查  贷前任务控制器
 *
 * @author  yuzhejia
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/survey/task/" })
public class LoanBeforeController extends BaseController {
	private static final Logger log = Logger.getLogger(LoanBeforeController.class);

	@Resource
	private LmProcessService lmProcessService;
	@Resource
	private IUserSV userSV;
	@Resource
	private ITaskSV taskSV;

	/**
	 * 跳转到待分配调查任务列表
	 * @param request
	 * @param response
	 * @param state
	 * @return
	 */
	@RequestMapping("pendingLbTaskList.do")
	public ModelAndView pendingTaskList(HttpServletRequest request, HttpServletResponse response,
										Integer state) {
		ModelAndView mav=  new ModelAndView("views/survey/task/pendingLbTask");
		return mav;
	}

	/**
	 * 查看营销过来的所有待分配任务
	 * 能进入到这个界面的角色  能看到所有
	 * 通过权限控制
	 * @param page
	 * @param rows
	 * @param customerName	过滤条件：客户名字
	 * @param customerNo	过滤条件：客户编号
	 * @return
	 */
	@RequestMapping("getLbPendingTaskList.json")
	@ResponseBody
	public ResultDto getLbPendingTaskList( @RequestParam(value="page",defaultValue="1")int page,
										   @RequestParam(value="rows",defaultValue="10")int rows,
										   String customerName, String customerNo) {
		ResultDto re = returnFail();
		try {
			Map<String, String> query = new HashMap<>();
			query.put("customerName", customerName);
			query.put("customerNo", customerNo);
			Pagination<LmTaskInfoDto> pager = lmProcessService.queryLmProcessPaging(query,null, null,
					"=", Constants.LoanProcessType.SURVEY_PENDING, page, rows);
			re = returnSuccess();
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 跳转到
	 * 分配具体调查经理界面
	 * @return
	 */
	@RequestMapping("selectUser.do")
	public ModelAndView selectUser(String serialNumber) {
		ModelAndView mav =  new ModelAndView("views/survey/task/query_user");
		mav.addObject("serialNumber", serialNumber);
		return mav;
	}

	/**
	 * 获取贷前调查经理的分页列表
	 * @param page
	 * @param rows
	 * @param userName
	 * @param userNo
	 * @return
	 */
	@RequestMapping("queryLbUserList.json")
	@ResponseBody
	public ResultDto queryLbUserList( @RequestParam(value="page",defaultValue="1")int page,
									  @RequestParam(value="rows",defaultValue="10")int rows,
									  String userName, String userNo) {
		ResultDto re = returnFail();
		try {
			Pagination<UserDto> pager = userSV.queryUserPaging(Constants.ROLE.LB_USER, userName, userNo, page, rows);
			re = returnSuccess();
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 营销进件任务由指定的调查经理去执行
	 * @param serialNumber
	 * @param userNo
	 * @return
	 */
	@RequestMapping("appointLbUser.json")
	@ResponseBody
	public ResultDto appointLbUser(HttpServletRequest request, String serialNumber, String userNo) {
		ResultDto re = returnFail("分配任务客户经理失败！");
		if(StringUtil.isEmptyString(serialNumber) || StringUtil.isEmptyString(userNo)){
			return returnFail("任务流水号或客户经理编号异常，请重试！");
		}
		User user = HttpUtil.getCurrentUser(request);
		try {
			String userName = userSV.queryNameByNos(userNo);
			lmProcessService.updateAppointLbUser(serialNumber, userNo, userName, user.getUserId());
			re = returnSuccess();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 显示贷前影像上传界面
	 * @return
	 */
	@RequestMapping("showSurveyIs.do")
	public ModelAndView imageSys(HttpServletRequest request, String serialNumber,String customerNo){
		ModelAndView mav = new ModelAndView("views/survey/isSurvey/uploadSurveyImage");
		mav.addObject("serialNumber",serialNumber);
		mav.addObject("customerNo",customerNo);
		return mav;
	}
	

	/**
	 * 跳转到
	 * 指定手动选择审查人员界面
	 * @return
	 */
	@RequestMapping("selectLeUser.do")
	public ModelAndView selectLeUser(Long taskId,String customerName, String custNo) {
		ModelAndView mav =  new ModelAndView("views/survey/task/query_le_user");
		mav.addObject("taskId", taskId);
		try{
			customerName = URLDecoder.decode(URLDecoder.decode(customerName, "utf-8"), "utf-8");
		}catch(Exception e){
			e.printStackTrace();
		}
		mav.addObject("customerName", customerName);
		mav.addObject("custNo", custNo);
		return mav;
	}
	
	/**
	 * 获取
	 * @param page
	 * @param rows
	 * @param userName
	 * @param userNo
	 * @return
	 */
	@RequestMapping("queryLeUserList.json")
	@ResponseBody
	public ResultDto queryLeUserList( @RequestParam(value="page",defaultValue="1")int page,
									  @RequestParam(value="rows",defaultValue="10")int rows,
									  String userName, String userNo) {
		ResultDto re = returnFail();
		try {
			Pagination<UserDto> pager = userSV.queryUserPaging(Constants.ROLE.LE_USER, userName, userNo, page, rows);
			re = returnSuccess();
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 保存贷前指定的审查人员
	 * @param taskId
	 * @param userNo
	 * @return
	 */
	@RequestMapping("appointLeUser.json")
	@ResponseBody
	public ResultDto appointLeUser(Long taskId, String userNo) {
		ResultDto re = returnFail();
		try {
			Task task = taskSV.getTaskById(taskId);
			task.setLeUserNo(userNo);
			taskSV.saveOrUpdate(task);
			re = returnSuccess();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	
}
