package com.beawan.corporateloan.controller;

import java.io.File;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.analysis.finansis.utils.CodeUtil;
import com.beawan.base.entity.SysDic;
import com.beawan.base.entity.User;
import com.beawan.base.service.ISysDicSV;
import com.beawan.common.BusinessException;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.common.config.AppConfig;
import com.beawan.core.BaseController;
import com.beawan.core.Pagination;
import com.beawan.core.ResultDto;
import com.beawan.corporateloan.dto.LeExamineContentDto;
import com.beawan.corporateloan.dto.LeTaskDto;
import com.beawan.corporateloan.entity.LcTask;
import com.beawan.corporateloan.entity.LeExamineContent;
import com.beawan.corporateloan.entity.LeExaminePoint;
import com.beawan.corporateloan.entity.LeTask;
import com.beawan.corporateloan.entity.LmProcess;
import com.beawan.corporateloan.service.LcTaskService;
import com.beawan.corporateloan.service.LeExamineContentService;
import com.beawan.corporateloan.service.LeExaminePointService;
import com.beawan.corporateloan.service.LeTaskService;
import com.beawan.corporateloan.service.LmProcessService;
import com.beawan.customer.bean.FinriskResult;
import com.beawan.customer.dto.QuotaFullDto;
import com.beawan.customer.service.FinriskResultService;
import com.beawan.imageSystem.entity.ISImageInfo;
import com.beawan.imageSystem.service.ISImageInfoService;
import com.beawan.library.bean.TreeData;
import com.beawan.library.service.IIndustrySV;
import com.beawan.survey.custInfo.bean.CompFinancingBank;
import com.beawan.survey.custInfo.service.ICompFinancingSV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.platform.util.DateUtil;
import com.platform.util.FreeMarkerWordUtil;
import com.platform.util.HttpUtil;
import com.platform.util.RgnshUtils;
import com.platform.util.StringUtil;

/**
 * 贷中  贷款审查控制器
 * 思路：leTask为审查记录表，如果本笔是一个审查人，这个表就是一条，两个审查人就是两条
 * leExaminePoint：审查细项表，用于配置一笔贷款需要审查的内容
 * 		如果出现不同贷款配置化要求，添加关联表---根据关联表获取审查内容
 * leExamineContent:审查细项记录表，每一次被打回后提交，会重新根据细项，新建对应的记录表
 * 		第一次提交：所有细项生成一次，默认设置通过
 *      被打回后提交：所有细项复制一次
 *      追回后提交：直接沿用上次的审查细项记录(没有被审查，不用统计上次的审查结果)
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/examine/" })
public class LoanExamineController extends BaseController {
	private static final Logger log = Logger.getLogger(LoanExamineController.class);
	@Resource
	private ITaskSV taskSV;
	@Resource
	private ISysDicSV sysDicSV;
	@Resource
	private LeExaminePointService leExaminePointService;
	@Resource
	private LmProcessService lmProcessService;
	@Resource
	private LeTaskService leTaskService;
	@Resource
	private LcTaskService lcTaskService;
	@Resource
	private LeExamineContentService leExamineContentService;
	@Resource
	private FinriskResultService finriskResultService;
	@Resource
	private ISImageInfoService iSImageInfoService;
	@Resource
	private ICompFinancingSV compFinancingSV;
	@Resource
	private IIndustrySV industrySV;

	//=============================审查要点=============
	@RequestMapping("examinePointList.do")
	public ModelAndView examinePointList(){
		ModelAndView mav = new ModelAndView("views/examine/examinePointList");
		try {
			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.EXAMINE_TYP};
			String dicData = sysDicSV.queryMapListByTypes(optTypes);
			mav.addObject("dicData", dicData);
		}catch (Exception e){
			log.error("跳转审查要点项页面异常", e);
			e.printStackTrace();
		}
		return mav;
	}

	//获取审查要点项
	@RequestMapping("queryExaminePointList.json")
	@ResponseBody
	public ResultDto queryExaminePointList(HttpServletRequest request, HttpServletResponse response,
										   @RequestParam(value="page",defaultValue="1")int page,
										   @RequestParam(value="rows",defaultValue="10")int rows){
		ResultDto resultDto = returnFail();
		try{
			Pagination<LeExaminePoint> pager = leExaminePointService.queryPageList(page,rows);
			resultDto = returnSuccess();
			resultDto.setRows(pager.getItems());
			resultDto.setTotal(pager.getRowsCount());
		}catch (Exception e){
			log.error("获取审查要点项异常", e);
			resultDto = returnFail("获取审查要点项异常");
			e.printStackTrace();
		}
		return resultDto;
	}


	/**
	 * 跳转到 新增贷款审查内容页
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("addExaminePoint.do")
	public ModelAndView addExaminePoint(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("views/examine/addExaminePoint");
		try {
			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.EXAMINE_TYP};
			String dicData = sysDicSV.queryMapListByTypes(optTypes);
			mav.addObject("dicData", dicData);
		}catch (Exception e){
			log.error("跳转添加审查页面异常", e);
			e.printStackTrace();
		}
		return mav;
	}

	/**
	 * 跳转到 修改贷款审查内容页
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("editExaminePoint.do")
	public ModelAndView editExaminePoint(HttpServletRequest request, HttpServletResponse response,
										Integer id) {
		ModelAndView mav = new ModelAndView("views/examine/editExaminePoint");
		try {
			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.EXAMINE_TYP};
			String dicData = sysDicSV.queryMapListByTypes(optTypes);
			mav.addObject("dicData", dicData);
			LeExaminePoint leExaminePoint = leExaminePointService.findByPrimaryKey(id);
			if(leExaminePoint == null){
				leExaminePoint = new LeExaminePoint();
			}
			mav.addObject("leExaminePoint",leExaminePoint);
		}catch (Exception e){
			log.error("跳转添加审查页面异常", e);
			e.printStackTrace();
		}
		return mav;
	}



	@RequestMapping("saveExaminePoint.json")
	@ResponseBody
	public ResultDto saveExaminePoint(HttpServletRequest request, HttpServletResponse response,LeExaminePoint exmainePoint){
		ResultDto resultDto = returnFail();
		if(exmainePoint == null){
			log.error("审查要点为空，保存失败");
			resultDto = returnFail("审查要点为空,保存失败");
			return resultDto;
		}
		User curUser = HttpUtil.getCurrentUser();
		try{
			LeExaminePoint saveEntity = null;
			if(RgnshUtils.isEmptyInteger(exmainePoint.getId())){
				//保存
				saveEntity = new LeExaminePoint();
				saveEntity.setCreater(curUser.getUserId());
			}else{
				saveEntity = leExaminePointService.findByPrimaryKey(exmainePoint.getId());
			}
			saveEntity.setExamineType(exmainePoint.getExamineType());
			saveEntity.setExamineName(URLDecoder.decode(URLDecoder.decode(exmainePoint.getExamineName(),"UTF-8"),"UTF-8"));
			saveEntity.setExamineMemo(URLDecoder.decode(URLDecoder.decode(exmainePoint.getExamineMemo(),"UTF-8"),"UTF-8"));
			saveEntity.setUpdater(curUser.getUserId());
			saveEntity.setUpdateTime(DateUtil.getNowTimestamp());
			leExaminePointService.saveOrUpdate(saveEntity);
			resultDto = returnSuccess();
		}catch (Exception e){
			log.error("保存审查要点项异常", e);
			resultDto = returnFail("保存审查要点项异常");
			e.printStackTrace();
		}
		return resultDto;
	}

	@RequestMapping("deleteExaminePoint.json")
	@ResponseBody
	public ResultDto deleteExaminePoint(HttpServletRequest request, HttpServletResponse response,Integer id){
		ResultDto resultDto = returnFail();
		User curUser = HttpUtil.getCurrentUser();
		try{
			LeExaminePoint deleteEntity = leExaminePointService.findByPrimaryKey(id);
			if(deleteEntity == null){
				resultDto.setMsg("删除审查要点异常，请重试");
				log.error("查询审查要点数据为空，查询id为：" + id);
				return resultDto;
			}
			deleteEntity.setUpdater(curUser.getUserId());
			deleteEntity.setUpdateTime(DateUtil.getNowTimestamp());
			deleteEntity.setStatus(Constants.DELETE);
			leExaminePointService.saveOrUpdate(deleteEntity);
			resultDto = returnSuccess();
		}catch (Exception e){
			log.error("删除审查要点项异常", e);
			resultDto = returnFail("删除审查要点项异常");
			e.printStackTrace();
		}
		return resultDto;
	}



	//============================审查任务=========

	/**
	 * 跳转到待审查任务列表页面
	 * @param request
	 * @return
	 */
	@RequestMapping("pendingTaskList.do")
	public ModelAndView pendingTaskList(HttpServletRequest request){
		ModelAndView mav = new ModelAndView("views/examine/pendingTaskList");
		return mav;
	}

	/**
	 * 获取待处理任务列表
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping("getLePedingTaskList.json")
	@ResponseBody
	public ResultDto getLePedingTaskList(@RequestParam(value="page",defaultValue="1")int page,
										 @RequestParam(value="rows",defaultValue="10")int rows){
		ResultDto resultDto = returnFail();
		User curUser = HttpUtil.getCurrentUser();
		String userNo = null;
		String orgNo = null;
		try{
			String userDA = curUser.getDataAccess();
			if(SysConstants.UserDataAccess.PERSONAL.equals(userDA)) {
				userNo = curUser.getUserId();
			}else if(SysConstants.UserDataAccess.ORG.equals(userDA)) {
				orgNo = curUser.getAccOrgNo();
			}

			Pagination<LeTaskDto> pager = lmProcessService.queryLeTaskPaging(Constants.LoanProcessType.EXAMINE_PENDING, userNo, orgNo,page,rows);
			resultDto = returnSuccess();
			resultDto.setRows(pager.getItems());
			resultDto.setTotal(pager.getRowsCount());
		}catch (Exception e){
			e.printStackTrace();
			resultDto.setMsg("获取待审查任务失败");
		}
		return resultDto;
	}
	
	
	/**
	 * 跳转到待审查任务列表页面
	 * @param request
	 * @return
	 */
	@RequestMapping("distributionTaskList.do")
	public ModelAndView distributionTaskList(HttpServletRequest request){
		ModelAndView mav = new ModelAndView("views/examine/distributionTaskList");
		return mav;
	}
	
	
	@RequestMapping("getLeDistributionTaskList.json")
	@ResponseBody
	public ResultDto getLeDistributionTaskList(@RequestParam(value="page",defaultValue="1")int page,
										 @RequestParam(value="rows",defaultValue="10")int rows){
		ResultDto resultDto = returnFail();
		User curUser = HttpUtil.getCurrentUser();

		String userNo = null;
		String orgNo = null;
		try{
			String userDA = curUser.getDataAccess();
			if(SysConstants.UserDataAccess.PERSONAL.equals(userDA)) {
				userNo = curUser.getUserId();
			}else if(SysConstants.UserDataAccess.ORG.equals(userDA)) {
				orgNo = curUser.getAccOrgNo();
			}

			Pagination<LeTaskDto> pager = 
					lmProcessService.queryLeTaskPaging(Constants.LoanProcessType.EXAMINE_DISTRIBUTION, 
							userNo, orgNo,page,rows);
			
			resultDto = returnSuccess();
			resultDto.setRows(pager.getItems());
			resultDto.setTotal(pager.getRowsCount());
		}catch (Exception e){
			e.printStackTrace();
			resultDto.setMsg("获取待审查任务失败");
		}
		return resultDto;
	}
	
	@RequestMapping("distrbutionUser.do")
	public ModelAndView selectUser(Integer leTaskId) {
		ModelAndView mav =  new ModelAndView("views/examine/distribution_user");
		mav.addObject("leTaskId", leTaskId);
		return mav;
	}
	
	
	@RequestMapping("distrbutionLeTask.json")
	@ResponseBody
	public ResultDto distrbutionLeTask(Integer leTaskId,String userNo) {
		ResultDto re = new ResultDto();
		if(RgnshUtils.isEmptyInteger(leTaskId) || StringUtil.isEmptyString(userNo)){
			log.error("分配信息不完整");
			return returnFail("分配信息不完整");
		}
		try{
			LeTask task = leTaskService.findByPrimaryKey(leTaskId);
			if(task==null){
				log.error("当前任务不存在，请重试，任务id为：" + leTaskId);
				return returnFail("当前任务不存在，请重试!");
			}
			User user = HttpUtil.getCurrentUser();
			lmProcessService.updateLeTask(task.getSerialNumber(), userNo, user.getUserId());
			re = returnSuccess();
			re.setMsg("信贷审查分配任务成功");
		}catch (BusinessException e){
			re.setMsg(e.getMessage());
			log.error(e.getMessage());
		}catch (Exception e){
			re.setMsg("系统异常");
			log.error("系统异常");
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 跳转到审查界面
	 * @param leTaskId
	 * @return
	 */
	@RequestMapping("showExaminePage.do")
	public ModelAndView showExaminePage(Integer leTaskId){
		ModelAndView mav = new ModelAndView("views/examine/examinePage");
		LeTask task = leTaskService.findByPrimaryKey(leTaskId);
		String serialNumber = task.getSerialNumber();
		Task lbTask = taskSV.getTaskBySerNo(serialNumber);
		List<CompFinancingBank> dataGrid = compFinancingSV.findCompFinaBankByTIdAndNo(lbTask.getId(), lbTask.getCustomerNo(), "M");
		ArrayList creditballist = new ArrayList();
		Double stockNumber = 0.0;
		for(CompFinancingBank finance : dataGrid){
			stockNumber += finance.getCreditBal();
		}
		mav.addObject("lbTask",lbTask);
		mav.addObject("stockNumber",stockNumber);

		FinriskResult finriskResult = finriskResultService
				.selectSingleByProperty("dgSerialNumber", serialNumber);
		if(finriskResult!=null){
			mav.addObject("finriskResult", finriskResult);
		}
		if(task == null){
			log.error("当前id异常，数据库不存在相应审查任务");
		}
		mav.addObject("task",task);
		return mav;
	}
	
	

	/**
	 * 跳转到审查通过界面
	 * @param request
	 * @return
	 */
	@RequestMapping("lePassTaskList.do")
	public ModelAndView lePassTaskList(HttpServletRequest request){
		ModelAndView mav = new ModelAndView("views/examine/passLeTaskList");
		return mav;
	}

	/**
	 * 获取审查通过列表
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping("getLePassTaskList.json")
	@ResponseBody
	public ResultDto getLePassTaskList(@RequestParam(value="page",defaultValue="1")int page,
									   @RequestParam(value="rows",defaultValue="10")int rows){
		ResultDto resultDto = returnFail();
		User curUser = HttpUtil.getCurrentUser();
		String userNo = null;
		String orgNo = null;
		try{
			String userDA = curUser.getDataAccess();
			if(SysConstants.UserDataAccess.PERSONAL.equals(userDA)) {
				userNo = curUser.getUserId();
			}else if(SysConstants.UserDataAccess.ORG.equals(userDA)) {
				orgNo = curUser.getAccOrgNo();
			}
			Pagination<LeTaskDto> pager = lmProcessService.queryLePassTaskPaging("",userNo,orgNo,page,rows);
			resultDto = returnSuccess();
			resultDto.setRows(pager.getItems());
			resultDto.setTotal(pager.getRowsCount());
		}catch (Exception e){
			e.printStackTrace();
			resultDto.setMsg("获取审查通过列表失败");
		}
		return resultDto;
	}

	/**
	 * 跳转到开始合规审查界面
	 * @param request
	 * @param leTaskId
	 * @return
	 */
	@RequestMapping("startExamine.do")
	public ModelAndView startCompliance(HttpServletRequest request, Integer leTaskId){
		ModelAndView mav = new ModelAndView("views/examine/examineMain");
		LeTask task = leTaskService.findByPrimaryKey(leTaskId);
		mav.addObject("task", task);
		return mav;
	}
	/**
	 * 审查  操作界面
	 * @param request
	 * @param leTaskId
	 * @return
	 */
	@RequestMapping("examineData.do")
	public ModelAndView examineData(HttpServletRequest request, Integer leTaskId)throws Exception{
		ModelAndView mav = new ModelAndView("views/examine/examineData");
		User User = HttpUtil.getCurrentUser();
		//获取所有的审查详情表
		List<LeExaminePoint> leExaminPointList = leExaminePointService.getAllLeExaminPoint();
		//获取审查任务
		LeTask task = leTaskService.findByPrimaryKey(leTaskId);
		
		//获取审查细项信息
		List<LeExamineContent> leExamineContents = leExamineContentService.queryByCondition(leTaskId,task.getReviewCount());
		
		if(CollectionUtils.isEmpty(leExamineContents)){
			for(LeExaminePoint leExaminPoint : leExaminPointList){
				LeExamineContent leExamineContent = new LeExamineContent();
				leExamineContent.setCreater(User.getUserId());
				leExamineContent.setCreateTime(DateUtil.getNowTimestamp());
				leExamineContent.setLeTaskId(leTaskId);
				leExamineContent.setExamineType(leExaminPoint.getExamineType());
				leExamineContent.setExamineName(leExaminPoint.getExamineName());
				leExamineContent.setExaminePointId(leExaminPoint.getId());
				leExamineContent.setIsPass(Constants.LE_REVIEW_CONTENT_STATE.IS_PASS);
				leExamineContent.setState(Constants.LE_REVIEW_CONTENT_STATE.SOLVE);
				leExamineContent.setExamineCount(task.getReviewCount());
				leExamineContent.setUpdater(User.getUserId());
				leExamineContent.setUpdateTime(DateUtil.getNowTimestamp());
				leExamineContentService.saveOrUpdate(leExamineContent);
			}
			leExamineContents = leExamineContentService.queryByCondition(leTaskId,task.getReviewCount());
		}
		
		
		
		//要使用这个查询，需要连dic的表
		//Map<String, List<LeExamineContent>> collect = leExamineContents.stream().collect(Collectors.groupingBy(LeExamineContent::getExamineType));
		//审查细项字典表数据
		List<SysDic> dicData = sysDicSV.queryByTypesOrderByENName(SysConstants.BsDicConstant.EXAMINE_TYP);
		List<LeExamineContentDto> leExamineContentDtos = new ArrayList<>();
		int j = 0;
		for(int i = 0 ;i<dicData.size();i++){
			LeExamineContentDto leExamineContentDto = new LeExamineContentDto();
			leExamineContentDto.setExamineTypeCN(dicData.get(i).getCnName());
			List<LeExamineContent> child = new ArrayList<>();
			for(;j<leExamineContents.size();j++){
				if(dicData.get(i).getEnName().equals(leExamineContents.get(j).getExamineType())){
					child.add(leExamineContents.get(j));
				}else{
					//都排序过，所以一旦不相等，直接到下一个外层循环
					break;
				}
			}
			leExamineContentDto.setChildExamine(child);
			leExamineContentDtos.add(leExamineContentDto);
		}
		mav.addObject("task",task);
		mav.addObject("leExamineContentDtos",leExamineContentDtos);
		return mav;
	}

	/**
	 * 打回贷前调查任务到调查中状态
	 * @param id
	 * @return
	 */
	@RequestMapping("examineRepluse.json")
	@ResponseBody
	public ResultDto examineRepluse(HttpServletRequest request, Integer id){
		ResultDto re = returnFail();
		if(RgnshUtils.isEmptyInteger(id)){
			re.setMsg("任务信息异常，请重试");
			return re;
		}
		User currentUser = HttpUtil.getCurrentUser(request);
		try{
			LeTask task = leTaskService.findByPrimaryKey(id);
			if(task==null){
				re.setMsg("当前任务不存在，请重试");
				log.error("当前任务不存在，请重试，任务id为：" + id);
				return re;
			}
			lmProcessService.repluseExamine(task.getSerialNumber(), currentUser.getUserId());
			re = returnSuccess();
			re.setMsg("信贷审查打回任务成功");
		}catch (Exception e){
			e.printStackTrace();
		}
		return re;
	}


	@RequestMapping("showExamineIs.do")
	public ModelAndView showExamineIs(HttpServletRequest request,String serialNumber){
		ModelAndView mav = new ModelAndView("views/examine/imageList");
		try{
			LeTask task = leTaskService.queryBySerialNumber(serialNumber);
			mav.addObject("serialNumber",task.getSerialNumber());
			mav.addObject("customerName",task.getCustomerName());
		}catch (Exception e){
			e.printStackTrace();
		}
		return mav;
	}

	/**
	 * 跳转到某目录下所有图片
	 * @param request
	 * @param serialNumber
	 * @param dirPath
	 * @return
	 */
	@RequestMapping("IsExamineImage.do")
	public ModelAndView IsComplianceImage(HttpServletRequest request,String serialNumber,String dirPath){
		ModelAndView mav = new ModelAndView("views/examine/isExamineImage");
		try{
			LeTask task = leTaskService.queryBySerialNumber(serialNumber);
			List<ISImageInfo> imageInfoList = iSImageInfoService.queryByCondition(task.getSerialNumber(),task.getCustomerNo(),Constants.LoanImageType.SURVEY,dirPath);
			mav.addObject("imageInfoList",imageInfoList);
		}catch (Exception e){
			e.printStackTrace();
		}
		return mav;
	}

	/**
	 * 保存合规审查任务基本信息
	 * @param taskId
	 * @param submissionTime  yyyy-MM-dd格式
	 * @param reviewTime
	 * @param industry
	 * @param guarantor
	 * @return
	 */
	@RequestMapping("saveExamineInfo.json")
	@ResponseBody
	public ResultDto saveExamineInfo(Integer taskId, String submissionTime, String industry,
										String summary, String occurType, String guarantor,Double stockNumber,
										Double thisApply,Double loanNumber){
		ResultDto re = returnFail();
		if(taskId==null){
			re.setMsg("当前审查任务异常，请重试");
			return re;
		}
		try{
			LeTask task = leTaskService.findByPrimaryKey(taskId);
			task.setSubmissionTime(DateUtil.parseYmdTimestamp(submissionTime));
//			task.setReviewTime(DateUtil.parseYmdTimestamp(reviewTime));
//			task.setCreditCondition(creditCondition);
			
			summary = URLDecoder.decode(summary, "UTF-8");
			occurType = URLDecoder.decode(occurType, "UTF-8");
			task.setStockNumber(stockNumber);
			task.setSummary(summary);
			task.setOccurType(occurType);
			task.setThisApply(thisApply);
			task.setLoanNumber(loanNumber);
			
			leTaskService.saveOrUpdate(task);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存审查任务信息成功");
		}catch (Exception e){
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 获取审查结论
	 * @param request
	 * @param taskId
	 * @return
	 */
	@RequestMapping("queryExamineResult.json")
	@ResponseBody
	public ResultDto queryExamineResult(HttpServletRequest request, Integer taskId)throws Exception{
		ResultDto resultDto = returnFail();
		try{
			//获取审查任务
			//LeTask task = leTaskService.findByPrimaryKey(taskId);
            //获取审查不同的所有细项
			List<LeExamineContent> leExamineContents = leExamineContentService.queryExamineContResult(taskId);
			
			resultDto = returnSuccess();
			resultDto.setRows(leExamineContents);
		}catch (Exception e){
			resultDto.setMsg("获取审查结果细项失败");
			log.error("获取审查结果细项失败",e);
		}
		return resultDto;
	}

	/**
	 * 保存审查结果：
	 * @param taskId
	 * @param descrData
	 * @return
	 */
	@RequestMapping("saveExamineContent.json")
	@ResponseBody
	public ResultDto saveExamineContent(Integer taskId,String descrData){
		ResultDto resultDto = returnFail();
		try{
			descrData = URLDecoder.decode(descrData, "UTF-8");
			leExamineContentService.updateLeExamineContent(taskId,descrData);
			resultDto = returnSuccess();
		}catch (Exception e){
			resultDto.setMsg("保存审查结果细项失败");
			log.error("保存审查结果细项失败",e);
		}
		return resultDto;
	}

	/**
	 * 下载 贷前调查报告
	 * @param request
	 * @return
	 */
	@RequestMapping("downloadLeReport.do")
	public ResponseEntity<byte[]> downloadLeReport(HttpServletRequest request, String serNo) {
		ResponseEntity<byte[]> result = null;
		//模板数据
		Map<String, Object> dataMap = new HashMap<>();
		//User user = HttpUtil.getCurrentUser(request);
		if(StringUtil.isEmptyString(serNo)){
			log.error("参数信息为空，请排查原因");
			return result;
		}
		try {
			//获取审查意见表的模板
			String templateName = "loan_statistics/le/reviewReport.htm";
			//获取贷款主表信息
			LmProcess lmProcess = lmProcessService.selectSingleByProperty("serialNumber", serNo);
			dataMap = leTaskService.getExamineReportData(serNo);
			String fileName = serNo + "_review_report.xls";
			String destPath = AppConfig.Cfg.REPORT_PATH
					+ DateUtil.formatDate(lmProcess.getCreateTime(), "yyyy-MM" )+"/" + serNo;
			File file = new File(destPath , fileName);
			if(!file.getParentFile().exists())
				file.getParentFile().mkdirs();
			FreeMarkerWordUtil.createWord(dataMap, templateName, destPath, fileName);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentDispositionFormData("attachment", CodeUtil.parseGBK("审查意见表.xls"));
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			result = new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file), headers, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("下载审查意见表文件异常：", e);
		}
		return result;
	}

	/**
	 * 获取推荐额度：
	 * @param taskId
	 * @return
	 */
	@RequestMapping("getRecommendQuota.do")
	public ModelAndView getRecommendQuota(Integer taskId){
		ModelAndView mav = new ModelAndView("views/examine/examineQuota");
		if(taskId==null){
			return mav;
		}
		try{
			LeTask leTask = leTaskService.findByPrimaryKey(taskId);
			String serialNumber = leTask.getSerialNumber();
			QuotaFullDto commendQuota = finriskResultService.getCommendQuota(serialNumber);
			mav.addObject("commendQuota", commendQuota);
		}catch (Exception e){
			log.error("获取对公推荐额度失败",e);
		}
		return mav;
	}
	
	/**
	 * 跳转到
	 * 指定手动选择审查人员界面
	 * @return
	 */
	@RequestMapping("choseLeUser.do")
	public ModelAndView choseLeUser(Integer leTaskId,String customerName, String custNo) {
		ModelAndView mav =  new ModelAndView("views/examine/chose_le_user");
		mav.addObject("leTaskId", leTaskId);
		try{
			customerName = URLDecoder.decode(URLDecoder.decode(customerName, "utf-8"), "utf-8");
		}catch(Exception e){
			e.printStackTrace();
		}
		mav.addObject("customerName", customerName);
		mav.addObject("custNo", custNo);
		return mav;
	}
	
	/**
	 * 保存贷前指定的审查人员
	 * @param taskId
	 * @param userNo
	 * @return
	 */
	@RequestMapping("choseLeUser.json")
	@ResponseBody
	public ResultDto choseLeUser(Integer leTaskId, String userNo) {
		ResultDto re = returnFail();
		User curUser = HttpUtil.getCurrentUser();
		try {
			LeTask leTask = leTaskService.findByPrimaryKey(leTaskId);
			leTask.setUserNo(userNo);
			leTask.setUpdater(curUser.getUserId());
			leTask.setUpdateTime(DateUtil.getNowTimestamp());
			leTaskService.saveOrUpdate(leTask);
			re = returnSuccess();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 提交调查任务
	 * 
	 * @param request
	 * @param id
	 *            贷前调查任务 id
	 * @return
	 */
	@RequestMapping("transLeUser.json")
	@ResponseBody
	public ResultDto transLeUser(HttpServletRequest request, Integer id) {
		ResultDto re = returnFail("提交贷前调查报告失败");
		if (id == null || id == 0) {
			re.setMsg("任务参数异常，请重试");
			return re;
		}
		User currentUser = HttpUtil.getCurrentUser(request);
		try {
			LeTask leTask = leTaskService.findByPrimaryKey(id);
			String leUserNo = leTask.getUserNo();
			String serialNumber = leTask.getSerialNumber();
			
			Task task = taskSV.getTaskBySerNo(serialNumber);
			task.setLeUserNo(leUserNo);
			task.setUpdater(currentUser.getUserId());
			task.setUpdateTime(DateUtil.getNowTimestamp());
			taskSV.saveOrUpdate(task);
			
			LcTask lcTask = lcTaskService.queryBySerialNumber(serialNumber);
			lcTask.setLeUserNo(leUserNo);
			lcTask.setUpdater(currentUser.getUserId());
			lcTask.setUpdateTime(DateUtil.getNowTimestamp());
			lcTaskService.saveOrUpdate(lcTask);
			
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("移交贷前调查报告成功");
		}  catch (Exception e) {
			re.setMsg("系统异常，请联系管理员！");
			log.error("移交调查任务异常！", e);
			e.printStackTrace();
		}
		return re;
	}

	@RequestMapping("submitLeTask.json")
	@ResponseBody
	public ResultDto submitLeTask(HttpServletRequest request, Integer id){
		ResultDto re = returnFail();
		if(RgnshUtils.isEmptyInteger(id)){
			re.setMsg("任务信息异常，请重试");
			return re;
		}
		User currentUser = HttpUtil.getCurrentUser(request);
		try{
			LeTask leTask = leTaskService.findByPrimaryKey(id);
			if(leTask==null){
				re.setMsg("当前任务不存在，请重试");
				log.error("当前任务不存在，请重试，任务id为：" + id);
				return re;
			}
			System.out.println("currentUser.getUserId()="+currentUser.getUserId());
			//提交到信贷审查
			LmProcess process = lmProcessService.selectSingleByProperty("serialNumber", leTask.getSerialNumber());
			process.setStage(Constants.LoanProcessType.EXAMINE_PASS);
			process.setUpdater(currentUser.getUserId());
			process.setUpdateTime(DateUtil.getNowTimestamp());
			lmProcessService.saveOrUpdate(process);
			re = returnSuccess();
			re.setMsg("贷款审查任务提交");
		}catch (Exception e){
			e.printStackTrace();
		}
		return re;
	}


}
