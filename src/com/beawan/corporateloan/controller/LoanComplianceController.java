package com.beawan.corporateloan.controller;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.analysis.finansis.utils.CodeUtil;
import com.beawan.base.entity.User;
import com.beawan.base.service.IUserSV;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.common.config.AppConfig;
import com.beawan.core.BaseController;
import com.beawan.core.Pagination;
import com.beawan.core.ResultDto;
import com.beawan.corporateloan.dto.LcManagerRiskStatDto;
import com.beawan.corporateloan.dto.LcPointRiskDto;
import com.beawan.corporateloan.dto.LcReportDto;
import com.beawan.corporateloan.dto.LcRiskStatDto;
import com.beawan.corporateloan.dto.LcTaskDto;
import com.beawan.corporateloan.dto.LcTaskRiskDto;
import com.beawan.corporateloan.dto.LcTaskStatDto;
import com.beawan.corporateloan.entity.LcDoc;
import com.beawan.corporateloan.entity.LcReviewContent;
import com.beawan.corporateloan.entity.LcReviewPoint;
import com.beawan.corporateloan.entity.LcRiskDb;
import com.beawan.corporateloan.entity.LcTask;
import com.beawan.corporateloan.entity.LmProcess;
import com.beawan.corporateloan.service.LcDocService;
import com.beawan.corporateloan.service.LcReviewContentService;
import com.beawan.corporateloan.service.LcReviewPointService;
import com.beawan.corporateloan.service.LcRiskDbService;
import com.beawan.corporateloan.service.LcTaskService;
import com.beawan.corporateloan.service.LmProcessService;
import com.beawan.customer.dto.QuotaFullDto;
import com.beawan.customer.service.FinriskResultService;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.imageSystem.entity.ISImageInfo;
import com.beawan.imageSystem.service.ISImageInfoService;
import com.platform.util.DateUtil;
import com.platform.util.FreeMarkerWordUtil;
import com.platform.util.HttpUtil;
import com.platform.util.RgnshUtils;
import com.platform.util.StringUtil;

/**
 * 贷中  合规审查控制器
 * 
 * @author
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/compliance/" })
public class LoanComplianceController extends BaseController {
	private static final Logger log = Logger.getLogger(LoanComplianceController.class);

	@Resource
	private ICusBaseSV cusBaseSV;
	@Resource
	private IUserSV userSV;
	@Resource
	private LcReviewPointService lcReviewPointService;
	@Resource
	private LcRiskDbService lcRiskDbService;
	@Resource
	private LcTaskService lcTaskService;
	@Resource
	private LcReviewContentService lcReviewContentService;
	@Resource
	private LmProcessService lmProcessService;
	@Resource
	private FinriskResultService finriskResultService;
	@Resource
	private ISImageInfoService iSImageInfoService;
	@Resource
	private LcDocService lcDocService;



	/************************* 合规审查文档  *************************************/

	/**
	 * 跳转到 权限与职责
	 * 
	 * @param request
	 * @param type
	 * @return
	 */
	@RequestMapping("remitDoc.do")
	public ModelAndView remitDoc(HttpServletRequest request, String type) {
		ModelAndView mav = new ModelAndView("views/compliance/doc/remitDoc");
		LcDoc lcDoc = lcDocService.selectSingleByProperty("type", Constants.LC_DOC_TYPE.REMIT);
		mav.addObject("lcDoc", lcDoc);
		return mav;
	}

	/**
	 * 跳转到工作流程
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("workDoc.do")
	public ModelAndView workDoc(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("views/compliance/doc/workDoc");
		LcDoc lcDoc = lcDocService.selectSingleByProperty("type", Constants.LC_DOC_TYPE.WORK);
		mav.addObject("lcDoc", lcDoc);
		return mav;
	}

	/**
	 * 跳转到 合规审查要点
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("reviewPointDoc.do")
	public ModelAndView reviewPointDoc(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("views/compliance/doc/reviewPointDoc");
		LcDoc lcDoc = lcDocService.selectSingleByProperty("type", Constants.LC_DOC_TYPE.REVIEW_POINT);
		mav.addObject("lcDoc", lcDoc);
		return mav;
	}

	/**
	 * 跳转到操作手册界面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("operate.do")
	public ModelAndView operateDoc(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("views/compliance/doc/operateDoc");
		return mav;
	}


	/**
	 * 跳转到 修改合规文档界面
	 * @param request
	 * @param docId
	 * @return
	 */
	@RequestMapping("editLcDoc.do")
	public ModelAndView editLcDoc(HttpServletRequest request, Integer docId) {
		ModelAndView mav = new ModelAndView("views/compliance/doc/editLcDoc");
		LcDoc doc = lcDocService.findByPrimaryKey(docId);
		String docName = "";
		if(Constants.LC_DOC_TYPE.REMIT.equals(doc.getType())){
			docName = "职责与权限";
		}else if(Constants.LC_DOC_TYPE.WORK.equals(doc.getType())){
			docName = "工作流程";
		}else{
			docName = "合规审查要点";
		}
		mav.addObject("docName", docName);
		mav.addObject("doc", doc);
		return mav;
	}


	/**
	 * 保存文档内容
	 * @param docId
	 * @param docInfo
	 * @param noHtml
	 * @return
	 */
	@RequestMapping("saveDocInfo.json")
	@ResponseBody
	public ResultDto saveDocInfo(Integer docId, String docInfo, String noHtml){
		ResultDto re = returnFail();
		if(StringUtil.isEmptyString(docInfo)){
			re.setMsg("请输入编辑内容");
			return re;
		}
		try{
			docInfo = URLDecoder.decode(URLDecoder.decode(docInfo, "utf-8"), "utf-8");
			String type = lcDocService.saveDoc(docId, docInfo, noHtml);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setRows(type);
		}catch (Exception e){
			e.printStackTrace();
		}
		return re;
	}


	/************************ 审查文档结束  ************************************************/

	/**
	 * 跳转到 分行业 审查内容列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("riskDetail.do")
	public String riskDetail(HttpServletRequest request, HttpServletResponse response) {
		return "views/compliance/riskDetail";
	}

	/**
	 * 跳转到 分行业 审查内容列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("reviewPoint.do")
	public ModelAndView reviewPoint(HttpServletRequest request, HttpServletResponse response, String indu) {
		ModelAndView mav = new ModelAndView("views/compliance/reviewPoint");
		mav.addObject("indu", indu);
		return mav;
	}

	/**
	 * 跳转到 新增审查内容页
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("addReviewPoint.do")
	public ModelAndView addReviewPoint(HttpServletRequest request, HttpServletResponse response,
									   Integer id,String indu) {
		ModelAndView mav = new ModelAndView("views/compliance/addReviewPoint");
		if(id!=null && !"".equals(id) && id!=0){
			LcReviewPoint reviewPoint = lcReviewPointService.findByPrimaryKey(id);
			if(reviewPoint==null){
				log.error("修改核查项内容数据获取异常！");
			}
			mav.addObject("id", reviewPoint.getId());
			mav.addObject("reviewName", reviewPoint.getReviewName());
			mav.addObject("reviewContent", reviewPoint.getReviewContent());

		}

		String induName = "";
		if(Constants.LC_RISK_INDU.COMM.equals(indu)){
			induName = "公共部分";
		}else if(Constants.LC_RISK_INDU.MANUF.equals(indu)){
			induName = "制造业";
		}else if(Constants.LC_RISK_INDU.BUSINESS.equals(indu)){
			induName = "贸易业";
		}else if(Constants.LC_RISK_INDU.BUILD.equals(indu)){
			induName = "建筑业";
		}else{
			induName = "其他类";
		}
		mav.addObject("indu", indu);
		mav.addObject("induName", induName);
		return mav;
	}

	/**
	 * 跳转到 查看审查项细项
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("viewReviewPoint.do")
	public ModelAndView viewReviewPoint(HttpServletRequest request, HttpServletResponse response,
									   Integer id) {
		ModelAndView mav = new ModelAndView("views/compliance/viewReviewPoint");
		if(id!=null && !"".equals(id) && id!=0){
			LcReviewPoint reviewPoint = lcReviewPointService.findByPrimaryKey(id);
			if(reviewPoint==null){
				log.error("获取核查项内容数据获取异常！");
			}
			mav.addObject("id", reviewPoint.getId());
			mav.addObject("reviewName", reviewPoint.getReviewName());
			mav.addObject("reviewContent", reviewPoint.getReviewContent());

			List<LcRiskDb> riskList = lcRiskDbService.selectByProperty("pointId", reviewPoint.getId());
			mav.addObject("riskList", riskList);
		}
		return mav;
	}

	/**
	 * 获取审查详细风险点 列表
	 * @param pointId 审查项id
	 * @return
	 */
	@RequestMapping("getRiskList.json")
	@ResponseBody
	public ResultDto getRiskList(Integer pointId){
		ResultDto re = returnFail("获取审查详细风险点失败");
		if(pointId==null || "".equals(pointId)){
			re.setMsg("传输参数为空，请重试");
			return  re;
		}
		try {
			List<LcRiskDb> riskList = lcRiskDbService.selectByProperty("pointId", pointId);
			re.setRows(riskList);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("审查详细风险点成功");
		}catch (Exception e){
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 保存或修改  存在问题项
	 * @return
	 */
	@RequestMapping("saveRiskDb.json")
	@ResponseBody
	public ResultDto saveRiskDb(HttpServletRequest request, Integer id, Integer pointId, String riskInfo){
		ResultDto re = returnFail("保存问题项失败");
		if(pointId==null || "".equals(pointId)){
			re.setMsg("审查内容不存在，请重试");
			return  re;
		}
		if(riskInfo==null || "".equals(riskInfo.trim())){
			re.setMsg("问题项内容为空，请重新输入问题项");
			return re;
		}
		LcRiskDb riskDb;
		try {
			User user = HttpUtil.getCurrentUser(request);
			if(id!=null && id!=0){
				riskDb = lcRiskDbService.findByPrimaryKey(id);
			}else{
				riskDb = new LcRiskDb();
				riskDb.setCreater(user.getUserId());
			}
			riskDb.setPointId(pointId);
			riskDb.setRiskInfo(URLDecoder.decode(URLDecoder.decode(riskInfo,"UTF-8")));
			riskDb.setUpdater(user.getUserId());
			riskDb.setUpdateTime(DateUtil.getNowTimestamp());
			lcRiskDbService.saveOrUpdate(riskDb);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存问题项成功");
		}catch (Exception e){
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 删除合规审查的风险点项
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("removeRisk.json")
	@ResponseBody
	public ResultDto removeRisk(HttpServletRequest request, Integer id){
		ResultDto re = returnFail("删除风险点失败");
		if(id==null || "".equals(id)){
			re.setMsg("传输参数异常，请重试");
			return  re;
		}
		try {
			User user = HttpUtil.getCurrentUser(request);
			LcRiskDb risk = lcRiskDbService.findByPrimaryKey(id);
			if(risk==null){
				re.setMsg("传输参数异常，请重试");
				log.error("通过id删除合规审查风险点时，查询到数据为空，异常id为："+id);
				return re;
			}
			risk.setUpdateTime(DateUtil.getNowTimestamp());
			risk.setUpdater(user.getUserId());
			risk.setStatus(Constants.DELETE);
			lcRiskDbService.saveOrUpdate(risk);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除风险点成功");
		}catch (Exception e){
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 保存合规审查要点
	 * @return
	 */
	@RequestMapping(value = "saveReviewPoint.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto saveReviewPoint(LcReviewPoint reviewPoint) {
		ResultDto re = returnFail("保存失败");
		if(reviewPoint==null || reviewPoint.getReviewName()==null || "".equals(reviewPoint.getReviewName())){
			re.setMsg("传输数据异常！请重试");
			return re;
		}
		try {
			Integer id = reviewPoint.getId();
			if(id==null || "".equals(id) || id==0){
				LcReviewPoint key = lcReviewPointService.selectSingleByProperty("reviewName", reviewPoint.getReviewName());
				if(key!=null){
					re.setMsg("当前核查内容项已存在！请添加其他核查内容");
					return re;
				}
			}
			reviewPoint.setReviewName(URLDecoder.decode(URLDecoder.decode(reviewPoint.getReviewName(), "UTF-8")));
			reviewPoint.setReviewContent(URLDecoder.decode(URLDecoder.decode(reviewPoint.getReviewContent(), "UTF-8")));
			lcReviewPointService.saveOrUpdate(reviewPoint);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}



	/** 删除审查内容 **/
	@RequestMapping("removeReviewPoint.json")
	@ResponseBody
	public ResultDto removeReviewPoint(Integer id) {
		ResultDto re = returnFail("获取审查内容列表失败");
		if(id==null || "".equals(id)){
			re.setMsg("参数传输为空，请重试");
			return re;
		}
		try {
			LcReviewPoint revicePoint = lcReviewPointService.findByPrimaryKey(id);
			if(revicePoint==null){
				re.setMsg("删除数据异常！请重试");
				log.error("删除合规审查内容数据失败！通过主键无法获取数据！");
				return re;
			}
			revicePoint.setStatus(Integer.parseInt(Constants.STATE_DELETE));
			lcReviewPointService.saveOrUpdate(revicePoint);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/** 获取审查内容列表 **/
	@RequestMapping("getReviewPoint.json")
	@ResponseBody
	public ResultDto getReviewPoint(String indu) {
		ResultDto re = returnFail("获取审查内容列表失败");
		if(StringUtil.isEmptyString(indu)){
			log.error("行业分类异常，请检查参数");
			re.setMsg("行业分类异常，请检查参数");
			return re;
		}
		try {
			List<LcReviewPoint> list = lcReviewPointService
					.select("status="+Constants.STATE_NORMAL + " AND indu='" + indu + "'");
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setRows(list);
			re.setMsg("获取审查内容列表成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/***********************    合规审查任务    开始      *********************/

	/**
	 * 跳转到   待处理合规审查界面
	 * @param request
	 * @return
	 */
	@RequestMapping("pendingLcTask.do")
	public ModelAndView pendingLcTask(HttpServletRequest request){
		ModelAndView mav = new ModelAndView("views/compliance/pendingLcTask");
		return mav;
	}

	/**
	 * 跳转到   已提交合规审查界面
	 * @param request
	 * @return
	 */
	@RequestMapping("finishLcTask.do")
	public ModelAndView finishLcTask(HttpServletRequest request){
		ModelAndView mav = new ModelAndView("views/compliance/finishLcTask");
		return mav;
	}

	/**
	 * 打回贷前调查任务到调查中状态
	 * @param id
	 * @return
	 */
	@RequestMapping("repluse.json")
	@ResponseBody
	public ResultDto repluse(HttpServletRequest request, Integer id){
		ResultDto re = returnFail();
		if(id==null || id==0){
			re.setMsg("任务信息异常，请重试");
			return re;
		}
		User currentUser = HttpUtil.getCurrentUser(request);
		try{
			LcTask task = lcTaskService.findByPrimaryKey(id);
			if(task==null){
				re.setMsg("当前任务不存在，请重试");
				log.error("当前任务不存在，请重试，任务id为：" + id);
				return re;
			}
			lmProcessService.repluseCompliance(task.getSerialNumber(), currentUser.getUserId());

			re = returnSuccess();
			re.setMsg("合规部打回任务成功");
		}catch (Exception e){
			e.printStackTrace();
		}
		return re;
	}
	
	@RequestMapping("submitLcTask.json")
	@ResponseBody
	public ResultDto submitLcTask(HttpServletRequest request, Integer id){
		ResultDto re = returnFail();
		if(RgnshUtils.isEmptyInteger(id)){
			re.setMsg("任务信息异常，请重试");
			return re;
		}
		User currentUser = HttpUtil.getCurrentUser(request);
		try{
			LcTask task = lcTaskService.findByPrimaryKey(id);
			if(task==null){
				re.setMsg("当前任务不存在，请重试");
				log.error("当前任务不存在，请重试，任务id为：" + id);
				return re;
			}
			System.out.println("currentUser.getUserId()="+currentUser.getUserId());
			//提交到信贷审查
			lmProcessService.submitToExamine(task.getSerialNumber(),currentUser.getUserId());
			re = returnSuccess();
			re.setMsg("合规审查任务提交");
		}catch (Exception e){
			e.printStackTrace();
		}
		return re;
	}




	/**
	 * 合规审查材料 汇总界面
	 * @param request
	 * @param lcTaskId 审查任务表id
	 * @return
	 */
	@RequestMapping("showCompliance.do")
	public ModelAndView showCompliance(HttpServletRequest request, Integer lcTaskId){
		ModelAndView mav = new ModelAndView("views/compliance/complianceInfo");
		mav.addObject("taskId", lcTaskId);
		LcTask task = lcTaskService.findByPrimaryKey(lcTaskId);
		if(task==null){
			log.error("当前id异常，数据库不存在相应合规审查任务");
		}
		mav.addObject("task", task);
		return mav;
	}

	/**
	 * 保存合规审查任务基本信息
	 * @param taskId
	 * @param submissionTime  yyyy-MM-dd格式
	 * @param reviewTime
	 * @param industry
	 * @param guarantor
	 * @return
	 */
	@RequestMapping("saveComplianceInfo.json")
	@ResponseBody
	public ResultDto saveComplianceInfo(Integer taskId, String submissionTime, String reviewTime, String industry,
										String guarantor, String memo){
		ResultDto re = returnFail();
		if(taskId==null){
			re.setMsg("当前审查任务异常，请重试");
			return re;
		}
		try{

			industry = URLDecoder.decode(industry, "UTF-8");
			guarantor = URLDecoder.decode(guarantor, "UTF-8");
			if(!StringUtil.isEmptyString(memo)){
				memo = URLDecoder.decode(memo, "UTF-8");
			}
			
			lcReviewContentService.updateContent(taskId, industry);
			LcTask task = lcTaskService.findByPrimaryKey(taskId);
			task.setSubmissionTime(DateUtil.parseYmdTimestamp(submissionTime));
			task.setReviewTime(DateUtil.parseYmdTimestamp(reviewTime));
			task.setIndustry(industry);
			task.setGuarantor(guarantor);
			task.setMemo(memo);
			lcTaskService.saveOrUpdate(task);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存审查任务信息成功");
		}catch (Exception e){
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 检查当前是否选择行业
	 * 且 如果选择行业是否和之前的选择的行业是否相同
	 * @param taskId
	 * @param indu
	 * @return
	 */
	@RequestMapping("checkIndu.json")
	@ResponseBody
	public ResultDto checkIndu(Integer taskId, String indu){
		ResultDto re =returnFail();
//		if(StringUtil.isEmptyString(indu)){
//			re.setMsg("请选择行业后进行审查");
//			return re;
//		}
		try{
			indu = URLDecoder.decode(indu, "utf-8");
			LcTask task = lcTaskService.findByPrimaryKey(taskId);
			Map<String, Object> params = new HashMap<>();
			params.put("lcCount", task.getLcCount());
			params.put("lcTaskId", taskId);
			List<LcReviewContent> histList = lcReviewContentService.selectByProperty(params);
			if(!CollectionUtils.isEmpty(histList) && !task.getIndustry().equals(indu)){
				re.setMsg("选择的行业与之前审查内容不一致，会覆盖上一次的审查内容，确定使用该行业进行审查吗？");
				return re;
//				re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			}
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);

		}catch (Exception e){
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 开始审查前的初始化
	 * 未打回则默认全部审查通过
	 * 对于打回的  则复用上一次审查结果
	 * @param taskId
	 * @param indu
	 * @return
	 */
	@RequestMapping("initLcContent.json")
	@ResponseBody
	public ResultDto initLcContent(Integer taskId, String indu){
		ResultDto re =returnFail();
		try{
			indu = URLDecoder.decode(indu, "utf-8");
			lcReviewContentService.updateContent(taskId, indu);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		}catch (Exception e){
			e.printStackTrace();
		}
		return re;
	}


	/**
	 * 跳转到开始合规审查界面
	 * @param request
	 * @param lcTaskId
	 * @return
	 */
	@RequestMapping("startCompliance.do")
	public ModelAndView startCompliance(HttpServletRequest request, Integer lcTaskId){
		ModelAndView mav = new ModelAndView("views/compliance/complianceMain");
		LcTask task = lcTaskService.findByPrimaryKey(lcTaskId);
		mav.addObject("task", task);
		return mav;
	}

	/**
	 * 显示审查的影像----影像列表
	 * @return
	 */
	@RequestMapping("showComplianceIsList.do")
	public ModelAndView showComplianceIsList(HttpServletRequest request,String serialNumber){
		ModelAndView mav = new ModelAndView("views/compliance/isCompliance/imageList");
		try{
			LcTask task = lcTaskService.queryBySerialNumber(serialNumber);
			
			mav.addObject("customerNo",task.getCustomerNo());
			mav.addObject("serialNumber",task.getSerialNumber());
			mav.addObject("customerName",task.getCustomerName());
		}catch (Exception e){
			e.printStackTrace();
		}
		return mav;
	}

	/**
	 * 跳转到某目录下所有图片
	 * @param request
	 * @param serialNumber
	 * @return
	 */
	@RequestMapping("IsComplianceImage.do")
	public ModelAndView IsComplianceImage(HttpServletRequest request,String serialNumber,String dirPath){
		ModelAndView mav = new ModelAndView("views/compliance/isCompliance/isComplianceImage");
		try{
			LcTask task = lcTaskService.queryBySerialNumber(serialNumber);
			List<ISImageInfo> imageInfoList = iSImageInfoService.queryByCondition(task.getSerialNumber(),task.getCustomerNo(),Constants.LoanImageType.SURVEY,dirPath);
			mav.addObject("imageInfoList",imageInfoList);
		}catch (Exception e){
			e.printStackTrace();
		}
		return mav;
	}


	/**
	 * 合规审查  操作界面
	 * @param request
	 * @param lcTaskId
	 * @return
	 */
	@RequestMapping("complianceData.do")
	public ModelAndView complianceData(HttpServletRequest request, Integer lcTaskId){
		ModelAndView mav = new ModelAndView("views/compliance/complianceData");
		LcTask task = lcTaskService.findByPrimaryKey(lcTaskId);
		//获取分行业审查要点
		String industry = task.getIndustry();
		mav.addObject("task", task);
		//根据审查内容分类 获取数据列表
		List<LcPointRiskDto> pointRiskList = lcReviewPointService.getPointRiskList(industry);
		mav.addObject("pointRiskList", pointRiskList);

		Map<String, Object> params  = new HashMap<>();
		params.put("lcTaskId", task.getId());
		//params.put("lcCount", task.getLcCount());
		params.put("state", Constants.NORMAL);
		List<LcReviewContent> contents = lcReviewContentService.selectByProperty(params);
		mav.addObject("contents", contents);
		return mav;
	}

    /**
     * 合规审查  结束界面
     * @param request
     * @param lcTaskId
     * @return
     */
    @RequestMapping("complianceResult.do")
    public ModelAndView complianceResult(HttpServletRequest request, Integer lcTaskId){
        ModelAndView mav = new ModelAndView("views/compliance/complianceData");
        //根本审查内容分类 获取数据列表
//        List<LcPointRiskDto> pointRiskList = lcReviewPointService.getPointRiskList();
//        mav.addObject("pointRiskList", pointRiskList);
        return mav;
    }

    /**
     * 保存合规审查结果
     * @return
     */
    @RequestMapping("submitCompliance.json")
    @ResponseBody
    public ResultDto submitCompliance(Integer taskId, String descrData){
    	
        ResultDto re = returnFail();
        if(taskId==null){
            re.setMsg("审查信息数据异常，请重试");
            return re;
        }
        try{
            descrData = URLDecoder.decode(descrData, "UTF-8");
            lcReviewContentService.saveOrUpdateContent(taskId, descrData);
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("保存审查信息成功");
        }catch (Exception e){
            e.printStackTrace();
        }
        return re;
    }



    /**
	 * 获取  合规审查 待处理任务  分页结果
	 * @return
	 */
	@RequestMapping("getLcPendingTaskList.json")
	@ResponseBody
	public ResultDto getLcInitTaskList( @RequestParam(value="page",defaultValue="1")int page,
										@RequestParam(value="rows",defaultValue="10")int rows,
										String custName, String userName, String startTime, String endTime) {
		ResultDto re = returnFail();
		User curUser = HttpUtil.getCurrentUser();
		String userNo = null;
		String orgNo = null;
		try {
			String userDA = curUser.getDataAccess();
			if(SysConstants.UserDataAccess.PERSONAL.equals(userDA)) {
				userNo = curUser.getUserId();
			}else if(SysConstants.UserDataAccess.ORG.equals(userDA)) {
				orgNo = curUser.getAccOrgNo();
			}
			Pagination<LcTaskDto> pager =
					lmProcessService.queryLcTaskPaging(Constants.LoanProcessType.COMPLIANCE_PENDING,
							userNo,orgNo,custName, userName, startTime, endTime, page, rows);
			re = returnSuccess();
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 获取 合规审查 已提交任务  分页结果
	 * @return
	 */
	@RequestMapping("getLcFinishTaskList.json")
	@ResponseBody
	public ResultDto getLcFinishTaskList( @RequestParam(value="page",defaultValue="1")int page,
										@RequestParam(value="rows",defaultValue="10")int rows,
										  String custName, String userName, String startTime, String endTime) {
		ResultDto re = returnFail();
		User curUser = HttpUtil.getCurrentUser();
		String userNo = null;
		String orgNo = null;
		try {
			String userDA = curUser.getDataAccess();
			if(SysConstants.UserDataAccess.PERSONAL.equals(userDA)) {
				userNo = curUser.getUserId();
			}else if(SysConstants.UserDataAccess.ORG.equals(userDA)) {
				orgNo = curUser.getAccOrgNo();
			}
			Pagination<LcTaskDto> pager =
					lmProcessService.queryLcTaskFinishPaging(Constants.LoanProcessType.COMPLIANCE_FINISH,
							userNo,orgNo,custName, userName, startTime, endTime, page, rows);
			re = returnSuccess();
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 获取合规审查的基本信息   类似客户名字  授信品种等
	 * @param request
	 * @param id
	 * @return
	 */
	@RequestMapping("getComplianceBaseInfo.json")
	@ResponseBody
	public ResultDto getComplianceBaseInfo(HttpServletRequest request, Integer id){
		ResultDto re = returnFail();
		if(id==null || "".equals(id)){
			re.setMsg("当前审查任务信息异常，请重试");
			return re;
		}
		try{
			LcTask task = lcTaskService.findByPrimaryKey(id);
			if(task==null){
				re.setMsg("当前审查任务信息异常，请重试");
				log.error("当前id异常，数据库不存在相应合规审查任务");
				return re;
			}
			re.setRows(task);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		}catch (Exception e){
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 获取 授信任务中存在的异常点  分页
	 * @param id	任务id
	 * @param page	页码
	 * @param rows	每页数
	 * @return
	 */
	@RequestMapping("getTaskRiskPaging.json")
	@ResponseBody
	public ResultDto getTaskRiskPaging(Integer id, HttpServletRequest request,
                                       @RequestParam(value="page",defaultValue="1")int page,
                                       @RequestParam(value="rows",defaultValue="10")int rows) {
		ResultDto re = returnFail();
		try {
            User user = HttpUtil.getCurrentUser(request);
            Pagination<LcTaskRiskDto> pager = lcTaskService.queryTaskRiskPaging(id, user.getUserId(), page, rows);
			re = returnSuccess();
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}


    //查询 合规审查内容列表  （非分页）
    @RequestMapping("queryTaskRiskList.json")
    @ResponseBody
    public ResultDto queryTaskRiskList(Integer taskId){
        ResultDto re = returnFail();
        try {
            List<LcTaskRiskDto> lcTaskRiskDtos = lcTaskService.queryTaskRiskList(taskId);
            
            Map<String, List<LcTaskRiskDto>> taskRiskMap =
                    lcTaskRiskDtos.stream().collect(Collectors.groupingBy(LcTaskRiskDto::getReviewName));
            System.out.println(taskRiskMap);
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("获取合规审查结果列表成功");
            re.setRows(taskRiskMap);
        }catch (Exception e){
            e.printStackTrace();
        }
        return re;
    }

    /**
     * 即时即改（修改合规审查的结果状态（待处理-》已整改））
     */
    @RequestMapping("immediateChange.json")
    @ResponseBody
    public ResultDto immediateChange(Integer reviewContentId, Integer rectify) {
        ResultDto re = returnFail();
        if(reviewContentId==null || "".equals(reviewContentId)){
            re.setMsg("风险项数据异常");
            return re;
        }
        try {
            LcReviewContent reviewContent = lcReviewContentService.findByPrimaryKey(reviewContentId);
            if(reviewContent==null){
                re.setMsg("风险项数据异常");
                return re;
            }
            reviewContent.setRectify(rectify);
            lcReviewContentService.saveOrUpdate(reviewContent);
            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("修改问题项状态成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }


	/**
	 * 下载合规审查工作底稿
	 * @param request
	 * @param response
	 * @param taskId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("downloadLcAccount.json")
	public ResponseEntity<byte[]> downloadLcAccount(HttpServletRequest request,
												 HttpServletResponse response, Integer taskId) throws Exception {

		ResponseEntity<byte[]> result = null;
		//模板数据
		Map<String, Object> dataMap = new HashMap<>();
		try{
			//获取合规审查任务信息
			LcTask lcTsk = lcTaskService.findByPrimaryKey(taskId);
			String industry = lcTaskService.ChineseIndustry(lcTsk.getIndustry());
			//获取合规审查风险项
			List<LcTaskRiskDto> lcTaskRiskDtos = lcTaskService.queryTaskRiskList(taskId);
			//风险项分组
			Map<String, List<LcTaskRiskDto>> taskRiskMap =
					lcTaskRiskDtos.stream().collect(Collectors.groupingBy(LcTaskRiskDto::getReviewName));
			
			
			dataMap.put("task", lcTsk);
			dataMap.put("industry", industry);
			dataMap.put("riskMap", taskRiskMap);

			String path = AppConfig.Cfg.ACCOUNT_PATH;// 合规审查工作底稿地址
			File dir = new File(path + DateUtil.formatDate(new Date(), "yyyy-MM" ));
			//+ File.separator + lcTsk.getSerialNumber()
			if(!dir.exists())
				dir.mkdirs();
			File file = new File(dir, lcTsk.getSerialNumber() + "_LC_ACCOUNT.xls");
			FreeMarkerWordUtil.createWord(dataMap, "loan_statistics/lc/lcAccount.htm", dir.getPath(), file.getName());

			HttpHeaders headers = new HttpHeaders();
			headers.setContentDispositionFormData("attachment", CodeUtil.parseGBK("授信合规审查工作底稿.xls"));
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			result = new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file),
					headers, HttpStatus.OK);
		} catch (Exception e) {
			log.error("下载授信合规审查工作底稿文件异常：", e);
		}

		return result;
	}

	/**
	 * 下载合规审查意见书
	 * @param request
	 * @param response
	 * @param taskId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("downloadLcProposal.json")
	public ResponseEntity<byte[]> downloadLcProposal(HttpServletRequest request,
													HttpServletResponse response, Integer taskId) throws Exception {

		ResponseEntity<byte[]> result = null;
		//模板数据
		Map<String, Object> dataMap = new HashMap<>();
		try{
			//获取合规审查任务信息
			LcTask lcTsk = lcTaskService.findByPrimaryKey(taskId);
			//获取合规审查风险项
			List<LcTaskRiskDto> lcTaskRiskDtos = lcTaskService.queryTaskRiskList(taskId);
			//风险项分组
			Map<String, List<LcTaskRiskDto>> taskRiskMap =
					lcTaskRiskDtos.stream().collect(Collectors.groupingBy(LcTaskRiskDto::getReviewName));

			dataMap.put("task", lcTsk);
			dataMap.put("riskMap", taskRiskMap);

			String path = AppConfig.Cfg.ACCOUNT_PATH;// 合规审查工作底稿地址
			File dir = new File(path + DateUtil.formatDate(new Date(), "yyyy-MM" ));
			//+ File.separator + lcTsk.getSerialNumber()
			if(!dir.exists())
				dir.mkdirs();
			File file = new File(dir, lcTsk.getSerialNumber() + "_LC_PROPOSAL.xls");
			FreeMarkerWordUtil.createWord(dataMap, "loan_statistics/lc/lcProposal.htm", dir.getPath(), file.getName());

			HttpHeaders headers = new HttpHeaders();
			headers.setContentDispositionFormData("attachment", CodeUtil.parseGBK("合规审查意见书.xls"));
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			result = new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file),
					headers, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("下载合规审查意见书文件异常：", e);
		}

		return result;
	}


	/**
	 * 查看合规审查报告 界面
	 * @return
	 */
	@RequestMapping("viewLcReport.do")
	public ModelAndView viewLcReport(HttpServletRequest request, String startTime, String endTime){
		ModelAndView mav = new ModelAndView("views/compliance/lcReport");
        User user = HttpUtil.getCurrentUser(request);
        String userNo = user.getUserId();
		LcReportDto reportDto = lcTaskService.queryLcReportData(userNo, startTime, endTime);
		mav.addObject("data", reportDto);
		return mav;
	}

	/**
	 * 下载合规审查报告
	 * @param request
	 * @param response
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	@RequestMapping("downloadLcReport.json")
	public ResponseEntity<byte[]> downloadLcReport(HttpServletRequest request,
													 HttpServletResponse response, String startTime,String endTime)  {
		ResponseEntity<byte[]> result = null;
		//模板数据
		Map<String, Object> dataMap = new HashMap<>();
		User user = HttpUtil.getCurrentUser(request);
		try{
			//风险项分组
			String userNo = user.getUserId();
			LcReportDto reportDto = lcTaskService.queryLcReportData(userNo, startTime, endTime);
			dataMap.put("data", reportDto);

			dataMap.put("startTime", startTime);
			dataMap.put("endTime", endTime);

			int year = DateUtil.getYear(new Date());
			int month = DateUtil.getMonth(new Date());
			int day = DateUtil.getDay(new Date());
			dataMap.put("year", year);
			dataMap.put("month", month);
			dataMap.put("day", day);

			String path = AppConfig.Cfg.ACCOUNT_PATH;// 合规审查工作底稿地址
			File dir = new File(path + DateUtil.formatDate(new Date(), "yyyy-MM" ));
			//+ File.separator + lcTsk.getSerialNumber()
			if(!dir.exists())
				dir.mkdirs();
			File file = new File(dir, "LC_REPORT.doc");
			FreeMarkerWordUtil.createWord(dataMap, "loan_statistics/lc/lcReport_excel.htm", dir.getPath(), file.getName());

			HttpHeaders headers = new HttpHeaders();
			headers.setContentDispositionFormData("attachment", CodeUtil.parseGBK("合规审查报告.xls"));
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			result = new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file),
					headers, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("下载合规审查报告文件异常：", e);
		}

		return result;
	}


    /***********************    合规审查任务    结束      *********************/



    /**
     * 手动插入合规审查数据
     * @return
     */
    @RequestMapping(value = "insertDemoData.json",method = RequestMethod.GET)
    @ResponseBody
    public ResultDto insertDemoData() {
        ResultDto re = returnFail();
        try {
            LmProcess process  = new LmProcess();
            process.setStage(Constants.LoanProcessType.COMPLIANCE_PENDING);
            process.setCustomerNo("20200324165810");
            process.setSerialNumber("LC_TEST_00003");
            process.setCreater("8888");
            process.setCreateTime(DateUtil.getNowTimestamp());
            process.setUpdater("8888");
            process.setUpdateTime(DateUtil.getNowTimestamp());
            lmProcessService.saveOrUpdate(process);

            LcTask task = new LcTask();
            task.setSerialNumber("LC_TEST_00003");
            task.setUserNo("8888");
            task.setCustomerNo("20200324165810");
            task.setCustomerName("XXXX石业有限公司");
            task.setLrUserNo("8888");
            task.setLbUserNo("8888");
            task.setLbUserName("超级管理员");
            task.setGuarantor("李四");
            task.setBusinessType(4);
            task.setLoanNumber(6000000d);
            task.setLoanTerm(36);
            task.setStockNumber(50000d);
            task.setLoanUse("购买原材料");
            task.setAssureMeans("担保保证");
            task.setLoanRate(8.7f);
            task.setSubmissionTime(DateUtil.getNowTimestamp());
            task.setCreditExpireDate(DateUtil.getLastMonthTimestamp(5));
            task = lcTaskService.saveOrUpdate(task);

            LcReviewContent content1 = new LcReviewContent(task.getId(),50, "11111111111");
            LcReviewContent content2 = new LcReviewContent(task.getId(),51, "问题二：ccccccccc");
            LcReviewContent content3 = new LcReviewContent(task.getId(),100, "问题三：333333333");
            LcReviewContent content4 = new LcReviewContent(task.getId(),101, "问题四：444444444444");
            lcReviewContentService.saveOrUpdate(content1);
            lcReviewContentService.saveOrUpdate(content2);
            lcReviewContentService.saveOrUpdate(content3);
            lcReviewContentService.saveOrUpdate(content4);

            re.setCode(ResultDto.RESULT_CODE_SUCCESS);
            re.setMsg("success");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return re;
    }

	/** 跳转到 汇总统计 */
	@RequestMapping("summaryStat.do")
	public ModelAndView summaryStat(HttpServletRequest request, HttpServletResponse response){
		return new ModelAndView("views/compliance/sumStat");
	}

	/**
	 * 获取合规审查 汇总统计列表
	 * @param startTime 起始时间
	 * @param endTime 结束时间
	 * @param page 当前页
	 * @param rows 每页条数
	 * @return
	 */
	@RequestMapping(value = "getTaskStatList.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto getTaskStatList(HttpServletRequest request,
			@RequestParam(value="startTime", required = false)String startTime,
			@RequestParam(value="endTime", required = false)String endTime,
			@RequestParam(value="page",defaultValue="1")int page,
			@RequestParam(value="rows",defaultValue="10")int rows){
		ResultDto re = returnFail("获取合规审查汇总统计列表失败");
		try {
			User user = HttpUtil.getCurrentUser(request);
			String userNo = null;
			if(SysConstants.UserDataAccess.PERSONAL.equals(user.getDataAccess()))
				userNo = user.getUserId();
			Pagination<LcTaskStatDto> pager =
					lcTaskService.queryTaskStatPaging(userNo, startTime, endTime, page, rows);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());
			re.setMsg("获取合规审查汇总统计列表成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/** 跳转到 问题列表 */
	@RequestMapping("riskStat.do")
	public ModelAndView riskStat(HttpServletRequest request, HttpServletResponse response){
		return new ModelAndView("views/compliance/riskStat");
	}

	/**
	 * 获取合规审查 问题汇总列表
	 * @param startTime 起始时间
	 * @param endTime 结束时间
	 * @param rectify 整改情况
	 * @param page 当前页
	 * @param rows 每页条数
	 * @return
	 */
	@RequestMapping(value = "getRiskStatList.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto getRiskStatList(HttpServletRequest request,
			@RequestParam(value="startTime", required = false)String startTime,
			@RequestParam(value="endTime", required = false)String endTime,
			String rectify,
			@RequestParam(value="page",defaultValue="1")int page,
			@RequestParam(value="rows",defaultValue="10")int rows){
		ResultDto result = returnFail("获取合规审查、问题统计列表失败");
		try {
			User user = HttpUtil.getCurrentUser(request);
			String userNo = null;
			if(SysConstants.UserDataAccess.PERSONAL.equals(user.getDataAccess()))
				userNo = user.getUserId();
			Pagination<LcRiskStatDto> pager =
					lcTaskService.queryRiskStatPaging(userNo, startTime, endTime, rectify, page, rows);
			result.setCode(ResultDto.RESULT_CODE_SUCCESS);
			result.setRows(pager.getItems());
			result.setTotal(pager.getRowsCount());
			result.setMsg("获取合规审查、问题统计列表成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/** 跳转到 客户经理问题列表 */
	@RequestMapping("managerRiskStat.do")
	public ModelAndView managerRiskStat(HttpServletRequest request, HttpServletResponse response){
		return new ModelAndView("views/compliance/managerRiskStat");
	}

	/**
	 * 获取合规审查 客户经理问题汇总列表
	 * @param startTime 起始时间
	 * @param endTime 结束时间
	 * @param page 当前页
	 * @param rows 每页条数
	 * @return
	 */
	@RequestMapping(value = "getManagerRiskStat.json", method = RequestMethod.POST)
	@ResponseBody
	public ResultDto getManagerRiskStat(HttpServletRequest request,
			@RequestParam(value="startTime", required = false)String startTime,
			@RequestParam(value="endTime", required = false)String endTime,
			@RequestParam(value="page",defaultValue="1")int page,
			@RequestParam(value="rows",defaultValue="10")int rows){
		ResultDto result = returnFail("获取合规审查、客户经理问题统计列表失败");
		try {

			User user = HttpUtil.getCurrentUser(request);
			String userNo = null;
			if(SysConstants.UserDataAccess.PERSONAL.equals(user.getDataAccess()))
				userNo = user.getUserId();
			Pagination<LcManagerRiskStatDto> pager =
					lcTaskService.queryManagerRiskStatPaging(userNo, startTime, endTime, page, rows);
			result.setCode(ResultDto.RESULT_CODE_SUCCESS);
			result.setRows(pager.getItems());
			result.setTotal(pager.getRowsCount());
			result.setMsg("获取合规审查、客户经理问题统计列表成功");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 合规审查 导出Excel
	 * @param fileType 表的类型，如汇总表、问题表
	 * @param startTime 起始时间
	 * @param endTime 结束时间
	 * @return
	 */
	@RequestMapping(value = "downloadExcel.do", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<byte[]> downloadLcExcel(String fileType, HttpServletRequest request, HttpServletResponse response,
									@RequestParam(value="startTime", required = false)String startTime,
									@RequestParam(value="endTime", required = false)String endTime,
									@RequestParam(value="rectify", required = false)String rectify){
		ResponseEntity<byte[]> result = null;
		try {
			User user = HttpUtil.getCurrentUser(request);
			Map<String, Object> dataMap = this.getExcelData(user.getUserId(), fileType, rectify, startTime, endTime);
			String templateName = (String) dataMap.get("templateName");
			String fileName = dataMap.get("titleName") + ".xls";
			String destPath = AppConfig.Cfg.ACCOUNT_PATH
					+ DateUtil.formatDate(new Date(), "yyyy-MM" );
			File file = new File(destPath , fileName);
			if(!file.getParentFile().exists())
				file.getParentFile().mkdirs();
			FreeMarkerWordUtil.createWord(dataMap, templateName, destPath, file.getName());

			HttpHeaders headers = new HttpHeaders();
			
			headers.setContentDispositionFormData("attachment", CodeUtil.parseGBK(fileName));
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			result = new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file), headers, HttpStatus.OK);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * 获取 Excel 数据集
	 * @param userNo 客户经理Id
	 * @param fileType Excel文件类型
	 * @param startTime 起始时间
	 * @param endTime 结束时间
	 * @return
	 */
	public Map<String, Object> getExcelData(String userNo, String fileType, String rectify,String startTime, String endTime) {
		String templateName = "", titleName = "", dateTime = "";
		Map<String, Object> dataMap = new HashMap<>();
		if (Constants.LC_REVIEW_CONTENT_STATE.SUM_STAT.equals(fileType)){			// 汇总统计表
			templateName = "loan_statistics/lc/sumStat.htm";
			titleName = DateUtil.getTimeLocation(startTime, endTime) + "合规审查统计表";
			List<LcTaskStatDto> resultList = lcTaskService.queryTaskStatList(userNo, startTime, endTime);
			double loanSum = resultList.stream().mapToDouble(LcTaskStatDto::getLoanNumber).sum();
			dataMap.put("taskStatList", resultList);
			dataMap.put("loanSum", loanSum);
		}else if (Constants.LC_REVIEW_CONTENT_STATE.RISK_STAT.equals(fileType)){		// 问题汇总表
			templateName = "loan_statistics/lc/riskStat.htm";
			titleName = DateUtil.getTimeLocation(startTime, endTime) + "合规审查问题汇总表";
			List<LcRiskStatDto> resultList = lcTaskService.queryRiskStatList(userNo, rectify,startTime,endTime);
			int solvedSum = 0;
			if(!CollectionUtils.isEmpty(resultList)){
				for(LcRiskStatDto lcRiskStat : resultList){
					if(lcRiskStat.getRectify() != null && lcRiskStat.getRectify() != 0){
						solvedSum++; 
					}
				}
			}
            //int solvedSum = resultList.size() - resultList.stream().mapToInt(LcRiskStatDto::getState).sum();
			dataMap.put("solvedSum", solvedSum);
            dataMap.put("riskStatList", resultList);
		}else if (Constants.LC_REVIEW_CONTENT_STATE.MANAGER_RISK_STAT.equals(fileType)){	// 客户经理问题汇总表
			templateName = "loan_statistics/lc/managerRiskStat.htm";
			titleName = DateUtil.getTimeLocation(startTime, endTime) + "客户经理问题汇总表";
			List<LcManagerRiskStatDto> resultList = 
					lcTaskService.queryManagerRiskStatList(userNo, startTime, endTime);
			dataMap.put("managerRiskStatList", resultList);
		}
		if (startTime != null && !"".equals(startTime))
			dateTime = startTime + "至" + endTime;
		dataMap.put("dateTime", dateTime);
		dataMap.put("titleName", titleName);
		dataMap.put("templateName", templateName);
		return dataMap;
	}

	/**
	 * 获取推荐额度：
	 * @param taskId
	 * @return
	 */
	@RequestMapping("getRecommendQuota.do")
	public ModelAndView getRecommendQuota(Integer taskId){
		ModelAndView mav = new ModelAndView("views/examine/examineQuota");
		if(taskId==null){
			return mav;
		}
		try{
			LcTask lcTask = lcTaskService.findByPrimaryKey(taskId);
			String serialNumber = lcTask.getSerialNumber();
			QuotaFullDto commendQuota = finriskResultService.getCommendQuota(serialNumber);
			mav.addObject("commendQuota", commendQuota);
		}catch (Exception e){
			log.error("获取对公推荐额度失败",e);
		}
		return mav;
	}


}
