package com.beawan.corporateloan.entity;

import com.beawan.core.BaseEntity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * 营销记录表
 */

@Entity
@Table(name = "LR_VISIT_RECORD",schema = "GDTCESYS")
public class LrVisitRecord extends BaseEntity {
	@Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LR_VISIT_RECORD_SEQ")
  //  @SequenceGenerator(name="LR_VISIT_RECORD_SEQ",allocationSize=1,initialValue=1, sequenceName="LR_VISIT_RECORD_SEQ")
    private Integer id;
	@Column(name = "SERIAL_NUMBER")
	private String serialNumber;//流水号
	@Column(name = "VISIT_WAY")
    private Integer visitWay;//拜访方式 0微信  1电话  2实地
	@Column(name = "VISIT_PERSON_NAME")
    private String visitPersonName;//拜访客户名字-对接人
	@Column(name = "VISIT_SITUATION")
    private String visitSituation;//拜访信息
	@Column(name = "VISIT_TIME")
    private Timestamp visitTime;//拜访时间
	@Column(name = "VISIT_ADDR")
    private String visitAddr;//拜访地址
	@Column(name = "LONGITUDE")
    private Double longGitude;//经度
	@Column(name = "LATITUDE")
    private Double latitude;//纬度
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public Integer getVisitWay() {
		return visitWay;
	}
	public void setVisitWay(Integer visitWay) {
		this.visitWay = visitWay;
	}
	public String getVisitPersonName() {
		return visitPersonName;
	}
	public void setVisitPersonName(String visitPersonName) {
		this.visitPersonName = visitPersonName;
	}
	public String getVisitSituation() {
		return visitSituation;
	}
	public void setVisitSituation(String visitSituation) {
		this.visitSituation = visitSituation;
	}
	public Timestamp getVisitTime() {
		return visitTime;
	}
	public void setVisitTime(Timestamp visitTime) {
		this.visitTime = visitTime;
	}
	public String getVisitAddr() {
		return visitAddr;
	}
	public void setVisitAddr(String visitAddr) {
		this.visitAddr = visitAddr;
	}
	public Double getLongGitude() {
		return longGitude;
	}
	public void setLongGitude(Double longGitude) {
		this.longGitude = longGitude;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
    
}
