package com.beawan.corporateloan.entity;


import com.beawan.core.BaseEntity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * 贷款审查任务类
 */
@Entity
@Table(name = "LE_TASK",schema = "GDTCESYS")
public class LeTask extends BaseEntity {
    @Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LE_TASK_SEQ")
   // @SequenceGenerator(name="LE_TASK_SEQ",allocationSize=1,initialValue=1, sequenceName="LE_TASK_SEQ")
    private Integer id;
    @Column(name = "SERIAL_NUMBER")
    private String serialNumber;
    @Column(name = "USER_NO")
    private String userNo;// 当前审查经理
    @Column(name = "CUSTOMER_NO")
    private String customerNo;
    @Column(name = "CUSTOMER_NAME")
    private String customerName;
    @Column(name = "LB_USER_NO")
    private String lbUserNo;// 贷前客户经理id
    @Column(name = "LB_USER_NAME")
    private String lbUserName;// 贷前客户经理
    @Column(name = "BUSINESS_TYPE")
    private Integer businessType;// 业务类型
    @Column(name = "LOAN_NUMBER")
    private Double loanNumber;// 授信总额
    @Column(name = "LOAN_TERM")
    private Integer loanTerm;// 期限
    @Column(name = "STOCK_NUMBER")
    private Double stockNumber;// 存量授信
    @Column(name = "LOAN_USE")
    private String loanUse;// 贷款用途
    @Column(name = "ASSURE_MEANS")
    private String assureMeans;// 担保方式
    @Column(name = "LOAN_RATE")
    private Float loanRate;// 利率

    @Column(name = "DG_RATE")
    private Double dgRate;
    @Column(name = "DG_GRADE")// 对公评级模型
    private String dgGrade;
    @Column(name = "SUBMISSION_TIME")
    private Timestamp submissionTime;// 送审时间
    @Column(name = "REVIEW_TIME")
    private Timestamp reviewTime;// 审查时间
    @Column(name = "REVIEW_COUNT")
    private Integer reviewCount;// 送审次数--每次提交这里会加1，初始值为1
    @Column(name = "CREDIT_CONDITION")
    private String creditCondition;// 授信前提条件
    @Column(name = "THIS_APPLY")
    private Double thisApply;//本次申请
    @Column(name = "OCCUR_TYPE")
    private String occurType;//发生类型
    @Column(name = "SUMMARY")
    private String summary;//概况


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getLbUserNo() {
        return lbUserNo;
    }

    public void setLbUserNo(String lbUserNo) {
        this.lbUserNo = lbUserNo;
    }

    public String getLbUserName() {
        return lbUserName;
    }

    public void setLbUserName(String lbUserName) {
        this.lbUserName = lbUserName;
    }

    public Integer getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Integer businessType) {
        this.businessType = businessType;
    }

    public Double getLoanNumber() {
        return loanNumber;
    }

    public void setLoanNumber(Double loanNumber) {
        this.loanNumber = loanNumber;
    }

    public Integer getLoanTerm() {
        return loanTerm;
    }

    public void setLoanTerm(Integer loanTerm) {
        this.loanTerm = loanTerm;
    }

    public Double getStockNumber() {
        return stockNumber;
    }

    public void setStockNumber(Double stockNumber) {
        this.stockNumber = stockNumber;
    }

    public String getLoanUse() {
        return loanUse;
    }

    public void setLoanUse(String loanUse) {
        this.loanUse = loanUse;
    }

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    public Float getLoanRate() {
        return loanRate;
    }

    public void setLoanRate(Float loanRate) {
        this.loanRate = loanRate;
    }

    public Timestamp getSubmissionTime() {
        return submissionTime;
    }

    public void setSubmissionTime(Timestamp submissionTime) {
        this.submissionTime = submissionTime;
    }

    public Timestamp getReviewTime() {
        return reviewTime;
    }

    public void setReviewTime(Timestamp reviewTime) {
        this.reviewTime = reviewTime;
    }

    public Double getDgRate() {
        return dgRate;
    }

    public void setDgRate(Double dgRate) {
        this.dgRate = dgRate;
    }

    public Integer getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(Integer reviewCount) {
        this.reviewCount = reviewCount;
    }

    public String getDgGrade() {
        return dgGrade;
    }

    public void setDgGrade(String dgGrade) {
        this.dgGrade = dgGrade;
    }

	public String getCreditCondition() {
		return creditCondition;
	}

	public void setCreditCondition(String creditCondition) {
		this.creditCondition = creditCondition;
	}
	
    public Double getThisApply() {
        return thisApply;
    }

    public void setThisApply(Double thisApply) {
        this.thisApply = thisApply;
    }
    
	public String getOccurType() {
		return occurType;
	}

	public void setOccurType(String occurType) {
		this.occurType = occurType;
	}
	
	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}
    
}
