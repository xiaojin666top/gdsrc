package com.beawan.corporateloan.entity;


import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 授信审查要点
 */
@Entity
@Table(name = "LE_EXAMINE_POINT",schema = "GDTCESYS")
public class LeExaminePoint extends BaseEntity {

    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LE_EXAMINE_POINT_SEQ")
   // @SequenceGenerator(name="LE_EXAMINE_POINT_SEQ",allocationSize=1,initialValue=1, sequenceName="LE_EXAMINE_POINT_SEQ")
    private Integer id;

    /**
     * 审查类别
     */
    @Column(name = "EXAMINE_TYPE")
    private String examineType;

    /**
     * 审查目录
     */
    @Column(name = "EXAMINE_NAME")
    private String examineName;

    /**
     * 审查要点
     */
    @Column(name = "EXAMINE_MEMO")
    private String examineMemo;

    /**
     * 是否审查
     */
    @Column(name = "FLAG")
    private String flag;

    /**
     * 估计用时 （暂时不需要）
     */
    @Column(name = "NEED_MINUTE")
    private Integer needMinute;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getExamineType() {
        return examineType;
    }

    public void setExamineType(String examineType) {
        this.examineType = examineType;
    }

    public String getExamineName() {
        return examineName;
    }

    public void setExamineName(String examineName) {
        this.examineName = examineName;
    }

    public String getExamineMemo() {
        return examineMemo;
    }

    public void setExamineMemo(String examineMemo) {
        this.examineMemo = examineMemo;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public Integer getNeedMinute() {
        return needMinute;
    }

    public void setNeedMinute(Integer needMinute) {
        this.needMinute = needMinute;
    }
}
