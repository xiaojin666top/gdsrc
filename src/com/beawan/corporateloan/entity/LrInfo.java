package com.beawan.corporateloan.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;

/**
 * 营销信息表
 * @author songchao
 *
 */
@Entity
@Table(name = "LR_INFO",schema = "GDTCESYS")
public class LrInfo extends BaseEntity{
	@Id
  //  @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LR_INFO_SEQ")
    //@SequenceGenerator(name="LR_INFO_SEQ",allocationSize=1,initialValue=1, sequenceName="LR_INFO_SEQ")
	private Integer id;
	@Column(name = "CUSTOMER_NO")
	private String customerNo;//客户编号
	@Column(name = "SERIAL_NUMBER")
	private String serialNumber;//主流程业务编号
	@Column(name = "USER_NO")
	private String userNo;//用户编号---营销经理号
	@Column(name = "CUSTOMER_CLASS")
	private Integer customerClass;//当前客户分类
	
	@Column(name = "RETAIL_PRODUCT")
    private String retailProduct;//业务类型
	@Column(name = "RETAIL_TERM")
    private Integer retailTerm;//期限
	@Column(name = "RETAIL_MONEY")
    private Double retailMoney;//额度
	@Column(name = "RETAIL_RATE")
    private Double retailRate;//利率
	@Column(name = "RETAIL_USE")
    private String retailUse;//用途
	@Column(name = "RETAIL_GUARANTE")
    private String retailGuarante;//担保方式
	@Column(name = "retail_status")
	private String retailStatus;//营销结果 '0'：营销成功


	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getUserNo() {
		return userNo;
	}
	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}
	public Integer getCustomerClass() {
		return customerClass;
	}
	public void setCustomerClass(Integer customerClass) {
		this.customerClass = customerClass;
	}
	public String getRetailStatus() {
		return retailStatus;
	}
	public void setRetailStatus(String retailStatus) {
		this.retailStatus = retailStatus;
	}
	public String getRetailProduct() {
		return retailProduct;
	}
	public void setRetailProduct(String retailProduct) {
		this.retailProduct = retailProduct;
	}
	public Integer getRetailTerm() {
		return retailTerm;
	}
	public void setRetailTerm(Integer retailTerm) {
		this.retailTerm = retailTerm;
	}
	public Double getRetailMoney() {
		return retailMoney;
	}
	public void setRetailMoney(Double retailMoney) {
		this.retailMoney = retailMoney;
	}
	public Double getRetailRate() {
		return retailRate;
	}
	public void setRetailRate(Double retailRate) {
		this.retailRate = retailRate;
	}
	public String getRetailUse() {
		return retailUse;
	}
	public void setRetailUse(String retailUse) {
		this.retailUse = retailUse;
	}
	public String getRetailGuarante() {
		return retailGuarante;
	}
	public void setRetailGuarante(String retailGuarante) {
		this.retailGuarante = retailGuarante;
	}
	

}
