package com.beawan.corporateloan.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 合规核查 详细风险点
 */
@Entity
@Table(name = "LC_RISK_DB",schema = "GDTCESYS")
public class LcRiskDb extends BaseEntity {

    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LC_RISK_DB_SEQ")
   // @SequenceGenerator(name="LC_RISK_DB_SEQ",allocationSize=1,initialValue=1, sequenceName="LC_RISK_DB_SEQ")
    private Integer id;

    @Column(name = "POINT_ID")
    private Integer pointId;

    @Column(name = "RISK_INFO")
    private String riskInfo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPointId() {
        return pointId;
    }

    public void setPointId(Integer pointId) {
        this.pointId = pointId;
    }

    public String getRiskInfo() {
        return riskInfo;
    }

    public void setRiskInfo(String riskInfo) {
        this.riskInfo = riskInfo;
    }
}
