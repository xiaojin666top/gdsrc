package com.beawan.corporateloan.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;

/**
 * 营销记录表
 */
@Entity
@Table(name = "LR_RECORD",schema = "GDTCESYS")
public class LrRecordList extends BaseEntity {
	@Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LR_RECORD_SEQ")
   // @SequenceGenerator(name="LR_RECORD_SEQ",allocationSize=1,initialValue=1, sequenceName="LR_RECORD_SEQ")
    private Integer id;
	@Column(name = "SERIAL_NUMBER")
    private String serialNumber;//流水号
	@Column(name = "CUSTOMER_NO")
    private String customerNo;//客户编号
	@Column(name = "USER_NO")
    private String userNo;//用户编号
	@Column(name = "DEAL_TYPE")
    private Integer dealType;//操作类型
	@Column(name = "RECORD_REMARK")
    private String recordRemark;//操作说明
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public Integer getDealType() {
		return dealType;
	}
	public void setDealType(Integer dealType) {
		this.dealType = dealType;
	}
	public String getUserNo() {
		return userNo;
	}
	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}
	public String getRecordRemark() {
		return recordRemark;
	}
	public void setRecordRemark(String recordRemark) {
		this.recordRemark = recordRemark;
	}
}