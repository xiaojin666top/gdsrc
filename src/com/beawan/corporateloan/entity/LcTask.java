package com.beawan.corporateloan.entity;


import com.beawan.core.BaseEntity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * 合规审查任务类
 */
@Entity
@Table(name = "LC_TASK",schema = "GDTCESYS")
public class LcTask extends BaseEntity {

    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LC_TASK_SEQ")
   // @SequenceGenerator(name="LC_TASK_SEQ",allocationSize=1,initialValue=1, sequenceName="LC_TASK_SEQ")
    private Integer id;

    @Column(name = "SERIAL_NUMBER")
    private String serialNumber;
    @Column(name = "USER_NO")
    private String userNo;// 当前审查经理
    @Column(name = "CUSTOMER_NO")
    private String customerNo;
    @Column(name = "CUSTOMER_NAME")
    private String customerName;
    @Column(name = "LR_USER_NO")
    private String lrUserNo;//营销客户经理号
    @Column(name = "LB_USER_NO")
    private String lbUserNo;//贷前调查客户经理号
    @Column(name = "LB_USER_NAME")
    private String lbUserName;//贷前调查客户经理名字
    @Column(name = "GUARANTOR")
    private String guarantor;//担保人
    @Column(name = "BUSINESS_TYPE")
    private Integer businessType;//业务类型
    @Column(name = "LOAN_NUMBER")
    private Double loanNumber;//授信金额
    @Column(name = "LOAN_TERM")
    private Integer loanTerm;//期限
    @Column(name = "STOCK_NUMBER")
    private Double stockNumber;//存量金额
    @Column(name = "LOAN_USE")
    private String loanUse;//贷款用途
    @Column(name = "ASSURE_MEANS")
    private String assureMeans;//担保方式
    @Column(name = "LOAN_RATE")
    private Float loanRate;//利率
    @Column(name = "DG_RATE")
    private Double dgRate;

    @Column(name = "DG_GRADE")//对公评级模型
    private String dgGrade;
    @Column(name = "SUBMISSION_TIME")
    private Timestamp submissionTime;//送审时间
    @Column(name = "CREDIT_EXPIRE_DATE")
    private Timestamp creditExpireDate;//最近一笔授信到期日
    @Column(name = "REVIEW_TIME")
    private Timestamp reviewTime;//审查时间
    @Column(name = "INDUSTRY")
    private String industry;//行业大类分类

    @Column(name = "LC_COUNT")
    private Integer lcCount;//合规审查次数

    @Column(name = "MEMO")
    private String memo;//其他说明


	@Column(name = "LE_USER_NO")
	private String leUserNo;//贷前指定的审查审批经理id


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getLrUserNo() {
        return lrUserNo;
    }

    public void setLrUserNo(String lrUserNo) {
        this.lrUserNo = lrUserNo;
    }

    public String getLbUserNo() {
        return lbUserNo;
    }

    public void setLbUserNo(String lbUserNo) {
        this.lbUserNo = lbUserNo;
    }

    public String getLbUserName() {
        return lbUserName;
    }

    public void setLbUserName(String lbUserName) {
        this.lbUserName = lbUserName;
    }

    public String getGuarantor() {
        return guarantor;
    }

    public Double getDgRate() {
        return dgRate;
    }

    public void setDgRate(Double dgRate) {
        this.dgRate = dgRate;
    }

    public void setGuarantor(String guarantor) {
        this.guarantor = guarantor;
    }

    public Integer getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Integer businessType) {
        this.businessType = businessType;
    }

    public Double getLoanNumber() {
        return loanNumber;
    }

    public void setLoanNumber(Double loanNumber) {
        this.loanNumber = loanNumber;
    }

    public Integer getLoanTerm() {
        return loanTerm;
    }

    public void setLoanTerm(Integer loanTerm) {
        this.loanTerm = loanTerm;
    }

    public Double getStockNumber() {
        return stockNumber;
    }

    public void setStockNumber(Double stockNumber) {
        this.stockNumber = stockNumber;
    }

    public String getLoanUse() {
        return loanUse;
    }

    public void setLoanUse(String loanUse) {
        this.loanUse = loanUse;
    }

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    public Float getLoanRate() {
        return loanRate;
    }

    public void setLoanRate(Float loanRate) {
        this.loanRate = loanRate;
    }

    public Timestamp getSubmissionTime() {
        return submissionTime;
    }

    public void setSubmissionTime(Timestamp submissionTime) {
        this.submissionTime = submissionTime;
    }

    public Timestamp getCreditExpireDate() {
        return creditExpireDate;
    }

    public void setCreditExpireDate(Timestamp creditExpireDate) {
        this.creditExpireDate = creditExpireDate;
    }

    public Timestamp getReviewTime() {
        return reviewTime;
    }

    public void setReviewTime(Timestamp reviewTime) {
        this.reviewTime = reviewTime;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getDgGrade() {
        return dgGrade;
    }

    public void setDgGrade(String dgGrade) {
        this.dgGrade = dgGrade;
    }

    public Integer getLcCount() {
        return lcCount;
    }

    public void setLcCount(Integer lcCount) {
        this.lcCount = lcCount;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

	public String getLeUserNo() {
		return leUserNo;
	}

	public void setLeUserNo(String leUserNo) {
		this.leUserNo = leUserNo;
	}

    
    
}
