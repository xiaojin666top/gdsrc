package com.beawan.corporateloan.entity;


import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 授信审查详细信息表
 * 打回重新提交后，本笔贷款的这个审查记录表会出现多条
 */
@Entity
@Table(name = "LE_EXAMINE_CONTENT",schema = "GDTCESYS")
public class LeExamineContent extends BaseEntity {

    @Id
  //  @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LE_EXAMINE_CONTENT_SEQ")
 //   @SequenceGenerator(name="LE_EXAMINE_CONTENT_SEQ",allocationSize=1,initialValue=1, sequenceName="LE_EXAMINE_CONTENT_SEQ")
    private Integer id;

    @Column(name = "le_task_id")
    private Integer leTaskId;//贷款审查任务号主键id

    /**
     * 审查点id（外键）
     */
    @Column(name = "EXAMINE_POINT_ID")
    private Integer examinePointId;
    /**
     * 审查类别
     */
    @Column(name = "EXAMINE_TYPE")
    private String examineType;
    /**
     * 审查目录
     */
    @Column(name = "EXAMINE_NAME")
    private String examineName;

    /**
     * 审查信息 说明--不通过原因
     */
    @Column(name = "CONTENT")
    private String content;

    /**
     * 标记是那一次提交的审查记录
     */
    @Column(name = "EXAMINE_COUNT")
    private Integer examineCount;
    /**
     * 标记是否通过    0 通过  1不通过
     */
    @Column(name = "IS_PSAS")
    private Integer isPass;

    //状态  0已处理  1未处理
    @Column(name = "STATE")
    private Integer state;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLeTaskId() {
        return leTaskId;
    }

    public void setLeTaskId(Integer leTaskId) {
        this.leTaskId = leTaskId;
    }

    public Integer getExaminePointId() {
        return examinePointId;
    }

    public void setExaminePointId(Integer examinePointId) {
        this.examinePointId = examinePointId;
    }

    public String getExamineName() {
        return examineName;
    }

    public void setExamineName(String examineName) {
        this.examineName = examineName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getExamineCount() {
        return examineCount;
    }

    public void setExamineCount(Integer examineCount) {
        this.examineCount = examineCount;
    }

    public Integer getIsPass() {
        return isPass;
    }

    public void setIsPass(Integer isPass) {
        this.isPass = isPass;
    }

    public String getExamineType() {
        return examineType;
    }

    public void setExamineType(String examineType) {
        this.examineType = examineType;
    }
}
