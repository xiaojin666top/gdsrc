package com.beawan.corporateloan.entity;


import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 合规审查文档
 * 该表就3条数据
 */
@Entity
@Table(name = "LC_DOC", schema = "GDTCESYS")
public class LcDoc extends BaseEntity {

    @Id
    @Column(name = "ID")
    private Integer id;

    @Column(name = "TYPE")
    private String type;//类型  remit -->职责与权限  work   -->工作内容  reviewPoint --》审查要点
    @Column(name = "DOC_INFO")
    private String docInfo;//文档内容
    @Column(name = "NO_HTML_DOC")
    private String noHtmlDoc;//去掉html标签的文档

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDocInfo() {
        return docInfo;
    }

    public void setDocInfo(String docInfo) {
        this.docInfo = docInfo;
    }

    public String getNoHtmlDoc() {
        return noHtmlDoc;
    }

    public void setNoHtmlDoc(String noHtmlDoc) {
        this.noHtmlDoc = noHtmlDoc;
    }
}
