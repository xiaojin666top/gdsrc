package com.beawan.corporateloan.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 审查工作内容记录
 */

@Entity
@Table(name = "LC_REVIEW_CONTENT",schema = "GDTCESYS")
public class LcReviewContent extends BaseEntity {

    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LC_REVIEW_CONTENT_SEQ")
  //  @SequenceGenerator(name="LC_REVIEW_CONTENT_SEQ",allocationSize=1,initialValue=1, sequenceName="LC_REVIEW_CONTENT_SEQ")
    private Integer id;

    @Column(name = "lc_task_id")
    private Integer lcTaskId;//合规审查任务号主键id

    @Column(name = "RISK_ID")
    private Integer riskId;

    @Column(name = "risk_descr")
    private String riskDescr;

    @Column(name = "IS_PASS")
    private Integer isPass;

    @Column(name = "STATE")
    private Integer state;

    //状态  0已处理  1未整改   2 部分整改
    @Column(name = "RECTIFY")
    private Integer rectify;

    @Column(name = "LC_COUNT")
    private Integer lcCount;


    public LcReviewContent() {
    }

    public LcReviewContent(Integer lcTaskId, Integer riskId, String riskDescr) {
        this.lcTaskId = lcTaskId;
        this.riskId = riskId;
        this.riskDescr = riskDescr;
    }

    public LcReviewContent(Integer lcTaskId, Integer riskId, String riskDescr, Integer state) {
        this.lcTaskId = lcTaskId;
        this.riskId = riskId;
        this.riskDescr = riskDescr;
        this.state = state;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRiskId() {
        return riskId;
    }

    public void setRiskId(Integer riskId) {
        this.riskId = riskId;
    }

    public String getRiskDescr() {
        return riskDescr;
    }

    public void setRiskDescr(String riskDescr) {
        this.riskDescr = riskDescr;
    }

    public Integer getLcTaskId() {
        return lcTaskId;
    }

    public void setLcTaskId(Integer lcTaskId) {
        this.lcTaskId = lcTaskId;
    }

    public Integer getIsPass() {
        return isPass;
    }

    public void setIsPass(Integer isPass) {
        this.isPass = isPass;
    }

    public Integer getLcCount() {
        return lcCount;
    }

    public void setLcCount(Integer lcCount) {
        this.lcCount = lcCount;
    }

    public Integer getRectify() {
        return rectify;
    }

    public void setRectify(Integer rectify) {
        this.rectify = rectify;
    }
}
