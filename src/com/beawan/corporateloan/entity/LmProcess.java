package com.beawan.corporateloan.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;

/**
 * 贷款业务主表
 */
@Entity
@Table(name = "LM_PROCESS",schema = "GDTCESYS")
public class LmProcess extends BaseEntity {
	@Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LM_PROCESS_SEQ")
    //@SequenceGenerator(name="LM_PROCESS_SEQ",allocationSize=1,initialValue=1, sequenceName="LM_PROCESS_SEQ")
    private Integer id;
	@Column(name = "CUSTOMER_NO")
    private String customerNo;//客户编号
	@Column(name = "SERIAL_NUMBER")
    private String serialNumber;//业务流水号
	@Column(name = "STAGE")
    private Integer stage;//所处节点
	@Column(name = "POSITION_USER")
	private String positionUser;//当前节点操作人，使用逗号分隔
	@Column(name = "POSITION_USER_NAME")
	private String positionUserName;//当前节点操作人名字，使用逗号分隔
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Integer getStage() {
		return stage;
	}

	public void setStage(Integer stage) {
		this.stage = stage;
	}

	public String getPositionUser() {
		return positionUser;
	}

	public void setPositionUser(String positionUser) {
		this.positionUser = positionUser;
	}

	public String getPositionUserName() {
		return positionUserName;
	}

	public void setPositionUserName(String positionUserName) {
		this.positionUserName = positionUserName;
	}

}
