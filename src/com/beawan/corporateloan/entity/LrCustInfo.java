package com.beawan.corporateloan.entity;


import java.sql.Timestamp;

import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;

/**
 * 
 * @ClassName: LfCustInfo
 * @Description: TODO(营销客户信息)
 * @author xyh
 * @date 19 Jun 2020
 *
 */
@Entity
@Table(name = "LR_CUST_INFO",schema = "GDTCESYS")
public class LrCustInfo extends BaseEntity {
	@Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LR_CUST_INFO_SEQ")
   // @SequenceGenerator(name="LR_CUST_INFO_SEQ",allocationSize=1,initialValue=1, sequenceName="LR_CUST_INFO_SEQ")
    private Integer id;
	@Column(name = "CUSTOMER_NO")
    private String customerNo;//客户编号
	@Column(name = "CUSTOMER_CLASS")
    private Integer customerClass;//客户分类:0无效客户,1潜在客户,2机会客户,3意向客户,4成功客户
	@Column(name = "CUSTOMER_LR_TYPE")
	private String customerLrType;//-1:黑名单客户,0:公共池客户,1:营销客户
	//营销客户特有
	@Column(name = "CUST_ADDR")
	private String custAddr;//所在街道
	@Column(name = "EXPIRE_STATUS")
    private Integer expireStatus;//营销管户关系是否稳定 营销成功就永远是当前客户经理管户。若失败会过期。0营销成功 1还未营销成功
	@Column(name = "EXPIRE_TIME")
    private Timestamp expireTime;//过期时间--如果不会过期为空
	@Column(name = "HOLD_USER_NO")
	private String holdUserNo;//营销客户经理编号
	
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public Integer getCustomerClass() {
        return customerClass;
    }

    public void setCustomerClass(Integer customerClass) {
        this.customerClass = customerClass;
    }

	public String getCustomerLrType() {
		return customerLrType;
	}

	public void setCustomerLrType(String customerLrType) {
		this.customerLrType = customerLrType;
	}

	public String getCustAddr() {
		return custAddr;
	}

	public void setCustAddr(String custAddr) {
		this.custAddr = custAddr;
	}

	public Integer getExpireStatus() {
		return expireStatus;
	}

	public void setExpireStatus(Integer expireStatus) {
		this.expireStatus = expireStatus;
	}

	public Timestamp getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(Timestamp expireTime) {
		this.expireTime = expireTime;
	}

	public String getHoldUserNo() {
		return holdUserNo;
	}

	public void setHoldUserNo(String holdUserNo) {
		this.holdUserNo = holdUserNo;
	}

}
