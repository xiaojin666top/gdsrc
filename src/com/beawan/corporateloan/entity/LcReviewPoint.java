package com.beawan.corporateloan.entity;

import com.beawan.core.BaseEntity;

import javax.persistence.*;


/**
 * 合规审查内容说明表
 */
@Entity
@Table(name = "LC_REVIEW_POINT",schema = "GDTCESYS")
public class LcReviewPoint extends BaseEntity {

    @Id
    //@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LC_REVIEW_POINT_SEQ")
   // @SequenceGenerator(name="LC_REVIEW_POINT_SEQ",allocationSize=1,initialValue=1, sequenceName="LC_REVIEW_POINT_SEQ")
    private Integer id;

    @Column(name = "REVIEW_NAME")
    private String reviewName;

    @Column(name = "REVIEW_CONTENT")
    private String reviewContent;

    @Column(name = "INDU")
    private String indu;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getReviewName() {
        return reviewName;
    }

    public void setReviewName(String reviewName) {
        this.reviewName = reviewName;
    }

    public String getReviewContent() {
        return reviewContent;
    }

    public void setReviewContent(String reviewContent) {
        this.reviewContent = reviewContent;
    }

    public String getIndu() {
        return indu;
    }

    public void setIndu(String indu) {
        this.indu = indu;
    }
}
