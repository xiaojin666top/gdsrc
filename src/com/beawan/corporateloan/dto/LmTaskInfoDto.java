package com.beawan.corporateloan.dto;//企业贷款

import java.sql.Timestamp;

/**
 *
 * @ClassName: LmTaskInfo
 * @Description: TODO(贷款任务信息表)
 * @author xyh
 * @date 17 Jun 2020
 *
 */
public class LmTaskInfoDto {
	
	private String serialNumber;//业务流水号
	private String userNo;//当前查询节点用户编号---营销经理/贷前经理/合规审查人
	private String userName;//当前查询节点用户姓名---营销经理/贷前经理/合规审查人
	private String customerNo;//客户编号
	private String customerName;//客户名字
	private Integer stage;//节点信息
	private Timestamp updateTime;//更新时间
	private String lrUserName;//营销客户经理名称
	private String lrUpdateTime;//最新营销时间
	private String lrCustomerClass;//这次拜访后的客户分层分类
	
	
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getUserNo() {
		return userNo;
	}
	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Integer getStage() {
		return stage;
	}
	public void setStage(Integer stage) {
		this.stage = stage;
	}
	public Timestamp getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public String getLrUserName() {
		return lrUserName;
	}

	public void setLrUserName(String lrUserName) {
		this.lrUserName = lrUserName;
	}

	public String getLrUpdateTime() {
		return lrUpdateTime;
	}

	public void setLrUpdateTime(String lrUpdateTime) {
		this.lrUpdateTime = lrUpdateTime;
	}

	public String getLrCustomerClass() {
		return lrCustomerClass;
	}

	public void setLrCustomerClass(String lrCustomerClass) {
		this.lrCustomerClass = lrCustomerClass;
	}
}
