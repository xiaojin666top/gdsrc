package com.beawan.corporateloan.dto;


import com.beawan.core.BaseEntity;

import java.sql.Timestamp;

/**
 * 审查任务类数据集合
 */
public class LeTaskDto extends BaseEntity {

    private Integer leTaskId;//这里只是命名出了点错误，暂时不修改，取的是LeTask的id
    private String serialNumber;
    private String userNo;//审查人编号
    private String userName;//审查人名字
    private String customerNo;
    private String customerName;
    private String lrUserNo;//营销客户经理号
    private String lbUserNo;//贷前调查客户经理号
    private String lbUserName;//贷前调查客户经理名字
    private String guarantor;//担保人
    private Integer businessType;//业务类型
    private Double loanNumber;//授信金额
    private Integer loanTerm;//期限
    private Double stockNumber;//存量金额
    private String loanUse;//贷款用途
    private String assureMeans;//担保方式
    private Float loanRate;//利率
    private Timestamp submissionTime;//送审时间
    private Timestamp creditExpireDate;//最近一笔授信到期日
    private Integer stage;//所处节点



    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getLrUserNo() {
        return lrUserNo;
    }

    public void setLrUserNo(String lrUserNo) {
        this.lrUserNo = lrUserNo;
    }

    public String getLbUserNo() {
        return lbUserNo;
    }

    public void setLbUserNo(String lbUserNo) {
        this.lbUserNo = lbUserNo;
    }

    public String getLbUserName() {
        return lbUserName;
    }

    public void setLbUserName(String lbUserName) {
        this.lbUserName = lbUserName;
    }

    public String getGuarantor() {
        return guarantor;
    }

    public void setGuarantor(String guarantor) {
        this.guarantor = guarantor;
    }

    public Integer getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Integer businessType) {
        this.businessType = businessType;
    }

    public Double getLoanNumber() {
        return loanNumber;
    }

    public void setLoanNumber(Double loanNumber) {
        this.loanNumber = loanNumber;
    }

    public Integer getLoanTerm() {
        return loanTerm;
    }

    public void setLoanTerm(Integer loanTerm) {
        this.loanTerm = loanTerm;
    }

    public Double getStockNumber() {
        return stockNumber;
    }

    public void setStockNumber(Double stockNumber) {
        this.stockNumber = stockNumber;
    }

    public String getLoanUse() {
        return loanUse;
    }

    public void setLoanUse(String loanUse) {
        this.loanUse = loanUse;
    }

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    public Float getLoanRate() {
        return loanRate;
    }

    public void setLoanRate(Float loanRate) {
        this.loanRate = loanRate;
    }

    public Timestamp getSubmissionTime() {
        return submissionTime;
    }

    public void setSubmissionTime(Timestamp submissionTime) {
        this.submissionTime = submissionTime;
    }

    public Timestamp getCreditExpireDate() {
        return creditExpireDate;
    }

    public void setCreditExpireDate(Timestamp creditExpireDate) {
        this.creditExpireDate = creditExpireDate;
    }

    public Integer getLeTaskId() {
		return leTaskId;
	}

	public void setLeTaskId(Integer leTaskId) {
		this.leTaskId = leTaskId;
	}

	public Integer getStage() {
        return stage;
    }

    public void setStage(Integer stage) {
        this.stage = stage;
    }
}
