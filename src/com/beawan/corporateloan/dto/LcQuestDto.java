package com.beawan.corporateloan.dto;

/**
 * 合规审查报告中--》
 * 问题整改情况 数据对象
 */
public class LcQuestDto {


    private Integer riskId;
    private String riskInfo;
    private Integer yearAppear;//今年以来频次
    private Integer yearFix;//今年以来整改
    private Integer monthAppear;//本月频次
    private Integer monthFix;//本月整改

    public Integer getRiskId() {
        return riskId;
    }

    public void setRiskId(Integer riskId) {
        this.riskId = riskId;
    }

    public String getRiskInfo() {
        return riskInfo;
    }

    public void setRiskInfo(String riskInfo) {
        this.riskInfo = riskInfo;
    }

    public Integer getYearAppear() {
        return yearAppear;
    }

    public void setYearAppear(Integer yearAppear) {
        this.yearAppear = yearAppear;
    }

    public Integer getYearFix() {
        return yearFix;
    }

    public void setYearFix(Integer yearFix) {
        this.yearFix = yearFix;
    }

    public Integer getMonthAppear() {
        return monthAppear;
    }

    public void setMonthAppear(Integer monthAppear) {
        this.monthAppear = monthAppear;
    }

    public Integer getMonthFix() {
        return monthFix;
    }

    public void setMonthFix(Integer monthFix) {
        this.monthFix = monthFix;
    }
}
