package com.beawan.corporateloan.dto;

import com.beawan.survey.guarantee.entity.CompFinanceInfo;
import java.util.List;

/**
 * 贷前 担保企业信息
 * @author zxh
 * @date 2020/7/21 16:05
 */
public class LbGuaCompany {

    private Integer id;// 担保企业ID
    private List<CompFinanceInfo> financeInfos;// 担保企业财务状况
    private String companyName;// 担保企业
    private String loanCardInfo;// 贷款卡年检情况
    private Double guaranteeAmt;// 拟担保金额
    private String guaranteeWay;// 担保方式
    private String foundDate;// 成立日期
    private String dueDate;// 到期日期
    private Double regCapital; // 注册资本
    private Double realCapital;// 实收资本
    private String legalPerson;// 法定代表人
    private String controlPerson;// 实际控制人
    private String controlPersonInfo;// 实际控制人情况介绍
    private String address;// 经营或注册地址
    private String mainBusiness;// 主营业务
    private String equityInfo;// 股权结构
    private String runInfo;// 经营状况
    private String financeGuaInfo;// 融资及对外担保情况
    private String creditRecord; // 征信记录：有无不良记录
    private String badEffect;// 不良征信记录影响程度
    private String relation; // 与借款人关系
    private String isOfferGua; // 是否提供连带责任保证
    private String remark;// 分析评价


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<CompFinanceInfo> getFinanceInfos() {
        return financeInfos;
    }

    public void setFinanceInfos(List<CompFinanceInfo> financeInfos) {
        this.financeInfos = financeInfos;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getLoanCardInfo() {
        return loanCardInfo;
    }

    public void setLoanCardInfo(String loanCardInfo) {
        this.loanCardInfo = loanCardInfo;
    }

    public Double getGuaranteeAmt() {
        return guaranteeAmt;
    }

    public void setGuaranteeAmt(Double guaranteeAmt) {
        this.guaranteeAmt = guaranteeAmt;
    }

    public String getGuaranteeWay() {
        return guaranteeWay;
    }

    public void setGuaranteeWay(String guaranteeWay) {
        this.guaranteeWay = guaranteeWay;
    }

    public String getFoundDate() {
        return foundDate;
    }

    public void setFoundDate(String foundDate) {
        this.foundDate = foundDate;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public Double getRegCapital() {
        return regCapital;
    }

    public void setRegCapital(Double regCapital) {
        this.regCapital = regCapital;
    }

    public Double getRealCapital() {
        return realCapital;
    }

    public void setRealCapital(Double realCapital) {
        this.realCapital = realCapital;
    }

    public String getLegalPerson() {
        return legalPerson;
    }

    public void setLegalPerson(String legalPerson) {
        this.legalPerson = legalPerson;
    }

    public String getControlPerson() {
        return controlPerson;
    }

    public void setControlPerson(String controlPerson) {
        this.controlPerson = controlPerson;
    }

    public String getControlPersonInfo() {
        return controlPersonInfo;
    }

    public void setControlPersonInfo(String controlPersonInfo) {
        this.controlPersonInfo = controlPersonInfo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMainBusiness() {
        return mainBusiness;
    }

    public void setMainBusiness(String mainBusiness) {
        this.mainBusiness = mainBusiness;
    }

    public String getEquityInfo() {
        return equityInfo;
    }

    public void setEquityInfo(String equityInfo) {
        this.equityInfo = equityInfo;
    }

    public String getRunInfo() {
        return runInfo;
    }

    public void setRunInfo(String runInfo) {
        this.runInfo = runInfo;
    }

    public String getFinanceGuaInfo() {
        return financeGuaInfo;
    }

    public void setFinanceGuaInfo(String financeGuaInfo) {
        this.financeGuaInfo = financeGuaInfo;
    }

    public String getCreditRecord() {
        return creditRecord;
    }

    public void setCreditRecord(String creditRecord) {
        this.creditRecord = creditRecord;
    }

    public String getBadEffect() {
        return badEffect;
    }

    public void setBadEffect(String badEffect) {
        this.badEffect = badEffect;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getIsOfferGua() {
        return isOfferGua;
    }

    public void setIsOfferGua(String isOfferGua) {
        this.isOfferGua = isOfferGua;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
