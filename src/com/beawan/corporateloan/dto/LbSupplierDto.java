package com.beawan.corporateloan.dto;

import javax.persistence.Column;

/**
 * 采购环节 主要供应商DTO
 * @author zxh
 * @date 2020/7/20 17:59
 */
public class LbSupplierDto {

    private String supplyName;//上游供应商
    private String stuffName;//采购主要原材料名称
    private Double lastPurchaseAmt;//上年采购额
    private Double currPurchaseAmt;//本年当期采购额
    private String settleWay;//结算方式
    private Integer accountPeriod;//账期
    private Double currAmtRate;//占总成本比重

    public String getSupplyName() {
        return supplyName;
    }

    public void setSupplyName(String supplyName) {
        this.supplyName = supplyName;
    }

    public String getStuffName() {
        return stuffName;
    }

    public void setStuffName(String stuffName) {
        this.stuffName = stuffName;
    }

    public Double getLastPurchaseAmt() {
        return lastPurchaseAmt;
    }

    public void setLastPurchaseAmt(Double lastPurchaseAmt) {
        this.lastPurchaseAmt = lastPurchaseAmt;
    }

    public Double getCurrPurchaseAmt() {
        return currPurchaseAmt;
    }

    public void setCurrPurchaseAmt(Double currPurchaseAmt) {
        this.currPurchaseAmt = currPurchaseAmt;
    }

    public String getSettleWay() {
        return settleWay;
    }

    public void setSettleWay(String settleWay) {
        this.settleWay = settleWay;
    }

    public Integer getAccountPeriod() {
        return accountPeriod;
    }

    public void setAccountPeriod(Integer accountPeriod) {
        this.accountPeriod = accountPeriod;
    }

    public Double getCurrAmtRate() {
        return currAmtRate;
    }

    public void setCurrAmtRate(Double currAmtRate) {
        this.currAmtRate = currAmtRate;
    }
}
