package com.beawan.corporateloan.dto;

/**
 * 贷前报告 法定代表人/实际控制人 DTO
 * @author zxh
 * @date 2020/7/7 15:24
 */
public class LbManagerInfoDto {

    private String name;//高管姓名
    private String age;//年龄
    private String birthday;//出生日期
    private String birthPlace;//籍贯
    private String managerPost;//高管职务
    private String maritalStatus; //婚姻状况
    private String education; //学历
    private String livingPlace; //居住地
    private String workExperience; //从业经历
    private String creditInfo; //信用记录
    private String overallMerit;//综合评价
    private String remark;//其他说明

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getManagerPost() {
        return managerPost;
    }

    public void setManagerPost(String managerPost) {
        this.managerPost = managerPost;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getLivingPlace() {
        return livingPlace;
    }

    public void setLivingPlace(String livingPlace) {
        this.livingPlace = livingPlace;
    }

    public String getWorkExperience() {
        return workExperience;
    }

    public void setWorkExperience(String workExperience) {
        this.workExperience = workExperience;
    }

    public String getCreditInfo() {
        return creditInfo;
    }

    public void setCreditInfo(String creditInfo) {
        this.creditInfo = creditInfo;
    }

    public String getOverallMerit() {
        return overallMerit;
    }

    public void setOverallMerit(String overallMerit) {
        this.overallMerit = overallMerit;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
