package com.beawan.corporateloan.dto;

import java.sql.Timestamp;

/**
 * 营销结果信息
 */
public class LrResultDto {

    private Integer id;
    private String serNo;//流水号
    private String userNo;
    private String userName;
    private String customerNo;
    private String customerName;
    private Integer customerClass;//客户分类情况
    private Integer retailWay;//拜访方式 0微信  1电话  2实地
    private Timestamp retailTime;//拜访时间
    private String retailAddr;//拜访地址
    private String latLong;
    private String retailProduct;//产品
    private Integer retailTerm;//期限
    private Double retailMoney;//额度
    private Float retailRate;//利率
    private String retailUse;//用途
    private String retailGuarante;//担保方式
    private String visitRecord;//拜访内容说明
    private Integer retailResult; //营销结果  0暂存  1营销结束（失败）  2营销进件（成功）
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSerNo() {
		return serNo;
	}
	public void setSerNo(String serNo) {
		this.serNo = serNo;
	}
	public String getUserNo() {
		return userNo;
	}
	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Integer getCustomerClass() {
		return customerClass;
	}
	public void setCustomerClass(Integer customerClass) {
		this.customerClass = customerClass;
	}
	public Integer getRetailWay() {
		return retailWay;
	}
	public void setRetailWay(Integer retailWay) {
		this.retailWay = retailWay;
	}
	public Timestamp getRetailTime() {
		return retailTime;
	}
	public void setRetailTime(Timestamp retailTime) {
		this.retailTime = retailTime;
	}
	public String getRetailAddr() {
		return retailAddr;
	}
	public void setRetailAddr(String retailAddr) {
		this.retailAddr = retailAddr;
	}
	public String getLatLong() {
		return latLong;
	}
	public void setLatLong(String latLong) {
		this.latLong = latLong;
	}
	public String getRetailProduct() {
		return retailProduct;
	}
	public void setRetailProduct(String retailProduct) {
		this.retailProduct = retailProduct;
	}
	public Integer getRetailTerm() {
		return retailTerm;
	}
	public void setRetailTerm(Integer retailTerm) {
		this.retailTerm = retailTerm;
	}
	public Double getRetailMoney() {
		return retailMoney;
	}
	public void setRetailMoney(Double retailMoney) {
		this.retailMoney = retailMoney;
	}
	public Float getRetailRate() {
		return retailRate;
	}
	public void setRetailRate(Float retailRate) {
		this.retailRate = retailRate;
	}
	public String getRetailUse() {
		return retailUse;
	}
	public void setRetailUse(String retailUse) {
		this.retailUse = retailUse;
	}
	public String getRetailGuarante() {
		return retailGuarante;
	}
	public void setRetailGuarante(String retailGuarante) {
		this.retailGuarante = retailGuarante;
	}
	public String getVisitRecord() {
		return visitRecord;
	}
	public void setVisitRecord(String visitRecord) {
		this.visitRecord = visitRecord;
	}
	public Integer getRetailResult() {
		return retailResult;
	}
	public void setRetailResult(Integer retailResult) {
		this.retailResult = retailResult;
	}

   
}
