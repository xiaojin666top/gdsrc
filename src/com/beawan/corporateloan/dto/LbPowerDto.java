package com.beawan.corporateloan.dto;


/**
 * 贷前 生产环节 能耗详情
 * @author zxh
 * @date 2020/7/20 21:08
 */
public class LbPowerDto {

    private String year;// 年份
    private double electricityFee;// 电费
    private double otherFee;// 水/汽/煤等其他费用
    private double staffPay;// 员工工资
    private Integer staffNum;// 员工人数

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public double getElectricityFee() {
        return electricityFee;
    }

    public void setElectricityFee(double electricityFee) {
        this.electricityFee = electricityFee;
    }

    public double getOtherFee() {
        return otherFee;
    }

    public void setOtherFee(double otherFee) {
        this.otherFee = otherFee;
    }

    public double getStaffPay() {
        return staffPay;
    }

    public void setStaffPay(double staffPay) {
        this.staffPay = staffPay;
    }

    public Integer getStaffNum() {
        return staffNum;
    }

    public void setStaffNum(Integer staffNum) {
        this.staffNum = staffNum;
    }
}
