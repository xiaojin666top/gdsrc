package com.beawan.corporateloan.dto;


import com.beawan.core.BaseEntity;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

/**
 * 合规审查报告 数据传输对象
 */
public class LcReportDto {

    private String year;//统计年
    private String month;//统计月

    private String startTime;//结束时间
    private String endTime;//开始时间

    private LcMonthStatDto monthStatDto;//二、与上月对比：本月合规审查 基本统计
    private LcMonthStatDto lastMonthStatDto;//二、与上月对比：上月合规审查 基本统计
    private List<LcTaskRiskDto> riskDtoList;//三、今年以来涉及的风险点、频次及即时整改情况：
    private Map<String, List<LcTaskRiskDto>> taskRiskMap;//四、触及流程环节及业务领域的风险点：
    private Map<String, Map<String, List<LcTaskRiskDto>>> questMap;//五、存在的主要问题：


    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public LcMonthStatDto getMonthStatDto() {
        return monthStatDto;
    }

    public void setMonthStatDto(LcMonthStatDto monthStatDto) {
        this.monthStatDto = monthStatDto;
    }

    public LcMonthStatDto getLastMonthStatDto() {
        return lastMonthStatDto;
    }

    public void setLastMonthStatDto(LcMonthStatDto lastMonthStatDto) {
        this.lastMonthStatDto = lastMonthStatDto;
    }

    public Map<String, List<LcTaskRiskDto>> getTaskRiskMap() {
        return taskRiskMap;
    }

    public void setTaskRiskMap(Map<String, List<LcTaskRiskDto>> taskRiskMap) {
        this.taskRiskMap = taskRiskMap;
    }

    public List<LcTaskRiskDto> getRiskDtoList() {
        return riskDtoList;
    }

    public void setRiskDtoList(List<LcTaskRiskDto> riskDtoList) {
        this.riskDtoList = riskDtoList;
    }

    public Map<String, Map<String, List<LcTaskRiskDto>>> getQuestMap() {
        return questMap;
    }

    public void setQuestMap(Map<String, Map<String, List<LcTaskRiskDto>>> questMap) {
        this.questMap = questMap;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
