package com.beawan.corporateloan.dto;

/**
 * 贷前 产品销售详情DTO
 * @author zxh
 * @date 2020/7/21 10:10
 */
public class LbProductSales {

    private String productName;// 产品名称
    private String yearSalesVolume;// 年销售量（吨、套等）
    private double yearSales;// 年销售额（万元）
    private String mainCustomer;// 主要客户
    private String settleWay;//结算方式
    private String settleCycle;//结算周期

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getYearSalesVolume() {
        return yearSalesVolume;
    }

    public void setYearSalesVolume(String yearSalesVolume) {
        this.yearSalesVolume = yearSalesVolume;
    }

    public double getYearSales() {
        return yearSales;
    }

    public void setYearSales(double yearSales) {
        this.yearSales = yearSales;
    }

    public String getMainCustomer() {
        return mainCustomer;
    }

    public void setMainCustomer(String mainCustomer) {
        this.mainCustomer = mainCustomer;
    }

    public String getSettleWay() {
        return settleWay;
    }

    public void setSettleWay(String settleWay) {
        this.settleWay = settleWay;
    }

    public String getSettleCycle() {
        return settleCycle;
    }

    public void setSettleCycle(String settleCycle) {
        this.settleCycle = settleCycle;
    }
}
