package com.beawan.corporateloan.dto;

import java.util.List;
import java.util.Map;

import com.beawan.analysis.finansis.bean.FnCash;
import com.beawan.analysis.finansis.bean.FnFixedAssetsBuilding;
import com.beawan.analysis.finansis.bean.FnFixedAssetsEquipment;
import com.beawan.analysis.finansis.bean.FnFixedAssetsLand;
import com.beawan.analysis.finansis.bean.FnInventory;
import com.beawan.analysis.finansis.bean.FnOtherPay;
import com.beawan.analysis.finansis.bean.FnOtherReceive;
import com.beawan.analysis.finansis.bean.FnPay;
import com.beawan.analysis.finansis.bean.FnReceive;
import com.beawan.analysis.finansis.dto.FinaRatioDto;
import com.beawan.survey.custInfo.bean.CompFinance;
import com.beawan.survey.custInfo.bean.CompFinanceTax;
import com.beawan.survey.custInfo.bean.CompRunArea;
import com.beawan.survey.custInfo.entity.CompContribute;
import com.beawan.survey.custInfo.entity.CompInnerManage;
import com.beawan.survey.guarantee.entity.GuaAccounts;
import com.beawan.survey.guarantee.entity.GuaAnalysis;
import com.beawan.survey.guarantee.entity.GuaChattel;
import com.beawan.survey.guarantee.entity.GuaDepositReceipt;
import com.beawan.survey.guarantee.entity.GuaEstate;
import com.beawan.survey.guarantee.entity.GuaPerson;
import com.beawan.survey.loanInfo.bean.ApplyInfo;
import com.beawan.survey.loanInfo.bean.ApplyScheme;
import com.beawan.survey.loanInfo.bean.RiskAnalysis;

/**
 * 贷前调查报告 DTO
 * @author zxh
 * @date 2020/7/6 17:41
 */
public class LbReportDto {

    private LbCompBaseDto compBase;//企业基本信息
    private List<CompRunArea> runAreas;//经营场所情况
    private List<LbManagerInfoDto> managerInfos;//高管信息
    private CompInnerManage manageInfo;//内部管理情况
    private List<LbLinkedCompDto> linkedComps;//关联企业信息
    private List<LbFinancingInfoDto> financingInfos;//企业融资信息
    private LbCompRunInfoDto compRunInfo; // 企业经营情况说明
    private List<LbSupplierDto> supplies; // 主要供应商
    private List<LbProductDto> products; // 产品详情
    private LbPowersDto powers; // 能耗详情
    private List<LbProductSales> productSales; // 产品销售详情
    private List<LbGuaCompany> guaCompanies;// 担保企业
    private List<GuaPerson> guaPerson;// 担保自然人
    private List<GuaEstate> guaEstates;// 房地产抵押
    private List<GuaChattel> guaChattels;// 动产抵押
    private List<GuaAccounts> guaAccounts;// 应收账款质押
    private List<GuaDepositReceipt> guaDepositReceipts; // 存单质押
    private GuaAnalysis guaAnalysis;// 抵、质押物担保 分析说明
    private List<LbGuaWayStat> guaWayStatList;// 担保方式统计
    private CompContribute contribute;// 综合贡献度
    private RiskAnalysis riskAnalysis;// 风险分析及防范

    private Map<String, Object> finaImportData;//财务简报数据
    private CompFinance compFinance;//财务分析部分的文字说明  包括总体概况、四力分析、现金流分析、纳税分析
    private CompFinanceTax financeTax;//财务税率
    private String[] taxDateList;//税率对应的年份
    private String[] years;//财报指标对应的年份
    private Map<String, Object> cashflowData;//现金流数据
    private Map<String, List<FinaRatioDto>> ratioMap;//四力分析比率指标
    private Map<String, Object> quotaMap;//流动资金测算表
    private ApplyInfo applyInfo;//授信申请理由
    private List<ApplyScheme> applySchemeList;//授信申请列表
    //重要财务科目 --》资产类
    private List<FnCash> cashList;//货币资金
    private List<FnInventory> inventoryList;//存货
    private List<FnReceive> receiveList; //应收账款
    private List<FnOtherReceive> otherReceiveList;//其他应收款
    //固定资产分三块
    private List<FnFixedAssetsLand> landList;//土地
    private List<FnFixedAssetsBuilding> buildingList;//房屋
    private List<FnFixedAssetsEquipment> equipmentList;//设备
    //负债类
    private List<FnPay> payList;//应付账款
    private List<FnOtherPay> otherPayList;//其他应付款
    private Map<String, String> errExplainMap;//各个重要指标说明信息

    Map<String, String> nowBanlanceData;//各个重要科目的账面值    key为中文科目名


    public LbCompBaseDto getCompBase() {
        return compBase;
    }

    public void setCompBase(LbCompBaseDto compBase) {
        this.compBase = compBase;
    }

    public List<CompRunArea> getRunAreas() {
        return runAreas;
    }

    public void setRunAreas(List<CompRunArea> runAreas) {
        this.runAreas = runAreas;
    }

    public List<LbManagerInfoDto> getManagerInfos() {
        return managerInfos;
    }

    public void setManagerInfos(List<LbManagerInfoDto> managerInfos) {
        this.managerInfos = managerInfos;
    }

    public List<LbLinkedCompDto> getLinkedComps() {
        return linkedComps;
    }

    public void setLinkedComps(List<LbLinkedCompDto> linkedComps) {
        this.linkedComps = linkedComps;
    }

    public List<LbFinancingInfoDto> getFinancingInfos() {
        return financingInfos;
    }

    public void setFinancingInfos(List<LbFinancingInfoDto> financingInfos) {
        this.financingInfos = financingInfos;
    }

    
    public CompInnerManage getManageInfo() {
        return manageInfo;
    }

    public void setManageInfo(CompInnerManage manageInfo) {
        this.manageInfo = manageInfo;
    }

    public LbCompRunInfoDto getCompRunInfo() {
        return compRunInfo;
    }

    public void setCompRunInfo(LbCompRunInfoDto compRunInfo) {
        this.compRunInfo = compRunInfo;
    }

    public List<LbSupplierDto> getSupplies() {
        return supplies;
    }

    public void setSupplies(List<LbSupplierDto> supplies) {
        this.supplies = supplies;
    }

    public List<LbProductDto> getProducts() {
        return products;
    }

    public void setProducts(List<LbProductDto> products) {
        this.products = products;
    }

    public LbPowersDto getPowers() {
        return powers;
    }

    public void setPowers(LbPowersDto powers) {
        this.powers = powers;
    }

    public List<LbProductSales> getProductSales() {
        return productSales;
    }

    public void setProductSales(List<LbProductSales> productSales) {
        this.productSales = productSales;
    }

    public List<LbGuaCompany> getGuaCompanies() {
        return guaCompanies;
    }

    public void setGuaCompanies(List<LbGuaCompany> guaCompanies) {
        this.guaCompanies = guaCompanies;
    }

    public List<GuaPerson> getGuaPerson() {
        return guaPerson;
    }

    public void setGuaPerson(List<GuaPerson> guaPerson) {
        this.guaPerson = guaPerson;
    }

    public List<GuaEstate> getGuaEstates() {
        return guaEstates;
    }

    public void setGuaEstates(List<GuaEstate> guaEstates) {
        this.guaEstates = guaEstates;
    }

    public List<GuaChattel> getGuaChattels() {
        return guaChattels;
    }

    public void setGuaChattels(List<GuaChattel> guaChattels) {
        this.guaChattels = guaChattels;
    }

    public List<GuaAccounts> getGuaAccounts() {
        return guaAccounts;
    }

    public void setGuaAccounts(List<GuaAccounts> guaAccounts) {
        this.guaAccounts = guaAccounts;
    }

    public List<GuaDepositReceipt> getGuaDepositReceipts() {
        return guaDepositReceipts;
    }

    public void setGuaDepositReceipts(List<GuaDepositReceipt> guaDepositReceipts) {
        this.guaDepositReceipts = guaDepositReceipts;
    }

    public GuaAnalysis getGuaAnalysis() {
        return guaAnalysis;
    }

    public void setGuaAnalysis(GuaAnalysis guaAnalysis) {
        this.guaAnalysis = guaAnalysis;
    }

    public Map<String, Object> getFinaImportData() {
        return finaImportData;
    }

    public CompFinance getCompFinance() {
        return compFinance;
    }

    public void setCompFinance(CompFinance compFinance) {
        this.compFinance = compFinance;
    }

    public void setFinaImportData(Map<String, Object> finaImportData) {
        this.finaImportData = finaImportData;
    }

    public CompFinanceTax getFinanceTax() {
        return financeTax;
    }

    public void setFinanceTax(CompFinanceTax financeTax) {
        this.financeTax = financeTax;
    }

    public String[] getYears() {
        return years;
    }

    public void setYears(String[] years) {
        this.years = years;
    }

    public String[] getTaxDateList() {
        return taxDateList;
    }

    public void setTaxDateList(String[] taxDateList) {
        this.taxDateList = taxDateList;
    }

    public Map<String, List<FinaRatioDto>> getRatioMap() {
        return ratioMap;
    }

    public void setRatioMap(Map<String, List<FinaRatioDto>> ratioMap) {
        this.ratioMap = ratioMap;
    }

    public Map<String, Object> getCashflowData() {
        return cashflowData;
    }

    public void setCashflowData(Map<String, Object> cashflowData) {
        this.cashflowData = cashflowData;
    }

    public Map<String, Object> getQuotaMap() {
        return quotaMap;
    }

    public void setQuotaMap(Map<String, Object> quotaMap) {
        this.quotaMap = quotaMap;
    }

    public ApplyInfo getApplyInfo() {
        return applyInfo;
    }

    public void setApplyInfo(ApplyInfo applyInfo) {
        this.applyInfo = applyInfo;
    }

    public List<ApplyScheme> getApplySchemeList() {
        return applySchemeList;
    }

    public void setApplySchemeList(List<ApplyScheme> applySchemeList) {
        this.applySchemeList = applySchemeList;
    }

    public List<FnCash> getCashList() {
        return cashList;
    }

    public void setCashList(List<FnCash> cashList) {
        this.cashList = cashList;
    }

    public List<FnInventory> getInventoryList() {
        return inventoryList;
    }

    public void setInventoryList(List<FnInventory> inventoryList) {
        this.inventoryList = inventoryList;
    }

    public List<FnReceive> getReceiveList() {
        return receiveList;
    }

    public void setReceiveList(List<FnReceive> receiveList) {
        this.receiveList = receiveList;
    }

    public List<FnOtherReceive> getOtherReceiveList() {
        return otherReceiveList;
    }

    public void setOtherReceiveList(List<FnOtherReceive> otherReceiveList) {
        this.otherReceiveList = otherReceiveList;
    }

    public List<FnFixedAssetsLand> getLandList() {
        return landList;
    }

    public void setLandList(List<FnFixedAssetsLand> landList) {
        this.landList = landList;
    }

    public List<FnFixedAssetsBuilding> getBuildingList() {
        return buildingList;
    }

    public void setBuildingList(List<FnFixedAssetsBuilding> buildingList) {
        this.buildingList = buildingList;
    }

    public List<FnFixedAssetsEquipment> getEquipmentList() {
        return equipmentList;
    }

    public void setEquipmentList(List<FnFixedAssetsEquipment> equipmentList) {
        this.equipmentList = equipmentList;
    }

    public List<FnPay> getPayList() {
        return payList;
    }

    public void setPayList(List<FnPay> payList) {
        this.payList = payList;
    }

    public List<FnOtherPay> getOtherPayList() {
        return otherPayList;
    }

    public void setOtherPayList(List<FnOtherPay> otherPayList) {
        this.otherPayList = otherPayList;
    }

    public Map<String, String> getErrExplainMap() {
        return errExplainMap;
    }

    public void setErrExplainMap(Map<String, String> errExplainMap) {
        this.errExplainMap = errExplainMap;
    }

    public Map<String, String> getNowBanlanceData() {
        return nowBanlanceData;
    }

    public void setNowBanlanceData(Map<String, String> nowBanlanceData) {
        this.nowBanlanceData = nowBanlanceData;
    }

    public List<LbGuaWayStat> getGuaWayStatList() {
        return guaWayStatList;
    }

    public void setGuaWayStatList(List<LbGuaWayStat> guaWayStatList) {
        this.guaWayStatList = guaWayStatList;
    }

    public CompContribute getContribute() {
        return contribute;
    }

    public void setContribute(CompContribute contribute) {
        this.contribute = contribute;
    }

    public RiskAnalysis getRiskAnalysis() {
        return riskAnalysis;
    }

    public void setRiskAnalysis(RiskAnalysis riskAnalysis) {
        this.riskAnalysis = riskAnalysis;
    }
}
