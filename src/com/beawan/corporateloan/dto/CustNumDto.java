package com.beawan.corporateloan.dto;

public class CustNumDto {
	
	private Long allNum=0l;		// 所有客户数量
	private Long invalidNum=0l; // 无效客户数量
	private Long leadNum=0l;	// 潜在客户数量
	private Long opporNum=0l;   // 机会客户数量
	private Long intendenNum=0l;// 意向客户数量
	private Long succNum=0l;	// 成功客户数量
	
	public Long getAllNum() {
		return allNum;
	}
	public void setAllNum(Long allNum) {
		this.allNum = allNum;
	}
	public Long getInvalidNum() {
		return invalidNum;
	}
	public void setInvalidNum(Long invalidNum) {
		this.invalidNum = invalidNum;
	}
	public Long getLeadNum() {
		return leadNum;
	}
	public void setLeadNum(Long leadNum) {
		this.leadNum = leadNum;
	}
	public Long getOpporNum() {
		return opporNum;
	}
	public void setOpporNum(Long opporNum) {
		this.opporNum = opporNum;
	}
	public Long getIntendenNum() {
		return intendenNum;
	}
	public void setIntendenNum(Long intendenNum) {
		this.intendenNum = intendenNum;
	}
	public Long getSuccNum() {
		return succNum;
	}
	public void setSuccNum(Long succNum) {
		this.succNum = succNum;
	}
	
	
}
