package com.beawan.corporateloan.dto;

/**
 * 贷前 生产环节 产品详情DTO
 * @author zxh
 * @date 2020/7/20 20:47
 */
public class LbProductDto {

    private String productName;// 产品名称
    private String designCapacity; // 设计产能
    private String actualOutput; // 实际产量
    private String produceCycle; // 生产周期
    private String produceWay; // 生产方式
    private String mainUse; // 主要用途

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDesignCapacity() {
        return designCapacity;
    }

    public void setDesignCapacity(String designCapacity) {
        this.designCapacity = designCapacity;
    }

    public String getActualOutput() {
        return actualOutput;
    }

    public void setActualOutput(String actualOutput) {
        this.actualOutput = actualOutput;
    }

    public String getProduceCycle() {
        return produceCycle;
    }

    public void setProduceCycle(String produceCycle) {
        this.produceCycle = produceCycle;
    }

    public String getProduceWay() {
        return produceWay;
    }

    public void setProduceWay(String produceWay) {
        this.produceWay = produceWay;
    }

    public String getMainUse() {
        return mainUse;
    }

    public void setMainUse(String mainUse) {
        this.mainUse = mainUse;
    }
}
