package com.beawan.corporateloan.dto;

/**
 * 贷前 企业经营情况分析DTO
 * @author zxh
 * @date 2020/7/20 16:10
 */
public class LbCompRunInfoDto {

    private String industry; //行业分类
    private String industryInfo;//行业基本情况简要分析
    private String environmentInfo;//环保达标情况
    private String supplierInfo;//主要供应商分析
    private String rawMaterialInfo;//主要原材料分析
    private String produceInfo;//企业生产情况分析
    private String productInfo;//企业产品情况分析
    private String productSalesInfo; //产品销售情况分析
    private String qualifyChangeInfo;//企业生产资质变化情况
    private String businessPlace;//经营场所情况
    private String businessPlaceLand;//经营场所情况(土地)
    private String businessPlacePlant;//经营场所情况(厂房)
    private String relatedCompanies;//关联企业情况
    private String supplyInfo;//供应情况说明
    
    
    public String getSupplyInfo() {
		return supplyInfo;
	}

	public void setSupplyInfo(String supplyInfo) {
		this.supplyInfo = supplyInfo;
	}

	public String getBusinessPlace() {
        return businessPlace;
    }

    public void setBusinessPlace(String businessPlace) {
        this.businessPlace = businessPlace;
    }

    public String getRelatedCompanies() {
        return relatedCompanies;
    }

    public void setRelatedCompanies(String relatedCompanies) {
        this.relatedCompanies = relatedCompanies;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getIndustryInfo() {
        return industryInfo;
    }

    public void setIndustryInfo(String industryInfo) {
        this.industryInfo = industryInfo;
    }
    
    public String getProductInfo() {
		return productInfo;
	}

	public void setProductInfo(String productInfo) {
		this.productInfo = productInfo;
	}

	public String getEnvironmentInfo() {
        return environmentInfo;
    }

    public void setEnvironmentInfo(String environmentInfo) {
        this.environmentInfo = environmentInfo;
    }

    public String getSupplierInfo() {
        return supplierInfo;
    }

    public void setSupplierInfo(String supplierInfo) {
        this.supplierInfo = supplierInfo;
    }

    public String getRawMaterialInfo() {
        return rawMaterialInfo;
    }

    public void setRawMaterialInfo(String rawMaterialInfo) {
        this.rawMaterialInfo = rawMaterialInfo;
    }

    public String getProduceInfo() {
        return produceInfo;
    }

    public void setProduceInfo(String produceInfo) {
        this.produceInfo = produceInfo;
    }

    public String getProductSalesInfo() {
        return productSalesInfo;
    }

    public void setProductSalesInfo(String productSalesInfo) {
        this.productSalesInfo = productSalesInfo;
    }

    public String getQualifyChangeInfo() {
        return qualifyChangeInfo;
    }

    public void setQualifyChangeInfo(String qualifyChangeInfo) {
        this.qualifyChangeInfo = qualifyChangeInfo;
    }

	public String getBusinessPlaceLand() {
		return businessPlaceLand;
	}

	public void setBusinessPlaceLand(String businessPlaceLand) {
		this.businessPlaceLand = businessPlaceLand;
	}

	public String getBusinessPlacePlant() {
		return businessPlacePlant;
	}

	public void setBusinessPlacePlant(String businessPlacePlant) {
		this.businessPlacePlant = businessPlacePlant;
	}
    
    
}
