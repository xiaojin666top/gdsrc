package com.beawan.corporateloan.dto;
/**
 * 
 * @Author: xyh
 * @Date: 2020年10月10日
 * @Description: 按支行，按客户经理的客户分层分类结果
 */
public class LrCustSubClassDto {
	private String organno;//编号
	private String organshortform;//简称
	private String userId;//客户经理编号
	private String userName;//客户经理姓名
	private Integer sum1;//无效客户
	private Integer sum2;//潜在客户
	private Integer sum3;//机会客户
	private Integer sum4;//意向客户
	private Integer sum5;//成功客户
	public String getOrganno() {
		return organno;
	}
	public void setOrganno(String organno) {
		this.organno = organno;
	}
	public String getOrganshortform() {
		return organshortform;
	}
	public void setOrganshortform(String organshortform) {
		this.organshortform = organshortform;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Integer getSum1() {
		return sum1;
	}
	public void setSum1(Integer sum1) {
		this.sum1 = sum1;
	}
	public Integer getSum2() {
		return sum2;
	}
	public void setSum2(Integer sum2) {
		this.sum2 = sum2;
	}
	public Integer getSum3() {
		return sum3;
	}
	public void setSum3(Integer sum3) {
		this.sum3 = sum3;
	}
	public Integer getSum4() {
		return sum4;
	}
	public void setSum4(Integer sum4) {
		this.sum4 = sum4;
	}
	public Integer getSum5() {
		return sum5;
	}
	public void setSum5(Integer sum5) {
		this.sum5 = sum5;
	}

}
