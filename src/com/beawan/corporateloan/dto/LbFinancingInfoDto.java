package com.beawan.corporateloan.dto;

/**
 * 贷前报告 企业融资情况DTO
 * @author zxh
 * @date 2020/7/8 15:11
 */
public class LbFinancingInfoDto {

    private String financialOrg; // 融资机构
    private String businessType; // 融资品种
    private Double creditAmt; // 授信额度
    private Double creditBal; // 融资余额
    private String guaranteeType; // 担保方式

    public String getFinancialOrg() {
        return financialOrg;
    }

    public void setFinancialOrg(String financialOrg) {
        this.financialOrg = financialOrg;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public Double getCreditAmt() {
        return creditAmt;
    }

    public void setCreditAmt(Double creditAmt) {
        this.creditAmt = creditAmt;
    }

    public Double getCreditBal() {
        return creditBal;
    }

    public void setCreditBal(Double creditBal) {
        this.creditBal = creditBal;
    }

    public String getGuaranteeType() {
        return guaranteeType;
    }

    public void setGuaranteeType(String guaranteeType) {
        this.guaranteeType = guaranteeType;
    }

}
