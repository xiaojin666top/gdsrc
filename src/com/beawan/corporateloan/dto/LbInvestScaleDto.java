package com.beawan.corporateloan.dto;

import com.beawan.survey.project.dto.SubProjectDto;

import java.util.List;

/**
 * 贷前 投资规模DTO（项目类）
 * @author zxh
 * @date 2020/8/8 15:00
 */
public class LbInvestScaleDto {

    private double planAmt; // 计划投资额
    private double actualAmt; // 实际投资额
    private String fundsSource; // 资金来源情况
    private String fundsUse; // 资金使用情况
    private String progressInfo; // 项目进展情况
    private String investmentInfo; // 项目投资规模情况
    private List<SubProjectDto> subProjectList; //子项目(名称、投资金额、备注)
    

    public double getPlanAmt() {
        return planAmt;
    }

    public void setPlanAmt(double planAmt) {
        this.planAmt = planAmt;
    }

    public double getActualAmt() {
        return actualAmt;
    }

    public void setActualAmt(double actualAmt) {
        this.actualAmt = actualAmt;
    }

    public String getFundsSource() {
        return fundsSource;
    }

    public void setFundsSource(String fundsSource) {
        this.fundsSource = fundsSource;
    }

    public String getFundsUse() {
        return fundsUse;
    }

    public void setFundsUse(String fundsUse) {
        this.fundsUse = fundsUse;
    }

    public String getProgressInfo() {
        return progressInfo;
    }

    public void setProgressInfo(String progressInfo) {
        this.progressInfo = progressInfo;
    }

    public List<SubProjectDto> getSubProjectList() {
        return subProjectList;
    }

    public void setSubProjectList(List<SubProjectDto> subProjectList) {
        this.subProjectList = subProjectList;
    }

	public String getInvestmentInfo() {
		return investmentInfo;
	}

	public void setInvestmentInfo(String investmentInfo) {
		this.investmentInfo = investmentInfo;
	}
    
}
