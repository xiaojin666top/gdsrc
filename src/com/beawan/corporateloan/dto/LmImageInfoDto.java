package com.beawan.corporateloan.dto;

import javax.persistence.Column;

public class LmImageInfoDto {

    private Integer id;             //核查 创建的风险点id
    private String serialNumber;   //流水号
    private String userNo;     //客户编号
    private String type;        //类型：100营销      200贷前    300审批   400贷后
    private String orgiName;   //原始名
    private String formatName;  //格式化后名称
    private String fileType;   //文件后缀类型
    private Long size;       //文件大小 K
    private String filePath;   //文件保存地址
    private String mappingPath;//查看文件映射路径
    private String directory;  //父级标签   基本信息/营业执照

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOrgiName() {
        return orgiName;
    }

    public void setOrgiName(String orgiName) {
        this.orgiName = orgiName;
    }

    public String getFormatName() {
        return formatName;
    }

    public void setFormatName(String formatName) {
        this.formatName = formatName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getMappingPath() {
        return mappingPath;
    }

    public void setMappingPath(String mappingPath) {
        this.mappingPath = mappingPath;
    }

    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }
}
