package com.beawan.corporateloan.dto;

import java.sql.Timestamp;

/**
 * 合规审查 任务汇总统计DTO
 * @author zxh
 * @date 2020/6/18 21:33
 */
public class LcTaskStatDto {

    private Integer lcTaskId;       // 任务主键
    private String lbUserName;      // 贷前客户经理
    private String customerName;    // 客户名称
    private String guarantor;       // 担保人
    private Integer businessType;   // 授信品种
    private Double loanNumber;      // 授信金额
    private Integer loanTerm;       // 授信期限
    private Double stockNumber;     // 存量金额
    private String loanUse;         // 贷款用途
    private String assureMeans;     // 担保方式
    private Float loanRate;         // 利率
    private Timestamp submissionTime;// 送审时间
    private Timestamp creditExpireDate;// 最近一笔授信到期日

    public Integer getLcTaskId() {
        return lcTaskId;
    }

    public void setLcTaskId(Integer lcTaskId) {
        this.lcTaskId = lcTaskId;
    }

    public String getLbUserName() {
        return lbUserName;
    }

    public void setLbUserName(String lbUserName) {
        this.lbUserName = lbUserName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getGuarantor() {
        return guarantor;
    }

    public void setGuarantor(String guarantor) {
        this.guarantor = guarantor;
    }

    public Integer getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Integer businessType) {
        this.businessType = businessType;
    }

    public Double getLoanNumber() {
        return loanNumber;
    }

    public void setLoanNumber(Double loanNumber) {
        this.loanNumber = loanNumber;
    }

    public Integer getLoanTerm() {
        return loanTerm;
    }

    public void setLoanTerm(Integer loanTerm) {
        this.loanTerm = loanTerm;
    }

    public Double getStockNumber() {
        return stockNumber;
    }

    public void setStockNumber(Double stockNumber) {
        this.stockNumber = stockNumber;
    }

    public String getLoanUse() {
        return loanUse;
    }

    public void setLoanUse(String loanUse) {
        this.loanUse = loanUse;
    }

    public String getAssureMeans() {
        return assureMeans;
    }

    public void setAssureMeans(String assureMeans) {
        this.assureMeans = assureMeans;
    }

    public Float getLoanRate() {
        return loanRate;
    }

    public void setLoanRate(Float loanRate) {
        this.loanRate = loanRate;
    }

    public Timestamp getSubmissionTime() {
        return submissionTime;
    }

    public void setSubmissionTime(Timestamp submissionTime) {
        this.submissionTime = submissionTime;
    }

    public Timestamp getCreditExpireDate() {
        return creditExpireDate;
    }

    public void setCreditExpireDate(Timestamp creditExpireDate) {
        this.creditExpireDate = creditExpireDate;
    }
}
