package com.beawan.corporateloan.dto;

/**
 * 贷前 对外担保信息
 * @author User
 *
 */
public class LbExternalGuaDto {

    private String guaranteeName; // 被担保企业
    private Double guaranteeAmt; // 担保金额
    private String financialOrg; // 融资机构
    private String antiGuaranteeWay; // 反担保方式
    private String relation; // 关联关系
    private String fiveClass;//五级分类

    public String getGuaranteeName() {
        return guaranteeName;
    }

    public void setGuaranteeName(String guaranteeName) {
        this.guaranteeName = guaranteeName;
    }

    public Double getGuaranteeAmt() {
        return guaranteeAmt;
    }

    public void setGuaranteeAmt(Double guaranteeAmt) {
        this.guaranteeAmt = guaranteeAmt;
    }

    public String getFinancialOrg() {
        return financialOrg;
    }

    public void setFinancialOrg(String financialOrg) {
        this.financialOrg = financialOrg;
    }

    public String getAntiGuaranteeWay() {
        return antiGuaranteeWay;
    }

    public void setAntiGuaranteeWay(String antiGuaranteeWay) {
        this.antiGuaranteeWay = antiGuaranteeWay;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

	public String getFiveClass() {
		return fiveClass;
	}

	public void setFiveClass(String fiveClass) {
		this.fiveClass = fiveClass;
	}
        
}
