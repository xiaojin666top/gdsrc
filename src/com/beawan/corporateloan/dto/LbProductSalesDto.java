package com.beawan.corporateloan.dto;

import com.beawan.survey.custInfo.bean.CompProductSales;

import java.util.List;

/**
 * 产品销售详情DTO (商贸类)
 * @author zxh
 * @date 2020/8/7 15:18
 */
public class LbProductSalesDto {

    List<CompProductSales> thisYear; // 今年
    List<CompProductSales> lastYear; // 去年

    public List<CompProductSales> getThisYear() {
        return thisYear;
    }

    public void setThisYear(List<CompProductSales> thisYear) {
        this.thisYear = thisYear;
    }

    public List<CompProductSales> getLastYear() {
        return lastYear;
    }

    public void setLastYear(List<CompProductSales> lastYear) {
        this.lastYear = lastYear;
    }
}
