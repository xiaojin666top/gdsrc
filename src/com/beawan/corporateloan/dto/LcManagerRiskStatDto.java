package com.beawan.corporateloan.dto;

/**
 * 合规审查 客户经理问题统计DTO
 * @author zxh
 * @date 2020/6/20 13:57
 */
public class LcManagerRiskStatDto {

    private String lbUserNo;      // 贷前客户经理编号
    private String lbUserName;    // 贷前客户经理姓名
    private Integer loanNum;      // 贷款户数/客户数
    private Double loanSum;       // 授信总额
    private Integer riskSum;      // 问题总数
    private String colorCate;     // 按问题数区分颜色

    public String getLbUserNo() {
        return lbUserNo;
    }

    public void setLbUserNo(String lbUserNo) {
        this.lbUserNo = lbUserNo;
    }

    public String getLbUserName() {
        return lbUserName;
    }

    public void setLbUserName(String lbUserName) {
        this.lbUserName = lbUserName;
    }

    public Integer getRiskSum() {
        return riskSum;
    }

    public void setRiskSum(Integer riskSum) {
        this.riskSum = riskSum;
    }

    public Integer getLoanNum() {
        return loanNum;
    }

    public void setLoanNum(Integer loanNum) {
        this.loanNum = loanNum;
    }

    public Double getLoanSum() {
        return loanSum;
    }

    public void setLoanSum(Double loanSum) {
        this.loanSum = loanSum;
    }

    public String getColorCate() {
        return colorCate;
    }

    public void setColorCate(String colorCate) {
        this.colorCate = colorCate;
    }
}
