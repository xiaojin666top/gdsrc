package com.beawan.corporateloan.dto;

import com.beawan.corporateloan.entity.LcReviewContent;
import com.beawan.corporateloan.entity.LcRiskDb;

import java.util.List;

/**
 *  根据审查项分类问题 即一对多的数据集合
 */
public class LcPointRiskDto {

    //审查内容
    private String reviewName;
    //当前审查内容下的问题点列表
    private List<LcRiskDb> riskList;

    private List<LcTaskRiskDto> contentList;

    public String getReviewName() {
        return reviewName;
    }

    public void setReviewName(String reviewName) {
        this.reviewName = reviewName;
    }

    public List<LcRiskDb> getRiskList() {
        return riskList;
    }

    public void setRiskList(List<LcRiskDb> riskList) {
        this.riskList = riskList;
    }

    public List<LcTaskRiskDto> getContentList() {
        return contentList;
    }

    public void setContentList(List<LcTaskRiskDto> contentList) {
        this.contentList = contentList;
    }
}
