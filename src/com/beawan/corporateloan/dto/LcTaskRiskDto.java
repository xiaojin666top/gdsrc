package com.beawan.corporateloan.dto;

public class LcTaskRiskDto {

    private Integer reviewContentId;    //核查 创建的风险点id
    private Integer taskId;
    private Integer riskId;     //风险点id
    private String riskInfo;    //风险点
    private Integer reviewId;   //审查点Id
    private String reviewName;  //审查内容
    private String riskDescr;   //问题描述
    private Integer isPass;		//是否通过  0通过  1不通过
    private Integer state;
    private Integer rectify;    //是否整改状态
    private Integer groupNum;//根据审查内容  问题点group by之后的统计数量
    private Integer groupFixNum;//根据审查内容  问题点group by之后的统计数量

    private Integer groupMonthNum;//月统计数
    private Integer groupMonthFixNum;//月整改数
    private Integer groupYearNum;//年统计数
    private Integer groupYearFixNum;//年整改数

    private String customerName;//客户名称
    private Integer businessType;//申请授信品种
    private Double loanNumber;//授信金额

    public Integer getGroupFixNum() {
        return groupFixNum;
    }

    public void setGroupFixNum(Integer groupFixNum) {
        this.groupFixNum = groupFixNum;
    }

    public Integer getReviewContentId() {
        return reviewContentId;
    }

    public void setReviewContentId(Integer reviewContentId) {
        this.reviewContentId = reviewContentId;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public Integer getRiskId() {
        return riskId;
    }

    public void setRiskId(Integer riskId) {
        this.riskId = riskId;
    }

    public String getRiskInfo() {
        return riskInfo;
    }

    public void setRiskInfo(String riskInfo) {
        this.riskInfo = riskInfo;
    }

    public Integer getReviewId() {
        return reviewId;
    }

    public void setReviewId(Integer reviewId) {
        this.reviewId = reviewId;
    }

    public String getReviewName() {
        return reviewName;
    }

    public void setReviewName(String reviewName) {
        this.reviewName = reviewName;
    }

    public String getRiskDescr() {
        return riskDescr;
    }

    public void setRiskDescr(String riskDescr) {
        this.riskDescr = riskDescr;
    }

    public Integer getIsPass() {
		return isPass;
	}

	public void setIsPass(Integer isPass) {
		this.isPass = isPass;
	}

	public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getGroupNum() {
        return groupNum;
    }

    public void setGroupNum(Integer groupNum) {
        this.groupNum = groupNum;
    }

    public Integer getGroupMonthNum() {
        return groupMonthNum;
    }

    public void setGroupMonthNum(Integer groupMonthNum) {
        this.groupMonthNum = groupMonthNum;
    }

    public Integer getGroupYearNum() {
        return groupYearNum;
    }

    public void setGroupYearNum(Integer groupYearNum) {
        this.groupYearNum = groupYearNum;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Integer getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Integer businessType) {
        this.businessType = businessType;
    }

    public Double getLoanNumber() {
        return loanNumber;
    }

    public Integer getGroupMonthFixNum() {
        return groupMonthFixNum;
    }

    public void setGroupMonthFixNum(Integer groupMonthFixNum) {
        this.groupMonthFixNum = groupMonthFixNum;
    }

    public Integer getGroupYearFixNum() {
        return groupYearFixNum;
    }

    public void setGroupYearFixNum(Integer groupYearFixNum) {
        this.groupYearFixNum = groupYearFixNum;
    }

    public void setLoanNumber(Double loanNumber) {
        this.loanNumber = loanNumber;
    }

    public Integer getRectify() {
        return rectify;
    }

    public void setRectify(Integer rectify) {
        this.rectify = rectify;
    }
    @Override
    public String toString(){
		return "LcTaskRiskDto [reviewContentId=" + reviewContentId + "isPass=" + isPass + "]";
    	
    }
//	@Override
//	public int compareTo(LcTaskRiskDto arg0) {
//		
//		return this.isPass.compareTo(arg0.getIsPass());
//	}
}
