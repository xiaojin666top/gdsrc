package com.beawan.corporateloan.dto;

import java.sql.Timestamp;

/**
 * 合规审查 问题统计DTO
 * @author zxh
 * @date 2020/6/19 16:55
 */
public class LcRiskStatDto {

    private Integer lcTaskId;       // 任务主键
    private String customerName;    // 客户名称
    private Integer businessType;   // 授信品种
    private Integer loanTerm;       // 授信期限
    private Double loanNumber;      // 授信金额
    private String loanUse;         // 贷款用途
    private String guarantor;       // 担保人
    private String lbUserName;      // 贷前客户经理
    private String riskInfo;        // 问题点
    private String riskDescr;       // 问题描述
    private Integer rectify;		// 整改情况
    private Integer state;          // 问题状态(0 已解决, 1 未解决)

    public Integer getLcTaskId() {
        return lcTaskId;
    }

    public void setLcTaskId(Integer lcTaskId) {
        this.lcTaskId = lcTaskId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Integer getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Integer businessType) {
        this.businessType = businessType;
    }

    public Integer getLoanTerm() {
        return loanTerm;
    }

    public void setLoanTerm(Integer loanTerm) {
        this.loanTerm = loanTerm;
    }

    public Double getLoanNumber() {
        return loanNumber;
    }

    public void setLoanNumber(Double loanNumber) {
        this.loanNumber = loanNumber;
    }

    public String getLoanUse() {
        return loanUse;
    }

    public void setLoanUse(String loanUse) {
        this.loanUse = loanUse;
    }

    public String getGuarantor() {
        return guarantor;
    }

    public void setGuarantor(String guarantor) {
        this.guarantor = guarantor;
    }

    public String getLbUserName() {
        return lbUserName;
    }

    public void setLbUserName(String lbUserName) {
        this.lbUserName = lbUserName;
    }

    public String getRiskInfo() {
        return riskInfo;
    }

    public void setRiskInfo(String riskInfo) {
        this.riskInfo = riskInfo;
    }

    public String getRiskDescr() {
        return riskDescr;
    }

    public void setRiskDescr(String riskDescr) {
        this.riskDescr = riskDescr;
    }

    public Integer getState() {
        return state;
    }
    
    
    public void setState(Integer state) {
        this.state = state;
    }

	public Integer getRectify() {
		return rectify;
	}

	public void setRectify(Integer rectify) {
		this.rectify = rectify;
	}
    
}
