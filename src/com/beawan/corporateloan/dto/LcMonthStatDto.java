package com.beawan.corporateloan.dto;

public class LcMonthStatDto {

    private Integer loanCount;//本月审查贷款户数
    private Double loanMoney;//本月贷款金额合计
    private Integer questNum;//本月发现问题总数
    private Integer fixQuestNum;//本月及时整改的问题数

    public Integer getLoanCount() {
        return loanCount;
    }

    public void setLoanCount(Integer loanCount) {
        this.loanCount = loanCount;
    }

    public Double getLoanMoney() {
        return loanMoney;
    }

    public void setLoanMoney(Double loanMoney) {
        this.loanMoney = loanMoney;
    }

    public Integer getQuestNum() {
        return questNum;
    }

    public void setQuestNum(Integer questNum) {
        this.questNum = questNum;
    }

    public Integer getFixQuestNum() {
        return fixQuestNum;
    }

    public void setFixQuestNum(Integer fixQuestNum) {
        this.fixQuestNum = fixQuestNum;
    }
}
