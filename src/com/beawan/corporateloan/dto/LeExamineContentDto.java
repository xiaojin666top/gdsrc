package com.beawan.corporateloan.dto;

import com.beawan.corporateloan.entity.LeExamineContent;

import java.util.List;

/**
 * @Author: xyh
 * @Date: 04/08/2020
 * @Description:
 */
public class LeExamineContentDto {

    private String examineTypeCN;
    private List<LeExamineContent> childExamine;

    public String getExamineTypeCN() {
        return examineTypeCN;
    }

    public void setExamineTypeCN(String examineTypeCN) {
        this.examineTypeCN = examineTypeCN;
    }

    public List<LeExamineContent> getChildExamine() {
        return childExamine;
    }

    public void setChildExamine(List<LeExamineContent> childExamine) {
        this.childExamine = childExamine;
    }
}
