package com.beawan.corporateloan.dto;

import com.platform.util.GsonUtil;

/**
 * 区域营销情况统计DTO
 */
public class LrAreaStatDto {

    private String address; // 街道乡镇
    private Double customerSum; // 客户总数
    private Double visitNum; // 客户拜访数
    private String coverageRate;// 覆盖率
    private Double retailSum; // 营销总次数
    private Double retailSuccessNum; // 营销成功次数
    private String conversionRate;// 转化率

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getCustomerSum() {
        return customerSum;
    }

    public void setCustomerSum(Double customerSum) {
        this.customerSum = customerSum;
    }

    public Double getVisitNum() {
        return visitNum;
    }

    public void setVisitNum(Double visitNum) {
        this.visitNum = visitNum;
    }

    public String getCoverageRate() {
        if (this.customerSum <= 0) return "0.00%";
        double rate = this.visitNum * 100 / this.customerSum;
        return String.format("%.2f", rate) + "%";
    }

    public void setCoverageRate(String coverageRate) {
        this.coverageRate = coverageRate;
    }

    public Double getRetailSum() {
        return retailSum;
    }

    public void setRetailSum(Double retailSum) {
        this.retailSum = retailSum;
    }

    public Double getRetailSuccessNum() {
        return retailSuccessNum;
    }

    public void setRetailSuccessNum(Double retailSuccessNum) {
        this.retailSuccessNum = retailSuccessNum;
    }

    public String getConversionRate() {
        if (this.retailSum <= 0) return "0.00%";
        double rate = this.retailSuccessNum * 100 / this.retailSum;
        return String.format("%.2f", rate) + "%";
    }

    public void setConversionRate(String conversionRate) {
        this.conversionRate = conversionRate;
    }
}
