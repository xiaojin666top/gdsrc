package com.beawan.corporateloan.dto;

import java.util.List;

/**
 * @author zxh
 * @date 2020/7/20 21:46
 */
public class LbPowersDto {

    private List<String> years; //年份
    private List<Double> electricityFee;// 电费
    private List<Double> otherFee;// 水/汽/煤等其他费用
    private List<Double> staffPay;// 员工工资
    private List<Integer> staffNum;// 员工人数

    public List<String> getYears() {
        return years;
    }

    public void setYears(List<String> years) {
        this.years = years;
    }

    public List<Double> getElectricityFee() {
        return electricityFee;
    }

    public void setElectricityFee(List<Double> electricityFee) {
        this.electricityFee = electricityFee;
    }

    public List<Double> getOtherFee() {
        return otherFee;
    }

    public void setOtherFee(List<Double> otherFee) {
        this.otherFee = otherFee;
    }

    public List<Double> getStaffPay() {
        return staffPay;
    }

    public void setStaffPay(List<Double> staffPay) {
        this.staffPay = staffPay;
    }

    public List<Integer> getStaffNum() {
        return staffNum;
    }

    public void setStaffNum(List<Integer> staffNum) {
        this.staffNum = staffNum;
    }
}
