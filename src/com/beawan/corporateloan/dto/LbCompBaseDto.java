package com.beawan.corporateloan.dto;

import java.util.List;

/**
 * 贷前报告 企业基本信息 DTO
 * @author zxh
 * @date 2020/7/6 20:55
 */
public class LbCompBaseDto {

    private String customerName;//企业名称
    private String registerAddress;//注册地址
    private String runAddress;//经营地址
    private String mainBusiness;//主营业务
    private String runScope;   //经营范围
    private String registerCapital;//注册资本
    private String rcCurrency;//注册资本币种
    private String pcCurrency;//实收资本币种
    private String realCapital;//实收资本
    private String legalPerson;//法定代表人
    private String legalWorkingYears;//法定代表人从业年限
    private String controlPerson;//实际控制人
    private String controlWorkingYears;//实际控制人从业年限
    private String foundDate;//成立日期
    private String creditLevel;//内部信用等级
    private String bankAccType;//本行账户性质
    private String isMbankHolder;//是否本行股东
    private Integer stockNum; // 持股数
    private String isAntiPledge;// 股权是否反质押
    private String creditRecord;//企业征信记录
    private List<String> equity;//股权结构
    private String otherInfo;//其他说明
    private String indusPolicy;// 行业政策
    private String taxGrade;//纳税等级
    private String eiaGrade;//环评信用等级
    private String isDischargePermit;//是否有排污许可证
    private String stockCustomer;//客户类别

    public Integer getStockNum() {
        return stockNum;
    }

    public void setStockNum(Integer stockNum) {
        this.stockNum = stockNum;
    }

    public String getIsAntiPledge() {
        return isAntiPledge;
    }

    public void setIsAntiPledge(String isAntiPledge) {
        this.isAntiPledge = isAntiPledge;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getRegisterAddress() {
        return registerAddress;
    }

    public void setRegisterAddress(String registerAddress) {
        this.registerAddress = registerAddress;
    }

    public String getRunAddress() {
        return runAddress;
    }

    public void setRunAddress(String runAddress) {
        this.runAddress = runAddress;
    }

    public String getRcCurrency() {
		return rcCurrency;
	}

	public void setRcCurrency(String rcCurrency) {
		this.rcCurrency = rcCurrency;
	}

	public String getPcCurrency() {
		return pcCurrency;
	}

	public void setPcCurrency(String pcCurrency) {
		this.pcCurrency = pcCurrency;
	}

	public String getMainBusiness() {
        return mainBusiness;
    }

    public void setMainBusiness(String mainBusiness) {
        this.mainBusiness = mainBusiness;
    }

    public String getRegisterCapital() {
        return registerCapital;
    }

    public void setRegisterCapital(String registerCapital) {
        this.registerCapital = registerCapital;
    }

    public String getRealCapital() {
        return realCapital;
    }

    public void setRealCapital(String realCapital) {
        this.realCapital = realCapital;
    }

    public String getRunScope() {
		return runScope;
	}

	public void setRunScope(String runScope) {
		this.runScope = runScope;
	}

	public String getLegalPerson() {
        return legalPerson;
    }

    public void setLegalPerson(String legalPerson) {
        this.legalPerson = legalPerson;
    }

    public String getLegalWorkingYears() {
        return legalWorkingYears;
    }

    public void setLegalWorkingYears(String legalWorkingYears) {
        this.legalWorkingYears = legalWorkingYears;
    }

    public String getControlPerson() {
        return controlPerson;
    }

    public void setControlPerson(String controlPerson) {
        this.controlPerson = controlPerson;
    }

    public String getControlWorkingYears() {
        return controlWorkingYears;
    }

    public void setControlWorkingYears(String controlWorkingYears) {
        this.controlWorkingYears = controlWorkingYears;
    }

    public String getFoundDate() {
        return foundDate;
    }

    public void setFoundDate(String foundDate) {
        this.foundDate = foundDate;
    }

    public String getCreditLevel() {
        return creditLevel;
    }

    public void setCreditLevel(String creditLevel) {
        this.creditLevel = creditLevel;
    }

    public String getBankAccType() {
        return bankAccType;
    }

    public void setBankAccType(String bankAccType) {
        this.bankAccType = bankAccType;
    }

    public String getIsMbankHolder() {
		return isMbankHolder;
	}

	public void setIsMbankHolder(String isMbankHolder) {
		this.isMbankHolder = isMbankHolder;
	}

	public String getCreditRecord() {
        return creditRecord;
    }

    public void setCreditRecord(String creditRecord) {
        this.creditRecord = creditRecord;
    }

    public List<String> getEquity() {
        return equity;
    }

    public void setEquity(List<String> equity) {
        this.equity = equity;
    }

    public String getOtherInfo() {
        return otherInfo;
    }

    public void setOtherInfo(String otherInfo) {
        this.otherInfo = otherInfo;
    }

	public String getIndusPolicy() {
		return indusPolicy;
	}

	public void setIndusPolicy(String indusPolicy) {
		this.indusPolicy = indusPolicy;
	}

	public String getTaxGrade() {
		return taxGrade;
	}

	public void setTaxGrade(String taxGrade) {
		this.taxGrade = taxGrade;
	}

	public String getEiaGrade() {
		return eiaGrade;
	}

	public void setEiaGrade(String eiaGrade) {
		this.eiaGrade = eiaGrade;
	}

	public String getIsDischargePermit() {
		return isDischargePermit;
	}

	public void setIsDischargePermit(String isDischargePermit) {
		this.isDischargePermit = isDischargePermit;
	}
	
	public String getStockCustomer() {
		return stockCustomer;
	}

	public void setStockCustomer(String stockCustomer) {
		this.stockCustomer = stockCustomer;
	}
	
}
