package com.beawan.corporateloan.dto;

import java.sql.Timestamp;

public class LrCustDto {

    private String customerNo;
    private String customerName;//客户名称
    private String customerType;//客户类型
    private String certType;//证件类型
    private String certCode;//证件号

    private String custAddr;//所在街道
    private Integer retail;//是否营销类型客户   0是  1否
    private String retailUser;//营销管户经理名称
    private Timestamp createTime;//开始管户开始时间
    private Integer importWay;//导入方式
    private Integer expireStatus;//是否会过期
    private String expireDate;
    private String userNo;
    private String userName;//管户客户经理名字
    private String blacklistReason;
    private String customerClass;

    public String getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(String customerNo) {
        this.customerNo = customerNo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getCustAddr() {
        return custAddr;
    }

    public void setCustAddr(String custAddr) {
        this.custAddr = custAddr;
    }

    public Integer getRetail() {
        return retail;
    }

    public void setRetail(Integer retail) {
        this.retail = retail;
    }

    public String getRetailUser() {
        return retailUser;
    }

    public void setRetailUser(String retailUser) {
        this.retailUser = retailUser;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Integer getImportWay() {
        return importWay;
    }

    public void setImportWay(Integer importWay) {
        this.importWay = importWay;
    }

    public Integer getExpireStatus() {
        return expireStatus;
    }

    public void setExpireStatus(Integer expireStatus) {
        this.expireStatus = expireStatus;
    }

    public String getUserNo() {
        return userNo;
    }

    public void setUserNo(String userNo) {
        this.userNo = userNo;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getBlacklistReason() {
        return blacklistReason;
    }

    public void setBlacklistReason(String blacklistReason) {
        this.blacklistReason = blacklistReason;
    }

    public String getCustomerClass() {
        return customerClass;
    }

    public void setCustomerClass(String customerClass) {
        this.customerClass = customerClass;
    }
}
