package com.beawan.corporateloan.dto;

/**
 * 客户分层分类数据统计
 */
public class LrCustClassDto {

    private Integer customerClassCode;//客户分类code
    private String customerClassName;//客户分类
    private Long num;//当前分类下的客户数量
    private String dataRange;//数据范围
    private Integer value;//这个字段至少给前端显示漏斗形状用的
    private String percent="0.00";//占比 2位小数保留

    public LrCustClassDto() {
    }

    public LrCustClassDto(String customerClassName, Integer value) {
        this.customerClassName = customerClassName;
        this.value = value;
    }

    public LrCustClassDto(String customerClassName, Long num, Integer value) {
        this.customerClassName = customerClassName;
        this.num = num;
        this.value = value;
    }

    public LrCustClassDto(String customerClassName, Long num, Integer value, String percent) {
        this.customerClassName = customerClassName;
        this.num = num;
        this.value = value;
        this.percent = percent;
    }

    public Integer getCustomerClassCode() {
        return customerClassCode;
    }

    public void setCustomerClassCode(Integer customerClassCode) {
        this.customerClassCode = customerClassCode;
    }


    public String getCustomerClassName() {
        return customerClassName;
    }

    public void setCustomerClassName(String customerClassName) {
        this.customerClassName = customerClassName;
    }

    public Long getNum() {
        return num;
    }

    public void setNum(Long num) {
        this.num = num;
    }

    public String getDataRange() {
        return dataRange;
    }

    public void setDataRange(String dataRange) {
        this.dataRange = dataRange;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }
}
