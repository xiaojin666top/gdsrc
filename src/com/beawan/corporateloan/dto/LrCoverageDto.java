package com.beawan.corporateloan.dto;

/**
 * 营销客户覆盖率、转化率数据传输对象
 */
public class LrCoverageDto {

    private String userId;
    private String userName;
    private Long custNum;//管户客户总数
    private Long visitNumber;//拜访客户数量
    private String coverageRate;//覆盖率 保留两位小数

    private Long retailSucc;//营销成功次数
    private Long retailCount;//营销总次数
    private String conversion;//转化率
    private Integer rank;//排名
    private Double sumAmount = 0d;//营销成功总金额

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getCustNum() {
        return custNum;
    }

    public void setCustNum(Long custNum) {
        this.custNum = custNum;
    }

    public Long getVisitNumber() {
        return visitNumber;
    }

    public void setVisitNumber(Long visitNumber) {
        this.visitNumber = visitNumber;
    }

    public String getCoverageRate() {
        return coverageRate;
    }

    public void setCoverageRate(String coverageRate) {
        this.coverageRate = coverageRate;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Long getRetailSucc() {
        return retailSucc;
    }

    public void setRetailSucc(Long retailSucc) {
        this.retailSucc = retailSucc;
    }

    public Long getRetailCount() {
        return retailCount;
    }

    public void setRetailCount(Long retailCount) {
        this.retailCount = retailCount;
    }

    public String getConversion() {
        return conversion;
    }

    public void setConversion(String conversion) {
        this.conversion = conversion;
    }

    public Double getSumAmount() {
        return sumAmount;
    }

    public void setSumAmount(Double sumAmount) {
        this.sumAmount = sumAmount;
    }
}
