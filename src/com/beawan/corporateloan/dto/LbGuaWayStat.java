package com.beawan.corporateloan.dto;

/**
 * @author zxh
 * @date 2020/7/22 14:08
 */
public class LbGuaWayStat {

    private double guaAmount; // 担保金额
    private String guaranteeWay; // 担保方式

    public LbGuaWayStat(double guaAmount, String guaranteeWay) {
        this.guaAmount = guaAmount;
        this.guaranteeWay = guaranteeWay;
    }

    public double getGuaAmount() {
        return guaAmount;
    }

    public void setGuaAmount(double guaAmount) {
        this.guaAmount = guaAmount;
    }

    public String getGuaranteeWay() {
        return guaranteeWay;
    }

    public void setGuaranteeWay(String guaranteeWay) {
        this.guaranteeWay = guaranteeWay;
    }
}
