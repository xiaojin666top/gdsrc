package com.beawan.corporateloan.dto;

/**
 * 贷前报告 关联企业信息DTO
 * @author zxh
 * @date 2020/7/7 20:59
 */
public class LbLinkedCompDto {

    private String companyName;//关联企业
    private Double registerCapital;//注册资本
    private String legalPerson;//法定代表人
    private String mainBusiness;//主营业务
    private String companyType;//企业性质
    private String relationship;//关联关系

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Double getRegisterCapital() {
        return registerCapital;
    }

    public void setRegisterCapital(Double registerCapital) {
        this.registerCapital = registerCapital;
    }

    public String getLegalPerson() {
        return legalPerson;
    }

    public void setLegalPerson(String legalPerson) {
        this.legalPerson = legalPerson;
    }

    public String getMainBusiness() {
        return mainBusiness;
    }

    public void setMainBusiness(String mainBusiness) {
        this.mainBusiness = mainBusiness;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }
}
