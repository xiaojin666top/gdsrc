package com.beawan.corporateloan.dto;

/**
 * 财务分析地址
 */
public class FnUrlDto {

    private String labelName;
    private String url;

    public FnUrlDto() {
    }

    public FnUrlDto(String labelName, String url) {
        this.labelName = labelName;
        this.url = url;
    }

    public String getLabelName() {
        return labelName;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
