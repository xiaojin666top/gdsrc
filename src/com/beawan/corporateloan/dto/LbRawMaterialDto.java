package com.beawan.corporateloan.dto;

import com.beawan.survey.custInfo.entity.CompRawMaterial;
import org.apache.poi.common.usermodel.LineStyle;

import java.util.List;

/**
 * 主要原材料采购情况DTO (商贸类)
 * @author zxh
 * @date 2020/8/7 14:45
 */
public class LbRawMaterialDto {

    List<CompRawMaterial> lastYear; // 去年
    List<CompRawMaterial> thisYear; // 今年

    public List<CompRawMaterial> getLastYear() {
        return lastYear;
    }

    public void setLastYear(List<CompRawMaterial> lastYear) {
        this.lastYear = lastYear;
    }

    public List<CompRawMaterial> getThisYear() {
        return thisYear;
    }

    public void setThisYear(List<CompRawMaterial> thisYear) {
        this.thisYear = thisYear;
    }
}
