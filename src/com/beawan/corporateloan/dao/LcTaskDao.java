package com.beawan.corporateloan.dao;

import com.beawan.core.BaseDao;
import com.beawan.corporateloan.entity.LcTask;

/**
 * @author yzj
 */
public interface LcTaskDao extends BaseDao<LcTask> {
}
