package com.beawan.corporateloan.dao;

import com.beawan.core.BaseDao;
import com.beawan.corporateloan.entity.LcReviewContent;

import java.util.List;

/**
 * @author yzj
 */
public interface LcReviewContentDao extends BaseDao<LcReviewContent> {


    List<LcReviewContent> queryByLcTaskIdAndCount(Integer taskId, Integer lcCount);
}
