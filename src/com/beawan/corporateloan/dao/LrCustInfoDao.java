package com.beawan.corporateloan.dao;

import com.beawan.core.BaseDao;
import com.beawan.core.Pagination;
import com.beawan.corporateloan.dto.LrAreaStatDto;
import com.beawan.corporateloan.dto.LrCoverageDto;
import com.beawan.corporateloan.entity.LrCustInfo;

/**
 * @author yzj
 */
public interface LrCustInfoDao extends BaseDao<LrCustInfo> {
    //获取营销客户经理总的管户客户数
    Pagination<LrCoverageDto> getHoldCustNumPaging(int page, int pageSize);

    //获取营销客户经理总的管户客户数
    Pagination<LrCoverageDto> getVisitCustNumPaging(String startTime, String endTime, int page, int pageSize);

    //结合以上两个查询结果，直接获取覆盖率及其排名
    Pagination<LrCoverageDto> getCustCoverage(String userName, String startTime, String endTime, int page, int pageSize);

    /**
     * 获取 营销客户 转化率   营销成功次数/总营销次数
     * @param userName
     * @param startTime
     * @param endTime
     * @param page
     * @param pageSize
     * @return
     */
    Pagination<LrCoverageDto> getCustConversion(String userName, String startTime, String endTime, int page, int pageSize);


    Pagination<LrAreaStatDto> getAreaStatData(String userNo, String startTime, String endTime, int page, int pageSize);
}
