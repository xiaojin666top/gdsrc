package com.beawan.corporateloan.dao;
import com.beawan.core.BaseDao;
import com.beawan.corporateloan.entity.LeExaminePoint;
import java.util.List;

/**
 * @Author: xyh
 * @Date: 03/08/2020
 * @Description:
 */
public interface LeExaminePointDao extends BaseDao<LeExaminePoint>{
	
	/**
     *获取所有可用的审查要点（状态正常）	
     *
     */
	List<LeExaminePoint> getAllLeExaminPoint();
}
