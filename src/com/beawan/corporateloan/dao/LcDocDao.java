package com.beawan.corporateloan.dao;

import com.beawan.core.BaseDao;
import com.beawan.corporateloan.entity.LcDoc;

/**
 * @author yzj
 */
public interface LcDocDao extends BaseDao<LcDoc> {
}
