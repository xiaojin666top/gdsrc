package com.beawan.corporateloan.dao;

import com.beawan.core.BaseDao;
import com.beawan.corporateloan.entity.LrInfo;

/**
 * @author yzj
 */
public interface LrInfoDao extends BaseDao<LrInfo> {
}
