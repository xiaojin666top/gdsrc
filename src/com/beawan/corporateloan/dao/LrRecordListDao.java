package com.beawan.corporateloan.dao;

import com.beawan.core.BaseDao;
import com.beawan.corporateloan.entity.LrRecordList;

/**
 * @author yzj
 */
public interface LrRecordListDao extends BaseDao<LrRecordList> {
}
