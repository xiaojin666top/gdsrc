package com.beawan.corporateloan.dao;

import com.beawan.core.BaseDao;
import com.beawan.corporateloan.entity.LcReviewPoint;

/**
 * @author yzj
 */
public interface LcReviewPointDao extends BaseDao<LcReviewPoint> {
}
