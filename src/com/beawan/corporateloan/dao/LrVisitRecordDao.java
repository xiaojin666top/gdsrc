package com.beawan.corporateloan.dao;

import com.beawan.core.BaseDao;
import com.beawan.corporateloan.entity.LrVisitRecord;

/**
 * @author yzj
 */
public interface LrVisitRecordDao extends BaseDao<LrVisitRecord> {
}
