package com.beawan.corporateloan.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.corporateloan.dao.LrInfoDao;
import com.beawan.corporateloan.entity.LrInfo;
import org.springframework.stereotype.Repository;
import com.beawan.corporateloan.entity.LrInfo;

/**
 * @author yzj
 */
@Repository("lrInfoDao")
public class LrInfoDaoImpl extends BaseDaoImpl<LrInfo> implements LrInfoDao{

}
