package com.beawan.corporateloan.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.corporateloan.dao.LcReviewContentDao;
import com.beawan.corporateloan.entity.LcReviewContent;
import org.springframework.stereotype.Repository;
import com.beawan.corporateloan.entity.LcReviewContent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yzj
 */
@Repository("lcReviewContentDao")
public class LcReviewContentDaoImpl extends BaseDaoImpl<LcReviewContent> implements LcReviewContentDao{

    @Override
    public List<LcReviewContent> queryByLcTaskIdAndCount(Integer taskId, Integer lcCount) {

        Map<String, Object> params = new HashMap<>();
        params.put("lcTaskId", taskId);
        params.put("lcCount", lcCount);

        return selectByProperty(params);
    }
}
