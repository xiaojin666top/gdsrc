package com.beawan.corporateloan.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.corporateloan.dao.LcDocDao;
import com.beawan.corporateloan.entity.LcDoc;
import org.springframework.stereotype.Repository;
import com.beawan.corporateloan.entity.LcDoc;

/**
 * @author yzj
 */
@Repository("lcDocDao")
public class LcDocDaoImpl extends BaseDaoImpl<LcDoc> implements LcDocDao{

}
