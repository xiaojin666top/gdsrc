package com.beawan.corporateloan.dao.impl;

import com.beawan.common.Constants;
import com.beawan.core.BaseDaoImpl;
import com.beawan.corporateloan.dao.LeExaminePointDao;
import com.beawan.corporateloan.entity.LeExaminePoint;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

/**
 * @Author: xyh
 * @Date: 03/08/2020
 * @Description:
 */
@Repository("leExaminePointDao")
public class LeExaminePointDaoImpl extends BaseDaoImpl<LeExaminePoint> implements LeExaminePointDao {
	/**
     *获取所有可用的审查要点（状态正常）	
     *
     */
	@Override
	public List<LeExaminePoint> getAllLeExaminPoint() {
		
		Map<String, Object> params = new HashMap<>();
		StringBuilder sql = new StringBuilder(
				"SELECT ID,EXAMINE_TYPE as examineType,EXAMINE_NAME as examineName,EXAMINE_MEMO as examineMemo,flag,NEED_MINUTE as needMinute ");
		sql.append("FROM LE_EXAMINE_POINT ");
		sql.append(" where 1=1 and status=:status ");
		sql.append(" order by EXAMINE_TYPE asc ");
		params.put("status", Constants.NORMAL);
		
		return findCustListBySql(LeExaminePoint.class, sql, params);
	}
}
