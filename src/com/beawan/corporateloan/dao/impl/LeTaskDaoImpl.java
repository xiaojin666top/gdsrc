package com.beawan.corporateloan.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.corporateloan.dao.LeTaskDao;
import com.beawan.corporateloan.entity.LeTask;
import org.springframework.stereotype.Repository;

/**
 * @Author: xyh
 * @Date: 03/08/2020
 * @Description:
 */
@Repository("leTaskDao")
public class LeTaskDaoImpl extends BaseDaoImpl<LeTask> implements LeTaskDao {
}
