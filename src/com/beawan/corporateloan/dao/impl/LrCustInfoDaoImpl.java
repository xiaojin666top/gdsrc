package com.beawan.corporateloan.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.core.Pagination;
import com.beawan.corporateloan.dao.LrCustInfoDao;
import com.beawan.corporateloan.dto.LrAreaStatDto;
import com.beawan.corporateloan.dto.LrCoverageDto;
import com.beawan.corporateloan.entity.LrCustInfo;
import com.platform.util.StringUtil;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

/**
 * @author yzj
 */
@Repository("lrCustInfoDao")
public class LrCustInfoDaoImpl extends BaseDaoImpl<LrCustInfo> implements LrCustInfoDao{

    @Override
    public Pagination<LrCoverageDto> getHoldCustNumPaging(int page, int pageSize) {
        StringBuilder sql = new StringBuilder();
        sql.append("select A.USER_ID userId,A.USER_NAME userName,COUNT(C.ID) custNum from GDTCESYS.BS_USER_INFO  A")
                .append(" join GDTCESYS.BS_ROLE_INFO B on A.ROLE_NO=B.ROLE_NO")
                .append(" LEFT JOIN GDTCESYS.LR_CUST_INFO C ON A.USER_ID=C.HOLD_USER_NO")
                //ggg因为客户都在0008超级管理员角色上  暂时加上or
                .append(" where B.ROLE_NO='0010' OR  B.ROLE_NO='0008'")
                .append(" GROUP BY A.USER_ID,A.USER_NAME")
                .append(" ORDER BY A.USER_ID");
        return findCustSqlPagination(LrCoverageDto.class, sql, null, page, pageSize);
    }

    @Override
    public Pagination<LrCoverageDto> getVisitCustNumPaging(String startTime, String endTime, int page, int pageSize) {

//        --营销客户经理拜访过的客户数量
//        SELECT USER_ID,USER_NAME,
//        sum(case when visit !=0 then 1 else 0 end) as visitNumber FROM (
//                SELECT USER_ID,USER_NAME,CUSTOMER_NO,
//                sum(case when visit!=0 then 1 else 0 end) as visit
//        FROM (
//                select A.USER_ID,A.USER_NAME,C.CUSTOMER_NO,D.SERIAL_NUMBER,
//                sum( case when d.update_time is null then 0 else  1 end ) visit
//        from GDTCESYS.BS_USER_INFO  A
//        join GDTCESYS.BS_ROLE_INFO B on A.ROLE_NO=B.ROLE_NO
//        LEFT JOIN GDTCESYS.LR_INFO C ON A.USER_ID=C.USER_NO
//        LEFT JOIN GDTCESYS.LR_VISIT_RECORD D ON C.SERIAL_NUMBER=D.SERIAL_NUMBER
//        where (B.ROLE_NO='0010' OR  B.ROLE_NO='0008' )
//        AND A.STATUS='0' AND ( C.STATUS='0' or C.STATUS IS NULL)
//                --通过拜访时间段过滤
//                --and D.update
//        and (d.update_time is null or (d.update_time >='2020-07-06 00:00:00' and d.update_time <='2020-07-10 00:00:00'))
//        GROUP BY A.USER_ID,A.USER_NAME,C.CUSTOMER_NO,D.SERIAL_NUMBER
//        ORDER BY A.USER_ID
// )GROUP BY USER_ID,USER_NAME,CUSTOMER_NO
// ) GROUP BY USER_ID,USER_NAME
        Map<String,Object> params = new HashMap<>();
        StringBuilder sql = new StringBuilder("");
        sql.append(" SELECT USER_ID userId,USER_NAME userName,")
                .append(" sum(case when visit !=0 then 1 else 0 end) as visitNumber FROM (")
                .append(" SELECT USER_ID,USER_NAME,CUSTOMER_NO,")
                .append(" sum(case when visit!=0 then 1 else 0 end) as visit FROM (")
                .append(" select A.USER_ID,A.USER_NAME,C.CUSTOMER_NO,D.SERIAL_NUMBER,")
                .append(" sum( case when d.update_time is null then 0 else  1 end ) visit")
                .append(" from GDTCESYS.BS_USER_INFO  A")
                .append(" join GDTCESYS.BS_ROLE_INFO B on A.ROLE_NO=B.ROLE_NO")
                .append(" LEFT JOIN GDTCESYS.LR_INFO C ON A.USER_ID=C.USER_NO")
                .append(" LEFT JOIN GDTCESYS.LR_VISIT_RECORD D ON C.SERIAL_NUMBER=D.SERIAL_NUMBER")
                .append(" where (B.ROLE_NO='0010' OR  B.ROLE_NO='0008' )")
                .append(" AND A.STATUS='0' AND ( C.STATUS='0' or C.STATUS IS NULL)");
        if(!StringUtil.isEmptyString(startTime) && !StringUtil.isEmptyString(endTime)){
            sql.append(" and (d.update_time is null or (d.update_time >=:startTime and d.update_time <=:endTime))");
            params.put("startTime", startTime);
            params.put("endTime", endTime);
        }else if(!StringUtil.isEmptyString(startTime)){
            sql.append(" and (d.update_time is null or d.update_time >=:startTime)");
            params.put("startTime", startTime);
        }else if(!StringUtil.isEmptyString(endTime)){
            sql.append(" and (d.update_time is null or d.update_time <=:endTime)");
            params.put("endTime", endTime);
        }
        sql.append(" GROUP BY A.USER_ID,A.USER_NAME,C.CUSTOMER_NO,D.SERIAL_NUMBER")
                .append(" ORDER BY A.USER_ID")
                .append(" )GROUP BY USER_ID,USER_NAME,CUSTOMER_NO")
                .append(" ) GROUP BY USER_ID,USER_NAME");

        return findCustSqlPagination(LrCoverageDto.class, sql, params, page, pageSize);
    }

    public Pagination<LrCoverageDto> getCustCoverage(String userName, String startTime, String endTime, int page, int pageSize) {
        Map<String,Object> params = new HashMap<>();
        StringBuilder sql = new StringBuilder();

        sql.append("SELECT a.user_id userId,a.user_name userName,a.custnum custNum,b.VISITNUMBER visitNumber, " +
                " CASE WHEN a.custnum=0 " +
                " THEN CAST('0.00' AS DECIMAL(10,2) ) " +
                " ELSE  CAST(b.visitnumber*100.0/a.custnum AS DECIMAL(10,2))  " +
                " END  " +
                " coverageRate " +
                "   FROM " +
                "   ( " +
                "   " +
                " SELECT USER_ID,USER_NAME,COUNT(ID) custNum " +
                "  FROM  (SELECT  A.*, B.ROLE_NAME,B.DEPARTMENG_NO ,B.ROLE_DESCR, B.ORGANNO, C.ID,C.CUSTOMER_NO,C.CUSTOMER_CLASS," +
                "  C.CUSTOMER_LR_TYPE,C.EXPIRE_STATUS, C.EXPIRE_TIME, C.HOLD_USER_NO,C.CUST_ADDR  FROM BS_USER_INFO  A " +
                "  JOIN BS_ROLE_INFO B ON A.ROLE_NO=B.ROLE_NO LEFT JOIN LR_CUST_INFO C ON A.USER_ID=C.HOLD_USER_NO" +
                "   WHERE B.ROLE_NO='0010' OR  B.ROLE_NO='0008'  OR  B.ROLE_NO='0001'  GROUP BY A.USER_ID,A.USER_NAME)  ab1) a" +
                "  JOIN (" +
                "SELECT USER_ID,USER_NAME, SUM(CASE WHEN visit !=0 THEN 1 ELSE 0 END) AS visitNumber " +
                " FROM (  " +
                " SELECT USER_ID,USER_NAME,CUSTOMER_NO, SUM(CASE WHEN visit!=0 THEN 1 ELSE 0 END) AS visit " +
                " FROM ( SELECT A.USER_ID,A.USER_NAME,C.CUSTOMER_NO,D.SERIAL_NUMBER, " +
                " SUM( CASE WHEN d.VISIT_TIME IS NULL THEN 0 ELSE  1 END ) visit " +
                " FROM " +
                " BS_USER_INFO  A JOIN BS_ROLE_INFO B ON A.ROLE_NO=B.ROLE_NO LEFT JOIN LR_INFO C ON A.USER_ID=C.USER_NO LEFT JOIN     LR_VISIT_RECORD D ON C.SERIAL_NUMBER=D.SERIAL_NUMBER " +
                " WHERE (B.ROLE_NO='0010' OR  B.ROLE_NO='0008'  OR  B.ROLE_NO='0001' ) AND A.STATUS='0' " +
                " AND ( C.STATUS='0' OR C.STATUS IS NULL)) tb1) tb2) b ");
        if(!StringUtil.isEmptyString(startTime) && !StringUtil.isEmptyString(endTime)){
            sql.append(" and (d.VISIT_TIME is null or (d.VISIT_TIME >="+startTime+"and"+ "d.VISIT_TIME<="+endTime);
            // sql.append(" and (d.VISIT_TIME is null or (d.VISIT_TIME >=:startTime and d.VISIT_TIME <=:endTime))");

//            params.put("startTime", startTime);
//            params.put("endTime", endTime);
        }else if(!StringUtil.isEmptyString(startTime)){
            sql.append(" and (d.VISIT_TIME is null or d.VISIT_TIME >="+startTime);
            // params.put("startTime", startTime);
        }else if(!StringUtil.isEmptyString(endTime)){
            sql.append(" and (d.VISIT_TIME is null or d.VISIT_TIME <=)"+endTime);
            //params.put("endTime", endTime);
        }
//        sql.append(" GROUP BY USER_ID,A.USER_NAME,C.CUSTOMER_NO,D.SERIAL_NUMBER")
//                .append(" )GROUP BY USER_ID,USER_NAME,CUSTOMER_NO")
//                .append(" ) GROUP BY USER_ID,USER_NAME) b")
//                .append(" on a.user_id=b.user_id")
//                .append("  order by coverageRate desc")
//                .append(" ) )where 1=1 ");
        sql.append(" GROUP BY userid ,userName");
        if(!StringUtil.isEmptyString(userName)){
//            銆併€乮f (!StringUtil.isEmptyString(custName)) {
//                sql.append(" and b.customer_name like '%"+custName+"%'");
//                //	params.put("custName", "%" + custName + "%");
//            }
            sql.append(" and userName like :'%"+userName+"%'");
            //  params.put("userName", "%" + userName + "%");
        }
        return findCustSqlPagination(LrCoverageDto.class, sql, params, page, pageSize);
    }


    @Override
    public Pagination<LrCoverageDto> getCustConversion(String userName, String startTime, String endTime, int page, int pageSize) {
        Map<String,Object> params = new HashMap<>();
        StringBuilder sql = new StringBuilder();
//        sql.append("select * from (")
//                .append(" select count(*), ")
//                .append(" user_id userId, user_name userName,retailSucc,retailCount,sumAmount,conversion")
//                .append(" from ( select user_id,user_name, retailSucc,retailCount,sumAmount,")
//                .append(" case when retailCount=0 then cast('0.00' as decimal(10,2) ) else   cast(retailSucc*100.0/retailCount as decimal(10,2)) end as conversion")
//                .append(" from ( select a.user_id,a.user_Name, sum(C.RETAIL_MONEY) sumAmount,")
//                .append(" sum(case when c.RETAIL_STATUS =0 then 1 else 0 end) retailSucc,")
//                .append(" count(c.RETAIL_STATUS) retailCount")
//                .append(" from .BS_USER_INFO  A")
//                .append(" join .BS_ROLE_INFO B on A.ROLE_NO=B.ROLE_NO")
//                .append(" LEFT JOIN .LR_INFO C ON A.USER_ID=C.USER_NO")
//                .append(" where (B.ROLE_NO='0010' OR  B.ROLE_NO='0008' )")
//                .append(" AND A.STATUS='0' AND ( C.STATUS='0' or C.STATUS IS NULL)")
//                .append(" ) tb1")
//                .append(" ) tb2 ORDER BY conversion DESC")
//                .append(" tb3 ");


        sql.append("select * from ( " +
                "select  count(*) ,user_id userId, user_name userName,retailSucc,retailCount,sumAmount,conversion " +
                " from  ( " +
                " select user_id,user_name, retailSucc,retailCount,sumAmount, " +
                " case when retailCount=0 " +
                " then cast('0.00' as decimal(10,2) ) else   " +
                " cast(retailSucc*100.0/retailCount " +
                " as decimal(10,2)) " +
                " end as conversion " +
                " from (" +
                " select a.user_id,a.user_Name, sum(C.RETAIL_MONEY) sumAmount, " +
                " sum(case when c.RETAIL_STATUS =0 then 1 else 0 end) retailSucc, count(c.RETAIL_STATUS) retailCount  " +
                " from BS_USER_INFO  A " +
                " join BS_ROLE_INFO B " +
                " on A.ROLE_NO=B.ROLE_NO " +
                " LEFT JOIN LR_INFO C ON A.USER_ID=C.USER_NO " +
                " where (B.ROLE_NO='0010' OR  B.ROLE_NO='0008' ) " +
                " AND A.STATUS='0' AND  C.STATUS='0' or C.STATUS IS NULL" +
                ") tb1 " +
                ") tb2 " +
                "ORDER BY conversion DESC  ) tb3 ");

        //select * from (
        //	select  count(*) ,user_id userId, user_name userName,retailSucc,retailCount,sumAmount,conversion
        //	 from  (
        //	 select user_id,user_name, retailSucc,retailCount,sumAmount,
        //	 case when retailCount=0
        //	 then cast('0.00' as decimal(10,2) ) else
        //	 cast(retailSucc*100.0/retailCount
        //	 as decimal(10,2))
        //	 end as conversion
        //	 from (
        //	 select a.user_id,a.user_Name, sum(C.RETAIL_MONEY) sumAmount,
        //	 sum(case when c.RETAIL_STATUS =0 then 1 else 0 end) retailSucc, count(c.RETAIL_STATUS) retailCount
        //	 from BS_USER_INFO  A
        //	 join BS_ROLE_INFO B
        //	 on A.ROLE_NO=B.ROLE_NO
        //	 LEFT JOIN LR_INFO C ON A.USER_ID=C.USER_NO
        //	 where (B.ROLE_NO='0010' OR  B.ROLE_NO='0008' )
        //	 AND A.STATUS='0' AND  C.STATUS='0' or C.STATUS IS NULL
        //) tb1
        //) tb2
        //ORDER BY conversion DESC
        //) tb3 group by userid,username ;

        if(!StringUtil.isEmptyString(startTime) && !StringUtil.isEmptyString(endTime)){
            sql.append(" and (c.update_time is null or (c.update_time >="+startTime+" and"+" c.update_time <="+endTime);
//            params.put("startTime", startTime);
//            params.put("endTime", endTime);
        }else if(!StringUtil.isEmptyString(startTime)){
            sql.append(" and (c.update_time is null or c.update_time >="+startTime);
            // params.put("startTime", startTime);
        }else if(!StringUtil.isEmptyString(endTime)){
            sql.append(" and (c.update_time is null or c.update_time <="+endTime);
            // params.put("endTime", endTime);
        }
        sql.append(" group by userid,username");
//                .append(" order by conversion desc")
        //.append(" )) where 1=1");
        if(!StringUtil.isEmptyString(userName)){
            sql.append(" and userName like '%"+userName+"%'");
            // params.put("userName", "%" + userName + "%");
        }
        return findCustSqlPagination(LrCoverageDto.class, sql, params, page, pageSize);
    }



    @Override
    public Pagination<LrAreaStatDto> getAreaStatData(String userNo, String startTime, String endTime, int page, int pageSize) {
        Map<String, Object> params = new HashMap<>();
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT CASE WHEN address IS NULL THEN '其他' ELSE address END address, COUNT(customerNo) customerSum,")
                .append(" SUM(CASE WHEN visitSum!=0 THEN 1 ELSE 0 END) visitNum,")
                .append(" SUM(retailCount) retailSum, SUM(successCount) retailSuccessNum FROM(")
                .append(" SELECT address, customerNo, COUNT(serNo) retailCount,")
                .append(" SUM(CASE WHEN retailStatus=0 then 1 else 0 end) successCount,")
                .append(" SUM(CASE WHEN visitCount!=0 then 1 else 0 end) visitSum FROM(")
                .append(" SELECT lci.CUST_ADDR address, li.CUSTOMER_NO customerNo, li.SERIAL_NUMBER serNo,")
                .append(" li.RETAIL_STATUS retailStatus, SUM(CASE WHEN lvr.VISIT_TIME IS NULL THEN 0 ELSE 1 END) visitCount")

                .append(" FROM LR_INFO li LEFT JOIN LR_CUST_INFO lci ON lci.CUSTOMER_NO = li.CUSTOMER_NO")
                .append(" LEFT JOIN LR_VISIT_RECORD lvr ON lvr.SERIAL_NUMBER = li.SERIAL_NUMBER")
                .append(" WHERE li.STATUS = '0' AND lvr.STATUS = '0'  ) tb1  ) tb2") ;

        //SELECT CASE WHEN address IS NULL THEN '其他' ELSE address END address,
        //COUNT(customerNo) customerSum,
        //SUM(CASE WHEN visitSum!=0 THEN 1 ELSE 0 END) visitNum,
        //SUM(retailCount) retailSum,
        //SUM(successCount) retailSuccessNum FROM
        //(
        //SELECT address, customerNo,
        //COUNT(serNo) retailCount,
        //SUM(CASE WHEN retailStatus=0 THEN 1 ELSE 0 END) successCount,
        //SUM(CASE WHEN visitCount!=0 THEN 1 ELSE 0 END) visitSum
        //FROM(
        //SELECT lci.CUST_ADDR address, li.CUSTOMER_NO customerNo, li.SERIAL_NUMBER serNo, li.RETAIL_STATUS retailStatus,
        //SUM(CASE WHEN lvr.VISIT_TIME IS NULL THEN 0 ELSE 1 END) visitCount
        //FROM LR_INFO li LEFT JOIN LR_CUST_INFO lci ON lci.CUSTOMER_NO = li.CUSTOMER_NO
        //LEFT JOIN LR_VISIT_RECORD lvr ON lvr.SERIAL_NUMBER = li.SERIAL_NUMBER WHERE li.STATUS = 0 AND lvr.STATUS = 0
        //) tb1
        //
        //)tb2
        if (!StringUtil.isEmptyString(userNo)){
            sql.append(" AND li.USER_NO = "+userNo);
            // params.put("userNo", userNo);
        }
        if(!StringUtil.isEmptyString(startTime) && !StringUtil.isEmptyString(endTime)){
            // sql.append(" AND (lvr.VISIT_TIME IS NULL OR (lvr.VISIT_TIME >= :startTime and lvr.VISIT_TIME <= :endTime))");

            sql.append(" AND (lvr.VISIT_TIME IS NULL OR (lvr.VISIT_TIME >= "+startTime+" and"+" lvr.VISIT_TIME <= "+endTime+")");
//            params.put("startTime", startTime);
//            params.put("endTime", endTime);
        }else if(!StringUtil.isEmptyString(startTime)){
//            sql.append(" AND (lvr.VISIT_TIME IS NULL OR lvr.VISIT_TIME >= :startTime)");
//            params.put("startTime", startTime);
            sql.append(" AND (lvr.VISIT_TIME IS NULL OR lvr.VISIT_TIME >= "+startTime+")");
        }else if(!StringUtil.isEmptyString(endTime)){
//            sql.append(" AND (lvr.VISIT_TIME IS NULL OR lvr.VISIT_TIME <= :endTime)");
//            params.put("endTime", endTime);
            sql.append(" AND (lvr.VISIT_TIME IS NULL OR lvr.VISIT_TIME <= "+endTime+")");
        }
//        sql.append(" GROUP BY lci.CUST_ADDR, li.CUSTOMER_NO, li.SERIAL_NUMBER, li.RETAIL_STATUS)")
//                .append(" GROUP BY address, customerNo)")
        sql.append(" GROUP BY address");
        return this.findCustSqlPagination(LrAreaStatDto.class, sql.toString(), params, page, pageSize);
    }
}
