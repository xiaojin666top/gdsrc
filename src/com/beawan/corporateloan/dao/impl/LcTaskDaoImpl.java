package com.beawan.corporateloan.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.corporateloan.dao.LcTaskDao;
import com.beawan.corporateloan.entity.LcTask;
import org.springframework.stereotype.Repository;
import com.beawan.corporateloan.entity.LcTask;

/**
 * @author yzj
 */
@Repository("lcTaskDao")
public class LcTaskDaoImpl extends BaseDaoImpl<LcTask> implements LcTaskDao{

}
