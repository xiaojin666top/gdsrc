package com.beawan.corporateloan.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.corporateloan.dao.LcReviewPointDao;
import com.beawan.corporateloan.entity.LcReviewPoint;
import org.springframework.stereotype.Repository;
import com.beawan.corporateloan.entity.LcReviewPoint;

/**
 * @author yzj
 */
@Repository("lcReviewPointDao")
public class LcReviewPointDaoImpl extends BaseDaoImpl<LcReviewPoint> implements LcReviewPointDao{

}
