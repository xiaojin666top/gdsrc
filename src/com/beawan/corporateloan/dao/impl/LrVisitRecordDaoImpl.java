package com.beawan.corporateloan.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.corporateloan.dao.LrVisitRecordDao;
import com.beawan.corporateloan.entity.LrVisitRecord;
import org.springframework.stereotype.Repository;
import com.beawan.corporateloan.entity.LrVisitRecord;

/**
 * @author yzj
 */
@Repository("lrVisitRecordDao")
public class LrVisitRecordDaoImpl extends BaseDaoImpl<LrVisitRecord> implements LrVisitRecordDao{

}
