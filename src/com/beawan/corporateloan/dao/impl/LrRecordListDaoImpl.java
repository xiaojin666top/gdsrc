package com.beawan.corporateloan.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.corporateloan.dao.LrRecordListDao;
import com.beawan.corporateloan.entity.LrRecordList;
import org.springframework.stereotype.Repository;
import com.beawan.corporateloan.entity.LrRecordList;

/**
 * @author yzj
 */
@Repository("lrRecordListDao")
public class LrRecordListDaoImpl extends BaseDaoImpl<LrRecordList> implements LrRecordListDao{

}
