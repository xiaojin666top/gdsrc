package com.beawan.corporateloan.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.corporateloan.dao.LmProcessDao;
import com.beawan.corporateloan.entity.LmProcess;

/**
 * 
 * @ClassName: LmProcessDaoImpl
 * @Description: TODO(主表dao实现类)
 * @author xyh
 * @date 16 Jun 2020
 *
 */
@Repository("lmProcessDao")
public class LmProcessDaoImpl extends BaseDaoImpl<LmProcess> implements LmProcessDao{

}
