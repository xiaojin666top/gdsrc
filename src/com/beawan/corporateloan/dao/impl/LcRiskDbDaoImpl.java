package com.beawan.corporateloan.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.corporateloan.dao.LcRiskDbDao;
import com.beawan.corporateloan.entity.LcRiskDb;
import org.springframework.stereotype.Repository;
import com.beawan.corporateloan.entity.LcRiskDb;

/**
 * @author yzj
 */
@Repository("lcRiskDbDao")
public class LcRiskDbDaoImpl extends BaseDaoImpl<LcRiskDb> implements LcRiskDbDao{

}
