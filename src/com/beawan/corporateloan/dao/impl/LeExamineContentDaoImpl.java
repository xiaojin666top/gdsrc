package com.beawan.corporateloan.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.corporateloan.dao.LeExamineContentDao;
import com.beawan.corporateloan.entity.LeExamineContent;
import org.springframework.stereotype.Repository;

/**
 * @Author: xyh
 * @Date: 03/08/2020
 * @Description:
 */
@Repository("leExamineContentDao")
public class LeExamineContentDaoImpl extends BaseDaoImpl<LeExamineContent> implements LeExamineContentDao {
}
