package com.beawan.corporateloan.dao;

import com.beawan.core.BaseDao;
import com.beawan.corporateloan.entity.LcRiskDb;

/**
 * @author yzj
 */
public interface LcRiskDbDao extends BaseDao<LcRiskDb> {
}
