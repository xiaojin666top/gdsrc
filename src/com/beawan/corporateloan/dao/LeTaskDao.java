package com.beawan.corporateloan.dao;
import com.beawan.core.BaseDao;
import com.beawan.corporateloan.entity.LeTask;

/**
 * @Author: xyh
 * @Date: 03/08/2020
 * @Description:
 */
public interface LeTaskDao extends BaseDao<LeTask>{
}
