package com.beawan.corporateloan.dao;

import com.beawan.core.BaseDao;
import com.beawan.corporateloan.entity.LmProcess;
/**
 * 
 * @ClassName: LmProcessDao
 * @Description: TODO(贷款主表)
 * @author xyh
 * @date 16 Jun 2020
 *
 */
public interface LmProcessDao extends BaseDao<LmProcess>{

}
