package com.beawan.library.bean;

import java.io.Serializable;


import com.beawan.library.bean.ErrTemplet;


/**
 * @ClassName ErrTemplet
 * @Description (异常库——模板)
 * @author czc
 * @Date 2017年5月15日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public class ErrTemplet implements Serializable{

	private static final long serialVersionUID = 7892971997495503267L;
	
	private Long id;
	
	/** @Field @customerNo : (异常名) */
	private String name;
	
	/** @Field @url : (url地址) */
	private String url;
	
	/** @Field @formula : (公式) */
	private String formula;       
	
	/** @Field @rateval : (触发器参数最小值) */
	private String rateval;         
	
	/** @Field @report : (相关联的报表，所对应的明细表id) */
	private Long reportTempId;
	
	/** @Field @isedit : (参数能否设置) */
	private String isedit="true";       
	
	/** @Field @report : (备注) */
	private String remark;
	
	@Override
	public boolean equals(Object obj) {
		ErrTemplet errkno = null;
		if(null == obj) {
			return false;
		}
		if(obj instanceof ErrTemplet) {
			errkno = (ErrTemplet) obj;
		}else {
			return false;
		}
		return this.name.equals(errkno.getName());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFormula() {
		return formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	public String getRateval() {
		return rateval;
	}

	public void setRateval(String rateval) {
		this.rateval = rateval;
	}

	public Long getReportTempId() {
		return reportTempId;
	}

	public void setReportTempId(Long reportTempId) {
		this.reportTempId = reportTempId;
	}

	public String getIsedit() {
		return isedit;
	}

	public void setIsedit(String isedit) {
		this.isedit = isedit;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	
}
