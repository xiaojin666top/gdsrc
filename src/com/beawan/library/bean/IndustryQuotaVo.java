package com.beawan.library.bean;

import java.io.Serializable;

public class IndustryQuotaVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2318597806763054883L;
	
	private String stdId;
	
	private String industryCode;
	
	private String parentCode;
	
	private String industryName;
	
	private String enlocate;
	
	private String oneLevel;
	
	private String twoLevel;
	
	private String threeLevel;
	
	private String fourLevel;
	
	private String fiveLevel;

	public IndustryQuotaVo(IndustryQuota quota) {
		super();
		this.stdId = quota.getStdId();
		this.industryCode = quota.getIndustryCode();
		this.parentCode = quota.getParentCode();
		this.industryName = quota.getIndustryName();
		this.enlocate = quota.getEnlocate();
		splitIndustry(quota.getCnlocate());
	}

	public String getStdId() {
		return stdId;
	}

	public void setStdId(String stdId) {
		this.stdId = stdId;
	}

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

	public String getIndustryName() {
		return industryName;
	}

	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}

	public String getEnlocate() {
		return enlocate;
	}

	public void setEnlocate(String enlocate) {
		this.enlocate = enlocate;
	}

	public String getOneLevel() {
		return oneLevel;
	}

	public void setOneLevel(String oneLevel) {
		this.oneLevel = oneLevel;
	}

	public String getTwoLevel() {
		return twoLevel;
	}

	public void setTwoLevel(String twoLevel) {
		this.twoLevel = twoLevel;
	}

	public String getThreeLevel() {
		return threeLevel;
	}

	public void setThreeLevel(String threeLevel) {
		this.threeLevel = threeLevel;
	}

	public String getFourLevel() {
		return fourLevel;
	}

	public void setFourLevel(String fourLevel) {
		this.fourLevel = fourLevel;
	}

	public String getFiveLevel() {
		return fiveLevel;
	}

	public void setFiveLevel(String fiveLevel) {
		this.fiveLevel = fiveLevel;
	}
	
	public void splitIndustry(String cnlocate) {
		String[] indStr = cnlocate.split(",");
		if(indStr.length == 0 || indStr.length == 1) {
			return;
		}
		if(indStr.length == 2) {
			oneLevel = indStr[1];
		}else if(indStr.length == 3) {
			oneLevel = indStr[1];
			twoLevel = indStr[2];
		}else if(indStr.length == 4) {
			oneLevel = indStr[1];
			twoLevel = indStr[2];
			threeLevel = indStr[3];
		}else if(indStr.length == 5) {
			oneLevel = indStr[1];
			twoLevel = indStr[2];
			threeLevel = indStr[3];
			fourLevel = indStr[4];
		}else if(indStr.length == 6) {
			oneLevel = indStr[1];
			twoLevel = indStr[2];
			threeLevel = indStr[3];
			fourLevel = indStr[4];
			fiveLevel = indStr[5];
		}
	}
}
