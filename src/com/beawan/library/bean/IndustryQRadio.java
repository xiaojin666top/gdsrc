package com.beawan.library.bean;

import java.io.Serializable;


/**
 * @ClassName IndustryQRadio
 * @Description (行业比率)
 * @author czc
 * @Date 2017年5月23日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public class IndustryQRadio implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2318597806763054883L;
	
	private String stdId;
	
	private String quotaId;
	
	private String itemName;
	
	private Double excellentValue;
	
	private Double fineValue;
	
	private Double averageValue;
	
	private Double lowerValue;
	
	private Double poorValue;
	
	private String scopeRange;
	
	private String belong;
	
	public String getStdId() {
		return stdId;
	}

	public void setStdId(String stdId) {
		this.stdId = stdId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Double getExcellentValue() {
		return excellentValue;
	}

	public void setExcellentValue(Double excellentValue) {
		this.excellentValue = excellentValue;
	}

	public Double getFineValue() {
		return fineValue;
	}

	public void setFineValue(Double fineValue) {
		this.fineValue = fineValue;
	}

	public Double getAverageValue() {
		return averageValue;
	}

	public void setAverageValue(Double averageValue) {
		this.averageValue = averageValue;
	}

	public Double getLowerValue() {
		return lowerValue;
	}

	public void setLowerValue(Double lowerValue) {
		this.lowerValue = lowerValue;
	}

	public Double getPoorValue() {
		return poorValue;
	}

	public void setPoorValue(Double poorValue) {
		this.poorValue = poorValue;
	}

	public String getScopeRange() {
		return scopeRange;
	}

	public void setScopeRange(String scopeRange) {
		this.scopeRange = scopeRange;
	}

	public String getBelong() {
		return belong;
	}

	public void setBelong(String belong) {
		this.belong = belong;
	}

	public String getQuotaId() {
		return quotaId;
	}

	public void setQuotaId(String quotaId) {
		this.quotaId = quotaId;
	}
}
