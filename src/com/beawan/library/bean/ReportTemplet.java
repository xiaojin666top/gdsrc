package com.beawan.library.bean;

import java.io.Serializable;


/**
 * @ClassName Report
 * @Description TODO(资产负债表、利润表、应收账款等表模板)
 * @author czc
 * @Date 2017年5月12日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public class ReportTemplet implements Serializable {

	/**
	 * 表格
	 */
	private static final long serialVersionUID = -8366239942131435465L;
	
	
	private Long id;

	/** @Field @name : TODO(表名，例如资产负债表) */
	private String name;
	
	/** @Field @url : TODO(模板url地址) */
	private String url;

	/** @Field @showUrl : TODO(表头url地址) */
	private String showUrl;
	
	/** @Field @reportType : TODO(表类型，用以区分不同的表，例如应收账款是receival) */
	private String reportType;
	
	/** @Field @item : TODO(报表科目名称) */
	private String item;  

	/** @Field @remark : TODO(备注,区分异常所对应的表，与异常中的remark字段相同) */
	private String remark;


	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getShowUrl() {
		return showUrl;
	}

	public void setShowUrl(String showUrl) {
		this.showUrl = showUrl;
	}

	public String getReportType() {
		return reportType;
	}

	public void setReportType(String reportType) {
		this.reportType = reportType;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}



	
	
}
