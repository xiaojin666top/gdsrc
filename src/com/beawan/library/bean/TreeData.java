package com.beawan.library.bean;

import java.io.Serializable;

/**
 * @ClassName TreeData
 * @Description TODO(国标行业)
 * @author czc
 * @Date 2017年5月23日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public class TreeData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2318597806763054883L;

	private String stdId;

	private String enName;

	private String cnName;

	private String abvenName;

	private String locate;

	private String optType;

	private String memo;

	private String flag;

	private String levels;

	private Integer orderId;

	private String isParent;

	private String industryCode;

	public String getStdId() {
		return stdId;
	}

	public void setStdId(String stdId) {
		this.stdId = stdId;
	}

	public String getEnName() {
		return enName;
	}

	public void setEnName(String enName) {
		this.enName = enName;
	}

	public String getCnName() {
		return cnName;
	}

	public void setCnName(String cnName) {
		this.cnName = cnName;
	}

	public String getAbvenName() {
		return abvenName;
	}

	public void setAbvenName(String abvenName) {
		this.abvenName = abvenName;
	}

	public String getLocate() {
		return locate;
	}

	public void setLocate(String locate) {
		this.locate = locate;
	}

	public String getOptType() {
		return optType;
	}

	public void setOptType(String optType) {
		this.optType = optType;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getLevels() {
		return levels;
	}

	public void setLevels(String levels) {
		this.levels = levels;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public String getIsParent() {
		return isParent;
	}

	public void setIsParent(String isParent) {
		this.isParent = isParent;
	}

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	@Override
	public String toString() {
		return "TreeData [stdId=" + stdId + ", enName=" + enName + ", cnName="
				+ cnName + ", abvenName=" + abvenName + ", locate=" + locate
				+ ", optType=" + optType + ", memo=" + memo + ", flag=" + flag
				+ ", levels=" + levels + ", orderId=" + orderId + ", isParent="
				+ isParent + ", industryCode=" + industryCode + "]";
	}

}
