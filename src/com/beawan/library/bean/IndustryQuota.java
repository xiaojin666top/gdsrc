package com.beawan.library.bean;

import java.io.Serializable;

/**
 * @ClassName IndustryQuota
 * @Description TODO(行业库)
 * @author czc
 * @Date 2017年5月23日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public class IndustryQuota implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2318597806763054883L;
	
	private String stdId;
	
	private String industryCode;
	
	private String parentCode;
	
	private String industryName;
	
	private String enlocate;
	
	private String cnlocate;
	
	private String treeDataEnname;
	
	public String getStdId() {
		return stdId;
	}

	public void setStdId(String stdId) {
		this.stdId = stdId;
	}

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public String getParentCode() {
		return parentCode;
	}

	public void setParentCode(String parentCode) {
		this.parentCode = parentCode;
	}

	public String getIndustryName() {
		return industryName;
	}

	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}

	public String getEnlocate() {
		return enlocate;
	}

	public void setEnlocate(String enlocate) {
		this.enlocate = enlocate;
	}

	public String getCnlocate() {
		return cnlocate;
	}

	public void setCnlocate(String cnlocate) {
		this.cnlocate = cnlocate;
	}

	public String getTreeDataEnname() {
		return treeDataEnname;
	}

	public void setTreeDataEnname(String treeDataEnname) {
		this.treeDataEnname = treeDataEnname;
	}
}
