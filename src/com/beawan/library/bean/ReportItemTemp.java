package com.beawan.library.bean;

import java.io.Serializable;

public class ReportItemTemp implements Serializable{
	
	/** @Field @serialVersionUID : TODO(这里用一句话描述这个字段的作用) */
	private static final long serialVersionUID = 827405957194791969L;

	private Long id;
	
	private String name;
	
	private Integer sort;
	

	private String needChangeName;
	
	private String type;
	
	public ReportItemTemp() {
	}

	public ReportItemTemp(String name, Integer sort,String needChangeName,String type) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.sort =sort;
		this.needChangeName =needChangeName;
		this.type =type;
	}

	public String getName() {
		return name;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNeedChangeName() {
		return needChangeName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public void setNeedChangeName(String needChangeName) {
		this.needChangeName = needChangeName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	

}
