package com.beawan.library.bean;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.beawan.library.bean.IndustryAnalysis;


/**
 * @ClassName IndustryAnalysis
 * @Description (行业分析)
 * @author czc
 * @Date 2017年5月23日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public class IndustryAnalysis implements Serializable {

	/** @Field @serialVersionUID : (这里用一句话描述这个字段的作用) */
	private static final long serialVersionUID = -5760657204594433934L;
	
	private static DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private Long id;

	/** @Field @parentId : (树状结构，父节点ID) */
	private String parentId;

	/** @Field @industryName : (行业名称) */
	private String industryName;
	
	/** @Field @industryAnalysis : (行业当前状况及前景分析) */
	private String industryAnalysis;
	
	
	/** @Field @receiveRatio : (应收账款结构占比平均范围) */
	private String receiveRatio;
	
	/** @Field @receiveRatio : (预付账款结构占比平均范围) */
	private String prePayRatio;
	
	/** @Field @receiveRatio : (存货结构占比平均范围) */
	private String stockRatio;
	
	/** @Field @receiveRatio : (固定资产结构占比平均范围) */
	private String assetsRatio;
	
	/** @Field @receiveRatio : (行业毛利率平均范围) */
	private String profitRatio;
	
	/** @Field @receiveRatio : (一年以内应收账款计提政策) */
	private String oneYearReceivePolicy;
	
	/** @Field @receiveRatio : (1-2年应收账款计提政策) */
	private String twoYearReceivePolicy;
	
	/** @Field @receiveRatio : (2-3年应收账款计提政策) */
	private String threeYearReceivePolicy;
	
	/** @Field @receiveRatio : (3-4年应收账款计提政策) */
	private String fourYearReceivePolicy;
	
	/** @Field @receiveRatio : (4-5年应收账款计提政策) */
	private String fiveYearReceivePolicy;
	
	/** @Field @receiveRatio : (5年以上应收账款计提政策) */
	private String moreFiveYearReceivePolicy;
	
	/** @Field @receiveRatio : (应收账款计提政策) */
	private String receivePolicy;

	/** @Field @receiveRatio : (存货计提政策) */
	private String stockPolicy;

	/** @Field @treeDataStd : (与国标行业相关联，通过国标行业的STD) */
	private String treeDataStd;
	
	private List<IndustryAnalysis> children;
	
	/** @Field @industryCharac : (行业特征) */
	private String industryCharac;
	
	/** @Field @updateTime : (更新时间) */
	private Date updateTime = new Date();
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getIndustryName() {
		return industryName;
	}

	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}

	public String getIndustryAnalysis() {
		return industryAnalysis;
	}

	public void setIndustryAnalysis(String industryAnalysis) {
		this.industryAnalysis = industryAnalysis;
	}


	public String getTreeDataStd() {
		return treeDataStd;
	}

	public void setTreeDataStd(String treeDataStd) {
		this.treeDataStd = treeDataStd;
	}

	public String getIndustryCharac() {
		return industryCharac;
	}

	public void setIndustryCharac(String industryCharac) {
		this.industryCharac = industryCharac;
	}

	public String getReceiveRatio() {
		return receiveRatio;
	}

	public void setReceiveRatio(String receiveRatio) {
		this.receiveRatio = receiveRatio;
	}

	public String getPrePayRatio() {
		return prePayRatio;
	}

	public void setPrePayRatio(String prePayRatio) {
		this.prePayRatio = prePayRatio;
	}

	public String getStockRatio() {
		return stockRatio;
	}

	public void setStockRatio(String stockRatio) {
		this.stockRatio = stockRatio;
	}

	public String getAssetsRatio() {
		return assetsRatio;
	}

	public void setAssetsRatio(String assetsRatio) {
		this.assetsRatio = assetsRatio;
	}

	public String getProfitRatio() {
		return profitRatio;
	}

	public void setProfitRatio(String profitRatio) {
		this.profitRatio = profitRatio;
	}


	public String getOneYearReceivePolicy() {
		return oneYearReceivePolicy;
	}

	public void setOneYearReceivePolicy(String oneYearReceivePolicy) {
		this.oneYearReceivePolicy = oneYearReceivePolicy;
	}

	public String getTwoYearReceivePolicy() {
		return twoYearReceivePolicy;
	}

	public void setTwoYearReceivePolicy(String twoYearReceivePolicy) {
		this.twoYearReceivePolicy = twoYearReceivePolicy;
	}

	public String getThreeYearReceivePolicy() {
		return threeYearReceivePolicy;
	}

	public void setThreeYearReceivePolicy(String threeYearReceivePolicy) {
		this.threeYearReceivePolicy = threeYearReceivePolicy;
	}

	public String getFourYearReceivePolicy() {
		return fourYearReceivePolicy;
	}

	public void setFourYearReceivePolicy(String fourYearReceivePolicy) {
		this.fourYearReceivePolicy = fourYearReceivePolicy;
	}

	public String getFiveYearReceivePolicy() {
		return fiveYearReceivePolicy;
	}

	public void setFiveYearReceivePolicy(String fiveYearReceivePolicy) {
		this.fiveYearReceivePolicy = fiveYearReceivePolicy;
	}

	public String getMoreFiveYearReceivePolicy() {
		return moreFiveYearReceivePolicy;
	}

	public void setMoreFiveYearReceivePolicy(String moreFiveYearReceivePolicy) {
		this.moreFiveYearReceivePolicy = moreFiveYearReceivePolicy;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getStockPolicy() {
		return stockPolicy;
	}

	public void setStockPolicy(String string) {
		this.stockPolicy = string;
	}

	public String getReceivePolicy() {
		return receivePolicy;
	}

	public void setReceivePolicy(String receivePolicy) {
		this.receivePolicy = receivePolicy;
	}

	public List<IndustryAnalysis> getChildren() {
		return children;
	}

	public void setChildren(List<IndustryAnalysis> children) {
		this.children = children;
	}

	public Date getUpdateTime() {
		if (this.updateTime == null) {
			return new Date();
		}
		return this.updateTime;
	}

	public String getUpdateTimeString() {
		if (this.updateTime == null) {
			return "";
		}
		return IndustryAnalysis.dateTimeFormat.format(this.updateTime);
	}
}
