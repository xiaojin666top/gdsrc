package com.beawan.library.bean;

/**
 * @ClassName IndustryData
 * @Description TODO(行业现状及未来发展前景，存储表)
 * @author czc
 * @Date 2018年3月4日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public class IndustryData {
	private Long id;
	//行业id
	private String stdId;
	//行业分析url
	private String url;
	
	private String enName;
	
	
	
	public String getEnName() {
		return enName;
	}
	public void setEnName(String enName) {
		this.enName = enName;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getStdId() {
		return stdId;
	}
	public void setStdId(String stdId) {
		this.stdId = stdId;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
}
