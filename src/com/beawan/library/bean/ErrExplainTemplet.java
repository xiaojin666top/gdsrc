package com.beawan.library.bean;

import java.io.Serializable;

/**
 * @ClassName ErrExplaIntegeremplet
 * @Description (异常项中系统提供的解释说明以及pad端选择的解释说明_模板)
 * @author czc
 * @Date 2017年5月15日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public class ErrExplainTemplet implements Serializable{

	private static final long serialVersionUID = -1985012812955667458L;

	private Long id;

	/** @Field @isRequired : (名字) */
	private String name;

	/** @Field @isRequired : (结论是否需要客户意见 0 不需要 1需要 2不要名字（净资产收益率）) */
	private Integer isRequired = 0;

	/** @Field @errKnoId : (与errkno模板相关联的字段，存放异常模板的id) */
	private Long errTempletId;

	/** @Field @custRejection : (拒绝原因) */
	private String custRejection;

	/** @Field @seat : (位置字段) */
	private Integer seat;

	/** @Field @seat : (备注) */
	private String remark;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public ErrExplainTemplet() {
		super();
	}

	public Integer getIsRequired() {
		return isRequired;
	}

	public void setIsRequired(Integer isRequired) {
		this.isRequired = isRequired;
	}

	public String getCustRejection() {
		return custRejection;
	}

	public void setCustRejection(String custRejection) {
		this.custRejection = custRejection;
	}

	public Integer getSeat() {
		return seat;
	}

	public void setSeat(Integer seat) {
		this.seat = seat;
	}

	public Long getErrTempletId() {
		return errTempletId;
	}

	public void setErrTempletId(Long errTempletId) {
		this.errTempletId = errTempletId;
	}

}
