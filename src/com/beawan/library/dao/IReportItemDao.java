package com.beawan.library.dao;

import java.util.List;

import com.beawan.library.bean.ReportItemTemp;

public interface IReportItemDao {
	
	public void saveReportItem(ReportItemTemp reportItemTemp);
	
	public void deleteReportItem(ReportItemTemp reportItemTemp);
	
	public ReportItemTemp getReportItemByName(String name);
	
	public List<ReportItemTemp> getAllReportItemByType(String type);

}
