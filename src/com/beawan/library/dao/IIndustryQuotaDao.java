package com.beawan.library.dao;

import java.util.Map;

import com.beawan.library.bean.IndustryQuota;

public interface IIndustryQuotaDao {
	
	public Map<String, Object> getIndustryQuota(String query, int index, int count, Object... args);
	
	public IndustryQuota queryByIndustryCode(String industryCode);
}
