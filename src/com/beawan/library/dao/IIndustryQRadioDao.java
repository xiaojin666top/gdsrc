package com.beawan.library.dao;

import java.util.List;
import java.util.Map;

import com.beawan.common.dao.IBaseDao;
import com.beawan.common.dao.impl.SqlParam;
import com.beawan.library.bean.IndustryQRadio;
import com.beawan.web.IRequestSerializer;

public interface IIndustryQRadioDao extends IBaseDao<IndustryQRadio> {
	
	public SqlParam getSqlParam(IRequestSerializer request);
	
	List<IndustryQRadio> getRadioDataQuota(String quotaId, String scope);
	
	Map<String, Object> getQuotaRadioData(String query, int index, int count, Object... args);
}
