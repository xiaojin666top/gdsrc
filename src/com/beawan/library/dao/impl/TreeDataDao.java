package com.beawan.library.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.beawan.common.dao.impl.BaseDao;
import com.beawan.common.dao.impl.SqlParam;
import com.beawan.library.bean.TreeData;
import com.beawan.library.dao.ITreeDataDao;
import com.beawan.web.IRequestSerializer;

@Repository("treeDataDao")
public class TreeDataDao extends BaseDao<TreeData> implements ITreeDataDao {
	
	@Override
	public SqlParam getSqlParam(IRequestSerializer request) {
		return null;
	}

	@Override
	public List<TreeData> getTreeDataAbvenName(String abvenName, String optType) {
		return this.select("abvenName = ? and optType = ?", abvenName, optType);
	}

	@Override
	public Map<String, Object> getTreeData(String query, int index, int count, Object... args) {
		return null;
	}

	@Override
	public List<TreeData> getTreeDataByLocate(String locate,String optType) {
		return this.select("locate = ? and  optType = ?", locate,optType);
	}
	
	@Override
	public TreeData getTreeDataById(String id){
		return  this.selectSingle("stdId = ?", id);
	}
	
	@Override
	public TreeData geTreeDataByEnName(String enName,String optType){
		return this.selectSingle("enName = ? and optType = ?", enName, optType);
	}
	
	@Override
	public TreeData geTreeDataByCnName(String cnName,String optType){
		return  this.selectSingle("cnName = ? and optType = ?", cnName,optType);
	}
}
