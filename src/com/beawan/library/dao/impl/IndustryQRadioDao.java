package com.beawan.library.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.beawan.common.dao.impl.BaseDao;
import com.beawan.common.dao.impl.SqlParam;
import com.beawan.library.bean.IndustryQRadio;
import com.beawan.library.dao.IIndustryQRadioDao;
import com.beawan.web.IRequestSerializer;

@Repository("industryQRadioDao")
public class IndustryQRadioDao extends BaseDao<IndustryQRadio> implements IIndustryQRadioDao {
	
	@Override
	public SqlParam getSqlParam(IRequestSerializer request) {
		return null;
	}

	@Override
	public List<IndustryQRadio> getRadioDataQuota(String quotaId, String scope) {
		return this.select("quotaId = ? and scopeRange = ?", quotaId, scope);
	}

	@Override
	public Map<String, Object> getQuotaRadioData(String query, int index, int count, Object... args) {
		return null;
	}
}
