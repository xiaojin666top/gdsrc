package com.beawan.library.dao.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.beawan.common.dao.impl.DaoHandler;
import com.beawan.common.dao.impl.SerializeDao;
import com.beawan.library.bean.ErrExplainTemplet;
import com.beawan.library.dao.IErrExplTempDao;

@Repository("errExplTempDao")
public class ErrExplTempDaoimpl extends DaoHandler implements IErrExplTempDao{

	@Override
	public ErrExplainTemplet getErrExplainTempByName(String name) {
		// TODO Auto-generated method stub
		String query = "from ErrExplainTemplet as model where model.name ='" + name +"'";
		List<ErrExplainTemplet> list = getEntityManager().createQuery(query).getResultList();
		if(list.size() > 0){
			return list.get(0);
		}else{
			return null;
		}
	}

	@Override
	public ErrExplainTemplet getErrExplainTempById(Long id) {
		// TODO Auto-generated method stub
		String query = "from ErrExplainTemplet as model where model.id ='" + id +"'";
		List<ErrExplainTemplet> list = getEntityManager().createQuery(query).getResultList();
		if(list.size() > 0){
			return list.get(0);
		}else{
			return null;
		}
	}

	@Override
	public void saveErrExplainTemplet(ErrExplainTemplet data) throws DataAccessException {
		// TODO Auto-generated method stub
		save(ErrExplainTemplet.class, data);
	}

	@Override
	public void deleteErrExplainTemplet(ErrExplainTemplet data) throws DataAccessException {
		// TODO Auto-generated method stub
		delete(ErrExplainTemplet.class, data);
	}

	@Override
	public List<ErrExplainTemplet> getErrExplainTempletByErrTempletId(Long errTempletId){
		// TODO Auto-generated method stub
		String query = "from ErrExplainTemplet as model where model.errTempletId ='" + errTempletId +"' order by seat";
		List<ErrExplainTemplet> list = getEntityManager().createQuery(query).getResultList();
		if(list.size() > 0){
			return list;
		}else{
			return null;
		}
	}
	
	

}
