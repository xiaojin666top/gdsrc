package com.beawan.library.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.beawan.common.dao.impl.BaseDao;
import com.beawan.common.dao.impl.SqlParam;
import com.beawan.library.bean.IndustryQuota;
import com.beawan.library.dao.IIndustryQuotaDao;
import com.beawan.web.IRequestSerializer;

@Repository("industryQuotaDao")
public class IndustryQuotaDao extends BaseDao<IndustryQuota> implements IIndustryQuotaDao {
	
	@Override
	public Map<String, Object> getIndustryQuota(String query, int index, int count, Object... args) {
		index = index < 0 ? 0 : index;
		count = count < 0 ? 0 : count;
		long total = this.selectCount(query, args);
		List<IndustryQuota> entities = this.selectRange(query, index, count, args);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("total", total);
		json.put("rows", entities);
		return json;
	}
	
	public IndustryQuota queryByIndustryCode(String industryCode){
		String query = "from IndustryQuota where industryCode = ?";
		return this.selectSingle(query, industryCode);
	}
	
}
