package com.beawan.library.dao.impl;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.beawan.common.dao.impl.DaoHandler;
import com.beawan.library.bean.ReportTemplet;
import com.beawan.library.dao.IReportTempletDao;

@Repository("reportTempletDao")
public class ReportTempletDao extends DaoHandler implements IReportTempletDao {

	@Override
	public ReportTemplet getReportTempletById(Long id){
		String query = "from ReportTemplet as model where id ='" + id +"'";
		List<ReportTemplet> list = getEntityManager().createQuery(query).getResultList();
		if(list.size() > 0){
			return list.get(0);
		}else{
			return null;
		}
	}
	
	@Override
	public ReportTemplet getReportTempletByName(String name) {
		String query = "from ReportTemplet as model where model.name ='" + name +"'";
		List<ReportTemplet> list = getEntityManager().createQuery(query).getResultList();
		if(list.size() > 0){
			return list.get(0);
		}else{
			return null;
		}
	}

	@Override
	public List<ReportTemplet> getAllReportTemplets() {
		String query = "from ReportTemplet";
		List<ReportTemplet> list = getEntityManager().createQuery(query).getResultList();
		if(list.size() > 0){
			return list;
		}else{
			return null;
		}
	}
	
	
	@Override
	public void deleteReportTemplet(ReportTemplet data) throws DataAccessException{
		delete(ReportTemplet.class, data);
	}
	@Override
	public void saveReportTemplet(ReportTemplet data) throws DataAccessException{
		save(ReportTemplet.class, data);
	}
	


}
