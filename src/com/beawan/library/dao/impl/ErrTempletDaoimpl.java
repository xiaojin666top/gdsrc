package com.beawan.library.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.beawan.common.dao.impl.DaoHandler;
import com.beawan.library.bean.ErrTemplet;
import com.beawan.library.dao.IErrTempletDao;

@Repository("errTempletDao")
public class ErrTempletDaoimpl extends DaoHandler implements IErrTempletDao {

	@Override
	public ErrTemplet getErrTempletByName(String name) {
		// TODO Auto-generated method stub
		String query = "from ErrTemplet as model where model.name ='" + name + "'";
		List<ErrTemplet> list = getEntityManager().createQuery(query).getResultList();
		if (list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}
	}

	@Override
	public List<ErrTemplet> getAllErrTemplet() {
		// TODO Auto-generated method stub
		String query = "from ErrTemplet";
		List<ErrTemplet> list = getEntityManager().createQuery(query).getResultList();
		if (list.size() > 0) {
			return list;
		} else {
			return null;
		}
	}

	@Override
	public Map<String, Object> getErrks(String query, int index, int count, Object... args) {
		// TODO Auto-generated method stub
		index = index < 0 ? 0 : index;
		count = count < 0 ? 0 : count;
		long total = selectCount(ErrTemplet.class, query, args);
		List<ErrTemplet> entities = selectRange(ErrTemplet.class, query, index, count, args);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("total", total);
		json.put("rows", entities);
		return json;
	}

	@Override
	public ErrTemplet getErrTempletById(Long id) {
		// TODO Auto-generated method stub
		String query = "from ErrTemplet as model where model.id ='" + id + "'";
		List<ErrTemplet> list = getEntityManager().createQuery(query).getResultList();
		if (list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}
	}

	@Override
	public ErrTemplet saveErrTemplet(ErrTemplet data) throws DataAccessException {
		// TODO Auto-generated method stub
		return save(ErrTemplet.class, data);
	}

	@Override
	public void deleteErrTemplet(ErrTemplet data) throws DataAccessException {
		delete(ErrTemplet.class, data);
	}

}
