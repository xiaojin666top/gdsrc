package com.beawan.library.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.beawan.base.dao.ISysTreeDicDAO;
import com.beawan.common.SysConstants;
import com.beawan.common.dao.impl.DaoHandler;
import com.beawan.library.bean.InduSituation;
import com.beawan.library.bean.IndustryAnalysis;
import com.beawan.library.bean.IndustryData;
import com.beawan.library.bean.TreeData;
import com.beawan.library.dao.IIndustryAnalysisDao;
import com.beawan.library.dao.ITreeDataDao;

@Repository("industryAnalysisDao")
public class IndustrAnalysisDao extends DaoHandler implements IIndustryAnalysisDao {

	@Resource
	protected ISysTreeDicDAO sysTreeDicDAO;
	
	@Resource
	protected ITreeDataDao treeDataDao;
	
	@Override
	public IndustryAnalysis getIndustryById(Long id){
		String query = "from IndustryAnalysis as model where id ='" + id +"'";
		List<IndustryAnalysis> list = getEntityManager().createQuery(query).getResultList();
		if(list.size() > 0){
			return list.get(0);
		}else{
			return null;
		}
	}
	
	@Override
	public IndustryAnalysis getIndustryByName(String name) {
		String query = "from IndustryAnalysis as model where industryName ='" + name +"'";
		List<IndustryAnalysis> list = getEntityManager().createQuery(query).getResultList();
		if(list.size() > 0){
			return list.get(0);
		}else{
			return null;
		}
	}
	
	@Override
	public List<IndustryAnalysis> getAllIndustryAnalysis() {
		// TODO Auto-generated method stub
		String query = "from IndustryAnalysis";
		List<IndustryAnalysis> list = getEntityManager().createQuery(query).getResultList();
		if(list.size() > 0){
			return list;
		}else{
			return null;
		}
	}

	@Override
	public IndustryAnalysis getIndustryAnalysisByTreeDataId(String treeDataStd) {
		String query = "from IndustryAnalysis as model where treeDataStd ='" + treeDataStd +"'";
		List<IndustryAnalysis> list = getEntityManager().createQuery(query).getResultList();
		if(list.size() > 0){
			return list.get(0);
		}else{
			return null;
		}
	}
	
	@Override
	public void saveIndustry(IndustryAnalysis data){
		save(IndustryAnalysis.class, data);
	}
	
	@Override
	public void deleteIndustry(IndustryAnalysis data){
		delete(IndustryAnalysis.class, data);
	}

	@Override
	public Map<String, Object> getIndustryAnas(String query, int index, int count, Object... args) {
		// TODO Auto-generated method stub
		index = index < 0 ? 0 : index;
		count = count < 0 ? 0 : count;
		Long industryCount = selectCount(IndustryAnalysis.class, query, args);
		List<IndustryAnalysis> entities = selectRange(IndustryAnalysis.class, query, index, count, args);
		for (IndustryAnalysis industryAnalysis : entities) {
			TreeData treedata = sysTreeDicDAO.getTreeDataById(industryAnalysis.getTreeDataStd());
			if (treedata != null) {
				String str = "";
				String lastIndustry = treedata.getCnName();
				String locate = treedata.getLocate();
				String[] spi = locate.split(",");
				if (spi.length > 2 ) {
					for(int i=1;i<spi.length-1;i++){
						TreeData temp = sysTreeDicDAO.geTreeDataByEnName(spi[i],
								SysConstants.TreeDicConstant.GB_IC_4754_2017);
						str += temp.getCnName()+"——";
					}
				}
				str += lastIndustry;
				industryAnalysis.setTreeDataStd(str);
			}
		}
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("total", industryCount);
		json.put("rows", entities);
		return json;
	}
	
	/*8
	 * (non-Javadoc)
	 * @查询行业分析zyp
	 */
	@Override
	public IndustryData getIndustryByEnName(String enName) {
		TreeData treeData = treeDataDao.geTreeDataByEnName(enName,"STD_GB_4754-2017");
		if(treeData != null){
			String[] locate1= treeData.getLocate().split(",");
			if(locate1.length > 1){
				for(int i=1; i<locate1.length; i++){
					String locate2 = locate1[locate1.length-i];
					String query = "from IndustryData as model where enName ='" + locate2 +"'";
					List<IndustryData> list = getEntityManager().createQuery(query).getResultList();
					if(list != null&&list.size()>0){
						return list.get(0);
					}
				}
			}
		}
		return null;
		
	}
	
	
	/*8
	 * (non-Javadoc)
	 * @查询行业编号zyp
	 */
	@Override
	public IndustryData getIndustryByCnName(String cnName) {
		TreeData treeData = treeDataDao.geTreeDataByCnName(cnName,"STD_GB_4754-2017");
		if(treeData != null && treeData.getEnName() != null){
			String enNameOne = treeData.getEnName();
			String query = "from IndustryData as model where enName ='" + treeData.getEnName() +"'";
			List<IndustryData> list = getEntityManager().createQuery(query).getResultList();
				if(list.size() == 0){
					String enName3 = "";
					String enName2 = "";
					 enName3 = enNameOne.substring(0,4);
					 enName2 = enNameOne.substring(0,3);
					String query2 = "from IndustryData as model where enName ='" + enName3 +"'";
					List<IndustryData> list2 = getEntityManager().createQuery(query2).getResultList();
					if(list2.size() > 0){
						return list2.get(0);
					}else{
						String query3 = "from IndustryData as model where enName ='" + enName2 +"'";
						List<IndustryData> list3 = getEntityManager().createQuery(query3).getResultList();
						if(list2.size() > 0){
							return list2.get(0);
						}else{
							return null;
						}
					}
				}
			if(list.size() > 0){
				return list.get(0);
			}else{
				return null;
			}
		}
		return null;
	}
	
	@Override
	public IndustryData getIndustryDataByStid(String stid) {
		String query = "from IndustryData as model where stdId ='" + stid +"'";
		List<IndustryData> list = getEntityManager().createQuery(query).getResultList();
		if(list.size() > 0){
			return list.get(0);
		}else{
			return null;
		}
	}
	
	@Override
	public int getIndustryByLikeName(String enName) {
		String hql = "enName like:enName";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("enName", enName+"%");
		List<IndustryData> industryDatas=select(IndustryData.class, hql, params);
		return industryDatas.size();
	}

	@Override
	public InduSituation getIndusSituationByName(String enName) {

		String query = "from InduSituation as model where model.enName ='" + enName +"'";
		List<InduSituation> list = getEntityManager().createQuery(query).getResultList();
		if(list.size() > 0){
			return list.get(0);
		}else{
			return null;
		}
		
	}
}
