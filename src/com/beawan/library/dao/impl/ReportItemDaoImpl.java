package com.beawan.library.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.beawan.common.dao.impl.DaoHandler;
import com.beawan.library.bean.ReportItemTemp;
import com.beawan.library.dao.IReportItemDao;
import com.beawan.survey.custInfo.bean.CompBase;
import com.beawan.survey.custInfo.bean.PersonBase;
@Repository("reportItemDao")
public class ReportItemDaoImpl  extends DaoHandler implements IReportItemDao{

	@Override
	public void saveReportItem(ReportItemTemp reportItemTemp) {
		// TODO Auto-generated method stub
		save(ReportItemTemp.class, reportItemTemp);
	}

	@Override
	public void deleteReportItem(ReportItemTemp reportItemTemp) {
		// TODO Auto-generated method stub
		delete(ReportItemTemp.class, reportItemTemp);
	}

	@Override
	public ReportItemTemp getReportItemByName(String name) {
		// TODO Auto-generated method stub
		String hql = "name=:name";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("name", name);
		return selectSingle(ReportItemTemp.class, hql, params);
	}

	@Override
	public List<ReportItemTemp> getAllReportItemByType(String type) {
		// TODO Auto-generated method stub
		String hql = "type=:type order by sort";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("type", type);
		return select(ReportItemTemp.class, hql, params);
	}

}
