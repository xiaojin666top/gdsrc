package com.beawan.library.dao;

import java.util.List;
import java.util.Map;

import com.beawan.library.bean.InduSituation;
import com.beawan.library.bean.IndustryAnalysis;
import com.beawan.library.bean.IndustryData;

public interface IIndustryAnalysisDao {
	
	/**
	 * @Description (通过id查找行业分析)
	 * @param id
	 * @return
	 */
	public IndustryAnalysis getIndustryById(Long id);
	
	/**
	 * @Description (通过name查找行业分析)
	 * @param id
	 * @return
	 */
	public IndustryAnalysis getIndustryByName(String name);
	
	
	/**
	 * @Description (通过name查找行业概况)
	 * @param enName
	 * @return
	 */
	public InduSituation getIndusSituationByName(String enName);
	
	/**
	 * @Description (保存行业分析)
	 * @param id
	 * @return
	 */
	public void saveIndustry(IndustryAnalysis data);
	
	/**
	 * @Description (删除行业分析)
	 * @param id
	 * @return
	 */
	public void deleteIndustry(IndustryAnalysis data);
	
	List<IndustryAnalysis> getAllIndustryAnalysis();
	
	IndustryAnalysis getIndustryAnalysisByTreeDataId(String id);
	
	Map<String, Object> getIndustryAnas(String query, int index, int count, Object... args);
	
	public IndustryData getIndustryByEnName(String enName);
	/**
	 * 
	 * @param cnName
	 * @return
	 * 根据行业名称查询行业编号
	 */
	public IndustryData getIndustryByCnName(String cnName);
	
	public IndustryData getIndustryDataByStid(String stid);
	public int getIndustryByLikeName(String enName);

}
