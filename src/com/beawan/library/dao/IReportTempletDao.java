package com.beawan.library.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.library.bean.ReportTemplet;

public interface IReportTempletDao {
	
	/**
	 * @Description (通过id查询)
	 * @param id 
	 * @return
	 */
	public ReportTemplet getReportTempletById(Long id);
	
	/**
	 * @Description (通过表名查询)
	 * @param name 表名
	 * @return
	 */
	public ReportTemplet getReportTempletByName(String name);

	/**
	 * @Description (查询所有表格)
	 * @return
	 */
	public List<ReportTemplet> getAllReportTemplets();

	/**
	 * @Description (保存表格)
	 * @param data 表格实体
	 */
	public void saveReportTemplet(ReportTemplet data) throws DataAccessException;
	
	/**
	 * @Description (刪除表格)
	 * @param data	表格实体
	 */
	public void deleteReportTemplet(ReportTemplet data) throws DataAccessException;
	
	
}
