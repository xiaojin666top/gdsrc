package com.beawan.library.dao;

import java.util.List;
import java.util.Map;

import com.beawan.common.dao.impl.SqlParam;
import com.beawan.library.bean.TreeData;
import com.beawan.web.IRequestSerializer;

public interface ITreeDataDao {
	
	public SqlParam getSqlParam(IRequestSerializer request);
	
	List<TreeData> getTreeDataByLocate(String locate,String optType);
	
	List<TreeData> getTreeDataAbvenName(String abvenName, String optType);

	Map<String, Object> getTreeData(String query, int index, int count, Object... args);
	
	public TreeData getTreeDataById(String id);
	
	TreeData geTreeDataByCnName(String cnName, String optType);
	
	TreeData geTreeDataByEnName(String enName, String optType);
	
}
