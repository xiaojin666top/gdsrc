package com.beawan.library.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ISerializeDao;
import com.beawan.library.bean.ErrExplainTemplet;

public interface IErrExplTempDao{

	/**
	 * @Description (通过名字查询ErrExplainTemplet)
	 * @param name
	 * @return
	 */
	public ErrExplainTemplet getErrExplainTempByName(String name);
	
	/**
	 * @Description (通过id查询ErrExplainTemplet)
	 * @param id
	 * @return
	 */
	public ErrExplainTemplet getErrExplainTempById(Long id);
	
	
	/**
	 * @Description (保存ErrExplainTemplet)
	 * @param data
	 * @return
	 */
	public void saveErrExplainTemplet(ErrExplainTemplet data) throws DataAccessException;
	
	
	/**
	 * @Description (删除ErrExplainTemplet)
	 * @param data
	 * @return
	 */
	public void deleteErrExplainTemplet(ErrExplainTemplet data) throws DataAccessException;
	
	/**
	 * @Description (得到与异常模板关联的解释原因)
	 * @param data
	 * @return
	 */
	public List<ErrExplainTemplet> getErrExplainTempletByErrTempletId(Long errTempletId);
	
	
}
