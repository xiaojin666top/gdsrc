package com.beawan.library.dao;

import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;

import com.beawan.library.bean.ErrTemplet;


public interface IErrTempletDao {

	/**
	 * @Description (通过名字查询errkno)
	 * @param name
	 * @return
	 */
	public ErrTemplet getErrTempletByName(String name);
	
	/**
	 * @Description (通过id查询errkno)
	 * @param name
	 * @return
	 */
	public ErrTemplet getErrTempletById(Long id);
	
	/**
	 * @Description (查询所有异常模板)
	 * @return
	 */
	public List<ErrTemplet> getAllErrTemplet();
	
	/**
	 * @Description (异常模板的表格数据)
	 * @param query 查询语句
	 * @param index	位置
	 * @param count	每页显示页数
	 * @param args
	 * @return
	 */
	Map<String, Object> getErrks(String query, int index, int count, Object... args);
	
	/**
	 * @Description (保存errkno)
	 * @param data
	 * @return
	 */
	public ErrTemplet saveErrTemplet(ErrTemplet data) throws DataAccessException;
	
	/**
	 * @Description (删除errkno)
	 * @param data
	 * @return
	 */
	public void deleteErrTemplet(ErrTemplet data) throws DataAccessException;
	
}
