package com.beawan.library.uitls;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.beawan.main.tool.FileResource;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import com.beawan.common.SysConstants;
import com.beawan.library.bean.ErrExplainTemplet;
import com.beawan.library.bean.ErrTemplet;
import com.beawan.library.bean.IndustryAnalysis;
import com.beawan.library.bean.ReportTemplet;
import com.beawan.library.bean.TreeData;
import com.beawan.library.service.IIndustrySV;
import com.beawan.library.service.ITempletSV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;

public class InitKnowledge {

	// 初始化表格知识库
	public String initdatabase(HttpServletRequest request, HttpServletResponse response, ITaskSV taskSV,
			ITempletSV templetService) {
		// String path =
		// request.getServletContext().getRealPath("/").replace("\\", "/");
		// FileResource f = new FileResource();
		// File file =f.getReportFile();

		FileInputStream fileInputStream = null;
		List<ReportTemplet> deleteReports = templetService.getAllReportTemplets();
		List<Task> deleteTasks = taskSV.getAllTasks();
		if (deleteReports != null && deleteReports.size() > 0) {
			for (ReportTemplet report : deleteReports) {
				templetService.deleteReportTemplet(report);
			}
		}
		
		try {
			
			if (deleteTasks != null && deleteTasks.size() > 0) {
				for (Task task : deleteTasks) {
					taskSV.deleteTask(task);
				}
			}
			
			FileResource f = new FileResource();
			InputStream is = f.getReportFile();
			Document doc = new SAXReader().read(is);
			List<ReportTemplet> reportsList = null;
			reportsList = doc.selectNodes("//reports/table");
			for (Iterator<ReportTemplet> reportIter = reportsList.iterator(); reportIter.hasNext();) {
				List<ReportTemplet> reportList = new ArrayList<ReportTemplet>();
				Element reportElement = (Element) reportIter.next();
				reportList = reportElement.selectNodes("report");
				for (Iterator<ReportTemplet> reportIterator = reportList.iterator(); reportIterator.hasNext();) {
					Element tableElement = (Element) reportIterator.next();
					ReportTemplet report = new ReportTemplet();
					report.setName(tableElement.attributeValue("name"));
					report.setUrl(tableElement.attributeValue("url"));
					report.setReportType(tableElement.attributeValue("reporttype"));
					report.setShowUrl(tableElement.attributeValue("showurl"));
					report.setRemark(tableElement.attributeValue("remark"));
					report.setItem(tableElement.attributeValue("item"));
					// report.setIndustry(industry);
					templetService.saveReportTemplet(report);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "index";
	}

	// 初始化异常知识库
	public String initErrTempletKnowledge(HttpServletRequest request, HttpServletResponse response,
			ITempletSV templetService) {
		try {
			FileResource f = new FileResource();
			InputStream is = f.getErrKnowledgeFile();
			Document doc = new SAXReader().read(is);
			List<ErrTemplet> err_knos = doc.selectNodes("//knowledge/error");
			for (Iterator<ErrTemplet> ErrTemplet_Iter = err_knos.iterator(); ErrTemplet_Iter.hasNext();) {
				Element err_element = (Element) ErrTemplet_Iter.next();
				ErrTemplet ere = new ErrTemplet();

				ere.setName(err_element.attributeValue("name"));
				ere.setUrl(err_element.attributeValue("url"));
				ere.setRemark(err_element.attributeValue("remark"));
				ere.setRateval(err_element.attributeValue("rateval"));
				ere.setIsedit(err_element.attributeValue("isedit"));
				ere.setFormula(err_element.attributeValue("formula"));
				List<ReportTemplet> reports = templetService.getAllReportTemplets();
				
				for (ReportTemplet report : reports) {
					if (ere.getRemark() != null && report.getRemark().equals(ere.getRemark())) {
						ere.setReportTempId(report.getId());
						break;
					}
				}
				ere = templetService.saveErrTemplet(ere);
				
				List<ErrExplainTemplet> caseChild = err_element.selectNodes("item");
				int seat = 0;
				for (Iterator<ErrExplainTemplet> case_Iter = caseChild.iterator(); case_Iter.hasNext();) {
					Element case_element = (Element) case_Iter.next();
					ErrExplainTemplet case1 = new ErrExplainTemplet();
					case1.setName(case_element.attributeValue("name"));
					case1.setSeat(seat);
					if (case_element.attributeValue("detail") != null) {
						case1.setIsRequired(Integer.valueOf(case_element.attributeValue("detail")));
					}
					case1.setErrTempletId(ere.getId());
					templetService.saveErrExplainTemplet(case1);
					seat++;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "index";
	}

	// 初始化行业分析知识库
	public void initIndustryAnalysis(HttpServletRequest request, HttpServletResponse response,
			IIndustrySV industrySV) {
		try {
			FileResource f = new FileResource();
			InputStream is = f.getIndustryAnalysisFile();
			Document doc = new SAXReader().read(is);

			List<IndustryAnalysis> anaLists = null;
			anaLists = doc.selectNodes("//all");
			for (Iterator<IndustryAnalysis> anaIter = anaLists.iterator(); anaIter.hasNext();) {
				List<IndustryAnalysis> anaList = new ArrayList<IndustryAnalysis>();
				Element anaElement = (Element) anaIter.next();
				anaList = anaElement.selectNodes("industry");
				for (Iterator<IndustryAnalysis> anaIterator = anaList.iterator(); anaIterator.hasNext();) {
					Element indusAnaElement = (Element) anaIterator.next();
					IndustryAnalysis analysis = new IndustryAnalysis();
					String name = indusAnaElement.attributeValue("name");
					analysis.setIndustryName(name);
					analysis.setIndustryAnalysis(indusAnaElement.attributeValue("analysis"));
					analysis.setReceiveRatio(indusAnaElement.attributeValue("receiveRatio"));
					analysis.setPrePayRatio(indusAnaElement.attributeValue("prePay"));
					analysis.setStockRatio(indusAnaElement.attributeValue("stockRatio"));
					analysis.setAssetsRatio(indusAnaElement.attributeValue("assetsRatio"));
					analysis.setProfitRatio(indusAnaElement.attributeValue("profitRatio"));
					if (indusAnaElement.attributeValue("oneYearReceivePolicy") != null) {
						String one = indusAnaElement.attributeValue("oneYearReceivePolicy");
						analysis.setOneYearReceivePolicy(one);
						String two = indusAnaElement.attributeValue("twoYearReceivePolicy");
						analysis.setTwoYearReceivePolicy(two);
						String three = indusAnaElement.attributeValue("threeYearReceivePolicy");
						analysis.setThreeYearReceivePolicy(three);
						String four = indusAnaElement.attributeValue("fourYearReceivePolicy");
						analysis.setFourYearReceivePolicy(four);
						String five = indusAnaElement.attributeValue("fiveYearReceivePolicy");
						analysis.setFiveYearReceivePolicy(five);
						String moreFive = indusAnaElement.attributeValue("moreFiveYearReceivePolicy");
						analysis.setMoreFiveYearReceivePolicy(moreFive);
						String receivePolicy = "0-1年计提" + one + "%，1-2年计提" + two + "%，2-3年计提" + three + "%，3-4年计提"
								+ four + "%，4-5年计提" + five + "%，5年以上计提" + moreFive + "%。";
						analysis.setReceivePolicy(receivePolicy);
					}
					analysis.setStockPolicy(indusAnaElement.attributeValue("stockPolicy"));
					TreeData treeData = industrySV.getTreeDataByCnName(name,
							SysConstants.TreeDicConstant.GB_IC_4754_2017);
					if(treeData != null ){
						analysis.setTreeDataStd(treeData.getStdId());
					}
					industrySV.saveIndustryAnalysis(analysis);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
