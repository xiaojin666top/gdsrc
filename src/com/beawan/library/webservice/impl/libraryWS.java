package com.beawan.library.webservice.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.beawan.base.entity.SysDic;
import com.beawan.base.dao.ISysDicDAO;
import com.beawan.base.dao.ISysTreeDicDAO;
import com.beawan.common.SysConstants;
import com.beawan.common.util.WSResult;
import com.beawan.library.bean.ErrExplainTemplet;
import com.beawan.library.bean.ErrTemplet;
import com.beawan.library.bean.ReportTemplet;
import com.beawan.library.bean.TreeData;
import com.beawan.library.dao.IErrExplTempDao;
import com.beawan.library.dao.IErrTempletDao;
import com.beawan.library.dao.IReportTempletDao;
import com.beawan.library.webservice.ILibraryWS;
import com.platform.util.JacksonUtil;

@Service("libraryWS")
public class libraryWS implements ILibraryWS {
	
	private static final Logger log = Logger.getLogger(ILibraryWS.class);

	@Resource
	protected ISysTreeDicDAO sysTreeDicDAO;
	
	@Resource
	protected ISysDicDAO  sysDicDAO;
	
	@Resource
	protected IErrTempletDao  errTempletDao;
	
	@Resource
	protected IErrExplTempDao  errExplainDao;
	
	@Resource
	protected  IReportTempletDao reportTempletDao;

	@Override
	public String queryTreeData(String abvenName) {
		String result = null;
		try {
			List<TreeData> treeDataList = this.sysTreeDicDAO.getTreeDataAbvenName(abvenName,
					SysConstants.TreeDicConstant.GB_IC_4754_2017);
			if (treeDataList == null) {
				treeDataList = new ArrayList<TreeData>();
			}
			result = JacksonUtil.serialize(treeDataList);
		} catch (Exception e) {
			e.printStackTrace();
			result = "error";
			log.error("获取行业信息异常！", e);
		}
		return result;
	}
	
	@Override
	public String findByOptType(String optType) {
		String result = null;
		try {
			List<SysDic> dicList = sysDicDAO.queryByOptType(optType);
			if (dicList == null) {
				dicList = new ArrayList<SysDic>();
			}
			result = JacksonUtil.serialize(dicList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = "error";
			log.error("获取行业信息异常！", e);
		}
		return result;
	}

	@Override
	public String getReportTempletById(Long id) {
		// TODO Auto-generated method stub
		String result = null;
		try {
			ReportTemplet templet =reportTempletDao.getReportTempletById(id);
			result = JacksonUtil.serialize(templet);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = "error";
			log.error("获取明细表模板异常！", e);
		}
		return result;
	}

	@Override
	public String getReportTempletByName(String name) {
		// TODO Auto-generated method stub
		String result = null;
		try {
			ReportTemplet templet =reportTempletDao.getReportTempletByName(name);
			result = JacksonUtil.serialize(templet);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = "error";
			log.error("获取明细表模板异常！", e);
		}
		return result;
	}

	@Override
	public String getAllReportTemplets() {
		// TODO Auto-generated method stub
		String result = null;
		try {
			List<ReportTemplet> templet =reportTempletDao.getAllReportTemplets();
			result = JacksonUtil.serialize(templet);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = "error";
			log.error("获取所有明细表模板异常！", e);
		}
		return result;
	}

	@Override
	public String getErrTempletByName(String name) {
		// TODO Auto-generated method stub
		String result = null;
		try {
			ErrTemplet templet =errTempletDao.getErrTempletByName(name);
			result = JacksonUtil.serialize(templet);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = "error";
			log.error("获取异常模板失败！", e);
		}
		return result;
	}

	@Override
	public String getErrTempletById(Long id) {
		// TODO Auto-generated method stub
		String result = null;
		try {
			ErrTemplet templet =errTempletDao.getErrTempletById(id);
			result = JacksonUtil.serialize(templet);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = "error";
			log.error("获取异常模板失败！", e);
		}
		return result;
	}

	@Override
	public String getAllErrTemplet() {
		// TODO Auto-generated method stub
		String result = null;
		try {
			List<ErrTemplet> templet =errTempletDao.getAllErrTemplet();
			result = JacksonUtil.serialize(templet);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = "error";
			log.error("获取所有异常模板异常！", e);
		}
		return result;
	}

	@Override
	public String getErrExplainTempletByName(String name) {
		// TODO Auto-generated method stub
		String result = null;
		try {
			ErrExplainTemplet templet =errExplainDao.getErrExplainTempByName(name);
			result = JacksonUtil.serialize(templet);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = "error";
			log.error("获取解释说明模板失败！", e);
		}
		return result;
	}

	@Override
	public String getErrExplainTempletById(Long id) {
		// TODO Auto-generated method stub
		String result = null;
		try {
			ErrExplainTemplet templet =errExplainDao.getErrExplainTempById(id);
			result = JacksonUtil.serialize(templet);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = "error";
			log.error("获取解释说明模板失败！", e);
		}
		return result;
	}

	@Override
	public String getErrExplainTempletByErrTempletId(Long errKnoId) {
		// TODO Auto-generated method stub
		String result = null;
		try {
			List<ErrExplainTemplet> templet =errExplainDao.getErrExplainTempletByErrTempletId(errKnoId);
			result = JacksonUtil.serialize(templet);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = "error";
			log.error("获取所有解释说明模板异常！", e);
		}
		return result;
	}

	@Override
	public String queryParentTreeData(String enName) {
		WSResult result = new WSResult();
		
		try {
			TreeData treeData = this.sysTreeDicDAO.geTreeDataByEnName(enName,
					SysConstants.TreeDicConstant.GB_IC_4754_2017);
			if(treeData!=null) {
			result.data = JacksonUtil.serialize(treeData);
			}else {
				result.data =null;
			}
		} catch (Exception e) {
			e.printStackTrace();
			result.status = WSResult.STATUS_ERROR;
			log.error("获取行业信息异常！", e);
		}
		return result.json();
	}
}
