package com.beawan.library.webservice;

public interface ILibraryWS {

	/**
	 * @Description (查找一个行业的子行业)
	 * @param abvenName	行业的前一级代码
	 * @return
	 */
	public String queryTreeData(String abvenName);
	
	
	/**
	 * @Description (查找一个行业的父行业)
	 * @param enName	行业代码
	 * @return
	 */
	public String queryParentTreeData(String enName);
	
	/**
	 * @Description (通过类型查找常量表)
	 * @param optType
	 * @return
	 */
	public String findByOptType(String optType);
	
	
	/**
	 * @Description (通过id查找表格模板)
	 * @param id
	 * @return
	 */
	public String getReportTempletById(Long id);

	/**
	 * @Description (通过表格名称查找表格模板)
	 * @param name
	 * @return
	 */
	public String getReportTempletByName(String name);


	/**
	 * @Description (找出所有表格模板)
	 * @return
	 */
	public String  getAllReportTemplets();

	/* ReportTemplet end */
	
	/* ErrorKno */
	
	/**
	 * @Description (通过名字查询ErrTemplet)
	 * @param name
	 * @return
	 */
	public String getErrTempletByName(String name);
	
	/**
	 * @Description (通过id查询ErrTemplet)
	 * @param name
	 * @return
	 */
	public String getErrTempletById(Long id);
	
	/**
	 * @Description (查询所有异常模板)
	 * @return
	 */
	public String getAllErrTemplet();
	
	/**
	 * @Description (通过名字查询ErrExplainTemplet)
	 * @param name
	 * @return
	 */
	public String getErrExplainTempletByName(String name);
	
	/**
	 * @Description (通过id查询ErrExplainTemplet)
	 * @param id
	 * @return
	 */
	public String getErrExplainTempletById(Long id);
	
	/**
	 * @Description (得到与errKno关联的ErrExplainTemplet)
	 * @param data
	 * @return
	 */
	public String getErrExplainTempletByErrTempletId(Long errKnoId);
	
	

}
