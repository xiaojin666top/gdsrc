package com.beawan.library.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.Interface.GetErrTemp;
import com.beawan.library.bean.ErrExplainTemplet;
import com.beawan.library.bean.ErrTemplet;
import com.beawan.library.bean.ReportItemTemp;
import com.beawan.library.bean.ReportTemplet;
import com.beawan.library.dao.IErrExplTempDao;
import com.beawan.library.dao.IErrTempletDao;
import com.beawan.library.dao.IReportItemDao;
import com.beawan.library.dao.IReportTempletDao;
import com.beawan.library.service.ITempletSV;

@Service("templetSV")
public class TempletSVImpl implements ITempletSV, GetErrTemp<ErrTemplet>{
	
	/* Rerport */
	
	@Resource
	protected IReportTempletDao reportTempletDao;
	
	@Override
	public ReportTemplet getReportTempletById(Long id) {
		// TODO Auto-generated method stub
		return this.reportTempletDao.getReportTempletById(id);
	}

	@Override
	public ReportTemplet getReportTempletByName(String name) {
		// TODO Auto-generated method stub
		return this.reportTempletDao.getReportTempletByName(name);
	}

	@Override
	public void saveReportTemplet(ReportTemplet ReportTemplet) {
		// TODO Auto-generated method stub
		this.reportTempletDao.saveReportTemplet(ReportTemplet);
	}

	@Override
	public void deleteReportTemplet(ReportTemplet ReportTemplet) {
		// TODO Auto-generated method stub
		this.reportTempletDao.deleteReportTemplet(ReportTemplet);
	}

	@Override
	public List<ReportTemplet> getAllReportTemplets() {
		// TODO Auto-generated method stub
		return this.reportTempletDao.getAllReportTemplets();
	}
	
	@Override
	public void saveAllReportTempletSet(List<ReportTemplet> ReportTempletList) {
		for (ReportTemplet rep : ReportTempletList) {
			reportTempletDao.saveReportTemplet(rep);
		}
	}
	/* ReportTemplet end */
	
	/* ErrTemplet */
	@Resource
	protected IErrTempletDao errTempletDao;

	@Override
	public ErrTemplet getErrTempletByName(String name) {
		return this.errTempletDao.getErrTempletByName(name);
	}
	
	@Override
	public ErrTemplet onGetErrTempletByName(String name) {
		// TODO Auto-generated method stub
		return this.errTempletDao.getErrTempletByName(name);
	}
	
	@Override
	public ErrTemplet onGetErrTempletById(Long id) {
		// TODO Auto-generated method stub
		return this.errTempletDao.getErrTempletById(id);
	}

	@Override
	public ErrTemplet getErrTempletById(Long id) {
		return this.errTempletDao.getErrTempletById(id);
	}

	@Override
	public List<ErrTemplet> getAllErrTemplet() {
		return this.errTempletDao.getAllErrTemplet();
	}

	@Override
	public Map<String, Object> getErrksForTable(String query, int index, int count, Object... args) {
		int startIndex=index*count;
		
		return this.errTempletDao.getErrks(query, startIndex, count, args);
	}

	@Override
	public ErrTemplet saveErrTemplet(ErrTemplet data) throws Exception {
		return this.errTempletDao.saveErrTemplet(data);
	}

	@Override
	public void deleteErrTemplet(ErrTemplet data) throws Exception {
		this.errTempletDao.deleteErrTemplet(data);
	}

	/* ErrTemplet end */
	
	/* ErrExplainTemplet */
	@Resource
	protected IErrExplTempDao errExplTempDao;

	@Override
	public ErrExplainTemplet getErrExplainTempletByName(String name) {
		return this.errExplTempDao.getErrExplainTempByName(name);
	}

	@Override
	public ErrExplainTemplet getErrExplainTempletById(Long id) {
		return this.errExplTempDao.getErrExplainTempById(id);
	}

	@Override
	public void saveErrExplainTemplet(ErrExplainTemplet data) throws Exception {
		this.errExplTempDao.saveErrExplainTemplet(data);
	}

	@Override
	public void saveErrExplainTemplets(List<ErrExplainTemplet> ErrExplainTemplets) {
		if (ErrExplainTemplets != null) {
			for (ErrExplainTemplet ErrExplainTemplet : ErrExplainTemplets) {
				this.errExplTempDao.saveErrExplainTemplet(ErrExplainTemplet);
			}
		}
	}

	@Override
	public void deleteErrExplainTemplet(ErrExplainTemplet data) throws Exception {
		this.errExplTempDao.deleteErrExplainTemplet(data);
	}

	@Override
	public List<ErrExplainTemplet> getErrExplainTempletByErrTempletId(Long errTempletId) {
		return this.errExplTempDao.getErrExplainTempletByErrTempletId(errTempletId);
	}

	@Resource
	protected IReportItemDao reportItemDao;
	
	
	@Override
	public void saveReportItem(ReportItemTemp data) throws Exception {
		// TODO Auto-generated method stub
		reportItemDao.saveReportItem(data);
	}

	@Override
	public void deleteReportItem(ReportItemTemp data) throws Exception {
		// TODO Auto-generated method stub
		reportItemDao.deleteReportItem(data);
	}

	@Override
	public List<ReportItemTemp> getAllReportItemByType(String type) {
		// TODO Auto-generated method stub
		return reportItemDao.getAllReportItemByType(type);
	}

	@Override
	public ReportItemTemp getAllReportItemByName(String name) {
		// TODO Auto-generated method stub
		return reportItemDao.getReportItemByName(name);
	}

	/* ErrExplainTemplet end */

}
