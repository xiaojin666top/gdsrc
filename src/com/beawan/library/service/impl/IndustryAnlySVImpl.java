package com.beawan.library.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.base.dao.ISysTreeDicDAO;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.library.bean.TreeData;
import com.beawan.library.dao.IIndustryAnalysisDao;
import com.beawan.library.service.IIndustryAnlySV;
import com.platform.util.JdbcUtil;
import com.platform.util.StringUtil;

@Service("industryAnlySV")
public class IndustryAnlySVImpl implements  IIndustryAnlySV{

	@Resource
	private IIndustryAnalysisDao industryAnlysisDao;
	
	@Resource
	private ISysTreeDicDAO sysTreeDicDAO;
	
	
	@Override
	public List<Map<String, Object>> queryPaging(TreeData queryCondition, String orderCondition, int pageIndex,
			int pageSize) throws Exception {
		String sql = "SELECT T.*"
				   + " FROM ("
		           + " SELECT ROW_NUMBER() OVER() AS ROW_NUM,IPS.*,TD.CNNAME AS INDUS_NAME"
		           + " FROM GDTCESYS.DG_BS_INDUSTRY_PAGE IPS"
		           + " LEFT JOIN GDTCESYS.DG_BS_TREEDIC TD ON TD.ENNAME = IPS.ENNAME"
		           + " AND TD.OPTTYPE='" + SysConstants.TreeDicConstant.GB_IC_4754_2017 + "'"
		           + " WHERE $"
		           + " ) T WHERE T.ROW_NUM > ? AND T.ROW_NUM <=?";
		
		String queryStr = genQueryStr(queryCondition);
		sql = sql.replace("$", queryStr);
		
		if (!StringUtil.isEmptyString(orderCondition))
			sql = sql.replace("OVER()", "OVER(ORDER BY " + orderCondition + ")");
		
		int startIndex = pageIndex*pageSize;
		int endIndex = startIndex + pageSize;
		Object[] params = new Object[]{startIndex, endIndex};
		
		return JdbcUtil.query(Constants.DataSource.TCE_DS, sql, params);
	}

	@Override
	public long queryCount(TreeData queryCondition) throws Exception {
		String sql = "SELECT COUNT(*)"
				   + " FROM GDTCESYS.DG_BS_INDUSTRY_PAGE IPS"
		           + " LEFT JOIN GDTCESYS.DG_BS_TREEDIC TD ON TD.ENNAME = IPS.ENNAME"
		           + " AND TD.OPTTYPE='" + SysConstants.TreeDicConstant.GB_IC_4754_2017 + "'"
		           + " WHERE $";
		
		String queryStr = genQueryStr(queryCondition);
		sql = sql.replace("$", queryStr);
		
		return JdbcUtil.queryForLong(Constants.DataSource.TCE_DS, sql, null);
	}

private String genQueryStr(TreeData queryCondition) throws Exception {
		
		StringBuffer listSql = new StringBuffer("1=1");
		
		if (queryCondition != null) {
			
			if (!StringUtil.isEmptyString(queryCondition.getEnName())) {
				listSql.append(" AND IPS.ENNAME = '")
				       .append(queryCondition.getEnName())
				       .append("'");
			}
			if (!StringUtil.isEmptyString(queryCondition.getCnName())) {
				listSql.append(" AND TD.CNNAME like '%")
				       .append(queryCondition.getCnName())
				       .append("%'");
			}
			
		}
		
		return listSql.toString();
	}
}
