package com.beawan.library.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.library.bean.InduSituation;
import com.beawan.library.dao.IIndustryAnalysisDao;
import com.beawan.library.service.IIndustrySituationSV;

@Service("indusSituationSV")
public class IndusSituationImpl implements IIndustrySituationSV{

	@Resource
	private IIndustryAnalysisDao industryAnlysisDao;
	
	@Override
	public InduSituation findInduSituationByName(String enName) throws Exception {
		
		InduSituation induSituation = null;
		for(int i=0;i<enName.length();i++) {
				String name = enName.substring(0,enName.length()-i);
				induSituation = industryAnlysisDao.getIndusSituationByName(name);
				if(induSituation != null)
					break;
		}
		return induSituation;
	}

}
