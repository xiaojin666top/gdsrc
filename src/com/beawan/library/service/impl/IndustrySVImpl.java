package com.beawan.library.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.beawan.base.dao.ISysTreeDicDAO;
import com.beawan.common.SysConstants;
import com.beawan.library.bean.IndustryAnalysis;
import com.beawan.library.bean.IndustryData;
import com.beawan.library.bean.IndustryQRadio;
import com.beawan.library.bean.IndustryQuota;
import com.beawan.library.bean.TreeData;
import com.beawan.library.dao.IIndustryAnalysisDao;
import com.beawan.library.dao.IIndustryQRadioDao;
import com.beawan.library.dao.IIndustryQuotaDao;
import com.beawan.library.dao.ITreeDataDao;
import com.beawan.library.service.IIndustrySV;
import com.beawan.survey.comm.TreeItemBean;
import com.platform.util.StringUtil;

/**
 * @ClassName IndustryService
 * @Description TODO(行业相关逻辑层)
 * @author czc
 * @Date 2017年5月23日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
@Service("industrySV")
public class IndustrySVImpl implements IIndustrySV {

	/* industryAnalysis */

	@Resource
	protected IIndustryAnalysisDao anaDao;

	@Resource
	protected ISysTreeDicDAO sysTreeDicDAO;
	
	@Resource
	protected ITreeDataDao treeDataDao;

	public List<IndustryAnalysis> getAllIndustryAnalysis() {
		return anaDao.getAllIndustryAnalysis();
	}

	public IndustryAnalysis getIndustryAnalysisById(Long id) {
		return anaDao.getIndustryById(id);
	}

	public void saveIndustryAnalysis(IndustryAnalysis data) {
		anaDao.saveIndustry(data);
	}

	public void deleteIndustryAnalysis(IndustryAnalysis data) {
		anaDao.deleteIndustry(data);
	}

	public IndustryAnalysis getIndustryAnalysisByTaskTreeData(TreeData data) {
		String fourTreedataID = data.getAbvenName();
		IndustryAnalysis industryFour = anaDao.getIndustryAnalysisByTreeDataId(data.getStdId());
		if (industryFour != null) {
			return industryFour;
		}
		TreeData threeTreedata = sysTreeDicDAO.geTreeDataByEnName(fourTreedataID, SysConstants.TreeDicConstant.GB_IC_4754_2017);
		String threeTreedataId = threeTreedata.getStdId();
		IndustryAnalysis industryThree = anaDao.getIndustryAnalysisByTreeDataId(threeTreedataId);
		if (industryThree != null) {
			return industryThree;
		}
		TreeData twoTreedata = sysTreeDicDAO.geTreeDataByEnName(threeTreedata.getAbvenName(), SysConstants.TreeDicConstant.GB_IC_4754_2017);
		String twoTreedataId = twoTreedata.getStdId();
		IndustryAnalysis industryTwo = anaDao.getIndustryAnalysisByTreeDataId(twoTreedataId);
		if (industryTwo != null) {
			return industryTwo;
		}
		String oneTreedataId = sysTreeDicDAO.geTreeDataByEnName(twoTreedata.getAbvenName(), SysConstants.TreeDicConstant.GB_IC_4754_2017)
				.getStdId();
		IndustryAnalysis industryOne = anaDao.getIndustryAnalysisByTreeDataId(oneTreedataId);
		if (industryOne != null) {
			return industryOne;
		}
		return null;
	}

	public List<IndustryAnalysis> getIndustryAnalysisListForPage() {
		List<TreeData> treeDatasOne = sysTreeDicDAO.getTreeDataAbvenName("all", SysConstants.TreeDicConstant.GB_IC_4754_2017);
		List<IndustryAnalysis> industryAnalysisOne = new ArrayList<IndustryAnalysis>();
		for (TreeData treeOne : treeDatasOne) {
			IndustryAnalysis industryOne = anaDao.getIndustryAnalysisByTreeDataId(treeOne.getStdId());
			if (industryOne != null) {
				List<TreeData> treeDatasTwo = sysTreeDicDAO.getTreeDataAbvenName(treeOne.getEnName(), SysConstants.TreeDicConstant.GB_IC_4754_2017);
				List<IndustryAnalysis> treeDataOneChilren = new ArrayList<IndustryAnalysis>();
				for (TreeData treeTwo : treeDatasTwo) {
					IndustryAnalysis industryTwo = anaDao.getIndustryAnalysisByTreeDataId(treeTwo.getStdId());
					if (industryTwo != null) {
						List<TreeData> treeDatasThree = sysTreeDicDAO.getTreeDataAbvenName(treeTwo.getEnName(),
								SysConstants.TreeDicConstant.GB_IC_4754_2017);
						List<IndustryAnalysis> treeDataTwoChilren = new ArrayList<IndustryAnalysis>();
						for (TreeData treeThree : treeDatasThree) {
							IndustryAnalysis industryThree = anaDao
									.getIndustryAnalysisByTreeDataId(treeThree.getStdId());
							if (industryThree != null) {
								treeDataTwoChilren.add(industryThree);
							}
						}
						industryTwo.setChildren(treeDataTwoChilren);
						treeDataOneChilren.add(industryTwo);
					}
				}
				industryOne.setChildren(treeDataOneChilren);
				industryAnalysisOne.add(industryOne);
			}
		}
		return industryAnalysisOne;
	}

	public List<IndustryAnalysis> getIndustryAnalysisListForLevel(String oneLevel, String twoLevel, String threeLevel) {
		List<IndustryAnalysis> industryAnalysisOne = new ArrayList<IndustryAnalysis>();
		if (!threeLevel.equals("")) {
			TreeData treeThreeLevel = sysTreeDicDAO.geTreeDataByEnName(threeLevel, SysConstants.TreeDicConstant.GB_IC_4754_2017);
			IndustryAnalysis industryThree = anaDao.getIndustryAnalysisByTreeDataId(treeThreeLevel.getStdId());
			if (industryThree != null) {
				industryAnalysisOne.add(industryThree);
				return industryAnalysisOne;
			}
		}
		if (!twoLevel.equals("")) {
			TreeData treeTwoLevel = sysTreeDicDAO.geTreeDataByEnName(twoLevel, SysConstants.TreeDicConstant.GB_IC_4754_2017);
			IndustryAnalysis industryTwo = anaDao.getIndustryAnalysisByTreeDataId(treeTwoLevel.getStdId());
			if (industryTwo != null) {
				List<TreeData> treeDatasThree = sysTreeDicDAO.getTreeDataAbvenName(treeTwoLevel.getEnName(),
						SysConstants.TreeDicConstant.GB_IC_4754_2017);
				List<IndustryAnalysis> treeDataTwoChilren = new ArrayList<IndustryAnalysis>();
				for (TreeData treeThree : treeDatasThree) {
					IndustryAnalysis industryThree = anaDao.getIndustryAnalysisByTreeDataId(treeThree.getStdId());
					if (industryThree != null) {
						treeDataTwoChilren.add(industryThree);
					}
				}
				industryTwo.setChildren(treeDataTwoChilren);
				industryAnalysisOne.add(industryTwo);
				return industryAnalysisOne;
			}
		}
		if (!oneLevel.equals("")) {
			TreeData treeOneLevel = sysTreeDicDAO.geTreeDataByEnName(oneLevel, SysConstants.TreeDicConstant.GB_IC_4754_2017);
			IndustryAnalysis industryone = anaDao.getIndustryAnalysisByTreeDataId(treeOneLevel.getStdId());
			if (industryone != null) {
				List<TreeData> treeDatasTwo = sysTreeDicDAO.getTreeDataAbvenName(treeOneLevel.getEnName(),
						SysConstants.TreeDicConstant.GB_IC_4754_2017);
				List<IndustryAnalysis> treeDataOneChilren = new ArrayList<IndustryAnalysis>();
				for (TreeData treeTwo : treeDatasTwo) {
					IndustryAnalysis industryTwo = anaDao.getIndustryAnalysisByTreeDataId(treeTwo.getStdId());
					if (industryTwo != null) {
						List<TreeData> treeDatasThree = sysTreeDicDAO.getTreeDataAbvenName(treeTwo.getEnName(),
								SysConstants.TreeDicConstant.GB_IC_4754_2017);
						List<IndustryAnalysis> treeDataTwoChilren = new ArrayList<IndustryAnalysis>();
						for (TreeData treeThree : treeDatasThree) {
							IndustryAnalysis industryThree = anaDao
									.getIndustryAnalysisByTreeDataId(treeThree.getStdId());
							if (industryThree != null) {
								treeDataTwoChilren.add(industryThree);
							}
						}
						industryTwo.setChildren(treeDataTwoChilren);
						treeDataOneChilren.add(industryTwo);
					}
				}
				industryone.setChildren(treeDataOneChilren);
				industryAnalysisOne.add(industryone);
				return industryAnalysisOne;
			}
		}
		return industryAnalysisOne;
	}

	public boolean haveChildIndustry(TreeData treeData) {
		IndustryAnalysis industry = anaDao.getIndustryAnalysisByTreeDataId(treeData.getStdId());
		if (industry != null) {
			List<TreeData> treeDataChild = sysTreeDicDAO.getTreeDataAbvenName(treeData.getEnName(), SysConstants.TreeDicConstant.GB_IC_4754_2017);
			List<IndustryAnalysis> industryChildren = new ArrayList<IndustryAnalysis>();
			for (TreeData treeThree : treeDataChild) {
				IndustryAnalysis industryThree = anaDao.getIndustryAnalysisByTreeDataId(treeThree.getStdId());
				if (industryThree != null) {
					industryChildren.add(industryThree);
				}
			}
			if (industryChildren != null && industryChildren.size() > 0) {
				return true;
			}
		}
		return false;
	}

	@Override
	public IndustryAnalysis getIndustryAnalysisByTreeDataId(String treeDataid) {
		// TODO Auto-generated method stub
		return anaDao.getIndustryAnalysisByTreeDataId(treeDataid);
	}

	@Override
	public Map<String, Object> getIndustryAnas(String query, int index, int count, Object... args) {
		return anaDao.getIndustryAnas(query, index, count, args);
	}

	@Override
	public IndustryAnalysis getIndustryAnalysisByName(String name) {
		return anaDao.getIndustryByName(name);
	}

	/* industryAnalysis end */

	/* TreeData */

	@Resource
	protected IIndustryQRadioDao industryQRadioDao;

	@Resource
	protected IIndustryQuotaDao industryQuotaDao;

	@Override
	public List<TreeData> getTreeDataByAbvenName(String abvenName, String optType) {
		return this.sysTreeDicDAO.getTreeDataAbvenName(abvenName, optType);
	}

	@Override
	public TreeData getTreeDataByCnName(String cnName, String optType) {
		return sysTreeDicDAO.geTreeDataByCnName(cnName, optType);
	}

	@Override
	public Map<String, Object> getIndustryQuota(String query, int index, int count, Object... args) {
		int startIndex=index*count;
		return this.industryQuotaDao.getIndustryQuota(query, startIndex, count, args);
	}

	@Override
	public List<TreeData> getTreeDataByLocate(String locate) {
		return this.sysTreeDicDAO.getTreeDataByLocate(locate);
	}
	
	@Override
	public String getFullNameByLocate(String locate, String optType){
		
		String fullName = "";
		String[] enNames = locate.split(",");
		
		int i = 0;
		if(enNames.length > 2)
			i = 1;
		
		for(;i<enNames.length;i++){
			TreeData treeData = sysTreeDicDAO.geTreeDataByEnName(enNames[i], optType);
			fullName = fullName + "->" + treeData.getCnName();
		}
		
		if(!fullName.equals(""))
			fullName = fullName.substring(2);
		
		return fullName;
	}

	@Override
	public TreeData getTreeDataById(String id) {
		return this.sysTreeDicDAO.getTreeDataById(id);
	}

	@Override
	public TreeData getTreeDataByEnname(String enname, String optType) {
		return this.sysTreeDicDAO.geTreeDataByEnName(enname, optType);
	}

	@Override
	public List<List<IndustryQRadio>> getRadioDataQuota(String quotaId, String scope) {
		List<IndustryQRadio> radioList = this.industryQRadioDao.getRadioDataQuota(quotaId, scope);
		List<IndustryQRadio> list1 = new ArrayList<IndustryQRadio>();
		List<IndustryQRadio> list2 = new ArrayList<IndustryQRadio>();
		List<IndustryQRadio> list3 = new ArrayList<IndustryQRadio>();
		List<IndustryQRadio> list4 = new ArrayList<IndustryQRadio>();
		List<IndustryQRadio> list5 = new ArrayList<IndustryQRadio>();
		for (IndustryQRadio radio : radioList) {
			if (SysConstants.FinanceAbility.YLNLZK_SCOPE.equals(radio.getBelong())) {
				list1.add(radio);
			} else if (SysConstants.FinanceAbility.ZCZLZK_SCOPE.equals(radio.getBelong())) {
				list2.add(radio);
			} else if (SysConstants.FinanceAbility.ZWFXZK_SCOPE.equals(radio.getBelong())) {
				list3.add(radio);
			} else if (SysConstants.FinanceAbility.JYZZZK_SCOPE.equals(radio.getBelong())) {
				list4.add(radio);
			} else if (SysConstants.FinanceAbility.BCZL_SCOPE.equals(radio.getBelong())) {
				list5.add(radio);
			}
		}
		List<List<IndustryQRadio>> radioMap = new ArrayList<List<IndustryQRadio>>();
		radioMap.add(list1);
		radioMap.add(list2);
		radioMap.add(list3);
		radioMap.add(list4);
		radioMap.add(list5);
		return radioMap;
	}

	@Override
	public void saveRadioDataQuota(List<IndustryQRadio> radioList) {
		for (IndustryQRadio raido : radioList) {
			if (null == raido.getStdId() || "".equals(raido.getStdId())) {
				raido.setStdId(UUID.randomUUID().toString());
			}
			industryQRadioDao.saveOrUpdate(raido);
		}
	}

	@Override
	public Map<String, IndustryQRadio> getQuotaByIndustry(TreeData industry) {
		
		Map<String, IndustryQRadio> quotaMap = new HashMap<String, IndustryQRadio>();
		
		if(industry == null)
			return quotaMap;
		
		//由于行业比率表中采用的行业代码为非国标代码，TreeData中的industryCode为关联字段
		String quotaIndusCode = industry.getIndustryCode();
		if(StringUtil.isEmptyString(quotaIndusCode)){
			//若本级行业无关联的行业比率指标数据，则按国标行业逐级网上查找
			String[] codes = industry.getLocate().split(",");
			for(int i=codes.length-2; i>=0; i--){
				TreeData indus = sysTreeDicDAO.geTreeDataByEnName(codes[i], 
						SysConstants.TreeDicConstant.GB_IC_4754_2017);
				//找到后跳出循环
				if(indus != null && !StringUtil.isEmptyString(indus.getIndustryCode())){
					quotaIndusCode = indus.getIndustryCode();
					break;
				}
			}
		}
		
		//查询关联的行业对象
		IndustryQuota industryQuota = industryQuotaDao.queryByIndustryCode(quotaIndusCode);
		if(industryQuota != null){
			//查询行业比率数据
			List<IndustryQRadio> radioList = industryQRadioDao.getRadioDataQuota(industryQuota.getStdId(), "全行业");
			if (!CollectionUtils.isEmpty(radioList)){
				for (IndustryQRadio radio : radioList) {
					quotaMap.put(radio.getItemName(), radio);
				}
			}
		}
		
		return quotaMap;
	}

	@Override
	public Map<String, IndustryQRadio> getQuotaByEnName(String enName) {
		TreeData indus = sysTreeDicDAO.geTreeDataByEnName(enName, 
				SysConstants.TreeDicConstant.GB_IC_4754_2017);
		return this.getQuotaByIndustry(indus);
	}

	@Override
	public List<TreeItemBean> findIndustryTree() {
		List<TreeItemBean> treeList = new ArrayList<TreeItemBean>();
		List<TreeData> dataList = sysTreeDicDAO.getTreeDataAbvenName("all", SysConstants.TreeDicConstant.GB_IC_4754_2017);
		for (TreeData data : dataList) {
			TreeItemBean bean = new TreeItemBean(data.getStdId(), data.getCnName());
			List<TreeItemBean> treeList1 = new ArrayList<TreeItemBean>();
			for (TreeData data1 : sysTreeDicDAO.getTreeDataAbvenName(data.getEnName(), SysConstants.TreeDicConstant.GB_IC_4754_2017)) {
				TreeItemBean bean1 = new TreeItemBean(data1.getStdId(), data1.getCnName());
				List<TreeItemBean> treeList2 = new ArrayList<TreeItemBean>();
				for (TreeData data2 : sysTreeDicDAO.getTreeDataAbvenName(data1.getEnName(), SysConstants.TreeDicConstant.GB_IC_4754_2017)) {
					TreeItemBean bean2 = new TreeItemBean(data2.getStdId(), data2.getCnName());
					List<TreeItemBean> treeList3 = new ArrayList<TreeItemBean>();
					for (TreeData data3 : sysTreeDicDAO.getTreeDataAbvenName(data2.getEnName(), SysConstants.TreeDicConstant.GB_IC_4754_2017)) {
						TreeItemBean bean3 = new TreeItemBean(data3.getStdId(), data3.getCnName());
						treeList3.add(bean3);
					}
					bean2.setChildren(treeList3);
					treeList2.add(bean2);
				}
				bean1.setChildren(treeList2);
				treeList1.add(bean1);
			}
			bean.setChildren(treeList1);
			treeList.add(bean);
		}
		return treeList;
	}

	@Override
	public IndustryData getIndustryDataByEnName(String enName) {
		// TODO Auto-generated method stub
		return anaDao.getIndustryByEnName(enName);
	}
	
	@Override
	public IndustryData getIndustryDataByCnName(String cnName) {
		// TODO Auto-generated method stub
		return anaDao.getIndustryByCnName(cnName);
	}
	
	@Override
	public int getIndustryByLikeName(String enName) {
		// TODO Auto-generated method stub
		return anaDao.getIndustryByLikeName(enName);
	}
	
	/*@Override
	public List<TreeData> getTreeDataByAbvenName(String abvenName, String optType) {
		return this.treeDataDao.getTreeDataAbvenName(abvenName, optType);
	}*/
	
	@Override
	public IndustryData getIndustryDataByStid(String stid) {
		// TODO Auto-generated method stub
		return anaDao.getIndustryDataByStid(stid);
	}
}
