package com.beawan.library.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.beawan.library.bean.ErrExplainTemplet;
import com.beawan.library.bean.ErrTemplet;
import com.beawan.library.bean.ReportItemTemp;
import com.beawan.library.bean.ReportTemplet;
import com.beawan.library.dao.IReportTempletDao;


/**
 * @ClassName TempletService
 * @Description TODO(表格、异常、异常解释原因 模板逻辑层)
 * @author czc
 * @Date 2017年5月23日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface ITempletSV {
	/* ReportTemplet */

	/**
	 * @Description (通过id查找表格模板)
	 * @param id
	 * @return
	 */
	public ReportTemplet getReportTempletById(Long id);

	/**
	 * @Description (通过表格名称查找表格模板)
	 * @param name
	 * @return
	 */
	public ReportTemplet getReportTempletByName(String name);

	/**
	 * @Description (保存表格模板)
	 * @param ReportTemplet
	 */
	public void saveReportTemplet(ReportTemplet ReportTemplet) ;
	
	/**
	 * @Description (循环保存表格模板)
	 * @param ReportTempletList
	 */
	public void saveAllReportTempletSet(List<ReportTemplet> ReportTempletList);

	/**
	 * @Description (删除表格模板)
	 * @param ReportTemplet
	 */
	public void deleteReportTemplet(ReportTemplet ReportTemplet);

	/**
	 * @Description (找出所有表格模板)
	 * @return
	 */
	public List<ReportTemplet> getAllReportTemplets();

	/* ReportTemplet end */
	
	/* ErrorKno */
	
	/**
	 * @Description (通过名字查询ErrTemplet)
	 * @param name
	 * @return
	 */
	public ErrTemplet getErrTempletByName(String name);
	
	/**
	 * @Description (通过id查询ErrTemplet)
	 * @param name
	 * @return
	 */
	public ErrTemplet getErrTempletById(Long id);
	
	/**
	 * @Description (查询所有异常模板)
	 * @return
	 */
	public List<ErrTemplet> getAllErrTemplet();
	
	/**
	 * @Description (异常模板的表格数据)
	 * @param query 查询语句
	 * @param index	位置
	 * @param count	每页显示页数
	 * @param args
	 * @return
	 */
	public Map<String, Object> getErrksForTable(String query, int index, int count, Object... args);
	
	/**
	 * @Description (保存ErrTemplet)
	 * @param data
	 * @return
	 */
	public ErrTemplet saveErrTemplet(ErrTemplet data) throws Exception;
	
	/**
	 * @Description (删除ErrTemplet)
	 * @param data
	 * @return
	 */
	public void deleteErrTemplet(ErrTemplet data) throws Exception;

	/* ErrorKno end */
	
	/* ErrExplainTemplet */
	
	/**
	 * @Description (循环保存ErrExplainTemplets)
	 * @param ErrExplainTemplets
	 */
	public void saveErrExplainTemplets(List<ErrExplainTemplet> ErrExplainTemplets);
	
	/**
	 * @Description (通过名字查询ErrExplainTemplet)
	 * @param name
	 * @return
	 */
	public ErrExplainTemplet getErrExplainTempletByName(String name);
	
	/**
	 * @Description (通过id查询ErrExplainTemplet)
	 * @param id
	 * @return
	 */
	public ErrExplainTemplet getErrExplainTempletById(Long id);
	
	
	/**
	 * @Description (保存ErrExplainTemplet)
	 * @param data
	 * @return
	 */
	public void saveErrExplainTemplet(ErrExplainTemplet data) throws Exception;
	
	
	/**
	 * @Description (删除ErrExplainTemplet)
	 * @param data
	 * @return
	 */
	public void deleteErrExplainTemplet(ErrExplainTemplet data) throws Exception;
	
	/**
	 * @Description (得到与errKno关联的ErrExplainTemplet)
	 * @param data
	 * @return
	 */
	public List<ErrExplainTemplet> getErrExplainTempletByErrTempletId(Long errKnoId);
	
	/* ErrExplainTemplet end */
	
	
	public void saveReportItem(ReportItemTemp data) throws Exception;
	
	public void deleteReportItem(ReportItemTemp data) throws Exception;
	
	public List<ReportItemTemp> getAllReportItemByType(String type);
	
	public ReportItemTemp getAllReportItemByName(String name);
	

}
