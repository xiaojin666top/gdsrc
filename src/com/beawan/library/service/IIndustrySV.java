package com.beawan.library.service;

import java.util.List;
import java.util.Map;

import com.beawan.library.bean.IndustryAnalysis;
import com.beawan.library.bean.IndustryData;
import com.beawan.library.bean.IndustryQRadio;
import com.beawan.library.bean.TreeData;
import com.beawan.survey.comm.TreeItemBean;


public interface IIndustrySV {
	
	/*  industryAnalysis */
	
	/**
	 * @Description (得到所有的行业分析)
	 * @return
	 */
	public List<IndustryAnalysis> getAllIndustryAnalysis();

	/**
	 * @Description (通过id找出行业分析)
	 * @param id
	 * @return
	 */
	public IndustryAnalysis getIndustryAnalysisById(Long id) ;
	
	/**
	 * @Description (通过名字找出行业分析)
	 * @param name
	 * @return
	 */
	public IndustryAnalysis getIndustryAnalysisByName(String name) ;
	
	/**
	 * @Description (得到展现页面上  树形结构的行业分析，树形结构与国标行业相同)
	 * @return
	 */
	public List<IndustryAnalysis> getIndustryAnalysisListForPage();
	
	/**
	 * @Description (保存行业分析)
	 * @param data
	 */
	public void saveIndustryAnalysis(IndustryAnalysis data);

	/**
	 * @Description (删除行业分析)
	 * @param data
	 */
	public void deleteIndustryAnalysis(IndustryAnalysis data);
	
	/**
	 * @Description (通过与任务相关的行业找到相关联的行业分析，从四级开始往上找)
	 * @param data
	 * @return	行业分析
	 */
	public IndustryAnalysis getIndustryAnalysisByTaskTreeData(TreeData data);
	
	/**
	 * @Description (根据前端页面查询所需要显示的行业知识库)
	 * @param oneLevel 行业一级
	 * @param twoLevel 行业二级
	 * @param threeLevel 行业三级
	 * @return
	 */
	public List<IndustryAnalysis> getIndustryAnalysisListForLevel(String oneLevel,String twoLevel,String threeLevel);
	
	/**
	 * @Description (国标行业有无子行业分析)
	 * @param treeData  国标行业
	 * @return
	 */
	public boolean haveChildIndustry(TreeData treeData);
	
	/**
	 * @Description (通过所关联的行业id查找行业分析)
	 * @param treeDataid
	 * @return
	 */
	public IndustryAnalysis getIndustryAnalysisByTreeDataId(String treeDataid);
	
	/**
	 * @Description (行业分析表格数据)
	 * @param query
	 * @param index
	 * @param count
	 * @param args
	 * @return
	 */
	public Map<String, Object> getIndustryAnas(String query, int index, int count, Object... args);
	

	
	/*  industryAnalysis end*/
	
	
	/*  TreeData */
	
	/**
	 * @Description (找出一个行业上一级行业的 所有 子行业)
	 * @param abvenName
	 * @param optType "STD_GB_4754-2017"
	 * @return
	 */
	//public List<TreeData> getTreeDataByAbvenName(String abvenName, String optType);
	
	/**
	 * @Description (根据stid得到行业数据)
	 * @return
	 */
	public IndustryData getIndustryDataByStid(String stid);
	
	/*  TreeData end*/
	/**
	 * @Description (根据模糊名称得到数据集)
	 * @return
	 */
	public int getIndustryByLikeName(String enName);
	/**
	 * @Description (通过中文名字找出这个行业)
	 * @param cnName 中文名字
	 * @param optType "STD_GB_4754-2017"
	 * @return
	 */
	public TreeData getTreeDataByCnName(String cnName, String optType);
	
	public List<TreeData> getTreeDataByLocate(String locate);
	
	/**
	 * TODO 通过定位获得树形结构字典项全称，用->隔开
	 * @param locate 定位
	 * @param optType 字典项类型
	 * @return
	 */
	public String getFullNameByLocate(String locate, String optType);
	
	/**
	 * @Description (通过 std 找出该行业)
	 * @param id
	 * @return
	 */
	public TreeData getTreeDataById(String id);
	
	/**
	 * @Description (通过英文代码找出该行业)
	 * @param enname
	 * @param optType
	 * @return
	 */
	public TreeData getTreeDataByEnname(String enname, String optType);
	
	/**
	 * @Description (行业比率表格)
	 * @param query  查新语句
	 * @param index	 位置
	 * @param count	 每一页显示的数量
	 * @param args  参数
	 * @return
	 */
	public Map<String, Object> getIndustryQuota(String query, int index, int count, Object... args);
	
	
	/**
	 * @Description (行业比率列表)
	 * @param quotaId
	 * @param scope
	 * @return
	 */
	public List<List<IndustryQRadio>> getRadioDataQuota(String quotaId, String scope);
	
	/**
	 * @Description (保存行业比率)
	 * @param radioList
	 */
	public void saveRadioDataQuota(List<IndustryQRadio> radioList);
	
	/**
	 * @Description (通过行业找出相应的比率)
	 * @param industry
	 * @return
	 */
	public Map<String, IndustryQRadio> getQuotaByIndustry(TreeData industry);
	
	/**
	 * @Description (通过行业英文代码查出相应比率)
	 * @param enName
	 * @return
	 */
	public Map<String, IndustryQRadio> getQuotaByEnName(String  enName);
	
	/**
	 * @Description (查询行业树)
	 * @return
	 */
	public List<TreeItemBean> findIndustryTree();
	
	/**
	 * @Description (找出一个行业上一级行业的 所有 子行业)
	 * @param abvenName
	 * @param optType "STD_GB_4754-2011"
	 * @return
	 */
	public List<TreeData> getTreeDataByAbvenName(String abvenName, String optType);
	
	/**
	 * @Description (根据enName得到行业数据)
	 * @return
	 */
	public IndustryData getIndustryDataByEnName(String enName);
	
	/**
	 * @param enName
	 * 根据名称查询行业编号
	 * @return
	 */
	public IndustryData getIndustryDataByCnName(String cnName);
	
	
}
