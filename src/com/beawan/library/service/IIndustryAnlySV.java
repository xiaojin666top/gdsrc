package com.beawan.library.service;

import java.util.List;
import java.util.Map;

import com.beawan.library.bean.TreeData;

public interface IIndustryAnlySV {
	/**
	 * TODO 根据条件分页查询
	 * @param queryCondition 查询条件
	 * @param orderCondition 排序条件
	 * @param pageIndex 起始页数
	 * @param pageSize 每页大小
	 * @return
	 * @throws Exception
	 */
	public List<Map<String,Object>> queryPaging(TreeData queryCondition,
			String orderCondition, int pageIndex, int pageSize) throws Exception;
	
	
	/**
	 * TODO 根据条件查询记录总数
	 * @param queryCondition 查询条件
	 * @return
	 * @throws Exception
	 */
	public long queryCount(TreeData queryCondition) throws Exception;
}
