package com.beawan.library.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.base.service.ISysTreeDicSV;
import com.beawan.common.SysConstants;
import com.beawan.library.bean.ErrTemplet;
import com.beawan.library.bean.IndustryAnalysis;
import com.beawan.library.bean.IndustryData;
import com.beawan.library.bean.IndustryQRadio;
import com.beawan.library.bean.IndustryQuota;
import com.beawan.library.bean.IndustryQuotaVo;
import com.beawan.library.bean.ReportTemplet;
import com.beawan.library.bean.TreeData;
import com.beawan.library.service.IIndustryAnlySV;
import com.beawan.library.service.IIndustrySV;
import com.beawan.library.service.ITempletSV;
import com.beawan.task.service.ITaskSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;


@Controller
@RequestMapping({ "/library" })
public class LibraryCtl {
	private static final Logger log = Logger.getLogger(LibraryCtl.class);
	private Map<String, Object> model = new HashMap<String, Object>();
	@Resource
	private ITaskSV taskSV;
	@Resource
	private IIndustrySV industrySV;
	@Resource
	private ITempletSV templetService;
	@Resource
	private ISysTreeDicSV sysTreeDicSV;
	
	@Resource
	private IIndustryAnlySV industryAnlySV;
	
	/**
	 * @Description (得到所有行业分析的行业分析树)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("industryAnalysis.json")
	@ResponseBody
	public String industryAnalysisJSON(HttpServletRequest request, HttpServletResponse response) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		List<IndustryAnalysis> analysis = industrySV.getIndustryAnalysisListForPage();
		hashMap.put("industry", analysis);
		return jRequest.serialize(hashMap);
	}
	
	/**
	 * @Description (参数设置跳转)
	 * @param request
	 * @param response
	 * @param index
	 * @return
	 */
	@RequestMapping("ratioPage.do")
	public String ratioPage(HttpServletRequest request, HttpServletResponse response, String index) {
		return "views/libraryManage/ratio" + index;
	}

	/**
	 * @Description (异常模板参数设置)
	 * @param request
	 * @param response
	 * @param search
	 * @param order
	 * @param offset
	 * @param limit
	 * @return
	 */
	@RequestMapping("abnormalRatio.do")
	@ResponseBody
	public String abnormalRatio(HttpServletRequest request, HttpServletResponse response, String search, String order,
			int page, int rows) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = null;
		
		if (null == search) {
			search = "";
		}
		json = templetService.getErrksForTable("name like ? AND rateval is not null", page-1, rows,
				"%" + search + "%");
		if (json == null) {
			return jRequest.serialize(false, "数据不存在");
		} else {
			return jRequest.serialize(json, true);
		}
	}

	/**
	 * @Description (行业指标设置)
	 * @param request
	 * @param response
	 * @param search
	 * @param order
	 * @param offset
	 * @param limit
	 * @return
	 */
	@RequestMapping("IndustryAnaRatio.do")
	@ResponseBody
	public String setIndustryAnaRatio(HttpServletRequest request, HttpServletResponse response, String search,
			String order, int offset, int limit) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = null;
		if (null == search) {
			search = "";
		}
		json = templetService.getErrksForTable("rateval like ?", offset, limit,
				"%" + search + "%");
		json = industrySV.getIndustryAnas("industryName like ?", offset, limit,
				"%" + search + "%");
		if (json == null) {
			return jRequest.serialize(false, "数据不存在");
		} else {
			return jRequest.serialize(json, true);
		}
	}

	/**
	 * @Description (编辑行业分析)
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("editIndustryAna.do")
	public String editIndustryAnaRatio(HttpServletRequest request, HttpServletResponse response, String id) {
		IndustryAnalysis industryAnalysis = this.industrySV.getIndustryAnalysisById(Long.valueOf(id));
		if (industryAnalysis == null) {
			industryAnalysis = new IndustryAnalysis();
		}
		request.setAttribute("industryAnalysis", industryAnalysis);
		return "views/libraryManage/ratioEdit/industryAnalysisEdit";
	}

	/**
	 * @Description (编辑异常指标)
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("editAbnormalRatio.do")
	public String editAbnormalRatio(HttpServletRequest request, HttpServletResponse response, String id) {
		ErrTemplet errk = this.templetService.getErrTempletById(Long.valueOf(id));
		if (errk == null) {
			errk = new ErrTemplet();
		}
		request.setAttribute("errk", errk);
		return "views/libraryManage/ratioEdit/abnormalRatio";
	}

	/**
	 * @Description (保存异常模板)
	 * @param request
	 * @param response
	 * @param rateval
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("saveErrk.do")
	@ResponseBody
	public String saveErrk(HttpServletRequest request, HttpServletResponse response, String rateval, String id)
			 {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);
		try {
		ErrTemplet errk = this.templetService.getErrTempletById(Long.valueOf(id));
		errk.setRateval(rateval);
		this.templetService.saveErrTemplet(errk);
		json.put("result", true);
		}catch (Exception e) {
			json.put("msg", "保存出错！");
			log.error("保存异常触发值异常", e);
		}
		return jRequest.serialize(json, true);
	}

	/**
	 * @Description (保存行业比率)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveIndustryQRatio.do")
	@ResponseBody
	public String saveIndustryQRatio(HttpServletRequest request, HttpServletResponse response) {
		String dataJSON = request.getParameter("radioList");
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		ObjectMapper mapper = new ObjectMapper();
		List<IndustryQRadio> radioList = new ArrayList<IndustryQRadio>();
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			radioList = mapper.readValue(dataJSON,
					TypeFactory.defaultInstance().constructCollectionType(ArrayList.class, IndustryQRadio.class));
			industrySV.saveRadioDataQuota(radioList);
			json.put("result", true);
		} catch (IOException e) {
			json.put("result", false);
			json.put("msg", "系统异常");
			e.printStackTrace();
		}

		return jRequest.serialize(json, true);
	}

	/**
	 * @Description (保存表格模板)
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveReportSet.do")
	@ResponseBody
	public String saveReportSet(HttpServletRequest request, HttpServletResponse response) {
		String dataJSON = request.getParameter("reportList");
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		ObjectMapper mapper = new ObjectMapper();
		List<ReportTemplet> reportList = new ArrayList<ReportTemplet>();
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			reportList = mapper.readValue(dataJSON,
					TypeFactory.defaultInstance().constructCollectionType(ArrayList.class, ReportTemplet.class));
			templetService.saveAllReportTempletSet(reportList);
			json.put("result", true);
		} catch (IOException e) {
			json.put("result", false);
			json.put("msg", "系统异常");
			e.printStackTrace();
		}

		return jRequest.serialize(json, true);
	}

	/**
	 * @Description (得到行业指标比率数据)
	 * @param request
	 * @param response
	 * @param search
	 * @param order
	 * @param offset
	 * @param limit
	 * @return
	 */
	@RequestMapping("queryIndustryQuota.do")
	@ResponseBody
	public String queryIndustryQuota(HttpServletRequest request, HttpServletResponse response, String search,
			String order, int page, int rows) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = null;
		if (null == search) {
			search = "";
		}
		String condition = "all," + search;
		String industryCode = "";
		List<TreeData> treeDataList = industrySV.getTreeDataByLocate(condition);
		if (null != treeDataList && treeDataList.size() > 0) {
			industryCode = treeDataList.get(0).getIndustryCode();
		}
		if ("".equals(search)) {
			json = industrySV.getIndustryQuota("", page-1, rows);
		} else {
			json = industrySV.getIndustryQuota("industryCode=?", page-1, rows, industryCode);
		}
		if (json == null) {
			return jRequest.serialize(false, "数据不存在");
		} else {
			List<IndustryQuotaVo> quotaList = new ArrayList<IndustryQuotaVo>();
			for (IndustryQuota quota : (List<IndustryQuota>) (json.get("rows"))) {
				quotaList.add(new IndustryQuotaVo(quota));
			}
			json.remove("rows");
			json.put("rows", quotaList);
			return jRequest.serialize(json, true);
		}
	}

	/**
	 * @Description (保存行业分析)
	 * @param request
	 * @param response
	 * @param condition
	 * @param industryAnalysis
	 * @param id
	 * @return
	 */
	@RequestMapping("saveIndustryAna.do")
	@ResponseBody
	public String saveIndustryAna(HttpServletRequest request, HttpServletResponse response, String condition,
			String industryAnalysis, String id) {
		IndustryAnalysis industryAna = industrySV.getIndustryAnalysisById(Long.valueOf(id));
		industryAna.setIndustryAnalysis(industryAnalysis);
		if (null == condition) {
			condition = "";
		}
		condition = "all," + condition;
		List<TreeData> treeDataList = industrySV.getTreeDataByLocate(condition);
		if (null != treeDataList && treeDataList.size() > 0) {
			industryAna.setTreeDataStd(treeDataList.get(0).getStdId());
		}
		industrySV.saveIndustryAnalysis(industryAna);
		return "views/libraryManage/ratio56";
	}

	/**
	 * @Description (查找行业树，子行业)
	 * @param request
	 * @param response
	 * @param abvenName
	 * @return
	 */
	@RequestMapping("queryTreeData.do")
	@ResponseBody
	public String queryTreeData(HttpServletRequest request, HttpServletResponse response, String abvenName) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<TreeData> treeDataList = this.industrySV.getTreeDataByAbvenName(abvenName,
				SysConstants.TreeDicConstant.GB_IC_4754_2017);
		TreeData treeData = this.industrySV.getTreeDataByEnname(abvenName, 
				SysConstants.TreeDicConstant.GB_IC_4754_2017);
		boolean flag = industrySV.haveChildIndustry(treeData);
		if (treeDataList == null) {
			treeDataList = new ArrayList<TreeData>();
		}
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("flag", flag);
		json.put("treeData", treeDataList);
		return jRequest.serialize(json, true);
	}

	/**
	 * @Description (找到三级行业的 行业分析)
	 * @param request
	 * @param response
	 * @param oneLevel 第一级行业
	 * @param twoLevel  第二级行业
	 * @param threeLevel	第三级行业
	 * @return
	 */
	@RequestMapping("industryAnalysisLevel.json")
	@ResponseBody
	public String industryAnalysisLevelJSON(HttpServletRequest request, HttpServletResponse response, String oneLevel,
			String twoLevel, String threeLevel) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		HashMap<String, Object> hashMap = new HashMap<String, Object>();
		List<IndustryAnalysis> analysis = industrySV.getIndustryAnalysisListForLevel(oneLevel, twoLevel,
				threeLevel);
		hashMap.put("industry", analysis);
		return jRequest.serialize(hashMap);
	}

	/**
	 * @Description (编辑行业比率)
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("editIndustryRatio.do")
	public String editIndustryRatio(HttpServletRequest request, HttpServletResponse response, String id) {
		request.setAttribute("allIndustry",
				industrySV.getRadioDataQuota(id, SysConstants.IndustryScope.ALL_INDUSTRY));
		request.setAttribute("largeIndustry",
				industrySV.getRadioDataQuota(id, SysConstants.IndustryScope.LARGE_INDUSTRY));
		request.setAttribute("middleIndustry",
				industrySV.getRadioDataQuota(id, SysConstants.IndustryScope.MIDDLE_INDUSTRY));
		request.setAttribute("smallIndustry",
				industrySV.getRadioDataQuota(id, SysConstants.IndustryScope.SMALL_INDUSTRY));
		request.setAttribute("stdId", id);
		return "views/libraryManage/ratioEdit/industryRatioEdit";
	}

	/**
	 * @Description (编辑行业比率)
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("IndustryRatioView.do")
	public String IndustryRatioView(HttpServletRequest request, HttpServletResponse response, String id) {
		request.setAttribute("allIndustry",
				industrySV.getRadioDataQuota(id, SysConstants.IndustryScope.ALL_INDUSTRY));
		request.setAttribute("largeIndustry",
				industrySV.getRadioDataQuota(id, SysConstants.IndustryScope.LARGE_INDUSTRY));
		request.setAttribute("middleIndustry",
				industrySV.getRadioDataQuota(id, SysConstants.IndustryScope.MIDDLE_INDUSTRY));
		request.setAttribute("smallIndustry",
				industrySV.getRadioDataQuota(id, SysConstants.IndustryScope.SMALL_INDUSTRY));
		return "views/libraryManage/ratioEdit/industryRatioShow";
	}

	@RequestMapping("importGBIndustry.do")
	@ResponseBody
	public String importGBIndustry(HttpServletRequest request, HttpServletResponse response) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);
		
		try {
			
			/*List<MultipartFile> mFileList = ((MultipartRequest) request).getFiles("file");
			if (CollectionUtils.isEmpty(mFileList)) 
				ExceptionUtil.throwException("未上传文件！");
			
			MultipartFile mfile = mFileList.get(0);*/
			
			File file = new File("C:/Users/rain/Desktop/国标行业分类2017.xlsx");
			FileInputStream fis = new FileInputStream(file);
			
			Sheet sheet;
			if(file.getName().endsWith(".xls")){
				HSSFWorkbook wb = new HSSFWorkbook(fis);
				sheet = wb.getSheetAt(0);
			}else{
				XSSFWorkbook wb = new XSSFWorkbook(fis);
				sheet = wb.getSheetAt(0);
			}
			
			List<TreeData> dics = new ArrayList<TreeData>();
			TreeData tdata = new TreeData();
			tdata.setStdId("GB_IC_4754_0");
			tdata.setOptType("STD_GB_4754-2017");
			tdata.setMemo("国标行业分类2017（树形字典）");
			tdata.setOrderId(0);
			tdata.setEnName("all");
			tdata.setCnName("全行业");
			tdata.setLevels("0");
			tdata.setLocate("all");
			tdata.setIsParent("1");
			dics.add(tdata);
			
			for(int i = sheet.getFirstRowNum()+2; i <= sheet.getLastRowNum(); i++){
				
				String value = null;
				Row row = sheet.getRow(i);
				int j = row.getFirstCellNum();
				for( ; j < 4; j++){
					
					Cell cell = row.getCell(j);
					if(cell == null)
						continue;
					
					switch(cell.getCellType()){
						case Cell.CELL_TYPE_NUMERIC: value = (int)cell.getNumericCellValue() + ""; break;
						case Cell.CELL_TYPE_BOOLEAN: value = String.valueOf(cell.getBooleanCellValue()); break;
						default: value = cell.getStringCellValue().trim();
					}
					
					if(StringUtil.isEmptyString(value))
						continue;
					
					tdata = new TreeData();
					tdata.setStdId("GB_IC_4754_" + (dics.size()));
					tdata.setOptType("STD_GB_4754-2017");
					tdata.setMemo("国标行业分类2017（树形字典）");
					tdata.setOrderId(dics.size());
					tdata.setEnName(value);
					tdata.setLevels((j+1)+"");
					
					if(j == 0) {
						tdata.setAbvenName("all");
						tdata.setLocate("all," + value);
						tdata.setIsParent("1");
					} else {
					
						TreeData lastData = dics.get(dics.size()-1);
						String[] locates = lastData.getLocate().split(",");
						
						switch(j){
							case 1:
								tdata.setAbvenName(locates[1]);
								tdata.setLocate(locates[0]+","+locates[1]+","+value);
								tdata.setIsParent("1");
								break;
							case 2:
								tdata.setAbvenName(locates[2]);
								tdata.setLocate(locates[0]+","+locates[1]+","+locates[2]+","+value);
								tdata.setIsParent("1");
								break;
							case 3:
								tdata.setAbvenName(locates[3]);
								tdata.setLocate(locates[0]+","+locates[1]+","+locates[2]+","+locates[3]+","+value);
								break;
						}
					}
					
					tdata.setCnName(row.getCell(4).getStringCellValue().trim());
					
					TreeData old = industrySV.getTreeDataByEnname(value,
							SysConstants.TreeDicConstant.GB_IC_4754_2011);
					if(old != null)
						tdata.setIndustryCode(old.getIndustryCode());
					
					dics.add(tdata);
				}
				
			}
			
			for(int i=0;i<dics.size();i++){
				sysTreeDicSV.saveOrUpdate(dics.get(i));
			}
			
			json.put("result", true);
			
		} catch (Exception e) {
			json.put("msg", "导入列表出错！");
			e.printStackTrace();
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description (查找行业树，子行业)
	 * @param request
	 * @param response
	 * @param abvenName
	 * @return
	 */
	@RequestMapping("queryIndustry.json")
	@ResponseBody
	public String queryIndustry(HttpServletRequest request, HttpServletResponse response, String abvenName) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		List<TreeData> treeDataList = this.industrySV.getTreeDataByAbvenName(abvenName, "STD_GB_4754-2011");
		if(!abvenName.equals("all")){
			for (TreeData treeData : treeDataList) {
				IndustryData industryData=this.industrySV.getIndustryDataByStid(treeData.getStdId());
				if(industryData!=null) {
					treeData.setFlag("2");
					continue;
				}
				String enName=treeData.getEnName();
				int size=this.industrySV.getIndustryByLikeName(enName);
				if(size>0) treeData.setFlag("1");
			}
			
		}
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("treeData", treeDataList);
		return jRequest.serialize(json, true);
	}
	
	/**
	 * 行业分析页面
	 * @return
	 */
	@RequestMapping("/indusAnalyList.do")
	public String searchIndustryList(HttpServletRequest request,
			HttpServletResponse response) {
		return "views/industry/indusAnalyList";
	}
	
	/**
	 * TODO 查询行业分析信息列表
	 * @param request
	 * @param response
	 * @param search 查询条件
	 * @param order 排序条件
	 * @param page 页码，从1开始
	 * @param rows 每页行数
	 * @return
	 */
	@RequestMapping("getIndustryAnlysis.do")
	@ResponseBody
	public String getIndusPolicys(HttpServletRequest request, HttpServletResponse response,
			String search, String order, int page, int rows) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);
		
		try {
			
			TreeData queryCondition = null;			
			if (!StringUtil.isEmptyString(search)) {
				queryCondition = JacksonUtil.fromJson(search, TreeData.class);
			}
			
			List<?> list = industryAnlySV.queryPaging(queryCondition, order,
					page-1, rows);			
			long total = industryAnlySV.queryCount(queryCondition);
			json.put("total", total);
			json.put("rows", list);
			
			json.put("result", true);
		} catch (Exception e) {
			log.error(" 查询行业分析信息列表", e);
		}
		
		return jRequest.serialize(json, true);
	}
	
	
	
	
	/**
	 * @Description (跳转具体数据页面)
	 * @param request
	 * @param response
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping("/getNewhtml.json")
	@ResponseBody
	public Map<String, Object> getNewhtml(HttpServletRequest request,
			HttpServletResponse response, String enName)
			throws UnsupportedEncodingException {
		IndustryData data = industrySV.getIndustryDataByEnName(enName);

		// cnName = new String(cnName.getBytes("ISO-8859-1"),"UTF-8");
		
		String url = "";
		if(null==data||"".equals(data.getUrl())) {
			url="noMess";
		}else {
			url=data.getUrl();
		}
		model.put("url", "views/industry/" + url);
		return model;
	}
	
}
