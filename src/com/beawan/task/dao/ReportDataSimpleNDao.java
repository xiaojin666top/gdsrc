package com.beawan.task.dao;

import com.beawan.core.BaseDao;
import com.beawan.task.bean.ReportDataSimpleN;

public interface ReportDataSimpleNDao extends BaseDao<ReportDataSimpleN>{

}
