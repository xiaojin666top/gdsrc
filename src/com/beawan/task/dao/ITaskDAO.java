package com.beawan.task.dao;

import java.util.List;
import java.util.Map;

import com.beawan.common.dao.ICommDAO;
import com.beawan.core.BaseDao;
import com.beawan.task.bean.Task;


public interface ITaskDAO extends BaseDao<Task> {
	
	/**
	 * @Description (通过id查找任务)
	 * @param id
	 * @return
	 */
	public Task getTaskById(Long id);

	/**
	 * @Description (通过任务流水号查找任务)
	 * @param serNo
	 * @return
	 */
	public Task getTaskBySerNo(String serNo);
	
	/**
	 * @Description (任务列表表格)
	 * @param query
	 * @param index
	 * @param count
	 * @param args
	 * @return
	 */
	public Map<String, Object> getTasks(String query, int index, int count, Object... args);
	
	
	/**
	 * @Description (找出所有任务)
	 * @return
	 */
	public List<Task> getAllTasks();
	
	/**
	 * @Description (通过条件找出所有任务)
	 * @param query 查询语句
	 * @param args
	 * @return
	 */
	public List<Task> getAllTasksWithCondition(String query, Object... args);
	
	
	/**
	 * @Description (通过任务编号查找任务)
	 * @param customerNo
	 * @return
	 */
	public List<Task> getTaskByCustomerNo(String customerNo);
	
	
	/**
	 * @Description (保存任务)
	 * @param data
	 */
	public Task saveTask(Task data);
	
	/**
	 * @Description (删除任务)
	 * @param data
	 */
	public void deleteTask(Task data);
	
}