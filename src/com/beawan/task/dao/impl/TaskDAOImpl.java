package com.beawan.task.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.core.BaseDaoImpl;
import com.beawan.task.bean.Task;
import com.beawan.task.dao.ITaskDAO;

@Repository("taskDAO")
public class TaskDAOImpl extends BaseDaoImpl<Task> implements ITaskDAO {

	@Override
	public List<Task> getAllTasks(){
		return this.getAll();
	}
	
	@Override
	public List<Task> getAllTasksWithCondition(String query, Object... args){
		return this.select(query, args);
	}
	
	@Override
	public Task getTaskById(Long id){
		return this.selectByPrimaryKey(id);
	}

	@Override
	public Task getTaskBySerNo(String serNo) {
		return selectSingleByProperty("serNo", serNo);
	}

	@Override
	public Map<String, Object> getTasks(String query, int index, int count, Object... args) {
		index = index<0 ? 0:index;
		count = count<0 ? 0:count;
		List<Task> entities = this.selectRange(query, index, count, args);
		long total = this.selectCount(query, args);
		Map<String, Object> json = new HashMap<String,Object>();
    	json.put("total", total);
    	json.put("rows", entities);
		return json;
	}
	

	@Override
	public List<Task> getTaskByCustomerNo(String customerNo) {
		String query = "from Task as model"
				     + " where model.customerNo ='" + customerNo
				     +"' order by model.applicationTime desc";
		return this.select(query);
	}
	
	@Override
	public Task saveTask(Task data){
		return this.save(data);
	}
	
	@Override
	public void deleteTask(Task data){
		this.delete(data);
	}
	
}
