package com.beawan.task.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.task.bean.ReportDataSimpleN;
import com.beawan.task.dao.ReportDataSimpleNDao;

@Repository("reportDataSimpleNDao")
public class ReportDataSimpleNDaoImpl  extends BaseDaoImpl<ReportDataSimpleN> implements ReportDataSimpleNDao{

}
