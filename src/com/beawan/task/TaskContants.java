package com.beawan.task;

/**
 * 任务常量
 * 
 * @author yzj
 *
 */
public class TaskContants {

	/** 任务状态  1待调查任务   2 调查中任务  3 已提交任务  4 被退回任务  5已删除*/
	public static final int PREPAR = 1;
	public static final int ING = 2;
	public static final int FINISH = 3;
	public static final int BACK = 4;
	public static final int DELETE = 5;
	
}
