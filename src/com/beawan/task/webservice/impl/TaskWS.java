package com.beawan.task.webservice.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.beawan.survey.custInfo.dto.CompBaseDto;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.beawan.analysis.finansis.service.IFinanasisService;
import com.beawan.base.entity.User;
import com.beawan.base.service.IPlatformLogSV;
import com.beawan.base.service.ITableSubjCodeSV;
import com.beawan.base.service.IUserSV;
import com.beawan.base.webservice.IUserWS;
import com.beawan.common.SysConstants;
import com.beawan.common.util.WSResult;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.beawan.common.BusinessException;
import com.beawan.common.Constants;
import com.beawan.customer.service.ICusFSToolSV;
import com.beawan.library.service.IIndustrySV;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.beawan.survey.loanInfo.bean.Guarantor;
import com.beawan.survey.loanInfo.service.IGuaraInfoSV;
import com.beawan.survey.loanInfo.service.ISurveyReportSV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.beawan.task.webservice.ITaskWS;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.platform.util.DateUtil;
import com.platform.util.EncryptUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;

@Service("taskWS")
public class TaskWS implements ITaskWS {

	private static final Logger log = Logger.getLogger(IUserWS.class);

	@Resource
	protected IUserSV userSV;

	@Resource
	private ITableSubjCodeSV tableSubjCodeSV;
	@Resource
	private IPlatformLogSV platformLogSV;
	@Resource
	private IFinanasisService finanasisSV;
	@Resource
	private IIndustrySV industrySV;
	@Resource
	private ITaskSV taskSV;
	@Resource
	private ISurveyReportSV surveyReportSV;
	@Resource
	private ICmisInInvokeSV cmisInvokeSV;

	@Resource
	protected ICompBaseSV compBaseSV;
	
	@Resource
	private IGuaraInfoSV guaraInfoSV;
	
	@Resource
	private ICusFSToolSV cusFSToolSV;
	
	@Override
	public String getTasks(String userId, String query, String order,
			int pageIndex, int pageSize) {
		
		WSResult result = new WSResult();
		
		Task queryCondition = null;
		
		try {
			
			User user = userSV.queryById(userId);
			
			if (!StringUtil.isEmptyString(query))
				queryCondition = JacksonUtil.fromJson(query, Task.class);
			else
				queryCondition = new Task();
			
			String userDA = user.getDataAccess();
			if(SysConstants.UserDataAccess.PERSONAL.equals(userDA))
				queryCondition.setUserNo(user.getUserId());
			else if(SysConstants.UserDataAccess.ORG.equals(userDA))
				queryCondition.setInstitutionNo(user.getAccOrgNo());
			
			Map<String, Object> dataMap = taskSV.getTaskPaging(queryCondition,
					order, pageIndex, pageSize);
			
			result.data = JacksonUtil.serialize(dataMap);
			
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("任务分页查询异常：", e);
		}
		
		return result.json();
	}
	
	@Override
	public String getTaskTable(String username, int index,String dataAccess,String orgNo) {
		WSResult result = new WSResult();
		try {
			String query="state=?  and userName = ? order by applicationTime desc";
			List<Task> tasks=new ArrayList<Task>();
			if(null!=dataAccess){
			if(SysConstants.UserDataAccess.ALL.equals(dataAccess)) {
				query="state=? order by applicationTime desc";
				tasks=taskSV.getAllTasksWithCondition(query, index);
			}else if(SysConstants.UserDataAccess.ORG.equals(dataAccess)) {
				if(orgNo!=null) {
					query="state=? and institutionNo=? order by applicationTime desc";
					tasks=taskSV.getAllTasksWithCondition(query, index,orgNo);
				}
				
			}else {
				query="state=?  and userName = ? order by applicationTime desc";
				taskSV.getAllTasksWithCondition(query, index,username);
			}			
			}else {
				query="state=?  and userName = ? order by applicationTime desc";
				taskSV.getAllTasksWithCondition(query, index,username);
			}
			if (CollectionUtils.isEmpty(tasks)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(tasks);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			result.status=WSResult.STATUS_ERROR;
			log.error("查询任务异常！", e);
		}
		return result.json();
	}

	@Override
	public String addTask() {
		String result = null;
		try {
			Task task = new Task();
			result = JacksonUtil.serialize(task);
		} catch (Exception e) {
			e.printStackTrace();
			result = "error";
			log.error("新建任务异常！", e);
		}
		return result;
	}

	@Override
	public String saveTask(String taskInfo,String finaAnalyFlag) {
		WSResult result = new WSResult();
		
		ObjectMapper mapper = new ObjectMapper();
		Task task = null;
		
		try {
			task = mapper.readValue(taskInfo, Task.class);
			task.setState(0);
			taskSV.saveTaskInfo(task, finaAnalyFlag);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			result.message = e.getMessage();
			log.error("保存任务异常：",e);
		}
		
		return result.json();
	}
	
	@Override
	public String deleteTask(long taskId, String userId) {
		
		WSResult result = new WSResult();
		
		try {
			
			taskSV.deleteTask(taskId, userSV.queryById(userId));
			
		} catch (BusinessException be) {
			
			result.status = WSResult.STATUS_ERROR;
			result.message = be.getMessage();
			log.error("删除任务异常：" + be.getMessage());
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("删除任务异常！",e);
		}
		
		return result.json();
	}
	
	@Override
	public String assistCusMgeSignIn(Long taskId, String cusMgeNo,String password) {
		WSResult result = new WSResult();
		try {
			
			Task task = taskSV.getTaskById(taskId);
			if(!StringUtil.isEmptyString(task.getHelpUserNo())){
				result.status = WSResult.STATUS_ERROR;
				result.message="协办客户经理：" + task.getHelpUserName() + "已经签到，请勿重复签到！";
			}
			
			User user = userSV.queryById(cusMgeNo);
			if(user==null){
				result.status = WSResult.STATUS_ERROR;
				result.message="用户不存在！";
			}
			if (!EncryptUtil.md5(password).equals(user.getPassword())) {
				result.status = WSResult.STATUS_ERROR;
				result.message="密码不正确!";
			}
			if (!user.getAccOrgNo().equals(task.getInstitutionNo())) {
				result.status = WSResult.STATUS_ERROR;
				result.message="主办客户经理和协办客户经理不在同一个机构!";
			}
			
			
			if(result.status!=WSResult.STATUS_ERROR) {
			task.setHelpUserNo(cusMgeNo);
			task.setHelpUserName(user.getUserName());
			taskSV.saveOrUpdate(task);
			}
		}  catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("协办客户经理签到异常！", e);
		}
		return result.json();
	}
	
	@Override
	public String submitSurveyTask(long taskId) {
		
		WSResult result = new WSResult();
		Task task=taskSV.getTaskById(taskId);
		try {
			// 判断是否符合提交要求
				if(SysConstants.CusFinaValidFlag.TEMP_UNAVAIL.equals(task.getFinaSyncFlag())) {
					result.status = WSResult.STATUS_ERROR;
					result.message ="财务信息不全，提交失败！";
				}
				
				if(StringUtil.isEmptyString(task.getIndustry())) {
					result.status = WSResult.STATUS_ERROR;
					result.message ="未填写客户所属行业，提交失败！";
				}
				
				String customerNo="";
				if(task.getBusinessNo().equals(SysConstants.LoanTypeConstant.LOAN_PERSONAL))
					customerNo =task.getCmBaseId();
				else
					customerNo=task.getCustomerNo();

			CompBaseDto compBase = compBaseSV.findCompBaseByCustNo(customerNo);
				if(compBase==null) {
					result.status = WSResult.STATUS_ERROR;
					result.message ="未填写客户基本信息，提交失败！";
					
				}
					
				if(StringUtil.isEmptyString(compBase.getIndustryCode())) {
					result.status = WSResult.STATUS_ERROR;
					result.message ="未填写客户所属行业，提交失败！";
				}
			
				List<Guarantor> guarantors=guaraInfoSV.findGuarantorByTaskId(task.getId(), SysConstants.GuarantorType.COMP_GUARA);
				if(guarantors!=null) {
					for(Guarantor guarantor:guarantors) {
						CompBaseDto compBaseTemp = compBaseSV.findCompBaseByCustNo(guarantor.getCusNo());
						if(compBaseTemp==null) {
							result.status = WSResult.STATUS_ERROR;
							result.message ="未填写公司保证人基本信息，提交失败！";
						}
						
						if(StringUtil.isEmptyString(compBaseTemp.getIndustryCode()))
							result.status = WSResult.STATUS_ERROR;
							result.message ="未填写公司保证人所属行业，提交失败！";
					}
				}
				if(StringUtil.isEmptyString(task.getHelpUserNo())) {
					result.status = WSResult.STATUS_ERROR;
					result.message = "协办客户经理尚未签到，提交失败！";
				}
			
				if(result.status!=WSResult.STATUS_ERROR) {
				// 生成调查报告
				surveyReportSV.createSurveyReport(taskId);
				// 更新状态
				task.setState(SysConstants.TaskState.SUBMITTED);
				// 设置提交时间
				task.setSubmitTime(DateUtil.formatDate(DateUtil.getSystemTime(), Constants.DATETIME_MASK));
				taskSV.saveOrUpdate(task);
				}
			
		} catch (Exception e) {
			result.status = WSResult.STATUS_ERROR;
			log.error("提交调查报告异常！", e);
		}
		
		return result.json();
	}
	
	@Override
	public String cusInfoCheck(String customerNo, int finaRepYear, int finaRepMonth) {

		WSResult result = new WSResult();
		
		try {
			
			cusFSToolSV.checkCusInfoRule(customerNo, finaRepYear, finaRepMonth);
			
		} catch (BusinessException be){
			
			result.status = WSResult.STATUS_ERROR;
			result.message = be.getMessage();
			log.error("客户信息完整性检查：" + be.getMessage());
			
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			result.message ="系统异常";
			log.error("客户信息完整性检查异常：", e);
		}
		

		return result.json();
	}

	@Override
	public String cusInfoCheckAndSync(long taskId) {
		
		WSResult result = new WSResult();
		
		Task task=taskSV.getTaskById(taskId);
		
		boolean flag = false;
		
		try {
			
			finanasisSV.saveFinaDataAndAnalyze(task, "1");
			
			flag = true;
			
		} catch (BusinessException be) { 
			result.message = be.getMessage();
			log.error(be.getMessage() + be);
		} catch (Exception e) { 
			result.status = WSResult.STATUS_ERROR;
			log.error("客户信息完整性检查异常！", e);
		} finally{
			result.data = "" + flag;
		}
		
		return result.json();
	}
	
	@Override
	public String queryOrgName(String orgNo) {
		
		WSResult result = new WSResult();
		
		try {
			result.data = cmisInvokeSV.queryOrgName(orgNo);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("查找机构名称异常！",e);
		}
		
		return result.json();
	}

	@Override
	public String withdrawSurveyTask(Long taskId) {
		WSResult result = new WSResult();
		
		try {
			Task task =  taskSV.getTaskById(taskId);
			task.setState(SysConstants.TaskState.TO_DEAL_WITH);
			taskSV.saveOrUpdate(task);
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("撤回申请异常！",e);
		}
		
		return result.json();
	}

	@Override
	public String findTaskById(Long taskId) {
		
		WSResult result = new WSResult();
		
		try {
			
			Task task =  taskSV.getTaskById(taskId);
			
			if(task == null)
				result.data = null;
			else
				result.data = JacksonUtil.serialize(task);
			
		}catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("查找任务信息异常！",e);
		}
		
		return result.json();
	}

}
