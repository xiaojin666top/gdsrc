package com.beawan.task.webservice;


public interface ITaskWS {
	
	/**
	 * TODO 分页查询
	 * @param userId 操作员用户号
	 * @param query 查询条件 （符合Task类转换的json串）
	 * @param order 排序条件
	 * @param pageIndex 页下标
	 * @param pageSize 每页大小
	 * @return
	 */
	public String getTasks(String userId, String query, String order,
			int pageIndex, int pageSize);
	
	/**
	 * @Description (得到客户经理任务列表)
	 * @param username 客户经理编号
	 * @param index 
	 * @return	任务集合
	 */
	public String getTaskTable(String username,int index,String dataAccess,String orgNo);
	
	/**
	 * @Description (新建任务)
	 * @return
	 */
	public String addTask();
	
	/**
	 * @Description (保存任务)
	 * @param taskInfo
	 * @param finaAnalyFlag
	 * @return
	 */
	public String saveTask(String taskInfo, String finaAnalyFlag);
	
	/**
	 * TODO 删除任务及关联数据
	 * @param taskId 任务号
	 * @param operator 操作人
	 * @throws Exception
	 */
	public String deleteTask(long taskId, String userId);
	
	
	/**
	 * 协办客户经理签到
	 * @param taskId
	 * @param cusMgeNo
	 * @param password
	 * @return
	 */
	public String assistCusMgeSignIn(Long taskId,String cusMgeNo,String password);
	
	/**
	 * TODO 任务提交
	 * @param taskId 任务号
	 * @return
	 */
	public String submitSurveyTask(long taskId);
	
	/**
	 * TODO 企业客户基本信息及财报信息完整性检查
	 * @param customerNo 客户号
	 * @param finaRepYear 财报年份
	 * @param finaRepMonth 财报月份
	 * @return
	 */
	public String cusInfoCheck(String customerNo, int finaRepYear, int finaRepMonth);
	
	
	/**
	 * TODO 企业客户基本信息及财报信息完整性检查和同步
	 * @param taskId 任务号
	 * @return
	 */
	public String cusInfoCheckAndSync(long taskId);
	
	
	/**
	 * TODO 查找机构名称
	 * @param orgNo
	 * @return
	 */
	public String queryOrgName(String orgNo);
	
	
	/**
	 * TODO 主动撤回任务
	 * @param id
	 * @return
	 */
	public String withdrawSurveyTask(Long taskId);
	
	
	/**
	 * TODO 根据ID查找任务
	 * @param id
	 * @return
	 */
	public String findTaskById(Long taskId);
	
	
}
