package com.beawan.task.bean;

import java.io.Serializable;

public class ReportDataSimpleN implements Serializable{
	/** @Field @lineNo : 行次 */
	private String lineNo;
	
	/** @Field @oldName : 原先的名字 */
	private String oldName;
	
	private long id;
	/** @Field @name : (表格每行科目名字) */
	private String name;

	/** @Field @value : (期末值) */
	private Double value;

	/** @Field @value_early : (期初值) */
	private Double value_early;

	/** @Field @type : (报表名，如：利润表、资产负债表、现金流量表) */
	private String type;
	/** @Field @rankOrder : 一期的id,用于标记在哪一期 */
	private long ReportDataItemId;

	@Override
	public String toString() {
		return "ReportDataSimpleN [lineNo=" + lineNo + ", oldName=" + oldName + ", id=" + id + ", name=" + name
				+ ", value=" + value + ", value_early=" + value_early + ", type=" + type + ", ReportDataItemId="
				+ ReportDataItemId + "]";
	}
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getReportDataItemId() {
		return ReportDataItemId;
	}

	public void setReportDataItemId(long reportDataItemId) {
		ReportDataItemId = reportDataItemId;
	}

	public Double getValue_early() {
		return value_early;
	}

	public void setValue_early(Double value_early) {
		this.value_early = value_early;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}


	public String getLineNo() {
		return lineNo;
	}

	public void setLineNo(String lineNo) {
		this.lineNo = lineNo;
	}

	public String getOldName() {
		return oldName;
	}

	public void setOldName(String oldName) {
		this.oldName = oldName;
	}
}
