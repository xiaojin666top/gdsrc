package com.beawan.task.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.beawan.core.BaseEntity;

/**
 * @ClassName Task
 * @Description (贷前任务表)
 * @author rain
 * @Date 2018年1月3日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
@Entity
@Table(name = "LB_TASK",schema = "GDTCESYS")
public class Task extends BaseEntity implements java.io.Serializable {

	// Fields
	@Id
	//@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="LB_TASK_SEQ")
//	@SequenceGenerator(name="LB_TASK_SEQ",allocationSize=1,initialValue=1, sequenceName="LB_TASK_SEQ")
	private Long id;

	@Column(name = "SER_NO")
	private String serNo;// 业务流水号

	/** @Field @name : (客户名称) */
	@Column(name = "CUSTOMER_NAME")
	private String customerName;

	/** @Field @loan : (贷款金额) */
	@Column(name = "LOAN_NUMBER")
	private Double loanNumber = 0.0;
	
	/** @Field @loan : (贷款期限) */
	@Column(name = "LOAN_TERM")
	private Integer loanTerm;

	/** 
	 * 这个状态个人不推荐过于密集直接12345
	 * 如果想在中间加入一个状态----会很尴尬
	 * @Field @state : ( 任务状态  1待调查任务   2 调查中任务  3 已提交任务  4 被退回任务  5已删除)
	 * @已废弃
	 **/
	@Column(name = "STATE")
	private Integer state = 1;
	

	/** @Field @year : (当期报表年份) */
	@Column(name = "YEAR")
	private Integer year;

	/** @Field @month : (当期报表月份) */
	@Column(name = "MONTH")
	private Integer month;
	
	/** @Field industry : (所处行业标识) */
	@Column(name = "INDUSTRY")
	private String industry;

	/** @Field @applicationTime : (申请时间) */
	@Column(name = "APPLICATION_TIME")
	private String applicationTime;

	/** @Field @applicationTime : (提交完成任务时间) */
	@Column(name = "SUBMIT_TIME")
	private String submitTime;

	/** @Field @businessNo : (模版类型编号) */
	@Column(name = "BUSINESS_NO")
	private String businessNo;
	
	/** @Field @business : (模版类型名称) */
	@Column(name = "BUSINESS_TYPE")
	private String businessType;

	/** @Field @customerNo : (客户编号) */
	@Column(name = "CUSTOMER_NO")
	private String customerNo;

	/** @Field userNo : (主办客户经理编号) */
	@Column(name = "USER_NO")
	private String userNo;

	/** @Field userName : (主办客户经理姓名) */
	@Column(name = "USER_NAME")
	private String userName;

	/** @Field userName : (协办客户经理编号) */
	@Column(name = "HELP_USER_NO")
	private String helpUserNo;

	/** @Field @helpUserName : (协办客户经理名字) */
	@Column(name = "HELP_USER_NAME")
	private String helpUserName;

	/** @Field institutionNo : (机构号) */
	@Column(name = "INSTITUTION_NO")
	private String institutionNo;

	/** @Field institutionNo : (机构名称) */
	@Column(name = "INSTITUTION_NAME")
	private String institutionName;

	/** @Field cmBaseId : (企业信息标识（用于小企业主经营贷）) */
	@Column(name = "CM_BASE_ID")
	private String cmBaseId;
	
	/** @Field reuseLast : (是否复用上一次调查录入的信息，1：是) */
	@Column(name = "REUSE_LAST")
	private String reuseLast;
	
	/** @Field finaSyncFlag : (财务信息保存分析标识，0：财报数据完全不能满足系统分析要求；1：已保存并分析；2：暂时不能满足系统系统分析要求，待分析) */
	@Column(name = "FINA_SYNC_FLAG")
	private String finaSyncFlag;
	
	/** @Field finaSyncFlag : 财务报表备注说明，当无法采集到满足要求的财报数据时必填 */
	@Column(name = "FINA_REP_REMARK")
	private String finaRepRemark;

	@Column(name = "ASSURE_MEANS")
	private String assureMeans;//担保方式 --STD_SY_GUAR_TYPE  暂时不用这个
	@Column(name = "REPLY_WAR")
	private String replyWay;//还款方式 --STD_REPLY_WAY
	@Column(name = "LOAN_USE")
	private String loanUse;//贷款用途  --STD_ZB_DKYT
	@Column(name = "LOAN_RATE")
	private String loanRate;//贷款利率

	@Column(name = "DG_RATE")
	private Double dgRate;//对公测算利率  单位%
	@Column(name = "DG_GRADE")//对公评级模型
	private String dgGrade;
	@Column(name = "PRODUCT_ID")
	private Integer productId;//产品编号
	@Column(name = "PRODUCT_NAME")
	private String productName;//授信产品

	@Column(name = "LE_USER_NO")
	private String leUserNo;//贷前指定的审查审批经理id
	
	
	// Constructors

	/** default constructor */
	public Task() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSerNo() {
		return serNo;
	}

	public void setSerNo(String serNo) {
		this.serNo = serNo;
	}

	public String getBusinessNo() {
		return businessNo;
	}

	public void setBusinessNo(String businessNo) {
		this.businessNo = businessNo;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Double getLoanNumber() {
		return loanNumber;
	}

	public void setLoanNumber(Double loanNumber) {
		this.loanNumber = loanNumber;
	}

	public Integer getLoanTerm() {
		return loanTerm;
	}

	public void setLoanTerm(Integer loanTerm) {
		this.loanTerm = loanTerm;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getApplicationTime() {
		return applicationTime;
	}

	public void setApplicationTime(String applicationTime) {
		this.applicationTime = applicationTime;
	}

	public String getSubmitTime() {
		return submitTime;
	}

	public void setSubmitTime(String submitTime) {
		this.submitTime = submitTime;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getHelpUserNo() {
		return helpUserNo;
	}

	public void setHelpUserNo(String helpUserNo) {
		this.helpUserNo = helpUserNo;
	}

	public String getHelpUserName() {
		return helpUserName;
	}

	public void setHelpUserName(String helpUserName) {
		this.helpUserName = helpUserName;
	}

	public String getInstitutionNo() {
		return institutionNo;
	}

	public void setInstitutionNo(String institutionNo) {
		this.institutionNo = institutionNo;
	}

	public String getInstitutionName() {
		return institutionName;
	}

	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}
	
	public String getReuseLast() {
		return reuseLast;
	}

	public void setReuseLast(String reuseLast) {
		this.reuseLast = reuseLast;
	}

	public String getCmBaseId() {
		return cmBaseId;
	}

	public void setCmBaseId(String cmBaseId) {
		this.cmBaseId = cmBaseId;
	}

	public String getFinaSyncFlag() {
		return finaSyncFlag;
	}

	public void setFinaSyncFlag(String finaSyncFlag) {
		this.finaSyncFlag = finaSyncFlag;
	}

	public String getFinaRepRemark() {
		return finaRepRemark;
	}

	public void setFinaRepRemark(String finaRepRemark) {
		this.finaRepRemark = finaRepRemark;
	}

	public String getAssureMeans() {
		return assureMeans;
	}

	public void setAssureMeans(String assureMeans) {
		this.assureMeans = assureMeans;
	}

	public String getLoanUse() {
		return loanUse;
	}

	public void setLoanUse(String loanUse) {
		this.loanUse = loanUse;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getReplyWay() {
		return replyWay;
	}

	public void setReplyWay(String replyWay) {
		this.replyWay = replyWay;
	}

	public String getLoanRate() {
		return loanRate;
	}

	public void setLoanRate(String loanRate) {
		this.loanRate = loanRate;
	}

	public Double getDgRate() {
		return dgRate;
	}

	public void setDgRate(Double dgRate) {
		this.dgRate = dgRate;
	}

	public String getDgGrade() {
		return dgGrade;
	}

	public void setDgGrade(String dgGrade) {
		this.dgGrade = dgGrade;
	}

	public String getLeUserNo() {
		return leUserNo;
	}

	public void setLeUserNo(String leUserNo) {
		this.leUserNo = leUserNo;
	}
}