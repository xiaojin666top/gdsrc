package com.beawan.task.bean;

import java.io.Serializable;
import java.util.Map;
/**
 * 从财报那边过来的数据回传固定格式
 * @author 86178
 *
 */
public class retData implements Serializable{
	private int code;
	private String message;
	private Map<String,Object> data;
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Map<String,Object> getData() {
		return data;
	}
	public void setData(Map<String,Object> data) {
		this.data = data;
	}
}
