package com.beawan.task.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.beawan.common.Constants;
import com.beawan.core.Pagination;
import com.beawan.corporateloan.service.LmProcessService;
import com.beawan.customer.dto.WarnItemDto;
import com.beawan.customer.service.FinriskResultService;
import com.beawan.common.config.AppConfig;
import com.beawan.scoreCard.service.IAssessCardSV;
import com.beawan.survey.custInfo.bean.CompRunAnalysis;
import com.beawan.survey.custInfo.dto.CompBaseDto;
import com.beawan.survey.custInfo.service.compRunAnalysisService;
import com.google.gson.JsonParser;
import com.platform.util.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;

import com.beawan.analysis.abnormal.service.IAbnormalService;
import com.beawan.analysis.finansis.service.IFinanasisService;
import com.beawan.base.entity.User;
import com.beawan.base.service.IPlatformLogSV;
import com.beawan.base.service.ISysDicSV;
import com.beawan.base.service.ISysTreeDicSV;
import com.beawan.base.service.ITableSubjCodeSV;
import com.beawan.base.service.IUserSV;
import com.beawan.common.BusinessException;
import com.beawan.common.SysConstants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.bean.FinriskResult;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.customer.service.ICusFSToolSV;
import com.beawan.dtmp.service.DtmpService;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.beawan.library.service.IIndustrySV;
import com.beawan.library.service.ITempletSV;
import com.beawan.loanAfter.service.LfTaskService;
import com.beawan.rate.entity.RcCondition;
import com.beawan.rate.entity.RcModel;
import com.beawan.rate.entity.RcQuota;
import com.beawan.rate.dto.RateQuotaDto;
import com.beawan.rate.dto.RateResult;
import com.beawan.rate.service.IRcModelSV;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.beawan.survey.custInfo.service.ICompFinancingSV;
import com.beawan.survey.loanInfo.bean.ApplyRateInfo;
import com.beawan.survey.loanInfo.service.ApplyRateInfoService;
import com.beawan.survey.loanInfo.service.IGuaraInfoSV;
import com.beawan.survey.loanInfo.service.ISurveyReportSV;
import com.beawan.task.bean.Task;
import com.beawan.task.dto.DGTaskDto;
import com.beawan.task.dto.RepaySchuDto;
import com.beawan.task.service.ITaskSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import org.springframework.web.servlet.ModelAndView;

/**
 * 任务web层
 * 
 * @author beawan_fengjj
 *
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/task" })
public class TaskCtl extends BaseController {

	private static Logger log = Logger.getLogger(TaskCtl.class);

	@Resource
	private ITaskSV taskSV;
	@Resource
	private IFinanasisService finanasisSV;
	@Resource
	private ITempletSV templetSV;
	@Resource
	private IIndustrySV industrySV;
	@Resource
	private IAbnormalService abnormalService;
	@Resource
	private ICmisInInvokeSV cmisInInvokeSV;
	@Resource
	private ICusBaseSV cusBaseSV;
	@Resource
	private ISysDicSV sysDicSV;
	@Resource
	private ISysTreeDicSV sysTreeDicSV;
	@Resource
	private ITableSubjCodeSV tableSubjCodeSV;
	@Resource
	private IPlatformLogSV platformLogSV;
	@Resource
	private ISurveyReportSV surveyReportSV;
	@Resource
	private IUserSV userSV;
	@Resource
	private ICompBaseSV compBaseSV;
	@Resource
	private IGuaraInfoSV guaraInfoSV;
	@Resource
	private ICusFSToolSV cusFSToolSV;
	@Resource
	private IRcModelSV rcModelSV;
	@Resource
	private LmProcessService lmProcessService;
	@Resource
	private FinriskResultService finriskResultService;
	@Resource
	private compRunAnalysisService compAnalysisService;
	@Resource
	private IAssessCardSV assessCardSV;
	@Resource
	private LfTaskService lfTaskService;

	
	@Resource
	private DtmpService dtmpService;
	@Resource
	private ApplyRateInfoService applyRateInfoService;
	@Resource
	protected  ICompFinancingSV financingSV;
	


	
	

	/**
	 * 企业客户基本信息及财报信息完整性检查
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("checkComCusInfo.do")
	@ResponseBody
	public String checkComCusInfo(HttpServletRequest request, HttpServletResponse response, String customerNo,
			int finaRepYear, int finaRepMonth) throws Exception {

		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();

		try {
			String checkDate = "";
			Calendar cal = Calendar.getInstance();
			int nowYear = cal.get(Calendar.YEAR);
			int nowMonth = cal.get(Calendar.MONTH) + 1;
			int diffenceYear = nowYear - finaRepYear;
			int diffenceMonth = 0;
			if(diffenceYear == 0){
				diffenceMonth = nowMonth - finaRepMonth;
			}else{
				diffenceMonth = 12 * diffenceYear + nowMonth - finaRepMonth;
			}
			if(diffenceMonth > 3){
				checkDate = "财报年份与当前时间超过3个月";
				json.put("checkDate", checkDate);
			}else if(diffenceMonth < 0){
				checkDate = "财报选择年月超过当前年月";
				json.put("checkDate", checkDate);
			}
			
			
			cusFSToolSV.checkCusInfoRule(customerNo, finaRepYear, finaRepMonth);
			
			json.put("result", true);

		} catch (BusinessException be) {

			json.put("result", true);
			json.put("msg", be.getMessage());
			log.error("客户信息完整性检查：" + be.getMessage());

		} catch (Exception e) {

			json.put("result", false);
			json.put("msg", "系统异常！");
			log.error("客户信息完整性检查异常：", e);
		}

		return jRequest.serialize(json, true);
	}

	/**
	 * 新增授信任务 1.检查客户信息 是否已经存在 不存在则新增客户信息 2.客户信息的数据准备
	 * 
	 * @param taskInfo
	 * @return
	 */
	@RequestMapping("saveTaskAndDataReady.json")
	@ResponseBody
	public ResultDto saveTaskAndDataReady(HttpServletRequest request, String taskInfo) {
		ResultDto re = returnFail("");
		if (taskInfo == null) {
			re.setMsg("企业授信信息异常,新增授信失败");
			return re;
		}
		try {
			User user = HttpUtil.getCurrentUser(request);
			DGTaskDto taskDto = GsonUtil.GsonToBean(taskInfo, DGTaskDto.class);
			if (taskDto == null) {
				ExceptionUtil.throwException("企业授信信息异常,新增授信失败");
			}
			/**
			 * 创建贷前调查任务，并获取天眼查数据 因为创建失败希望回滚，所以放入到serice层
			 */
			taskSV.createTaskInfoAndSyncTyc(taskDto, user);
			// 添加日志
			platformLogSV.saveOperateLog("新增贷前调查任务", SysConstants.PlatformLogType.NEW_TASK, user.getUserId());

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("新增授信任务成功");
		} catch (BusinessException e) {
			log.error(e.getMessage());
			re.setMsg(e.getMessage());
			return re;
		} catch (Exception e) {
			log.error(e.getMessage());
			re.setMsg("系统异常，请联系管理员");
			return re;
		}
		return re;
	}

	/**
	 * 保存任务
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("saveTask.do")
	@ResponseBody
	public String saveTask(HttpServletRequest request, HttpServletResponse response) throws Exception {

		User user = HttpUtil.getCurrentUser(request);
		String dataJSON = request.getParameter("taskInfo");
		String finaAnalyFlag = request.getParameter("finaAnalyFlag");

		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		Task task = null;
		try {
			
			task = JacksonUtil.fromJson(dataJSON, Task.class);
			task.setUserNo(user.getUserId());
			task.setUserName(user.getUserName());
			task.setUpdater(user.getUserId());
			task = taskSV.saveTaskInfo(task, finaAnalyFlag);
			json.put("taskId", task.getId());
			lmProcessService.saveLbTask(task.getSerNo(), task.getCustomerNo(), user.getUserId(), user.getUserName(),
					user.getUserId());
			
			Long id = task.getId();
			
			ApplyRateInfo entity = applyRateInfoService.selectSingleByProperty("taskId", id);
			if(entity != null){
				entity.setExecuteRate(Double.parseDouble(task.getLoanRate()));
				entity.setUpdater(user.getUserId());
				entity.setUpdateTime(DateUtil.getNowTimestamp());
				applyRateInfoService.saveOrUpdate(entity);
			}else{
				entity = new ApplyRateInfo();
				entity.setCreater(user.getUserId());
				entity.setCreateTime(DateUtil.getNowTimestamp());
				if(!StringUtils.isEmpty(task.getLoanRate())){
					entity.setExecuteRate(Double.parseDouble(task.getLoanRate()));
				}
				entity.setUpdater(user.getUserId());
				entity.setUpdateTime(DateUtil.getNowTimestamp());
				applyRateInfoService.saveOrUpdate(entity);
			}
			
			CusBase cusBase = cusBaseSV.queryByCusNo(task.getCustomerNo());

		
//			CusBase cusBase = cusBaseSV.queryByCusName(task.getCustomerName());
			String xdCustNo = cusBase.getXdCustomerNo();
			String serNo = task.getSerNo();
			if(!StringUtils.isEmpty(xdCustNo) && !StringUtils.isEmpty(serNo)){
				/**
				 * 获取担保企业信息
				 */
				dtmpService.syncGuaCompany(xdCustNo, serNo);
						
				/**
				 * 获取担保自然人担保信息
				 */
				dtmpService.syncGuaPerson(xdCustNo,serNo);
				/**
				 * 同步获取房地产抵押担保信息（从信贷系统中获取）
				 */
				dtmpService.syncGuaEstate(xdCustNo,serNo);
				
				/**
				 * 同步获取动产抵押担保信息（从信贷系统中获取）
				 */
				dtmpService.syncGuaChattel( xdCustNo,serNo);
				/**
				 * 同步获取应收账款质押担保信息（从信贷系统中获取）
				 */
				dtmpService.syncGuaAccounts( xdCustNo,serNo);
	
				/**
				 * 同步获取存单质押担保信息（从信贷系统中获取）
				 */
				dtmpService.syncGuaDepositReceipt(xdCustNo,serNo);
				/**
				 * 同步企业对外担保信息（只涉及企业）
				 */
				dtmpService.syncExtGuara(xdCustNo, task.getId(), task.getCustomerNo());
				
				/**
				 * 同步企业关联关系
				 */
				dtmpService.syncCompConnetCm(xdCustNo, task.getCustomerNo());
				//同步客户在我行在风险预警信息
				dtmpService.syncWmWarning(task.getId(), cusBase.getXdCustomerNo());
				//同步客户在我行授信信息
				financingSV.syncCompFinaBankFromCmis(task.getId(), task.getCustomerNo());
	
				// 开始财务分析 这一步放到第一次贷前调查按钮上
				finriskResultService.syncFinriskResult(task.getSerNo(), task.getCustomerNo(), task.getIndustry());
				
				/**
				 * 获取担保企业财政信息
				 */
				dtmpService.syncCompFinanceInfo(serNo);
			}
			// 开始行业分析
			try {
				CompRunAnalysis runAnalysis = compAnalysisService.selectSingleByProperty("serNo", task.getSerNo());
				if (runAnalysis == null || StringUtil.isEmptyString(runAnalysis.getIndustryInfo())) {
					runAnalysis = compAnalysisService.syncInduAnalysis(task.getSerNo());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 进行客户评级测算
			assessCardSV.syncCalcAssetResult(task.getSerNo());
			
			// 同步客户在我行融资情况
			// taskSV.insertCreditSelfBankInfo(task.getId());
			// finriskResultService.getInduAnalysis("");
			/**
			 * 开启贷前调查流程
			 */
			// ProcessInstance processInst = surveyProcSV.startWorkflow(task,
			// user.getUserId());
			// if(processInst != null) {
			//
			// System.out.println("贷前调查流程启动并执行第一个节点成功");
			// }
			
			
		
			
			
			
			
			platformLogSV.saveOperateLog("新增贷前调查任务", SysConstants.PlatformLogType.NEW_TASK, user.getUserId());
			json.put("result", true);

		} catch (BusinessException be) {
			json.put("result", false);
			json.put("msg", be.getMessage());
			log.error(" 保存任务异常：" + be.getMessage());
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", "系统异常");
			log.error(" 保存任务异常", e);
		}
		return jRequest.serialize(json, true);
	}

	/**
	 * 测试流动资金缺口数据
	 * 
	 * @return
	 */
	@RequestMapping("testLDZJ.json")
	@ResponseBody
	public ResultDto testLDZJ() {
		ResultDto re = returnFail();
		Map<String, String> params = new HashMap<>();
		try {

			params.put("taskId", "24186");
			// 获得解析器
			JsonParser parser = new JsonParser();

			// 对接本地财务分析 并保存结果
			HttpClientUtil instance3 = HttpClientUtil.getInstance(AppConfig.Cfg.GET_QUOTA_INFO);
			String responseBody = instance3.doPost(params);
			responseBody = URLDecoder.decode(responseBody, "utf-8");

			Integer resultCode = parser.parse(responseBody).getAsJsonObject().get("code").getAsInt();
			String resultMsg = parser.parse(responseBody).getAsJsonObject().get("msg").getAsString();
			if (resultCode == ResultDto.RESULT_CODE_FAIL) {
				ExceptionUtil.throwException(resultMsg);
			}
			String localResponse = parser.parse(responseBody).getAsJsonObject().get("rows").toString();
			re.setRows(localResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	@RequestMapping("submitModifyTask.json")
	@ResponseBody
	public String submitModifyTask(HttpServletRequest request, HttpServletResponse response) throws Exception {

		User user = HttpUtil.getCurrentUser(request);
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);

		try {

			String dataJSON = request.getParameter("taskInfo");
			String finaAnalyFlag = request.getParameter("finaAnalyFlag");

			Task newTask = JacksonUtil.fromJson(dataJSON, Task.class);

			Task task = taskSV.getTaskById(newTask.getId());

			task.setLoanNumber(newTask.getLoanNumber());
			task.setLoanTerm(newTask.getLoanTerm());
			task.setIndustry(newTask.getIndustry());
			task.setYear(newTask.getYear());
			task.setMonth(newTask.getMonth());

			if (!StringUtil.isEmpty(newTask.getFinaSyncFlag()))
				task.setFinaSyncFlag(newTask.getFinaSyncFlag());
			if (!StringUtil.isEmpty(newTask.getFinaRepRemark()))
				task.setFinaRepRemark(newTask.getFinaRepRemark());

			task = this.taskSV.saveTaskInfo(task, finaAnalyFlag);

			platformLogSV.saveOperateLog("修改贷前调查任务", SysConstants.PlatformLogType.NEW_TASK, user.getUserId());

			json.put("result", true);

		} catch (BusinessException b) {

			json.put("msg", b.getMessage());

		} catch (Exception e) {

			json.put("msg", "系统异常!");
			log.error(" 保存任务异常", e);
		}

		return jRequest.serialize(json, true);
	}

	@RequestMapping("reanalyzeFina.json")
	@ResponseBody
	public String reanalyzeFina(HttpServletRequest request, HttpServletResponse response, long taskId)
			throws Exception {

		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);

		try {

			Task task = taskSV.getTaskById(taskId);
			task.setFinaSyncFlag(SysConstants.CusFinaValidFlag.TEMP_UNAVAIL);
			finanasisSV.saveFinaDataAndAnalyze(task, "1");

			json.put("result", true);

		} catch (BusinessException b) {

			json.put("msg", b.getMessage());

		} catch (Exception e) {

			json.put("msg", "系统异常！");
			log.error("重新分析系统异常：", e);
		}

		return jRequest.serialize(json, true);
	}

	/**
	 * 返回任务列表
	 * 
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("saveTaskPage.do")
	public String saveTaskPage(HttpServletRequest request, HttpServletResponse response, String id) {
		return "views/survey/task/pendingTaskList";
	}

	/**
	 * 删除任务
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("deleteTask.do")
	@ResponseBody
	public Map<String, Object> deleteTask(HttpServletRequest request, HttpServletResponse response, long taskId) {

		Map<String, Object> json = new HashMap<String, Object>();
		json.put("success", false);

		try {
			Task task = taskSV.getTaskById(taskId);
			task.setStatus(Constants.DELETE);
			taskSV.saveOrUpdate(task);
//			taskSV.deleteTask(taskId, HttpUtil.getCurrentUser(request));

			User user = HttpUtil.getCurrentUser(request);
			platformLogSV.saveOperateLog("删除贷前调查任务", SysConstants.PlatformLogType.DELETE_TASK, user.getUserId());

			json.put("success", true);
		} catch (BusinessException e) {
			json.put("msg", e.getMessage());
			log.error(" 删除任务异常：" + e.getMessage());
		} catch (Exception e) {
			log.error(" 删除任务异常：", e);
		}

		return json;
	}

	/**
	 * @Description 新增任务
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("addTask.do")
	public String addTask(HttpServletRequest request, HttpServletResponse response) {

		try {
			User user = HttpUtil.getCurrentUser(request);
			request.setAttribute("userName", user.getUserName());
			request.setAttribute("orgNo", user.getAccOrgNo());

			request.setAttribute("orgName", cmisInInvokeSV.queryOrgName(user.getAccOrgNo()));

			// 加载字典数据
			String[] optTypes = { SysConstants.BsDicConstant.STD_SY_COM_SURVEY_TYPE };
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));

		} catch (Exception e) {
			e.printStackTrace();
			log.error(" 跳转新增申请页面异常", e);
		}
		return "views/survey/task/newTask";
	}

	/**
	 * 获取授信任务信息数据
	 * 
	 * @param taskId
	 * @return
	 *//*
		 * @RequestMapping("getTaskInfo.json")
		 * 
		 * @ResponseBody public ResultDto getTaskInfo(Long taskId) { ResultDto
		 * re = returnFail(""); Map<String, Object> data = new HashMap<>(); try
		 * { Task task = taskSV.getTaskById(taskId);
		 * 
		 * //贷款用途 List<SysDic> useType =
		 * sysDicSV.findByType(SysConstants.BsDicConstant.STD_ZB_USE_TYPE);
		 * //担保方式 List<SysDic> guarType =
		 * sysDicSV.findByType(SysConstants.BsDicConstant.STD_SY_GUAR_TYPE);
		 * //还款方式 List<SysDic> replyWay =
		 * sysDicSV.findByType(SysConstants.BsDicConstant.STD_REPLY_WAY);
		 * 
		 * 
		 * //通过选择的产品工厂 计算利率 Long productId = task.getProductId(); LoanProPrice
		 * proPrice = loanProPriceSV.selectSingleByProperty("productId",
		 * productId); if(proPrice!=null) { Long priceId = proPrice.getId();
		 * RcModel rcModel = rcModelSV.queryById(priceId);
		 * 
		 * int loanTerm=task.getLoanTerm(); double basePrice=0; if(loanTerm<=1)
		 * { basePrice=4.35; }else if(loanTerm<=5) { basePrice=4.75; }else {
		 * basePrice=4.9; } Double dbfs =
		 * calcluate(rcModel,"DBFS","抵押-其他房地产").getValue(); Double xyjl
		 * =calcluate(rcModel,"XYJL","无不良记录").getValue(); Double xhed =
		 * calcluate(rcModel,"XHED","循环额度形式").getValue(); Double khpj =
		 * calcluate(rcModel,"KHPJ","AAA类客户").getValue();
		 * 
		 * Double sxje =
		 * calcluate(rcModel,"SXJE",Double.valueOf(1200)).getValue(); Double
		 * khzc = calcluate(rcModel,"KHZC","基本户开立在本行").getValue();
		 * 
		 * Double resultRate=basePrice*(1+khpj)+dbfs+xyjl+xhed+sxje+khzc;
		 * 
		 * resultRate = new BigDecimal(resultRate).setScale(2,
		 * RoundingMode.HALF_UP) .doubleValue();
		 * 
		 * data.put("rcModel", rcModel); data.put("rate", resultRate); }
		 * 
		 * 
		 * data.put("task", task); data.put("useType", useType);
		 * data.put("guarType", guarType); data.put("replyWay", replyWay);
		 * 
		 * re.setRows(data); re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		 * re.setMsg("获取授信任务信息数据成功");
		 * 
		 * } catch (Exception e) { e.printStackTrace();
		 * re.setMsg("获取授信任务信息数据失败"); } return re; }
		 */

	/**
	 * 获取 还款计划表
	 * 
	 * @param taskId
	 * @return
	 */
	@RequestMapping("getRepaySchu.json")
	@ResponseBody
	public ResultDto getRepaySchu(Long taskId, Double repayVal, Integer loanYear, Double rc, Integer repayWay) {
		ResultDto re = returnFail("");
		try {
			List<RepaySchuDto> list = new ArrayList<>();
			repayVal = 500000d;
			loanYear = 3;
			rc = 0.059;// 5.9%
			/*
			 * 一次性归还借款本息 1 按期结息到期还本 2 分期还款 3 等额本金 4 等额本息 5
			 */
			if (repayWay == 1) {
				Double lixi = repayVal * (Math.pow((1 + rc), loanYear) - 1);
				String repayTime = DateUtil.format(DateUtil.addYear(new Date(), loanYear), "yyyy/MM/dd");
				RepaySchuDto repay = new RepaySchuDto(1, repayTime,
						new BigDecimal(repayVal).setScale(2, RoundingMode.HALF_UP).doubleValue(),
						new BigDecimal(lixi).setScale(2, RoundingMode.HALF_UP).doubleValue());
				list.add(repay);
				re.setRows(list);
				re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			} else if (repayWay == 4) {
				// 等额本金 每月还款本金=贷款总额/贷款月数
				Double benjin = repayVal / (loanYear * 12);
				// 每月还款利息=贷款本金余额*贷款月利率（ 贷款月利率=年利率/12）
				// 贷款本金余额=贷款总额-已还款月数*每月还款本金
				for (int i = 1; i <= loanYear * 12; i++) {
					String repayTime = DateUtil.format(DateUtil.addMonth(new Date(), i), "yyyy/MM/dd");
					Double lixi = (repayVal - i * benjin) * (rc / 12);
					RepaySchuDto repay = new RepaySchuDto(i, repayTime,
							new BigDecimal(benjin).setScale(2, RoundingMode.HALF_UP).doubleValue(),
							new BigDecimal(lixi).setScale(2, RoundingMode.HALF_UP).doubleValue());
					list.add(repay);
				}
				re.setRows(list);
				re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			} else if (repayWay == 5) {
				// 等额本息
				// 每月应还款额＝借款本金×月利率×（1＋月利率）^还款月数/[（1＋月利率）^还款月数－1]
				Double monthRc = rc / 12;
				Double every = repayVal * monthRc * Math.pow(1 + monthRc, loanYear * 12)
						/ (Math.pow(1 + monthRc, loanYear * 12) - 1);
				// System.out.println("等额本息: "+ new
				// BigDecimal(every).setScale(2,
				// RoundingMode.HALF_UP).doubleValue());
				re.setRows(list);
				re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	private static RateResult calcluate(RcModel rcModel, String conditionName, Object conditionValue) {
		RateResult rateResult = new RateResult();
		if (rcModel == null)
			return rateResult;
		List<RcQuota> rcQuotas = rcModel.getQuotas();
		if (rcQuotas == null || rcQuotas.size() == 0)
			return rateResult;

		for (RcQuota rcQuota : rcQuotas) {
			if (rcQuota.getCode() != null && rcQuota.getCode().equals(conditionName)) {
				rateResult = getRateResult(rcQuota, conditionName, conditionValue);
				rcQuota.setValue(conditionValue.toString());
				rcQuota.setRateResult(rateResult);
				break;
			}
		}
		return rateResult;
	}

	private static RateResult getRateResult(RcQuota rcQuota, String conditionName, Object conditionValue) {
		RateResult rateResult = new RateResult();

		if (rcQuota == null)
			return rateResult;
		List<RcCondition> rcConditions = rcQuota.getConditions();
		if (rcConditions == null || rcConditions.size() == 0)
			return rateResult;
		RcCondition findCondition = null;
		if (rcQuota.getType() != null && rcQuota.getType().equals("S")) {
			for (RcCondition condition : rcConditions) {
				if (conditionValue.equals(condition.getContent())) {
					findCondition = condition;
					break;
				}
			}

		} else {
			for (RcCondition condition : rcConditions) {
				if (fit(condition, (Double) conditionValue)) {
					findCondition = condition;
					break;
				}
			}
		}
		if (findCondition == null)
			return rateResult;
		RateQuotaDto resultQuota = findCondition.getResultQuota();
		rateResult.setCode(resultQuota.getCode());
		rateResult.setName(resultQuota.getName());
		rateResult.setUnit(resultQuota.getUnit());
		if (resultQuota.getUnit() != null && !"null".equals(resultQuota.getUnit())) {
			rateResult.setValueString(findCondition.getResultValue().toString() + resultQuota.getUnit());
		} else {
			rateResult.setValueString(findCondition.getResultValue().toString());
		}

		if (resultQuota.getUnit() != null && resultQuota.getUnit().equals("%")) {
			rateResult.setValue(findCondition.getResultValue() / 100);
		} else {
			rateResult.setValue(findCondition.getResultValue());
		}

		return rateResult;
	}

	private static boolean fit(RcCondition condition, Double conditionValue) {
		boolean isfit = false;
		Double downDouble = condition.getDown();
		Double upperDouble = condition.getUpper();
		if (downDouble == null || upperDouble == null)
			return isfit;
		if (downDouble > upperDouble) {
			Double value = upperDouble;
			upperDouble = downDouble;
			downDouble = value;
		}
		String type = "OUT";
		if (conditionValue <= upperDouble && conditionValue >= downDouble) {
			type = "IN";
			if (conditionValue == upperDouble)
				type = "L";
			if (conditionValue == downDouble)
				type = "R";
		}
		String conditionType = condition.getAttrValue();
		if (conditionType == null || type == "OUT")
			return isfit;
		if (conditionType == "L") {
			if (type == "IN" || type == "L") {
				isfit = true;
			}
		} else if (conditionType == "R") {
			if (type == "IN" || type == "R") {
				isfit = true;
			}
		} else {
			if (type == "IN") {
				isfit = true;
			}
		}
		return isfit;
	}

	/**
	 * 跳转修改任务页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("modifyTask.do")
	public String modifyTask(HttpServletRequest request, HttpServletResponse response) {
		try {
			Long id = HttpUtil.getAsLong(request, "id");
			Task task = taskSV.getTaskById(id);
			request.setAttribute("task", task);

			String industryFullName = sysTreeDicSV.getFullNameByEnName(task.getIndustry(),
					SysConstants.TreeDicConstant.GB_IC_4754_2017);
			request.setAttribute("industryFullName", industryFullName);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("跳转修改任务页面异常！", e);
		}
		return "views/survey/task/modifyTask";
	}

	/**
	 * @Description 跳转调查信息编辑页面
	 * @param request
	 * @param response
	 * @param id
	 * @return
	 */
	@RequestMapping("toSurveyPage.do")
	public String toSurveyPage(HttpServletRequest request, HttpServletResponse response, String id) {
		try {
			Task task = this.taskSV.getTaskById(Long.valueOf(id));
			request.setAttribute("task", task);

			request.setAttribute("taskId", task.getId());
			request.setAttribute("customerName", task.getCustomerName());
			request.setAttribute("customerNo", task.getCustomerNo());
			request.setAttribute("loanType", task.getBusinessNo());
			request.setAttribute("loanName", task.getBusinessType());
			// 小微企业主贷款用
			request.setAttribute("cmBaseId", task.getCmBaseId());

			// 财务信息未保存分析
			if (SysConstants.CusFinaValidFlag.TEMP_UNAVAIL.equals(task.getFinaSyncFlag()))
				finanasisSV.saveFinaDataAndAnalyze(task, "1");

		} catch (BusinessException be) {
			if ("CUS_FS".equals(be.getFlag()))
				request.setAttribute("cusFSNotice", be.getMessage());
			log.error("跳转调查信息编辑页面异常！" + be.getMessage());
		} catch (Exception e) {
			log.error("跳转调查信息编辑页面异常！", e);
		}
		return "views/survey/survey_index";
	}
	
	/**
	 * 财务报表是否满足系统分析要求判断
	 * 
	 * @param request
	 * @param id
	 *            贷前调查任务 id
	 * @return
	 */
	@RequestMapping("cusFinaValidFlag.json")
	@ResponseBody
	public ResultDto cusFinaValidFlag(HttpServletRequest request, String id) {
		ResultDto re = returnFail("财务报表暂时不能满足系统分析要求，创建贷前任务失败");
		Task task = this.taskSV.getTaskById(Long.valueOf(id));
		try {
			if (SysConstants.CusFinaValidFlag.TEMP_UNAVAIL.equals(task.getFinaSyncFlag()))
				finanasisSV.saveFinaDataAndAnalyze(task, "1");
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("创建贷前任务成功");
		
		} catch (Exception e) {
			re.setMsg("系统异常，请联系管理员！");
			log.error("创建贷前任务异常！", e);
			e.printStackTrace();
		}
		return re;
	}

	
	@RequestMapping("toSurveyPage2.do")
	public ModelAndView toSurveyPage2(HttpServletRequest request, String id) {
		ModelAndView mav = new ModelAndView();
		try {
			/*log.info(1);*/
			// 获取任务信息 项目
			Task task = this.taskSV.getTaskById(Long.valueOf(id));
			//log.info(2);
			String serNo = task.getSerNo();
			String loanType = task.getBusinessNo();
			FinriskResult finriskResult = finriskResultService.getLocalFinrisk(serNo);
			//log.info(3);
			if (finriskResult != null) {
				String resultId = finriskResult.getResultTaskId();
				mav.addObject("eightCashflow", AppConfig.Cfg.EIGHT_CASH_FLOW + "?taskId=" + resultId);
			}
			switch (loanType) {
				case Constants.LB_SURVEY_TYPE.COMMON:
				case Constants.LB_SURVEY_TYPE.COMMERCIAL_TRADE:
				case Constants.LB_SURVEY_TYPE.TEXTILE:
				case Constants.LB_SURVEY_TYPE.FOOD:
				case Constants.LB_SURVEY_TYPE.MANUFACTURE:
					mav.setViewName("views/survey/template/commonSurvey");
					break;
				case Constants.LB_SURVEY_TYPE.BUILDING:
					mav.setViewName("views/survey/template/buildingSurvey");
					break;
				case Constants.LB_SURVEY_TYPE.PROJECT:
					mav.setViewName("views/survey/template/projectSurvey");
					break;
				default:
					break;
			}
			//log.info(4);
			mav.addObject("taskId", task.getId());
			mav.addObject("serNo", serNo);
			mav.addObject("customerNo", task.getCustomerNo());
			mav.addObject("customerName", task.getCustomerName());
			mav.addObject("loanType", loanType);
			// 财务信息未保存分析
			if (SysConstants.CusFinaValidFlag.TEMP_UNAVAIL.equals(task.getFinaSyncFlag()))
				finanasisSV.saveFinaDataAndAnalyze(task, "1");
			//log.info(5);
			// 是否存在财务异常预警
			List<WarnItemDto> warnItemList = finriskResultService.getFinaWarn(serNo);
			//log.info(6);
			if (!CollectionUtils.isEmpty(warnItemList))
				mav.addObject("warnListSize", warnItemList.size());
		} catch (BusinessException e) {
			if ("CUS_FS".equals(e.getFlag()))
				mav.addObject("cusFSNotice", e.getMessage());
			log.error("跳转调查信息编辑页面异常！" + e.getMessage());
		} catch (Exception e) {
			log.error("跳转调查信息编辑页面异常！", e);
		}
		return mav;
	}

	@RequestMapping("toViewInfoPage.do")
	public ModelAndView toViewInfoPage(HttpServletRequest request, String id) {
		ModelAndView mav = new ModelAndView();
		try {
			// 获取任务信息 项目
			Task task = this.taskSV.getTaskById(Long.valueOf(id));
			String serNo = task.getSerNo();
			String loanType = task.getBusinessNo();
			FinriskResult finriskResult = finriskResultService.getLocalFinrisk(serNo);
			if (finriskResult != null) {
				String resultId = finriskResult.getResultTaskId();
				mav.addObject("eightCashflow", AppConfig.Cfg.EIGHT_CASH_FLOW + "?taskId=" + resultId);
			}
			switch (loanType) {
			case Constants.LB_SURVEY_TYPE.COMMON:
			case Constants.LB_SURVEY_TYPE.COMMERCIAL_TRADE:
			case Constants.LB_SURVEY_TYPE.TEXTILE:
			case Constants.LB_SURVEY_TYPE.FOOD:
			case Constants.LB_SURVEY_TYPE.MANUFACTURE:
				mav.setViewName("views/survey/template/commonSurveyView");
				break;
			case Constants.LB_SURVEY_TYPE.BUILDING:
				mav.setViewName("views/survey/template/buildingSurveyView");
				break;
			case Constants.LB_SURVEY_TYPE.PROJECT:
				mav.setViewName("views/survey/template/projectSurveyView");
				break;
			default:
				break;
			}
			mav.addObject("taskId", task.getId());
			mav.addObject("serNo", serNo);
			mav.addObject("customerNo", task.getCustomerNo());
			mav.addObject("customerName", task.getCustomerName());
			mav.addObject("loanType", loanType);
			// 财务信息未保存分析
			if (SysConstants.CusFinaValidFlag.TEMP_UNAVAIL.equals(task.getFinaSyncFlag()))
				finanasisSV.saveFinaDataAndAnalyze(task, "1");
			// 是否存在财务异常预警
			List<WarnItemDto> warnItemList = finriskResultService.getFinaWarn(serNo);
			if (!CollectionUtils.isEmpty(warnItemList))
				mav.addObject("warnListSize", warnItemList.size());
		} catch (BusinessException e) {
			if ("CUS_FS".equals(e.getFlag()))
				mav.addObject("cusFSNotice", e.getMessage());
			log.error("跳转调查信息编辑页面异常！" + e.getMessage());
		} catch (Exception e) {
			log.error("跳转调查信息编辑页面异常！", e);
		}
		return mav;
	}

	/**
	 * 提交调查任务
	 * 
	 * @param request
	 * @param id
	 *            贷前调查任务 id
	 * @return
	 */
	@RequestMapping("submitSurveyTask.json")
	@ResponseBody
	public ResultDto submitSurveyTask(HttpServletRequest request, Long id) {
		ResultDto re = returnFail("提交贷前调查报告失败");
		if (id == null || id == 0) {
			re.setMsg("任务参数异常，请重试");
			return re;
		}
		User currentUser = HttpUtil.getCurrentUser(request);
		try {
			// Task task = taskSV.getTaskById(id);
			taskSV.submitSurveyTask(id, currentUser.getUserId());

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("提交贷前调查报告成功");
		} catch (BusinessException b) {
			re.setMsg(b.getMessage());
			b.printStackTrace();
		} catch (Exception e) {
			re.setMsg("系统异常，请联系管理员！");
			log.error("提交调查任务异常！", e);
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 打回调查任务
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("returnSurveyTask.json")
	@ResponseBody
	public Map<String, Object> returnSurveyTask(HttpServletRequest request, HttpServletResponse response) {

		Map<String, Object> json = new HashMap<String, Object>();
		json.put("success", false);

		try {
			User currUser = HttpUtil.getCurrentUser(request);

			Long id = HttpUtil.getAsLong(request, "id");
			Task task = taskSV.getTaskById(id);

			if (currUser.getUserId().equals(task.getUserNo()))
				ExceptionUtil.throwException("对不起，您没有权限打回该笔业务！");

			task.setState(SysConstants.TaskState.TO_DEAL_WITH);
			taskSV.saveOrUpdate(task);

			json.put("success", true);
		} catch (BusinessException b) {
			json.put("msg", b.getMessage());
		} catch (Exception e) {
			json.put("msg", "系统异常，请联系管理员！");
			log.error("打回调查任务异常！", e);
		}

		return json;
	}

	/**
	 * 主动撤回调查任务
	 * @param request
	 * @return
	 */
	@RequestMapping("withdrawTask.json")
	@ResponseBody
	public Map<String, Object> withdrawSurveyTask(HttpServletRequest request) {

		Map<String, Object> json = new HashMap<String, Object>();
		json.put("success", false);

		try {
			User currUser = HttpUtil.getCurrentUser(request);

			Long id = HttpUtil.getAsLong(request, "id");
			Task task = taskSV.getTaskById(id);

			if (!currUser.getUserId().equals(task.getUserNo()))
				ExceptionUtil.throwException("对不起，您没有权限撤回该笔业务！");

			task.setState(SysConstants.TaskState.TO_DEAL_WITH);
			taskSV.saveOrUpdate(task);

			json.put("success", true);
		} catch (BusinessException b) {
			json.put("msg", b.getMessage());
		} catch (Exception e) {
			json.put("msg", "系统异常，请联系管理员！");
			log.error("撤回调查任务异常！", e);
		}
		return json;
	}

	/**
	 * @Description 跳转查询客户页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("toQueryCustPage.do")
	public String toQueryCustPage(HttpServletRequest request, HttpServletResponse response) {

		try {
			String customerType = request.getParameter("customerType");
			String loanType = request.getParameter("loanType");
			request.setAttribute("loanType", loanType);
			request.setAttribute("customerType", customerType);

			// 加载字典数据
			String[] optTypes = { SysConstants.BsDicConstant.STD_SY_ODS0001 };
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));

		} catch (Exception e) {
			log.error("跳转查询客户页面异常：", e);
		}

		return "views/survey/task/query_cust";
	}

	/**
	 * 查询客户列表
	 * @param request
	 * @param response
	 * @param search 查询条件
	 * @param order 排序条件
	 * @param page 页码，从1开始
	 * @param rows 每页行数 
	 * @return
	 */
	@RequestMapping("getCusBaseList.json")
	@ResponseBody
	public String getCusBaseList(HttpServletRequest request, HttpServletResponse response, String search, String order,
			int page, int rows) {

		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();

		try {
			CusBase queryCondition = null;
			if (!StringUtil.isEmptyString(search)) {
				queryCondition = JacksonUtil.fromJson(search, CusBase.class);
			}
			if (queryCondition == null)
				queryCondition = new CusBase();
			/*
			 * User user = HttpUtil.getCurrentUser(request); if
			 * (user.getRoleNo().contains("0001")) {
			 * queryCondition.setManagerUserId(user.getUserId()); }
			 * 
			 * //默认查对公客户
			 * if(StringUtil.isEmptyString(queryCondition.getCustomerType()))
			 * queryCondition.setCustomerType(SysConstants.CusNoType.COMP_CUS);
			 */

			List<CusBase> list = cmisInInvokeSV.queryCustomerPaging(queryCondition, order, page - 1, rows);
			if (list == null)
				list = new ArrayList<CusBase>();
			long total = cmisInInvokeSV.queryCustomerCount(queryCondition);
			json.put("total", total);
			json.put("rows", list);

		} catch (Exception e) {
			e.printStackTrace();
			log.error(" 获取获取客户列表异常", e);
		}
		return jRequest.serialize(json, true);
	}

	@RequestMapping("getCusIndustry.json")
	@ResponseBody
	public String getCusIndustry(HttpServletRequest request, HttpServletResponse response, String customerNo) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();

		try {

			CompBaseDto compBase = cmisInInvokeSV.queryCompBaseInfo(customerNo);
			if (compBase != null) {
				json.put("industryCode", compBase.getIndustryCode());
				json.put("industryName", compBase.getIndustryType());
			}

			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			log.error(" 获取获取客户所属行业失败", e);
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}

	/**
	 * 跳转协办客户经理签到页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("xbCusMgeSignInHtml.do")
	public String xbCusMgeSignInHtml(HttpServletRequest request, HttpServletResponse response) {
		try {
			Long taskId = HttpUtil.getAsLong(request, "taskId");
			request.setAttribute("taskId", taskId);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("协办客户经理签到异常！", e);
		}
		return "views/survey/common/xb_cusMge_signIn";
	}

	/**
	 * @Description 提交协办客户经理签到信息
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("submitXbCusMgeSignIn.json")
	@ResponseBody
	public Map<String, Object> submitXbCusMgeSignIn(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			Long taskId = HttpUtil.getAsLong(request, "taskId");
			String userId = request.getParameter("userName");
			String password = request.getParameter("password");
			Task task = taskSV.getTaskById(taskId);
			if (!StringUtil.isEmptyString(task.getHelpUserNo())) {
				ExceptionUtil.throwException("协办客户经理：" + task.getHelpUserName() + "已经签到，请勿重复签到！");
			}
			if (userId.equals(task.getUserNo())) {
				ExceptionUtil.throwException("协办客户经理与主办客户经理不能是同一人！");
			}
			User user = userSV.queryById(userId);
			if (user == null) {
				ExceptionUtil.throwException("用户不存在！");
			}
			if (!EncryptUtil.md5(password).equals(user.getPassword())) {
				ExceptionUtil.throwException("密码不正确！");
			}

			User mainCusMgr = userSV.queryById(task.getUserNo());
			if (!mainCusMgr.getAccOrgNo().equals(user.getAccOrgNo())) {
				ExceptionUtil.throwException("协办客户经理与主办客户经理不在同一机构！");
			}
			task.setHelpUserNo(userId);
			task.setHelpUserName(user.getUserName());
			taskSV.saveOrUpdate(task);
			json.put("success", true);
		} catch (BusinessException b) {
			json.put("msg", b.getMessage());
		} catch (Exception e) {
			json.put("msg", "系统异常请联系管理员！");
			e.printStackTrace();
			log.error("协办客户经理签到异常！", e);
		}
		return json;
	}

	/**
	 * 查询申请记录
	 * @param request
	 * @param response
	 * @param order
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("getApplyRecord.do")
	@ResponseBody
	public ResultDto getApplyRecord(HttpServletRequest request,String order,
			String stage, String custName, @RequestParam(value = "page", defaultValue = "1") int page,
			@RequestParam(value = "rows", defaultValue = "10") int rows) throws Exception {
		ResultDto re = returnFail("");
		User user = HttpUtil.getCurrentUser(request);
		String userNo = null;
		String orgNo = null;
		try {
			String userDA = user.getDataAccess();
			if (SysConstants.UserDataAccess.PERSONAL.equals(userDA)) {
				userNo = user.getUserId();
			} else if (SysConstants.UserDataAccess.ORG.equals(userDA)) {
				orgNo = user.getAccOrgNo();
			}
			//贷前调查，调查中任务，显示的数据列表
			Pagination<Task> pager = lmProcessService.queryLbTaskPager(userNo, orgNo, custName, stage, page, rows);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setRows(pager.getItems());
			re.setTotal(pager.getRowsCount());

		} catch (Exception e) {
			e.printStackTrace();
			log.error(" 获取申请记录列表异常", e);
		}
		return re;
	}

	/**
	 * 导入excel文件
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("uploadExceluploadExcel.json")
	@ResponseBody
	public String uploadExcel(HttpServletRequest request, HttpServletResponse response) {
		
		InputStream is = null;
		OutputStream os = null;
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);
		try {
			// 获取taskId
			String taskId = HttpUtil.getAsString(request, "taskId");
			List<MultipartFile> files = ((MultipartRequest) request).getFiles("file");
			if (CollectionUtils.isEmpty(files))
				ExceptionUtil.throwException("请选择文件！");
			// 获取上传的文件
			MultipartFile mf = files.get(0);
		} catch (BusinessException be) {
			log.error("报表导入异常：" + be.getMessage());
			json.put("msg", be.getMessage());
		} catch (Exception e) {
			log.error("导入文件异常：", e);
			json.put("msg", "系统异常，导入失败！");
		} finally {
			try {
				if (is != null)
					is.close();
				if (os != null)
					os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return jRequest.serialize(json, true);
	}

	
	
	/**
	 * 同步xxxxxxxx数据(一般数据丢失的时候，单独使用)
	 * @param request
	 * @param response
	 * @param order
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("test.do")
	@ResponseBody
	public ResultDto test(HttpServletRequest request) throws Exception {
		ResultDto re = returnFail("");

		try {
			List<Task> tasks = taskSV.getAllTasks();
			if(!CollectionUtils.isEmpty(tasks)){
				int index = 0;
				for(Task task:tasks){
					System.out.println(task.getCustomerName() + "=============" + (++index) + "/" +tasks.size());
					CusBase cusBase = cusBaseSV.queryByCusNo(task.getCustomerNo());
					dtmpService.syncCompConnetCm(cusBase.getXdCustomerNo(), task.getCustomerNo());
					System.out.println("=============" + (index) + "/" +tasks.size());
				}
			}
			
			/*financingSV.syncCompFinaBankFromCmis(97L, "CM20201016913029");*/
		} catch (Exception e) {
			e.printStackTrace();
			log.error(" 获取申请记录列表异常", e);
		}
		return re;
	}
	
}
