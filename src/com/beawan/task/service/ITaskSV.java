package com.beawan.task.service;

import java.util.List;
import java.util.Map;

import com.beawan.base.entity.User;
import com.beawan.core.BaseService;
import com.beawan.task.bean.Task;
import com.beawan.task.dto.DGTaskDto;
import com.beawan.task.dto.TaskStatDto;



/**
 * @ClassName ITaskSV
 * @Description (用户及贷前任务逻辑层)
 * @author czc
 * @Date 2017年5月12日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface ITaskSV {

	/**
	 * @Description (通过id查找任务)
	 * @param id
	 * @return
	 */
	public Task getTaskById(Long id);

	/**
	 * @Description (通过任务流水号查找任务)
	 * @param serNo
	 * @return
	 */
	public Task getTaskBySerNo(String serNo);
	
	/**
	 * @Description (通过客户号查找任务)
	 * @return
	 */
	public List<Task> getTaskByCustmerNo(String customerNo) throws Exception;
	
	/**
	 * TODO 获得最近一次申请
	 * @param customerNo
	 * @return
	 * @throws Exception
	 */
	public Task getLastTask(String customerNo) throws Exception;

	/**
	 * @Description (保存任务信息)
	 * @param task
	 * @param finaAnalyFlag  客户财务信息分析标识，null：不分析，1：分析并且分析前进行校验，其他：分析但不校验
	 */
	public Task saveTaskInfo(Task task, String finaAnalyFlag) throws Exception;
	
	/**
	 * TODO 更新，仅对Task实体，不做其他操作
	 * @param task
	 * @throws Exception
	 */
	public Task saveOrUpdate(Task task) throws Exception;

	/**
	 * @Description (删除任务及关联数据)
	 * @param task
	 */
	public void deleteTask(Task task) throws Exception;
	
	/**
	 * TODO 删除任务及关联数据
	 * @param taskId 任务号
	 * @param operator 操作人
	 * @throws Exception
	 */
	public void deleteTask(long taskId, User operator) throws Exception;

	/**
	 * @Description (无条件找出所有任务)
	 * @return
	 */
	public List<Task> getAllTasks();
	
	/**
	 * @Description (通过条件找出所有任务)
	 * @param query 查询语句
	 * @param args  条件
	 * @return
	 */
	public List<Task> getAllTasksWithCondition(String query, Object... args);

	/**
	 * @Description 分页查询
	 * @param query
	 * @param pageIndex
	 * @param pageSize
	 * @param args
	 * @return
	 */
	public Map<String, Object> getTasks(String query, int pageIndex, int pageSize, Object... args);

	/**
	 * @Description 分页查询
	 * @param pageIndex
	 * @param pageSize
	 * @return
	 */
	public Map<String, Object> getTaskPaging(Task queryCondition, String orderCondition,
			int pageIndex, int pageSize)throws Exception;
	
	
	/**
	 * @Description (复用上一次授信调查时录入的信息并保存)
	 * @param task
	 * @return
	 */
	public void reuseLastInfo(Task task, Task lastTask) throws Exception;
	
	/**
	 * TODO 提交调查任务
	 * @param taskId 任务号
	 * @param userNo 当前操作人no
	 * @throws Exception
	 */
	public void submitSurveyTask(long taskId, String userNo) throws Exception;
	
	/**
	 * 按贷款类型统计
	 * @param year
	 * @param loanTerm
	 * @return
	 */
	public List<TaskStatDto> getTaskStatByBusinessType(String applicationTime,String loanTerm);
	
	/**
	 * 按贷款期限统计
	 * @param year
	 * @param businessType
	 * @return
	 */
	public List<TaskStatDto> getTaskStatByLoanTerm(String applicationTime,String businessNo);

	/**
	 * 按年份统计
	 * @param businessType
	 * @return
	 */
	public List<TaskStatDto> getTaskStatByYear(String businessNo);
	/**
	 * 新建贷前调查，并同步天眼查数据
	 * @param taskDto 客户端传过来的新建贷款的信息
	 * @throws Exception
	 */
	public void createTaskInfoAndSyncTyc(DGTaskDto taskDto,User user)throws Exception;

	/**
	 * 贷前获取客户在我行融资情况
	 * @param parseInt
	 */
	public void insertCreditSelfBankInfo(Long taskId) throws Exception;
}