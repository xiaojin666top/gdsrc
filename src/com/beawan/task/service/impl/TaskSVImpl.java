package com.beawan.task.service.impl;

import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.beawan.corporateloan.service.LmProcessService;
import com.beawan.survey.custInfo.dto.CompBaseDto;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.beawan.analysis.abnormal.bean.ErrData;
import com.beawan.analysis.abnormal.dao.IErrDataDao;
import com.beawan.analysis.abnormal.service.IAbnormalService;
import com.beawan.analysis.finansis.bean.ReportData;
import com.beawan.analysis.finansis.bean.ReportDataTR;
import com.beawan.analysis.finansis.dao.IReportDataDao;
import com.beawan.analysis.finansis.dao.IReportDataTRDao;
import com.beawan.analysis.finansis.service.IFinanasisService;
import com.beawan.analysis.finansis.service.IFnTableService;
import com.beawan.analysis.salesIncome.dao.ISaleBankStateDAO;
import com.beawan.analysis.salesIncome.service.ISaleIncomeSV;
import com.beawan.base.entity.User;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.util.SystemUtil;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.dao.ICusBaseDAO;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.dtmp.dao.LnlnslnsDao;
import com.beawan.dtmp.dto.CustLoanDto;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.beawan.library.service.IIndustrySV;
import com.beawan.library.service.ITempletSV;
import com.beawan.loanAfter.entity.LfCreditSelfBank;
import com.beawan.loanAfter.entity.LfTask;
import com.beawan.survey.custInfo.bean.CompFinance;
import com.beawan.survey.custInfo.bean.CompFinanceOperCF;
import com.beawan.survey.custInfo.bean.CompFinanceTax;
import com.beawan.survey.custInfo.bean.CompFinancing;
import com.beawan.survey.custInfo.bean.CompFinancingBank;
import com.beawan.survey.custInfo.bean.CompFinancingExtGuara;
import com.beawan.survey.custInfo.bean.CompRun;
import com.beawan.survey.custInfo.bean.CompRunConst;
import com.beawan.survey.custInfo.bean.CompRunConstSPDetail;
import com.beawan.survey.custInfo.bean.CompRunConstSPStruc;
import com.beawan.survey.custInfo.bean.CompRunCustomer;
import com.beawan.survey.custInfo.bean.CompRunEsta;
import com.beawan.survey.custInfo.bean.CompRunEstaDevedProj;
import com.beawan.survey.custInfo.bean.CompRunEstaDevingProj;
import com.beawan.survey.custInfo.bean.CompRunEstaLand;
import com.beawan.survey.custInfo.bean.CompRunFixed;
import com.beawan.survey.custInfo.bean.CompRunPower;
import com.beawan.survey.custInfo.bean.CompRunPrdSale;
import com.beawan.survey.custInfo.bean.CompRunProduce;
import com.beawan.survey.custInfo.bean.CompRunSupply;
import com.beawan.survey.custInfo.dao.ICompRunDAO;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.beawan.survey.custInfo.service.ICompFinanceSV;
import com.beawan.survey.custInfo.service.ICompFinancingSV;
import com.beawan.survey.custInfo.service.ICompRunSV;
import com.beawan.survey.loanInfo.bean.ApplyInfo;
import com.beawan.survey.loanInfo.bean.ApplyRate;
import com.beawan.survey.loanInfo.bean.ApplyScheme;
import com.beawan.survey.loanInfo.bean.Conclusion;
import com.beawan.survey.loanInfo.bean.EstaProjBase;
import com.beawan.survey.loanInfo.bean.FixedProjBase;
import com.beawan.survey.loanInfo.bean.FixedProjCashFow;
import com.beawan.survey.loanInfo.bean.FixedProjFinaPlan;
import com.beawan.survey.loanInfo.bean.FixedProjInvest;
import com.beawan.survey.loanInfo.bean.FixedProjInvestEst;
import com.beawan.survey.loanInfo.bean.FixedProjPlan;
import com.beawan.survey.loanInfo.bean.GuaranteeInfo;
import com.beawan.survey.loanInfo.bean.MortgageInfo;
import com.beawan.survey.loanInfo.bean.PledgeInfo;
import com.beawan.survey.loanInfo.bean.RiskAnalysis;
import com.beawan.survey.loanInfo.dao.IApplyInfoDAO;
import com.beawan.survey.loanInfo.dao.IApplyRateDAO;
import com.beawan.survey.loanInfo.dao.IApplySchemeDAO;
import com.beawan.survey.loanInfo.dao.IConclusionDAO;
import com.beawan.survey.loanInfo.dao.IEstaProjBaseDAO;
import com.beawan.survey.loanInfo.dao.IFixedInvestDAO;
import com.beawan.survey.loanInfo.dao.IFixedInvestEstDAO;
import com.beawan.survey.loanInfo.dao.IFixedProjBaseDAO;
import com.beawan.survey.loanInfo.dao.IFixedProjCashFlowDAO;
import com.beawan.survey.loanInfo.dao.IFixedProjFinaPlanDAO;
import com.beawan.survey.loanInfo.dao.IFixedProjPlanDAO;
import com.beawan.survey.loanInfo.dao.IGuaranteeInfoDAO;
import com.beawan.survey.loanInfo.dao.IMortgageInfoDAO;
import com.beawan.survey.loanInfo.dao.IPledgeInfoDAO;
import com.beawan.survey.loanInfo.dao.IRiskAnalysisDAO;
import com.beawan.survey.loanInfo.service.IApplyInfoSV;
import com.beawan.survey.loanInfo.service.IConclusionSV;
import com.beawan.survey.loanInfo.service.IEstaProjSV;
import com.beawan.survey.loanInfo.service.IFixedProjSV;
import com.beawan.survey.loanInfo.service.IGuaraInfoSV;
import com.beawan.survey.loanInfo.service.ISurveyReportSV;
import com.beawan.task.bean.Task;
import com.beawan.task.dao.ITaskDAO;
import com.beawan.task.dto.DGTaskDto;
import com.beawan.task.dto.TaskStatDto;
import com.beawan.task.service.ITaskSV;
import com.platform.util.DateUtil;
import com.platform.util.ExceptionUtil;
import com.platform.util.StringUtil;

@Service("taskSV")
public class TaskSVImpl implements ITaskSV {

	@Resource
	protected ITaskDAO taskDAO;

	@Resource
	protected IReportDataDao reportDataDao;

	@Resource
	protected IReportDataTRDao reportDataTRDao;

	@Resource
	protected IErrDataDao errDataDao;

	@Resource
	protected ISaleBankStateDAO saleIncomeDao;

	@Resource
	protected ICompRunDAO compRunDAO;
	
	@Resource
	protected ICompBaseSV compBaseSV;
	
	@Resource
	protected ICmisInInvokeSV cmisInInvokeSV;
	
	@Resource
	protected IIndustrySV industrySV;
	
	@Resource
	protected IFinanasisService finanasisSV;
	
	@Resource
	protected ITempletSV templetSV;
	
	@Resource
	protected IAbnormalService abnormalSV;
	
	@Resource
	protected IApplyInfoSV applyInfoSV;

	@Resource
	private IApplyInfoDAO applyInfoDao;

	@Resource
	private IApplySchemeDAO applySchemaDao;
	
	@Resource
	private IApplyRateDAO applyRateDao;
	
	@Resource
	private ICompFinancingSV compFinancingSV;
	
	@Resource
	private ICompFinanceSV compFinanceSV;
	@Resource
	private  IConclusionSV conclusionSV;
	
	@Resource
	private IConclusionDAO  concluDao;
	
	@Resource
	private IRiskAnalysisDAO riskDao;
	
	@Resource
	private IGuaraInfoSV guaraInfoSV;
	
	@Resource
	private IGuaranteeInfoDAO  guaranteeInfoDAO;
	
	@Resource
	private IMortgageInfoDAO mortgageInfoDAO;
	
	@Resource
	private IPledgeInfoDAO pledgeInfoDAO;
	
	@Resource
	private ICompRunSV compRunSV;
	
	@Resource
	private IFixedProjSV fixedProjSV;
	
	@Resource
	private IFixedProjBaseDAO fixedProjBaseDao;
	
	@Resource
	private IFixedProjCashFlowDAO fixedcashFlowDao;
	
	@Resource
	private IFixedProjPlanDAO fixedplanDao;
	
	@Resource 
	private IFixedInvestDAO fixedinvestDao;
	
	@Resource
	private IFixedInvestEstDAO fixedestDao;
	
	@Resource
	private IFixedProjFinaPlanDAO fixedfinaPlanDao;
	
	@Resource
	private IEstaProjSV eEstaProjSV;
	
	@Resource
	private IEstaProjBaseDAO estaProjBaseDao;
	
	@Resource
	private IFnTableService fnTableSV;

	@Resource
	private ISaleIncomeSV saleIncomeSV;
	
	@Resource
	private ISurveyReportSV surveyReportSV;
	@Resource
	private ICusBaseSV cusBaseSV;
	@Resource
	private LmProcessService lmProcessService;

    @Resource
    private ICusBaseDAO cusBaseDAO;
    @Resource
    private LnlnslnsDao lnlnslnsDao;
	
	@Override
	public Task getTaskById(Long id) {
		return taskDAO.getTaskById(id);
	}

	@Override
	public Task getTaskBySerNo(String serNo) {
		return taskDAO.getTaskBySerNo(serNo);
	}

	@Override
	public Task saveOrUpdate(Task task) throws Exception{
		return taskDAO.saveTask(task);
	}

	@Override
	public List<Task> getTaskByCustmerNo(String custmerNo) {
		return this.taskDAO.getTaskByCustomerNo(custmerNo);
	}
	
	@Override
	public Task getLastTask(String customerNo) throws Exception {
		List<Task> tasks = taskDAO.getTaskByCustomerNo(customerNo);
		if(!CollectionUtils.isEmpty(tasks))
			return tasks.get(0);
		else
			return null;
	}

	@Override
	public Task saveTaskInfo(Task task, String finaAnalyFlag) throws Exception {
		Task entity;
		// 业务流水号为空，说明为新增保存
		if(task.getId()!=null && task.getId()!=0) {
			entity = taskDAO.getTaskById(task.getId());
		}else{
			entity = new Task();
			entity.setCreater(task.getUpdater());;
			entity.setCreateTime(DateUtil.getNowTimestamp());
			entity.setSerNo(SystemUtil.generateSerialNo());
			entity.setCustomerNo(task.getCustomerNo());
			entity.setCustomerName(task.getCustomerName());
			entity.setInstitutionNo(task.getInstitutionNo());
			entity.setInstitutionName(task.getInstitutionName());
			entity.setUserNo(task.getUserNo());
			entity.setUserName(task.getUserName());
		}
		entity.setYear(task.getYear());
		entity.setMonth(task.getMonth());
		entity.setBusinessNo(task.getBusinessNo());
		entity.setBusinessType(task.getBusinessType());
		entity.setProductId(task.getProductId());
		entity.setProductName(task.getProductName());
		entity.setAssureMeans(task.getAssureMeans());
		entity.setLoanNumber(task.getLoanNumber());
		entity.setLoanTerm(task.getLoanTerm());
		entity.setIndustry(task.getIndustry());
		entity.setFinaSyncFlag(task.getFinaSyncFlag());
		entity.setUpdater(task.getUpdater());
		entity.setUpdateTime(DateUtil.getNowTimestamp());
		entity.setApplicationTime(DateUtil.getServerTime());
		
		/*//从信贷系统同步客户信息
		cmisInInvokeSV.syncCusInfo(task.getCustomerNo());
		compBaseSV.syncCustomerInfo(task.getCustomerNo());
		//复用上一次录入的信息
		Task lastTask = this.getLastTask(task.getCustomerNo());
		this.reuseLastInfo(entity, lastTask);
		entity.setReuseLast("1");
		if(!SysConstants.LoanTypeConstant.LOAN_PERSONAL.equals(task.getBusinessNo())
				&& !StringUtil.isEmpty(finaAnalyFlag))
			finanasisSV.saveFinaDataAndAnalyze(task, finaAnalyFlag);*/
		return taskDAO.saveTask(entity);
	}
	
	public void deleteTask(long taskId, User operator) throws Exception{
		Task task = this.getTaskById(taskId);
		if(operator.getRoleNo().contains(SysConstants.UserRole.SUPER_ADMIN)
				|| operator.getRoleNo().contains(SysConstants.UserRole.SYSTEM_ADMIN)){
			this.deleteTask(task);
		}else{
			//非管理员仅允许删除自己名下的业务，并且提交过的业务不允许删除
			if(task.getUserNo().equals(operator.getUserId())
					&& task.getSubmitTime() == null)
				this.deleteTask(task);
			else
				ExceptionUtil.throwException("对不起，您没有权限删除该笔业务！");
		}
	}

	@Override
	public void deleteTask(Task task) throws Exception {
		
		if (task == null)
			return;
		
		String customerNo = "";
		
		if(task.getBusinessNo().equals(SysConstants.LoanTypeConstant.LOAN_PERSONAL))
			customerNo=task.getCmBaseId();
		else 
			customerNo=task.getCustomerNo();

		//imageSV.deleteImage(task.getSerNo());
		
		ReportData balance = reportDataDao.getReportDataByTaskIdCustNoAndType(task.getId(), task.getCustomerNo(),
				SysConstants.ReportType.BALANCE);
		ReportData newBalance = reportDataDao.getReportDataByTaskIdCustNoAndType(task.getId(), task.getCustomerNo(),
				SysConstants.ReportType.NEW_BALANCE);
		ReportData income = reportDataDao.getReportDataByTaskIdCustNoAndType(task.getId(), task.getCustomerNo(),
				SysConstants.ReportType.INCOME);
		ReportData newIncome = reportDataDao.getReportDataByTaskIdCustNoAndType(task.getId(), task.getCustomerNo(),
				SysConstants.ReportType.NEW_INCOME);
		ReportData cashFlow = reportDataDao.getReportDataByTaskIdCustNoAndType(task.getId(), task.getCustomerNo(),
				SysConstants.ReportType.CASH_FLOW);
		ReportData newCashFlow = reportDataDao.getReportDataByTaskIdCustNoAndType(task.getId(), task.getCustomerNo(),
				SysConstants.ReportType.NEW_CASH_FLOW);
		if (balance != null) {
			List<ReportDataTR> trs = reportDataTRDao.getReportDataTrsByReportDataId(balance.getId());
			if (trs != null) {
				for (ReportDataTR reportDataTR : trs) {
					reportDataTRDao.deleteReportDataTr(reportDataTR);
				}
			}
			reportDataDao.daleteReportData(balance);
		}
		if (newBalance != null) {
			List<ReportDataTR> trs = reportDataTRDao.getReportDataTrsByReportDataId(newBalance.getId());
			if (trs != null) {
				for (ReportDataTR reportDataTR : trs) {
					reportDataTRDao.deleteReportDataTr(reportDataTR);
				}
			}
			reportDataDao.daleteReportData(newBalance);
		}
		if (income != null) {
			List<ReportDataTR> trs = reportDataTRDao.getReportDataTrsByReportDataId(income.getId());
			if (trs != null) {
				for (ReportDataTR reportDataTR : trs) {
					reportDataTRDao.deleteReportDataTr(reportDataTR);
				}
			}
			reportDataDao.daleteReportData(income);
		}
		if (newIncome != null) {
			List<ReportDataTR> trs = reportDataTRDao.getReportDataTrsByReportDataId(newIncome.getId());
			if (trs != null) {
				for (ReportDataTR reportDataTR : trs) {
					reportDataTRDao.deleteReportDataTr(reportDataTR);
				}
			}
			reportDataDao.daleteReportData(newIncome);
		}
		if (cashFlow != null) {
			List<ReportDataTR> trs = reportDataTRDao.getReportDataTrsByReportDataId(cashFlow.getId());
			if (trs != null) {
				for (ReportDataTR reportDataTR : trs) {
					reportDataTRDao.deleteReportDataTr(reportDataTR);
				}
			}
			reportDataDao.daleteReportData(cashFlow);
		}
		if (newCashFlow != null) {
			List<ReportDataTR> trs = reportDataTRDao.getReportDataTrsByReportDataId(newCashFlow.getId());
			if (trs != null) {
				for (ReportDataTR reportDataTR : trs) {
					reportDataTRDao.deleteReportDataTr(reportDataTR);
				}
			}
			reportDataDao.daleteReportData(newCashFlow);
		}
		List<ErrData> errDatas = errDataDao.findErrdataByTaskId(task.getId());
		if (errDatas != null) {
			for (ErrData errData : errDatas) {
				errDataDao.deleteErrData(errData);
				abnormalSV.deleteAll(errData.getId());
			}
		}

		ApplyInfo applyInfo=applyInfoSV.findApplyInfoByTaskId(task.getId());
	
		if(applyInfo!=null) {
			applyInfoDao.deleteEntity(applyInfo);
		}
		
		List<ApplyScheme> applySchemes=applyInfoSV.findApplySchemeListByTaskId(task.getId());
		
		if(applySchemes!=null&&applySchemes.size()>0) {
			for(ApplyScheme data:applySchemes) {
				applySchemaDao.deleteEntity(data);
			}
		}
		
		List<ApplyRate> applyRates=applyInfoSV.queryApplyRateListByTaskId(task.getId());
		if(applyRates!=null&&applyRates.size()>0) {
			for(ApplyRate data:applyRates) {
				applyRateDao.deleteEntity(data);
			}
		}
		CompFinancing compfinancing = compFinancingSV.findCompFinancingByTIdAndNo(task.getId(), customerNo);
		if(compfinancing!=null) {
		compFinancingSV.deleteCompFinancing(compfinancing);
		}
		List<CompFinancingBank> compFinancingBanks=compFinancingSV.findCompFinaBankByTIdAndNo(task.getId(), customerNo);
		
		if(compFinancingBanks!=null&&compFinancingBanks.size()>0) {
			for(CompFinancingBank data:compFinancingBanks) {
				compFinancingSV.deleteCompFinaBank(data);
			}
		}
		
		List<CompFinancingExtGuara> compFinancingExtGuaras=compFinancingSV.findCompFinaExtGuaraByTIdAndNo(task.getId(), customerNo);
		
		if(compFinancingExtGuaras!=null&&compFinancingExtGuaras.size()>0) {
			
			for(CompFinancingExtGuara data:compFinancingExtGuaras) {
				compFinancingSV.deleteCompFinaExtGuara(data);
			}
		}
		
		
		RiskAnalysis riskAnalysis=conclusionSV.findRiskAnalysisByTaskId(task.getId());
		if(riskAnalysis!=null) {
			riskDao.deleteEntity(riskAnalysis);
		}
		
		
		Conclusion conclusion=conclusionSV.updateAndFindConclusionByTaskId(task.getId());
		if(conclusion!=null) {
			concluDao.deleteEntity(conclusion);
		}
		
		
		
		GuaranteeInfo guaranteeInfo=guaraInfoSV.findGuaraInfoBytaskId(task.getId());
		
		if (guaranteeInfo!=null) {
			guaranteeInfoDAO.deleteEntity(guaranteeInfo);
		}
		
		guaraInfoSV.deleteGuarantorByTaskId(task.getId());
		
		List<MortgageInfo> mortgagelist=mortgageInfoDAO.queryMortgageInfoListByTaskId(task.getId());
		if(mortgagelist!=null) {
		for(MortgageInfo data:mortgagelist) {
			mortgageInfoDAO.deleteEntity(data);
		}
		}
		List<PledgeInfo> pledgelist=pledgeInfoDAO.queryPledgeInfoListByTaskId(task.getId());
		if(pledgelist!=null) {
		for(PledgeInfo data:pledgelist) {
			pledgeInfoDAO.deleteEntity(data);
		}
		}
		
		String loanType=task.getBusinessNo();
		
		if(loanType!=null) {
			if(loanType.equals(SysConstants.LoanTypeConstant.LOAN_LIQUIDITY_MANUFACT)) {
				CompRun compRun =compRunSV.queryCompRunByTIdAndNo(task.getId(), customerNo);
				if(compRun!=null) {
				compRunSV.deleteCompRun(compRun);
				}
				
				List<CompRunSupply> cSupplies=compRunSV.queryRunSupplyByTIdAndNo(task.getId(), customerNo);
				if(cSupplies!=null) {
				for(CompRunSupply supply:cSupplies) {
					compRunSV.deleteRunSupply(supply);
				}
				}
				
				List<CompRunCustomer> runCustomers=compRunSV.queryRunCustomerByTIdAndNo(task.getId(),customerNo);
				if(runCustomers!=null) {
				for(CompRunCustomer customer:runCustomers) {
					compRunSV.deleteRunCustomer(customer);
				}
				}
				
				List<CompRunPower> runPowers=compRunSV.queryRunPowerByTIdAndNo(task.getId(), customerNo);
				if(runPowers!=null) {
				for(CompRunPower power:runPowers) {
					compRunSV.deleteRunPower(power);
				}
				}
				
				List<CompRunPrdSale> prdSales=compRunSV.queryRunPrdSaleByTIdAndNo(task.getId(), customerNo);
				if(prdSales!=null) {
				for(CompRunPrdSale prdSale:prdSales) {
					compRunSV.deleteRunPrdSale(prdSale);
				}
				}
				CompRunProduce produceinfo=compRunSV.queryRunProduceByTIdAndNo(task.getId(), customerNo);
				if(produceinfo!=null) {
					compRunSV.deleteRunProduce(produceinfo);
				}
			}else if(loanType.equals(SysConstants.LoanTypeConstant.LOAN_LIQUIDITY_TEXTILE)){
				CompRun compRun =compRunSV.queryCompRunByTIdAndNo(task.getId(), customerNo);
				if(compRun!=null) {
				compRunSV.deleteCompRun(compRun);
				}
				
				List<CompRunSupply> cSupplies=compRunSV.queryRunSupplyByTIdAndNo(task.getId(), customerNo);
				if(cSupplies!=null) {
				for(CompRunSupply supply:cSupplies) {
					compRunSV.deleteRunSupply(supply);
				}
				}
				
				List<CompRunCustomer> runCustomers=compRunSV.queryRunCustomerByTIdAndNo(task.getId(),customerNo);
				if(runCustomers!=null) {
				for(CompRunCustomer customer:runCustomers) {
					compRunSV.deleteRunCustomer(customer);
				}
				}
				
				List<CompRunPower> runPowers=compRunSV.queryRunPowerByTIdAndNo(task.getId(), customerNo);
				if(runPowers!=null) {
				for(CompRunPower power:runPowers) {
					compRunSV.deleteRunPower(power);
				}
				}
				
				List<CompRunPrdSale> prdSales=compRunSV.queryRunPrdSaleByTIdAndNo(task.getId(), customerNo);
				if(prdSales!=null) {
				for(CompRunPrdSale prdSale:prdSales) {
					compRunSV.deleteRunPrdSale(prdSale);
				}
				}
				CompRunProduce produceinfo=compRunSV.queryRunProduceByTIdAndNo(task.getId(), customerNo);
				if(produceinfo!=null) {
					compRunSV.deleteRunProduce(produceinfo);
				}
				
			}else if(loanType.equals(SysConstants.LoanTypeConstant.LOAN_LIQUIDITY_AFP)){
				CompRun compRun =compRunSV.queryCompRunByTIdAndNo(task.getId(), customerNo);
				if(compRun!=null) {
				compRunSV.deleteCompRun(compRun);
				}
				
				List<CompRunSupply> cSupplies=compRunSV.queryRunSupplyByTIdAndNo(task.getId(), customerNo);
				if(cSupplies!=null) {
				for(CompRunSupply supply:cSupplies) {
					compRunSV.deleteRunSupply(supply);
				}
				}
				
				List<CompRunCustomer> runCustomers=compRunSV.queryRunCustomerByTIdAndNo(task.getId(),customerNo);
				if(runCustomers!=null) {
				for(CompRunCustomer customer:runCustomers) {
					compRunSV.deleteRunCustomer(customer);
				}
				}
				
				List<CompRunPower> runPowers=compRunSV.queryRunPowerByTIdAndNo(task.getId(), customerNo);
				if(runPowers!=null) {
				for(CompRunPower power:runPowers) {
					compRunSV.deleteRunPower(power);
				}
				}
				
				List<CompRunPrdSale> prdSales=compRunSV.queryRunPrdSaleByTIdAndNo(task.getId(), customerNo);
				if(prdSales!=null) {
				for(CompRunPrdSale prdSale:prdSales) {
					compRunSV.deleteRunPrdSale(prdSale);
				}
				}
				CompRunProduce produceinfo=compRunSV.queryRunProduceByTIdAndNo(task.getId(), customerNo);
				if(produceinfo!=null) {
					compRunSV.deleteRunProduce(produceinfo);
				}
			}else if(loanType.equals(SysConstants.LoanTypeConstant.LOAN_LIQUIDITY_WRT)){
				List<CompRunPrdSale> prdSales=compRunSV.queryRunPrdSaleByTIdAndNo(task.getId(), customerNo);
				if(prdSales!=null) {
				for(CompRunPrdSale prdSale:prdSales) {
					compRunSV.deleteRunPrdSale(prdSale);
				}
				}
				CompRun compRun =compRunSV.queryCompRunByTIdAndNo(task.getId(), customerNo);
				if(compRun!=null) {
				compRunSV.deleteCompRun(compRun);
				}
				List<CompRunSupply> cSupplies=compRunSV.queryRunSupplyByTIdAndNo(task.getId(), customerNo);
				if(cSupplies!=null) {
				for(CompRunSupply supply:cSupplies) {
					compRunSV.deleteRunSupply(supply);
				}
				}
				
				List<CompRunCustomer> runCustomers=compRunSV.queryRunCustomerByTIdAndNo(task.getId(),customerNo);
				if(runCustomers!=null) {
				for(CompRunCustomer customer:runCustomers) {
					compRunSV.deleteRunCustomer(customer);
				}
				}
				
			}else if(loanType.equals(SysConstants.LoanTypeConstant.LOAN_LIQUIDITY_CONST)){
				CompRunConst compRunConst=compRunSV.queryRunConstByTIdAndNo(task.getId(), customerNo);
				if(compRunConst!=null) {
					compRunSV.deleteRunConst(compRunConst);
				}
				List<CompRunConstSPDetail> spDetails=compRunSV.queryRunConstSPDetailByTIdAndNo(task.getId(), customerNo);
				if(spDetails!=null) {
				for(CompRunConstSPDetail spDetail:spDetails) {
					compRunSV.deleteRunConstSPDetail(spDetail);
				}
				}
				List<CompRunConstSPStruc> spStrucs=compRunSV.queryRunConstSPStrucByTIdAndNo(task.getId(), customerNo);
				if(spStrucs!=null) {
				for(CompRunConstSPStruc spStruc:spStrucs) {
					compRunSV.deleteRunConstSPStruc(spStruc);
				}
				}
			}else if(loanType.equals(SysConstants.LoanTypeConstant.LOAN_FIXED_ASSETS)){
				
				CompRunFixed runFixed=compRunSV.queryRunFixedByTIdAndNo(task.getId(), customerNo);
				if(runFixed!=null) {
					compRunSV.deleteRunFixed(runFixed);
				}
				List<CompRunPrdSale> prdSales=compRunSV.queryRunPrdSaleByTIdAndNo(task.getId(), customerNo);
				if(prdSales!=null) {
				for(CompRunPrdSale prdSale:prdSales) {
					compRunSV.deleteRunPrdSale(prdSale);
				}
				}
				
				FixedProjBase fixedProjBase=fixedProjSV.queryProjBaseByTaskId(task.getId());
				if(fixedProjBase!=null) {
					fixedProjBaseDao.deleteEntity(fixedProjBase);
				}
				
				List<FixedProjCashFow> fixedProjCashFows=fixedProjSV.queryProjCashFowByTaskId(task.getId());
				if(fixedProjCashFows!=null) {
				for(FixedProjCashFow cashFow:fixedProjCashFows) {
					fixedcashFlowDao.deleteEntity(cashFow);
				}
				}
				List<FixedProjFinaPlan> fixedProjFinaPlans=fixedProjSV.queryProjFinaPlanByTaskId(task.getId());
				if(fixedProjFinaPlans!=null) {
				for(FixedProjFinaPlan fProjFinaPlan:fixedProjFinaPlans) {
					fixedfinaPlanDao.deleteEntity(fProjFinaPlan);
				}
				}
				
				List<FixedProjInvest> fInvests=fixedProjSV.queryProjInvestByTaskId(task.getId());
				if(fInvests!=null) {
				for(FixedProjInvest fInvest:fInvests ) {
					fixedinvestDao.deleteEntity(fInvest);
				}
				}
				
				List<FixedProjInvestEst> fEsts=fixedProjSV.queryProjInvestEstByTaskId(task.getId());
				if(fEsts!=null) {
				for(FixedProjInvestEst fEst:fEsts) {
					fixedestDao.deleteEntity(fEst);
				}
				}
				List<FixedProjPlan>  fixedProjPlans=fixedProjSV.queryProjPlanByTaskId(task.getId());
				if(fixedProjPlans!=null) {
				for(FixedProjPlan fixedProjPlan:fixedProjPlans) {
					fixedplanDao.deleteEntity(fixedProjPlan);
				}
				}
				
			}else if(loanType.equals(SysConstants.LoanTypeConstant.LOAN_ESTATE)){
				
				EstaProjBase estaProjBase=eEstaProjSV.findProjBaseByTaskId(task.getId());
				if(estaProjBase!=null) {
					estaProjBaseDao.deleteEntity(estaProjBase);
				}
				CompRunEsta compRunEsta=compRunSV.queryRunEstaByTIdAndNo(task.getId(), customerNo);
				if(compRunEsta!=null) {
					compRunSV.deleteRunEsta(compRunEsta);
				}
				List<CompRunEstaDevedProj> devedProjs=compRunSV.queryRunEstaDevedProjByTIdAndNo(task.getId(), customerNo);
				if(devedProjs!=null) {
				for(CompRunEstaDevedProj devedProj:devedProjs) {
					compRunSV.deleteRunEstaDevedProj(devedProj);
				}
				}
				List<CompRunEstaDevingProj> devingProjs=compRunSV.queryRunEstaDevingProjByTIdAndNo(task.getId(), customerNo);
				if(devingProjs!=null) {
				for(CompRunEstaDevingProj devingProj:devingProjs) {
					compRunSV.deleteRunEstaDevingProj(devingProj);
				}
				}
				List<CompRunEstaLand> estaLands=compRunSV.queryRunEstaLandByTIdAndNo(task.getId(), customerNo);
				if(estaLands!=null) {
				for(CompRunEstaLand estaLand:estaLands) {
					compRunSV.deleteRunEstaLand(estaLand);
				}
				}
			}else if(loanType.equals(SysConstants.LoanTypeConstant.LOAN_PERSONAL)){
				CompRun compRun =compRunSV.queryCompRunByTIdAndNo(task.getId(), customerNo);
				if(compRun!=null) {
				compRunSV.deleteCompRun(compRun);
				}
				
				List<CompRunSupply> cSupplies=compRunSV.queryRunSupplyByTIdAndNo(task.getId(), customerNo);
				if(cSupplies!=null) {
				for(CompRunSupply supply:cSupplies) {
					compRunSV.deleteRunSupply(supply);
				}
				}
				
				List<CompRunCustomer> runCustomers=compRunSV.queryRunCustomerByTIdAndNo(task.getId(),customerNo);
				if(runCustomers!=null) {
				for(CompRunCustomer customer:runCustomers) {
					compRunSV.deleteRunCustomer(customer);
				}
				}
				
				List<CompRunPower> runPowers=compRunSV.queryRunPowerByTIdAndNo(task.getId(), customerNo);
				if(runPowers!=null) {
				for(CompRunPower power:runPowers) {
					compRunSV.deleteRunPower(power);
				}
				}
				
				List<CompRunPrdSale> prdSales=compRunSV.queryRunPrdSaleByTIdAndNo(task.getId(), customerNo);
				if(prdSales!=null) {
				for(CompRunPrdSale prdSale:prdSales) {
					compRunSV.deleteRunPrdSale(prdSale);
				}
				}
				CompRunProduce produceinfo=compRunSV.queryRunProduceByTIdAndNo(task.getId(), customerNo);
				if(produceinfo!=null) {
					compRunSV.deleteRunProduce(produceinfo);
				}
			}
		}
			
			fnTableSV.deleteAll(task.getId(), task.getCustomerNo());
			CompFinance compfinance=compFinanceSV.findCompFinanceByTIdAndNo(task.getId(), task.getCustomerNo());
			if(compfinance!=null) {
				compFinanceSV.deleteCompFinance(compfinance);
			}
			List<CompFinanceOperCF> operCFs=compFinanceSV.findFinanceOperCFByTIdAndNo(task.getId(), task.getCustomerNo());
			if(operCFs!=null){
			for(CompFinanceOperCF operCF:operCFs) {
				compFinanceSV.deleteFinanceOperCF(operCF);
			}
			}
			
			List<CompFinanceTax> compFinanceTaxs=compFinanceSV.findFinanceTaxByTIdAndNo(task.getId(), task.getCustomerNo());
			if(compFinanceTaxs!=null) {
			for(CompFinanceTax tax:compFinanceTaxs) {
				compFinanceSV.deleteFinanceTax(tax);
			}
			}
		
			saleIncomeSV.deleteAll(task.getId());
		
		this.taskDAO.deleteTask(task);
	}

	@Override
	public List<Task> getAllTasks() {
		return this.taskDAO.getAllTasks();
	}

	@Override
	public Map<String, Object> getTasks(String query, int pageIndex, int pageSize, Object... args) {
		return this.taskDAO.getTasks(query, pageIndex*pageSize, pageSize, args);
	}

	public List<Task> getAllTasksWithCondition(String query, Object... args) {
		return this.taskDAO.getAllTasksWithCondition(query, args);
	}

	@Override
	public Map<String, Object> getTaskPaging(Task queryCondition, String orderCondition,
			int pageIndex, int pageSize) throws Exception {
		
		StringBuffer listSql = new StringBuffer("1=1");
		StringBuffer totalSql = new StringBuffer("1=1");
		
		Map<String, Object> params = new HashMap<String, Object>();
		if (queryCondition != null) {
			
			if (!StringUtil.isEmptyString(queryCondition.getCustomerNo())) {
				listSql.append(" and customerNo=:customerNo");
				totalSql.append(" and customerNo=:customerNo");
				params.put("customerNo", queryCondition.getCustomerNo());
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getCustomerName())) {
				listSql.append(" and customerName like:customerName");
				totalSql.append(" and customerName like:customerName");
				String name = URLDecoder.decode(queryCondition.getCustomerName(), "UTF-8");
				params.put("customerName", "%" + name + "%");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getBusinessNo())) {
				listSql.append(" and businessNo=:businessNo");
				totalSql.append(" and businessNo=:businessNo");
				params.put("businessNo", queryCondition.getBusinessNo());
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getInstitutionNo())) {
				listSql.append(" and institutionNo=:institutionNo");
				totalSql.append(" and institutionNo =:institutionNo");
				params.put("institutionNo", queryCondition.getInstitutionNo());
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getInstitutionName())) {
				listSql.append(" and institutionName like:institutionName");
				totalSql.append(" and institutionName like:institutionName");
				String name = URLDecoder.decode(queryCondition.getInstitutionName(), "UTF-8");
				params.put("institutionName", "%" + name + "%");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getUserNo())) {
				listSql.append(" and userNo=:userNo");
				totalSql.append(" and userNo =:userNo");
				params.put("userNo", queryCondition.getUserNo());
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getUserName())) {
				listSql.append(" and userName like:userName");
				totalSql.append(" and userName like:userName");
				String name = URLDecoder.decode(queryCondition.getUserName(), "UTF-8");
				params.put("userName", "%" + name + "%");
			}
			
			if (queryCondition.getState() != null) {
				listSql.append(" and state=:state");
				totalSql.append(" and state =:state");
				params.put("state", queryCondition.getState());
			}
		}
		
		if (!StringUtil.isEmptyString(orderCondition))
			listSql.append(" order by ").append(orderCondition);
		else
			listSql.append(" order by applicationTime desc");
		
		Map<String, Object> dataMap = new HashMap<String, Object>();
		
		List<Task> list = taskDAO.selectRange(listSql.toString(),
				pageIndex*pageSize, pageSize, params);
		long total = taskDAO.selectCount(totalSql.toString(), params);
		dataMap.put("total", total);
		dataMap.put("rows", list);
		
		return dataMap;
	}

	@Override
	public void reuseLastInfo(Task task, Task lastTask) throws Exception {
		
		if(lastTask == null)
			return;
		
		Long newTaskId = task.getId();
		Long oldTaskId = lastTask.getId();
		String customerNo = task.getCustomerNo();
		String businessNo = task.getBusinessNo();
		
		if(SysConstants.LoanTypeConstant.LOAN_LIQUIDITY_MANUFACT.equals(businessNo)
				|| SysConstants.LoanTypeConstant.LOAN_LIQUIDITY_AFP.equals(businessNo)
				|| SysConstants.LoanTypeConstant.LOAN_LIQUIDITY_TEXTILE.equals(businessNo)
				|| SysConstants.LoanTypeConstant.LOAN_LIQUIDITY_WRT.equals(businessNo)) {
			
			CompRun compRun = compRunSV.queryCompRunByTIdAndNo(oldTaskId,
					customerNo);
			if(compRun != null) {
				CompRun cn = new CompRun();
				BeanUtils.copyProperties(cn,compRun);
				cn.setId(null);
				cn.setTaskId(newTaskId);
				compRunSV.saveCompRun(cn);
			}
			
			List<CompRunSupply> supplies=compRunSV.queryRunSupplyByTIdAndNo(oldTaskId,
					customerNo);
			if(supplies!=null) {
				for(CompRunSupply supply:supplies) {
					CompRunSupply nSupply=new CompRunSupply();
					BeanUtils.copyProperties(nSupply, supply);
					nSupply.setId(null);
					nSupply.setTaskId(newTaskId);
					compRunSV.saveRunSupply(nSupply);
				}
			}
			
			List<CompRunCustomer> customers=compRunSV.queryRunCustomerByTIdAndNo(oldTaskId,
					customerNo);
			if(customers!=null) {
				for(CompRunCustomer customer:customers) {
					CompRunCustomer nCustomer=new CompRunCustomer();
					BeanUtils.copyProperties(nCustomer, customer);
					nCustomer.setId(null);
					nCustomer.setTaskId(newTaskId);
					compRunSV.saveRunCustomer(nCustomer);
				}
			}
			
			//非批发零售业
			if(!SysConstants.LoanTypeConstant.LOAN_LIQUIDITY_WRT.equals(businessNo)){
				CompRunProduce compRunProduce=compRunSV.queryRunProduceByTIdAndNo(oldTaskId,
						customerNo);
				if(compRunProduce != null) {
				
					CompRunProduce cp=new CompRunProduce();
					BeanUtils.copyProperties(cp,compRunProduce);
					cp.setId(null);
					cp.setTaskId(newTaskId);
					compRunSV.saveRunProduce(cp);
				}
			}
			
		}else if(SysConstants.LoanTypeConstant.LOAN_LIQUIDITY_CONST.equals(businessNo)) {
			
			CompRunConst runConst=compRunSV.queryRunConstByTIdAndNo(oldTaskId,
					customerNo);
			if(runConst!=null) {
				CompRunConst nRunConst =new CompRunConst();
				BeanUtils.copyProperties(nRunConst, runConst);
				nRunConst.setId(null);
				nRunConst.setTaskId(newTaskId);
				compRunSV.saveRunConst(nRunConst);
			}
			
			List<CompRunConstSPDetail> spDetails=compRunSV.queryRunConstSPDetailByTIdAndNo(oldTaskId,
					customerNo);
			if(spDetails!=null) {
				for(CompRunConstSPDetail spDetail:spDetails) {
					CompRunConstSPDetail newsp=new CompRunConstSPDetail();
					BeanUtils.copyProperties(newsp, spDetail);
					newsp.setId(null);
					newsp.setTaskId(newTaskId);
					compRunSV.saveRunConstSPDetail(newsp);
				}
			}
			
			List<CompRunConstSPStruc> spStrucs=compRunSV.queryRunConstSPStrucByTIdAndNo(oldTaskId,
					customerNo);
			if(spStrucs!=null) {
				for(CompRunConstSPStruc spStruc:spStrucs) {
				CompRunConstSPStruc newstru=new CompRunConstSPStruc();
				BeanUtils.copyProperties(newstru, spStruc);
				newstru.setId(null);
				newstru.setTaskId(newTaskId);
				compRunSV.saveRunConstSPStruc(newstru);
				}
			}
			
		}else if(SysConstants.LoanTypeConstant.LOAN_FIXED_ASSETS.equals(businessNo)) {
				
			CompRunFixed cFixed=compRunSV.queryRunFixedByTIdAndNo(oldTaskId,
					customerNo);
			if(cFixed!=null) {
				CompRunFixed newrunfixed=new CompRunFixed();
				BeanUtils.copyProperties(newrunfixed, cFixed);
				newrunfixed.setId(null);
				newrunfixed.setTaskId(newTaskId);
				compRunSV.saveRunFixed(newrunfixed);
			}
			
		}else if(SysConstants.LoanTypeConstant.LOAN_ESTATE.equals(businessNo)) {
				
			CompRunEsta compRunEsta=compRunSV.queryRunEstaByTIdAndNo(oldTaskId,
					customerNo);
			if(compRunEsta!=null) {
				CompRunEsta nEsta=new CompRunEsta();
				BeanUtils.copyProperties(nEsta, compRunEsta);
				nEsta.setId(null);
				nEsta.setTaskId(newTaskId);
				compRunSV.saveRunEsta(nEsta);
			}
			
			List<CompRunEstaDevedProj> devedProjs=compRunSV.queryRunEstaDevedProjByTIdAndNo(oldTaskId,
					customerNo);
			if(devedProjs!=null) {
				for(CompRunEstaDevedProj devedProj:devedProjs) {
					CompRunEstaDevedProj newde=new CompRunEstaDevedProj();
					BeanUtils.copyProperties(newde, devedProj);
					newde.setId(null);
					newde.setTaskId(newTaskId);
					compRunSV.saveRunEstaDevedProj(newde);
				}
			}
		
			List<CompRunEstaDevingProj> devingProjs=compRunSV.queryRunEstaDevingProjByTIdAndNo(oldTaskId,
					customerNo);
			if(devingProjs!=null) {
				for(CompRunEstaDevingProj devingProj:devingProjs) {
					CompRunEstaDevingProj newdeing=new CompRunEstaDevingProj();
					BeanUtils.copyProperties(newdeing, devingProj);
					newdeing.setId(null);
					newdeing.setTaskId(newTaskId);
					compRunSV.saveRunEstaDevingProj(newdeing);
				}
				
			}
			
			List<CompRunEstaLand> lands=compRunSV.queryRunEstaLandByTIdAndNo(oldTaskId,
					customerNo);
			if(lands!=null) {
				for(CompRunEstaLand land:lands) {
					CompRunEstaLand newland=new CompRunEstaLand();
					BeanUtils.copyProperties(newland, land);
					newland.setId(null);
					newland.setTaskId(newTaskId);
					compRunSV.saveRunEstaLand(newland);
				}
			}
			
		}else if(SysConstants.LoanTypeConstant.LOAN_PERSONAL.equals(businessNo)) {
			
			String cmBaseId = lastTask.getCmBaseId();
			
			CompRun compRun = compRunSV.queryCompRunByTIdAndNo(oldTaskId, cmBaseId);
			if(compRun != null) {
				CompRun cn = new CompRun();
				BeanUtils.copyProperties(cn,compRun);
				cn.setId(null);
				cn.setTaskId(newTaskId);
				compRunSV.saveCompRun(cn);
			}
			
			List<CompRunSupply> supplies=compRunSV.queryRunSupplyByTIdAndNo(oldTaskId,
					cmBaseId);
			if(supplies!=null) {
				for(CompRunSupply supply:supplies) {
					CompRunSupply nSupply=new CompRunSupply();
					BeanUtils.copyProperties(nSupply, supply);
					nSupply.setId(null);
					nSupply.setTaskId(newTaskId);
					compRunSV.saveRunSupply(nSupply);
				}
			}
			
			List<CompRunCustomer> customers=compRunSV.queryRunCustomerByTIdAndNo(oldTaskId,
					cmBaseId);
			if(customers!=null) {
				for(CompRunCustomer customer:customers) {
					CompRunCustomer nCustomer=new CompRunCustomer();
					BeanUtils.copyProperties(nCustomer, customer);
					nCustomer.setId(null);
					nCustomer.setTaskId(newTaskId);
					compRunSV.saveRunCustomer(nCustomer);
				}
			}
			
			CompRunProduce compRunProduce=compRunSV.queryRunProduceByTIdAndNo(oldTaskId,
					cmBaseId);
			if(compRunProduce != null) {
			
				CompRunProduce cp=new CompRunProduce();
				BeanUtils.copyProperties(cp,compRunProduce);
				cp.setId(null);
				cp.setTaskId(newTaskId);
				compRunSV.saveRunProduce(cp);
			}
		}
		
		reuseLastCommon(newTaskId, oldTaskId, task.getCustomerNo());
	}
	
	private void reuseLastCommon(Long newTaskId, Long oldTaskId, String customerNo) throws Exception{
		
		CompFinance oldcompFinance=compFinanceSV.findCompFinanceByTIdAndNo(oldTaskId,
				customerNo);
		if(oldcompFinance!=null) {
			CompFinance newCompFinance=new CompFinance();
			newCompFinance.setTaskId(newTaskId);
			newCompFinance.setCustomerNo(customerNo);
			newCompFinance.setTaxAnaly(oldcompFinance.getTaxAnaly());
			newCompFinance.setExcRateRisk(oldcompFinance.getExcRateRisk());
			newCompFinance.setFinanceAnaly(oldcompFinance.getFinanceAnaly());
			compFinanceSV.saveCompFinance(newCompFinance);
		}
		
		CompFinancing compFinancing=compFinancingSV.findCompFinancingByTIdAndNo(oldTaskId,
				customerNo);
		if(compFinancing!=null) {
			CompFinancing newcompFinancing=new CompFinancing();
			BeanUtils.copyProperties(newcompFinancing, compFinancing);
			newcompFinancing.setId(null);
			newcompFinancing.setTaskId(newTaskId);
			compFinancingSV.saveCompFinancing(newcompFinancing);
		}
		
		List<CompFinancingBank> banks=compFinancingSV.findCompFinaBankByTIdAndNo(oldTaskId,
				customerNo);
		if(banks!=null) {
			for(CompFinancingBank bank:banks) {
				CompFinancingBank nBank=new CompFinancingBank();
				BeanUtils.copyProperties(nBank, bank);
				nBank.setId(null);
				nBank.setTaskId(newTaskId);
				compFinancingSV.saveCompFinaBank(nBank);
			}
		}
		
		List<CompFinancingExtGuara> extGuaras=compFinancingSV.findCompFinaExtGuaraByTIdAndNo(oldTaskId,
				customerNo);
		if(extGuaras!=null) {
			for(CompFinancingExtGuara extGuara:extGuaras) {
				CompFinancingExtGuara nExtGuara=new CompFinancingExtGuara();
				BeanUtils.copyProperties(nExtGuara, extGuara);
				nExtGuara.setId(null);
				nExtGuara.setTaskId(newTaskId);
				compFinancingSV.saveCompFinaExtGuara(nExtGuara);
			}
		}
		
		ApplyInfo applyInfo=applyInfoSV.findApplyInfoByTaskId(oldTaskId);
		if(applyInfo!=null) {
			ApplyInfo nApplyInfo=new ApplyInfo();
			BeanUtils.copyProperties(nApplyInfo, applyInfo);
			nApplyInfo.setTaskId(newTaskId);
			applyInfoSV.saveApplyInfo(nApplyInfo);
		}
		
		List<ApplyScheme> applySchemes=applyInfoSV.findApplySchemeListByTaskId(oldTaskId);
		if(applySchemes!=null) {
			for(ApplyScheme aScheme:applySchemes) {
				ApplyScheme nsApplyScheme=new ApplyScheme();
				BeanUtils.copyProperties(nsApplyScheme, aScheme);
				nsApplyScheme.setId(null);
				nsApplyScheme.setTaskId(newTaskId);
				applyInfoSV.saveApplyScheme(nsApplyScheme);
			}
		}
		
		RiskAnalysis riskAnalysis=conclusionSV.findRiskAnalysisByTaskId(oldTaskId);
		if(riskAnalysis!=null) {
			RiskAnalysis nRiskAnalysis=new RiskAnalysis();
			BeanUtils.copyProperties(nRiskAnalysis, riskAnalysis);
			Task task = taskDAO.getTaskById(newTaskId);
			nRiskAnalysis.setSerNo(task.getSerNo());
			conclusionSV.saveRiskAnalysis(nRiskAnalysis);
		}
		
		Conclusion conclusion=conclusionSV.updateAndFindConclusionByTaskId(oldTaskId);
		if(conclusion!=null) {
			Conclusion nconclusion=new Conclusion();
			BeanUtils.copyProperties(nconclusion, conclusion);
			nconclusion.setTaskId(newTaskId);
			conclusionSV.saveConclusion(nconclusion);
		}
		
	}
	
	@Override
	public void submitSurveyTask(long taskId, String userNo) throws Exception {
		
		Task task =  taskDAO.getTaskById(taskId);

		// 判断是否符合提交要求
//		if(SysConstants.CusFinaValidFlag.TEMP_UNAVAIL.equals(task.getFinaSyncFlag()))
//			ExceptionUtil.throwException("财务信息不全，提交失败！", "CUS_FS");
		
		if(StringUtil.isEmptyString(task.getIndustry()))
			ExceptionUtil.throwException("未填写客户所属行业，提交失败！");
		
		String customerNo="";
		if(task.getBusinessNo().equals(SysConstants.LoanTypeConstant.LOAN_PERSONAL))
			customerNo =task.getCmBaseId();
		else
			customerNo=task.getCustomerNo();

		CompBaseDto compBase=compBaseSV.findCompBaseByCustNo(customerNo);
		if(compBase==null)
			ExceptionUtil.throwException("未填写客户基本信息，提交失败！");
			
		if(StringUtil.isEmptyString(compBase.getIndustryCode()))
			ExceptionUtil.throwException("未填写客户所属行业，提交失败！");
	
//		List<Guarantor> guarantors=guaraInfoSV.findGuarantorByTaskId(task.getId(), SysConstants.GuarantorType.COMP_GUARA);
//		if(guarantors!=null) {
//			for(Guarantor guarantor:guarantors) {
//				CompBaseDto compBaseTemp = compBaseSV.findCompBaseByCustNo(guarantor.getCusNo());
//				if(compBaseTemp==null)
//					ExceptionUtil.throwException("未填写公司保证人基本信息，提交失败！");
//
//				if(StringUtil.isEmptyString(compBaseTemp.getIndustryCode()))
//					ExceptionUtil.throwException("未填写公司保证人所属行业，提交失败！");
//			}
//		}
		
		if (StringUtil.isEmptyString(task.getHelpUserNo()))
			ExceptionUtil.throwException("协办客户经理尚未签到，提交失败！");
			
		// 生成调查报告
//		surveyReportSV.createSurveyReport(taskId);
		// 更新状态
		task.setState(SysConstants.TaskState.SUBMITTED);
		// 设置提交时间
		task.setSubmitTime(DateUtil.formatDate(DateUtil.getSystemTime(), Constants.DATETIME_MASK));
		taskDAO.saveTask(task);

		//修改主流程中任务状态  有贷前的调查中的任务--》合规审查待处理任务
		lmProcessService.updateLbTask(task.getSerNo(), userNo);
		//跳过合规审查,到贷款审查中
		lmProcessService.submitToExamine(task.getSerNo(),userNo);

	}

	@Override
	public List<TaskStatDto> getTaskStatByBusinessType(String applicationTime, String loanTerm) {
		Map<String,Object> params = new HashMap<>();
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT lt.BUSINESS_TYPE businessType, count(1) loanCount, sum(lt.LOAN_NUMBER) loanSum")
				.append(" FROM LB_TASK lt")
				.append(" WHERE lt.STATUS = :status");
		params.put("status", Constants.NORMAL);
		if(!StringUtil.isEmptyString(applicationTime)) {
			sql.append(" AND YEAR(lt.APPLICATION_TIME) = :applicationTime");
			params.put("applicationTime", applicationTime);
		}
		if(!StringUtil.isEmptyString(loanTerm)) {
			if("1".equals(loanTerm)){// 贷款期限（月）在1到3年的
				sql.append(" AND lt.LOAN_TERM >= 1 AND lt.LOAN_TERM <= 36");
			}else if("2".equals(loanTerm)) {// 贷款期限（月）3年以上的
				sql.append(" AND lt.LOAN_TERM > 36");
			}
		}
		sql.append(" GROUP BY lt.BUSINESS_TYPE");
		sql.append(" ORDER BY loanSum DESC");
		return taskDAO.findCustListBySql(TaskStatDto.class, sql.toString(), params);
	}

	@Override
	public List<TaskStatDto> getTaskStatByLoanTerm(String applicationTime, String businessNo) {
		Map<String,Object> params = new HashMap<>();
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT lt.LOAN_TERM loanTerm, COUNT(1) loanCount, sum(LOAN_NUMBER) loanSum")
				.append(" FROM LB_TASK lt")
				.append(" WHERE lt.STATUS = :status");
		params.put("status", Constants.NORMAL);
		if(!StringUtil.isEmptyString(applicationTime)) {
			sql.append(" AND YEAR(lt.APPLICATION_TIME) = :applicationTime");
			params.put("applicationTime", applicationTime);
		}
		if(!StringUtil.isEmptyString(businessNo)) {
			sql.append(" AND lt.BUSINESS_NO = :businessNo ");
			params.put("businessNo", businessNo);
		}
		sql.append(" GROUP BY lt.LOAN_TERM");
		sql.append(" ORDER BY lt.LOAN_TERM ASC");
		return taskDAO.findCustListBySql(TaskStatDto.class, sql.toString(), params);
	}

	@Override
	public List<TaskStatDto> getTaskStatByYear(String businessNo) {
		Map<String,Object> params = new HashMap<>();
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT YEAR(lt.APPLICATION_TIME) applicationTime, COUNT(1) loanCount, SUM(lt.LOAN_NUMBER) loanSum")
				.append(" FROM LB_TASK lt")
				.append(" WHERE lt.STATUS = :status");
		params.put("status", Constants.NORMAL);
		if(!StringUtil.isEmptyString(businessNo)) {
			sql.append(" AND lt.BUSINESS_NO = :businessNo ");
			params.put("businessNo", businessNo);
		}
		sql.append(" GROUP BY YEAR(lt.APPLICATION_TIME)");
		sql.append(" ORDER BY YEAR(lt.APPLICATION_TIME) ASC");
		return taskDAO.findCustListBySql(TaskStatDto.class, sql.toString(), params);
	}

	@Override
	public void createTaskInfoAndSyncTyc(DGTaskDto taskDto,User user) throws Exception {
		//获取到taskDto中的数据信息
		String custName = taskDto.getCustomerName();
		String certCode = taskDto.getCreditCode();
		String assureMeans = taskDto.getAssureMeans();
		String businessNo = taskDto.getBusinessNo();
		String businessType = taskDto.getBusinessType();
		Double loanNumber = taskDto.getLoanNumber();
		Integer loanTerm = taskDto.getLoanTerm();
		String loanUse = taskDto.getLoanUse();
		String productId = taskDto.getProductId();
		String productName = taskDto.getProductName();
		String replyWay = taskDto.getRepayWay();
		String industry = taskDto.getIndustry();
		if(StringUtil.isEmptyString(custName)) {
			ExceptionUtil.throwException("企业名称为空,新增授信失败");
		}
		//判断当前客户是否为新增客户
		CusBase cusBase = cusBaseSV.queryByCusName(custName);
		if(cusBase==null) {//用户为空表示为新增客户
			cusBase = new CusBase(); 
			SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddHHmmss");
			cusBase.setCustomerName(custName);
			cusBase.setCertCode(certCode);
			String date = DateUtil.format(new Date(), Constants.DATE_MASK);
			cusBase.setInputDate(date);
			cusBase.setCustomerNo(sf.format(new Date()));
			cusBase.setMfCustomerNo(cusBase.getCustomerNo());
			cusBase.setInputUserId(user.getUserId());
			cusBase.setInputOrgId(user.getAccOrgNo());
			cusBase.setInputDate(date);
			cusBase.setManagerUserId(user.getUserId());
			cusBase.setManagerOrgId(user.getAccOrgNo());
			cusBase.setUpdateDate(date);
			cusBaseSV.save(cusBase);
		}
		//开始保存授信任务
		Task task = new Task();
		task.setSerNo(SystemUtil.generateSerialNo());
	    task.setApplicationTime(DateUtil.getServerTime());
		task.setLoanNumber(loanNumber*10000);
		task.setCustomerNo(cusBase.getCustomerNo());
		task.setCustomerName(custName);
		task.setAssureMeans(assureMeans);
		task.setLoanTerm(loanTerm);
		task.setBusinessNo(businessNo);
		task.setBusinessType(businessType);
		task.setLoanUse(loanUse);
		task.setProductId(Integer.parseInt(productId));
		task.setProductName(productName);
		task.setUserNo(user.getUserId());
		task.setUserName(user.getUserName());
		task.setReplyWay(replyWay);
		task.setIndustry(industry);
		task = saveOrUpdate(task);
		if(task.getId()==null || task.getId()==0l) {
			ExceptionUtil.throwException("保存未任务 未获得taskId值");
		}
	}
	
	public void insertCreditSelfBankInfo(Long taskId) throws Exception {
		//获取到贷后客户的核心号
		Task task = taskDAO.selectByPrimaryKey(taskId);
		CusBase cusBase = cusBaseDAO.selectSingleByProperty("customerNo", task.getCustomerNo());
		//获取用信情况
		List<CustLoanDto> custLoanDtos = lnlnslnsDao.getCustLoan(cusBase.getMfCustomerNo(), null);
		if(CollectionUtils.isEmpty(custLoanDtos)){
			return;
		}
		for(CustLoanDto dto : custLoanDtos){
			CompFinancingBank finanBank = new CompFinancingBank();
			finanBank.setTaskId(taskId);
			finanBank.setCustomerNo(cusBase.getCustomerNo());
			finanBank.setBusinessType(dto.getTypename());
			finanBank.setCreditBal(dto.getBalance());
			String frstAlfdDt = dto.getFrstAlfdDt();
			if(!StringUtils.isEmpty(frstAlfdDt) && frstAlfdDt.length()>=10){
				frstAlfdDt = frstAlfdDt.substring(0, 10);
			}
			finanBank.setStartDate(frstAlfdDt);
			String dueDt = dto.getDueDt();
			if(!StringUtils.isEmpty(dueDt) && dueDt.length()>=10){
				dueDt = dueDt.substring(0, 10);
			}
			finanBank.setEndDate(dueDt);
			finanBank.setUseDesc(dto.getPurpose());
			finanBank.setGuaranteeType(dto.getVouchtype());
			finanBank.setFinancialOrg("RGRCB");
			
			finanBank.setCreater(Constants.SYS_SHORT_NAME);
			finanBank.setUpdater(Constants.SYS_SHORT_NAME);
			finanBank.setUpdateTime(DateUtil.getNowTimestamp());
			
			compFinancingSV.saveCompFinaBank(finanBank);
		}
	}
	
}