package com.beawan.task.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.task.bean.ReportDataSimpleN;
import com.beawan.task.service.ReportDataSimpleNService;
@Service("reportDataSimpleNService")
public class ReportDataSimpleNServiceImpl extends BaseServiceImpl<ReportDataSimpleN> implements ReportDataSimpleNService{
	/**
	    * 注入DAO
	    */
		@Resource(name = "reportDataSimpleNDao")
	    public void setDao(BaseDao<ReportDataSimpleN> dao) {
	    	super.setDao(dao);
	    }

	@Override
	public List<ReportDataSimpleN> queryByReportDataItemId(long reportDataItemId) throws Exception {
		String query = "from ReportDataSimpleN as model"
			     + " where model.ReportDataItemId ='" + reportDataItemId
			     +"'";
		return this.select(query);
	}
}
