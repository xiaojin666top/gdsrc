package com.beawan.task.service;

import java.util.List;

import com.beawan.core.BaseService;
import com.beawan.task.bean.ReportDataSimpleN;

public interface ReportDataSimpleNService extends BaseService<ReportDataSimpleN>{
	public List<ReportDataSimpleN> queryByReportDataItemId(long reportDataItemId)throws Exception;
}
