package com.beawan.task.dto;

public class RepaySchuDto {

	private int  repayIndex;// 还款期数
	private String repayTime;//还款时间
	private Double principal;//本金
	private Double interest;//利息
	private Double total;//总数
	
	
	
	public RepaySchuDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public RepaySchuDto(int repayIndex, String repayTime, Double principal, Double interest) {
		super();
		this.repayIndex = repayIndex;
		this.repayTime = repayTime;
		this.principal = principal;
		this.interest = interest;
		this.total = principal + interest;
	}


	public int getRepayIndex() {
		return repayIndex;
	}
	public void setRepayIndex(int repayIndex) {
		this.repayIndex = repayIndex;
	}
	public String getRepayTime() {
		return repayTime;
	}
	public void setRepayTime(String repayTime) {
		this.repayTime = repayTime;
	}
	public Double getPrincipal() {
		return principal;
	}
	public void setPrincipal(Double principal) {
		this.principal = principal;
	}
	public Double getInterest() {
		return interest;
	}
	public void setInterest(Double interest) {
		this.interest = interest;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	
	
	
}
