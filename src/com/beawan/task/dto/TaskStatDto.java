package com.beawan.task.dto;


public class TaskStatDto {


	private Long id;

	private String serNo;// 业务流水号

	/** @Field @businessNo : (业务类型编号) */
	private String businessNo;

	/** @Field @name : (客户名称) */
	private String customerName;

	/** @Field @loan : (贷款金额) */
	private Double loanNumber = 0.0;
	
	/** @Field @loan : (贷款期限) */
	private Integer loanTerm;

	/** @Field @state : (任务状态,0待处理，1已处理) */
	private Integer state = 0;

	/** @Field @year : (当期报表年份) */
	private Integer year;

	/** @Field @month : (当期报表月份) */
	private Integer month;
	
	/** @Field industry : (所处行业标识) */
	private String industry;

	/** @Field @applicationTime : (申请时间) */
	private String applicationTime;

	/** @Field @applicationTime : (提交完成任务时间) */
	private String submitTime;

	/** @Field @business : (业务类型名称) */
	private String businessType;

	/** @Field @customerNo : (客户编号) */
	private String customerNo;

	/** @Field userNo : (主办客户经理编号) */
	private String userNo;

	/** @Field userName : (主办客户经理姓名) */
	private String userName;

	/** @Field userName : (协办客户经理编号) */
	private String helpUserNo;

	/** @Field @helpUserName : (协办客户经理名字) */
	private String helpUserName;

	/** @Field institutionNo : (机构号) */
	private String institutionNo;

	/** @Field institutionNo : (机构名称) */
	private String institutionName;

	/** @Field cmBaseId : (企业信息标识（用于小企业主经营贷）) */
	private String cmBaseId;
	
	/** @Field reuseLast : (是否复用上一次调查录入的信息，1：是) */
	private String reuseLast;
	
	/** @Field finaSyncFlag : (财务信息保存分析标识，0：财报数据完全不能满足系统分析要求；1：已保存并分析；2：暂时不能满足系统系统分析要求，待分析) */
	private String finaSyncFlag;
	
	/** @Field finaSyncFlag : 财务报表备注说明，当无法采集到满足要求的财报数据时必填 */
	private String finaRepRemark;

	private Integer loanCount;//申请贷款笔数
	private Double loanSum;//申请贷款总额

	
	
	// Constructors

	/** default constructor */
	

	public Long getId() {
		return id;
	}

	public TaskStatDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSerNo() {
		return serNo;
	}

	public void setSerNo(String serNo) {
		this.serNo = serNo;
	}

	public String getBusinessNo() {
		return businessNo;
	}

	public void setBusinessNo(String businessNo) {
		this.businessNo = businessNo;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Double getLoanNumber() {
		return loanNumber;
	}

	public void setLoanNumber(Double loanNumber) {
		this.loanNumber = loanNumber;
	}

	public Integer getLoanTerm() {
		return loanTerm;
	}

	public void setLoanTerm(Integer loanTerm) {
		this.loanTerm = loanTerm;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getApplicationTime() {
		return applicationTime;
	}

	public void setApplicationTime(String applicationTime) {
		this.applicationTime = applicationTime;
	}

	public String getSubmitTime() {
		return submitTime;
	}

	public void setSubmitTime(String submitTime) {
		this.submitTime = submitTime;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getHelpUserNo() {
		return helpUserNo;
	}

	public void setHelpUserNo(String helpUserNo) {
		this.helpUserNo = helpUserNo;
	}

	public String getHelpUserName() {
		return helpUserName;
	}

	public void setHelpUserName(String helpUserName) {
		this.helpUserName = helpUserName;
	}

	public String getInstitutionNo() {
		return institutionNo;
	}

	public void setInstitutionNo(String institutionNo) {
		this.institutionNo = institutionNo;
	}

	public String getInstitutionName() {
		return institutionName;
	}

	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}
	
	public String getReuseLast() {
		return reuseLast;
	}

	public void setReuseLast(String reuseLast) {
		this.reuseLast = reuseLast;
	}

	public String getCmBaseId() {
		return cmBaseId;
	}

	public void setCmBaseId(String cmBaseId) {
		this.cmBaseId = cmBaseId;
	}

	public String getFinaSyncFlag() {
		return finaSyncFlag;
	}

	public void setFinaSyncFlag(String finaSyncFlag) {
		this.finaSyncFlag = finaSyncFlag;
	}

	public String getFinaRepRemark() {
		return finaRepRemark;
	}

	public void setFinaRepRemark(String finaRepRemark) {
		this.finaRepRemark = finaRepRemark;
	}

	public Integer getLoanCount() {
		return loanCount;
	}

	public void setLoanCount(Integer loanCount) {
		this.loanCount = loanCount;
	}

	public Double getLoanSum() {
		return loanSum;
	}

	public void setLoanSum(Double loanSum) {
		this.loanSum = loanSum;
	}
	
	
}
