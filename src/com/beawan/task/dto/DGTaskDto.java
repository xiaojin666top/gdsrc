package com.beawan.task.dto;

public class DGTaskDto {
	
	private String customerName;//企业名称
	private String creditCode;//同一社会信用号
	private String legalPerson;//法人代表
	private String productId;//产品id
	private String productName;//产品名称
	private String businessNo;//模板编号
	private String businessType;//贷款类型名称
	private Double loanNumber;
	private Integer loanTerm;//贷款期限 (年)
	private String industry;//行业代码
	private String assureMeans;//担保类型
	private String loanUse;//贷款用途
	private String industryName;//行业名称
	private String repayWay;//还款方式
	
	public String getRepayWay() {
		return repayWay;
	}
	public void setRepayWay(String repayWay) {
		this.repayWay = repayWay;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCreditCode() {
		return creditCode;
	}
	public void setCreditCode(String creditCode) {
		this.creditCode = creditCode;
	}
	public String getLegalPerson() {
		return legalPerson;
	}
	public void setLegalPerson(String legalPerson) {
		this.legalPerson = legalPerson;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getBusinessNo() {
		return businessNo;
	}
	public void setBusinessNo(String businessNo) {
		this.businessNo = businessNo;
	}
	public String getBusinessType() {
		return businessType;
	}
	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}
	public Double getLoanNumber() {
		return loanNumber;
	}
	public void setLoanNumber(Double loanNumber) {
		this.loanNumber = loanNumber;
	}
	public Integer getLoanTerm() {
		return loanTerm;
	}
	public void setLoanTerm(Integer loanTerm) {
		this.loanTerm = loanTerm;
	}
	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	public String getAssureMeans() {
		return assureMeans;
	}
	public void setAssureMeans(String assureMeans) {
		this.assureMeans = assureMeans;
	}
	public String getLoanUse() {
		return loanUse;
	}
	public void setLoanUse(String loanUse) {
		this.loanUse = loanUse;
	}
	public String getIndustryName() {
		return industryName;
	}
	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}
	
	
	
	
}
