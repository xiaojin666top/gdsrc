package com.beawan.out.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.common.Constants;
import com.beawan.core.ResultDto;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.msg.dto.TransCusBase;
import com.beawan.msg.entity.TransactionMsg;
import com.beawan.msg.request.ReqBizHeader;
import com.beawan.msg.request.ReqSysHeader;
import com.beawan.msg.request.ReqTransaction;
import com.beawan.msg.response.ResBizBodyStatus;
import com.beawan.msg.response.ResponseTransaction;
import com.beawan.msg.service.IMsgSV;
import com.beawan.msg.util.MsgUtil;
import com.beawan.base.entity.User;
import com.beawan.base.service.IUserSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.DateUtil;
import com.platform.util.ExceptionUtil;
import com.platform.util.GsonUtil;
import com.platform.util.HttpUtil;
import com.platform.util.StringUtil;

@Controller
@RequestMapping({ "/out" })
/**
 * 对外暴露的接口
 * @author yzj
 *
 */
public class OutCtl {
	
	private static final Logger log = Logger.getLogger(OutCtl.class);

	@Resource
	private IUserSV userSV;
	
	@Resource
	private IMsgSV msgSV;
	
	@Resource
	private ICusBaseSV cusBaseSV;
	
	/**
	 * @Description 跳转新增用户页面
	 * @param request
	 * @param response
	 * @param userId
	 * @return
	 */
	@RequestMapping("user/Adduser.json")
	@ResponseBody
	public ResponseTransaction<ResBizBodyStatus> addUser(HttpServletRequest request, HttpServletResponse response) {
		
		ResponseTransaction<ResBizBodyStatus> myRes = null;
		//System.out.println("收到请求");
		// 用来接收传过来的参数
		String requestParam = new String();

		TransCusBase reqBizBody = null;
		ReqSysHeader reqSysHeader = null;
		ReqBizHeader reqBizHeader = null;

		//记录报文通信信息
		TransactionMsg msg = new TransactionMsg();
		try {
			requestParam = HttpUtil.getRequestMsg(request,"utf-8");
			// 得到了信贷系统的数据
			if (requestParam == null) {
				return MsgUtil.resStatus(reqSysHeader, ResultDto.RESULT_CODE_FAIL + "", "传输的报文数据为null!", "", "");
			}
			/********** 打印接收的参数 **********/
			reqBizHeader = MsgUtil.getDealCode(requestParam);
			String dealCode = reqBizHeader.getDEALCODE();
			log.info("收到请求，交易码：" + dealCode + "------->输出信贷系统接收到的报文");
			log.info(requestParam.toString());
			

			msg.setDealCode(dealCode);
			msg.setRequestMsg(requestParam);
			msg.setRequestTime(new Timestamp(new Date().getTime()));
			
			if(!"ADDUSER_001".equals(dealCode)) {
				myRes = MsgUtil.resStatus(reqSysHeader, ResultDto.RESULT_CODE_FAIL + "", "传输的报文交易码异常!", "", "");
				msg.setResponseMsg(GsonUtil.GsonString(myRes));
				msg.setResultCode(ResultDto.RESULT_CODE_FAIL);
				msg.setResultText("交易码异常");
				return myRes;
			}
			
			
			ReqTransaction<TransCusBase> reqTrans = MsgUtil.getReqTrans(requestParam.toString(), TransCusBase.class);

//			log.info("解析报文结束；时间："+DateUtil.getNowStr());
			// 这块内容是信贷穿过来的 内容是esb规范的 不能修改
			reqSysHeader = reqTrans.getHeader().getSysHeader();
			reqBizHeader = reqTrans.getBody().getRequest().getBizHeader();
			// 这个是我们的请求题内容 包含了新增客户信息
			reqBizBody = reqTrans.getBody().getRequest().getBizBody();
			
			msg.setMsgId(reqSysHeader.getMsgId());
			
			//业务处理   及新增客户
			CusBase cusBase = new CusBase();
			String cusNo = reqBizBody.getCustomerNo();
			if(cusNo!=null) {
				CusBase query = cusBaseSV.queryByCusNo(cusNo);
				if(query!=null) {
					myRes = MsgUtil.resStatus(reqSysHeader, ResultDto.RESULT_CODE_FAIL + "", "客户号已存在，交易失败", "", "");
					msg.setResponseMsg(GsonUtil.GsonString(myRes));
					msg.setResultCode(ResultDto.RESULT_CODE_FAIL);
					msg.setResultText("客户号异常");
					return myRes;
				}else {
					cusBase.setCustomerNo(cusNo);
				}
					
			}else {
				SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddHHmmss");
				cusBase.setCustomerNo(sf.format(new Date()));
			}
			cusBase.setCustomerName(reqBizBody.getCustomerName());
			cusBase.setCustomerType(reqBizBody.getCustomerType());
			cusBase.setCertCode(reqBizBody.getCertCode());
			String date = DateUtil.format(new Date(), Constants.DATE_MASK);
			cusBase.setInputDate(date);
			cusBase.setMfCustomerNo(cusBase.getCustomerNo());
			cusBase.setManagerUserId(reqBizBody.getManagerUserId());
			
			cusBaseSV.save(cusBase);
			myRes = MsgUtil.resStatus(reqSysHeader, ResultDto.RESULT_CODE_SUCCESS + "", "客户信息传输成功！" , "", "");
			msg.setResponseMsg(GsonUtil.GsonString(myRes));
			msg.setResultCode(ResultDto.RESULT_CODE_SUCCESS);
			
			log.info("请求结束-交易码：" + dealCode);
		} catch (Exception e) {
			log.error("接收数据失败", e);
			e.printStackTrace();
			myRes = MsgUtil.resStatus(reqSysHeader, ResultDto.RESULT_CODE_FAIL + "", "客户信息传输异常", "", "");
			msg.setResponseMsg(GsonUtil.GsonString(myRes));
			msg.setResultText("接收数据异常");
			return myRes;
		}finally {
			msg.setDataType("JSON");
			msg.setDealName("新增客户信息");
			msg.setReqFrom("第三方其它系统");
			msg.setTransType("HTTP");
			msgSV.saveTranscationMsg(msg);
			
		}
		
		return myRes;
	}
	
	/**
	 * @Description 检查用户是否已经存在
	 * @param request
	 * @param response
	 * @param userId 用户编号
	 * @return
	 */
	@RequestMapping("userCheck.do")
	@ResponseBody
	public String userCheck(HttpServletRequest request, HttpServletResponse response,
			String userId) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			
			if(StringUtil.isEmptyString(userId))
				ExceptionUtil.throwException("用户编号不能为空！");
			
			User user = userSV.queryById(userId);
			if(user != null)
				ExceptionUtil.throwException("用户已存在！");
			
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", e.getMessage());
			log.error("检查用户是否已经存出错：" + e.getMessage());
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description 保存新增用户信息
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("saveUser.do")
	@ResponseBody
	public String saveUser(HttpServletRequest request, HttpServletResponse response,
			String jsondata) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			JSONObject userInfo = JSONObject.fromObject(jsondata);
			User user = userSV.queryById(userInfo.getString("userId"));
			if(user != null)
				ExceptionUtil.throwException("用户已存在！");
			userSV.saveUserInfo(jsondata);
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", e.getMessage());
			log.error("保存用户信息出错", e);
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description 更新用户信息
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("updateUser.do")
	@ResponseBody
	public String updateUser(HttpServletRequest request, HttpServletResponse response,
			String jsondata) {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			
			User newUser = userSV.saveUserInfo(jsondata);
			User loginUser = HttpUtil.getCurrentUser(request);
			//修改当前用户信息
			if(newUser.getUserId().equals(loginUser.getUserId())){
				HttpUtil.getSession().setAttribute(
						Constants.SessionKey.SESSION_KEY_USER, newUser);
			}
			
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", e.getMessage());
			log.error("更新用户信息出错", e);
		}
		
		return jRequest.serialize(json, true);
	}
}
