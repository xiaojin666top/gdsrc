package com.beawan.qcc.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.model.service.AdmitResultService;
import com.beawan.qcc.service.SynchronizationQCCService;

/**
 * 企查查对接 控制器
 * @author 
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/qcc/" })
public class QccController extends BaseController{
	private static final Logger log = Logger.getLogger(QccController.class);

	@Resource
	private SynchronizationQCCService synchronizationQCCService;
	@Resource
	private AdmitResultService admitResultService;


	/** 
	 * 同步企查查的客户工商数据
	 * 
	 *  
	 ***/
	@RequestMapping("syncCustInfo.json")
	@ResponseBody
	public ResultDto syncCustInfo(HttpServletRequest request, HttpServletResponse response,String custNo){
		ResultDto re = returnFail("同步企查查的客户数据失败");
		try {
			
			synchronizationQCCService.syncQccAllData(custNo);
			
			//同步企查查工商数据
//			synchronizationQCCService.syncCompFullInfo(custNo, null);
			//同步企查查裁判文书
//			synchronizationQCCService.syncJudgmentDoc(custNo);
			
			//同步企查查裁判文书的详细内容   有点复杂  晚点做
//			 QccJudgDtlDto judgDtl = synchronizationQCCService.getJudgmentDtl("cc7a7354eb892b9768494a08ab340be9");
			
			//同步企查查对外投资
//			synchronizationQCCService.syncInvestmentList(custNo);
			
			//同步企查查最终受益人
//			QccBeneficiaryInfoDto beneficiaryInfo = synchronizationQCCService.getStockList(null, custNo);

			//同步企查查疑似实际控制人
//			QccActualControlDto actualControl = synchronizationQCCService.getActualControl(null, custNo);

			//同步企查查对外投资穿透
//			QccInvestThroughDto investThrough = synchronizationQCCService.getInvestmentThrough(null, custNo);
			
			
//			System.out.println(beneficiaryInfo);
			
			//准入分析
//			admitResultService.saveAdmitCheck(custNo);

//			finriskResultService.getInduAnalysis("");
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("同步企查查的客户数据成功");
		} catch (Exception e) {
			log.error("同步企查查的客户数据异常", e);
			re.setMsg(e.getMessage());
		}
		return re;
	}
	
	/**
	 * 获取云端服务器状态 ---》正常代表云端可以正常使用，不正常代表云端挂了，或是云服务器免费试用期过期
	 * @return
	 */
	@RequestMapping("getAnalyFlag.json")
	@ResponseBody
	public ResultDto getAnalyFlag(){
		ResultDto re = returnFail("碧湾云平台状态已过期，请联系管理员");
		try {
//			boolean analyFlag = synchronizationQCCService.getAnalyFlag();
			boolean analyFlag = true;
			if(analyFlag){
				re.setCode(ResultDto.RESULT_CODE_SUCCESS);
				re.setMsg("SUCCESS");
			}
        } catch (Exception e) {
            e.printStackTrace();
        }
		return re;
	}
	
	/** 
	 * 同步企查查的客户工商数据
	 * 
	 *  
	 ***/
	@RequestMapping("testQcc.json")
	@ResponseBody
	public ResultDto testQcc(HttpServletRequest request, String url){
		ResultDto re = returnFail("测试企查查内网接口失败");
		System.out.println(123);
		try {
			synchronizationQCCService.syncJudgmentDoc("CM202008101605206245");
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("测试企查查内网接口成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
		return re;
	}
	
}

