package com.beawan.qcc.service.impl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.base.dto.RandomizingID;
import com.beawan.common.Constants;
import com.beawan.common.config.AppConfig;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.qcc.dto.ChangeRecordsDto;
import com.beawan.qcc.dto.EmployeesDto;
import com.beawan.qcc.dto.IndustryDto;
import com.beawan.qcc.dto.PartnersDto;
import com.beawan.qcc.dto.PenaltyDto;
import com.beawan.qcc.dto.QccActualControlDto;
import com.beawan.qcc.dto.QccBeneficiaryInfoDto;
import com.beawan.qcc.dto.QccInvestThroughDto;
import com.beawan.qcc.dto.QccInvestmentDto;
import com.beawan.qcc.dto.QccJudgDtlDto;
import com.beawan.qcc.dto.QccJudgmentDto;
import com.beawan.qcc.dto.QccPagingDto;
import com.beawan.qcc.dto.QccResultFullDatailDto;
import com.beawan.qcc.dto.ShiXinItemsDto;
import com.beawan.qcc.dto.ZhiXingItemsDto;
import com.beawan.qcc.service.SynchronizationQCCService;
import com.beawan.qcc.util.QccFormatUtil;
import com.beawan.survey.custInfo.bean.CompBaseEquity;
import com.beawan.survey.custInfo.bean.CompBaseItemChange;
import com.beawan.survey.custInfo.bean.CompPenalty;
import com.beawan.survey.custInfo.bean.CompShiXinItems;
import com.beawan.survey.custInfo.bean.CompZhiXingItems;
import com.beawan.survey.custInfo.dao.CompInvestmentDao;
import com.beawan.survey.custInfo.dao.CompJudgmentDao;
import com.beawan.survey.custInfo.dto.CompBaseDto;
import com.beawan.survey.custInfo.dto.CompBaseManagerDto;
import com.beawan.survey.custInfo.dto.PersonBaseDto;
import com.beawan.survey.custInfo.entity.CompInvestment;
import com.beawan.survey.custInfo.entity.CompJudgment;
import com.beawan.survey.custInfo.service.CompPenaltyService;
import com.beawan.survey.custInfo.service.CompShiXinItemsService;
import com.beawan.survey.custInfo.service.CompZhiXingItemsService;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.platform.dto.WsReturnDto;
import com.platform.util.DateUtil;
import com.platform.util.ExceptionUtil;
import com.platform.util.FileUtil;
import com.platform.util.GsonUtil;
import com.platform.util.MapperUtil;
import com.platform.util.SoapUtil;
import com.platform.util.StringUtil;

/**
 * @Author: xyh
 * @Date: 07/08/2020
 * @Description:
 */
@Service("synchronizationQCCService")
public class SynchronizationQCCServiceImpl implements SynchronizationQCCService {


	private static final Logger log = Logger.getLogger(SynchronizationQCCServiceImpl.class);

    @Resource
    private ICusBaseSV cusBaseSV;//客户基本信息
    @Resource
    private ICompBaseSV compBaseSV;//客户详细信息
    @Resource
    private CompPenaltyService compPenaltyService;//行政处罚
    @Resource
    private CompShiXinItemsService compShiXinItemsService;//失信人
    @Resource
    private CompZhiXingItemsService compZhiXingItemsService;//法院执行
    
    @Resource
    private CompJudgmentDao compJudgmentDao;//裁判文书
    @Resource
    private CompInvestmentDao compInvestmentDao;//对外投资 

    @Override
    public void syncQccAllData(String custNo) throws Exception {
    	//同步企查查工商数据
		this.syncCompFullInfo(custNo, null);
//		//同步企查查裁判文书
		this.syncJudgmentDoc(custNo);
//		//同步企查查对外投资
		this.syncInvestmentList(custNo);
    }
    
    /**
     * 调用行内webservice在核心代码
     */
    @Override
    public synchronized String accessService(String wsdl, String method, List<Object> list) throws Exception{
		StringBuffer buffer = new StringBuffer();
		for(int i=0; i < list.size(); i++){
			buffer.append("<arg" + i + ">" + list.get(i) + "</arg" + i + ">");
		}
		String soapReturn = "";
		String soapResponseData = "";
		//拼接soap
		StringBuffer requestData = new StringBuffer("");
		requestData.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ser=\"http://webservice.rg.com/\">");
		requestData.append("<soapenv:Header/>");
		requestData.append("<soapenv:Body>");
		requestData.append("<ser:" + method + ">");
		requestData.append(buffer);
		requestData.append("</ser:" + method + ">");
		requestData.append("</soapenv:Body>" + "</soapenv:Envelope>");
        PostMethod postMethod = new PostMethod(wsdl);
     // 然后把Soap请求数据添加到PostMethod中
        byte[] b = null;
        InputStream is = null;
        try {
            b = requestData.toString().getBytes("utf-8");
            is = new ByteArrayInputStream(b, 0, b.length);
            RequestEntity re = new InputStreamRequestEntity(is, b.length, "text/xml; charset=UTF-8");
            postMethod.setRequestEntity(re);
            HttpClient httpClient = new HttpClient();
            httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(30000);
            httpClient.getHttpConnectionManager().getParams().setSoTimeout(60000);
            int status = httpClient.executeMethod(postMethod);

            if (status == 200) {
                soapResponseData = postMethod.getResponseBodyAsString();
                //接口输出过来在"被转义，需要全局替换下
                soapResponseData = soapResponseData.replaceAll("&quot;", "\"");
                WsReturnDto wsReturnDto = SoapUtil.parseSoapMessage(soapResponseData);
                soapReturn = wsReturnDto.getResult();
            }else{
            	log.error("访问企查查接口异常，异常代码：" + status + ";");
            }
            //输出接口返回报文
        	log.info("企查查接口返回数据报文：");
        	log.info(postMethod.getResponseBodyAsString());
        } catch (Exception e) {
            log.error("获取企查查"+method+"数据失败.", e);
            e.printStackTrace();
        } finally {
            if (is != null) {
                is.close();
            }
        }
        return soapReturn;
	}
    
    //会请求数据很多次

    @Override
    public void syncCompFullInfo(String custNo, Integer yearScope) throws Exception {
        //从企查查获取完整客户信息
        QccResultFullDatailDto fullData = this.getQccFullByCustNo(custNo);

        this.syncCompBase(custNo, fullData);
        this.syncManagerInfo(custNo, fullData);
        this.syncCompEquity(custNo, fullData);
        this.syncChangeItem(custNo, yearScope, fullData);
        this.syncCompPenalty(custNo, yearScope, fullData);
        this.syncCompShiXin(custNo, yearScope, fullData);
        this.syncZhiXing(custNo, yearScope, fullData);
    }
    /**
     * 根据客户编号获取客户基本信息
     * @param custNo
     * @return
     * @throws Exception
     */
    public CusBase getCusBase(String custNo) throws Exception{
        if(StringUtil.isEmptyString(custNo)){
            ExceptionUtil.throwException("客户号为空！同步企业基本数据失败");
        }
        CusBase cusBase = cusBaseSV.queryByCusNo(custNo);
        if(cusBase==null){
            ExceptionUtil.throwException("客户基本信息异常！客户号为："+ custNo +";同步企业基本数据失败");
        }
        return cusBase;
    }

    @Override
    public QccResultFullDatailDto getQccFullByCustNo(String custNo) throws Exception {
        //获取客户数据
        CusBase cusBase = this.getCusBase(custNo);
        //从企查查获取完整客户信息
        QccResultFullDatailDto fullData = this.getQccFullByCustomerName(cusBase.getCustomerName());
        return fullData;
    }

    @Override
    public QccResultFullDatailDto getQccFullByCustomerName(String customerName) throws Exception {
//        //获取full大报文
//        Map<String,Object> params = new HashMap<>();
//        HttpClientUtil instance = HttpClientUtil.getInstance(AppConfig.Cfg.GET_QCC_FULL_URL);
//        
//        String responseBody = instance.doGet(params);
//        //不一定需要
//        responseBody = URLDecoder.decode(responseBody,"utf-8");

        List<Object> list = new ArrayList<>();
		list.add(AppConfig.Cfg.QCC_SYSKEY);
		list.add(customerName);
		list.add(30);
		
		String responseBody;
		if(AppConfig.Cfg.QCC_SWITCH.equals("on")){
			responseBody = this.accessService(AppConfig.Cfg.QCC_API_URL, AppConfig.Cfg.QCC_GET_ENT_BASICINFO, list);
		}else{
			responseBody = FileUtil.txt2String(ResourceUtils.getFile("classpath:qichacha/qccfull.json"));
		}
        
        
        //模拟数据
        
       
		//数据查询内容为空
		if(StringUtil.isEmptyString(responseBody)){
			log.error(customerName + "在企查查中暂无工商数据，请核实企业名称");
			ExceptionUtil.throwException(customerName + "在企查查中暂无工商数据，请核实企业名称");
		}
		
        //解析json数据
        //获得解析器
        JsonParser parser = new JsonParser();
        Integer resultCode = parser.parse(responseBody).getAsJsonObject().get("Status").getAsInt();
        String resultMsg = parser.parse(responseBody).getAsJsonObject().get("Message").getAsString();
        if(resultCode != 200){
            ExceptionUtil.throwException("获取企查查数据失败：" + resultMsg);
        }
        //获取result的json
        String resultFull = GsonUtil.GsonString(parser.parse(responseBody).getAsJsonObject().get("Result"));
        log.info(resultFull);

        QccResultFullDatailDto resultFullDatailDto = GsonUtil.GsonToBean(resultFull,QccResultFullDatailDto.class);

        return resultFullDatailDto;
    }

    /**
     * 获取企业裁判文书
     */
    @Override
    public List<QccJudgmentDto> getJudgmentDoc(String customerName) throws Exception{
    	List<QccJudgmentDto> resultList = new ArrayList<>();
//    	customerName = "北京宇信科技集团股份有限公司";
    	List<Object> list = new ArrayList<>();
		list.add(AppConfig.Cfg.QCC_SYSKEY);
		list.add(customerName);
		list.add("");
		list.add("1");
    	for(int pageIndex = 1; pageIndex <= 2; pageIndex++){
    	
    		list.set(3, pageIndex+"");
    		
    		String responseBody;
    		if(AppConfig.Cfg.QCC_SWITCH.equals("on")){
    			responseBody = this.accessService(AppConfig.Cfg.QCC_API_URL, AppConfig.Cfg.QCC_GET_JUDGMENT_DOC, list);
    		}else{
    	        //裁判文书 模拟数据
    	        responseBody = FileUtil.txt2String(ResourceUtils.getFile("classpath:qichacha/judgment.json"));
    		}
			
			if(StringUtil.isEmptyString(responseBody)){
				log.error(customerName + "在企查查中暂无裁判文书信息");
				ExceptionUtil.throwException(customerName + "在企查查中暂无裁判文书信息");
			}
			
			JsonParser parser = new JsonParser();
	        Integer resultCode = parser.parse(responseBody).getAsJsonObject().get("Status").getAsInt();
	        String resultMsg = parser.parse(responseBody).getAsJsonObject().get("Message").getAsString();
	        if(resultCode != 200){
	        	log.info(resultMsg);
	        	return null;
//	            ExceptionUtil.throwException("获取企查查数据失败：" + resultMsg);
	        }
	        //获取result的json
	        String resultFull = GsonUtil.GsonString(parser.parse(responseBody).getAsJsonObject().get("Result"));
	        //裁判文书结果集列表
	        @SuppressWarnings("serial")
			List<QccJudgmentDto> judgList = new Gson().fromJson(resultFull, new TypeToken<List<QccJudgmentDto>>() {}.getType());
	        //分页对象
	        String paging = GsonUtil.GsonString(parser.parse(responseBody).getAsJsonObject().get("Paging"));
	        QccPagingDto pagingDto = GsonUtil.GsonToBean(paging,QccPagingDto.class);
	        
	        resultList.addAll(judgList);
	        if(judgList.size() >= pagingDto.getTotalRecords()){
	        	break;
	        }
    	}
    	return resultList;
    }
    

	@Override
	public QccJudgDtlDto getJudgmentDtl(String judgId) throws Exception {
		//获取裁判文书详情
	//  public String getJudgmentDtl(String SysKey, String id,int IntevalDay);
    	List<Object> list = new ArrayList<>();
		list.add(AppConfig.Cfg.QCC_SYSKEY);
		list.add(judgId);//客户名称
		list.add("30");//数据有效期（天）
		
		String responseBody;
		if(AppConfig.Cfg.QCC_SWITCH.equals("on")){
			responseBody = this.accessService(AppConfig.Cfg.QCC_API_URL, AppConfig.Cfg.QCC_GET_JUDGMENT_DTL, list);
		}else{
			//裁判文书详情 模拟数据
			responseBody = FileUtil.txt2String(ResourceUtils.getFile("classpath:qichacha/judgDtl.json"));
		}
		
		if(StringUtil.isEmptyString(responseBody)){
			log.error("裁判文书id：" + judgId + "--在企查查暂无裁判文书详情信息");
			ExceptionUtil.throwException("暂无裁判文书详情信息");
		}
		
		JsonParser parser = new JsonParser();
        Integer resultCode = parser.parse(responseBody).getAsJsonObject().get("Status").getAsInt();
        String resultMsg = parser.parse(responseBody).getAsJsonObject().get("Message").getAsString();
        if(resultCode != 200){
        	log.error("获取企查查裁判文书详情数据失败：" + resultMsg);
            ExceptionUtil.throwException("获取企查查裁判文书详情数据失败：" + resultMsg);
        }
        //获取result的json
        String resultFull = GsonUtil.GsonString(parser.parse(responseBody).getAsJsonObject().get("Result"));
        
        //对外投资详情
        QccJudgDtlDto judgDtl = GsonUtil.GsonToBean(resultFull, QccJudgDtlDto.class);
    	return judgDtl;
		
		
	}
    
    @Override
	public List<QccInvestmentDto> getInvestmentList(String customerName) throws Exception{
    	List<QccInvestmentDto> resultList = new ArrayList<>();
//    	customerName = "北京宇信科技集团股份有限公司";
    	//具体接口    public String getInvestmentList(String SysKey,String searchKey,String pageIndex);
    	List<Object> list = new ArrayList<>();
		list.add(AppConfig.Cfg.QCC_SYSKEY);
		list.add(customerName);
		int pageIndex = 1;
		list.add("1");
    	while(true){
    	
    		list.set(2, pageIndex+"");

    		String responseBody;
    		if(AppConfig.Cfg.QCC_SWITCH.equals("on")){
    			responseBody = this.accessService(AppConfig.Cfg.QCC_API_URL, AppConfig.Cfg.QCC_INVESTMENT_LIST, list);
    		}else{
   			 //对外投资 模拟数据
    			responseBody = FileUtil.txt2String(ResourceUtils.getFile("classpath:qichacha/investment.json"));
    		}
			
			
			if(StringUtil.isEmptyString(responseBody)){
				log.error(customerName + "在企查查中暂无对外投资信息");
				return null;
//				ExceptionUtil.throwException(customerName + "在企查查中暂无对外投资信息");
			}
			
			JsonParser parser = new JsonParser();
	        Integer resultCode = parser.parse(responseBody).getAsJsonObject().get("Status").getAsInt();
	        //String resultMsg = parser.parse(responseBody).getAsJsonObject().get("Message").getAsString();
	        if(resultCode != 200){
	        	return null;
//	            ExceptionUtil.throwException("获取企查查数据失败：" + resultMsg);
	        }
	        //获取result的json
	        String resultFull = GsonUtil.GsonString(parser.parse(responseBody).getAsJsonObject().get("Result"));
	        //对外投资结果集列表
	        @SuppressWarnings("serial")
			List<QccInvestmentDto> inventmentList = new Gson().fromJson(resultFull, new TypeToken<List<QccInvestmentDto>>() {}.getType());
	        //分页对象
	        String paging = GsonUtil.GsonString(parser.parse(responseBody).getAsJsonObject().get("Paging"));
	        QccPagingDto pagingDto = GsonUtil.GsonToBean(paging,QccPagingDto.class);
	        
	        resultList.addAll(inventmentList);
	        if(inventmentList.size() >= pagingDto.getTotalRecords()){
	        	break;
	        }
	        pageIndex++;
        
    	}
    	return resultList;
    }
    
    @Override
    public QccBeneficiaryInfoDto getStockList(String customerName, String custNo) throws Exception{
    	if(StringUtil.isEmptyString(customerName)){
    		CusBase cusBase = cusBaseSV.queryByCusNo(custNo);
    		customerName = cusBase.getCustomerName();
    	}
    	
    	//已废弃public String getBeneficiaryInfo(String SysKey,String EntName,String Percent,int IntevalDay);
    	//现使用  public String getStockList(String SysKey,String EntName,String Percent,String Mode,int IntevalDay);
    	List<Object> list = new ArrayList<>();
		list.add(AppConfig.Cfg.QCC_SYSKEY);
		list.add(customerName);
		list.add("30");//股权穿透最小百分比
		list.add("0");//穿透方式，0：穿透受益自然人，1：穿透受益企业法人， 2： 穿透受益自然人和企业法人。默认值为0
		list.add("30");//数据有效期（天）

		String responseBody;
		if(AppConfig.Cfg.QCC_SWITCH.equals("on")){
			responseBody = this.accessService(AppConfig.Cfg.QCC_API_URL, AppConfig.Cfg.QCC_GET_STOCK_LIST, list);
		}else{
			//收益所有人列表 模拟数据
	        responseBody = FileUtil.txt2String(ResourceUtils.getFile("classpath:qichacha/stockList.json"));
		}
		
		
		
		if(StringUtil.isEmptyString(responseBody)){
			log.error(customerName + "在企查查暂无最终收益人信息");
			ExceptionUtil.throwException(customerName + "在企查查中暂无最终收益人信息");
		}
		
		JsonParser parser = new JsonParser();
        Integer resultCode = parser.parse(responseBody).getAsJsonObject().get("status").getAsInt();
        String resultMsg = parser.parse(responseBody).getAsJsonObject().get("msg").getAsString();
        if(resultCode != 200){
            ExceptionUtil.throwException("获取企查查最终收益人数据失败：" + resultMsg);
        }
        //获取result的json
        String resultFull = GsonUtil.GsonString(parser.parse(responseBody).getAsJsonObject().get("result"));
        
        //最终受益人
        QccBeneficiaryInfoDto beneficiary = GsonUtil.GsonToBean(resultFull, QccBeneficiaryInfoDto.class);
    	return beneficiary;
    }
    
    @Override
    public QccActualControlDto getActualControl(String customerName, String custNo) throws Exception {
    	if(StringUtil.isEmptyString(customerName)){
    		CusBase cusBase = cusBaseSV.queryByCusNo(custNo);
    		customerName = cusBase.getCustomerName();
    	}
    	
    	//  public String getActualControl(String SysKey,String EntName, int IntevalDay);
    	List<Object> list = new ArrayList<>();
		list.add(AppConfig.Cfg.QCC_SYSKEY);
		list.add(customerName);//客户名称
		list.add("30");//数据有效期（天）

		String responseBody;
		if(AppConfig.Cfg.QCC_SWITCH.equals("on")){
			responseBody = this.accessService(AppConfig.Cfg.QCC_API_URL, AppConfig.Cfg.QCC_GET_ACTUAL_CONTROL, list);
		}else{
			//疑似实际控制人 模拟数据
	        responseBody = FileUtil.txt2String(ResourceUtils.getFile("classpath:qichacha/actualControl.json"));
		}
		
		if(StringUtil.isEmptyString(responseBody)){
			log.error(customerName + "在企查查暂无疑似实际控制人信息");
			ExceptionUtil.throwException(customerName + "在企查查中暂无疑似实际控制人信息");
		}
		
		JsonParser parser = new JsonParser();
        Integer resultCode = parser.parse(responseBody).getAsJsonObject().get("Status").getAsInt();
        String resultMsg = parser.parse(responseBody).getAsJsonObject().get("Message").getAsString();
        if(resultCode != 200){
        	log.error("获取企查查疑似实际控制人数据失败：" + resultMsg);
            ExceptionUtil.throwException("获取企查查疑似实际控制人数据失败：" + resultMsg);
        }
        //获取result的json
        String resultFull = GsonUtil.GsonString(parser.parse(responseBody).getAsJsonObject().get("Result"));
        
        //疑似实际控制人
        QccActualControlDto actualControl = GsonUtil.GsonToBean(resultFull, QccActualControlDto.class);
    	return actualControl;
    }
    
    @Override
    public QccInvestThroughDto getInvestmentThrough(String customerName, String custNo) throws Exception {
    	if(StringUtil.isEmptyString(customerName)){
    		CusBase cusBase = cusBaseSV.queryByCusNo(custNo);
    		customerName = cusBase.getCustomerName();
    	}
    	
    	//  public String getInvestmentThrough(String SysKey,String searchKey,String percent,int IntevalDay);
    	List<Object> list = new ArrayList<>();
		list.add(AppConfig.Cfg.QCC_SYSKEY);
		list.add(customerName);//客户名称
		list.add("50");//对外投资穿透占比
		list.add("30");//数据有效期（天）

		String responseBody;
		if(AppConfig.Cfg.QCC_SWITCH.equals("on")){
			responseBody = this.accessService(AppConfig.Cfg.QCC_API_URL, AppConfig.Cfg.QCC_GET_INVESTMENT_THROUTH, list);
		}else{
			//对外投资穿透 模拟数据
	        responseBody = FileUtil.txt2String(ResourceUtils.getFile("classpath:qichacha/investThrough.json"));
		}
		
		if(StringUtil.isEmptyString(responseBody)){
			log.error(customerName + "在企查查暂无对外投资穿透信息");
			ExceptionUtil.throwException(customerName + "在企查查中暂无对外投资穿透信息");
		}
		
		JsonParser parser = new JsonParser();
        Integer resultCode = parser.parse(responseBody).getAsJsonObject().get("Status").getAsInt();
        String resultMsg = parser.parse(responseBody).getAsJsonObject().get("Message").getAsString();
        if(resultCode != 200){
        	log.error("获取企查查对外投资穿透数据失败：" + resultMsg);
            ExceptionUtil.throwException("获取企查查对外投资穿透数据失败：" + resultMsg);
        }
        //获取result的json
        String resultFull = GsonUtil.GsonString(parser.parse(responseBody).getAsJsonObject().get("Result"));
        
        //对外投资穿透
        QccInvestThroughDto investThrough = GsonUtil.GsonToBean(resultFull, QccInvestThroughDto.class);
    	return investThrough;
    }
    
    
    
    /******* 以下为同步到对公系统在操作  ********/
    @Override
    public void syncCompBase(String custNo, QccResultFullDatailDto fullData) throws Exception {
        //CusBase cusBase = this.getCusBase(custNo);
        CompBaseDto compBase = compBaseSV.findCompBaseByCustNo(custNo);
        if(compBase==null){
            compBase = new CompBaseDto();
            compBase.setCustomerNo(custNo);
        }
        //出资人及出资信息
        List<PartnersDto> partners = fullData.getPartners();
        //实际控制人		先判断是否信贷系统中已经
        if(!CollectionUtils.isEmpty(partners)){
            for(PartnersDto partner : partners){
                List<String> tagsList = partner.getTagsList();
                if(tagsList.contains("实际控制人")){
                    compBase.setControlPerson(partner.getStockName());
                    break;
                }
            }
        }
        compBase.setCustomerName(fullData.getName());
        //证券号码
        compBase.setCertCode(fullData.getCreditCode());
        compBase.setCertType("统一社会信用代码");
        String startDate = fullData.getStartDate();
        //只有数据为空，才从企查查获取数据  --》不然一律使用信贷系统中下发数据
        if(StringUtils.isEmpty(compBase.getFoundDate())){
        	compBase.setFoundDate(startDate);
        }
        //法人从业年限默认以公司成立起至今数据同步日计算  -->直接粗略计算
        if(!StringUtil.isEmptyString(startDate)) {
            int nowYear = DateUtil.getYear(new Date());
            int workYears = nowYear - Integer.parseInt(startDate.substring(0, 4));
            compBase.setLegalWorkingYears(String.valueOf(workYears));
            compBase.setControlWorkingYears(String.valueOf(workYears));
        }
        IndustryDto industry = fullData.getIndustry();
        if(industry!=null) {
            compBase.setIndustryCode(industry.getSmallCategoryCode());
            compBase.setIndustryType(industry.getSmallCategory());
        }
        compBase.setLegalPerson(fullData.getOperName());
        if(StringUtil.isEmptyString(compBase.getControlPerson())){
            compBase.setControlPerson(fullData.getOperName());
        }
        compBase.setMainBusiness(fullData.getScope());
        String recCap = fullData.getRecCap();
        if(!StringUtil.isEmptyString(recCap)) {
        	//当信贷系统中已经存在实收资本，则直接使用，优先级最高
        	if(compBase.getRealCapital()==null || compBase.getRealCapital()==0){
        		compBase.setRealCapital(StringUtil.getDouVal(recCap));
                String[] recCapStr = StringUtil.spiltMoney(recCap);
                compBase.setPcCurrency(recCapStr[2]);
        	}
        }
       
        //GGG
        compBase.setRegisterAddress(fullData.getAddress());
        String registCapi = fullData.getRegistCapi();
        if(!StringUtil.isEmptyString(registCapi)) {
            compBase.setRegisterCapital(StringUtil.getDouVal(registCapi));
            String[] registCapiStr = StringUtil.spiltMoney(registCapi);
            compBase.setRcCurrency(registCapiStr[2]);
        }
        compBase.setRunAddress(fullData.getAddress());
        compBase.setRunScope(fullData.getScope());
//        compBase.setStockCustomer();
        //GGG
        compBase.setCompanyNature(QccFormatUtil.formatEntType(fullData.getEntType()));
        String insuredCount = fullData.getInsuredCount();
        if(!StringUtil.isEmptyString(insuredCount)){
            compBase.setEmployeeNum(Integer.parseInt(insuredCount));
        }
        compBase.setRegNumber(fullData.getBelondOrg());
        compBase.setOrgNumber(fullData.getOrgNo());
        compBase.setApprovedTime(fullData.getUpdateDate());
        compBase.setRegInstitute(fullData.getBelondOrg());
        //设置行业默认政策为 非限制
        if(StringUtil.isEmptyString(compBase.getIndusPolicy())){
        	//01 限制类行业 02 淘汰类行业 03 禁止类行业 04 非限制类、禁止类、淘汰类行业 -->字典项类别 STD_INDUSTRY_POLICY
        	compBase.setIndusPolicy("04");
        }
        
        compBaseSV.saveCompBase(compBase);

    }

    @Override
    public void syncManagerInfo(String custNo, QccResultFullDatailDto fullData) throws Exception {
        //CusBase cusBase = this.getCusBase(custNo);
        RandomizingID random = new RandomizingID("PS", "yyyyMMddHHmmss", 4, false);
        List<CompBaseManagerDto> managerInfoByCustNo = compBaseSV.findCompManagerByCustNo(custNo);
        List<EmployeesDto> employees = fullData.getEmployees();
        if(!CollectionUtils.isEmpty(employees)){
            for(EmployeesDto emp : employees){
                boolean flag = false;
                //如果已经存在高管
                if(!CollectionUtils.isEmpty(managerInfoByCustNo)){
                    manager:for(CompBaseManagerDto manager : managerInfoByCustNo){
                        if(emp.getName().equals(manager.getManagerName())){
                            flag = true;
                            manager.setManagerType(emp.getJob());
                            compBaseSV.saveCompManager("", manager);
                            break manager;
                        }
                    }
                    //若企查查中的高管名称在系统中不存在，则新增
                    if(!flag){
                        CompBaseManagerDto managerInfo = new CompBaseManagerDto();
                        managerInfo.setManagerName(emp.getName());
                        managerInfo.setManagerType(emp.getJob());
                        managerInfo.setCmCusNo(custNo);

                        PersonBaseDto personBase = new PersonBaseDto();
                        personBase.setCustomerNo(random.genNewId());
                        personBase.setName(emp.getName());
                        managerInfo.setPsCusNo(personBase.getCustomerNo());
                        managerInfo.setPersonBase(personBase);
                        compBaseSV.saveCompManager("", managerInfo);
                    }
                }else{
                    CompBaseManagerDto managerInfo = new CompBaseManagerDto();
                    managerInfo.setManagerName(emp.getName());
                    managerInfo.setManagerType(emp.getJob());
                    managerInfo.setCmCusNo(custNo);

                    PersonBaseDto personBase = new PersonBaseDto();
                    personBase.setCustomerNo(random.genNewId());
                    personBase.setName(emp.getName());
                    managerInfo.setPsCusNo(personBase.getCustomerNo());
                    managerInfo.setPersonBase(personBase);
                    compBaseSV.saveCompManager("", managerInfo);

                }
            }
        }

    }

    @Override
    public void syncCompEquity(String custNo, QccResultFullDatailDto fullData) throws Exception {
        //CusBase cusBase = this.getCusBase(custNo);
        //从企查查获取完整客户信息
        List<CompBaseEquity> compEquityByCustNo = compBaseSV.findCompEquityByCustNo(custNo);
        List<PartnersDto> partners = fullData.getPartners();

        if(!CollectionUtils.isEmpty(partners)) {
            for (PartnersDto partner : partners) {
                if (!CollectionUtils.isEmpty(compEquityByCustNo)) {
                    equity:for(CompBaseEquity equity : compEquityByCustNo){
                        if(partner.getStockName().equals(equity.getStockName())){
                            equity.setStockName(partner.getStockName());
                            //默认设置为人民币  对应的字典项为CNY
                            equity.setCurrency("CNY");
                            equity.setFundAmount(partner.getShouldCapi());
                            equity.setFundRate(partner.getStockPercent());
                            equity.setFundWay(partner.getInvestType());
                            equity.setInvtFactAmt(partner.getRealCapi());
                            List<String> tagsList = partner.getTagsList();
                            if(tagsList!=null && tagsList.size()!=0){
                                StringBuilder remark = new StringBuilder();
                                for(String tag : tagsList){
                                    remark.append(tag + ";");
                                }
                                equity.setRelationDesc(remark.toString());
                            }
                            compBaseSV.saveCompEquity(equity);
                            break equity;
                        }
                    }
                }else{
                    CompBaseEquity custEquity = new CompBaseEquity();
                    custEquity.setCustomerNo(custNo);
                    custEquity.setStockName(partner.getStockName());
                    custEquity.setCurrency("CNY");
                    custEquity.setFundAmount(partner.getShouldCapi());
                    custEquity.setFundRate(partner.getStockPercent());
                    custEquity.setFundWay(partner.getInvestType());
                    custEquity.setInvtFactAmt(partner.getRealCapi());
                    List<String> tagsList = partner.getTagsList();
                    if(tagsList!=null && tagsList.size()!=0){
                        StringBuilder remark = new StringBuilder();
                        for(String tag : tagsList){
                            remark.append(tag + ";");
                        }
                        custEquity.setRelationDesc(remark.toString());
                    }
                    compBaseSV.saveCompEquity(custEquity);
                }
            }
        }
    }

    @Override
    public void syncChangeItem(String custNo, Integer yearScope, QccResultFullDatailDto fullData) throws Exception {
        if(yearScope==null || yearScope==0){
            yearScope = 3;//若为空，则默认为3年
        }
        //CusBase cusBase = this.getCusBase(custNo);
        List<CompBaseItemChange> changeListByCustNo = compBaseSV.findChangeListByCustNo(custNo);
        List<ChangeRecordsDto> changeRecords = fullData.getChangeRecords();
        if(changeListByCustNo!=null && changeListByCustNo.size()!=0) {
            for (CompBaseItemChange item : changeListByCustNo) {
                compBaseSV.deleteCompEquityChange(item);
            }
        }
        if(changeRecords!=null && changeRecords.size()!=0){
            for(ChangeRecordsDto record : changeRecords){
                //因为变更信息太多了--》yearScope为向前推的时间范围
                Date sratr = DateUtil.addYear(new Date(), -yearScope);
                Date changeDate = DateUtil.parseDate(record.getChangeDate());
                if(changeDate.compareTo(sratr)>=0){
                    CompBaseItemChange change = new CompBaseItemChange();
                    change.setCustomerNo(custNo);
                    change.setChangeDate(record.getChangeDate());
                    change.setChangeItem(record.getProjectName());
                    change.setChangeBefore(record.getBeforeContent());
                    change.setChangeAfter(record.getAfterContent());
//                change.setChangeDate();
                    compBaseSV.saveComItemChange(change);
                }
            }
        }
    }

    @Override
    public void syncCompPenalty(String custNo, Integer yearScope, QccResultFullDatailDto fullData) throws Exception {
        if(yearScope==null || yearScope==0){
            yearScope = 3;//若为空，则默认为3年
        }
        //CusBase cusBase = this.getCusBase(custNo);

        List<PenaltyDto> penaltyList = fullData.getPenalty();
        if(!CollectionUtils.isEmpty(penaltyList)) {
            //原来数据--》逻辑全部移除
            List<CompPenalty> compPenalty = compPenaltyService.selectByProperty("customerNo", custNo);
            if (!CollectionUtils.isEmpty(compPenalty)) {
                for (CompPenalty penalty : compPenalty) {
                    penalty.setStatus(Constants.DELETE);
                    compPenaltyService.saveOrUpdate(penalty);
                }
            }
            for(PenaltyDto dto : penaltyList){
                CompPenalty penalty = MapperUtil.trans(dto, CompPenalty.class);
                penalty.setCustomerNo(custNo);
                compPenaltyService.saveOrUpdate(penalty);
            }
        }
    }

    @Override
    public void syncCompShiXin(String custNo, Integer yearScope, QccResultFullDatailDto fullData) throws Exception {
        if(yearScope==null || yearScope==0){
            yearScope = 3;//若为空，则默认为3年
        }
        //CusBase cusBase = this.getCusBase(custNo);

        List<ShiXinItemsDto> shiXinItems = fullData.getShiXinItems();
        if(!CollectionUtils.isEmpty(shiXinItems)) {
            //原来数据--》逻辑全部移除
            List<CompShiXinItems> compShiXins = compShiXinItemsService.selectByProperty("customerNo", custNo);
            if (!CollectionUtils.isEmpty(compShiXins)) {
                for (CompShiXinItems shixin : compShiXins) {
                    shixin.setStatus(Constants.DELETE);
                    compShiXinItemsService.saveOrUpdate(shixin);
                }
            }
            for(ShiXinItemsDto dto : shiXinItems){
                CompShiXinItems shixin = MapperUtil.trans(dto, CompShiXinItems.class);
                shixin.setCustomerNo(custNo);
                compShiXinItemsService.saveOrUpdate(shixin);
            }
        }
    }

    @Override
    public void syncZhiXing(String custNo, Integer yearScope, QccResultFullDatailDto fullData) throws Exception {
    	if(yearScope==null || yearScope==0){
            yearScope = 3;//若为空，则默认为3年
        }
        //CusBase cusBase = this.getCusBase(custNo);

        List<ZhiXingItemsDto> zhixingItems = fullData.getZhiXingItems();
        if(!CollectionUtils.isEmpty(zhixingItems)) {
            //原来数据--》逻辑全部移除
            List<CompZhiXingItems> compZhiXings = compZhiXingItemsService.selectByProperty("customerNo", custNo);
            if (!CollectionUtils.isEmpty(compZhiXings)) {
                for (CompZhiXingItems zx : compZhiXings) {
                	zx.setStatus(Constants.DELETE);
                	compZhiXingItemsService.saveOrUpdate(zx);
                }
            }
            for(ZhiXingItemsDto dto : zhixingItems){
            	CompZhiXingItems zhixing = MapperUtil.trans(dto, CompZhiXingItems.class);
            	zhixing.setCustomerNo(custNo);
            	compZhiXingItemsService.saveOrUpdate(zhixing);
            }
        }
    }
	
    @Override
    public void syncJudgmentDoc(String custNo) throws Exception{

        CusBase cusBase = this.getCusBase(custNo);
        List<QccJudgmentDto> dtoList = this.getJudgmentDoc(cusBase.getCustomerName());
        List<CompJudgment> judgList = MapperUtil.trans(dtoList, CompJudgment.class);
        if(!CollectionUtils.isEmpty(judgList)){
        	for(CompJudgment judg : judgList){
        		judg.setCustomerNo(custNo);
        		compJudgmentDao.saveOrUpdate(judg);
        	}
        }
//        compJudgmentDao.batchSaveUpdata(judgList);
    }
	
    @Override
    public void syncInvestmentList(String custNo) throws Exception{

        CusBase cusBase = this.getCusBase(custNo);
        List<QccInvestmentDto> dtoList = this.getInvestmentList(cusBase.getCustomerName());
        List<CompInvestment> investList = MapperUtil.trans(dtoList, CompInvestment.class);
        if(!CollectionUtils.isEmpty(investList)){
        	for(CompInvestment invest : investList){
        		invest.setCustomerNo(custNo);
        		compInvestmentDao.saveOrUpdate(invest);
        	}
        }
//        compInvestmentDao.batchSaveUpdata(investList);
    }

    
	@Override
	public boolean getAnalyFlag(){
		try{
			List<Object> list = new ArrayList<>();
			list.add(AppConfig.Cfg.BEAWAN_SYSKEY);
			list.add("JSRG001");
			list.add("1");
			
			String responseBody = this.accessService(AppConfig.Cfg.BEAWAN_API_URL, 
					AppConfig.Cfg.BEAWAN_GETANALYSISFLAG, list);
			//数据查询内容为空
			if(StringUtil.isEmptyString(responseBody)){
				log.error("碧湾云服务器异常，请联系系统管理员");
				return false;
			}
			JsonParser parser = new JsonParser();
	        Integer resultCode = parser.parse(responseBody).getAsJsonObject().get("code").getAsInt();
	        if(resultCode != 0){
	        	log.error("碧湾云平台已过期，请联系系统管理员");
	        	return false;
	        }
		}catch(Exception e){
			log.error("碧湾云服务器异常，请联系系统管理员");
			e.printStackTrace();
			return false;
		}
		return true;
	}

    

}














