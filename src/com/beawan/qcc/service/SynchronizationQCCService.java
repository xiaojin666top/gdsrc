package com.beawan.qcc.service;

import java.util.List;

import com.beawan.qcc.dto.QccActualControlDto;
import com.beawan.qcc.dto.QccBeneficiaryInfoDto;
import com.beawan.qcc.dto.QccInvestThroughDto;
import com.beawan.qcc.dto.QccInvestmentDto;
import com.beawan.qcc.dto.QccJudgDtlDto;
import com.beawan.qcc.dto.QccJudgmentDto;
import com.beawan.qcc.dto.QccResultFullDatailDto;

/**
 * @Author: xyh
 * @Date: 07/08/2020
 * @Description:
 */
public interface SynchronizationQCCService{
	
	/**
	 * 同步企查查所有接口
	 * @param custNo
	 * @throws Exception
	 */
	public void syncQccAllData(String custNo) throws Exception;
	
	/**
	 * 对接行内企查查接口   webservice在接口wsdl  通过http   
	 * @param wsdl		wsdl地址
	 * @param method	方法名
	 * @param list		参数  --》需要根据接口在参数顺序
	 * @return
	 * @throws Exception
	 */
	public String accessService(String wsdl, String method, List<Object> list)throws Exception;

    /**
     * 同步企查查full接口数据，直接同步进数据库
     * 包括
     * @param custNo        客户号
     * @param yearScope     时间年份范围-》针对变更信息的有效期
     */
    void syncCompFullInfo(String custNo, Integer yearScope) throws Exception;

    /**
     * 根据对公系统客户号  获取企查查所有数据
     * @param custNo  客户编号
     * @return
     * @throws Exception
     */
    QccResultFullDatailDto getQccFullByCustNo(String custNo) throws Exception;

    /**
     * 根据客户姓名，企查查full数据
     * @param customerName 客户姓名
     * @throws Exception
     */
    QccResultFullDatailDto getQccFullByCustomerName(String customerName)throws Exception;
    
    /**
     * 获取企业最新100条司法数据
     * @param custerName
     * @return
     * @throws Exception
     */
    List<QccJudgmentDto> getJudgmentDoc(String customerName) throws Exception;
    

    /***
     *    获取裁判文书详情  本地不需要保存
     * @param judgId	裁判文书id
     */
	QccJudgDtlDto getJudgmentDtl(String judgId) throws Exception;

    
    
    /**
     * 获取企业对外投资数据
     * @param customerName
     * @return
     * @throws Exception
     */
    List<QccInvestmentDto> getInvestmentList(String customerName) throws Exception;
    
    /***
     * 
     * 获取企业受益人穿透信息
     * @param customerName    企业的名称
     * @param custNo		若企业名称为空的时候，才会使用，通过对公客户号查询企业名称
     * @return
     * @throws Exception
     */
    QccBeneficiaryInfoDto getStockList(String customerName, String custNo) throws Exception;
    
    
    /***
     * 获取企业疑似实际控制人
     * @param customerName	企业名称
     * @param custNo		若企业名称为空的时候，才会使用，通过对公客户号查询企业名称
     * @return
     * @throws Exception
     */
    QccActualControlDto getActualControl(String customerName, String custNo) throws Exception;
    
    /***
     * 企业对外投资穿透
     * @param customerName
     * @param custNo
     * @return
     * @throws Exception
     */
    QccInvestThroughDto getInvestmentThrough(String customerName, String custNo) throws Exception;
    
    
    /**************************************  同步企业工商信息  开始*****************************************************/
    
    /**
     * 同步企业基本信息
     * @param custNo    需要同步的客户号
     * @throws Exception
     */
    void syncCompBase(String custNo, QccResultFullDatailDto fullData)throws Exception;
    /**
     * 同步企业高管信息
     * 目前同步才用非覆盖策略，采用混合策略-》原来数据库中的有的数据  与企查查查询重复，进行更新。若企查查中没有的，则还原始保留
     * @param custNo    需要同步的客户号
     * @throws Exception
     */
    void syncManagerInfo(String custNo, QccResultFullDatailDto fullData)throws Exception;

    /**
     * 同步股权结果
     * @param custNo
     * @throws Exception
     */
    void syncCompEquity(String custNo, QccResultFullDatailDto fullData) throws Exception;

    /**
     * 同步企业变更信息
     * 只同步进3年
     * @param custNo
     * @param yearScope     变更年限范围
     * @throws Exception
     */
    void syncChangeItem(String custNo, Integer yearScope, QccResultFullDatailDto fullData) throws Exception;

    /**
     * 同步企业行政处罚（非信用中国）
     * @param custNo        客户号
     * @param yearScope     数据有限范围（年）
     * @throws Exception
     */
    void syncCompPenalty(String custNo, Integer yearScope, QccResultFullDatailDto fullData) throws  Exception;

    /**
     * 同步企业失信人名单
     * @param custNo        客户号
     * @param yearScope     数据有限范围
     * @throws Exception
     */
    void syncCompShiXin(String custNo, Integer yearScope, QccResultFullDatailDto fullData) throws Exception;
    
    /**
     * 同步执行列表
     * @param custNo		客户号
     * @param yearScope		数据范围（年）
     * @param fullData
     * @throws Exception
     */
    void syncZhiXing(String custNo, Integer yearScope, QccResultFullDatailDto fullData) throws Exception;
    
    
    /**************************************  同步企业工商信息  结束*****************************************************/


    /**
     * 同步企业裁判文书信息
     * @param custNo
     * @param judgList
     * @throws Exception
     */
    void syncJudgmentDoc(String custNo) throws Exception;
    

    /**
     * 同步企业对外投资信息
     * @param custNo
     * @param judgList
     * @throws Exception
     */
    void syncInvestmentList(String custNo) throws Exception;

    
    /**********************************   碧湾云平台     **************/
    /** 
     * 获取云平台  接口是否过期
     * @return
     */
    boolean getAnalyFlag() ;
}
