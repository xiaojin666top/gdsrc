package com.beawan.qcc.dto;

import com.google.gson.annotations.SerializedName;

/***
 * 裁判文书当事人
 * @author yuzhejia
 *
 */
public class CaseRoleDto {

    @SerializedName("P")
	private String person;//当事人名称

    @SerializedName("R")
	private String role;//角色

    @SerializedName("N")
	private String keyNo;//当事人id

    @SerializedName("O")
	private Integer o;//不知道是什么

	public String getPerson() {
		return person;
	}

	public void setPerson(String person) {
		this.person = person;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getKeyNo() {
		return keyNo;
	}

	public void setKeyNo(String keyNo) {
		this.keyNo = keyNo;
	}

	public Integer getO() {
		return o;
	}

	public void setO(Integer o) {
		this.o = o;
	}
    
    
	
	

}
