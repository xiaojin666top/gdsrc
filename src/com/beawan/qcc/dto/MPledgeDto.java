package com.beawan.qcc.dto;

import com.google.gson.annotations.SerializedName;

/**
 * 动产抵押
 */
public class MPledgeDto {

    @SerializedName("PublicDate")
    private String publicDate;//公示时间
    @SerializedName("RegisterNo")
    private String registerNo;//登记编号
    @SerializedName("RegisterDate")
    private String registerDate;//登记时间
    @SerializedName("RegisterOffice")
    private String registerOffice;//登记机关
    @SerializedName("DebtSecuredAmount")
    private String debtSecuredAmount;//被担保债权数额
    @SerializedName("Status")
    private String status;//状态

    public String getPublicDate() {
        return publicDate;
    }

    public void setPublicDate(String publicDate) {
        this.publicDate = publicDate;
    }

    public String getRegisterNo() {
        return registerNo;
    }

    public void setRegisterNo(String registerNo) {
        this.registerNo = registerNo;
    }

    public String getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    public String getRegisterOffice() {
        return registerOffice;
    }

    public void setRegisterOffice(String registerOffice) {
        this.registerOffice = registerOffice;
    }

    public String getDebtSecuredAmount() {
        return debtSecuredAmount;
    }

    public void setDebtSecuredAmount(String debtSecuredAmount) {
        this.debtSecuredAmount = debtSecuredAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
