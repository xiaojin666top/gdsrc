package com.beawan.qcc.dto;

import com.google.gson.annotations.SerializedName;

/**
 * 执行
 */
public class ZhiXingItemsDto {

    @SerializedName("Id")
    private String id;
    @SerializedName("Updatedate")
    private String updatedate;//数据更新时间
    @SerializedName("PartyCardNum")
    private String partyCardNum;//身份证号码/组织机构代码
    @SerializedName("Anno")
    private String anno;//立案号
    @SerializedName("Name")
    private String name;//名称
    @SerializedName("Status")
    private String state;//状态
    @SerializedName("Sourceid")
    private String sourceid;//官网系统id
    @SerializedName("ExecuteGov")
    private String executeGov;//执行法院
    @SerializedName("Liandate")
    private String liandate;//立案时间
    @SerializedName("Biaodi")
    private String biaodi;//标地

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(String updatedate) {
        this.updatedate = updatedate;
    }

    public String getPartyCardNum() {
        return partyCardNum;
    }

    public void setPartyCardNum(String partyCardNum) {
        this.partyCardNum = partyCardNum;
    }

    public String getAnno() {
        return anno;
    }

    public void setAnno(String anno) {
        this.anno = anno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getSourceid() {
        return sourceid;
    }

    public void setSourceid(String sourceid) {
        this.sourceid = sourceid;
    }

    public String getExecuteGov() {
        return executeGov;
    }

    public void setExecuteGov(String executeGov) {
        this.executeGov = executeGov;
    }

    public String getLiandate() {
        return liandate;
    }

    public void setLiandate(String liandate) {
        this.liandate = liandate;
    }

    public String getBiaodi() {
        return biaodi;
    }

    public void setBiaodi(String biaodi) {
        this.biaodi = biaodi;
    }
}
