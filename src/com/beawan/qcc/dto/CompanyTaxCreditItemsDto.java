package com.beawan.qcc.dto;


import com.google.gson.annotations.SerializedName;

/**
 * 纳税信用等级
 */
public class CompanyTaxCreditItemsDto {


    @SerializedName("Name")
    private String name;//纳税人名称
    @SerializedName("No")
    private String no;//纳税人识别号
    @SerializedName("Level")
    private String level;//信用等级
    @SerializedName("Year")
    private String year;//评价年度

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
