package com.beawan.qcc.dto;

import com.google.gson.annotations.SerializedName;

/**
 * 企查查接口带分页对象
 * @author User
 *
 */
public class QccPagingDto {


    @SerializedName("PageSize")
	private int pageSize;
    @SerializedName("PageIndex")
	private int pageIndex;
    @SerializedName("TotalRecords")
	private int totalRecords;
    
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getPageIndex() {
		return pageIndex;
	}
	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}
	public int getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}
    
    
}
