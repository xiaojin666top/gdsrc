package com.beawan.qcc.dto;

import com.google.gson.annotations.SerializedName;

/**
 * 区域
 */
public class AreaDto {

    @SerializedName("Province")
    private String province;//省份
    @SerializedName("City")
    private String city;//城市
    @SerializedName("County")
    private String county;//区域

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }
}
