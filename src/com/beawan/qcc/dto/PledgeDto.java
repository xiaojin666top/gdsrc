package com.beawan.qcc.dto;


import com.google.gson.annotations.SerializedName;

/**
 * 股权出质
 */
public class PledgeDto {

    @SerializedName("CompanyId")
    private String companyId;//

    @SerializedName("PledgeeNo")
    private String pledgeeNo;//质权人证照编号
    @SerializedName("PublicDate")
    private String publicDate;//公示时间
    @SerializedName("Status")
    private String status;//出质状态
    @SerializedName("PledgorNo")
    private String pledgorNo;//出质人证照编号
    @SerializedName("Pledgee")
    private String pledgee;//质权人
    @SerializedName("PledgedAmount")
    private String pledgedAmount;//出质股权数额
    @SerializedName("RegDate")
    private String regDate;//股权出质设立登记日期
    @SerializedName("Pledgor")
    private String pledgor;//出质人
    @SerializedName("RegistNo")
    private String registNo;//质权登记编号

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getPledgeeNo() {
        return pledgeeNo;
    }

    public void setPledgeeNo(String pledgeeNo) {
        this.pledgeeNo = pledgeeNo;
    }

    public String getPublicDate() {
        return publicDate;
    }

    public void setPublicDate(String publicDate) {
        this.publicDate = publicDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPledgorNo() {
        return pledgorNo;
    }

    public void setPledgorNo(String pledgorNo) {
        this.pledgorNo = pledgorNo;
    }

    public String getPledgee() {
        return pledgee;
    }

    public void setPledgee(String pledgee) {
        this.pledgee = pledgee;
    }

    public String getPledgedAmount() {
        return pledgedAmount;
    }

    public void setPledgedAmount(String pledgedAmount) {
        this.pledgedAmount = pledgedAmount;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getPledgor() {
        return pledgor;
    }

    public void setPledgor(String pledgor) {
        this.pledgor = pledgor;
    }

    public String getRegistNo() {
        return registNo;
    }

    public void setRegistNo(String registNo) {
        this.registNo = registNo;
    }
}
