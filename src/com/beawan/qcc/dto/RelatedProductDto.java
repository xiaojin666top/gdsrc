package com.beawan.qcc.dto;


import com.google.gson.annotations.SerializedName;

/**
 * 关联产品
 */
public class RelatedProductDto {

    @SerializedName("Id")
    private String id;//
    @SerializedName("Round")
    private String round;//融资轮次
    @SerializedName("FinancingCount")
    private String financingCount;//融资次数
    @SerializedName("Name")
    private String name;//产品名称

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    public String getFinancingCount() {
        return financingCount;
    }

    public void setFinancingCount(String financingCount) {
        this.financingCount = financingCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
