package com.beawan.qcc.dto;


import com.google.gson.annotations.SerializedName;

/**
 * 关联机构
 */
public class RelatedOrgDto {

    @SerializedName("Id")
    private String id;
    @SerializedName("Name")
    private String name;//机构名称


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
