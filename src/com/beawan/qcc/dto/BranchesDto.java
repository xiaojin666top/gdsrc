package com.beawan.qcc.dto;


import com.google.gson.annotations.SerializedName;

/**
 * 分支机构
 */
public class BranchesDto {

    @SerializedName("Name")
    private String name;//名称
    @SerializedName("CreditCode")
    private String creditCode;//社会统一信用代码( 保留字段)
    @SerializedName("RegNo")
    private String regNo;//注册号
    @SerializedName("BelongOrg")
    private String belongOrg;//登记机关
    @SerializedName("CompanyId")
    private String companyId;//公司 KeyNo
    @SerializedName("OperName")
    private String operName;//法人姓名（保留字段）

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreditCode() {
        return creditCode;
    }

    public void setCreditCode(String creditCode) {
        this.creditCode = creditCode;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getBelongOrg() {
        return belongOrg;
    }

    public void setBelongOrg(String belongOrg) {
        this.belongOrg = belongOrg;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getOperName() {
        return operName;
    }

    public void setOperName(String operName) {
        this.operName = operName;
    }
}
