package com.beawan.qcc.dto;


import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * 联系信息
 */
public class ContactInfoDto {

    @SerializedName("WebSite")
    private List<WebSiteDto> webSite;
    @SerializedName("Email")
    private String email;//邮箱
    @SerializedName("PhoneNumber")
    private String phoneNumber;//联系电话

    public List<WebSiteDto> getWebSite() {
        return webSite;
    }

    public void setWebSite(List<WebSiteDto> webSite) {
        this.webSite = webSite;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
