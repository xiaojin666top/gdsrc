package com.beawan.qcc.dto;


import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * 企查查返回工商数据全部报文数据接口--》所有企查查的数据都在这个类里面
 */
public class QccResultFullDatailDto {

    @SerializedName("Partners")
    private List<PartnersDto> partners;//投资人及出资信息
    //企业类型 0-企业（包括个体工商户），1-社会组织 ，3-香港公司，4-政府机构，
    //5-台湾公司，6-基金会，7-医院，8-海外公司，9-律师事务所，10-学 校 ，-1-其他

    @SerializedName("EntType")
    private String entType;
    @SerializedName("EconKindCodeList")
    private List<String> econKindCodeList;//企业类型数组(具体枚举值请查看附件)
    @SerializedName("BelongOrg")
    private String belondOrg;//登记机关
    @SerializedName("No")
    private String no;//注册号
    @SerializedName("Province")
    private String province;//省份
    @SerializedName("Exceptions")
    private List<ExceptionsDto> exceptions;//经营异常
    @SerializedName("RecCap")
    private String recCap;//实缴资本
    @SerializedName("Area")
    private AreaDto area;//区域
    @SerializedName("Scope")
    private String scope;//营业范围
    @SerializedName("IsOnStock")
    private String isOnStock;//是否上市
    @SerializedName("Status")
    private String status;//状态
    @SerializedName("Industry")
    private IndustryDto industry;//行业分类数据
    @SerializedName("MPledge")
    private List<MPledgeDto> mPledge;//动产抵押
    @SerializedName("CheckDate")
    private String checkDate;//发照日期
    @SerializedName("Liquidation")
    private LiquidationDto liquidation;//清算
    @SerializedName("TeamEnd")
    private String teamEnd;//营业期限至
    @SerializedName("ContactInfo")
    private ContactInfoDto contactInfo;//联系信息
    @SerializedName("Name")
    private String name;//公司名称
    @SerializedName("OperName")
    private String operName;//法人
    @SerializedName("Penalty")
    private List<PenaltyDto> penalty;//行政处罚
    @SerializedName("RegistCapi")
    private String registCapi;//注册资本
    @SerializedName("StockNumber")
    private String stockNumber;//证券号
    @SerializedName("PenaltyCreditChina")
    private List<PenaltyCreditChinaDto> penaltyCreditChina;//行政处罚（信用中国）
    @SerializedName("ShiXinItems")
    private List<ShiXinItemsDto> shiXinItems;//失信
    @SerializedName("CompanyProducts")
    private List<CompanyProductsDto> companyProducts;//公司产品
    @SerializedName("StockType")
    private String stockType;//证券类型
    @SerializedName("EndDate")
    private String endDate;//吊销日期
    @SerializedName("ZhiXingItems")
    private List<ZhiXingItemsDto> zhiXingItems;//执行
    @SerializedName("TermStart")
    private String termStart;//营销期限始
    @SerializedName("StartDate")
    private String startDate;//成立时间
    @SerializedName("InsuredCount")
    private String insuredCount;//参保人数
    @SerializedName("SpotCheck")
    private List<SpotCheckDto> spotCheck;//抽查检查
    @SerializedName("ImageUrl")
    private String imageUrl;//logo地址
    @SerializedName("CompanyTaxCreditItems")
    private List<CompanyTaxCreditItemsDto> companyTaxCreditItems;//纳税信用等级
    @SerializedName("PermissionInfo")
    private List<PermissionInfoDto> permissionInfo;//行政许可【信用中国】
    @SerializedName("KeyNo")
    private String keyNo;//根据此字段获取详细信息
    @SerializedName("OriginalName")
    private List<OriginalNameDto> originalName;//曾用名
    @SerializedName("ChangeRecords")
    private List<ChangeRecordsDto> changeRecords;//变更信息
    @SerializedName("PermissionEciInfo")
    private List<PermissionEciInfoDto> permissionEciInfo;//行政许可【工商局】
    @SerializedName("EconKind")
    private String econKind;//类型
    @SerializedName("TagList")
    private List<TagListDto> tagList;//标签列表
    @SerializedName("Branches")
    private List<BranchesDto> branches;//分支机构
    @SerializedName("PersonScope")
    private String persionScope;//人员规模
    @SerializedName("OrgNo")
    private String orgNo;//组织机构代码
    @SerializedName("Address")
    private String address;//地址
    @SerializedName("Pledge")
    private List<PledgeDto> pledge;//股权出质
    @SerializedName("EnglishName")
    private String englistName;//英文名
    @SerializedName("ARContactList")
    private List<ARContactListDto> aRContactList;//最新企业年报中的联系方式
    @SerializedName("UpdatedDate")
    private String updateDate;//更新日期
    @SerializedName("Employees")
    private List<EmployeesDto> employees;//主要人员
    @SerializedName("CreditCode")
    private String creditCode;//信用代码

    public List<PartnersDto> getPartners() {
        return partners;
    }

    public void setPartners(List<PartnersDto> partners) {
        this.partners = partners;
    }

    public String getEntType() {
        return entType;
    }

    public void setEntType(String entType) {
        this.entType = entType;
    }

    public List<String> getEconKindCodeList() {
        return econKindCodeList;
    }

    public void setEconKindCodeList(List<String> econKindCodeList) {
        this.econKindCodeList = econKindCodeList;
    }

    public String getBelondOrg() {
        return belondOrg;
    }

    public void setBelondOrg(String belondOrg) {
        this.belondOrg = belondOrg;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public List<ExceptionsDto> getExceptions() {
        return exceptions;
    }

    public void setExceptions(List<ExceptionsDto> exceptions) {
        this.exceptions = exceptions;
    }

    public String getRecCap() {
        return recCap;
    }

    public void setRecCap(String recCap) {
        this.recCap = recCap;
    }

    public AreaDto getArea() {
        return area;
    }

    public void setArea(AreaDto area) {
        this.area = area;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getIsOnStock() {
        return isOnStock;
    }

    public void setIsOnStock(String isOnStock) {
        this.isOnStock = isOnStock;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public IndustryDto getIndustry() {
        return industry;
    }

    public void setIndustry(IndustryDto industry) {
        this.industry = industry;
    }

    public List<MPledgeDto> getmPledge() {
        return mPledge;
    }

    public void setmPledge(List<MPledgeDto> mPledge) {
        this.mPledge = mPledge;
    }

    public String getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(String checkDate) {
        this.checkDate = checkDate;
    }

    public LiquidationDto getLiquidation() {
        return liquidation;
    }

    public void setLiquidation(LiquidationDto liquidation) {
        this.liquidation = liquidation;
    }

    public String getTeamEnd() {
        return teamEnd;
    }

    public void setTeamEnd(String teamEnd) {
        this.teamEnd = teamEnd;
    }

    public ContactInfoDto getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(ContactInfoDto contactInfo) {
        this.contactInfo = contactInfo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOperName() {
        return operName;
    }

    public void setOperName(String operName) {
        this.operName = operName;
    }

    public List<PenaltyDto> getPenalty() {
        return penalty;
    }

    public void setPenalty(List<PenaltyDto> penalty) {
        this.penalty = penalty;
    }

    public String getRegistCapi() {
        return registCapi;
    }

    public void setRegistCapi(String registCapi) {
        this.registCapi = registCapi;
    }

    public String getStockNumber() {
        return stockNumber;
    }

    public void setStockNumber(String stockNumber) {
        this.stockNumber = stockNumber;
    }

    public List<PenaltyCreditChinaDto> getPenaltyCreditChina() {
        return penaltyCreditChina;
    }

    public void setPenaltyCreditChina(List<PenaltyCreditChinaDto> penaltyCreditChina) {
        this.penaltyCreditChina = penaltyCreditChina;
    }

    public List<ShiXinItemsDto> getShiXinItems() {
        return shiXinItems;
    }

    public void setShiXinItems(List<ShiXinItemsDto> shiXinItems) {
        this.shiXinItems = shiXinItems;
    }

    public List<CompanyProductsDto> getCompanyProducts() {
        return companyProducts;
    }

    public void setCompanyProducts(List<CompanyProductsDto> companyProducts) {
        this.companyProducts = companyProducts;
    }

    public String getStockType() {
        return stockType;
    }

    public void setStockType(String stockType) {
        this.stockType = stockType;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public List<ZhiXingItemsDto> getZhiXingItems() {
        return zhiXingItems;
    }

    public void setZhiXingItems(List<ZhiXingItemsDto> zhiXingItems) {
        this.zhiXingItems = zhiXingItems;
    }

    public String getTermStart() {
        return termStart;
    }

    public void setTermStart(String termStart) {
        this.termStart = termStart;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getInsuredCount() {
        return insuredCount;
    }

    public void setInsuredCount(String insuredCount) {
        this.insuredCount = insuredCount;
    }

    public List<SpotCheckDto> getSpotCheck() {
        return spotCheck;
    }

    public void setSpotCheck(List<SpotCheckDto> spotCheck) {
        this.spotCheck = spotCheck;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<CompanyTaxCreditItemsDto> getCompanyTaxCreditItems() {
        return companyTaxCreditItems;
    }

    public void setCompanyTaxCreditItems(List<CompanyTaxCreditItemsDto> companyTaxCreditItems) {
        this.companyTaxCreditItems = companyTaxCreditItems;
    }

    public List<PermissionInfoDto> getPermissionInfo() {
        return permissionInfo;
    }

    public void setPermissionInfo(List<PermissionInfoDto> permissionInfo) {
        this.permissionInfo = permissionInfo;
    }

    public String getKeyNo() {
        return keyNo;
    }

    public void setKeyNo(String keyNo) {
        this.keyNo = keyNo;
    }

    public List<OriginalNameDto> getOriginalName() {
        return originalName;
    }

    public void setOriginalName(List<OriginalNameDto> originalName) {
        this.originalName = originalName;
    }

    public List<ChangeRecordsDto> getChangeRecords() {
        return changeRecords;
    }

    public void setChangeRecords(List<ChangeRecordsDto> changeRecords) {
        this.changeRecords = changeRecords;
    }

    public List<PermissionEciInfoDto> getPermissionEciInfo() {
        return permissionEciInfo;
    }

    public void setPermissionEciInfo(List<PermissionEciInfoDto> permissionEciInfo) {
        this.permissionEciInfo = permissionEciInfo;
    }

    public String getEconKind() {
        return econKind;
    }

    public void setEconKind(String econKind) {
        this.econKind = econKind;
    }

    public List<TagListDto> getTagList() {
        return tagList;
    }

    public void setTagList(List<TagListDto> tagList) {
        this.tagList = tagList;
    }

    public List<BranchesDto> getBranches() {
        return branches;
    }

    public void setBranches(List<BranchesDto> branches) {
        this.branches = branches;
    }

    public String getPersionScope() {
        return persionScope;
    }

    public void setPersionScope(String persionScope) {
        this.persionScope = persionScope;
    }

    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<PledgeDto> getPledge() {
        return pledge;
    }

    public void setPledge(List<PledgeDto> pledge) {
        this.pledge = pledge;
    }

    public String getEnglistName() {
        return englistName;
    }

    public void setEnglistName(String englistName) {
        this.englistName = englistName;
    }

    public List<ARContactListDto> getaRContactList() {
        return aRContactList;
    }

    public void setaRContactList(List<ARContactListDto> aRContactList) {
        this.aRContactList = aRContactList;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public List<EmployeesDto> getEmployees() {
        return employees;
    }

    public void setEmployees(List<EmployeesDto> employees) {
        this.employees = employees;
    }

    public String getCreditCode() {
        return creditCode;
    }

    public void setCreditCode(String creditCode) {
        this.creditCode = creditCode;
    }
}
