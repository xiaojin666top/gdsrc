package com.beawan.qcc.dto;

import java.util.List;

/**
 * 受益所有人详情包含的元素说明
 * @author yuzhejia
 *
 */
public class BreakThroughDto {

	private String name;//受益所有人名称
	private String keyNo;//受益所有人标识KeyNo
	private String totalStockPercent;//受益所有人穿透总持股比例
	private List<DetailInfoDto> detailInfoList;//受益所有人穿透的详细信息
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getKeyNo() {
		return keyNo;
	}
	public void setKeyNo(String keyNo) {
		this.keyNo = keyNo;
	}
	public String getTotalStockPercent() {
		return totalStockPercent;
	}
	public void setTotalStockPercent(String totalStockPercent) {
		this.totalStockPercent = totalStockPercent;
	}
	public List<DetailInfoDto> getDetailInfoList() {
		return detailInfoList;
	}
	public void setDetailInfoList(List<DetailInfoDto> detailInfoList) {
		this.detailInfoList = detailInfoList;
	}
	
	
}
