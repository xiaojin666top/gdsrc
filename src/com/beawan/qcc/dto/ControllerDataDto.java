package com.beawan.qcc.dto;

import java.util.List;

import com.google.gson.annotations.SerializedName;

/***
 * 疑似实际控股人分析数据
 * @author yuzhejia
 *
 */
public class ControllerDataDto {


    @SerializedName("KeyNo")
	private String keyNo;//公司keyno
    @SerializedName("Name")
	private String name;//公司名称
    @SerializedName("Percent")
	private String percent;//出资比例
    @SerializedName("PercentTotal")
	private String percentTotal;//总出资比例
    @SerializedName("Level")
	private String level;//层级，多个层级以“，”相间隔
    @SerializedName("PathCount")
	private Integer pathCount;//层级数量
    @SerializedName("Paths")
	private List<List<PathsDto>> paths;//路径
//    @SerializedName("PersonList")
//	private PersonDto personList;//控制人信息
    
    
}
