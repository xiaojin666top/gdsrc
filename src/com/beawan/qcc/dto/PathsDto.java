package com.beawan.qcc.dto;

import com.google.gson.annotations.SerializedName;

/**
 * 路径
 * @author yuzhejia
 *
 */
public class PathsDto {


    @SerializedName("KeyNo")
	private String keyNo;//内部keyno

    @SerializedName("Name")
	private String name;//名称

    @SerializedName("Percent")
	private String percent;//出资比例

    @SerializedName("PercentTotal")
	private String percentTotal;//总出资比例

    @SerializedName("Level")
	private String level;//层级

	public String getKeyNo() {
		return keyNo;
	}

	public void setKeyNo(String keyNo) {
		this.keyNo = keyNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPercent() {
		return percent;
	}

	public void setPercent(String percent) {
		this.percent = percent;
	}

	public String getPercentTotal() {
		return percentTotal;
	}

	public void setPercentTotal(String percentTotal) {
		this.percentTotal = percentTotal;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}
    
    
}
