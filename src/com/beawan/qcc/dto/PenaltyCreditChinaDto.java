package com.beawan.qcc.dto;


import com.google.gson.annotations.SerializedName;

/**
 * 行政处罚（信用中国）
 */
public class PenaltyCreditChinaDto {

    @SerializedName("Liandate")
    private String liandate;//决定日期
    @SerializedName("CaseNo")
    private String caseNo;//决定文书号
    @SerializedName("Province")
    private String province;//地域
    @SerializedName("CaseReason")
    private String caseReason;//处罚事由
    @SerializedName("OwnerName")
    private String ownerName;//所属人
    @SerializedName("Name")
    private String name;//处罚名称

    public String getLiandate() {
        return liandate;
    }

    public void setLiandate(String liandate) {
        this.liandate = liandate;
    }

    public String getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCaseReason() {
        return caseReason;
    }

    public void setCaseReason(String caseReason) {
        this.caseReason = caseReason;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
