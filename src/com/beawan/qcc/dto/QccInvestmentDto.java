package com.beawan.qcc.dto;

import com.google.gson.annotations.SerializedName;

/**
 * 企查查对外投资列表
 * @author User
 *
 */
public class QccInvestmentDto {


    @SerializedName("KeyNo")
    private String keyNo;//KeyNo
    @SerializedName("CreditCode")
    private String creditCode;//社会统一信用代码
    @SerializedName("EconKind")
    private String econKind;//企业类型
    @SerializedName("FundedRatio")
    private String fundedRatio;//出资比列
    @SerializedName("ImageUrl")
    private String imageUrl;//公司Logo
    @SerializedName("Name")
    private String name;//企业名称
    @SerializedName("No")
    private String no;//注册号
    @SerializedName("OperName")
    private String operName;//法人名称
    @SerializedName("RegistCapi")
    private String registCapi;//注册资本
    @SerializedName("StartDate")
    private String startDate;//社成立日期
    @SerializedName("Status")
    private String state;//状态
	public String getCreditCode() {
		return creditCode;
	}
	public void setCreditCode(String creditCode) {
		this.creditCode = creditCode;
	}
	public String getEconKind() {
		return econKind;
	}
	public void setEconKind(String econKind) {
		this.econKind = econKind;
	}
	public String getFundedRatio() {
		return fundedRatio;
	}
	public void setFundedRatio(String fundedRatio) {
		this.fundedRatio = fundedRatio;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getKeyNo() {
		return keyNo;
	}
	public void setKeyNo(String keyNo) {
		this.keyNo = keyNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getOperName() {
		return operName;
	}
	public void setOperName(String operName) {
		this.operName = operName;
	}
	public String getRegistCapi() {
		return registCapi;
	}
	public void setRegistCapi(String registCapi) {
		this.registCapi = registCapi;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
    
    
    
    
}
