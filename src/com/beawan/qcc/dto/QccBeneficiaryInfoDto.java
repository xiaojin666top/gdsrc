package com.beawan.qcc.dto;

import java.util.List;

/**|
 * 企业最终受益人
 * @author User
 *
 */
public class QccBeneficiaryInfoDto {

	private String keyNo;	//公司标识KeyNo
	private String companyName;//公司名称
	private String findMatched;//是否存在直接间接控股的受益人，Y-存在，N-不存在
	private String operName;
	private String remark;//备注
	private List<BreakThroughDto> breakThroughList;//受益所有人列表
	private List<ExecutiveDto> executives;//高级管理人员列表，只在最终受益所有人为高级管理人员时返回
	
	public String getKeyNo() {
		return keyNo;
	}
	public void setKeyNo(String keyNo) {
		this.keyNo = keyNo;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getFindMatched() {
		return findMatched;
	}
	public void setFindMatched(String findMatched) {
		this.findMatched = findMatched;
	}
	public String getOperName() {
		return operName;
	}
	public void setOperName(String operName) {
		this.operName = operName;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public List<BreakThroughDto> getBreakThroughList() {
		return breakThroughList;
	}
	public void setBreakThroughList(List<BreakThroughDto> breakThroughList) {
		this.breakThroughList = breakThroughList;
	}
	public List<ExecutiveDto> getExecutives() {
		return executives;
	}
	public void setExecutives(List<ExecutiveDto> executives) {
		this.executives = executives;
	}
	
	
	
}
