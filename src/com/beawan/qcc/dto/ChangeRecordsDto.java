package com.beawan.qcc.dto;


import com.google.gson.annotations.SerializedName;

/**
 * 变更信息
 */
public class ChangeRecordsDto {

    @SerializedName("ChangeDate")
    private String changeDate;//变更日期
    @SerializedName("AfterContent")
    private String afterContent;//变更后内容
    @SerializedName("BeforeContent")
    private String beforeContent;//变更前内容
    @SerializedName("ProjectName")
    private String projectName;//变更事项

    public String getChangeDate() {
        return changeDate;
    }

    public void setChangeDate(String changeDate) {
        this.changeDate = changeDate;
    }

    public String getAfterContent() {
        return afterContent;
    }

    public void setAfterContent(String afterContent) {
        this.afterContent = afterContent;
    }

    public String getBeforeContent() {
        return beforeContent;
    }

    public void setBeforeContent(String beforeContent) {
        this.beforeContent = beforeContent;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
