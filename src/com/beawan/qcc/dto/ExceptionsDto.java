package com.beawan.qcc.dto;


import com.google.gson.annotations.SerializedName;

/**
 * 经营异常
 */
public class ExceptionsDto {

    @SerializedName("RomoveReason")
    private String romoveReason;//移出经营异常名录原因
    @SerializedName("RemoveDate")
    private String removeDate;//移出日期
    @SerializedName("AddReason")
    private String addReason;//列入经营异常名录原因
    @SerializedName("RemoveDecisionOffice")
    private String removeDecisionOffice;//移除决定机关
    @SerializedName("AddDate")
    private String addDate;//列入日期
    @SerializedName("DecisionOffice")
    private String decisionOffice;//作出决定机关

    public String getRomoveReason() {
        return romoveReason;
    }

    public void setRomoveReason(String romoveReason) {
        this.romoveReason = romoveReason;
    }

    public String getRemoveDate() {
        return removeDate;
    }

    public void setRemoveDate(String removeDate) {
        this.removeDate = removeDate;
    }

    public String getAddReason() {
        return addReason;
    }

    public void setAddReason(String addReason) {
        this.addReason = addReason;
    }

    public String getRemoveDecisionOffice() {
        return removeDecisionOffice;
    }

    public void setRemoveDecisionOffice(String removeDecisionOffice) {
        this.removeDecisionOffice = removeDecisionOffice;
    }

    public String getAddDate() {
        return addDate;
    }

    public void setAddDate(String addDate) {
        this.addDate = addDate;
    }

    public String getDecisionOffice() {
        return decisionOffice;
    }

    public void setDecisionOffice(String decisionOffice) {
        this.decisionOffice = decisionOffice;
    }
}
