package com.beawan.qcc.dto;


import com.google.gson.annotations.SerializedName;

/**
 * 曾用名
 */
public class OriginalNameDto {

    @SerializedName("ChangeDate")
    private String changeDate;//变更日期
    @SerializedName("Name")
    private String name;//名称

    public String getChangeDate() {
        return changeDate;
    }

    public void setChangeDate(String changeDate) {
        this.changeDate = changeDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
