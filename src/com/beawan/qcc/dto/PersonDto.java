package com.beawan.qcc.dto;

import com.google.gson.annotations.SerializedName;

/**
 * 控制人信息
 * @author yuzhejia
 *
 */
public class PersonDto {


    @SerializedName("Org")
	private Integer org;//企业类型

    @SerializedName("Name")
	private String name;//名称
    
    @SerializedName("KeyNo")
	private String keyNo;//内部keyno

	public Integer getOrg() {
		return org;
	}

	public void setOrg(Integer org) {
		this.org = org;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKeyNo() {
		return keyNo;
	}

	public void setKeyNo(String keyNo) {
		this.keyNo = keyNo;
	}

    
    
}
