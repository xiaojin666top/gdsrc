package com.beawan.qcc.dto;


import com.google.gson.annotations.SerializedName;

/**
 * 最新企业年报中的联系方式
 */
public class ARContactListDto {

    @SerializedName("Address")
    private String address;//企业通讯地址
    @SerializedName("ContactNo")
    private String contactNo;//企业联系电话
    @SerializedName("EmailAddress")
    private String emailAddress;//电子邮箱

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
