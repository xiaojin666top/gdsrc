package com.beawan.qcc.dto;

import java.util.List;

import com.google.gson.annotations.SerializedName;

/***
 * 裁判文书详情  关联开庭公告
 * @author yuzhejia
 *
 */
public class CourtNoticeDto {

	@SerializedName("TotalNum")
	private String totalNum;//关联开庭公告总条目数
	@SerializedName("CourtNoticeInfo")
	private List<CourtNoticeInfoDto> courtNoticeInfo;//CourtNoticeInfo 列表字段
	
	
	public String getTotalNum() {
		return totalNum;
	}
	public void setTotalNum(String totalNum) {
		this.totalNum = totalNum;
	}
	public List<CourtNoticeInfoDto> getCourtNoticeInfo() {
		return courtNoticeInfo;
	}
	public void setCourtNoticeInfo(List<CourtNoticeInfoDto> courtNoticeInfo) {
		this.courtNoticeInfo = courtNoticeInfo;
	}

	
}
