package com.beawan.qcc.dto;

/***
 * 受益所有人穿透的详细信息包含的元素说明
 * @author yuzhejia
 *
 */
public class DetailInfoDto {

	
	private Integer level;//层级
	private Double shouldCapi;//认缴出资额(万)
	private String capitalType;//出资类型
	private String breakthroughStockPercent;//当前穿透持股比例
	private String stockType;//持股方式
	private String path;//层级关系
	private String stockPercent;//持股比例
	
	
	public Integer getLevel() {
		return level;
	}
	public void setLevel(Integer level) {
		this.level = level;
	}
	public Double getShouldCapi() {
		return shouldCapi;
	}
	public void setShouldCapi(Double shouldCapi) {
		this.shouldCapi = shouldCapi;
	}
	public String getCapitalType() {
		return capitalType;
	}
	public void setCapitalType(String capitalType) {
		this.capitalType = capitalType;
	}
	public String getBreakthroughStockPercent() {
		return breakthroughStockPercent;
	}
	public void setBreakthroughStockPercent(String breakthroughStockPercent) {
		this.breakthroughStockPercent = breakthroughStockPercent;
	}
	public String getStockType() {
		return stockType;
	}
	public void setStockType(String stockType) {
		this.stockType = stockType;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getStockPercent() {
		return stockPercent;
	}
	public void setStockPercent(String stockPercent) {
		this.stockPercent = stockPercent;
	}
	
	
}
