package com.beawan.qcc.dto;

import com.google.gson.annotations.SerializedName;

/**
 *
 */
public class TagListDto {

    @SerializedName("Name")
    private String name;//标签列表
    @SerializedName("Type")
    private String type;//类型

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
