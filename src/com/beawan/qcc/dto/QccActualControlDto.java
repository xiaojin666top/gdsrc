package com.beawan.qcc.dto;

import com.google.gson.annotations.SerializedName;

/**
 * 企查查疑似实际控制人
 * @author ytuzhejia
 *
 */
public class QccActualControlDto {

	@SerializedName("KeyNo")
	private String keyNo;//公司keyno
	@SerializedName("CompanyName")
	private String companyName;//公司名称
	@SerializedName("UpdateTime")
	private String updateTime;//更新时间
	@SerializedName("ControllerData")
	private ControllerDataDto controllerData;//疑似实际控股人分析数据
	@SerializedName("ActualControl")
	private ActualControlDto actualControl;//公示的疑似实际控股人
	
	
	public String getKeyNo() {
		return keyNo;
	}
	public void setKeyNo(String keyNo) {
		this.keyNo = keyNo;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public ControllerDataDto getControllerData() {
		return controllerData;
	}
	public void setControllerData(ControllerDataDto controllerData) {
		this.controllerData = controllerData;
	}
	public ActualControlDto getActualControl() {
		return actualControl;
	}
	public void setActualControl(ActualControlDto actualControl) {
		this.actualControl = actualControl;
	}
	
	
	
}
