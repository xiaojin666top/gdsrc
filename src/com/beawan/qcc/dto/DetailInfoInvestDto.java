package com.beawan.qcc.dto;

import com.google.gson.annotations.SerializedName;

/***
 * 对外投资穿透的详细信息包含的元素说明
 * 妈的也是和受益所有人穿透数据结构一样  但是字段开头字母是大写，所以不能复用   狗币企查查
 * @author yuzhejia
 *
 */
public class DetailInfoInvestDto {


	@SerializedName("Level")
	private String level;//层级
	@SerializedName("ShouldCapi")
	private String shouldCapi;//认缴出资额(万)
	@SerializedName("CapitalType")
	private String capitalType;//出资类型
	@SerializedName("BreakThroughStockPercent")
	private String breakthroughStockPercent;//当前穿透持股比例
	@SerializedName("StockType")
	private String stockType;//持股方式
	@SerializedName("Path")
	private String path;//层级关系
	@SerializedName("StockPercent")
	private String stockPercent;//持股比例
	
	
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getShouldCapi() {
		return shouldCapi;
	}
	public void setShouldCapi(String shouldCapi) {
		this.shouldCapi = shouldCapi;
	}
	public String getCapitalType() {
		return capitalType;
	}
	public void setCapitalType(String capitalType) {
		this.capitalType = capitalType;
	}
	public String getBreakthroughStockPercent() {
		return breakthroughStockPercent;
	}
	public void setBreakthroughStockPercent(String breakthroughStockPercent) {
		this.breakthroughStockPercent = breakthroughStockPercent;
	}
	public String getStockType() {
		return stockType;
	}
	public void setStockType(String stockType) {
		this.stockType = stockType;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getStockPercent() {
		return stockPercent;
	}
	public void setStockPercent(String stockPercent) {
		this.stockPercent = stockPercent;
	}
	
	
	
}
