package com.beawan.qcc.dto;


import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * 投资人及出资信息
 */
public class PartnersDto {

    @SerializedName("KeyNo")
    private String keyNo;//keyno
    @SerializedName("StockPercent")
    private String stockPercent;//出资比例
    @SerializedName("StockName")
    private String stockName;//投资人
    @SerializedName("InvestType")
    private String investType;//认出资方式
    @SerializedName("CapiDate")
    private String capiDate;//实缴时间
    @SerializedName("ShoudDate")
    private String shoudDate;//认缴出资时间
    @SerializedName("TagsList")
    private List<String> tagsList;//股东标签数组
    @SerializedName("RelatedOrg")
    private RelatedOrgDto relatedOrg;//关联机构
    @SerializedName("RelatedProduct")
    private RelatedProductDto relatedProduct;//关联产品
    @SerializedName("StockType")
    private String stockType;//投资人类型
    @SerializedName("ShouldCapi")
    private String shouldCapi;//认缴出资额
    @SerializedName("FinalBenefitPercent")
    private String finalBenefitPercent;//最终受益股份
    @SerializedName("InvestName")
    private String investName;//实出资方式
    @SerializedName("RealCapi")
    private String realCapi;//实缴出资额

    public String getKeyNo() {
        return keyNo;
    }

    public void setKeyNo(String keyNo) {
        this.keyNo = keyNo;
    }

    public String getStockPercent() {
        return stockPercent;
    }

    public void setStockPercent(String stockPercent) {
        this.stockPercent = stockPercent;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getInvestType() {
        return investType;
    }

    public void setInvestType(String investType) {
        this.investType = investType;
    }

    public String getCapiDate() {
        return capiDate;
    }

    public void setCapiDate(String capiDate) {
        this.capiDate = capiDate;
    }

    public String getShoudDate() {
        return shoudDate;
    }

    public void setShoudDate(String shoudDate) {
        this.shoudDate = shoudDate;
    }

    public List<String> getTagsList() {
        return tagsList;
    }

    public void setTagsList(List<String> tagsList) {
        this.tagsList = tagsList;
    }

    public RelatedOrgDto getRelatedOrg() {
        return relatedOrg;
    }

    public void setRelatedOrg(RelatedOrgDto relatedOrg) {
        this.relatedOrg = relatedOrg;
    }

    public RelatedProductDto getRelatedProduct() {
        return relatedProduct;
    }

    public void setRelatedProduct(RelatedProductDto relatedProduct) {
        this.relatedProduct = relatedProduct;
    }

    public String getStockType() {
        return stockType;
    }

    public void setStockType(String stockType) {
        this.stockType = stockType;
    }

    public String getShouldCapi() {
        return shouldCapi;
    }

    public void setShouldCapi(String shouldCapi) {
        this.shouldCapi = shouldCapi;
    }

    public String getFinalBenefitPercent() {
        return finalBenefitPercent;
    }

    public void setFinalBenefitPercent(String finalBenefitPercent) {
        this.finalBenefitPercent = finalBenefitPercent;
    }

    public String getInvestName() {
        return investName;
    }

    public void setInvestName(String investName) {
        this.investName = investName;
    }

    public String getRealCapi() {
        return realCapi;
    }

    public void setRealCapi(String realCapi) {
        this.realCapi = realCapi;
    }
}
