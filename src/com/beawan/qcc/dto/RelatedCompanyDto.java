package com.beawan.qcc.dto;

import com.google.gson.annotations.SerializedName;

/**
 * 裁判文书详情  关联公司列表
 * @author yuzhejia
 *
 */
public class RelatedCompanyDto {

	@SerializedName("KeyNo")
	private String keyNo;//公司KeyNo
	@SerializedName("Name")
	private String name;//公司名称
	
	
	public String getKeyNo() {
		return keyNo;
	}
	public void setKeyNo(String keyNo) {
		this.keyNo = keyNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	

}
