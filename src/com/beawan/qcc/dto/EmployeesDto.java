package com.beawan.qcc.dto;


import com.google.gson.annotations.SerializedName;

/**
 * 主要人员
 */
public class EmployeesDto {


    @SerializedName("Name")
    private String name;//名称
    @SerializedName("Job")
    private String job;//职位

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }
}
