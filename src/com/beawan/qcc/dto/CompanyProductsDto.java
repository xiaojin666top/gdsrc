package com.beawan.qcc.dto;


import com.google.gson.annotations.SerializedName;

/**
 * 公司产品
 */
public class CompanyProductsDto {

    @SerializedName("CompanyName")
    private String companyName;//企业名称
    @SerializedName("Domain")
    private String domain;//产品领域
    @SerializedName("Name")
    private String name;//产品名称
    @SerializedName("ImageUrl")
    private String imageUrl;//产品的图片
    @SerializedName("Link")
    private String link;//企业的关联链接
    @SerializedName("Category")
    private String category;//产品类型
    @SerializedName("Tags")
    private String tags;//产品标签
    @SerializedName("Description")
    private String description;//产品描述

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
