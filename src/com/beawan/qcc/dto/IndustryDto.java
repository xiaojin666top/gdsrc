package com.beawan.qcc.dto;


import com.google.gson.annotations.SerializedName;

/**
 * 行业分类数据
 */
public class IndustryDto {

    @SerializedName("Industry")
    private String industry;//行业门类描述
    @SerializedName("SubIndustry")
    private String subIndustry;//行业大类描述
    @SerializedName("SubIndustryCode")
    private String subIndustryCode;//行业大类 code
    @SerializedName("IndustryCode")
    private String industryCode;//行业门类 code
    @SerializedName("MiddleCategory")
    private String middleCategory;//行业中类描述
    @SerializedName("MiddleCategoryCode")
    private String middleCategoryCode;//行业中类 code
    @SerializedName("SmallCategory")
    private String smallCategory;//行业小类描述
    @SerializedName("SmallCategoryCode")
    private String smallCategoryCode;//行业小类 code

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getSubIndustry() {
        return subIndustry;
    }

    public void setSubIndustry(String subIndustry) {
        this.subIndustry = subIndustry;
    }

    public String getSubIndustryCode() {
        return subIndustryCode;
    }

    public void setSubIndustryCode(String subIndustryCode) {
        this.subIndustryCode = subIndustryCode;
    }

    public String getIndustryCode() {
        return industryCode;
    }

    public void setIndustryCode(String industryCode) {
        this.industryCode = industryCode;
    }

    public String getMiddleCategory() {
        return middleCategory;
    }

    public void setMiddleCategory(String middleCategory) {
        this.middleCategory = middleCategory;
    }

    public String getMiddleCategoryCode() {
        return middleCategoryCode;
    }

    public void setMiddleCategoryCode(String middleCategoryCode) {
        this.middleCategoryCode = middleCategoryCode;
    }

    public String getSmallCategory() {
        return smallCategory;
    }

    public void setSmallCategory(String smallCategory) {
        this.smallCategory = smallCategory;
    }

    public String getSmallCategoryCode() {
        return smallCategoryCode;
    }

    public void setSmallCategoryCode(String smallCategoryCode) {
        this.smallCategoryCode = smallCategoryCode;
    }
}
