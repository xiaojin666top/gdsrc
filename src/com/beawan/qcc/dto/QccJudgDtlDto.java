package com.beawan.qcc.dto;

import java.util.List;

import com.google.gson.annotations.SerializedName;

/***
 * 裁判文书详情
 * @author User
 *
 */
public class QccJudgDtlDto {

	@SerializedName("Id")
	private String id;
	@SerializedName("ContentClear")
	private String contentClear;//裁判文书内容（QCC 加工）
	@SerializedName("PlaintiffRequest")
	private String plaintiffRequest;//原告诉求(文书内容)
	@SerializedName("CaseName")
	private String caseName;//裁判文书名字
	@SerializedName("CaseNo")
	private String caseNo;//裁判文书编号
	@SerializedName("CaseType")
	private String caseType;//裁判文书类型
	@SerializedName("Content")
	private String content;//裁判文书内容
	@SerializedName("Court")
	private String court;//执行法院
	@SerializedName("CreateDate")
	private String createDate;//创建时间
	@SerializedName("SubmitDate")
	private String submitDate;//提交时间
	@SerializedName("UpdateDate")
	private String updateDate;//修改时间
	@SerializedName("Appellor")
	private List<String> appellor;//当事人
	@SerializedName("JudgeDate")
	private String judgeDate;//裁判时间
	@SerializedName("CaseReason")
	private String caseReason;//案由
	@SerializedName("TrialRound")
	private String trialRound;//审理程序
	@SerializedName("Defendantlist")
	private List<String> defendantlist;//被告
	@SerializedName("Prosecutorlist")
	private List<String> prosecutorlist;//原告
	@SerializedName("PlaintiffRequestOfFirst")
	private String plaintiffRequestOfFirst;//一审原告诉求(文书内容)
	@SerializedName("CourtConsider")
	private String courtConsider;//本院认为(文书内容)
	@SerializedName("AppellantRequest")
	private String appellantRequest;//上诉人诉求(文书内容)
	@SerializedName("DefendantReply")
	private String defendantReply;//被告答辩(文书内容)
	@SerializedName("AppelleeArguing")
	private String appelleeArguing;//被上诉人答辩(文书内容)
	@SerializedName("Recorder")
	private String recorder;//记录员(文书内容)
	@SerializedName("ExecuteProcess")
	private String executeProcess;//执行经过(文书内容)
	@SerializedName("DefendantReplyOfFirst")
	private String defendantReplyOfFirst;//一审被告答辩(文书内容)
	@SerializedName("TrialProcedure")
	private String trialProcedure;//审理经过(文书内容)
	@SerializedName("IsValid")
	private String IsValid;//是否有效，True 或false
	@SerializedName("CourtInspect")
	private String courtInspect;//本院查明(文书内容)
	@SerializedName("PartyInfo")
	private String partyInfo;//当事人(文书内容)
	@SerializedName("CourtInspectOfFirst")
	private String courtInspectOfFirst;//一审法院查明(文书内容)
	@SerializedName("CollegiateBench")
	private String collegiateBench;//合议庭(文书内容)
	@SerializedName("JudgeResult")
	private String judgeResult;//判决结果（文书内容）
	@SerializedName("CourtConsiderOfFirst")
	private String courtConsiderOfFirst;//一审法院认为(文书内容)
	@SerializedName("Judege_Date")
	private String judegeDate;//裁判日期(文书内容)
	@SerializedName("RelatedCompanies")
	private List<RelatedCompanyDto> relatedCompanies;//关联公司列表
	@SerializedName("CourtNoticeList")
	private CourtNoticeDto courtNoticeList;//关联开庭公告
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getContentClear() {
		return contentClear;
	}
	public void setContentClear(String contentClear) {
		this.contentClear = contentClear;
	}
	public String getPlaintiffRequest() {
		return plaintiffRequest;
	}
	public void setPlaintiffRequest(String plaintiffRequest) {
		this.plaintiffRequest = plaintiffRequest;
	}
	public String getCaseName() {
		return caseName;
	}
	public void setCaseName(String caseName) {
		this.caseName = caseName;
	}
	public String getCaseNo() {
		return caseNo;
	}
	public void setCaseNo(String caseNo) {
		this.caseNo = caseNo;
	}
	public String getCaseType() {
		return caseType;
	}
	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCourt() {
		return court;
	}
	public void setCourt(String court) {
		this.court = court;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getSubmitDate() {
		return submitDate;
	}
	public void setSubmitDate(String submitDate) {
		this.submitDate = submitDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	
	public List<String> getAppellor() {
		return appellor;
	}
	public void setAppellor(List<String> appellor) {
		this.appellor = appellor;
	}
	public String getJudgeDate() {
		return judgeDate;
	}
	public void setJudgeDate(String judgeDate) {
		this.judgeDate = judgeDate;
	}
	public String getCaseReason() {
		return caseReason;
	}
	public void setCaseReason(String caseReason) {
		this.caseReason = caseReason;
	}
	public String getTrialRound() {
		return trialRound;
	}
	public void setTrialRound(String trialRound) {
		this.trialRound = trialRound;
	}
	
	public List<String> getDefendantlist() {
		return defendantlist;
	}
	public void setDefendantlist(List<String> defendantlist) {
		this.defendantlist = defendantlist;
	}
	public List<String> getProsecutorlist() {
		return prosecutorlist;
	}
	public void setProsecutorlist(List<String> prosecutorlist) {
		this.prosecutorlist = prosecutorlist;
	}
	public String getPlaintiffRequestOfFirst() {
		return plaintiffRequestOfFirst;
	}
	public void setPlaintiffRequestOfFirst(String plaintiffRequestOfFirst) {
		this.plaintiffRequestOfFirst = plaintiffRequestOfFirst;
	}
	public String getCourtConsider() {
		return courtConsider;
	}
	public void setCourtConsider(String courtConsider) {
		this.courtConsider = courtConsider;
	}
	public String getAppellantRequest() {
		return appellantRequest;
	}
	public void setAppellantRequest(String appellantRequest) {
		this.appellantRequest = appellantRequest;
	}
	public String getDefendantReply() {
		return defendantReply;
	}
	public void setDefendantReply(String defendantReply) {
		this.defendantReply = defendantReply;
	}
	public String getAppelleeArguing() {
		return appelleeArguing;
	}
	public void setAppelleeArguing(String appelleeArguing) {
		this.appelleeArguing = appelleeArguing;
	}
	public String getRecorder() {
		return recorder;
	}
	public void setRecorder(String recorder) {
		this.recorder = recorder;
	}
	public String getExecuteProcess() {
		return executeProcess;
	}
	public void setExecuteProcess(String executeProcess) {
		this.executeProcess = executeProcess;
	}
	public String getDefendantReplyOfFirst() {
		return defendantReplyOfFirst;
	}
	public void setDefendantReplyOfFirst(String defendantReplyOfFirst) {
		this.defendantReplyOfFirst = defendantReplyOfFirst;
	}
	public String getTrialProcedure() {
		return trialProcedure;
	}
	public void setTrialProcedure(String trialProcedure) {
		this.trialProcedure = trialProcedure;
	}
	public String getIsValid() {
		return IsValid;
	}
	public void setIsValid(String isValid) {
		IsValid = isValid;
	}
	public String getCourtInspect() {
		return courtInspect;
	}
	public void setCourtInspect(String courtInspect) {
		this.courtInspect = courtInspect;
	}
	public String getPartyInfo() {
		return partyInfo;
	}
	public void setPartyInfo(String partyInfo) {
		this.partyInfo = partyInfo;
	}
	public String getCourtInspectOfFirst() {
		return courtInspectOfFirst;
	}
	public void setCourtInspectOfFirst(String courtInspectOfFirst) {
		this.courtInspectOfFirst = courtInspectOfFirst;
	}
	public String getCollegiateBench() {
		return collegiateBench;
	}
	public void setCollegiateBench(String collegiateBench) {
		this.collegiateBench = collegiateBench;
	}
	public String getJudgeResult() {
		return judgeResult;
	}
	public void setJudgeResult(String judgeResult) {
		this.judgeResult = judgeResult;
	}
	public String getCourtConsiderOfFirst() {
		return courtConsiderOfFirst;
	}
	public void setCourtConsiderOfFirst(String courtConsiderOfFirst) {
		this.courtConsiderOfFirst = courtConsiderOfFirst;
	}
	public String getJudegeDate() {
		return judegeDate;
	}
	public void setJudegeDate(String judegeDate) {
		this.judegeDate = judegeDate;
	}
	public List<RelatedCompanyDto> getRelatedCompanies() {
		return relatedCompanies;
	}
	public void setRelatedCompanies(List<RelatedCompanyDto> relatedCompanies) {
		this.relatedCompanies = relatedCompanies;
	}
	public CourtNoticeDto getCourtNoticeList() {
		return courtNoticeList;
	}
	public void setCourtNoticeList(CourtNoticeDto courtNoticeList) {
		this.courtNoticeList = courtNoticeList;
	}
	
	
	
	
	
}
