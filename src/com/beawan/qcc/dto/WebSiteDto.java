package com.beawan.qcc.dto;


import com.google.gson.annotations.SerializedName;

/**
 *
 */
public class WebSiteDto {

    @SerializedName("Url")
    private String url;//网址地址
    @SerializedName("Name")
    private String name;//网址名称

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
