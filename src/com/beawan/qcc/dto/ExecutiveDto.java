package com.beawan.qcc.dto;


/***
 * 高级管理人员列表，只在最终受益人为高级管理人员时返回
 * @author yuzhejia
 *
 */
public class ExecutiveDto {

	private String name;//高级管理人员姓名
	private String position;//高级管理人员职位
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	
}
