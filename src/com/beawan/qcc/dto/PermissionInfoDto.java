package com.beawan.qcc.dto;


import com.google.gson.annotations.SerializedName;

/**
 *  行政许可【信用中国】
 */
public class PermissionInfoDto {

    @SerializedName("Liandate")
    private String liandate;//决定日期
    @SerializedName("Name")
    private String name;//项目名称
    @SerializedName("CaseNo")
    private String caseNo;//决定文书号
    @SerializedName("Province")
    private String province;//地域

    public String getLiandate() {
        return liandate;
    }

    public void setLiandate(String liandate) {
        this.liandate = liandate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCaseNo() {
        return caseNo;
    }

    public void setCaseNo(String caseNo) {
        this.caseNo = caseNo;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }
}
