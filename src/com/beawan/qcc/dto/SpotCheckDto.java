package com.beawan.qcc.dto;

import com.google.gson.annotations.SerializedName;

/**
 * 抽查检查
 */
public class SpotCheckDto {

    @SerializedName("Type")
    private String type;//类型
    @SerializedName("No")
    private String no;//登记编号
    @SerializedName("Date")
    private String date;//日期
    @SerializedName("ExecutiveOrg")
    private String executiveOrg;//检查实施籍贯
    @SerializedName("Consequence")
    private String consequence;//结果
    @SerializedName("Remark")
    private String remark;//备注

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getExecutiveOrg() {
        return executiveOrg;
    }

    public void setExecutiveOrg(String executiveOrg) {
        this.executiveOrg = executiveOrg;
    }

    public String getConsequence() {
        return consequence;
    }

    public void setConsequence(String consequence) {
        this.consequence = consequence;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
