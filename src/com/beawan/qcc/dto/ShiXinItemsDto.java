package com.beawan.qcc.dto;


import com.google.gson.annotations.SerializedName;

/**
 * 失信
 */
public class ShiXinItemsDto {

    @SerializedName("Liandate")
    private String liandate;//立案日期
    @SerializedName("Executestatus")
    private String executestatus;//被执行人的履行情况
    @SerializedName("Publicdate")
    private String publicdate;//发布时间
    @SerializedName("Anno")
    private String anno;//立案文书号
    @SerializedName("Name")
    private String name;//公司名
    @SerializedName("Executegov")
    private String executegov;//执行法院
    @SerializedName("Actionremark")
    private String actionremark;//行为备注
    @SerializedName("Executeno")
    private String executeno;//执行依据文号
    @SerializedName("Orgno")
    private String orgno;//组织机构代码

    public String getLiandate() {
        return liandate;
    }

    public void setLiandate(String liandate) {
        this.liandate = liandate;
    }

    public String getExecutestatus() {
        return executestatus;
    }

    public void setExecutestatus(String executestatus) {
        this.executestatus = executestatus;
    }

    public String getPublicdate() {
        return publicdate;
    }

    public void setPublicdate(String publicdate) {
        this.publicdate = publicdate;
    }

    public String getAnno() {
        return anno;
    }

    public void setAnno(String anno) {
        this.anno = anno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExecutegov() {
        return executegov;
    }

    public void setExecutegov(String executegov) {
        this.executegov = executegov;
    }

    public String getActionremark() {
        return actionremark;
    }

    public void setActionremark(String actionremark) {
        this.actionremark = actionremark;
    }

    public String getExecuteno() {
        return executeno;
    }

    public void setExecuteno(String executeno) {
        this.executeno = executeno;
    }

    public String getOrgno() {
        return orgno;
    }

    public void setOrgno(String orgno) {
        this.orgno = orgno;
    }
}
