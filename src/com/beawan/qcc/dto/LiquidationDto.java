package com.beawan.qcc.dto;


import com.google.gson.annotations.SerializedName;

/**
 * 清算
 */
public class LiquidationDto {


    @SerializedName("Leader")
    private String leader;//清算组负责人
    @SerializedName("Member")
    private String member;//清算组成员

    public String getLeader() {
        return leader;
    }

    public void setLeader(String leader) {
        this.leader = leader;
    }

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }
}
