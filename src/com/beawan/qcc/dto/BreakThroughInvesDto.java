package com.beawan.qcc.dto;

import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * 受对外投资企业列表
 * @author yuzhejia
 * 数据字段类型和受益人穿透那边一模一样，妈的，但是返回数据的key不一样，这边开头字母大写    那边和字段名一样，不需要转
 *
 */
public class BreakThroughInvesDto {


	@SerializedName("KeyNo")
	private String keyNo;//KeyNo
	@SerializedName("Name")
	private String name;//企业名称
	@SerializedName("TotalStockPercent")
	private String totalStockPercent;//对外投资穿透总持股比例
	@SerializedName("DetailInfoList")
	private List<DetailInfoInvestDto> detailInfoList;//详细信息
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getKeyNo() {
		return keyNo;
	}
	public void setKeyNo(String keyNo) {
		this.keyNo = keyNo;
	}
	public String getTotalStockPercent() {
		return totalStockPercent;
	}
	public void setTotalStockPercent(String totalStockPercent) {
		this.totalStockPercent = totalStockPercent;
	}
	public List<DetailInfoInvestDto> getDetailInfoList() {
		return detailInfoList;
	}
	public void setDetailInfoList(List<DetailInfoInvestDto> detailInfoList) {
		this.detailInfoList = detailInfoList;
	}
	
	
}
