package com.beawan.qcc.dto;


import com.google.gson.annotations.SerializedName;

/**
 * 行政许可【工商局】
 */
public class PermissionEciInfoDto {

    @SerializedName("LicensDocNo")
    private String licensDocNo;//许可文件编号
    @SerializedName("LicensContent")
    private String licensContent;//许可内容
    @SerializedName("LicensDocName")
    private String licensDocName;//许可文件名称
    @SerializedName("ValidityTo")
    private String validityTo;//有效期至
    @SerializedName("ValidityFrom")
    private String validityFrom;//有效期自
    @SerializedName("LicensOffice")
    private String licensOffice;//许可机关

    public String getLicensDocNo() {
        return licensDocNo;
    }

    public void setLicensDocNo(String licensDocNo) {
        this.licensDocNo = licensDocNo;
    }

    public String getLicensContent() {
        return licensContent;
    }

    public void setLicensContent(String licensContent) {
        this.licensContent = licensContent;
    }

    public String getLicensDocName() {
        return licensDocName;
    }

    public void setLicensDocName(String licensDocName) {
        this.licensDocName = licensDocName;
    }

    public String getValidityTo() {
        return validityTo;
    }

    public void setValidityTo(String validityTo) {
        this.validityTo = validityTo;
    }

    public String getValidityFrom() {
        return validityFrom;
    }

    public void setValidityFrom(String validityFrom) {
        this.validityFrom = validityFrom;
    }

    public String getLicensOffice() {
        return licensOffice;
    }

    public void setLicensOffice(String licensOffice) {
        this.licensOffice = licensOffice;
    }
}
