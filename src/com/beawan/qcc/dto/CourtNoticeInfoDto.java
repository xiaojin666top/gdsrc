package com.beawan.qcc.dto;

import com.google.gson.annotations.SerializedName;

/**
 * 开庭公告详细信息 列表字段
 * @author yuzhejia
 *
 */
public class CourtNoticeInfoDto {


	@SerializedName("Id")
	private String id;//KeyNo
	@SerializedName("OpenDate")
	private String openDate;//开庭日期
	@SerializedName("CaseReason")
	private String caseReason;//案由
	@SerializedName("Defendant")
	private String defendant;//被告
	@SerializedName("CaseNo")
	private String caseNo;//案号
	@SerializedName("Prosecutor")
	private String prosecutor;//原告
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOpenDate() {
		return openDate;
	}
	public void setOpenDate(String openDate) {
		this.openDate = openDate;
	}
	public String getCaseReason() {
		return caseReason;
	}
	public void setCaseReason(String caseReason) {
		this.caseReason = caseReason;
	}
	public String getDefendant() {
		return defendant;
	}
	public void setDefendant(String defendant) {
		this.defendant = defendant;
	}
	public String getCaseNo() {
		return caseNo;
	}
	public void setCaseNo(String caseNo) {
		this.caseNo = caseNo;
	}
	public String getProsecutor() {
		return prosecutor;
	}
	public void setProsecutor(String prosecutor) {
		this.prosecutor = prosecutor;
	}
	
	
}
