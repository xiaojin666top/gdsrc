package com.beawan.qcc.dto;

import java.util.List;

import com.google.gson.annotations.SerializedName;

/***
 * 企业对外投资穿透
 * @author yuzhejia
 *
 */
public class QccInvestThroughDto {
	

	@SerializedName("KeyNo")
	private String keyNo;//KeyNo
	@SerializedName("CompanyName")
	private String companyName;//企业名称
	@SerializedName("FindMatched")
	private String findMatched;//是否穿透出对外投资 N Y
	@SerializedName("Remark")
	private String remark;//备注
	@SerializedName("BreakThroughList")
	private List<BreakThroughInvesDto> breakThroughList;//KeyNo
	
	
	public String getKeyNo() {
		return keyNo;
	}
	public void setKeyNo(String keyNo) {
		this.keyNo = keyNo;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getFindMatched() {
		return findMatched;
	}
	public void setFindMatched(String findMatched) {
		this.findMatched = findMatched;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public List<BreakThroughInvesDto> getBreakThroughList() {
		return breakThroughList;
	}
	public void setBreakThroughList(List<BreakThroughInvesDto> breakThroughList) {
		this.breakThroughList = breakThroughList;
	}
	
	

}
