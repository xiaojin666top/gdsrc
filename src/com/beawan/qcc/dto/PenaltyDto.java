package com.beawan.qcc.dto;


import com.google.gson.annotations.SerializedName;

/**
 * 行政处罚
 */
public class PenaltyDto {

    @SerializedName("Content")
    private String content;//行政处罚内容
    @SerializedName("PenaltyType")
    private String penaltyType;//违法行为类型
    @SerializedName("Remark")
    private String remark;//备注
    @SerializedName("OfficeName")
    private String officeName;//行政处罚决定机关名称
    @SerializedName("PenaltyDate")
    private String penaltyDate;//作出行政处罚决定日期
    @SerializedName("PublicDate")
    private String publicDate;//作出行政公示日期
    @SerializedName("DocNo")
    private String docNo;//行政处罚决定书文号

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPenaltyType() {
        return penaltyType;
    }

    public void setPenaltyType(String penaltyType) {
        this.penaltyType = penaltyType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getOfficeName() {
        return officeName;
    }

    public void setOfficeName(String officeName) {
        this.officeName = officeName;
    }

    public String getPenaltyDate() {
        return penaltyDate;
    }

    public void setPenaltyDate(String penaltyDate) {
        this.penaltyDate = penaltyDate;
    }

    public String getPublicDate() {
        return publicDate;
    }

    public void setPublicDate(String publicDate) {
        this.publicDate = publicDate;
    }

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }
}
