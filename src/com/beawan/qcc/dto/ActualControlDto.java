package com.beawan.qcc.dto;

import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * 公示的疑似实际控股人
 * @author yuzhejia
 *
 */
public class ActualControlDto {


    @SerializedName("StockPercent")
	private String stockPercent;//总股权比例

    @SerializedName("PersonList")
	private List<PersonDto> personList;//总股权比例

	public String getStockPercent() {
		return stockPercent;
	}

	public void setStockPercent(String stockPercent) {
		this.stockPercent = stockPercent;
	}

	public List<PersonDto> getPersonList() {
		return personList;
	}

	public void setPersonList(List<PersonDto> personList) {
		this.personList = personList;
	}
    
    
    
}
