package com.beawan.qcc.util;

import com.platform.util.StringUtil;

public class QccFormatUtil {

    private QccFormatUtil() {
    }

    //企业类型 0-企业（包括个体工商户），1-社会组织 ，3-香港公司，4-政府机构，
    //5-台湾公司，6-基金会，7-医院，8-海外公司，9-律师事务所，10-学 校 ，-1-其他

    private static final String COMP = "0";
    private static final String SOCIAL = "1";
    private static final String HKONG = "3";
    private static final String ORG = "4";
    private static final String TAIWAN = "5";
    private static final String JINJI = "6";
    private static final String HOSPITAL = "7";
    private static final String SEAOUT = "8";
    private static final String LAYER = "9";
    private static final String SCHOOLE = "10";
    private static final String OTHER = "-1";

    /**
     * 格式化企业性质  将企查查的企业性质转化成信贷企业性质
     * @param entType
     * @return
     */
    public static String formatEntType(String entType){
        if(StringUtil.isEmptyString(entType)){
            return entType;
        }
        switch (entType){
            case COMP:
                //民营企业	18
                return "18";
            case SOCIAL:
                //社会团体	08
                return "08";
            default:
                //其他	05
                return "05";
        }
    }

}
