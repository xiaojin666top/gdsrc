package com.beawan.customer.qbean;

/**
 * CusBase entity. @author MyEclipse Persistence Tools
 */

public class QCusBase implements java.io.Serializable {

	// Fields

	private String customerNo;//客户号
	private String customerName;//客户名称
	private String managerUserId;//管户人
	private String managerUserName;//管户人姓名
	private String managerOrgId;//管户机构ID
	private String managerOrgName;//管户机构名称
	
	// Constructors

	/** default constructor */
	public QCusBase() {
	}

	/** minimal constructor */
	public QCusBase(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getManagerUserId() {
		return managerUserId;
	}

	public void setManagerUserId(String managerUserId) {
		this.managerUserId = managerUserId;
	}

	public String getManagerUserName() {
		return managerUserName;
	}

	public void setManagerUserName(String managerUserName) {
		this.managerUserName = managerUserName;
	}

	public String getManagerOrgId() {
		return managerOrgId;
	}

	public void setManagerOrgId(String managerOrgId) {
		this.managerOrgId = managerOrgId;
	}

	public String getManagerOrgName() {
		return managerOrgName;
	}

	public void setManagerOrgName(String managerOrgName) {
		this.managerOrgName = managerOrgName;
	}

}