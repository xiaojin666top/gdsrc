package com.beawan.customer.utils;

import java.util.List;

import org.apache.log4j.Logger;

import com.beawan.customer.bean.CreditRating;
import com.ibm.db2.jcc.a.e;

//用于计算客户评级
public class RatingCountUtil {
	
	private static Logger log = Logger.getLogger(RatingCountUtil.class);
	
	
	/**
	 * TODO	获取评级得分
	 * @param enName
	 * @param value
	 */
	public static Object[] countScore(String enName,Double value) {
		Object[] obj = new Object[2];
		Double score=0.0;
		String analy = "";
		obj[0] = score;
		obj[1] = analy;
		
		if(null==enName||null==value||"".equals(enName)) {
			return obj;
		}
		
		if(enName.equals("111")) {
			if(value==0) {
				score= 3.0;
			}else if (value==1) {
				score= 1.0;
			}else {
				score= 0.0;
			}
		}
		
		if(enName.equals("112")) {
			if(value>=5) {
				score= 1.0;
				analy = "≥ 5";
			}else if (value>=3) {
				score= 0.5;
				analy = "≥ 3";
			}else {
				score= 0.0;
				analy = "< 3";
			}
		}
		
		if(enName.equals("121")) {
			if(value==0) {
				score= 1.0;
			}else if (value==1) {
				score= 0.5;
			}else {
				score= 0.0;
			}
		}
		
		if(enName.equals("122")) {
			if(value==0) {
				score= 2.0;
			}else if (value==1) {
				score= 1.0;
			}else {
				score= 0.0;
			}
		}
		
		if(enName.equals("131")) {
			if(value==0) {
				score= 2.0;
			}else if (value==1) {
				score= 1.0;
			}else {
				score= 0.0;
			}
		}
		
		if(enName.equals("132")) {
			if(value>=50) {
				score= 1.0;
				analy = "≥ 50%";
			}else if (value>=30) {
				score= 0.5;
				analy = "≥ 30%";
			}else {
				score= 0.0;
				analy = "< 30%";
			}
		}
		
		if(enName.equals("211")) {
			if(value==0) {
				score= 8.0;
			}else if (value==1) {
				score= 0.0;
			}else {
				score= 4.0;
			}
		}
		
		if(enName.equals("212")) {
			if(value==0) {
				score= 7.0;
			}else if (value==1) {
				score= 0.0;
			}else {
				score= 4.0;
			}
		}
		
		if(enName.equals("311")) {
			if(value>=1000) {
				score= 8.0;
				analy = "≥ 1000万";
			}else if (value>=300) {
				score= 6.0;
				analy = "≥ 300万";
			}else if(value>=100){
				score= 4.0;
				analy = "≥ 100万";
			}else if(value>=50){
				score= 2.0;
				analy = "≥ 50万";
			}
			else {
				score= 1.0;
				analy = "< 50万";
			}
		}
		
		if(enName.equals("312")) {
			if(value>=30) {
				score= 4.0;
				analy = "≥ 30%";
			}else if (value>=20) {
				score= 3.0;
				analy = "≥ 20%";
			}else if(value>=10){
				score= 2.0;
				analy = "≥ 10%";
			}
			else {
				score= 1.0;
				analy = "< 10%";
			}
		}
		
		if(enName.equals("411")) {
			if(value>=1.4) {
				score= 7.0;
				analy = "≥ 1.4";
			}else if (value>=1.2) {
				score= 6.0;
				analy = "≥ 1.2";
			}else if(value>=1.0){
				score= 5.0;
				analy = "≥ 1.0";
			}else if(value>=0.8){
				score= 4.0;
				analy = "≥ 0.8";
			}
			else if(value>=0.4){
				score= 3.0;
				analy = "≥ 0.4";
			}
			else {
				score= 0.0;
				analy = "< 0.4";
			}
		}
		
		if(enName.equals("412")) {
			if(value<40) {
				score= 10.0;
				analy = "< 40%";
			}else if (value<50) {
				score= 8.0;
				analy = "< 50%";
			}else if(value<60){
				score= 6.0;
				analy = "< 60%";
			}else if(value<70){
				score= 4.0;
				analy = "< 70%";
			}
			else {
				score= 0.0;
				analy = "≥ 80%";
			}
		}
		
		if(enName.equals("413")) {
			if(value>=1) {
				score= 7.0;
				analy = "≥ 1.0";
			}else if (value>=0.8) {
				score= 6.0;
				analy = "≥ 0.8";
			}else if(value>=0.6){
				score= 5.0;
				analy = "≥ 0.6";
			}else if(value>=0.4){
				score= 4.0;
				analy = "≥ 0.4";
			}
			else if(value>=0.2){
				score= 3.0;
				analy = "≥ 0.2";
			}
			else {
				score= 0.0;
				analy = "< 0.2";
			}
		}
		
		if(enName.equals("421")) {
			if(value>=10) {
				score= 5.0;
				analy = "≥ 10%";
			}else if (value>=8) {
				score= 4.0;
				analy = "≥ 8%";
			}else if(value>=6){
				score= 3.0;
				analy = "≥ 6%";
			}else if(value>=4){
				score= 2.0;
				analy = "≥ 4%";
			}
			else if(value>=1){
				score= 1.0;
				analy = "0-4%";
			}
			else {
				score= 0.0;
				analy = "< 1%";
			}
		}
		
		if(enName.equals("422")) {
			if(value>=5) {
				score= 6.0;
				analy = "≥ 5%";
			}else if (value>=3) {
				score= 4.0;
				analy = "≥ 3%";
			}else if(value>=2){
				score= 2.0;
				analy = "≥ 2%";
			}
			else {
				score= 0.0;
				analy = "< 2%";
			}
		}
		
		if(enName.equals("431")) {
			if(value>=10) {
				score= 5.0;
				analy = "≥ 10";
			}else if (value>=8) {
				score= 4.0;
				analy = "≥ 8";
			}else if(value>=6){
				score= 3.0;
				analy = "≥ 6";
			}
			else if(value>=4){
				score= 2.0;
				analy = "≥ 4";
			}
			else if(value>=2){
				score= 1.0;
				analy = "≥ 2";
			}
			else {
				score= 0.0;
				analy = "< 2";
			}
		}
		
		if(enName.equals("432")) {
			if(value>=8) {
				score= 5.0;
				analy = "≥ 8";
			}else if (value>=6) {
				score= 4.0;
				analy = "≥ 6";
			}else if(value>=4){
				score= 3.0;
				analy = "≥ 4";
			}
			else if(value>=2){
				score= 1.0;
				analy = "≥ 2";
			}
			else {
				score= 0.0;
				analy = "< 2";
			}
		}
		
		if(enName.equals("511")) {
			if(value==0) {
				score= 3.0;
			}else if (value==1) {
				score= 1.0;
			}else if(value==2){
				score= 0.0;
			}
			else;
		}
		
		if(enName.equals("512")) {
			if(value>=8) {
				score= 4.0;
				analy = "≥ 8%";
			}else if (value>=6) {
				score= 3.0;
				analy = "≥ 6%";
			}else if(value>=4){
				score= 2.0;
				analy = "≥ 4%";
			}
			else if(value>=2){
				score= 1.0;
				analy = "≥ 2%";
			}
			else if(value>=0) {
				score= 0.5;
				analy = "≥ 0%";
			}
			else {
				score= 0.0;
				analy = "< 0%";
			}
		}
		
		if(enName.equals("513")) {
			if(value>=4) {
				score= 4.0;
				analy = "≥ 4%";
			}else if (value>=2) {
				score= 3.0;
				analy = "≥ 2%";
			}else if(value>=1){
				score= 1.0;
				analy = "≥ 1%";
			}
			else {
				score= 0.0;
				analy = "< 1%";
			}
			
		}
		
		if(enName.equals("514")) {
			if(value>=4) {
				score= 2.0;
				analy = "≥ 4%";
			}else if (value>=2) {
				score= 1.0;
				analy = "≥ 2%";
			}else if(value>=0){
				score= 0.5;
				analy = "≥ 0%";
			}
			else {
				score= 0.0;
				analy = "< 0%";
			}
		}
		
		if(enName.equals("611")) {
			if(value<=0) {
				score= 5.0;
				analy = "0";
			}else if (value<20) {
				score= 3.0;
				analy = "< 20%";
			}else if(value<30){
				score= 1.0;
				analy = "< 30%";
			}
			else {
				score= 0.0;
				analy = "≥ 30%";
			}
		}
		obj[0] = score;
		obj[1] = analy;
		return obj;
	}
	
	/**
	 * TODO	获取总分
	 * @param List
	 */
	public static Double getResult(List<CreditRating> list) {
		
		double result=0;
		
		for(CreditRating data:list) {
			result=result+data.getScore();
		}
		
		return result;
	}
	
	/**
	 * TODO	获取等级
	 * @param score
	 */
	public static String getLevel(Double score) {
		
		if(score>=90) {
			return "AAA";
		}else if(score>=85) {
			return "AA";
		}else if(score>=80) {
			return "A";
		}else if(score>=70) {
			return "BBB";
		}else if(score>=65) {
			return "BB";
		}else if(score>=60) {
			return "B";
		}else {
			return "C";
		}
	}
}
