package com.beawan.customer.utils;

public class ItemUtil {

	
	/**
	 * 将科目名称进行预处理 
	 * @param item 目标科目别名
	 * @return
	 */
	public static String preProcess(String item){
		item = item.replaceAll("               ", "");
		item = item.replaceAll("              ", "");
		item = item.replaceAll("             ", "");
		item = item.replaceAll("            ", "");
		item = item.replaceAll("           ", "");
		item = item.replaceAll("          ", "");
		item = item.replaceAll("         ", "");
		item = item.replaceAll("        ", "");
		item = item.replaceAll("       ", "");
		item = item.replaceAll("      ", "");
		item = item.replaceAll("     ", "");
		item = item.replaceAll("    ", "");
		item = item.replaceAll("   ", "");
		item = item.replaceAll("  ", "");
		item = item.replaceAll("　", "");
		item = item.replaceAll(" ", "");
		item = item.trim();
		item = item.replace(" ", "");
		item = item.replaceAll("\\s*", "");

		item = item.replaceAll("-", "");
		item = item.replaceAll("（[^）]*）", "");
		item = item.replaceAll("\\([^\\)]*\\)", "");
		item = item.replaceAll("其它", "其他");
		item = item.replaceAll("减：", "");
		item = item.replaceAll("加：", "");
		item = item.replaceAll("减:", "");
		item = item.replaceAll("加:", "");
		item = item.replaceAll("帐", "账");
		item = item.replaceAll("其中:", "");
		item = item.replaceAll("其中：", "");
		item = item.replaceAll("：", "");
		item = item.replaceAll(":", "");
		item = item.replaceAll("一", "");
		item = item.replaceAll("二", "");
		item = item.replaceAll("三", "");
		item = item.replaceAll("四", "");
		item = item.replaceAll("五", "");
		item = item.replaceAll("六", "");
		item = item.replaceAll("七", "");
		item = item.replaceAll("八", "");
		item = item.replaceAll("九", "");
		item = item.replaceAll("十", "");
		item = item.replaceAll(" +", "");
		item = item.replaceAll("1", "");
		item = item.replaceAll("2", "");
		item = item.replaceAll("3", "");
		item = item.replaceAll("4", "");
		item = item.replaceAll("5", "");
		item = item.replaceAll("6", "");
		item = item.replaceAll("7", "");
		item = item.replaceAll("8", "");
		item = item.replaceAll("9", "");
		item = item.replaceAll("0", "");
		item = item.replaceAll("、", "");
		item = item.replaceAll("【", "");
		item = item.replaceAll("】", "");
		item = item.replaceAll("\\(", "");
		item = item.replaceAll("\\)", "");
		item = item.replaceAll("[*]", "");
		item = item.replaceAll("\\.", "");
		item = item.replaceAll("\\+", "");
		return item;
	}
}
