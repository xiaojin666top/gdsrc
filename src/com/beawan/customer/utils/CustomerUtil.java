package com.beawan.customer.utils;

import org.apache.log4j.Logger;

import com.beawan.common.ServiceFactory;
import com.beawan.customer.service.ICusFSRecordSV;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.service.ICusBaseSV;

public class CustomerUtil {
	
	private static Logger log = Logger.getLogger(CustomerUtil.class);

	/**
	 * TODO 异步从信贷系统数据库同步客户财务报表数据
	 * @param year 需要同步的截止年份，<=0默认为当前年
	 * @param month 需要同步的截止月份 <=0默认为当前月
	 * @param customerId
	 */
	public static void asynCusFSFromCmis(final String customerId, 
			final int year, final int month){
		//异步检查同步客户财务报表数据
		new Thread(){
			@Override
			public void run(){
				try {
					ICusFSRecordSV cusFSRecordSV = ServiceFactory.getService(ICusFSRecordSV.class);
					ICusBaseSV cusBaseSV = ServiceFactory.getService(ICusBaseSV.class);
					CusBase cusBase = cusBaseSV.queryByCusNo(customerId);
					if(cusBase!=null)
						cusFSRecordSV.syncFSFromCmisByCusNo(customerId, cusBase.getXdCustomerNo(), year, month);
				} catch (Exception e) {
					log.error("客户财务数据同步异常：", e);
				}
			}
		}.start();
	}
	
}
