package com.beawan.customer.utils;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.wltea.expression.ExpressionEvaluator;

import com.beawan.base.entity.SReportData;
import com.beawan.base.entity.SReportModelRow;
import com.beawan.base.entity.SReportRecord;
import com.platform.util.ExceptionUtil;
import com.platform.util.StringUtil;

public class ReportCalculator {
	
	private static Logger log = Logger.getLogger(ReportCalculator.class);
	
	private static DecimalFormat dformat = new DecimalFormat("0.000000");
	
	public static final String CURR_TERM = "当期";
	public static final String LAST_YEAR = "上年年末";
	public static final String LEFT_COL = "左栏";
	public static final String RIGHT_COL = "右栏";
	
	public ReportCalculator(){}
	
	/**
	 * TODO 自动生成报表数据
	 * @param rRecord 具体报表记录
	 * @param modelRows 报表模型行数据
	 * @param dataMap 包含当期和上年年末所有报表数据集合
	 * @return 返回生成对应报表的数据集合
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Object> genReprtData(SReportRecord rRecord,
			List<SReportModelRow> modelRows, Map<String, Object> dataMap) throws Exception {
		
		//当期全部数据
		Map<String, Object> currData = (Map<String, Object>) dataMap.get(CURR_TERM);
		//当期对应报表数据
		Map<String, Object> reportData = (Map<String, Object>) currData.get(rRecord.getModelNo());
		if(reportData == null){
			reportData = new HashMap<String, Object>();
			currData.put(rRecord.getModelNo(), reportData);
		}
		
		//报表模型数据
		for(SReportModelRow row : modelRows){
			
			SReportData data = (SReportData) reportData.get(row.getRowSubject());
			
			if(data == null){
				data = ReportCalculator.getReportRowData(rRecord.getReportNo(), row);
				reportData.put(row.getRowSubject(), data);
			}
			
			String col2Def = row.getCol2Def();
			if(!StringUtil.isEmpty(col2Def)){
				data.setCol2Value(getColValue(row.getRowName(), col2Def, dataMap));
			}
			
			String col1Def = row.getCol1Def();
			if(!StringUtil.isEmpty(col1Def)){
				data.setCol1Value(getColValue(row.getRowName(), col1Def, dataMap));
			}
		}
		
		return reportData;
	}
	
	public Double getColValue(String rowName, String colDef, Map<String, Object> dataMap) {
		
		Double result = null;
		String expression = null;
		
		try{
			//先转换校验
			expression = convertExpression(colDef);
			
			String pattern = "\\{[^\\}]+\\}";
	        Pattern p = Pattern.compile(pattern);
	        Matcher m = p.matcher(expression);
	        
	        Map<String,Double> vars = new HashMap<String,Double>();
	        
	        while(m.find()){
	        	
	        	String variable = m.group();
	        	if(vars.containsKey(variable))
	        		continue;
	        	
	        	double value = getVariableValue(variable, dataMap);
	        	String varStr = dformat.format(value);
	        	if(value < 0)
	        		varStr = "(" + varStr + ")";
	        	//将列定义中的变量标记替换成具体数值
	        	expression = expression.replace(variable, varStr);
	        	
	        	vars.put(variable, value);
	        }
        
	        result = calcExpValue(expression);
	        
        }catch(Exception e){
        	log.error("\n" + e.getMessage()+ "\nrowName:" + rowName + "\ncolDef:" + colDef + "\nexpression:" + expression);
        }
        
        return result;
	}
	
	@SuppressWarnings("unchecked")
	public double getVariableValue(String variable, Map<String, Object> dataMap){
		
		//对变量进行拆分，{当期.0150.404.右栏}
		String[] arr = variable.substring(1, variable.length()-1).split("\\.");
		String dateFlag = arr[0];
		String modelNo = arr[1];
		String subject = arr[2];
		String colFlag = arr[3];
		
		Double result = null;
		boolean lastYearDataFlag = false;//上年年末数据存在标识
		if(LAST_YEAR.equals(dateFlag)){
			//取得上年年末数据集合
			Map<String, Object> lastDataMap = (Map<String, Object>) dataMap.get(LAST_YEAR);
			if(lastDataMap != null){
				//取得对应报表数据集合
				Map<String, Object> subjectDataMap = (Map<String, Object>) lastDataMap.get(modelNo);
				if(subjectDataMap != null){
					//取得对应科目数据对象
					SReportData rd = (SReportData) subjectDataMap.get(subject);
					if(rd != null){
						if(RIGHT_COL.equals(colFlag))
							result = rd.getCol2Value();
						else
							result = rd.getCol1Value();
						lastYearDataFlag = true;//上年年末对应报表数据不存在
					}
				}
			}
		}
		
		//当期数据，或者是上年年末右栏数据
		if(CURR_TERM.equals(dateFlag) || (!lastYearDataFlag && RIGHT_COL.equals(colFlag))){
			//取得当期数据集合
			Map<String, Object> currDataMap = (Map<String, Object>) dataMap.get(CURR_TERM);
			if(currDataMap != null){
				//取得对应报表数据集合
				Map<String, Object> subjectDataMap = (Map<String, Object>) currDataMap.get(modelNo);
				if(subjectDataMap != null){
					//取得对应科目数据对象
					SReportData rd = (SReportData) subjectDataMap.get(subject);
					if(rd != null){
						if(LAST_YEAR.equals(dateFlag)){
							//上年年末对应报表数据不存在，则取当期报表中左栏数据
							result = rd.getCol1Value();
						}else{
							if(RIGHT_COL.equals(colFlag))
								result = rd.getCol2Value();
							else
								result = rd.getCol1Value();
						}
					}
				}
			}
		}
		
		if(result == null)
			result = 0.0;
		
		return result;
	}
	
	public double calcExpValue(String expression){
		
		List<String> ifs = findIfExp(expression);
		
		if(!CollectionUtils.isEmpty(ifs)){
			for(String ifStr : ifs){
				String temp = ifStr.substring(3, ifStr.length()-1);
	        	double result = calcExpValue(temp);
	        	expression = expression.replace(ifStr, dformat.format(result));
			}
		}
		
        Object result;
		
    	//说明是IF语句
    	if(expression.contains(",")){
    		//拆分
        	String[] ifoptions = expression.split(",");
        	
    		Boolean condition = (Boolean) ExpressionEvaluator.evaluate(ifoptions[0]);
    		if(condition){
    			if(StringUtil.isRealNumber(ifoptions[1]))
    				result = ifoptions[1];
    			else
    				result = ExpressionEvaluator.evaluate(ifoptions[1]);
    		}else{
    			if(StringUtil.isRealNumber(ifoptions[2]))
    				result = ifoptions[2];
    			else
    				result = ExpressionEvaluator.evaluate(ifoptions[2]);
    		}
    	}else
    		result = ExpressionEvaluator.evaluate(expression);
    	
		return Double.parseDouble(result.toString());
	}
	
	private List<String> findIfExp(String str){
		
		StringBuffer sb = new StringBuffer(str);
		int index = sb.indexOf("IF");
		if(index == -1)
			return null;
		
		List<String> list = new ArrayList<String>();
		
		int count = 0;
		for(int i=index+2;i<sb.length();i++){
			char c = sb.charAt(i);
			if(c == '(')
				count++;
			if(c == ')')
				count--;
			
			if(count == 0){
				list.add(sb.substring(index, i+1));
				index = sb.indexOf("IF", i);
				if(index != -1)
					i = index+1;
				else
					break;
			}
		}
		
		return list;
	}
	
	/**
	 * TODO 通过报表模型行对象生成报表数据初始对象（无数值）
	 * @param reportNo 报表编号
	 * @param modelRow 报表模型行对象
	 * @return 报表数据对象
	 */
	public static SReportData getReportRowData(String reportNo, SReportModelRow modelRow){
		
		SReportData data = new SReportData();
		data.setReportNo(reportNo);
		data.setRowNo(modelRow.getRowNo());
		data.setRowName(modelRow.getRowName());
		data.setRowSubject(modelRow.getRowSubject());
		data.setRowDimType(modelRow.getRowDimType());
		data.setDisplayOrder(modelRow.getDisplayOrder());
		data.setRowAttribute(modelRow.getRowAttribute());
		
		return data;
	}
	
	/**
	 * TODO 表达式转换
	 * @param expression
	 * @return
	 * @throws Exception
	 */
	private static String convertExpression(String expression) throws Exception{
		
		//先校验是否存在语法错误
		checkExpression(expression);
		
		//去掉特殊字符及转换
		String tempExp = expression.replace("\\n", "").replace("<>", "!=").toUpperCase();
		
		return tempExp;
	}
	
	/**
	 * TODO 表达式校验
	 * @param expression
	 * @return
	 * @throws Exception 
	 */
	private static void checkExpression(String expression) throws Exception {
		
		StringBuffer sb = new StringBuffer(expression);
		
		Stack<Integer> brace = new Stack<Integer>();
		Stack<Integer> bracket = new Stack<Integer>();
		
		for(int i=0;i<sb.length();i++){
			char c = sb.charAt(i);
			if(c == '{')
				brace.push(i);
			else if(c == '}')
				if(brace.isEmpty()) brace.push(i); else brace.pop();
			else if(c == '(')
				bracket.push(i);
			else if(c == ')'){
				if(bracket.isEmpty()) bracket.push(i); else bracket.pop();
			}
		}
		
		if(!brace.isEmpty() || !bracket.isEmpty()){
			
			StringBuffer msg = new StringBuffer("表达式错误");
			
			if(brace.size() > 0)
				msg.append("，缺少或多余}");
			
			if(bracket.size() > 0)
				msg.append("，缺少或多余）");
			
			ExceptionUtil.throwException(msg.toString());
		}
	}
	
}
