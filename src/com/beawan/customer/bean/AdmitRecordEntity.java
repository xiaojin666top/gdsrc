package com.beawan.customer.bean;

public class AdmitRecordEntity {
	private Long id;
	private String customerNo;
	private String identifCode;
	private String quotaData;
	private String quotaResult;
	private String quotaDescribe;
	private String admitResult;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getIdentifCode() {
		return identifCode;
	}
	public void setIdentifCode(String identifCode) {
		this.identifCode = identifCode;
	}
	public String getQuotaData() {
		return quotaData;
	}
	public void setQuotaData(String quotaData) {
		this.quotaData = quotaData;
	}
	public String getQuotaResult() {
		return quotaResult;
	}
	public void setQuotaResult(String quotaResult) {
		this.quotaResult = quotaResult;
	}
	
	public String getQuotaDescribe() {
		return quotaDescribe;
	}
	public void setQuotaDescribe(String quotaDescribe) {
		this.quotaDescribe = quotaDescribe;
	}
	public String getAdmitResult() {
		return admitResult;
	}
	public void setAdmitResult(String admitResult) {
		this.admitResult = admitResult;
	}
	
	
}
