package com.beawan.customer.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 客户财务报表记录实体类
 * 记录的是每期大的实体 即财报的主表
 */
@Entity
@Table(name = "CUS_FSRECORD",schema = "GDTCESYS")
public class CusFSRecord extends BaseEntity implements java.io.Serializable {
	
	// Fields
	@Id
	@Column(name = "RECORD_NO")
	private String recordNo;
	@Column(name = "CUSTOMER_ID")
	private String customerId;//客户编号
	@Column(name = "REPORT_DATE")
	private String reportDate;//报表日期
	@Column(name = "REPORT_SCOPE")
	private String reportScope; //报表口径 01：合并，02：本部
	@Column(name = "REPORT_PERIOD")
	private String reportPeriod; //报表周期 01：月报，02：季报，03：半年报，04：年报
	@Column(name = "REPORT_CURRENCY")
	private String reportCurrency; //币种，CNY：人民币
	@Column(name = "REPORT_UNIT")
	private String reportUnit; //单位，01：元
	@Column(name = "REPORT_STATUS")
	private String reportStatus; //报表状态，01：新增，02：完成，03：锁定
	@Column(name = "REPORT_FLAG")
	private String reportFlag;//报表检查标志
	@Column(name = "REPORT_OPINION")
	private String reportOpinion;//报表注释
	@Column(name = "AUDIT_FLAG")
	private String auditFlag;//审计标志
	@Column(name = "AUDIT_OFFICE")
	private String auditOffice;//审计单位
	@Column(name = "AUDIT_OPINION")
	private String auditOpinion;//审计意见
	@Column(name = "INPUT_DATE")
	private String inputDate;//登记日期
	@Column(name = "ORG_ID")
	private String orgId;//创建机构
	@Column(name = "USER_ID")
	private String userId;//操作员
	@Column(name = "REMARK")
	private String remark;//备注
	@Column(name = "REPORT_LOCKED")
	private String reportLocked; //报表锁定标识，1：锁定，0：正常
	@Column(name = "MODEL_CLASS")
	private String modelClass;//报表模型类别
	@Column(name = "FINISH_DATE")
	private String finishDate;//完成日期
	@Column(name = "UPDATE_DATE")
	private String updateDate;//更新日期

	// Constructors

	/** default constructor */
	public CusFSRecord() {
	}

	/** minimal constructor */
	public CusFSRecord(String recordNo) {
		this.recordNo = recordNo;
	}

	/** full constructor */
	public CusFSRecord(String recordNo, String customerId, String reportDate,
			String reportScope, String reportPeriod, String reportCurrency,
			String reportUnit, String reportStatus, String reportFlag,
			String reportOpinion, String auditFlag, String auditOffice,
			String auditOpinion, String inputDate, String orgId, String userId,
			String remark, String reportLocked, String modelClass,
			String finishDate) {
		this.recordNo = recordNo;
		this.customerId = customerId;
		this.reportDate = reportDate;
		this.reportScope = reportScope;
		this.reportPeriod = reportPeriod;
		this.reportCurrency = reportCurrency;
		this.reportUnit = reportUnit;
		this.reportStatus = reportStatus;
		this.reportFlag = reportFlag;
		this.reportOpinion = reportOpinion;
		this.auditFlag = auditFlag;
		this.auditOffice = auditOffice;
		this.auditOpinion = auditOpinion;
		this.inputDate = inputDate;
		this.orgId = orgId;
		this.userId = userId;
		this.remark = remark;
		this.reportLocked = reportLocked;
		this.modelClass = modelClass;
		this.finishDate = finishDate;
	}

	// Property accessors

	public String getRecordNo() {
		return this.recordNo;
	}

	public void setRecordNo(String recordNo) {
		this.recordNo = recordNo;
	}

	public String getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getReportDate() {
		return this.reportDate;
	}

	public void setReportDate(String reportDate) {
		this.reportDate = reportDate;
	}

	public String getReportScope() {
		return this.reportScope;
	}

	public void setReportScope(String reportScope) {
		this.reportScope = reportScope;
	}

	public String getReportPeriod() {
		return this.reportPeriod;
	}

	public void setReportPeriod(String reportPeriod) {
		this.reportPeriod = reportPeriod;
	}

	public String getReportCurrency() {
		return this.reportCurrency;
	}

	public void setReportCurrency(String reportCurrency) {
		this.reportCurrency = reportCurrency;
	}

	public String getReportUnit() {
		return this.reportUnit;
	}

	public void setReportUnit(String reportUnit) {
		this.reportUnit = reportUnit;
	}

	public String getReportStatus() {
		return this.reportStatus;
	}

	public void setReportStatus(String reportStatus) {
		this.reportStatus = reportStatus;
	}

	public String getReportFlag() {
		return this.reportFlag;
	}

	public void setReportFlag(String reportFlag) {
		this.reportFlag = reportFlag;
	}

	public String getReportOpinion() {
		return this.reportOpinion;
	}

	public void setReportOpinion(String reportOpinion) {
		this.reportOpinion = reportOpinion;
	}

	public String getAuditFlag() {
		return this.auditFlag;
	}

	public void setAuditFlag(String auditFlag) {
		this.auditFlag = auditFlag;
	}

	public String getAuditOffice() {
		return this.auditOffice;
	}

	public void setAuditOffice(String auditOffice) {
		this.auditOffice = auditOffice;
	}

	public String getAuditOpinion() {
		return this.auditOpinion;
	}

	public void setAuditOpinion(String auditOpinion) {
		this.auditOpinion = auditOpinion;
	}

	public String getInputDate() {
		return this.inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getOrgId() {
		return this.orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getReportLocked() {
		return this.reportLocked;
	}

	public void setReportLocked(String reportLocked) {
		this.reportLocked = reportLocked;
	}

	public String getModelClass() {
		return this.modelClass;
	}

	public void setModelClass(String modelClass) {
		this.modelClass = modelClass;
	}

	public String getFinishDate() {
		return this.finishDate;
	}

	public void setFinishDate(String finishDate) {
		this.finishDate = finishDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

}