package com.beawan.customer.bean;

/***
 * 准入调查模型
 * @author songchao
 *
 */
public class Admittance {
	private Long id;
	private String customerNo;
	//公司成立年限
	private String compLife;
	
	//还款能力  资产负债率
	private String creditInfo;
	
	//最近五年强行执行记录：
	private String settlement;
	//过去一年内预警情况
	private String earlyWaring;
	//涉及我行限制类行业
	private String induWaring;
	//对外担保情况
	private String guarntee;
	//近三年盈利情况
	private String beoverdue;
	//企业对外担保金额占净资产比率
	private String guarnteeAmount;
	//企业征信记录
	private String creditRecord;
	//公司涉诉情况
	private String enforcement;
	//公司法人涉诉情况
	private String punish;
	//准入结果:0,可准入；1，增信准入；2：不予准入
	private String admittanceResult;
	//准入结果详情
	private String admittanceInfo;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCustomerNo() {
		return customerNo;
	}
	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}
	public String getCompLife() {
		return compLife;
	}
	public void setCompLife(String compLife) {
		this.compLife = compLife;
	}
	public String getCreditInfo() {
		return creditInfo;
	}
	public void setCreditInfo(String creditInfo) {
		this.creditInfo = creditInfo;
	}
	public String getSettlement() {
		return settlement;
	}
	public void setSettlement(String settlement) {
		this.settlement = settlement;
	}
	public String getEarlyWaring() {
		return earlyWaring;
	}
	public void setEarlyWaring(String earlyWaring) {
		this.earlyWaring = earlyWaring;
	}
	public String getGuarntee() {
		return guarntee;
	}
	public void setGuarntee(String guarntee) {
		this.guarntee = guarntee;
	}
	public String getBeoverdue() {
		return beoverdue;
	}
	public void setBeoverdue(String beoverdue) {
		this.beoverdue = beoverdue;
	}
	public String getGuarnteeAmount() {
		return guarnteeAmount;
	}
	public void setGuarnteeAmount(String guarnteeAmount) {
		this.guarnteeAmount = guarnteeAmount;
	}
	public String getCreditRecord() {
		return creditRecord;
	}
	public void setCreditRecord(String creditRecord) {
		this.creditRecord = creditRecord;
	}
	public String getEnforcement() {
		return enforcement;
	}
	public void setEnforcement(String enforcement) {
		this.enforcement = enforcement;
	}
	public String getPunish() {
		return punish;
	}
	public void setPunish(String punish) {
		this.punish = punish;
	}
	public String getAdmittanceInfo() {
		return admittanceInfo;
	}
	public void setAdmittanceInfo(String admittanceInfo) {
		this.admittanceInfo = admittanceInfo;
	}
	public String getAdmittanceResult() {
		return admittanceResult;
	}
	public void setAdmittanceResult(String admittanceResult) {
		this.admittanceResult = admittanceResult;
	}
	public String getInduWaring() {
		return induWaring;
	}
	public void setInduWaring(String induWaring) {
		this.induWaring = induWaring;
	}
	@Override
	public String toString() {
		return "Admittance [id=" + id + ", customerNo=" + customerNo + ", compLife=" + compLife + ", creditInfo="
				+ creditInfo + ", settlement=" + settlement + ", earlyWaring=" + earlyWaring + ", induWaring="
				+ induWaring + ", guarntee=" + guarntee + ", beoverdue=" + beoverdue + ", guarnteeAmount="
				+ guarnteeAmount + ", creditRecord=" + creditRecord + ", enforcement=" + enforcement + ", punish="
				+ punish + ", admittanceResult=" + admittanceResult + ", admittanceInfo=" + admittanceInfo + "]";
	}
	
	
	
}
