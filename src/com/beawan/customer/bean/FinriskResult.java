package com.beawan.customer.bean;


import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 财务分析结果表
 */
@Entity
@Table(name = "FINRISK_RESULT",schema = "GDTCESYS")
public class FinriskResult extends BaseEntity {

	@Id
	@GeneratedValue()
	//@SequenceGenerator(name="FINRISK_RESULT_SEQ",allocationSize=1,initialValue=1, sequenceName="FINRISK_RESULT_SEQ")
	private Long id;

	@Column(name = "DG_SERIAL_NUMBER")
	private String dgSerialNumber;//对公系统的任务流水号
	@Column(name = "RESULT_TASK_ID")
	private String resultTaskId;//财务分析任务返回的id
	@Column(name = "PLATFORM_TYPE")
	private Integer platformType;//平台类型  1：本地财务分析系统   2：云端财务分析系统
	//返回数据的报文     对报文字符串长度进行切割   保证不超过3000字节长度
	@Column(name = "JSON_DATA")
	private String jsonData;//全部财务分析json报文
	@Column(name = "QUOTA_INFO")
	private String quotaInfo;//流动资金测算
	
	public FinriskResult() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FinriskResult(String dgSerialNumber, String resultTaskId, Integer platformType) {
		this.dgSerialNumber = dgSerialNumber;
		this.resultTaskId = resultTaskId;
		this.platformType = platformType;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDgSerialNumber() {
		return dgSerialNumber;
	}

	public void setDgSerialNumber(String dgSerialNumber) {
		this.dgSerialNumber = dgSerialNumber;
	}

	public String getResultTaskId() {
		return resultTaskId;
	}

	public void setResultTaskId(String resultTaskId) {
		this.resultTaskId = resultTaskId;
	}

	public Integer getPlatformType() {
		return platformType;
	}

	public void setPlatformType(Integer platformType) {
		this.platformType = platformType;
	}

	public String getJsonData() {
		return jsonData;
	}

	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}

	public String getQuotaInfo() {
		return quotaInfo;
	}

	public void setQuotaInfo(String quotaInfo) {
		this.quotaInfo = quotaInfo;
	}
}
