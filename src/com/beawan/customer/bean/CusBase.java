package com.beawan.customer.bean;

import com.beawan.core.BaseEntity;

import javax.persistence.*;

/**
 * 客户基本信息表 --》和主流程无关
 *
 * 所有信息来自cmis表
 */

@Entity
@Table(name = "CUS_BASE",schema = "GDTCESYS")
public class CusBase extends BaseEntity implements java.io.Serializable {

	// Fields
	//对公客户号
	@Id
	@Column(name = "CUSTOMER_NO")
	private String customerNo;
	@Column(name = "CUSTOMER_NAME")
	private String customerName;//客户名称
	@Column(name = "CUSTOMER_TYPE")
	private String customerType;//客户类型
	@Column(name = "CERT_TYPE")
	private String certType;//证件类型
	@Column(name = "CERT_CODE")
	private String certCode;//证件号
	@Column(name = "INPUT_ORG_ID")
	private String inputOrgId;//登记机构
	@Column(name = "INPUT_USER_ID")
	private String inputUserId;//登记人
	@Column(name = "INPUT_DATE")
	private String inputDate;//登记日期
	@Column(name = "MF_CUSTOMER_NO")
	private String mfCustomerNo;//核心客户号
	@Column(name = "XD_CUSTOMER_NO")
	private String xdCustomerNo;//信贷客户号
	@Column(name = "STATE")
	private String state;//状态
	@Column(name = "MANAGER_USER_ID")
	private String managerUserId;//管户人
	@Column(name = "MANAGER_ORG_ID")
	private String managerOrgId;//管户机构ID
	@Column(name = "BLACK_SHEET_OR_NOT")
	private String blackSheetOrNot;//是否黑名客户
	@Column(name = "CONFIRM_OR_NOT")
	private String confirmOrNot;//是否生效
	@Column(name = "CLIENT_CLASS_N")
	private String clientClassN;//当前客户分类
	@Column(name = "CLIENT_CLASS_M")
	private String clientClassM;//客户分类调整
	@Column(name = "BUSINESS_STATE")
	private String businessState;//存量字段标志
	@Column(name = "UPDATE_DATE")
	private String updateDate;//更新日期
	@Column(name = "REMARK")
	private String remark;//备注
	@Column(name = "SYNC_TIME")
	private String syncTime;//客户信息更新同步时间
	@Column(name = "FINA_SYNC_TIME")
	private String finaSyncTime;//财报同步时间
	@Column(name = "QCC_SYNC_TIME")
	private String qccSyncTime;//企查查更新时间

	// Constructors

	/** default constructor */
	public CusBase() {
	}

	/** minimal constructor */
	public CusBase(String customerNo) {
		this.customerNo = customerNo;
	}
	
	

	/** full constructor */
	public CusBase(String customerNo, String customerName, String customerType,
			String certType, String certCode, String inputOrgId,
			String inputUserId, String inputDate, String mfCustomerNo,
			String state, String managerUserId, String managerOrgId,
			String blackSheetOrNot, String confirmOrNot, String clientClassN,
			String clientClassM, String businessState, String updateDate,
			String remark, String syncTime) {
		
		this.customerNo = customerNo;
		this.customerName = customerName;
		this.customerType = customerType;
		this.certType = certType;
		this.certCode = certCode;
		this.inputOrgId = inputOrgId;
		this.inputUserId = inputUserId;
		this.inputDate = inputDate;
		this.mfCustomerNo = mfCustomerNo;
		this.state = state;
		this.managerUserId = managerUserId;
		this.managerOrgId = managerOrgId;
		this.blackSheetOrNot = blackSheetOrNot;
		this.confirmOrNot = confirmOrNot;
		this.clientClassN = clientClassN;
		this.clientClassM = clientClassM;
		this.businessState = businessState;
		this.updateDate = updateDate;
		this.remark = remark;
		this.syncTime = syncTime;
		
	}

	/*public String getRetailUser() {
		return retailUser;
	}

	public void setRetailUser(String retailUser) {
		this.retailUser = retailUser;
	}*/
// Property accessors

	public String getCustomerNo() {
		return this.customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerType() {
		return this.customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getCertType() {
		return this.certType;
	}

	public void setCertType(String certType) {
		this.certType = certType;
	}

	public String getCertCode() {
		return this.certCode;
	}

	public void setCertCode(String certCode) {
		this.certCode = certCode;
	}

	public String getInputOrgId() {
		return this.inputOrgId;
	}

	public void setInputOrgId(String inputOrgId) {
		this.inputOrgId = inputOrgId;
	}

	public String getInputUserId() {
		return this.inputUserId;
	}

	public void setInputUserId(String inputUserId) {
		this.inputUserId = inputUserId;
	}

	public String getInputDate() {
		return this.inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getMfCustomerNo() {
		return this.mfCustomerNo;
	}

	public void setMfCustomerNo(String mfCustomerNo) {
		this.mfCustomerNo = mfCustomerNo;
	}


	public String getManagerUserId() {
		return this.managerUserId;
	}

	public void setManagerUserId(String managerUserId) {
		this.managerUserId = managerUserId;
	}

	public String getManagerOrgId() {
		return this.managerOrgId;
	}

	public void setManagerOrgId(String managerOrgId) {
		this.managerOrgId = managerOrgId;
	}

	public String getBlackSheetOrNot() {
		return this.blackSheetOrNot;
	}

	public void setBlackSheetOrNot(String blackSheetOrNot) {
		this.blackSheetOrNot = blackSheetOrNot;
	}

	public String getConfirmOrNot() {
		return this.confirmOrNot;
	}

	public void setConfirmOrNot(String confirmOrNot) {
		this.confirmOrNot = confirmOrNot;
	}

	public String getClientClassN() {
		return this.clientClassN;
	}

	public void setClientClassN(String clientClassN) {
		this.clientClassN = clientClassN;
	}

	public String getClientClassM() {
		return this.clientClassM;
	}

	public void setClientClassM(String clientClassM) {
		this.clientClassM = clientClassM;
	}

	public String getBusinessState() {
		return this.businessState;
	}

	public void setBusinessState(String businessState) {
		this.businessState = businessState;
	}

	public String getUpdateDate() {
		return this.updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getSyncTime() {
		return this.syncTime;
	}

	public void setSyncTime(String syncTime) {
		this.syncTime = syncTime;
	}

	/*public String getCustAddr() {
		return custAddr;
	}

	public void setCustAddr(String custAddr) {
		this.custAddr = custAddr;
	}

	public Integer getRetail() {
		return retail;
	}

	public void setRetail(Integer retail) {
		this.retail = retail;
	}

	public Integer getImportWay() {
		return importWay;
	}

	public void setImportWay(Integer importWay) {
		this.importWay = importWay;
	}

	public Integer getRetailClass() {
		return retailClass;
	}

	public void setRetailClass(Integer retailClass) {
		this.retailClass = retailClass;
	}*/

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getXdCustomerNo() {
		return xdCustomerNo;
	}

	public void setXdCustomerNo(String xdCustomerNo) {
		this.xdCustomerNo = xdCustomerNo;
	}

	public String getFinaSyncTime() {
		return finaSyncTime;
	}

	public void setFinaSyncTime(String finaSyncTime) {
		this.finaSyncTime = finaSyncTime;
	}

	public String getQccSyncTime() {
		return qccSyncTime;
	}

	public void setQccSyncTime(String qccSyncTime) {
		this.qccSyncTime = qccSyncTime;
	}
	
	
}