package com.beawan.customer.bean;

import java.util.List;

/**
 * 异常项
 * @author yzj
 *
 */
public class ErrAbnorDto {

	private String name;
	private String errReason;
	private String reason;
	private int warningLv;
	
	private List<String> reasonList;
	
	
	
	public ErrAbnorDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	public ErrAbnorDto(String name, String errReason, String reason, int warningLv) {
		super();
		this.name = name;
		this.errReason = errReason;
		this.reason = reason;
		this.warningLv = warningLv;
	}



	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getErrReason() {
		return errReason;
	}
	public void setErrReason(String errReason) {
		this.errReason = errReason;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public int getWarningLv() {
		return warningLv;
	}
	public void setWarningLv(int warningLv) {
		this.warningLv = warningLv;
	}



	public List<String> getReasonList() {
		return reasonList;
	}



	public void setReasonList(List<String> reasonList) {
		this.reasonList = reasonList;
	}
	
	
}
