package com.beawan.customer;

public class AdmittDto {

	private String standar;
	private String value;
	private String analy;
	private String result;
	
	
	public AdmittDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public AdmittDto(String standar, String value, String analy, String result) {
		super();
		if(value==null)
			value = "";
		this.standar = standar;
		this.value = value;
		this.analy = analy;
		this.result = result;
	}
	public String getStandar() {
		return standar;
	}
	public void setStandar(String standar) {
		this.standar = standar;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getAnaly() {
		return analy;
	}
	public void setAnaly(String analy) {
		this.analy = analy;
	}
	
	
	
}
