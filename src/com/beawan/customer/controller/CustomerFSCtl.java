package com.beawan.customer.controller;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.HandlerMapping;

import com.beawan.base.dto.RandomizingID;
import com.beawan.base.entity.SReportData;
import com.beawan.base.entity.SReportModelRow;
import com.beawan.base.entity.User;
import com.beawan.base.service.IPlatformLogSV;
import com.beawan.base.service.ISReportDataSV;
import com.beawan.base.service.ISReportModelSV;
import com.beawan.base.service.ISReportRecordSV;
import com.beawan.base.service.ISysDicSV;
import com.beawan.base.service.IUserSV;
import com.beawan.common.BusinessException;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.common.config.AppConfig;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.bean.CusFSRecord;
import com.beawan.customer.dto.CustReportData;
import com.beawan.customer.dto.ReportDataOneItem;
import com.beawan.customer.dto.ReportDataSimple;
import com.beawan.customer.service.FinriskResultService;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.customer.service.ICusFSRecordSV;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.beawan.exterinvoke.util.FncStatMapUtil;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.beawan.task.service.ITaskSV;
import com.beawan.tranform.util.TranformUtil;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.internal.LinkedTreeMap;
import com.platform.util.ExceptionUtil;
import com.platform.util.GsonUtil;
import com.platform.util.HttpClientUtil;
import com.platform.util.HttpUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.PropertiesUtil;
import com.platform.util.StringUtil;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/cusfs" })
public class CustomerFSCtl extends BaseController{

	private static Logger log = Logger.getLogger(CustomerFSCtl.class);

	@Resource
	private ICmisInInvokeSV cmisInInvokeSV;
	@Resource
	private ICusBaseSV cusBaseSV;
	@Resource
	private ICusFSRecordSV cusFSRecordSV;
	@Resource
	private ISReportRecordSV sReportRecordSV;
	@Resource
	private ISReportDataSV sReportDataSV;
	@Resource
	private ISReportModelSV sReportModelSV;
	@Resource
	private ISysDicSV sysDicSV;
	@Resource
	private IPlatformLogSV platformLogSV;
	@Resource
	private IUserSV userSV;
	@Resource
	private ITaskSV taskSV;
	@Resource
	private ICompBaseSV compBaseSV;
	@Resource
	private FinriskResultService finriskResultService;

	
	/**
	 * @Description 跳转客户财务报表记录页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("cusFSRecord.do")
	public String toCusFSRecord(HttpServletRequest request, HttpServletResponse response) {
		
		try{
			String stage = request.getParameter("stage");
			if(StringUtil.isEmptyString(stage)){
				stage = Constants.EDIT;
			}
			request.setAttribute("stage", stage);
			String customerNo = request.getParameter("customerNo");
			request.setAttribute("customerNo", customerNo);
			User user = HttpUtil.getCurrentUser(request);
			request.setAttribute("inputUserName", user.getUserName());
			
			//加载字典数据
			String[] optTypes = {
					SysConstants.BsDicConstant.STD_SY_ODS0001,
					SysConstants.BsDicConstant.STD_SY_ODS0112,
					SysConstants.BsDicConstant.STD_SY_CFS_CYC,
					SysConstants.BsDicConstant.STD_SY_CFS_STATUS};
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
			
		} catch (Exception e) {
			log.error("跳转客户财务报表记录页面异常：",e);
		}
		
		return "views/custInfo/fs/cusFSRecord";
	}
	
	/**
	 * TODO 分页查询查询客户列表
	 * @param request
	 * @param response
	 * @param search
	 * @param order
	 * @param page
	 * @param rows
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("getCusFSRecords.json")
	@ResponseBody
	public String getCusFSRecords(HttpServletRequest request, HttpServletResponse response,
			String search, String order, int page, int rows) throws Exception {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		CusFSRecord queryCondition = null;
		
		try {
			
			if (!StringUtil.isEmptyString(search)) {
				queryCondition = JacksonUtil.fromJson(search, CusFSRecord.class);
			}
			
			if(queryCondition==null)
				queryCondition = new CusFSRecord();

			queryCondition.setStatus(Constants.NORMAL);
			
			List<Map<String, Object>> result = cusFSRecordSV.queryPaging(queryCondition,
					order, page-1, rows);
			long total = cusFSRecordSV.queryCount(queryCondition);
			
			json.put("rows", result);
			json.put("total", total);
			
		} catch (Exception e) {
			log.error(" 获取客户列表异常", e);
		}
		
		return jRequest.serialize(json, true);
	}
	
	/**
	 * TODO 保存客户财报记录
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("saveCusFSRecord.json")
	@ResponseBody
	public String saveCusFSRecord(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);
		
		try {
			
			String jsondata = request.getParameter("jsondata");
			String option = request.getParameter("option");
			if("save".equals(option))
				cusFSRecordSV.save(jsondata);
			else
				cusFSRecordSV.update(jsondata);
			
			json.put("result", true);
			
		}catch (BusinessException be) {
			json.put("msg", be.getMessage());
			log.error("保存客户财报记录失败：" + be.getMessage());
		}catch (Exception e) {
			json.put("msg", "系统异常！");
			log.error("保存客户财报记录异常：", e);
		}

		return jRequest.serialize(json, true);
	}
	
	/**
	 * TODO 删除客户报表数据
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("deleteCusFSRecord.json")
	@ResponseBody
	public String deleteCusFSRecord(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);
		
		try {
			
			String recordNo = request.getParameter("recordNo");
			
			cusFSRecordSV.deleteByNo(recordNo);
			
			json.put("result", true);
			
		}catch (Exception e) {
			json.put("msg", "系统异常，删除失败！");
			log.error("删除客户报表数据异常：", e);
		}

		return jRequest.serialize(json, true);
	}
	
	/**
	 * TODO 完成报表录入
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("finishReportInput.json")
	@ResponseBody
	public String finishReportInput(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);
		
		try {
			
			String recordNo = request.getParameter("recordNo");
			cusFSRecordSV.finishReportInput(recordNo);
			
			json.put("result", true);
			
		}catch (BusinessException be) {
			
			json.put("msg", be.getMessage());
			log.error("完成报表录入失败：" + be.getMessage());
			
		}catch (Exception e) {
			
			json.put("msg", "系统异常！");
			log.error("完成报表录入异常：", e);
		}

		return jRequest.serialize(json, true);
	}
	
	/**
	 * TODO 取消完成报表录入
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("cancelFinishInput.json")
	@ResponseBody
	public String cancelFinishInput(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);
		
		try {
			
			String recordNo = request.getParameter("recordNo");
			CusFSRecord  record = cusFSRecordSV.queryByRecordNo(recordNo);
			//非完成状态
			if(!SysConstants.CusFSRecordStatus.FINISHED.equals(record.getReportStatus()))
				ExceptionUtil.throwException("非完成状态的报表记录不允许取消！");
			
			record.setReportStatus(SysConstants.CusFSRecordStatus.NEW);
			cusFSRecordSV.saveOrUpdate(record);
			
			json.put("result", true);
			
		}catch (BusinessException be) {
			
			json.put("msg", be.getMessage());
			log.error("取消完成报表录入失败：" + be.getMessage());
			
		}catch (Exception e) {
			
			json.put("msg", "系统异常！");
			log.error("取消完成报表录入异常：", e);
		}

		return jRequest.serialize(json, true);
	}
	
	/**
	 * TODO 财务信息同步（从信贷系统）
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("syncCusFSRecord.json")
	@ResponseBody
	public String syncCusFSRecord(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);
		
		try {
			
			String recordNo = request.getParameter("recordNo");
			
			if(StringUtil.isEmpty(recordNo)){
				String customerId = request.getParameter("customerId");
				CusBase cusBase = cusBaseSV.queryByCusNo(customerId);
				cusFSRecordSV.syncFSFromCmisByCusNo(customerId, cusBase.getXdCustomerNo(), 0, 0);
			}else
				cusFSRecordSV.syncFSFromCmisByRecNo(recordNo);//同步对应报表记录数据
			
			json.put("result", true);
			
		}catch (BusinessException be) {
			
			json.put("msg", be.getMessage());
			log.error("财务信息同步异常：" + be.getMessage());
			
		}catch (Exception e) {
			
			json.put("msg", "系统异常！");
			log.error("财务信息同步异常：", e);
		}

		return jRequest.serialize(json, true);
	}
	
	/**
	 * @Description 跳转客户财报记录详情页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("viewCusFSDetail.do")
	public String viewCusFSDetail(HttpServletRequest request, HttpServletResponse response) {
		
		try{
			
			String recordNo = request.getParameter("recordNo");
			String stage = request.getParameter("stage");
			
			if(StringUtil.isEmptyString(stage)){
				stage = Constants.EDIT;
			}
			request.setAttribute("stage", stage);
			
			CusFSRecord cusFSRecord = cusFSRecordSV.queryByRecordNo(recordNo);
			request.setAttribute("cusFSRecord", cusFSRecord);
			
			CusBase cusBase = cusBaseSV.queryByCusNo(cusFSRecord.getCustomerId());
			request.setAttribute("customerName", cusBase.getCustomerName());
			request.setAttribute("customerNo", cusBase.getCustomerNo());
			
			//加载字典数据
			String[] optTypes = {
					SysConstants.BsDicConstant.STD_SY_CFS_CYC};
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
			
		} catch (Exception e) {
			log.error("跳转客户财报记录详情页面异常：",e);
		}
		
		return "views/custInfo/fs/cusFSDetail";
	}
	
	
	/**
	 * aaaaaaaaa
	 * 获取当前客户所有的财报  本地财务分析结果
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("getLocalAnalysisResult.json")
	@ResponseBody
	public ResultDto getAnalysisResult(HttpServletRequest request, HttpServletResponse response, Long dgTaskId, Long taskId) {
		ResultDto re = returnFail("");
		if(taskId==null) {
			re.setMsg("任务信息异常！请重试");
			return re;
		}
		Map<String, String> params = new HashMap<>();
		try {
			//通过返回的流水号去查询 所有分析结果 respongse 并分解入库
			//根据结果id获取到对公数据的json格式数据
			params.put("taskId", taskId+"");
			//对接本地财务分析 并保存结果
			HttpClientUtil instance2 = HttpClientUtil.getInstance(AppConfig.Cfg.LOCAL_FINRISK);
			String responseBody = instance2.doPost(params);
			responseBody = URLDecoder.decode(responseBody, "utf-8");
			//获得解析器
			JsonParser parser = new JsonParser();

			Integer resultCode = parser.parse(responseBody).getAsJsonObject().get("code").getAsInt();
			String resultMsg = parser.parse(responseBody).getAsJsonObject().get("msg").getAsString();
			String cloudResponse = parser.parse(responseBody).getAsJsonObject().get("rows").getAsString();
			
			if(resultCode==ResultDto.RESULT_CODE_FAIL) {
				re.setMsg(resultMsg);
				return re;
			}
//			finriskResultService.saveFinriskResult(dgTaskId, taskId+"", cloudResponse, Constants.Platform.LOCAL_FINRISK);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("获取本地端财务分析系统分析结果成功！");
		}catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	

/**
 * 获取当前客户所有的财报  云端财务分析结果
 * @param request
 * @param response
 * @param dgTaskId 对公系统中的任务id
 * @param taskId 这里是界面上传过来的财报云端分析结果id
 * @return
 */
	@RequestMapping("getCloudAnalysisResult.json")
	@ResponseBody
	public ResultDto getCloudAnalysisResult(HttpServletRequest request, HttpServletResponse response, Long dgTaskId, Long taskId) {
		ResultDto re = returnFail("");
		if(taskId==null) {
			re.setMsg("任务信息异常！请重试");
			return re;
		}
		Map<String, String> params = new HashMap<>();
		try {
			//通过返回的流水号去查询 所有分析结果 respongse 并分解入库
			//根据结果id获取到对公数据的json格式数据
			params.put("taskId", taskId+"");
			//对接本地财务分析 并保存结果
			HttpClientUtil instance2 = HttpClientUtil.getInstance(AppConfig.Cfg.CLOUD_FINRISK);
			String responseBody = instance2.doPost(params);
			responseBody = URLDecoder.decode(responseBody, "utf-8");
			//获得解析器
			JsonParser parser = new JsonParser();

			Integer resultCode = parser.parse(responseBody).getAsJsonObject().get("code").getAsInt();
			String resultMsg = parser.parse(responseBody).getAsJsonObject().get("msg").getAsString();
			String cloudResponse = parser.parse(responseBody).getAsJsonObject().get("rows").getAsString();
			
			if(resultCode==ResultDto.RESULT_CODE_FAIL) {
				re.setMsg(resultMsg);
				return re;
			}
//			finriskResultService.saveFinriskResult(dgTaskId, taskId+"", cloudResponse, Constants.Platform.CLOUD_FINRISK);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("获取云端财务分析系统分析结果成功！");
			
			
			/*customerNo = "20200302173440";
			String induCode = "1311";
			
			String json = getReportJsonData(customerNo, induCode);
			System.out.println(json);
			params.put("data", json);
			
			
			HttpClientUtil instance1 = HttpClientUtil.getInstance(AppConfig.Cfg.CLOUD_ANALYSIS);
			String resultTask = instance1.doPost(params);
			
			System.out.println(resultTask);
			resultTask = URLDecoder.decode(resultTask, "utf-8");
			
			//获得解析器
			JsonParser parser = new JsonParser();
			//获取根节点元素
			Integer code = parser.parse(resultTask).getAsJsonObject().get("code").getAsInt();
			String msg = parser.parse(resultTask).getAsJsonObject().get("msg").getAsString();
			String resultTaskId = parser.parse(resultTask).getAsJsonObject().get("rows").getAsString();
			if(code==ResultDto.RESULT_CODE_FAIL) {
				re.setMsg(msg);
				return re;
			}
//			String resultTaskId = "26265";
			
			//通过返回的流水号去查询 所有分析结果 respongse 并分解入库
			params.put("taskId", resultTaskId);
			

			//对接本地财务分析 并保存结果
			HttpClientUtil instance2 = HttpClientUtil.getInstance(AppConfig.Cfg.CLOUD_FINRISK);
			String responseBody = instance2.doPost(params);
			responseBody = URLDecoder.decode(responseBody, "utf-8");
			

			Integer resultCode = parser.parse(responseBody).getAsJsonObject().get("code").getAsInt();
			String resultMsg = parser.parse(responseBody).getAsJsonObject().get("msg").getAsString();
			String localResponse = parser.parse(responseBody).getAsJsonObject().get("rows").getAsString();
			
			if(resultCode==ResultDto.RESULT_CODE_FAIL) {
				re.setMsg(resultMsg);
				return re;
			}
			finriskResultService.saveFinriskResult(taskId, resultTaskId, localResponse, Constants.Platform.CLOUD_FINRISK);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("获取本地财务分析系统分析结果成功！");*/
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	/**
	 * 获取本地财务分析结果 --》针对如皋使用
	 * @param request
	 * @param response
	 * @param serialNumber 对公项目的流水号
	 * @param customerNo	//客户号
	 * @return
	 */
	@RequestMapping("getLocalFinriskAnalysis.json")
	@ResponseBody
	public ResultDto getLocalFinriskAnalysis(HttpServletRequest request, HttpServletResponse response, String serialNumber, String customerNo) {
		ResultDto re = returnFail("");
		if(serialNumber==null || "".equals(serialNumber)) {
			re.setMsg("任务信息异常！请重试");
			return re;
		}
		Map<String, String> params = new HashMap<>();
		try {


//			customerNo = "20070423005182";
//			customerNo = "CM2012080200001529";

			customerNo = "ID202007061434496534";
			serialNumber = "DGZJ20200709645";
			String induCode = "1311";

			String json = getReportJsonData(customerNo, induCode);
			System.out.println(json);
			params.put("data", json);


			HttpClientUtil instance1 = HttpClientUtil.getInstance(AppConfig.Cfg.LOCAL_FINRISK_ANALYSIS);
			String resultTask = instance1.doPost(params);

			log.info("对公传输财报数据为报文为：");
			log.info(resultTask);
			resultTask = URLDecoder.decode(resultTask, "utf-8");

			//获得解析器
			JsonParser parser = new JsonParser();
			//获取根节点元素
			Integer code = parser.parse(resultTask).getAsJsonObject().get("code").getAsInt();
			String msg = parser.parse(resultTask).getAsJsonObject().get("msg").getAsString();
			String resultTaskId = parser.parse(resultTask).getAsJsonObject().get("rows").getAsString();
			if(code==ResultDto.RESULT_CODE_FAIL) {
				re.setMsg(msg);
				return re;
			}
//			resultTaskId = "DGZJ20200619465";

			//通过返回的流水号去查询 所有分析结果 respongse 并分解入库
			params.put("taskId", resultTaskId);


			//对接本地财务分析 并保存结果
			HttpClientUtil instance2 = HttpClientUtil.getInstance(AppConfig.Cfg.CLOUD_FINRISK);
			String responseBody = instance2.doPost(params);
			responseBody = URLDecoder.decode(responseBody, "utf-8");


			Integer resultCode = parser.parse(responseBody).getAsJsonObject().get("code").getAsInt();
			String resultMsg = parser.parse(responseBody).getAsJsonObject().get("msg").getAsString();
			String localResponse = parser.parse(responseBody).getAsJsonObject().get("rows").getAsString();

			if(resultCode==ResultDto.RESULT_CODE_FAIL) {
				re.setMsg(resultMsg);
				return re;
			}
			if(!StringUtil.isEmptyString(localResponse)){
				localResponse = URLDecoder.decode(localResponse, "utf-8");
			}
			finriskResultService.saveFinriskResult(serialNumber, resultTaskId, localResponse, Constants.Platform.CLOUD_FINRISK);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("获取本地财务分析系统分析结果成功！");

		}catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}


	
	/**
	 * 获取对公项目的财务报表
	 * @param customerNo
	 * @param induCode
	 * @return
	 */
	public String getReportJsonData(String customerNo, String induCode) {
		String json = null;
		
		if(customerNo==null || "".equals(customerNo)) {
			return null;
		}
		if(induCode==null || "".equals(induCode)) {
			return null;
		}
		
		try {
			List<CusFSRecord> cusFSRecordList = cusFSRecordSV.queryByCusId(customerNo);
			//完整的与财务分析系统 传输对象
			
			CustReportData custReportData = new CustReportData(customerNo, induCode);
			List<ReportDataOneItem> reportDataOneItem = new ArrayList<>();
			
			for(int i=0; i < cusFSRecordList.size(); i++) {

				CusFSRecord cusFSRecord = cusFSRecordList.get(i);
				String recordNo = cusFSRecord.getRecordNo();
				String reportDate = cusFSRecord.getReportDate();
				
				
				String typeBalance = "balance";
				String typeIncome = "income";
				
//				String reportType = request.getParameter("reportType");
				
				//开始资产表
				//获得对应报表模型号
				String b_modelNo = FncStatMapUtil.getModelNo(cusFSRecord.getModelClass(), typeBalance);
				//取得具体报表数据
				List<SReportData> b_currentData = cusFSRecordSV.getReportData(recordNo, b_modelNo);
				//开始利润表
				String i_modelNo = FncStatMapUtil.getModelNo(cusFSRecord.getModelClass(), typeIncome);
				List<SReportData> i_currentData = cusFSRecordSV.getReportData(recordNo, i_modelNo);
				

				String year = reportDate.split("-")[0];
				String month = reportDate.split("-")[1];
				ReportDataOneItem reportItem = new ReportDataOneItem(Integer.parseInt(year), Integer.parseInt(month), "0");
				List<ReportDataSimple> reportLists = new ArrayList<>();
				
				for(SReportData data : b_currentData) {
					ReportDataSimple simple = new ReportDataSimple();
					simple.setName(data.getRowName());
					simple.setValue_early(data.getCol1Value());
					simple.setValue(data.getCol2Value());
					simple.setType("资产负债表");
					reportLists.add(simple);
				}
				for(SReportData data : i_currentData) {
					ReportDataSimple simple = new ReportDataSimple();
					simple.setName(data.getRowName());
					simple.setValue_early(data.getCol1Value());
					simple.setValue(data.getCol2Value());
					simple.setType("利润表");
					reportLists.add(simple);
				}
				reportItem.setReportLists(reportLists);
				reportDataOneItem.add(reportItem);
			}
			//上面查询当前客户的财报数据  下面上传财报 返回分析结果的流水号 resultTaskId
			custReportData.setReportDataOneItem(reportDataOneItem);
			
			json = GsonUtil.GsonString(custReportData);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return json;
	}
	
	/**
	 * @Description 跳转资产负债详情页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("cusFSInput.do")
	public String cusFSInput(HttpServletRequest request, HttpServletResponse response) {
		
		try{
			
			String recordNo = request.getParameter("recordNo");
			String reportType = request.getParameter("reportType");
			String customerNo = request.getParameter("customerNo");
			
			//客户财务报表记录
			CusFSRecord cusFSRecord = cusFSRecordSV.queryByRecordNo(recordNo);
			//获得对应报表模型号
			String modelNo = FncStatMapUtil.getModelNo(cusFSRecord.getModelClass(), reportType);
			
			//获得报表模型科目数据
			List<SReportModelRow> subjects = sReportModelSV.queryModelRowByNo(modelNo);
			
			//取得具体报表数据
			List<SReportData> currentData = cusFSRecordSV.getReportData(cusFSRecord.getRecordNo(), modelNo);
			
			//资产负债表
			if(SysConstants.CmisFianRepType.BALANCE.equals(reportType)){
				//取得上年年末数据
				List<SReportData> lastYearData = cusFSRecordSV.getLastYearData(cusFSRecord.getCustomerId(),
						cusFSRecord.getReportDate(), cusFSRecord.getModelClass(), modelNo);
				request.setAttribute("lastYearData", JacksonUtil.serialize(lastYearData));
			//利润表
			}else if(SysConstants.CmisFianRepType.INCOME.equals(reportType)){
			
			//现金流量表
			}else if(SysConstants.CmisFianRepType.CASH_FLOW_INPUT.equals(reportType)){
				
			}
//			String modelName = sReportModelSV.queryModelNameByNo(modelNo);
//			System.out.println(modelName);
//			request.setAttribute("modelName", modelName);
			request.setAttribute("currentData", JacksonUtil.serialize(currentData));
			request.setAttribute("cusFSRecord", cusFSRecord);
			request.setAttribute("modelNo", modelNo);
			request.setAttribute("reportType", reportType);
			request.setAttribute("customerNo", customerNo);
			request.setAttribute("subjects", JacksonUtil.serialize(subjects));
			
		} catch (Exception e) {
			log.error("跳转资产负债详情页面异常：",e);
		}
		
		return "views/custInfo/fs/reportInput";
	}
	
	/**
	 * @Description 跳转报表查看页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("cusFSView.do")
	public String cusFSView(HttpServletRequest request, HttpServletResponse response) {
		
		try{
			
			String recordNo = request.getParameter("recordNo");
			String reportType = request.getParameter("reportType");
			
			//客户财务报表记录
			CusFSRecord cusFSRecord = cusFSRecordSV.queryByRecordNo(recordNo);
			//获得对应报表模型号
			String modelNo = FncStatMapUtil.getModelNo(cusFSRecord.getModelClass(), reportType);
			request.setAttribute("modelName", sReportModelSV.queryModelNameByNo(modelNo));
			
			//取得具体报表数据
			List<SReportData> dataList = cusFSRecordSV.getReportData(cusFSRecord.getRecordNo(), modelNo);
			request.setAttribute("dataList", JacksonUtil.serialize(dataList));
			
			request.setAttribute("cusFSRecord", cusFSRecord);
			request.setAttribute("modelNo", modelNo);
			request.setAttribute("reportType", reportType);
			
		} catch (Exception e) {
			log.error("跳转报表查看页面异常：",e);
		}
		
		return "views/custInfo/fs/reportView";
	}
	
	/**
	 * TODO 保存财务报表数据
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("checkTurnover.do")
	@ResponseBody
	public String checkTurnover(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
			
		String dataArray = request.getParameter("dataArray");
		String customerNo = request.getParameter("customerNo");
		String reportDate = request.getParameter("reportDate");
		
		Double turnover = cusFSRecordSV.getOldReportTurnover(customerNo, reportDate);
			//注意，这里不能用JSONArray转换，会导致数据精度丢失			
		if(turnover!=null){
			List<SReportData> array = JacksonUtil.fromJson(dataArray,
					JacksonUtil.getJavaType(ArrayList.class, SReportData.class));
			for(SReportData reportData : array){
				if(reportData.getRowSubject().equals("301")){
					Double nowTurnover = reportData.getCol2Value();
					if(turnover > nowTurnover){
						json.put("checkTurnover","该利润表营业收入少于当年前期利润表营业收入值");
					}
				}
			}
		}
			
		json.put("result", true);
		
		return jRequest.serialize(json, true);
	}

	
	/**
	 * TODO 保存财务报表数据
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("saveReportData.json")
	@ResponseBody
	public String saveReportData(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);
		
		try {
		
			String cusFSRecordNo = request.getParameter("cusFSRecordNo");
			String modelNo = request.getParameter("modelNo");
			String dataArray = request.getParameter("dataArray");
			String calcFlag = request.getParameter("calFlag");
						
			cusFSRecordSV.saveReportData(cusFSRecordNo, modelNo, dataArray,
					calcFlag, HttpUtil.getCurrentUser(request));
			
			json.put("result", true);
			
		}catch (BusinessException be) {
			
			json.put("msg", be.getMessage());
			log.error("保存财务报表数据失败：" + be.getMessage());
			
		}catch (Exception e) {
			json.put("msg", "系统异常！");
			log.error("保存财务报表数据异常：", e);
		}

		return jRequest.serialize(json, true);
	}


	/**
	 * 传输报表到云平台  进行报表格式自动转换
	 * @param request
	 * @param response
	 * @param dataFile
	 * @return
	 */
	@RequestMapping("transformCompReport.json")
	@ResponseBody
	public String transformCompReport(HttpServletRequest request, HttpServletResponse response,
								 @RequestParam(value = "file", required = false) MultipartFile dataFile,
								 String customerNo, String recordNo, String reportType) {
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		// 专门给ie和火狐下使用 uploadify插件
		request.setAttribute(HandlerMapping.PRODUCIBLE_MEDIA_TYPES_ATTRIBUTE, null);
		Map<String, Object> json = new HashMap<String, Object>();
		RandomizingID random = new RandomizingID("ID", "yyyyMMddHHmmss", 4, false);

		
		String originalFilename = dataFile.getOriginalFilename();
		int lastIndexOf = originalFilename.lastIndexOf(".");
		String fileType = originalFilename.substring(lastIndexOf);
		
		try {
			CusFSRecord rsRecord = cusFSRecordSV.queryByRecordNo(recordNo);
			if(rsRecord==null){
				ExceptionUtil.throwException("当前报表编号异常，重刷新后重试");
			}
			String reportDate = rsRecord.getReportDate();
			// 流水号或者时间
			// xxx
			File target = new File(AppConfig.Cfg.TRANS_IN_PATH, random.genNewId() + fileType);
			if (!target.getParentFile().exists()) {
				target.getParentFile().mkdirs();
			}
			dataFile.transferTo(target);
			
//			//获取新旧报表准则
			String modelClass = rsRecord.getModelClass();
			int rule = 0;
			if(StringUtils.isEmpty(modelClass)){
				log.error("当前企业未确定新旧报表准则");
			}else if("010".equals(modelClass)){
				rule = 0;
			}else if("020".equals(modelClass)){
				rule = 1;
			}
			
			// 分析excel
			String outPath = this.tranToTemplate(target.getAbsolutePath(), recordNo, reportDate, rule);//本地excel导入导入
			json = this.localImport(recordNo, reportType, outPath);
			
			/*//交给公司的接口识别
			Map<String, String> resultMap = this.changeToTemplate(target, recordNo, "2020-07", "0");
			System.out.println(resultMap);

			if (resultMap.get("success") != null && resultMap.get("success").equals("true")) {
				//传输成功  -->转换后的文件地址
				String path = resultMap.get("path");

				//本地excel导入导入
				json = this.localImport(recordNo, reportType, path);

			} else {
				//失败
			}*/

		} catch (Exception e) {
			e.printStackTrace();
		}
		return jRequest.serialize(json, true);
	}

	/**
	 * 对企业电子财报进行转换
	 * @param excelFile		需要转换的文件地址
	 * @param recordNo		财报记录号
	 * @param reportDate	财报日期
	 * @return	返回转化后文件地址
	 * @throws Exception
	 */
	private String tranToTemplate(String excelFile, String recordNo, String reportDate, Integer rule) throws Exception {
		String templateExcelPath = AppConfig.Cfg.TRANS_OUT_PATH;
		String fileAddress = templateExcelPath + recordNo + File.separator + reportDate + File.separator;
		File directory = new File(fileAddress);
		if(!directory.exists()){
			directory.mkdirs();
		}
		// 文件名字需要修改为不重名，所以拼接参数
		//args1： 企业电子报告绝对路径    
		//args2： 行内模板绝对地址   
		//args3： 生成转化后的财报的完整文件夹目录
		//args4： 新旧报表准则   0新     1旧
//		String outFileName = TranformUtil.companyToBankTranform(excelFile, AppConfig.Cfg.TRANS_BANK_MODEL, fileAddress);
		String outFileName = TranformUtil.companyToBankTranform(
				excelFile,
				AppConfig.Cfg.TRANS_BANK_MODEL,
				fileAddress,
				rule
				);
//		TranformUtil.companyToBankTranform(excelFile, AppConfig.Cfg.TRANS_BANK_MODEL, fileAddress, TranformUtil.N);
		if(StringUtil.isEmptyString(outFileName)){
			ExceptionUtil.throwException("企业电子报表格式自动转化失败");
		}
		return outFileName;
	}
	
	/**
	 * 原先通过云端http请求进行访问，现在通过内嵌jar包
	 * @param excelFile
	 * @param recordNo
	 * @param reportDate
	 * @param type
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	public Map<String, String> changeToTemplate(File excelFile, String recordNo, String reportDate, String type) throws Exception {
		
		String templateExcelPath = "D:\\RG_PRO\\file\\trans\\";

		String sendExcelUrl = AppConfig.Cfg.TRANSFORM_EXCEL_RG;

		Map<String, String> returnMap = new HashMap<>();

		Map<String , String> map = new HashMap<>();

		FileInputStream fis = new FileInputStream(excelFile);
		byte[] data = new byte[fis.available()];
		fis.read(data);

		String dataStr = new BASE64Encoder().encode(data);
		String fileName = excelFile.getName();

		map.put("data", dataStr);
		map.put("name", fileName);
		map.put("reportDate", reportDate);

		String strResult = "";
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		Gson gson = new Gson();
		HttpPost httpPost = new HttpPost(sendExcelUrl);
		httpPost.setHeader("Content-Type","application/json;charset=utf-8");
		httpPost.setHeader("Accept", "application/json");

		httpPost.setEntity(new StringEntity(gson.toJson(map),"utf-8"));
		HttpResponse response = httpClient.execute(httpPost);
		int statusCode = response.getStatusLine().getStatusCode();
		if (HttpStatus.SC_OK == statusCode){
			strResult = EntityUtils.toString(response.getEntity());
		}

		if(StringUtil.isNotEmptyString(strResult)){

			Map<String, Object> resultMap = GsonUtil.GsonToMaps(strResult);

			Map<String, Object> data1 = (LinkedTreeMap<String, Object>)resultMap.get("data");
			String str = (String) data1.get("data");
			String suffix = (String)data1.get("name");
			Double code = Double.parseDouble(resultMap.get("code")+"");
			if (code == 200){
				if (suffix.toLowerCase().endsWith(".xls")){
					suffix = ".xls";
				}else if (suffix.toLowerCase().endsWith(".xlsx")){
					suffix = ".xlsx";
				}

				String fileAddress = templateExcelPath + recordNo + "/" + reportDate;
				File templateExcel = new File(fileAddress);

				if (!templateExcel.exists()){
					templateExcel.mkdirs();
				}
				// 文件名字需要修改为不重名，所以拼接参数
				String newFileName = recordNo+"_"+reportDate+"_"+type;
				fileAddress += "/" + newFileName + suffix;
				templateExcel = new File(fileAddress);

				byte[] bytes = new BASE64Decoder().decodeBuffer(str);
				ByteArrayInputStream in = new ByteArrayInputStream(bytes);

				byte[] buffer = new byte[1024];
				FileOutputStream fos = new FileOutputStream(templateExcel);
				int byteread = 0;
				while((byteread = in.read(buffer)) != -1){
					fos.write(buffer, 0, byteread);
				}
				in.close();
				fos.flush();
				fos.close();

				returnMap.put("success", "true");
				returnMap.put("path", templateExcelPath + recordNo + "/" + reportDate + "/" + newFileName + suffix);

				return returnMap;
			}else {
				String msg = (String)resultMap.get("msg");
				System.out.println(msg);
				returnMap.put("success", "false");
				returnMap.put("msg", msg);
				return returnMap;
			}
		}
		return returnMap;
	}


	/**
	 *
	 上传文件进行本地财务
	 */
	public Map<String, Object> localImport(String recordNo, String reportType, String filePath) {

		Map<String, Object> json = new HashMap<String, Object>();

		json.put("result", false);
		try {
			File mf = new File(filePath);
			if (mf==null)
				ExceptionUtil.throwException("请选择文件！");
//			MultipartFile mf = files.get(0);

			//客户财务报表记录
			CusFSRecord cusFSRecord = cusFSRecordSV.queryByRecordNo(recordNo);
			//获得对应报表模型号
			String modelNo = FncStatMapUtil.getModelNo(cusFSRecord.getModelClass(), reportType);
			String modelName = sReportModelSV.queryModelNameByNo(modelNo);

			Workbook workbook = null;
			if(mf.getName().endsWith(".xls")){
				workbook = new HSSFWorkbook(new FileInputStream(mf));
			}else if(mf.getName().endsWith(".xlsx")){
				workbook = new XSSFWorkbook(new FileInputStream(mf));
			}
			Sheet sheet = workbook.getSheet(modelName);
			if(null==sheet)
				ExceptionUtil.throwException("导入的文件中不存在名称为" + modelName + "的表格！");

			//获得报表模型科目数据
			List<SReportModelRow> subjects = sReportModelSV.queryModelRowByNo(modelNo);
			List<SReportData> currentData=new ArrayList<SReportData>();

			for(SReportModelRow data:subjects) {

				SReportData sReportData=new SReportData();
				sReportData.setRowNo(data.getRowNo());
				sReportData.setRowAttribute(data.getRowAttribute());
				sReportData.setRowName(data.getRowName());
				sReportData.setRowSubject(data.getRowSubject());
				sReportData.setDisplayOrder(data.getDisplayOrder());
				sReportData.setRowDimType(data.getRowDimType());
				currentData.add(sReportData);
			}

			if(SysConstants.CmisFianRepType.BALANCE.equals(reportType)) {

				int size=currentData.size()/2;
				int rowNumber=sheet.getLastRowNum();
				if((rowNumber-1) !=size) {
					ExceptionUtil.throwException("导入失败！导入的报表与当前报表模板不匹配!");
				}

				for(int i=0;i<size;i++) {
					if(sheet.getRow(i+2).getCell(2).getCellType()!=HSSFCell.CELL_TYPE_NUMERIC) {
						currentData.get(i).setCol1Value(0.00);
					}else {
						currentData.get(i).setCol1Value(sheet.getRow(i+2).getCell(2).getNumericCellValue());
					}

					if(sheet.getRow(i+2).getCell(3).getCellType()!=HSSFCell.CELL_TYPE_NUMERIC) {
						currentData.get(i).setCol2Value(0.00);
					}else {
						currentData.get(i).setCol2Value(sheet.getRow(i+2).getCell(3).getNumericCellValue());
					}

					if(sheet.getRow(i+2).getCell(6).getCellType()!=HSSFCell.CELL_TYPE_NUMERIC) {
						currentData.get(i+size).setCol1Value(0.00);
					}else {
						currentData.get(i+size).setCol1Value(sheet.getRow(i+2).getCell(6).getNumericCellValue());
					}

					if(sheet.getRow(i+2).getCell(7).getCellType()!=HSSFCell.CELL_TYPE_NUMERIC) {
						currentData.get(i+size).setCol2Value(0.00);
					}else {
						currentData.get(i+size).setCol2Value(sheet.getRow(i+2).getCell(7).getNumericCellValue());
					}
				}
			}else {

				int size=currentData.size();
				int rowNumber=sheet.getLastRowNum();
				if((rowNumber-1) !=size)
					ExceptionUtil.throwException("导入失败！导入的报表与当前报表模板不匹配!");

				for(int i=0;i<size;i++) {
					if(sheet.getRow(i+2).getCell(2).getCellType()!=HSSFCell.CELL_TYPE_NUMERIC) {
						currentData.get(i).setCol2Value(0.00);
					}else {
						currentData.get(i).setCol2Value(sheet.getRow(i+2).getCell(2).getNumericCellValue());
					}

				}
			}
			json.put("currentData", JacksonUtil.serialize(currentData));

			json.put("result", true);
		}catch(BusinessException be){
			log.error("本地报表导入异常：" + be.getMessage());
			json.put("msg", be.getMessage());
		}catch(Exception e){
			log.error("导入文件异常：", e);
			json.put("msg", "系统异常，导入失败！");
		}
		return json;
	}
}
