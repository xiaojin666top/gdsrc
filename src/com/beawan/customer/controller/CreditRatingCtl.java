package com.beawan.customer.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.beawan.survey.custInfo.dto.CompBaseDto;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.base.entity.User;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.customer.bean.CreditRating;
import com.beawan.customer.bean.CusFSRecord;
import com.beawan.customer.service.ICreditRateSV;
import com.beawan.customer.service.ICusFSRecordSV;
import com.beawan.customer.utils.RatingCountUtil;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.HttpUtil;

import net.sf.json.JSONObject;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/credit" })
public class CreditRatingCtl {

	private static Logger log = Logger.getLogger(CreditRatingCtl.class);

	@Resource
	private ICreditRateSV creditRateSV;

	@Resource
	private ICompBaseSV compBaseSV;

	@Resource
	private ICusFSRecordSV cusFSRecordSV;

	/**
	 * TODO 客户评级
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("creditRating.do")
	public String creditRating(HttpServletRequest request, HttpServletResponse response) throws Exception {

		HashMap<String, Object> map = new HashMap<String, Object>();

		try {

			String customerNo = request.getParameter("customerNo");
			request.setAttribute("customerNo", customerNo);

			List<CusFSRecord> cusFSRecords = cusFSRecordSV.queryByCusIdDesc(customerNo);

			if (null == cusFSRecords || cusFSRecords.size() <= 0) {

				request.setAttribute("isRecord", false);

			} else {

				List<String> reportDate = new ArrayList<String>();

				for (CusFSRecord data : cusFSRecords) {
					reportDate.add(data.getReportDate());
				}
				request.setAttribute("reportDate", reportDate);

				String date = reportDate.get(0);

				request.setAttribute("date", date);

				List<CreditRating> list = creditRateSV.queryCredit(customerNo, date);

				if (list == null || list.size() == 0) {
					list = creditRateSV.createCreditRating(customerNo, date);
				}

				list = creditRateSV.sysnFinacing(customerNo, date, list);

				double score = RatingCountUtil.getResult(list);
				String level = RatingCountUtil.getLevel(score);

				CompBaseDto compBase = compBaseSV.findCompBaseByCustNo(customerNo);
				compBase.setCreditLevel(level);
				compBaseSV.saveCompBase(compBase);

				request.setAttribute("score", score);
				request.setAttribute("level", level);

				for (CreditRating data : list) {
					map.put(data.getEnName(), data);
				}

				request.setAttribute("credit", map);

				request.setAttribute("isRecord", true);

			}
			User user = HttpUtil.getCurrentUser(request);
			request.setAttribute("isTest", user.getRoleNo().contains("0009") ? "0" : "1");
		} catch (Exception e) {
			log.error("跳转评级页面异常", e);
		}

		return "views/custInfo/creditRating";
	}

	@RequestMapping("creditRatingDetail.do")
	public String creditRatingDetail(HttpServletRequest request, HttpServletResponse response) {
		String date = request.getParameter("reportDate");
		String customerNo = request.getParameter("customerNo");

		HashMap<String, Object> map = new HashMap<String, Object>();

		try {

			List<CusFSRecord> cusFSRecords = cusFSRecordSV.queryByCusIdDesc(customerNo);

			List<String> reportDate = new ArrayList<String>();

			for (CusFSRecord data : cusFSRecords) {
				reportDate.add(data.getReportDate());
			}
			request.setAttribute("reportDate", reportDate);

			request.setAttribute("date", date);
			request.setAttribute("customerNo", customerNo);

			List<CreditRating> list = creditRateSV.queryCredit(customerNo, date);

			if (list == null || list.size() == 0) {
				list = creditRateSV.createCreditRating(customerNo, date);
			}

			list = creditRateSV.sysnFinacing(customerNo, date, list);

			double score = RatingCountUtil.getResult(list);
			String level = RatingCountUtil.getLevel(score);

			CompBaseDto compBase = compBaseSV.findCompBaseByCustNo(customerNo);
			compBase.setCreditLevel(level);
			compBaseSV.saveCompBase(compBase);

			request.setAttribute("score", score);
			request.setAttribute("level", level);

			for (CreditRating data : list) {
				map.put(data.getEnName(), data);
			}

			request.setAttribute("credit", map);
		} catch (Exception e) {

			log.error("载入信息异常", e);
		}
		return "views/custInfo/creditRatDetail";
	}

	/**
	 * TODO 保存客户评级
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("saveCredit.do")
	@ResponseBody
	public String saveCreditRating(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String newData = request.getParameter("newData");
		String customerNo = request.getParameter("customerNo");
		String reportDate = request.getParameter("reportDate");

		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();

		try {
			List<CreditRating> list = creditRateSV.createCreditRating(customerNo, reportDate);

			JSONObject jsonObject = JSONObject.fromObject(newData);

			for (int i = 0; i < list.size(); i++) {
				String enName = list.get(i).getEnName();
				Double value = Double.valueOf(jsonObject.get(enName).toString());
				list.get(i).setValue(value);
				list.get(i).setScore((Double)RatingCountUtil.countScore(enName, value)[0]);
			}

			creditRateSV.saveCreditRateList(customerNo, reportDate, list);

			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			log.error("保存评级情况异常", e);
		}

		return jRequest.serialize(json, true);
	}

	/**
	 * TODO 日期变更
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("changeDate.do")
	public String changeCreditRating(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String date = request.getParameter("reportDate");
		String customerNo = request.getParameter("customerNo");

		HashMap<String, Object> map = new HashMap<String, Object>();

		try {

			List<CusFSRecord> cusFSRecords = cusFSRecordSV.queryByCusIdDesc(customerNo);

			List<String> reportDate = new ArrayList<String>();

			for (CusFSRecord data : cusFSRecords) {
				reportDate.add(data.getReportDate());
			}
			request.setAttribute("reportDate", reportDate);

			request.setAttribute("date", date);
			request.setAttribute("customerNo", customerNo);

			List<CreditRating> list = creditRateSV.queryCredit(customerNo, date);

			if (list == null || list.size() == 0) {
				list = creditRateSV.createCreditRating(customerNo, date);
			}

			list = creditRateSV.sysnFinacing(customerNo, date, list);

			double score = RatingCountUtil.getResult(list);
			String level = RatingCountUtil.getLevel(score);

			CompBaseDto compBase = compBaseSV.findCompBaseByCustNo(customerNo);
			compBase.setCreditLevel(level);
			compBaseSV.saveCompBase(compBase);

			request.setAttribute("score", score);
			request.setAttribute("level", level);

			for (CreditRating data : list) {
				map.put(data.getEnName(), data);
			}

			request.setAttribute("credit", map);
			request.setAttribute("isRecord", true);
		} catch (Exception e) {

			log.error("载入信息异常", e);
		}

		return "views/custInfo/creditRating";
	}
}
