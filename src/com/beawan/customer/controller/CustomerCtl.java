package com.beawan.customer.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.base.entity.User;
import com.beawan.base.service.IPlatformLogSV;
import com.beawan.base.service.ISysDicSV;
import com.beawan.base.service.IUserSV;
import com.beawan.common.BusinessException;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.customer.bean.Admittance;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.qbean.QCusBase;
import com.beawan.customer.service.IAdmittanceSV;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.customer.service.ICusFSRecordSV;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.beawan.model.entity.AdmitResult;
import com.beawan.model.service.AdmitResultService;
import com.beawan.qcc.service.SynchronizationQCCService;
import com.beawan.survey.custInfo.dto.CompBaseDto;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.DateUtil;
import com.platform.util.HttpUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/customer" })
public class CustomerCtl extends BaseController{

	private static Logger log = Logger.getLogger(CustomerCtl.class);

	@Resource
	private ICmisInInvokeSV cmisInInvokeSV;
	@Resource
	private ICusBaseSV cusBaseSV;
	@Resource
	private ISysDicSV sysDicSV;
	@Resource
	private IPlatformLogSV platformLogSV;
	@Resource
	private IUserSV userSV;
	@Resource
	private ITaskSV taskSV;
	@Resource
	private ICompBaseSV compBaseSV;
	@Resource
	private IAdmittanceSV admittanceSV;
	@Resource
	private AdmitResultService admitResultService;
	@Resource
	private ICusFSRecordSV cusFSRecordSV;
	@Resource
	private SynchronizationQCCService synchronizationQCCService;
	
	
	/**
	 * @Description 跳转我的客户页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("myCustomer.do")
	public String toQueryCustPage(HttpServletRequest request, HttpServletResponse response) {
		
		try{
			User user = HttpUtil.getCurrentUser(request);
			request.setAttribute("user", user);
			
			//加载字典数据
			String[] optTypes = {
					SysConstants.BsDicConstant.STD_SY_ODS0001};
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
		
		} catch (Exception e) {
			log.error("跳转我的客户页面异常：",e);
		}
		
		return "views/custInfo/myCustomer";
	}
	
	/**
	 * @Description 跳转我的客户页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("unContiCust.do")
	public String unContiCust(HttpServletRequest request, HttpServletResponse response) {
		
		try{
			User user = HttpUtil.getCurrentUser(request);
			request.setAttribute("user", user);
			
			//加载字典数据
			String[] optTypes = {
					SysConstants.BsDicConstant.STD_SY_ODS0001};
			request.setAttribute("dicData", sysDicSV.queryMapListByTypes(optTypes));
		
		} catch (Exception e) {
			log.error("跳转我的客户页面异常：",e);
		}
		
		return "views/custInfo/unContiCust";
	}
	

	/**
	 * 同步 企业财报数据
	 * @param customerNo
	 * @return
	 */
	@RequestMapping("syncCusSReport.json")
	@ResponseBody
	public ResultDto syncCusSReport(String customerNo){
		ResultDto re = returnFail();
		if(StringUtils.isEmpty(customerNo)){
			re.setMsg("客户号为空");
			return re;
		}
		try{
			CusBase cusBase = cusBaseSV.queryByCusNo(customerNo);
			if(cusBase==null){
				re.setMsg("客户信息异常，无当前编号的客户基本信息");
				return re;
			}
			//判断是否同步财报数据   -->每一天打开时候都会去同步//同步财报
			String finaSyncTime = cusBase.getFinaSyncTime();
			if(StringUtils.isEmpty(finaSyncTime) 
					|| DateUtil.getTime(DateUtil.parse(finaSyncTime, Constants.DATETIME_MASK), new Date(), DateUtil.SKIP_TYPE_DAY ) > 1){
				cusFSRecordSV.syncFSFromCmisByCusNo(cusBase.getCustomerNo(), cusBase.getXdCustomerNo(), 0, 0);
				cusBase.setFinaSyncTime(DateUtil.getNowDatetime());
				cusBaseSV.save(cusBase);
			}
			
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("同步客户财报数据成功");
		}catch(Exception e){
			log.error("检查同步客户号为"+customerNo + "操作失败");
			e.printStackTrace();
		}
		return re;
	}


	/**
	 * 检查当前客户企查查数据是否同步 或 同步时间是否过久
	 * @param customerNo
	 * @return
	 */
	@RequestMapping("checkQccSync.json")
	@ResponseBody
	public ResultDto checkQccSync(String customerNo){
		ResultDto re = returnFail();
		try{

			CusBase cusBase = cusBaseSV.queryByCusNo(customerNo);
			String syncTime = cusBase.getQccSyncTime();
			if(syncTime==null || 
					DateUtil.getSkip(new Date(), DateUtil.parse(syncTime, "yyyy-MM-dd"), DateUtil.SKIP_TYPE_DAY) >=30){
				re.setRows(syncTime);
				re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * @Description 跳转客户信息页面
	 * @param request
	 * @param customerNo
	 * @param flag		1进行同步操作，默认为空不进行
	 * @return
	 */
	@RequestMapping("viewCustomer.do")
	public ModelAndView viewCustomer(HttpServletRequest request, String customerNo,String flag) {
		ModelAndView mav = new ModelAndView("views/custInfo/customerIndex");
		try{
			User user = HttpUtil.getCurrentUser(request);
			CusBase cusBase = cusBaseSV.queryByCusNo(customerNo);
			
			// 去信贷系统查询 客户经理名称为 userName的 然后将客户经理的id插入Crm表中
			String syncTime = cusBase.getQccSyncTime();
			if("1".equals(flag) && (syncTime==null || 
					DateUtil.getSkip(new Date(), DateUtil.parse(syncTime, "yyyy-MM-dd"), DateUtil.SKIP_TYPE_DAY) >=30)){
				synchronizationQCCService.syncQccAllData(cusBase.getCustomerNo());
				cusBase.setQccSyncTime(DateUtil.getNowY_m_dStr());
				cusBaseSV.save(cusBase);
			}
			
			//获取是否存在准入信息
			Integer maxTimes = admitResultService.getMaxTimes(customerNo);
			Map<String, Object> params = new HashMap<>();
			params.put("times", maxTimes);
			params.put("custNo", customerNo);
			List<AdmitResult> admitResultList = admitResultService.selectByProperty(params);

			mav.addObject("user", user);
			mav.addObject("customerNo", customerNo);
			mav.addObject("customerName", cusBase.getCustomerName());
			mav.addObject("admitList", admitResultList);
		} catch (Exception e) {
			e.printStackTrace();
			log.error("跳转客户信息页面异常：",e);
		}
		return mav;
	}
	
	/***
	 * 检查是否准入
	 * @param request
	 * @param response 
	 * @return
	 */
	@RequestMapping("checkCredit.do")
	@ResponseBody
	public Map<String, Object> checkCredit(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String customerNo = request.getParameter("customerNo");
			Admittance admittance = admittanceSV.getAdmittanceByCustmoerNo(customerNo);
			admittance = new Admittance();
			admittance.setAdmittanceResult("0");
			map.put("admittance", admittance);
		} catch (Exception e) {
			log.error("跳转新增授信页面异常", e);
		}
		
		return map;
	}
	
	/**
	 * @Description 跳转申请授信页面
	 * @param request
	 * @return
	 */
	@RequestMapping("newCredit.do")
	public ModelAndView newCredit(HttpServletRequest request, String id) {
		ModelAndView mav = new ModelAndView("views/custInfo/newCredit");

		String customerNo = null;
		User user = HttpUtil.getCurrentUser(request);
		
		try {
			if(!StringUtil.isEmptyString(id)){
				//从营销过来的任务 进行确认流程 注意：此时贷前任务已经生成
				Task task = taskSV.getTaskById(Long.parseLong(id));
				customerNo = task.getCustomerNo();
				Integer year = task.getYear();
				Integer month = task.getMonth();
				String finaDate = "";
				if(month < 10){
					finaDate = year + "-0" + month;
				}else{
					finaDate = year + "-" + month;
				}
				Integer loanTerm = task.getLoanTerm();
				Double loanNumber = task.getLoanNumber();
				String assureMeans = task.getAssureMeans();
				Integer productId = task.getProductId(); 
				
				
				//mav.addObject("finaDate", finaDate);
				mav.addObject("finaDate", new Date().toString());
				mav.addObject("productId", productId);
				mav.addObject("loanTerm", loanTerm);
				mav.addObject("loanNumber", loanNumber);
				mav.addObject("assureMeans", assureMeans);
				mav.addObject("id", id);

			}else {
				//从客户列表进行新增授信
				customerNo = request.getParameter("customerNo");
				CusBase cusBase = cusBaseSV.queryByCusNo(customerNo);
				String finaSyncTime = cusBase.getFinaSyncTime();
				
				
				
				//同步财报
				if(StringUtils.isEmpty(finaSyncTime) 
						|| DateUtil.getTime(DateUtil.parse(finaSyncTime, Constants.DATETIME_MASK), new Date(), DateUtil.SKIP_TYPE_DAY ) > 1){
					cusFSRecordSV.syncFSFromCmisByCusNo(cusBase.getCustomerNo(), cusBase.getXdCustomerNo(), 0, 0);
					cusBase.setFinaSyncTime(DateUtil.getNowDatetime());
					cusBaseSV.save(cusBase);
				}
				
				
				
			}

			//涉及到了CMIs下的表
			//String reportDate = cusFSRecordSV.getMaxReportDate(customerNo);
			String reportDate = new Date().toString();
			mav.addObject("userName", user.getUserName());
			mav.addObject("reportDate", reportDate);
			mav.addObject("orgNo", user.getAccOrgNo());
			mav.addObject("orgName", "cmis下的orgName");
			//mav.addObject("orgName", cmisInInvokeSV.queryOrgName(user.getAccOrgNo()));

			//加载字典数据
			String[] optTypes = {SysConstants.BsDicConstant.STD_PRO_RG,/** @Field STD_SY_DATA_ACCESS :如皋业务品种*/
					SysConstants.BsDicConstant.STD_SY_COM_SURVEY_TYPE};//(企业授信调查报告模板类型)
			//调查模板：在这里！！！
			String s = sysDicSV.queryMapListByTypes(optTypes, user);
			System.out.println(s);
			mav.addObject("dicData",s);

			CusBase cusBase = cusBaseSV.queryByCusNo(customerNo);
			mav.addObject("customerNo", customerNo);
			mav.addObject("customerName", cusBase.getCustomerName());

			CompBaseDto compBase = compBaseSV.findCompBaseByCustNo(customerNo);
			if (compBase != null) {
				mav.addObject("industryCode", compBase.getIndustryCode());
				mav.addObject("industryName", compBase.getIndustryType());
			}
		
		} catch (Exception e) {
			log.error("跳转新增授信页面异常", e);
		}
		
		return mav;
	}
	
	/**
	 * TODO 分页查询查询客户列表
	 * @param request
	 * @param response
	 * @param search
	 * @param order
	 * @param page
	 * @param rows
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("getCustomers.json")
	@ResponseBody
	public ResultDto getCustomers(HttpServletRequest request, HttpServletResponse response,
			String search, String order, int page, int rows) throws Exception {
		
		ResultDto re = returnFail("");
		User user = HttpUtil.getCurrentUser(request);
		QCusBase queryCondition = null;
		
		try {
			if (!StringUtil.isEmptyString(search)) {
				queryCondition = JacksonUtil.fromJson(search, QCusBase.class);
			}
			if(queryCondition==null)
				queryCondition = new QCusBase();
			
			String userDA = user.getDataAccess();
			if(SysConstants.UserDataAccess.PERSONAL.equals(userDA))
				queryCondition.setManagerUserId(user.getUserId());
			else if(SysConstants.UserDataAccess.ORG.equals(userDA))
				queryCondition.setManagerOrgId(user.getAccOrgNo());
			
			List<Map<String, Object>> result = cusBaseSV.queryPaging(queryCondition,
					order, page-1, rows);
			long total = cusBaseSV.queryCount(queryCondition);
			
			re = returnSuccess();
			re.setRows(result);
			re.setTotal(total);
			re.setMsg("请求成功！");
		} catch (Exception e) {
			log.error(" 获取客户列表异常", e);
		}
		
		return re;
	}

	/**
	 * TODO 新增客户保存
	 * @param request
	 * @return
	 */
	@RequestMapping("saveNewCustomer.json")
	@ResponseBody
	public ResultDto saveNewCustomer(HttpServletRequest request){
		ResultDto re = returnFail("保存数据异常！");
		String jsonData = request.getParameter("jsondata");
		try {
			CusBase cusBase = JacksonUtil.fromJson(jsonData, CusBase.class);
			if (cusBase == null) return returnFail("传输数据为空，请重试！");
			
			String customerName = cusBase.getCustomerName();
			CusBase existEntity = cusBaseSV.queryByCusName(customerName);
			if (existEntity != null){
				String managerUser = userSV.queryNameByNos(existEntity.getManagerUserId());
				return returnFail("该客户已存在，由客户经理：" + managerUser + " 管户！");
			}

			User user = HttpUtil.getCurrentUser(request);
			cusBase.setCustomerNo(StringUtil.genRandomNo());
			cusBase.setInputUserId(user.getUserId());
			cusBase.setInputOrgId(user.getAccOrgNo());
			cusBase.setManagerUserId(user.getUserId());
			cusBase.setManagerOrgId(user.getAccOrgNo());
			cusBase.setInputDate(DateUtil.getNowYmdStr());
			cusBase.setUpdateDate(DateUtil.getNowYmdStr());
			cusBase.setStatus(Constants.NORMAL);
			cusBase.setCreater(user.getUserId());
			cusBase.setUpdater(user.getUserId());
			cusBase.setCreateTime(DateUtil.getNowTimestamp());
			cusBase.setUpdateTime(DateUtil.getNowTimestamp());
			cusBaseSV.save(cusBase);
			//同步企查查数据
			synchronizationQCCService.syncQccAllData(cusBase.getCustomerNo());
			
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("保存成功！");
		}catch (BusinessException e) {
			re.setMsg(e.getMessage());
		}catch (Exception e) {
			re.setMsg("系统异常！");
			log.error("新增客户保存异常：", e);
		}
		return re;
	}
	
	/**
	 * TODO 删除客户
	 * @param customerNo
	 * @return
	 */
	@RequestMapping("deleteCustomer.json")
	@ResponseBody
	public ResultDto deleteCustomer(HttpServletRequest request, String customerNo){
		ResultDto re = returnFail("删除数据异常！");
		try {
			List<Task> list = taskSV.getTaskByCustmerNo(customerNo);
			if(!CollectionUtils.isEmpty(list))
				return returnFail("该客户已有业务信息，无法删除！");
			
			User user = HttpUtil.getCurrentUser(request);
			CusBase cusBase = cusBaseSV.queryByCusNo(customerNo);
			cusBase.setStatus(Constants.DELETE);//删除
			cusBase.setUpdater(user.getUserId());
			cusBase.setUpdateTime(DateUtil.getNowTimestamp());
			cusBaseSV.save(cusBase);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除数据成功！");
		}catch (BusinessException be) {
			re.setMsg(be.getMessage());
			log.error(be.getMessage()+be);
		}catch (Exception e) {
			re.setMsg("系统异常！");
			log.error("客户基本信息同步异常：", e);
		}
		return re;
	}
	
	/**
	 * TODO 客户基本信息同步（从信贷系统）
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("syncCustomer.json")
	@ResponseBody
	public String syncCustomer(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		
		try {
			
			String customerNo = request.getParameter("customerNo");
			
			cmisInInvokeSV.syncCusInfo(customerNo);
			
			json.put("result", true);
			
		}catch (Exception e) {
			
			json.put("result", false);
			json.put("msg", "系统异常！");
			log.error("客户基本信息同步异常：", e);
		}

		return jRequest.serialize(json, true);
	}
	
	
}
