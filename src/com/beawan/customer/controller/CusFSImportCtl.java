package com.beawan.customer.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;

import com.beawan.analysis.finansis.bean.FnFixedAssetsBuilding;
import com.beawan.analysis.finansis.bean.FnFixedAssetsEquipment;
import com.beawan.analysis.finansis.bean.FnFixedAssetsLand;
import com.beawan.analysis.finansis.bean.FnInventory;
import com.beawan.analysis.finansis.bean.FnOtherPay;
import com.beawan.analysis.finansis.bean.FnOtherReceive;
import com.beawan.analysis.finansis.bean.FnPay;
import com.beawan.analysis.finansis.bean.FnPreReceive;
import com.beawan.analysis.finansis.bean.FnPrepay;
import com.beawan.analysis.finansis.bean.FnReceive;
import com.beawan.analysis.finansis.service.IFnTableService;
import com.beawan.base.entity.SReportData;
import com.beawan.base.entity.SReportModelRow;
import com.beawan.base.service.IPlatformLogSV;
import com.beawan.base.service.ISReportDataSV;
import com.beawan.base.service.ISReportModelSV;
import com.beawan.base.service.ISReportRecordSV;
import com.beawan.base.service.ISysDicSV;
import com.beawan.base.service.IUserSV;
import com.beawan.common.BusinessException;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.customer.bean.CusFSRecord;
import com.beawan.customer.dto.AnnoEntity;
import com.beawan.customer.dto.Coordinate;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.customer.service.ICusFSRecordSV;
import com.beawan.customer.utils.ItemUtil;
import com.beawan.customer.utils.NumericUtil;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.beawan.exterinvoke.util.FncStatMapUtil;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.beawan.task.service.ITaskSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.ExcelUtil;
import com.platform.util.ExceptionUtil;
import com.platform.util.HttpUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.PropertiesUtil;

import net.sf.json.JSONArray;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/cusfs/import" })
public class CusFSImportCtl {

	private static Logger log = Logger.getLogger(CusFSImportCtl.class);
	
	private static String dataTempPath = PropertiesUtil.getProperty(Constants.SYSTEM_PROPS_PATH, "data.temp.path");//本地缓存地址

	private String errorMsg;
	
	@Resource
	private ICmisInInvokeSV cmisInInvokeSV;
	@Resource
	private ICusBaseSV cusBaseSV;
	@Resource
	private ICusFSRecordSV cusFSRecordSV;
	@Resource
	private ISReportRecordSV sReportRecordSV;
	@Resource
	private ISReportDataSV sReportDataSV;
	@Resource
	private ISReportModelSV sReportModelSV;
	@Resource
	private ISysDicSV sysDicSV;
	@Resource
	private IPlatformLogSV platformLogSV;
	@Resource
	private IUserSV userSV;
	@Resource
	private ITaskSV taskSV;
	@Resource
	private ICompBaseSV compBaseSV;
	@Resource
	private IFnTableService fnTableService ;
	
	/**
	 * 导入excel文件
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("uploadExcel.json")
	@ResponseBody
	public String uploadExcel(HttpServletRequest request, HttpServletResponse response){
		
		InputStream is = null;
		
		OutputStream os = null;
		
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		
		Map<String, Object> json = new HashMap<String, Object>();
		
		json.put("result", false);
		
		try {
			
			String recordNo = HttpUtil.getAsString(request, "recordNo");
			
			String reportType = HttpUtil.getAsString(request,  "reportType");
			
			List<MultipartFile> files = ((MultipartRequest) request).getFiles("file");
			
			if (CollectionUtils.isEmpty(files))
				ExceptionUtil.throwException("请选择文件！");
				
			MultipartFile mf = files.get(0); 
			
			//客户财务报表记录
			CusFSRecord cusFSRecord = cusFSRecordSV.queryByRecordNo(recordNo);
			//获得对应报表模型号
			String modelNo = FncStatMapUtil.getModelNo(cusFSRecord.getModelClass(), reportType);
			String modelName = sReportModelSV.queryModelNameByNo(modelNo);

			Workbook workbook = null;
			if(mf.getOriginalFilename().endsWith(".xls")){
				workbook = new HSSFWorkbook(mf.getInputStream());
			}else if(mf.getOriginalFilename().endsWith(".xlsx")){
				workbook = new XSSFWorkbook(mf.getInputStream());
			}
			Sheet sheet = workbook.getSheet(modelName);
			if(null==sheet)
				ExceptionUtil.throwException("导入的文件中不存在名称为" + modelName + "的表格！");
			
			//获得报表模型科目数据
			List<SReportModelRow> subjects = sReportModelSV.queryModelRowByNo(modelNo);
			List<SReportData> currentData=new ArrayList<SReportData>();
				
			for(SReportModelRow data:subjects) {
				
				SReportData sReportData=new SReportData();
												
				sReportData.setRowNo(data.getRowNo());
				
				sReportData.setRowAttribute(data.getRowAttribute());
				
				sReportData.setRowName(data.getRowName());
				
				sReportData.setRowSubject(data.getRowSubject());
				
				sReportData.setDisplayOrder(data.getDisplayOrder());
				
				sReportData.setRowDimType(data.getRowDimType());
				
				currentData.add(sReportData);
			}
				
			if(SysConstants.CmisFianRepType.BALANCE.equals(reportType)) {
								
				int size=currentData.size()/2;
				
				int rowNumber=sheet.getLastRowNum();
						
				if((rowNumber-1) !=size) {
					ExceptionUtil.throwException("导入失败！导入的报表与当前报表模板不匹配!");
				}
				
				for(int i=0;i<size;i++) {
					
					if(sheet.getRow(i+2).getCell(2).getCellType()!=HSSFCell.CELL_TYPE_NUMERIC) {
						
						currentData.get(i).setCol1Value(0.00);
					
					}else {
					
						currentData.get(i).setCol1Value(sheet.getRow(i+2).getCell(2).getNumericCellValue());
				
					}
				
					if(sheet.getRow(i+2).getCell(3).getCellType()!=HSSFCell.CELL_TYPE_NUMERIC) {
					
						currentData.get(i).setCol2Value(0.00);
					
					}else {
					
						currentData.get(i).setCol2Value(sheet.getRow(i+2).getCell(3).getNumericCellValue());
				
					}
				
					if(sheet.getRow(i+2).getCell(6).getCellType()!=HSSFCell.CELL_TYPE_NUMERIC) {
						
						currentData.get(i+size).setCol1Value(0.00);
					
					}else {
					
						currentData.get(i+size).setCol1Value(sheet.getRow(i+2).getCell(6).getNumericCellValue());
				
					}
				
					if(sheet.getRow(i+2).getCell(7).getCellType()!=HSSFCell.CELL_TYPE_NUMERIC) {
					
						currentData.get(i+size).setCol2Value(0.00);
					
					}else {
					
						currentData.get(i+size).setCol2Value(sheet.getRow(i+2).getCell(7).getNumericCellValue());
				
					}
				}
			}else {
				
				int size=currentData.size();
				
				int rowNumber=sheet.getLastRowNum();
				
				if((rowNumber-1) !=size)
					ExceptionUtil.throwException("导入失败！导入的报表与当前报表模板不匹配!");
				
				for(int i=0;i<size;i++) {
					
					if(sheet.getRow(i+2).getCell(2).getCellType()!=HSSFCell.CELL_TYPE_NUMERIC) {
						
						currentData.get(i).setCol2Value(0.00);
						
					}else {
						
					currentData.get(i).setCol2Value(sheet.getRow(i+2).getCell(2).getNumericCellValue());
					
					}
					
				}
			}
						
			json.put("currentData", JacksonUtil.serialize(currentData));
			
			json.put("result", true);

		}catch(BusinessException be){
			
			log.error("报表导入异常：" + be.getMessage());
			json.put("msg", be.getMessage());
			
		}catch(Exception e){
			
			log.error("导入文件异常：", e);
			json.put("msg", "系统异常，导入失败！");
			
		}finally{
			try {
				if(is != null)
					is.close();
				if(os != null)
					os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		 
		return jRequest.serialize(json, true);
	}
	
	/**
	 * 读取附注内容
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("uploadAnnoExcel.json")
	@ResponseBody
	public String uploadAnnoExcel(HttpServletRequest request, HttpServletResponse response, long taskId){
		
		InputStream is = null;
		OutputStream os = null;
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("result", false);
		try {
			String recordNo = HttpUtil.getAsString(request, "recordNo");
			String reportType = HttpUtil.getAsString(request,  "reportType");
			List<MultipartFile> files = ((MultipartRequest) request).getFiles("file");
			if (CollectionUtils.isEmpty(files))
				ExceptionUtil.throwException("请选择文件！");
			MultipartFile mf = files.get(0); 
			String name = mf.getOriginalFilename();
			// 打开工作表
			is = mf.getInputStream();
			Workbook workbook = null;
			if (name.endsWith("xls")) {
				workbook = new HSSFWorkbook(is);
			} else if (name.endsWith("xlsx")) {
				workbook = new XSSFWorkbook(is);
			} else {
				ExceptionUtil.throwException("导入的文件不是xls、xlsx格式！");
			}
			// 获取工作表数量
			int sheetCount = workbook.getNumberOfSheets();
			// 获取第一张工作表
			if (sheetCount == 0) {
				ExceptionUtil.throwException("导入失败！表格内容为空");
			}
//			for (int x = 0; x < sheetCount; x++) {
			Sheet sheet1 = workbook.getSheetAt(0);

			List<AnnoEntity> annoList = new ArrayList<AnnoEntity>();
			// 获取行列数
			int lastRow = sheet1.getLastRowNum() + 1;
			// 遍历每一行 获得最大的列标
			int lastCol = 0;
			for (Row r : sheet1) {
				int temp = r.getLastCellNum();
				if (temp > lastCol)
					lastCol = temp;
			}
			FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
			int unit = 10000;

			// 获取工作表的名字
			String lName = sheet1.getSheetName();
			// 当前sheet页的类型 设置是 负债表还是 利润表
			Integer sheetType;

			// 科目名的列
			List<Coordinate> itemCols = new ArrayList<Coordinate>();

			List<Coordinate> valList = new ArrayList<Coordinate>();// 金额 所在列位置
			List<Coordinate> rateList = new ArrayList<Coordinate>();// 比率 列位置
			// 获得各个关键字的位置
			for (int i = 0; i < lastRow; i++) {
				for (int j = 0; j < lastCol; j++) {
					Row row =  sheet1.getRow(i);
					if(row==null){
						continue;
					}
					Cell cell = row.getCell(j);

					String value = ExcelUtil.getCellValue(cell);
					if (value == null || "".equals(value))
						continue;

					value = ItemUtil.preProcess(value);
					/** 获取科目名的列   开始***/
					if (value.contains("货币资金")) {
						Coordinate coor = new Coordinate(i, j, Constants.Anno.MONETARY_FUND);
						itemCols.add(coor);
					}
					if ("应收账款".equals(value)||"应收账款明细账".equals(value)||"应收款项".equals(value)) {
						Coordinate coor = new Coordinate(i, j, Constants.Anno.ACCOUNT_RECE);
						itemCols.add(coor);
					}
					if (value.contains("存货")) {
						Coordinate coor = new Coordinate(i, j, Constants.Anno.STOCK);
						itemCols.add(coor);
					}
					if ("应付账款".equals(value)||"应付账款明细账".equals(value)) {
						Coordinate coor = new Coordinate(i, j, Constants.Anno.ACCOUNT_PAY);
						itemCols.add(coor);
					}
					if (value.contains("固定资产")) {
						Coordinate coor = new Coordinate(i, j, Constants.Anno.FIXED_ASSET);
						itemCols.add(coor);
					}
					if (value.contains("其他应收款")) {
						Coordinate coor = new Coordinate(i, j, Constants.Anno.OTHER_RECE);
						itemCols.add(coor);
					}
					if (value.contains("长期投资")) {
						Coordinate coor = new Coordinate(i, j, Constants.Anno.LONG_INVEST);
						itemCols.add(coor);
					}
					if (value.contains("在建工程")) {
						Coordinate coor = new Coordinate(i, j, Constants.Anno.CONSTRU_PROGRESS);
						itemCols.add(coor);
					}
					if (value.contains("无形资产")) {
						Coordinate coor = new Coordinate(i, j, Constants.Anno.INTANG_ASSET);
						itemCols.add(coor);
					}
					if (value.contains("短期借款")) {
						Coordinate coor = new Coordinate(i, j, Constants.Anno.SHORT_LOAN);
						itemCols.add(coor);
					}
					if (value.contains("实收资本")) {
						Coordinate coor = new Coordinate(i, j, Constants.Anno.PAID_CAPITAL);
						itemCols.add(coor);
					}
					if (value.contains("其他应付款")) {
						Coordinate coor = new Coordinate(i, j, Constants.Anno.OTHER_PAY);
						itemCols.add(coor);
					}

					/** 结束***/
					// 获取期初值的列
					if (value.contains("金额")) {
						Coordinate coor = new Coordinate(i, j);
						valList.add(coor);
					}
					// 获取期末值的列
					if (value.contains("比例")||value.contains("比率")) {
						Coordinate coor = new Coordinate(i, j);
						rateList.add(coor);
					}
				}
			}

			if(valList.size()<=0){
				ExceptionUtil.throwException("导入失败！未找到'金额'关键字，请检查报表格式");
//					continue;
			}
			if(itemCols.size()<=0){
				ExceptionUtil.throwException("导入失败！未找到具体科目相，请检查报表格式");
//					continue;
			}
			
			int valColIndex = valList.get(0).getAbscissa();
			
			Collections.sort(itemCols,new Comparator<Coordinate>() {
				public int compare(Coordinate o1, Coordinate o2) {
					// TODO Auto-generated method stub
					return o1.getAbscissa()-o2.getAbscissa();
				}
			});
			List<AnnoEntity> menetaryList = new ArrayList<AnnoEntity>();
			List<AnnoEntity> accountReceList = new ArrayList<AnnoEntity>();
			List<AnnoEntity> stockList = new ArrayList<AnnoEntity>();
			List<AnnoEntity> accountPayList = new ArrayList<AnnoEntity>();
			List<AnnoEntity> fixedAssetList = new ArrayList<AnnoEntity>();
			List<AnnoEntity> otherReceList = new ArrayList<AnnoEntity>();
			List<AnnoEntity> longInvestList = new ArrayList<AnnoEntity>();
			List<AnnoEntity> construProgressList = new ArrayList<AnnoEntity>();
			List<AnnoEntity> intangAssetList = new ArrayList<AnnoEntity>();
			List<AnnoEntity> shortLoanList = new ArrayList<AnnoEntity>();
			List<AnnoEntity> paidCapitalList = new ArrayList<AnnoEntity>();
			List<AnnoEntity> otherPayList = new ArrayList<AnnoEntity>();
			List<AnnoEntity> prePayList = new ArrayList<AnnoEntity>();
			List<AnnoEntity> preReceList = new ArrayList<AnnoEntity>();
			
			
			for(int i = 0; i < itemCols.size() ; i++){
				Coordinate coor = itemCols.get(i);
				int aimX = coor.getOrdinate();
				int aimY = coor.getAbscissa();
				if((i+1)<itemCols.size()){
					for(int m = (aimX+1); m < itemCols.get(i+1).getOrdinate();m++){
						Row custRow = sheet1.getRow(m);
						if(custRow == null){
							continue;
						}
						Cell custCell = custRow.getCell(aimY);
						String custName = ExcelUtil.getCellValue(custCell);
						if (custName == null || "".equals(custName) || "合计".equals(custName) || "总计".equals(custName))
							continue;
						Cell valCell = sheet1.getRow(m).getCell(valColIndex);
						String value = ExcelUtil.getCellValue(valCell);
						if (value == null || "".equals(value))
							continue;
						if(!NumericUtil.isNumeric(value))
							continue;
						AnnoEntity anno = new AnnoEntity();
						anno.setCustName(custName);
						anno.setMoney(Double.parseDouble(value));
						anno.setType(coor.getItem());
						switch(coor.getItem()){
						case Constants.Anno.MONETARY_FUND:
							menetaryList.add(anno);
							break;
						case Constants.Anno.ACCOUNT_RECE:
							accountReceList.add(anno);
							break;
						case Constants.Anno.STOCK:
							stockList.add(anno);
							break;
						case Constants.Anno.ACCOUNT_PAY:
							accountPayList.add(anno);
							break;
						case Constants.Anno.FIXED_ASSET:
							fixedAssetList.add(anno);
							break;
						case Constants.Anno.OTHER_RECE:
							otherReceList.add(anno);
							break;
						case Constants.Anno.LONG_INVEST:
							longInvestList.add(anno);
							break;
						case Constants.Anno.CONSTRU_PROGRESS:
							construProgressList.add(anno);
							break;
						case Constants.Anno.INTANG_ASSET:
							intangAssetList.add(anno);
							break;
						case Constants.Anno.SHORT_LOAN:
							shortLoanList.add(anno);
							break;
						case Constants.Anno.PAID_CAPITAL:
							paidCapitalList.add(anno);
							break;
						case Constants.Anno.OTHER_PAY:
							otherPayList.add(anno);
							break;
						case Constants.Anno.PRE_PAY:
							prePayList.add(anno);
							break;
						case Constants.Anno.PRE_RECE:
							preReceList.add(anno);
							break;
							
						}
					}
				}else{
					//无法判断当前科目有多少具体项   当无效行超过10   认为已经结束
					int limit = 0;
					for(int m = (aimX+1); ;m++){
						if(limit>=10)
							break;
						Row custRow = sheet1.getRow(m);
						if(custRow == null){
							limit++;
							continue;
						}
						Cell custCell = custRow.getCell(aimY);
						String custName = ExcelUtil.getCellValue(custCell);
						if (custName == null || "".equals(custName)){
							limit++;
							continue;
						}
						Cell valCell = sheet1.getRow(m).getCell(valColIndex);
						String value = ExcelUtil.getCellValue(valCell);
						if (value == null || "".equals(value)){
							continue;
						}
						if(!NumericUtil.isNumeric(value)){
							continue;
						}
						AnnoEntity anno = new AnnoEntity();
						anno.setCustName(custName);
						anno.setMoney(Double.parseDouble(value));
						anno.setType(coor.getItem());
						switch(coor.getItem()){
						case Constants.Anno.MONETARY_FUND:
							menetaryList.add(anno);
							break;
						case Constants.Anno.ACCOUNT_RECE:
							accountReceList.add(anno);
							break;
						case Constants.Anno.STOCK:
							stockList.add(anno);
							break;
						case Constants.Anno.ACCOUNT_PAY:
							accountPayList.add(anno);
							break;
						case Constants.Anno.FIXED_ASSET:
							fixedAssetList.add(anno);
							break;
						case Constants.Anno.OTHER_RECE:
							otherReceList.add(anno);
							break;
						case Constants.Anno.LONG_INVEST:
							longInvestList.add(anno);
							break;
						case Constants.Anno.CONSTRU_PROGRESS:
							construProgressList.add(anno);
							break;
						case Constants.Anno.INTANG_ASSET:
							intangAssetList.add(anno);
							break;
						case Constants.Anno.SHORT_LOAN:
							shortLoanList.add(anno);
							break;
						case Constants.Anno.PAID_CAPITAL:
							paidCapitalList.add(anno);
							break;
						case Constants.Anno.OTHER_PAY:
							otherPayList.add(anno);
							break;
						case Constants.Anno.PRE_PAY:
							prePayList.add(anno);
							break;
						case Constants.Anno.PRE_RECE:
							preReceList.add(anno);
							break;
						}
					}
				}
			}
			//保存 存货 科目
			if(stockList.size()>0){
				List<FnInventory> list = new ArrayList<FnInventory>();
				for(AnnoEntity anno : stockList){
					FnInventory invent = new FnInventory();
					if("原材料".equals(anno.getCustName())){
						invent.setType("1");
					}else if("半成品".equals(anno.getCustName())){
						invent.setType("2");
					}else if("产成品".equals(anno.getCustName())){
						invent.setType("3");
					}
					invent.setBookValue(anno.getMoney());
					invent.setEvaluatValue(anno.getMoney());
					if(invent.getType()!=null&&!"".equals(invent.getType()))
						list.add(invent);
				}
				JSONArray jsonArray = JSONArray.fromObject(list);
				fnTableService.saveFnInventory(taskId, jsonArray.toString());
			}
			//保存 应收账款 科目
			if(accountReceList.size()>0){
				List<FnReceive> list = new ArrayList<FnReceive>();
				double sum = 0d;
				for(AnnoEntity anno : accountReceList){
					sum += anno.getMoney();
				}
				for(AnnoEntity anno : accountReceList){
					FnReceive rece = new FnReceive();
					rece.setCusName(anno.getCustName());
					rece.setRemainAmt(anno.getMoney());
					rece.setTaskId(taskId);
					rece.setRate(new BigDecimal(anno.getMoney()/sum*100).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
					list.add(rece);
				}
				JSONArray jsonArray = JSONArray.fromObject(list);
				fnTableService.saveReceive(taskId, jsonArray.toString());	
			}

			//保存 其他应收款 
			if(otherReceList.size()>0){
				List<FnOtherReceive> list = new ArrayList<FnOtherReceive>();
				double sum = 0d;
				for(AnnoEntity anno : otherReceList){
					sum += anno.getMoney();
				}
				for(AnnoEntity anno : otherReceList){
					FnOtherReceive otherRece = new FnOtherReceive();
					otherRece.setCusName(anno.getCustName());
					otherRece.setRemainAmt(anno.getMoney());
					otherRece.setTaskId(taskId);
					otherRece.setRate(new BigDecimal(anno.getMoney()/sum*100).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
					list.add(otherRece);
				}
				JSONArray jsonArray = JSONArray.fromObject(list);
				fnTableService.saveOtherReceive(taskId, jsonArray.toString());	
			}
			//保存 预付账款
			if(prePayList.size()>0){
				List<FnPrepay> list = new ArrayList<FnPrepay>();
				double sum = 0d;
				for(AnnoEntity anno : prePayList){
					sum += anno.getMoney();
				}
				for(AnnoEntity anno : prePayList){
					FnPrepay prepay = new FnPrepay();
					prepay.setSupplierName(anno.getCustName());
					prepay.setPaidAmt(anno.getMoney());
					prepay.setTaskId(taskId);
					prepay.setRate(new BigDecimal(anno.getMoney()/sum*100).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
					list.add(prepay);
				}
				JSONArray jsonArray = JSONArray.fromObject(list);
				fnTableService.savePrepay(taskId, jsonArray.toString());
			}

			//保存 应付账款
			if(accountPayList.size()>0){
				List<FnPay> list = new ArrayList<FnPay>();
				double sum = 0d;
				for(AnnoEntity anno : accountPayList){
					sum += anno.getMoney();
				}
				for(AnnoEntity anno : accountPayList){
					FnPay pay = new FnPay();
					pay.setSupplierName(anno.getCustName());
					pay.setRemainAmt(anno.getMoney());
					pay.setTaskId(taskId);
					pay.setRate(new BigDecimal(anno.getMoney()/sum*100).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
					list.add(pay);
				}
				JSONArray jsonArray = JSONArray.fromObject(list);
				fnTableService.savePay(taskId, jsonArray.toString());
			}
			//保存 其他应付款
			if(otherPayList.size()>0){
				List<FnOtherPay> list = new ArrayList<FnOtherPay>();
				double sum = 0d;
				for(AnnoEntity anno : otherPayList){
					sum += anno.getMoney();
				}
				for(AnnoEntity anno : otherPayList){
					FnOtherPay otherPay = new FnOtherPay();
					otherPay.setSupplierName(anno.getCustName());
					otherPay.setRemainAmt(anno.getMoney());
					otherPay.setTaskId(taskId);
					otherPay.setRate(new BigDecimal(anno.getMoney()/sum*100).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
					list.add(otherPay);
				}
				JSONArray jsonArray = JSONArray.fromObject(list);
				fnTableService.saveOtherPay(taskId, jsonArray.toString());
			}
			//保存 预收账款
			if(preReceList.size()>0){
				List<FnPreReceive> list = new ArrayList<FnPreReceive>();
				double sum = 0d;
				for(AnnoEntity anno : preReceList){
					sum += anno.getMoney();
				}
				for(AnnoEntity anno : preReceList){
					FnPreReceive preRece = new FnPreReceive();
					preRece.setCusName(anno.getCustName());
					preRece.setPaidAmt(anno.getMoney());
					preRece.setTaskId(taskId);
					preRece.setRate(new BigDecimal(anno.getMoney()/sum*100).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
					list.add(preRece);
				}
				JSONArray jsonArray = JSONArray.fromObject(list);
				fnTableService.savePreReceive(taskId, jsonArray.toString());
			}
			//保存 固定资产
			if(fixedAssetList.size()>0){
				List<FnFixedAssetsLand> lands = new ArrayList<FnFixedAssetsLand>();
				List<FnFixedAssetsBuilding> builds = new ArrayList<FnFixedAssetsBuilding>();
				List<FnFixedAssetsEquipment> equips = new ArrayList<FnFixedAssetsEquipment>();
				Pattern p = Pattern.compile("[^0-9]");
				for(AnnoEntity anno : fixedAssetList){
					String custName = anno.getCustName();
					if(custName.contains("土地")){
						FnFixedAssetsLand land = new FnFixedAssetsLand();
						land.setTaskId(taskId);
						land.setName(custName);
						land.setOriginalValue(anno.getMoney());
						lands.add(land);
					}else if(custName.contains("房产")){
						FnFixedAssetsBuilding build = new FnFixedAssetsBuilding();
						build.setTaskId(taskId);
						build.setType(custName);
						build.setOriginalValue(anno.getMoney());
						build.setNetValue(anno.getMoney());
						builds.add(build);
					}else{
						String arr[] = custName.split("\\s+");
						FnFixedAssetsEquipment equip = new FnFixedAssetsEquipment();
						equip.setTaskId(taskId);
						equip.setOriginalValue(anno.getMoney());
						equip.setNetValue(anno.getMoney());
						if(arr.length==1){
							equip.setName(arr[0]);
						}else if(arr.length==2){
							equip.setName(arr[0]);
							Matcher m = p.matcher(arr[1]);  
							String number = m.replaceAll("").trim();
							equip.setNumber(Integer.parseInt(number));
						}
						equips.add(equip);
					}
					//土地
//					{"id":379,"taskId":1899,"name":"土地1","warrant":"","landNature":"","ownershipNature":"","area":"","use":"","originalValue":"206
//					FnFixedAssetsLand
//					fnTableService.saveFnFixedAssetsLands(taskId, jsonArray);
					
					//房产
//					{"taskId":"1899","type":"房地产","warrant":"","ownershipNature":"","area":"","originalValue":"100","netValue":"90","isMortgage":"","mortgagee":""}
//					FnFixedAssetsBuilding
//					fnTableService.saveFnFixedAssetsBuildings(taskId, jsonArray);
					
					//设备
//					"taskId":"1899","name":"设备1","number":"1","originalValue":"5","netValue":"4","originPlace":"","purchaseDate":"","isMortgage":"","mortgagee":""}
//					FnFixedAssetsEquipment
//					fnTableService.saveFnFixedAssetsEquipments(taskId, jsonArray);
				}
				JSONArray landArray = JSONArray.fromObject(lands);
				JSONArray buildArray = JSONArray.fromObject(builds);
				JSONArray equipArray = JSONArray.fromObject(equips);
				fnTableService.saveFnFixedAssetsLands(taskId, landArray.toString());
				fnTableService.saveFnFixedAssetsBuildings(taskId, buildArray.toString());
				fnTableService.saveFnFixedAssetsEquipments(taskId, equipArray.toString());
			}
			
		}catch(Exception e){
			log.error("导入文件异常：", e);
			json.put("msg", "系统异常，导入失败！");
			
		}finally{
			try {
				if(is != null)
					is.close();
				if(os != null)
					os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return jRequest.serialize(json, true);
	}
	
}
