package com.beawan.customer.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.customer.service.ICalculateAssessCardSV;
import com.beawan.scoreCard.bean.AcLogRecord;
import com.beawan.scoreCard.bean.AcLogRecordDetail;
import com.beawan.scoreCard.service.IAcLogRecordSV;

import jxl.common.Logger;

@Controller
@UserSessionAnnotation
@RequestMapping({"/customer/assess"})
public class CountAssessCtl extends BaseController{
	
	private static Logger log = Logger.getLogger(CountAssessCtl.class);
	
	@Resource
	private ICalculateAssessCardSV calculateAssessCardSV;
	@Resource
	private IAcLogRecordSV acLogRecordSV;
	
	@RequestMapping("/countAssess.do")
	@ResponseBody
	public ResultDto countAssess(HttpServletRequest request, String customerNo){
		ResultDto re = returnFail("计算评级失败");
		try {
			
			String basePath = request.getSession().getServletContext().getRealPath("/");
			
			String result = calculateAssessCardSV.calculateAssessCard(5109, customerNo, basePath);
			
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setRows(result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error("计算评级失败");
			e.printStackTrace();
		}
		return re;
	}
	
	@RequestMapping("/getCusAcRecord.do")
	public String getCusAcRecord(String customerNo, HttpServletRequest request){
		
		AcLogRecord logRecord = acLogRecordSV.selectLogRecordByCusNo(customerNo);
		
		List<AcLogRecordDetail> details = acLogRecordSV.selectLogRecordDetailsByLogId(logRecord.getId());
		
		request.setAttribute("logRecord", logRecord);
		request.setAttribute("details", details);
		
		return "/views/custInfo/assessCardRecord";
	}
}
