package com.beawan.customer.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.beawan.analysis.finansis.utils.CodeUtil;
import com.beawan.base.entity.SReportData;
import com.beawan.base.entity.SReportModel;
import com.beawan.base.entity.SReportModelRow;
import com.beawan.base.service.ISReportDataSV;
import com.beawan.base.service.ISReportModelSV;
import com.beawan.base.service.ISReportRecordSV;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.bean.CusFSRecord;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.customer.service.ICusFSRecordSV;
import com.beawan.exterinvoke.util.FncStatMapUtil;
import com.platform.util.HttpUtil;
import com.platform.util.PropertiesUtil;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/cusfs/export" })
public class CusFSExportCtl {

	private static Logger log = Logger.getLogger(CusFSExportCtl.class);
	
	private static String dataTempPath = PropertiesUtil.getProperty(Constants.SYSTEM_PROPS_PATH, "data.temp.path");//本地缓存地址

	private CellStyle headStyle;
	private CellStyle topHeadStyle;
	private CellStyle cellStyle;
	private CellStyle cellStyle2;
	private String tabName;
	private String cellName;
	
	@Resource
	private ICusFSRecordSV cusFSRecordSV;
	@Resource
	private ISReportRecordSV sReportRecordSV;
	@Resource
	private ISReportDataSV sReportDataSV;
	@Resource
	private ISReportModelSV sReportModelSV;
	@Resource
	private ICusBaseSV cusBaseSV;
	
	@RequestMapping("cusfsexport.do")
	public ResponseEntity<byte[]> export(HttpServletRequest request, HttpServletResponse response) {
		
		ResponseEntity<byte[]> result = null;
		
		try {
			String recordNo = HttpUtil.getAsString(request, "recordNo");
			
			String reportType = HttpUtil.getAsString(request,  "reportType");
			
			//资产负债表
			if(SysConstants.CmisFianRepType.BALANCE.equals(reportType)){
			
				tabName="资产负债表";
				
				cellName="";
				
			//利润表
			}else if(SysConstants.CmisFianRepType.INCOME.equals(reportType)){
			
				tabName="利润表";
				
				cellName="本年累计数";
				
			//现金流量表
			}else if(SysConstants.CmisFianRepType.CASH_FLOW_INPUT.equals(reportType)){
				
				tabName="现金流量表(人工填写)";
				
				cellName="金额";
				
			}else if(SysConstants.CmisFianRepType.CASH_FLOW.equals(reportType)){
				
				tabName="现金流量表(自动生成)";
				
				cellName="金额";
				
			}else if(SysConstants.CmisFianRepType.RATIO_INDEX.equals(reportType)){
				
				tabName="主要财务指标表";
				
				cellName="指标值";
				
			}else {
				tabName="统计表";
				
				cellName="数量";
			}
						
			//客户财务报表记录
			CusFSRecord cusFSRecord = cusFSRecordSV.queryByRecordNo(recordNo);
			
			//获得对应报表模型号
			String modelNo = FncStatMapUtil.getModelNo(cusFSRecord.getModelClass(), reportType);
			
			//获得报表模型科目数据
			List<SReportModelRow> subjects = sReportModelSV.queryModelRowByNo(modelNo);
			
			//取得具体报表数据
			List<SReportData> currentData = cusFSRecordSV.getReportData(cusFSRecord.getRecordNo(), modelNo);

			if(null==currentData||currentData.size()<=0) {
				
				currentData=new ArrayList<SReportData>();
				
				for(SReportModelRow data:subjects) {
					
					SReportData spData=new SReportData();
					
					spData.setRowAttribute(data.getRowAttribute());
					
					spData.setRowName(data.getRowName());
															
					currentData.add(spData);
					
				}
			}
			
			CusBase cusBase=cusBaseSV.queryByCusNo(cusFSRecord.getCustomerId());
			
			HSSFWorkbook workbook=null;
			
			if(!SysConstants.CmisFianRepType.BALANCE.equals(reportType)) {
				
				 workbook = genWorkbook(currentData);
			
			}else {
				
				 workbook=genBanlanceWorkbook(currentData);
			}
			
			File dir = new File(dataTempPath);
			
			if (!dir.exists())
				
				dir.mkdirs();
			
			File xls = new File(dir, recordNo+tabName+".xls");
			// 如果此文件不存在则创建一个
			if (!xls.exists()) {
				
				xls.createNewFile();
				
			}
			
			FileOutputStream fileOut = new FileOutputStream(xls);
			
			workbook.write(fileOut);// 将workbook中的 内容写入fileOut
			
			fileOut.flush();
			
			fileOut.close();
			
			HttpHeaders headers = new HttpHeaders();
			
			String namefinal = CodeUtil.parseGBK(cusBase.getCustomerName()+"-"+tabName+"("+cusFSRecord.getReportDate()+")");
			
			headers.setContentDispositionFormData("attachment", namefinal+".xls");
			
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			
			result = new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(xls),
					headers, HttpStatus.OK);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
		}
		
		return result;
		
	}
	
	public HSSFWorkbook genWorkbook(List<SReportData> currentData) throws Exception {
		
		// 创建一个xls
		HSSFWorkbook workbook = new HSSFWorkbook();
		
		CreationHelper cHelper = workbook.getCreationHelper();
		
		topHeadStyle=getTopHeadCellStyle(workbook);
		
		headStyle = getHeadCellStyle(workbook);
		
		cellStyle = getCellStyle(workbook,false);
		
		cellStyle2 = getCellStyle(workbook,true);

		HSSFSheet sheet = workbook.createSheet(tabName);// 设置表名
		
		sheet.setDefaultRowHeightInPoints(20);
		
		Row topHeader = sheet.createRow(0);
		
		Cell topCell = topHeader.createCell(0);
		
		topCell.setCellValue(tabName);
		
		topCell.setCellStyle(topHeadStyle);
	
		Row header = sheet.createRow(1);
		
		Cell cell1 = header.createCell(0);
		
		cell1.setCellValue("项目");
		
		cell1.setCellStyle(headStyle);
		
		Cell cell2 = header.createCell(1);
		
		cell2.setCellValue("行次");
		
		cell2.setCellStyle(headStyle);
		
		Cell cell3 = header.createCell(2);
		
		cell3.setCellValue(cellName);
		
		cell3.setCellStyle(headStyle);
		
		int collen=0;
		
		for(int i=0;i<currentData.size();i++) {
			
			cellStyle = getCellStyle(workbook,false);
			
			cellStyle2 = getCellStyle(workbook,true);
			
			if(null!=currentData.get(i).getRowAttribute()) {
				
				String attribute=currentData.get(i).getRowAttribute();
				
				if(attribute.contains("yellow")||attribute.contains("#99FFFF")||attribute.contains("#CCCCCC")) {
					
					cellStyle=getColorCellStyle(workbook,false);
					
					cellStyle2=getColorCellStyle(workbook, true);
					
				}
			}
			
			Row row = sheet.createRow(i+2);
			
			cell1 = row.createCell(0);
			
			cell1.setCellValue(currentData.get(i).getRowName());
			
			cell1.setCellStyle(cellStyle);
			
			String cellname=currentData.get(i).getRowName();
			
			if(null!=cellname) {
				
				int len=cellname.getBytes().length;
				
				if(len>collen) {
					
					collen=len;
					
				}
				
			}
			
			cell2 = row.createCell(1);
			
			cell2.setCellValue(i+1);
			
			cell2.setCellStyle(cellStyle2);
			
			cell3 = row.createCell(2);
			
			if(null==currentData.get(i).getCol2Value()) {
				
				cell3.setCellValue("");
				
			}else {
		
			double value=currentData.get(i).getCol2Value();
			if(null==currentData.get(i).getRowDimType()||!currentData.get(i).getRowDimType().equals("2"))
				cell3.setCellValue(new BigDecimal(value).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue());
			else
				cell3.setCellValue(new BigDecimal(value*100).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue());
			
			}
			
			cell3.setCellStyle(cellStyle);
			
		}
		//合并行列
		CellRangeAddress region1 = new CellRangeAddress(0, 0, 0, 2);
		
		// 设置列宽度
		sheet.setColumnWidth(0,collen*400);
		
		sheet.setColumnWidth(1,3500);
		
		sheet.setColumnWidth(2, 7500);
		
		sheet.createFreezePane(0, 2);
		
		sheet.addMergedRegion(region1);
		
		return workbook;
	}

	
	public HSSFWorkbook genBanlanceWorkbook(List<SReportData> currentData) throws Exception {
		
		// 创建一个xls
		HSSFWorkbook workbook = new HSSFWorkbook();
		
		CreationHelper cHelper = workbook.getCreationHelper();
		
		topHeadStyle=getTopHeadCellStyle(workbook);
		
		headStyle = getHeadCellStyle(workbook);
		
		cellStyle = getCellStyle(workbook,false);
		
		cellStyle2 = getCellStyle(workbook,true);

		HSSFSheet sheet = workbook.createSheet(tabName);// 设置表名
		
		sheet.setDefaultRowHeightInPoints(20);
		
		Row topHeader = sheet.createRow(0);
		
		Cell topCell = topHeader.createCell(0);
		
		topCell.setCellValue(tabName);
		
		topCell.setCellStyle(topHeadStyle);
	
		Row header = sheet.createRow(1);
		
		Cell cell1 = header.createCell(0);
		
		cell1.setCellValue("资产");
		
		cell1.setCellStyle(headStyle);
		
		Cell cell2 = header.createCell(1);
		
		cell2.setCellValue("行次");
		
		cell2.setCellStyle(headStyle);
		
		Cell cell3 = header.createCell(2);
		
		cell3.setCellValue("年初值");
		
		cell3.setCellStyle(headStyle);
		
		
		Cell cell4 = header.createCell(3);
		
		cell4.setCellValue("年末值");
		
		cell4.setCellStyle(headStyle);
		
		
		Cell cell5 = header.createCell(4);
		
		cell5.setCellValue("负债及股东权益");
		
		cell5.setCellStyle(headStyle);
		
		Cell cell6 = header.createCell(5);
		
		cell6.setCellValue("行次");
		
		cell6.setCellStyle(headStyle);
		
		Cell cell7 = header.createCell(6);
		
		cell7.setCellValue("年初值");
		
		cell7.setCellStyle(headStyle);
		
		
		Cell cell8 = header.createCell(7);
		
		cell8.setCellValue("年末值");
		
		cell8.setCellStyle(headStyle);
		
		
		int collen=0;
		
		int size=currentData.size()/2;
		
		for(int i=0;i<size;i++) {
			
			cellStyle = getCellStyle(workbook,false);
			
			cellStyle2 = getCellStyle(workbook,true);
			
			if(null!=currentData.get(i).getRowAttribute()) {
				
				String attribute=currentData.get(i).getRowAttribute();
				
				if(attribute.contains("yellow")||attribute.contains("#99FFFF")||attribute.contains("#CCCCCC")) {
					
					cellStyle=getColorCellStyle(workbook,false);
					
					cellStyle2=getColorCellStyle(workbook, true);
					
				}
			}
			
			
			Row row = sheet.createRow(i+2);
			
			cell1 = row.createCell(0);
			
			if(null==currentData.get(i).getRowName()) {
				cell1.setCellValue("");
			}else {
			cell1.setCellValue(currentData.get(i).getRowName());
			}
			
			cell1.setCellStyle(cellStyle);
			
			String cellname=currentData.get(i).getRowName();
			
			if(null!=cellname) {
				
				int len=cellname.getBytes().length;
				
				if(len>collen) {
					
					collen=len;
					
				}
				
			}
			
			cell2 = row.createCell(1);
			
			cell2.setCellValue(i+1);
			
			cell2.setCellStyle(cellStyle2);
			
			cell3 = row.createCell(2);
			
			if(null==currentData.get(i).getCol1Value()) {
				
				cell3.setCellValue("");
				
			}else {
				
			double value=currentData.get(i).getCol1Value();
			if(null==currentData.get(i).getRowDimType()||!currentData.get(i).getRowDimType().equals("2"))
				cell3.setCellValue(new BigDecimal(value).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue());
			else
				cell3.setCellValue(new BigDecimal(value*100).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue());
			
			}
			
			cell3.setCellStyle(cellStyle);
			
			
			cell4 = row.createCell(3);
			
			if(null==currentData.get(i).getCol2Value()) {
				
				cell4.setCellValue("");
				
			}else {
				
			double value=currentData.get(i).getCol2Value();
			if(null==currentData.get(i).getRowDimType()||!currentData.get(i).getRowDimType().equals("2"))
				cell4.setCellValue(new BigDecimal(value).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue());
			else
				cell4.setCellValue(new BigDecimal(value*100).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue());
			
			}
			
			cell4.setCellStyle(cellStyle);
			
			cellStyle = getCellStyle(workbook,false);
			cellStyle2 = getCellStyle(workbook,true);
			if(null!=currentData.get(i+size).getRowAttribute()) {
				
				String attribute=currentData.get(i+size).getRowAttribute();
				
				if(attribute.contains("yellow")||attribute.contains("#99FFFF")||attribute.contains("#CCCCCC")) {
					
					cellStyle=getColorCellStyle(workbook,false);
					
					cellStyle2=getColorCellStyle(workbook, true);
					
				}
			}
			
			cell5 = row.createCell(4);
			
			if(null==currentData.get(i+size).getRowName()) {
				cell5.setCellValue("");
			}else {
				cell5.setCellValue(currentData.get(i+size).getRowName());
			}
			
			cell5.setCellStyle(cellStyle);
			
			String cellname2=currentData.get(i+size).getRowName();
			
			if(null!=cellname2) {
				
				int len=cellname2.getBytes().length;
				
				if(len>collen) {
					
					collen=len;
					
				}
				
			}
			
			cell6 = row.createCell(5);
			
			cell6.setCellValue(i+1+size);
			
			cell6.setCellStyle(cellStyle2);
			
			cell7 = row.createCell(6);
			
			if(null==currentData.get(i+size).getCol1Value()) {
				
				cell7.setCellValue("");
				
			}else {

				double value=currentData.get(i+size).getCol1Value();
				if(null==currentData.get(i+size).getRowDimType()||!currentData.get(i+size).getRowDimType().equals("2"))
					cell7.setCellValue(new BigDecimal(value).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue());
				else
					cell7.setCellValue(new BigDecimal(value*100).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue());
			}
			
			cell7.setCellStyle(cellStyle);
			
			
			cell8 = row.createCell(7);
			
			if(null==currentData.get(i+size).getCol2Value()) {
				
				cell8.setCellValue("");
				
			}else {
				
			double value=currentData.get(i+size).getCol2Value();
			if(null==currentData.get(i+size).getRowDimType()||!currentData.get(i+size).getRowDimType().equals("2"))
				cell8.setCellValue(new BigDecimal(value).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue());
			else
				cell8.setCellValue(new BigDecimal(value*100).setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue());
			
			}
			
			cell8.setCellStyle(cellStyle);
			
			
		}
		
		CellRangeAddress region1 = new CellRangeAddress(0, 0, 0, 7);
		
		// 设置列宽度
		sheet.setColumnWidth(0,10000);
		
		sheet.setColumnWidth(1,2000);
		
		sheet.setColumnWidth(2, 4500);
		
		sheet.setColumnWidth(3, 4500);
		
		sheet.setColumnWidth(4,10000);
		
		sheet.setColumnWidth(5,2000);
		
		sheet.setColumnWidth(6, 4500);
		
		sheet.setColumnWidth(7, 4500);
		
		sheet.createFreezePane(0, 2);
		
		sheet.addMergedRegion(region1);
		
		return workbook;
	}

	
	@RequestMapping("modelexport.do")
	public ResponseEntity<byte[]> modelexport(HttpServletRequest request, HttpServletResponse response) {
		
		ResponseEntity<byte[]> result = null;
		
		try {

			//一般企业法人财务报表(新会计准则口径)
			List<SReportModel> newAccModel = sReportModelSV.queryModelByClass(SysConstants.CmisFianRepModClass.NEW_ACC);
			
			//一般企业法人财务报表(旧会计准则口径)
			List<SReportModel> oldAccModel = sReportModelSV.queryModelByClass(SysConstants.CmisFianRepModClass.OLD_ACC);
			
			
			HSSFWorkbook workbook1=genModelExcel(newAccModel);
			
			HSSFWorkbook workbook2=genModelExcel(oldAccModel);
			
			File dir = new File(dataTempPath);
			
			if (!dir.exists())
				
				dir.mkdirs();
			
			File newAccModelxls = new File(dir, "一般企业法人财务报表(新会计准则口径).xls");
			// 如果此文件存在   先删除   再创建一个

			if (newAccModelxls.exists()) {
				newAccModelxls.delete();
			}

			newAccModelxls.createNewFile();

			FileOutputStream newFileOut = new FileOutputStream(newAccModelxls);

			workbook1.write(newFileOut);// 将workbook中的 内容写入fileOut

			newFileOut.flush();

			newFileOut.close();

			
			
			File oldAccModelxls = new File(dir, "一般企业法人财务报表(旧会计准则口径).xls");

			// 如果此文件存在   先删除   再创建一个
			if (oldAccModelxls.exists()) {
				oldAccModelxls.delete();
			}

			oldAccModelxls.createNewFile();

			FileOutputStream oldFileOut = new FileOutputStream(oldAccModelxls);

			workbook2.write(oldFileOut);// 将workbook中的 内容写入fileOut

			oldFileOut.flush();

			oldFileOut.close();

			File zipFile = new File(dir,"model.zip");

			if (zipFile.exists()) {
				zipFile.delete();
			}

			zipFile.createNewFile();

			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFile));

			byte[] buffer = new byte[1024];

			File[] file1 = {newAccModelxls, oldAccModelxls};

			for (int i = 0; i < file1.length; i++) {

				FileInputStream fis = new FileInputStream(file1[i]);

				out.putNextEntry(new ZipEntry(file1[i].getName()));

				int len;

				//读入需要下载的文件的内容，打包到zip文件

				while ((len = fis.read(buffer)) > 0) {

					out.write(buffer, 0, len);

				}

				out.closeEntry();

				fis.close();

			}

			out.close();
		

			HttpHeaders headers = new HttpHeaders();
			
			String namefinal = CodeUtil.parseGBK("一般企业法人财务报表模板");
			
			headers.setContentDispositionFormData("attachment", namefinal+".zip");
			
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			
			result = new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(zipFile),
					headers, HttpStatus.OK);
			
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
		}
		
		return result;
		
	}
	
	public HSSFWorkbook genModelExcel(List<SReportModel> accModel) throws Exception {
		
		HSSFWorkbook workbook = new HSSFWorkbook();
		
		CreationHelper cHelper = workbook.getCreationHelper();
		
		String balance="";
		
		String income="";
		
		String cashflow="";
		
		for(SReportModel data:accModel) {
			
			if(data.getModelName().equals("资产负债表"))
				balance=data.getModelNo();
			else if(data.getModelName().equals("利润表"))
				income=data.getModelNo();
			else if(data.getModelName().equals("现金流量表(人工录入)"))
				cashflow=data.getModelNo();
			else;
		}
		
		List<SReportModelRow> balanceRow = sReportModelSV.queryModelRowByNo(balance);
		
		List<SReportModelRow> incomeRow = sReportModelSV.queryModelRowByNo(income);
		
		List<SReportModelRow> cashflowRow = sReportModelSV.queryModelRowByNo(cashflow);
		
		genBanlanceSheet(workbook, balanceRow, "资产负债表");
		
		genSheet(workbook, incomeRow,"利润表","本年累计数");
		
		genSheet(workbook, cashflowRow, "现金流量表(人工录入)","金额");
		
		return workbook;
		
	}

	
public void genSheet(HSSFWorkbook workbook,List<SReportModelRow> currentData,String sheetName,String cName) throws Exception {
		
		
		CreationHelper cHelper = workbook.getCreationHelper();
		
		topHeadStyle=getTopHeadCellStyle(workbook);
		
		headStyle = getHeadCellStyle(workbook);
		
		cellStyle = getCellStyle(workbook,false);
		
		cellStyle2 = getCellStyle(workbook,true);

		HSSFSheet sheet = workbook.createSheet(sheetName);// 设置表名
		
		sheet.setDefaultRowHeightInPoints(20);
		
		Row topHeader = sheet.createRow(0);
		
		Cell topCell = topHeader.createCell(0);
		
		topCell.setCellValue(sheetName);
		
		topCell.setCellStyle(topHeadStyle);
	
		Row header = sheet.createRow(1);
		
		Cell cell1 = header.createCell(0);
		
		cell1.setCellValue("项目");
		
		cell1.setCellStyle(headStyle);
		
		Cell cell2 = header.createCell(1);
		
		cell2.setCellValue("行次");
		
		cell2.setCellStyle(headStyle);
		
		Cell cell3 = header.createCell(2);
		
		cell3.setCellValue(cName);
		
		cell3.setCellStyle(headStyle);
		
		int collen=0;
		
		for(int i=0;i<currentData.size();i++) {
			
			cellStyle = getCellStyle(workbook,false);
			
			cellStyle2 = getCellStyle(workbook,true);
			
			if(null!=currentData.get(i).getRowAttribute()) {
				
				String attribute=currentData.get(i).getRowAttribute();
				
				if(attribute.contains("yellow")||attribute.contains("#99FFFF")||attribute.contains("#CCCCCC")) {
					
					cellStyle=getColorCellStyle(workbook,false);
					
					cellStyle2=getColorCellStyle(workbook, true);
					
				}
			}
			
			Row row = sheet.createRow(i+2);
			
			cell1 = row.createCell(0);
			
			cell1.setCellValue(currentData.get(i).getRowName());
			
			cell1.setCellStyle(cellStyle);
			
			String cellname=currentData.get(i).getRowName();
			
			if(null!=cellname) {
				
				int len=cellname.getBytes().length;
				
				if(len>collen) {
					
					collen=len;
					
				}
				
			}
			
			cell2 = row.createCell(1);
			
			cell2.setCellValue(i+1);
			
			cell2.setCellStyle(cellStyle2);
			
			cell3 = row.createCell(2);
			
			cell3.setCellValue("");
				
			cell3.setCellStyle(cellStyle);
			
		}

		CellRangeAddress region1 = new CellRangeAddress(0, 0, 0, 2);
		
		// 设置列宽度
		sheet.setColumnWidth(0,collen*400);
		
		sheet.setColumnWidth(1,3500);
		
		sheet.setColumnWidth(2, 7500);
		
		sheet.createFreezePane(0, 2);
		
		sheet.addMergedRegion(region1);
		
	}	
	
	
	
	
public void genBanlanceSheet(HSSFWorkbook workbook,List<SReportModelRow> currentData,String sheetName) throws Exception {
		
	
		CreationHelper cHelper = workbook.getCreationHelper();
		
		topHeadStyle=getTopHeadCellStyle(workbook);
		
		headStyle = getHeadCellStyle(workbook);
		
		cellStyle = getCellStyle(workbook,false);
		
		cellStyle2 = getCellStyle(workbook,true);

		HSSFSheet sheet = workbook.createSheet(sheetName);// 设置表名
		
		sheet.setDefaultRowHeightInPoints(20);
		
		Row topHeader = sheet.createRow(0);
		
		Cell topCell = topHeader.createCell(0);
		
		topCell.setCellValue(sheetName);
		
		topCell.setCellStyle(topHeadStyle);
	
		Row header = sheet.createRow(1);
		
		Cell cell1 = header.createCell(0);
		
		cell1.setCellValue("资产");
		
		cell1.setCellStyle(headStyle);
		
		Cell cell2 = header.createCell(1);
		
		cell2.setCellValue("行次");
		
		cell2.setCellStyle(headStyle);
		
		Cell cell3 = header.createCell(2);
		
		cell3.setCellValue("年初值");
		
		cell3.setCellStyle(headStyle);
		
		
		Cell cell4 = header.createCell(3);
		
		cell4.setCellValue("年末值");
		
		cell4.setCellStyle(headStyle);
		
		
		Cell cell5 = header.createCell(4);
		
		cell5.setCellValue("负债及股东权益");
		
		cell5.setCellStyle(headStyle);
		
		Cell cell6 = header.createCell(5);
		
		cell6.setCellValue("行次");
		
		cell6.setCellStyle(headStyle);
		
		Cell cell7 = header.createCell(6);
		
		cell7.setCellValue("年初值");
		
		cell7.setCellStyle(headStyle);
		
		
		Cell cell8 = header.createCell(7);
		
		cell8.setCellValue("年末值");
		
		cell8.setCellStyle(headStyle);
		
		
		int collen=0;
		
		int size=currentData.size()/2;
		
		for(int i=0;i<size;i++) {
			
			cellStyle = getCellStyle(workbook,false);
			
			cellStyle2 = getCellStyle(workbook,true);
			
			if(null!=currentData.get(i).getRowAttribute()) {
				
				String attribute=currentData.get(i).getRowAttribute();
				
				if(attribute.contains("yellow")||attribute.contains("#99FFFF")||attribute.contains("#CCCCCC")) {
					
					cellStyle=getColorCellStyle(workbook,false);
					
					cellStyle2=getColorCellStyle(workbook, true);
					
				}
			}
			
			
			Row row = sheet.createRow(i+2);
			
			cell1 = row.createCell(0);
			
			if(null==currentData.get(i).getRowName()) {
				cell1.setCellValue("");
			}else {
			cell1.setCellValue(currentData.get(i).getRowName());
			}
			
			cell1.setCellStyle(cellStyle);
			
			String cellname=currentData.get(i).getRowName();
			
			if(null!=cellname) {
				
				int len=cellname.getBytes().length;
				
				if(len>collen) {
					
					collen=len;
					
				}
				
			}
			
			cell2 = row.createCell(1);
			
			cell2.setCellValue(i+1);
			
			cell2.setCellStyle(cellStyle2);
			
			cell3 = row.createCell(2);
			
			cell3.setCellValue("");

			cell3.setCellStyle(cellStyle);
			
			
			cell4 = row.createCell(3);
				
			cell4.setCellValue("");
				
			cell4.setCellStyle(cellStyle);
			
			cellStyle = getCellStyle(workbook,false);
			cellStyle2 = getCellStyle(workbook,true);
			if(null!=currentData.get(i+size).getRowAttribute()) {
				
				String attribute=currentData.get(i+size).getRowAttribute();
				
				if(attribute.contains("yellow")||attribute.contains("#99FFFF")||attribute.contains("#CCCCCC")) {
					
					cellStyle=getColorCellStyle(workbook,false);
					
					cellStyle2=getColorCellStyle(workbook, true);
					
				}
			}
			
			cell5 = row.createCell(4);
			
			if(null==currentData.get(i+size).getRowName()) {
				cell5.setCellValue("");
			}else {
				cell5.setCellValue(currentData.get(i+size).getRowName());
			}
			
			cell5.setCellStyle(cellStyle);
			
			String cellname2=currentData.get(i+size).getRowName();
			
			if(null!=cellname2) {
				
				int len=cellname2.getBytes().length;
				
				if(len>collen) {
					
					collen=len;
					
				}
				
			}
			
			cell6 = row.createCell(5);
			
			cell6.setCellValue(i+1+size);
			
			cell6.setCellStyle(cellStyle2);
			
			cell7 = row.createCell(6);
			
			cell7.setCellValue("");
				
			cell7.setCellStyle(cellStyle);
			
			
			cell8 = row.createCell(7);
			
			cell8.setCellValue("");
				
			cell8.setCellStyle(cellStyle);
			
			
		}
		
		CellRangeAddress region1 = new CellRangeAddress(0, 0, 0, 7);
		
		// 设置列宽度
		sheet.setColumnWidth(0,10000);
		
		sheet.setColumnWidth(1,2000);
		
		sheet.setColumnWidth(2, 4500);
		
		sheet.setColumnWidth(3, 4500);
		
		sheet.setColumnWidth(4,10000);
		
		sheet.setColumnWidth(5,2000);
		
		sheet.setColumnWidth(6, 4500);
		
		sheet.setColumnWidth(7, 4500);
		
		sheet.createFreezePane(0, 2);
		
		sheet.addMergedRegion(region1);
	}
	
	
	
	
	private CellStyle getCellStyle(Workbook workbook,Boolean horizoncenter) {
	// 单元格样式
	CellStyle cellStyle = workbook.createCellStyle();
	
	Font cellFront = workbook.createFont();
	
	cellFront.setFontName("宋体");
	
	cellFront.setFontHeightInPoints((short)12);
	
	cellStyle.setFont(cellFront);
	
	cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN); //下边框    
	
	cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);//左边框    
	
	cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);//上边框    
	
	cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);//右边框    
	
	cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
	
	cellStyle.setWrapText(true);
	
	if (horizoncenter) {
		
		cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		
	}else {
		
		cellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00"));
		
	}
	
	return cellStyle;
}

	
	private CellStyle getColorCellStyle(Workbook workbook,Boolean horizoncenter) {
		
		// 单元格样式
		CellStyle cellStyle = workbook.createCellStyle();
		
		Font cellFront = workbook.createFont();
		
		cellFront.setFontName("宋体");
		
		cellFront.setFontHeightInPoints((short)12);
		
		cellStyle.setFont(cellFront);
		
		cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN); //下边框    
		
		cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);//左边框    
		
		cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);//上边框    
		
		cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);//右边框    
		
		cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		
		cellStyle.setWrapText(true);
		
		if (horizoncenter) {
			
			cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			
		}else {
			
			cellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00"));
			
		}
		
		cellStyle.setFillForegroundColor(HSSFColor.YELLOW.index);		
		
		cellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);  
	
		return cellStyle;
	}
	
	private CellStyle getHeadCellStyle(Workbook workbook) {
	
	// 表头样式
	CellStyle headStyle = workbook.createCellStyle();
	
	Font headFront = workbook.createFont();
	
	headFront.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
	
	headFront.setFontName("宋体");
	
	headFront.setFontHeightInPoints((short)12);
	
	headStyle.setFont(headFront);

	headStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
	
	headStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
	
	headStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());  
	
	headStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
	
	headStyle.setBorderTop(CellStyle.BORDER_THIN);
	
	headStyle.setBorderBottom(CellStyle.BORDER_THIN);
	
	headStyle.setBorderTop(CellStyle.BORDER_THIN);
	
	headStyle.setBorderRight(CellStyle.BORDER_THIN);
	
	headStyle.setBorderLeft(CellStyle.BORDER_THIN);
	
	return headStyle;
}
	
	private CellStyle getTopHeadCellStyle(Workbook workbook) {
		
		// 表头样式
		CellStyle headStyle = workbook.createCellStyle();
		
		Font headFront = workbook.createFont();
		
		headFront.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		
		headFront.setFontName("宋体");
		
		headFront.setFontHeightInPoints((short)12);
		
		headStyle.setFont(headFront);
		
		headStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		
		headStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		
		headStyle.setBorderTop(CellStyle.BORDER_THIN);
		
		headStyle.setBorderBottom(CellStyle.BORDER_THIN);
		
		headStyle.setBorderTop(CellStyle.BORDER_THIN);
		
		headStyle.setBorderRight(CellStyle.BORDER_THIN);
		
		headStyle.setBorderLeft(CellStyle.BORDER_THIN);
		
		return headStyle;
	}

	private CellStyle getLinkCellStyle(Workbook workbook) {
		
	// 超链接样式
	CellStyle linkStyle = workbook.createCellStyle();
	
	Font linkFront = workbook.createFont();
	
	linkFront.setFontName("宋体");
	
	linkFront.setUnderline(Font.U_SINGLE);
	
	linkFront.setFontHeightInPoints((short)12);
	
	linkFront.setColor(IndexedColors.BLUE.getIndex());
	
	linkStyle.setFont(linkFront);
	
	linkStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

	return linkStyle;
}
	
}
