package com.beawan.customer.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.customer.AdmittDto;
import com.beawan.customer.bean.AdmitRecordEntity;
import com.beawan.customer.bean.Admittance;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.dao.AdmitRecordDao;
import com.beawan.customer.service.AdmitRecordService;
import com.beawan.customer.service.IAdmittanceSV;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.model.entity.AdmitQuotaEntity;
import com.beawan.model.service.AdmitQuotaAttrService;
import com.beawan.model.service.AdmitQuotaService;
import com.google.gson.Gson;

import net.sf.json.JSONObject;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/credit" })
public class CreditAdmittanceCtl extends BaseController{
	
	private static Logger log = Logger.getLogger(CreditAdmittanceCtl.class);
	@Resource
	private IAdmittanceSV admittanceSV;
	@Resource
	private ICusBaseSV cusBaseSV;
	@Resource
	private AdmitRecordDao admitRecordDao;
	@Resource
	private AdmitRecordService admitRecordService;
	@Resource
	private AdmitQuotaService admitQuotaServcie;
	@Resource
	private AdmitQuotaAttrService admitQuotaAttrService;
	
	/***
	 * 准入页面跳转
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/admittanceInfo.do")
	public String admittance(HttpServletRequest request,
			HttpServletResponse response,String customerNo)throws Exception{
		HashMap<String,Object> map=new HashMap<String, Object>();
		try {
			Admittance admittance = admittanceSV.getAdmittanceByCustmoerNo(customerNo);
			request.setAttribute("customerNo", customerNo);
			request.setAttribute("admittance", admittance);
		} catch (Exception e) {
			log.error("跳转准入页面异常", e);
		}
		return "views/custInfo/admittanceInfo";
	}
	
	/**
	 * 进行准入分析   并保存
	 * @param request
	 * @param response
	 * @param admittanceStr
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/calculationAdmittance.do")
	@ResponseBody
	public Map<String,Object> calculationAdmittance(HttpServletRequest request,
			HttpServletResponse response,String admittanceStr)throws Exception{
		HashMap<String,Object> map=new HashMap<String, Object>();
		try {
			JSONObject json = JSONObject.fromObject(admittanceStr);
			Admittance admittance = (Admittance)JSONObject.toBean(json, Admittance.class);
			Map<String, AdmittDto> data = admittanceSV.saveAdmittance(admittance);
			map.put("result", "success");
		} catch (Exception e) {
			log.error("计算准入模型异常", e);
		}
		return map;
	}
	
	
	@RequestMapping("/checkCalculationAdmittance.do")
	@ResponseBody
	public Map<String,Object> checkCalculationAdmittance(HttpServletRequest request,
			HttpServletResponse response,String admittanceStr)throws Exception{
		HashMap<String,Object> map=new HashMap<String, Object>();
		Map<String, AdmittDto> data = new HashMap<String, AdmittDto>();
		try {
			String customerNo = request.getParameter("customerNo");
			Admittance admittance = admittanceSV.getAdmittanceByCustmoerNo(customerNo);
			if(admittance==null) {
				map.put("flag", "0");
			}else {
				map.put("flag", "1");
			}
		}catch (Exception e) {
			log.error("计算准入模型异常", e);
		}
		return map;
	}
	
	/**
	 * 查看准入分析结果
	 * @param request
	 * @param response
	 * @param admittanceStr
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/viewcalculationAdmittance.do")
	public String viewcalculationAdmittance(HttpServletRequest request,
			HttpServletResponse response,String admittanceStr)throws Exception{
		HashMap<String,Object> map=new HashMap<String, Object>();
		Map<String, AdmittDto> data = new HashMap<String, AdmittDto>();
		try {
			String customerNo = request.getParameter("customerNo");
			Admittance admittance = admittanceSV.getAdmittanceByCustmoerNo(customerNo);
			data = admittanceSV.listAdmittance(admittance);
			String code = admittance.getAdmittanceResult();
			String result;
			if("0".equals(code)) {
				result = "准入通过";
			}else {
				result = "不予准入";
			}
			List<AdmittDto> list = new ArrayList<AdmittDto>();
			if(data!=null&&data.size()>=0) {
				for(Map.Entry<String, AdmittDto> entry : data.entrySet()) {
					AdmittDto dto = entry.getValue();
					list.add(dto);
				}
			}
			request.setAttribute("result", result);
			request.setAttribute("data", list);
			
		} catch (Exception e) {
			log.error("计算准入模型异常", e);
		}
		return "views/custInfo/admittanceRecord";
	}
	
	//提供给平板的接口
	@RequestMapping("/viewcalculationAdmittanceForIpad.json")
	@ResponseBody
	public ResultDto viewcalculationAdmittanceForIpad(HttpServletRequest request,
			HttpServletResponse response,String customerName)throws Exception{
		ResultDto re = returnFail("");
//		HashMap<String,Object> map=new HashMap<String, Object>();
		Map<String, AdmittDto> data = new HashMap<String, AdmittDto>();
		Admittance admittance;
		try {
			String customerNo = request.getParameter("customerNo");
			if(customerNo!=null && !"".equals(customerNo))
				admittance = admittanceSV.getAdmittanceByCustmoerNo(customerNo);
			else {
				if(customerName==null || "".equals(customerName)) {
					re.setMsg("当前客户信息异常");
					return re;
				}
				CusBase cus = cusBaseSV.queryByCusName(customerName);
				admittance = admittanceSV.getAdmittanceByCustmoerNo(cus.getCustomerNo());
			}
			data = admittanceSV.listAdmittance(admittance);
			String code = admittance.getAdmittanceResult();
			String result;
			if("0".equals(code)) {
				result = "准入通过";
			}else {
				result = "不予准入";
			}
			List<AdmittDto> list = new ArrayList<AdmittDto>();
			if(data!=null&&data.size()>=0) {
				for(Map.Entry<String, AdmittDto> entry : data.entrySet()) {
					AdmittDto dto = entry.getValue();
					list.add(dto);
				}
			}
			Map<String,Object> rows = new HashMap<>();
			rows.put("result", result);
			rows.put("data", list);

			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setRows(list);
			re.setMsg("数据获取成功");
			
		} catch (Exception e) {
			log.error("计算准入模型异常", e);
		}
		return re;
	}
	
	@RequestMapping("/getCusAdmitModel.do")
	@ResponseBody
	public ResultDto getCusAdmitModel(String customerNo){
		ResultDto re = returnFail("获取客户准入模型失败");
		
		AdmitRecordEntity admitRecord = admitRecordDao.selectSingleByProperty("customerNo", customerNo);
		if(admitRecord!=null){
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setRows(admitRecord);
		}
		return re;
	}
	
	@RequestMapping("/saveCusAdmitRecord.do")
	@ResponseBody
	public ResultDto saveCusAdmitRecord(String jsonString, String customerNo, String identifCode){
		ResultDto re = returnFail("保存客户准入指标记录失败");
		
		try {
			Gson gson = new Gson();
			
			AdmitRecordEntity admitRecord = admitRecordService.selectSingleByProperty("customerNo", customerNo);
			if(admitRecord == null){
				admitRecord = new AdmitRecordEntity();
				admitRecord.setCustomerNo(customerNo);
			}
			admitRecord.setIdentifCode(identifCode);
			admitRecord.setQuotaResult(jsonString);
			
			admitRecordService.saveOrUpdate(admitRecord);
			
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return re;
		
	}
	
	@RequestMapping("/analysisCusAdmitRecord.do")
	@ResponseBody
	public ResultDto checkCusAdmitRecord(String customerNo, HttpServletRequest request){
		ResultDto re = returnFail("获取客户准入指标结果失败");
		
		try {
			String admitResult = admitRecordService.getAdmitResult(customerNo, request);
			
			AdmitRecordEntity admitRecord = admitRecordService.selectSingleByProperty("customerNo", customerNo);
			if(admitRecord ==null){
				re.setCode(ResultDto.RESULT_CODE_FAIL);
			}else{
				re.setCode(ResultDto.RESULT_CODE_SUCCESS);
				re.setRows(admitRecord);
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.error("获取客户准入指标结果失败", e);
			re.setCode(ResultDto.RESULT_CODE_ERROR);
		}
		return re;
	}
	
	@RequestMapping("/getCusAdmitRecord.do")
	public String getCusAdmitRecord(String customerNo, HttpServletRequest request){
		
		AdmitRecordEntity admitRecord = admitRecordService.selectSingleByProperty("customerNo", customerNo);
		String quotaResultString = admitRecord.getQuotaResult();
		String quotaDescribeString = admitRecord.getQuotaDescribe();
		String admitResult = admitRecord.getAdmitResult();
		
		Gson gson = new Gson();
		HashMap<String, Object> quotaResultMap = gson.fromJson(quotaResultString, HashMap.class);
		HashMap<String, Object> quotaDescribeMap = gson.fromJson(quotaDescribeString, HashMap.class);
		
		String result = "";
		List<Map<String, Object>> admitResults = new ArrayList();
		for (String key : quotaResultMap.keySet()) {
			AdmitQuotaEntity admitQuota = admitQuotaServcie.selectSingleByProperty("quotaCode", key);
			Map<String, Object> admitDataMap = new HashMap();
			
			StringBuilder sql = new StringBuilder("1=1");
			sql.append(" and quotaCode = :quotaCode");
			sql.append(" and optionCode = :optionCode");
			Map<String, Object> params = new HashMap<>();
			params.put("quotaCode", admitQuota.getQuotaCode());
		
			if(admitResult.equals("B")||admitResult.equals("Z")){
				if(quotaResultMap.get(key) != null){
					if(quotaResultMap.get(key).equals(admitResult)){
						admitDataMap.put("quotaName", admitQuota.getQuotaName());
						admitDataMap.put("content", quotaDescribeMap.get(key));
						admitDataMap.put("quotaResult", quotaResultMap.get(key));
						admitResults.add(admitDataMap);
					}
				}
			}
		}
		
		request.setAttribute("admitResults", admitResults);
		request.setAttribute("result", admitResult);
		return "views/custInfo/admittanceRecord";
	}
}
