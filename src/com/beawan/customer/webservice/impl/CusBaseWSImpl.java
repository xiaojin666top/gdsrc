package com.beawan.customer.webservice.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;

import com.beawan.base.entity.User;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.util.WSResult;
import com.beawan.customer.bean.Admittance;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.service.IAdmittanceSV;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.customer.webservice.ICusBaseWS;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.platform.util.DateUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;
import com.beawan.customer.qbean.QCusBase;
@Service("cusBaseWS")
public class CusBaseWSImpl implements ICusBaseWS{
	
	private static final Logger log = Logger.getLogger(ICusBaseWS.class);
	
	@Resource
	private ICusBaseSV cusbaseSV;
	@Resource
	private ICmisInInvokeSV cmisInInvokeSV;
	@Resource
	private IAdmittanceSV admittanceSV;
	
	@Override
	public String queryCusBasePaging(String queryCondition, String orderCondition, 
			int index, int count)
		{
		
		WSResult result=new WSResult();
		ObjectMapper mapper = new ObjectMapper();
		CusBase cusBase = null;
		
		try {
			
			cusBase = mapper.readValue(queryCondition,CusBase.class);
			//默认查对公客户
			if(StringUtil.isEmptyString(cusBase.getCustomerType()))
				cusBase.setCustomerType(SysConstants.CusNoType.COMP_CUS);
			
			List<CusBase> list = cmisInInvokeSV.queryCustomerPaging(cusBase,
					orderCondition, index, count);
			long total = cmisInInvokeSV.queryCustomerCount(cusBase);
			
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put("total", total);
			dataMap.put("rows", list);
			
			/*Map<String, Object> dataMap = cusbaseSV.queryCusBasePaging(cusBase, 
					orderCondition, index, count);*/
			
			if(CollectionUtils.isEmpty(dataMap))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(dataMap);
		} catch (Exception e) {
			
			result.status = WSResult.STATUS_ERROR;
			log.error("获取客户列表异常！",e);
			e.printStackTrace();
		}
		
		return result.json();
	}

	@Override
	public String queryCusOfMPaging(String queryCondition, String userInfo, String orderCondition, int index,
			int count) {
		WSResult result=new WSResult();
		User user=null;
		QCusBase qCusBase=null;
		try {
			if(!StringUtil.isEmptyString(userInfo)) {
				 user=JacksonUtil.fromJson(userInfo, User.class);
			}
			
			if(!StringUtil.isEmptyString(queryCondition)) {
				qCusBase=JacksonUtil.fromJson(queryCondition, QCusBase.class);
			}
			
			if(qCusBase==null) 
				qCusBase=new QCusBase();
			
			String userDA = user.getDataAccess();
			if(SysConstants.UserDataAccess.PERSONAL.equals(userDA))
				qCusBase.setManagerUserId(user.getUserId());
			else if(SysConstants.UserDataAccess.ORG.equals(userDA))
				qCusBase.setManagerOrgId(user.getAccOrgNo());
			
			
			List<Map<String, Object>> list = cusbaseSV.queryPaging(qCusBase,
					orderCondition, index, count);
			long total = cusbaseSV.queryCount(qCusBase);
	
			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put("total", total);
			dataMap.put("rows", list);
			
			if(CollectionUtils.isEmpty(dataMap))
				result.data = null;
			else
				result.data = JacksonUtil.serialize(dataMap);
		} 
		 catch (Exception e) {
			// TODO Auto-generated catch block
			 result.status = WSResult.STATUS_ERROR;
				log.error("获取客户列表异常！",e);
				e.printStackTrace();
		}

		return result.json();
	}

	@Override
	public String saveNewCustomer(String cusinfo, String userInfo) {
		WSResult result=new WSResult();
		try {
			CusBase cusBase=JacksonUtil.fromJson(cusinfo, CusBase.class);
		
			//从信贷系统导入
			if(StringUtil.isEmptyString(cusBase.getCustomerType())){
				
				cusbaseSV.save(cusBase);
				cmisInInvokeSV.syncCusInfo(cusBase.getCustomerNo());
				
			}else{//新增草稿户
				
				User   user=JacksonUtil.fromJson(userInfo, User.class);
				String date = DateUtil.format(new Date(), Constants.DATE_MASK);
				
				cusBase.setInputUserId(user.getUserId());
				cusBase.setInputOrgId(user.getAccOrgNo());
				cusBase.setInputDate(date);
				cusBase.setManagerUserId(user.getUserId());
				cusBase.setManagerOrgId(user.getAccOrgNo());
				cusBase.setUpdateDate(date);
				
				cusbaseSV.save(cusBase);
			}
			
			
			
		} catch (Exception e) {
			
			result.status=WSResult.STATUS_ERROR;
			log.error("系统异常！：", e);
		}
		
		
		return result.json();
	}

	@Override
	public String deleteCustomer(String customerNo, String trash) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String syncCustomer(String customerNo) {
		WSResult result=new WSResult();
		try {
			
			cmisInInvokeSV.syncCusInfo(customerNo);
			
		} catch (Exception e) {
			
			result.status=WSResult.STATUS_ERROR;
			log.error("系统异常！：", e);
		}
		
		return result.json();
	}

	@Override
	public String checkCredit(String customerNo) {
		
		WSResult result=new WSResult();
		
		try {
			
			Admittance admittance = admittanceSV.getAdmittanceByCustmoerNo(customerNo);
			
			Map<String, Object> dataMap=new HashMap<String, Object>();
			
			dataMap.put("admittance", admittance);
			
			if(CollectionUtils.isEmpty(dataMap)) {
				result.data=null;
			}else {
				result.data=JacksonUtil.serialize(dataMap);
			}
			
		} catch (Exception e) {
			result.status=WSResult.STATUS_ERROR;
			log.error("系统异常！：",e);
		}
		
		return result.json();
	}



}
