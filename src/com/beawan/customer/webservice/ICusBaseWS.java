package com.beawan.customer.webservice;

import java.util.Map;

public interface ICusBaseWS {
	/**
	 * @TODO 分页查询
	 * @param queryCondition 查询条件
	 * @param orderCondition 排序条件
	 * @param index 起始游标
	 * @param count 每页数量
	 * @return
	 * @throws Exception
	 */
	public String queryCusBasePaging(String queryCondition,
			String orderCondition, int index, int count);
	
	
	
	/**
	 * @TODO 分页查询(客户经理管理的客户)
	 * @param queryCondition 查询条件
	 * @param userInfo 客户经理信息
	 * @param orderCondition 排序条件
	 * @param index 起始游标
	 * @param count 每页数量
	 * @return
	 * @throws Exception
	 */
	public String queryCusOfMPaging(String queryCondition,String userInfo,
			String orderCondition, int index, int count);
	
	/**
	 * @TODO 保存新增客户
	 * @param cusinfo 客户信息
	 * @param userinfo 用户信息
	 * @return
	 * @throws Exception
	 */
	public String saveNewCustomer(String cusinfo,String userInfo);
	
	
	/**
	 * @TODO 删除客户
	 * @param customerNo 客户编号
	 * @param userinfo 用户信息
	 * @return
	 * @throws Exception
	 */
	public String deleteCustomer(String customerNo,String trash);
	
	
	/**
	 * TODO 客户基本信息同步（从信贷系统）
	 * @param customerNo 客户编号
	 * @return
	 * @throws Exception
	 */
	public String syncCustomer(String customerNo);
	
	/**TODO 调查客户是否准入
	 * @param customerNo 客户号
	 * @return
	 */
	public String checkCredit(String customerNo);
}
