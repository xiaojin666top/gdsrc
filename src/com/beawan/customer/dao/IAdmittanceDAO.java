package com.beawan.customer.dao;

import java.util.List;

import com.beawan.common.dao.ICommDAO;
import com.beawan.customer.bean.Admittance;

public interface IAdmittanceDAO extends ICommDAO<Admittance> {

	List<Admittance> getAdmittanceByCustmoerNo(String custmoerNo) throws Exception;
}
