package com.beawan.customer.dao;

import com.beawan.core.BaseDao;
import com.beawan.customer.bean.AdmitRecordEntity;

public interface AdmitRecordDao extends BaseDao<AdmitRecordEntity> {
	
}
