package com.beawan.customer.dao;

import com.beawan.core.BaseDao;
import com.beawan.customer.bean.FinriskResult;

/**
 * @author yzj
 */
public interface FinriskResultDao extends BaseDao<FinriskResult>  {
}
