package com.beawan.customer.dao;

import com.beawan.core.BaseDao;
import com.beawan.customer.bean.CusFSRecord;

/**
 * @Description
 * @author rain
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public interface ICusFSRecordDAO extends BaseDao<CusFSRecord> {
	/**
	 * TODO 获取客户最新一期财报日期
	 * @param 
	 * @throws Exception
	 */
	String getMaxReportDate(String customerNo);


	/**
	 * TODO 获取客户当年前一期财报营业收入
	 * @param customerNo 客户号
	 * @param nowDate 当期财报日期
	 */
	Double getOldReportTurnover(String customerNo, String nowDate);
	
}
