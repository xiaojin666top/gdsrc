package com.beawan.customer.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.beawan.common.dao.ICommDAO;
import com.beawan.customer.bean.CreditRating;

public interface ICreditRateDAO extends ICommDAO<CreditRating>{

	/**
	 * @Description (根据客户号查询评级各项指标)
	 * @param customerNo 客户号
	 * @return List<CreditRating>
	 * @throws Exception
	 */
	public List<CreditRating> queryCreditRateListByCustomerNo(String customerNo,String reportDate) throws DataAccessException;
}
