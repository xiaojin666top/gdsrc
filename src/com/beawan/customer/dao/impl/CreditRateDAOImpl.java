package com.beawan.customer.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.customer.bean.CreditRating;
import com.beawan.customer.dao.ICreditRateDAO;

@Repository("creditRateDAO")
public class CreditRateDAOImpl extends CommDAOImpl<CreditRating> implements ICreditRateDAO{

	@Override
	public List<CreditRating> queryCreditRateListByCustomerNo(String customerNo,String reportDate) throws DataAccessException {
		// TODO Auto-generated method stub
		
		String query = "customerNo=:customerNo and reportDate=:reportDate";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("customerNo", customerNo);
		params.put("reportDate", reportDate);

		
		return select(query, params);
	}

}
