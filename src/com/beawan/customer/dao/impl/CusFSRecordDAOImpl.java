package com.beawan.customer.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.beawan.common.Constants;
import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.core.BaseDaoImpl;
import com.beawan.customer.bean.CusFSRecord;
import com.beawan.customer.dao.ICusFSRecordDAO;
import com.beawan.survey.guarantee.entity.GuaChattel;

@Service("cusFSRecordDAO")
public class CusFSRecordDAOImpl extends BaseDaoImpl<CusFSRecord> implements ICusFSRecordDAO {

	@Override
	public String getMaxReportDate(String customerNo) {

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT REPORT_DATE FROM CUS_FSRECORD ")
				.append("WHERE CUSTOMER_ID =?0 and status=?1 order by  REPORT_DATE DESC FETCH FIRST 1 ROWS ONLY ");
		return findCustOneBySql(String.class, sql, customerNo, Constants.NORMAL);

	}

	@Override
	public Double getOldReportTurnover(String customerNo, String nowDate) {
		String[] date = nowDate.split("-");
		String oldDate = (Integer.parseInt(date[0]) - 1) + "-12";

		StringBuilder sql = new StringBuilder();
		sql.append("SELECT COL2_VALUE FROM ")
				.append(" (SELECT * FROM CUS_FSRECORD WHERE CUSTOMER_ID =?0 AND  REPORT_DATE>?1 ")
				.append(" AND REPORT_DATE<?2 ORDER BY REPORT_DATE DESC FETCH FIRST 1 ROWS ONLY) A ")
				.append(" JOIN CUS_REPORT_RECORD B ON A.RECORD_NO=B.OBJECT_NO ")
				.append(" JOIN CUS_REPORT_DATA C ON B.REPORT_NO=C.REPORT_NO ")
				.append(" WHERE (B.MODEL_NO='0020'OR B.MODEL_NO='0120') AND C.ROW_SUBJECT='301' ");
		// 301 主营收入
		return findCustOneBySql(Double.class, sql, customerNo, oldDate, nowDate);

	}

}
