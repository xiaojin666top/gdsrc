package com.beawan.customer.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.customer.bean.Admittance;
import com.beawan.customer.dao.IAdmittanceDAO;

@Repository("admittanceDAO")
public class AdmittanceDAOImpl extends CommDAOImpl<Admittance> implements IAdmittanceDAO {

	@Override
	public List<Admittance> getAdmittanceByCustmoerNo(String custmoerNo) throws Exception {
		
		String queryString = "from Admittance as model where model.customerNo=:customerNo";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("customerNo", custmoerNo);
		return execQuery(Admittance.class,queryString,params);
		
	}

}
