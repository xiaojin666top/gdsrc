package com.beawan.customer.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.customer.bean.FinriskResult;
import com.beawan.customer.dao.FinriskResultDao;

/**
 * @author yzj
 */
@Repository("finriskResultDao")
public class FinriskResultDaoImpl extends BaseDaoImpl<FinriskResult> implements FinriskResultDao{

}
