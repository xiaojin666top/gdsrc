package com.beawan.customer.dao.impl;

import com.beawan.core.BaseDaoImpl;
import org.springframework.stereotype.Repository;

import com.beawan.common.dao.impl.CommDAOImpl;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.dao.ICusBaseDAO;

@Repository("cusBaseDAO")
public class CusBaseDAOImpl extends BaseDaoImpl<CusBase> implements ICusBaseDAO {

}
