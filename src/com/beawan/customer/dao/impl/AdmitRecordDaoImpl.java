package com.beawan.customer.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.customer.bean.AdmitRecordEntity;
import com.beawan.customer.dao.AdmitRecordDao;

@Repository("admitRecordDao")
public class AdmitRecordDaoImpl extends BaseDaoImpl<AdmitRecordEntity> implements AdmitRecordDao{

}
