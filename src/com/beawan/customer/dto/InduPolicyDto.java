package com.beawan.customer.dto;

import java.io.Serializable;

public class InduPolicyDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4585278210373012141L;
	
	private Long id; 
	private String induCode;     //行业代码
	private String induName;     //行业名字
	private String publishDate;   //发布时间
	private String department; //发布部门
	private String fileName;  //发布文件
	private String fileGist;  //文件要点
	private Integer status;//状态0正常 1删除
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getInduCode() {
		return induCode;
	}
	public void setInduCode(String induCode) {
		this.induCode = induCode;
	}
	public String getInduName() {
		return induName;
	}
	public void setInduName(String induName) {
		this.induName = induName;
	}
	public String getPublishDate() {
		return publishDate;
	}
	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileGist() {
		return fileGist;
	}
	public void setFileGist(String fileGist) {
		this.fileGist = fileGist;
	}

	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "InduPolicy [induCode=" + induCode + ", induName=" + induName + ", publishDate=" + publishDate
				+ ", department=" + department + ", fileName=" + fileName + ", fileGist=" + fileGist + ", status="
				+ status + "]";
	}
	
	
	

}
