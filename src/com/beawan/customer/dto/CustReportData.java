package com.beawan.customer.dto;

import java.util.List;

/**
 * 客户报表数据
 * 
 * @author beawan
 *
 */
public class CustReportData {

	
	
	private long id;//主键ID
	
	private String customerNo;// 客户编号
	private String customerName;// 客户名，信贷系统不提供，自动根据客户编号生成

	private long comId;//上市公司Id
	private String userNo;//用户编号
	private String comCode;//上市公司股票代码
	private String remark;//备注
	private String industryCode;// 行业代码
	
	
	private List<ReportDataOneItem> reportDataOneItem;// 多期的报表数据

	
	
	public CustReportData() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	public CustReportData(String customerNo, String industryCode) {
		super();
		this.customerNo = customerNo;
		this.industryCode = industryCode;
	}


	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}
	
	
	public long getComId() {
		return comId;
	}

	public void setComId(long comId) {
		this.comId = comId;
	}

	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}

	public String getComCode() {
		return comCode;
	}

	public void setComCode(String comCode) {
		this.comCode = comCode;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}



	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public List<ReportDataOneItem> getReportDataOneItem() {
		return reportDataOneItem;
	}

	public void setReportDataOneItem(List<ReportDataOneItem> reportDataOneItem) {
		this.reportDataOneItem = reportDataOneItem;
	}

}
