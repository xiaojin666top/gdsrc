package com.beawan.customer.dto;

import java.util.List;

/***
 * 行业对接接口返回值
 */
public class InduMapDto {

    //行业概述
    private String surveys;
    //行业政策
    private List<InduPolicyDto> policys;
    //行业壁垒
    private List<InduRiskDto> induBarrier;
    //有利因素
    private List<InduRiskDto> beneficials;
    //不利因素
    private List<InduRiskDto> harmfuls;
    //风险因素
    private List<InduRiskDto> risks;


    public String getSurveys() {
        return surveys;
    }

    public void setSurveys(String surveys) {
        this.surveys = surveys;
    }

    public List<InduPolicyDto> getPolicys() {
        return policys;
    }

    public void setPolicys(List<InduPolicyDto> policys) {
        this.policys = policys;
    }

    public List<InduRiskDto> getInduBarrier() {
        return induBarrier;
    }

    public void setInduBarrier(List<InduRiskDto> induBarrier) {
        this.induBarrier = induBarrier;
    }

    public List<InduRiskDto> getBeneficials() {
        return beneficials;
    }

    public void setBeneficials(List<InduRiskDto> beneficials) {
        this.beneficials = beneficials;
    }

    public List<InduRiskDto> getHarmfuls() {
        return harmfuls;
    }

    public void setHarmfuls(List<InduRiskDto> harmfuls) {
        this.harmfuls = harmfuls;
    }

    public List<InduRiskDto> getRisks() {
        return risks;
    }

    public void setRisks(List<InduRiskDto> risks) {
        this.risks = risks;
    }
}
