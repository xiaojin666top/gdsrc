package com.beawan.customer.dto;


/**
 * 流动资金缺口测算表 子项
 */
public class QuotaItemDto {

    private String name;
    private String safety;//安全系数
    private Double startVal;//期初值
    private Double endVaL;//期末值

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSafety() {
        return safety;
    }

    public void setSafety(String safety) {
        this.safety = safety;
    }

    public Double getStartVal() {
        return startVal;
    }

    public void setStartVal(Double startVal) {
        this.startVal = startVal;
    }

    public Double getEndVaL() {
        return endVaL;
    }

    public void setEndVaL(Double endVaL) {
        this.endVaL = endVaL;
    }
}
