package com.beawan.customer.dto;

public class InduRiskDto {
	
	private String title;
	private String content;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	@Override
	public String toString() {
		return "InduRisk [title=" + title + ", content=" + content + "]";
	}
	
	

}
