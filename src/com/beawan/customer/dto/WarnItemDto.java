package com.beawan.customer.dto;

import java.util.List;
import java.util.Map;

/**
 * 预警提示DTO
 * @author zxh
 * @date 2020/8/26 14:37
 */
public class WarnItemDto {

    private String serNo;
    private String abnormalItem; // 异常项
    private String errReason; // 现象描述
    private String reason; // 异常原因
    private String reasonList; // 异常提示(json): ["reason1","reason2",...]
    private String remark;// 异常说明

    public WarnItemDto(String serNo, String abnormalItem, String errReason, String reasonList) {
        this.serNo = serNo;
        this.abnormalItem = abnormalItem;
        this.errReason = errReason;
        this.reasonList = reasonList;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getAbnormalItem() {
        return abnormalItem;
    }

    public void setAbnormalItem(String abnormalItem) {
        this.abnormalItem = abnormalItem;
    }

    public String getErrReason() {
        return errReason;
    }

    public void setErrReason(String errReason) {
        this.errReason = errReason;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReasonList() {
        return reasonList;
    }

    public void setReasonList(String reasonList) {
        this.reasonList = reasonList;
    }
}
