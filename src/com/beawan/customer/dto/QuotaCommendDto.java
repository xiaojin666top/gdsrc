package com.beawan.customer.dto;

/**
 * 推荐额度 数据类
 */
public class QuotaCommendDto {

    private String name;//测算法名称
    private String detail;//描述
    private String num;//测算额度
    private String formula;//单位还是啥的

    public QuotaCommendDto() {
    }

    public QuotaCommendDto(String name, String detail, String num, String formula) {
        this.name = name;
        this.detail = detail;
        this.num = num;
        this.formula = formula;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }
}
