package com.beawan.customer.dto;


import java.util.List;

/**
 * 3个维度推荐额度 和 流动资金测算表  综合数据类
 */
public class QuotaFullDto {

    private String month;
    private String year;
    private List<QuotaCommendDto> quotaArrayList1;//营业收入测算
    private List<QuotaCommendDto> quotaArrayList2;//银监会流动资金缺口测算
    private List<QuotaCommendDto> quotaArrayList3;//资产负债测算
    private List<QuotaItemDto> dtoList;//详细流动资金缺口测算表

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public List<QuotaCommendDto> getQuotaArrayList1() {
        return quotaArrayList1;
    }

    public void setQuotaArrayList1(List<QuotaCommendDto> quotaArrayList1) {
        this.quotaArrayList1 = quotaArrayList1;
    }

    public List<QuotaCommendDto> getQuotaArrayList2() {
        return quotaArrayList2;
    }

    public void setQuotaArrayList2(List<QuotaCommendDto> quotaArrayList2) {
        this.quotaArrayList2 = quotaArrayList2;
    }

    public List<QuotaCommendDto> getQuotaArrayList3() {
        return quotaArrayList3;
    }

    public void setQuotaArrayList3(List<QuotaCommendDto> quotaArrayList3) {
        this.quotaArrayList3 = quotaArrayList3;
    }

    public List<QuotaItemDto> getDtoList() {
        return dtoList;
    }

    public void setDtoList(List<QuotaItemDto> dtoList) {
        this.dtoList = dtoList;
    }
}
