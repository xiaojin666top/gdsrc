package com.beawan.customer.dto;

import java.io.Serializable;

/**
 * @ClassName ReportDataSimple
 * @Description (一份报表的数据)
 * @author innnnk
 * @Date 2018年9月20日
 * @version 1.0.0
 * @Company 杭州碧湾信息技术有限公司
 */
public class ReportDataSimple implements Serializable {

	private long id;
	/** @Field @name : (表格每行科目名字) */
	private String name;

	/** @Field @value : (期末值) */
	private Double value;

	/** @Field @value_early : (期初值) */
	private Double value_early;

	/** @Field @type : (报表名，如：利润表、资产负债表、现金流量表) */
	private String type;
	/** @Field @rankOrder : 一期的id,用于标记在哪一期 */
	private long ReportDataItemId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getReportDataItemId() {
		return ReportDataItemId;
	}

	public void setReportDataItemId(long reportDataItemId) {
		ReportDataItemId = reportDataItemId;
	}

	public ReportDataSimple() {
		super();
	}

	public ReportDataSimple(String name) {
		super();
		this.name = name;
	}
	public ReportDataSimple(String name,String type) {
		super();
		this.name = name;
		this.type = type;
	}
	public Double getValue_early() {
		return value_early;
	}

	public void setValue_early(Double value_early) {
		this.value_early = value_early;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	@Override
	public boolean equals(Object obj) {
		if (null == obj || null == name||type==null) {
			return false;
		}
		if (obj instanceof ReportDataSimple) {
			ReportDataSimple other = (ReportDataSimple) obj;
			return name.equals(other.getName())&&other.getType().contains(type);
		} else {
			return false;
		}

	}

	@Override
	public String toString() {
		return "ReportDataSimple [name=" + name + ", value=" + value + ", value_early=" + value_early + ", type=" + type
				+ ", ReportDataItemId=" + ReportDataItemId + "]";
	}
	
	

}
