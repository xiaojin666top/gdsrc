package com.beawan.customer.dto;

public class InduStandardDto {

	private String indexName;
	private String firstVal;
	private String firstState;
	private String secondVal;
	private String secondState;
	private String thirdVal;
	private String thirdState;
	private String avgVal;
	
	public InduStandardDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
//	不包含行业均值
	public InduStandardDto(String indexName, String firstVal, String firstState, String secondVal, String secondState,
			String thirdVal, String thirdState) {
		super();
		this.indexName = indexName;
		this.firstVal = firstVal;
		this.firstState = firstState;
		this.secondVal = secondVal;
		this.secondState = secondState;
		this.thirdVal = thirdVal;
		this.thirdState = thirdState;
	}
	
	
	//包含行业均值
	public InduStandardDto(String indexName, String firstVal, String firstState, String secondVal, String secondState,
			String thirdVal, String thirdState, String avgVal) {
		super();
		this.indexName = indexName;
		this.firstVal = firstVal;
		this.firstState = firstState;
		this.secondVal = secondVal;
		this.secondState = secondState;
		this.thirdVal = thirdVal;
		this.thirdState = thirdState;
		this.avgVal = avgVal;
	}
	public String getIndexName() {
		return indexName;
	}
	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}
	public String getFirstVal() {
		return firstVal;
	}
	public void setFirstVal(String firstVal) {
		this.firstVal = firstVal;
	}
	public String getFirstState() {
		return firstState;
	}
	public void setFirstState(String firstState) {
		this.firstState = firstState;
	}
	public String getSecondVal() {
		return secondVal;
	}
	public void setSecondVal(String secondVal) {
		this.secondVal = secondVal;
	}
	public String getSecondState() {
		return secondState;
	}
	public void setSecondState(String secondState) {
		this.secondState = secondState;
	}
	public String getThirdVal() {
		return thirdVal;
	}
	public void setThirdVal(String thirdVal) {
		this.thirdVal = thirdVal;
	}
	public String getThirdState() {
		return thirdState;
	}
	public void setThirdState(String thirdState) {
		this.thirdState = thirdState;
	}
	public String getAvgVal() {
		return avgVal;
	}
	public void setAvgVal(String avgVal) {
		this.avgVal = avgVal;
	}
	
	

	
	
}
