package com.beawan.customer.dto;

import java.util.List;

public class FinriskRatioListDto {

	String ratioName;
	List<String> valList;
	
	
	
	public FinriskRatioListDto(String ratioName, List<String> valList) {
		super();
		this.ratioName = ratioName;
		this.valList = valList;
	}
	public FinriskRatioListDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getRatioName() {
		return ratioName;
	}
	public void setRatioName(String ratioName) {
		this.ratioName = ratioName;
	}
	public List<String> getValList() {
		return valList;
	}
	public void setValList(List<String> valList) {
		this.valList = valList;
	}
	
	
}
