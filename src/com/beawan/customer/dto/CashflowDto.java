package com.beawan.customer.dto;

public class CashflowDto {

	private String id;
	private String name;
	private String firstYearValue;
	private String secondYearValue;
	private String thirdYearValue;
	private String fourthYearValue;
	private String lastYearTermValue;
	private String index;
	private String reportDataId;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFirstYearValue() {
		return firstYearValue;
	}
	public void setFirstYearValue(String firstYearValue) {
		this.firstYearValue = firstYearValue;
	}
	public String getSecondYearValue() {
		return secondYearValue;
	}
	public void setSecondYearValue(String secondYearValue) {
		this.secondYearValue = secondYearValue;
	}
	public String getThirdYearValue() {
		return thirdYearValue;
	}
	public void setThirdYearValue(String thirdYearValue) {
		this.thirdYearValue = thirdYearValue;
	}
	public String getFourthYearValue() {
		return fourthYearValue;
	}
	public void setFourthYearValue(String fourthYearValue) {
		this.fourthYearValue = fourthYearValue;
	}
	public String getLastYearTermValue() {
		return lastYearTermValue;
	}
	public void setLastYearTermValue(String lastYearTermValue) {
		this.lastYearTermValue = lastYearTermValue;
	}
	public String getIndex() {
		return index;
	}
	public void setIndex(String index) {
		this.index = index;
	}
	public String getReportDataId() {
		return reportDataId;
	}
	public void setReportDataId(String reportDataId) {
		this.reportDataId = reportDataId;
	}
	
	
}
