package com.beawan.customer.dto;

public class Coordinate {

	private int ordinate;// 纵坐标
	private int abscissa;// 横坐标
	
	private int item;//当前科目
	
	public Coordinate() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Coordinate(int ordinate, int abscissa) {
		super();
		this.ordinate = ordinate;
		this.abscissa = abscissa;
	}

	public Coordinate(int ordinate, int abscissa,int item) {
		super();
		this.ordinate = ordinate;
		this.abscissa = abscissa;
		this.item = item;
	}
	

	public int getAbscissa() {
		return abscissa;
	}
	/**
	 * 设置横坐标
	 * @param abscissa
	 */
	public void setAbscissa(int abscissa) {
		this.abscissa = abscissa;
	}
	public int getOrdinate() {
		return ordinate;
	}
	/**
	 * 设置纵坐标
	 * @param abscissa
	 */
	public void setOrdinate(int ordinate) {
		this.ordinate = ordinate;
	}

	public int getItem() {
		return item;
	}

	public void setItem(int item) {
		this.item = item;
	}
	
	
	
	
}
