package com.beawan.customer.dto;

import java.util.List;

/**
 * 
 * @author zhufayu
 *
 */
public class ReportDataOneItem {

	
	private long id;
	private Integer year; //**
	private Integer month;//**
	private String reportRule;//报表准则   0新  1旧 ***
	private String customerNo;// 客户编号,用于连接客户的标识
	private String reportNo;//一期的number,信贷系统传输过来，标识一期
	private Double riskScore;//风险分
	private Double riskScoreLocal;//本地风险分

	private String applicationTime;//提交时间
	
	
	private List<ReportDataSimple> reportLists;
	
	
	
	
	public ReportDataOneItem() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	public ReportDataOneItem(Integer year, Integer month, String reportRule) {
		super();
		this.year = year;
		this.month = month;
		this.reportRule = reportRule;
	}


	public String getApplicationTime() {
		return applicationTime;
	}

	public void setApplicationTime(String applicationTime) {
		this.applicationTime = applicationTime;
	}

	public Double getRiskScore() {
		return riskScore;
	}

	public void setRiskScore(Double riskScore) {
		this.riskScore = riskScore;
	}

	public Double getRiskScoreLocal() {
		return riskScoreLocal;
	}

	public void setRiskScoreLocal(Double riskScoreLocal) {
		this.riskScoreLocal = riskScoreLocal;
	}

	

	public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getReportNo() {
		return reportNo;
	}

	public void setReportNo(String reportNo) {
		this.reportNo = reportNo;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}



	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public String getReportRule() {
		return reportRule;
	}

	public void setReportRule(String reportRule) {
		this.reportRule = reportRule;
	}

	public List<ReportDataSimple> getReportLists() {
		return reportLists;
	}

	public void setReportLists(List<ReportDataSimple> reportLists) {
		this.reportLists = reportLists;
	}

}
