package com.beawan.customer.dto;

import java.util.List;

import com.beawan.customer.bean.ErrAbnorDto;
import com.beawan.customer.bean.RatioUpDownDto;

public class AbnormalWarnDto {

	private Double riskScore;
	private String[] summary;
	
	private List<ErrAbnorDto> errAbnor;
	private List<RatioUpDownDto> ratiosUpDown;
	
	
	
	
	public Double getRiskScore() {
		return riskScore;
	}
	public void setRiskScore(Double riskScore) {
		this.riskScore = riskScore;
	}
	public String[] getSummary() {
		return summary;
	}
	public void setSummary(String[] summary) {
		this.summary = summary;
	}
	public List<ErrAbnorDto> getErrAbnor() {
		return errAbnor;
	}
	public void setErrAbnor(List<ErrAbnorDto> errAbnor) {
		this.errAbnor = errAbnor;
	}
	public List<RatioUpDownDto> getRatiosUpDown() {
		return ratiosUpDown;
	}
	public void setRatiosUpDown(List<RatioUpDownDto> ratiosUpDown) {
		this.ratiosUpDown = ratiosUpDown;
	}
	
	
}
