package com.beawan.customer.dto;

import java.util.List;

public class FinriskRatioDto {

	String ratioName;
	String ratioVal;
	
	
	
	
	public FinriskRatioDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public FinriskRatioDto(String ratioName, String ratioVal) {
		super();
		this.ratioName = ratioName;
		this.ratioVal = ratioVal;
	}
	public String getRatioName() {
		return ratioName;
	}
	public void setRatioName(String ratioName) {
		this.ratioName = ratioName;
	}
	public String getRatioVal() {
		return ratioVal;
	}
	public void setRatioVal(String ratioVal) {
		this.ratioVal = ratioVal;
	}
	
	
}
