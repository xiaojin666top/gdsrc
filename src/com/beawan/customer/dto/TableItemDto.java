package com.beawan.customer.dto;

/**
 * 用来存放每一行表格数据  
 * @author yuzhejia
 *
 */
public class TableItemDto {

	private String indexName;
	private String firstVal;
	private String secondVal;
	private String secondChange;
	private String thirdVal;
	private String thirdChange;
	private String fourthVal;
	private String fourthChange;
	
	public TableItemDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
	public TableItemDto(String indexName, String firstVal, String secondVal, String secondChange, String thirdVal,
			String thirdChange, String fourthVal, String fourthChange) {
		super();
		this.indexName = indexName;
		this.firstVal = firstVal;
		this.secondVal = secondVal;
		this.secondChange = secondChange;
		this.thirdVal = thirdVal;
		this.thirdChange = thirdChange;
		this.fourthVal = fourthVal;
		this.fourthChange = fourthChange;
	}



	public String getIndexName() {
		return indexName;
	}

	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}

	public String getFirstVal() {
		return firstVal;
	}

	public void setFirstVal(String firstVal) {
		this.firstVal = firstVal;
	}

	public String getSecondVal() {
		return secondVal;
	}

	public void setSecondVal(String secondVal) {
		this.secondVal = secondVal;
	}

	public String getSecondChange() {
		return secondChange;
	}

	public void setSecondChange(String secondChange) {
		this.secondChange = secondChange;
	}

	public String getThirdVal() {
		return thirdVal;
	}

	public void setThirdVal(String thirdVal) {
		this.thirdVal = thirdVal;
	}

	public String getThirdChange() {
		return thirdChange;
	}

	public void setThirdChange(String thirdChange) {
		this.thirdChange = thirdChange;
	}

	public String getFourthVal() {
		return fourthVal;
	}

	public void setFourthVal(String fourthVal) {
		this.fourthVal = fourthVal;
	}

	public String getFourthChange() {
		return fourthChange;
	}

	public void setFourthChange(String fourthChange) {
		this.fourthChange = fourthChange;
	}

	
	
}
