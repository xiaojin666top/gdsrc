package com.beawan.customer.service;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import com.beawan.analysis.abnormal.bean.ErrData;
import com.beawan.analysis.finansis.dto.FinaRatioDto;
import com.beawan.analysis.finansis.dto.FinaRowDto;
import com.beawan.base.entity.InduInfo;
import com.beawan.core.BaseService;
import com.beawan.customer.bean.FinriskResult;
import com.beawan.customer.dto.InduMapDto;
import com.beawan.customer.dto.QuotaFullDto;
import com.beawan.customer.dto.WarnItemDto;
import com.beawan.model.entity.AdmitItemEntity;

/**
 * @author yzj
 */
public interface FinriskResultService extends BaseService<FinriskResult> {

	/**
	 * 直接进行财务分析！！！！
	 * 获取对公系统中的财报数据
	 * 发送财务分析要求数据报文 进行分析
	 * 最后将财务分析结果保存到库中
	 * @param serialNumber
	 * @param customerNo
	 * @param induCode
	 */
	void syncFinriskResult(String serialNumber, String customerNo,  String induCode) throws Exception;

    /**
     * 获取流动资金测算结果
     * @param growth
     * @return
     */
    FinriskResult updateQuotaByGrowth(String growth, String serNo) throws Exception;

	/**
	 * 解析 保存财务分析结果
	 * @param response
	 * @param platform
	 */
	FinriskResult saveFinriskResult(String serialNumber, String resultTaskId, String response, Integer platform) throws Exception;

	/**
	 * 根据对公的id  获得相应的本地财务分析结果
	 * @param taskId
	 * @return
	 */
	String getLocalResultData(String taskId) throws UnsupportedEncodingException;
	
	/**
	 * 根据对公的id  获得相应的云端财务分析结果
	 * @param taskId
	 * @return
	 */
	String getCloudResultData(String taskId) throws UnsupportedEncodingException;
	
	/**
	 * 获取财务分析结果报文
	 * @param serNo		 流水号
	 * @param platform   平台类型  本地1  云端2
	 * @return
	 */
	String getFinriskData(String serNo, Integer platform)  throws UnsupportedEncodingException;


	/**
	 * 获取推荐额度结果报文
	 * @param serNo		 流水号
	 * @param platform   平台类型  本地1  云端2
	 * @return
	 */
	String getQuotaData(String serNo, Integer platform)  throws UnsupportedEncodingException;
	
	/**
	 * 获得本地分析结果
	 * @return
	 */
	FinriskResult getLocalFinrisk(String serNo);

	/**
	 * 获得云端分析结果
	 * @return
	 */
	FinriskResult getCloudFinrisk(String serNo);
	
	/**
	 * 调用查询数据重复，抽出一个service接口
	 * @param taskId
	 * @return
	 * @throws Exception
	 */
	Map<String, Object> getLocalData(String taskId)throws Exception;

	/**
	 * 获取到行业对标分析和四力分析
	 * @param taskId
	 * @return
	 * @throws Exception
	 */
	Map<String, Object> getLocalFinAnaly(String taskId)throws Exception;
	/**
	 * 现金流概述
	 * @param taskId
	 * @return
	 * @throws Exception
	 */
	Map<String, Object> getLocalCashflowAnalyData(String taskId)throws Exception;

	/**
	 * 获取财务 重要科目分析
	 * @param serNo
	 * @return
	 * @throws Exception
	 */
	List<ErrData> getErr(String serNo)throws Exception;

	/**
	 * 获取四力分析所需要的比率指标
	 * @param serNo
	 * @return
	 * @throws Exception
	 */
	List<FinaRatioDto> getRatio(String serNo) throws  Exception;

	/**
	 * 获取三个维度的推荐额度
	 * @param serNo
	 * @throws Exception
	 */
	QuotaFullDto getCommendQuota(String serNo) throws Exception;


	/**
	 * 获取计算评级所需的比率指标
	 * @return
	 * @throws Exception
	 */
	List<FinaRatioDto> getAssessCardNeedRatio(String serNo) throws Exception;


	/**
	 * 获取预警分数在 max以上的预警提示
	 * max默认75分才需要异常项检查
	 * 异常项 目前只取预警等级最高的1级 （预警等级由低到高为 1 2 3）
	 * 且1级预警 数量不得超过5个
	 * @param serNo
	 * @param score 分数预警线
	 * @return
	 * @throws Exception
	 */
	List<WarnItemDto> getFinaWarn(String serNo, Double score) throws Exception;

	/**
	 * 获取预警分数在 60分以上 的预警提示
	 * @param serNo
	 * @return
	 * @throws Exception
	 */
	List<WarnItemDto> getFinaWarn(String serNo) throws Exception;

	/**
	 * 通过行业code码获取行业分析
	 * @param induCode
	 * @throws Exception
	 */
	InduInfo getInduAnalysis(String induCode) throws Exception;
}
