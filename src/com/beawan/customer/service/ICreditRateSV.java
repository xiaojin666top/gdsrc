package com.beawan.customer.service;

import java.util.List;

import com.beawan.customer.bean.CreditRating;



public interface ICreditRateSV {

	/**
	 * TODO 根据客户号获取评级各项分数
	 * @param customerno
	 * @return
	 * @throws Exception
	 */
	public List<CreditRating> queryCredit(String customerNo,String reportDate) throws Exception;
	
	/**
	 * TODO 保存各项评级情况
	 * @param customerno
	 * @param enName
	 * @throws Exception
	 */
	public void saveCreditRateList(String customerNo,String reportDate,List<CreditRating> list) throws Exception;
	
	/**
	 * TODO 生成默认项
	 * @param customerno
	 * @throws Exception
	 */
	public List<CreditRating> createCreditRating(String customerNo,String reportDate) throws Exception;
	
	
	/**
	 * TODO 更新财务信息
	 * @param list
	 * @throws Exception
	 */
	public List<CreditRating> sysnFinacing(String customerNo,String reportDate,List<CreditRating> list) throws Exception;
	
}
