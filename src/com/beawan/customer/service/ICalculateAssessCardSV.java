package com.beawan.customer.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

public interface ICalculateAssessCardSV {
	Map<String, Object> getAcQuotaResult(long modelId, String basePath) throws Exception;
	public String getResultLevel(long modelId, double score) throws Exception;
	String calculateAssessCard(long modelId, String customerNo, String basePath) throws Exception;
}
