package com.beawan.customer.service;

import java.util.Map;

import com.beawan.customer.AdmittDto;
import com.beawan.customer.bean.Admittance;

public interface IAdmittanceSV {
	Map<String,AdmittDto> saveAdmittance(Admittance admittance)throws Exception;
	
	Map<String,AdmittDto> listAdmittance(Admittance admittance)throws Exception;
	
	
	Admittance getAdmittanceByCustmoerNo(String custmoerNo)throws Exception;
}
