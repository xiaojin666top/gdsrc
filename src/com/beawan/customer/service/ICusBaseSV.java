package com.beawan.customer.service;

import java.util.List;
import java.util.Map;

import com.beawan.customer.bean.CusBase;
import com.beawan.customer.qbean.QCusBase;

public interface ICusBaseSV {

	public CusBase save(CusBase cusBase);
	
	public void delete(CusBase cusBase);
	
	public CusBase queryByCusNo(String cusNo);
	
	public List<CusBase> queryAll();

	/**
	 * @TODO HSQL分页查询，仅查询CusBase
	 * @param queryCondition 查询条件
	 * @param orderCondition 排序条件
	 * @param index 起始游标
	 * @param count 每页数量
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> queryCusBasePaging(CusBase queryCondition,
			String orderCondition, int index, int count) throws Exception;

	/**
	 * TODO SQL分页查询，关联用户和机构
	 * @param queryCondition 查询条件
	 * @param orderCondition 排序条件
	 * @param pageIndex 页标
	 * @param pageSize 每页大小
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> queryPaging(QCusBase queryCondition,
			String orderCondition, int pageIndex, int pageSize)
			throws Exception;

	/**
	 * TODO SLQ查询记录数
	 * @param queryCondition 查询条件
	 * @return
	 * @throws Exception
	 */
	public long queryCount(QCusBase queryCondition) throws Exception;

	public CusBase queryByCusName(String customerName);
	
	public CusBase queryByXdCusNo(String xdCustomerNo);
}
