package com.beawan.customer.service.impl;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.net.ntp.TimeStamp;
import org.nutz.json.Json;
import org.springframework.stereotype.Service;

import com.beawan.customer.service.ICalculateAssessCardSV;
import com.beawan.scoreCard.bean.AcConfiguration;
import com.beawan.scoreCard.bean.AcLogRecord;
import com.beawan.scoreCard.bean.AcLogRecordDetail;
import com.beawan.scoreCard.bean.AcQuota;
import com.beawan.scoreCard.bean.AcQuotaAttr;
import com.beawan.scoreCard.bean.AssessCard;
import com.beawan.scoreCard.bean.SubQuotaAttr;
import com.beawan.scoreCard.dao.IAcLogRecordDAO;
import com.beawan.scoreCard.dao.IAcLogRecordDetailDAO;
import com.beawan.scoreCard.service.IAcConfigurationSV;
import com.beawan.scoreCard.service.IAcLogRecordSV;
import com.beawan.scoreCard.service.IAssessCardSV;

import net.sf.json.JSONObject;
import oracle.sql.TIMESTAMP;

@Service("calculateAssessCardSV")
public class CalculateAssessCardSVImpl implements ICalculateAssessCardSV{
	
	@Resource
	private IAssessCardSV assessCardSV;
	@Resource
	private IAcConfigurationSV acConfigurationSV;
	@Resource
	private IAcLogRecordSV acLogRecordSV;
	@Resource
	private IAcLogRecordDAO acLogRecordDao;
	@Resource
	private IAcLogRecordDetailDAO acLogRecordDetailDao;
	
	@Override
	public String getResultLevel(long modelId, double score) throws Exception{
		
		
		
		AcConfiguration configuration = acConfigurationSV.queryByAcId(modelId);
		
		String xml = configuration.getAcCusLevelSet();
		
		String level = acConfigurationSV.getGradeByScore(xml, score);
		
		return level;
	}

	@Override
	public String calculateAssessCard(long modelId, String customerNo, String basePath) throws Exception{
		
		int score = 0;
		
		//获取每个指标的结果，用于评分卡的计算
		Map<String, Object> quotaResult = getAcQuotaResult(modelId, basePath);
		
		AssessCard assessCard = assessCardSV.queryInfoById2(modelId);
		
		List<AcQuota> quotaList = assessCard.getUnFinanceQuotaList();
		List<AcLogRecordDetail> detailList = new ArrayList<>();
		
		for (AcQuota parentQuota : quotaList) {
			List<AcQuota> childQuotaList = parentQuota.getChildrenList();
			for (AcQuota childQuota : childQuotaList) {
				String quotaCode = childQuota.getCode();
				List<SubQuotaAttr> subAttrList = childQuota.getSubQuotaAttrList();
				List<AcQuotaAttr> attrList = childQuota.getAcQuotaAttrs();
				if(childQuota.getType().equals("S")){
					for (SubQuotaAttr subAttr : subAttrList) {
						for (AcQuotaAttr attr : attrList) {
							if(attr.getId().equals(subAttr.getAttrId())){
								if(attr.getCode().equals(quotaResult.get(quotaCode))){
									AcLogRecordDetail detail = new AcLogRecordDetail();
									detail.setQuotaCode(quotaCode);
									detail.setQuotaName(childQuota.getName());
									detail.setQuotaScore(subAttr.getScord());
									detail.setQuotaValue(attr.getContent());
									detailList.add(detail);
									score+=subAttr.getScord();
								}
							}
						}
					}
				}else if(childQuota.getType().equals("V")){
					for (SubQuotaAttr subAttr : subAttrList) {
						for (AcQuotaAttr attr : attrList) {
							double quotaData = Double.valueOf(String.valueOf(quotaResult.get(quotaCode)));
							if(subAttr.getAttrId().equals(attr.getId())){
								if(attr.getAttrValue().equals("L")){
									if(quotaData >= attr.getDown() && quotaData < attr.getUpper()){
										AcLogRecordDetail detail = new AcLogRecordDetail();
										detail.setQuotaCode(quotaCode);
										detail.setQuotaName(childQuota.getName());
										detail.setQuotaScore(subAttr.getScord());
										detail.setQuotaValue(getPrettyNumber(quotaData));
										detailList.add(detail);
										score+=subAttr.getScord();
									}
								}else if(attr.getAttrValue().equals("R")){
									if(quotaData > attr.getDown() && quotaData <= attr.getUpper()){
										AcLogRecordDetail detail = new AcLogRecordDetail();
										detail.setQuotaCode(quotaCode);
										detail.setQuotaName(childQuota.getName());
										detail.setQuotaScore(subAttr.getScord());
										detail.setQuotaValue(getPrettyNumber(quotaData));
										detailList.add(detail);
										score+=subAttr.getScord();
									}
								}else if(attr.getAttrValue().equals("A")){
									if(quotaData >= attr.getDown() && quotaData <= attr.getUpper()){
										AcLogRecordDetail detail = new AcLogRecordDetail();
										detail.setQuotaCode(quotaCode);
										detail.setQuotaName(childQuota.getName());
										detail.setQuotaScore(subAttr.getScord());
										detail.setQuotaValue(getPrettyNumber(quotaData));
										detailList.add(detail);
										score+=subAttr.getScord();
									}
								}else if(attr.getAttrValue().equals("O")){
									if(quotaData > attr.getDown() && quotaData < attr.getUpper()){
										AcLogRecordDetail detail = new AcLogRecordDetail();
										detail.setQuotaCode(quotaCode);
										detail.setQuotaName(childQuota.getName());
										detail.setQuotaScore(subAttr.getScord());
										detail.setQuotaValue(getPrettyNumber(quotaData));
										detailList.add(detail);
										score+=subAttr.getScord();
									}
								}
							}							
						}
					}
				}
			}
		}
		
		String levelResult = getResultLevel(modelId, score);

		AcLogRecord logRecord = acLogRecordDao.selectSingleByProperty("serialNumber", customerNo);
		if(logRecord == null){
			logRecord = new AcLogRecord();
			logRecord.setSerialNumber(customerNo);
		}
		logRecord.setAcId(modelId);
		logRecord.setAcName(assessCard.getName());
		logRecord.setLoanType(assessCard.getLoanType());
		logRecord.setLogTime(new Date(System.currentTimeMillis()));
		logRecord.setScore(Double.valueOf(String.valueOf(score)));
		logRecord.setCustGrade(levelResult);
		
		logRecord = acLogRecordSV.saveOrUpdateLogRecord(logRecord);
		
		List<AcLogRecordDetail> historyDetailList = acLogRecordDetailDao.selectByProperty("logId", logRecord.getId());
		for (AcLogRecordDetail detail : historyDetailList) {
			acLogRecordDetailDao.deleteEntity(detail);
		}
		
		for (AcLogRecordDetail detail : detailList) {
			detail.setLogId(logRecord.getId());
			acLogRecordSV.saveOrUpdateLogDetail(detail);
		}
		
		return levelResult;
	}
	
	
	@Override
	public Map<String, Object> getAcQuotaResult(long modelId, String basePath) throws Exception {
		// TODO Auto-generated method stub
		
		Map<String, Object> result = new HashMap<>();
		String filePath = "";
		String dataStr = "";
		
		switch((int)modelId){
			//工业生产制造类评分卡
			case 5109:
				filePath = basePath + "/statics/tempFile/IndustryData.json";
				dataStr = getDataString(filePath);
				result = getIndustryAcQuotaData(modelId, dataStr);
				break;
		}
		return result;
	}
	
	//获取工业制造类评分卡指标数据
	Map<String, Object> getIndustryAcQuotaData(long modelId, String dataStr) throws IOException{
		Map<String, Object> quotaDataMap = new HashMap<String, Object>();

		JSONObject data = JSONObject.fromObject(dataStr);
		
		String FRXYTY = (String)data.get("法定代表人信誉和还款意愿");
		int FRNXTY = (int)data.get("法定代表人从事本行业年限");
		String SYTY = (String)data.get("商誉");
		String QYGLSP = (String)data.get("企业经营管理水平");
		String JSLLGY = (String)data.get("技术力量、工艺装备水平");
		String DQDKTY = (String)data.get("到期贷款偿还率");
		String DKLXCHTY = (String)data.get("贷款利息偿还记录");
		String TYJZLGY = (String)data.get("同业竞争力与市场前景");
		double JSJLTY = (double)data.get("结算记录");
		double YDBJE = (double)data.get("已担保金额");
		
		JSONObject current = data.getJSONObject("current");
		JSONObject last = data.getJSONObject("last");
		JSONObject first = data.getJSONObject("first");
		
		HashMap<String, Double> currentData = (HashMap)JSONObject.toBean(current, HashMap.class); 
		HashMap<String, Double> lastData = (HashMap)JSONObject.toBean(last, HashMap.class);
		HashMap<String, Double> firstData = (HashMap)JSONObject.toBean(first, HashMap.class);
		
		double ZBGXGY = currentData.get("固定资产")/lastData.get("固定资产");//装备更新情况
		double SYJZCGY = currentData.get("资产合计") - currentData.get("负债合计");//实有净资产
		double CQZCZBGY = (currentData.get("固定资产") + currentData.get("在建工程") + currentData.get("长期股权投资"))/ currentData.get("资产合计");//长期资产占比
		double LDBLGY = currentData.get("流动资产合计")/currentData.get("流动负债合计");//流动比率
		double FZBLGY = currentData.get("负债合计")/currentData.get("资产合计");//负债比率
		double YHFZGY = currentData.get("长期借款")/currentData.get("资产合计");//银行负债率
		double FCZXBLGY = (currentData.get("营业收入") + currentData.get("应收账款") - lastData.get("应收账款") + currentData.get("加:投资收益"))/currentData.get("流动负债合计");//非筹资性现金流入与流动负债比率
		double SDBLGY = (currentData.get("流动资产合计") - currentData.get("存货"))/currentData.get("流动负债合计");//速动比率
		double XSLRLGY = currentData.get("营业利润")/currentData.get("营业收入");//销售利润率
		double JZCLRLGY = currentData.get("利润总额")/(currentData.get("资产合计") - currentData.get("负债合计") + lastData.get("资产合计") - lastData.get("负债合计")/2);//净资产利润率
		double YSZKZZLGY = currentData.get("营业收入")/((currentData.get("应收账款")+lastData.get("应收账款"))/2);//应收账款周转率
		double CHZZLGY = currentData.get("减:营业成本")/((currentData.get("存货") + lastData.get("存货"))/2);//存货周转率
		double XSSRZZGY = (currentData.get("营业收入")-lastData.get("营业收入"))/lastData.get("营业收入");//销售收入增长率
		double JLRZZLGY = (currentData.get("净利润")-lastData.get("净利润"))/lastData.get("净利润");//净利润增长率
		double JZCZZLGY = (currentData.get("资产合计") - currentData.get("负债合计") - lastData.get("资产合计") + lastData.get("负债合计"))/(lastData.get("资产合计") - lastData.get("负债合计"));//净资产增长率
		double LDFZ = YDBJE/SYJZCGY;//连带负债
		
		quotaDataMap.put("FRXYTY", FRXYTY);
		quotaDataMap.put("FRNXTY", FRNXTY);
		quotaDataMap.put("SYTY", SYTY);
		quotaDataMap.put("QYGLSP", QYGLSP);
		quotaDataMap.put("JSLLGY", JSLLGY);
		quotaDataMap.put("DQDKTY", DQDKTY);
		quotaDataMap.put("DKLXCHTY", DKLXCHTY);
		quotaDataMap.put("TYJZLGY", TYJZLGY);
		quotaDataMap.put("JSJLTY", JSJLTY);
		quotaDataMap.put("ZBGXGY", ZBGXGY);
		quotaDataMap.put("SYJZCGY", SYJZCGY);
		quotaDataMap.put("CQZCZBGY", CQZCZBGY);
		quotaDataMap.put("LDBLGY", LDBLGY);
		quotaDataMap.put("FZBLGY", FZBLGY);
		quotaDataMap.put("YHFZGY", YHFZGY);
		quotaDataMap.put("FCZXBLGY", FCZXBLGY);
		quotaDataMap.put("SDBLGY", SDBLGY);
		quotaDataMap.put("XSLRLGY", XSLRLGY);
		quotaDataMap.put("JZCLRLGY", JZCLRLGY);
		quotaDataMap.put("YSZKZZLGY", YSZKZZLGY);
		quotaDataMap.put("CHZZLGY", CHZZLGY);
		quotaDataMap.put("XSSRZZGY", XSSRZZGY);
		quotaDataMap.put("JLRZZLGY", JLRZZLGY);
		quotaDataMap.put("JZCZZLGY", JZCZZLGY);
		quotaDataMap.put("LDFZ", LDFZ);
		
		return quotaDataMap;
	}
	
	String getDataString(String filePath) throws IOException{
		
		File jsonfile = new File(filePath);
		if(!jsonfile.exists()){
			jsonfile.createNewFile();
		}
		
		String dataStr = FileUtils.readFileToString(jsonfile, "UTF-8");
		return dataStr;
	}
	
	public static String getPrettyNumber(Double number) {
		DecimalFormat df = new DecimalFormat("#.00");
        return BigDecimal.valueOf(Double.valueOf(df.format(number)))
                .stripTrailingZeros().toPlainString();
    }

}
