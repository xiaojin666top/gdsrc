package com.beawan.customer.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.beawan.customer.bean.CusBase;
import com.beawan.customer.dao.impl.CusBaseDAOImpl;
import com.beawan.customer.dto.CustReportData;
import com.beawan.customer.dto.ReportDataOneItem;
import com.beawan.customer.dto.ReportDataSimple;
import com.platform.util.*;
import net.sf.json.JSONObject;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.beawan.base.entity.SReportData;
import com.beawan.base.entity.SReportModelRow;
import com.beawan.base.entity.SReportRecord;
import com.beawan.base.entity.User;
import com.beawan.base.service.ISReportDataSV;
import com.beawan.base.service.ISReportModelSV;
import com.beawan.base.service.ISReportRecordSV;
import com.beawan.common.BusinessException;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.util.SystemUtil;
import com.beawan.customer.bean.CusFSRecord;
import com.beawan.customer.dao.ICusFSRecordDAO;
import com.beawan.customer.service.ICusFSRecordSV;
import com.beawan.customer.utils.ReportCalculator;
import com.beawan.exterinvoke.in.ICmisInInvokeSV;
import com.beawan.exterinvoke.util.FncStatMapUtil;
import com.beawan.survey.custInfo.bean.CompBase;
import com.beawan.survey.custInfo.dao.ICompBaseDAO;

@Service("cusFSRecordSV")
public class CusFSRecordSVImpl implements ICusFSRecordSV {
	
	private static Logger log = Logger.getLogger(CusFSRecordSVImpl.class);
	
	@Resource
	private ICusFSRecordDAO cusFSRecordDAO;
	
	@Resource
	private ICompBaseDAO compBaseDAO;
	
	@Resource
	private ISReportDataSV sReportDataSV;
	
	@Resource
	private ISReportRecordSV sReportRecordSV;
	
	@Resource
	private ISReportModelSV sReportModelSV;
	
	@Resource
	private ICmisInInvokeSV cmisInInvokeSV;
	@Resource
	private CusBaseDAOImpl cusBaseDAO;
	
	@Override
	public CusFSRecord saveOrUpdate(CusFSRecord cusFSRecord) throws Exception {
		return cusFSRecordDAO.saveOrUpdate(cusFSRecord);
	}

	@Override
	public void delete(CusFSRecord cusFSRecord) throws Exception {
		cusFSRecordDAO.deleteEntity(cusFSRecord);
	}
	
	@Override
	public CusFSRecord save(String jsondata) throws Exception {
		
		CusFSRecord cusFSRecord = JacksonUtil.fromJson(jsondata, CusFSRecord.class);
		
		CompBase compBase = compBaseDAO.queryByCustNo(cusFSRecord.getCustomerId());
		if(compBase == null || StringUtil.isEmptyString(compBase.getFinanceBelong()))
			ExceptionUtil.throwException("请先在“客户基本信息”中选择财报所属，然后再保存！");
		
		CusFSRecord tempRecord = this.queryUnique(cusFSRecord.getCustomerId(),
				cusFSRecord.getReportDate(), compBase.getFinanceBelong());
		if(tempRecord != null)
			ExceptionUtil.throwException(cusFSRecord.getReportDate() + "报表已经存在！");
		
		cusFSRecord.setModelClass(compBase.getFinanceBelong());
		cusFSRecord.setRecordNo(SystemUtil.genCFSRecordNo());
		
		String date = DateUtil.format(new Date(), Constants.DATE_MASK);
		
		User user = HttpUtil.getCurrentUser();
		cusFSRecord.setUserId(user.getUserId());
		cusFSRecord.setOrgId(user.getAccOrgNo());
		cusFSRecord.setInputDate(date);
		cusFSRecord.setUpdateDate(date);
		cusFSRecord.setReportLocked("0");
		cusFSRecord.setReportStatus("01");
		
		return cusFSRecordDAO.saveOrUpdate(cusFSRecord);
	}

	@Override
	public CusFSRecord update(String jsondata) throws Exception {
		
		if(StringUtil.isEmpty(jsondata))
			return null;
		
		JSONObject json = JSONObject.fromObject(jsondata);
		CusFSRecord newCusFSRecord = (CusFSRecord) JSONObject.toBean(json,
				CusFSRecord.class);
		
		CusFSRecord oldCusFSRecord = cusFSRecordDAO.selectByPrimaryKey(newCusFSRecord.getRecordNo());
		
		BeanUtil.mergeProperties(newCusFSRecord, oldCusFSRecord, json);
		
		String date = DateUtil.format(new Date(), Constants.DATE_MASK);
		
		oldCusFSRecord.setUpdateDate(date);
		
		return cusFSRecordDAO.saveOrUpdate(oldCusFSRecord);
	}
	
	@Override
	public void deleteByNo(String recordNo) throws Exception {
		
		CusFSRecord cusFSRecord = this.queryByRecordNo(recordNo);
		cusFSRecord.setStatus(Constants.DELETE);
		cusFSRecordDAO.saveOrUpdate(cusFSRecord);
		
		List<SReportRecord> rrs = sReportRecordSV.queryByObjectNo(recordNo);
		if(CollectionUtils.isEmpty(rrs))
			return;
		
		for(SReportRecord rr : rrs){
			rr.setStatus(Constants.DELETE);
			sReportRecordSV.saveOrUpdate(rr);
			
			//查询数据
			List<SReportData> rds = sReportDataSV.queryByReportNo(rr.getReportNo());
			if(CollectionUtils.isEmpty(rds))
				continue;
			
			//删除数据
			for(SReportData rd : rds) {
				rd.setStatus(Constants.DELETE);
				sReportDataSV.saveOrUpdate(rd);
			}
		}
	}
	
	@Override
	public CusFSRecord queryByRecordNo(String recordNo) throws Exception {
		return cusFSRecordDAO.selectByPrimaryKey(recordNo);
	}
	
	@Override
	public CusFSRecord queryUnique(String customerId, String reportDate,
			String modelClass) throws Exception {
		
		String hql = "customerId=:customerId and reportDate=:reportDate and modelClass=:modelClass and status=:status";
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("customerId", customerId);
		params.put("reportDate", reportDate);
		params.put("modelClass", modelClass);
		params.put("status", Constants.NORMAL);
		
		return cusFSRecordDAO.selectSingle(hql, params);
	}
	
	@Override
	public List<CusFSRecord> queryByCusId(String customerId) throws Exception {
		String hsql = "from CusFSRecord where status='0' and customerId='" + customerId + "' order by reportDate asc";
		return cusFSRecordDAO.select(hsql);
	}

	@Override
	public List<CusFSRecord> queryByCusIdDesc(String customerId) throws Exception {
		String hsql = "from CusFSRecord where status='0' and customerId='" + customerId + "' and (reportPeriod= '03' or reportPeriod= '04')order by reportDate desc";
		return cusFSRecordDAO.select(hsql);
	}
	
	@Override
	public List<Map<String, Object>> queryPaging(CusFSRecord queryCondition,
			String orderCondition, int pageIndex, int pageSize) throws Exception {
		
		String sql = "SELECT *"
				   + " FROM ("
		           + " SELECT ROW_NUMBER() OVER() AS ROW_NUM,F.*,"
		           + " U.USER_NAME AS INPUT_USER_NAME"
		           + " FROM GDTCESYS.CUS_FSRECORD F"
		           + " LEFT JOIN GDTCESYS.BS_USER_INFO U ON U.USER_ID = F.USER_ID"
		           + " WHERE $"
		           + " ) T WHERE T.ROW_NUM > ? AND T.ROW_NUM <=?";
		
		String queryStr = genQueryStr(queryCondition);
		sql = sql.replace("$", queryStr);
		
		if (StringUtil.isEmptyString(orderCondition))
			orderCondition = "F.REPORT_DATE DESC";
		
		sql = sql.replace("OVER()", "OVER(ORDER BY " + orderCondition + ")");
		
		int startIndex = pageIndex*pageSize;
		int endIndex = startIndex + pageSize;
		Object[] params = new Object[]{startIndex, endIndex};
		
		return JdbcUtil.query(Constants.DataSource.TCE_DS, sql, params);
	}
	
	@Override
	public long queryCount(CusFSRecord queryCondition) throws Exception {
		String sql = "SELECT COUNT(*) "
				   + " FROM GDTCESYS.CUS_FSRECORD F"
		           + " LEFT JOIN GDTCESYS.BS_USER_INFO U ON U.USER_ID = F.USER_ID"
		           + " WHERE $";
		String queryStr = genQueryStr(queryCondition);
		sql = sql.replace("$", queryStr);
		return JdbcUtil.queryForLong(Constants.DataSource.TCE_DS, sql, null);
	}
	
	private String genQueryStr(CusFSRecord queryCondition) throws Exception {
		
		StringBuffer listSql = new StringBuffer("1=1");
		
		if (queryCondition != null) {
			
			if (!StringUtil.isEmptyString(queryCondition.getCustomerId())) {
				listSql.append(" AND F.CUSTOMER_ID = '")
				       .append(queryCondition.getCustomerId())
				       .append("'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getReportStatus())) {
				listSql.append(" AND F.REPORT_STATUS = '")
				       .append(queryCondition.getReportStatus())
				       .append("'");
			}

			if (null!=queryCondition.getStatus() && !"".equals(queryCondition.getStatus())) {
				listSql.append(" AND F.STATUS = '")
						.append(queryCondition.getStatus())
						.append("'");
			}

			if (!StringUtil.isEmptyString(queryCondition.getUserId())) {
				listSql.append(" AND U.USER_ID = '")
				       .append(queryCondition.getUserId())
				       .append("'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getOrgId())) {
				listSql.append(" AND O.ORGID = '")
				       .append(queryCondition.getOrgId())
				       .append("'");
			}

		}
		
		return listSql.toString();
	}
	
	@Override
	public List<SReportData> getReportData(String cusFSRecordNo,
			String modelNo) throws Exception {
		
		List<SReportData> currentDataList = null;
		
		//具体报表记录（如资产负债表）
		SReportRecord reportRecord = sReportRecordSV.queryUnique(cusFSRecordNo, modelNo);
		
		if(reportRecord != null)
			currentDataList = sReportDataSV.queryByReportNo(reportRecord.getReportNo());
		
		return currentDataList;
	}
	
	@Override
	public List<SReportData> getLastYearData(String customerId, String reportDate,
			String modelClass, String modelNo) throws Exception {
		
		List<SReportData> currentDataList = null;
		
		int lastYear = Integer.parseInt(reportDate.substring(0, 4))-1;
		
		CusFSRecord cusFSRecord = this.queryUnique(customerId, lastYear+"-12", modelClass);
		if(cusFSRecord != null){
		
			//具体报表记录（如资产负债表）
			SReportRecord reportRecord = sReportRecordSV.queryUnique(cusFSRecord.getRecordNo(), modelNo);
			
			if(reportRecord != null)
				currentDataList = sReportDataSV.queryByReportNo(reportRecord.getReportNo());
		}
		
		return currentDataList;
	}

	@Override
	public void saveReportData(String cusFSRecordNo, String modelNo,
			String dataArray, String calcFlag, User user) throws Exception {
		
		String time = DateUtil.format(new Date(), Constants.DATETIME_MASK);
		
		SReportRecord reportRecord = sReportRecordSV.queryUnique(cusFSRecordNo, modelNo);
		//首次保存
		if(reportRecord == null){
			reportRecord = new SReportRecord();
			reportRecord.setReportNo(SystemUtil.genReportNo());
			reportRecord.setObjectType("CustomerReport");
			reportRecord.setObjectNo(cusFSRecordNo);
			reportRecord.setModelNo(modelNo);
			reportRecord.setReportName(sReportModelSV.queryModelNameByNo(modelNo));
			reportRecord.setUserId(user.getUserId());
			reportRecord.setOrgId(user.getAccOrgNo());
			reportRecord.setInputTime(time);
		}
		reportRecord.setUpdateTime(DateUtil.getNowTimestamp());
		
		//注意，这里不能用JSONArray转换，会导致数据精度丢失
		List<SReportData> array = JacksonUtil.fromJson(dataArray,
				JacksonUtil.getJavaType(ArrayList.class, SReportData.class));
		
		for(SReportData reportData : array){
			
			if(StringUtil.isEmpty(reportData.getReportNo()))
				reportData.setReportNo(reportRecord.getReportNo());
			
			sReportDataSV.saveOrUpdate(reportData);
		}
		
		CusFSRecord cusFSRecord = this.queryByRecordNo(cusFSRecordNo);
		
		//测算
		if(!StringUtil.isEmpty(calcFlag)){
			this.calculate(cusFSRecord, modelNo, array);
			//报表平衡性前端已经校验，此时将报表设置为生效状态
			reportRecord.setValidFlag("1");
		}else
			reportRecord.setValidFlag(null);//暂存处于未生效状态
		
		sReportRecordSV.saveOrUpdate(reportRecord);
		
		cusFSRecord.setUpdateDate(time.substring(0, 10));
		cusFSRecordDAO.saveOrUpdate(cusFSRecord);
	}
	
	@Override
	public void finishReportInput(String cusFSRecordNo) throws Exception {
		
		try{
		
			CusFSRecord cusFSRecord = this.queryByRecordNo(cusFSRecordNo);
			
			this.checkCalcData(cusFSRecordNo, cusFSRecord.getModelClass(), null);
			
			cusFSRecord.setReportStatus(SysConstants.CusFSRecordStatus.FINISHED);
			cusFSRecord.setFinishDate(DateUtil.format(new Date(), Constants.DATE_MASK));
			this.saveOrUpdate(cusFSRecord);
			
		}catch(BusinessException be){
			ExceptionUtil.throwException(be.getMessage() + " 请使用“保存并测算”功能使之生效。");
		}catch(Exception e){
			throw e;
		}
		
	}
	
	/**
	 * TODO 根据财报记录编号从信贷系统同步对应的财务数据
	 * @param cusFSRecordNo 财报记录编号
	 * @throws Exception
	 */
	@Override
	public void syncFSFromCmisByRecNo(String cusFSRecordNo) throws Exception{
		
		//本地查询财报记录
		CusFSRecord record = this.queryByRecordNo(cusFSRecordNo);

		CusBase cusBase = cusBaseDAO.selectSingleByProperty("customerNo", record.getCustomerId());
		String mfCustomerNo = cusBase.getMfCustomerNo();

		CusFSRecord condition = new CusFSRecord();
		condition.setCustomerId(mfCustomerNo);
		condition.setReportDate(record.getReportDate());
		condition.setModelClass(record.getModelClass());
		condition.setReportScope(record.getReportScope());
		
		String message = "信贷系统不存在" + record.getReportDate() + "期的报表数据！";
		List<CusFSRecord> list = cmisInInvokeSV.queryCusFSRecord(condition);
		if(CollectionUtils.isEmpty(list))
			ExceptionUtil.throwException(message);
		
		Map<String,?> finaModelMap = FncStatMapUtil.getFinaModelMap();
		String modelNos = JacksonUtil.serialize(finaModelMap.get("010")) + JacksonUtil.serialize(finaModelMap.get("020"));
		
		CusFSRecord cmisRecord = list.get(0);
		syncCusFSDataFromCmis(cmisRecord, modelNos);
		
		//如果编号不一致，则删除原来的，避免重复
		if(!record.getRecordNo().equals(cmisRecord.getRecordNo()))
			this.deleteByNo(record.getRecordNo());
	}

	@Override
	public String getReportJsonData(String customerNo, String induCode, Integer currYear, Integer currMonth) throws Exception {
		String json = null;

		if(customerNo==null || "".equals(customerNo)) {
			return null;
		}
		if(induCode==null || "".equals(induCode)) {
			return null;
		}
		if(currYear==null){
			return null;
		}
		if(currMonth==null){
			return null;
		}
		
		String currDate, lastsameDate, lastoneDate, lasttwoDate, lastthirdDate;
		//当期
		if(currMonth<10){
			currDate = currYear + "-0" +currMonth;
			lastsameDate = (currYear-1) + "-0" +currMonth;
			lastoneDate = (currYear-1) + "-12";
			lasttwoDate = (currYear-2) + "-12" ;
			lastthirdDate = (currYear-3) + "-12" ;
		}else {
			currDate = currYear + "-" +currMonth;
			lastsameDate = (currYear-1) + "-" +currMonth;
			lastoneDate = (currYear-1) + "-12";
			lasttwoDate = (currYear-2) + "-12" ;
			lastthirdDate = (currYear-3) + "-12" ;
		}
		
		
		try {
			List<CusFSRecord> cusFSRecordList = this.queryByCusId(customerNo);
			//完整的与财务分析系统 传输对象

			CustReportData custReportData = new CustReportData(customerNo, induCode);
			List<ReportDataOneItem> reportDataOneItem = new ArrayList<>();

			for(int i=0; i < cusFSRecordList.size(); i++) {

				CusFSRecord cusFSRecord = cusFSRecordList.get(i);
				String recordNo = cusFSRecord.getRecordNo();
				String reportDate = cusFSRecord.getReportDate();

				//currDate, lastsameDate, lastoneDate, lasttwoDate, lastthirdDate;
				if(!currDate.equals(reportDate)
						&& !lastsameDate.equals(reportDate)
						&& !lastoneDate.equals(reportDate)
						&& !lasttwoDate.equals(reportDate)
						&& !lastthirdDate.equals(reportDate)){
					continue;
				}
					

				String year = reportDate.split("-")[0];
				String month = reportDate.split("-")[1];
				
				

				String typeBalance = "balance";
				String typeIncome = "income";

//				String reportType = request.getParameter("reportType");

				//开始资产表
				//获得对应报表模型号
				String b_modelNo = FncStatMapUtil.getModelNo(cusFSRecord.getModelClass(), typeBalance);
				//取得具体报表数据
				List<SReportData> b_currentData = this.getReportData(recordNo, b_modelNo);
				//开始利润表
				String i_modelNo = FncStatMapUtil.getModelNo(cusFSRecord.getModelClass(), typeIncome);
				List<SReportData> i_currentData = this.getReportData(recordNo, i_modelNo);


				ReportDataOneItem reportItem = new ReportDataOneItem(Integer.parseInt(year), Integer.parseInt(month), "0");
				List<ReportDataSimple> reportLists = new ArrayList<>();

				if(CollectionUtils.isEmpty(b_currentData) || CollectionUtils.isEmpty(i_currentData)){
					continue;
				}
				for(SReportData data : b_currentData) {
					ReportDataSimple simple = new ReportDataSimple();
					simple.setName(data.getRowName());
					//根据财务分析系统要求  转化成万元
					if(data.getCol1Value()!=null) {
						simple.setValue_early(data.getCol1Value() / 10000);
					}else{
						simple.setValue_early(0d);
					}
					if(data.getCol2Value()!=null) {
						simple.setValue(data.getCol2Value() / 10000);
					}else{
						simple.setValue(0d);
					}
					simple.setType("资产负债表");
					reportLists.add(simple);
				}
				for(SReportData data : i_currentData) {
					ReportDataSimple simple = new ReportDataSimple();
					simple.setName(data.getRowName());
					//根据财务分析系统要求  转化成万元
					if(data.getCol1Value()!=null) {
						simple.setValue_early(data.getCol1Value() / 10000);
					}
					if(data.getCol2Value()!=null) {
						simple.setValue(data.getCol2Value() / 10000);
					}
					simple.setType("利润表");
					reportLists.add(simple);
				}
				reportItem.setReportLists(reportLists);
				reportDataOneItem.add(reportItem);
			}
			//上面查询当前客户的财报数据  下面上传财报 返回分析结果的流水号 resultTaskId
			custReportData.setReportDataOneItem(reportDataOneItem);

			json = GsonUtil.GsonString(custReportData);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return json;
	}

	@Override
	public void syncFSFromCmisByCusNoWhole(String customerNo, String xdCustNo) throws Exception{
		
//		List<String> dateList = genSyncCusFSDate(year, month);
		List<CusFSRecord> records = this.queryByCusId(customerNo);
		
		Map<String,?> finaModelMap = FncStatMapUtil.getFinaModelMap();
		String modelNos = JacksonUtil.serialize(finaModelMap.get("010")) + JacksonUtil.serialize(finaModelMap.get("020"));
		
		CusFSRecord condition = new CusFSRecord();
		if(StringUtils.isEmpty(xdCustNo)){
			log.warn("客户号为：" + customerNo + "的信贷号为空，同步财报流程跳过");
			return;
		}
		condition.setCustomerId(xdCustNo);

		List<CusFSRecord> list = cmisInInvokeSV.queryCusFSRecord(condition);
		if(CollectionUtils.isEmpty(list)){
			System.out.println("客户号为" + customerNo + "在信贷系统中无数据，该客户同步结束");
			return;
		}
		for(CusFSRecord  record : list){
			//标志  当前期数的财报在本地对公系统中是否已经 存在或同步过了
			boolean flag = true;
			for(int j=0;j<records.size();j++){
				//本地已经存在且非新增状态的则不自动进行同步  --》对公本地保存的时候  年份是带 -  所有匹配时间是否相等的时候  需要去掉英文-
				if(record.getReportDate().equals(records.get(j).getReportDate().replace("-", ""))
						&& !SysConstants.CusFSRecordStatus.NEW.equals(records.get(j).getReportStatus())){
					System.out.println("报表期数为：" + record.getReportDate() + "已同步，且状态不为新增，跳过");
					flag = false;
					break;
				}
			}
			//开始同步
			if(flag){
				System.out.println("开始同步" + record.getReportDate() + "报表");
				record.setCustomerId(customerNo);
				syncCusFSDataFromCmis(record, modelNos);
			}
		}
		System.out.println("客户号为" + customerNo + "同步结束");
	}
	
	@Override
	public void syncFSFromCmisByCusNo(String customerNo, String xdCustNo,
			int year, int month) throws Exception{
		
		List<String> dateList = genSyncCusFSDate(year, month);
		List<CusFSRecord> records = this.queryByCusId(customerNo);
		
		Map<String,?> finaModelMap = FncStatMapUtil.getFinaModelMap();
		String modelNos = JacksonUtil.serialize(finaModelMap.get("010")) + JacksonUtil.serialize(finaModelMap.get("020"));
		
		CusFSRecord condition = new CusFSRecord();
		if(StringUtils.isEmpty(xdCustNo)){
			log.warn("客户号为：" + customerNo + "的信贷号为空，同步财报流程跳过");
			return;
		}
		condition.setCustomerId(xdCustNo);
		
		for(int i=0; i<dateList.size(); i++){
			
			String date = dateList.get(i);
			for(int j=0;j<records.size();j++){
				//本地已经存在 	则不自动进行同步   不管状态如何
				if(date.equals(records.get(j).getReportDate())
						/*&& !SysConstants.CusFSRecordStatus.NEW.equals(records.get(j).getReportStatus())*/){
					records.remove(j);
					date = null;
					break;
				}
			}
			
			//说明有新的
			if(date != null){
				condition.setReportDate(date);
				List<CusFSRecord> list = cmisInInvokeSV.queryCusFSRecord(condition);
				if(CollectionUtils.isEmpty(list))
					continue;
				System.out.println("报表期数为：" + date + "有数据，开始同步详细信息");
				for(CusFSRecord record : list){
					record.setCustomerId(customerNo);
					syncCusFSDataFromCmis(record, modelNos);
				}
			}
		}
	}
	
	/**
	 * TODO 根据客户号和报表日期从信贷系统同步对应的财务数据
	 * @param cmisRecord 客户报表记录对象
	 * @param modelNos 需要同步的模型号
	 * @throws Exception
	 */
	public void syncCusFSDataFromCmis(CusFSRecord cmisRecord, String modelNos) throws Exception {
//		Long time1 = new Date().getTime();
		cusFSRecordDAO.saveOrUpdate(cmisRecord);
//		Long time2 = new Date().getTime();
//		System.out.println("保存财报住记录表需要时间："+(time2-time1)/1000);
		//必须优化  现在需要2.5s左右
		List<SReportRecord> rrs = cmisInInvokeSV.queryReportRecord(cmisRecord.getRecordNo());
//		Long time3 = new Date().getTime();
//		System.out.println("获取财报REPORT_RECORD（中间表）用时："+(time3-time2)/1000);
		if(CollectionUtils.isEmpty(rrs))
			return;
		
		for(SReportRecord rr : rrs){
			
			//仅保存需要的
			if(!modelNos.contains(rr.getModelNo()))
				continue;
			rr.setStatus(Constants.NORMAL);
			sReportRecordSV.saveOrUpdate(rr);

//			Long time4 = new Date().getTime();
//			System.out.println("更新财报REPORT_RECORD（中间表）状态用时："+(time4-time3)/1000);
			
			List<SReportData> rds = cmisInInvokeSV.queryReportData(rr.getReportNo());
//			Long time5 = new Date().getTime();
//			System.out.println("获取cmis财报详细信息（数据表）状态用时："+(time5-time4)/1000);
			if(CollectionUtils.isEmpty(rds))
				continue;
			
			for(SReportData rd : rds){
				rd.setStatus(Constants.NORMAL);
				sReportDataSV.saveOrUpdate(rd);
			}
//			Long time6 = new Date().getTime();
//			System.out.println("保存cmis一个资产表或者利润表财报详细信息（数据表）状态用时："+(time6-time5)/1000);
		}
	}
	
	/**
	 * TODO 生成需要从信贷系统同步的财务报表日期列表
	 * @param year 需要同步的截止年份，<=0默认为当前年
	 * @param month 需要同步的截止月份 <=0默认为当前月
	 * @return
	 */
	public List<String> genSyncCusFSDate(int year, int month){
		
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		
		List<String> dateList = new ArrayList<String>();
		
		if(month > 0 && month <= 12){
			
			if(year <= 0)
				year = calendar.get(Calendar.YEAR);
			
			String temp = month<10?"0"+month:""+month;
			
			dateList.add(year + "-" + temp);//当期
			dateList.add(--year + "-" + temp);//上年同期
			if(month != 12)
				dateList.add(year + "-12");
			dateList.add(--year + "-12");
			dateList.add(--year + "-12");
			
		}else{
			
			year = calendar.get(Calendar.YEAR);
			month = calendar.get(Calendar.MONTH) + 1;
			
			for(int i=0;i<2;i++){
				
				if(month == 12)
					i += 2;
				
				for(; month>0; month--){
					String temp = month<10?"0"+month:""+month;
					dateList.add(year + "-" + temp);
				}
				
				if(month == 0)
					month = 12;
				
				year--;
			}
			
			dateList.add(year + "-12");
			dateList.add((--year) + "-12");//大前年末
		}
		
		return dateList;
	}

	public void calculate(CusFSRecord currRecord, String currModelNo,
			List<SReportData> currDataList) throws Exception {
		
		try{
			//先校验数据完整性
			this.checkCalcData(currRecord.getRecordNo(), currRecord.getModelClass(), currModelNo);
		
			Map<String, Object> dataMap = new HashMap<String, Object>();
			//取得当期资产负债表、利润表和现金流量表
			Map<String, Object> currTermData = this.getCusFSRecordData(currRecord.getRecordNo(),
					currRecord.getModelClass());
			//当前提交的数据是最新的，覆盖已有的
			if(!StringUtil.isEmpty(currModelNo) && !CollectionUtils.isEmpty(currDataList))
				currTermData.put(currModelNo, sReportDataSV.getMap(currDataList));
			//获得当期数据
			dataMap.put(ReportCalculator.CURR_TERM, currTermData);
			
			String lastYearDate = (Integer.parseInt(currRecord.getReportDate().substring(0, 4))-1) + "-12";
			//获得上年末报表记录
			CusFSRecord lastRecord = this.queryUnique(currRecord.getCustomerId(),
					lastYearDate, currRecord.getModelClass());
			if(lastRecord != null){
				//获得上年末数据
				Map<String, Object> lastYearData = this.getCusFSRecordData(lastRecord.getRecordNo(),
						lastRecord.getModelClass());
				//获得上年末数据
				dataMap.put(ReportCalculator.LAST_YEAR, lastYearData);
			}
			
			//生成现金流量表
			this.genReprtData(currRecord, SysConstants.CmisFianRepType.CASH_FLOW, dataMap);
			//生成财务指标表
			this.genReprtData(currRecord, SysConstants.CmisFianRepType.RATIO_INDEX, dataMap);
		
		}catch(BusinessException be){
			System.out.println(be);
			log.error("生成现金流量表、财务指标表失败：" + be.getMessage() + "customerId:" + currRecord.getCustomerId());
		}catch(Exception e){
			throw e;
		}
	}
	
	/**
	 * TODO 资产负债表和利润表数据有效性校验
	 * @param cusFSRecordNo 客户财报记录编号
	 * @param modelClass 报表模型类别
	 * @param exceptModelNo 需要去除的模型号
	 * @throws Exception
	 */
	private void checkCalcData(String cusFSRecordNo,
			String modelClass, String exceptModelNo) throws Exception{
		
		String balanceModelNo = FncStatMapUtil.getModelNo(modelClass,
				SysConstants.CmisFianRepType.BALANCE);
		String incomeModelNo = FncStatMapUtil.getModelNo(modelClass,
				SysConstants.CmisFianRepType.INCOME);
		
		String modelNos = "," + balanceModelNo + "," + incomeModelNo;
		if(exceptModelNo != null)
			modelNos = modelNos.replace("," + exceptModelNo, "");
		
		SReportRecord condition = new SReportRecord();
		condition.setObjectNo(cusFSRecordNo);
		condition.setModelNo(modelNos.substring(1));
		
		boolean check = false;
		//获得具体报表（如资产负债表、利润表等）记录
		List<SReportRecord> rrs = sReportRecordSV.queryByCondition(condition);
		String rNames = "";
		if(!CollectionUtils.isEmpty(rrs)){
			for(SReportRecord rr : rrs){
				//未生效
				if(!"1".equals(rr.getValidFlag()))
					rNames += "," + rr.getReportName();
			}
			
			if("".equals(rNames))
				check = true;
			else
				rNames = rNames.substring(1);
		}
		
		if(!check)
			ExceptionUtil.throwException(rNames + "数据未生效！");
	}
	
	/**
	 * TODO 获取客户财报数据map集合（包括资产负债表、利润表、现金流量表）
	 * @param cusFSRecordNo 客户财报记录编号
	 * @param modelClass 报表模型类别
	 * @return
	 * @throws Exception
	 */
	private Map<String, Object> getCusFSRecordData(String cusFSRecordNo,
			String modelClass) throws Exception{
		
		String balanceModelNo = FncStatMapUtil.getModelNo(modelClass,
				SysConstants.CmisFianRepType.BALANCE);
		String incomeModelNo = FncStatMapUtil.getModelNo(modelClass,
				SysConstants.CmisFianRepType.INCOME);
		String cashFlowModelNo = FncStatMapUtil.getModelNo(modelClass,
				SysConstants.CmisFianRepType.CASH_FLOW_INPUT);
		
		SReportRecord condition = new SReportRecord();
		condition.setObjectNo(cusFSRecordNo);
		condition.setModelNo(balanceModelNo + "," + incomeModelNo + "," + cashFlowModelNo);
		
		//获得具体报表（如资产负债表、利润表等）记录
		List<SReportRecord> rrs = sReportRecordSV.queryByCondition(condition);
		
		Map<String, Object> dataMap = null;
		
		if(!CollectionUtils.isEmpty(rrs)){
			
			dataMap = new HashMap<String, Object>();
			
			for(SReportRecord rr : rrs){
				//获得具体报表数据
				dataMap.put(rr.getModelNo(), sReportDataSV.queryMapByRepNo(rr.getReportNo()));
			}
		}
		
		return dataMap;
	}
	
	/**
	 * TODO 自动生成报表数据
	 * @param currRecord 当期报表记录
	 * @param reportType 报表类型
	 * @param dataMap 当期和上期的数据集合
	 * @throws Exception
	 */
	private void genReprtData(CusFSRecord currRecord, String reportType,
			Map<String, Object> dataMap) throws Exception {
		
		String time = DateUtil.format(new Date(), Constants.DATETIME_MASK);
		
		String modelNo = FncStatMapUtil.getModelNo(currRecord.getModelClass(), reportType);
		
		SReportRecord rRecord = sReportRecordSV.queryUnique(currRecord.getRecordNo(), modelNo);
		
		//保存
		if(rRecord == null){
			rRecord = new SReportRecord();
			rRecord.setReportNo(SystemUtil.genReportNo());
			rRecord.setObjectType("CustomerReport");
			rRecord.setObjectNo(currRecord.getRecordNo());
			rRecord.setModelNo(modelNo);
			rRecord.setReportName(sReportModelSV.queryModelNameByNo(modelNo));
			//自动生成的报表默认生效
			rRecord.setValidFlag("1");
			
			User user = HttpUtil.getCurrentUser();
			rRecord.setUserId(user.getUserId());
			rRecord.setOrgId(user.getAccOrgNo());
			
			rRecord.setInputTime(time);
		}
		rRecord.setUpdateTime(DateUtil.getNowTimestamp());
		sReportRecordSV.saveOrUpdate(rRecord);
		
		//报表模型数据
		List<SReportModelRow> modelRows = sReportModelSV.queryModelRowByNo(modelNo);
		
		Map<String, Object> reportData = new ReportCalculator().genReprtData(rRecord,
				modelRows, dataMap);
		
		for(SReportModelRow row : modelRows){
			SReportData data;
			//非真实财务科目，空行或统计行
			if("1000".equals(row.getRowSubject())){
				data = ReportCalculator.getReportRowData(rRecord.getReportNo(), row);
			}else {
				data = (SReportData) reportData.get(row.getRowSubject());
				//行号不一致则说明存在科目相同的行
				if(!row.getRowNo().equals(data.getRowNo())){
					SReportData newdata = ReportCalculator.getReportRowData(
							rRecord.getReportNo(), row);
					newdata.setCol1Value(data.getCol1Value());
					newdata.setCol2Value(data.getCol2Value());
					data = newdata;
				}
			}
			sReportDataSV.saveOrUpdate(data);
		}
	}
	/**
	 * TODO 获取客户最新一期财报日期
	 * @param customerNo 客户号
	 */
	@Override
	public String getMaxReportDate(String customerNo){

		String reportDate = cusFSRecordDAO.getMaxReportDate(customerNo);
		return reportDate;
	}
	
	/**
	 * TODO 获取客户当年前一期财报营业收入
	 * @param customerNo 客户号
	 * @param nowDate 当期财报日期
	 */
	@Override
	public Double getOldReportTurnover(String customerNo,String nowDate){

		Double oldTurnover = cusFSRecordDAO.getOldReportTurnover(customerNo,nowDate);
		return oldTurnover;
	}
	
}
