package com.beawan.customer.service.impl;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.base.entity.SReportData;
import com.beawan.base.entity.SReportRecord;
import com.beawan.base.service.ISReportDataSV;
import com.beawan.base.service.ISReportRecordSV;
import com.beawan.customer.AdmittDto;
import com.beawan.customer.bean.Admittance;
import com.beawan.customer.bean.CusFSRecord;
import com.beawan.customer.dao.IAdmittanceDAO;
import com.beawan.customer.service.IAdmittanceSV;
import com.beawan.customer.service.ICusFSRecordSV;

@Service("admittanceSV")
public class AdmittanceSVImpl implements IAdmittanceSV{
	
	@Resource
	private ICusFSRecordSV cusFSRecordSV;
	@Resource
	private ISReportRecordSV sReportRecordSV;
	@Resource
	private ISReportDataSV sReportDataSV;
	@Resource
	private IAdmittanceDAO admittanceDAO;
	
	//当期资产负债
	private Map<String,SReportData> newBalanceMap = new HashMap<String,SReportData>();
	//当期现金流量
	private Map<String,SReportData> newCashFlowMap = new HashMap<String,SReportData>();
	//当期利润表
	private Map<String,SReportData> newIncomeMap = new HashMap<String,SReportData>();
	
	//去年资产负债
	private Map<String,SReportData> newBalanceMap_last = new HashMap<String,SReportData>();
	//去年现金流量
	/*private Map<String,SReportData> newCashFlowMap_last = new HashMap<String,SReportData>();*/
	//去年利润表
	private Map<String,SReportData> newIncomeMap_last = new HashMap<String,SReportData>();
	
	//去年资产负债
	private Map<String,SReportData> newBalanceMap_first = new HashMap<String,SReportData>();
	/*//去年现金流量
	private Map<String,SReportData> newCashFlowMap_first = new HashMap<String,SReportData>();
	//去年利润表
	private Map<String,SReportData> newIncomeMap_first = new HashMap<String,SReportData>();*/
	
	private Admittance admittance;
	 
	
	static {
		/*
		 * admittMap.put("compLife",new AdmittDto("公司成立年限","","大于3年","可准入"));
		 * admittMap.put("settlement",new AdmittDto("在本行贷款和结算纪录","","有贷款，无不良记录","可准入"));
		 * admittMap.put("earlyWaring",new AdmittDto("存量客户，过去一年内预警情况","","未出现","可准入"));
		 * admittMap.put("creditRecord",new AdmittDto("企业征信记录","","有且无逾期记录","可准入"));
		 * admittMap.put("beoverdue",new AdmittDto("对外担保的贷款","","无不良记录","可准入"));
		 * admittMap.put("enforcement",new AdmittDto("最近五年内有强制执行记录","","无行政处罚","可准入"));
		 * admittMap.put("punish",new AdmittDto("最近两年内行政处罚记录","","无行政处罚","可准入"));
		 * admittMap.put("guarntee",new
		 * AdmittDto("对外担保金额","","企业对外担保金额占净资产小于30%","可准入"));
		 * admittMap.put("debtRatio",new
		 * AdmittDto("还款能力: 偿债保障比率","","企业偿债保障比率大于200%","可准入"));
		 * admittMap.put("salesRevenueRate",new
		 * AdmittDto("销售收入增长情况","","企业销售增长率正常","可准入")); admittMap.put("ysDay",new
		 * AdmittDto("应收账款账期风险(应收账款周转天数较年初增加)","","小于40天","可准入"));
		 * admittMap.put("profitLife",new AdmittDto("（营业利润）赢利年限","","3年内盈利2年以上","可准入"));
		 * admittMap.put("financialLeverageRatio",new
		 * AdmittDto("财务杠杆率 （总负债/净资产）","","指标正常","可准入"));
		 */
		
	}
	
	@Override
	public Map<String,AdmittDto> saveAdmittance(Admittance admittance) throws Exception {
		//获取2017年报,分别以新旧准则查询
		CusFSRecord queryUnique; 
		queryUnique = cusFSRecordSV.queryUnique(admittance.getCustomerNo(), "2017-12", "020");
		if(queryUnique == null){
			queryUnique = cusFSRecordSV.queryUnique(admittance.getCustomerNo(), "2017-12", "010");
		} 
		if(queryUnique != null){
			List<SReportRecord> queryByObjectNo = sReportRecordSV.queryByObjectNo(queryUnique.getRecordNo());
			for (SReportRecord sReportRecord : queryByObjectNo) {
				if("资产负债表".equals(sReportRecord.getReportName())){
					newBalanceMap = sReportDataSV.queryMapByRepNo(sReportRecord.getReportNo());
				}else if("现金流量表(自动生成)".equals(sReportRecord.getReportName())){
					newCashFlowMap = sReportDataSV.queryMapByRepNo(sReportRecord.getReportNo());
				}else if("利润表".equals(sReportRecord.getReportName())){
					newIncomeMap = sReportDataSV.queryMapByRepNo(sReportRecord.getReportNo());
				}
			}
		}
		
		//获取2016年报,分别以新旧准则查询
		CusFSRecord queryUnique_last; 
		queryUnique_last = cusFSRecordSV.queryUnique(admittance.getCustomerNo(), "2016-12", "020");
		if(queryUnique_last == null){
			queryUnique_last = cusFSRecordSV.queryUnique(admittance.getCustomerNo(), "2016-12", "010");
		} 
		if(queryUnique_last !=null){
			List<SReportRecord> queryByObjectNo_last = sReportRecordSV.queryByObjectNo(queryUnique_last.getRecordNo());
			if(queryByObjectNo_last !=null){
				for (SReportRecord sReportRecord : queryByObjectNo_last) {
					if("资产负债表".equals(sReportRecord.getReportName())){
						newBalanceMap_last = sReportDataSV.queryMapByRepNo(sReportRecord.getReportNo());
					}else if("现金流量表(自动生成)".equals(sReportRecord.getReportName())){
						/*newCashFlowMap_last = sReportDataSV.queryMapByRepNo(sReportRecord.getReportNo());*/
					}else if("利润表".equals(sReportRecord.getReportName())){
						newIncomeMap_last = sReportDataSV.queryMapByRepNo(sReportRecord.getReportNo());
					}
				}
			}
		}
		
		//获取2015年报,分别以新旧准则查询
		CusFSRecord queryUnique_first; 
		queryUnique_first = cusFSRecordSV.queryUnique(admittance.getCustomerNo(), "2015-12", "020");
		if(queryUnique_first == null){
			queryUnique_first = cusFSRecordSV.queryUnique(admittance.getCustomerNo(), "2015-12", "010");
		} 
		if(queryUnique_first !=null){
			List<SReportRecord> queryByObjectNo_first = sReportRecordSV.queryByObjectNo(queryUnique_first.getRecordNo());
			if(queryByObjectNo_first !=null){
				for (SReportRecord sReportRecord : queryByObjectNo_first) {
					if("资产负债表".equals(sReportRecord.getReportName())){
						newBalanceMap_first = sReportDataSV.queryMapByRepNo(sReportRecord.getReportNo());
					}/*else if("现金流量表(自动生成)".equals(sReportRecord.getReportName())){
						newCashFlowMap_first = sReportDataSV.queryMapByRepNo(sReportRecord.getReportNo());
					}else if("利润表".equals(sReportRecord.getReportName())){
						newIncomeMap_first = sReportDataSV.queryMapByRepNo(sReportRecord.getReportNo());
					}*/
				}
			}
		}
		Map<String,AdmittDto> admittMap = new HashMap<String,AdmittDto>();
		admittMap.put("report",new AdmittDto("财报完整性","","企业年报数据完整","正常"));
		this.admittance = admittance;
		//判断是否可准入
		boolean admittance_info = isAdmittance(admittMap);

		if (admittance_info == true) {
			admittance.setAdmittanceResult("0");
		}else {
			admittance.setAdmittanceResult("1");
		}
//		if(admittance_info == true){
//			boolean addCredit_info = isAddCredit(admittMap);
//			if(addCredit_info == true){
//				admittance.setAdmittanceResult("0");
//			}else{
//				admittance.setAdmittanceResult("1");
//			}
//		}else{
//			admittance.setAdmittanceResult("2");
//		}
		admittanceDAO.saveOrUpdate(admittance);
		return admittMap;
	}
	
	/***
	 * 判断是否可准入
	 * false:不可准入
	 * true:可准入
	 */
	private boolean isAdmittance(Map<String,AdmittDto> admittMap ) throws Exception {
//		int serNumber = 1;
		boolean condition = true;
		//四舍五入转化
		DecimalFormat df = new DecimalFormat("#.00");
		
		//对外担保
		if("0".equals(admittance.getGuarntee())){
			admittMap.put("guarntee",new AdmittDto("对外担保情况","","无对外担保","正常"));
		}else if("1".equals(admittance.getGuarntee())){
			admittMap.put("guarntee",new AdmittDto("对外担保情况","","有对外担保，无逾期","黄色预警"));
		}else if("2".equals(admittance.getGuarntee())){
			admittMap.put("guarntee",new AdmittDto("对外担保情况","","有对外担保，逾期天数在90天以内","橙色预警"));
		}else if("3".equals(admittance.getGuarntee())){
			admittMap.put("guarntee",new AdmittDto("对外担保情况","","逾期天数超过90天","不可准入"));
			condition = false;
			admittance.setAdmittanceInfo("逾期天数超过90天;\n");
		}

		//企业征信记录
		if("0".equals(admittance.getCreditRecord())){
			admittMap.put("creditRecord",new AdmittDto("企业征信记录","","有且无逾期记录","正常"));
		}else if("1".equals(admittance.getCreditRecord())){
			admittMap.put("creditRecord",new AdmittDto("企业征信记录","","无记录","黄色预警"));
		}else if("2".equals(admittance.getCreditRecord())){
			admittMap.put("creditRecord",new AdmittDto("企业征信记录","","有逾期记录但不超过90天","橙色预警"));
		}else if("3".equals(admittance.getCreditRecord())){
			admittMap.put("creditRecord",new AdmittDto("企业征信记录","","逾期天数超过90天","不可准入"));
			condition = false;
			admittance.setAdmittanceInfo("逾期天数超过90天;\n");
		}

		//公司涉诉情况
		if("0".equals(admittance.getCreditRecord())){
			admittMap.put("enforcement",new AdmittDto("公司涉诉情况","","无涉诉记录","正常"));
		}else if("1".equals(admittance.getCreditRecord())){
			admittMap.put("enforcement",new AdmittDto("公司涉诉情况","","有涉诉记录,非被诉方","黄色预警"));
		}else if("2".equals(admittance.getCreditRecord())){
			admittMap.put("enforcement",new AdmittDto("公司涉诉情况","","作为被诉方但涉诉金额较低","橙色预警"));
		}else if("3".equals(admittance.getCreditRecord())){
			admittMap.put("enforcement",new AdmittDto("公司涉诉情况","","作为被诉方且涉诉金额超过净资产的50%","不可准入"));
			condition = false;
			admittance.setAdmittanceInfo("公司作为被诉方且涉诉金额超过净资产的50%;\n");
		}

		//公司法人涉诉情况
		if("0".equals(admittance.getPunish())){
			admittMap.put("punish",new AdmittDto("公司法人涉诉情况","","无涉诉记录","正常"));
		}else if("1".equals(admittance.getPunish())){
			admittMap.put("punish",new AdmittDto("公司法人涉诉情况","","有涉诉记录,非被诉方","黄色预警"));
		}else if("2".equals(admittance.getPunish())){
			admittMap.put("punish",new AdmittDto("公司法人涉诉情况","","作为被诉方但涉诉金额较低","橙色预警"));
		}else if("3".equals(admittance.getPunish())){
			admittMap.put("punish",new AdmittDto("公司法人涉诉情况","","作为被诉方且涉诉金额超过净资产的50%","不可准入"));
			condition = false;
			admittance.setAdmittanceInfo("公司法人作为被诉方且涉诉金额超过净资产的50%;\n");
		}
		
		//公司成立年限
		String comLife = admittance.getCompLife();
		if(comLife==null||"".equals(comLife))
			comLife = "0";
		if(Integer.parseInt(comLife)<1){
			admittMap.put("compLife",new AdmittDto("公司成立年限",comLife+"年","公司成立年限小于一年","橙色预警"));
		}else if(Integer.parseInt(comLife)>=1 && Integer.parseInt(comLife)<=3){
			admittMap.put("compLife",new AdmittDto("公司成立年限",admittance.getCompLife()+"年","公司成立年限在一到三年","黄色预警"));
		}else {
			admittMap.put("compLife",new AdmittDto("公司成立年限",comLife+"年","公司成立年限超过三年","正常"));
		}

		//企业对外担保金额占净资产比率
		if("0".equals(admittance.getGuarnteeAmount())){
			admittMap.put("guarnteeAmount",new AdmittDto("企业对外担保金额占净资产比率","","30%","正常"));
		}else if("1".equals(admittance.getGuarnteeAmount())){
			admittMap.put("guarnteeAmount",new AdmittDto("企业对外担保金额占净资产比率","","30%到100%","黄色预警"));
		}else if("2".equals(admittance.getGuarnteeAmount())){
			admittMap.put("guarnteeAmount",new AdmittDto("企业对外担保金额占净资产比率","","超过100%","橙色预警"));
		}
		
		//过去一年内预警情况
		if("0".equals(admittance.getEarlyWaring())){
			admittMap.put("earlyWaring",new AdmittDto("过去一年内预警情况","","未出现","正常"));
		}else if("1".equals(admittance.getEarlyWaring())){
			admittMap.put("earlyWaring",new AdmittDto("过去一年内预警情况","","出现严重预警情况，已解决","黄色预警"));
		}else if("2".equals(admittance.getEarlyWaring())){
			admittMap.put("earlyWaring",new AdmittDto("过去一年内预警情况","","出现严重预警情况，未解决","橙色预警"));
		}

		//近三年盈利情况
		if("0".equals(admittance.getBeoverdue())){
			admittMap.put("beoverdue",new AdmittDto("近三年盈利情况","","三年内盈利两年以上","正常"));
		}else if("1".equals(admittance.getBeoverdue())){
			admittMap.put("beoverdue",new AdmittDto("近三年盈利情况","","三年内盈利一年","黄色预警"));
		}else if("2".equals(admittance.getBeoverdue())){
			admittMap.put("beoverdue",new AdmittDto("近三年盈利情况","","三年未盈利","橙色预警"));
		}

		//最近五年强行执行记录
		if("0".equals(admittance.getSettlement())){
			admittMap.put("settlement",new AdmittDto("最近五年强行执行记录","","无行政处罚","正常"));
		}else if("1".equals(admittance.getSettlement())){
			admittMap.put("settlement",new AdmittDto("最近五年强行执行记录","","受行政处罚，申请复议且复议结果对本人有利","黄色预警"));
		}else if("2".equals(admittance.getSettlement())){
			admittMap.put("settlement",new AdmittDto("最近五年强行执行记录","","受行政处罚被暂扣或者吊销许可证","橙色预警"));
		}

		//是否涉及我行限制类行业范围
		if("0".equals(admittance.getSettlement())){
			admittMap.put("settlement",new AdmittDto("是否涉及我行限制类行业范围","","否","正常"));
		}else if("1".equals(admittance.getSettlement())){
			admittMap.put("settlement",new AdmittDto("是否涉及我行限制类行业范围","","部分业务涉及","黄色预警"));
		}else if("2".equals(admittance.getSettlement())){
			admittMap.put("settlement",new AdmittDto("是否涉及我行限制类行业范围","","主体业务涉及","橙色预警"));
		}
		
		
		
//		if("1".equals(admittance.getCreditInfo())){
//			if("1".equals(admittance.getSettlement())){
//				admittMap.put("settlement",new AdmittDto("在本行贷款和结算纪录","","在我行贷款有不良记录","不予准入"));
//				condition = false;
//				admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".在我行贷款有不良记录;\n");
//				serNumber ++ ;
//			}
//			if("2".equals(admittance.getEarlyWaring())){
//				condition = false;
//				admittMap.put("earlyWaring",new AdmittDto("存量客户，过去一年内预警情况","","出现严重预警情况，未解决","不予准入"));
//				admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".出现严重预警情况，未解决;\n");
//				serNumber ++ ;
//			}
//		}
//		if("2".equals(admittance.getCreditRecord())){
//			condition = false;
//			admittMap.put("creditRecord",new AdmittDto("企业征信记录","","征信有逾期记录","不予准入"));
//			admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".征信有逾期记录;\n");
//			serNumber ++ ;
//		}
//		if("1".equals(admittance.getGuarntee())){
//			if("1".equals(admittance.getBeoverdue())){
//				condition = false;
//				admittMap.put("beoverdue",new AdmittDto("对外担保的贷款","","企业对外担保已逾期","不予准入"));
//				admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".企业对外担保已逾期;\n");
//				serNumber ++ ;
//			}
//		}
//		
//		if("2".equals(admittance.getEnforcement())){
//			condition = false;
//			admittMap.put("enforcement",new AdmittDto("最近五年内有强制执行记录","","主要股东受行政处罚被暂扣或者吊销许可证、暂扣或者吊销执照，没有申请复议或者复议结果对本人不利","不予准入"));
//			admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".主要股东受行政处罚被暂扣或者吊销许可证、暂扣或者吊销执照，没有申请复议或者复议结果对本人不利;\n");
//			serNumber ++ ;
//		}
//		
//		if("2".equals(admittance.getPunish())){
//			condition = false;
//			admittMap.put("punish",new AdmittDto("最近两年内行政处罚记录","","主要股东受行政处罚被暂扣或者吊销许可证、暂扣或者吊销执照，没有申请复议或者复议结果对本人不利","不予准入"));
//			admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".主要股东曾受行政处罚被责令限期改正或五年内曾被没收违法所得的，且没有申请复议或者复议结果对本人不利;\n");
//			serNumber ++ ;
//		}
		
		if(newBalanceMap!=null&&newCashFlowMap!=null&&newIncomeMap!=null&&newBalanceMap_last!=null&&newIncomeMap_last!=null&&newBalanceMap_first!=null){
			try{
//				if("1".equals(admittance.getGuarntee())){
//					if(admittance.getGuarnteeAmount() != null && !"0".equals(admittance.getGuarnteeAmount()) && !"".equals(admittance.getGuarnteeAmount())){
//						double rate = calculationGuarntee();
//						if(rate >= 1){
//							admittMap.put("guarntee",new AdmittDto("对外担保金额",df.format(rate*100)+"%","企业对外担保金额占净资产为高于100%,占比过高","不予准入"));
//							condition = false;
//							admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".企业对外担保金额占净资产为"+df.format(rate*100)+"%,占比过高;\n");
//							serNumber ++ ;
//						}
//					}
//				}
				
				double debtRatio = calculationDebtRatio();
				if(debtRatio<1){
					admittMap.put("debtRatio",new AdmittDto("还款能力: 偿债保障比率",df.format(debtRatio*100)+"%","企业偿债保障比率低于100%,比率过低","橙色预警"));
//					admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".企业偿债保障比率为"+df.format(debtRatio*100)+"%,比率过低;\n");
				}else if(debtRatio<2) {
					admittMap.put("debtRatio",new AdmittDto("还款能力: 偿债保障比率",df.format(debtRatio*100)+"%","企业偿债保障比率在100%~200%之间,比率较低","黄色预警"));
				}else if(debtRatio >= 2) {
					admittMap.put("debtRatio",new AdmittDto("还款能力: 偿债保障比率",df.format(debtRatio*100)+"%","企业偿债保障比率超过200%","正常"));
				}
				
//				double salesRevenueRate = calculationSalesGrowthRate();
//				if(salesRevenueRate<-0.1){
//					condition = false;
//					admittMap.put("salesRevenueRate",new AdmittDto("销售收入增长情况",df.format(salesRevenueRate*100)+"%","企业销售增长率低于-10%，销售负增长过高","不予准入"));
//					admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".企业销售增长率为"+df.format(salesRevenueRate*100)+"%，销售负增长过高;\n");
//					serNumber ++ ;
//				}
//				
//				int day = calculationAccountsReceivableRisk();
//				if(day>55){
//					condition = false;
//					admittMap.put("ysDay",new AdmittDto("应收账款账期风险（应收账款周转天数较年初增加）",day+"天","应收账款周转天数较年初增加天数超过55天，应收账款账期风险过高","不予准入"));
//					admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".应收账款周转天数较年初增加"+day+"天，应收账款账期风险过高;\n");
//					serNumber ++ ;
//				}
//				
//				int profitLife = getProfitLife();
//				if(profitLife == 0){
//					condition = false;
//					admittMap.put("profitLife",new AdmittDto("（营业利润）赢利年限","","三年内未盈利","不予准入"));
//					admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".三年内未盈利.\n");
//					serNumber ++ ;
//				}
//				
//				double financialLeverageRatio = calculationFinancialLeverageRatio();
//				if(financialLeverageRatio > 4){
//					condition = false;
//					admittMap.put("financialLeverageRatio",new AdmittDto("财务杠杆率 （总负债/净资产）",df.format(financialLeverageRatio)+"倍","财务杠杆率高于4.0倍","不予准入"));
//					admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".财务杠杆率过高;\n");
//					serNumber ++ ;
//				}
			}
			catch (NullPointerException e) {
				e.printStackTrace();
				admittMap.put("report",new AdmittDto("财报完整性","","企业财报数据缺失","黄色预警"));
			}
			
		}
		return condition;
	}
	
	/***
	 * 判断是否需要增信准入
	 * false:需要增信准入
	 * true:可直接准入
	 */
	private boolean isAddCredit(Map<String,AdmittDto> admittMap ) throws Exception {
		int serNumber = 1;
		boolean condition = true;
		//四舍五入转化
		DecimalFormat df = new DecimalFormat("#0.00");
		String comLife = admittance.getCompLife();
		if(comLife==null||"".equals(comLife))
			comLife = "0";
		if(Integer.parseInt(comLife)<3){
			admittMap.put("compLife",new AdmittDto("公司成立年限",comLife+"年","公司成立年限不足三年","增信准入"));
			admittance.setAdmittanceInfo(serNumber + ".公司成立年限不足三年;\n");
			serNumber ++ ;
		} else{
			admittance.setAdmittanceInfo("");
			admittMap.put("compLife",new AdmittDto("公司成立年限",comLife+"年","公司成立年限大于三年","可准入"));
		} 
		if("0".equals(admittance.getCreditInfo())){
			condition = false;
			admittMap.put("settlement",new AdmittDto("在本行贷款和结算纪录","","与我行未合作","增信准入"));
			admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".与我行未合作;\n");
			serNumber ++ ;
		}else {
			admittMap.put("settlement",new AdmittDto("在本行贷款和结算纪录","","有贷款，无不良记录","可准入"));
		}
		if("1".equals(admittance.getCreditRecord())){
			condition = false;
			admittMap.put("creditRecord",new AdmittDto("企业征信记录","","未查出征信记录","增信准入"));
			admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".未查出征信记录;\n");
			serNumber ++ ;
		}else {
			admittMap.put("creditRecord",new AdmittDto("企业征信记录","","有且无逾期记录","可准入"));
		}
		if("1".equals(admittance.getEarlyWaring())){
			condition = false;
			admittMap.put("earlyWaring",new AdmittDto("存量客户，过去一年内预警情况","","出现严重预警情况，已解决","增信准入"));
		}else {
			admittMap.put("earlyWaring",new AdmittDto("存量客户，过去一年内预警情况","","未出现","可准入"));
		}
		if("1".equals(admittance.getGuarntee())){
			if("1".equals(admittance.getBeoverdue())){
				condition = false;
				admittMap.put("beoverdue",new AdmittDto("对外担保的贷款","","企业有对外担保","增信准入"));
				admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".企业有对外担保;\n");
				serNumber ++ ;
			}else {
				admittMap.put("beoverdue",new AdmittDto("对外担保的贷款","","无对外担保","可准入"));
			}
			/*if(admittance.getGuarnteeAmount() != null && "0".equals(admittance.getGuarnteeAmount())){
				double rate = calculationGuarntee();
				if(rate < 1 && rate>=0.3){
					condition = false;
					admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".企业对外担保金额占净资产为"+df.format(rate*100)+"%,占比高;\n");
					serNumber ++ ;
				}
			}*/
		}else {
			admittMap.put("beoverdue",new AdmittDto("对外担保的贷款","","无对外担保","可准入"));
		}
		
		if("1".equals(admittance.getEnforcement())){
			condition = false;
			admittMap.put("enforcement",new AdmittDto("最近五年内有强制执行记录","","受行政处罚，申请复议且复议结果对本人有利","增信准入"));
			admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".受行政处罚，申请复议且复议结果对本人有利;\n");
			serNumber ++ ;
		}else {
			admittMap.put("enforcement",new AdmittDto("最近五年内有强制执行记录","","无强制执行记录","可准入"));
		}
		
		if("1".equals(admittance.getPunish())){
			condition = false;
			admittMap.put("punish",new AdmittDto("最近两年内行政处罚记录","","曾受行政处罚被责令限期改正或五年内曾被没收违法所得的，申请复议且复议结果对本人有利","增信准入"));
			admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".曾受行政处罚被责令限期改正或五年内曾被没收违法所得的，申请复议且复议结果对本人有利;\n");
			serNumber ++ ;
		}else {
			admittMap.put("punish",new AdmittDto("最近两年内行政处罚记录","","无行政处罚","可准入"));
		}
		if(newBalanceMap!=null&&newCashFlowMap!=null&&newIncomeMap!=null&&newBalanceMap_last!=null&&newIncomeMap_last!=null&&newBalanceMap_first!=null){
			try{
				if("1".equals(admittance.getGuarntee())){
					if(admittance.getGuarnteeAmount() != null && "0".equals(admittance.getGuarnteeAmount())){
						double rate = calculationGuarntee();
						if(rate < 1 && rate>=0.3){
							admittMap.put("guarntee",new AdmittDto("对外担保金额",df.format(rate*100)+"%","企业对外担保金额占净资产处于30%至100%之间,占比高","增信准入"));
							condition = false;
							admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".企业对外担保金额占净资产为"+df.format(rate*100)+"%,占比高;\n");
							serNumber ++ ;
						}else {
							admittMap.put("guarntee",new AdmittDto("对外担保金额",df.format(rate*100)+"%","企业对外担保金额占净资产低于30%","可准入"));
						}
					}else {
						admittMap.put("guarntee",new AdmittDto("对外担保金额","","企业对外担保金额占净资产小于30%","可准入"));
					}
				}
				double debtRatio = calculationDebtRatio();
				if(debtRatio>=1 && debtRatio<2){
					condition = false;
					admittMap.put("debtRatio",new AdmittDto("还款能力: 偿债保障比率",df.format(debtRatio*100)+"%","企业偿债保障比率处于100%到200%之间,比率低","增信准入"));
					admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".企业偿债保障比率为"+df.format(debtRatio*100)+"%,比率低;\n");
					serNumber ++ ;
				}else {
					admittMap.put("debtRatio",new AdmittDto("还款能力: 偿债保障比率",df.format(debtRatio*100)+"%","企业偿债保障比率大于200%","可准入"));
				}
				
				double salesRevenueRate = calculationSalesGrowthRate();
				if(salesRevenueRate>=-0.1 && salesRevenueRate<0.05){
					condition = false;
					admittMap.put("salesRevenueRate",new AdmittDto("销售收入增长情况",df.format(salesRevenueRate*100)+"%","企业销售增长率处于-10% 到 5% 之间，销售增长低","增信准入"));
					admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".企业销售增长率为"+df.format(salesRevenueRate*100)+"%，销售增长低;\n");
					serNumber ++ ;
				}else {
					admittMap.put("salesRevenueRate",new AdmittDto("销售收入增长情况",df.format(salesRevenueRate*100)+"%","企业销售增长率大于 5% ，销售增长高","可准入"));
				}
				
				int day = calculationAccountsReceivableRisk();
				if(day<=55 && day >40){
					condition = false;
					admittMap.put("ysDay",new AdmittDto("应收账款账期风险（应收账款周转天数较年初增加）",day+"天","应收账款周转天数较年初增加天数处于40天至55天，应收账款账期风险高","增信准入"));
					admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".应收账款周转天数较年初增加"+day+"天，应收账款账期风险高;\n");
					serNumber ++ ;
				}else {
					admittMap.put("ysDay",new AdmittDto("应收账款账期风险（应收账款周转天数较年初增加）",day+"天","应收账款周转天数较年初增加天数少于40天，应收账款账期风险低","可准入"));
				}
				
				int profitLife = getProfitLife();
				if(profitLife == 1){
					condition = false;
					admittMap.put("profitLife",new AdmittDto("（营业利润）赢利年限","","三年内只盈利一年","增信准入"));
					admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".三年内只盈利一年.\n");
					serNumber ++ ;
				}else {
					admittMap.put("profitLife",new AdmittDto("（营业利润）赢利年限","","三年内盈利两年以上","增信准入"));
				}
				
				double financialLeverageRatio = calculationFinancialLeverageRatio();
				if(financialLeverageRatio <= 4 && financialLeverageRatio>2.75){
					condition = false;
					admittMap.put("financialLeverageRatio",new AdmittDto("财务杠杆率 （总负债/净资产）",df.format(financialLeverageRatio)+"倍","财务杠杆率处于2.75倍到4.0倍之间","增信准入"));
					admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".财务杠杆率高;\n");
					serNumber ++ ;
				}else {
					admittMap.put("financialLeverageRatio",new AdmittDto("财务杠杆率 （总负债/净资产）",df.format(financialLeverageRatio)+"倍","财务杠杆率低于2.75倍","可准入"));
				}
			}catch (NullPointerException e) {
				condition = false;
				admittMap.put("report",new AdmittDto("财报完整性","","企业年报数据不全","增信准入"));
				admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".企业年报数据不全;\n");
				serNumber ++ ;
			}
		}else{
				condition = false;
				admittMap.put("report",new AdmittDto("财报完整性","","企业年报数据不全","增信准入"));
				admittance.setAdmittanceInfo(admittance.getAdmittanceInfo()+" " + serNumber + ".企业年报数据不全;\n");
				serNumber ++ ;
		}
		return condition;
	}
	
	//计算企业对外担保金额占净资产比率
	private double calculationGuarntee(){
		SReportData sReportData = newBalanceMap.get("266");
		double guarnteeAmount = Double.parseDouble(admittance.getGuarnteeAmount());
		double rate = guarnteeAmount/sReportData.getCol2Value();
		return rate;
	}
	
	//计算偿债保障比率
	private double calculationDebtRatio() throws Exception{
		SReportData Total_liabilities = newBalanceMap.get("246");
		SReportData Cashflow_activities = newCashFlowMap.get("418");
		double debtRatio = Total_liabilities.getCol2Value()/Cashflow_activities.getCol2Value();
		return debtRatio;
	}
	
	//计算销售增长率
	private double calculationSalesGrowthRate(){
		double salesRevenue = newIncomeMap.get("301").getCol2Value();
		double salesRevenue_last = newIncomeMap_last.get("301").getCol2Value();
		double salesRevenueRate = (salesRevenue-salesRevenue_last)/salesRevenue_last;
		return salesRevenueRate;
	}
	
	//应收账期风险
	private int calculationAccountsReceivableRisk(){
		double salesRevenue = newIncomeMap.get("301").getCol2Value();
		double accountsReceivable = newBalanceMap.get("107").getCol2Value();
		double accountsReceivable_last = newBalanceMap_last.get("107").getCol2Value();
		double accountsReceivableRate = salesRevenue/((accountsReceivable+accountsReceivable_last)/2);
		int accountsReceivableDay = (int) (365/accountsReceivableRate);
		
		double salesRevenue_last = newIncomeMap_last.get("301").getCol2Value();
		double accountsReceivable_first = newBalanceMap_first.get("107").getCol2Value();
		double accountsReceivableRate_last = salesRevenue_last/((accountsReceivable_first+accountsReceivable_last)/2);
		int accountsReceivableDay_last = (int) (365/accountsReceivableRate_last);
		int day = accountsReceivableDay - accountsReceivableDay_last;
		return day;
	}
	
	//获取近三年盈利年限
	private int getProfitLife(){
		int profitLife = 0;
		double profitNumber = newIncomeMap.get("333").getCol2Value();
		if(profitNumber > 0){
			profitLife++;
		}
		double profitNumber_last = newIncomeMap_last.get("333").getCol2Value();
		if(profitNumber_last > 0){
			profitLife++;
		}
		double profitNumber_first = newIncomeMap_last.get("333").getCol2Value();
		if(profitNumber_first > 0){
			profitLife++;
		}
		return profitLife;
	}
	
	//财务杠杆率
	private double calculationFinancialLeverageRatio(){
		double totalLiabilities = newBalanceMap.get("246").getCol2Value();
		double netAssets = newBalanceMap.get("266").getCol2Value();
		return totalLiabilities/netAssets;
	}

	@Override
	public Admittance getAdmittanceByCustmoerNo(String custmoerNo) throws Exception {
//		List<Admittance> list = admittanceDAO.getAdmittanceByCustmoerNo(custmoerNo);
		List<Admittance> list = null;
		if(list != null && list.size()!=0){
			return list.get(0);
		}
		return null;
	}

	@Override
	public Map<String, AdmittDto> listAdmittance(Admittance admittance) throws Exception {
		// 获取2017年报,分别以新旧准则查询
		CusFSRecord queryUnique;
		queryUnique = cusFSRecordSV.queryUnique(admittance.getCustomerNo(), "2017-12", "020");
		if (queryUnique == null) {
			queryUnique = cusFSRecordSV.queryUnique(admittance.getCustomerNo(), "2017-12", "010");
		}
		if (queryUnique != null) {
			List<SReportRecord> queryByObjectNo = sReportRecordSV.queryByObjectNo(queryUnique.getRecordNo());
			for (SReportRecord sReportRecord : queryByObjectNo) {
				if ("资产负债表".equals(sReportRecord.getReportName())) {
					newBalanceMap = sReportDataSV.queryMapByRepNo(sReportRecord.getReportNo());
				} else if ("现金流量表(自动生成)".equals(sReportRecord.getReportName())) {
					newCashFlowMap = sReportDataSV.queryMapByRepNo(sReportRecord.getReportNo());
				} else if ("利润表".equals(sReportRecord.getReportName())) {
					newIncomeMap = sReportDataSV.queryMapByRepNo(sReportRecord.getReportNo());
				}
			}
		}

		// 获取2016年报,分别以新旧准则查询
		CusFSRecord queryUnique_last;
		queryUnique_last = cusFSRecordSV.queryUnique(admittance.getCustomerNo(), "2016-12", "020");
		if (queryUnique_last == null) {
			queryUnique_last = cusFSRecordSV.queryUnique(admittance.getCustomerNo(), "2016-12", "010");
		}
		if (queryUnique_last != null) {
			List<SReportRecord> queryByObjectNo_last = sReportRecordSV.queryByObjectNo(queryUnique_last.getRecordNo());
			if (queryByObjectNo_last != null) {
				for (SReportRecord sReportRecord : queryByObjectNo_last) {
					if ("资产负债表".equals(sReportRecord.getReportName())) {
						newBalanceMap_last = sReportDataSV.queryMapByRepNo(sReportRecord.getReportNo());
					} else if ("现金流量表(自动生成)".equals(sReportRecord.getReportName())) {
						/*
						 * newCashFlowMap_last =
						 * sReportDataSV.queryMapByRepNo(sReportRecord.getReportNo());
						 */
					} else if ("利润表".equals(sReportRecord.getReportName())) {
						newIncomeMap_last = sReportDataSV.queryMapByRepNo(sReportRecord.getReportNo());
					}
				}
			}
		}

		// 获取2015年报,分别以新旧准则查询
		CusFSRecord queryUnique_first;
		queryUnique_first = cusFSRecordSV.queryUnique(admittance.getCustomerNo(), "2015-12", "020");
		if (queryUnique_first == null) {
			queryUnique_first = cusFSRecordSV.queryUnique(admittance.getCustomerNo(), "2015-12", "010");
		}
		if (queryUnique_first != null) {
			List<SReportRecord> queryByObjectNo_first = sReportRecordSV
					.queryByObjectNo(queryUnique_first.getRecordNo());
			if (queryByObjectNo_first != null) {
				for (SReportRecord sReportRecord : queryByObjectNo_first) {
					if ("资产负债表".equals(sReportRecord.getReportName())) {
						newBalanceMap_first = sReportDataSV.queryMapByRepNo(sReportRecord.getReportNo());
					} /*
						 * else if("现金流量表(自动生成)".equals(sReportRecord.getReportName())){
						 * newCashFlowMap_first =
						 * sReportDataSV.queryMapByRepNo(sReportRecord.getReportNo()); }else
						 * if("利润表".equals(sReportRecord.getReportName())){ newIncomeMap_first =
						 * sReportDataSV.queryMapByRepNo(sReportRecord.getReportNo()); }
						 */
				}
			}
		}

		Map<String,AdmittDto> admittMap = new HashMap<String,AdmittDto>();
		admittMap.put("report",new AdmittDto("财报完整性","","企业年报数据完整","可准入"));
		this.admittance = admittance;
		// 判断是否可准入
		boolean admittance_info = isAdmittance(admittMap);
		if (admittance_info == true) {
			admittance.setAdmittanceResult("0");
		}else {
			admittance.setAdmittanceResult("1");
		}
//		if (admittance_info == true) {
//			boolean addCredit_info = isAddCredit(admittMap);
//			if (addCredit_info == true) {
//				admittance.setAdmittanceResult("0");
//			} else {
//				admittance.setAdmittanceResult("1");
//			}
//		} else {
//			admittance.setAdmittanceResult("2");
//		}
		return admittMap;
	}
	
}
