package com.beawan.customer.service.impl;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.common.Constants;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.dao.ICusBaseDAO;
import com.beawan.customer.qbean.QCusBase;
import com.beawan.customer.service.ICusBaseSV;
import com.platform.util.JdbcUtil;
import com.platform.util.StringUtil;

@Service("cusBaseSV")
public class CusBaseSVImpl implements ICusBaseSV {

	@Resource
	private ICusBaseDAO cusBaseDAO;
	
	@Override
	public CusBase save(CusBase cusBase) {
		return cusBaseDAO.saveOrUpdate(cusBase);
	}

	@Override
	public void delete(CusBase cusBase) {
		cusBaseDAO.deleteEntity(cusBase);
	}

	@Override
	public CusBase queryByCusNo(String cusNo) {
		return cusBaseDAO.selectByPrimaryKey(cusNo);
	}
	
	@Override
	public List<CusBase> queryAll() {
		return cusBaseDAO.getAll();
	}
	
	@Override
	public List<Map<String, Object>> queryPaging(QCusBase queryCondition,
			String orderCondition, int pageIndex, int pageSize) throws Exception {

		String queryStr = genQueryStr(queryCondition);
		int startIndex = pageIndex*pageSize;
		int endIndex = startIndex + pageSize;

		String sql = "SELECT * FROM ("
	          // + " SELECT ROW_NUMBER() OVER() AS ROW_NUM,C.CUSTOMER_NO,C.CUSTOMER_NAME,C.CUSTOMER_TYPE,C.MF_CUSTOMER_NO,"
	           + " SELECT count(*) AS ROW_NUM,C.CUSTOMER_NO,C.CUSTOMER_NAME,C.CUSTOMER_TYPE,C.MF_CUSTOMER_NO,"
	           + " C.UPDATE_DATE,O.ORGANNAME AS ORG_FULLNAME,O.ORGANSHORTFORM AS ORG_NAME,U.USER_NAME AS MANAGER_USER_NAME"
	           + " FROM CUS_BASE C"
	           + " LEFT JOIN BS_USER_INFO U ON U.USER_ID = C.MANAGER_USER_ID"
	           + " LEFT JOIN BS_ORG O ON U.ACC_ORG_NO = O.ORGANNO"
	           + " WHERE "+queryStr
				+ " ) T limit "+pageIndex+","+pageSize;
	          // + " ) T WHERE T.ROW_NUM > "+startIndex+ " AND T.ROW_NUM <= " + endIndex;
		
		//String queryStr = genQueryStr(queryCondition);
		//sql = sql.replace("$", queryStr);
		
		if (StringUtil.isEmptyString(orderCondition))
			orderCondition = "C.UPDATE_DATE desc nulls last";
		
		sql = sql.replace("OVER()", "OVER(ORDER BY " + orderCondition + ")");
		
		//int startIndex = pageIndex*pageSize;
		//int endIndex = startIndex + pageSize;
		//Object[] params = new Object[]{startIndex, endIndex};
		Object[] params = null;

		return JdbcUtil.query(Constants.DataSource.TCE_DS, sql, params);
	}
	
	@Override
	public long queryCount(QCusBase queryCondition) throws Exception {
		String sql = "SELECT COUNT(*) "
				   + " FROM CUS_BASE C"
		           + " LEFT JOIN BS_USER_INFO U ON U.USER_ID = C.MANAGER_USER_ID"
		           + " LEFT JOIN BS_ORG O ON U.ACC_ORG_NO = O.ORGANNO"
		           + " WHERE ";
		StringBuilder stringBuilder = new StringBuilder(sql);
		String queryStr = genQueryStr(queryCondition);
		stringBuilder.append(queryStr);

		 sql = stringBuilder.toString();
		 //sql = sql.replace("$", queryStr);
		return JdbcUtil.queryForLong(Constants.DataSource.TCE_DS, sql, null);
	}
	
	private String genQueryStr(QCusBase queryCondition) throws Exception {
		
		StringBuffer listSql = new StringBuffer("C.STATUS='0' AND C.CUSTOMER_TYPE NOT LIKE '%03%'");
		
		if (queryCondition != null) {
			
			if (!StringUtil.isEmptyString(queryCondition.getCustomerNo())) {
				listSql.append(" AND C.CUSTOMER_NO = '")
				       .append(queryCondition.getCustomerNo())
				       .append("'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getCustomerName())) {
				String name = URLDecoder.decode(queryCondition.getCustomerName(), "UTF-8");
				listSql.append(" AND C.CUSTOMER_NAME LIKE '%")
			       .append(name)
			       .append("%'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getManagerUserId())) {
				listSql.append(" AND U.USER_ID = '")
				       .append(queryCondition.getManagerUserId())
				       .append("'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getManagerUserName())) {
				String name = URLDecoder.decode(queryCondition.getManagerUserName(), "UTF-8");
				listSql.append(" AND U.USER_NAME LIKE '%")
			       .append(name)
			       .append("%'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getManagerOrgId())) {
				listSql.append(" AND O.ORGANNO = '")
				       .append(queryCondition.getManagerOrgId())
				       .append("'");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getManagerOrgName())) {
				String name = URLDecoder.decode(queryCondition.getManagerOrgName(), "UTF-8");
				listSql.append(" AND O.ORGANNAME LIKE '%").append(name).append("%'");
			}
		}
		
		return listSql.toString();
	}

	@Override
	public Map<String, Object> queryCusBasePaging(CusBase queryCondition, String orderCondition, int index, int count)
			throws Exception {
		
		StringBuffer listSql = new StringBuffer("1=1");
		StringBuffer totalSql = new StringBuffer("1=1");
		
		Map<String, Object> params = new HashMap<String, Object>();
		if (queryCondition != null) {
			
			if (!StringUtil.isEmptyString(queryCondition.getCustomerNo())) {
				listSql.append(" and customerNo=:customerNo");
				totalSql.append(" and customerNo=:customerNo");
				params.put("customerNo", queryCondition.getCustomerNo());
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getCustomerName())) {
				listSql.append(" and customerName like:customerName");
				totalSql.append(" and customerName like:customerName");
				String name = URLDecoder.decode(queryCondition.getCustomerName(), "UTF-8");
				params.put("customerName", "%" + name + "%");
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getCustomerType())) {
				listSql.append(" and customerType=:customerType");
				totalSql.append(" and customerType=:customerType");
				params.put("customerType", queryCondition.getCustomerType());
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getCertType())) {
				listSql.append(" and certType=:certType");
				totalSql.append(" and certType=:certType");
				params.put("certType", queryCondition.getCertType());
			}

			/*if (queryCondition.getRetail()!=null) {
				listSql.append(" and retail=:retail");
				totalSql.append(" and retail=:retail");
				params.put("retail", queryCondition.getRetail());
			}*/
			
			if (!StringUtil.isEmptyString(queryCondition.getCertCode())) {
				listSql.append(" and certCode=:certCode");
				totalSql.append(" and certCode=:certCode");
				params.put("certCode", queryCondition.getCertCode());
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getManagerUserId())) {
				listSql.append(" and managerUserId=:managerUserId");
				totalSql.append(" and managerUserId =:managerUserId");
				params.put("managerUserId", queryCondition.getManagerUserId());
			}
			
			if (!StringUtil.isEmptyString(queryCondition.getManagerOrgId())) {
				listSql.append(" and managerOrgId=:managerOrgId");
				totalSql.append(" and managerOrgId =:managerOrgId");
				params.put("managerUserId", queryCondition.getManagerOrgId());
			}
		}
		
		if (!StringUtil.isEmptyString(orderCondition))
			listSql.append(" order by ").append(orderCondition);
		else
			listSql.append(" order by inputDate desc");
		
		Map<String, Object> dataMap = new HashMap<String, Object>();
		
		List<CusBase> list = cusBaseDAO.selectRange(listSql.toString(),
				index, count, params);
		long total = cusBaseDAO.selectCount(totalSql.toString(), params);
		dataMap.put("total", total);
		dataMap.put("rows", list);
		
		return dataMap;
	}

	@Override
	public CusBase queryByCusName(String customerName) {
		// TODO Auto-generated method stub
		String sql = "status=:status and customerName=:customerName";
		Map<String,Object> params = new HashMap<>();
		params.put("customerName", customerName);
		params.put("status", Constants.NORMAL);
		return cusBaseDAO.selectSingle(sql, params);
	}
	
	@Override
	public CusBase queryByXdCusNo(String xdCustomerNo){
		String sql = "status=:status and xdCustomerNo=:xdCustomerNo";
		Map<String,Object> params = new HashMap<>();
		params.put("xdCustomerNo", xdCustomerNo);
		params.put("status", Constants.NORMAL);
		return cusBaseDAO.selectSingle(sql, params);	
	}

}
