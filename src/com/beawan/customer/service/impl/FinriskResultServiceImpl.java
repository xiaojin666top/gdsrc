package com.beawan.customer.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.Resource;

import com.beawan.analysis.abnormal.bean.ErrData;
import com.beawan.analysis.abnormal.dao.IErrDataDao;
import com.beawan.analysis.finansis.dto.FinaRatioDto;
import com.beawan.base.entity.InduInfo;
import com.beawan.base.service.InduInfoService;
import com.beawan.core.ResultDto;
import com.beawan.customer.dto.*;
import com.beawan.customer.service.ICusFSRecordSV;
import com.beawan.library.bean.TreeData;
import com.beawan.library.service.IIndustrySV;
import com.beawan.common.config.AppConfig;
import com.beawan.survey.custInfo.bean.CompFinance;
import com.beawan.survey.custInfo.service.ICompFinanceSV;
import com.beawan.survey.loanInfo.bean.RiskAnalysis;
import com.beawan.survey.loanInfo.service.IConclusionSV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.platform.util.*;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.loader.custom.ResultRowProcessor;
import org.springframework.stereotype.Service;

import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.customer.bean.FinriskResult;
import com.beawan.customer.dao.FinriskResultDao;
import com.beawan.customer.service.FinriskResultService;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * @author yzj
 * 财务分析service层
 */
@Service("finriskResultService")
public class FinriskResultServiceImpl extends BaseServiceImpl<FinriskResult> implements FinriskResultService{


	private static Logger log = Logger.getLogger(FinriskResultServiceImpl.class);
    /**
    * 注入DAO
    */
    @Resource(name = "finriskResultDao")
    public void setDao(BaseDao<FinriskResult> dao) {
        super.setDao(dao);
    }
    @Resource
    public FinriskResultDao finriskResultDao;
	@Resource
	private ICusFSRecordSV cusFSRecordSV;
	@Resource
	protected IErrDataDao errDataDao;
	@Resource
	protected ITaskSV taskSV;
	@Resource
	protected ICompFinanceSV compFinanceSV;
	@Resource
	private IConclusionSV conclusionSV;
	@Resource
	private InduInfoService induInfoService;
	@Resource
	private IIndustrySV industrySV;
    
    @Override
	public String getLocalResultData(String taskId) throws UnsupportedEncodingException {
		return this.getFinriskData(taskId, Constants.Platform.LOCAL_FINRISK);
		
	}
    
	@Override
	public String getCloudResultData(String taskId) throws UnsupportedEncodingException {
		return this.getFinriskData(taskId, Constants.Platform.CLOUD_FINRISK);
		
	}

	@Override
	public String getFinriskData(String serNo, Integer platform) throws UnsupportedEncodingException {
		Map<String, Object> params = new HashMap<>();
		params.put("serNo", serNo);
		params.put("platformType", platform);
//		FinriskResult result = finriskResultDao.selectSingleByProperty("dgSerialNumber", Long.parseLong(taskId));
		FinriskResult result = finriskResultDao.selectSingle("dgSerialNumber=:serNo and platformType=:platformType", params);
		if(result==null)
			return null;
//		String jsonData = URLDecoder.decode(result.getJsonData(), "UTF-8");
		return result.getJsonData();
	}

	@Override
	public String getQuotaData(String serNo, Integer platform) throws UnsupportedEncodingException {
		Map<String, Object> params = new HashMap<>();
		params.put("serNo", serNo);
		params.put("platformType", platform);
//		FinriskResult result = finriskResultDao.selectSingleByProperty("dgSerialNumber", Long.parseLong(taskId));
		FinriskResult result = finriskResultDao.selectSingle("dgSerialNumber=:serNo and platformType=:platformType", params);
		if(result==null)
			return null;
//		String jsonData = URLDecoder.decode(result.getJsonData(), "UTF-8");
		return result.getQuotaInfo();
	}

	@Override
	public void syncFinriskResult(String serialNumber, String customerNo, String induCode) throws Exception{
		Map<String, String> params = new HashMap<>();
//		customerNo = "ID202007061434496534";
//		serialNumber = "DGZJ20200709645";
//		induCode = "1311";

		Task task = taskSV.getTaskBySerNo(serialNumber);
		Integer year = task.getYear();
		Integer month = task.getMonth();

		String json = cusFSRecordSV.getReportJsonData(customerNo, induCode, year, month);
		System.out.println(json);
		params.put("data", json);


		HttpClientUtil instance1 = HttpClientUtil.getInstance(AppConfig.Cfg.LOCAL_FINRISK_ANALYSIS);
		String resultTask = instance1.doPost(params);

		log.info("对公传输财报数据为报文为：");
		log.info(resultTask);
		resultTask = URLDecoder.decode(resultTask, "utf-8");

		//获得解析器
		JsonParser parser = new JsonParser();
		//获取根节点元素
		Integer code = parser.parse(resultTask).getAsJsonObject().get("code").getAsInt();
		String msg = parser.parse(resultTask).getAsJsonObject().get("msg").getAsString();
		String resultTaskId = parser.parse(resultTask).getAsJsonObject().get("rows").getAsString();
		if(code==ResultDto.RESULT_CODE_FAIL) {
			ExceptionUtil.throwException(msg);
		}
//			resultTaskId = "DGZJ20200619465";

		//通过返回的流水号去查询 所有分析结果 respongse 并分解入库
		params.clear();
		params.put("taskId", resultTaskId);


		//对接本地财务分析 并保存结果
		HttpClientUtil instance2 = HttpClientUtil.getInstance(AppConfig.Cfg.LOCAL_ANALYSIS_RESULT);
		String responseBody = instance2.doPost(params);
		responseBody = URLDecoder.decode(responseBody, "utf-8");


		Integer resultCode = parser.parse(responseBody).getAsJsonObject().get("code").getAsInt();
		String resultMsg = parser.parse(responseBody).getAsJsonObject().get("msg").getAsString();
		if(resultCode==ResultDto.RESULT_CODE_FAIL) {
			ExceptionUtil.throwException(resultMsg);
		}
		String localResponse = parser.parse(responseBody).getAsJsonObject().get("rows").getAsString();

		if(!StringUtil.isEmptyString(localResponse)){
			localResponse = URLDecoder.decode(localResponse, "utf-8");
		}
		FinriskResult finriskResult = this.saveFinriskResult(serialNumber, resultTaskId
				, localResponse, Constants.Platform.LOCAL_FINRISK);
		//将财务分析结果保存到对公系统的各个模块中
		//重要科目的异常项同步
		List<ErrData> errDataList = this.getErr(serialNumber);
		if(errDataList!=null && errDataList.size()!=0){
			for(ErrData data : errDataList){
				data.setTaskId(task.getId());
				data.setExplain(data.getErrReason() + data.getReason());
				errDataDao.savaErrData(data);
			}
		}
		//
		//同步 现金流量科目相关信息
		Map<String, Object>  cashflowAnalyData = this.getLocalCashflowAnalyData(task.getSerNo());
		CompFinance compFinance = compFinanceSV.findCompFinanceByTaskId(task.getId());
		if(compFinance==null || StringUtil.isEmptyString(compFinance.getCashFlowAnaly())){
			if(compFinance==null){
				compFinance = new CompFinance();
				compFinance.setTaskId(task.getId());
				compFinance.setCustomerNo(task.getCustomerNo());
			}
			String cashFlowAnaly = cashflowAnalyData.get("third").toString() + "\n"
					+ cashflowAnalyData.get("four").toString() + "\n"
					+ cashflowAnalyData.get("five").toString();
			compFinance.setCashFlowAnaly(cashFlowAnaly);
			compFinance = compFinanceSV.saveCompFinance(compFinance);
		}
		//同步财务分析结论
		if(compFinance!=null){
			Map<String, Object> localFinAnaly = this.getLocalFinAnaly(task.getSerNo());
			if(StringUtil.isEmptyString(compFinance.getProfitAnaly())){
				compFinance.setProfitAnaly(String.valueOf(localFinAnaly.get("profit")));
			}
			if(StringUtil.isEmptyString(compFinance.getDevelopAnaly())){
				compFinance.setDevelopAnaly(String.valueOf(localFinAnaly.get("develop")));
			}
			if(StringUtil.isEmptyString(compFinance.getOperateAnaly())){
				compFinance.setOperateAnaly(String.valueOf(localFinAnaly.get("operate")));
			}
			if(StringUtil.isEmptyString(compFinance.getSolvency())){
				compFinance.setSolvency(String.valueOf(localFinAnaly.get("debt")));
			}
			if(StringUtil.isEmptyString(compFinance.getSystemAnaly())){
				compFinance.setSystemAnaly(String.valueOf(localFinAnaly.get("summay")));
			}
			compFinance = compFinanceSV.saveCompFinance(compFinance);
		}
		//同步财务风险
		List<WarnItemDto> warnItemList = this.getFinaWarn(serialNumber);
		StringBuilder financialRisk = new StringBuilder("财务信息经系统分析检测，状况正常。");
		if(!CollectionUtils.isEmpty(warnItemList)){
			financialRisk = new StringBuilder();
			for(WarnItemDto warn : warnItemList){
				String abnormalItem = warn.getAbnormalItem();
				String errReason = warn.getErrReason();
				financialRisk.append(abnormalItem).append("，").append(errReason);
			}
		}
		

		RiskAnalysis riskAnalysis = conclusionSV.findRiskAnalysisByTaskId(task.getId());
		if(riskAnalysis==null){
			riskAnalysis = new RiskAnalysis();
			riskAnalysis.setSerNo(serialNumber);
		}
		riskAnalysis.setFinancialRisk(financialRisk.toString());
		conclusionSV.saveRiskAnalysis(riskAnalysis);
		//获取流动资金测算
		//对接本地财务分析 并保存结果
		HttpClientUtil instance3 = HttpClientUtil.getInstance(AppConfig.Cfg.GET_QUOTA_INFO);
		String quotaBody = instance3.doPost(params);
//		quotaBody = URLDecoder.decode(quotaBody, "utf-8");


		Integer quotaCode = parser.parse(quotaBody).getAsJsonObject().get("code").getAsInt();
		String quotatMsg = parser.parse(quotaBody).getAsJsonObject().get("msg").getAsString();
		if (quotaCode == ResultDto.RESULT_CODE_FAIL) {
			ExceptionUtil.throwException(quotatMsg);
		}
		String quotaResponse = parser.parse(quotaBody).getAsJsonObject().get("rows").toString();
		finriskResult.setQuotaInfo(quotaResponse);
		finriskResultDao.saveOrUpdate(finriskResult);


		log.info("获取本地财务分析系统信息并同步对公系统成功！");
	}

    @Override
    public FinriskResult updateQuotaByGrowth(String growth, String serNo) throws Exception {
        Map<String , Object> params = new HashMap<>();
        params.put("dgSerialNumber", serNo);
        params.put("platformType", Constants.Platform.LOCAL_FINRISK);
        FinriskResult finriskResult = selectByProperty(params).get(0);

        Map<String , String> header = new HashMap<>();
        header.put("taskId", finriskResult.getResultTaskId());
        header.put("growth", growth);
        //获取流动资金测算
        //对接本地财务分析 并保存结果
        HttpClientUtil instance = HttpClientUtil.getInstance(AppConfig.Cfg.GET_QUOTA_INFO);
        String quotaBody = instance.doPost(header);
        quotaBody = URLDecoder.decode(quotaBody, "utf-8");

        //获得解析器
        JsonParser parser = new JsonParser();
        Integer quotaCode = parser.parse(quotaBody).getAsJsonObject().get("code").getAsInt();
        String quotatMsg = parser.parse(quotaBody).getAsJsonObject().get("msg").getAsString();
        if (quotaCode == ResultDto.RESULT_CODE_FAIL) {
            log.error("获取财务系统中新增流动资金缺口测算数据失败，返回错误异常内容：" + quotatMsg);
            ExceptionUtil.throwException(quotatMsg);
        }
        String quotaResponse = parser.parse(quotaBody).getAsJsonObject().get("rows").toString();
        finriskResult.setQuotaInfo(quotaResponse);
        return finriskResultDao.saveOrUpdate(finriskResult);
    }

    @Override
	public FinriskResult saveFinriskResult(String serialNumber, String resultTaskId, String response, Integer platform) throws Exception{
		if(response==null || "".equals(response))
			throw new Exception("返回报文数据内容为空");

		FinriskResult finriskResult = new FinriskResult();
		finriskResult.setDgSerialNumber(serialNumber);
		finriskResult.setResultTaskId(resultTaskId);
		if(platform==null || "".equals(platform))
			platform = Constants.Platform.LOCAL_FINRISK;
		finriskResult.setPlatformType(platform);
		finriskResult.setJsonData(response);
		return saveOrUpdate(finriskResult);
		
	}

	@Override
	public FinriskResult getLocalFinrisk(String serNo) {
		return getFinrisk(serNo, Constants.Platform.LOCAL_FINRISK);
	}

	@Override
	public FinriskResult getCloudFinrisk(String serNo) {
		return getFinrisk(serNo, Constants.Platform.CLOUD_FINRISK);
	}
	
	public FinriskResult getFinrisk(String serNo, Integer platform) {
		Map<String,Object> finriskParams =new HashMap<>();
		finriskParams.put("dgSerialNumber", serNo);
		finriskParams.put("platformType", platform);
		FinriskResult finriskResult = finriskResultDao.selectSingle(
				"dgSerialNumber=:dgSerialNumber AND platformType=:platformType", finriskParams);
		return finriskResult;
	}

	@Override
	public Map<String, Object> getLocalData(String taskId) throws Exception {
		Map<String, Object> data = new TreeMap<>();
		String resultStr = this.getFinriskData(taskId, Constants.Platform.LOCAL_FINRISK);
		//获得解析器
		JsonParser parser = new JsonParser();
		//获取根节点元素
		JsonElement element = parser.parse(resultStr);
		JsonObject rows = element.getAsJsonObject().get("rows").getAsJsonObject().get("detailReport").getAsJsonObject();
		int year = rows.get("year").getAsInt();
		data.put("year", year);
		int month = rows.get("month").getAsInt();
		data.put("month", month);
		//资产概览
		List<TableItemDto> assetList = new ArrayList<>();
		JsonArray itemList = rows.get("AssetOne").getAsJsonArray();
		for(JsonElement ele : itemList) {
			JsonArray array = ele.getAsJsonArray();
			TableItemDto dto = new TableItemDto();
			dto.setIndexName(array.get(0).getAsString());
			dto.setFirstVal(array.get(1).getAsString());
			dto.setSecondVal(array.get(2).getAsString());
			dto.setSecondChange(array.get(6).getAsString());
			dto.setThirdVal(array.get(3).getAsString());
			dto.setThirdChange(array.get(7).getAsString());
			dto.setFourthVal(array.get(4).getAsString());
			dto.setFourthChange(array.get(8).getAsString());
			assetList.add(dto);
		}
		data.put("assetList", assetList);
		String asset1 = rows.get("AssetTwo").getAsString();
		String asset2 = rows.get("AssetThree").getAsString();
		List<String> assetAnaly = new ArrayList<>();
		assetAnaly.add(asset1);
		assetAnaly.add(asset2);
		data.put("asset", assetAnaly);
		
		List<String> debtList = new ArrayList<>();
		JsonArray debts = rows.get("AssetFour").getAsJsonArray();
		for(JsonElement ele : debts) {
			String str = ele.getAsString();
			debtList.add(str);
		}
		data.put("debt", debtList);
		
		//利润
		List<TableItemDto> profitList = new ArrayList<>();
		JsonArray profitItemList = rows.get("Incfirst").getAsJsonArray();
		for(int i=0; i < 9; i++) {
			JsonElement ele = profitItemList.get(i);
			TableItemDto dto = new TableItemDto();
			String indexName = null;
			boolean flag = false;
			switch(i) {
				case 0: indexName = "销售收入";break;
				case 1: indexName = "营业成本";break;
				case 2: indexName = "毛利润";break;
				case 3: indexName = "销售费用";break;
				case 4: indexName = "管理费用";break;
				case 5: indexName = "财务费用";break;
				case 6: flag = true;break;
				case 7: indexName = "净利润";break;
				case 8: indexName = "销售毛利率(%)";break;
				case 9: indexName = "销售利润率(%)";break;
			}
			if(flag)
				continue;
			dto.setIndexName(indexName);
			JsonArray array = ele.getAsJsonArray();
			dto.setFirstVal(array.get(0).getAsString());
			dto.setSecondVal(array.get(1).getAsString());
			dto.setSecondChange(array.get(5).getAsString());
			dto.setThirdVal(array.get(2).getAsString());
			dto.setThirdChange(array.get(6).getAsString());
			dto.setFourthVal(array.get(3).getAsString());
			dto.setFourthChange(array.get(7).getAsString());
			profitList.add(dto);
		}
		data.put("profitList", profitList);

		String profitStr = rows.get("Incsecond").getAsString();
		List<String> profitAnaly = new ArrayList<>();
		profitAnaly.add(profitStr);
		data.put("profit", profitAnaly);
		return data;
	}

	@Override
	public Map<String, Object> getLocalFinAnaly(String taskId) throws Exception {
		Map<String, Object> data = new TreeMap<>();
		String resultStr = this.getFinriskData(taskId, Constants.Platform.LOCAL_FINRISK);
		if(StringUtil.isEmptyString(resultStr)) return data;
		
		//获得解析器
		JsonParser parser = new JsonParser();
		//获取根节点元素
		JsonElement element = parser.parse(resultStr);
		JsonObject rows = element.getAsJsonObject().get("rows").getAsJsonObject().get("detailReport").getAsJsonObject();
		int year = rows.get("year").getAsInt();
		data.put("year", year);
		int month = rows.get("month").getAsInt();
		data.put("month", month);
		
		JsonArray array = rows.get("json2").getAsJsonObject().get("data").getAsJsonArray();
		List<List<String>> list=  parseArrList(array);
		List<InduStandardDto> induStandardList = new ArrayList<>();
		
		InduStandardDto dto1 = new InduStandardDto("净资产收益率（％）", list.get(1).get(0), list.get(2).get(0), list.get(3).get(0), list.get(4).get(0), list.get(5).get(0), list.get(6).get(0));
		InduStandardDto dto2 = new InduStandardDto("总资产报酬率（％）", list.get(1).get(1), list.get(2).get(1), list.get(3).get(1), list.get(4).get(1), list.get(5).get(1), list.get(6).get(1));
		InduStandardDto dto3 = new InduStandardDto("成本费用利润率（％）", list.get(1).get(2), list.get(2).get(2), list.get(3).get(2), list.get(4).get(2), list.get(5).get(2), list.get(6).get(2));
		InduStandardDto dto4 = new InduStandardDto("主营业务利润率（％）", list.get(1).get(3), list.get(2).get(3), list.get(3).get(3), list.get(4).get(3), list.get(5).get(3), list.get(6).get(3));
		InduStandardDto dto5 = new InduStandardDto("资产负债率（％）", list.get(1).get(4), list.get(2).get(4), list.get(3).get(4), list.get(4).get(4), list.get(5).get(4), list.get(6).get(4));
		InduStandardDto dto6 = new InduStandardDto("速动比率（％）", list.get(1).get(5), list.get(2).get(5), list.get(3).get(5), list.get(4).get(5), list.get(5).get(5), list.get(6).get(5));
		InduStandardDto dto7 = new InduStandardDto("利息保障倍数（％）", list.get(1).get(6), list.get(2).get(6), list.get(3).get(6), list.get(4).get(6), list.get(5).get(6), list.get(6).get(6));
		InduStandardDto dto8 = new InduStandardDto("总资产周转率（％）", list.get(1).get(7), list.get(2).get(7), list.get(3).get(7), list.get(4).get(7), list.get(5).get(7), list.get(6).get(7));
		InduStandardDto dto9 = new InduStandardDto("存货周转率（％）", list.get(1).get(8), list.get(2).get(8), list.get(3).get(8), list.get(4).get(8), list.get(5).get(8), list.get(6).get(8));
		InduStandardDto dto10 = new InduStandardDto("应收账款周转率（％）", list.get(1).get(9), list.get(2).get(9), list.get(3).get(9), list.get(4).get(9), list.get(5).get(9), list.get(6).get(9));
		InduStandardDto dto11 = new InduStandardDto("总资产增长率（％）", list.get(1).get(10), list.get(2).get(10), list.get(3).get(10), list.get(4).get(10), list.get(5).get(10), list.get(6).get(10));
		
		
		induStandardList.add(dto1);
		induStandardList.add(dto2);
		induStandardList.add(dto3);
		induStandardList.add(dto4);
		induStandardList.add(dto5);
		induStandardList.add(dto6);
		induStandardList.add(dto7);
		induStandardList.add(dto8);
		induStandardList.add(dto9);
		induStandardList.add(dto10);
		induStandardList.add(dto11);
		
		data.put("induRatio", induStandardList);
		
		//盈利能力分析
		String profit = rows.get("RatioOne").getAsString();
		//运营能力分析
		String operate = rows.get("RatioThree").getAsString();
		//偿债能力分析
		String debt = rows.get("RatioTwo").getAsString();
		//发展能力分析
		String develop = rows.get("RatioFour").getAsString();
		//总体分析结论
		JsonArray summaryArray = rows.get("Summary").getAsJsonArray();
		StringBuilder summay = new StringBuilder();
		if(summaryArray!=null && summaryArray.size()>0) {
			for(int i = 0 ; i < summaryArray.size() ; i++){
				JsonElement obj = summaryArray.get(i);
				String str = obj.getAsJsonObject().get("describe").getAsString();
				if(str.startsWith("共检测出")){
					continue;
				}
				summay.append(str);
			}
			data.put("summay", summay);
		}
		
		data.put("profit", profit);
		data.put("operate", operate);
		data.put("debt", debt);
		data.put("develop", develop);
		
		return data;
	}
	private List<List<String>> parseArrList(JsonArray datas){
		List<List<String>> eghgtList = new ArrayList<>();
		for(JsonElement ele : datas) {
			JsonArray arrays = ele.getAsJsonArray();
			List<String> itemList = new ArrayList<>();
			if(arrays!=null && arrays.size()!=0) {
				for(JsonElement dto : arrays) {
					itemList.add(dto.getAsString());
				}
			}
			eghgtList.add(itemList);
		}
		return eghgtList;
	}

	@Override
	public Map<String, Object> getLocalCashflowAnalyData(String serNo) throws Exception {
		Map<String, Object> data = new TreeMap<>();
		String resultStr = this.getFinriskData(serNo, Constants.Platform.LOCAL_FINRISK);
		if(StringUtil.isEmptyString(resultStr)) return null;
		//获得解析器
		JsonParser parser = new JsonParser();
		//获取根节点元素
		JsonElement element = parser.parse(resultStr);
		JsonObject rows = element.getAsJsonObject().get("rows").getAsJsonObject().get("detailReport").getAsJsonObject();
		int year = rows.get("year").getAsInt();
		data.put("year", year);
		int month = rows.get("month").getAsInt();
		data.put("month", month);
		JsonArray tableone = rows.get("tableOne").getAsJsonArray();
		List<String> addItems = new ArrayList<>();
		for(JsonElement ele : tableone) {
			String str = ele.getAsString();
			addItems.add(str);
		}
		List<String> reduceItems = new ArrayList<>();
		JsonArray tableTwo = rows.get("tableTwo").getAsJsonArray();
		for(JsonElement ele : tableTwo) {
			String str = ele.getAsString();
			reduceItems.add(str);
		}
		data.put("addItems", addItems);
		data.put("reduceItems", reduceItems);
		JsonArray cashFlowArr = rows.get("cashFlowArr").getAsJsonArray();
		String json = GsonUtil.GsonString(cashFlowArr);
		List<CashflowDto> cashflowList = GsonUtil.GsonToList(json, CashflowDto.class);
		data.put("cashflowList", cashflowList);
		String third = rows.get("third").getAsString();
		String four = rows.get("four").getAsString();
		String five = rows.get("five").getAsString();
		if(!StringUtil.isEmptyString(third)){
			third = StringUtil.replaceEndBrHtml(third);
		}
		if(!StringUtil.isEmptyString(four)){
			four = StringUtil.replaceEndBrHtml(four);
		}
		if(!StringUtil.isEmptyString(five)){
			five = StringUtil.replaceEndBrHtml(five);
		}
		data.put("third", third);
		data.put("four", four);
		data.put("five", five);
		return data;
	}

	@Override
	public List<ErrData> getErr(String serNo) throws Exception {
		List<ErrData> data = new ArrayList<>();
		String resultStr = this.getFinriskData(serNo, Constants.Platform.LOCAL_FINRISK);
		//获得解析器
		JsonParser parser = new JsonParser();
		//获取根节点元素
		JsonElement element = parser.parse(resultStr);
		JsonObject rows = element.getAsJsonObject().get("rows").getAsJsonObject().get("detailReport").getAsJsonObject();
		JsonArray errPros = rows.get("errPros").getAsJsonArray();
		if(errPros==null)
			return null;
		for(JsonElement errPro : errPros){
			String name = errPro.getAsJsonObject().get("name").getAsString();
			String errReason = errPro.getAsJsonObject().get("errReason").getAsString();
			String reason = null;
			if(errPro.getAsJsonObject().get("reason")!=null)
				reason = errPro.getAsJsonObject().get("reason").getAsString();
			ErrData errData=  new ErrData(name, errReason, reason);
			data.add(errData);
		}
		return data;
	}

	@Override
	public List<FinaRatioDto> getRatio(String serNo) throws Exception {
		List<FinaRatioDto> data = new ArrayList<>();
		String resultStr = this.getFinriskData(serNo, Constants.Platform.LOCAL_FINRISK);
		if(StringUtil.isEmptyString(resultStr)) return null;
		
		//获得解析器
		JsonParser parser = new JsonParser();
		//获取根节点元素
		JsonElement element = parser.parse(resultStr);
		JsonObject rows = element.getAsJsonObject().get("rows").getAsJsonObject().get("detailReport").getAsJsonObject();

		//第一年数据
		JsonArray dataList3 = rows.get("ratejson3").getAsJsonObject().get("data").getAsJsonArray();
		//第二年数据
		JsonArray dataList2 = rows.get("ratejson2").getAsJsonObject().get("data").getAsJsonArray();
		//当期数据
		JsonArray dataList1 = rows.get("json1").getAsJsonObject().get("data").getAsJsonArray();

		//盈利能力
		//主营业务利润率
		String zyywRatio1= dataList3.get(6).getAsString();
		String zyywRatio2= dataList2.get(6).getAsString();
		String zyywRatio3= dataList1.get(6).getAsString();
		FinaRatioDto zyyw = new FinaRatioDto("主营业务利润率" , zyywRatio1, zyywRatio2, zyywRatio3, Constants.FINA_RATIO_TYPE.PROFIT);
		//销售利润率
		String xsRatio1= dataList3.get(7).getAsString();
		String xsRatio2= dataList2.get(7).getAsString();
		String xsRatio3= dataList1.get(7).getAsString();
		FinaRatioDto xs = new FinaRatioDto("销售利润率" , xsRatio1, xsRatio2, xsRatio3, Constants.FINA_RATIO_TYPE.PROFIT);
		//净利润率
		String jlrRatio1= dataList3.get(3).getAsString();
		String jlrRatio2= dataList2.get(3).getAsString();
		String jlrRatio3= dataList1.get(3).getAsString();
		FinaRatioDto jlr = new FinaRatioDto("净利润率" , jlrRatio1, jlrRatio2, jlrRatio3, Constants.FINA_RATIO_TYPE.PROFIT);
		data.add(zyyw);
		data.add(xs);
		data.add(jlr);


		//负债能力
		//流动比率
		String ldRatio1= dataList3.get(17).getAsString();
		String ldRatio2= dataList2.get(17).getAsString();
		String ldRatio3= dataList1.get(17).getAsString();
		FinaRatioDto ldbl = new FinaRatioDto("流动比率" , ldRatio1, ldRatio2, ldRatio3, Constants.FINA_RATIO_TYPE.DEBT);
		//速动比率
		String sdRatio1= dataList3.get(18).getAsString();
		String sdRatio2= dataList2.get(18).getAsString();
		String sdRatio3= dataList1.get(18).getAsString();
		FinaRatioDto sdbl = new FinaRatioDto("速动比率" , sdRatio1, sdRatio2, sdRatio3, Constants.FINA_RATIO_TYPE.DEBT);
		//资产负债率
		String zcfzRatio1= dataList3.get(30).getAsString();
		String zcfzRatio2= dataList2.get(30).getAsString();
		String zcfzRatio3= dataList1.get(30).getAsString();
		FinaRatioDto zcfz = new FinaRatioDto("资产负债率" , zcfzRatio1, zcfzRatio2, zcfzRatio3, Constants.FINA_RATIO_TYPE.DEBT);
		//利息保障倍数
		String lxbzRatio1= dataList3.get(32).getAsString();
		String lxbzRatio2= dataList2.get(32).getAsString();
		String lxbzRatio3= dataList1.get(32).getAsString();
		FinaRatioDto lxbz = new FinaRatioDto("利息保障倍数" , lxbzRatio1, lxbzRatio2, lxbzRatio3, Constants.FINA_RATIO_TYPE.DEBT);
		data.add(ldbl);
		data.add(sdbl);
		data.add(zcfz);
		data.add(lxbz);

		//运营能力
		//总资产周转率
		String zzcRatio1= dataList3.get(10).getAsString();
		String zzcRatio2= dataList2.get(10).getAsString();
		String zzcRatio3= dataList1.get(10).getAsString();
		FinaRatioDto zzc = new FinaRatioDto("总资产周转率" , zzcRatio1, zzcRatio2, zzcRatio3, Constants.FINA_RATIO_TYPE.OPERATE);
		//应收账款周转率
		String yszkRatio1= dataList3.get(12).getAsString();
		String yszkRatio2= dataList2.get(12).getAsString();
		String yszkRatio3= dataList1.get(12).getAsString();
		FinaRatioDto yszk = new FinaRatioDto("应收账款周转率" , yszkRatio1, yszkRatio2, yszkRatio3, Constants.FINA_RATIO_TYPE.OPERATE);
		//存货周转率
		String chRatio1= dataList3.get(14).getAsString();
		String chRatio2= dataList2.get(14).getAsString();
		String chRatio3= dataList1.get(14).getAsString();
		FinaRatioDto ch = new FinaRatioDto("存货周转率" , chRatio1, chRatio2, chRatio3, Constants.FINA_RATIO_TYPE.OPERATE);
		data.add(zzc);
		data.add(yszk);
		data.add(ch);


		//发展能力
		//净利润增长率
		String jlrUp1= dataList3.get(23).getAsString();
		String jlrUp2= dataList2.get(23).getAsString();
		String jlrUp3= dataList1.get(23).getAsString();
		FinaRatioDto jlrUp = new FinaRatioDto("净利润增长率" , zzcRatio1, zzcRatio2, zzcRatio3, Constants.FINA_RATIO_TYPE.DEVELOP);
		//总资产增长率
		String zzcUp1= dataList3.get(25).getAsString();
		String zzcUp2= dataList2.get(25).getAsString();
		String zzcUp3= dataList1.get(25).getAsString();
		FinaRatioDto zzcUp = new FinaRatioDto("总资产增长率" , zzcUp1, zzcUp2, zzcUp3, Constants.FINA_RATIO_TYPE.DEVELOP);
		//营业收入增长率
		String yysrUp1= dataList3.get(28).getAsString();
		String yysrUp2= dataList2.get(28).getAsString();
		String yysrUp3= dataList1.get(28).getAsString();
		FinaRatioDto yysr = new FinaRatioDto("营业收入增长率" , yysrUp1, yysrUp2, yysrUp3, Constants.FINA_RATIO_TYPE.DEVELOP);
		data.add(jlrUp);
		data.add(zzcUp);
		data.add(yysr);

		return data;
	}

	@Override
	public QuotaFullDto getCommendQuota(String serNo) throws Exception {
		String quotaData = this.getQuotaData(serNo, Constants.Platform.LOCAL_FINRISK);
		if(StringUtil.isEmptyString(quotaData)){
			return null;
		}
		QuotaFullDto quotaFullDto = GsonUtil.GsonToBean(quotaData, QuotaFullDto.class);

		return quotaFullDto;
	}

	@Override
	public List<FinaRatioDto> getAssessCardNeedRatio(String serNo) throws Exception {
		List<FinaRatioDto> data = new ArrayList<>();
		String resultStr = this.getFinriskData(serNo, Constants.Platform.LOCAL_FINRISK);
		//获得解析器
		JsonParser parser = new JsonParser();
		//获取根节点元素
		JsonElement element = parser.parse(resultStr);
		JsonObject rows = element.getAsJsonObject().get("rows").getAsJsonObject().get("detailReport").getAsJsonObject();

		//第一年数据
//		JsonArray dataList3 = rows.get("ratejson3").getAsJsonObject().get("data").getAsJsonArray();
		//第二年数据
//		JsonArray dataList2 = rows.get("ratejson2").getAsJsonObject().get("data").getAsJsonArray();
		//当期数据
		JsonArray dataList = rows.get("json1").getAsJsonObject().get("data").getAsJsonArray();

		FinaRatioDto llrzz = new FinaRatioDto("净利润增长率" , dataList.get(0).getAsString());
		FinaRatioDto chzz = new FinaRatioDto("存货周转率" , dataList.get(14).getAsString());
		FinaRatioDto yszkzz = new FinaRatioDto("应收账款周转率" , dataList.get(12).getAsString());
		FinaRatioDto ldbl = new FinaRatioDto("流动比率" , dataList.get(17).getAsString());
		FinaRatioDto fzbl = new FinaRatioDto("负债比率" , dataList.get(23).getAsString());
		FinaRatioDto sdbl = new FinaRatioDto("速动比率" , dataList.get(18).getAsString());
		FinaRatioDto xslrl = new FinaRatioDto("销售利润率" , dataList.get(7).getAsString());
		FinaRatioDto xssrzzl = new FinaRatioDto("销售收入增长率" , dataList.get(41).getAsString());

		//还有一部分涉及财务指标未获取到
		//净资产利润率
		//净资产增长率
		//实有净资产
		//连带负债
		//长期资产(占资产%)
		//非筹资性现金流入与流动负债比率

		data.add(llrzz);
		data.add(chzz);
		data.add(yszkzz);
		data.add(ldbl);
		data.add(fzbl);
		data.add(sdbl);
		data.add(xslrl);
		data.add(xssrzzl);

		return data;
	}

	@Override
	public List<WarnItemDto> getFinaWarn(String serNo) throws Exception {
		return this.getFinaWarn(serNo, 60.0d);
	}

	@Override
	public List<WarnItemDto> getFinaWarn(String serNo, Double score) throws Exception {

		String resultStr = this.getFinriskData(serNo, Constants.Platform.LOCAL_FINRISK);
		if (StringUtil.isEmptyString(resultStr)) return null;
		//获得解析器
		JsonParser parser = new JsonParser();
		//获取根节点元素
		JsonElement element = parser.parse(resultStr);
		JsonObject rows = element.getAsJsonObject().get("rows").getAsJsonObject().get("report").getAsJsonObject();
		if (score == null){
			 score = 60.0d;
		}
		double riskScore = rows.get("riskScore").getAsDouble();
		if (riskScore < score) return null;

		List<WarnItemDto> warnItemList = new ArrayList<>();
		JsonArray abnormalWarnList = rows.get("errAbnors").getAsJsonArray();
		for (JsonElement abnormalWarn : abnormalWarnList) {
			JsonObject warn = abnormalWarn.getAsJsonObject();
			int warningLv = warn.get("warningLv").getAsInt();
			if (warningLv != 1) continue; // 异常项 只取预警等级最高的1级

			String abnormalItem = warn.get("name").getAsString();
			String errReason = warn.get("errReason").getAsString();
			String[] list = warn.get("reason").getAsString().split("。");
			List<String> reasonList = new ArrayList<>();
			boolean flag = false;
			for (String str : list) {
				if(StringUtils.isEmpty(str)){
					continue;
				}
				String reason = str.substring(4);
				reasonList.add(reason);
				if (reason.equals("其他")) flag = true;
			}
			if (!flag) {
				reasonList.add(reasonList.size(), "其他");
			}
			warnItemList.add(new WarnItemDto(serNo, abnormalItem, errReason, JacksonUtil.serialize(reasonList)));
		}
		return warnItemList;
	}

	@Override
	public InduInfo getInduAnalysis(String induCode) {
		if(StringUtil.isEmptyString(induCode)){
			log.warn("行业代码为：" + induCode + "行业信息为空,系统管理员需要补充");
			return null;
		}
		InduInfo induInfo = induInfoService.selectSingleByProperty("induCode", induCode);
		if(induInfo==null){
			while(induCode.length() > 2){
				induCode = induCode.substring(0, induCode.length()-1);
				induInfo = induInfoService.selectSingleByProperty("induCode", induCode);
				if(induInfo!=null){
					break;
				}
			}
		}
		//若到二级行业还是没有数据，则通过二级行业  获取一级行业代码，通过一级行业代码（大写英文字母）查询
		if(induInfo==null){
			TreeData treeData = industrySV.getTreeDataByEnname(induCode, SysConstants.TreeDicConstant.GB_IC_4754_2017);
			String abvenName = treeData.getAbvenName();
			induInfo = induInfoService.selectSingleByProperty("induCode", abvenName);
		}
		return induInfo;
		
		/* 之前是从云端获取财务分析 现在存在对公本地
		 * Map<String, String> params = new HashMap<>();
		params.put("induCode", induCode);

		HttpClientUtil instance = HttpClientUtil.getInstance(AppConfig.Cfg.INDU_ANALYSIS);
		String resultTask = instance.doPost(params);

		log.info("行业分析系统返回数据为报文为：");
		log.info(resultTask);

		//获得解析器
		JsonParser parser = new JsonParser();
		//获取根节点元素
		Integer code = parser.parse(resultTask).getAsJsonObject().get("code").getAsInt();
		String msg = parser.parse(resultTask).getAsJsonObject().get("msg").getAsString();
		JsonElement rows = parser.parse(resultTask).getAsJsonObject().get("rows");

		if (code == ResultDto.RESULT_CODE_SUCCESS) {
			if (rows == null) {
				log.error("行业分析云平台获取数据内容为空");
				return map;
			}
			map = GsonUtil.GsonToBean(rows.toString(), InduMapDto.class);
		} else {
			log.error("行业分析云平台获取数据失败，异常原因：" + msg);
		}*/
	}
}
