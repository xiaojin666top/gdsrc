package com.beawan.customer.service.impl;

import java.util.*;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.beawan.base.entity.SReportData;
import com.beawan.base.entity.SReportModelRow;
import com.beawan.base.service.ISReportModelSV;
import com.beawan.common.BusinessException;
import com.beawan.common.Constants;
import com.beawan.common.SysConstants;
import com.beawan.common.util.SystemUtil;
import com.beawan.customer.service.ICusFSToolSV;
import com.beawan.customer.utils.CustomerUtil;
import com.beawan.exterinvoke.util.FncStatMapUtil;
import com.platform.util.ExceptionUtil;
import com.platform.util.JdbcUtil;
import com.platform.util.StringUtil;

/**
 * TODO 客户财务报表工具相关接口实现
 */
@Service("cusFSToolSV")
public class CusFSToolSVImpl implements ICusFSToolSV {
	
	@Resource
	private ISReportModelSV sReportModelSV;
	
	@Override
	public void checkCusInfoRule(String cusNo, int finaRepYear, int finaRepMonth) throws Exception {

		try{
			
			this.checkComFinaRule(cusNo, finaRepYear, finaRepMonth);
			
		}catch(BusinessException be){
			
			if("CUS_FS".equals(be.getFlag()))
				CustomerUtil.asynCusFSFromCmis(cusNo, finaRepYear, finaRepMonth);
			
			throw be;
		}
	}
	
	@Override
	public void checkComFinaRule(String customerNo, int finaRepYear, int finaRepMonth)throws Exception{
		
		//查询原始财报数据
		List<Map<String, Double>> balanceData = this.findRepDataByType(customerNo, finaRepYear,
				finaRepMonth, SysConstants.CmisFianRepType.BALANCE);
		List<Map<String, Double>> incomeData = this.findRepDataByType(customerNo, finaRepYear,
				finaRepMonth, SysConstants.CmisFianRepType.INCOME);
		
		boolean flag = false;// 判断符合规则的标记，默认为true
		StringBuffer msg = new StringBuffer();

		int currYear = Integer.valueOf(finaRepYear).intValue();
		String[] yearMonthArr = {(currYear-2) + "-12",(currYear-1) + "-12", currYear + "-" + (finaRepMonth<10?"0"+finaRepMonth:""+finaRepMonth)};
		for (int i = 1; i < 4; i++) {
			//通过“资产合计”科目判断财报信息是否已填写
			if (balanceData.get(i).get("165") == null 
					|| balanceData.get(i).get("165") == 0.0) {
				msg.append(yearMonthArr[i-1] + "资产负债表信息不全！\n");
			}
			
			//通过“主营业务收入”科目判断财报信息是否已填写
			if (incomeData.get(i).get("301") == null 
					|| incomeData.get(i).get("301") == 0.0) {
				msg.append(yearMonthArr[i-1] + "利润表信息不全！\n");
			}
		}
		
		if(StringUtil.isEmpty(msg))
			flag = true;
		
		if (!flag) {
			msg.append("请先录入必要的客户信息！");
			ExceptionUtil.throwException(msg.toString(), "CUS_FS");
		}
	}
	
	@Override
	public List<SReportModelRow> queryReportModel(String modelNo)throws Exception {
		return sReportModelSV.queryModelRowByNo(modelNo);
	}
	
	@Override
	public List<Map<String, Double>> queryReportData(String cusId, 
			int repYear, int repMonth, String modelNo)throws Exception {
		List<String> dateList = SystemUtil.genFinaDateList(repYear, repMonth, true, false);
		return this.queryReportData(cusId, dateList, modelNo);
	}

	@Override
	public List<Map<String, Double>> queryReportData(String cusId, 
			List<String> dateList, String modelNo)throws Exception {
		
		String sql = "SELECT RD.ROW_SUBJECT,RD.COL1_VALUE,RD.COL2_VALUE,RD.ROW_DIM_TYPE"
				   + " FROM GDTCESYS.CUS_REPORT_RECORD RR"
				   + " LEFT JOIN GDTCESYS.CUS_FSRECORD FS ON FS.RECORD_NO = RR.OBJECT_NO"
				   + " LEFT JOIN GDTCESYS.CUS_REPORT_DATA RD ON RD.REPORT_NO = RR.REPORT_NO"
				   + " WHERE FS.STATUS='0' AND FS.CUSTOMER_ID = ? AND FS.REPORT_DATE = ? AND RR.MODEL_NO = ?"
				   + " ORDER BY RD.DISPLAY_ORDER ASC";
		
		String[] params = new String[]{cusId, "", modelNo};
		
		List<Map<String, Double>> result = new ArrayList<Map<String, Double>>();
		
		for(int i=0;i<dateList.size();i++){
			Map<String, Double> data = new HashMap<String, Double>();
			params[1] = dateList.get(i);

			List<SReportData> list = JdbcUtil.executeQuery(sql,
					Constants.DataSource.TCE_DS, params, SReportData.class);
			if(!CollectionUtils.isEmpty(list)){
				for(SReportData rd : list){
					
					data.put(rd.getRowSubject(), 
							getColValue(rd.getCol2Value(), rd.getRowDimType()));
				}
			}
			
			result.add(data);
		}
		
		//如果只有4期数据（当期为12月时），则默认为第三年数据为上年同期
//		if(result.size() == 4)
//			result.add(result.get(2));
		
		return result;
	}
	
	private double getColValue(Double value, String dimType){
		
		double result = 0.0;
		if(value != null){
			//金额（元为单位），转为万元
			if(StringUtil.isEmpty(dimType) || "1".equals(dimType))
				result = value/10000;
			//比例，乘以100
			else if("2".equals(dimType))
				result = value*100;
			else
				result = value;
		}
		
		return result;
	}
	
	@Override
	public List<SReportModelRow> findRepModelByType(String cusId,
			String reportType) throws Exception {

		return findRepModelByType(cusId, reportType, null);
	}

	@Override
	public List<SReportModelRow> findRepModelByType(String cusId, String reportType, String dataType) throws Exception {
		//获取不到，这里需要根据cmis库表查询
		List<SReportModelRow> result = new ArrayList<>();
		//获得模型编号（无）
		//String modelNo = FncStatMapUtil.getModelNo(getFinaRepModelClass(cusId), reportType);
		String modelNo = "";
		//根据模型编号获取所有科目字段
		//List<SReportModelRow> modelRows = this.queryReportModel(modelNo);
		List<SReportModelRow> modelRows = null;

		//判断是否只需要重要科目字段
		if(dataType!=null && Constants.FINA_MODEL_TYPE.IMPORT_ROWS.equals(dataType)){
			Map<String, String> importRows = null;
			if (getFinaRepModelClass(cusId)!=null && reportType !=null){
				 importRows = FncStatMapUtil.getImportMap(getFinaRepModelClass(cusId), reportType);
			}
			if(modelRows!=null && modelRows.size()>0){
				for(SReportModelRow modelRow : modelRows){
					for(Map.Entry<String, String> entry : importRows.entrySet()){
						String key = entry.getKey();
						String value = entry.getValue();
						if(modelRow.getRowSubject().equals(key)){
							modelRow.setRowName(value);
							result.add(modelRow);
							break;
						}
					}
				}
			}

		}else{
			result = modelRows;
		}
		return result;
	}

	@Override
	public List<Map<String, Double>> findRepDataByType(String cusId, 
			int repYear, int repMonth, String reportType) throws Exception {
		
		String modelClass = getFinaRepModelClass(cusId);
		//获得模型编号
		String modelNo = FncStatMapUtil.getModelNo(modelClass, reportType);
		
		List<Map<String, Double>> reportDataList = this.queryReportData(cusId,
				repYear, repMonth, modelNo);
		
//		this.newAccSubToOld(modelClass, reportType, reportDataList);
		
		return reportDataList;
	}
	
	@Override
	public List<Map<String, Double>> findRepDataByType(String cusId, 
			List<String> dateList, String reportType) throws Exception {
		
		String modelClass = this.getFinaRepModelClass(cusId);
		//获得模型编号
		List<Map<String, Double>> reportDataList = null;
		if(getFinaRepModelClass(cusId)!=null && reportType !=null) {
			String modelNo = FncStatMapUtil.getModelNo(getFinaRepModelClass(cusId),
					reportType);

			reportDataList = this.queryReportData(cusId, dateList, modelNo);
		}
//		this.newAccSubToOld(modelClass, reportType, reportDataList);
		
		return reportDataList;
	}
	
	/**
	 * TODO 对新会计准则部分科目进行转换
	 * @param modelClass
	 * @param reportType
	 * @return
	 */
	private void newAccSubToOld(String modelClass, String reportType,
			List<Map<String, Double>> reportDataList) {
		
		//如果不是是新会计准则科目
		if(!SysConstants.CmisFianRepModClass.NEW_ACC.equals(modelClass))
			return;
		
		//资产负债表
		if(SysConstants.CmisFianRepType.BALANCE.equals(reportType)){
			
			for(Map<String, Double> reportData : reportDataList){
				
				if(reportData == null || reportData.size() == 0)
					continue;
				
				//一年内到期的非流动资产
				double value119 = reportData.get("119");
				if(value119 == 0)
					reportData.put("119", -9999.0);
				
				//固定资产
				double value137 = reportData.get("137");
				//在建工程
				double value139 = reportData.get("139");
				//工程物资
				double value141 = reportData.get("141");
				//固定资产清理
				double value143 = reportData.get("143");
				
				//旧会计准则，固定资产原价
				reportData.put("142", value137);
				//旧会计准则，固定资产净值
				reportData.put("146", value137);
				//旧会计准则，固定资产净额
				reportData.put("150", value137);
				//旧会计准则，固定资产合计
				reportData.put("154", value137 + value139 + value141 + value143);
			}
			
		}else if(SysConstants.CmisFianRepType.INCOME.equals(reportType)){
			
			for(Map<String, Double> reportData : reportDataList){
				
				if(reportData == null || reportData.size() == 0)
					continue;
				
				//营业收入
				double value301 = reportData.get("301");
				//营业成本
				double value303 = reportData.get("303");
				//营业税金及附加
				double value305 = reportData.get("305");
				
				//旧会计准则，主营业务利润
				reportData.put("308", value301 - value303 - value305);
			}
		}
	}
	
	@Override
	public String getFinaRepModelClass(String cusId) throws Exception {
		
		String sql = "SELECT FINANCE_BELONG FROM CM_BASE WHERE CUSTOMER_NO = '" + cusId + "'";
		String financeBelong = JdbcUtil.queryForString(Constants.DataSource.TCE_DS, sql, null);

		//CMIS表是银行给的，
/*		if(StringUtil.isEmptyString(financeBelong)){
			//sql = "SELECT FINANCEBELONG FROM CMIS.ENT_INFO WHERE CUSTOMERID = '" + cusId + "'";
			sql = "SELECT FINANCEBELONG FROM CMIS.ENT_INFO WHERE CUSTOMERID = '" + cusId + "'";
			financeBelong = JdbcUtil.queryForString(Constants.DataSource.TCE_DS, sql, null);
		}*/
		
		/*if(StringUtil.isEmptyString(financeBelong))
			ExceptionUtil.throwException("客户财报新旧会计准则尚未确定！");*/
		
		return financeBelong;
	}
	
}
