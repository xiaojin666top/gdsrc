package com.beawan.customer.service.impl;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.nutz.json.Json;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.customer.bean.AdmitRecordEntity;
import com.beawan.customer.service.AdmitRecordService;
import com.beawan.model.entity.AdjustItemEntity;
import com.beawan.model.entity.AdmitEntity;
import com.beawan.model.entity.AdmitQuotaAttrEntity;
import com.beawan.model.entity.AdmitQuotaEntity;
import com.beawan.model.service.AdmitService;

import net.sf.json.JSONObject;

@Service("admitRecordService")
public class AdmitRecordServiceImpl extends BaseServiceImpl<AdmitRecordEntity> implements AdmitRecordService{
	
	@Resource
	private AdmitService admitService;
	@Resource
	private AdmitRecordService admitRecordService;
	
	/**
    * 注入DAO
    */
	@Resource(name = "admitRecordDao")
    public void setDao(BaseDao<AdmitRecordEntity> dao) {
    	super.setDao(dao);
    }
    
    //默认按照
    public Map<String, Map<String, Object>> getQuotaResultString(HttpServletRequest request) throws IOException{
    	Map<String, Object> quotaDataMap = new HashMap();
    	Map<String, Object> quotaDescribeMap = new HashMap();
    
    	
    	//获取模拟数据
    	String path = request.getSession().getServletContext()
    			.getRealPath("/statics/tempFile/simpleData.json");
    	
    	File jsonFile = new File(path);
    	if (!jsonFile.exists()) {
    		jsonFile.createNewFile();
    	   }
    	String dataStr = FileUtils.readFileToString(jsonFile, "UTF-8");
    	
    	JSONObject data = JSONObject.fromObject(dataStr);
    	int GSCLNX = (int)data.get("公司成立年限");
    	double GGBHL = (double)data.get("高管变化率");
    	String lastFDDBR = (String)data.get("上一年法人");
    	String currentFDDBR = (String)data.get("当前法人");
    	String GQZGFBH = (String)data.get("股权最高方近一年出现在新增名单中");
    	int JWNJDJF = (int)data.get("近五年内发生借贷纠纷");
    	int JYNSXCS = (int)data.get("近1年失信被执行人次数");
    	int ZBHJKJE = (int)data.get("在本行贷款金额");
    	int ZBHBLCS = (int)data.get("在本行不良记录");
    	String CLKHYJ = (String)data.get("存量客户，过去一年内预警情况");
    	int SNNYLNS = (int)data.get("三年内赢利年限");
    	int GLQYDBSL = (int)data.get("关联企业相互担保数量");
    	int JYHDXJLL = (int)data.get("经营活动现金净流量");
    	int DWDBSL = (int)data.get("对外担保企业数量");
    	int DWDBJE = (int)data.get("对外担保贷款金额");
    	int DWDBYQJE = (int)data.get("对外担保逾期金额");
    	int ZCLDK = (int)data.get("正常类贷款");
    	int BLLDK = (int)data.get("不良类贷款");
    	int RZYE = (int)data.get("融资余额");
    	double HKNL = (double)data.get("偿债比率");
    	int YSZKTSZJ = (int)data.get("应收账款天数增加");
    	double XSSRZZY = (double)data.get("销售收入增长情况");
    	double CWGGLZZY = (double)data.get("财务杠杆率");
    	double GXFZBLZZY = (double)data.get("刚性负债杠杆比率");
    	int currentYear = (int)data.get("currentYear");
    	int currentIncome = (int)data.get("currentIncome");
    	int lastYear = (int)data.get("lastYear");
    	int lastIncome = (int)data.get("lastIncome");
    	int firstYear = (int)data.get("firstYear");
    	int firstIncome = (int)data.get("firstIncome");
    	String XZZDGDXM = (String)data.get("新增最大股东姓名");
    	double XZZDGDZB = (double)data.get("新增最大股东占比");
    	
    	JSONObject current = data.getJSONObject("current");
    	JSONObject last = data.getJSONObject("last");
    	JSONObject first = data.getJSONObject("first");
    	
    	Map<String, Integer> currentReportData = (HashMap<String, Integer>) current.toBean(current, HashMap.class); 
    	Map<String, Integer> lastReportData = (HashMap<String, Integer>) last.toBean(last, HashMap.class);
    	Map<String, Integer> firstReportData = (HashMap<String, Integer>) first.toBean(first, HashMap.class);
    	
    	//公司成立年限
    	quotaDataMap.put("GSCLNX", GSCLNX);
    	quotaDescribeMap.put("GSCLNX", "公司成立年数为" + GSCLNX + "年");
    	//高管变化率
    	quotaDataMap.put("GGBHL", GGBHL);
    	quotaDescribeMap.put("GGBHL", "高管变化率为" + GGBHL*100 + "%");
    	//法定代表人近一年内出现变更
    	if(lastFDDBR.equals(currentFDDBR)){
    		quotaDataMap.put("FDDBRBH", "K");
    		quotaDescribeMap.put("FDDBRBH", "法定代表人未变更");
    	}else{
    		quotaDataMap.put("FDDBRBH", "Z");
	    	quotaDescribeMap.put("FDDBRBH", "法定代表人由" + lastFDDBR + "变更为" + currentFDDBR);	
    	}
    	//股权最高方近一年出现在新增名单中
    	if(XZZDGDXM == null || "".equals(XZZDGDXM)){
    		quotaDataMap.put("GQZGFBH", "K");
    		quotaDescribeMap.put("GQZGFBH", "新增名单中未出现股权最高方");
    	}else{
    		quotaDataMap.put("GQZGFBH", "Z");
    		quotaDescribeMap.put("GQZGFBH", XZZDGDXM + "占股权 " + XZZDGDZB*100 + " %，近一年出现在股权新增名单中");
    	}
    	//近五年内发生借贷纠纷
    	quotaDataMap.put("JWNJDJF", JWNJDJF);
       	quotaDescribeMap.put("JWNJDJF", "近五年内发生了" + JWNJDJF + "次借贷纠纷");
       	//近1年失信被执行人次数
       	quotaDataMap.put("JYNSXCS", JYNSXCS);
       	quotaDescribeMap.put("JYNSXCS", "近一年失信被执行人有" + JYNSXCS + "人次");
       	//在本行贷款和结算记录
       	if(ZBHJKJE > 0){
       		if(ZBHBLCS > 0){
       			quotaDataMap.put("DKJSJL", "B");
       			quotaDescribeMap.put("DKJSJL", "在本行贷款" + ZBHJKJE +"万元， 不良记录次数为" + ZBHBLCS + "次");
       		}else{
       			quotaDataMap.put("DKJSJL", "K");
       			quotaDescribeMap.put("DKJSJL", "在本行贷款" + ZBHJKJE +"万元， 无不良记录");
       		}
       	}else{
       		quotaDataMap.put("DKJSJL", "Z");
       		quotaDescribeMap.put("DKJSJL", "未在本行有过借款");
       	}
       	//存量客户，过去一年内预警情况
       	quotaDataMap.put("CLKHYJQK", CLKHYJ);
       	
       	//企业征信记录
       	if(ZCLDK > 0){
       		if( BLLDK > 0){
       			quotaDataMap.put("QYZXJL", "B");
       			quotaDescribeMap.put("QYZXJL", "当前负债余额"+ (ZCLDK+BLLDK) + "万元，其中不良或违约类贷款" + BLLDK + "万元");
       		}else{
       			quotaDataMap.put("QYZXJL", "K");
       			quotaDescribeMap.put("QYZXJL", "当前负债余额"+ (ZCLDK+BLLDK) + "万元，无不良或违约类贷款");
       		}
       	}else{
       		quotaDataMap.put("QYZXJL", "Z");
       		quotaDescribeMap.put("QYZXJL", "无贷款记录");
       	}
       	//对外担保贷款情况
       	if( DWDBJE > 0){
       		if(DWDBYQJE > 0){
       			quotaDataMap.put("DWDBQK", "B");
       			quotaDescribeMap.put("DWDBQK", "企业对外担保企业数量有" + DWDBSL + "家，对外担保贷款" + DWDBJE + "万元， 其中逾期金额" + DWDBYQJE + "万元");
       		}else{
       			quotaDataMap.put("DWDBQK", "Z");
       			quotaDescribeMap.put("DWDBQK", "企业对外担保企业数量有" + DWDBSL + "家，对外担保贷款" + DWDBJE + "万元，未产生逾期金额");
       		}
       		
       	}else{
       		quotaDataMap.put("DWDBQK", "K");
       		quotaDescribeMap.put("DWDBQK", "企业无对外担保贷款");
       	}
       	//关联企业相互担保
       	if(GLQYDBSL > 0){
       		quotaDataMap.put("GLQYDB", "B");
       		quotaDescribeMap.put("GLQYDB", "关联企业相互担保数量为"+ GLQYDBSL);
       	}else{
       		quotaDataMap.put("GLQYDB", "K");
       		quotaDescribeMap.put("GLQYDB", "无关联企业相互担保情况");
       	}
       	//还款能力，偿债保障比率
       	quotaDataMap.put("HKNL", HKNL);
       	quotaDescribeMap.put("HKNL", "当期偿债比率为"+ HKNL + "倍");
       	//三年内赢利年限
       	int count = 3;
       	StringBuilder YLNXDesc = new StringBuilder();
       	if(currentIncome < 0){
       		count --;
       		YLNXDesc.append(currentYear + "年、");
       	}
       	if(lastIncome < 0){
       		count --;
       		YLNXDesc.append(lastYear + "年、");
       	}
       	if(firstIncome < 0){
       		count --;
       		YLNXDesc.append(firstYear + "年、");
       	}
       	if(count >= 0){
       		YLNXDesc.deleteCharAt(YLNXDesc.lastIndexOf("、"));
       		YLNXDesc.append("营业利润为负");
       	}else if(count == 3){
       		YLNXDesc.append("近三年均赢利");
       	}
       	
       	quotaDataMap.put("YLNX", count);
       	quotaDescribeMap.put("YLNX", YLNXDesc.toString());
       	//应收账款周转天数较年初增加
       	quotaDataMap.put("YSZKFX", YSZKTSZJ);
       	quotaDescribeMap.put("YSZKFX", "应收账款天数较年初增加" + YSZKTSZJ + "天");
       	//销售收入增长情况
       	quotaDataMap.put("XSSRZZY", XSSRZZY);
       	if(XSSRZZY > 0){
       		quotaDescribeMap.put("XSSRZZY", "销售收入相比上一年增长" + XSSRZZY*100 + "%");
       	}else{
       		quotaDescribeMap.put("XSSRZZY", "销售收入相比上一年减少" + XSSRZZY*-1*100 + "%");
       	}
       	
       	//财务杠杆率
       	quotaDataMap.put("CWGGLZZY", GXFZBLZZY);
       	quotaDescribeMap.put("GXFZBLZZY", "财务杠杆率为" + CWGGLZZY + "倍");
       	//刚性负债杠杆比率
       	quotaDataMap.put("GXFZBLZZY", GXFZBLZZY);
       	quotaDescribeMap.put("GXFZBLZZY", "刚性负债杠杆率" + GXFZBLZZY + "倍");
       	
       	Map<String, Map<String, Object>> resultMap = new HashMap();
       	resultMap.put("quotaDataMap", quotaDataMap);
       	resultMap.put("quotaDescribeMap", quotaDescribeMap);
       	
    	return resultMap;
    }

	@Override
	public String getAdmitResult(String customerNo, HttpServletRequest request) throws IOException {
		// TODO Auto-generated method stub
		
		Map<String, Map<String, Object>> resultMap = getQuotaResultString(request);
		
		AdmitRecordEntity admitRecord = admitRecordService.selectSingleByProperty("customerNo", customerNo);
		
		if(admitRecord == null){
			admitRecord = new AdmitRecordEntity();
			admitRecord.setCustomerNo(customerNo);
		}
		
		Map<String, Object> quotaResultMap = resultMap.get("quotaDataMap");
		Map<String, Object> quotaDescribeMap = resultMap.get("quotaDescribeMap");
		
		admitRecord.setQuotaData(Json.toJson(quotaResultMap));
		
		
		//默认碧湾准入模型
		String identifCode = "ZRMXCS";
		
		AdmitEntity admitModel = admitService.queryByIdentifCode(identifCode);
		
		Map<String, String> admitResultMap = new HashMap();
		List<AdmitQuotaEntity> quotaList =  admitModel.getQuotaList();
		for (AdmitQuotaEntity parentQuota : quotaList) {
			List<AdmitQuotaEntity> childQuotaList = parentQuota.getSubQuotaDtos();
			for (AdmitQuotaEntity childQuota : childQuotaList) {
				String quotaCode = childQuota.getQuotaCode();
				List<AdmitQuotaAttrEntity> attrList = childQuota.getOptions();
				if(childQuota.getValType().equals("S")){
					admitResultMap.put(childQuota.getQuotaCode(), (String)quotaResultMap.get(quotaCode));
					for (AdmitQuotaAttrEntity attr : attrList) {
						if(quotaDescribeMap.get(quotaCode) == null){
							if(((String)quotaResultMap.get(quotaCode)).equals(attr.getOptionCode())){							
								quotaDescribeMap.put(quotaCode, attr.getContent());
								break;
							}
						}
					}
					
				}else if(childQuota.getValType().equals("V")){
					if(quotaResultMap.get(quotaCode) != null){
						
						double quotaValue = Double.valueOf(String.valueOf(quotaResultMap.get(quotaCode)));
						
						for (AdmitQuotaAttrEntity attr : attrList) {
							if(attr.getRegMould().equals("L")){
								if(quotaValue >= attr.getLowerReg().doubleValue() && quotaValue < attr.getUpperReg().doubleValue()){
									admitResultMap.put(quotaCode, attr.getOptionCode());
									if(quotaDescribeMap.get(quotaCode) == null){
										quotaDescribeMap.put(quotaCode, attr.getContent());
									}
									break;
								}
							}else if(attr.getRegMould().equals("R")){
								if(quotaValue > attr.getLowerReg().doubleValue() && quotaValue <= attr.getUpperReg().doubleValue()){
									admitResultMap.put(childQuota.getQuotaCode(), attr.getOptionCode());
									if(quotaDescribeMap.get(quotaCode) == null){
										quotaDescribeMap.put(quotaCode, attr.getContent());
									}
									break;
								}
							}else if(attr.getRegMould().equals("A")){
								if(quotaValue >= attr.getLowerReg().doubleValue() && quotaValue <= attr.getUpperReg().doubleValue()){
									admitResultMap.put(childQuota.getQuotaCode(), attr.getOptionCode());
									if(quotaDescribeMap.get(quotaCode) == null){
										quotaDescribeMap.put(quotaCode, attr.getContent());
									}
									break;
								}
							}else if(attr.getRegMould().equals("O")){
								if(quotaValue > attr.getLowerReg().doubleValue() && quotaValue < attr.getUpperReg().doubleValue()){
									admitResultMap.put(childQuota.getQuotaCode(), attr.getOptionCode());
									if(quotaDescribeMap.get(quotaCode) == null){
										quotaDescribeMap.put(quotaCode, attr.getContent());
									}
									break;
								}
							}
						}
					}
					
				}
			}
		}
		
		admitRecord.setAdmitResult("K");
		for (String key : admitResultMap.keySet()) {
			if(admitResultMap.get(key).equals("Z")){
				admitRecord.setAdmitResult("Z");
				break;
			}
		}
		for (String key : admitResultMap.keySet()){
			if(admitResultMap.get(key).equals("B")){
				admitRecord.setAdmitResult("B");
				break;
			}
		}
		
		admitRecord.setQuotaDescribe(Json.toJson(quotaDescribeMap));
		admitRecord.setQuotaResult(Json.toJson(admitResultMap));
		
		admitRecordService.saveOrUpdate(admitRecord);
		return admitRecord.getAdmitResult();
	}

}
