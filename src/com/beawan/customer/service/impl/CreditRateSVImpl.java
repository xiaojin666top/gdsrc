package com.beawan.customer.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.base.entity.SReportData;
import com.beawan.base.service.ISReportDataSV;
import com.beawan.common.SysConstants;
import com.beawan.customer.bean.CreditRating;
import com.beawan.customer.bean.CusFSRecord;
import com.beawan.customer.dao.ICreditRateDAO;
import com.beawan.customer.service.ICreditRateSV;
import com.beawan.customer.service.ICusFSRecordSV;
import com.beawan.customer.utils.RatingCountUtil;
import com.beawan.exterinvoke.util.FncStatMapUtil;
import com.beawan.survey.custInfo.service.ICompBaseSV;

@Service("creditRateSV")
public class CreditRateSVImpl implements ICreditRateSV{
	
	@Resource
	private  ICreditRateDAO  creditRateDAO;
	
	@Resource
	private ICusFSRecordSV cusFSRecordSV;
	
	@Resource
	private ISReportDataSV sReportDataSV;
	@Resource
	private ICompBaseSV compBaseSV;
	
	@Override
	public List<CreditRating> queryCredit(String customerNo,String reportDate) throws Exception {
			
		return creditRateDAO.queryCreditRateListByCustomerNo(customerNo,reportDate);
		
	}

	@Override
	public void saveCreditRateList(String customerNo, String reportDate,List<CreditRating> newlist) throws Exception {
			
		//先删除
		List<CreditRating> list=creditRateDAO.queryCreditRateListByCustomerNo(customerNo,reportDate);
		for(CreditRating credit:list) {
			creditRateDAO.deleteEntity(credit);
		}
		
		//后插入修改的
		for(CreditRating data:newlist) {
			creditRateDAO.saveOrUpdate(data);
		}
		
	}

	@Override
	public List<CreditRating> createCreditRating(String customerNo,String reportDate) throws Exception {
		
		
		
		
		List<CreditRating> list=new ArrayList<CreditRating>();
		
		list.add(new CreditRating(customerNo,"111","法定代表人信誉和还款意愿",1.0,
				(Double)RatingCountUtil.countScore("111", 1.0)[0],RatingCountUtil.countScore("111", 1.0)[1]+"",reportDate));
		list.add(new CreditRating(customerNo,"112","法定代表人从事本行业年限",5.0,
				(Double)RatingCountUtil.countScore("112", 5.0)[0],RatingCountUtil.countScore("112", 5.0)[1]+"",reportDate));
		list.add(new CreditRating(customerNo,"121","企业经营管理水平",1.0,
				(Double)RatingCountUtil.countScore("121", 1.0)[0],RatingCountUtil.countScore("121", 1.0)[1]+"",reportDate));
		list.add(new CreditRating(customerNo,"122","商誉",1.0,
				(Double)RatingCountUtil.countScore("122", 1.0)[0],RatingCountUtil.countScore("122", 1.0)[1]+"",reportDate));
		list.add(new CreditRating(customerNo,"131","市场开发能力",1.0,
				(Double)RatingCountUtil.countScore("131", 1.0)[0],RatingCountUtil.countScore("131", 1.0)[1]+"",reportDate));
		list.add(new CreditRating(customerNo,"132","装备更新情况",50.0,
				(Double)RatingCountUtil.countScore("132", 50.0)[0],RatingCountUtil.countScore("132", 50.0)[1]+"",reportDate));
		list.add(new CreditRating(customerNo,"211","到期贷款偿还率",2.0,
				(Double)RatingCountUtil.countScore("211", 2.0)[0],RatingCountUtil.countScore("211", 2.0)[1]+"",reportDate));
		list.add(new CreditRating(customerNo,"212","贷款利息偿还记录",2.0,
				(Double)RatingCountUtil.countScore("212", 2.0)[0],RatingCountUtil.countScore("212", 2.0)[1]+"",reportDate));
		list.add(new CreditRating(customerNo,"511","同业竞争力与市场前景",1.0,
				(Double)RatingCountUtil.countScore("511", 1.0)[0],RatingCountUtil.countScore("511", 1.0)[1]+"",reportDate));
		list.add(new CreditRating(customerNo,"611","连带负债",0.0,
				(Double)RatingCountUtil.countScore("611", 0.0)[0],RatingCountUtil.countScore("611", 0.0)[1]+"",reportDate));
		
		return list;
	}

	@Override
	public List<CreditRating> sysnFinacing(String customerNo,String reportDate,List<CreditRating> list) throws Exception {

		List<CusFSRecord> cusFSRecords=cusFSRecordSV.queryByCusIdDesc(customerNo);
		
		CusFSRecord cusfs=cusFSRecords.get(0);
		for(CusFSRecord data :cusFSRecords) {
			if(data.getReportDate().equals(reportDate))
				cusfs=data;
		}
			
		String fscode=cusfs.getRecordNo();
		String modeClass=cusfs.getModelClass();

		//取得具体报表数据
		List<SReportData> balance = cusFSRecordSV.getReportData(fscode, FncStatMapUtil.getModelNo(modeClass, SysConstants.CmisFianRepType.BALANCE));
		List<SReportData> ratioIndex=cusFSRecordSV.getReportData(fscode, FncStatMapUtil.getModelNo(modeClass, SysConstants.CmisFianRepType.RATIO_INDEX));
		
		HashMap<String, Double> balanceMap=new HashMap<String, Double>();
		HashMap<String, Double> ratioIndexMap=new HashMap<String, Double>();
		for(SReportData data:balance) {
			balanceMap.put(data.getRowSubject(), data.getCol2Value());			
		}
		for(SReportData data:ratioIndex) {
			ratioIndexMap.put(data.getRowSubject(), data.getCol2Value());
		}
		
		for(int i=0;i<list.size();i++) {
			if(list.get(i).getEnName().equals("611")) {
				list.get(i).setScore((Double)RatingCountUtil.countScore("611",list.get(i).getValue()/balanceMap.get("266")*100)[0]);
			}
		}
		
		
		
		//计算长期资产占比
		double longAsset=0;
		if(modeClass.equals("010")) {
			longAsset=(balanceMap.get("137")+balanceMap.get("139")+balanceMap.get("133"))/balanceMap.get("165")*100;
		}else
		{
			longAsset=(balanceMap.get("146")+balanceMap.get("139")+balanceMap.get("138"))/balanceMap.get("165")*100;
		}
		
		list.add(new CreditRating(customerNo,"311","实有净资产",balanceMap.get("266")/10000,
				(Double)RatingCountUtil.countScore("311", balanceMap.get("266"))[0],RatingCountUtil.countScore("311", balanceMap.get("266"))[1]+"",reportDate));		
		list.add(new CreditRating(customerNo,"312","长期资产(占资产%)",longAsset,
				(Double)RatingCountUtil.countScore("411", longAsset)[0],RatingCountUtil.countScore("411", longAsset)[1]+"",reportDate));
		list.add(new CreditRating(customerNo,"411","流动比率",ratioIndexMap.get("600"),
				(Double)RatingCountUtil.countScore("411", ratioIndexMap.get("600"))[0],RatingCountUtil.countScore("411", ratioIndexMap.get("600"))[1]+"",reportDate));
		list.add(new CreditRating(customerNo,"412","负债比率",ratioIndexMap.get("612"),
				(Double)RatingCountUtil.countScore("412", ratioIndexMap.get("612"))[0],RatingCountUtil.countScore("412", ratioIndexMap.get("612"))[1]+"",reportDate));
		list.add(new CreditRating(customerNo,"413","速动比率",ratioIndexMap.get("602"),
				(Double)RatingCountUtil.countScore("413", ratioIndexMap.get("602"))[0],RatingCountUtil.countScore("413", ratioIndexMap.get("602"))[1]+"",reportDate));
		list.add(new CreditRating(customerNo,"421","销售利润率",ratioIndexMap.get("916"),
				(Double)RatingCountUtil.countScore("421", ratioIndexMap.get("916"))[0],RatingCountUtil.countScore("421", ratioIndexMap.get("916"))[1]+"",reportDate));
		list.add(new CreditRating(customerNo,"422","总资产利润率",ratioIndexMap.get("646"),
				(Double)RatingCountUtil.countScore("422", ratioIndexMap.get("646"))[0],RatingCountUtil.countScore("422", ratioIndexMap.get("646"))[1]+"",reportDate));
		list.add(new CreditRating(customerNo,"431","应收账款周转率",ratioIndexMap.get("650"),
				(Double)RatingCountUtil.countScore("431", ratioIndexMap.get("650"))[0],RatingCountUtil.countScore("431", ratioIndexMap.get("650"))[1]+"",reportDate));
		list.add(new CreditRating(customerNo,"432","存货周转率",ratioIndexMap.get("652"),
				(Double)RatingCountUtil.countScore("432", ratioIndexMap.get("652"))[0],RatingCountUtil.countScore("432", ratioIndexMap.get("652"))[1]+"",reportDate));	
		list.add(new CreditRating(customerNo,"512","销售收入增长率",ratioIndexMap.get("908"),
				(Double)RatingCountUtil.countScore("512", ratioIndexMap.get("908"))[0],RatingCountUtil.countScore("512", ratioIndexMap.get("908"))[1]+"",reportDate));
		list.add(new CreditRating(customerNo,"513","净利润增长率",ratioIndexMap.get("918"),
				(Double)RatingCountUtil.countScore("513", ratioIndexMap.get("918"))[0],RatingCountUtil.countScore("513", ratioIndexMap.get("918"))[1]+"",reportDate));
		list.add(new CreditRating(customerNo,"514","净资产增长率",ratioIndexMap.get("927"),
				(Double)RatingCountUtil.countScore("514", ratioIndexMap.get("927"))[0],RatingCountUtil.countScore("514", ratioIndexMap.get("927"))[1]+"",reportDate));
		
		
		
		return list;
	}

	

	

}
