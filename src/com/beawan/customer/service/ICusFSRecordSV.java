package com.beawan.customer.service;

import java.util.List;
import java.util.Map;

import com.beawan.base.entity.SReportData;
import com.beawan.base.entity.User;
import com.beawan.customer.bean.CusFSRecord;

public interface ICusFSRecordSV{
	
	public CusFSRecord save(String jsondata) throws Exception;
	
	public CusFSRecord update(String jsondata) throws Exception;

	public CusFSRecord saveOrUpdate(CusFSRecord cusFSRecord) throws Exception;
	
	/**
	 * TODO 删除客户报表记录（仅删除该实体信息，关联信息不删除）
	 * @param cusFSRecord
	 * @throws Exception
	 */
	public void delete(CusFSRecord cusFSRecord) throws Exception;
	
	/**
	 * TODO 删除客户报表记录及关联的所有数据
	 * @param recordNo 报表记录编号
	 * @throws Exception
	 */
	public void deleteByNo(String recordNo) throws Exception;
	
	public CusFSRecord queryByRecordNo(String recordNo) throws Exception;
	
	/**
	 * TODO 根据客户号、报表日期、报表模型号唯一查询
	 * @param customerId
	 * @param reportDate
	 * @param modelClass
	 * @return
	 * @throws Exception
	 */
	public CusFSRecord queryUnique(String customerId, String reportDate,
			String modelClass) throws Exception;
	
	public List<CusFSRecord> queryByCusId(String customerId) throws Exception;
	
	public List<CusFSRecord> queryByCusIdDesc(String customerId) throws Exception;
	
	/**
	 * TODO SQL分页查询，关联用户和机构
	 * @param queryCondition 查询条件
	 * @param orderCondition 排序条件
	 * @param pageIndex 页标
	 * @param pageSize 每页大小
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> queryPaging(CusFSRecord queryCondition,
			String orderCondition, int pageIndex, int pageSize)
			throws Exception;

	/**
	 * TODO SLQ查询记录数
	 * @param queryCondition 查询条件
	 * @return
	 * @throws Exception
	 */
	public long queryCount(CusFSRecord queryCondition) throws Exception;
	
	/**
	 * TODO 查询报表数据
	 * @param cusFSRecordNo 客户报表记录编号
	 * @param modelNo 报表模型编号
	 * @return
	 * @throws Exception
	 */
	public List<SReportData> getReportData(String cusFSRecordNo,
			String modelNo) throws Exception;
	
	/**
	 * TODO 查询上年末的数据
	 * @param customerId 客户号
	 * @param reportDate 当前报表日期
	 * @param modelClass 报表模型类别
	 * @param modelNo 报表模型号
	 * @return
	 * @throws Exception
	 */
	public List<SReportData> getLastYearData(String customerId, String reportDate,
			String modelClass, String modelNo) throws Exception;
	
	/**
	 * TODO 保存报表数据
	 * @param cusFSRecordNo 客户财务报表记录编号
	 * @param modelNo 报表模型编号
	 * @param dataArray 报表数据
	 * * @param calcFlag 测算标记，不为空则生成现金流量表并计算相关指标
	 * @param user 操作用户对象
	 * @throws Exception
	 */
	public void saveReportData(String cusFSRecordNo, String modelNo,
			String dataArray, String calcFlag, User user) throws Exception;
	
	/**
	 * TODO 完成财务报表录入
	 * @param cusFSRecordNo 财务报表记录编号
	 * @throws Exception
	 */
	public void finishReportInput(String cusFSRecordNo) throws Exception;
	
	/**
	 * TODO 根据客户号从信贷系统同步对应的财务数据（近三年末+当期+上年同期）
	 * @param customerNo 客户号
	 * @param year 需要同步的截止年份，<=0默认为当前年
	 * @param month 需要同步的截止月份 <=0默认为当前月
	 * @throws Exception
	 */
	public void syncFSFromCmisByCusNo(String customerNo, String xdCustNo,
			int year, int month) throws Exception;
	
	/**
	 * TODO 根据客户号从信贷系统同步对应的财务数据（所有期数都同步）
	 * 上面这个方法是一年一年的去取数，相当于每个客户都必须执行3年*12个月  36次的查询，该查询时间较长，本函数中统一查询
	 * @param customerNo 客户号
	 * @throws Exception
	 */
	public void syncFSFromCmisByCusNoWhole(String customerNo, String xdCustNo) throws Exception;
	
	/**
	 * TODO 从信贷同步财务数据
	 * @param cusFSRecordNo 财报记录编号
	 * @throws Exception
	 */
	public void syncFSFromCmisByRecNo(String cusFSRecordNo) throws Exception;



	/////////////////////////////////////////////////////////////////////////如皋新增开始///////////////

	//对接财务分析系统--》
	/**
	 * 获取对公项目的财务报表
	 * @param customerNo
	 * @param induCode
	 * @return
	 */
	public String getReportJsonData(String customerNo, String induCode, Integer year, Integer month) throws Exception;

	/**
	 * TODO 获取客户最新一期财报日期
	 * @param 
	 * @throws Exception
	 */
	String getMaxReportDate(String customerNo);


	/**
	 * TODO 获取客户当年前一期财报营业收入
	 * @param customerNo 客户号
	 * @param nowDate 当期财报日期
	 */
	Double getOldReportTurnover(String customerNo, String nowDate);


}
