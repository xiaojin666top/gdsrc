package com.beawan.customer.service;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.beawan.core.BaseService;
import com.beawan.customer.bean.AdmitRecordEntity;

public interface AdmitRecordService extends BaseService<AdmitRecordEntity>{
	Map<String, Map<String, Object>> getQuotaResultString(HttpServletRequest request) throws IOException;

	String getAdmitResult(String customerNo, HttpServletRequest request) throws IOException;

}
