package com.beawan.customer.service;

import java.util.List;
import java.util.Map;

import com.beawan.base.entity.SReportModelRow;

/**
 * TODO 客户财务报表工具相关接口
 */
public interface ICusFSToolSV {
	
	/**
	 * TODO 校验对公客户是否满足调查规则
	 * @param cusNo 客户号
	 * @param finaRepYear 财报年份
	 * @param finaRepMonth 财报月份
	 * @throws Exception
	 */
	public void checkCusInfoRule(String cusNo, int finaRepYear, int finaRepMonth)throws Exception;
	
	/**
	 * TODO 校验对公客户财务信息是否满足调查规则
	 * @param customerNo 客户号
	 * @param finaRepYear 财报年份
	 * @param finaRepMonth 财报月份
	 * @throws Exception
	 */
	public void checkComFinaRule(String customerNo, int finaRepYear, int finaRepMonth)throws Exception;

	/**
	 * TODO 根据模型号从信贷查询报表模型数据
	 * @param modelNo 报表模型编号
	 * @return
	 * @throws Exception
	 */
	public List<SReportModelRow> queryReportModel(String modelNo)throws Exception;
	
	/**
	 * TODO 根据模型号查询财务报表原始数据
	 * @param cusId 客户号
	 * @param repYear 报表年份
	 * @param repMonth 报表月份
	 * @param modelNo 模型号
	 * @return 返回财报原始数据
	 * @throws Exception
	 */
	public List<Map<String, Double>> queryReportData(String cusId, 
			int repYear, int repMonth, String modelNo) throws Exception;
			
	/**
	 * TODO 根据模型号查询财务报表原始数据
	 * @param cusId 客户号
	 * @param dateList 查询日期列表
	 * @param modelNo 报表模型号
	 * @return 返回财报原始数据
	 * @throws Exception
	 */
	public List<Map<String, Double>> queryReportData(String cusId, 
			List<String> dateList, String modelNo) throws Exception;
	
	/**
	 * TODO 根据报表类型查询报表模型数据
	 * @param cusId 客户号
	 * @param reportType 报表类型
	 * balance：资产负债表，income：利润表，cashFlow：现金流量表，ratioIndex：财务比率指标表
	 * @return
	 * @throws Exception
	 */
	public List<SReportModelRow> findRepModelByType(String cusId,
			String reportType) throws Exception;

	public List<SReportModelRow> findRepModelByType(String cusId,
			String reportType, String dataType) throws Exception;

	/**
	 * TODO 根据报表类型查询财务报表数据（对新会计准则部分科目进行转换）
	 * @param cusId 客户号
	 * @param repYear 报表年份
	 * @param repMonth 报表月份
	 * @param reportType 报表类型
	 * balance：资产负债表，income：利润表，cashFlow：现金流量表，ratioIndex：财务比率指标表
	 * @return 返回财报数据
	 * @throws Exception
	 */
	public List<Map<String, Double>> findRepDataByType(String cusId, 
			int repYear, int repMonth, String reportType) throws Exception;
			
	/**
	 * TODO 根据报表类型查询财务报表数据（对新会计准则部分科目进行转换）
	 * @param cusId 客户号
	 * @param dateList 查询日期列表
	 * @param reportType 报表类型
	 * balance：资产负债表，income：利润表，cashFlow：现金流量表，ratioIndex：财务比率指标表
	 * @return 返回财报数据
	 * @throws Exception
	 */
	public List<Map<String, Double>> findRepDataByType(String cusId, 
			List<String> dateList, String reportType) throws Exception;
	
	/**
	 * TODO 获得客户财报模板类别
	 * @param cusId 客户号
	 * @return
	 * @throws Exception
	 */
	public String getFinaRepModelClass(String cusId) throws Exception;


	
}
