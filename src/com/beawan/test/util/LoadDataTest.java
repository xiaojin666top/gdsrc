package com.beawan.test.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class LoadDataTest {

	private String basePath = "E:/Share/20180201";

	public static void main(String[] args) throws Exception {
		//Runtime.getRuntime().exec("C:/Program Files/IBM/SQLLIB/BIN/db2.exe 'connect to mclsys'");
		new LoadDataTest().load();
	}

	public void load() throws Exception{

		File baseDir = new File(basePath);
		if (!baseDir.exists())
			return;

		File[] files = baseDir.listFiles();
		if (files == null || files.length == 0)
			return;
		
		Runtime runtime = Runtime.getRuntime();

		/*List<String> cmdList = new ArrayList<String>();
		cmdList.add("db2cmd db2 connect to mclsys");*/
		Process p = runtime.exec("db2cmd db2 connect to mclsys");
		
		Thread.sleep(3000);

		File file;
		for (int i = 0; i < files.length; i++) {
			file = files[i];
			if (file.isDirectory() || !"del".equals(getSuffix(file)))
				continue;

			String fileName = file.getName();
			String tableName = fileName.substring(6, fileName.length() - 14);

			String cmd = "db2 load client from " + file.getAbsolutePath()
					   + " of del modified by coldel0x1d NOROWWARNINGS replace into CMIS."
					   + tableName + " nonrecoverable";

			//print(runtime.exec(cmd));
			//cmdList.add(cmd);
		}

		runtime.exec("db2cmd db2 disconnect mclsys");
		//cmdList.add("db2cmd db2 disconnect mclsys");

		/*String[] cmds = new String[cmdList.size()];
		print(runtime.exec(cmdList.toArray(cmds)));*/

	}

	public String getSuffix(File file) {
		String fileName = file.getName();
		String[] temp = fileName.split("\\.");
		return temp[temp.length - 1];
	}
	
	public void print(Process p){
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(p.getInputStream()));// 获取执行后出现的错误；getInputStream是获取执行后的结果

			String line = null;
			StringBuilder sb = new StringBuilder();
			while ((line = br.readLine()) != null) {
				sb.append(line + "\n");
				System.out.println(sb);
			}
			System.out.println(sb);// 打印执行后的结果
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
