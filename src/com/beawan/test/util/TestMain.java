package com.beawan.test.util;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class TestMain {
	
	public TestMain(){
		System.out.println("实例化TestMain");
	}
	
	public static void main(String[] args) throws Exception {
		//ApplicationContext ac = getContext();
		String a ="2010-03-22 00:00:00";
		if(a.length()>10){
			a = a.substring(0, 10);
			System.out.println(a);
		}
		
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar date = Calendar.getInstance();
		date.setTime(new Date());
		date.add(Calendar.YEAR, -1);//20200119
		Date time = date.getTime();
		String ti = sf.format(time);
		
		
		Date date2 = sf.parse("2020-01-20");
		Calendar enddate = Calendar.getInstance();
		date.setTime(date2);
		Date time2 = date.getTime();
		String ti2 = sf.format(time2);
		
//		if(date.after(enddate)){
//			System.out.println(ti2 + " 大于 " + ti);
//			System.out.println(time.compareTo(time2));
//		}else{
//			System.out.println(ti2 + " 小于 " + ti);
//			
//		}
		
		if(time.compareTo(time2) == -1){
			System.out.println(ti2 + " 大于 " + ti);
		}else{
			System.out.println(ti2 + " 小于 " + ti);
		}
		
		
	}

	private static ApplicationContext getContext() throws Exception {
		return new ClassPathXmlApplicationContext("applicationContext.xml");
	}
}
