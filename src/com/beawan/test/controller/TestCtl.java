package com.beawan.test.controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.base.entity.SysDic;
import com.beawan.base.entity.TableSubjCode;
import com.beawan.base.service.ISysDicSV;
import com.beawan.base.service.ITableSubjCodeSV;
import com.beawan.common.config.AppConfig;
import com.beawan.common.timer.SetSequence2OneTimer;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.customer.bean.CusBase;
import com.beawan.customer.service.FinriskResultService;
import com.beawan.customer.service.ICusBaseSV;
import com.beawan.customer.service.ICusFSRecordSV;
import com.beawan.dtmp.dao.LnlnslnsDao;
import com.beawan.dtmp.dto.CustLoanDto;
import com.beawan.dtmp.entity.CustomerInfo;
import com.beawan.dtmp.service.DtmpService;
import com.beawan.library.bean.ReportItemTemp;
import com.beawan.library.dao.IIndustryQuotaDao;
import com.beawan.library.service.ITempletSV;
import com.beawan.loanAfter.service.FcTaskService;
import com.beawan.loanAfter.service.LfTaskService;
import com.beawan.qcc.service.SynchronizationQCCService;
import com.beawan.rate.dto.RcDateDto;
import com.beawan.rate.service.RcResultService;
import com.beawan.scoreCard.service.IAssessCardSV;
import com.beawan.survey.custInfo.service.CompJudgmentService;
import com.beawan.survey.custInfo.service.ICompFinancingSV;
import com.beawan.survey.loanInfo.service.IConclusionSV;
import com.beawan.survey.loanInfo.service.ISurveyReportSV;
import com.beawan.task.service.ITaskSV;
import com.google.common.collect.Lists;

@Controller
@RequestMapping({ "/test" })
public class TestCtl extends BaseController {
	@Resource
	private DtmpService dtmpService;
	@Resource
	private LfTaskService lfTaskService;
    @Resource
    private FcTaskService fcTaskService;
	@Resource
	private IIndustryQuotaDao industryQuotaDao;
	@Resource
	private ITableSubjCodeSV tableSubjCodeSV;
	@Resource
	private ITempletSV templetService;
	@Resource
	private ISurveyReportSV surveyReportSV;
	
	@Resource
	private ICusBaseSV cusBaseSV;
	@Resource
	private ICusFSRecordSV cusFSRecordSV;
	@Resource
	private SynchronizationQCCService synchronizationQCCService;
	@Resource
	private IAssessCardSV assessCardSV;
	@Resource
	private CompJudgmentService compJudgmentService;

	@Resource
	private IConclusionSV conclusionSV;
	@Resource
	private FinriskResultService finriskResultService;

    @Resource
    private LnlnslnsDao lnlnslnsDao;

	@Resource
	private ITaskSV taskSV;
	@Resource
	private ISysDicSV sysDicSV;
	@Resource
	private RcResultService resultService;
	@Resource
	protected  ICompFinancingSV financingSV;


	/**
	 * 手动增加字典项
	 * @param request
	 * @return
	 */
	@RequestMapping("saveDic.json")
	@ResponseBody
	public ResultDto saveDic(HttpServletRequest request) {
		try {
//			SysDic dic = new ;
			
			
			ArrayList<SysDic> newArrayList = Lists.newArrayList(
					new SysDic("2", "较差", "RG_PRE_ITEM_LEVEL", "预设分析内容评价等级", 2L),
					new SysDic("3", "一般", "RG_PRE_ITEM_LEVEL", "预设分析内容评价等级", 3L),
					new SysDic("4", "较好", "RG_PRE_ITEM_LEVEL", "预设分析内容评价等级", 4L),
					new SysDic("5", "极好", "RG_PRE_ITEM_LEVEL", "预设分析内容评价等级", 5L)
					);
			for(SysDic dic : newArrayList){
				sysDicSV.saveOrUpdate(dic);
			}
		}catch (Exception e){
			e.printStackTrace();
		}
        return returnSuccess();
    }
	
	@RequestMapping("syncCompFinaBankFromCmis.json")
	@ResponseBody
	public ResultDto syncCompFinaBankFromCmis(HttpServletRequest request) {
		try {
			financingSV.syncCompFinaBankFromCmis(88L, "CM20201016026069");
		}catch (Exception e){
			e.printStackTrace();
		}
        return returnSuccess();
    }
	
	
	@RequestMapping("testMCQ.json")
	@ResponseBody
	public ResultDto testMCQ(HttpServletRequest request,String customerNo, String obj) {
		try {

			Double dgRate = resultService.syncRcResult("DGZJ20201202060");
			System.out.println(dgRate);
			
//			assessCardSV.syncCalcAssetResult("DGZJ20200812141");
		}catch (Exception e){
			e.printStackTrace();
		}
        return returnSuccess();
    }

	
	@RequestMapping("syncCusFSRecord.json")
	@ResponseBody
	public Map<String, Object> syncCusFSRecord(HttpServletRequest request,
			HttpServletResponse response) {
		
		Map<String, Object> json = new HashMap<String, Object>();
		json.put("success", false);
		
		try {
			
			long timeStart = new Date().getTime();
			
			List<CusBase> cusList = cusBaseSV.queryAll();
			for(CusBase cusBase : cusList){
				cusFSRecordSV.syncFSFromCmisByCusNo(cusBase.getCustomerNo(), cusBase.getXdCustomerNo(), 0, 0);
			}
			
			long timeEnd = new Date().getTime();
			
			json.put("seconds", (timeEnd-timeStart)/1000);
			json.put("success", true);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return json;
	}



	@RequestMapping("synQcc.do")
	@ResponseBody
	public String synQcc(){
		try {
			synchronizationQCCService.getQccFullByCustomerName("");
		}catch (Exception e){

		}
		return "success";
	}


	@RequestMapping("submitSuveryRep.do")
	@ResponseBody
	public Map<String, Object> submitSuveryRep(HttpServletRequest request,
			HttpServletResponse response, long taskId) {
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			surveyReportSV.createSurveyReport(taskId);
			json.put("success", true);
		} catch (Exception e) {
			json.put("success", false);
			e.printStackTrace();
		}
		return json;
	}

	@RequestMapping("report.do")
	@ResponseBody
	public Map<String, Object> report(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			ReportItemTemp temp1 = new ReportItemTemp("货币资金",1,"货币资产,","1");
			ReportItemTemp temp2 = new ReportItemTemp("短期投资",2,"","1");
			ReportItemTemp temp3 = new ReportItemTemp("应收票据",3,"","1");
			ReportItemTemp temp5 = new ReportItemTemp("应收账款",5,"","1");
			ReportItemTemp temp6 = new ReportItemTemp("其他应收款",6,"","1");
			ReportItemTemp temp7 = new ReportItemTemp("预付账款",7,"预付款项,","1");
			ReportItemTemp temp8 = new ReportItemTemp("应收补贴款",8,"","1");
			ReportItemTemp temp9 = new ReportItemTemp("存货",9,"","1");
			ReportItemTemp temp10 = new ReportItemTemp("待摊费用",10,"","1");
			ReportItemTemp temp11 = new ReportItemTemp("一年内到期的长期债券投资",11,"","1");
			ReportItemTemp temp12 = new ReportItemTemp("其他流动资产",12,"","1");
			ReportItemTemp temp13 = new ReportItemTemp("待处理流动资产净损失",13,"待处理流动资产损失,","1");
			ReportItemTemp temp14 = new ReportItemTemp("流动资产合计",14,"","1");
			ReportItemTemp temp15 = new ReportItemTemp("长期投资合计",15,"","1");
			
			ReportItemTemp temp16 = new ReportItemTemp("固定资产原价",16,"固定资产原值,","1");
			ReportItemTemp temp17 = new ReportItemTemp("累计折旧",17,"","1");
			ReportItemTemp temp18 = new ReportItemTemp("固定资产净值",18,"","1");
			ReportItemTemp temp19 = new ReportItemTemp("待处理固定资产净损失",19,"固定资产减值准备,待处理固定资产损失,","1");
			ReportItemTemp temp20 = new ReportItemTemp("固定资产净额",20,"","1");
			ReportItemTemp temp21 = new ReportItemTemp("工程物资",21,"","1");
			
			ReportItemTemp temp23 = new ReportItemTemp("在建工程",23,"","1");
			ReportItemTemp temp24 = new ReportItemTemp("固定资产清理",24,"","1");
			ReportItemTemp temp25 = new ReportItemTemp("固定资产合计",25,"","1");
			
			ReportItemTemp temp26 = new ReportItemTemp("无形资产",26,"","1");
			ReportItemTemp temp27 = new ReportItemTemp("其他长期资产",27,"其他资产,其他非流动资产,","1");
			ReportItemTemp temp28 = new ReportItemTemp("无形及其他资产合计",28,"无形资产及其他资产合计,","1");
			ReportItemTemp temp29 = new ReportItemTemp("递延资产",29,"递延税项,递延所得税资产,","1");
			
			ReportItemTemp temp30 = new ReportItemTemp("资产总计",30,"资产合计,","1");
			
			ReportItemTemp temp31 = new ReportItemTemp("短期借款",31,"","1");
			ReportItemTemp temp32 = new ReportItemTemp("应付票据",32,"","1");
			ReportItemTemp temp33 = new ReportItemTemp("应付账款",33,"","1");
			ReportItemTemp temp34 = new ReportItemTemp("预收账款",34,"预收款项,","1");
			
			ReportItemTemp temp39 = new ReportItemTemp("应付工资",39,"","1");
			ReportItemTemp temp40 = new ReportItemTemp("应付福利费",40,"","1");
			ReportItemTemp temp41 = new ReportItemTemp("应付股利",41,"","1");
			ReportItemTemp temp42 = new ReportItemTemp("应交税金",42,"应交税费,","1");
			ReportItemTemp temp43 = new ReportItemTemp("其他应交款",43,"","1");
			ReportItemTemp temp44 = new ReportItemTemp("其他应付款",44,"","1");
			ReportItemTemp temp45 = new ReportItemTemp("预提费用",45,"","1");
			ReportItemTemp temp46 = new ReportItemTemp("一年内到期的长期负债",46,"一年内到期的非流动负债,","1");
			ReportItemTemp temp47 = new ReportItemTemp("其他流动负债",47,"","1");
			ReportItemTemp temp48 = new ReportItemTemp("流动负债合计",48,"","1");
			
			ReportItemTemp temp49 = new ReportItemTemp("长期借款",49,"","1");
			ReportItemTemp temp50 = new ReportItemTemp("应付债券",50,"应付长期债券,","1");
			ReportItemTemp temp51 = new ReportItemTemp("长期应付款",51,"","1");
			ReportItemTemp temp52 = new ReportItemTemp("其他长期负债",52,"其他非流动负债,","1");
			ReportItemTemp temp53 = new ReportItemTemp("长期负债合计",53,"","1");
			
			ReportItemTemp temp54 = new ReportItemTemp("负债合计",54,"负债总额,负债总计,","1");
			
			ReportItemTemp temp55 = new ReportItemTemp("实收资本",55,"","1");
			ReportItemTemp temp56 = new ReportItemTemp("资本公积",56,"","1");
			ReportItemTemp temp57 = new ReportItemTemp("盈余公积",57,"","1");
			ReportItemTemp temp58 = new ReportItemTemp("未分配利润",58,"","1");
			ReportItemTemp temp59 = new ReportItemTemp("所有者权益合计",59,"","1");
			ReportItemTemp temp60 = new ReportItemTemp("负债及所有者权益总计",60,"负债及所有者权益合计,","1");
			
			templetService.saveReportItem(temp1);
			templetService.saveReportItem(temp2);
			templetService.saveReportItem(temp3);
			templetService.saveReportItem(temp5);
			templetService.saveReportItem(temp6);
			templetService.saveReportItem(temp7);
			templetService.saveReportItem(temp8);
			templetService.saveReportItem(temp9);
			templetService.saveReportItem(temp10);
			
			templetService.saveReportItem(temp11);
			templetService.saveReportItem(temp12);
			templetService.saveReportItem(temp13);
			templetService.saveReportItem(temp14);
			templetService.saveReportItem(temp15);
			templetService.saveReportItem(temp16);
			templetService.saveReportItem(temp17);
			templetService.saveReportItem(temp18);
			templetService.saveReportItem(temp19);
			templetService.saveReportItem(temp20);
			
			templetService.saveReportItem(temp21);
			templetService.saveReportItem(temp23);
			templetService.saveReportItem(temp24);
			templetService.saveReportItem(temp25);
			templetService.saveReportItem(temp26);
			templetService.saveReportItem(temp27);
			templetService.saveReportItem(temp28);
			templetService.saveReportItem(temp29);
			templetService.saveReportItem(temp30);
			
			templetService.saveReportItem(temp31);
			templetService.saveReportItem(temp32);
			templetService.saveReportItem(temp33);
			templetService.saveReportItem(temp34);
			templetService.saveReportItem(temp39);
			templetService.saveReportItem(temp40);
			
			templetService.saveReportItem(temp41);
			templetService.saveReportItem(temp42);
			templetService.saveReportItem(temp43);
			templetService.saveReportItem(temp44);
			templetService.saveReportItem(temp45);
			templetService.saveReportItem(temp46);
			templetService.saveReportItem(temp47);
			templetService.saveReportItem(temp48);
			templetService.saveReportItem(temp49);
			templetService.saveReportItem(temp50);
			
			templetService.saveReportItem(temp51);
			templetService.saveReportItem(temp52);
			templetService.saveReportItem(temp53);
			templetService.saveReportItem(temp54);
			templetService.saveReportItem(temp55);
			templetService.saveReportItem(temp56);
			templetService.saveReportItem(temp57);
			templetService.saveReportItem(temp58);
			templetService.saveReportItem(temp59);
			templetService.saveReportItem(temp60);
			
			
			ReportItemTemp temp61 = new ReportItemTemp("主营业务收入",61,"营业收入,","2");
			ReportItemTemp temp62 = new ReportItemTemp("主营业务成本",62,"营业成本,","2");
			ReportItemTemp temp63 = new ReportItemTemp("主营业务税金及附加",63,"税金及附加,营业税金及附加,","2");
			ReportItemTemp temp64 = new ReportItemTemp("主营业务利润",64,"","2");
			ReportItemTemp temp65 = new ReportItemTemp("营业费用",65,"销售费用,","2");
			ReportItemTemp temp66 = new ReportItemTemp("管理费用",66,"","2");
			ReportItemTemp temp67 = new ReportItemTemp("财务费用",67,"","2");
			ReportItemTemp temp68 = new ReportItemTemp("投资收益",68,"","2");
			ReportItemTemp temp69 = new ReportItemTemp("其他业务利润",69,"","2");
			ReportItemTemp temp70 = new ReportItemTemp("营业利润",70,"","2");
			ReportItemTemp temp71 = new ReportItemTemp("营业外收入",71,"","2");
			ReportItemTemp temp72 = new ReportItemTemp("补贴收入",72,"","2");
			ReportItemTemp temp73 = new ReportItemTemp("营业外支出",73,"","2");
			ReportItemTemp temp74 = new ReportItemTemp("利润总额",74,"","2");
			ReportItemTemp temp75 = new ReportItemTemp("所得税",75,"所得税费用,","2");
			ReportItemTemp temp76 = new ReportItemTemp("净利润",76,"","2");
			
			templetService.saveReportItem(temp61);
			templetService.saveReportItem(temp62);
			templetService.saveReportItem(temp63);
			templetService.saveReportItem(temp64);
			templetService.saveReportItem(temp65);
			templetService.saveReportItem(temp66);
			templetService.saveReportItem(temp67);
			templetService.saveReportItem(temp68);
			templetService.saveReportItem(temp69);
			templetService.saveReportItem(temp70);
			templetService.saveReportItem(temp71);
			templetService.saveReportItem(temp72);
			templetService.saveReportItem(temp73);
			templetService.saveReportItem(temp74);
			templetService.saveReportItem(temp75);
			templetService.saveReportItem(temp76);
			
			json.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}

	@RequestMapping("test.do")
	@ResponseBody
	public Map<String, Object> test(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// request.getSession().invalidate();
		Map<String, Object> json = new HashMap<String, Object>();
		/*
		 * String sql =
		 * "select m.CUS_NAME from CUS_COM m where m.CUS_ID='8000994143'";
		 * String str = JdbcUtil.queryForString(Constants.DataSource.CMIS_DS,
		 * sql, null);
		 */

		/*
		 * CompBase compBase = cmisInInvokeSV.queryCompBaseInfo("8001092178");
		 * json.put("data",compBase);
		 */

		// List<CompBaseManager> list =
		// cmisInInvokeSV.queryCompManager("8001133656", null);
		// List<CompBaseEquity> list =
		// cmisInInvokeSV.queryCompEquity("8000702843");
		/*
		 * PersonBase personBase = cmisInInvokeSV.queryPersonBase("2008246256");
		 * json.put("data", personBase);
		 */
		// json.put("data", cmisInInvokeSV.queryCurrCreditInfo("8000853736"));

		// List<CompBase> list =
		// cmisInInvokeSV.queryCompGrpMember("8000752956");

		// List<Map<String, Double>> list =
		// cmisInInvokeSV.queryBalanceSheet("8000901803", "2017", "3");
		// List<Map<String, Double>> list =
		// cmisInInvokeSV.queryIncomeSheet("8000901803", "2017", "3");
		// List<Map<String, Double>> list =
		// cmisInInvokeSV.queryCashFlowSheet("8000901803", "2017", "3");
		// List<Map<String, Double>> list =
		// cmisInInvokeSV.queryFinanIndex("8000901803", "2017", "3");

		/*
		 * List<CompFinancingAnalysis> list =
		 * qpcsInInvokeSV.queryFinancing("3205830002228426"); json.put("data",
		 * list);
		 */

		/*
		 * List<LastCreditInfo> list =
		 * cmisInInvokeSV.queryLastCreditInfoLowRisk("8000922258");
		 * json.put("data",list);
		 */
		// cmisInInvokeSV.checkCompCustRule("8001022803", "2017-3");

		/*
		 * List<LastCreditInfo> list =
		 * cmisInInvokeSV.queryLastCreditInfoPerson("2029603341");
		 * json.put("data",list);
		 */
		// cmisInInvokeSV.checkCompCustRule("8001022803", "2017-3");

		// System.out.println(qpcsInInvokeSV.queryCreditReportUrl("3211810000054429",
		// CustConstants.CusType.CUS_COMP));
		// json.put("data",cmisBaseInInvokeSV.queryUserByNo("66011029").getPassword().equals(StringUtil.getMD5("admin")));
		SetSequence2OneTimer oneTimer = new SetSequence2OneTimer();
		oneTimer.setSequence2One();

		return json;
	}

	@RequestMapping("tableSubjCode.do")
	@ResponseBody
	public Map<String, Object> tableSubjCode(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			// List<TableSubjCode> lists = tableSubjCodeSV.queryByBusType("01");

			/*
			 * List<TableSubjCode> lists =
			 * tableSubjCodeSV.queryByTableSubjCodes("03", "F04"); for
			 * (TableSubjCode tableSubjCode : lists) { TableSubjCode temp1 = new
			 * TableSubjCode(); temp1.setBusinType("09");
			 * temp1.setBusinType("04");
			 * temp1.setSubjName(tableSubjCode.getSubjName());
			 * temp1.setSort(tableSubjCode.getSort());
			 * temp1.setSubjType(tableSubjCode.getSubjType());
			 * tableSubjCodeSV.saveOrUpdate(temp1); }
			 * 
			 * TableSubjCode temp2 = new TableSubjCode();
			 * temp2.setBusinType("07");
			 * temp2.setSubjName(tableSubjCode.getSubjName());
			 * temp2.setSort(tableSubjCode.getSort());
			 * temp2.setSubjType(tableSubjCode.getSubjType());
			 * tableSubjCodeSV.saveOrUpdate(temp2); }
			 */
			/*
			 * String[] subjName2 = { "应纳增值税", "已缴增值税", "出口退税", "应纳所得税",
			 * "已缴所得税", "即征即退" }; for (int i = 1; i < subjName2.length + 1; i++)
			 * { TableSubjCode tableSubjCode = new TableSubjCode();
			 * tableSubjCode.setBusinType("04");
			 * tableSubjCode.setSubjName(subjName2[i - 1]);
			 * tableSubjCode.setSort((long) i);
			 * tableSubjCode.setSubjType("F0301");
			 * tableSubjCodeSV.saveOrUpdate(tableSubjCode); } for (int i = 1; i
			 * < subjName2.length + 1; i++) { TableSubjCode tableSubjCode = new
			 * TableSubjCode(); tableSubjCode.setBusinType("07");
			 * tableSubjCode.setSubjName(subjName2[i - 1]);
			 * tableSubjCode.setSort((long) i);
			 * tableSubjCode.setSubjType("F0301");
			 * tableSubjCodeSV.saveOrUpdate(tableSubjCode); }
			 */
			/*
			 * String[] subjName3 = { "用电度数（度数）", "用气量（立方）", "用水量（吨）",
			 * "工人人数（人）", "工资总额（万元）" };
			 * 
			 * String[] subjName1 = { "流动资产合计", "货币资金", "应收账款", "其他应收款", "预付账款",
			 * "存货", "固定资产合计", "无形资产", "其他资产", "资产总计", "流动负债合计", "短期借款", "应付票据",
			 * "应付账款", "预收账款", "其他应付款", "长期负债合计", "负债合计", "所有者权益合计", "资产负债率" };
			 * 
			 * for (int i = 1; i < subjName1.length + 1; i++) { TableSubjCode
			 * tableSubjCode = new TableSubjCode();
			 * tableSubjCode.setBusinType("01");
			 * tableSubjCode.setSubjName(subjName1[i - 1]);
			 * tableSubjCode.setSort((long) i);
			 * tableSubjCode.setSubjType("R01");
			 * tableSubjCodeSV.saveOrUpdate(tableSubjCode); }
			 */

			/*
			 * for (int i = 1; i < subjName3.length + 1; i++) { TableSubjCode
			 * tableSubjCode = new TableSubjCode();
			 * tableSubjCode.setBusinType("01");
			 * tableSubjCode.setSubjName(subjName3[i - 1]);
			 * tableSubjCode.setSort((long) i);
			 * tableSubjCode.setSubjType("R03");
			 * tableSubjCodeSV.saveOrUpdate(tableSubjCode); }
			 */

			/*
			 * String[] subjName4 = { "累计担保笔数", "累计担保金额", "时点末在保余额" }; for (int
			 * i = 1; i < subjName4.length + 1; i++) { TableSubjCode
			 * tableSubjCode = new TableSubjCode();
			 * tableSubjCode.setBusinType("11");
			 * tableSubjCode.setSubjName(subjName4[i - 1]);
			 * tableSubjCode.setSort((long) i);
			 * tableSubjCode.setSubjType("R0101");
			 * tableSubjCodeSV.saveOrUpdate(tableSubjCode); } String[] subjName5
			 * = { "房地产抵押", "存货等质押", "一般担保", "关联担保", "个人担保", "信用方式" }; for (int
			 * i = 1; i < subjName5.length + 1; i++) { TableSubjCode
			 * tableSubjCode = new TableSubjCode();
			 * tableSubjCode.setBusinType("11");
			 * tableSubjCode.setSubjName(subjName5[i - 1]);
			 * tableSubjCode.setSort((long) i);
			 * tableSubjCode.setSubjType("R0102");
			 * tableSubjCodeSV.saveOrUpdate(tableSubjCode); } String[] subjName6
			 * = { "1、累计发生代偿金额", "   其中：已追偿金额", "  可追偿尚未追偿到位金额",
			 * "         损失金额    ", "2、应代偿暂未代偿金额" }; for (int i = 1; i <
			 * subjName6.length + 1; i++) { TableSubjCode tableSubjCode = new
			 * TableSubjCode(); tableSubjCode.setBusinType("11");
			 * tableSubjCode.setSubjName(subjName6[i - 1]);
			 * tableSubjCode.setSort((long) i);
			 * tableSubjCode.setSubjType("R0104");
			 * tableSubjCodeSV.saveOrUpdate(tableSubjCode); }
			 */
			String[] subjName7 = { "应收账款", "其他应收款", "预付账款", "应付账款", "预收账款", "其他应付款", "长期应付款" };
			for (int i = 1; i < subjName7.length + 1; i++) {
				TableSubjCode tableSubjCode = new TableSubjCode();
				tableSubjCode.setBusinType("02");
				tableSubjCode.setSubjName(subjName7[i - 1]);
				tableSubjCode.setSort((long) i);
				tableSubjCode.setSubjType("ERR");
				tableSubjCodeSV.saveOrUpdate(tableSubjCode);
			}

			for (int i = 1; i < subjName7.length + 1; i++) {
				TableSubjCode tableSubjCode = new TableSubjCode();
				tableSubjCode.setBusinType("03");
				tableSubjCode.setSubjName(subjName7[i - 1]);
				tableSubjCode.setSort((long) i);
				tableSubjCode.setSubjType("ERR");
				tableSubjCodeSV.saveOrUpdate(tableSubjCode);
			}

			String[] subjName8 = { "应收账款", "其他应收款", "预付账款", "应付账款", "预收账款", "其他应付款" };
			for (int i = 1; i < subjName8.length + 1; i++) {
				TableSubjCode tableSubjCode = new TableSubjCode();
				tableSubjCode.setBusinType("01");
				tableSubjCode.setSubjName(subjName8[i - 1]);
				tableSubjCode.setSort((long) i);
				tableSubjCode.setSubjType("ERR");
				tableSubjCodeSV.saveOrUpdate(tableSubjCode);
			}

			/*
			 * String[] subjName5 = { "低风险业务", "低风险业务", "低风险业务", "低风险业务"}; for
			 * (int i = 1; i < subjName5.length + 1; i++) { TableSubjCode
			 * tableSubjCode = new TableSubjCode();
			 * tableSubjCode.setBusinType("06");
			 * tableSubjCode.setSubjName(subjName5[i - 1]);
			 * tableSubjCode.setSort((long) i);
			 * tableSubjCode.setSubjType("S03");
			 * tableSubjCodeSV.saveOrUpdate(tableSubjCode); }
			 */
			/*
			 * for (int i = 1; i < subjName5.length + 1; i++) { TableSubjCode
			 * tableSubjCode = new TableSubjCode();
			 * tableSubjCode.setBusinType("07");
			 * tableSubjCode.setSubjName(subjName5[i - 1]);
			 * tableSubjCode.setSort((long) i);
			 * tableSubjCode.setSubjType("S03");
			 * tableSubjCodeSV.saveOrUpdate(tableSubjCode); }
			 */
			/*
			 * String[] businTypeArr =
			 * {"01","02","03","04","07","08","09","10"}; for (int j = 0; j <
			 * businTypeArr.length; j++) { String[] subjName6 = { "①一般性存款",
			 * "②保证金帐户存款我行", "③定期存款", "④外币存款", "⑤利息收入", "⑥中间业务收入"}; for (int i =
			 * 1; i < subjName6.length + 1; i++) { TableSubjCode tableSubjCode =
			 * new TableSubjCode(); tableSubjCode.setBusinType(businTypeArr[j]);
			 * tableSubjCode.setSubjName(subjName6[i - 1]);
			 * tableSubjCode.setSort((long) i);
			 * tableSubjCode.setSubjType("Z02");
			 * tableSubjCodeSV.saveOrUpdate(tableSubjCode); } }
			 */

			json.put("success", true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}

	@RequestMapping("testApi.json")
	@ResponseBody
	public ResultDto testApi(HttpServletRequest request, HttpServletResponse response) {
		ResultDto re = returnFail();
		try {
//			List<CompJudgmentDto> newestJudgeList = compJudgmentService.getNewestJudgeList("2020-09-16", "CM202008101605206245");
//			List<WarnItemDto> warnItemList = finriskResultService.getFinaWarn("DGZJ20200813161");
//			StringBuilder financialRisk = new StringBuilder("财务信息经系统分析检测，状况正常");
//			if(!CollectionUtils.isEmpty(warnItemList)){
//				financialRisk = new StringBuilder();
//				for(WarnItemDto warn : warnItemList){
//					String abnormalItem = warn.getAbnormalItem();
//					String errReason = warn.getErrReason();
//					financialRisk.append(abnormalItem).append("，").append(errReason);
//				}
//			}
//			
//
//			RiskAnalysis riskAnalysis = conclusionSV.findRiskAnalysisByTaskId(3080L);
//			riskAnalysis.setFinancialRisk(financialRisk.toString());
//			conclusionSV.saveRiskAnalysis(riskAnalysis);
//			
//			
//			List<CompJudgmentDto> newestJudgeList = compJudgmentService.getNewestJudgeList("2020-09-16", "CM202008101605206245", "北京小桔科技有限公司");
//			String judgSimAnaly = compJudgmentService.getJudgSimAnaly("2020-09-16", "CM202008101605206245", "北京小桔科技有限公司");
			/*
			//获取用信情况
			List<CustLoanDto> custLoanDtos = lnlnslnsDao.getCustLoan("205843005147", null);
			//			taskSV.insertCreditSelfBankInfo(3080L);
			*/
			lfTaskService.syncFXThreeMonthTask2(null);
			//re.setRows(custLoanDtos);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	public static void main(String[] args) throws Exception {
		
		ArrayList<CustomerInfo> list = Lists.newArrayList(new CustomerInfo("A公司", "20200831"), 
				new CustomerInfo("", "20200831"), 
				new CustomerInfo("", "20200731"),
				new CustomerInfo("A公司", "20200416"), 
				new CustomerInfo("A公司", "20210416"), 
				new CustomerInfo("C公司", "20200516"));
		
		list = list.stream().sorted(Comparator.comparing(CustomerInfo::getCustomername, Comparator.nullsLast(Comparator.naturalOrder()))
				.thenComparing(CustomerInfo::getUpdatedate).reversed())
			.collect(Collectors.collectingAndThen(Collectors.toCollection(()-> 
			new TreeSet<>(Comparator.comparing(CustomerInfo::getCustomername))), ArrayList::new));
		

		
		System.out.println(list);
		
//		List<RepaySchuDto> list = new ArrayList<>();
//		Double repayVal = 500000d;
//		int loanYear = 3;
//		Double rc = 0.059;//5.9%
//		
////		等额本金
//		//每月还款本金=贷款总额/贷款月数 
//		//每月还款利息=贷款本金余额*贷款月利率（ 贷款月利率=年利率/12）
//		//贷款本金余额=贷款总额-已还款月数*每月还款本金
//		for(int i=1; i <= loanYear * 12; i++) {
//
//			Double benjin = repayVal / (loanYear * 12);
//			String repayTime = DateUtil.format(DateUtil.addMonth(new Date(), i), "yyyy/MM/dd");
//			Double lixi = (repayVal - i * benjin) * (rc/12);
//			RepaySchuDto repay = new RepaySchuDto(i, repayTime, 
//					new BigDecimal(benjin).setScale(2, RoundingMode.HALF_UP).doubleValue(), 
//					new BigDecimal(lixi).setScale(2, RoundingMode.HALF_UP).doubleValue());
//			list.add(repay);
//			
//			System.out.println(benjin + lixi);
//		}
//		
//		
//		//等额本息
//		//每月应还款额＝借款本金×月利率×（1＋月利率）^还款月数/[（1＋月利率）^还款月数－1]
//		Double monthRc = rc/12;
//		Double every = repayVal * monthRc * Math.pow(1 + monthRc, loanYear * 12) / (Math.pow(1 + monthRc, loanYear * 12) - 1);
//		System.out.println("等额本息: "+ new BigDecimal(every).setScale(2, RoundingMode.HALF_UP).doubleValue());
//		
//		
//		//一次性归还
//		Double lixi = repayVal * (Math.pow((1 + rc), loanYear) - 1);
//		String repayTime = DateUtil.format(DateUtil.addYear(new Date(), loanYear), "yyyy/MM/dd");
//		RepaySchuDto repay = new RepaySchuDto(1, repayTime, 
//				new BigDecimal(repayVal).setScale(2, RoundingMode.HALF_UP).doubleValue(), 
//				new BigDecimal(lixi).setScale(2, RoundingMode.HALF_UP).doubleValue());
//		list.add(repay);
//		System.out.println(repay);
		
		
		
//		RequestTransaction<TransCusBase> requestTransaction = new RequestTransaction<>();
//		ReqTransaction<TransCusBase> Transaction = new ReqTransaction<>();
//		ReqHeader Header = new ReqHeader();
//		ReqSysHeader sysHeader = MsgUtil.getReqSysHeader(ResultDto.RESULT_CODE_SUCCESS+"", "请求发送成功");
//		
//		ReqBody<TransCusBase> Body = new ReqBody<>();
//		ReqRequest<TransCusBase> request = new ReqRequest<>();
//		ReqBizHeader bizHeader = new ReqBizHeader();
//		TransCusBase transCusBase = new TransCusBase();
//		
//		transCusBase.setCustomerNo(DateUtil.getNow()+RandomUtils.nextInt(10));
//		transCusBase.setCustomerName("深圳南山科技发展有限公司");
//		
////		<option value="0110">一般企业</option>
////		<option value="0120">小企业</option>
////		<option value="0210">实体集团</option>
////		<option value="0220">虚拟集团</option>
//		transCusBase.setCustomerType("实体集团");
//		transCusBase.setCertType("营业执照/统一社会信用代码证");
//		transCusBase.setCertCode("91330000733796106P");
//		transCusBase.setManagerUserId("I052701");
//		
//		bizHeader.setDEALCODE("ADDUSER_001");
//		request.setBizHeader(bizHeader);
//		request.setBizBody(transCusBase);
//		Header.setSysHeader(sysHeader);
//		Body.setRequest(request);
//		Transaction.setHeader(Header);
//		Transaction.setBody(Body);
//		requestTransaction.setTransaction(Transaction);
//		
//		
//		String json = GsonUtil.GsonString(requestTransaction);
//		System.out.println(json);
		
		
		
		
//		System.out.println(StringUtil.getMD5("80701591"));
		// 58EEF4598EACD68D047955B0B3242BA5
		// 3fad19267b9408c9678be2442bb2479b
		

	}

}
