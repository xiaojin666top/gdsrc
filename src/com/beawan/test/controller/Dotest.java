package com.beawan.test.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import com.beawan.model.service.RiskWarningService;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
@TransactionConfiguration(transactionManager="transactionManager",defaultRollback=false)
public class Dotest implements ApplicationContextAware {
	
	@Resource
	public RiskWarningService riskWarningService;
	ApplicationContext applicationContext;

	@Test
	public void test1() throws Exception {
		String after = "1 <a href = \\\"https://www.tianyancha.com/human/1984012283-c22822\\\" target = \\\"_blank\\\">李彦宏</a> 执行董事<br>2 <a href = \\\"https://www.tianyancha.com/human/2020172991-c22822\\\" target = \\\"_blank\\\">梁志祥</a>* 经理<br>3 <a href = \\\"https://www.tianyancha.com/human/1839080315-c22822\\\" target = \\\"_blank\\\">向海龙</a> 监事<font color= #EF5644> [新增]</font><br><em><hr style=\\\"border-top:1px dashed #E0E0E0;\\\"/><font color= #9E9E9E>注：标有*标志的为法定代表人</font></em>";
		Document doc1 = Jsoup.parse(after);
		Elements elements1 = doc1.select("a");
		System.out.println(elements1.size());
	}
	
	
	
	@Test
	@Transactional
	public void ICGG004() {
		String taskId = "test00001";
		
		String name1 = "王春进";
		String name2 = "王鑫";
		String name3 = "赵芸蕾";
		List<String> newShareholders = new ArrayList<>();
		newShareholders.add(name1);
		newShareholders.add(name2);
		newShareholders.add(name3);
		String riskResult = riskWarningService.ICGG004(taskId, "张楠", newShareholders);
		
		System.out.println(riskResult);
	}
	
	@Test
	@Transactional
	public void SDYJ001() {
		String taskId = "test00001";
		
		double lastYearEleCharge = 1086.0;
		double currentEleCharge = 625.8;
		
		String riskResult = riskWarningService.SDYJ001(taskId, lastYearEleCharge, currentEleCharge);
		
		System.out.println(riskResult);
		
	}
	
	@Test
	@Transactional
	public void SDYJ002() {
		String taskId = "test00001";
		
		double lastMonthEleCharge = 222.0;
		double currentEleCharge = 50.5;
		
		String riskResult = riskWarningService.SDYJ002(taskId, lastMonthEleCharge, currentEleCharge);
		
		System.out.println(riskResult);
	}
	
	@Test
	@Transactional
	public void SDYJ003() {
		String taskId = "test00001";
		
		double lastYearEleCharge = 1086.0;
		double currentEleCharge = 625.8;
		double lastIncome = 1000000;
		double currentIncome = 120000;
		
		String riskResult = riskWarningService.SDYJ003(taskId, lastYearEleCharge, currentEleCharge, lastIncome, currentIncome);
		
		System.out.println(riskResult);
		
	}
	
	@Test
	@Transactional
	public void SDYJ004() {
		String taskId = "test00001";
		
		double lastMonthEleCharge = 222.0;
		double currentEleCharge = 50.5;
		double lastIncome = 1000000;
		double currentIncome = 120000;
		
		String riskResult = riskWarningService.SDYJ004(taskId, lastMonthEleCharge, currentEleCharge, lastIncome, currentIncome);
		
		System.out.println(riskResult);
	}
	
	@Test
	@Transactional
	public void SDYJ006() {
		String taskId = "test00001";
		
		double currentEleCharge = 1086.0;
		double industryOutput = 1000000;
		double tradeInduOutPut = 1200000;
		
		String riskResult = riskWarningService.SDYJ006(taskId, currentEleCharge, industryOutput, tradeInduOutPut);
		
		System.out.println(riskResult);
	}
	
	@Test
	@Transactional
	public void SDYJ007() {
		String taskId = "test00001";
		
		double currentEleCharge = 1086.0;
		double industryOutput = 1000000;
		double tradeInduOutPut = 1200000;
		
		String riskResult = riskWarningService.SDYJ007(taskId, currentEleCharge, industryOutput, tradeInduOutPut);
		
		System.out.println(riskResult);
	}
	
	@Test
	@Transactional
	public void ZXYJ001() {
		String taskId = "test00001";
		
		int times = 3;
		
		String riskResult = riskWarningService.ZXYJ001(taskId, times);
		
		System.out.println(riskResult);
	}
	
	@Test
	@Transactional
	public void ZXYJ007() {
		String taskId = "test00001";
		
		double WJQDKYE = 3000000;
		double JYHDXJLR = 2500000;
		
		String riskResult = riskWarningService.ZXYJ007(taskId, WJQDKYE, JYHDXJLR);
		
		System.out.println(riskResult);
	}
	
	@Test
	@Transactional
	public void ZXYJ008() {
		String taskId = "test00001";
		
		double DBJE = 3000000;
		double QYJZC = 2800000;
		
		String riskResult = riskWarningService.ZXYJ008(taskId, DBJE, QYJZC);
		
		System.out.println(riskResult);
	}
	
	@Test
	@Transactional
	public void ZXYJ009() {
		String taskId = "test00001";
		
		int number = 3;
		
		String riskResult = riskWarningService.ZXYJ009(taskId, number);
		
		System.out.println(riskResult);
	}
	
	@Test
	@Transactional
	public void ZXYJ010() {
		String taskId = "test00001";
		
		double DQJK = 560000;
		double CQJK = 600000;
		double YFPJ = 620000;
		double RZYE = 2000000;
		
		String riskResult = riskWarningService.ZXYJ010(taskId, DQJK, CQJK, YFPJ, RZYE);
		
		System.out.println();
	}

	@Override
	public void setApplicationContext(ApplicationContext arg0) throws BeansException {
		// TODO Auto-generated method stub
		applicationContext=arg0;
	}

}
