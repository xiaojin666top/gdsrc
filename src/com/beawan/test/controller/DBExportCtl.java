package com.beawan.test.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.beawan.common.Constants;
import com.platform.util.BeanCreator;
import com.platform.util.JdbcUtil;
import com.platform.util.PropertiesUtil;

@Controller
@RequestMapping({ "/test/dbexport" })
public class DBExportCtl {

	private String[] headerFileds = { "COLUMN_ID", "COLUMN_NAME", "COMMENTS", "DATA_TYPE",
			"DATA_LENGTH", "DATA_SCALE", "IS_PK", "NULLABLE" };
	
	private String[] headerNames = { "序号", "字段英文名", "字段中文名", "字段类型",
			"长度", "精度", "是否主键",  "空值否" };

	private CellStyle headStyle;
	private CellStyle cellStyle;
	private CellStyle linkStyle;

	@RequestMapping("export.do")
	public ResponseEntity<byte[]> export(HttpServletRequest request, HttpServletResponse response) {
		
		ResponseEntity<byte[]> result = null;
		
		try {
			HSSFWorkbook workbook = genWorkbook();
			
			String configFP = "/properties/system.properties";
			String tempPath = PropertiesUtil.getProperty(configFP,
					"survey.report.path");
			File dir = new File(tempPath);
			if (!dir.exists())
				dir.mkdirs();
			File xls = new File(dir, "tcesys.xls");
			// 如果此文件不存在则创建一个
			if (!xls.exists()) {
				xls.createNewFile();
			}
			FileOutputStream fileOut = new FileOutputStream(xls);
			workbook.write(fileOut);// 将workbook中的 内容写入fileOut
			fileOut.flush();
			fileOut.close();
			
			HttpHeaders headers = new HttpHeaders();
			headers.setContentDispositionFormData("attachment", xls.getName());
			headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
			result = new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(xls),
					headers, HttpStatus.OK);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	public HSSFWorkbook genWorkbook() throws Exception {
		
		// 创建一个xls
		HSSFWorkbook workbook = new HSSFWorkbook();
		CreationHelper cHelper = workbook.getCreationHelper();

		headStyle = getHeadCellStyle(workbook);
		cellStyle = getCellStyle(workbook);
		linkStyle = getLinkCellStyle(workbook);

		HSSFSheet sheet = workbook.createSheet("汇总表");// 设置表名
		sheet.setDefaultRowHeightInPoints(20);

		Row header = sheet.createRow(0);
		Cell cell1 = header.createCell(0);
		cell1.setCellValue("表名");
		cell1.setCellStyle(headStyle);
		Cell cell2 = header.createCell(1);
		cell2.setCellValue("备注");
		cell2.setCellStyle(headStyle);

		String tsql = "SELECT TABNAME AS TABLE_NAME,REMARKS AS COMMENTS FROM SYSCAT.TABLES "
				    + "WHERE TABSCHEMA = 'GDTCESYS' ORDER BY TABNAME";
		
		List<Map<String, Object>> results = JdbcUtil.executeQuery(tsql,
				Constants.DataSource.TCE_DS);

		int[] colLengths = { 0, 0 };
		for (int i = 1; i < results.size(); i++) {

			Map<String, Object> result = results.get(i);
			String tabName = (String) result.get("tableName");
			if (tabName != null) {
				int len = tabName.getBytes().length;
				if (len > colLengths[0])
					colLengths[0] = len;
			}
			String remarks = (String) result.get("comments");
			if (remarks != null) {
				int len = remarks.getBytes().length;
				if (len > colLengths[1])
					colLengths[1] = len;
			}

			Row row = sheet.createRow(i);
			cell1 = row.createCell(0);
			cell1.setCellValue(tabName);
			cell1.setCellStyle(cellStyle);
			cell2 = row.createCell(1);
			cell2.setCellValue(remarks);
			cell2.setCellStyle(cellStyle);
			
			genSheet(workbook, cHelper, tabName, remarks);
			
			Hyperlink link = cHelper
					.createHyperlink(Hyperlink.LINK_DOCUMENT);
			link.setAddress("'" + tabName + "'!A1");
			cell1.setHyperlink(link);
			cell1.setCellStyle(linkStyle);

			/*if (remarks != null && !remarks.equals("")
					&& !remarks.equals("null") && !remarks.equals("未用")) {
				genSheet(workbook, cHelper, tabName, remarks);

				Hyperlink link = cHelper
						.createHyperlink(Hyperlink.LINK_DOCUMENT);
				link.setAddress("'" + tabName + "'!A1");
				cell2.setHyperlink(link);
				cell2.setCellStyle(linkStyle);
			}*/
		}

		// 设置列宽度
		sheet.setColumnWidth(0, colLengths[0] * 384);
		sheet.setColumnWidth(1, colLengths[1] * 384);
		sheet.createFreezePane(0, 1);

		return workbook;
	}

	private void genSheet(HSSFWorkbook workbook, CreationHelper cHelper,
			String tabName, String tabCnName) throws Exception {

		HSSFSheet sheet = workbook.createSheet(tabName);// 设置表名
		
		CellRangeAddress cra0 = new CellRangeAddress(0, 0, 1, 7);
		CellRangeAddress cra1 = new CellRangeAddress(1, 1, 1, 7);
		CellRangeAddress cra2 = new CellRangeAddress(2, 2, 1, 7);
		sheet.addMergedRegion(cra0);
		sheet.addMergedRegion(cra1);
		sheet.addMergedRegion(cra2);
		
		Row r0 = sheet.createRow(0);
		Cell r0c0 = r0.createCell(0);
		r0c0.setCellValue("中文名");
		r0c0.setCellStyle(headStyle);
		Cell r0c1 = r0.createCell(1);
		r0c1.setCellValue(tabCnName);
		
		Cell r0c8 = r0.createCell(8);
		r0c8.setCellValue("返回");
		r0c8.setCellStyle(linkStyle);

		Hyperlink link = cHelper.createHyperlink(Hyperlink.LINK_DOCUMENT);
		link.setAddress("'汇总表'!A1");
		r0c8.setHyperlink(link);
		
		Row r1 = sheet.createRow(1);
		Cell r1c0 = r1.createCell(0);
		r1c0.setCellValue("英文名");
		r1c0.setCellStyle(headStyle);
		Cell r1c1 = r1.createCell(1);
		r1c1.setCellValue(tabName);
		
		Row r2 = sheet.createRow(2);
		Cell r2c0 = r2.createCell(0);
		r2c0.setCellValue("描述");
		r2c0.setCellStyle(headStyle);

		Row header = sheet.createRow(3);
		Cell cell;
		int[] colLengths = { 0, 0, 0, 0, 0, 0, 0, 0 };
		for (int i = 0; i < headerNames.length; i++) {
			cell = header.createCell(i);
			String hc = headerNames[i];
			cell.setCellValue(hc);
			cell.setCellStyle(headStyle);

			if (hc.getBytes().length > colLengths[i])
				colLengths[i] = hc.getBytes().length;
		}

		String sql = "SELECT "
				   + "C.COLNO AS COLUMN_ID,"
				   + "C.COLNAME AS COLUMN_NAME,"
				   + "C.REMARKS AS COMMENTS,"
				   + "C.TYPENAME AS DATA_TYPE,"
				   + "C.LENGTH AS DATA_LENGTH,"
				   + "C.SCALE AS DATA_SCALE,"
				   + "CASE WHEN TT.TYPE = 'P' THEN '是' else '' END IS_PK,"
				   + "CASE WHEN C.NULLS = 'N' THEN '否' else '' END NULLABLE "
				   + "FROM SYSCAT.COLUMNS  C "
				   + "LEFT JOIN ("
				   + "        SELECT K.TABSCHEMA,K.TABNAME,K.COLNAME,T.TYPE "
				   + "        FROM SYSCAT.KEYCOLUSE K "
				   + "        LEFT JOIN SYSCAT.TABCONST T "
				   + "        ON T.CONSTNAME = K.CONSTNAME AND  T.TABSCHEMA = K.TABSCHEMA AND T.TABNAME = K.TABNAME"
				   + ") TT "
				   + "ON TT.TABSCHEMA = C.TABSCHEMA AND TT.TABNAME = C.TABNAME AND TT.COLNAME = C.COLNAME "
				   + "WHERE C.TABSCHEMA = 'GDTCESYS' AND C.TABNAME = '" + tabName + "' ORDER BY C.COLNO ASC";
		
		List<Map<String, Object>> results = JdbcUtil.executeQuery(sql,
				Constants.DataSource.TCE_DS);
		for (int i = 0; i < results.size(); i++) {
			Map<String, Object> result = results.get(i);
			Row row = sheet.createRow(i+4);
			for (int j = 0; j < headerFileds.length; j++) {
				cell = row.createCell(j);
				String key = BeanCreator.genFieldName(headerFileds[j]);
				String value = null;
				Object obj = result.get(key);
				if (obj != null)
					value = "" + obj;

				cell.setCellValue(value);
				cell.setCellStyle(cellStyle);

				if (value != null
						&& value.getBytes().length > colLengths[j])
					colLengths[j] = value.getBytes().length;
			}
		}

		for (int i = 0; i < headerFileds.length; i++) {
			if(i == 2)
				sheet.setColumnWidth(i, colLengths[i]*256);
			else
				sheet.setColumnWidth(i, colLengths[i]*384);
		}
		
		sheet.createFreezePane(0, 4);
	}

	private CellStyle getCellStyle(Workbook workbook) {
		// 单元格样式
		CellStyle cellStyle = workbook.createCellStyle();
		Font cellFront = workbook.createFont();
		cellFront.setFontName("Arial");
		cellStyle.setFont(cellFront);
		cellStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		return cellStyle;
	}

	private CellStyle getHeadCellStyle(Workbook workbook) {
		
		// 表头样式
		CellStyle headStyle = workbook.createCellStyle();
		Font headFront = workbook.createFont();
		headFront.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		headFront.setFontName("Arial");
		headStyle.setFont(headFront);
		headStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		headStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		headStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());  
		headStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		headStyle.setBorderTop(CellStyle.BORDER_THIN);
		headStyle.setBorderBottom(CellStyle.BORDER_THIN);
		headStyle.setBorderTop(CellStyle.BORDER_THIN);
		headStyle.setBorderRight(CellStyle.BORDER_THIN);
		headStyle.setBorderLeft(CellStyle.BORDER_THIN);
		
		return headStyle;
	}

	private CellStyle getLinkCellStyle(Workbook workbook) {
		// 超链接样式
		CellStyle linkStyle = workbook.createCellStyle();
		Font linkFront = workbook.createFont();
		linkFront.setFontName("Arial");
		linkFront.setUnderline(Font.U_SINGLE);
		linkFront.setColor(IndexedColors.BLUE.getIndex());
		linkStyle.setFont(linkFront);
		linkStyle.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

		return linkStyle;
	}
	
}
