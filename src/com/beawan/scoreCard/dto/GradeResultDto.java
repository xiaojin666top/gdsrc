package com.beawan.scoreCard.dto;

public class GradeResultDto {

    private String itemName;//指标名称
    private String realValue;//实际值
    private String gradeDesc;//档次 具体说明
}
