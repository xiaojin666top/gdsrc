package com.beawan.scoreCard.dto;


/**
 * 用来评级模型全部数据
 */
public class ScoreCardFullDto {

    private String assessName;//评级模型名称
    private String loanType;    //贷款类型  唯一
    private Integer totalScore;  //总分
    private String quotaName;    //指标名称
    private String quotaType;    //指标类型
    private String parentName;   //父级指标名称
    private String content;      //具体项内容
    private String attrType;//V S 区间值和具体值
    private Double upper;//上限
    private Double down;//下限
    private Integer score;//分数

    private String realVal;//对具体指标项是通过实际值计算的进行赋值
//    select
//    A. NAME assessName,A.LOAN_TYPE loanType,A.TOTAL_SCORE totalScore,
//    C.NAME quotaName,c.TYPE quotaType,D.NAME parentName,
//    E.CONTENT content,E.ATTR_VALUE attrType,E.UPPER upper,E.DOWN down,
//    F.SCORD score
//    FROM  ASSESS_CARD_INFO A
//    JOIN  AC_CARD_QUOTA_REL B ON A.ID=B.ASSESS_CARD_ID
//    JOIN AC_QUOTA C ON B.QUOTA_ID=C.ID
//    LEFT JOIN AC_QUOTA D ON B.QUOTA_PID=D.ID
//    LEFT JOIN AC_QUOTA_ATTR E ON C.ID=E.QUOTA_ID
//    LEFT JOIN AC_SUB_QUOTA_ATTR F ON E.ID=F.ATTR_ID
//    WHERE A.LOAN_TYPE='2'


    public String getAssessName() {
        return assessName;
    }

    public void setAssessName(String assessName) {
        this.assessName = assessName;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public Integer getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Integer totalScore) {
        this.totalScore = totalScore;
    }

    public String getQuotaName() {
        return quotaName;
    }

    public void setQuotaName(String quotaName) {
        this.quotaName = quotaName;
    }

    public String getQuotaType() {
        return quotaType;
    }

    public void setQuotaType(String quotaType) {
        this.quotaType = quotaType;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAttrType() {
        return attrType;
    }

    public void setAttrType(String attrType) {
        this.attrType = attrType;
    }

    public Double getUpper() {
        return upper;
    }

    public void setUpper(Double upper) {
        this.upper = upper;
    }

    public Double getDown() {
        return down;
    }

    public void setDown(Double down) {
        this.down = down;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getRealVal() {
        return realVal;
    }

    public void setRealVal(String realVal) {
        this.realVal = realVal;
    }
}
