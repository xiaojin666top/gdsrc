package com.beawan.scoreCard;


/**
 * 评分卡相关常量
 */
public class AcConstants{
	
	/**贷款种类**/
	public interface LoanType {

//		<option value="1"<#if data.loanType = '1'>selected="selected"</#if>>农业企业类</option>
//							<option value="2"<#if data.loanType = '2'>selected="selected"</#if>>制造业企业</option>
//							<option value="3"<#if data.loanType = '3'>selected="selected"</#if>>商业企业类</option>
//							<option value="4"<#if data.loanType = '4'>selected="selected"</#if>>房地产开发类</option>
//							<option value="5"<#if data.loanType = '5'>selected="selected"</#if>>公用事业类</option>
//							<option value="6"<#if data.loanType = '6'>selected="selected"</#if>>综合类</option>

		/***农业企业类***/
		public static final String AGRICULTURE = " 1";
		/***制造业企业***/
		public static final String INDUSTRY = "2";
		/***商业企业类***/
		public static final String BUSINESS = "3";
		/***房地产开发类***/
		public static final String ESTATE = "4";
		/***公用事业类***/
		public static final String PUB_UTILITY = "5";
		/***综合类***/
		public static final String COMPREHENSIVE = "6";
	}


	/**
	 * 制造业工业 评级模型具体项
	 */
	public interface INDUSTRY_QUOTA{

		public String ECON_STRENGTH = "企业经济实力";
		public String MANAGER_LEVEL = "企业经营管理水平";
		public String CORPORATE_CREDIT = "企业银行信用";
		public String SOLVENCY_INDEX = "偿债能力指标";
		public String NET_PROFIT_GROWTH = "净利润增长率";			//  ✔
		public String RETURN_ON_NET_ASSETS = "净资产利润率";		//  ✔
		public String NET_ASSETS_GROWTH = "净资产增长率";			//  ✔
		public String MATURITY_LOAN_REPAY = "到期贷款偿还率";
		public String DEVELOP_POTENT = "发展潜力";
		public String MARKET_PROSPECT = "同业竞争力与市场前景";
		public String GOODWILL = "商誉";
		public String INVENTORY_TURN = "存货周转率";				//  ✔
		public String NET_ASSET = "实有净资产";					//  ✔
		public String ACCOUNT_RECE_TURN = "应收账款周转率";		//  ✔
		public String EQUIPMENT_LEVEL = "技术力量、工艺装备水平";
		public String TECH_QUALITY = "技术素质";
		public String LEGALER_YEAR = "法定代表人从事本行业年限";		//  ✔
		public String REPUTATION = "法定代表人信誉和还款意愿";
		public String CURRENT_RATIO = "流动比率";					//  ✔
		public String MANAGER_QUALITY = "经营素质";
		public String SETTLE_RECORD = "结算记录";
		public String PROFIT_INDICATOR = "获利能力指标";
		public String OPERATE_CAPACITY = "营运能力指标";
		public String EQUIP_UPDATE = "装备更新情况";
		public String DEBT_RATIO = "负债比率";					//  ✔
		public String LOAN_INTEREST_REPAY = "贷款利息偿还记录";
		public String JOINT_LIABILITY = "连带负债";				//  ✔
		public String QUICK_RATIO = "速动比率";					//  ✔
		public String BANK_DEBT_RATIO = "银行负债率";
		public String PROFIT_MARGIN_OF_SALES = "销售利润率";		//  ✔
		public String SALE_REVENUE_GROWTH = "销售收入增长率";		//  ✔
		public String LONG_TERM_ASSETS = "长期资产(占资产%)";		//  ✔
		public String NON_FINANCE_CASH_INFLOW = "非筹资性现金流入与流动负债比率";	//  ✔
		public String LEADER_QUALITY = "领导者素质";
	}
	
	/**评分卡种类**/
	public interface AssessCardType {
		public static final String ASSESS_CARD_STATE = "ASSESSCARDTYPE";
		public static final String GET_CUST = "01";//获客
		public static final String APPLY = "02";//申请
		public static final String COLLECTION = "03";//催收
		public static final String BEHAVIOR = "04";//行为
	}
	
	/**评分卡状态**/
	public interface AssessCardState {
		public static final String ASSESS_CARD_STATE = "ASSESSCARDSTATE";
		public static final String ENABLE = "E";
		public static final String DISABLE = "D";
	}
	
	/**指标属性类型**/
	public interface AcQuotaAttrType {
		public static final String AC_QUOTA_ATTR_TYPE = "ACQUOTAATTRTYPE";
		public static final String SECTION = "S";
		public static final String VALUE = "V";
	}
	
	/**指标属性类型**/
	public interface SectionType {
		public static final String SECTION_TYPE = "SECTIONTYPE";
		public static final String LEFT = "L";
		public static final String RIGHT = "R";
		public static final String ALL = "A";
	}


	/**
	 * ***********************利率常量
	 */
	/**
	 * 客户忠诚度
	 */
	public interface LOYAL{
		public static final String BASE_ACC = "基本户开立在本行";
		public static final String CREDIT_ACC = "连续在我行贷款3年以上且无违约记录";

	}




	/**
	 * 客户是否在我行贷款三年以上
	 */
	public interface LOAN_HISTORY{
		public static final String NONE = "none";
		public static final String TRUE = "true";
		public static final String FALSE = "false";
	}


	/**
	 * 客户是否循环授信
	 */
	public interface REVOLE_CREDIT{
		public static final String FALSE = "false";
		public static final String TRUE = "true";
	}

	/**
	 * 客户是否循环授信
	 */
	public interface GUARWAY{
		public static final String OTHER_COMP = "第三方企业保证担保";
		public static final String NORMAL_PERSON = "自然人保证担保";
		public static final String MORTGAGE = "抵（质）押物担保";
	}




}
