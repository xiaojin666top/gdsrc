package com.beawan.scoreCard.service;

import java.util.List;

import com.beawan.scoreCard.bean.AcLogRecord;
import com.beawan.scoreCard.bean.AcLogRecordDetail;
import com.beawan.scoreCard.qbean.QueryCondition;


public interface IAcLogRecordSV {
	/**
	 * 分页查询
	 * @param queryCondition
	 * @param sortCondition
	 * @param start
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	public List<AcLogRecord> paginate(QueryCondition queryCondition,String sortCondition,int start,int limit)throws Exception;
	
	/**
	 * 满足记录总数
	 * @param queryCondition
	 * @return
	 * @throws Exception
	 */
	public int totalCount(QueryCondition queryCondition)throws Exception;
	
	public AcLogRecord saveOrUpdateLogRecord(AcLogRecord logRecord);
	
	public void saveOrUpdateLogDetail(AcLogRecordDetail detail);
	
	public AcLogRecord selectLogRecordByCusNo(String customerNo);
	
	public List<AcLogRecordDetail> selectLogRecordDetailsByLogId(Long logId);
}
