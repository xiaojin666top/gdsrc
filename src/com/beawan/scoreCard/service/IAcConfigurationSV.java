package com.beawan.scoreCard.service;

import java.util.List;

import com.beawan.scoreCard.bean.AcConfiguration;
import com.beawan.scoreCard.bean.AcCusLevel;


public interface IAcConfigurationSV {
	/**
	 * 新增或更新
	 * @param configuration
	 * @throws Exception
	 */
	public void saveOrUpdate(AcConfiguration configuration)throws Exception;
	/**
	 * 根据评分卡ID查询
	 * @param assessCardId
	 * @return
	 * @throws Exception
	 */
	public AcConfiguration queryByAcId(long assessCardId)throws Exception;
	/**
	 * 根据参数查询
	 * @param loanType 贷款类型
	 * @param guaranteeType 担保方式
	 * @param cardType 评分卡类型
	 * @return
	 * @throws Exception
	 */
	public AcConfiguration queryEnable(String loanType, String guaranteeType, String cardType)throws Exception;
	/**
	 * 根据评分卡设置对象 获得评分卡分数客户等级指标
	 * @param configuration
	 * @return
	 * @throws Exception 
	 */
	public List<AcCusLevel> getAcCusLevelList(AcConfiguration configuration) throws Exception;

	/**
	 * 根据分数获得客户等级
	 * @param xml
	 * @param score
	 * @return
	 */
	public String getGradeByScore(String xml, double score) throws Exception;
	
	public String getGradeByScore(List<AcCusLevel> acCusLevels, double score)
			throws Exception;
}
