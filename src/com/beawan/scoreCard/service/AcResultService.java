package com.beawan.scoreCard.service;

import com.beawan.core.BaseService;
import com.beawan.scoreCard.bean.AcResult;

/**
 * @author yzj
 */
public interface AcResultService extends BaseService<AcResult> {
}
