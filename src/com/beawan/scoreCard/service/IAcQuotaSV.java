package com.beawan.scoreCard.service;

import java.util.List;

import com.beawan.scoreCard.bean.AcQuota;


public interface IAcQuotaSV {
	/**
	 * 分页查询
	 * @param queryCondition
	 * @param sortCondition
	 * @param start
	 * @param limit
	 * @return
	 * @throws Exception
	 */
    public List<AcQuota> paginate(AcQuota queryCondition,String sortCondition,int start ,int limit)throws Exception;
	/**
	 * 获取符合条件的条数
	 * @param queryCondition
	 * @return
	 * @throws Exception
	 */
	public int totalCount(AcQuota queryCondition)throws Exception;
	
	public List<AcQuota> queryAll() throws Exception;
	
	public AcQuota save(AcQuota acQuota) throws Exception;
	
	public AcQuota queryById(long id) throws Exception ;
	public void deleteNew(long id) throws Exception;
	
	public void update(AcQuota acQuota) throws Exception;
	
	/**
	 * 根据贷款类型和评分卡类型查询指标
	 * @param loanType
	 * @param cardType
	 * @return
	 * @throws Exception
	 */
	public List<AcQuota> queryByLoanType(String loanType)throws Exception;
	
	public List<AcQuota> queryByPid(long pid) throws Exception;
	
}
