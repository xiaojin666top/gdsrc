package com.beawan.scoreCard.service;

import java.util.List;

import com.beawan.scoreCard.bean.AssessCard;


public interface ISmallAssessCardSV {
	public List<AssessCard> paginateQuery(AssessCard queryCondition,
			String sortCondition, int start, int limit)throws Exception;
	
	public int totalCountQuery(AssessCard queryCondition)throws Exception;
	
	
	public AssessCard saveSmallCard(AssessCard assessCard)throws Exception;
	
	public void modifySmallCard(AssessCard assessCard)throws Exception;
	
	public AssessCard queryById(long id)throws Exception;
	
	public AssessCard queryInfoById(long id)throws Exception;
	
	public void deleteSmallCard(long id)throws Exception;


}
