package com.beawan.scoreCard.service;

import java.util.List;

import com.beawan.scoreCard.bean.AssessCard;

public interface IAssessCardSV {
	/**
	 * 根据条件查询记录总数
	 * @param queryCondition 查询条件
	 * @return
	 * @throws Exception
	 */
	public long queryAssessCardTotal(AssessCard queryCondition)throws Exception;
	/**
	 * TODO 根据条件查询分页
	 * @param queryCondition 查询条件
	 * @param orderCondition 排序条件
	 * @param index 起始页数
	 * @param count 每页大小
	 * @return
	 * @throws Exception
	 */
	public List<AssessCard> queryAssessCardPaging(AssessCard queryCondition,String orderCondition, int index, int count)throws Exception;
	
	//根据id查询评分卡
	public AssessCard queryInfoById2(long id) throws Exception;
		
	//保存评分卡(新)
	public AssessCard saveAssessCard2(AssessCard assessCard, String userNo) throws Exception;

	//修改评分卡(新)
	public void modifyAssessCard2(AssessCard assessCard) throws Exception;
	
	//删除评分卡(新)
	public void deleteAssessCard2(long id) throws Exception;

	/**
	 * 计算客户的评级结果
	 * @param serNo			任务流水号
	 * @throws Exception
	 */
	public void syncCalcAssetResult(String serNo) throws Exception;
}
