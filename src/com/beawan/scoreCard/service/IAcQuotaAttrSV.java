package com.beawan.scoreCard.service;

import java.util.List;

import com.beawan.scoreCard.bean.AcQuotaAttr;


public interface IAcQuotaAttrSV {
	
	public List<AcQuotaAttr> queryBySubquotaId(long subquotaId)throws Exception;
	
	public AcQuotaAttr queryById(long quotaAttrId) throws Exception;
	
	public void deleteQuotaAttr(AcQuotaAttr acQuotaAttr) throws Exception;

	public List<AcQuotaAttr> queryAll() throws Exception;

}
