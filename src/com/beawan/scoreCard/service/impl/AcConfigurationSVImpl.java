package com.beawan.scoreCard.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.stereotype.Service;

import com.beawan.scoreCard.AcConstants;
import com.beawan.scoreCard.bean.AcConfiguration;
import com.beawan.scoreCard.bean.AcCusLevel;
import com.beawan.scoreCard.dao.IAcConfigurationDAO;
import com.beawan.scoreCard.service.IAcConfigurationSV;
import com.platform.util.StringUtil;

@Service("acConfigurationSV")
public class AcConfigurationSVImpl implements IAcConfigurationSV {
	
	@Resource
	private IAcConfigurationDAO configurationDAO;

	@Override
	public void saveOrUpdate(AcConfiguration configuration) throws Exception {
		
		Document document = DocumentHelper.createDocument();
		Element root = document.addElement("item");
		for(int i=0; i<configuration.getAcCusLevelList().size(); i++) {
			AcCusLevel acCusLevel = configuration.getAcCusLevelList().get(i);
			Element dimensions = root.addElement("dimensions");
			dimensions.addAttribute("upValue", String.valueOf(acCusLevel.getUpValue()));
			dimensions.addAttribute("leftFlag", acCusLevel.getLeftFlag());
			dimensions.addAttribute("rightFlag", acCusLevel.getRightFlag());
			dimensions.addAttribute("downValue", String.valueOf(acCusLevel.getDownValue()));
			dimensions.addAttribute("flowValue", acCusLevel.getFlowValue());
		}
		configuration.setAcCusLevelSet(document.asXML());
		
		//将已经启用的评分卡设置为禁用状态
		//if(AcConstants.AssessCardState.ENABLE.equals(configuration.getState())){
			/*AcConfiguration existConfiguration = queryEnable(
					configuration.getLoanType(),
					configuration.getGuaranteeType(),
					configuration.getCardType());*/
		AcConfiguration existConfiguration = configurationDAO.queryByAcId(configuration.getAssessCardId());
		/*if(existConfiguration!=null){
			//if(configuration.getId().equals(existConfiguration.getId())){
				existConfiguration.setState(configuration.getState());
				existConfiguration.setCardType(configuration.getCardType());
				existConfiguration.setEnterpriseType(configuration.getEnterpriseType());
				existConfiguration.setFinanceRatio(configuration.getFinanceRatio());
				existConfiguration.setGuaranteeType(configuration.getGuaranteeType());
				existConfiguration.setLoanType(configuration.getLoanType());
				existConfiguration.setPassScore(configuration.getPassScore());
				existConfiguration.setRejectScore(configuration.getRejectScore());
				existConfiguration.setSuggScore(configuration.getSuggScore());
				existConfiguration.setScoreWay(configuration.getScoreWay());
				existConfiguration.setUnfinanceRatio(configuration.getUnfinanceRatio());
				existConfiguration.setAcCusLevelSet(configuration.getAcCusLevelSet());
				configurationDAO.saveOrUpdate(existConfiguration);
			}else{
				existConfiguration.setState(AcConstants.AssessCardState.DISABLE);
				configurationDAO.saveOrUpdate(existConfiguration);
				configurationDAO.saveOrUpdate(configuration);
			}
		}else{
			configurationDAO.saveOrUpdate(configuration);
		}
		*/
		configurationDAO.saveOrUpdate(configuration);
	}

	@Override
	public AcConfiguration queryByAcId(long assessCardId) throws Exception {
		AcConfiguration cf = configurationDAO.queryByAcId(assessCardId);
		if(cf != null){
			cf.setAcCusLevelList(this.getAcCusLevelList(cf));
		}
		return cf;
	}
	
	public AcConfiguration queryEnable(String loanType ,String guaranteeType, String cardType)throws Exception {
		StringBuffer queryString = new StringBuffer("from AcConfiguration as model");
		queryString.append(" where model.state='").append(AcConstants.AssessCardState.ENABLE).append("'");
		queryString.append(" and model.loanType='").append(loanType).append("'");
		queryString.append(" and model.cardType='").append(cardType).append("'");
		if(!StringUtil.isEmptyString(guaranteeType)){
			queryString.append(" and model.guaranteeType='").append(guaranteeType).append("'");
		}
		List<AcConfiguration> list = configurationDAO.queryByCondition(queryString.toString());
		if(!CollectionUtils.isEmpty(list)){
			AcConfiguration cf = list.get(0);
			cf.setAcCusLevelList(this.getAcCusLevelList(cf));
			return cf;
		}else{
			return null;
		}
	}

	@Override
	public List<AcCusLevel> getAcCusLevelList(AcConfiguration configuration) throws Exception {
		if(configuration == null) {
			return null;
		}
		String  acCusLevelSet = configuration.getAcCusLevelSet();
		if(acCusLevelSet == null || acCusLevelSet.trim().equals("")) {
			return null;
		}		
		List<AcCusLevel> acCusLevels = this.acCusLevelListByXml(acCusLevelSet);
        return acCusLevels;
	}
	
	public List<AcCusLevel> acCusLevelListByXml(String xml) throws Exception {
		List<AcCusLevel> acCusLevels = new ArrayList<AcCusLevel>();
		Document document = null;
		document =DocumentHelper.parseText(xml);
		// 获取根元素
        Element root = document.getRootElement();
        @SuppressWarnings("unchecked")
		List<Element> itemElementList = root.elements("dimensions");
        for(int i=0; i<itemElementList.size(); i++) {
        	AcCusLevel acCusLevel = new AcCusLevel();
        	acCusLevel.setUpValue(Double.valueOf(itemElementList.get(i).attributeValue("upValue")));
        	acCusLevel.setLeftFlag(itemElementList.get(i).attributeValue("leftFlag"));
        	acCusLevel.setRightFlag(itemElementList.get(i).attributeValue("rightFlag"));
        	acCusLevel.setDownValue(Double.valueOf(itemElementList.get(i).attributeValue("downValue")));
        	acCusLevel.setFlowValue(itemElementList.get(i).attributeValue("flowValue"));
        	acCusLevels.add(acCusLevel);
        }
        
        return acCusLevels;
	}
	
	/**
	 * 根据xml和分数 获得客户等级
	 * @throws Exception 
	 */
	@Override
	public String getGradeByScore(String xml, double score) throws Exception {
		List<AcCusLevel> acCusLevels = this.acCusLevelListByXml(xml);
		return getGradeByScore(acCusLevels, score);
	}
	
	/**
	 * 根据xml和分数 获得客户等级
	 * @throws Exception 
	 */
	@Override
	public String getGradeByScore(List<AcCusLevel> acCusLevels, double score) throws Exception {
		
		String grade = "";
		
		if(!CollectionUtils.isEmpty(acCusLevels)){
			for(AcCusLevel acCusLevel : acCusLevels) {
				
				String leftFlag = acCusLevel.getLeftFlag();
				String rightFlag = acCusLevel.getRightFlag();
				
				//左闭
				if("C".equals(leftFlag) && score < acCusLevel.getDownValue())
					continue;
				//左开
				if("O".equals(leftFlag) && score <= acCusLevel.getDownValue())
					continue;
				//右闭
				if("C".equals(rightFlag) && score > acCusLevel.getUpValue())
					continue;
				//右开
				if("O".equals(rightFlag) && score >= acCusLevel.getUpValue())
					continue;
				
				grade = acCusLevel.getFlowValue();
				break;
			}
		}
		
		return grade;
	}

}
