package com.beawan.scoreCard.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.beawan.analysis.finansis.dto.FinaRatioDto;
import com.beawan.common.Constants;
import com.beawan.customer.service.FinriskResultService;
import com.beawan.scoreCard.AcConstants;
import com.beawan.scoreCard.bean.AcConfiguration;
import com.beawan.scoreCard.bean.AcCusLevel;
import com.beawan.scoreCard.bean.AcQuota;
import com.beawan.scoreCard.bean.AcQuotaAttr;
import com.beawan.scoreCard.bean.AcQuotaRel;
import com.beawan.scoreCard.bean.AcResult;
import com.beawan.scoreCard.bean.AssessCard;
import com.beawan.scoreCard.bean.SubQuotaAttr;
import com.beawan.scoreCard.dao.AcResultDao;
import com.beawan.scoreCard.dao.IAcConfigurationDAO;
import com.beawan.scoreCard.dao.IAcQuotaAttrDAO;
import com.beawan.scoreCard.dao.IAcQuotaDAO;
import com.beawan.scoreCard.dao.IAcQuotaRelDAO;
import com.beawan.scoreCard.dao.IAssessCardDAO;
import com.beawan.scoreCard.dao.ISubQuotaAttrDAO;
import com.beawan.scoreCard.dto.ScoreCardFullDto;
import com.beawan.scoreCard.service.IAcConfigurationSV;
import com.beawan.scoreCard.service.IAssessCardSV;
import com.beawan.survey.custInfo.dto.CompBaseDto;
import com.beawan.survey.custInfo.service.ICompBaseSV;
import com.beawan.task.bean.Task;
import com.beawan.task.service.ITaskSV;
import com.platform.util.DateUtil;
import com.platform.util.ExceptionUtil;
import com.platform.util.StringUtil;


@Service("assessCardSV")
public class AssessCardSVImpl  implements IAssessCardSV {

	private final static Log log = LogFactory.getLog(IAssessCardSV.class);
	
	@Resource
	private IAssessCardDAO assessCardDAO;
	@Resource
	private IAcQuotaDAO acQuotaDAO;
	@Resource
	private IAcQuotaRelDAO acQuotaRelDAO;
	@Resource
	private ISubQuotaAttrDAO subQuotaAttrDAO;
	@Resource
	private IAcQuotaAttrDAO acQuotaAttrDAO; 
	@Resource
	private IAcConfigurationDAO acConfigurationDAO;

	@Resource
	private ITaskSV taskSV;
	@Resource
	private ICompBaseSV compBaseSV;
	@Resource
	private FinriskResultService finriskResultService;
	@Resource
	private AcResultDao acResultDao;
	@Resource
	private IAcConfigurationSV acConfigurationSV;
	
	
	
	@Override
	public long queryAssessCardTotal(AssessCard queryCondition) throws Exception {
		StringBuffer sqlString = new StringBuffer("from AssessCard as model where 1=1");
		List<Object> params = new ArrayList<Object>();
		if(queryCondition != null){
			if(!StringUtil.isEmptyString(queryCondition.getName())){
				sqlString.append(" and model.name like ? ");
				params.add("%" + queryCondition.getName() +"%");
			}
			if(!StringUtil.isEmptyString(queryCondition.getLoanType())){
				sqlString.append(" and model.loanType= ? ");
				params.add( queryCondition.getLoanType());
			}
		}
		List<AssessCard> list = assessCardDAO.queryCondition(sqlString.toString(), params.toArray());
		if(!CollectionUtils.isEmpty(list)){
			return list.size();
		}else{
			return 0;
		}
	}

	@Override
	public List<AssessCard> queryAssessCardPaging(AssessCard queryCondition, String orderCondition, int index, int count)
			throws Exception {
		StringBuffer sqlString = new StringBuffer("from AssessCard as model where 1=1");
		List<Object> params = new ArrayList<Object>();
		if(queryCondition != null){
			if(!StringUtil.isEmptyString(queryCondition.getName())){
				sqlString.append(" and model.name like ? ");
				params.add("%" + queryCondition.getName() +"%");
			}
			if(!StringUtil.isEmptyString(queryCondition.getLoanType())){
				sqlString.append(" and model.loanType= ? ");
				params.add( queryCondition.getLoanType());
			}
		}
		if(!StringUtil.isEmptyString(orderCondition)){
			sqlString.append(" order by ").append(orderCondition);
		}
		int startIndex = index * count;
		List<AssessCard> list = assessCardDAO.queryPaginate(sqlString.toString(), params.toArray(), startIndex, count);
		return list;
	}


	/**
	 * csk 4.12
	 * 根据id获得评分卡详情(新)
	 */
	@Override
	public AssessCard queryInfoById2(long id) throws Exception {
		AssessCard assessCard = assessCardDAO.queryById(id);
		List<AcQuotaRel> acQuotaRelList = acQuotaRelDAO.queryByAssIdAndPid(id, (long)-1);
		List<AcQuota> unFinanceQuotaList = new ArrayList<AcQuota>();
		for(AcQuotaRel acQuotaRel : acQuotaRelList){
			AcQuota acQuota = acQuotaDAO.queryById(acQuotaRel.getQuotaId());
			acQuota.setWeight(acQuotaRel.getWeight()*100);
			List<AcQuotaRel> quotaRelList = acQuotaRelDAO.queryByAssIdAndPid(id, acQuotaRel.getQuotaId());
			List<AcQuota> subQuotaList = new ArrayList<AcQuota>();
			for(AcQuotaRel quotaRel : quotaRelList) {
				AcQuota subQuota = acQuotaDAO.queryById(quotaRel.getQuotaId());
				subQuota.setWeight(quotaRel.getWeight()*100);
				subQuotaList.add(subQuota);
			}
			for(AcQuota subQuota : subQuotaList){
				List<SubQuotaAttr> subQuotaAttrList = subQuotaAttrDAO.queryByACIdAndQuId(id, subQuota.getId());
				subQuota.setSubQuotaAttrList(subQuotaAttrList);
				List<AcQuotaAttr> acQuotaAttrs = acQuotaAttrDAO.queryByQuotaId(subQuota.getId());
				subQuota.setAcQuotaAttrs(acQuotaAttrs);
				
			}
			acQuota.setChildrenList(subQuotaList);
			unFinanceQuotaList.add(acQuota);
		}
		assessCard.setUnFinanceQuotaList(unFinanceQuotaList);
		return assessCard;
	}
	
	/**
	 * csk 4.12
	 * 保存评分卡(新)
	 * @param assessCard
	 * @return
	 * @throws Exception
	 */
	//@Override
	public AssessCard saveAssessCard2(AssessCard assessCard, String userNo) throws Exception {
		assessCard.setCreateTime(DateUtil.getNowTimestamp());
		assessCard.setCreator(userNo);
		List<AcQuota> unFinanceQuotaList = assessCard.getUnFinanceQuotaList();
		assessCard = assessCardDAO.saveOrUpdate(assessCard);
		for(AcQuota unFinanceQuota : unFinanceQuotaList){

			AcQuotaRel acQuotaRel = new AcQuotaRel();
			acQuotaRel.setAssessCardId(assessCard.getId());
			acQuotaRel.setQuotaId(unFinanceQuota.getId());
			acQuotaRel.setQuotaPid((long) -1);
			acQuotaRel.setType("0");
			acQuotaRel.setWeight(unFinanceQuota.getWeight()/100);//保存小数
			acQuotaRelDAO.saveQuotaRel(acQuotaRel);
			
			for(AcQuota subQuota : unFinanceQuota.getChildrenList()){
				
				AcQuotaRel quotaRel = new AcQuotaRel();
				quotaRel.setAssessCardId(assessCard.getId());
				quotaRel.setQuotaId(subQuota.getId());
				quotaRel.setQuotaPid(unFinanceQuota.getId());
				quotaRel.setType("1");
				quotaRel.setWeight(subQuota.getWeight()/100);//保存小数
				acQuotaRelDAO.saveQuotaRel(quotaRel);
				
				for(SubQuotaAttr subQuotaAttr : subQuota.getSubQuotaAttrList()){
					subQuotaAttr.setAssessCardId(assessCard.getId());
					subQuotaAttrDAO.insert(subQuotaAttr);
				}
			}

		}
		return assessCard;
	}
	
	/**
	 * csk 4.12
	 * 修改评分卡(新)
	 */
	@Override
	public void modifyAssessCard2(AssessCard assessCard) throws Exception {
		long assessCardId = assessCard.getId();
		//删除关联表信息
		List<AcQuotaRel> acQuotaRelList = acQuotaRelDAO.queryByAssIdAndPid(assessCardId, (long)-1);
		for(AcQuotaRel acQuotaRel : acQuotaRelList){
			List<AcQuotaRel> subQuotaList = acQuotaRelDAO.queryByAssIdAndPid(assessCardId, acQuotaRel.getQuotaId());
			
			for(AcQuotaRel subQuota : subQuotaList){
				List<SubQuotaAttr> subQuotaAttrList = subQuotaAttrDAO.queryByACIdAndQuId(assessCardId, subQuota.getQuotaId());
				for(SubQuotaAttr subQuotaAttr : subQuotaAttrList ){
					subQuotaAttrDAO.delete(subQuotaAttr);
				}
				acQuotaRelDAO.delete(subQuota);
			}
			acQuotaRelDAO.delete(acQuotaRel);
		}
		AssessCard oldAssessCard = assessCardDAO.queryById(assessCardId);
		//更新
		oldAssessCard.setName(assessCard.getName());
		oldAssessCard.setCardType(assessCard.getCardType());
		oldAssessCard.setLoanType(assessCard.getLoanType());
		oldAssessCard.setEnterpriseType(assessCard.getEnterpriseType());
		oldAssessCard.setGuaranteeType(assessCard.getGuaranteeType());
		oldAssessCard.setTotalScore(assessCard.getTotalScore());
		oldAssessCard.setDescription(assessCard.getDescription());

		List<AcQuota> unFinanceQuotaList = assessCard.getUnFinanceQuotaList();
		for(AcQuota unFinanceQuota : unFinanceQuotaList){

			AcQuotaRel acQuotaRel = new AcQuotaRel();
			acQuotaRel.setAssessCardId(assessCard.getId());
			acQuotaRel.setQuotaId(unFinanceQuota.getId());
			acQuotaRel.setQuotaPid((long) -1);
			acQuotaRel.setType("0");
			acQuotaRel.setWeight(unFinanceQuota.getWeight()/100);//保存小数
			acQuotaRelDAO.saveQuotaRel(acQuotaRel);
			
			for(AcQuota subQuota : unFinanceQuota.getChildrenList()){
				
				AcQuotaRel quotaRel = new AcQuotaRel();
				quotaRel.setAssessCardId(assessCard.getId());
				quotaRel.setQuotaId(subQuota.getId());
				quotaRel.setQuotaPid(unFinanceQuota.getId());
				quotaRel.setType("1");
				quotaRel.setWeight(subQuota.getWeight()/100);//保存小数
				acQuotaRelDAO.saveQuotaRel(quotaRel);
				
				for(SubQuotaAttr subQuotaAttr : subQuota.getSubQuotaAttrList()){
					subQuotaAttr.setAssessCardId(assessCard.getId());
					subQuotaAttrDAO.insert(subQuotaAttr);
				}
			}

		}
		
		assessCard = assessCardDAO.saveOrUpdate(oldAssessCard);

	}
	
	/**
	 * csk 4.13
	 * 删除评分卡(新)
	 */
	@Override
	public void deleteAssessCard2(long id) throws Exception {
		AcConfiguration acConfiguration = acConfigurationDAO.queryByAcId(id);
		if(acConfiguration !=null && AcConstants.AssessCardState.ENABLE.equals(acConfiguration.getState())){
			ExceptionUtil.throwException("此评分卡正在使用中，不能删除！");
		}
		
		//删除关联表信息
	   List<AcQuotaRel> unRelationList = acQuotaRelDAO.queryByAssIdAndPid(id, (long)-1);
		for(AcQuotaRel unRelation : unRelationList){
			List<AcQuotaRel> subQuotaList = acQuotaRelDAO.queryByAssIdAndPid(id, unRelation.getQuotaId());
			
			for(AcQuotaRel subQuota : subQuotaList){
				List<SubQuotaAttr> subQuotaAttrList = subQuotaAttrDAO.queryByACIdAndQuId(id, subQuota.getQuotaId());
				for(SubQuotaAttr subQuotaAttr : subQuotaAttrList ){
					subQuotaAttrDAO.delete(subQuotaAttr);
				}
				acQuotaRelDAO.delete(subQuota);
			}
			acQuotaRelDAO.delete(unRelation);
		}
		AssessCard assessCard = assessCardDAO.queryById(id);
		//删除评分卡信息
		assessCardDAO.deleteEntity(assessCard);
		
		//删除评分卡配置信息
		acConfigurationDAO.delete(acConfiguration);
	}


	@Override
	public void syncCalcAssetResult(String serNo) throws Exception {
		if(StringUtil.isEmptyString(serNo)){
			ExceptionUtil.throwException("进行对公评级计算异常，业务流水号为空");
		}
		Task task = taskSV.getTaskBySerNo(serNo);
		String customerNo = task.getCustomerNo();
		CompBaseDto compBase = compBaseSV.findCompBaseByCustNo(customerNo);
		//获取客户大类行业
		String majorIndu = compBase.getMajorIndu();
		//根据行业大类获取对应的评级模型
		String loanType = this.formatLoanTypeByIndu(majorIndu);
		//工业类  -->暂时全部使用 制造业工业的评级模板
		if(!AcConstants.LoanType.INDUSTRY.equals(loanType)){
			loanType = AcConstants.LoanType.INDUSTRY;
		}
		AssessCard assCard = assessCardDAO.selectSingleByProperty("loanType", loanType);
		if(assCard==null){
			log.error("当前贷款类型的评级模型不存在，请重试;当前使用评级模型为:" + loanType);
			ExceptionUtil.throwException("当前贷款类型的评级模型不存在，请重试");
		}
		//根据评级模型具体项  对最终评级结果进行计算
		List<ScoreCardFullDto> full = assessCardDAO.getFullByCardId(assCard.getId());

		//涉及财务部分的指标
		List<FinaRatioDto> assessCardNeedRatio = finriskResultService.getAssessCardNeedRatio(serNo);
		//企业法人从业年限
		String legalWorkingYears = compBase.getLegalWorkingYears();

		List<ScoreCardFullDto> scoreCardFullDtos = new ArrayList<>();
		//region  开始分行业的评级模板
		if(AcConstants.LoanType.AGRICULTURE.equals(loanType)){
		}else if(AcConstants.LoanType.INDUSTRY.equals(loanType)){
			scoreCardFullDtos = this.calcIndustry(full, assessCardNeedRatio, legalWorkingYears);
		}
		//endregion

		if(org.springframework.util.CollectionUtils.isEmpty(scoreCardFullDtos)){
			ExceptionUtil.throwException("评级细项数据为空");
		}
		//去掉历史数据
		List<AcResult> hists = acResultDao.selectByProperty("serNo", serNo);
		if(!CollectionUtils.isEmpty(hists)){
			for(AcResult hist : hists){
				hist.setStatus(Constants.DELETE);
			}
			acResultDao.batchSaveUpdata(hists);
		}

		//评级结果的总分
		int score = 0;
		//对细项内容进行保存
		List<AcResult> results = new ArrayList<>();
		for(ScoreCardFullDto dto : scoreCardFullDtos){
			AcResult acResult = new AcResult();
			acResult.setSerNo(serNo);
			acResult.setAssessName(dto.getAssessName());
			acResult.setLoanType(dto.getLoanType());
			acResult.setQuotaName(dto.getQuotaName());
			acResult.setQuotaType(dto.getQuotaType());
			acResult.setParentName(dto.getParentName());
			acResult.setContent(dto.getContent());
			acResult.setAttrType(dto.getAttrType());
			acResult.setUpper(dto.getUpper());
			acResult.setDown(dto.getDown());
			acResult.setSccre(dto.getScore());
			acResult.setRealVal(dto.getRealVal());
			results.add(acResult);
			score += dto.getScore();
		}
		acResultDao.batchSaveUpdata(results);

		//保存评级结果到  贷前 审查（合规和授信审查）
		AcConfiguration configuration = acConfigurationSV.queryByAcId(assCard.getId());
		List<AcCusLevel> acCusLevelList = configuration.getAcCusLevelList();
		if(acCusLevelList==null || acCusLevelList.size()==0){
			ExceptionUtil.throwException("评级设置内容为空，请设置评级结果后进行测算");
		}
		//评级结果
		String gradeResult = "";
		for(AcCusLevel level : acCusLevelList){
			String leftFlag = level.getLeftFlag();
			String rightFlag = level.getRightFlag();
			double upValue = level.getUpValue();
			double downValue = level.getDownValue();
			if(leftFlag.equals("C") && rightFlag.equals("C")){
				if(score>=downValue && score <= upValue){
					gradeResult = level.getFlowValue();
					break;
				}
			}else if(leftFlag.equals("C") && rightFlag.equals("O")){
				if(score>=downValue && score < upValue){
					gradeResult = level.getFlowValue();
					break;
				}
			}else if(leftFlag.equals("O") && rightFlag.equals("C")){
				if(score>downValue && score <= upValue){
					gradeResult = level.getFlowValue();
					break;
				}
			}else if(leftFlag.equals("O") && rightFlag.equals("O")){
				if(score > downValue && score < upValue){
					gradeResult = level.getFlowValue();
					break;
				}
			}
		}

		task.setDgGrade(gradeResult);
		taskSV.saveOrUpdate(task);

		//结束
		log.info("客户号：" + serNo + " 评级成功，评级结果为:" + gradeResult);

	}

	/**
	 *
	 * 对2017行业国标进行分类---》一一映射到各个评级模型
	 */
	public String formatLoanTypeByIndu(String majorIndu){
		if(StringUtil.isEmptyString(majorIndu)){
			//默认使用工业制造业
			return AcConstants.LoanType.INDUSTRY;
		}
		//对2017国标行业进行整体分类
		List<String> agriStr = Arrays.asList(new String[]{"A"});
		List<String> induStr = Arrays.asList(new String[]{"B", "C", "D", "I"});
		List<String> busiStr = Arrays.asList(new String[]{"F", "H", "R", "R", "J"});
		List<String> estateStr = Arrays.asList(new String[]{"E", "K"});
		List<String> pubStr = Arrays.asList(new String[]{"G", "M", "N", "P", "Q", "S"});
		List<String> comprehenStr = Arrays.asList(new String[]{"L", "O", "T"});

		//农业
		if(agriStr.contains(majorIndu)){
			return AcConstants.LoanType.AGRICULTURE;
		}else if(induStr.contains(majorIndu)){			//制造业工业
			return AcConstants.LoanType.INDUSTRY;
		}else if(busiStr.contains(majorIndu)){			//商业
			return AcConstants.LoanType.BUSINESS;
		}else if(estateStr.contains(majorIndu)){		//房地产
			return AcConstants.LoanType.ESTATE;
		}else if(pubStr.contains(majorIndu)){			//公共事业
			return AcConstants.LoanType.PUB_UTILITY;
		}else if(comprehenStr.contains(majorIndu)){		//综合
			return AcConstants.LoanType.COMPREHENSIVE;
		}
		return AcConstants.LoanType.INDUSTRY;
	}

	/**
	 * 对制造业类企业进行评级结果测算
	 * @param full
	 */
	public List<ScoreCardFullDto> calcIndustry(List<ScoreCardFullDto> full, List<FinaRatioDto> assessCardNeedRatio,
											   String legalWorkingYears){
		//一个指标符合的 结果集合
		List<ScoreCardFullDto> result = new ArrayList<>();
		int workYear = 0;
		if(!StringUtil.isEmptyString(legalWorkingYears)){
			workYear = Integer.parseInt(legalWorkingYears);
		}

		Map<String, List<ScoreCardFullDto>> map = full.stream().collect(Collectors.groupingBy(ScoreCardFullDto::getQuotaName));
		for(Map.Entry<String, List<ScoreCardFullDto>> entry : map.entrySet()){
			String key = entry.getKey();
			List<ScoreCardFullDto> value = entry.getValue();
			FinaRatioDto ratioByName = this.getRatioByName(assessCardNeedRatio, key);
			if(ratioByName!=null) {
				switch (key) {
					case AcConstants.INDUSTRY_QUOTA.NET_PROFIT_GROWTH:
						for(ScoreCardFullDto dto : value){
							Double upper = dto.getUpper();
							Double down = dto.getDown();
							String ratioStr = ratioByName.getThirdYearValue();
							Double ratio = Double.parseDouble(ratioStr.substring(0, ratioStr.length() - 1));
							if(ratio>=down && ratio<upper){
								dto.setRealVal(ratio + "%");
								result.add(dto);
								break;
							}
						}
						break;
					case AcConstants.INDUSTRY_QUOTA.INVENTORY_TURN:
						for(ScoreCardFullDto dto : value){
							Double upper = dto.getUpper();
							Double down = dto.getDown();
							String ratioStr = ratioByName.getThirdYearValue();
							Double ratio = Double.parseDouble(ratioStr);
							if(ratio>=down && ratio<upper){
								dto.setRealVal(ratio+"%");
								result.add(dto);
								break;
							}
						}
						break;
					case AcConstants.INDUSTRY_QUOTA.ACCOUNT_RECE_TURN:
						for(ScoreCardFullDto dto : value){
							Double upper = dto.getUpper();
							Double down = dto.getDown();
							String ratioStr = ratioByName.getThirdYearValue();
							Double ratio = Double.parseDouble(ratioStr);
							if(ratio>=down && ratio<upper){
								dto.setRealVal(ratio+"%");
								result.add(dto);
								break;
							}
						}
						break;
					case AcConstants.INDUSTRY_QUOTA.LEGALER_YEAR:
						for(ScoreCardFullDto dto : value){
							Double upper = dto.getUpper();
							Double down = dto.getDown();
							if(workYear>=down && workYear<upper){
								dto.setRealVal(workYear+"年");
								result.add(dto);
								break;
							}
						}
						break;
					case AcConstants.INDUSTRY_QUOTA.CURRENT_RATIO:
						for(ScoreCardFullDto dto : value){
							Double upper = dto.getUpper();
							Double down = dto.getDown();
							String ratioStr = ratioByName.getThirdYearValue();
							Double ratio = Double.parseDouble(ratioStr.substring(0, ratioStr.length() - 1));
							if(ratio>=down && ratio<upper){
								dto.setRealVal(ratio+"%");
								result.add(dto);
								break;
							}
						}
						break;
					case AcConstants.INDUSTRY_QUOTA.DEBT_RATIO:
						for(ScoreCardFullDto dto : value){
							Double upper = dto.getUpper();
							Double down = dto.getDown();
							String ratioStr = ratioByName.getThirdYearValue();
							Double ratio = Double.parseDouble(ratioStr.substring(0, ratioStr.length() - 1));
							if(ratio>=down && ratio<upper){
								dto.setRealVal(ratio+"%");
								result.add(dto);
								break;
							}
						}
						break;
					case AcConstants.INDUSTRY_QUOTA.QUICK_RATIO:
						for(ScoreCardFullDto dto : value){
							Double upper = dto.getUpper();
							Double down = dto.getDown();
							String ratioStr = ratioByName.getThirdYearValue();
							Double ratio = Double.parseDouble(ratioStr.substring(0, ratioStr.length() - 1));
							if(ratio>=down && ratio<upper){
								dto.setRealVal(ratio+"%");
								result.add(dto);
								break;
							}
						}
						break;
					case AcConstants.INDUSTRY_QUOTA.PROFIT_MARGIN_OF_SALES:
						for(ScoreCardFullDto dto : value){
							Double upper = dto.getUpper();
							Double down = dto.getDown();
							String ratioStr = ratioByName.getThirdYearValue();
							Double ratio = Double.parseDouble(ratioStr.substring(0, ratioStr.length() - 1));
							if(ratio>=down && ratio<upper){
								dto.setRealVal(ratio+"%");
								result.add(dto);
								break;
							}
						}
						break;
					case AcConstants.INDUSTRY_QUOTA.SALE_REVENUE_GROWTH:
						for(ScoreCardFullDto dto : value){
							Double upper = dto.getUpper();
							Double down = dto.getDown();
							String ratioStr = ratioByName.getThirdYearValue();
							Double ratio = Double.parseDouble(ratioStr.substring(0, ratioStr.length() - 1));
							if(ratio>=down && ratio<upper){
								dto.setRealVal(ratio+"%");
								result.add(dto);
								break;
							}
						}
						break;
				}
			}else{
				//这是父级标签  无效跳过就行
				if(value!=null && value.size()==1 && StringUtil.isEmptyString(value.get(0).getParentName())){
					continue;
				}
				if(value==null || value.size()==0){
					System.out.println(value);
				}
				ScoreCardFullDto avg = this.getScoreAvg(value);
				result.add(avg);
			}
		}
		return result;
	}

	/**
	 * 从 所需的财务比率指标中  根据指标名称 获取特定的值
	 * @param assessCardNeedRatio
	 * @param name
	 * @return
	 */
	public FinaRatioDto getRatioByName(List<FinaRatioDto> assessCardNeedRatio, String name){
		if(assessCardNeedRatio==null || assessCardNeedRatio.size()==0){
			return null;
		}
		for(FinaRatioDto dto : assessCardNeedRatio){
			if(dto.getName().equals(name)){
				return dto;
			}
		}
		return null;
	}

	/**
	 * 对指标项进行默认赋值  取中间值
	 * @param list
	 * @return
	 */
	public ScoreCardFullDto getScoreAvg(List<ScoreCardFullDto> list){
		Collections.sort(list, new Comparator<ScoreCardFullDto>() {
			@Override
			public int compare(ScoreCardFullDto o1, ScoreCardFullDto o2) {
				if(o1.getScore()==null || o2.getScore()==null){
					System.out.println(123);
				}
				return o1.getScore() - o2.getScore();
			}
		});
		int size = list.size();
		int midIndex = size / 2;
		return list.get(midIndex);

	}

}
