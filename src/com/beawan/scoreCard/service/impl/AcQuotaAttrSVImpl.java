package com.beawan.scoreCard.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.scoreCard.bean.AcQuotaAttr;
import com.beawan.scoreCard.dao.IAcQuotaAttrDAO;
import com.beawan.scoreCard.service.IAcQuotaAttrSV;

@Service("acQuotaAttrSV")
public class AcQuotaAttrSVImpl implements IAcQuotaAttrSV {
	@Resource
	private IAcQuotaAttrDAO acQuotaAttrDAO;

	@Override
	public List<AcQuotaAttr> queryBySubquotaId(long subquotaId)
			throws Exception {
		return acQuotaAttrDAO.queryByQuotaId(subquotaId);
	}

	@Override
	public AcQuotaAttr queryById(long quotaAttrId) throws Exception {
		return acQuotaAttrDAO.queryById(quotaAttrId);
	}

	@Override
	public void deleteQuotaAttr(AcQuotaAttr acQuotaAttr) throws Exception {
		acQuotaAttrDAO.delete(acQuotaAttr);
	}

	@Override
	public List<AcQuotaAttr> queryAll() throws Exception{
		return acQuotaAttrDAO.queryAll();
	}

}
