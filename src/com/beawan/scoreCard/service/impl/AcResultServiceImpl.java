package com.beawan.scoreCard.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.beawan.core.BaseDao;
import com.beawan.core.BaseServiceImpl;
import com.beawan.scoreCard.bean.AcResult;
import com.beawan.scoreCard.dao.AcResultDao;
import com.beawan.scoreCard.service.AcResultService;

/**
 * @author yzj
 */
@Service("acResultService")
public class AcResultServiceImpl extends BaseServiceImpl<AcResult> implements AcResultService{
    /**
    * 注入DAO
    */
    @Resource(name = "acResultDao")
    public void setDao(BaseDao<AcResult> dao) {
        super.setDao(dao);
    }
    @Resource
    public AcResultDao acResultDao;
}
