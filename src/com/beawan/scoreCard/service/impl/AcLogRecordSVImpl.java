package com.beawan.scoreCard.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.beawan.scoreCard.bean.AcLogRecord;
import com.beawan.scoreCard.bean.AcLogRecordDetail;
import com.beawan.scoreCard.dao.IAcLogRecordDAO;
import com.beawan.scoreCard.dao.IAcLogRecordDetailDAO;
import com.beawan.scoreCard.qbean.QueryCondition;
import com.beawan.scoreCard.service.IAcLogRecordSV;
import com.platform.util.StringUtil;


@Service("acLogRecordSV")
public class AcLogRecordSVImpl implements IAcLogRecordSV {
	@Resource
	private IAcLogRecordDAO acLogRecordDAO;
	@Resource
	private IAcLogRecordDetailDAO acLogRecordDetailDAO;
	@Override
	public List<AcLogRecord> paginate(QueryCondition queryCondition, String sortCondition, int start, int limit)
			throws Exception {
		StringBuffer queryString=new StringBuffer("from AcLogRecord as model where 1=1");
		List<Object> params = new ArrayList<Object>();
		if(queryCondition!=null){
			if(!StringUtil.isEmptyString(queryCondition.getLoanType())){
				queryString.append(" and model.loanType = ? ");
				params.add(queryCondition.getLoanType());
			}
			if (!StringUtil.isEmptyString(queryCondition.getStartTime())) {
				queryString.append(" and to_char(model.logTime,'yyyy-mm-dd hh24:mi:ss') >= ? ");
				params.add(queryCondition.getStartTime());
			}
			if (!StringUtil.isEmptyString(queryCondition.getEndTime())) {
				queryString.append(" and to_char(model.logTime,'yyyy-mm-dd hh24:mi:ss') <= ? ");
				params.add(queryCondition.getEndTime());
			}
		}
		if(!StringUtil.isEmptyString(sortCondition)){
			queryString.append(" order by ").append(sortCondition);
		}
		int startIndex = start * limit;
		return acLogRecordDAO.queryPaginate(queryString.toString(), params.toArray(), startIndex, limit);
	}

	@Override
	public int totalCount(QueryCondition queryCondition) throws Exception {
		StringBuffer queryString=new StringBuffer("from AcLogRecord as model where 1=1");
		List<Object> params = new ArrayList<Object>();
		if(queryCondition!=null){
			if(!StringUtil.isEmptyString(queryCondition.getLoanType())){
				queryString.append(" and model.loanType = ? ");
				params.add(queryCondition.getLoanType());
			}
			if (!StringUtil.isEmptyString(queryCondition.getStartTime())) {
				queryString.append(" and to_char(model.logTime,'yyyy-mm-dd hh24:mi:ss') >= ? ");
				params.add(queryCondition.getStartTime());
			}
			if (!StringUtil.isEmptyString(queryCondition.getEndTime())) {
				queryString.append(" and to_char(model.logTime,'yyyy-mm-dd hh24:mi:ss') <= ? ");
				params.add(queryCondition.getEndTime());
			}
		}
		List<AcLogRecord> list = acLogRecordDAO.queryByCondition(queryString.toString(), params.toArray());
		if(!CollectionUtils.isEmpty(list)){
			return list.size();
		}else{
			return 0;
		}
	}

	@Override
	public AcLogRecord saveOrUpdateLogRecord(AcLogRecord logRecord) {
		// TODO Auto-generated method stub
		 logRecord = acLogRecordDAO.saveOrUpdate(logRecord);
		 return logRecord;
	}

	@Override
	public void saveOrUpdateLogDetail(AcLogRecordDetail detail) {
		// TODO Auto-generated method stub
		acLogRecordDetailDAO.saveOrUpdate(detail);
	}
	
	@Override
	public AcLogRecord selectLogRecordByCusNo(String customerNo){
		return acLogRecordDAO.selectSingleByProperty("serialNumber", customerNo);
	}
	
	@Override
	public List<AcLogRecordDetail> selectLogRecordDetailsByLogId(Long logId){
		return acLogRecordDetailDAO.selectByProperty("logId", logId);
	}
	
}
