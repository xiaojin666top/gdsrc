package com.beawan.scoreCard.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.beawan.scoreCard.bean.AcQuota;
import com.beawan.scoreCard.bean.AcQuotaAttr;
import com.beawan.scoreCard.bean.AcQuotaRel;
import com.beawan.scoreCard.dao.IAcQuotaAttrDAO;
import com.beawan.scoreCard.dao.IAcQuotaDAO;
import com.beawan.scoreCard.dao.IAcQuotaRelDAO;
import com.beawan.scoreCard.service.IAcQuotaSV;
import com.platform.util.ExceptionUtil;
import com.platform.util.StringUtil;

@Service("acQuotaSV")
public class AcQuotaSVImpl implements IAcQuotaSV  {
	@Resource
	private IAcQuotaDAO acQuotaDAO;
	@Resource
	private IAcQuotaAttrDAO acQuotaAttrDAO;
	@Resource
	private IAcQuotaRelDAO acQuotaRelDAO;
	
	@Override
	public List<AcQuota> paginate(AcQuota queryCondition, String sortCondition, int start, int limit) throws Exception {
		StringBuffer queryString = new StringBuffer("from AcQuota as model where 1=1 and model.pid=null");
		List<Object> params = new ArrayList<Object>();
		if(queryCondition!=null){
			if(!StringUtil.isEmptyString(queryCondition.getLoanType())){
				queryString.append(" and ( model.loanType = ? or model.loanType ='ALL' )");
				params.add(queryCondition.getLoanType());
			}
			if(!StringUtil.isEmptyString(queryCondition.getCardType())){
				queryString.append(" and ( model.cardType = ? or model.cardType ='00' )");
				params.add(queryCondition.getCardType());
			}
		}
		queryString.append(" order by model.loanType");
		int startIndex = start * limit;
		return acQuotaDAO.queryPaginate(queryString.toString(),params.toArray(), startIndex, limit);
	}

	@Override
	public int totalCount(AcQuota queryCondition) throws Exception {
		StringBuffer queryString = new StringBuffer("from AcQuota as model where 1=1 and model.pid=null");
		List<Object> params = new ArrayList<Object>();
		if(queryCondition!=null){
			if(!StringUtil.isEmptyString(queryCondition.getLoanType())){
				queryString.append(" and ( model.loanType = ? or model.loanType ='ALL' )");
				params.add( queryCondition.getLoanType());
			}
			if(!StringUtil.isEmptyString(queryCondition.getCardType())){
				queryString.append(" and ( model.cardType = ? or model.cardType ='00' )");
				params.add(queryCondition.getCardType());
			}
		}
		List<AcQuota> list = acQuotaDAO.queryByCondition(queryString.toString(), params.toArray());
		if(!CollectionUtils.isEmpty(list)){
			return list.size();
		}else{
			return 0;
		}
	}

	@Override
	public List<AcQuota> queryAll() throws Exception {
		return acQuotaDAO.queryAll();
	}

	@Override
	public AcQuota save(AcQuota acQuota) throws Exception  {
		List<AcQuota> childrenList = acQuota.getChildrenList();
		acQuota = acQuotaDAO.saveOrUpdate(acQuota);
		for(AcQuota child : childrenList){
			List<AcQuotaAttr> acQuotaAttrs = child.getAcQuotaAttrs();
			child.setPid(acQuota.getId());
			child = acQuotaDAO.saveOrUpdate(child);
			for(AcQuotaAttr attr : acQuotaAttrs){
				attr.setQuotaId(child.getId());
				acQuotaAttrDAO.save(attr);
			}
		}
		return acQuota;
	}

	@Override
	public AcQuota queryById(long id) throws Exception {
		AcQuota acQuota = acQuotaDAO.queryById(id);
		List<AcQuota> childrenList = acQuotaDAO.queryByPid(id);
		for(AcQuota child : childrenList){
			List<AcQuotaAttr> acQuotaAttrList =acQuotaAttrDAO.queryByQuotaId(child.getId());
			child.setAcQuotaAttrs(acQuotaAttrList);
		}
		acQuota.setChildrenList(childrenList);
		return acQuota;
	}

	@Override
	public void deleteNew(long id) throws Exception {
		List<AcQuotaRel> acQuotaRels = acQuotaRelDAO.queryByQuotaId(id);
		if(acQuotaRels != null && acQuotaRels.size() > 0) {
			ExceptionUtil.throwException("此指标被应用在评分卡中，不能删除！");
		}
		
		AcQuota acQuota = acQuotaDAO.queryById(id);
		List<AcQuota> childrenList = acQuotaDAO.queryByPid(id);
		for(AcQuota child : childrenList){
			List<AcQuotaAttr> acQuotaAttrList =acQuotaAttrDAO.queryByQuotaId(child.getId());
			for(AcQuotaAttr acQuotaAttr : acQuotaAttrList){
				acQuotaAttrDAO.deleteEntity(acQuotaAttr);
			}
			acQuotaDAO.deleteEntity(child);
		}
		acQuotaDAO.deleteEntity(acQuota);
		
	}
	
	@Override
	public void update(AcQuota acQuota) throws Exception {
		try{
			List<AcQuota> newChildrenList = acQuota.getChildrenList();
			for(AcQuota newChild : newChildrenList){
				List<AcQuotaAttr> acQuotaAttrs = newChild.getAcQuotaAttrs();
				newChild = acQuotaDAO.saveOrUpdate(newChild);
				for(AcQuotaAttr attr : acQuotaAttrs){
					attr.setQuotaId(newChild.getId());
					acQuotaAttrDAO.saveOrUpdate(attr);
				}
			}
			acQuotaDAO.saveOrUpdate(acQuota);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public List<AcQuota> queryByLoanType(String loanType) throws Exception {
		StringBuffer queryString = new StringBuffer("from AcQuota as model where 1=1");
		queryString.append(" and ( model.loanType='").append(loanType)
		.append("' or model.loanType='ALL' )");
		return acQuotaDAO.queryByCondition(queryString.toString());
	}
	
	@Override
	public List<AcQuota> queryByPid(long pid) throws Exception {
		List<AcQuota> list = acQuotaDAO.queryByPid(pid);
		return list;
	}

}
