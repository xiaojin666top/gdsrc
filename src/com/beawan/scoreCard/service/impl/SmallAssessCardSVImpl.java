package com.beawan.scoreCard.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.beawan.scoreCard.AcConstants;
import com.beawan.scoreCard.bean.AcConfiguration;
import com.beawan.scoreCard.bean.AcQuota;
import com.beawan.scoreCard.bean.AcQuotaAttr;
import com.beawan.scoreCard.bean.AssessCard;
import com.beawan.scoreCard.bean.FinanceQuotaRelation;
import com.beawan.scoreCard.bean.SubQuotaAttr;
import com.beawan.scoreCard.bean.UnFinaceQuotaRelation;
import com.beawan.scoreCard.dao.IAcConfigurationDAO;
import com.beawan.scoreCard.dao.IAcQuotaAttrDAO;
import com.beawan.scoreCard.dao.IAcQuotaDAO;
import com.beawan.scoreCard.dao.IAssessCardDAO;
import com.beawan.scoreCard.dao.IFinanceQuotaRelationDAO;
import com.beawan.scoreCard.dao.ISubQuotaAttrDAO;
import com.beawan.scoreCard.dao.IUnFinanceQuotaRelationDAO;
import com.beawan.scoreCard.service.ISmallAssessCardSV;
import com.platform.util.DateUtil;
import com.platform.util.ExceptionUtil;
import com.platform.util.StringUtil;


@Service("smallAssessCardSV")
public class SmallAssessCardSVImpl implements ISmallAssessCardSV{
	@Resource
	private IAssessCardDAO assessCardDAO;
	@Resource
	private ISubQuotaAttrDAO subQuotaAttrDAO;
	@Resource
	private IUnFinanceQuotaRelationDAO unFinanceQuotaRelationDAO;
	@Resource
	private IFinanceQuotaRelationDAO financeQuotaRelationDAO;
	@Resource
	private IAcQuotaDAO acQuotaDAO;
	@Resource
	private IAcQuotaAttrDAO acQuotaAttrDAO;
	@Resource
	private IAcConfigurationDAO acConfigurationDAO;
	
	
	@Override
	public List<AssessCard> paginateQuery(AssessCard queryCondition, String sortCondition, int start, int limit)
			throws Exception {
        StringBuffer queryString = new StringBuffer("from AssessCard as model where 1=1");
		List<Object> param = new ArrayList<Object>();
		if(queryCondition!=null){
			if(!StringUtil.isEmptyString(queryCondition.getLoanType())){
				queryString.append(" and ( model.loanType = ? or model.loanType = 'ALL' )");
				param.add(queryCondition.getLoanType());
			}
			if(!StringUtil.isEmptyString(queryCondition.getCardType())){
				queryString.append(" and model.cardType = ? ");
				param.add(queryCondition.getCardType());
			}
			if(!StringUtil.isEmptyString(queryCondition.getEnterpriseType())){
				queryString.append(" and model.enterpriseType = '");
				param.add(queryCondition.getEnterpriseType());
			}
			if(!StringUtil.isEmptyString(queryCondition.getGuaranteeType())){
				queryString.append(" and ( model.guaranteeType = ? or model.guaranteeType = '0' )");
				param.add(queryCondition.getGuaranteeType());
			}
			if(!StringUtil.isEmptyString(queryCondition.getName())){
				queryString.append(" and model.name like ? ");
				param.add("%" + queryCondition.getName() + "%");
			}
		}
		if(!StringUtil.isEmptyString(sortCondition)){
			queryString.append(" ORDER BY ").append(sortCondition);
		}
		int startIndex = start * limit;
		List<AssessCard> list = assessCardDAO.queryPaginate(queryString.toString(),param.toArray(), startIndex, limit);
		return list;
	}

	@Override
	public int totalCountQuery(AssessCard queryCondition) throws Exception {
		StringBuffer queryString = new StringBuffer("from AssessCard as model where 1=1");
		List<Object> param = new ArrayList<Object>();
		if(queryCondition!=null){
			if(!StringUtil.isEmptyString(queryCondition.getLoanType())){
				queryString.append(" and ( model.loanType = ? or model.loanType = 'ALL' )");
				param.add(queryCondition.getLoanType());
			}
			if(!StringUtil.isEmptyString(queryCondition.getCardType())){
				queryString.append(" and model.cardType = ? ");
				param.add(queryCondition.getCardType());
			}
			if(!StringUtil.isEmptyString(queryCondition.getEnterpriseType())){
				queryString.append(" and model.enterpriseType = '");
				param.add(queryCondition.getEnterpriseType());
			}
			if(!StringUtil.isEmptyString(queryCondition.getGuaranteeType())){
				queryString.append(" and ( model.guaranteeType = ? or model.guaranteeType = '0' )");
				param.add(queryCondition.getGuaranteeType());
			}
			if(!StringUtil.isEmptyString(queryCondition.getName())){
				queryString.append(" and model.name like ? ");
				param.add("%" + queryCondition.getName() + "%");
			}
		}
		List<AssessCard> list = assessCardDAO.queryCondition(queryString.toString(), param.toArray());
		if(!CollectionUtils.isEmpty(list)){
			return list.size();
		}else{
			return 0;
		}
	}


	@Override
	public AssessCard saveSmallCard(AssessCard assessCard) throws Exception {
		assessCard.setCreateTime(DateUtil.getNowTimestamp());
		assessCard.setCreator("123456");
		List<AcQuota> unFinanceQuotaList = assessCard.getUnFinanceQuotaList();
		List<FinanceQuotaRelation> financeQuotaList = assessCard.getFinanceQuotaList();
		assessCard = assessCardDAO.saveOrUpdate(assessCard);
		for(AcQuota unFinanceQuota :  unFinanceQuotaList){
			String subquotaIds = "";
			UnFinaceQuotaRelation unRelation = new UnFinaceQuotaRelation();
			unRelation.setAssessCardId(assessCard.getId());
			unRelation.setQuotaId(unFinanceQuota.getId());
			for(AcQuota subQuota : unFinanceQuota.getChildrenList()){
				subquotaIds += subQuota.getId() +",";
				for(SubQuotaAttr subQuotaAttr : subQuota.getSubQuotaAttrList()){
					subQuotaAttr.setAssessCardId(assessCard.getId());
					subQuotaAttrDAO.insert(subQuotaAttr);
				}
			}
			unRelation.setSubQuotaIds(subquotaIds.substring(0, subquotaIds.length()-1));
			unFinanceQuotaRelationDAO.save(unRelation);
		}
		if(financeQuotaList!=null && financeQuotaList.size()>0){
			for(FinanceQuotaRelation relation : financeQuotaList){
				relation.setAssessCardId(assessCard.getId());
				financeQuotaRelationDAO.save(relation);
			}
		}
		return assessCard;
	}

	@Override
	public void modifySmallCard(AssessCard assessCard) throws Exception {
		long assessCardId = assessCard.getId();
		//删除关联表信息
		List<UnFinaceQuotaRelation> unRelationList = unFinanceQuotaRelationDAO.queryByAssessCardId(assessCardId);
		for(UnFinaceQuotaRelation unRelation : unRelationList){
			List<AcQuota> subQuotaList = acQuotaDAO.queryByCondition("from AcQuota as model where model.id in("
					+ unRelation.getSubQuotaIds() + ") order by model.id");
			for(AcQuota subQuota : subQuotaList){
				List<SubQuotaAttr> subQuotaAttrList = subQuotaAttrDAO.queryByACIdAndQuId(assessCardId, subQuota.getId());
				for(SubQuotaAttr subQuotaAttr : subQuotaAttrList ){
					subQuotaAttrDAO.delete(subQuotaAttr);
				}
			}
			unFinanceQuotaRelationDAO.delete(unRelation);
		}
		List<FinanceQuotaRelation> financeQuotaRelationList=financeQuotaRelationDAO.queryByAsscardId(assessCardId);
		for(FinanceQuotaRelation relation : financeQuotaRelationList){
			financeQuotaRelationDAO.delete(relation);
		}
		AssessCard oldAssessCard = assessCardDAO.queryById(assessCardId);
		//更新
		oldAssessCard.setName(assessCard.getName());
		oldAssessCard.setCardType(assessCard.getCardType());
		oldAssessCard.setLoanType(assessCard.getLoanType());
		oldAssessCard.setEnterpriseType(assessCard.getEnterpriseType());
		oldAssessCard.setGuaranteeType(assessCard.getGuaranteeType());
		oldAssessCard.setTotalScore(assessCard.getTotalScore());
		oldAssessCard.setDescription(assessCard.getDescription());
		List<AcQuota> unFinanceQuotaList = assessCard.getUnFinanceQuotaList();
		List<FinanceQuotaRelation> financeQuotaList = assessCard.getFinanceQuotaList();
		for(AcQuota unFinanceQuota :  unFinanceQuotaList){
			String subquotaIds = "";
			UnFinaceQuotaRelation unRelation = new UnFinaceQuotaRelation();
			unRelation.setAssessCardId(assessCardId);
			unRelation.setQuotaId(unFinanceQuota.getId());
			for(AcQuota subQuota : unFinanceQuota.getChildrenList()){
				subquotaIds += subQuota.getId() +",";
				for(SubQuotaAttr subQuotaAttr : subQuota.getSubQuotaAttrList()){
					subQuotaAttr.setAssessCardId(assessCardId);
					subQuotaAttrDAO.insert(subQuotaAttr);
				}
			}
			unRelation.setSubQuotaIds(subquotaIds.substring(0, subquotaIds.length()-1));
			unFinanceQuotaRelationDAO.save(unRelation);
		}
		if(financeQuotaList!=null){
			for(FinanceQuotaRelation relation : financeQuotaList){
				relation.setAssessCardId(assessCardId);
				financeQuotaRelationDAO.save(relation);
			}
		}
		assessCardDAO.saveOrUpdate(oldAssessCard);
	}

	@Override
	public AssessCard queryById(long id) throws Exception {
		return assessCardDAO.queryById(id);
	}
	
	
	@Override
	public AssessCard queryInfoById(long id) throws Exception {
		AssessCard assessCard = assessCardDAO.queryById(id);
		List<UnFinaceQuotaRelation> unRelationList = unFinanceQuotaRelationDAO.queryByAssessCardId(id);
		List<AcQuota> unFinanceQuotaList = new ArrayList<AcQuota>();
		for(UnFinaceQuotaRelation unRelation : unRelationList){
			AcQuota acQuota = acQuotaDAO.queryById(unRelation.getQuotaId());
			List<AcQuota> subQuotaList = acQuotaDAO.queryByCondition("from AcQuota as model where model.id in("
					+ unRelation.getSubQuotaIds() + ") order by model.id");
			for(AcQuota subQuota : subQuotaList){
				List<SubQuotaAttr> subQuotaAttrList = subQuotaAttrDAO.queryByACIdAndQuId(id, subQuota.getId());
				subQuota.setSubQuotaAttrList(subQuotaAttrList);
				List<AcQuotaAttr> acQuotaAttrs = acQuotaAttrDAO.queryByQuotaId(subQuota.getId());
				subQuota.setAcQuotaAttrs(acQuotaAttrs);
			}
			acQuota.setChildrenList(subQuotaList);
			unFinanceQuotaList.add(acQuota);
		}
		assessCard.setUnFinanceQuotaList(unFinanceQuotaList);
		List<FinanceQuotaRelation> financeQuotaRelationList=financeQuotaRelationDAO.queryByAsscardId(id);
		assessCard.setFinanceQuotaList(financeQuotaRelationList);
		return assessCard;
	}

	@Override
	public void deleteSmallCard(long id) throws Exception {
		AcConfiguration acConfiguration = acConfigurationDAO.queryByAcId(id);
		if(acConfiguration !=null && AcConstants.AssessCardState.ENABLE.equals(acConfiguration.getState())){
			ExceptionUtil.throwException("此评分卡正在使用中，不能删除！");
		}
		//删除关联表信息
		List<UnFinaceQuotaRelation> unRelationList = unFinanceQuotaRelationDAO.queryByAssessCardId(id);
		for(UnFinaceQuotaRelation unRelation : unRelationList){
			List<AcQuota> subQuotaList = acQuotaDAO.queryByCondition("from AcQuota as model where model.id in("
					+ unRelation.getSubQuotaIds() + ") order by model.id");
			for(AcQuota subQuota : subQuotaList){
				List<SubQuotaAttr> subQuotaAttrList = subQuotaAttrDAO.queryByACIdAndQuId(id, subQuota.getId());
				for(SubQuotaAttr subQuotaAttr : subQuotaAttrList ){
					subQuotaAttrDAO.delete(subQuotaAttr);
				}
			}
			unFinanceQuotaRelationDAO.delete(unRelation);
		}
		List<FinanceQuotaRelation> financeQuotaRelationList=financeQuotaRelationDAO.queryByAsscardId(id);
		for(FinanceQuotaRelation relation : financeQuotaRelationList){
			financeQuotaRelationDAO.delete(relation);
		}
		AssessCard assessCard = assessCardDAO.queryById(id);
		//删除评分卡信息
		assessCardDAO.deleteEntity(assessCard);
		
		//删除评分卡配置信息
		acConfigurationDAO.delete(acConfiguration);
	}
}
