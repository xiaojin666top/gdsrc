package com.beawan.scoreCard.qbean;

import java.util.List;

public class AcQuotaResult {
	private Long id;
	private Long pid;
	private String quotaName;
	private String quotaValue;
	private Double quotaScore;
	private Double weight;
	private Double weightScore;
	private String flag;
	
	private List<AcQuotaResult> childList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public String getQuotaName() {
		return quotaName;
	}

	public void setQuotaName(String quotaName) {
		this.quotaName = quotaName;
	}

	public String getQuotaValue() {
		return quotaValue;
	}

	public void setQuotaValue(String quotaValue) {
		this.quotaValue = quotaValue;
	}

	public Double getQuotaScore() {
		return quotaScore;
	}

	public void setQuotaScore(Double quotaScore) {
		this.quotaScore = quotaScore;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getWeightScore() {
		return weightScore;
	}

	public void setWeightScore(Double weightScore) {
		this.weightScore = weightScore;
	}

	public List<AcQuotaResult> getChildList() {
		return childList;
	}

	public void setChildList(List<AcQuotaResult> childList) {
		this.childList = childList;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}
	
	
}
