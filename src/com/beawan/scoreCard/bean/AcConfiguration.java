package com.beawan.scoreCard.bean;

import java.util.List;
// default package


/**
 * 评分卡相关配置表
 */

public class AcConfiguration{

	// Fields

	private Long id;
	private Long assessCardId;
	private String state;
	private double passScore;
	private double rejectScore;
	private double financeRatio;
	private double unfinanceRatio;
	private String scoreWay;
	private String loanType;
	private String guaranteeType;
	private String enterpriseType;
	private String cardType;
	private double suggScore;
	private String acCusLevelSet;
	
	private List<AcCusLevel> acCusLevelList;
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAssessCardId() {
		return this.assessCardId;
	}

	public void setAssessCardId(Long assessCardId) {
		this.assessCardId = assessCardId;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public double getPassScore() {
		return this.passScore;
	}

	public void setPassScore(double passScore) {
		this.passScore = passScore;
	}

	public double getRejectScore() {
		return this.rejectScore;
	}

	public void setRejectScore(double rejectScore) {
		this.rejectScore = rejectScore;
	}

	public double getFinanceRatio() {
		return this.financeRatio;
	}

	public void setFinanceRatio(double financeRatio) {
		this.financeRatio = financeRatio;
	}

	public double getUnfinanceRatio() {
		return this.unfinanceRatio;
	}

	public void setUnfinanceRatio(double unfinanceRatio) {
		this.unfinanceRatio = unfinanceRatio;
	}

	public String getScoreWay() {
		return this.scoreWay;
	}

	public void setScoreWay(String scoreWay) {
		this.scoreWay = scoreWay;
	}

	public String getLoanType() {
		return loanType;
	}

	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}
	
	public String getGuaranteeType() {
		return guaranteeType;
	}

	public void setGuaranteeType(String guaranteeType) {
		this.guaranteeType = guaranteeType;
	}

	public String getEnterpriseType() {
		return enterpriseType;
	}

	public void setEnterpriseType(String enterpriseType) {
		this.enterpriseType = enterpriseType;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public double getSuggScore() {
		return suggScore;
	}

	public void setSuggScore(double suggScore) {
		this.suggScore = suggScore;
	}

	public String getAcCusLevelSet() {
		return acCusLevelSet;
	}

	public void setAcCusLevelSet(String acCusLevelSet) {
		this.acCusLevelSet = acCusLevelSet;
	}

	public List<AcCusLevel> getAcCusLevelList() {
		return acCusLevelList;
	}

	public void setAcCusLevelList(List<AcCusLevel> acCusLevelList) {
		this.acCusLevelList = acCusLevelList;
	}

	

	


}