package com.beawan.scoreCard.bean;

/**
 * 保存评分卡分数 对应的客户等级指标
 *
 */
public class AcCusLevel {	
	
	private double upValue; //起始分数
	
	private String leftFlag; //左区间标记，C：闭区间，O：开区间
	
	private String rightFlag; //右区间标记，C：闭区间，O：开区间
	
	private double downValue; //结束分数
	
	private String flowValue; //客户等级

	public double getUpValue() {
		return upValue;
	}

	public void setUpValue(double upValue) {
		this.upValue = upValue;
	}

	public double getDownValue() {
		return downValue;
	}

	public void setDownValue(double downValue) {
		this.downValue = downValue;
	}

	public String getFlowValue() {
		return flowValue;
	}

	public void setFlowValue(String flowValue) {
		this.flowValue = flowValue;
	}

	public String getLeftFlag() {
		return leftFlag;
	}

	public void setLeftFlag(String leftFlag) {
		this.leftFlag = leftFlag;
	}

	public String getRightFlag() {
		return rightFlag;
	}

	public void setRightFlag(String rightFlag) {
		this.rightFlag = rightFlag;
	}
	
}
