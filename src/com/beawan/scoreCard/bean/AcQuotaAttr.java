package com.beawan.scoreCard.bean;
// default package


/**
 * AcQuotaAttr entity. @author MyEclipse Persistence Tools
 */

public class AcQuotaAttr implements java.io.Serializable {

	// Fields

	private Long id;
	private Long quotaId;
	private String content;
	private String code;
	private String attrValue;
	private double upper;
	private double down;

	// Constructors

	/** default constructor */
	public AcQuotaAttr() {
	}

	/** full constructor */
	public AcQuotaAttr(Long quotaId, String content, String code,
			String attrValue, double upper, double down) {
		this.quotaId = quotaId;
		this.content = content;
		this.code = code;
		this.attrValue = attrValue;
		this.upper = upper;
		this.down = down;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getQuotaId() {
		return this.quotaId;
	}

	public void setQuotaId(Long quotaId) {
		this.quotaId = quotaId;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAttrValue() {
		return this.attrValue;
	}

	public void setAttrValue(String attrValue) {
		this.attrValue = attrValue;
	}

	public double getUpper() {
		return this.upper;
	}

	public void setUpper(double upper) {
		this.upper = upper;
	}

	public double getDown() {
		return this.down;
	}

	public void setDown(double down) {
		this.down = down;
	}

}