package com.beawan.scoreCard.bean;


import com.beawan.core.BaseEntity;

import javax.persistence.*;
import java.util.Objects;

/**
 * 评级详细结果记录表
 */
@Entity
@Table(name = "AC_RESULT",schema = "GDTCESYS")
public class AcResult extends BaseEntity {

    @Id
   // @GeneratedValue(strategy = GenerationType.SEQUENCE,generator="AC_RESULT_SEQ")
   // @SequenceGenerator(name="AC_RESULT_SEQ",allocationSize=1,initialValue=1, sequenceName="AC_RESULT_SEQ")
    private Long id;
    @Column(name = "SER_NO")
    private String serNo;
    @Column(name = "ASSESS_NAME")
    private String assessName;//评级模型名称
    @Column(name = "LOAN_TYPE")
    private String loanType;    //贷款类型  唯一
    @Column(name = "QUOTA_NAME")
    private String quotaName;    //指标名称
    @Column(name = "QUOTA_TYPE")
    private String quotaType;    //指标类型
    @Column(name = "PARENT_NAME")
    private String parentName;   //父级指标名称
    @Column(name = "CONTENT")
    private String content;      //具体项内容
    @Column(name = "ATTR_TYPE")
    private String attrType;//V S 区间值和具体值
    @Column(name = "UPPER")
    private Double upper;//上限
    @Column(name = "DOWN")
    private Double down;
    @Column(name = "SCORE")
    private Integer sccre;//分数
    @Column(name = "REAL_VAL")
    private String realVal;//客户实际值


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSerNo() {
        return serNo;
    }

    public void setSerNo(String serNo) {
        this.serNo = serNo;
    }

    public String getAssessName() {
        return assessName;
    }

    public void setAssessName(String assessName) {
        this.assessName = assessName;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getQuotaName() {
        return quotaName;
    }

    public void setQuotaName(String quotaName) {
        this.quotaName = quotaName;
    }

    public String getQuotaType() {
        return quotaType;
    }

    public void setQuotaType(String quotaType) {
        this.quotaType = quotaType;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAttrType() {
        return attrType;
    }

    public void setAttrType(String attrType) {
        this.attrType = attrType;
    }

    public Double getUpper() {
        return upper;
    }

    public void setUpper(Double upper) {
        this.upper = upper;
    }

    public Double getDown() {
        return down;
    }

    public void setDown(Double down) {
        this.down = down;
    }

    public Integer getSccre() {
        return sccre;
    }

    public void setSccre(Integer sccre) {
        this.sccre = sccre;
    }

    public String getRealVal() {
        return realVal;
    }

    public void setRealVal(String realVal) {
        this.realVal = realVal;
    }
}
