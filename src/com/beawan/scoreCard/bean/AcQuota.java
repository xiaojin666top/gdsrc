package com.beawan.scoreCard.bean;

import java.util.List;
// default package


/**
 * 指标信息表
 */

public class AcQuota{

	// Fields

	private Long id;
	private String loanType;
	private String code;
	private String name;
	private Long pid;
	private String type;
	private String cardType;
	private Double weight;
	private List<AcQuotaAttr> acQuotaAttrs;
	private List<AcQuota> childrenList;
	private List<SubQuotaAttr> subQuotaAttrList;

	/** default constructor */
	public AcQuota() {
	}

	/** full constructor */
	public AcQuota(String loanType, String code, String name, Long pid,
			String type) {
		this.loanType = loanType;
		this.code = code;
		this.name = name;
		this.pid = pid;
		this.type = type;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLoanType() {
		return this.loanType;
	}

	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getPid() {
		return this.pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public List<AcQuotaAttr> getAcQuotaAttrs() {
		return acQuotaAttrs;
	}

	public void setAcQuotaAttrs(List<AcQuotaAttr> acQuotaAttrs) {
		this.acQuotaAttrs = acQuotaAttrs;
	}

	public List<AcQuota> getChildrenList() {
		return childrenList;
	}

	public void setChildrenList(List<AcQuota> childrenList) {
		this.childrenList = childrenList;
	}

	public List<SubQuotaAttr> getSubQuotaAttrList() {
		return subQuotaAttrList;
	}

	public void setSubQuotaAttrList(List<SubQuotaAttr> subQuotaAttrList) {
		this.subQuotaAttrList = subQuotaAttrList;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}
	

}