package com.beawan.scoreCard.bean;

public class AcQuotaRel {

	private Long id;
	private Long assessCardId;
	private Long quotaId;
	private Long quotaPid;
	private Double weight;
	private String type;
	
	public AcQuotaRel() {}
	
	public AcQuotaRel(Long id, Long assessCardId, Long quotaId, Long quotaPid,
			Double weight, String type) {
		super();
		this.id = id;
		this.assessCardId = assessCardId;
		this.quotaId = quotaId;
		this.quotaPid = quotaPid;
		this.weight = weight;
		this.type = type;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getAssessCardId() {
		return assessCardId;
	}
	public void setAssessCardId(Long assessCardId) {
		this.assessCardId = assessCardId;
	}
	public Long getQuotaId() {
		return quotaId;
	}
	public void setQuotaId(Long quotaId) {
		this.quotaId = quotaId;
	}
	public Long getQuotaPid() {
		return quotaPid;
	}
	public void setQuotaPid(Long quotaPid) {
		this.quotaPid = quotaPid;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	

}