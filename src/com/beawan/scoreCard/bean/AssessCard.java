package com.beawan.scoreCard.bean;
// default package

import com.beawan.core.BaseEntity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * 评分卡基本信息表（主表）
 */

@Entity
@Table(name = "ASSESS_CARD_INFO",schema = "GDTCESYS")
public class AssessCard extends BaseEntity {

	// Fields

	@Id
//	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator="ASSESS_CARD_SEQ")
	//@SequenceGenerator(name="ASSESS_CARD_SEQ",allocationSize=1,initialValue=1, sequenceName="ASSESS_CARD_SEQ")
	private Long id;
	@Column(name = "NAME")
	private String name;//名称
	@Column(name = "CARD_TYPE")
	private String cardType;//卡种类
	@Column(name = "LOAN_TYPE")
	private String loanType;//所属贷款
	@Column(name = "TOTAL_SCORE")
	private double totalScore;
	@Column(name = "CREATOR")
	private String creator;
	@Column(name = "DESCRIPTION")
	private String description;
	@Column(name = "GUARANTEE_TYPE")
	private String guaranteeType;
	@Column(name = "ENTERPRISE_TYPE")
	private String enterpriseType;

	@Transient
	private List<AcQuota> unFinanceQuotaList;
	@Transient
	private List<FinanceQuotaRelation> financeQuotaList;
	
	public AssessCard() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCardType() {
		return this.cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getLoanType() {
		return this.loanType;
	}

	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}

	public double getTotalScore() {
		return this.totalScore;
	}

	public void setTotalScore(double totalScore) {
		this.totalScore = totalScore;
	}

	public String getCreator() {
		return this.creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<FinanceQuotaRelation> getFinanceQuotaList() {
		return financeQuotaList;
	}

	public void setFinanceQuotaList(List<FinanceQuotaRelation> financeQuotaList) {
		this.financeQuotaList = financeQuotaList;
	}

	public List<AcQuota> getUnFinanceQuotaList() {
		return unFinanceQuotaList;
	}

	public void setUnFinanceQuotaList(List<AcQuota> unFinanceQuotaList) {
		this.unFinanceQuotaList = unFinanceQuotaList;
	}

	public String getGuaranteeType() {
		return guaranteeType;
	}

	public void setGuaranteeType(String guaranteeType) {
		this.guaranteeType = guaranteeType;
	}

	public String getEnterpriseType() {
		return enterpriseType;
	}

	public void setEnterpriseType(String enterpriseType) {
		this.enterpriseType = enterpriseType;
	}

}