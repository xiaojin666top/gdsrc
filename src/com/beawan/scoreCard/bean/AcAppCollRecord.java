package com.beawan.scoreCard.bean;

import java.util.Map;

// default package

/**
 * 评分卡应用——催收评分卡评分结果记录
 */

public class AcAppCollRecord implements java.io.Serializable {

	// Fields

	private Long appTaskId;
	private String customerNo;

	private String customerName;
	private Double score;
	private String riskType;
	private String status;
	private Long logId;

	private String billNo;

	// 非持久化属性，显示信息
	private Map<String, Object> viewFields;

	/** default constructor */
	public AcAppCollRecord() {
	}

	/** minimal constructor */
	public AcAppCollRecord(Long appTaskId, String customerNo) {
		this.appTaskId = appTaskId;
		this.customerNo = customerNo;
	}

	/** full constructor */
	public AcAppCollRecord(Long appTaskId, String customerNo,
			String customerName, Double score, String riskType, String status,
			Long logId) {
		this.appTaskId = appTaskId;
		this.customerNo = customerNo;
		this.customerName = customerName;
		this.score = score;
		this.riskType = riskType;
		this.status = status;
		this.logId = logId;
	}

	public Long getAppTaskId() {
		return this.appTaskId;
	}

	public void setAppTaskId(Long appTaskId) {
		this.appTaskId = appTaskId;
	}

	public String getCustomerNo() {
		return this.customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Double getScore() {
		return this.score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public String getRiskType() {
		return this.riskType;
	}

	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getLogId() {
		return this.logId;
	}

	public void setLogId(Long logId) {
		this.logId = logId;
	}

	public String getBillNo() {
		return billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public Map<String, Object> getViewFields() {
		return viewFields;
	}

	public void setViewFields(Map<String, Object> viewFields) {
		this.viewFields = viewFields;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((appTaskId == null) ? 0 : appTaskId.hashCode());
		result = prime * result
				+ ((customerNo == null) ? 0 : customerNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AcAppCollRecord other = (AcAppCollRecord) obj;
		if (appTaskId == null) {
			if (other.appTaskId != null)
				return false;
		} else if (!appTaskId.equals(other.appTaskId))
			return false;
		if (customerNo == null) {
			if (other.customerNo != null)
				return false;
		} else if (!customerNo.equals(other.customerNo))
			return false;
		return true;
	}

}