package com.beawan.scoreCard.bean;
// default package


/**
 * UnFinaceQuotaRelation entity. @author MyEclipse Persistence Tools
 */

public class UnFinaceQuotaRelation implements java.io.Serializable {

	// Fields

	private Long id;
	private Long assessCardId;
	private Long quotaId;
	private String subQuotaIds;

	// Constructors

	/** default constructor */
	public UnFinaceQuotaRelation() {
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAssessCardId() {
		return this.assessCardId;
	}

	public void setAssessCardId(Long assessCardId) {
		this.assessCardId = assessCardId;
	}

	public Long getQuotaId() {
		return this.quotaId;
	}

	public void setQuotaId(Long quotaId) {
		this.quotaId = quotaId;
	}

	public String getSubQuotaIds() {
		return this.subQuotaIds;
	}

	public void setSubQuotaIds(String subQuotaIds) {
		this.subQuotaIds = subQuotaIds;
	}

}