package com.beawan.scoreCard.bean;
// default package

import java.util.Date;

/**
 * 评分卡记录信息
 */

public class AcLogRecord{

	// Fields

	private Long id;
	private String serialNumber;
	private Long acId;
	private String acName;
	private String loanType;
	private String guaranteeType;
	private String enterpriseType;
	private Date logTime;
	private Double score;
	private String result;
	private String cardType;
	private String scoreDetailDesc;
	private String custGrade;


	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSerialNumber() {
		return this.serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public Long getAcId() {
		return this.acId;
	}

	public void setAcId(Long acId) {
		this.acId = acId;
	}

	public String getAcName() {
		return this.acName;
	}

	public void setAcName(String acName) {
		this.acName = acName;
	}

	public String getLoanType() {
		return this.loanType;
	}

	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}

	public String getGuaranteeType() {
		return this.guaranteeType;
	}

	public void setGuaranteeType(String guaranteeType) {
		this.guaranteeType = guaranteeType;
	}

	public String getEnterpriseType() {
		return this.enterpriseType;
	}

	public void setEnterpriseType(String enterpriseType) {
		this.enterpriseType = enterpriseType;
	}

	public Date getLogTime() {
		return this.logTime;
	}

	public void setLogTime(Date logTime) {
		this.logTime = logTime;
	}

	public Double getScore() {
		return this.score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public String getResult() {
		return this.result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getScoreDetailDesc() {
		return scoreDetailDesc;
	}

	public void setScoreDetailDesc(String scoreDetailDesc) {
		this.scoreDetailDesc = scoreDetailDesc;
	}

	public String getCustGrade() {
		return custGrade;
	}

	public void setCustGrade(String custGrade) {
		this.custGrade = custGrade;
	}

}