package com.beawan.scoreCard.bean;
// default package


/**
 * 这个表只是用来存具体项的一个分数  像assessCardId  subQuotaId都是冗余字段
 */

public class SubQuotaAttr implements java.io.Serializable {

	// Fields

	private Long id;
	private Long assessCardId;
	private Long subQuotaId;
	private Long attrId;
	private double scord;
	private String attrContent;


	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAssessCardId() {
		return this.assessCardId;
	}

	public void setAssessCardId(Long assessCardId) {
		this.assessCardId = assessCardId;
	}

	public Long getSubQuotaId() {
		return this.subQuotaId;
	}

	public void setSubQuotaId(Long subQuotaId) {
		this.subQuotaId = subQuotaId;
	}

	public Long getAttrId() {
		return this.attrId;
	}

	public void setAttrId(Long attrId) {
		this.attrId = attrId;
	}

	public double getScord() {
		return this.scord;
	}

	public void setScord(double scord) {
		this.scord = scord;
	}

	public String getAttrContent() {
		return attrContent;
	}

	public void setAttrContent(String attrContent) {
		this.attrContent = attrContent;
	}

}