package com.beawan.scoreCard.bean;
// default package


/**
 * FinanceQuotaRelation entity. @author MyEclipse Persistence Tools
 */

public class FinanceQuotaRelation implements java.io.Serializable {

	// Fields

	private Long id;
	private Long assessCardId;
	private String targetNo;
	private String targetName;
	private double stardardValue;
	private double tarfetRatio;

	// Constructors

	/** default constructor */
	public FinanceQuotaRelation() {
	}

	/** full constructor */
	public FinanceQuotaRelation(Long assessCardId, String targetNo,
			String targetName, double stardardValue, double tarfetRatio) {
		this.assessCardId = assessCardId;
		this.targetNo = targetNo;
		this.targetName = targetName;
		this.stardardValue = stardardValue;
		this.tarfetRatio = tarfetRatio;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAssessCardId() {
		return this.assessCardId;
	}

	public void setAssessCardId(Long assessCardId) {
		this.assessCardId = assessCardId;
	}

	public String getTargetNo() {
		return this.targetNo;
	}

	public void setTargetNo(String targetNo) {
		this.targetNo = targetNo;
	}

	public String getTargetName() {
		return this.targetName;
	}

	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}

	public double getStardardValue() {
		return this.stardardValue;
	}

	public void setStardardValue(double stardardValue) {
		this.stardardValue = stardardValue;
	}

	public double getTarfetRatio() {
		return this.tarfetRatio;
	}

	public void setTarfetRatio(double tarfetRatio) {
		this.tarfetRatio = tarfetRatio;
	}

}