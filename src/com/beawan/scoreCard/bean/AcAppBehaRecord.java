package com.beawan.scoreCard.bean;
/**
 * 行为评分记录类
 */

public class AcAppBehaRecord  implements java.io.Serializable {


    // Fields    

     private String customerNo;
     private Long appTaskId;
     private String customerName;
     private Double lastLmtAmt;
     private Double score;
     private Double floatRate;
     private String status;
     private Long logId;
     
     //非持久化属性
     private Double newLmtAmt;


    // Constructors

    /** default constructor */
    public AcAppBehaRecord() {
    }

	/** minimal constructor */
    public AcAppBehaRecord(String customerNo , Long appTaskId) {
        this.customerNo = customerNo;
        this.appTaskId = appTaskId;
    }
    
   
    // Property accessors

    public String getCustomerName() {
        return this.customerName;
    }
    
    public String getCustomerNo() {
		return customerNo;
	}

	public void setCustomerNo(String customerNo) {
		this.customerNo = customerNo;
	}

	public Long getAppTaskId() {
		return appTaskId;
	}

	public void setAppTaskId(Long appTaskId) {
		this.appTaskId = appTaskId;
	}

	public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Double getLastLmtAmt() {
        return this.lastLmtAmt;
    }
    
    public void setLastLmtAmt(Double lastLmtAmt) {
        this.lastLmtAmt = lastLmtAmt;
    }

    public Double getScore() {
        return this.score;
    }
    
    public void setScore(Double score) {
        this.score = score;
    }

    public Double getFloatRate() {
        return this.floatRate;
    }
    
    public void setFloatRate(Double floatRate) {
        this.floatRate = floatRate;
    }

    public String getStatus() {
        return this.status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }

    public Long getLogId() {
        return this.logId;
    }
    
    public void setLogId(Long logId) {
        this.logId = logId;
    }
    
	public Double getNewLmtAmt() {
		return newLmtAmt;
	}

	public void setNewLmtAmt(Double newLmtAmt) {
		this.newLmtAmt = newLmtAmt;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((appTaskId == null) ? 0 : appTaskId.hashCode());
		result = prime * result
				+ ((customerNo == null) ? 0 : customerNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AcAppBehaRecord other = (AcAppBehaRecord) obj;
		if (appTaskId == null) {
			if (other.appTaskId != null)
				return false;
		} else if (!appTaskId.equals(other.appTaskId))
			return false;
		if (customerNo == null) {
			if (other.customerNo != null)
				return false;
		} else if (!customerNo.equals(other.customerNo))
			return false;
		return true;
	}

}