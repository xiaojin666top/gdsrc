package com.beawan.scoreCard.bean;

import java.util.Map;

// default package

/**
 * 评分卡应用——评分卡执行任务记录
 */

public class AcAppTask implements java.io.Serializable {

	// Fields

	private Long id;
	private String createTime;
	private String createUser;
	private String taskType;
	private String startTime;
	private String endTime;
	private Long cardId;
	private String cardName;
	private String executeStatus;
	private String approvalStatus;
	private String customerNos;

	private String taskFlag;// 任务标记，1：日终自动执行，0：手动单词执行
	private String exeStartDate;// 开始执行日期
	private Integer exeFrequency;// 执行频率（天）

	// 非持久化属性
	private Map<String, Object> viewFields;

	// Constructors

	/** default constructor */
	public AcAppTask() {
	}

	/** minimal constructor */
	public AcAppTask(Long id) {
		this.id = id;
	}

	// Property accessors

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Long getCardId() {
		return this.cardId;
	}

	public void setCardId(Long cardId) {
		this.cardId = cardId;
	}

	public String getCardName() {
		return this.cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	public String getExecuteStatus() {
		return this.executeStatus;
	}

	public void setExecuteStatus(String executeStatus) {
		this.executeStatus = executeStatus;
	}

	public String getApprovalStatus() {
		return this.approvalStatus;
	}

	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public String getCustomerNos() {
		return this.customerNos;
	}

	public void setCustomerNos(String customerNos) {
		this.customerNos = customerNos;
	}

	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public Map<String, Object> getViewFields() {
		return viewFields;
	}

	public void setViewFields(Map<String, Object> viewFields) {
		this.viewFields = viewFields;
	}

	public String getTaskFlag() {
		return taskFlag;
	}

	public void setTaskFlag(String taskFlag) {
		this.taskFlag = taskFlag;
	}

	public String getExeStartDate() {
		return exeStartDate;
	}

	public void setExeStartDate(String exeStartDate) {
		this.exeStartDate = exeStartDate;
	}

	public Integer getExeFrequency() {
		return exeFrequency;
	}

	public void setExeFrequency(Integer exeFrequency) {
		this.exeFrequency = exeFrequency;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AcAppTask other = (AcAppTask) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}