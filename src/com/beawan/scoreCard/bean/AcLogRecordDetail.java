package com.beawan.scoreCard.bean;

/**
 * AcLogRecordDetail entity. @author MyEclipse Persistence Tools
 */

public class AcLogRecordDetail {


    // Fields    

     private Long id;
     private Long logId;
     private String quotaName;
     private String quotaValue;
     private Double quotaScore;
     private String quotaCode;
     private String flag;


    // Constructors

    @Override
	public String toString() {
		return "AcLogRecordDetail [id=" + id + ", logId=" + logId + ", quotaName=" + quotaName + ", quotaValue="
				+ quotaValue + ", quotaScore=" + quotaScore + ", quotaCode=" + quotaCode + ", flag=" + flag + "]";
	}

	/** default constructor */
    public AcLogRecordDetail() {
    }
    
    /** full constructor */
    public AcLogRecordDetail(Long logId, String quotaName, String quotaValue, Double quotaScore, String quotaCode, String flag) {
        this.logId = logId;
        this.quotaName = quotaName;
        this.quotaValue = quotaValue;
        this.quotaScore = quotaScore;
        this.quotaCode = quotaCode;
        this.flag = flag;
    }

   
    // Property accessors

    

    public String getQuotaName() {
        return this.quotaName;
    }
    
    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getLogId() {
		return logId;
	}

	public void setLogId(Long logId) {
		this.logId = logId;
	}

	public void setQuotaName(String quotaName) {
        this.quotaName = quotaName;
    }

    public String getQuotaValue() {
        return this.quotaValue;
    }
    
    public void setQuotaValue(String quotaValue) {
        this.quotaValue = quotaValue;
    }

    public Double getQuotaScore() {
        return this.quotaScore;
    }
    
    public void setQuotaScore(Double quotaScore) {
        this.quotaScore = quotaScore;
    }

    public String getQuotaCode() {
        return this.quotaCode;
    }
    
    public void setQuotaCode(String quotaCode) {
        this.quotaCode = quotaCode;
    }

    public String getFlag() {
        return this.flag;
    }
    
    public void setFlag(String flag) {
        this.flag = flag;
    }
   








}