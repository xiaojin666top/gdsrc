package com.beawan.scoreCard.dao;

import java.util.List;

import com.beawan.core.BaseDao;
import com.beawan.scoreCard.bean.AssessCard;
import com.beawan.scoreCard.dto.ScoreCardFullDto;

public interface IAssessCardDAO extends BaseDao<AssessCard> {
	
	/**
	 * 分页查询
	 * @param queryString
	 * @param params
	 * @param start
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	public List<AssessCard> queryPaginate(String queryString,Object[] params,int start,int limit)throws Exception;
	/**
	 * 条件查询
	 * @param queryString
	 * @return
	 * @throws Exception
	 */
	public List<AssessCard> queryCondition(String queryString, Object[] params)throws Exception;
	

	public AssessCard queryById(long id)throws Exception;

	/**
	 * 获取评级模型所有数据
	 * @param cardId		评级模型id
	 * @return
	 * @throws Exception
	 */
	public List<ScoreCardFullDto> getFullByCardId(Long cardId) throws Exception;
	
}
