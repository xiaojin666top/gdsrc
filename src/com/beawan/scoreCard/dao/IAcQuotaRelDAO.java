package com.beawan.scoreCard.dao;

import java.util.List;

import com.beawan.core.BaseDao;
import com.beawan.scoreCard.bean.AcQuotaRel;
import com.beawan.scoreCard.bean.AssessCard;


public interface IAcQuotaRelDAO extends BaseDao<AcQuotaRel>{	
	
	public void saveQuotaRel(AcQuotaRel acQuotaRel);

	/**
	 * 
	 * <p>@Title: queryByAssId</p>
	 * <p>@Description: 查询评分卡指标关联信息</p>
	 * @param assId
	 * @return
	 * @author beawan_fengjj
	 * @date 2018年1月3日 上午11:42:28
	 */
	public List<AcQuotaRel> queryByAssId(long assId);

	public List<AcQuotaRel> queryByAssIdAndPid(long id, long pid);

	public void delete(AcQuotaRel acQuotaRel);
	/**
	 * 
	 * <p>@Title: queryByAssIdAndQuotaId</p>
	 * <p>@Description: 根据评分卡ID和指标ID查询</p>
	 * @param assId
	 * @param quotaId
	 * @return
	 * @author beawan_fengjj
	 * @date 2018年1月3日 上午11:49:46
	 */
	public AcQuotaRel queryByAssIdAndQuotaId(long assId, long quotaId);

	public List<AcQuotaRel> queryByQuotaId(long id);
}
