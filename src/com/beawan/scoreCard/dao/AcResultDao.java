package com.beawan.scoreCard.dao;

import com.beawan.core.BaseDao;
import com.beawan.scoreCard.bean.AcResult;

/**
 * @author yzj
 */
public interface AcResultDao extends BaseDao<AcResult> {
}
