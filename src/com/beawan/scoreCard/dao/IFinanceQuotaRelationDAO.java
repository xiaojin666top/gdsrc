package com.beawan.scoreCard.dao;

import java.util.List;

import com.beawan.core.BaseDao;
import com.beawan.scoreCard.bean.FinanceQuotaRelation;


public interface IFinanceQuotaRelationDAO extends BaseDao<FinanceQuotaRelation>{
	
	public FinanceQuotaRelation save(FinanceQuotaRelation financeQuotaRelation)throws Exception;
	
	public List<FinanceQuotaRelation> queryByAsscardId(long asscardId)throws Exception;
	
	public void delete(FinanceQuotaRelation financeQuotaRelation)throws Exception;
	
}
