package com.beawan.scoreCard.dao;

import java.util.List;

import com.beawan.core.BaseDao;
import com.beawan.scoreCard.bean.AcQuota;
import com.beawan.scoreCard.bean.AcQuotaAttr;


public interface IAcQuotaAttrDAO extends BaseDao<AcQuotaAttr>{
	
	public AcQuotaAttr save(AcQuotaAttr acQuotaAttr)throws Exception;
	
	public void delete(AcQuotaAttr acQuotaAttr) throws Exception;
	
	public AcQuotaAttr queryById(long id) throws Exception;
	/**
	 * 
	 * <p>@Title: queryByQuotaId</p>
	 * <p>@Description: 查询指标属性</p>
	 * @param quotaId
	 * @return
	 * @throws Exception
	 * @author beawan_fengjj
	 * @date 2018年1月3日 下午1:49:22
	 */
	public List<AcQuotaAttr> queryByQuotaId(long quotaId) throws Exception;


	/**
	 * TODO
	 * @param acQuotaAttr
	 * @return
	 * @throws Exception
	 */
	public AcQuotaAttr merge(AcQuotaAttr acQuotaAttr) throws Exception;

	public List<AcQuotaAttr> queryAll() throws Exception;
	
}
