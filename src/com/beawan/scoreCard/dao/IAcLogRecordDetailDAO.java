package com.beawan.scoreCard.dao;

import com.beawan.core.BaseDao;
import com.beawan.scoreCard.bean.AcLogRecordDetail;

public interface IAcLogRecordDetailDAO extends BaseDao<AcLogRecordDetail> {

}
