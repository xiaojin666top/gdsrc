package com.beawan.scoreCard.dao;

import java.util.List;

import com.beawan.core.BaseDao;
import com.beawan.scoreCard.bean.AcLogRecord;
import com.beawan.scoreCard.bean.AcQuotaAttr;


public interface IAcLogRecordDAO extends BaseDao<AcLogRecord> {
	/**
	 * 条件查询
	 * @param queryString
	 * @return
	 * @throws Exception
	 */
	public List<AcLogRecord> queryByCondition(String queryString,Object[] params)throws Exception;	
	/**
	 * 分页查询
	 * @param queryString
	 * @param start
	 * @param limit
	 * @return
	 * @throws Exception
	 */
	public List<AcLogRecord> queryPaginate(String queryString,Object[] params,int start,int limit)throws Exception;
}
