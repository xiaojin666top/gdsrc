package com.beawan.scoreCard.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.beawan.common.dao.impl.DaoHandler;
import com.beawan.core.BaseDaoImpl;
import com.beawan.scoreCard.bean.AcLogRecord;
import com.beawan.scoreCard.bean.AcQuotaAttr;
import com.beawan.scoreCard.dao.IAcLogRecordDAO;
@Repository("acLogRecordDAO")
public class AcLogRecordDAOImpl extends BaseDaoImpl<AcLogRecord> implements IAcLogRecordDAO{

	@Override
	public List<AcLogRecord> queryByCondition(String queryString, Object[] params) throws Exception {
		return execQuery(AcLogRecord.class,queryString,params);
	}

	@Override
	public List<AcLogRecord> queryPaginate(String queryString, Object[] params, int start, int limit) throws Exception {
		int index = start < 0 ? 0 : start;
		int count = limit < 0 ? 0 : limit;
		List<AcLogRecord> list = execQueryRange(AcLogRecord.class, queryString, index, count, params);
		return list;
	}

}
