package com.beawan.scoreCard.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.scoreCard.bean.AcQuota;
import com.beawan.scoreCard.dao.IAcQuotaDAO;
@Repository("acQuotaDAO")
public class AcQuotaDAOImpl extends  BaseDaoImpl<AcQuota>  implements IAcQuotaDAO{

	@Override
	public List<AcQuota> queryByCondition(String queryString, Object[] params) throws Exception {
		return execQuery(AcQuota.class,queryString,params);
	}

	@Override
	public List<AcQuota> queryPaginate(String queryString, Object[] params, int start, int limit) throws Exception {
		int index = start < 0 ? 0 : start;
		int count = limit < 0 ? 0 : limit;
		List<AcQuota> list = execQueryRange(AcQuota.class, queryString, index, count, params);
		return list;
	}

	@Override
	public List<AcQuota> queryAll() throws Exception {
		// TODO Auto-generated method stub
		return select("from AcQuota");
	}

	@Override
	public AcQuota queryById(long id) throws Exception {
		String queryString = "from AcQuota as model where model.id="+id;
		List<AcQuota> list = select(AcQuota.class, queryString);
		if(list.size()>0){
			return list.get(0);
		}else{
			return null;
		}
	}

	@Override
	public List<AcQuota> queryByPid(long pid) throws Exception {
		String queryString = "from AcQuota as model where model.pid="+pid + " order by model.id";
		List<AcQuota> list = select(AcQuota.class, queryString);
		return list;
	}
	
	@Override
	public List<AcQuota> queryByCondition(String queryString) throws Exception {
		@SuppressWarnings("unchecked")
		List<AcQuota> list = select(queryString);
		return list;
	}

}
