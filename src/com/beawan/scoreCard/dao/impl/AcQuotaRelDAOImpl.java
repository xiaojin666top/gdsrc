package com.beawan.scoreCard.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.scoreCard.bean.AcQuotaAttr;
import com.beawan.scoreCard.bean.AcQuotaRel;
import com.beawan.scoreCard.dao.IAcQuotaAttrDAO;
import com.beawan.scoreCard.dao.IAcQuotaRelDAO;
@Repository("acQuotaRelDAO")
public class AcQuotaRelDAOImpl extends BaseDaoImpl<AcQuotaRel> implements IAcQuotaRelDAO {
	
	@Override
	public List<AcQuotaRel> queryByAssId(long assId) {
		return select(AcQuotaRel.class, "from AcQuotaRel as model where model.assessCardId=?", assId);
	}

	@Override
	public List<AcQuotaRel> queryByAssIdAndPid(long assId, long pid) {
		return select(AcQuotaRel.class, "from AcQuotaRel as model where model.assessCardId=? and model.quotaPid=?", new Object[]{assId, pid});
	}

	/*
	 * @Override public void delete(AcQuotaRel acQuotaRel) {
	 * getHibernateTemplate().delete(acQuotaRel); }
	 */

	@Override
	public AcQuotaRel queryByAssIdAndQuotaId(long assId, long quotaId) {
		List<AcQuotaRel> list = select(AcQuotaRel.class, "from AcQuotaRel as model where model.assessCardId=? and model.quotaId=?", new Object[]{assId, quotaId});
		if(list != null && list.size() > 0) {
			return list.get(0);
		} return null;
	}

	@Override
	public List<AcQuotaRel> queryByQuotaId(long id) {
		return select(AcQuotaRel.class, "from AcQuotaRel as model where model.quotaId=?", id);
	}

	@Override
	public void saveQuotaRel(AcQuotaRel acQuotaRel) {
		save(acQuotaRel);
	}
	
	@Override
	public void delete(AcQuotaRel acQuotaRel) {
		super.delete(acQuotaRel);
	}
}
