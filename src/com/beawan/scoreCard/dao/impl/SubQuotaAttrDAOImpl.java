package com.beawan.scoreCard.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.scoreCard.bean.AssessCard;
import com.beawan.scoreCard.bean.SubQuotaAttr;
import com.beawan.scoreCard.dao.IAssessCardDAO;
import com.beawan.scoreCard.dao.ISubQuotaAttrDAO;
@Repository("subQuotaAttrDAO")
public class SubQuotaAttrDAOImpl  extends BaseDaoImpl<SubQuotaAttr>  implements ISubQuotaAttrDAO {
	

	@Override
	public SubQuotaAttr insert(SubQuotaAttr subQuotaAttr) throws Exception {
		save(subQuotaAttr);
		return subQuotaAttr;
	}

	@Override
	public List<SubQuotaAttr> queryByACIdAndQuId(long assessCardId,
			long subquotaId) throws Exception {
		String querystring = "from SubQuotaAttr as model where model.assessCardId =";
		querystring += assessCardId;
		querystring += " and model.subQuotaId = " + subquotaId;
		querystring += " order by model.id";
		List<SubQuotaAttr> list = (List<SubQuotaAttr>)select(SubQuotaAttr.class, querystring);
		return list;
	}

	@Override
	public void delete(SubQuotaAttr subQuotaAttr) throws Exception {
		super.delete(subQuotaAttr);
	}

	@Override
	public List<SubQuotaAttr> queryByQuId(long subquotaId) throws Exception {
		return (List<SubQuotaAttr>)select(SubQuotaAttr.class, "from SubQuotaAttr as model where model.subQuotaId=?", subquotaId);
	}

}
