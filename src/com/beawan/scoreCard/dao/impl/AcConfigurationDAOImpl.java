package com.beawan.scoreCard.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.scoreCard.bean.AcConfiguration;
import com.beawan.scoreCard.bean.AcQuotaAttr;
import com.beawan.scoreCard.dao.IAcConfigurationDAO;

@Repository("acConfigurationDAO")
public class AcConfigurationDAOImpl extends BaseDaoImpl<AcConfiguration> implements
	IAcConfigurationDAO {
	


	@Override
	public AcConfiguration queryByAcId(long assessCardId) throws Exception {
		String queryString = "from AcConfiguration as model where model.assessCardId="+assessCardId;
		@SuppressWarnings("unchecked")
		List<AcConfiguration> list = select(queryString);
		if(list.size()>0){
			return list.get(0);
		}else{
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AcConfiguration> queryByCondition(String queryString)
			throws Exception {
		return select(queryString);
	}

	@Override
	public void delete(AcConfiguration configuration) throws Exception {
		if(configuration!=null){
			super.delete(configuration);
		}
	}

}
