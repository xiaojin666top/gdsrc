package com.beawan.scoreCard.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.scoreCard.bean.UnFinaceQuotaRelation;
import com.beawan.scoreCard.dao.IUnFinanceQuotaRelationDAO;

@Repository("unFinanceQuotaRelationDAO")
public class UnFinanceQuotaRelationDAOImpl  extends BaseDaoImpl<UnFinaceQuotaRelation>
		implements IUnFinanceQuotaRelationDAO {
	

	@Override
	public UnFinaceQuotaRelation save(
			UnFinaceQuotaRelation unFinaceQuotaRelation) throws Exception {
		super.save(unFinaceQuotaRelation);
		return null;
	}

	@Override
	public List<UnFinaceQuotaRelation> queryByAssessCardId(long assessCardId)
			throws Exception {
		String queryString = "from UnFinaceQuotaRelation as model where model.assessCardId ="
				+ assessCardId + "order by model.id";
		List<UnFinaceQuotaRelation> list = select(queryString);
		return list;
	}

	@Override
	public List<UnFinaceQuotaRelation> queryByQuotaId(long quotaId)
			throws Exception {
		String queryString = "from UnFinaceQuotaRelation as model where model.quotaId ="
				+ quotaId + "order by model.id";
		List<UnFinaceQuotaRelation> list = select(queryString);
		return list;
	}

	@Override
	public void delete(UnFinaceQuotaRelation unFinaceQuotaRelation)
			throws Exception {
		super.delete(unFinaceQuotaRelation);
	}

	@Override
	public List<UnFinaceQuotaRelation> queryAll() {
		return select("from UnFinaceQuotaRelation");
	}


}
