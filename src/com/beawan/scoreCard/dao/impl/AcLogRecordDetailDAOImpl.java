package com.beawan.scoreCard.dao.impl;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.scoreCard.bean.AcLogRecordDetail;
import com.beawan.scoreCard.dao.IAcLogRecordDetailDAO;

@Repository("acLogRecordDetailDAO")
public class AcLogRecordDetailDAOImpl extends BaseDaoImpl<AcLogRecordDetail> implements IAcLogRecordDetailDAO{

}
