package com.beawan.scoreCard.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.beawan.scoreCard.dto.ScoreCardFullDto;
import org.springframework.stereotype.Repository;

import com.beawan.common.dao.impl.DaoHandler;
import com.beawan.core.BaseDaoImpl;
import com.beawan.scoreCard.bean.AcQuota;
import com.beawan.scoreCard.bean.AssessCard;
import com.beawan.scoreCard.dao.IAssessCardDAO;
import org.springframework.util.CollectionUtils;

@Repository("assessCardDAO")
public class AssessCardDAOImpl extends BaseDaoImpl<AssessCard>  implements IAssessCardDAO{

	@Override
	public List<AssessCard> queryPaginate(String queryString, Object[] params, int start, int limit) throws Exception {
		int index = start < 0 ? 0 : start;
		int count = limit < 0 ? 0 : limit;
		List<AssessCard> list = execQueryRange(AssessCard.class, queryString, index, count, params);
		return list;
	}

	@Override
	public List<AssessCard> queryCondition(String queryString, Object[] params) throws Exception {
		return execQuery(AssessCard.class,queryString,params);
	}
	

	@Override
	public AssessCard queryById(long id) throws Exception {
		String queryString = "from AssessCard as model where model.id =" + id;
		List list =select(AssessCard.class, queryString);
		if(list.size()!=0){
			return (AssessCard)list.get(0);
		}else{
			return null;
		}
	}

	@Override
	public List<ScoreCardFullDto> getFullByCardId(Long cardId) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append(" select")
				.append(" A. NAME assessName,A.LOAN_TYPE loanType,A.TOTAL_SCORE totalScore,")
				.append(" C.NAME quotaName,c.TYPE quotaType,D.NAME parentName,E.CONTENT content,")
				.append(" E.ATTR_VALUE attrType,E.UPPER upper,E.DOWN down,F.SCORD score")
				.append(" FROM  ASSESS_CARD_INFO A")
				.append(" JOIN  AC_CARD_QUOTA_REL B ON A.ID=B.ASSESS_CARD_ID")
				.append(" JOIN AC_QUOTA C ON B.QUOTA_ID=C.ID")
				.append(" LEFT JOIN AC_QUOTA D ON B.QUOTA_PID=D.ID")
				.append(" LEFT JOIN AC_QUOTA_ATTR E ON C.ID=E.QUOTA_ID")
				.append(" LEFT JOIN AC_SUB_QUOTA_ATTR F ON E.ID=F.ATTR_ID")
				.append(" WHERE A.ID=:cardId and D.NAME is not null");
		Map<String, Object> params = new HashMap<>();
		params.put("cardId", cardId);

		List<ScoreCardFullDto> list = this.findCustListBySql(ScoreCardFullDto.class, sql.toString(), params);
		return list;
	}

}
