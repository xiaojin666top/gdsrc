package com.beawan.scoreCard.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.scoreCard.bean.AcQuotaAttr;
import com.beawan.scoreCard.dao.IAcQuotaAttrDAO;

@Repository("acQuotaAttrDAO")
public class AcQuotaAtrrDAOImpl extends BaseDaoImpl<AcQuotaAttr> implements
		IAcQuotaAttrDAO {
	
	@Override
	public AcQuotaAttr save(AcQuotaAttr acQuotaAttr) throws Exception {
		acQuotaAttr = saveOrUpdate(acQuotaAttr);
		return acQuotaAttr;
	}
	
	
	@Override
	public AcQuotaAttr merge(AcQuotaAttr acQuotaAttr) throws Exception {
		return merge(acQuotaAttr);
	}

	@Override
	public void delete(AcQuotaAttr acQuotaAttr) throws Exception {
		delete(acQuotaAttr);
	}

	@Override
	public AcQuotaAttr queryById(long id) throws Exception {
		String queryString = "from AcQuotaAttr as model where model.id="+id;
		List<AcQuotaAttr> list = select(AcQuotaAttr.class, queryString);
//		List<AcQuotaAttr> list = (List<AcQuotaAttr>)getHibernateTemplate().find(queryString);
		if(list.size()>0){
			return list.get(0);
		}else{
			return null;
		}
		
	}

	@Override
	public List<AcQuotaAttr> queryByQuotaId(long quotaId) throws Exception {
		String queryString = "from AcQuotaAttr as model where model.quotaId="+quotaId + " order by model.id";
		List<AcQuotaAttr> list = select(AcQuotaAttr.class, queryString);
//		List<AcQuotaAttr> list = (List<AcQuotaAttr>)getHibernateTemplate().find(queryString);
		return list;
	}

	@Override
	public List<AcQuotaAttr> queryAll() throws Exception {
		return select(AcQuotaAttr.class, "from AcQuotaAttr");
	}

}
