package com.beawan.scoreCard.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

import com.beawan.core.BaseDaoImpl;
import com.beawan.scoreCard.bean.FinanceQuotaRelation;
import com.beawan.scoreCard.bean.SubQuotaAttr;
import com.beawan.scoreCard.dao.IFinanceQuotaRelationDAO;
@Repository("financeQuotaRelationDAO")
public class FinanceQuotaRelationDAOImpl  extends BaseDaoImpl<FinanceQuotaRelation> implements
		IFinanceQuotaRelationDAO {
	
	
	@Override
	public FinanceQuotaRelation save(FinanceQuotaRelation financeQuotaRelation)
			throws Exception {
		super.save(financeQuotaRelation);
		return financeQuotaRelation;
	}

	@Override
	public List<FinanceQuotaRelation> queryByAsscardId(long asscardId)
			throws Exception {
		String queryString = "from FinanceQuotaRelation as model where model.assessCardId="+asscardId;
		List<FinanceQuotaRelation> list = select(queryString);
		return list;
	}

	@Override
	public void delete(FinanceQuotaRelation financeQuotaRelation)
			throws Exception {
		super.delete(financeQuotaRelation);
	}

}
