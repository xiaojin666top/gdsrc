package com.beawan.scoreCard.dao.impl;

import com.beawan.core.BaseDaoImpl;
import com.beawan.scoreCard.dao.AcResultDao;
import com.beawan.scoreCard.bean.AcResult;
import org.springframework.stereotype.Repository;
import com.beawan.scoreCard.bean.AcResult;

/**
 * @author yzj
 */
@Repository("acResultDao")
public class AcResultDaoImpl extends BaseDaoImpl<AcResult> implements AcResultDao{

}
