package com.beawan.scoreCard.dao;

import java.util.List;

import com.beawan.core.BaseDao;
import com.beawan.scoreCard.bean.AcConfiguration;


public interface IAcConfigurationDAO extends BaseDao<AcConfiguration>{
		
//	public AcConfiguration saveOrUpdate(AcConfiguration configuration)throws Exception;
	
	public AcConfiguration queryByAcId(long assessCardId)throws Exception;
	
	public List<AcConfiguration> queryByCondition(String queryString)throws Exception;
	
	public void delete(AcConfiguration configuration)throws Exception;
}
