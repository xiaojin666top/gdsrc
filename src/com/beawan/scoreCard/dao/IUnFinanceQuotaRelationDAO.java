package com.beawan.scoreCard.dao;

import java.util.List;

import com.beawan.core.BaseDao;
import com.beawan.scoreCard.bean.UnFinaceQuotaRelation;


public interface IUnFinanceQuotaRelationDAO  extends BaseDao<UnFinaceQuotaRelation>{
	
	public UnFinaceQuotaRelation save(UnFinaceQuotaRelation unFinaceQuotaRelation)throws Exception;
	
	public List<UnFinaceQuotaRelation> queryByAssessCardId(long assessCardId)throws Exception;
	
	public List<UnFinaceQuotaRelation> queryByQuotaId(long quotaId)throws Exception;
	
	public void delete(UnFinaceQuotaRelation unFinaceQuotaRelation)throws Exception;
	
	public List<UnFinaceQuotaRelation> queryAll();
	
}
