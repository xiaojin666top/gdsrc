package com.beawan.scoreCard.dao;

import java.util.List;

import com.beawan.core.BaseDao;
import com.beawan.scoreCard.bean.AcQuota;


public interface IAcQuotaDAO  extends BaseDao<AcQuota>{
	
     public List<AcQuota> queryByCondition( String queryString,Object[] params)throws Exception;
	
	 public List<AcQuota> queryPaginate( String queryString,Object[] params,int start, int limit)throws Exception;

	public List<AcQuota> queryAll()throws Exception;

	public AcQuota queryById(long id)throws Exception;

	public List<AcQuota> queryByPid(long pid)throws Exception;
	
	public List<AcQuota> queryByCondition( String queryString)throws Exception;
}
