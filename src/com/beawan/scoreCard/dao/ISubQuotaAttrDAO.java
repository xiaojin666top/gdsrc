package com.beawan.scoreCard.dao;

import java.util.List;

import com.beawan.core.BaseDao;
import com.beawan.scoreCard.bean.SubQuotaAttr;



public interface ISubQuotaAttrDAO  extends BaseDao<SubQuotaAttr>{
	
	public SubQuotaAttr insert(SubQuotaAttr subQuotaAttr)throws Exception;
	
	public List<SubQuotaAttr> queryByACIdAndQuId(long assessCardId,long subquotaId)throws Exception;
	
	public void delete(SubQuotaAttr subQuotaAttr)throws Exception;
	
	public List<SubQuotaAttr> queryByQuId(long subquotaId) throws Exception;
}
