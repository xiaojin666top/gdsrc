package com.beawan.scoreCard.controller;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.common.BusinessException;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.scoreCard.bean.AcQuota;
import com.beawan.scoreCard.service.IAcQuotaSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.HttpUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;

/**
 * 指标项管理
 * @author 86178
 *
 */
@Controller
@UserSessionAnnotation
@RequestMapping({ "/base/acquota" })
public class AcQuotaCtl extends BaseController{
	private static final Logger log = Logger.getLogger(AcQuotaCtl.class);
	@Resource
	private IAcQuotaSV acQuotaSV;
	
	@RequestMapping("acQuotaList.do")
	public String acQuotaList(HttpServletRequest request, HttpServletResponse response){
		return "views/base/scoreCard/acQuotaList";
	}
	
	@RequestMapping("addQuota.do")
	public String addQuota(HttpServletRequest request, HttpServletResponse response){
		return "views/base/scoreCard/add_quota";
	}
	
	
	
	/**
	 * 保存
	 * @return
	 */
	@RequestMapping("saveQuota.do")
	@ResponseBody
	public ResultDto saveQuota(HttpServletRequest request, HttpServletResponse response){
		ResultDto re = returnFail("");
		Map<String,Object> model = new HashMap<>();
		try {
			String jsonString = HttpUtil.getAsString(request, "jsonstring");
			jsonString = URLDecoder.decode(jsonString, "utf-8");
			AcQuota acQuota = JacksonUtil.fromJson(jsonString, AcQuota.class);
			
			/**
			 * 判断code是否重复
			 */
			List<AcQuota> acQuotaList = acQuotaSV.queryAll(); //获得所有quota
			if(acQuotaList != null && acQuotaList.size() > 0) {
				List<AcQuota> childrenList = acQuota.getChildrenList();  //待保存的子指标
				if(childrenList != null && childrenList.size() > 0) {
					for(AcQuota acQuota1 : childrenList) {
						for(AcQuota acQuota2 : acQuotaList) {
							if(acQuota1.getCode().equals(acQuota2.getCode())) { //如果存在code重复，直接返回
								model.put("save", false);
								model.put("success", true);
								model.put("code", acQuota1.getCode());
								
								re.setCode(ResultDto.RESULT_CODE_SUCCESS);
								re.setMsg("success");
								re.setRows(model);
								return re;
							}							
						}					
					}
				}
			}			
			
			acQuotaSV.save(acQuota);
			//保存操作日志
//			platformLogSV.saveOperateLog("新增评分卡指标，评分卡指标名称：" + acQuota.getName(),
//					BaseConstants.PlatformLogType.ADD_ASSCARD_QUOTA, HttpUtil.getCurrentUser().getUserNo());
			model.put("save", true);
			model.put("success", true);
			
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("success");
			re.setRows(model);
		} catch (Exception e) {
			model.put("msg", "系统异常，请联系管理员！");
			log.error(e);
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 查看指标详情
	 * @return
	 */
	@RequestMapping("quotaInfo.do")
	public ModelAndView quotaInfo(HttpServletRequest request, HttpServletResponse response){
		ModelAndView mav = new ModelAndView("/views/base/scoreCard/quota_info");
		try {
			long id = HttpUtil.getAsLong(request, "id");
			AcQuota acQuota = acQuotaSV.queryById(id);
			mav.addObject("data", acQuota);
		} catch (Exception e) {
			log.error("查看指标详情系统异常！",e);
			e.printStackTrace();
		}
		return mav;
	}
	/**
	 * 删除指标
	 * @return
	 */
//	@Action(value="delete",results={
//			@Result(name="success",type="json",params={"root","model"})})
	@RequestMapping("deleteQuota.json")
	@ResponseBody
	public ResultDto deleteQuota(HttpServletRequest request, HttpServletResponse response){
		ResultDto re = returnFail("");
		try {
			long id = HttpUtil.getAsLong(request, "id");
			acQuotaSV.deleteNew(id);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		}catch (BusinessException b){
			re.setMsg(b.getMessage());
		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
			re.setMsg("系统异常，请联系管理员！");
		}
		return re;
	}
	
	/**
	 * 进入修改 评级指标页面
	 * @return
	 */
//	@Action(value="modify",results={
//			@Result(name="success",type="freemarker",location="/views/asscard/acquota/modify_quota.html")})
	@RequestMapping("modify.json")
	public ModelAndView modify(HttpServletRequest request, HttpServletResponse response){
		ModelAndView mav = new ModelAndView("/views/base/scoreCard/modify_quota");
		try {
			long quotaId = HttpUtil.getAsLong(request, "id");
			AcQuota acQuota = acQuotaSV.queryById(quotaId);
			mav.addObject("data", acQuota);
			
		} catch (Exception e) {
			log.error("进入指标修改页面系统异常！",e);
			e.printStackTrace();
		}
		return mav;
	}
	
//	@Action(value="update",results={
//			@Result(name="success",type="json",params={"root","model"})})
	/**
	 * 保存 修改指标的操作
	 * @return
	 */
	@RequestMapping("update.json")
	@ResponseBody
	public ResultDto update(HttpServletRequest request, HttpServletResponse response){
		ResultDto re = returnFail("");
		try {
			String jsonString = HttpUtil.getAsString(request, "jsonstring");
			jsonString = URLDecoder.decode(jsonString, "utf-8");
			AcQuota acQuota = JacksonUtil.fromJson(jsonString, AcQuota.class);
			
			/**
			 * 判断code是否重复
			 */
			List<AcQuota> acQuotaList = acQuotaSV.queryAll(); //获得所有quota
			if(acQuotaList != null && acQuotaList.size() > 0) {
				List<AcQuota> childrenList = acQuota.getChildrenList();  //待保存的子指标
				if(childrenList != null && childrenList.size() > 0) {
					for(AcQuota acQuota1 : childrenList) {
						for(AcQuota acQuota2 : acQuotaList) {
							if(acQuota1.getId() != null) {
								if(acQuota1.getId().equals(acQuota2.getId()))  //跳过同一个quotaAttr
									continue;
							}
							if(acQuota1.getCode().equals(acQuota2.getCode())) { //如果存在code重复，直接返回
								re.setMsg("存在指标编码重复，修改失败");
								return re;
							}							
						}					
					}
				}
			}		
			
			acQuotaSV.update(acQuota);
			//保存操作日志
//			platformLogSV.saveOperateLog("修改评分卡指标，评分卡指标名称：" + acQuota.getName(),
//					BaseConstants.PlatformLogType.MODIFY_ASSCARD_QUOTA, HttpUtil.getCurrentUser().getUserNo());
			re = returnSuccess();
		} catch (Exception e) {
//			model.put("msg", "系统异常，请联系管理员！");
			re.setMsg("系统异常，请联系管理员！");
			log.error(e);
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 删除子指标
	 * @return
	 */
	@RequestMapping("deleteSubQuota.json")
	@ResponseBody
	public ResultDto delete(HttpServletRequest request, HttpServletResponse response){
		ResultDto re = returnFail("");
		try {
			long id = HttpUtil.getAsLong(request, "id");
			acQuotaSV.deleteNew(id);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("删除成功！");
		}catch (BusinessException b){
			re.setMsg(b.getMessage());
		} catch (Exception e) {
			re.setMsg("系统异常，请联系管理员！");
			log.error(e);
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 获得评级指标列表
	 * @param request
	 * @param response
	 * @param search
	 * @param order
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping("getAcQuotaList.do")
	@ResponseBody
	public String getAcQuotaList(HttpServletRequest request, HttpServletResponse response,String search, String order,int page, int rows){
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			AcQuota condition = null;
			if(!StringUtil.isEmptyString(search)){
				condition = JacksonUtil.fromJson(search, AcQuota.class);
			}
			
			long totalCount = acQuotaSV.totalCount(condition);
			List<AcQuota> list =  acQuotaSV.paginate(condition, order, page-1, rows);
			json.put("rows", list);
			json.put("total", totalCount);
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", "查询指标项列表出错！");
			log.error("查询指标项列表出错", e);
		}
		return jRequest.serialize(json, true);
	}
	
}
