package com.beawan.scoreCard.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.scoreCard.bean.AcLogRecord;
import com.beawan.scoreCard.qbean.QueryCondition;
import com.beawan.scoreCard.service.IAcLogRecordSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;

@Controller
@UserSessionAnnotation
@RequestMapping({ "/base/acrecord" })
public class AcLogRecordCtl {
	private static final Logger log = Logger.getLogger(AcLogRecordCtl.class);
	@Resource
	private IAcLogRecordSV acLogRecordSV;
	/**
	 * 跳转到评分卡记录列表
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("acLogRecordList.do")
	public String acLogRecordList(HttpServletRequest request, HttpServletResponse response){
		return "views/base/scoreCard/acRecordList";
	}
	
	/**
	 * 查询日志流水列表
	 * @param request
	 * @param response
	 * @param search
	 * @param order
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping("getLogRecordList.do")
	@ResponseBody
	public String getLogRecordList(HttpServletRequest request, HttpServletResponse response,String search, String order,int page, int rows){
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			QueryCondition condition = null;
			if(!StringUtil.isEmptyString(search)){
				condition = JacksonUtil.fromJson(search, QueryCondition.class);
			}
			long totalCount = acLogRecordSV.totalCount(condition);
			List<AcLogRecord> list =  acLogRecordSV.paginate(condition, order, page-1, rows);
			json.put("rows", list);
			json.put("total", totalCount);
			json.put("result", true);
		}catch (Exception e) {
			json.put("result", false);
			json.put("msg", "查询日志流水出错！");
			log.error("查询日志流水出错", e);
		}
		return jRequest.serialize(json, true);
	}
	
	//////这个用不到增删改/////
}
