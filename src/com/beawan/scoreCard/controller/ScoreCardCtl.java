package com.beawan.scoreCard.controller;

import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.common.BusinessException;
import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.scoreCard.bean.AcQuota;
import com.beawan.scoreCard.bean.AcQuotaAttr;
import com.beawan.scoreCard.bean.AssessCard;
import com.beawan.scoreCard.service.IAcQuotaAttrSV;
import com.beawan.scoreCard.service.IAcQuotaSV;
import com.beawan.scoreCard.service.IAssessCardSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.HttpUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;


@Controller
@UserSessionAnnotation
@RequestMapping({ "/base/scorecard" })
public class ScoreCardCtl extends BaseController{
	private static final Logger log = Logger.getLogger(ScoreCardCtl.class);
	
	@Resource
	private IAssessCardSV assessCardSV;
	@Resource
	private IAcQuotaSV acQuotaSV;
	@Resource
	private IAcQuotaAttrSV acQuotaAttrSV;
	
	
	/**
	 * 跳转到信用评级列表的页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("scoreCardList.do")
	public String scoreCardList(HttpServletRequest request, HttpServletResponse response){
		return "views/base/scoreCard/scoreCardList";
	}
	
	/**
	 * 查询信用评级的列表
	 * @param request
	 * @param response
	 * @param search
	 * @param order
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping("getScoreCardList.do")
	@ResponseBody
	public String getScoreCardList(HttpServletRequest request, HttpServletResponse response,String search, String order,int page, int rows){
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			AssessCard condition = null;
			if(!StringUtil.isEmptyString(search)){
				condition = JacksonUtil.fromJson(search, AssessCard.class);
			}
			
			long totalCount = assessCardSV.queryAssessCardTotal(condition);
			List<AssessCard> list =  assessCardSV.queryAssessCardPaging(condition, order, page-1, rows);
			json.put("rows", list);
			json.put("total", totalCount);
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", "查询信用评级列表出错！");
			log.error("查询信用评级列表出错", e);
		}
		return jRequest.serialize(json, true);
	}
	

	@RequestMapping("viewInfo.do")
	public ModelAndView viewInfo(HttpServletRequest request, HttpServletResponse response){
		ModelAndView mav = new ModelAndView("/views/base/scoreCard/assesscard/assessCard_info2");
		try {
			long id = HttpUtil.getAsLong(request, "id");
			AssessCard assessCard = assessCardSV.queryInfoById2(id);
			mav.addObject("data", assessCard);
		} catch (Exception e) {
			log.error("查看贷款评级模型系统异常！",e);
			e.printStackTrace();
		}
		return mav;
	}
	
	/**
	 * 跳转到新增评分卡界面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("addCard.do")
	public String addCard(HttpServletRequest request, HttpServletResponse response){
		return "views/base/scoreCard/add_scoreCard";
	}
	
	/**
	 * 提交保存新的评分卡
	 * @return
	 */
	@RequestMapping("saveSubmit.json")
	@ResponseBody
	public ResultDto saveSubmit(HttpServletRequest request, HttpServletResponse response){
		ResultDto re = returnFail("");
		try {
			String jsonString = HttpUtil.getAsString(request, "jsonstring");
			jsonString = URLDecoder.decode(jsonString, "utf-8");
			AssessCard assessCard = JacksonUtil.fromJson(jsonString, AssessCard.class);
			if(assessCard.getId()==null){
				assessCardSV.saveAssessCard2(assessCard, HttpUtil.getCurrentUser().getUserId());
				//保存操作日志
//				platformLogSV.saveOperateLog("新增评分卡，评分卡名称：" + assessCard.getName(),
//						BaseConstants.PlatformLogType.ADD_ASS_CARD, HttpUtil.getCurrentUser().getUserNo());
			}else{
				assessCardSV.modifyAssessCard2(assessCard);
				//保存操作日志
//				platformLogSV.saveOperateLog("修改评分卡，评分卡名称：" + assessCard.getName(),
//						BaseConstants.PlatformLogType.MODIFY_ASS_CARD, HttpUtil.getCurrentUser().getUserNo());
			}
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		} catch (Exception e) {
			re.setMsg("系统异常，请联系管理员！");
			log.error(e);
			e.printStackTrace();
		}
		return re;
	}
	
	@RequestMapping("getQuotaList.json")
	@ResponseBody
	public ResultDto getQuotaList(HttpServletRequest request, HttpServletResponse response){
		ResultDto re = returnFail("");
		try {
			String loanType =  HttpUtil.getAsString(request, "loanType");
			List<AcQuota> quotaList = acQuotaSV.queryByLoanType(loanType);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setRows(quotaList);
		} catch (Exception e) {
			re.setMsg("获取指标模板系统异常！");
			log.error("获取指标模板系统异常！",e);
			e.printStackTrace();
		}
		return re;
	}
	
	@RequestMapping("subQuotaListHtml.json")
	@ResponseBody
	public ResultDto subQuotaListHtml(HttpServletRequest request, HttpServletResponse response){
		ResultDto re = returnFail("");
		try {
			long pid = HttpUtil.getAsLong(request, "pid");
			List<AcQuota> quotaList = acQuotaSV.queryByPid(pid);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setRows(quotaList);
		} catch (Exception e) {
			re.setMsg("获取子指标模板系统异常！");
			log.error("获取子指标模板系统异常！",e);
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 获取子指标对应的情况
	 * @return
	 */
	@RequestMapping("getQuotaAttrList.json")
	@ResponseBody
	public ResultDto getQuotaAttrList(HttpServletRequest request, HttpServletResponse response){
		ResultDto re = returnFail("");
		try {
			long subquotaId = HttpUtil.getAsLong(request, "subquotaId");
			List<AcQuotaAttr> list = acQuotaAttrSV.queryBySubquotaId(subquotaId);
			re.setRows(list);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			re.setMsg("获取子指标对应数据");
		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
		}
		return re;
	}
	
	//
	@RequestMapping("getQuotaHtml.do")
	public ModelAndView getQuotaHtml(HttpServletRequest request, HttpServletResponse response){
		ModelAndView mav = new ModelAndView("/views/base/scoreCard/quota_add_temp");
		try {
			String loanType =  HttpUtil.getAsString(request, "loanType");
			List<AcQuota> quotaList = acQuotaSV.queryByLoanType(loanType);
			mav.addObject(quotaList);
		} catch (Exception e) {
			log.error("获取指标模板系统异常！",e);
			e.printStackTrace();
		}
		return mav;
	}
	
	
	/**
	 * 删除评分卡
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("deleteCard.json")
	@ResponseBody
	public ResultDto deleteCard(HttpServletRequest request, HttpServletResponse response){
		ResultDto re = returnFail("");
		try {
			long id = HttpUtil.getAsLong(request, "id");
			assessCardSV.deleteAssessCard2(id);
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
		}catch (BusinessException b){
			re.setMsg(b.getMessage());
		} catch (Exception e) {
			re.setMsg("系统异常，请联系管理员！");
			log.error(e);
			e.printStackTrace();
		}
		return re;
	}
	
	/** 跳转到评分卡修改页面 **/
	@RequestMapping("modifyCard.do")
	public ModelAndView modifyCard(HttpServletRequest request, HttpServletResponse response){
		ModelAndView mav = new ModelAndView("/views/base/scoreCard/assesscard/assessCard_modify2");
		try {
			long id = HttpUtil.getAsLong(request, "id");
			AssessCard assessCard = assessCardSV.queryInfoById2(id);
			List<AcQuota> list = acQuotaSV.queryByLoanType(assessCard.getLoanType());
			mav.addObject("quotaList", list);
			mav.addObject("data", assessCard);
		} catch (Exception e) {
			log.error("进入贷款评级模型修改页面系统异常！",e);
			e.printStackTrace();
		}
		return mav;
	}
	
	@RequestMapping("/calculateAssessCard.do")
	@ResponseBody
	public String calculateAssessCard(){
		String result = "";
		
		
		return result; 
	}
	
	
}
