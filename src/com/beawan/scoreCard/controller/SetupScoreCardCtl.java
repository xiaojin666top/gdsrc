package com.beawan.scoreCard.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.beawan.common.annotation.UserSessionAnnotation;
import com.beawan.core.BaseController;
import com.beawan.core.ResultDto;
import com.beawan.scoreCard.bean.AcConfiguration;
import com.beawan.scoreCard.bean.AssessCard;
import com.beawan.scoreCard.service.IAcConfigurationSV;
import com.beawan.scoreCard.service.ISmallAssessCardSV;
import com.beawan.web.IRequestSerializer;
import com.beawan.web.impl.JsonRequestSerializer;
import com.platform.util.HttpUtil;
import com.platform.util.JacksonUtil;
import com.platform.util.StringUtil;

import jxl.common.Logger;
@Controller
@UserSessionAnnotation
@RequestMapping({ "/base/setupscore" })
public class SetupScoreCardCtl extends BaseController{
	private static final Logger log = Logger.getLogger(SetupScoreCardCtl.class);
	@Resource
	private ISmallAssessCardSV smallAssessCardSV;
	@Resource
	private IAcConfigurationSV acConfigurationSV;
	
	/**
	 * 跳转到信用评级列表的页面
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("setupScoreCardList.do")
	public String setupScoreCardList(HttpServletRequest request, HttpServletResponse response){
		return "views/base/scoreCard/setupcard/setupScoreCardList";
	}
	
	/**
	 * 查询信用评级的列表
	 * @param request
	 * @param response
	 * @param search
	 * @param order
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping("getsetupScoreCardList.do")
	@ResponseBody
	public String getSetupScoreCardList(HttpServletRequest request, HttpServletResponse response,String search, String order,int page, int rows){
		IRequestSerializer jRequest = new JsonRequestSerializer(request, response);
		Map<String, Object> json = new HashMap<String, Object>();
		try {
			AssessCard condition = null;
			if(!StringUtil.isEmptyString(search)){
				condition = JacksonUtil.fromJson(search, AssessCard.class);
			}
			
			long totalCount = smallAssessCardSV.totalCountQuery(condition);
			List<AssessCard> list =  smallAssessCardSV.paginateQuery(condition, order, page-1, rows);
			json.put("rows", list);
			json.put("total", totalCount);
			json.put("result", true);
		} catch (Exception e) {
			json.put("result", false);
			json.put("msg", "查询信用评级列表出错！");
			log.error("查询信用评级列表出错", e);
		}
		return jRequest.serialize(json, true);
	}
	
	
	@RequestMapping("setupInfo.do")
	public ModelAndView setupInfo(HttpServletRequest request, HttpServletResponse response){
		ModelAndView mav = new ModelAndView("/views/base/scoreCard/setupcard/setup_info");
		try {
			long id  = HttpUtil.getAsLong(request, "id");
			AssessCard assessCard = smallAssessCardSV.queryById(id);
			AcConfiguration configuration = acConfigurationSV.queryByAcId(id);		
			mav.addObject("assessCard", assessCard);	
			mav.addObject("configuration", configuration);
			
		} catch (Exception e) {
			log.error("进入评分卡设置详情页面系统异常！",e);
			e.printStackTrace();
		}
		return mav;
	}

	/**
	 * 进入评分卡设置详情页面系统异常！
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("setup.do")
	public ModelAndView modifyInfo(HttpServletRequest request, HttpServletResponse response){
		ModelAndView mav = new ModelAndView("/views/base/scoreCard/setupcard/setup");
		try {
			long id  = HttpUtil.getAsLong(request, "id");
			AssessCard assessCard = smallAssessCardSV.queryById(id);
			AcConfiguration configuration = acConfigurationSV.queryByAcId(id);		
			mav.addObject("assessCard", assessCard);
			mav.addObject("configuration", configuration);
			
		} catch (Exception e) {
			log.error("进入评分卡设置详情页面系统异常！",e);
			e.printStackTrace();
		}
		return mav;
	}
	
	@RequestMapping("saveSubmit.do")
	@ResponseBody
	public ResultDto saveSubmit(String jsonString, HttpServletRequest request){
		ResultDto re = returnFail("保存评级等级设置失败");
		
		try {
			AcConfiguration configuration = JacksonUtil.fromJson(jsonString, AcConfiguration.class);
			
			acConfigurationSV.saveOrUpdate(configuration);
			
			re.setCode(ResultDto.RESULT_CODE_SUCCESS);
			
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return re;
	}
	
}
